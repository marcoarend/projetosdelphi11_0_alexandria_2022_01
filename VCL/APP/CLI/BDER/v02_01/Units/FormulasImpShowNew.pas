unit FormulasImpShowNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, (*DBTables,*) Buttons, UnInternalConsts, mySQLDbTables,
  ExtCtrls, Grids, DBGrids, ComCtrls, frxClass, frxDBSet, frxDMPExport,
  dmkGeral, MyListas, AppListas, UnDmkEnums, UnMyPrinters, UnDmkProcFunc,
  UnPQ_PF, UnVS_EFD_ICMS_IPI, UnAppEnums;

type
  THSV = record  // hue saturation value (HSV)
           Hue, Sat, Val: Double;
         end;
  TFrFormulasImpShowNew = class(TForm)
    QrFormulas: TmySQLQuery;
    QrFormulasNumero: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrFormulasClienteI: TIntegerField;
    QrFormulasTipificacao: TIntegerField;
    QrFormulasDataI: TDateField;
    QrFormulasDataA: TDateField;
    QrFormulasTecnico: TWideStringField;
    QrFormulasTempoR: TIntegerField;
    QrFormulasTempoP: TIntegerField;
    QrFormulasTempoT: TIntegerField;
    QrFormulasHorasR: TIntegerField;
    QrFormulasHorasP: TIntegerField;
    QrFormulasHorasT: TIntegerField;
    QrFormulasHidrica: TIntegerField;
    QrFormulasLinhaTE: TIntegerField;
    QrFormulasCaldeira: TIntegerField;
    QrFormulasSetor: TIntegerField;
    QrFormulasEspessura: TIntegerField;
    QrFormulasQtde: TFloatField;
    QrFormulasLk: TIntegerField;
    QrFormulasDataCad: TDateField;
    QrFormulasDataAlt: TDateField;
    QrFormulasUserCad: TIntegerField;
    QrFormulasUserAlt: TIntegerField;
    QrFormulasIts: TmySQLQuery;
    Panel1: TPanel;
    Button1: TButton;
    BitBtn1: TBitBtn;
    Button2: TButton;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Edit1: TEdit;
    QrPages: TmySQLQuery;
    QrPagesPAGINAS: TIntegerField;
    QrView1: TmySQLQuery;
    QrView1Campo: TIntegerField;
    QrView1Valor: TWideStringField;
    QrView1Prefixo: TWideStringField;
    QrView1Sufixo: TWideStringField;
    QrView1Conta: TIntegerField;
    QrView1DBand: TIntegerField;
    QrView1DTopo: TIntegerField;
    QrView1DMEsq: TIntegerField;
    QrView1DComp: TIntegerField;
    QrView1DAltu: TIntegerField;
    QrView1Set_N: TIntegerField;
    QrView1Set_I: TIntegerField;
    QrView1Set_U: TIntegerField;
    QrView1Set_T: TIntegerField;
    QrView1Set_A: TIntegerField;
    QrView1Tipo: TIntegerField;
    QrView1Substituicao: TWideStringField;
    QrView1Substitui: TSmallintField;
    QrView1Nulo: TSmallintField;
    QrView1Forma: TIntegerField;
    QrView1Linha: TIntegerField;
    QrView1Colun: TIntegerField;
    QrView1Pagin: TIntegerField;
    QrTopos1: TmySQLQuery;
    QrTopos1DTopo: TIntegerField;
    QrTopos1Tipo: TIntegerField;
    QrImprimeBand: TmySQLQuery;
    QrImprimeBandCodigo: TIntegerField;
    QrImprimeBandControle: TIntegerField;
    QrImprimeBandTipo: TIntegerField;
    QrImprimeBandDataset: TIntegerField;
    QrImprimeBandPos_Topo: TIntegerField;
    QrImprimeBandPos_Altu: TIntegerField;
    QrImprimeBandNome: TWideStringField;
    QrImprimeBandLk: TIntegerField;
    QrImprimeBandDataCad: TDateField;
    QrImprimeBandDataAlt: TDateField;
    QrImprimeBandUserCad: TIntegerField;
    QrImprimeBandUserAlt: TIntegerField;
    QrImprimeBandCol_Qtd: TIntegerField;
    QrImprimeBandCol_Lar: TIntegerField;
    QrImprimeBandCol_GAP: TIntegerField;
    QrImprimeBandOrdem: TIntegerField;
    QrImprimeBandLinhas: TIntegerField;
    QrImprimeBandRepetencia: TSmallintField;
    QrImprime: TmySQLQuery;
    QrImprimeCodigo: TIntegerField;
    QrImprimeNome: TWideStringField;
    QrImprimeNomeFonte: TWideStringField;
    QrImprimeMSupDOS: TIntegerField;
    QrImprimePos_MSup2: TIntegerField;
    QrImprimePos_MEsq: TIntegerField;
    DsImprime: TDataSource;
    DsImprimeBand: TDataSource;
    QrFormulasNOMESETOR: TWideStringField;
    QrFormulasNOMECI: TWideStringField;
    QrLFormItsA: TMySQLQuery;
    QrLFormItsALinha: TIntegerField;
    QrLFormItsAEtapa: TIntegerField;
    QrLFormItsANomeEtapa: TWideStringField;
    QrLFormItsATempoP: TIntegerField;
    QrLFormItsATempoR: TIntegerField;
    QrLFormItsAObservacos: TWideStringField;
    QrLFormItsAPeso: TFloatField;
    QrLFormItsADescricao: TWideStringField;
    QrLFormItsAPERC_TXT: TWideStringField;
    QrLFormItsAPESO_TXT: TWideStringField;
    QrLFormItsATEXT_TXT: TWideStringField;
    QrLFormItsATEMPO_TXT: TWideStringField;
    QrLFormItsASubEtapa: TIntegerField;
    QrLFormItsADiluicao: TFloatField;
    QrLFormItsATXT_DILUICAO: TWideStringField;
    QrLFormItsAGraus: TFloatField;
    QrLFormItsAInsumo: TIntegerField;
    QrLFormItsAObserProce: TWideStringField;
    QrPreuso: TmySQLQuery;
    QrPreusoPeso_PQ: TFloatField;
    QrPreusoProduto: TIntegerField;
    QrPreusoPQCI: TIntegerField;
    QrPreusoProdutoCI: TIntegerField;
    QrPreusoNomePQ: TWideStringField;
    QrPreusoPeso: TFloatField;
    QrPreusoPESOFUTURO: TFloatField;
    QrPreusoCli_Orig: TIntegerField;
    QrPreusoNOMECLI_ORIG: TWideStringField;
    QrPreusoDEFASAGEM: TFloatField;
    QrPreusoCodigo: TIntegerField;
    QrPreusoControle: TIntegerField;
    QrPreusoCustoPadrao: TFloatField;
    QrPreusoMoedaPadrao: TSmallintField;
    QrBaixa: TmySQLQuery;
    QrBaixaControle: TIntegerField;
    QrEmitIts: TmySQLQuery;
    QrEmitItsCodigo: TIntegerField;
    QrEmitItsControle: TIntegerField;
    QrEmitItsNomePQ: TWideStringField;
    QrEmitItsAtivo: TSmallintField;
    QrEmitItsPQCI: TIntegerField;
    QrEmitItsCustoPadrao: TFloatField;
    QrEmitItsMoedaPadrao: TIntegerField;
    QrEmitItsNumero: TIntegerField;
    QrEmitItsPorcent: TFloatField;
    QrEmitItsProduto: TIntegerField;
    QrEmitItsTempoR: TIntegerField;
    QrEmitItsTempoP: TIntegerField;
    QrEmitItsObs: TWideStringField;
    QrEmitItsCusto: TFloatField;
    QrEmitItsGraus: TFloatField;
    QrEmitItsBoca: TWideStringField;
    QrEmitItsProcesso: TWideStringField;
    QrEmitItsObsProces: TWideStringField;
    QrEmitItsLk: TIntegerField;
    QrEmitItsDataCad: TDateField;
    QrEmitItsDataAlt: TDateField;
    QrEmitItsUserCad: TIntegerField;
    QrEmitItsUserAlt: TIntegerField;
    QrEmitItsOrdem: TIntegerField;
    QrEmitItsPeso_PQ: TFloatField;
    QrEmitItsProdutoCI: TIntegerField;
    QrEmitItsCli_Orig: TIntegerField;
    QrEmitItsCli_Dest: TIntegerField;
    QrEmitItsPeso: TFloatField;
    QrEmitItsCUSTOKG: TFloatField;
    QrEmitItsNOMECLI_ORIG: TWideStringField;
    QrPreusoEXISTE: TFloatField;
    frxRec2: TfrxReport;
    frxDsFormulasIts: TfrxDBDataset;
    frxDsFormulas: TfrxDBDataset;
    frxRec1: TfrxReport;
    frxBMZ: TfrxReport;
    frxDsBMZA: TfrxDBDataset;
    frxDsLFormIts: TfrxDBDataset;
    frxPesagemA: TfrxReport;
    frxPreuso: TfrxReport;
    frxDsPreUso: TfrxDBDataset;
    QrEmitItspH_Min: TFloatField;
    QrEmitItspH_Max: TFloatField;
    QrEmitItsBe_Min: TFloatField;
    QrEmitItsBe_Max: TFloatField;
    QrLFormItsApH_Min: TFloatField;
    QrLFormItsApH_Max: TFloatField;
    QrLFormItsABe_Min: TFloatField;
    QrLFormItsABe_Max: TFloatField;
    QrLFormItsAGC_Min: TFloatField;
    QrLFormItsAGC_Max: TFloatField;
    frxRec10: TfrxReport;
    QrLFormItsACustoKg: TFloatField;
    QrLFormItsACustoIt: TFloatField;
    QrLFormItsAMOSTRA_LINHA: TBooleanField;
    QrEmitItsGC_Min: TFloatField;
    QrEmitItsGC_Max: TFloatField;
    QrEmitItsPRECO_PQ: TFloatField;
    QrEmitItsCUSTO_PQ: TFloatField;
    QrLFormItsAPorcent: TFloatField;
    QrLFormItsATEMPO_R_TXT: TWideStringField;
    QrLFormItsAMOSTRA_GRADE: TFloatField;
    QrLotes: TmySQLQuery;
    QrFulonadas: TmySQLQuery;
    frxDsLotes: TfrxDBDataset;
    QrLotesControle: TIntegerField;
    QrLotesMPIn: TIntegerField;
    QrLotesPeso: TFloatField;
    QrLotesPecas: TFloatField;
    QrLotesFicha: TIntegerField;
    QrLotesMarca: TWideStringField;
    QrLotesLote: TWideStringField;
    QrFulonadasLote: TWideStringField;
    QrFulonadasPesoF: TFloatField;
    QrFulonadasPecasF: TFloatField;
    QrFulonadasPesoL: TFloatField;
    QrFulonadasPecasL: TFloatField;
    QrFulonadasNOMECLII: TWideStringField;
    frxDsFulonadas: TfrxDBDataset;
    QrFulonadasControle: TIntegerField;
    QrSumFul: TmySQLQuery;
    frxDsSumFul: TfrxDBDataset;
    frxBMZMini: TfrxReport;
    QrLFormItsADESCRI_TEMP: TWideStringField;
    frxRec1_MS: TfrxReport;
    frxDotMatrixExport1: TfrxDotMatrixExport;
    frxReport1: TfrxReport;
    frxRec1_ML: TfrxReport;
    QrPreusoAtivo: TSmallintField;
    frxRecIdeal: TfrxReport;
    QrEmitItsDiluicao: TFloatField;
    QrEmitItsMinimo: TFloatField;
    QrEmitItsMaximo: TFloatField;
    QrEmitItsCustoMedio: TFloatField;
    QrPreusoCustoMedio: TFloatField;
    QrReEmit: TmySQLQuery;
    QrReEmitCodigo: TIntegerField;
    QrReEmitControle: TIntegerField;
    QrReEmitNomePQ: TWideStringField;
    QrReEmitPQCI: TIntegerField;
    QrReEmitCli_Orig: TIntegerField;
    QrReEmitCli_Dest: TIntegerField;
    QrReEmitCustoMedio: TFloatField;
    QrReEmitCustoPadrao: TFloatField;
    QrReEmitMoedaPadrao: TIntegerField;
    QrReEmitNumero: TIntegerField;
    QrReEmitOrdem: TIntegerField;
    QrReEmitPorcent: TFloatField;
    QrReEmitProduto: TIntegerField;
    QrReEmitTempoR: TIntegerField;
    QrReEmitTempoP: TIntegerField;
    QrReEmitpH: TFloatField;
    QrReEmitpH_Min: TFloatField;
    QrReEmitpH_Max: TFloatField;
    QrReEmitBe: TFloatField;
    QrReEmitBe_Min: TFloatField;
    QrReEmitBe_Max: TFloatField;
    QrReEmitGC_Min: TFloatField;
    QrReEmitGC_Max: TFloatField;
    QrReEmitObs: TWideStringField;
    QrReEmitDiluicao: TFloatField;
    QrReEmitCusto: TFloatField;
    QrReEmitMinimo: TFloatField;
    QrReEmitMaximo: TFloatField;
    QrReEmitGraus: TFloatField;
    QrReEmitBoca: TWideStringField;
    QrReEmitProcesso: TWideStringField;
    QrReEmitObsProces: TWideStringField;
    QrReEmitPeso_PQ: TFloatField;
    QrReEmitProdutoCI: TIntegerField;
    QrReEmitGraGruX: TIntegerField;
    QrReEmitGramasTi: TFloatField;
    QrReEmitGramasKg: TFloatField;
    QrReEmitGramasTo: TFloatField;
    QrReEmitSiglaMoeda: TWideStringField;
    QrReEmitPrecoMo_Kg: TFloatField;
    QrReEmitCotacao_Mo: TFloatField;
    QrReEmitCustoRS_Kg: TFloatField;
    QrReEmitCustoRS_To: TFloatField;
    QrReEmitOrdemFlu: TIntegerField;
    QrReEmitOrdemIts: TIntegerField;
    QrReEmitTintas: TIntegerField;
    QrReEmitLk: TIntegerField;
    QrReEmitDataCad: TDateField;
    QrReEmitDataAlt: TDateField;
    QrReEmitUserCad: TIntegerField;
    QrReEmitUserAlt: TIntegerField;
    QrReEmitAlterWeb: TSmallintField;
    QrReEmitAtivo: TSmallintField;
    QrPEEmit: TmySQLQuery;
    QrPEEmitPESO_PQ: TFloatField;
    QrPEEmitPRECO_PQ: TFloatField;
    QrPEEmitCUSTO_PQ: TFloatField;
    QrPEEmitCPRODP: TFloatField;
    QrPEEmitCSOLUP: TFloatField;
    QrPEEmitCUSTOP: TFloatField;
    QrPEEmitSEQ: TIntegerField;
    QrPEEmitNome: TWideStringField;
    QrPEEmitPQCI: TIntegerField;
    QrPEEmitCustoMedio: TFloatField;
    QrPEEmitCustoPadrao: TFloatField;
    QrPEEmitMoedaPadrao: TSmallintField;
    QrPEEmitNumero: TIntegerField;
    QrPEEmitOrdem: TIntegerField;
    QrPEEmitControle: TIntegerField;
    QrPEEmitPorcent: TFloatField;
    QrPEEmitProduto: TIntegerField;
    QrPEEmitTempoR: TIntegerField;
    QrPEEmitTempoP: TIntegerField;
    QrPEEmitObs: TWideStringField;
    QrPEEmitSC: TIntegerField;
    QrPEEmitReciclo: TIntegerField;
    QrPEEmitDiluicao: TFloatField;
    QrPEEmitCProd: TFloatField;
    QrPEEmitCSolu: TFloatField;
    QrPEEmitCusto: TFloatField;
    QrPEEmitMinimo: TFloatField;
    QrPEEmitMaximo: TFloatField;
    QrPEEmitGraus: TFloatField;
    QrPEEmitBoca: TWideStringField;
    QrPEEmitProcesso: TWideStringField;
    QrPEEmitObsProces: TWideStringField;
    QrPEEmitReordem: TIntegerField;
    QrPEEmitPRODUTOCI: TIntegerField;
    QrPEEmitpH_Min: TFloatField;
    QrPEEmitpH_Max: TFloatField;
    QrPEEmitBe_Min: TFloatField;
    QrPEEmitBe_Max: TFloatField;
    QrPEEmitAtivo: TSmallintField;
    QrPEEmitGC_Min: TFloatField;
    QrPEEmitGC_Max: TFloatField;
    QrPEEmitpH_All: TWideStringField;
    QrPEEmitBe_All: TWideStringField;
    QrFormulasItsAtivo: TSmallintField;
    QrFormulasItsNome: TWideStringField;
    QrFormulasItsPQCI: TIntegerField;
    QrFormulasItsCustoPadrao: TFloatField;
    QrFormulasItsCustoMedio: TFloatField;
    QrFormulasItsMoedaPadrao: TIntegerField;
    QrFormulasItsProdutoCI: TIntegerField;
    QrFormulasItsNumero: TIntegerField;
    QrFormulasItsOrdem: TIntegerField;
    QrFormulasItsControle: TIntegerField;
    QrFormulasItsReordem: TIntegerField;
    QrFormulasItsProcesso: TWideStringField;
    QrFormulasItsPorcent: TFloatField;
    QrFormulasItsProduto: TIntegerField;
    QrFormulasItsTempoR: TIntegerField;
    QrFormulasItsTempoP: TIntegerField;
    QrFormulasItspH: TFloatField;
    QrFormulasItspH_Min: TFloatField;
    QrFormulasItspH_Max: TFloatField;
    QrFormulasItsBe: TFloatField;
    QrFormulasItsBe_Min: TFloatField;
    QrFormulasItsBe_Max: TFloatField;
    QrFormulasItsGC_Min: TFloatField;
    QrFormulasItsGC_Max: TFloatField;
    QrFormulasItsObs: TWideStringField;
    QrFormulasItsObsProces: TWideStringField;
    QrFormulasItsSC: TIntegerField;
    QrFormulasItsReciclo: TIntegerField;
    QrFormulasItsDiluicao: TFloatField;
    QrFormulasItsCProd: TFloatField;
    QrFormulasItsCSolu: TFloatField;
    QrFormulasItsCusto: TFloatField;
    QrFormulasItsMinimo: TFloatField;
    QrFormulasItsMaximo: TFloatField;
    QrFormulasItsGraus: TFloatField;
    QrFormulasItsBoca: TWideStringField;
    QrFormulasItsEhGGX: TSmallintField;
    QrFormulasItsGraGruX: TIntegerField;
    QrFormulasItsAlterWeb: TSmallintField;
    QrFormulasItsSEQ: TIntegerField;
    QrFormulasItsPESO_PQ: TFloatField;
    QrFormulasItsPRECO_PQ: TFloatField;
    QrFormulasItsCUSTO_PQ: TFloatField;
    QrFormulasItspH_All: TWideStringField;
    QrFormulasItsBe_All: TWideStringField;
    QrFormulasItsInicio: TWideStringField;
    QrFormulasItsFinal: TWideStringField;
    QrFormulasItskgLCusto: TWideStringField;
    QrFormulasItsAtivo_1: TSmallintField;
    frxRec1_BH: TfrxReport;
    QrVMIOri: TmySQLQuery;
    frxDsVMIOri: TfrxDBDataset;
    QrVMIOriCodigo: TLargeintField;
    QrVMIOriControle: TLargeintField;
    QrVMIOriMovimCod: TLargeintField;
    QrVMIOriMovimNiv: TLargeintField;
    QrVMIOriMovimTwn: TLargeintField;
    QrVMIOriEmpresa: TLargeintField;
    QrVMIOriTerceiro: TLargeintField;
    QrVMIOriCliVenda: TLargeintField;
    QrVMIOriMovimID: TLargeintField;
    QrVMIOriDataHora: TDateTimeField;
    QrVMIOriPallet: TLargeintField;
    QrVMIOriGraGruX: TLargeintField;
    QrVMIOriPecas: TFloatField;
    QrVMIOriPesoKg: TFloatField;
    QrVMIOriAreaM2: TFloatField;
    QrVMIOriAreaP2: TFloatField;
    QrVMIOriValorT: TFloatField;
    QrVMIOriSrcMovID: TLargeintField;
    QrVMIOriSrcNivel1: TLargeintField;
    QrVMIOriSrcNivel2: TLargeintField;
    QrVMIOriSrcGGX: TLargeintField;
    QrVMIOriSdoVrtPeca: TFloatField;
    QrVMIOriSdoVrtPeso: TFloatField;
    QrVMIOriSdoVrtArM2: TFloatField;
    QrVMIOriObserv: TWideStringField;
    QrVMIOriSerieFch: TLargeintField;
    QrVMIOriFicha: TLargeintField;
    QrVMIOriMisturou: TLargeintField;
    QrVMIOriFornecMO: TLargeintField;
    QrVMIOriCustoMOKg: TFloatField;
    QrVMIOriCustoMOM2: TFloatField;
    QrVMIOriCustoMOTot: TFloatField;
    QrVMIOriValorMP: TFloatField;
    QrVMIOriDstMovID: TLargeintField;
    QrVMIOriDstNivel1: TLargeintField;
    QrVMIOriDstNivel2: TLargeintField;
    QrVMIOriDstGGX: TLargeintField;
    QrVMIOriQtdGerPeca: TFloatField;
    QrVMIOriQtdGerPeso: TFloatField;
    QrVMIOriQtdGerArM2: TFloatField;
    QrVMIOriQtdGerArP2: TFloatField;
    QrVMIOriQtdAntPeca: TFloatField;
    QrVMIOriQtdAntPeso: TFloatField;
    QrVMIOriQtdAntArM2: TFloatField;
    QrVMIOriQtdAntArP2: TFloatField;
    QrVMIOriNotaMPAG: TFloatField;
    QrVMIOriNO_PALLET: TWideStringField;
    QrVMIOriNO_PRD_TAM_COR: TWideStringField;
    QrVMIOriNO_TTW: TWideStringField;
    QrVMIOriID_TTW: TLargeintField;
    QrVMIOriNO_FORNECE: TWideStringField;
    QrVMIOriNO_SerieFch: TWideStringField;
    QrVMIOriReqMovEstq: TLargeintField;
    QrVMIOriNO_ClientMO: TWideStringField;
    QrVMIOriMarca: TWideStringField;
    QrVMIOriClientMO: TLargeintField;
    QrFormulasGraCorCad: TIntegerField;
    QrFormulasNoGraCorCad: TWideStringField;
    QrFormulasEspRebaixe: TIntegerField;
    frxRec1A: TfrxReport;
    frxRec1Noroeste: TfrxReport;
    QrPEEmitDensidade: TFloatField;
    QrPEEmitUsarDensid: TSmallintField;
    QrPEEmitLITROS_PQ: TFloatField;
    QrReEmitDensidade: TFloatField;
    QrReEmitUsarDensid: TSmallintField;
    QrReEmitLITROS_PQ: TFloatField;
    QrFormulasItsDensidade: TFloatField;
    QrFormulasItsUsarDensid: TSmallintField;
    QrFormulasItsLITROS_PQ: TFloatField;
    frxRec1Panorama: TfrxReport;
    QrPEEmitNxLotes: TWideStringField;
    QrReEmitNxLotes: TWideStringField;
    QrFormulasItsNxLotes: TWideStringField;
    frxPesagemL: TfrxReport;
    QrLFormItsL: TMySQLQuery;
    QrLFormItsLLinha: TIntegerField;
    QrLFormItsLEtapa: TIntegerField;
    QrLFormItsLNomeEtapa: TWideStringField;
    QrLFormItsLTempoP: TIntegerField;
    QrLFormItsLTempoR: TIntegerField;
    QrLFormItsLObservacos: TWideStringField;
    QrLFormItsLPeso: TFloatField;
    QrLFormItsLDescricao: TWideStringField;
    QrLFormItsLPERC_TXT: TWideStringField;
    QrLFormItsLPESO_TXT: TWideStringField;
    QrLFormItsLTEXT_TXT: TWideStringField;
    QrLFormItsLTEMPO_TXT: TWideStringField;
    QrLFormItsLSubEtapa: TIntegerField;
    QrLFormItsLDiluicao: TFloatField;
    QrLFormItsLTXT_DILUICAO: TWideStringField;
    QrLFormItsLGraus: TFloatField;
    QrLFormItsLInsumo: TIntegerField;
    QrLFormItsLObserProce: TWideStringField;
    QrLFormItsLpH_Min: TFloatField;
    QrLFormItsLpH_Max: TFloatField;
    QrLFormItsLBe_Min: TFloatField;
    QrLFormItsLBe_Max: TFloatField;
    QrLFormItsLGC_Min: TFloatField;
    QrLFormItsLGC_Max: TFloatField;
    QrLFormItsLCustoKg: TFloatField;
    QrLFormItsLCustoIt: TFloatField;
    QrLFormItsLMOSTRA_LINHA: TBooleanField;
    QrLFormItsLPorcent: TFloatField;
    QrLFormItsLTEMPO_R_TXT: TWideStringField;
    QrLFormItsLMOSTRA_GRADE: TFloatField;
    QrLFormItsLDESCRI_TEMP: TWideStringField;
    QrLFormItsLNxLotes: TWideStringField;
    frxDsBMZL: TfrxDBDataset;
    QrNxLotes: TMySQLQuery;
    QrNxLotesNome: TWideStringField;
    QrNxLotesNxLotes: TWideMemoField;
    frxDsNxLotes: TfrxDBDataset;
    frxReceiLotesPQCurta: TfrxReport;
    QrFormulasItsNO_ETAPA: TWideStringField;
    QrNxL: TMySQLQuery;
    QrNxLProduto: TIntegerField;
    QrNxLNxLotes: TWideStringField;
    QrFormulasItsNxLLookup: TWideStringField;
    frxReceiLotesPQLonga: TfrxReport;
    QrEmitItsDataEmis: TDateTimeField;
    QrEmitItsHoraIni: TTimeField;
    QrFormulasNumCODIF: TWideStringField;
    QrFormulasVersao: TIntegerField;
    QrFormulasItsGC_All_2: TWideStringField;
    QrIQs: TMySQLQuery;
    QrIQsSIGLA: TWideStringField;
    QrPEEmitSiglaIQ: TWideStringField;
    QrReEmitSiglaIQ: TWideStringField;
    QrFormulasItsSiglaIQ: TWideStringField;
    QrLFormItsLSiglaIQ: TWideStringField;
    frxRec1LWG2023_06: TfrxReport;
    QrEmitItsCusIntrn: TFloatField;
    QrPreusoCusIntrn: TFloatField;
    QrReEmitCusIntrn: TFloatField;
    QrPEEmitCusIntrn: TFloatField;
    QrFormulasItsCusIntrn: TFloatField;
    procedure Button1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure QrLFormItsACalcFields(DataSet: TDataSet);
    procedure QrPreusoCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure frxRec2GetValue(const VarName: String; var Value: Variant);
    procedure QrEmitItsCalcFields(DataSet: TDataSet);
    procedure QrPEEmitCalcFields(DataSet: TDataSet);
    procedure QrLFormItsLCalcFields(DataSet: TDataSet);
    procedure QrFormulasItsCalcFields(DataSet: TDataSet);
    procedure QrFormulasItsBeforeClose(DataSet: TDataSet);
    procedure QrFormulasItsBeforeOpen(DataSet: TDataSet);
    procedure QrFormulasCalcFields(DataSet: TDataSet);
    procedure FormHide(Sender: TObject);
    // 2014-06-12 - Falta de objetos no dfm!
    //procedure QrEmitCusAfterScroll(DataSet: TDataSet);
    // FIM 2014-06-12
  private
    { Private declarations }
    FSomaT, FLinAtu: Integer;
    FPagina: Integer;
    FArqPrn: TextFile;
    FPrintedLine: Boolean;
    FTamLinha, FAltDados: Integer;
    F_A, F_M, F_T, F_L, F_R, F_W, FPula: Integer;
    FCustoTotal, FCustokg, FCustoM2, FCustoM2Semi: Double;
    FDivisorTop, FDivisorMid, FDivisorBot: String;
    FColunas: array[0..17] of Integer;
    FColMax: Integer;
    FFormulas, FFormulasIts: String;
    //
    FNO_ETAPA: String;
    //
    procedure Insere_Divisor(Separador: String);
    procedure ImprimeMatricial1_1;
    procedure ImprimeMatricial1_2(Mostra: Integer);
    procedure ImprimeMatricial;
    procedure CalculaItensA;
    //procedure CalculaItensB;
    function TextoAImprimir(Campo: Integer; Tipo: Integer): String;
    function TextoAImprimirB(Campo: Integer; Tipo: Integer): String;
    function MeuGetValue(ParName: String): Variant;
    function AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
    function EspacoEntreTextos(QrView: TmySQLQuery): String;
    function Formata(Texto, Prefixo, Sufixo: String; Formato: Integer; Nulo:
             Boolean): String;
    function FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
             Substitui, Alinhamento, Comprimento, Fonte, Formatacao: Integer;
             Nulo: Boolean): String;
    function CS(Texto, Compl: String; Tamanho, Centralizacao: Integer): String;
    //function N2(Valor: Double; Tamanho: Integer): String;
    //function N3(Valor: Double; Tamanho: Integer): String;
    procedure InsereLinha(Tipo: Integer);
    function  InsereLinhaB(Tipo: Integer): String;
    procedure InsereItem(Caption: String; T, L, W, N, I, F: Integer);
    procedure PreparaDivisores;
    function  Emite(Emit: Integer): Boolean;
    function  EfetuaBaixa(QrLotes: TmySQLQuery): Boolean;
    //function  AtualizaCustosEmit(SumC: Double; PreUsoCodigo: Integer): Boolean;

    // 2014-06-12 - Falta de objetos no dfm!
    //procedure ReopenEmitCus();
    //procedure FrxRec1_Mostra();
    //procedure FrxRec1_Imprime();
    // FIM 2014-06-12

  public
    { Public declarations }
    FProgressBar1: TProgressBar;
    FEmit, FCliIntCod, FFoiExpand, FCPI, FTipoPreco, FTipoImprime, FFormula,
    FSetor, FTipific, FCod_Espess, FCod_Rebaix, FCodDefPeca, FSourcMP, FEmitGru, FAllDest,
    FRetrabalho: Integer;
    FReImprime, FCancela, FCalcCustos, FCalcPrecos, FNoCloseAll,
    FLogo0Exists, FMatricialNovo, FGrade: Boolean;
    FEMCM, FPesoCalc, FArea, FPeso, FMedia, FQtde, FSemiAreaM2, FSemiRendim,
    FBRL_USD, FBRL_EUR: Double;
    FDtaCambio: TDateTime;
    FHHMM_P, FHHMM_R, FHHMM_T, FDefPeca, FObs, FFulao, FLinhasRebx, FLinhasSemi,
    FCliIntTxt, FLogo0Path, FEmitGru_TXT: String;
    FDivVert: Char;
    FDataP: TDateTime;
    FDtCorrApo: String;
    FEmpresa: Integer;
    FVSMovCod, FTemIMEIMrt: Integer;
    FHoraIni: TTime;
    FVSMovimSrc: TEstqmovimNiv;
    FTpReceita: TReceitaTipoSetor;

    procedure CalculaReceita(QrLotes: TmySQLQuery);
    procedure ReopenCustos1;
    procedure ReopenCustos2;
    procedure ReopenPreUso(Codigo: Integer);
  end;

var
  FrFormulasImpShowNew: TFrFormulasImpShowNew;

implementation

uses UnMyObjects, Module, Principal, BlueDermConsts, UMySQLModule, PQChange,
  PQx, MyDBCheck, UnGrade_Tabs, ModEmit, DmkDAC_PF, UCreate, ModuleGeral,
  CreateBlueDerm, UnVS_PF;

{$R *.DFM}

const
  FAltLin = CO_POLEGADA / 2.16;
  FLinhaTxt = '________________________________________________________________________________________________________________________________________';
  FFloat_15_2 = '#,###,##0.00;-#,###,##0.00; ';
  FFloat_15_3 = '#,###,##0.000;-#,###,##0.000; ';
  FFloat_15_4 = '#,###,##0.0000;-#,###,##0.0000; ';

procedure TFrFormulasImpShowNew.ReopenCustos1;
begin
  FCustoTotal := 0;
  QrFormulasIts.First;
  while not QrFormulasIts.Eof do
  begin
    FCustoTotal := FCustoTotal + QrFormulasItsCUSTO_PQ.Value;
    QrFormulasIts.Next;
  end;
  //
  if FPeso = 0 then FCustokg := 0 else FCustokg := FCustoTotal / FPeso;
  if FArea = 0 then FCustoM2 := 0 else FCustoM2 := FCustoTotal / FArea;
  if FSemiAreaM2 = 0 then FCustoM2Semi := 0 else FCustoM2Semi := FCustoTotal / FSemiAreaM2;
  //
  QrFormulasIts.First;
end;

procedure TFrFormulasImpShowNew.ReopenCustos2;
begin
  FCustoTotal := 0;
  QrEmitIts.First;
  while not QrEmitIts.Eof do
  begin
    FCustoTotal := FCustoTotal + QrEmitItsCUSTO_PQ.Value;
    QrEmitIts.Next;
  end;
  //
  if FPeso = 0 then FCustokg := 0 else FCustokg := FCustoTotal / FPeso;
  if FArea = 0 then FCustoM2 := 0 else FCustoM2 := FCustoTotal / FArea;
  if FSemiAreaM2 = 0 then FCustoM2Semi := 0 else FCustoM2Semi := FCustoTotal / FSemiAreaM2;
  //
  QrEmitIts.First;
end;

(*    // 2014-06-12 - Falta de objetos no dfm!
procedure TFrFormulasImpShow.ReopenEmitCus();
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrEmitCus, Dmod.MyDB, [
  'SELECT CONCAT(mpi.Texto, " ", mpi.CorTxt) TextoECor,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_CLI,',
  'ecu.* ',
  'FROM emitcus ecu',
  'LEFT JOIN mpvits mpi ON mpi.Controle=ecu.MPVIts',
  'LEFT JOIN mpp mpp ON mpp.Codigo=mpi.Pedido ',
  'LEFT JOIN entidades ent ON ent.Codigo=mpp.Cliente ',
  'WHERE ecu.Codigo=' + Geral.FF0(FEmit),
  '']);
end;
    // FIM 2014-06-12
*)

procedure TFrFormulasImpShowNew.ReopenPreUso(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPreUso, Dmod.MyDB, [
  'SELECT pq.GGXNiv2 Ativo, ems.Codigo, ems.Controle,  ',
  'pqc.MoedaPadrao, ems.ProdutoCI,  ',
  'SUM(ems.Peso_PQ) Peso_PQ, ems.Produto, ems.PQCI,  ',
  'ems.NomePQ, pqc.Peso, (pqc.Peso+999999999999) EXISTE,  ',
  ' ',
  'IF(pqc.CustoPadrao > 0, pqc.CustoPadrao,  ',
  'pqc.Valor/pqc.Peso) CustoPadrao,  ',
  ' ',
  'IF((pqc.Valor/pqc.Peso>0) AND (pqc.Valor>0) AND (pqc.Peso>0),  ',
  'pqc.Valor/pqc.Peso, pqc.CustoPadrao) CustoMedio,  ',
  'pq.CusIntrn, ',
  'ems.Cli_Orig, ',
  'IF(pqc.Peso IS NULL, 0, pqc.Peso) - SUM(ems.Peso_PQ) PESOFUTURO,  ',
  'IF(cli.Tipo=0, cli.RazaoSocial,cli.Nome) NOMECLI_ORIG ',
  'FROM emitits ems ',
  'LEFT JOIN PQCli     pqc ON pqc.Controle=ems.ProdutoCI ',
  'LEFT JOIN PQ     pq ON pq.Codigo=pqc.PQ ',
  'LEFT JOIN Entidades cli ON cli.Codigo=ems.Cli_Orig ',
  'WHERE ems.Ativo <> ' + Geral.FF0(CO_TipoCadPQ_003_Cod_Texto),
  'AND ems.Codigo=' + Geral.FF0(Codigo),
  'AND ems.Produto > 0',
  'GROUP BY ems.Produto, Cli_Orig ',
  ' ']);
  //Geral.MB_SQL(self, QrPreUso);
end;

function TFrFormulasImpShowNew.Formata(Texto, Prefixo, Sufixo: String; Formato: Integer;
 Nulo: Boolean): String;
var
  MeuTexto: String;
begin
  if Trim(Texto) = '' then MeuTexto := Texto else
  begin
    case Formato of
      1: MeuTexto := Geral.TFT(Texto, 0, siNegativo);
      2: MeuTexto := Geral.TFT(Texto, 1, siNegativo);
      3: MeuTexto := Geral.TFT(Texto, 2, siNegativo);
      4: MeuTexto := Geral.TFT(Texto, 3, siNegativo);
      5: MeuTexto := Geral.TFT(Texto, 4, siNegativo);
      6: MeuTexto := FormatDateTime(VAR_FORMATDATE3, StrToDate(Texto));
      7: MeuTexto := FormatDateTime(VAR_FORMATDATE2, StrToDate(Texto));
      8: MeuTexto := FormatDateTime(VAR_FORMATTIME2, StrToTime(Texto));
      9: MeuTexto := FormatDateTime(VAR_FORMATTIME , StrToTime(Texto));
      else MeuTexto := Texto;
    end;
  end;
  if (Formato in ([1,2,3,4,5])) and Nulo then
  begin
    if Geral.DMV(MeuTexto) < 0.0001 then MeuTexto := '';
  end;
  if MeuTexto = '' then Result := '' else
  begin
    if Prefixo<> '' then Prefixo := Prefixo + ' ';
    if Sufixo<> '' then Sufixo := Sufixo + ' ';
    Result := Prefixo+MeuTexto+Sufixo;
  end;
end;

function TFrFormulasImpShowNew.FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
  Substitui, Alinhamento, Comprimento, Fonte, Formatacao: Integer; Nulo:
  Boolean): String;
var
  i, Tam, Letras, CPI, Vazios: Integer;
  Esq: Boolean;
  Txt, Vaz: String;
  Increm: Double;
begin
  if Substitui = 1 then Txt := Substituicao else
  begin
    if Texto = '' then Txt := ''
    else Txt := Formata(Texto, Prefixo, Sufixo, Formatacao, Nulo);
  end;
  if Txt = '' then
  Txt := ' '; // evitar erro de Margem Esquerda ao imprimir ??
  if FCPI = 0 then CPI := 10 else CPI := FCPI;
  Tam := Fonte + (CPI*10);
  Letras := 0;
  case Tam of
    100: Letras := 17;
    101: Letras := 10;
    102: Letras := 05;
    120: Letras := 20;
    121: Letras := 12;
    122: Letras := 06;
    else Geral.MB_Erro('CPI indefinido!');
  end;
  Increm := (CO_POLEGADA * 100) / Letras;
  Letras := Trunc(((Comprimento / 100) / CO_POLEGADA) * Letras);
  FTamLinha := FTamLinha + Trunc(Increm * Letras);
  Vazios := Letras - Length(Txt);
  Esq := True;
  // [0: Esq] [1: Centro] [2: Direita]
  if Alinhamento = 1 then
  begin
    for i := 1 to Vazios do
    begin
      if Esq then
      begin
        Txt := Txt + ' ';
        Esq := False;
      end else begin
        Txt := ' ' + Txt;
        Esq := True;
      end;
    end;
  end else begin
    for i := 1 to Vazios do Vaz := Vaz + ' ';
    if Alinhamento = 0 then Txt := Txt + Vaz else Txt := Vaz + Txt;
  end;
  Result := Txt;
end;

procedure TFrFormulasImpShowNew.Button1Click(Sender: TObject);
begin
  if VAR_SP <> 'Sem Baixa' then
  begin
    //frRec1.PrepareReport;
    //frRec1.SaveToFile('C:\Dermatek\'+FloatToStr(VAR_SPNO)+'.frf');
    MyObjects.frxSalva(frxRec1, 'Receita', 'C:\Dermatek\'+FloatToStr(VAR_SPNO)+'.frf');
  end;
  //frRec1.PrepareReport;
  //frRec1.ShowPreparedReport;
  //
  MyObjects.frxMostra(frxRec1, 'Receita');
end;

procedure TFrFormulasImpShowNew.BitBtn1Click(Sender: TObject);
begin
  Hide;
end;

procedure TFrFormulasImpShowNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFrFormulasImpShowNew.Button2Click(Sender: TObject);
var
  Arquivo: String;
begin
  Arquivo := 'C:\Dermatek\'+Edit1.Text+'.frf';
  if FileExists(Arquivo) then
  begin
    //frRec1.LoadFromFile(Arquivo);
    //frRec1.PrepareReport;
    //frRec1.PrintPreparedReport(CO_VAZIO,1,False, frAll);
    frxRec1.LoadFromFile(Arquivo);
    MyObjects.frxImprime(frxRec1, 'Receita (Arquivo aberto)');
  end else ShowMessage('Arquivo n�o localizado!');
end;

procedure TFrFormulasImpShowNew.CalculaReceita(QrLotes: TmySQLQuery);
  procedure InsereLinhaAtual(Versao: Integer);
  var
    Nome, Processo, Obs, ObsProces, Boca, pH_All, Be_All, Inicio, Final_,
    kgLCusto, L_Agua: String;
    PQCI, MoedaPadrao, ProdutoCI, Numero, Ordem, Controle, Reordem, Produto,
    TempoR, TempoP, SC, Reciclo, EhGGX, GraGruX, SEQ, Ativo, Ativo_1: Integer;
    CustoPadrao, CustoMedio, CusIntrn, Porcent, pH, pH_Min, pH_Max, Be, Be_Min, Be_Max,
    GC_Min, GC_Max, Diluicao, CProd, CSolu, Custo, Minimo, Maximo, Graus,
    PESO_PQ, PRECO_PQ, CUSTO_PQ: Double;
    //
    Densidade, LITROS_PQ: Double;
    UsarDensid: Integer;
    NxLotes, SiglaIQ: String;
    //
    Formato: String;
  begin
    if Versao = 0 then
    begin
      Nome           := QrPEEmitNome       .Value;
      PQCI           := QrPEEmitPQCI       .Value;
      CustoPadrao    := QrPEEmitCustoPadrao.Value;
      CustoMedio     := QrPEEmitCustoMedio .Value;
      CusIntrn       := QrPEEmitCusIntrn.Value;
      MoedaPadrao    := QrPEEmitMoedaPadrao.Value;
      ProdutoCI      := QrPEEmitProdutoCI  .Value;
      Numero         := QrPEEmitNumero     .Value;
      Ordem          := QrPEEmitOrdem      .Value;
      Controle       := QrPEEmitControle   .Value;
      Reordem        := QrPEEmitReordem    .Value;
      Processo       := QrPEEmitProcesso   .Value;
      Porcent        := QrPEEmitPorcent    .Value;
      Produto        := QrPEEmitProduto    .Value;
      TempoR         := QrPEEmitTempoR     .Value;
      TempoP         := QrPEEmitTempoP     .Value;
      pH             := 0;//QrPEEmitpH         .Value;
      pH_Min         := QrPEEmitpH_Min     .Value;
      pH_Max         := QrPEEmitpH_Max     .Value;
      Be             := 0;//QrPEEmitBe         .Value;
      Be_Min         := QrPEEmitBe_Min     .Value;
      Be_Max         := QrPEEmitBe_Max     .Value;
      GC_Min         := QrPEEmitGC_Min     .Value;
      GC_Max         := QrPEEmitGC_Max     .Value;
      Obs            := QrPEEmitObs        .Value;
      ObsProces      := QrPEEmitObsProces  .Value;
      SC             := QrPEEmitSC         .Value;
      Reciclo        := QrPEEmitReciclo    .Value;
      Diluicao       := QrPEEmitDiluicao   .Value;
      CProd          := QrPEEmitCProd      .Value;
      CSolu          := QrPEEmitCSolu      .Value;
      Custo          := QrPEEmitCusto      .Value;
      Minimo         := QrPEEmitMinimo     .Value;
      Maximo         := QrPEEmitMaximo     .Value;
      Graus          := QrPEEmitGraus      .Value;
      Boca           := QrPEEmitBoca       .Value;
      EhGGX          := 0;//QrPEEmitEhGGX      .Value;
      GraGruX        := 0;//QrPEEmitGraGruX    .Value;
      SEQ            := QrPEEmit.RecNo;
      PESO_PQ        := QrPEEmitPESO_PQ .Value;
      PRECO_PQ       := QrPEEmitPRECO_PQ.Value;
      CUSTO_PQ       := QrPEEmitCUSTO_PQ.Value;
      pH_All         := QrPEEmitpH_All  .Value;
      Be_All         := QrPEEmitBe_All  .Value;
      //Inicio         := MeuGetValue('Inicio');
      //Final_         := MeuGetValue('Final');
      //kgLCusto       := MeuGetValue('kgLCusto');
      //
      Densidade      := QrPEEmitDensidade.Value;
      UsarDensid     := QrPEEmitUsarDensid.Value;
      LITROS_PQ      := QrPEEmitLITROS_PQ.Value;
      NxLotes        := QrPEEmitNxLotes.Value;
      SiglaIQ        := QrPEEmitSiglaIQ.Value;
      //
      Ativo_1        := 1;
      Ativo          := QrPEEmitAtivo.Value;
      if FCalcCustos or FCalcPrecos then
      begin
        CProd := CUSTO_PQ;
        CSolu := 0;
        Custo := CUSTO_PQ;
      end else
      begin
        CProd := 0;
        CSolu := 0;
        Custo := 0;
      end;
    end else
    begin
      Nome           := QrReEmitNomePQ.Value;
      PQCI           := QrReEmitPQCI.Value;
      CustoPadrao    := QrReEmitCustoPadrao.Value;
      CustoMedio     := QrReEmitCustoMedio.Value;
      CusIntrn       := QrReEmitCusIntrn.Value;
      MoedaPadrao    := QrReEmitMoedaPadrao.Value;
      ProdutoCI      := QrReEmitProdutoCI.Value;
      Numero         := QrReEmitNumero.Value;
      Ordem          := QrReEmitOrdem.Value;
      Controle       := QrReEmitControle.Value;
      Reordem        := 0;//QrReEmitReordem.Value;
      Processo       := QrReEmitProcesso   .Value;
      Porcent        := QrReEmitPorcent    .Value;
      Produto        := QrReEmitProduto    .Value;
      TempoR         := QrReEmitTempoR     .Value;
      TempoP         := QrReEmitTempoP     .Value;
      pH             := 0;//QrReEmitpH         .Value;
      pH_Min         := QrReEmitpH_Min     .Value;
      pH_Max         := QrReEmitpH_Max     .Value;
      Be             := 0;//QrReEmitBe         .Value;
      Be_Min         := QrReEmitBe_Min     .Value;
      Be_Max         := QrReEmitBe_Max     .Value;
      GC_Min         := QrReEmitGC_Min     .Value;
      GC_Max         := QrReEmitGC_Max     .Value;
      Obs            := QrReEmitObs        .Value;
      ObsProces      := QrReEmitObsProces  .Value;
      SC             := 0;// ???? QrReEmitSC         .Value;
      Reciclo        := 0; //QrReEmitReciclo    .Value;
      Diluicao       := QrReEmitDiluicao   .Value;
      CProd          := 0;
      CSolu          := 0;
      Custo          := 0;
      Minimo         := QrReEmitMinimo     .Value;
      Maximo         := QrReEmitMaximo     .Value;
      Graus          := QrReEmitGraus      .Value;
      Boca           := QrReEmitBoca       .Value;
      EhGGX          := 0;//QrReEmitEhGGX      .Value;
      GraGruX        := 0;//QrReEmitGraGruX    .Value;
      //Fazer no bd via sql!
      PESO_PQ := QrReEmitPorcent.Value / 100 * FPeso;
      if (Produto < 0) and (FTipoPreco <> 2) then
        PRECO_PQ := CusIntrn
      else
      begin
        case FTipoPreco of
          1: // Cadastro manual
            PRECO_PQ := DmModEmit.PrecoEmReais(
              QrReEmitMoedaPadrao.Value,
              QrReEmitCustoPadrao.Value, 0);
          2: // desativado
            PRECO_PQ := 0;
          else   // 0: Estoque Cliente Interno
            PRECO_PQ := QrReEmitCustoMedio.Value;
        end;
      end;
      //
      CUSTO_PQ := QrReEmitPESO_PQ.Value * PRECO_PQ;
      //
      if (QrReEmitpH_Min.Value > 0) and (QrReEmitpH_Max.Value > 0) then
        pH_All := FloatToStr(QrReEmitpH_Min.Value) + '-' + FloatToStr(QrReEmitpH_Max.Value)
      else if (QrReEmitpH_Min.Value > 0) then
        pH_All := FloatToStr(QrReEmitpH_Min.Value)
      else if (QrReEmitpH_Max.Value > 0) then
        pH_All := FloatToStr(QrReEmitpH_Max.Value)
      else
        pH_All := '';

      //

      if (Be_Min > 0) and (Be_Max > 0) then
        Be_All := FloatToStr(Be_Min) + '-' +  FloatToStr(Be_Max)
      else if (Be_Min > 0) then
        Be_All := FloatToStr(Be_Min)
      else if (Be_Max > 0) then
        Be_All := FloatToStr(Be_Max)
      else
        Be_All := '';

      SEQ            := QrReEmit.RecNo;
      //PESO_PQ        := QrReEmitPESO_PQ .Value;
      //PRECO_PQ       := QrReEmitPRECO_PQ.Value;
      //CUSTO_PQ       := QrReEmitCUSTO_PQ.Value;
      //pH_All         := QrReEmitpH_All  .Value;
      //Be_All         := QrReEmitBe_All  .Value;
      Ativo_1        := 1;
      Ativo          := QrReEmitAtivo.Value;
      CProd := 0;
      CSolu := 0;
      Custo := 0;
      if FCalcCustos or FCalcPrecos then
      begin
        CProd := CUSTO_PQ;
        CSolu := 0;
        Custo := CUSTO_PQ;
      end;
      //
      Densidade      := QrReEmitDensidade.Value;
      UsarDensid     := QrReEmitUsarDensid.Value;
      LITROS_PQ      := QrReEmitLITROS_PQ.Value;
      //
      NxLotes        := QrReEmitNxLotes.Value;
      SiglaIQ        := QrReEmitSiglaIQ.Value;
      //
    end;
    if FCalcCustos or FCalcPrecos then
    begin
      if FPesoCalc < 1 then
        Formato := FFloat_15_4
      else
        Formato := FFloat_15_2;
      //
      if CUSTO_PQ > 0 then
         Inicio := FormatFloat(Formato, CUSTO_PQ);
         Final_ := '';
      if CUSTO_PQ > 0 then
         kgLCusto := FormatFloat(Formato, CUSTO_PQ);
    end else
    begin
        Inicio         := '';
        Final_         := '';
        kgLCusto       := '';
        L_Agua         := FormatFloat(Formato, QrReEmitDiluicao.Value * CUSTO_PQ);
    end;
    //
    //if
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_formulasits_', False, [
    'Nome', 'PQCI', 'CustoPadrao',
    'CustoMedio', 'CusIntrn', 'MoedaPadrao', 'ProdutoCI',
    'Numero', 'Ordem', 'Controle',
    'Reordem', 'Processo', 'Porcent',
    'Produto', 'TempoR', 'TempoP',
    'pH', 'pH_Min', 'pH_Max',
    'Be', 'Be_Min', 'Be_Max',
    'GC_Min', 'GC_Max', 'Obs',
    'ObsProces', 'SC', 'Reciclo',
    'Diluicao', 'CProd', 'CSolu',
    'Custo', 'Minimo', 'Maximo',
    'Graus', 'Boca', 'EhGGX',
    'GraGruX', 'SEQ', 'PESO_PQ',
    'PRECO_PQ', 'CUSTO_PQ', 'pH_All',
    'Be_All', 'Inicio', 'Final',
    'kgLCusto',
//
    'Densidade', 'UsarDensid', 'LITROS_PQ',
//
    'NxLotes',
//
    'SiglaIQ',
//
    'Ativo_1', 'Ativo'], [
    ], [
    Nome, PQCI, CustoPadrao,
    CustoMedio, CusIntrn, MoedaPadrao, ProdutoCI,
    Numero, Ordem, Controle,
    Reordem, Processo, Porcent,
    Produto, TempoR, TempoP,
    pH, pH_Min, pH_Max,
    Be, Be_Min, Be_Max,
    GC_Min, GC_Max, Obs,
    ObsProces, SC, Reciclo,
    Diluicao, CProd, CSolu,
    Custo, Minimo, Maximo,
    Graus, Boca, EhGGX,
    GraGruX, SEQ, PESO_PQ,
    PRECO_PQ, CUSTO_PQ, pH_All,
    Be_All, Inicio, Final_,
    kgLCusto,
    //
    Densidade, UsarDensid, LITROS_PQ,
    //
    NxLotes,
    //
    SiglaIQ,
    //
    Ativo_1, Ativo], [
    ], False);
  end;
var                          // estoque negativo
  SubEtapa, EtapaNum, Linha, Neg, Controle: Integer;
  EtapaTit, ObserProce, Inexiste: String;
  TabLoc_ImpBMZ, Lotes: String;
  sMedia: String;
  nEmit: Integer;
var
  RibRecFmt: Integer;
  MeBeGCTit, MeBeGCVal: TfrxMemoView;
  //Report: TfrxReport;
begin
  // ini 2022-10-25 - Impedir impress�o indevida do VARF_DATAEMIS_HORAINI
  QrEmitIts.Close;
  // fim 2022-10-25
  MyObjects.Informa2VAR_LABELs('Preparando c�lculo da receita (0)');
  if Trim(FDefPeca) = '' then
    FDefPeca := 'Pe�a';
  PreparaDivisores;
  MyObjects.Informa2VAR_LABELs('Preparando c�lculo da receita (1A)');
  if not(Dmod.QrControleDOSGrades.Value in ([2,3])) then FDivVert := ' '
    else FDivVert := '|';
  MyObjects.Informa2VAR_LABELs('Preparando c�lculo da receita (1B)');
  if not (Dmod.QrControleDOSGrades.Value in ([1,3])) then
    FPula := Dmod.QrControleDOSDotLinBot.Value else FPula := 0;

  if FVSMovCod > 0 then
  begin
    MyObjects.Informa2VAR_LABELs('Preparando c�lculo da receita (2A)');
    VS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(QrVMIOri, FVSMovCod, 0, FTemIMEIMrt, FVSMovimSrc, '')
  end else
  begin
    MyObjects.Informa2VAR_LABELs('Preparando c�lculo da receita (2B)');
    //VS_PF.ReopenVSOpePrcOriIMEI(QrVMIOri, -999999999, 0, 0, FVSMovimSrc(*eminSorcCal*), '');
    //
    VS_PF.ReopenVSEmitCusIMEI(QrVMIOri, FEmit, 0, '');
  end;

(*
  QrFormulas.Close;
  QrFormulas.Params[0].AsInteger := FFormula;
  UnDmkDAC_PF.AbreQuery(QrFormulas, Dmod.MyDB);
*)
  MyObjects.Informa2VAR_LABELs('Preparando c�lculo da receita (3)');
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
  'SELECT IF(ci.Tipo=0, ci.RazaoSocial, ci.Nome) NOMECI, ',
  'ls.Nome NOMESETOR, gcc.Nome NoGraCorCad, fo.* ',
  'FROM Formulas fo',
  'LEFT JOIN listasetores ls ON ls.Codigo=fo.Setor',
  'LEFT JOIN entidades    ci ON ci.Codigo=fo.ClienteI',
  'LEFT JOIN  gracorcad gcc ON gcc.Codigo=fo.GraCorCad',
  'WHERE fo.Numero=' + Geral.FF0(FFormula),
  '']);
  //
  MyObjects.Informa2VAR_LABELs('Preparando c�lculo da receita (4)');
  QrFormulasIts.Close;
(*
  QrFormulasIts.Params[0].AsInteger := FCliIntCod;
  QrFormulasIts.Params[1].AsInteger := FFormula;
  QrFormulasIts.Params[2].AsFloat   := FMedia;
  UnDmkDAC_PF.AbreQuery(QrFormulasIts, Dmod.MyDB);
*)
  //FFormulas :=
    //UnCreateBlueDerm.RecriaTempTableNovo(ntrttFormulas, DModG.QrUpdPID1, False);
  FFormulasIts :=
    UnCreateBlueDerm.RecriaTempTableNovo(ntrttFormulasIts, DModG.QrUpdPID1, False);
  //
  MyObjects.Informa2VAR_LABELs('Criando receita espelho (1)');
  if not FReImprime then
  begin
    // ini 2021-10-16
    sMedia := Geral.FFT_Dot(FMedia, 4, siNegativo);
    // fim 2021-10-16
    UnDmkDAC_PF.AbreMySQLQuery0(QrPEEmit, Dmod.MyDB, [
    'SELECT pq.GGXNiv2 Ativo, pq.Nome, pcl.PQ PQCI,  ',
    //
    'pq.Densidade, pq.UsarDensid, ',
    //
    ' ',
    'IF(pcl.CustoPadrao > 0, pcl.CustoPadrao,  ',
    'pcl.Valor/pcl.Peso) CustoPadrao,  ',
    ' ',
    'IF((pcl.Valor/pcl.Peso>0) AND (pcl.Valor>0) AND (pcl.Peso>0),  ',
    'pcl.Valor/pcl.Peso,pcl.CustoPadrao) CustoMedio,  ',
    ' ',
    'pcl.MoedaPadrao, pcl.Controle PRODUTOCI, ',
    '',
    'IF(ent.Codigo=0, "",',
    'IF(ent.Sigla <> "", ent.Sigla,',
    '  IF(ent.Tipo=0, ',
    '    IF(ent.Fantasia<>"", ent.Fantasia, ent.RazaoSocial),',
    '    IF(ent.Apelido<>"", ent.Apelido, ent.Nome)))) SiglaIQ,',
    'pq.CusIntrn, ',
    'fi.* ',
    'FROM formulasits fi	 ',
    ' ',
    'LEFT JOIN pq    pq  ON pq.Codigo=fi.Produto ',
    'LEFT JOIN pqcli pcl ON pcl.PQ=fi.Produto AND pcl.CI=' + Geral.FF0(FCliIntCod),
    'LEFT JOIN entidades ent ON ent.Codigo=pq.IQ',
    'WHERE fi.Numero=' + Geral.FF0(FFormula),
    'AND ((fi.Minimo+fi.Maximo=0)  ',
    // ini 2021-10-16
    //'OR(' + Geral.FFF(FMedia, 2) + ' BETWEEN fi.Minimo AND fi.Maximo)) ',
    'OR(' + sMedia + ' >= fi.Minimo AND ' +sMedia + ' < fi.Maximo)) ',
    // fim 2021-10-16
    '']);
    //
    //Geral.MB_Teste(QrPEEmit.SQL.Text);
    //
    QrPEEmit.First;
    while not QrPEEmit.Eof do
    begin
      InsereLinhaAtual(0);
      //
      QrPEEmit.Next;
    end;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrReEmit, Dmod.MyDB, [
    'SELECT * ',
    'FROM emitits ',
    'WHERE Codigo=' + Geral.FF0(FEmit),
    '']);
    QrReEmit.First;
    while not QrReEmit.Eof do
    begin
      InsereLinhaAtual(1);
      //
      QrReEmit.Next;
    end;
  end;

  MyObjects.Informa2VAR_LABELs('Abrindo  receita espelho');
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulasIts, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM _formulasits_',
  '']);
  //  Parei Aqui








  if (FEmit > 0) and (not FReImprime) then
  begin
    MyObjects.Informa2VAR_LABELs('Verificando insumos sem cadastro');
    if PQ_PF.InsumosSemCadastroParaClienteInterno(FCliIntCod, FEmpresa,
    (*InsumoEspecifico*)0, FFormula, TTabPqNaoCad.tpncPesagem) then Exit;
    //
    MyObjects.Informa2VAR_LABELs('Executando "Emit"');
    Emite(FEmit);
    Neg := 0;
    MyObjects.Informa2VAR_LABELs('Abrindo "PreUso"');
    ReopenPreUso(FEmit);
    MyObjects.Informa2VAR_LABELs('Executando "PreUso"');
    QrPreuso.First;
    Inexiste := '';
    while not QrPreuso.Eof do
    begin
      if (QrPreusoEXISTE.Value <= 0) and (not (QrPreusoAtivo.Value in ([0,3]))) then
        Inexiste := Inexiste + QrPreusoNomePQ.Value+sLineBreak;
      if QrPreusoPESOFUTURO.Value < 0 then
      Neg := Neg + 1;
      QrPreuso.Next;
    end;
    MyObjects.Informa2VAR_LABELs('Verificando resultado do "PreUso"');
    if Inexiste <> '' then
    begin
      FCancela := True;
      Geral.MB_Aviso('A empresa "' + QrPreusoNOMECLI_ORIG.Value +
      '" n�o est� cadastrada no(s) seguinte(s) insumo(s): ' + sLineBreak +
      Inexiste);
    end else if Neg > 0 then
    begin
      if Dmod.QrControlePqNegBaixa.Value = 0 then
      begin
        if DBCheck.CriaFm(TFmPQChange, FmPQChange, afmoNegarComAviso) then
        begin
          //FmPQChange.QrBaixa.Params[0].AsInteger := FEmit;
          //FmPQChange.QrPreuso.Params[0].AsInteger := FEmit;
          FmPQChange.FEmit := FEmit;
          FmPQChange.FJanela := 1;
          FmPQChange.ReopenBaixa;
          FmPQChange.ShowModal;
          FAllDest := FmPQChange.FAllDest;
          FCancela := FmPQChange.FCancela;
          FmPQChange.Destroy;
        end else begin
          Geral.MB_Aviso('Existe um ou mais insumos que n�o tem ' +
          'estoque suficiente para emitir esta pesagem!');
        end
      end;
    end;
    if ( (Neg > 0) and (Dmod.QrControlePQNegBaixa.Value = 0) )
    or (Inexiste <> '') then
    begin
      if not FCancela then
      begin
        Neg := 0;
        ReopenPreuso(FEmit);
        QrPreuso.First;
        while not QrPreuso.Eof do
        begin
          if QrPreusoPESOFUTURO.Value < 0 then Neg := Neg + 1;
          QrPreuso.Next;
        end;
      end;
      if ((Neg > 0) and (Dmod.QrControlePQNegBaixa.Value = 0) and (FAllDest < 1))
      or FCancela then
      begin
        Screen.Cursor := crHourGlass;
        QrBaixa.Close;
        QrBaixa.Params[0].AsInteger := FEmit;
        UnDmkDAC_PF.AbreQuery(QrBaixa, Dmod.MyDB);
        if FProgressBar1 <> nil then
        begin
          FProgressBar1.Max := QrBaixa.RecordCount;
          FProgressBar1.Position := FProgressBar1.Max;
          FProgressBar1.Visible := True;
        end;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM emitcus WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := FEmit;
        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM emitits WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := FEmit;
        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM emit WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := FEmit;
        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO Livres SET Tabela="EmitIts", Codigo=:P0');
        QrBaixa.First;
        while not QrBaixa.Eof do
        begin
          if FProgressBar1 <> nil then
          begin
            FProgressBar1.Position := FProgressBar1.Position - 1;
            FProgressBar1.Update;
            Application.ProcessMessages;
          end;
          //
          Dmod.QrUpd.Params[0].AsInteger := QrBaixaControle.Value;
          Dmod.QrUpd.ExecSQL;
          QrBaixa.Next;
        end;
        Screen.Cursor := crDefault;
        Exit;
      end else
        EfetuaBaixa(QrLotes);
    end else
      EfetuaBaixa(QrLotes);
  end;
  MyObjects.Informa2VAR_LABELs('Reabrindo Custos (2?)');
  if FEmit = 0 then ReopenCustos1 else
  begin
    QrEmitIts.Close;
    QrEmitIts.Params[0].AsInteger := FEmit;
    UnDmkDAC_PF.AbreQuery(QrEmitIts, Dmod.MyDB);
    ReopenCustos2;
  end;
  //
  MyObjects.Informa2VAR_LABELs('Decidindo impress�o');
  if FTipoImprime in ([2,3]) then
  begin
    if FMatricialNovo then ImprimeMatricial1_2(FTipoImprime)
    else ImprimeMatricial1_1;
  end else begin
    if BDC_APRESENTACAO_IMPRESSAO = 12 then
    begin
      MyObjects.frxMostra(frxRecIdeal, 'Receita de Ribeira');
    end else
    if BDC_APRESENTACAO_IMPRESSAO in ([0,1,2,13,14,15,16,17]) then
    begin
      if FCalcPrecos then
      begin
        {frRec2.PrepareReport;
        case FTipoImprime of
          0: frRec2.ShowPreparedReport;
          1: frRec2.PrintPreparedReport(CO_VAZIO,1,True, frAll);
        end;}
        //
        case FTipoImprime of
          0: MyObjects.frxMostra(frxRec2, 'Receita');
          1: MyObjects.frxImprime(frxRec2, 'Receita');
        end;
      end else begin
        case FTipoImprime of
          0:
          if FMatricialNovo then
          begin
            if FGrade then
              MyObjects.frxMostra(frxRec1_ML, 'Receita')
            else
              MyObjects.frxMostra(frxRec1_MS, 'Receita');
          end else
          begin
            if BDC_APRESENTACAO_IMPRESSAO in ([15,16]) then
            begin
              // Depois!
              //MyObjects.frxMostra(frxGelatina, 'Receita');
            end else
            if BDC_APRESENTACAO_IMPRESSAO in ([14,17]) then
            begin
              {
              case BDC_APRESENTACAO_IMPRESSAO of
                14: //Report := frxRec1Panorama;
                begin
                  MeBeGCTit := frxRec1Panorama.FindObject('MeBeGCTit') as TfrxMemoView;
                  MeBeGCVal := frxRec1Panorama.FindObject('MeBeGCVal') as TfrxMemoView;
                end;
                17: //Report := frxRec1LWG2023_06;
                begin
                  MeBeGCTit := frxRec1LWG2023_06.FindObject('MeBeGCTit') as TfrxMemoView;
                  MeBeGCVal := frxRec1LWG2023_06.FindObject('MeBeGCVal') as TfrxMemoView;
                end;
                else
                begin
                  //Report := nil;
                  Geral.MB_Erro('Relat�rio n�o implementado! ' + sLineBreak +
                  'BDC_APRESENTACAO_IMPRESSAO: ' + Geral.FF0(BDC_APRESENTACAO_IMPRESSAO));
                end;
              end;
              case BDC_APRESENTACAO_IMPRESSAO of
                14: //Report := frxRec1Panorama;
                begin
                  MeBeGCTit := frxRec1Panorama.FindObject('MeBeGCTit') as TfrxMemoView;
                  MeBeGCVal := frxRec1Panorama.FindObject('MeBeGCVal') as TfrxMemoView;
                end;
                17: //Report := frxRec1LWG2023_06;
                begin
                  MeBeGCTit := frxRec1LWG2023_06.FindObject('MeBeGCTit') as TfrxMemoView;
                  MeBeGCVal := frxRec1LWG2023_06.FindObject('MeBeGCVal') as TfrxMemoView;
                end;
                else
                begin
                  //Report := nil;
                  Geral.MB_Erro('Relat�rio n�o implementado! ' + sLineBreak +
                  'BDC_APRESENTACAO_IMPRESSAO: ' + Geral.FF0(BDC_APRESENTACAO_IMPRESSAO));
                end;
              end;
              if FTpReceita = rectipsetrRecurtimento then
              begin
                MeBeGCTit.Memo.Text := '�C';
                MeBeGCVal.Memo.Text := '[frxDsFormulasIts."GC_All_2"]';
              end
              //if FTpReceita = rectipsetrRibeira then
              else
              begin
                MeBeGCTit.Memo.Text := 'B�';
                MeBeGCVal.Memo.Text := '[frxDsFormulasIts."Be_All"]';
              end;
              }
              case BDC_APRESENTACAO_IMPRESSAO of
                14: MyObjects.frxMostra(frxRec1Panorama, 'Receita');
                17: MyObjects.frxMostra(frxRec1LWG2023_06, 'Receita');
                else
                begin
                  //Report := nil;
                  Geral.MB_Erro('Relat�rio n�o implementado! ' + sLineBreak +
                  'BDC_APRESENTACAO_IMPRESSAO: ' + Geral.FF0(BDC_APRESENTACAO_IMPRESSAO));
                end;
              end;
            end else
            begin
              RibRecFmt := MyObjects.SelRadioGroup('Qual � o formato?',
              'Selecione o formato: ',
              ['A', 'B', 'C'], -1);
              case RibRecFmt of
                0: MyObjects.frxMostra(frxRec1A, 'Receita');
                1: MyObjects.frxMostra(frxRec1Noroeste, 'Receita');
                2: MyObjects.frxMostra(frxRec1, 'Receita');
              end;
            end;
          end;
          1:
          if FMatricialNovo then
          begin
            if FGrade then
              MyObjects.frxImprime(frxRec1_ML, 'Receita')
            else
              MyObjects.frxImprime(frxRec1_MS, 'Receita')
          end else
            MyObjects.frxImprime(frxRec1, 'Receita');
        end;
      end;
    end;
    if BDC_APRESENTACAO_IMPRESSAO in ([6,7,9,10,11,13,14,15,16,17]) then
    begin
      Screen.Cursor := crHourGlass;
      try
        TabLoc_ImpBMZ := UCriar.RecriaTempTableNovo(ntrttImpBMZ, DmodG.QrUpdPID1, False, 0);
        //
        DmodG.QrUpdPID1.SQL.Clear;
        DmodG.QrUpdPID1.SQL.Add('DELETE FROM ' + TabLoc_ImpBMZ);
        DmodG.QrUpdPID1.ExecSQL;
        //
        DmodG.QrUpdPID1.SQL.Clear;
        DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + TabLoc_ImpBMZ + ' SET ');
        DmodG.QrUpdPID1.SQL.Add('Etapa=:P0, NomeEtapa=:P1, TempoR=:P2, TempoP=:P3, ');
        DmodG.QrUpdPID1.SQL.Add('Observacos=:P4, Perc=:P5, Peso=:P6, Descricao=:P7, ');
        DmodG.QrUpdPID1.SQL.Add('Linha=:P8, SubEtapa=:P9, pH_Min=:P10, pH_Max=:P11, ');
        DmodG.QrUpdPID1.SQL.Add('Be_Min=:P12, Be_Max=:P13, GC_Min=:P14, GC_Max=:P5, ');
        DmodG.QrUpdPID1.SQL.Add('Diluicao=:P16, Graus=:P17, Insumo=:P18, ');
        DmodG.QrUpdPID1.SQL.Add('ObserProce=:P19, Custokg=:P20, CustoIt=:P21, ');
        DmodG.QrUpdPID1.SQL.Add('Porcent=:P22, NxLotes=:P23, SiglaIQ=:P24 ');
        //
        Linha := 0;
        EtapaNum := 0;
        SubEtapa := 0;
        EtapaTit := '';
        ObserProce := '';
        if FEmit = 0 then
        begin
          //QrFormulasIts.Close;
          //QrFormulasIts.Open;
          QrFormulasIts.First;
          while not QrFormulasIts.Eof do
          begin
            Linha := Linha + 1;
            if Trim(QrFormulasItsProcesso.Value) <> '' then
            begin
              EtapaNum := EtapaNum + 1;
              EtapaTit := QrFormulasItsProcesso.Value;
              ObserProce := '';
            end;
            if QrFormulasItsObsProces.Value <> '' then
            begin
              if ObserProce <> '' then ObserProce := ObserProce + '; ';
              ObserProce := ObserProce + QrFormulasItsObsProces.Value;
            end;
            DmodG.QrUpdPID1.Params[00].AsInteger := EtapaNum;
            DmodG.QrUpdPID1.Params[01].AsString  := EtapaTit;
            DmodG.QrUpdPID1.Params[02].AsInteger := QrFormulasItsTempoR.Value;
            DmodG.QrUpdPID1.Params[03].AsInteger := QrFormulasItsTempoP.Value;
            DmodG.QrUpdPID1.Params[04].AsString  := QrFormulasItsObs.Value;
            DmodG.QrUpdPID1.Params[05].AsFloat   := 0;//n�o funciona QrFormulasItsPorcent.Value;
            DmodG.QrUpdPID1.Params[06].AsFloat   := QrFormulasItsPESO_PQ.Value;
            DmodG.QrUpdPID1.Params[07].AsString  := QrFormulasItsNome.Value;
            DmodG.QrUpdPID1.Params[08].AsInteger := Linha;
            DmodG.QrUpdPID1.Params[09].AsInteger := SubEtapa;
            DmodG.QrUpdPID1.Params[10].AsFloat   := QrFormulasItspH_Min.Value;
            DmodG.QrUpdPID1.Params[11].AsFloat   := QrFormulasItspH_Max.Value;
            DmodG.QrUpdPID1.Params[12].AsFloat   := QrFormulasItsBe_Min.Value;
            DmodG.QrUpdPID1.Params[13].AsFloat   := QrFormulasItsBe_Max.Value;
            DmodG.QrUpdPID1.Params[14].AsFloat   := QrFormulasItsGC_Min.Value;
            DmodG.QrUpdPID1.Params[15].AsFloat   := QrFormulasItsGC_Max.Value;
            DmodG.QrUpdPID1.Params[16].AsFloat   := QrFormulasItsDiluicao.Value;
            DmodG.QrUpdPID1.Params[17].AsFloat   := QrFormulasItsGraus.Value;
            DmodG.QrUpdPID1.Params[18].AsInteger := QrFormulasItsProduto.Value;
            DmodG.QrUpdPID1.Params[19].AsString  := ObserProce;
            DmodG.QrUpdPID1.Params[20].AsFloat   := QrFormulasItsPRECO_PQ.Value;
            DmodG.QrUpdPID1.Params[21].AsFloat   := QrFormulasItsCUSTO_PQ.Value;
            DmodG.QrUpdPID1.Params[22].AsFloat   := QrFormulasItsPorcent.Value;
            DmodG.QrUpdPID1.Params[23].AsString  := QrFormulasItsNxLotes.Value;
            DmodG.QrUpdPID1.Params[24].AsString  := QrFormulasItsSiglaIQ.Value;
            DmodG.QrUpdPID1.ExecSQL;
            //
            if (QrFormulasItsTempoR.Value + QrFormulasItsTempoR.Value > 0) then
            begin
              SubEtapa := SubEtapa + 1;
              ObserProce := '';
            end;
            QrFormulasIts.Next;
          end;
        end else begin
          //QrFormulasIts.Close;
          //QrFormulasIts.Open;
          QrFormulasIts.First;
          while not QrEmitIts.Eof do
          begin
            Linha := Linha + 1;
            if Trim(QrEmitItsProcesso.Value) <> '' then
            begin
              EtapaNum := EtapaNum + 1;
              EtapaTit := QrEmitItsProcesso.Value;
              ObserProce := '';
            end;
            ObserProce := ObserProce + QrEmitItsObsProces.Value;
            DmodG.QrUpdPID1.Params[00].AsInteger := EtapaNum;
            DmodG.QrUpdPID1.Params[01].AsString  := EtapaTit;
            DmodG.QrUpdPID1.Params[02].AsInteger := QrEmitItsTempoR.Value;
            DmodG.QrUpdPID1.Params[03].AsInteger := QrEmitItsTempoP.Value;
            DmodG.QrUpdPID1.Params[04].AsString  := QrEmitItsObs.Value;
            DmodG.QrUpdPID1.Params[05].AsFloat   := QrEmitItsPorcent.Value;
            DmodG.QrUpdPID1.Params[06].AsFloat   := QrEmitItsPESO_PQ.Value;
            DmodG.QrUpdPID1.Params[07].AsString  := QrEmitItsNomePQ.Value;
            DmodG.QrUpdPID1.Params[08].AsInteger := Linha;
            DmodG.QrUpdPID1.Params[09].AsInteger := SubEtapa;
            DmodG.QrUpdPID1.Params[10].AsFloat   := QrEmitItspH_Min.Value;
            DmodG.QrUpdPID1.Params[11].AsFloat   := QrEmitItspH_Max.Value;
            DmodG.QrUpdPID1.Params[12].AsFloat   := QrEmitItsBe_Min.Value;
            DmodG.QrUpdPID1.Params[13].AsFloat   := QrEmitItsBe_Max.Value;
            DmodG.QrUpdPID1.Params[14].AsFloat   := QrEmitItsGC_Min.Value;
            DmodG.QrUpdPID1.Params[15].AsFloat   := QrEmitItsGC_Max.Value;
            DmodG.QrUpdPID1.Params[16].AsFloat   := QrEmitItsDiluicao.Value;
            DmodG.QrUpdPID1.Params[17].AsFloat   := QrEmitItsGraus.Value;
            DmodG.QrUpdPID1.Params[18].AsInteger := QrEmitItsProduto.Value;
            DmodG.QrUpdPID1.Params[19].AsString  := ObserProce;
            DmodG.QrUpdPID1.Params[20].AsFloat   := QrEmitItsPRECO_PQ.Value;
            DmodG.QrUpdPID1.Params[21].AsFloat   := QrEmitItsCUSTO_PQ.Value;
            DmodG.QrUpdPID1.Params[22].AsFloat   := QrEmitItsPorcent.Value;
            DmodG.QrUpdPID1.ExecSQL;
            //
            if (QrEmitItsTempoR.Value + QrEmitItsTempoR.Value > 0) then
            begin
              SubEtapa := SubEtapa + 1;
              ObserProce := '';
            end;
            QrEmitIts.Next;
          end;
        end;
        if BDC_APRESENTACAO_IMPRESSAO in ([6,9,11]) then
        begin
          QrLFormItsL.Close;
          QrLFormItsL.Database := DModG.MyPID_DB;
          QrLFormItsL.Params[0].AsFloat   := -100000000;
          QrLFormItsL.Params[1].AsInteger := -100000000;
          UnDmkDAC_PF.AbreQuery(QrLFormItsL, DmodG.MyPID_DB);
          Screen.Cursor := crDefault;
          if BDC_APRESENTACAO_IMPRESSAO = 11 then
            MyObjects.frxMostra(frxBMZMini, 'Receita')
          else
            MyObjects.frxMostra(frxBMZ, 'Receita');
        end;
        if BDC_APRESENTACAO_IMPRESSAO in ([7,9,13,14,15,16,17]) then
        begin
(*
          QrLFormItsL.Close;
          QrLFormItsL.Database := DModG.MyPID_DB;
          QrLFormItsL.Params[0].AsFloat   := 0;
          QrLFormItsL.Params[1].AsInteger := 0;
          UnDmkDAC_PF.AbreQuery(QrLFormItsL, DmodG.MyPID_DB);
*)
          UnDmkDAC_PF.AbreMySQLQuery0(QrLFormItsL, DmodG.MyPID_DB, [
          'SELECT * FROM ImpBMZ ',
          'WHERE Peso > ' + Geral.FF0(0),
          'AND Insumo > ' + Geral.FF0(0),
          'ORDER BY Linha ',
          '']);
          //
          if FEmit > 0 then
            nEmit := FEmit
          else
            nEmit := -99999999;
          UnDmkDAC_PF.AbreMySQLQuery0(QrNxLotes, Dmod.MyDB, [
          'SELECT pq_.Nome, GROUP_CONCAT(pqi.xLote SEPARATOR ", ") NxLotes',
          'FROM pqw pqw',
          'LEFT JOIN pqeits pqi ',
          '  ON pqi.Codigo=pqw.DstCodi',
          '  AND pqi.Controle=pqw.DstCtrl',
          'LEFT JOIN pq pq_ ON pq_.Codigo=pqi.Insumo',
          'WHERE OriCodi=' + Geral.FF0(nEmit),
          'GROUP BY pqi.Insumo',
          'ORDER BY pq_.Nome',
          '']);
          //
          {
          if nEmit > 0 then
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(QrIQs, Dmod.MyDB, [
            'SELECT pq_.Codigo, ',
            'IF(ent.Sigla <> "", ent.Sigla,',
            '  IF(ent.Tipo=0, ',
            '    IF(ent.Fantasia<>"", ent.Fantasia, ent.RazaoSocial),',
            '    IF(ent.Apelido<>"", ent.Apelido, ent.Nome))) SIGLA',
            'FROM pqw pqw',
            'LEFT JOIN pqeits pqi ',
            '  ON pqi.Codigo=pqw.DstCodi',
            '  AND pqi.Controle=pqw.DstCtrl',
            'LEFT JOIN pq pq_ ON pq_.Codigo=pqi.Insumo',
            'LEFT JOIN entidades ent ON ent.Codigo=pq_.IQ',
            'WHERE OriCodi=' + Geral.FF0(nEmit),
            '']);
          end else
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(QrIQs, Dmod.MyDB, [
            'SELECT DISTINCT pq_.Codigo, ',
            'IF(ent.Codigo=0, "",',
            'IF(ent.Sigla <> "", ent.Sigla,',
            '  IF(ent.Tipo=0, ',
            '    IF(ent.Fantasia<>"", ent.Fantasia, ent.RazaoSocial),',
            '    IF(ent.Apelido<>"", ent.Apelido, ent.Nome)))) SIGLA',
            'FROM formulasits its',
            'LEFT JOIN pq pq_ ON pq_.Codigo=its.Produto',
            'LEFT JOIN entidades ent ON ent.Codigo=pq_.IQ',
            'WHERE its.Numero=' + Geral.FF0(FFormula),
            '']);
          end;
          }
          Screen.Cursor := crDefault;
          if BDC_APRESENTACAO_IMPRESSAO in ([15,16]) then
          begin
            QrFormulasIts.Close;
            QrFormulasIts.Open;
            if BDC_APRESENTACAO_IMPRESSAO = 15 then
              MyObjects.frxMostra(frxReceiLotesPQCurta, 'Receita')
            else
              MyObjects.frxMostra(frxReceiLotesPQLonga, 'Receita');
          end;
          MyObjects.frxMostra(frxPesagemL, 'Pesagem');
        end;
        if BDC_APRESENTACAO_IMPRESSAO in ([10]) then
        begin
          QrLFormItsL.Close;
          QrLFormItsL.Database := DModG.MyPID_DB;
          QrLFormItsL.Params[0].AsFloat   := -100000000;
          QrLFormItsL.Params[1].AsInteger := -100000000;
          UnDmkDAC_PF.AbreQuery(QrLFormItsL, DmodG.MyPID_DB);
          //
          QrLotes.Close;
          QrLotes.Params[0].AsInteger := FEmit;
          UnDmkDAC_PF.AbreQuery(QrLotes, Dmod.MyDB);
          Lotes := '';
          while not QrLotes.Eof do
          begin
            Lotes := Lotes + ',' + FormatFloat('0', QrLotesMPIn.Value);
            QrLotes.Next;
          end;
          QrLotes.First;
          //
          if Length(Lotes) > 1 then
          begin
            Lotes := Copy(Lotes, 2, Length(Lotes));
            QrFulonadas.Close;
            QrFulonadas.SQL.Clear;
            QrFulonadas.SQL.Add('SELECT mpi.Controle, mpi.Lote, emc.Peso PesoF,');
            QrFulonadas.SQL.Add('emc.Pecas PecasF, mpi.PLE PesoL, mpi.Pecas PecasL,');
            QrFulonadas.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLII');
            QrFulonadas.SQL.Add('FROM Emit emi');
            QrFulonadas.SQL.Add('LEFT JOIN emitcus emc ON emc.Codigo=emi.Codigo');
            QrFulonadas.SQL.Add('LEFT JOIN MPIn mpi ON mpi.Controle=emc.MPIn');
            QrFulonadas.SQL.Add('LEFT JOIN Entidades ent ON ent.Codigo=mpi.ClienteI');
            QrFulonadas.SQL.Add('');
            QrFulonadas.SQL.Add('WHERE emi.Setor=' + IntToStr(FSetor));
            QrFulonadas.SQL.Add('AND emc.MPIn IN (' + Lotes + ')');
            QrFulonadas.SQL.Add('');
            QrFulonadas.SQL.Add('ORDER BY mpi.Data, mpi.Controle');
            QrFulonadas.SQL.Add('');
            UnDmkDAC_PF.AbreQuery(QrFulonadas, Dmod.MyDB);
            //
            QrSumFul.Close;
            QrSumFul.SQL.Clear;
            QrSumFul.SQL.Add('SELECT SUM(mpi.Pecas) Pecas, SUM(mpi.PLE) PLE');
            QrSumFul.SQL.Add('FROM Emit emi');
            QrSumFul.SQL.Add('LEFT JOIN emitcus emc ON emc.Codigo=emi.Codigo');
            QrSumFul.SQL.Add('LEFT JOIN MPIn mpi ON mpi.Controle=emc.MPIn');
            QrSumFul.SQL.Add('');
            QrSumFul.SQL.Add('WHERE emi.Setor=' + IntToStr(FSetor));
            QrSumFul.SQL.Add('AND emc.MPIn IN (' + Lotes + ')');
            QrSumFul.SQL.Add('');
            QrSumFul.SQL.Add('ORDER BY mpi.Data, mpi.Controle');
            QrSumFul.SQL.Add('');
            UnDmkDAC_PF.AbreQuery(QrSumFul, Dmod.MyDB);
          end;
          //
          Screen.Cursor := crDefault;
          frxRec10.Variables['LogoReceExiste'] :=
            FileExists(Dmod.QrControle.FieldByName('PathLogoRece').AsString);
          frxRec10.Variables['LogoReceCaminho'] :=
            QuotedStr(Dmod.QrControle.FieldByName('PathLogoRece').AsString);
          MyObjects.frxMostra(frxRec10, 'Receita COT-01');
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    {
    end else if BDC_APRESENTACAO_IMPRESSAO in ([10]) then
    begin
      Geral.MB_Aviso('Em constru��o', 'Aviso', MB_OK+MB_ICONINFORMATION);
      //
      //fazer nova tabela ?
    }
    end;
  end;
end;

procedure TFrFormulasImpShowNew.ImprimeMatricial1_1;
{
var
  TabLoc_Imprimir1: String;
}
begin
  ShowMessage('Procedimento desativado!');
{
  Screen.Cursor := crHourGlass;
  try
    TabLoc_Imprimir1 := UCriar.RecriaTempTableNovo(ntrttImprimir1, DmodG.QrUpdPID1, False, 0);
    FPagina          := 1;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('DELETE FROM ' + TabLoc_Imprimir1);
    DmodG.QrUpdPID1.ExecSQL;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + TabLoc_Imprimir1 + ' SET ');
    DmodG.QrUpdPID1.SQL.Add('Valor=:P0, Prefixo=:P1, Sufixo=:P2, ');
    DmodG.QrUpdPID1.SQL.Add('DTopo=:P3, DMEsq=:P4, DComp=:P5, DAltu=:P6, ');
    DmodG.QrUpdPID1.SQL.Add('DBand=:P7, Set_N=:P8, Set_I=:P9, ');
    DmodG.QrUpdPID1.SQL.Add('Set_U=:P10, Set_T=:P11, Set_A=:P12, ');
    DmodG.QrUpdPID1.SQL.Add('Forma=:P13, Substitui=:P14, ');
    DmodG.QrUpdPID1.SQL.Add('Substituicao=:P15, Nulo=:P16, ');
    DmodG.QrUpdPID1.SQL.Add('Campo=:P17, Tipo=:P18, Conta=:P19, Pagin=:P20');
    CalculaItensA;
    ImprimeMatricial;
    Screen.Cursor := crDefault;
  except;
    Screen.Cursor := crDefault;
    raise;
  end;
}
end;

procedure TFrFormulasImpShowNew.CalculaItensA;
//const
  //TxtCol = FDivVert;
var
  Texto: String;
  LinCab: Integer;
  AltLinha: Integer;
begin
  F_M := 0;
  AltLinha := 40;
  FSomat := 0;
  FLinAtu := 0;
  LinCab := -1;
  FAltDados := CO_AltLetraDOS + Dmod.QrControleDOSDotLinTop.Value;
  //////////////////////////////////////////////////////////////////////////////
  //InsereItem(Caption,T, R, L, W, N, I, F);
  Texto := CS(String(MeuGetValue('SP')),' ', 20, 0) +
           CS('F: '+String(MeuGetValue('FU')),' ', 20, 2);
  InsereItem(Texto, 0, 0, 2030, 0, 0, 2);
  //////////////////////////////////////////////////////////////////////////////
  LinCab := LinCab+1;
  Texto := CS(String(MeuGetValue('Custo_total')),' ', 15, 0) +
           CS(String(MeuGetValue('CustoTotal')) ,' ', 15, 2);
  InsereItem(Texto, AltLinha*LinCab+15, 1665, 1000, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  //LinCab := LinCab+1;
  Texto := CS(String(MeuGetValue('Custo_ton')),' ', 15, 0) +
           CS(String(MeuGetValue('CustoTon')) ,' ', 15, 2);
  InsereItem(Texto, AltLinha*LinCab+15, 3055, 1000, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  LinCab := LinCab+1;
  Texto := CS(String(MeuGetValue('Custo_kg')),' ', 15, 0) +
           CS(String(MeuGetValue('Custokg')) ,' ', 15, 2);
  InsereItem(Texto, AltLinha*LinCab+15, 1665, 1000, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  //LinCab := LinCab+1;
  Texto := CS(String(MeuGetValue('Custo_m')),' ', 15, 0) +
           CS(String(MeuGetValue('Custom2')) ,' ', 15, 2);
  InsereItem(Texto, AltLinha*LinCab+15, 3055, 1000, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  LinCab := LinCab+1;
  Texto := CS('Receita de '+QrFormulasNOMESETOR.Value ,' ', 25, 0);
  InsereItem(Texto, AltLinha*LinCab+15, 0, 1000, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  //LinCab := LinCab+1;
  Texto := CS('Respons�vel: '+QrFormulasTecnico.Value ,' ', 25, 0);
  InsereItem(Texto, AltLinha*LinCab+15, 1100, 1000, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  LinCab := LinCab+1;
  Texto := CS('Receita '+IntToStr(QrFormulasNumero.Value)+
              ' - '+QrFormulasNome.Value ,' ', 80, 0);
  InsereItem(Texto, AltLinha*LinCab+15, 0, 5600, 0, 0, 1);
  //////////////////////////////////////////////////////////////////////////////
  LinCab := LinCab+1;
  Texto := CS('Rebaixe: '+FLinhasRebx+ ' �rea: '+FormatFloat(FFloat_15_2, FArea)+
              ' Peso m�dio: '+FormatFloat(FFloat_15_2, FMedia) ,' ', 136, 0);
  InsereItem(Texto, AltLinha*LinCab+15, 0, 5600, 0, 0, 0);
  //////////////////////////////////////////////////////////////////////////////
  LinCab := LinCab+2;
  Texto := CS('Qtde: '+FormatFloat('0', FQtde)+
              '  Peso: '+FormatFloat('#,###,##0', FPeso) ,' ', 40, 0);
  InsereItem(Texto, AltLinha*LinCab, 0, 5600, 0, 0, 2);
  //////////////////////////////////////////////////////////////////////////////
  F_M := 95;
  //////////////////////////////////////////////////////////////////////////////
  Inserelinha(0);
  QrFormulasIts.First;
  Insere_Divisor(FDivisorTop);
  while not QrFormulasIts.Eof do
  begin
    InsereLinha(1);
    QrFormulasIts.Next;
  end;
  Insere_Divisor(FDivisorBot);
  InsereLinha(2);
end;

procedure TFrFormulasImpShowNew.ImprimeMatricial1_2(Mostra: Integer);
var
  FArqPrn: TextFile;
  ImprimeMid: Boolean;
  Arquivo: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Screen.Cursor := crDefault;
    { 2012-02-26 n�o funciona em SO 64 bits
    if Mostra = 2 then if not UnIO_DLL.CheckPort($0378) then Exit;
    }
    Arquivo := 'C:\Formula'+FormatDateTime('yymmdd"_"hhmmss', Now)+'.Txt';
    if Mostra = 2 then AssignFile(FArqPrn, 'LPT1:')
      else AssignFile(FArqPrn, Arquivo);
    ReWrite(FArqPrn);
    Write(FArqPrn, Char($1B)+Char($52)+Char($12)); // Seleciona tabela de caracteres sul americana
    Write(FArqPrn, Char($1B)+Char($74)+Char($01)); // Seleciona Caracteres Graficos extendidos
    Write(FArqPrn, Char($1B)+Char($06));           // Habilita caracteres imprim�veis
    Write(FArqPrn, Char($1B)+Char($78));           // Seleciona Fonte Draft (r�pida)
    Write(FArqPrn, Char($1B)+Char($0F));           // Ativa   modo condensado
    //Write(FArqPrn, Char($12));                   // Cancela modo condensado
    Write(FArqPrn, Char($1B)+Char($50));           // Seleciona 10 CPI
    //
    WriteLn(FArqPrn, FDivisorTop);
      WriteLn(FArqPrn, InsereLinhaB(0));
    QrFormulasIts.First;
    ImprimeMid := True;
    while not QrFormulasIts.Eof do
    begin
      if ImprimeMid then
        WriteLn(FArqPrn, FDivisorMid);
      ImprimeMid := True;
      WriteLn(FArqPrn, InsereLinhaB(1));
      QrFormulasIts.Next;
    end;
    WriteLn(FArqPrn, FDivisorBot);
    WriteLn(FArqPrn, InsereLinhaB(2));
    //
    Write(FArqPrn, Char($1B)+Char($40)); // ESC @  (Reseta (inicializa) impressora)
    CloseFile(FArqPrn);
  except;
    Screen.Cursor := crDefault;
    raise;
  end;
end;

(*procedure TFrFormulasImpShow.CalculaItensB;
begin
  F_M := 0;
  FSomat := 0;
  FLinAtu := 0;
  FAltDados := CO_AltLetraDOS + Dmod.QrControleDOSDotLinTop.Value;
end;*)

procedure TFrFormulasImpShowNew.InsereItem(Caption: String; T, L, W, N, I, F:
Integer);
begin
  F_T := Trunc(T / VAR_DOTIMP);
  F_L := Trunc(L / VAR_DOTIMP);
  F_W := Trunc(W / VAR_DOTIMP);
  F_A := F_T + F_M;
  if Trim(Caption) <> '' then
  begin
    DmodG.QrUpdPID1.Params[00].AsString  := Caption;
    DmodG.QrUpdPID1.Params[01].AsString  := '';//QrItensAPrefixo.Value;
    DmodG.QrUpdPID1.Params[02].AsString  := '';//QrItensASufixo.Value;
    DmodG.QrUpdPID1.Params[03].AsInteger := F_A;
    DmodG.QrUpdPID1.Params[04].AsInteger := F_L;//QrItensAPos_MEsq.Value+QrImprimePos_MEsq.Value;
    DmodG.QrUpdPID1.Params[05].AsInteger := F_W;//QrItensAPos_Larg.Value;
    DmodG.QrUpdPID1.Params[06].AsInteger := F_R;//QrItensAPos_Altu.Value;
    DmodG.QrUpdPID1.Params[07].AsInteger := 1;//QrItensABand.Value;
    DmodG.QrUpdPID1.Params[08].AsInteger := N;//QrItensASet_Negr.Value;
    DmodG.QrUpdPID1.Params[09].AsInteger := I;//QrItensASet_Ital.Value;
    DmodG.QrUpdPID1.Params[10].AsInteger := 0;//QrItensASet_Unde.Value;
    DmodG.QrUpdPID1.Params[11].AsInteger := F;//QrItensASet_T_DOS.Value;
    DmodG.QrUpdPID1.Params[12].AsInteger := 0;//QrItensASet_TAli.Value;
    DmodG.QrUpdPID1.Params[13].AsInteger := 0;//QrItensAFormato.Value;
    DmodG.QrUpdPID1.Params[14].AsInteger := 0;//QrItensASubstitui.Value;
    DmodG.QrUpdPID1.Params[15].AsString  := '';//QrItensASubstituicao.Value;
    DmodG.QrUpdPID1.Params[16].AsInteger := 0;//QrItensANulo.Value;
    DmodG.QrUpdPID1.Params[17].AsInteger := 0;//QrItensAControle.Value;
    DmodG.QrUpdPID1.Params[18].AsInteger := -1;//QrItensATab_Tipo.Value;
    DmodG.QrUpdPID1.Params[19].AsInteger := FLinAtu;
    DmodG.QrUpdPID1.Params[20].AsInteger := FPagina;
    DmodG.QrUpdPID1.ExecSQL;
  end;
end;

procedure TFrFormulasImpShowNew.InsereLinha(Tipo: Integer);
var
  Texto: String;
  z: Integer;
begin
  Insere_Divisor(FDivisorMid);
  ////////////////////////////////////////////////////////////////////////////
  FSomaT := FSomaT + FAltDados + FPula;
  F_T := Trunc(FSomaT                  / VAR_DOTIMP);
  F_R := Trunc(FAltDados               / VAR_DOTIMP);
  F_L := Trunc(0                       / VAR_DOTIMP);
  F_W := Trunc(2030                    / VAR_DOTIMP);
  ////////////////////////////////////////////////////////////////////////////
  case Tipo of
    0: for z := 1 to 17 do Texto := Texto + TextoAImprimir(z, Tipo);
    1: for z := 1 to 17 do Texto := Texto + TextoAImprimir(z, Tipo);
    2: Texto := CS(FormatDateTime('dd/mm/yy - hh:nn:ss', Now) ,' ', 136, 2);
    else Texto := '?*?';
  end;
  ////////////////////////////////////////////////////////////////////////////
  F_A := F_T + F_M;
  if Trim(Texto) <> '' then
  begin
    DModG.QrUpdPID1.Params[00].AsString  := Texto;
    DModG.QrUpdPID1.Params[01].AsString  := '';//QrItensAPrefixo.Value;
    DModG.QrUpdPID1.Params[02].AsString  := '';//QrItensASufixo.Value;
    DModG.QrUpdPID1.Params[03].AsInteger := F_A;
    DModG.QrUpdPID1.Params[04].AsInteger := F_L;//QrItensAPos_MEsq.Value+QrImprimePos_MEsq.Value;
    DModG.QrUpdPID1.Params[05].AsInteger := F_W;//QrItensAPos_Larg.Value;
    DModG.QrUpdPID1.Params[06].AsInteger := F_R;//QrItensAPos_Altu.Value;
    DModG.QrUpdPID1.Params[07].AsInteger := 1;//QrItensABand.Value;
    DModG.QrUpdPID1.Params[08].AsInteger := 0;//QrItensASet_Negr.Value;
    DModG.QrUpdPID1.Params[09].AsInteger := 0;//QrItensASet_Ital.Value;
    DModG.QrUpdPID1.Params[10].AsInteger := 0;//QrItensASet_Unde.Value;
    DModG.QrUpdPID1.Params[11].AsInteger := 0;//QrItensASet_T_DOS.Value;
    DModG.QrUpdPID1.Params[12].AsInteger := 0;//QrItensASet_TAli.Value;
    DModG.QrUpdPID1.Params[13].AsInteger := 0;//QrItensAFormato.Value;
    DModG.QrUpdPID1.Params[14].AsInteger := 0;//QrItensASubstitui.Value;
    DModG.QrUpdPID1.Params[15].AsString  := '';//QrItensASubstituicao.Value;
    DModG.QrUpdPID1.Params[16].AsInteger := 0;//QrItensANulo.Value;
    DModG.QrUpdPID1.Params[17].AsInteger := 0;//QrItensAControle.Value;
    DModG.QrUpdPID1.Params[18].AsInteger := -1;//QrItensATab_Tipo.Value;
    DModG.QrUpdPID1.Params[19].AsInteger := FLinAtu;
    DModG.QrUpdPID1.Params[20].AsInteger := FPagina;
    DModG.QrUpdPID1.ExecSQL;
  end;
end;

function TFrFormulasImpShowNew.InsereLinhaB(Tipo: Integer): String;
var
  Texto: String;
  z: Integer;
begin
  case Tipo of
    0: for z := 1 to 17 do Texto := Texto + TextoAImprimirB(z, Tipo);
    1: for z := 1 to 17 do Texto := Texto + TextoAImprimirB(z, Tipo);
    2: Texto := CS(FormatDateTime('dd/mm/yy - hh:nn:ss', Now) ,' ', 136, 2);
    else Texto := '?*?';
  end;
  ////////////////////////////////////////////////////////////////////////////
  Result := Texto;
end;

function TFrFormulasImpShowNew.TextoAImprimir(Campo: Integer; Tipo: Integer): String;
var
  Texto: String;
begin
  Texto := '';
  if Tipo=0 then
  begin
    case Campo of
      01: Texto := 'Seq.';
      02: Texto := String(MeuGetValue('Uso kg/L'));
      03: Texto := 'Uso %';
      04: Texto := 'C�d.';
      05: Texto := 'Nome do produto ou texto';
      06: Texto := 'B';
      07: Texto := '1:x';
      08: Texto := '�C';
      09: Texto := 'Roda';
      10: Texto := 'P�ra';
      11: Texto := 'pH min';
      12: Texto := 'pH m�x';
      13: Texto := 'B� min';
      14: Texto := 'B� m�x';
      15: Texto := 'Observa��es';
      16: Texto := String(MeuGetValue('Ini_txt'));
      17: Texto := String(MeuGetValue('Fin_txt'));
      else Texto := '***'
    end;
  end else begin
    case Campo of
      01: Texto := FormatFloat('00', QrFormulasItsSeq.Value);
      02: Texto := String(MeuGetValue('kgLCusto'));
      03: Texto := FormatFloat('#,##0.000; ; ', QrFormulasItsPorcent.Value);
      04: Texto := FormatFloat('0; ; ', QrFormulasItsProduto.Value);
      05: Texto := QrFormulasItsNome.Value;
      06: Texto := QrFormulasItsBoca.Value;
      07: Texto := FormatFloat('0;-0; ', QrFormulasItsDiluicao.Value);
      08: Texto := FormatFloat('0;-0; ', QrFormulasItsGraus.Value);
      09: Texto := FormatFloat('0;-0; ', QrFormulasItsTempoR.Value);
      10: Texto := FormatFloat('0;-0; ', QrFormulasItsTempoP.Value);
      11: Texto := FormatFloat('0;-0; ', QrFormulasItspH_Min.Value);
      12: Texto := FormatFloat('0;-0; ', QrFormulasItspH_Max.Value);
      13: Texto := FormatFloat('0;-0; ', QrFormulasItsBe_Min.Value);
      14: Texto := FormatFloat('0;-0; ', QrFormulasItsBe_Max.Value);
      15: Texto := FormatFloat('0;-0; ', QrFormulasItsGC_Min.Value);
      16: Texto := FormatFloat('0;-0; ', QrFormulasItsGC_Max.Value);
      17: Texto := QrFormulasItsObs.Value;
      18: Texto := String(MeuGetValue('Inicio'));
      19: Texto := String(MeuGetValue('Final'));
      else Texto := '***'
    end;
  end;
  case Campo of
    01: Texto := CS(Texto, ' ', FColunas[01], 0)+FDivVert;//?? Seq.Sub
    02: Texto := CS(Texto, ' ', FColunas[02], 2)+FDivVert;//?? Peso
    03: Texto := CS(Texto, ' ', FColunas[03], 2)+FDivVert;//09 %
    04: Texto := CS(Texto, ' ', FColunas[04], 2)+FDivVert;//04 Prod
    05: Texto := CS(Texto, ' ', FColunas[05], 0)+FDivVert;//?? Nome
    06: Texto := CS(Texto, ' ', FColunas[06], 1)+FDivVert;//01 Boca
    07: Texto := CS(Texto, ' ', FColunas[07], 1)+FDivVert;//02 Diluicao
    08: Texto := CS(Texto, ' ', FColunas[08], 1)+FDivVert;//02 Graus
    09: Texto := CS(Texto, ' ', FColunas[09], 1)+FDivVert;//03 TempoR
    10: Texto := CS(Texto, ' ', FColunas[10], 1)+FDivVert;//03 TempoP
    11: Texto := CS(Texto, ' ', FColunas[11], 1)+FDivVert;//05 pH Min
    12: Texto := CS(Texto, ' ', FColunas[12], 1)+FDivVert;//05 pH M�x
    13: Texto := CS(Texto, ' ', FColunas[13], 1)+FDivVert;// B� Min
    14: Texto := CS(Texto, ' ', FColunas[14], 1)+FDivVert;// B� M�x
    15: Texto := CS(Texto, ' ', FColunas[13], 1)+FDivVert;// GC Min
    16: Texto := CS(Texto, ' ', FColunas[14], 1)+FDivVert;// GC M�x
    17: Texto := CS(Texto, ' ', FColunas[15], 0)+FDivVert;//?? Obs
    18: Texto := CS(Texto, ' ', FColunas[16], 1)+FDivVert;//?? Inicio
    19: Texto := CS(Texto, ' ', FColunas[17], 1)+FDivVert;//?? Final
    else Texto := '***'
  end;
  FLinAtu := FLinAtu +1;
  Result := Texto;
end;

function TFrFormulasImpShowNew.TextoAImprimirB(Campo: Integer; Tipo: Integer): String;
var
  Divisor: Char;
  Texto: String;
begin
  Divisor := '|';//Char(Dmod.QrControleCharSepara_11.Value);
  Texto := '';
  if Tipo=0 then
  begin
    case Campo of
      01: Texto := 'Seq.';
      02: Texto := String(MeuGetValue('Uso kg/L'));
      03: Texto := 'Uso %';
      04: Texto := 'C�d.';
      05: Texto := 'Nome do produto ou texto';
      06: Texto := 'B';
      07: Texto := '1:x';
      08: Texto := '�C';
      09: Texto := 'Roda';
      10: Texto := 'P�ra';
      11: Texto := 'pH min';
      12: Texto := 'pH m�x';
      13: Texto := 'B� min';
      14: Texto := 'B� m�x';
      15: Texto := 'Observa��es';
      16: Texto := String(MeuGetValue('Ini_txt'));
      17: Texto := String(MeuGetValue('Fin_txt'));
      else Texto := '***'
    end;
  end else begin
    case Campo of
      01: Texto := FormatFloat('00', QrFormulasItsSeq.Value);
      02: Texto := String(MeuGetValue('kgLCusto'));
      03: Texto := FormatFloat('#,##0.000; ; ', QrFormulasItsPorcent.Value);
      04: Texto := FormatFloat('0; ; ', QrFormulasItsProduto.Value);
      05: Texto := QrFormulasItsNome.Value;
      06: Texto := QrFormulasItsBoca.Value;
      07: Texto := FormatFloat('0;-0; ', QrFormulasItsDiluicao.Value);
      08: Texto := FormatFloat('0;-0; ', QrFormulasItsGraus.Value);
      09: Texto := FormatFloat('0;-0; ', QrFormulasItsTempoR.Value);
      10: Texto := FormatFloat('0;-0; ', QrFormulasItsTempoP.Value);
      11: Texto := FormatFloat('0;-0; ', QrFormulasItspH_Min.Value);
      12: Texto := FormatFloat('0;-0; ', QrFormulasItspH_Max.Value);
      13: Texto := FormatFloat('0;-0; ', QrFormulasItsBe_Min.Value);
      14: Texto := FormatFloat('0;-0; ', QrFormulasItsBe_Max.Value);
      15: Texto := FormatFloat('0;-0; ', QrFormulasItsGC_Min.Value);
      16: Texto := FormatFloat('0;-0; ', QrFormulasItsGC_Max.Value);
      17: Texto := QrFormulasItsObs.Value;
      18: Texto := String(MeuGetValue('Inicio'));
      19: Texto := String(MeuGetValue('Final'));
      else Texto := '***'
    end;
  end;
  case Campo of
    01: Texto := Divisor+
                 CS(Texto, ' ', FColunas[01], 0)+Divisor;//?? Seq.Sub
    02: Texto := CS(Texto, ' ', FColunas[02], 2)+Divisor;//?? Peso
    03: Texto := CS(Texto, ' ', FColunas[03], 2)+Divisor;//09 %
    04: Texto := CS(Texto, ' ', FColunas[04], 2)+Divisor;//04 Prod
    05: Texto := CS(Texto, ' ', FColunas[05], 0)+Divisor;//?? Nome
    06: Texto := CS(Texto, ' ', FColunas[06], 1)+Divisor;//01 Boca
    07: Texto := CS(Texto, ' ', FColunas[07], 1)+Divisor;//02 Diluicao
    08: Texto := CS(Texto, ' ', FColunas[08], 1)+Divisor;//02 Graus
    09: Texto := CS(Texto, ' ', FColunas[09], 1)+Divisor;//03 TempoR
    10: Texto := CS(Texto, ' ', FColunas[10], 1)+Divisor;//03 TempoP
    11: Texto := CS(Texto, ' ', FColunas[11], 1)+Divisor;//05 pH Min
    12: Texto := CS(Texto, ' ', FColunas[12], 1)+Divisor;//05 pH M�x
    13: Texto := CS(Texto, ' ', FColunas[13], 1)+Divisor;// B� Min
    14: Texto := CS(Texto, ' ', FColunas[14], 1)+Divisor;// B� M�x
    15: Texto := CS(Texto, ' ', FColunas[13], 1)+Divisor;// GC Min
    16: Texto := CS(Texto, ' ', FColunas[14], 1)+Divisor;// GC M�x
    17: Texto := CS(Texto, ' ', FColunas[15], 0)+Divisor;//?? Obs
    18: Texto := CS(Texto, ' ', FColunas[16], 1)+Divisor;//?? Inicio
    19: Texto := CS(Texto, ' ', FColunas[17], 1)+Divisor;//?? Final
    else Texto := '***'
  end;
  FLinAtu := FLinAtu +1;
  Result := Texto;
end;

procedure TFrFormulasImpShowNew.Insere_Divisor(Separador: String); //Separador
var
  AltDivis: Integer;
begin
  if not (Dmod.QrControleDOSGrades.Value in ([1,3])) then Exit;
  AltDivis := Dmod.QrControleDOSDotLinBot.Value;
  FSomaT := FSomaT + AltDivis;
  F_L := Trunc(0                       / VAR_DOTIMP);
  F_T := Trunc(FSomaT                  / VAR_DOTIMP);
  F_W := Trunc(2030                    / VAR_DOTIMP);
  F_R := Trunc(AltDivis                / VAR_DOTIMP);
  F_A := F_T + F_M;
  FLinAtu := FLinAtu +1;
  DModG.QrUpdPID1.Params[00].AsString  := Separador;
  DModG.QrUpdPID1.Params[01].AsString  := '';//QrItensAPrefixo.Value;
  DModG.QrUpdPID1.Params[02].AsString  := '';//QrItensASufixo.Value;
  DModG.QrUpdPID1.Params[03].AsInteger := F_A;
  DModG.QrUpdPID1.Params[04].AsInteger := F_L;//QrItensAPos_MEsq.Value+QrImprimePos_MEsq.Value;
  DModG.QrUpdPID1.Params[05].AsInteger := F_W;//QrItensAPos_Larg.Value;
  DModG.QrUpdPID1.Params[06].AsInteger := F_R;//QrItensAPos_Altu.Value;
  DModG.QrUpdPID1.Params[07].AsInteger := 1;//QrItensABand.Value;
  DModG.QrUpdPID1.Params[08].AsInteger := 0;//QrItensASet_Negr.Value;
  DModG.QrUpdPID1.Params[09].AsInteger := 0;//QrItensASet_Ital.Value;
  DModG.QrUpdPID1.Params[10].AsInteger := 0;//QrItensASet_Unde.Value;
  DModG.QrUpdPID1.Params[11].AsInteger := 0;//QrItensASet_T_DOS.Value;
  DModG.QrUpdPID1.Params[12].AsInteger := 0;//QrItensASet_TAli.Value;
  DModG.QrUpdPID1.Params[13].AsInteger := 0;//QrItensAFormato.Value;
  DModG.QrUpdPID1.Params[14].AsInteger := 0;//QrItensASubstitui.Value;
  DModG.QrUpdPID1.Params[15].AsString  := '';//QrItensASubstituicao.Value;
  DModG.QrUpdPID1.Params[16].AsInteger := 0;//QrItensANulo.Value;
  DModG.QrUpdPID1.Params[17].AsInteger := 0;//QrItensAControle.Value;
  DModG.QrUpdPID1.Params[18].AsInteger := -1;//QrItensATab_Tipo.Value;
  DModG.QrUpdPID1.Params[19].AsInteger := FLinAtu;
  DModG.QrUpdPID1.Params[20].AsInteger := FPagina;
  DModG.QrUpdPID1.ExecSQL;
end;

procedure TFrFormulasImpShowNew.ImprimeMatricial;
var
  //Cancela,
  i, BTopo, LinhaAtual, MSupDOS, Substitui: Integer;
  Nulo: Boolean;
  Arquivo, Espaco, MyCPI, Linha, FormataA, FormataI, FormataNI, FormataNF, Texto: String;
begin
    { 2012-02-26 n�o funciona em SO 64 bits
    if Mostra = 2 then if not UnIO_DLL.CheckPort($0378) then Exit;
    }
  Screen.Cursor := crHourGlass;
  try
    // Compatibilidade
    UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
    //
    Arquivo := 'C:\Formula'+FormatDateTime('yymmdd"_"hhmmss', Now)+'.Txt';
    if FTipoImprime = 2 then AssignFile(FArqPrn, 'LPT1:')
    else AssignFile(FArqPrn, Arquivo);
    ReWrite(FArqPrn);
    //
    //
    Write(FArqPrn, Char($1B)+Char($52)+Char($12)); // Seleciona tabela de caracteres sul americana
    Write(FArqPrn, Char($1B)+Char($74)+Char($01)); // Seleciona Caracteres Graficos extendidos
    Write(FArqPrn, Char($1B)+Char($06));           // Habilita caracteres imprim�veis
    Write(FArqPrn, Char($1B)+Char($78));           // Seleciona Fonte Draft (r�pida)
    Write(FArqPrn, Char($1B)+Char($0F));           // Ativa   modo condensado
    //
    //Write(FArqPrn, #27't1'); // caracter table
    //Write(FArqPrn, #27+'%'+'1');
    Write(FArqPrn, #13);
    if FCPI = 12 then MyCPI := 'M' else MyCPI := 'P';
    Write(FArqPrn, #27+MyCPI);
    FormataA  := #15;
    FormataI  := #15;
    FormataNI := #15;
    FormataNF := #15;
    //
    QrPages.Close;
    UnDmkDAC_PF.AbreQuery(QrPages, DmodG.MyPID_DB);
    MSupDOS := QrImprimeMSupDOS.Value;
    for i := 0 to QrPagesPAGINAS.Value-1 do
    begin
      if (i < QrPagesPAGINAS.Value-1) then Substitui := 1 else Substitui := 0;
      LinhaAtual := 0;
      if i = 0 then
      begin
        if MSupDos < 0 then LinhaAtual := LinhaAtual - MSupDos else
          LinhaAtual := AvancaCarro(LinhaAtual, MSupDos);
      end;
      QrImprimeBand.Close;
      QrImprimeBand.Params[0].AsInteger := QrImprimeCodigo.Value;
      UnDmkDAC_PF.AbreQuery(QrImprimeBand, Dmod.MyDB);
      QrImprimeBand.First;
      //while not QrImprimeBand.Eof do
      //begin
        BTopo := QrImprimeBandPos_Topo.Value;
        if i > 0 then BTopo := BTopo + QrImprimePos_MSup2.Value;
        QrTopos1.Close;
        QrTopos1.Params[0].AsInteger := 1;//QrImprimeBandControle.Value;
        UnDmkDAC_PF.AbreQuery(QrTopos1, DmodG.MyPID_DB);
        QrTopos1.First;
        while not QrTopos1.Eof do
        begin
          LinhaAtual := AvancaCarro(LinhaAtual, QrTopos1DTopo.Value+BTopo);
          if not FPrintedLine then
          begin
            QrView1.Close;
            QrView1.Params[0].AsInteger := QrTopos1DTopo.Value;
            QrView1.Params[1].AsInteger := 1;//QrImprimeBandControle.Value;
            UnDmkDAC_PF.AbreQuery(QrView1, DmodG.MyPID_DB);
            QrView1.First;
            Linha := '';
            FTamLinha  := 0;
            FFoiExpand := 0;
            while not QrView1.Eof do
            begin
              case QrView1Set_T.Value of
                0: FormataA := #15;
                1: FormataA := #18;
                2: FormataA := #18#14;
              end;
              Espaco := EspacoEntreTextos(QrView1);
              if QrView1Nulo.Value = 1 then Nulo := True else Nulo := False;
                Texto := FormataTexto(Geral.SemAcento(QrView1Valor.Value),
                QrView1Prefixo.Value,
                QrView1Sufixo.Value, QrView1Substituicao.Value, Substitui *
                QrView1Substitui.Value, QrView1Set_A.Value, QrView1DComp.Value,
                QrView1Set_T.Value, QrView1Forma.Value, Nulo);
              //
              Linha := Linha + Espaco + FormataA + Texto;
              // J� usou expandido na mesma linha
              if QrView1Set_T.Value = 2 then FFoiExpand := 100;
              QrView1.Next;
            end;
            Write(FarqPrn, #27+'3'+#0);
            //WriteLn(FArqPrn, Geral.SemAcento(Linha));
            WriteLn(FArqPrn, Linha);
            FPrintedLine := True;
          end;
          QrTopos1.Next;
        end;
        //QrImprimeBand.Next;
      //end;
    end;
    Write(FArqPrn, #13);
    WriteLn(FArqPrn, #27+'0');
    Write(FArqPrn, #12);
    Write(FArqPrn, Char($1B)+Char($40)); //  Reseta
    CloseFile(FArqPrn);
    if FTipoImprime = 3 then
      Geral.MB_Aviso('Arquivo salvo: "'+Arquivo+'"');
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFrFormulasImpShowNew.MeuGetValue(ParName: String): Variant;
var
  CProd, CSolu, Custo, Formato: String;
  ParValue: Variant;
  AguaDil: Double;
begin
  if FPesoCalc < 1 then
    Formato := FFloat_15_4
  else
    Formato := FFloat_15_2;
  //
  CProd := CO_VAZIO;
  CSolu := CO_VAZIO;
  Custo := CO_VAZIO;
  if FCalcCustos or FCalcPrecos then
  begin
    if QrFormulasItsCUSTO_PQ.Value > 0 then
       CProd := FormatFloat(Formato, QrFormulasItsCUSTO_PQ.Value);
    //if QrFormulasItsCSolu.Value > 0 then
       CSolu := '';//FormatFloat(Formato, QrFormulasItsCSoluP.Value);
    if QrFormulasItsCUSTO_PQ.Value > 0 then
       Custo := FormatFloat(Formato, QrFormulasItsCUSTO_PQ.Value);
    if ParName = 'Uso kg/L' then ParValue := 'Custo item';
    if ParName = 'CustoTotal' then ParValue := FormatFloat(FFloat_15_4, FCustoTotal);
    if ParName = 'CustoTon' then ParValue := FormatFloat(FFloat_15_2, FCustokg*1000);
    if ParName = 'Custokg' then ParValue := FormatFloat(FFloat_15_4, FCustokg);
    if ParName = 'Custom2_WB' then ParValue := FormatFloat(FFloat_15_4, FCustoM2);
    if ParName = 'Custom2_SA' then ParValue := FormatFloat(FFloat_15_4, FCustoM2Semi);
    if ParName = 'Custo_total' then ParValue := 'Custo total:';
    if ParName = 'Custo_ton' then ParValue := 'Custo ton:';
    if ParName = 'Custo_kg' then ParValue := 'Custo kg:';
    if ParName = 'Custo_m' then ParValue := 'Custo m�:';
    if ParName = 'Custo_ft' then ParValue := 'Custo ft�:';
    if ParName = 'Ini_txt' then ParValue := 'C.Prod.';
    if ParName = 'Fin_txt' then ParValue := 'C.Outros';
    if ParName = 'Inicio' then ParValue := CProd;
    if ParName = 'Final' then ParValue := CSolu;
    if ParName = 'kgLCusto' then ParValue := Custo;
    if ParName = 'VARF_L_AGUA' then
    ParValue := '';

  end else begin
    if QrFormulasItsPESO_PQ.Value > 0 then
{Peso}Custo := FormatFloat(FFloat_15_3,QrFormulasItsPESO_PQ.Value);
    if ParName = 'Uso kg/L' then ParValue := ParName;
    if ParName = 'CustoTotal' then ParValue := CO_VAZIO;
    if ParName = 'CustoTon' then ParValue := CO_VAZIO;
    if ParName = 'Custokg' then ParValue := CO_VAZIO;
    if ParName = 'Custom2_WB' then ParValue := CO_VAZIO;
    if ParName = 'Custom2_SA' then ParValue := CO_VAZIO;
    if ParName = 'Custo_total' then ParValue := CO_VAZIO;
    if ParName = 'Custo_ton' then ParValue := CO_VAZIO;
    if ParName = 'Custo_kg' then ParValue := CO_VAZIO;
    if ParName = 'Custo_m' then ParValue := CO_VAZIO;
    if ParName = 'Custo_ft' then ParValue := CO_VAZIO;
    if ParName = 'Ini_txt' then ParValue := 'In�cio';
    if ParName = 'Fin_txt' then ParValue := 'Final';
    if ParName = 'Inicio' then ParValue := CO_TempoVAZIO;
    if ParName = 'Final' then ParValue := CO_TempoVAZIO;
    if ParName = 'kgLCusto' then ParValue := Custo;//Peso
    if ParName = 'VARF_L_AGUA' then
    begin
      ParValue := '';
      if QrFormulasItsDiluicao.Value <> 0 then
      begin
        AguaDil := QrFormulasItsDiluicao.Value * QrFormulasItsPESO_PQ.Value;
        if FPeso < 200 then
          ParValue := Geral.FFT(AguaDil, 3, siNegativo)
        else
          ParValue := Geral.FF0(Trunc(AguaDil));
      end;
      if QrFormulasItsBoca.Value <> '' then
        ParValue := ParValue + 'Boca'
    end;
  end;
  if ParName = 'Espessura' then ParValue := FLinhasRebx; // Compatibilidade
  if ParName = 'EspessuraRebx' then ParValue := FLinhasRebx;
  if ParName = 'EspessuraSemi' then ParValue := FLinhasSemi;
  if ParName = 'Qtde' then ParValue :=
                FormatFloat('#,###,##0.0',FQtde)+' '+FDefPeca;
  if ParName = 'Peso' then ParValue := FormatFloat(FFloat_15_3, FPeso)+' kg';
  if ParName = 'PesoMedio' then
    ParValue := FormatFloat(FFloat_15_3, FMedia)+' kg/'+FDefPeca;
  if ParName = 'Area' then
    ParValue := FormatFloat(FFloat_15_2, FArea);
  if ParName = 'FU' then ParValue := FFulao;
  if ParName = 'OS' then ParValue := '';//VAR_OS;
  if ParName = 'SP' then ParValue := VAR_SP;
  //if ParName = 'Lotes' then ParValue := Geral.Substitui(FObs, sLineBreak, ' ');
  if ParName = 'Lotes' then
    ParValue := FObs;
  Result := ParValue;
end;

function TFrFormulasImpShowNew.AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
var
  LinAtu: Integer;
begin
  NovaPosicao := Trunc(NovaPosicao * 250 / 90);
  if PosicaoAtual < NovaPosicao then
  begin
    LinAtu := Trunc((NovaPosicao - PosicaoAtual) / FAltLin);
    while LinAtu > 0 do
    begin
      if LinAtu > 216 then
      begin
        Write(FArqPrn, #27+'3'+#216);
        LinAtu := LinAtu - 216;
        WriteLn(FArqPrn,' ');
      end else begin
        Write(FArqPrn, MyPrinters.DefAlturaDotImp(3, LinAtu, ''));
        WriteLn(FArqPrn,' ');
        LinAtu := 0;
      end;
    end;
  end;
  Result := NovaPosicao;
  FPrintedLine := False;
end;

function TFrFormulasImpShowNew.EspacoEntreTextos(QrView: TmySQLQuery): String;
var
  Texto: String;
  i, CPI, Letras: Integer;
  Soma: Double;
begin
  Texto := '';
  if FTamLinha > QrView.FieldByName('DMEsq').AsInteger then
  begin
    // Erro !!!
  end;
  if FTamLinha < QrView.FieldByName('DMEsq').AsInteger then
  begin
    Texto := #15;
    if FCPI = 0 then CPI := 10 else CPI := FCPI;
    CPI := CPI + FFoiExpand;
    case CPI of
     010: CPI := 17;
     012: CPI := 20;
     110: CPI := 10;
     112: CPI := 10;
    end;
    Soma := (CO_POLEGADA * 100)/ CPI;
    Letras := Trunc((QrView.FieldByName('DMEsq').AsInteger - FTamLinha) / (CO_POLEGADA * 100) * CPI);
    for i := 1 to Letras do Texto := Texto + ' ';
    FTamLinha := FTamLinha + Trunc(Soma * Letras);
  end;
  Result := Texto;
end;

function TFrFormulasImpShowNew.CS(Texto, Compl: String; Tamanho, Centralizacao: Integer): String;
var
  Alinha: TAlignment;
begin
  if Length(Texto) > Tamanho then
  begin
    if Compl = '0' then Texto := Copy(Texto, Length(Texto)-Tamanho+1, Tamanho)
    else Texto := Copy(Texto, 1, Tamanho);
  end;
  case Centralizacao of
    0: Alinha := taLeftJustify;
    1: Alinha := taCenter;
    2: Alinha := taRightJustify;
    else Alinha := taLeftJustify;
  end;
  Result := Geral.CompletaString(Texto, Compl, Tamanho, Alinha, False);
end;

procedure TFrFormulasImpShowNew.PreparaDivisores;
var
  //i, j: Integer;
  FArqPrn: TextFile;
const
  Arquivo = 'C:\Dermatek\CarcteresUnicode.txt';
begin
  FColMax := 15;
  FColunas[01] := 05;
  FColunas[02] := 10;
  FColunas[03] := 10;
  FColunas[04] := 05;
  FColunas[05] := 26;
  FColunas[06] := 02;
  FColunas[07] := 03;
  FColunas[08] := 03;
  FColunas[09] := 04;
  FColunas[10] := 04;
  FColunas[11] := 05;
  FColunas[12] := 05;
  FColunas[13] := 05;
  FColunas[14] := 05;
  FColunas[15] := 24;
  FColunas[16] := 07;
  FColunas[17] := 07;
  //
  FDivisorTop := '_';//Char(Dmod.QrControleCharSepara_01.Value);
  FDivisorMid := '|';//Char(Dmod.QrControleCharSepara_04.Value);
  FDivisorBot := '_';//Char(Dmod.QrControleCharSepara_07.Value);
  (*for i := 1 to FColMax do
  begin
    for j := 1 to FColunas[i] do
    begin
      FDivisorTop := FDivisorTop + Char(Dmod.QrControleCharSepara_10.Value);
      FDivisorMid := FDivisorMid + Char(Dmod.QrControleCharSepara_10.Value);
      FDivisorBot := FDivisorBot + Char(Dmod.QrControleCharSepara_10.Value);
    end;
    if i < FColMax then
    begin
      FDivisorTop := FDivisorTop + Char(Dmod.QrControleCharSepara_02.Value);
      FDivisorMid := FDivisorMid + Char(Dmod.QrControleCharSepara_05.Value);
      FDivisorBot := FDivisorBot + Char(Dmod.QrControleCharSepara_08.Value);
    end;
  end;
  FDivisorTop := FDivisorTop + Char(Dmod.QrControleCharSepara_03.Value);
  FDivisorMid := FDivisorMid + Char(Dmod.QrControleCharSepara_06.Value);
  FDivisorBot := FDivisorBot + Char(Dmod.QrControleCharSepara_09.Value);
  //*)
  AssignFile(FArqPrn, Arquivo);
  ReWrite(FArqPrn);
  Write(FArqPrn, Char($1B)+Char($52)+Char($12)); // Seleciona tabela de caracteres sul americana
  Write(FArqPrn, Char($1B)+Char($74)+Char($01)); // Seleciona Caracteres Graficos extendidos
  Write(FArqPrn, Char($1B)+Char($06));           // Habilita caracteres imprim�veis
  Write(FArqPrn, Char($1B)+Char($78));           // Seleciona Fonte Draft (r�pida)
  Write(FArqPrn, Char($1B)+Char($0F));           // Ativa   modo condensado
  //Write(FArqPrn, Char($12));                   // Cancela modo condensado
  Write(FArqPrn, Char($1B)+Char($50));           // Seleciona 10 CPI
  //
  WriteLn(FArqPrn, FDivisorTop);
  WriteLn(FArqPrn, FDivisorMid);
  WriteLn(FArqPrn, FDivisorBot);
  WriteLn(FArqPrn, '');
  (*WriteLn(FArqPrn, Char(C01)+Char(C10)+Char(C02)+Char(C10)+Char(C03));
  WriteLn(FArqPrn, Char(C11)+Char(C_A)+Char(C11)+Char(C_B)+Char(C11));
  WriteLn(FArqPrn, Char(C04)+Char(C10)+Char(C05)+Char(C10)+Char(C06));
  WriteLn(FArqPrn, Char(C11)+Char(C_C)+Char(C11)+Char(C_D)+Char(C11));
  WriteLn(FArqPrn, Char(C07)+Char(C10)+Char(C08)+Char(C10)+Char(C09));*)
  //
  Write(FArqPrn, Char($1B)+Char($40));           // ESC @  (Reseta (inicializa) impressora)
  CloseFile(FArqPrn);
end;

procedure TFrFormulasImpShowNew.QrLFormItsACalcFields(DataSet: TDataSet);
var
  Tempo, Min, Max, Medida: String;
begin
  if QrLFormItsAPorcent.Value <= 0 then QrLFormItsAPERC_TXT.Value := '' else
  QrLFormItsAPERC_TXT.Value :=
    Geral.FFT(QrLFormItsAPorcent.Value, 3, siPositivo) + ' %';
  //
  if QrLFormItsAInsumo.Value > 0 then Medida := ' kg' else Medida := ' L ';
  if QrLFormItsAPeso.Value <= 0 then QrLFormItsAPESO_TXT.Value := '' else
  QrLFormItsAPESO_TXT.Value := Geral.FFT(QrLFormItsAPeso.Value, 3, siPositivo)
  + Medida;
  //
  {if BDC_APRESENTACAO_IMPRESSAO = 10 then
    QrLFormItsATEXT_TXT.Value := '- * - * -'
  else}
    QrLFormItsATEXT_TXT.Value := QrLFormItsAPERC_TXT.Value +
      QrLFormItsAPESO_TXT.Value + QrLFormItsADescricao.Value +
      QrLFormItsAObservacos.Value;

  if BDC_APRESENTACAO_IMPRESSAO = 10 then
    QrLFormItsAMOSTRA_LINHA.Value := True
  else
    QrLFormItsAMOSTRA_LINHA.Value := Trim(QrLFormItsAPERC_TXT.Value +
      QrLFormItsAPESO_TXT.Value + QrLFormItsADescricao.Value +
      QrLFormItsAObservacos.Value) <> '';

  if Trim(QrLFormItsAPERC_TXT.Value +
    QrLFormItsAPESO_TXT.Value + QrLFormItsADescricao.Value) <> '' then
      QrLFormItsAMOSTRA_GRADE.Value := 0.1
    else
      QrLFormItsAMOSTRA_GRADE.Value := 0;

  if QrLFormItsADiluicao.Value = 0 then QrLFormItsATXT_DILUICAO.Value := ''
  else QrLFormItsATXT_DILUICAO.Value := 'Diluir em '+Geral.FFT(
    QrLFormItsAPeso.Value * QrLFormItsADiluicao.Value, 3, siPositivo)+
    ' litros de �gua.';
  //
  Tempo := '';
  if QrLFormItsATempoR.Value > 0 then Tempo := Tempo +
    'Rodar ' + dmkPF.HorasMHT(QrLFormItsATempoR.Value, True);
  if (QrLFormItsATempoR.Value > 0) and (QrLFormItsATempoP.Value > 0) then
    Tempo := Tempo + ' e ';
  if QrLFormItsATempoP.Value > 0 then Tempo := Tempo +
    'Parar ' + dmkPF.HorasMHT(QrLFormItsATempoP.Value, True);
  if (QrLFormItsATempoR.Value + QrLFormItsATempoR.Value) > 0 then
    Tempo := Tempo + '    In�cio: ________ Final: ________';
  if (QrLFormItsApH_Min.Value <> 0) or (QrLFormItsApH_Max.Value <> 0) then
  begin
    Min := Geral.FFT(QrLFormItsApH_Min.Value, 2, siPositivo);
    Max := Geral.FFT(QrLFormItsApH_Max.Value, 2, siPositivo);
    if (QrLFormItsApH_Min.Value <> 0) and (QrLFormItsApH_Max.Value <> 0) then
      Tempo := Tempo + ' pH ('+ Min + ' a ' +Max + '):__________'
    else if (QrLFormItsApH_Min.Value <> 0) then
      Tempo := Tempo + ' pH ('+Min + '):__________'
    else Tempo := Tempo + ' pH ('+Max + '):__________';
  end;
  if (QrLFormItsABe_Min.Value <> 0) or (QrLFormItsABe_Max.Value <> 0)  then
  begin
    Min := Geral.FFT(QrLFormItsABe_Min.Value, 2, siPositivo);
    Max := Geral.FFT(QrLFormItsABe_Max.Value, 2, siPositivo);
    if (QrLFormItsABe_Min.Value <> 0) and (QrLFormItsABe_Max.Value <> 0)  then
      Tempo := Tempo + ' B� ('+ Min + ' a ' + Max +'):__________'
    else if (QrLFormItsABe_Min.Value <> 0) then
      Tempo := Tempo + ' B� ('+ Min + '):__________'
    else
      Tempo := Tempo + ' B� ('+ Max + '):__________';
  end;
  if (QrLFormItsAGC_Min.Value <> 0) or (QrLFormItsAGC_Max.Value <> 0)  then
  begin
    Min := FloatToStr(QrLFormItsAGC_Min.Value);
    Max := FloatToStr(QrLFormItsAGC_Max.Value);
    if (QrLFormItsAGC_Min.Value <> 0) and (QrLFormItsAGC_Max.Value <> 0)  then
      Tempo := Tempo + ' �C ('+ Min + ' a ' + Max +'):__________'
    else if (QrLFormItsAGC_Min.Value <> 0) then
      Tempo := Tempo + ' �C ('+ Min + '):__________'
    else
      Tempo := Tempo + ' �C ('+ Max + '):__________';
  end;
  if QrLFormItsAGraus.Value <> 0 then Tempo := Tempo + ' �C ('+FloatToStr(
    QrLFormItsAGraus.Value)+'):______';
  if Tempo <> '' then Tempo := Tempo + ' Respons�vel:_______________________';
  QrLFormItsATEMPO_TXT.Value := Tempo;
  QrLFormItsATEMPO_R_TXT.Value := dmkPF.IntToTempo(QrLFormItsATempoR.Value);
  //
  QrLFormItsADESCRI_TEMP.Value := QrLFormItsADescricao.Value;
  if QrLFormItsAGraus.Value <> 0 then
  // Parei Aqui??
    QrLFormItsADESCRI_TEMP.Value := QrLFormItsADescricao.Value + ' ' +
    FloatToStr(QrLFormItsAGraus.Value) + ' �C';

end;

procedure TFrFormulasImpShowNew.QrLFormItsLCalcFields(DataSet: TDataSet);
var
  Tempo, Min, Max, Medida: String;
begin
  if QrLFormItsLPorcent.Value <= 0 then QrLFormItsLPERC_TXT.Value := '' else
  QrLFormItsLPERC_TXT.Value :=
    Geral.FFT(QrLFormItsLPorcent.Value, 3, siPositivo) + ' %';
  //
  if QrLFormItsLInsumo.Value > 0 then Medida := ' kg' else Medida := ' L ';
  if QrLFormItsLPeso.Value <= 0 then QrLFormItsLPESO_TXT.Value := '' else
  QrLFormItsLPESO_TXT.Value := Geral.FFT(QrLFormItsLPeso.Value, 3, siPositivo)
  + Medida;
  //
  {if BDC_APRESENTACAO_IMPRESSAO = 10 then
    QrLFormItsLTEXT_TXT.Value := '- * - * -'
  else}
    QrLFormItsLTEXT_TXT.Value := QrLFormItsLPERC_TXT.Value +
      QrLFormItsLPESO_TXT.Value + QrLFormItsLDescricao.Value +
      QrLFormItsLObservacos.Value;

  if BDC_APRESENTACAO_IMPRESSAO = 10 then
    QrLFormItsLMOSTRA_LINHA.Value := True
  else
    QrLFormItsLMOSTRA_LINHA.Value := Trim(QrLFormItsLPERC_TXT.Value +
      QrLFormItsLPESO_TXT.Value + QrLFormItsLDescricao.Value +
      QrLFormItsLObservacos.Value) <> '';

  if Trim(QrLFormItsLPERC_TXT.Value +
    QrLFormItsLPESO_TXT.Value + QrLFormItsLDescricao.Value) <> '' then
      QrLFormItsLMOSTRA_GRADE.Value := 0.1
    else
      QrLFormItsLMOSTRA_GRADE.Value := 0;

  if QrLFormItsLDiluicao.Value = 0 then QrLFormItsLTXT_DILUICAO.Value := ''
  else QrLFormItsLTXT_DILUICAO.Value := 'Diluir em '+Geral.FFT(
    QrLFormItsLPeso.Value * QrLFormItsLDiluicao.Value, 3, siPositivo)+
    ' litros de �gua.';
  //
  Tempo := '';
  if QrLFormItsLTempoR.Value > 0 then Tempo := Tempo +
    'Rodar ' + dmkPF.HorasMHT(QrLFormItsLTempoR.Value, True);
  if (QrLFormItsLTempoR.Value > 0) and (QrLFormItsLTempoP.Value > 0) then
    Tempo := Tempo + ' e ';
  if QrLFormItsLTempoP.Value > 0 then Tempo := Tempo +
    'Parar ' + dmkPF.HorasMHT(QrLFormItsLTempoP.Value, True);
  if (QrLFormItsLTempoR.Value + QrLFormItsLTempoR.Value) > 0 then
    Tempo := Tempo + '    In�cio: ________ Final: ________';
  if (QrLFormItsLpH_Min.Value <> 0) or (QrLFormItsLpH_Max.Value <> 0) then
  begin
    Min := Geral.FFT(QrLFormItsLpH_Min.Value, 2, siPositivo);
    Max := Geral.FFT(QrLFormItsLpH_Max.Value, 2, siPositivo);
    if (QrLFormItsLpH_Min.Value <> 0) and (QrLFormItsLpH_Max.Value <> 0) then
      Tempo := Tempo + ' pH ('+ Min + ' a ' +Max + '):__________'
    else if (QrLFormItsLpH_Min.Value <> 0) then
      Tempo := Tempo + ' pH ('+Min + '):__________'
    else Tempo := Tempo + ' pH ('+Max + '):__________';
  end;
  if (QrLFormItsLBe_Min.Value <> 0) or (QrLFormItsLBe_Max.Value <> 0)  then
  begin
    Min := Geral.FFT(QrLFormItsLBe_Min.Value, 2, siPositivo);
    Max := Geral.FFT(QrLFormItsLBe_Max.Value, 2, siPositivo);
    if (QrLFormItsLBe_Min.Value <> 0) and (QrLFormItsLBe_Max.Value <> 0)  then
      Tempo := Tempo + ' B� ('+ Min + ' a ' + Max +'):__________'
    else if (QrLFormItsLBe_Min.Value <> 0) then
      Tempo := Tempo + ' B� ('+ Min + '):__________'
    else
      Tempo := Tempo + ' B� ('+ Max + '):__________';
  end;
  if (QrLFormItsLGC_Min.Value <> 0) or (QrLFormItsLGC_Max.Value <> 0)  then
  begin
    Min := FloatToStr(QrLFormItsLGC_Min.Value);
    Max := FloatToStr(QrLFormItsLGC_Max.Value);
    if (QrLFormItsLGC_Min.Value <> 0) and (QrLFormItsLGC_Max.Value <> 0)  then
      Tempo := Tempo + ' �C ('+ Min + ' a ' + Max +'):__________'
    else if (QrLFormItsLGC_Min.Value <> 0) then
      Tempo := Tempo + ' �C ('+ Min + '):__________'
    else
      Tempo := Tempo + ' �C ('+ Max + '):__________';
  end;
  if QrLFormItsLGraus.Value <> 0 then Tempo := Tempo + ' �C ('+FloatToStr(
    QrLFormItsLGraus.Value)+'):______';
  if Tempo <> '' then Tempo := Tempo + ' Respons�vel:_______________________';
  QrLFormItsLTEMPO_TXT.Value := Tempo;
  QrLFormItsLTEMPO_R_TXT.Value := dmkPF.IntToTempo(QrLFormItsLTempoR.Value);
  //
  QrLFormItsLDESCRI_TEMP.Value := QrLFormItsLDescricao.Value;
  if QrLFormItsLGraus.Value <> 0 then
  // Parei Aqui??
    QrLFormItsLDESCRI_TEMP.Value := QrLFormItsLDescricao.Value + ' ' +
    FloatToStr(QrLFormItsLGraus.Value) + ' �C';

end;

function TFrFormulasImpShowNew.Emite(Emit: Integer): Boolean;
var
  Controle: Integer;
var
  DataEmis, NOMECI, NOMESETOR, Tecnico, NOME, Espessura, DefPeca, Fulao,
  Obs: String;
  Codigo, Status, Numero, ClienteI, Tipificacao, TempoR, TempoP, Setor, Tipific,
  SetrEmi, Cod_Espess, CodDefPeca: Integer;
  Peso, Custo, Qtde, AreaM2: Double;
var
  (*Codigo, Controle,*) PQCI, Cli_Orig, Cli_Dest, MoedaPadrao, (*Numero,*)
  Ordem, Produto, (*TempoR, TempoP,*) ProdutoCI, GraGruX, Ativo, SourcMP,
  GraCorCad, VSMovCod, SemiCodEspReb, Empresa: Integer;
  CustoMedio, CusIntrn, CustoPadrao, Porcent, pH, Be, (*Custo,*) Graus, Peso_PQ, pH_Min,
  pH_Max, Be_Min, Be_Max, GC_Min, GC_Max, GramasTi, GramasKg, GramasTo,
  PrecoMo_Kg, Cotacao_Mo, CustoRS_Kg, CustoRS_To, Minimo, Maximo, Diluicao,
  SemiAreaM2, SemiRendim, BRL_USD, BRL_EUR: Double;
  NomePQ, (*Obs,*) Boca, Processo, ObsProces, SiglaMoeda, DtaCambio, HoraIni,
  NoGraCorCad, SemiTxtEspReb, DtaBaixa, DtCorrApo: String;
  Densidade, LITROS_PQ: Double;
  UsarDensid, Versao: Integer;
  NxLotes, SiglaIQ: String;
begin
  try
    if FProgressBar1 <> nil then
    begin
      FProgressBar1.Max := QrFormulasIts.RecordCount + 1;
      FProgressBar1.Position := 0;
      FProgressBar1.Visible := True;
    end;
    Screen.Cursor := crHourGlass;
(*
    Dmod.QrUpdW.SQL.Clear;
    Dmod.QrUpdW.SQL.Add('INSERT INTO Emit SET');
    Dmod.QrUpdW.SQL.Add('DataEmis=:P0, Status=:P1, Numero=:P2, NOMECI=:P3, ');
    Dmod.QrUpdW.SQL.Add('NOMESETOR=:P4, Tecnico=:P5, Nome=:P6, ClienteI=:P7, ');
    Dmod.QrUpdW.SQL.Add('Tipificacao=:P8, TempoR=:P9, TempoP=:P10, Setor=:P11, ');
    Dmod.QrUpdW.SQL.Add('Espessura=:P12, Peso=:P13, Qtde=:P14, AreaM2=:P15, ');
    Dmod.QrUpdW.SQL.Add('Fulao=:P16, DefPeca=:P17, Obs=:P18, Tipific=:P19, ');
    Dmod.QrUpdW.SQL.Add('Codigo=:Pa');
    Dmod.QrUpdW.Params[00].AsDateTime  := Now();
    Dmod.QrUpdW.Params[01].AsInteger   := 0;
    Dmod.QrUpdW.Params[02].AsInteger   := QrFormulasNumero.Value;
    Dmod.QrUpdW.Params[03].AsString    := FCliIntTxt;
    Dmod.QrUpdW.Params[04].AsString    := QrFormulasNOMESETOR.Value;
    Dmod.QrUpdW.Params[05].AsString    := QrFormulasTecnico.Value;
    Dmod.QrUpdW.Params[06].AsString    := QrFormulasNome.Value;
    Dmod.QrUpdW.Params[07].AsInteger   := FCliIntCod;
    Dmod.QrUpdW.Params[08].AsInteger   := QrFormulasTipificacao.Value;
    Dmod.QrUpdW.Params[09].AsInteger   := QrFormulasTempoR.Value;
    Dmod.QrUpdW.Params[10].AsInteger   := QrFormulasTempoP.Value;
    Dmod.QrUpdW.Params[11].AsInteger   := QrFormulasSetor.Value;
    Dmod.QrUpdW.Params[12].AsString    := FLinhas;
    Dmod.QrUpdW.Params[13].AsFloat     := FPeso;
    Dmod.QrUpdW.Params[14].AsFloat     := FQtde;
    Dmod.QrUpdW.Params[15].AsFloat     := FArea;
    Dmod.QrUpdW.Params[16].AsString    := FFulao;
    Dmod.QrUpdW.Params[17].AsString    := FDefPeca;
    Dmod.QrUpdW.Params[18].AsString    := FObs;
    Dmod.QrUpdW.Params[19].AsInteger   := FTipific;
    //
    Dmod.QrUpdW.Params[20].AsInteger   := Emit;
    Dmod.QrUpdW.ExecSQL;
*)

    Codigo              := Emit;
    DataEmis            := Geral.FDT(Now(), 1);
    DtaBaixa            := Geral.FDT(FDataP, 1);
    DtCorrApo           := FDtCorrApo;
    Empresa             := FEmpresa;
    Status              := 0;
    Numero              := QrFormulasNumero.Value;
    NOMECI              := FCliIntTxt;
    NOMESETOR           := QrFormulasNOMESETOR.Value;
    Tecnico             := QrFormulasTecnico.Value;
    Nome                := QrFormulasNome.Value;
    ClienteI            := FCliIntCod;
    Tipificacao         := QrFormulasTipificacao.Value;
    TempoR              := QrFormulasTempoR.Value;
    TempoP              := QrFormulasTempoP.Value;
    Setor               := QrFormulasSetor.Value;
    Espessura           := FLinhasRebx;
    SemiTxtEspReb       := FLinhasSemi;
    DefPeca             := FDefPeca;
    Peso                := FPeso;
    Custo               := 0;
    Qtde                := FQtde;
    AreaM2              := FArea;
    Fulao               := FFulao;
    Obs                 := FObs;
    Tipific             := FTipific;
    SetrEmi             := CO_SetrEmi_MOLHADO;
    SourcMP             := FSourcMP; // CO_SourcMP_WetSome;
    Cod_Espess          := FCod_Espess;
    SemiCodEspReb       := FCod_Rebaix;
    CodDefPeca          := FCodDefPeca;
    SemiAreaM2          := FSemiAreaM2;
    SemiRendim          := FSemiRendim;
    BRL_USD             := FBRL_USD;
    BRL_EUR             := FBRL_EUR;
    DtaCambio           := Geral.FDT(FDtaCambio, 1);
    VSMovCod            := FVSMovCod;
    HoraIni             := Geral.FDT(FHoraIni, 100);
    GraCorCad           := QrFormulasGraCorCad.Value;
    NoGraCorCad         := QrFormulasNoGraCorCad.Value;
    SemiTxtEspReb       := FLinhasSemi;
    Versao              := QrFormulasVersao.Value;
    //

    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'emit', False, [
    'DataEmis', 'Status', 'Numero',
    'NOMECI', 'NOMESETOR', 'Tecnico',
    'NOME', 'ClienteI', 'Tipificacao',
    'TempoR', 'TempoP', 'Setor',
    'Espessura', 'DefPeca', 'Peso',
    'Custo', 'Qtde', 'AreaM2',
    'Fulao', 'Obs', 'Tipific',
    'SetrEmi', 'SourcMP', 'Cod_Espess',
    'CodDefPeca', 'EmitGru', 'Retrabalho',
    'SemiAreaM2', 'SemiRendim',
    'BRL_USD', 'BRL_EUR', 'DtaCambio',
    'VSMovCod', 'HoraIni', 'GraCorCad',
    'NoGraCorCad', 'SemiCodEspReb', 'SemiTxtEspReb',
    'DtaBaixa', 'DtCorrApo', 'Versao'], [
    'Codigo'], [
    DataEmis, Status, Numero,
    NOMECI, NOMESETOR, Tecnico,
    NOME, ClienteI, Tipificacao,
    TempoR, TempoP, Setor,
    Espessura, DefPeca, Peso,
    Custo, Qtde, AreaM2,
    Fulao, Obs, Tipific,
    SetrEmi, SourcMP, Cod_Espess,
    CodDefPeca, FEmitGru, FRetrabalho,
    SemiAreaM2, SemiRendim,
    BRL_USD, BRL_EUR, DtaCambio,
    VSMovCod, HoraIni, GraCorCad,
    NoGraCorCad, SemiCodEspReb, SemiTxtEspReb,
    DtaBaixa, DtCorrApo, Versao], [
    Codigo], True) then
    begin
      if FProgressBar1 <> nil then
      begin
        FProgressBar1.Position := FProgressBar1.Position + 1;
        FProgressBar1.Update;
      end;
      //
{
      Dmod.QrUpdW.SQL.Clear;
      Dmod.QrUpdW.SQL.Add('INSERT INTO EmitIts SET');
      Dmod.QrUpdW.SQL.Add('NOMEPQ=:P0, PQCI=:P1, CustoPadrao=:P2, MoedaPadrao=:P3, ');
      Dmod.QrUpdW.SQL.Add('Numero=:P4, Ordem=:P5, Porcent=:P6, Produto=:P7, ');
      Dmod.QrUpdW.SQL.Add('TempoR=:P8, TempoP=:P9, pH_Min=:P10, pH_Max=:P11, ');
      Dmod.QrUpdW.SQL.Add('Be_Min=:P12, Be_Max=:P13, GC_Min=:P14, GC_Max=:P15, ');
      Dmod.QrUpdW.SQL.Add('Obs=:P16, Diluicao=:P17, Custo=:P18, Minimo=:P19, ');
      Dmod.QrUpdW.SQL.Add('Maximo=:P20, Graus=:P21, Boca=:P22, Processo=:P23, ');
      Dmod.QrUpdW.SQL.Add('ObsProces=:P24, Ativo=:P25, Peso_PQ=:P26, ');
      Dmod.QrUpdW.SQL.Add('ProdutoCI=:P27, Cli_Orig=:P28, Cli_Dest=:P29, ');
      //
      Dmod.QrUpdW.SQL.Add('Codigo=:Pa, Controle=:Pb');
}
      QrFormulasIts.First;
      while not QrFormulasIts.Eof do
      begin
        Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'EmitIts',
          'EmitIts', 'Controle');
(*
        Dmod.QrUpdW.Params[00].AsString  := QrFormulasItsNome.Value;
        Dmod.QrUpdW.Params[01].AsInteger := QrFormulasItsPQCI.Value;
        Dmod.QrUpdW.Params[02].AsFloat   := QrFormulasItsCustoPadrao.Value;
        Dmod.QrUpdW.Params[03].AsInteger := QrFormulasItsMoedaPadrao.Value;
        Dmod.QrUpdW.Params[04].AsInteger := QrFormulasItsNumero.Value;
        Dmod.QrUpdW.Params[05].AsInteger := QrFormulasItsOrdem.Value;
        Dmod.QrUpdW.Params[06].AsFloat   := QrFormulasItsPorcent.Value;
        Dmod.QrUpdW.Params[07].AsInteger := QrFormulasItsProduto.Value;
        Dmod.QrUpdW.Params[08].AsInteger := QrFormulasItsTempoR.Value;
        Dmod.QrUpdW.Params[09].AsInteger := QrFormulasItsTempoP.Value;
        Dmod.QrUpdW.Params[10].AsFloat   := QrFormulasItspH_Min.Value;
        Dmod.QrUpdW.Params[11].AsFloat   := QrFormulasItspH_Max.Value;
        Dmod.QrUpdW.Params[12].AsFloat   := QrFormulasItsBe_Min.Value;
        Dmod.QrUpdW.Params[13].AsFloat   := QrFormulasItsBe_Max.Value;
        Dmod.QrUpdW.Params[14].AsFloat   := QrFormulasItsGC_Min.Value;
        Dmod.QrUpdW.Params[15].AsFloat   := QrFormulasItsGC_Max.Value;
        Dmod.QrUpdW.Params[16].AsString  := QrFormulasItsObs.Value;
        Dmod.QrUpdW.Params[17].AsFloat   := QrFormulasItsDiluicao.Value;
        Dmod.QrUpdW.Params[18].AsFloat   := QrFormulasItsCusto.Value;
        Dmod.QrUpdW.Params[19].AsFloat   := QrFormulasItsMinimo.Value;
        Dmod.QrUpdW.Params[20].AsFloat   := QrFormulasItsMaximo.Value;
        Dmod.QrUpdW.Params[21].AsFloat   := QrFormulasItsGraus.Value;
        Dmod.QrUpdW.Params[22].AsString  := QrFormulasItsBoca.Value;
        Dmod.QrUpdW.Params[23].AsString  := QrFormulasItsProcesso.Value;
        Dmod.QrUpdW.Params[24].AsString  := QrFormulasItsObsProces.Value;
        Dmod.QrUpdW.Params[25].AsFloat   := QrFormulasItsAtivo.Value;
        Dmod.QrUpdW.Params[26].AsFloat   := QrFormulasItsPESO_PQ.Value;
        Dmod.QrUpdW.Params[27].AsInteger := QrFormulasItsPRODUTOCI.Value;
        Dmod.QrUpdW.Params[28].AsInteger := FCliIntCod;
        Dmod.QrUpdW.Params[29].AsInteger := FCliIntCod;
        //
        Dmod.QrUpdW.Params[30].AsInteger := Emit;
        Dmod.QrUpdW.Params[31].AsInteger := Controle;
        Dmod.QrUpdW.ExecSQL;
*)
        Codigo          := Emit;
        //Controle        := Controle;
        NOMEPQ          := QrFormulasItsNome.Value;
        PQCI            := QrFormulasItsPQCI.Value;
        Cli_Orig        := FCliIntCod;
        Cli_Dest        := FCliIntCod;
        CustoMedio      := QrFormulasItsCustoMedio.Value;
        CusIntrn        := QrFormulasItsCusIntrn.Value;
        CustoPadrao     := QrFormulasItsCustoPadrao.Value;
        MoedaPadrao     := QrFormulasItsMoedaPadrao.Value;
        Numero          := QrFormulasItsNumero.Value;
        Ordem           := QrFormulasItsOrdem.Value;
        Porcent         := QrFormulasItsPorcent.Value;
        Produto         := QrFormulasItsProduto.Value;
        TempoR          := QrFormulasItsTempoR.Value;
        TempoP          := QrFormulasItsTempoP.Value;
        pH              := 0;
        Be              := 0;
        Obs             := QrFormulasItsObs.Value;
        Diluicao        := QrFormulasItsDiluicao.Value;
        Custo           := QrFormulasItsCusto.Value;
        Minimo          := QrFormulasItsMinimo.Value;
        Maximo          := QrFormulasItsMaximo.Value;
        Graus           := QrFormulasItsGraus.Value;
        Boca            := QrFormulasItsBoca.Value;
        Processo        := QrFormulasItsProcesso.Value;
        ObsProces       := QrFormulasItsObsProces.Value;
        Peso_PQ         := QrFormulasItsPESO_PQ.Value;
        ProdutoCI       := QrFormulasItsPRODUTOCI.Value;
        pH_Min          := QrFormulasItspH_Min.Value;
        pH_Max          := QrFormulasItspH_Max.Value;
        Be_Min          := QrFormulasItsBe_Min.Value;
        Be_Max          := QrFormulasItsBe_Max.Value;
        GC_Min          := QrFormulasItsGC_Min.Value;
        GC_Max          := QrFormulasItsGC_Max.Value;
        GraGruX         := 0;
        GramasTi        := 0;
        GramasKg        := 0;
        GramasTo        := 0;
        SiglaMoeda      := 'R$';
        PrecoMo_Kg      := 0;
        Cotacao_Mo      := 0;
        CustoRS_Kg      := 0;
        CustoRS_To      := 0;
        Densidade       := QrFormulasItsDensidade.Value;
        UsarDensid      := QrFormulasItsUsarDensid.Value;
        LITROS_PQ       := QrFormulasItsLITROS_PQ.Value;
        NxLotes         := QrFormulasItsNxLotes.Value;
        SiglaIQ         := QrFormulasItsSiglaIQ.Value;


        //
        Ativo           := QrFormulasItsAtivo.Value;
        //
        Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'emitits', False, [
        'Codigo', 'NomePQ', 'PQCI',
        'Cli_Orig', 'Cli_Dest', 'CustoPadrao', 'CustoMedio', 'CusIntrn',
        'MoedaPadrao', 'Numero', 'Ordem',
        'Porcent', 'Produto', 'TempoR',
        'TempoP', 'pH', 'Be',
        'Obs', 'Diluicao', 'Custo',
        'Minimo', 'Maximo', 'Graus',
        'Boca', 'Processo', 'ObsProces',
        'Peso_PQ', 'ProdutoCI', 'pH_Min',
        'pH_Max', 'Be_Min', 'Be_Max',
        'GC_Min', 'GC_Max', 'GraGruX',
        'GramasTi', 'GramasKg', 'GramasTo',
        'SiglaMoeda', 'PrecoMo_Kg', 'Cotacao_Mo',
        'CustoRS_Kg', 'CustoRS_To',
        'Densidade', 'UsarDensid', 'LITROS_PQ',
        'NxLotes', 'SiglaIQ',
        'Ativo'], [
        'Controle'], [
        Codigo, NomePQ, PQCI,
        Cli_Orig, Cli_Dest, CustoPadrao, CustoMedio, CusIntrn,
        MoedaPadrao, Numero, Ordem,
        Porcent, Produto, TempoR,
        TempoP, pH, Be,
        Obs, Diluicao, Custo,
        Minimo, Maximo, Graus,
        Boca, Processo, ObsProces,
        Peso_PQ, ProdutoCI, pH_Min,
        pH_Max, Be_Min, Be_Max,
        GC_Min, GC_Max, GraGruX,
        GramasTi, GramasKg, GramasTo,
        SiglaMoeda, PrecoMo_Kg, Cotacao_Mo,
        CustoRS_Kg, CustoRS_To,
        Densidade, UsarDensid, LITROS_PQ,
        NxLotes, SiglaIQ,
        Ativo], [
        Controle], True);
        //
        if FProgressBar1 <> nil then
        begin
          FProgressBar1.Position := FProgressBar1.Position + 1;
          FProgressBar1.Update;
        end;
        //
        QrFormulasIts.Next;
      end;
      if FProgressBar1 <> nil then
      begin
        FProgressBar1.Position := 0;
        FProgressBar1.Visible := False;
      end;
      Screen.Cursor := crDefault;
      Result := True;
    end;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFrFormulasImpShowNew.QrPEEmitCalcFields(DataSet: TDataSet);
begin
  //Fazer no bd via sql!
  //
  QrPEEmitSEQ.Value := QrPEEmit.RecNo;
  //
  QrPEEmitPESO_PQ.Value := QrPEEmitPorcent.Value / 100 * FPeso;
  //
  if (QrPEEmitProduto.Value < 0) and (FTipoPreco <> 2) then
    QrPEEmitPRECO_PQ.Value := QrPEEmitCusIntrn.Value
  else
  begin
    case FTipoPreco of
      1: // Cadastro manual
        QrPEEmitPRECO_PQ.Value := DmModEmit.PrecoEmReais(
          QrPEEmitMoedaPadrao.Value,
          QrPEEmitCustoPadrao.Value, 0);
      2: // desativado
        QrPEEmitPRECO_PQ.Value := 0;
      else   // 0: Estoque Cliente Interno
        QrPEEmitPRECO_PQ.Value := QrPEEmitCustoMedio.Value;
    end;
  end;
  //
  QrPEEmitCUSTO_PQ.Value := QrPEEmitPESO_PQ.Value *
  QrPEEmitPRECO_PQ.Value;
  //
  if (QrPEEmitpH_Min.Value > 0) and (QrPEEmitpH_Max.Value > 0) then
    QrPEEmitpH_All.Value := FloatToStr(QrPEEmitpH_Min.Value) + '-' +
      FloatToStr(QrPEEmitpH_Max.Value)
  else if (QrPEEmitpH_Min.Value > 0) then
    QrPEEmitpH_All.Value := FloatToStr(QrPEEmitpH_Min.Value)
  else if (QrPEEmitpH_Max.Value > 0) then
    QrPEEmitpH_All.Value := FloatToStr(QrPEEmitpH_Max.Value)
  else
    QrPEEmitpH_All.Value := '';

  //

  if (QrPEEmitBe_Min.Value > 0) and (QrPEEmitBe_Max.Value > 0) then
    QrPEEmitBe_All.Value := FloatToStr(QrPEEmitBe_Min.Value) + '-' +
      FloatToStr(QrPEEmitBe_Max.Value)
  else if (QrPEEmitBe_Min.Value > 0) then
    QrPEEmitBe_All.Value := FloatToStr(QrPEEmitBe_Min.Value)
  else if (QrPEEmitBe_Max.Value > 0) then
    QrPEEmitBe_All.Value := FloatToStr(QrPEEmitBe_Max.Value)
  else
    QrPEEmitBe_All.Value := '';

  //

  if (QrPEEmitUsarDensid.Value = 0) or (QrPEEmitDensidade.Value = 0) then
    QrPEEmitLITROS_PQ.Value := 0
  else
    QrPEEmitLITROS_PQ.Value := QrPEEmitPESO_PQ.Value / QrPEEmitDensidade.Value;

  //
  QrPEEmitNxLotes.Value := '';
end;

procedure TFrFormulasImpShowNew.QrPreusoCalcFields(DataSet: TDataSet);
begin
  if QrPreusoPESOFUTURO.Value < 0 then
     QrPreusoDEFASAGEM.Value := QrPreusoPESOFUTURO.Value * -1
  else
     QrPreusoDEFASAGEM.Value := 0;
end;

function TFrFormulasImpShowNew.EfetuaBaixa(QrLotes: TmySQLQuery): Boolean;
begin
  DmModEmit.EfetuaBaixa(FProgressBar1, FDataP, FTipoPreco, FCliIntCod,
  FEmit,  QrPreuso, QrLotes, QrPreusoMoedaPadrao, QrPreusoCustoPadrao,
  QrPreusoPeso_PQ, QrPreusoCustoMedio, QrPreusoCli_Orig, QrPreusoProduto,
  QrPreUsoCodigo, QrPreusoControle, TTipoSourceMP(FSourcMP), FDtCorrApo,
  FEmpresa);
end;

procedure TFrFormulasImpShowNew.FormCreate(Sender: TObject);
begin
  FEmitGru     := 0;
  FRetrabalho  := 0;
  FEmitGru_TXT := '';
  FVSMovCod    := 0;
  FLogo0Path   := Geral.ReadAppKey('Logo0', Application.Title, ktString, '',
    HKEY_LOCAL_MACHINE);
  FLogo0Exists := FileExists(FLogo0Path);
end;

procedure TFrFormulasImpShowNew.FormHide(Sender: TObject);
begin
end;

{
    // 2014-06-12 - Falta de objetos no dfm!
procedure TFrFormulasImpShow.FrxRec1_Imprime;
begin
  ReopenEmitCus();
  MyObjects.frxImprime(frxQUI_RECEI_999_01_B, 'Receita');
end;

procedure TFrFormulasImpShow.FrxRec1_Mostra;
begin
  ReopenEmitCus();
  MyObjects.frxMostra(frxQUI_RECEI_999_01_B, 'Receita');
end;
    // FIM 2014-06-12
}

procedure TFrFormulasImpShowNew.frxRec2GetValue(const VarName: String;
  var Value: Variant);
var
  CProd, CSolu, Custo, Formato, CusEUnid: String;
  AguaDil: Double;
  Tipo: Integer;
begin
  // 2022-03-03
  if VarName = 'VARF_TipoLinha' then
  begin
    Tipo := 0;
    if (QrFormulasItsTempoR.Value > 0) then
      Tipo := Tipo + 1;
    if (QrFormulasItsTempoP.Value > 0) then
      Tipo := Tipo + 2;
    //
    if (QrFormulasItspH_Min.Value > 0) or (QrFormulasItspH_Min.Value > 0)
    or (QrFormulasItspH_Min.Value > 0) or (QrFormulasItspH_Min.Value > 0)
    or (QrFormulasItspH_Min.Value > 0) or (QrFormulasItspH_Min.Value > 0) then
      Tipo := Tipo + 4;
    //
    if (QrFormulasItsProduto.Value = -10) // Escorrer
    or (QrFormulasItsProduto.Value = -11) // Escorrer bem
    then
      //Tipo := Tipo + 4;
      Tipo := 8;
    //
    Value := Tipo;
  end;
  if VarName = 'VARF_pH_All' then
  begin
    if (QrFormulasItspH_Min.Value > 0) and (QrFormulasItspH_Max.Value > 0) then
      Value := FloatToStr(QrFormulasItspH_Min.Value) + '-' + FloatToStr(QrFormulasItspH_Max.Value)
    else if (QrFormulasItspH_Min.Value > 0) then
      Value := FloatToStr(QrFormulasItspH_Min.Value)
    else if (QrFormulasItspH_Max.Value > 0) then
      Value := FloatToStr(QrFormulasItspH_Max.Value)
    else
      Value := '';
  end;
  if VarName = 'VARF_Be_All' then
  begin
    if (QrFormulasItsBe_Min.Value > 0) and (QrFormulasItsBe_Max.Value > 0) then
      Value := FloatToStr(QrFormulasItsBe_Min.Value) + '-' +
        FloatToStr(QrFormulasItsBe_Max.Value)
    else if (QrFormulasItsBe_Min.Value > 0) then
      Value := FloatToStr(QrFormulasItsBe_Min.Value)
    else if (QrFormulasItsBe_Max.Value > 0) then
      Value := FloatToStr(QrFormulasItsBe_Max.Value)
    else
      Value := '';
  end;
  if VarName = 'VARF_GC_All' then
  begin
    if (QrFormulasItsGC_Min.Value > 0) and (QrFormulasItsGC_Max.Value > 0) then
      Value := FloatToStr(QrFormulasItsGC_Min.Value) + '-' +
        FloatToStr(QrFormulasItsGC_Max.Value)
    else if (QrFormulasItsGC_Min.Value > 0) then
      Value := FloatToStr(QrFormulasItsGC_Min.Value)
    else if (QrFormulasItsGC_Max.Value > 0) then
      Value := FloatToStr(QrFormulasItsGC_Max.Value)
    else
      Value := '';
  end;

  // 2022-03-03
  if FPesoCalc < 1 then
    Formato := FFloat_15_4
  else
    Formato := FFloat_15_2;
  //
  CProd := CO_VAZIO;
  CSolu := CO_VAZIO;
  Custo := CO_VAZIO;
  if VarName = 'VAR_MOSTRA_LINHA' then Value := QrLFormItsLMOSTRA_LINHA.Value
  else if VarName = 'VAR_MOSTRA_GRADE' then Value := QrLFormItsLMOSTRA_GRADE.Value else
  if VarName = 'Espessura' then Value   := FLinhasRebx else // Compatibilidade
  if VarName = 'EspessuraRebx' then Value   := FLinhasRebx else
  if VarName = 'EspessuraSemi' then Value   := FLinhasSemi else
  if VarName = 'Qtde' then Value :=
                FormatFloat('#,###,##0.0',FQtde)+' '+FDefPeca else
  if VarName = 'Peso' then Value := FormatFloat(FFloat_15_3, FPeso)+' kg' else
  if VarName = 'PesoFulao' then Value := FPeso else
  //fazer peso base (salgado -> pr�-descarnado)
  if VarName = 'PesoMedio' then
    Value := FormatFloat(FFloat_15_3,FMedia)+' kg/'+FDefPeca else
  if VarName = 'Area' then
    Value := FormatFloat(FFloat_15_2,FArea);
  if VarName = 'FU' then Value := FFulao else
  if VarName = 'OS' then Value := ''(*VAR_OS*) else
  if VarName = 'SP' then Value := VAR_SP else
  //if VarName = 'Lotes' then Value := Geral.Substitui(FObs, sLineBreak, ' ');
  if VarName = 'Lotes' then
    Value := FObs;
  if FCalcCustos or FCalcPrecos then
  begin
    if QrFormulasItsCUSTO_PQ.Value > 0 then
       CProd := FormatFloat(Formato, QrFormulasItsCUSTO_PQ.Value);
    if QrFormulasItsCSolu.Value > 0 then
       CSolu := '';//FormatFloat(Formato, QrFormulasItsCSoluP.Value);
    if QrFormulasItsCUSTO_PQ.Value > 0 then
       Custo := FormatFloat(Formato, QrFormulasItsCUSTO_PQ.Value);
    if VarName = 'Uso kg/L' then Value := 'Custo item';
    //
    if VarName = 'CustoTotal' then Value := FormatFloat(FFloat_15_4,FCustoTotal);
    if VarName = 'CustoTon' then Value := FormatFloat(FFloat_15_2,FCustokg*1000);
    if VarName = 'Custokg' then Value := FormatFloat(FFloat_15_4,FCustokg);
    if VarName = 'Custom2_WB' then Value := FormatFloat(FFloat_15_4,FCustoM2);
    if VarName = 'Custom2_SA' then Value := FormatFloat(FFloat_15_4,FCustoM2Semi);
    if VarName = 'Custoft2_WB' then Value := FormatFloat(FFloat_15_4,FCustoM2 * CO_M2toFT2);
    if VarName = 'Custoft2_SA' then Value := FormatFloat(FFloat_15_4,FCustoM2Semi * CO_M2toFT2);
    //
    if FBRL_USD = 0 then
    begin
      if VarName = 'VAR_BRL_USD' then Value := '';
      if VarName = 'U_CustoTotal' then Value := '';
      if VarName = 'U_CustoTon' then Value := '';
      if VarName = 'U_Custokg' then Value := '';
      if VarName = 'U_Custom2_WB' then Value := '';
      if VarName = 'U_Custom2_SA' then Value := '';
      if VarName = 'U_Custoft2_WB' then Value := '';
      if VarName = 'U_Custoft2_SA' then Value := '';
    end else begin
      if VarName = 'VAR_BRL_USD' then Value := Geral.FFT(FBRL_USD, 6, siPositivo);
      if VarName = 'U_CustoTotal' then Value := FormatFloat(FFloat_15_4, FCustoTotal/FBRL_USD);
      if VarName = 'U_CustoTon' then Value := FormatFloat(FFloat_15_2, FCustokg*1000/FBRL_USD);
      if VarName = 'U_Custokg' then Value := FormatFloat(FFloat_15_4,(FCustokg/FBRL_USD));
      if VarName = 'U_Custom2_WB' then Value := FormatFloat(FFloat_15_4, FCustoM2/FBRL_USD);
      if VarName = 'U_Custom2_SA' then Value := FormatFloat(FFloat_15_4, FCustoM2Semi/FBRL_USD);
      if VarName = 'U_Custoft2_WB' then Value := FormatFloat(FFloat_15_4, FCustoM2/FBRL_USD * CO_M2toFT2);
      if VarName = 'U_Custoft2_SA' then Value := FormatFloat(FFloat_15_4, FCustoM2Semi/FBRL_USD * CO_M2toFT2);
    end;
    //
    if FBRL_EUR = 0 then
    begin
      if VarName = 'VAR_BRL_EUR' then Value := '';
      if VarName = 'E_CustoTotal' then Value := '';
      if VarName = 'E_CustoTon' then Value := '';
      if VarName = 'E_Custokg' then Value := '';
      if VarName = 'E_Custom2_WB' then Value := '';
      if VarName = 'E_Custom2_SA' then Value := '';
      if VarName = 'E_Custoft2_WB' then Value := '';
      if VarName = 'E_Custoft2_SA' then Value := '';
    end else begin
      if VarName = 'VAR_BRL_EUR' then Value := Geral.FFT(FBRL_EUR, 6, siPositivo);
      if VarName = 'E_CustoTotal' then Value := FormatFloat(FFloat_15_4,FCustoTotal/FBRL_EUR);
      if VarName = 'E_CustoTon' then Value := FormatFloat(FFloat_15_2,FCustokg*1000/FBRL_EUR);
      if VarName = 'E_Custokg' then Value := FormatFloat(FFloat_15_4,(FCustokg/FBRL_EUR));
      if VarName = 'E_Custom2_WB' then Value := FormatFloat(FFloat_15_4,FCustoM2/FBRL_EUR);
      if VarName = 'E_Custom2_SA' then Value := FormatFloat(FFloat_15_4,FCustoM2Semi/FBRL_EUR);
      if VarName = 'E_Custoft2_WB' then Value := FormatFloat(FFloat_15_4,FCustoM2/FBRL_EUR * CO_M2toFT2);
      if VarName = 'E_Custoft2_SA' then Value := FormatFloat(FFloat_15_4,FCustoM2Semi/FBRL_EUR * CO_M2toFT2);
    end;
    //
    if VarName = 'Data_Moedas' then
    begin
      if FDtaCambio < 2 then
        Value := 'N/D'
      else
        Value := Geral.FDT(FDtaCambio, 2);
    end;
    if VarName = 'Custo_total' then Value := 'Custo total:';
    if VarName = 'Custo_ton' then Value := 'Custo ton:';
    if VarName = 'Custo_kg' then Value := 'Custo kg:';
    if VarName = 'Custo_m' then Value := 'Custo m�:';
    if VarName = 'Custo_ft' then Value := 'Custo ft�:';
    if VarName = 'Ini_txt' then Value := 'C.Prod.';
    if VarName = 'Fin_txt' then Value := 'C.Outros';
    if VarName = 'Inicio' then Value := CProd;
    if VarName = 'Final' then Value := CSolu;
    if VarName = 'kgLCusto' then Value := Custo;
    if VarName = 'VARF_L_AGUA' then
    Value := '';

  end else begin
  // ini 2021-10-18
  {
    if QrFormulasItsPESO_PQ.Value > 0 then
Custo := FormatFloat(FFloat_15_3, QrFormulasItsPESO_PQ.Value); //Peso
  } // fim 2021-10-18
    if VarName = 'Uso kg/L' then Value := VarName;
    if VarName = 'CustoTotal' then Value := CO_VAZIO;
    if VarName = 'CustoTon' then Value := CO_VAZIO;
    if VarName = 'Custokg' then Value := CO_VAZIO;
    if VarName = 'Custom2_WB' then Value := CO_VAZIO;
    if VarName = 'Custom2_SA' then Value := CO_VAZIO;
    if VarName = 'Custoft2_WB' then Value := CO_VAZIO;
    if VarName = 'Custoft2_SA' then Value := CO_VAZIO;
    if VarName = 'Custo_total' then Value := CO_VAZIO;
    if VarName = 'Custo_ton' then Value := CO_VAZIO;
    if VarName = 'Custo_kg' then Value := CO_VAZIO;
    if VarName = 'Custo_m' then Value := CO_VAZIO;
    if VarName = 'Custo_ft' then Value := CO_VAZIO;
    if VarName = 'Ini_txt' then Value := 'In�cio';
    if VarName = 'Fin_txt' then Value := 'Final';
    if VarName = 'Inicio' then Value := CO_TempoVAZIO;
    if VarName = 'Final' then Value := CO_TempoVAZIO;
    if VarName = 'kgLCusto' then
    // ini 2021-10-18
    begin
      Custo := FormatFloat(FFloat_15_3, QrFormulasItsPESO_PQ.Value); //Peso
      if (QrFormulasItsUsarDensid.Value = 1)
      then
      begin
        Custo := FormatFloat(FFloat_15_3, QrFormulasItsLITROS_PQ.Value);
        if QrFormulasItsPESO_PQ.Value > 0 then
          Custo := Custo + ' (L)';
      end else
      if BDC_APRESENTACAO_IMPRESSAO in ([15,16]) then
      begin
        if (QrFormulasItsProduto.Value = -2)
        or (QrFormulasItsProduto.Value = -3)
        then
        begin
          if QrFormulasItsPESO_PQ.Value > 0 then
            Custo := Custo + ' L';
        end else
        begin
          if QrFormulasItsPESO_PQ.Value > 0 then
            Custo := Custo + ' kg';
        end;
      end;
      Value := Custo;//Peso
    end;
    // fim 2021-10-18
    if VarName = 'VARF_L_AGUA' then
    begin
      Value := '';
      if QrFormulasItsDiluicao.Value <> 0 then
      begin
        AguaDil := QrFormulasItsDiluicao.Value * QrFormulasItsPESO_PQ.Value;
        if FPeso < 200 then
          Value := Geral.FFT(AguaDil, 3, siNegativo)
        else
          Value := Geral.FF0(Trunc(AguaDil));
      end;
      if QrFormulasItsBoca.Value <> '' then
        Value := Value + 'Boca    '
    end;
    //
    if VarName = 'U_CustoTotal' then Value := '';
    if VarName = 'U_CustoTon' then Value := '';
    if VarName = 'U_Custokg' then Value := '';
    if VarName = 'U_Custom2_WB' then Value := '';
    if VarName = 'U_Custom2_SA' then Value := '';
    if VarName = 'U_Custoft2_WB' then Value := '';
    if VarName = 'U_Custoft2_SA' then Value := '';
    //
    if VarName = 'E_CustoTotal' then Value := '';
    if VarName = 'E_CustoTon' then Value := '';
    if VarName = 'E_Custokg' then Value := '';
    if VarName = 'E_Custom2_WB' then Value := '';
    if VarName = 'E_Custom2_SA' then Value := '';
    if VarName = 'E_Custoft2_WB' then Value := '';
    if VarName = 'E_Custoft2_SA' then Value := '';
    //
    if VarName = 'Data_Moedas' then Value := '';
    if VarName = 'VAR_BRL_USD' then Value := '';
    if VarName = 'VAR_BRL_EUR' then Value := '';
  end;
  if VarName = 'VAR_TEMPORECEITA' then Value :=
  'Tempo parado: '+FHHMM_P+' + Tempo rodando: '+FHHMM_R+' = Tempo total: '+FHHMM_T
  else if AnsiCompareText(VarName, 'VAR_OBSERPROCES') = 0 then
    Value := Trim(QrLFormItsLObserProce.Value)
  else if AnsiCompareText(VarName, 'VAR_PESAGEM') = 0 then
  begin
    if FEmit = 0 then Value := 'SEM BAIXA'
    else Value := 'Pesagem N� '+IntToStr(FEmit);
  end
  else if AnsiCompareText(VarName, 'OBS') = 0 then
    Value := FObs;
  if AnsiCompareText(VarName, 'VAR_TXT') = 0 then
  begin
    if QrLFormItsLInsumo.Value = -5 then
      Value := '-5'
    else
      Value := QrLFormItsLTEXT_TXT.Value;
  end;
  if AnsiCompareText(VarName, 'VAR_TEMPO') = 0 then
  begin
    if QrLFormItsLTEMPO_TXT.Value = '' then Value := False else Value := True;
  end
  else if AnsiCompareText(VarName, 'VAR_TEMOBSERPROCES') = 0 then
  begin
    if Trim(QrLFormItsLObserProce.Value) = '' then Value := False else Value := True;
  end
  else if AnsiCompareText(VarName, 'VARF_NEGATIVO') = 0 then
  begin
    if QrPreusoPESOFUTURO.Value < 0 then Value := True else Value := False;
  end
  else if AnsiCompareText(VarName, 'VARF_CLIINT_TXT') = 0 then
    Value := FCliIntTxt
  else
  if VarName = 'LogoReceExiste' then
    Value := FileExists(Dmod.QrControle.FieldByName('PathLogoRece').AsString)
  else
  if VarName = 'LogoRecePath' then
    Value := Dmod.QrControle.FieldByName('PathLogoRece').AsString
  else
  if VarName = 'VAR_EMITGRU' then
  begin
    if FEmitGru <> 0 then
      Value := 'Grupo: ' + FEmitGru_TXT
    else
      Value := ''//'Sem grupo definido.'
  end else
  if VarName = 'VAR_RETRABALHO' then
  begin
    if FRetrabalho = 0 then
      Value := ''//'N�O'
    else
      Value := 'SIM';
  end else
  if VarName = 'VAR_TIT_RETRAB' then
  begin
    if FRetrabalho = 0 then
      Value := ''//'N�O'
    else
      Value := 'Retrabalho:';
  end else
  if VarName = 'VARF_TEMPO_R' then
    Value := dmkPF.IntToTempo(QrFormulasTempoR.Value)
  else
  if VarName = 'VARF_TEMPO_P' then
    Value := dmkPF.IntToTempo(QrFormulasTempoP.Value)
  else
  if VarName = 'VARF_TEMPO_T' then
    Value := dmkPF.IntToTempo(QrFormulasTempoT.Value)
  else
  if VarName = 'VARF_DATAEMIS_HORAINI' then
    Value := Geral.FDT(Trunc(QrEmitItsDataEmis.Value) + QrEmitItsHoraIni.Value, 107)
    //Value := Geral.FDT(Trunc(QrFormulasItsDataEmis.Value) + QrFormulasItsHoraIni.Value, 107)
  else
  //?
end;

{
    // 2014-06-12 - Falta de objetos no dfm!
procedure TFrFormulasImpShow.QrEmitCusAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBMovIts, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'WHERE wmi.Codigo=' + Geral.FF0(QrEmitCusMPVIts.Value),
  'AND wmi.LnkNivXtr1=' + Geral.FF0(QrEmitCusCodigo.Value),
  'AND wmi.LnkNivXtr2=' + Geral.FF0(QrEmitCusControle.Value),
  //'ORDER BY NO_Pallet, wmi.Controle ',
  'ORDER BY wmi.Controle ',
  '']);
end;
    // FIM 2014-06-12
}

procedure TFrFormulasImpShowNew.QrEmitItsCalcFields(DataSet: TDataSet);
begin
  if (QrEmitItsProduto.Value < 0) and (FTipoPreco <> 2) then
    QrEmitItsPRECO_PQ.Value := QrEmitItsCusIntrn.Value
  else
  begin
    case FTipoPreco of
      1: // Cadastro manual
        QrEmitItsPRECO_PQ.Value := DmModEmit.PrecoEmReais(
          QrEmitItsMoedaPadrao.Value,
          QrEmitItsCustoPadrao.Value, 0);
      2: // desativado
        QrEmitItsPRECO_PQ.Value := 0;
      else   // 0: Estoque Cliente Interno
        QrEmitItsPRECO_PQ.Value := QrEmitItsCustoMedio.Value;
    end;
  end;
  //
  //
  QrEmitItsCUSTO_PQ.Value := QrEmitItsPESO_PQ.Value *
  QrEmitItsPRECO_PQ.Value;
  //
end;

procedure TFrFormulasImpShowNew.QrFormulasCalcFields(DataSet: TDataSet);
begin
  QrFormulasNumCODIF.Value := dmkPF.IntToCODIF_Receita(QrFormulasNumero.Value);
end;

procedure TFrFormulasImpShowNew.QrFormulasItsBeforeClose(DataSet: TDataSet);
begin
  QrNxL.Close;
end;

procedure TFrFormulasImpShowNew.QrFormulasItsBeforeOpen(DataSet: TDataSet);
var
  Emit: Integer;
begin
  if FEmit > 0 then
    Emit := FEmit
  else
    Emit := -99999999;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNxL, Dmod.MyDB, [
  'SELECT DISTINCT Produto, NxLotes ',
  'FROM emitits ',
  'WHERE Codigo=' + Geral.FF0(FEmit),
  'AND NXLotes <> "" ',
  ' ']);
end;

procedure TFrFormulasImpShowNew.QrFormulasItsCalcFields(DataSet: TDataSet);
var
  GC_All: String;
begin
  if QrFormulasItsProcesso.Value = EmptyStr then
  begin
    if QrFormulasIts.RecNo = 1 then
      QrFormulasItsNO_ETAPA.Value := 'ETAPA INICIAL'
    else
      QrFormulasItsNO_ETAPA.Value := FNO_ETAPA;
  end else
    QrFormulasItsNO_ETAPA.Value := QrFormulasItsProcesso.Value;
  //
  FNO_ETAPA := QrFormulasItsNO_ETAPA.Value;
  //
  GC_All := '';
  if (QrFormulasItsGC_Min.Value > 0) and (QrFormulasItsGC_Max.Value > 0) then
    GC_All := Geral.FFT(QrFormulasItsGC_Min.Value, 0, siNegativo) + '-' +
      Geral.FFT(QrFormulasItsGC_Max.Value, 0, siNegativo)
  else
  if (QrFormulasItsGC_Min.Value > 0) then
    GC_All := Geral.FFT(QrFormulasItsGC_Min.Value, 0, siNegativo)
  else
  if (QrFormulasItsGC_Max.Value > 0) then
    GC_All := Geral.FFT(QrFormulasItsGC_Max.Value, 0, siNegativo);
  //
  QrFormulasItsGC_All_2.Value := GC_All;
end;

end.
