object FmMPInAdd: TFmMPInAdd
  Left = 297
  Top = 226
  Caption = 'COU-MP_IN-003 :: Entrada de Couros'
  ClientHeight = 463
  ClientWidth = 969
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel8: TPanel
    Left = 0
    Top = 48
    Width = 969
    Height = 261
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 484
      Height = 261
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 74
        Height = 13
        Caption = 'Dono do couro:'
      end
      object Label3: TLabel
        Left = 8
        Top = 44
        Width = 63
        Height = 13
        Caption = 'Proced'#234'ncia:'
      end
      object Label15: TLabel
        Left = 8
        Top = 84
        Width = 54
        Height = 13
        Caption = 'Transporte:'
      end
      object SpeedButton1: TSpeedButton
        Left = 456
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object SpeedButton3: TSpeedButton
        Left = 456
        Top = 100
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton3Click
      end
      object Label8: TLabel
        Left = 8
        Top = 172
        Width = 59
        Height = 13
        Caption = '% Comiss'#227'o:'
      end
      object Label6: TLabel
        Left = 8
        Top = 128
        Width = 40
        Height = 13
        Caption = 'Corretor:'
      end
      object Label16: TLabel
        Left = 76
        Top = 172
        Width = 171
        Height = 13
        Caption = 'Condi'#231#245'es de pagamento comiss'#227'o:'
      end
      object Label2: TLabel
        Left = 8
        Top = 212
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object Label12: TLabel
        Left = 108
        Top = 212
        Width = 63
        Height = 13
        Caption = 'Ficha RAMP:'
      end
      object Label13: TLabel
        Left = 176
        Top = 212
        Width = 24
        Height = 13
        Caption = 'Lote:'
      end
      object Label14: TLabel
        Left = 244
        Top = 212
        Width = 33
        Height = 13
        Caption = 'Marca:'
      end
      object Label9: TLabel
        Left = 393
        Top = 212
        Width = 80
        Height = 13
        Caption = 'Desc. adiantam.:'
      end
      object SpeedButton4: TSpeedButton
        Left = 456
        Top = 144
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton4Click
      end
      object CBClienteI: TdmkDBLookupComboBox
        Left = 57
        Top = 20
        Width = 398
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsDonos
        TabOrder = 1
        dmkEditCB = EdClienteI
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdClienteI: TdmkEditCB
        Left = 8
        Top = 20
        Width = 48
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdClienteIRedefinido
        DBLookupComboBox = CBClienteI
        IgnoraDBLookupComboBox = False
      end
      object EdProcedencia: TdmkEditCB
        Left = 8
        Top = 60
        Width = 48
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdProcedenciaChange
        DBLookupComboBox = CBProcedencia
        IgnoraDBLookupComboBox = False
      end
      object CBProcedencia: TdmkDBLookupComboBox
        Left = 57
        Top = 60
        Width = 398
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsProcedencias
        TabOrder = 3
        dmkEditCB = EdProcedencia
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGAbateTipo: TRadioGroup
        Left = 252
        Top = 172
        Width = 225
        Height = 39
        Caption = ' Tipo de abate: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Matadouro'
          'Frigor'#237'fico')
        TabOrder = 11
      end
      object EdTransporte: TdmkEditCB
        Left = 8
        Top = 100
        Width = 48
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransporte
        IgnoraDBLookupComboBox = False
      end
      object CBTransporte: TdmkDBLookupComboBox
        Left = 57
        Top = 100
        Width = 398
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsTransporte
        TabOrder = 6
        dmkEditCB = EdTransporte
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object dmkEdComisPer: TdmkEdit
        Left = 8
        Top = 188
        Width = 61
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CBCorretor: TdmkDBLookupComboBox
        Left = 57
        Top = 144
        Width = 398
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTI'
        ListSource = DsCorretores
        TabOrder = 8
        dmkEditCB = EdCorretor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCorretor: TdmkEditCB
        Left = 8
        Top = 144
        Width = 48
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCorretor
        IgnoraDBLookupComboBox = False
      end
      object EdCondComis: TEdit
        Left = 76
        Top = 188
        Width = 169
        Height = 21
        TabOrder = 10
      end
      object TPData: TDateTimePicker
        Left = 8
        Top = 228
        Width = 97
        Height = 21
        Date = 38547.003402592600000000
        Time = 38547.003402592600000000
        TabOrder = 12
        OnClick = TPDataClick
        OnChange = TPDataChange
      end
      object EdFicha: TdmkEdit
        Left = 108
        Top = 228
        Width = 64
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 13
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdFichaExit
      end
      object EdLote: TdmkEdit
        Left = 176
        Top = 228
        Width = 64
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 11
        TabOrder = 14
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdMarca: TdmkEdit
        Left = 244
        Top = 228
        Width = 145
        Height = 21
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 15
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object dmkEdDescAdiant: TdmkEdit
        Left = 392
        Top = 228
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object BitBtn1: TBitBtn
        Left = 456
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        TabOrder = 4
        TabStop = False
        OnClick = BitBtn1Click
      end
    end
    object Panel4: TPanel
      Left = 484
      Top = 0
      Width = 485
      Height = 261
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel6: TPanel
        Left = 0
        Top = 208
        Width = 485
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 485
          Height = 44
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label7: TLabel
            Left = 4
            Top = 4
            Width = 66
            Height = 13
            Caption = 'Rec. PDA kg:'
          end
          object Label4: TLabel
            Left = 80
            Top = 4
            Width = 65
            Height = 13
            Caption = 'Rec. PTA kg:'
          end
          object Label5: TLabel
            Left = 156
            Top = 4
            Width = 73
            Height = 13
            Caption = 'Raspa PTA kg:'
          end
          object Label18: TLabel
            Left = 236
            Top = 4
            Width = 70
            Height = 13
            Caption = 'Couro PTA kg:'
          end
          object Label19: TLabel
            Left = 312
            Top = 4
            Width = 60
            Height = 13
            Caption = 'Wet blue m'#178':'
          end
          object Label20: TLabel
            Left = 392
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Wet blue ft'#178':'
          end
          object dmkEdRecorte_PDA: TdmkEdit
            Left = 4
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdRecorte_PTA: TdmkEdit
            Left = 80
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdRaspa_PTA: TdmkEdit
            Left = 156
            Top = 20
            Width = 76
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdPTA: TdmkEdit
            Left = 236
            Top = 19
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdFimM2: TdmkEdit
            Left = 312
            Top = 19
            Width = 76
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = dmkEdFimM2Exit
          end
          object dmkEdFimP2: TdmkEdit
            Left = 392
            Top = 19
            Width = 76
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = dmkEdFimP2Exit
          end
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 485
        Height = 208
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label11: TLabel
          Left = 4
          Top = 164
          Width = 83
          Height = 13
          Caption = 'Local de entrega:'
        end
        object Label10: TLabel
          Left = 248
          Top = 164
          Width = 189
          Height = 13
          Caption = 'Condi'#231#245'es de pagamento mat'#233'ria-prima:'
        end
        object RGTipificacao: TRadioGroup
          Left = 4
          Top = 4
          Width = 469
          Height = 77
          Caption = ' Tipifica'#231#227'o: '
          Items.Strings = (
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9'
            '10'
            '11'
            '12')
          TabOrder = 0
          OnClick = RGTipificacaoClick
        end
        object RGAparasCabelo: TRadioGroup
          Left = 4
          Top = 84
          Width = 221
          Height = 39
          Caption = ' Aparas com cabelo: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Gera'
            'N'#227'o gera'
            'J'#225' gerou')
          TabOrder = 1
        end
        object RGAnimal: TRadioGroup
          Left = 228
          Top = 84
          Width = 245
          Height = 79
          Caption = ' Tipo de animal: '
          Items.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
          TabOrder = 3
          OnClick = RGAnimalClick
        end
        object RGSeboPreDescarne: TRadioGroup
          Left = 4
          Top = 124
          Width = 221
          Height = 39
          Caption = ' Carna'#231'a no pr'#233'-descarne:'
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Gera'
            'N'#227'o gera'
            'J'#225' gerou')
          TabOrder = 2
        end
        object EdLocalEntrg: TEdit
          Left = 4
          Top = 180
          Width = 241
          Height = 21
          TabOrder = 4
        end
        object EdCondPagto: TEdit
          Left = 248
          Top = 180
          Width = 225
          Height = 21
          TabOrder = 5
        end
      end
    end
  end
  object Panel9: TPanel
    Left = 0
    Top = 309
    Width = 969
    Height = 46
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clActiveCaption
    Font.Height = -27
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object Panel10: TPanel
      Left = 0
      Top = 0
      Width = 969
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel10'
      TabOrder = 0
      object Panel11: TPanel
        Left = 0
        Top = 0
        Width = 76
        Height = 41
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label17: TLabel
          Left = 4
          Top = 4
          Width = 69
          Height = 14
          Caption = 'Observa'#231#245'es:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
      end
      object MeObserv: TMemo
        Left = 76
        Top = 0
        Width = 893
        Height = 41
        TabStop = False
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 969
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 921
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 873
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 228
        Height = 32
        Caption = 'Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 228
        Height = 32
        Caption = 'Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 228
        Height = 32
        Caption = 'Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 355
    Width = 969
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel12: TPanel
      Left = 2
      Top = 15
      Width = 965
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 399
    Width = 969
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel13: TPanel
      Left = 2
      Top = 15
      Width = 965
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label21: TLabel
        Left = 284
        Top = 4
        Width = 126
        Height = 13
        Caption = 'Mat'#233'ria-prima selecionada:'
      end
      object PnSaiDesis: TPanel
        Left = 821
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste: TBitBtn
          Tag = 13
          Left = 3
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 28
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
      object BtUsarEste: TBitBtn
        Tag = 11
        Left = 151
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Usar este'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Visible = False
        OnClick = BtUsarEsteClick
      end
      object EdGraGruX: TdmkEdit
        Left = 284
        Top = 20
        Width = 48
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdGraGruXChange
      end
      object EdNO_GraGru1: TdmkEdit
        Left = 332
        Top = 20
        Width = 469
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object QrDonos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 224
    Top = 58
    object QrDonosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDonosNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsDonos: TDataSource
    DataSet = QrDonos
    Left = 252
    Top = 58
  end
  object DsProcedencias: TDataSource
    DataSet = QrProcedencias
    Left = 252
    Top = 98
  end
  object QrProcedencias: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emp.Corretor, emp.Comissao, emp.DescAdiant,'
      'emp.CondPagto, emp.CondComis, emp.LocalEntrg,'
      'emp.PreDescarn, emp.MaskLetras, emp.MaskFormat, '
      'emp.Abate, emp.Transporte, '
      'emp.LoteLetras, emp.LoteFormat,  ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'LEFT JOIN entimp emp ON emp.Codigo=ent.Codigo'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 224
    Top = 98
    object QrProcedenciasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProcedenciasNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrProcedenciasPreDescarn: TSmallintField
      FieldName = 'PreDescarn'
    end
    object QrProcedenciasMaskLetras: TWideStringField
      FieldName = 'MaskLetras'
      Size = 10
    end
    object QrProcedenciasMaskFormat: TWideStringField
      FieldName = 'MaskFormat'
      Size = 10
    end
    object QrProcedenciasCorretor: TIntegerField
      FieldName = 'Corretor'
    end
    object QrProcedenciasComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrProcedenciasDescAdiant: TFloatField
      FieldName = 'DescAdiant'
    end
    object QrProcedenciasCondPagto: TWideStringField
      FieldName = 'CondPagto'
      Size = 30
    end
    object QrProcedenciasCondComis: TWideStringField
      FieldName = 'CondComis'
      Size = 30
    end
    object QrProcedenciasLocalEntrg: TWideStringField
      FieldName = 'LocalEntrg'
      Size = 50
    end
    object QrProcedenciasAbate: TIntegerField
      FieldName = 'Abate'
    end
    object QrProcedenciasTransporte: TIntegerField
      FieldName = 'Transporte'
    end
    object QrProcedenciasLoteLetras: TWideStringField
      FieldName = 'LoteLetras'
      Size = 10
    end
    object QrProcedenciasLoteFormat: TWideStringField
      FieldName = 'LoteFormat'
      Size = 10
    end
  end
  object QrTransporte: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 224
    Top = 138
    object QrTransporteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransporteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransporte: TDataSource
    DataSet = QrTransporte
    Left = 252
    Top = 138
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Controle '
      'FROM mpin'
      'WHERE Marca=:P0'
      'AND Controle<>:P1')
    Left = 272
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrCorretores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTI'
      'FROM entidades '
      'ORDER BY Nome')
    Left = 224
    Top = 176
    object QrCorretoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCorretoresNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Required = True
      Size = 100
    end
  end
  object DsCorretores: TDataSource
    DataSet = QrCorretores
    Left = 252
    Top = 176
  end
  object PMFornece: TPopupMenu
    Left = 501
    Top = 100
    object Entidades1: TMenuItem
      Caption = '&Entidades'
      OnClick = Entidades1Click
    end
    object FornecedoresdeMP1: TMenuItem
      Caption = '&Fornecedores de MP'
      OnClick = FornecedoresdeMP1Click
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nome'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggx.Controle=:P0')
    Left = 392
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruXNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
end
