object FmMPPProgEntreg: TFmMPPProgEntreg
  Left = 339
  Top = 185
  Caption = 'VEN-COURO-104 :: Programa'#231#227'o de Entregas'
  ClientHeight = 629
  ClientWidth = 644
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 644
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 596
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 548
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 316
        Height = 32
        Caption = 'Programa'#231#227'o de Entregas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 316
        Height = 32
        Caption = 'Programa'#231#227'o de Entregas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 316
        Height = 32
        Caption = 'Programa'#231#227'o de Entregas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 644
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 644
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 644
        Height = 467
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 640
          Height = 30
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label2: TLabel
            Left = 8
            Top = 8
            Width = 108
            Height = 13
            Caption = 'Setor de acabamento: '
            Enabled = False
          end
          object Label1: TLabel
            Left = 448
            Top = 8
            Width = 36
            Height = 13
            Caption = 'm2/dia:'
          end
          object SpeedButton1: TSpeedButton
            Left = 596
            Top = 3
            Width = 23
            Height = 23
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object EdSetor: TdmkEditCB
            Left = 128
            Top = 4
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdSetorRedefinido
            DBLookupComboBox = CBSetor
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBSetor: TdmkDBLookupComboBox
            Left = 184
            Top = 4
            Width = 253
            Height = 21
            Enabled = False
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsListaListaSetores
            TabOrder = 1
            dmkEditCB = EdSetor
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdM2DiaAcabPCP: TdmkEdit
            Left = 492
            Top = 4
            Width = 101
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdM2DiaAcabPCPRedefinido
          end
        end
        object DBGrid1: TdmkDBGridZTO
          Left = 2
          Top = 45
          Width = 640
          Height = 420
          Align = alClient
          DataSource = DsMPVIts
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDrawColumnCell = DBGrid1DrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Entrega'
              Title.Caption = 'Dt. Entrega'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'M2Pedido'
              Title.Caption = 'm'#178' Pedido'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Percentual'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DrawPB_Percentual'
              Title.Caption = 'Percentual '
              Width = 400
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 644
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 640
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 644
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 498
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 496
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrListaSetores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, m2Dia  '
      'FROM listasetores  '
      'ORDER BY TpReceita DESC, m2Dia DESC ')
    Left = 152
    Top = 256
    object QrListaSetoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrListaSetoresNome: TWideStringField
      FieldName = 'Nome'
    end
    object QrListaSetoresm2Dia: TFloatField
      FieldName = 'm2Dia'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsListaListaSetores: TDataSource
    DataSet = QrListaSetores
    Left = 152
    Top = 300
  end
  object QrMPVIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Entrega, SUM(M2Pedido) M2Pedido,'
      'IF(SUM(M2Pedido) > 1500, 100, '
      '  SUM(M2Pedido) / 1500 * 100) PercNormal,'
      'IF(SUM(M2Pedido) <= 1500, 0, '
      '  IF(SUM(M2Pedido) > (1500 * 2), 100, '
      '  (SUM(M2Pedido) / 1500 * 100) - 100)) PercExtra'
      'FROM mpvits'
      'WHERE Entrega >= "2020-01-31"'
      'GROUP BY Entrega'
      'ORDER BY Entrega ')
    Left = 356
    Top = 364
    object QrMPVItsEntrega: TDateField
      FieldName = 'Entrega'
      Required = True
    end
    object QrMPVItsM2Pedido: TFloatField
      FieldName = 'M2Pedido'
      DisplayFormat = '###,###,##0.00'
    end
    object QrMPVItsPercentual: TFloatField
      FieldName = 'Percentual'
      DisplayFormat = '###,###,##0.00'
    end
  end
  object DsMPVIts: TDataSource
    DataSet = QrMPVIts
    Left = 356
    Top = 412
  end
end
