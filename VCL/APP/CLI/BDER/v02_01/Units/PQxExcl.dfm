object FmPQxExcl: TFmPQxExcl
  Left = 234
  Top = 172
  Caption = 'QUI-RECEI-008 :: Gerenciamento de Pesagem'
  ClientHeight = 743
  ClientWidth = 1074
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 96
    Width = 1074
    Height = 101
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label3: TLabel
      Left = 4
      Top = 4
      Width = 47
      Height = 13
      Caption = 'Pesagem:'
    end
    object Label5: TLabel
      Left = 64
      Top = 4
      Width = 42
      Height = 13
      Caption = 'Emiss'#227'o:'
      FocusControl = DBEdit1
    end
    object Label6: TLabel
      Left = 388
      Top = 4
      Width = 40
      Height = 13
      Caption = 'Receita:'
      FocusControl = DBEdit2
    end
    object Label7: TLabel
      Left = 720
      Top = 4
      Width = 35
      Height = 13
      Caption = 'Cliente:'
      FocusControl = DBEdit4
    end
    object Label8: TLabel
      Left = 208
      Top = 4
      Width = 27
      Height = 13
      Caption = 'Peso:'
      FocusControl = DBEdit5
    end
    object Label9: TLabel
      Left = 276
      Top = 4
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
      FocusControl = DBEdit6
    end
    object Label15: TLabel
      Left = 344
      Top = 4
      Width = 29
      Height = 13
      Caption = 'Ful'#227'o:'
      FocusControl = DBEdit10
    end
    object Label14: TLabel
      Left = 388
      Top = 42
      Width = 122
      Height = 13
      Caption = 'Cor padr'#227'o (recurtimento):'
    end
    object Label16: TLabel
      Left = 149
      Top = 43
      Width = 42
      Height = 13
      Caption = 'Rebaixe:'
    end
    object Label17: TLabel
      Left = 268
      Top = 43
      Width = 52
      Height = 13
      Caption = 'Espessura:'
    end
    object Label22: TLabel
      Left = 4
      Top = 43
      Width = 28
      Height = 13
      Caption = 'Setor:'
    end
    object Label18: TLabel
      Left = 720
      Top = 44
      Width = 37
      Height = 13
      Caption = 'EmitGru'
      FocusControl = DBEdit20
    end
    object Label19: TLabel
      Left = 136
      Top = 4
      Width = 29
      Height = 13
      Caption = 'Baixa:'
      FocusControl = DBEdit22
    end
    object EdPesagem: TdmkEdit
      Left = 4
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = EdPesagemExit
      OnKeyDown = EdPesagemKeyDown
    end
    object DBEdit1: TDBEdit
      Left = 64
      Top = 20
      Width = 68
      Height = 21
      DataField = 'DataEmis'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 1
    end
    object DBEdit2: TDBEdit
      Left = 388
      Top = 20
      Width = 40
      Height = 21
      DataField = 'NumCODIF'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 2
    end
    object DBEdit3: TDBEdit
      Left = 428
      Top = 20
      Width = 245
      Height = 21
      DataField = 'NOME'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 3
    end
    object DBEdit4: TDBEdit
      Left = 720
      Top = 20
      Width = 289
      Height = 21
      DataField = 'NOMECI'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 4
    end
    object DBEdit5: TDBEdit
      Left = 208
      Top = 20
      Width = 64
      Height = 21
      DataField = 'Peso'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 5
    end
    object DBEdit6: TDBEdit
      Left = 276
      Top = 20
      Width = 64
      Height = 21
      DataField = 'Qtde'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 6
    end
    object ProgressBar1: TProgressBar
      Left = 0
      Top = 84
      Width = 1074
      Height = 17
      Align = alBottom
      TabOrder = 7
      Visible = False
    end
    object DBEdit10: TDBEdit
      Left = 344
      Top = 20
      Width = 41
      Height = 21
      DataField = 'Fulao'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 8
    end
    object DBEdit11: TDBEdit
      Left = 388
      Top = 58
      Width = 40
      Height = 21
      DataField = 'GraCorCad'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 9
    end
    object DBEdit12: TDBEdit
      Left = 428
      Top = 58
      Width = 289
      Height = 21
      DataField = 'NoGraCorCad'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 10
    end
    object DBEdit13: TDBEdit
      Left = 36
      Top = 58
      Width = 108
      Height = 21
      DataField = 'NOMESETOR'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 11
    end
    object DBEdit14: TDBEdit
      Left = 4
      Top = 58
      Width = 32
      Height = 21
      DataField = 'Setor'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 12
    end
    object DBEdit15: TDBEdit
      Left = 148
      Top = 58
      Width = 32
      Height = 21
      DataField = 'SemiCodEspReb'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 13
    end
    object DBEdit16: TDBEdit
      Left = 180
      Top = 58
      Width = 84
      Height = 21
      DataField = 'RebLin'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 14
    end
    object DBEdit17: TDBEdit
      Left = 300
      Top = 58
      Width = 84
      Height = 21
      DataField = 'Espessura'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 15
    end
    object DBEdit18: TDBEdit
      Left = 268
      Top = 58
      Width = 32
      Height = 21
      DataField = 'Cod_Espess'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 16
    end
    object DBEdit19: TDBEdit
      Left = 676
      Top = 20
      Width = 40
      Height = 21
      DataField = 'Numero'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 17
    end
    object DBEdit20: TDBEdit
      Left = 720
      Top = 58
      Width = 54
      Height = 21
      DataField = 'EmitGru'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 18
    end
    object DBEdit21: TDBEdit
      Left = 776
      Top = 58
      Width = 289
      Height = 21
      DataField = 'NO_EmitGru'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 19
    end
    object DBEdit22: TDBEdit
      Left = 136
      Top = 20
      Width = 68
      Height = 21
      DataField = 'DtaBaixa'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 20
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1074
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1026
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 978
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 344
        Height = 32
        Caption = 'Gerenciamento de Pesagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 344
        Height = 32
        Caption = 'Gerenciamento de Pesagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 344
        Height = 32
        Caption = 'Gerenciamento de Pesagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 679
    Width = 1074
    Height = 64
    Align = alBottom
    TabOrder = 2
    Visible = False
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1070
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 942
        Top = 0
        Width = 128
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste2: TBitBtn
          Tag = 15
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesiste2Click
        end
      end
      object BtConfirma2: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirma2Click
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1074
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1070
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBControle: TGroupBox
    Left = 0
    Top = 615
    Width = 1074
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1070
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel7: TPanel
        Left = 942
        Top = 0
        Width = 128
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtCancela: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 116
          Height = 40
          Caption = '&Sair'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtCancelaClick
        end
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 292
        Top = 4
        Width = 116
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtImprimeClick
      end
      object BtPesquisa: TBitBtn
        Tag = 40
        Left = 408
        Top = 4
        Width = 116
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtPesquisaClick
      end
      object BitBtn1: TBitBtn
        Left = 524
        Top = 4
        Width = 116
        Height = 40
        Caption = '&Recalcula'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BitBtn1Click
      end
      object BtAdiciona: TBitBtn
        Tag = 10
        Left = 640
        Top = 4
        Width = 116
        Height = 40
        Caption = '&Adiciona'
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtAdicionaClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 756
        Top = 4
        Width = 116
        Height = 40
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtExcluiClick
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 7
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 176
        Top = 4
        Width = 116
        Height = 40
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAlteraClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 197
    Width = 1074
    Height = 418
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 5
    object DBGrid1: TDBGrid
      Left = 0
      Top = 0
      Width = 721
      Height = 326
      Align = alLeft
      DataSource = DsPQx
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Insumo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPQ'
          Title.Caption = 'Insumo'
          Width = 168
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cedente do insumo'
          Width = 247
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CUSTOMEDIO'
          Title.Caption = 'Custo kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OrigemCtrl'
          Title.Caption = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Tipo'
          Title.Caption = 'ID Tipo'
          Visible = True
        end>
    end
    object PnInsumos: TPanel
      Left = 0
      Top = 326
      Width = 1074
      Height = 92
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 37
        Height = 13
        Caption = 'Insumo:'
      end
      object Label2: TLabel
        Left = 384
        Top = 4
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label4: TLabel
        Left = 324
        Top = 44
        Width = 42
        Height = 13
        Caption = 'Peso kg:'
      end
      object Label10: TLabel
        Left = 4
        Top = 44
        Width = 57
        Height = 13
        Caption = 'Estoque kg:'
        FocusControl = DBEdit7
      end
      object Label11: TLabel
        Left = 112
        Top = 44
        Width = 51
        Height = 13
        Caption = 'Estoque $:'
        FocusControl = DBEdit8
      end
      object Label12: TLabel
        Left = 220
        Top = 44
        Width = 56
        Height = 13
        Caption = 'Custo $/kg:'
        FocusControl = DBEdit9
      end
      object Label13: TLabel
        Left = 428
        Top = 44
        Width = 75
        Height = 13
        Caption = 'Saldo futuro kg:'
      end
      object Label21: TLabel
        Left = 760
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdPQ: TdmkEditCB
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPQChange
        DBLookupComboBox = CBPQ
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPQ: TdmkDBLookupComboBox
        Left = 60
        Top = 20
        Width = 321
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPQ
        TabOrder = 1
        dmkEditCB = EdPQ
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCI: TdmkEditCB
        Left = 384
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCIChange
        DBLookupComboBox = CBCI
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCI: TdmkDBLookupComboBox
        Left = 440
        Top = 20
        Width = 317
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECI'
        ListSource = DsCI
        TabOrder = 3
        dmkEditCB = EdCI
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPesoAdd: TdmkEdit
        Left = 324
        Top = 60
        Width = 100
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdPesoAddExit
      end
      object DBEdit7: TDBEdit
        Left = 4
        Top = 60
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'Peso'
        DataSource = DsSaldo
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 112
        Top = 60
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'Valor'
        DataSource = DsSaldo
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 220
        Top = 60
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'CUSTO'
        DataSource = DsSaldo
        TabOrder = 9
      end
      object EdSaldoFut: TdmkEdit
        Left = 428
        Top = 60
        Width = 100
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CkDtCorrApo: TCheckBox
        Left = 532
        Top = 64
        Width = 185
        Height = 17
        Caption = #201' corre'#231#227'o de apontamento. Data:'
        TabOrder = 11
        OnClick = CkDtCorrApoClick
      end
      object TPDtCorrApo: TdmkEditDateTimePicker
        Left = 720
        Top = 60
        Width = 129
        Height = 21
        Date = 44769.000000000000000000
        Time = 0.833253726850671200
        Enabled = False
        TabOrder = 12
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN_MAX
      end
      object EdEmpresa: TdmkEditCB
        Left = 760
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 816
        Top = 20
        Width = 193
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 5
        TabStop = False
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object PmManyGrids: TPanel
      Left = 721
      Top = 0
      Width = 353
      Height = 326
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object Splitter1: TSplitter
        Left = 0
        Top = 261
        Width = 353
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitTop = 0
        ExplicitWidth = 112
      end
      object DBGEmitCusBH: TDBGrid
        Left = 180
        Top = -4
        Width = 295
        Height = 41
        DataSource = DsEmitCusBH
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Visible = False
        Columns = <
          item
            Expanded = False
            FieldName = 'MPIn'
            Title.Caption = 'ID Lote'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Marca'
            Width = 79
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Custo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataMPIn'
            Title.Caption = 'Data MP'
            Visible = True
          end>
      end
      object DBGWBMovIts: TDBGrid
        Left = 0
        Top = 264
        Width = 353
        Height = 62
        Align = alBottom
        DataSource = DsWBMovIts
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Visible = False
        Columns = <
          item
            Expanded = False
            FieldName = 'Pallet'
            Title.Caption = 'ID Pallet'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PALLET'
            Title.Caption = 'Pallet'
            Width = 95
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaP2'
            Title.Caption = #193'rea ft'#178
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso kg'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorT'
            Title.Caption = 'Valor total'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID item'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtPeca'
            Title.Caption = 'Sdo virtual p'#231
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Observ'
            Title.Caption = 'Observa'#231#245'es'
            Width = 300
            Visible = True
          end>
      end
      object DBGEmitCusWE: TDBGrid
        Left = 200
        Top = 44
        Width = 295
        Height = 41
        DataSource = DsEmitCusWE
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Visible = False
        Columns = <
          item
            Expanded = False
            FieldName = 'MPVIts'
            Title.Caption = 'Item pedido'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Title.Caption = 'Peso kg'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TextoECor'
            Title.Caption = 'Artigo e cor (Texto livre)'
            Width = 159
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_CLI'
            Title.Caption = 'Cliente final'
            Visible = True
          end>
      end
      object DBGEmitCusVS_BH: TDBGrid
        Left = 16
        Top = 84
        Width = 295
        Height = 57
        DataSource = DsLotes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Visible = False
        Columns = <
          item
            Expanded = False
            FieldName = 'VSMovIts'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_SerieFch'
            Title.Caption = 'S'#233'rie'
            Width = 71
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ficha'
            Title.Caption = 'Ficha RMP'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Custo'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Marca'
            Width = 103
            Visible = True
          end>
      end
      object DBGEmitCusVS_WE: TDBGrid
        Left = 16
        Top = 148
        Width = 295
        Height = 53
        DataSource = DsLotes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'VSMovIts'
            Title.Caption = 'IME-I'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Custo'
            Width = 72
            Visible = True
          end>
      end
    end
  end
  object QrEmit: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEmitAfterOpen
    BeforeClose = QrEmitBeforeClose
    AfterScroll = QrEmitAfterScroll
    OnCalcFields = QrEmitCalcFields
    SQL.Strings = (
      'SELECT gru.Nome NO_EmitGru, '
      'esp.Linhas, reb.Linhas RebLin, emi.*'
      'FROM emit emi '
      'LEFT JOIN emitgru gru ON gru.Codigo=emi.EmitGru '
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=emi.GraCorCad '
      'LEFT JOIN espessuras esp ON esp.Codigo=emi.Espessura'
      'LEFT JOIN espessuras reb ON reb.Codigo=emi.SemiCodEspReb'
      'WHERE emi.Codigo>=0')
    Left = 429
    Top = 65535
    object QrEmitNumCODIF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NumCODIF'
      Size = 11
      Calculated = True
    end
    object QrEmitCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitDataEmis: TDateTimeField
      FieldName = 'DataEmis'
    end
    object QrEmitStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrEmitNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrEmitNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrEmitNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrEmitTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrEmitNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrEmitClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrEmitTipificacao: TSmallintField
      FieldName = 'Tipificacao'
    end
    object QrEmitTempoR: TSmallintField
      FieldName = 'TempoR'
    end
    object QrEmitTempoP: TSmallintField
      FieldName = 'TempoP'
    end
    object QrEmitSetor: TSmallintField
      FieldName = 'Setor'
    end
    object QrEmitEspessura: TWideStringField
      FieldName = 'Espessura'
      Size = 15
    end
    object QrEmitPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEmitQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmitLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmitDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmitDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmitUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmitAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEmitFulao: TWideStringField
      FieldName = 'Fulao'
      Size = 5
    end
    object QrEmitDefPeca: TWideStringField
      FieldName = 'DefPeca'
      Size = 15
    end
    object QrEmitCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrEmitObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
    object QrEmitAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEmitTipific: TSmallintField
      FieldName = 'Tipific'
    end
    object QrEmitAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitSetrEmi: TSmallintField
      FieldName = 'SetrEmi'
    end
    object QrEmitCod_Espess: TIntegerField
      FieldName = 'Cod_Espess'
    end
    object QrEmitCodDefPeca: TIntegerField
      FieldName = 'CodDefPeca'
    end
    object QrEmitSourcMP: TSmallintField
      FieldName = 'SourcMP'
    end
    object QrEmitCustoTo: TFloatField
      FieldName = 'CustoTo'
    end
    object QrEmitCustoKg: TFloatField
      FieldName = 'CustoKg'
    end
    object QrEmitCustoM2: TFloatField
      FieldName = 'CustoM2'
    end
    object QrEmitCusUSM2: TFloatField
      FieldName = 'CusUSM2'
    end
    object QrEmitEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrEmitRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
    end
    object QrEmitNO_EmitGru: TWideStringField
      FieldName = 'NO_EmitGru'
      Size = 60
    end
    object QrEmitSemiAreaM2: TFloatField
      FieldName = 'SemiAreaM2'
    end
    object QrEmitSemiRendim: TFloatField
      FieldName = 'SemiRendim'
    end
    object QrEmitBRL_USD: TFloatField
      FieldName = 'BRL_USD'
    end
    object QrEmitBRL_EUR: TFloatField
      FieldName = 'BRL_EUR'
    end
    object QrEmitDtaCambio: TDateField
      FieldName = 'DtaCambio'
    end
    object QrEmitVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
    end
    object QrEmitGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrEmitNoGraCorCad: TWideStringField
      FieldName = 'NoGraCorCad'
      Size = 60
    end
    object QrEmitLinhas: TWideStringField
      FieldName = 'Linhas'
      Size = 7
    end
    object QrEmitRebLin: TWideStringField
      FieldName = 'RebLin'
      Size = 7
    end
    object QrEmitSemiCodPeca: TIntegerField
      FieldName = 'SemiCodPeca'
    end
    object QrEmitSemiTxtPeca: TWideStringField
      FieldName = 'SemiTxtPeca'
      Size = 15
    end
    object QrEmitSemiPeso: TFloatField
      FieldName = 'SemiPeso'
    end
    object QrEmitSemiQtde: TFloatField
      FieldName = 'SemiQtde'
    end
    object QrEmitSemiCodEspe: TIntegerField
      FieldName = 'SemiCodEspe'
    end
    object QrEmitSemiTxtEspe: TWideStringField
      FieldName = 'SemiTxtEspe'
      Size = 15
    end
    object QrEmitHoraIni: TTimeField
      FieldName = 'HoraIni'
    end
    object QrEmitSemiCodEspReb: TIntegerField
      FieldName = 'SemiCodEspReb'
    end
    object QrEmitSemiTxtEspReb: TWideStringField
      FieldName = 'SemiTxtEspReb'
      Size = 15
    end
    object QrEmitDtaBaixa: TDateField
      FieldName = 'DtaBaixa'
      Required = True
    end
    object QrEmitDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
      Required = True
    end
    object QrEmitEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEmitAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrEmitAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrEmitSbtCouIntros: TFloatField
      FieldName = 'SbtCouIntros'
      Required = True
    end
    object QrEmitSbtAreaM2: TFloatField
      FieldName = 'SbtAreaM2'
      Required = True
    end
    object QrEmitSbtAreaP2: TFloatField
      FieldName = 'SbtAreaP2'
      Required = True
    end
    object QrEmitSbtRendEsper: TFloatField
      FieldName = 'SbtRendEsper'
      Required = True
    end
    object QrEmitOperPosStatus: TSmallintField
      FieldName = 'OperPosStatus'
      Required = True
    end
    object QrEmitOperPosGrandz: TIntegerField
      FieldName = 'OperPosGrandz'
      Required = True
    end
    object QrEmitOperPosQtdDon: TFloatField
      FieldName = 'OperPosQtdDon'
      Required = True
    end
    object QrEmitOperPosDthIni: TDateTimeField
      FieldName = 'OperPosDthIni'
      Required = True
    end
    object QrEmitOperPosDthFim: TDateTimeField
      FieldName = 'OperPosDthFim'
      Required = True
    end
    object QrEmitVersao: TIntegerField
      FieldName = 'Versao'
      Required = True
    end
    object QrEmitTpReceita: TSmallintField
      FieldName = 'TpReceita'
    end
  end
  object DsEmit: TDataSource
    DataSet = QrEmit
    Left = 429
    Top = 43
  end
  object QrMPIn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MPIn, MPVIts '
      'FROM emitcus'
      'WHERE Codigo=:P0')
    Left = 149
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPInMPIn: TIntegerField
      FieldName = 'MPIn'
    end
    object QrMPInMPVIts: TIntegerField
      FieldName = 'MPVIts'
    end
  end
  object QrPQx: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial '
      'ELSE cli.Nome END NOMECLI, pq_.Nome NOMEPQ, pqx.*,'
      '(pqx.Valor/pqx.Peso) CUSTOMEDIO'
      'FROM pqx pqx'
      'LEFT JOIN pq        pq_ ON pq_.Codigo=pqx.Insumo'
      'LEFT JOIN entidades cli ON cli.Codigo=pqx.CliOrig'
      'WHERE pqx.OrigemCodi=:P0'
      'AND pqx.Tipo=110')
    Left = 145
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQxInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQxCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQxDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQxTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPQxCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPQxPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQxValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQxOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQxOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQxNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrPQxNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQxCUSTOMEDIO: TFloatField
      FieldName = 'CUSTOMEDIO'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQxEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object DsPQx: TDataSource
    DataSet = QrPQx
    Left = 144
    Top = 260
  end
  object PMExclui: TPopupMenu
    OnPopup = PMExcluiPopup
    Left = 700
    Top = 588
    object Pesagem1: TMenuItem
      Caption = '&Pesagem'
      object Itemdepesagem1: TMenuItem
        Caption = '&Item de pesagem selecionado'
        OnClick = Itemdepesagem1Click
      end
      object Todapesagem1: TMenuItem
        Caption = '&Toda pesagem'
        OnClick = Todapesagem1Click
      end
    end
    object Produto1: TMenuItem
      Caption = '&Produto resultante'
      Enabled = False
      Visible = False
      object Lotedecurtimento1: TMenuItem
        Caption = 'Lote de curtimento'
        OnClick = Lotedecurtimento1Click
      end
      object Lotedesemiacabado1: TMenuItem
        Caption = '&Lote de semi / acabado'
        OnClick = Lotedesemiacabado1Click
      end
    end
    object Matriaprima1: TMenuItem
      Caption = 'Mat'#233'ria-prima'
      OnClick = Matriaprima1Click
    end
    object Recurtimento1: TMenuItem
      Caption = 'MP &Recurtimento (antigo)'
      Enabled = False
      Visible = False
      OnClick = Recurtimento1Click
    end
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq '
      'WHERE Ativo=1'
      'AND GGXNiv2 IN (-1,-2,-4, 1, 2, 4)'
      'ORDER BY Nome')
    Left = 76
    Top = 288
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 76
    Top = 244
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 76
    Top = 196
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 76
    Top = 336
  end
  object QrSaldo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO '
      'FROM pqcli'
      'WHERE CI=:P0'
      'AND PQ=:P1')
    Left = 16
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSaldoPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSaldoValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSaldoCUSTO: TFloatField
      FieldName = 'CUSTO'
    end
    object QrSaldoCI: TIntegerField
      FieldName = 'CI'
    end
    object QrSaldoPQ: TIntegerField
      FieldName = 'PQ'
    end
  end
  object DsSaldo: TDataSource
    DataSet = QrSaldo
    Left = 16
    Top = 244
  end
  object QrSumEmit: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEmitAfterOpen
    SQL.Strings = (
      'SELECT SUM(Custo) CUSTO '
      'FROM emit'
      'WHERE Codigo=:P0'
      '')
    Left = 13
    Top = 291
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumEmitCUSTO: TFloatField
      FieldName = 'CUSTO'
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Codigo) UltimaPesagem'
      'FROM emit')
    Left = 352
    Top = 164
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrEmitCusBH: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mpi.Marca, mpi.Data DataMPIn,'
      'emc.Pecas, emc.Peso, emc.Custo, emc.MPIn'
      'FROM emitcus emc'
      'LEFT JOIN mpin mpi ON mpi.Controle=emc.MPIn'
      'WHERE emc.Codigo=:P0'
      '')
    Left = 504
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmitCusBHMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrEmitCusBHDataMPIn: TDateField
      FieldName = 'DataMPIn'
    end
    object QrEmitCusBHPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrEmitCusBHPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrEmitCusBHCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrEmitCusBHMPIn: TIntegerField
      FieldName = 'MPIn'
      Required = True
    end
  end
  object DsEmitCusBH: TDataSource
    DataSet = QrEmitCusBH
    Left = 504
    Top = 44
  end
  object QrWBMovIts: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrWBMovItsAfterOpen
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM wbmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 644
    Top = 1
    object QrWBMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWBMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrWBMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrWBMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrWBMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrWBMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrWBMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWBMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWBMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWBMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWBMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWBMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWBMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWBMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrWBMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrWBMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrWBMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrWBMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrWBMovItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrWBMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrWBMovItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsWBMovIts: TDataSource
    DataSet = QrWBMovIts
    Left = 644
    Top = 45
  end
  object QrEmitCusWE: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEmitCusWEBeforeClose
    AfterScroll = QrEmitCusWEAfterScroll
    SQL.Strings = (
      'SELECT CONCAT(mpi.Texto, " ", mpi.CorTxt) TextoECor,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_CLI,'
      'ecu.* '
      'FROM emitcus ecu'
      'LEFT JOIN mpvits mpi ON mpi.Controle=ecu.MPVIts'
      'LEFT JOIN mpp mpp ON mpp.Codigo=mpi.Pedido '
      'LEFT JOIN entidades ent ON ent.Codigo=mpp.Cliente '
      'WHERE ecu.Controle>0 ')
    Left = 576
    Top = 1
    object QrEmitCusWETextoECor: TWideStringField
      FieldName = 'TextoECor'
      Size = 81
    end
    object QrEmitCusWENOME_CLI: TWideStringField
      FieldName = 'NOME_CLI'
      Size = 100
    end
    object QrEmitCusWECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitCusWEControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEmitCusWEMPIn: TIntegerField
      FieldName = 'MPIn'
    end
    object QrEmitCusWEFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrEmitCusWEDataEmis: TDateTimeField
      FieldName = 'DataEmis'
    end
    object QrEmitCusWEPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEmitCusWECusto: TFloatField
      FieldName = 'Custo'
    end
    object QrEmitCusWEPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEmitCusWEAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitCusWEMPVIts: TIntegerField
      FieldName = 'MPVIts'
    end
    object QrEmitCusWEAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEmitCusWEAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsEmitCusWE: TDataSource
    DataSet = QrEmitCusWE
    Left = 576
    Top = 45
  end
  object PMImprime: TPopupMenu
    OnPopup = PMImprimePopup
    Left = 32
    Top = 12
    object Estoquedematriaprimadesemiacabadomostraroutrajanela1: TMenuItem
      Caption = 
        '&Estoque de mat'#233'ria-prima de semi / acabado (mostrar'#225' outra jane' +
        'la)'
      OnClick = Estoquedematriaprimadesemiacabadomostraroutrajanela1Click
    end
    object Zerapreos1: TMenuItem
      Caption = 'Zera pre'#231'os de todos movimentos de pq desta empresa'
      Enabled = False
      OnClick = Zerapreos1Click
    end
  end
  object QrLotes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emc.Controle, emc.VSMovIts, emc.Peso, emc.Pecas,   '
      'vmi.SerieFch, vmi.Ficha, vmi.Marca,fch.Nome NO_SerieFch  '
      'FROM emitcus emc '
      'LEFT JOIN vsmovits vmi ON vmi.Controle=emc.VSMovIts '
      'LEFT JOIN vsserfch fch ON fch.Codigo=vmi.SerieFch '
      'WHERE emc.Codigo>0 ')
    Left = 352
    Top = 261
    object QrLotesControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLotesVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrLotesPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrLotesPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrLotesSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrLotesFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrLotesMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrLotesNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrLotesCusto: TFloatField
      FieldName = 'Custo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotesOP: TIntegerField
      FieldName = 'OP'
    end
  end
  object DsLotes: TDataSource
    DataSet = QrLotes
    Left = 352
    Top = 309
  end
  object PMAdiciona: TPopupMenu
    Left = 568
    Top = 592
    object Insumonapessagem1: TMenuItem
      Caption = '&Insumo na pessagem'
      OnClick = Insumonapessagem1Click
    end
    object MateriaPrima1: TMenuItem
      Caption = '&Mat'#233'ria-prima'
      OnClick = MateriaPrima1Click
    end
  end
end
