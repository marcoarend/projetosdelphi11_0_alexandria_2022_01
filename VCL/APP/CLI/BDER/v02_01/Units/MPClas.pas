unit MPClas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkEdit, UnDmkProcFunc,
  dmkLabel, dmkImage, UnDmkEnums;

type
  TFmMPClas = class(TForm)
    PainelDados: TPanel;
    DsMPClas: TDataSource;
    QrMPClas: TmySQLQuery;
    QrMPClasLk: TIntegerField;
    QrMPClasDataCad: TDateField;
    QrMPClasDataAlt: TDateField;
    QrMPClasUserCad: TIntegerField;
    QrMPClasUserAlt: TIntegerField;
    QrMPClasCodigo: TSmallintField;
    QrMPClasNome: TWideStringField;
    PainelEdita: TPanel;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMPClasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrMPClasAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMPClasBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    FSeq: Integer;
  end;

var
  FmMPClas: TFmMPClas;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMPClas.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMPClas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMPClasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMPClas.DefParams;
begin
  VAR_GOTOTABELA := 'MPClas';
  VAR_GOTOMYSQLTABLE := QrMPClas;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM mpclas');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmMPClas.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := '';
        EdNome.Text   := '';
      end else begin
        EdCodigo.Text := IntToStr(QrMPClasCodigo.Value);
        EdNome.Text   := QrMPClasNome.Value;
      end;
      EdNome.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmMPClas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmMPClas.QueryPrincipalAfterOpen;
begin
end;

procedure TFmMPClas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmMPClas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMPClas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMPClas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMPClas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMPClas.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmMPClas.BtAlteraClick(Sender: TObject);
var
  MPClas : Integer;
begin
  MPClas := QrMPClasCodigo.Value;
  if not UMyMod.SelLockY(MPClas, Dmod.MyDB, 'MPClas', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(MPClas, Dmod.MyDB, 'MPClas', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmMPClas.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrMPClasCodigo.Value;
  Close;
end;

procedure TFmMPClas.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descrição.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO mpclas SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'MPClas', 'MPClas', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE mpclas SET ');
    Codigo := QrMPClasCodigo.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Nome=:P0, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  //
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[03].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'MPClas', 'Codigo');
  MostraEdicao(0, stLok, 0);
  LocCod(Codigo,Codigo);
  if FSeq = 1 then Close;
end;

procedure TFmMPClas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'MPClas', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'MPClas', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'MPClas', 'Codigo');
end;

procedure TFmMPClas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FSeq := 0;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmMPClas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMPClasCodigo.Value,LaRegistro.Caption);
end;

procedure TFmMPClas.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmMPClas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMPClas.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmMPClas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrMPClasCodigo.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmMPClas.QrMPClasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmMPClas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'MPClas', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, stIns, 0);
end;

procedure TFmMPClas.QrMPClasAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrMPClasCodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrMPClasCodigo.Value, False);
end;

procedure TFmMPClas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrMPClasCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'MPClas', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmMPClas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPClas.QrMPClasBeforeOpen(DataSet: TDataSet);
begin
  QrMPClasCodigo.DisplayFormat := FFormatFloat;
end;

end.

