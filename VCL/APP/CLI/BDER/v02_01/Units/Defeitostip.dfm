object FmDefeitostip: TFmDefeitostip
  Left = 368
  Top = 194
  Caption = 'COU-MP_IN-010 :: Tipos de Defeitos'
  ClientHeight = 317
  ClientWidth = 728
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 728
    Height = 221
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    ExplicitHeight = 193
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 728
      Height = 121
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 120
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 360
        Top = 8
        Width = 113
        Height = 13
        Caption = 'Proced'#234'ncia do defeito:'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 288
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Sigla:'
        FocusControl = DBEdit2
      end
      object ShCorBord2: TShape
        Left = 556
        Top = 60
        Width = 17
        Height = 17
      end
      object Label6: TLabel
        Left = 580
        Top = 64
        Width = 46
        Height = 13
        Caption = 'Cor borda'
      end
      object ShCorMiol2: TShape
        Left = 556
        Top = 80
        Width = 17
        Height = 17
      end
      object Label7: TLabel
        Left = 580
        Top = 84
        Width = 43
        Height = 13
        Caption = 'Cor miolo'
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsDefeitostip
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 120
        Top = 24
        Width = 162
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsDefeitostip
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 360
        Top = 24
        Width = 357
        Height = 21
        DataField = 'NOMEPROCEDE'
        DataSource = DsDefeitostip
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 288
        Top = 24
        Width = 69
        Height = 21
        DataField = 'Sigla'
        DataSource = DsDefeitostip
        TabOrder = 3
      end
      object PnImageDado: TPanel
        Left = 16
        Top = 56
        Width = 55
        Height = 55
        DoubleBuffered = True
        ParentDoubleBuffered = False
        TabOrder = 4
        object ImgDado: TImage
          Left = 3
          Top = 3
          Width = 49
          Height = 49
        end
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 76
        Top = 48
        Width = 469
        Height = 65
        Caption = ' S'#237'mbolo: '
        Columns = 6
        DataField = 'Simbolo'
        DataSource = DsDefeitostip
        Items.Strings = (
          'Nenhum'
          'Risco'
          'Rasgo'
          'C'#237'rculo'
          'Quadrado'
          'Estrela'
          'Cruz'
          'Losango'
          'Triangulo'
          'Saturno'
          'Ret'#226'ngulo'
          'ForRew')
        ParentBackground = True
        TabOrder = 5
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9'
          '10'
          '11'
          '12'
          '13'
          '14'
          '15'
          '16'
          '17'
          '18'
          '19'
          '20')
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 157
      Width = 728
      Height = 64
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 129
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 57
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 231
        Top = 15
        Width = 495
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 362
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 124
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 244
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 728
    Height = 221
    Align = alClient
    BevelOuter = bvNone
    DoubleBuffered = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    ExplicitHeight = 193
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 728
      Height = 121
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 120
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 288
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Sigla:'
      end
      object SbCorBorda: TSpeedButton
        Left = 560
        Top = 60
        Width = 24
        Height = 24
        Caption = '...'
        OnClick = SbCorBordaClick
      end
      object SbCorMiolo: TSpeedButton
        Left = 560
        Top = 84
        Width = 24
        Height = 24
        Caption = '...'
        OnClick = SbCorMioloClick
      end
      object ShCorBorda: TShape
        Left = 589
        Top = 64
        Width = 17
        Height = 17
      end
      object LaCorMiolo: TLabel
        Left = 612
        Top = 65
        Width = 43
        Height = 13
        Caption = 'Cor miolo'
      end
      object ShCorMiolo: TShape
        Left = 589
        Top = 88
        Width = 17
        Height = 17
      end
      object LaCorBorda: TLabel
        Left = 612
        Top = 90
        Width = 46
        Height = 13
        Caption = 'Cor borda'
      end
      object EdCodigo: TdmkEdit
        Left = -36
        Top = 32
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 120
        Top = 24
        Width = 162
        Height = 21
        MaxLength = 255
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSigla: TdmkEdit
        Left = 288
        Top = 24
        Width = 162
        Height = 21
        MaxLength = 255
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGProcede: TRadioGroup
        Left = 455
        Top = 8
        Width = 261
        Height = 41
        Caption = ' Proced'#234'ncia do defeito: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Cria'#231#227'o'
          'Frigor'#237'fico'
          'Curtume')
        TabOrder = 3
      end
      object RGSimbolo: TdmkRadioGroup
        Left = 84
        Top = 48
        Width = 469
        Height = 65
        Caption = ' Simbolo:'
        Columns = 6
        Items.Strings = (
          'Nenhum'
          'Risco'
          'Rasgo'
          'C'#237'rculo'
          'Quadrado'
          'Estrela'
          'Cruz'
          'Losango'
          'Triangulo'
          'Saturno'
          'Ret'#226'ngulo'
          'ForRew')
        TabOrder = 4
        OnClick = RGSimboloClick
        QryCampo = 'Simbolo'
        UpdCampo = 'Simbolo'
        UpdType = utYes
        OldValor = 0
      end
      object PnImageEdit: TPanel
        Left = 16
        Top = 56
        Width = 55
        Height = 55
        DoubleBuffered = True
        ParentDoubleBuffered = False
        TabOrder = 5
        object ImgEdita: TImage
          Left = 3
          Top = 3
          Width = 49
          Height = 49
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 157
      Width = 728
      Height = 64
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 129
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 724
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 580
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 12
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 728
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 680
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 464
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 211
        Height = 32
        Caption = 'Tipos de Defeitos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 211
        Height = 32
        Caption = 'Tipos de Defeitos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 211
        Height = 32
        Caption = 'Tipos de Defeitos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 728
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 724
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsDefeitostip: TDataSource
    DataSet = QrDefeitostip
    Left = 504
    Top = 53
  end
  object QrDefeitostip: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrDefeitostipBeforeOpen
    AfterOpen = QrDefeitostipAfterOpen
    AfterScroll = QrDefeitostipAfterScroll
    OnCalcFields = QrDefeitostipCalcFields
    SQL.Strings = (
      'SELECT * FROM defeitostip'
      'WHERE Codigo > 0')
    Left = 500
    Top = 9
    object QrDefeitostipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDefeitostipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrDefeitostipLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrDefeitostipDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDefeitostipDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDefeitostipUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDefeitostipUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDefeitostipProcede: TIntegerField
      FieldName = 'Procede'
    end
    object QrDefeitostipNOMEPROCEDE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEPROCEDE'
      Calculated = True
    end
    object QrDefeitostipSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 5
    end
    object QrDefeitostipSimbolo: TIntegerField
      FieldName = 'Simbolo'
    end
    object QrDefeitostipCorMiolo: TIntegerField
      FieldName = 'CorMiolo'
    end
    object QrDefeitostipCorBorda: TIntegerField
      FieldName = 'CorBorda'
    end
  end
end
