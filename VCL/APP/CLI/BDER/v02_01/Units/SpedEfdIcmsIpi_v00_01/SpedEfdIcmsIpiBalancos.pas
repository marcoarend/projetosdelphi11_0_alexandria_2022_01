unit SpedEfdIcmsIpiBalancos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkCheckGroup, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Variants, dmkEditDateTimePicker,
  frxClass, frxDBSet, AppListas, SPED_Listas, UnSPED_EFD_PF;

type
  TFmSpedEfdIcmsIpiBalancos = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrSumPQx: TmySQLQuery;
    QrSumPQxInsumo: TIntegerField;
    QrSumPQxPeso: TFloatField;
    QrSumPQxValor: TFloatField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrX999: TmySQLQuery;
    QrGraGruXSigla: TWideStringField;
    QrSumPQxCliDest: TIntegerField;
    QrNivelSel1: TmySQLQuery;
    QrNivelSel1Codigo: TIntegerField;
    QrNivelSel1Nome: TWideStringField;
    DsNivelSel1: TDataSource;
    QrNivelSel2: TmySQLQuery;
    QrNivelSel2Codigo: TIntegerField;
    QrNivelSel2Nome: TWideStringField;
    DsNivelSel2: TDataSource;
    QrVMI: TmySQLQuery;
    QrVMISigla: TWideStringField;
    QrVMICouNiv2: TIntegerField;
    QrVMIGraGruY: TIntegerField;
    QrVMIGragRuX: TIntegerField;
    QrVMIGraGru1: TIntegerField;
    QrVMIUnMedTxt: TWideStringField;
    QrVMITipoEstq: TLargeintField;
    QrVMISdoVrtPeca: TFloatField;
    QrVMISdoVrtArM2: TFloatField;
    QrVMISdoVrtPeso: TFloatField;
    QrVMINO_PRD_TAM_COR: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrVMIEmpresa: TIntegerField;
    QrVMIGrandeza: TSmallintField;
    QrVMIUnidMed: TIntegerField;
    QrVMIPecas: TFloatField;
    QrVMIAreaM2: TFloatField;
    QrVMIPesoKg: TFloatField;
    QrVMIValorT: TFloatField;
    PnPeriodo: TPanel;
    LaMes: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    LaAno: TLabel;
    PCRegistro: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel5: TPanel;
    GBImportar1: TGroupBox;
    RGNivPlaCta1: TRadioGroup;
    Panel8: TPanel;
    Label1: TLabel;
    EdGenPlaCta1: TdmkEditCB;
    CBGenPlaCta1: TdmkDBLookupComboBox;
    GBImportar2: TGroupBox;
    RGNivPlaCta2: TRadioGroup;
    Panel9: TPanel;
    Label2: TLabel;
    EdGenPlaCta2: TdmkEditCB;
    CBGenPlaCta2: TdmkDBLookupComboBox;
    LaData: TLabel;
    TPData: TdmkEditDateTimePicker;
    CGImportar: TdmkCheckGroup;
    PB1: TProgressBar;
    MeAviso: TMemo;
    QrVMIEntiSitio: TIntegerField;
    QrVMISitProd: TSmallintField;
    QrVMIClientMO: TIntegerField;
    DsVMI: TDataSource;
    PageControl1: TPageControl;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    DBGrid1: TDBGrid;
    QrVMI2: TmySQLQuery;
    QrVMI2IMEI: TIntegerField;
    QrGraGruXTipo_Item: TIntegerField;
    QrVMITipo_Item: TIntegerField;
    QrCA_VS: TmySQLQuery;
    QrCA_VSEntiSitio: TIntegerField;
    QrCA_VSGraGruX: TIntegerField;
    QrCA_VSPecas: TFloatField;
    QrCA_VSPesoKg: TFloatField;
    QrCA_VSAreaM2: TFloatField;
    QrCA_VSMovimID: TIntegerField;
    QrCA_VSCodigo: TIntegerField;
    QrCA_VSMovimCod: TIntegerField;
    QrCA_VSMovimNiv: TIntegerField;
    QrCA_VSControle: TIntegerField;
    QrCA_VSMes: TLargeintField;
    QrCA_VSAno: TLargeintField;
    QrCA_VSGrandeza: TIntegerField;
    QrCA_VSDataHora: TDateTimeField;
    QrCA_VSDtCorrApo: TDateTimeField;
    QrCA_VSClientMO: TIntegerField;
    QrCA_VSFornecMO: TIntegerField;
    CGRegistros: TdmkCheckGroup;
    QrVMIIMEI: TIntegerField;
    QrUniPQx: TmySQLQuery;
    QrUniPQxInsumo: TIntegerField;
    QrUniPQxCliDest: TIntegerField;
    QrUniPQxPeso: TFloatField;
    QrUniPQxValor: TFloatField;
    QrUniPQxMovimCod: TWideStringField;
    QrUniPQxOrigemCodi: TIntegerField;
    QrUniPQxOrigemCtrl: TIntegerField;
    QrUniPQxDataX: TDateField;
    QrUniPQxDtCorrApo: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGNivPlaCta1Click(Sender: TObject);
    procedure CGImportarClick(Sender: TObject);
    procedure RGNivPlaCta2Click(Sender: TObject);
    procedure PCRegistroChanging(Sender: TObject; var AllowChange: Boolean);
  private
    { Private declarations }
    //
    procedure ImportaPQxSumH010(Bal_TXT: String; UnidMedKG: Integer);
    procedure ImportaPQxUniK200(Bal_TXT: String; UnidMedKG: Integer);
    //
    function  InsereItemAtual_H010(BalID, BalNum, BalItm, BalEnt: Integer;
              COD_ITEM, UNID, IND_PROP, COD_PART, TXT_COMPL, COD_CTA: String;
              QTD, VL_UNIT, VL_ITEM, VL_ITEM_IR: Double): Boolean;
    function  InsereItemAtual_K200(BalTab: TEstqSPEDTabSorc; BalID, BalCod,
              BalNum, BalItm: Integer;
              DT_EST, COD_ITEM: String; var QTD: Double; IND_EST, COD_PART:
              String; GraGruX, Grandeza: Integer; var Pecas, AreaM2, PesoKg:
              Double; Entidade, Tipo_Item: Integer):
              Boolean;
    //function  InsumosSemKg(DataBal: String): Boolean;
    function  ErrUnidMedUsoConsumo(DataBal: String): Boolean;
    function  ReopenGraGruX(Nivel1: Integer): Boolean;
  public
    { Public declarations }
    FImporExpor, FAnoMes, FEmpresa, FRegPai: Integer;
    FData: TDateTime;
  end;

  var
  FmSpedEfdIcmsIpiBalancos: TFmSpedEfdIcmsIpiBalancos;

implementation

uses UnMyObjects, Module, UnDmkProcFunc, DmkDAC_PF, UMySQLModule, UnFinanceiro,
  ModuleFin, ModuleGeral, UnVS_PF, ModProd, ModAppGraG1, MyDBCheck, StqCenCad,
  GraCusPrc, UnidMed;

{$R *.DFM}

procedure TFmSpedEfdIcmsIpiBalancos.BtOKClick(Sender: TObject);
const
  sProcName = 'FmSPED_EFD_X999_Balancos.BtOKClick()';
  //
  function AbreSQLGraGruX(Query: TmySQLQuery; SQL_Fld, SQL_PSQ: String): Boolean;
  const
    FldSdoVrtArM2 = 'vmi.SdoVrtArM2';
    FldSdoVrtPeso = 'vmi.SdoVrtPeso';
    FldSdoVrtPeca = 'vmi.SdoVrtPeca';
  var
    Parte1, TipoEstq, ATT_UnMedTxt: String;
  begin
    if SQL_Fld <> '' then
      Parte1 := SQL_Fld
    else
    begin
      TipoEstq := VS_PF.SQL_TipoEstq_DefinirCodi(False, '', False,
      FldSdoVrtArM2, FldSdoVrtPeso, FldSdoVrtPeca);
      ATT_UnMedTxt := dmkPF.ArrayToTexto(TipoEstq, 'UnMedTxt', pvPos, False,
        sGrandezaUnidMed, 2);
      //
      Parte1 := Geral.ATS([
      'SELECT scc.EntiSitio, scc.SitProd, med.Sigla, med.Grandeza, ',
      'vmi.IMEI, vmi.CouNiv2, vmi.GraGruY, ',
      'vmi.GragRuX, vmi.GraGru1, vmi.Empresa, vmi.Pecas, vmi.AreaM2, ',
      'vmi.PesoKg, vmi.ValorT, gg1.UnidMed, CONCAT(gg1.Nome,',
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
      'NO_PRD_TAM_COR, ',
      ATT_UnMedTxt,
      (*
      'IF((vmi.GraGruY<2048 OR  ',
      'vmi.CouNiv2<>1) AND vmi.SdoVrtPeso > 0, "Kg",   ',
      'IF(vmi.CouNiv2=1 AND vmi.SdoVrtArM2 > 0, "m�", "Erro!")) UnMedTxt,   ',
      *)
      VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
      FldSdoVrtArM2, FldSdoVrtPeso, FldSdoVrtPeca),
      (*
      'IF((vmi.GraGruY<2048 OR  ',
      'vmi.CouNiv2<>1) AND vmi.SdoVrtPeso > 0, 2,   ',
      'IF(vmi.CouNiv2=1 AND vmi.SdoVrtArM2 > 0, 1, -1)) TipoEstq,   ',
      *)
      'vmi.SdoVrtPeca, ',
      'vmi.SdoVrtArM2, ',
      'vmi.SdoVrtPeso, ',
      'vmi.ClientMO ,',
      'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
      'pgt.Tipo_Item) Tipo_Item ',
      '']);
    end;
    Result := UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
    Parte1,
    'FROM _vsmovimp1_ vmi  ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=vmi.GraGru1',
    'LEFT JOIN ' + TMeuDB + '.gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
    'LEFT JOIN ' + TMeuDB + '.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
    'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI',
    'LEFT JOIN ' + TMeuDB + '.stqcencad  scc ON scc.Codigo=vmi.StqCenCad',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=vmi.GraGruX',
    'WHERE ( ',
    '  vmi.SdoVrtPeca > 0   ',
    '  OR  ',
(*  Inicio 2017-11-20*)
    //'  (ggx.GraGruY<1024 AND vmi.Pecas=0 AND vmi.SdoVrtPeso > 0) ',
    '  (vmi.Pecas=0 AND vmi.SdoVrtPeso > 0) ',
(*  Fim 2017-11-20*)
    ') ',
    'AND (  ',
    '  ((vmi.GraGruY<2048 OR vmi.CouNiv2<>1) AND vmi.SdoVrtPeso > 0)  ',
    '   OR  ',
//    '  (vmi.CouNiv2=1 AND vmi.SdoVrtArM2 > 0)  ',
    '  (vmi.SdoVrtArM2 > 0)  ',
    ')',
    SQL_PSQ,
    '']);
    //Geral.MB_SQL(Self, Query);
  end;
const
  DBG00GraGruY = nil;
  DBG00GraGruX = nil;
  DBG00CouNiv2 = nil;
  Qr00GraGruY  = nil;
  Qr00GraGruX  = nil;
  Qr00CouNiv2  = nil;
  DataCompra   = False;
  EmProcessoBH = True;
  GraCusPrc    = 0;
  NFeIni       = 0;
  NFeFim       = 0;
  UsaSerie     = False;
  Serie        = 0;
  MovimCod     = 0;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekEstoque;
var
  DataBal: TDateTime;
  Bal_TXT: String;
  NoUnid: Integer;

  //
  BalID, BalNum, BalItm, BalEnt: Integer;
  COD_ITEM, UNID, IND_PROP, COD_PART, TXT_COMPL, COD_CTA: String;
  QTD, VL_UNIT, VL_ITEM, VL_ITEM_IR, Preco: Double;
var
  Empresa, Entidade, Filial, Terceiro, Ordem1, Ordem2, Ordem3, Ordem4, Ordem5,
  Agrupa, StqCenCad, ZeroNegat: Integer;
  TableSrc, NO_Empresa, NO_StqCenCad, NO_Terceiro, SQL_PSQ: String;
  EstoqueEm, DataRelativa: TDateTime;
  MostraFrx: Boolean;
  //
  UnidMed, Nivel1, UnidMedPc, UnidMedKg, UnidMedM2: Integer;
  Corda, Aviso: String;
  Erro: Boolean;
  Fator, MyQtd, MyVlU, MyBas: Double;
  Tipo: TGrandezaUnidMed;
  //
  DT_EST, IND_EST, SQL_ERR, SQL_PeriodoPQ, SQL_PeriodoVS: String;
  DescrAgruNoItm: Boolean;
  I, GraGruX, Grandeza, Tipo_Item, LinArq: Integer;
  Pecas, AreaM2, PesoKg, Qtde: Double;
  //
  DiaIni, DiaFim, DiaPos: TDateTime;
  OriBalID: TSPED_EFD_Bal;
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  OriESTSTabSorc: TEstqSPEDTabSorc;
  OriOrigemOpeProc: TOrigemOpeProc;
  OriOrigemIDKnd: TOrigemIDKnd;
  TipoPeriodoFiscal: TTipoPeriodoFiscal;
  BalTab: TEstqSPEDTabSorc;
  BalCod: Integer;
begin
  MeAviso.Lines.Clear;
  TipoPeriodoFiscal := TTipoPeriodoFiscal(
    SPED_EFD_PF.ObtemTipoPeriodoRegistro(FEmpresa, 'K100', sProcName));
  if (TipoPeriodoFiscal = TTipoPeriodoFiscal.spedperIndef) and
  (PCRegistro.ActivePageIndex = 2) then Exit;
  //
  DiaIni := EncodeDate(Geral.IMV(CBAno.Text), CBMes.ItemIndex + 1, 1);
  DiaFim := IncMonth(DiaIni, 1) -1;
  DiaPos := DiaFim + 1;
  //
  Erro := False;
  if MyObjects.FIC(CGImportar.Value = 0, CGImportar,
    'Selecione pelo menos um tipo de item a ser importado!') then Exit;
  if not DmProd.ObtemCodigoPecaPc(UnidMedPc) then Exit;
  if not DmProd.ObtemCodigoPesoKg(UnidMedKg) then Exit;
  if not DmProd.ObtemCodigoAreaM2(UnidMedM2) then Exit;
  if (UnidMedKg = 0) or (UnidMedM2 = 0) then Exit;
  // Uso e consumo
  if DmkPF.IntInConjunto2(1, CGImportar.Value) then
  begin
/////////////////////////////////  Bloco H  ////////////////////////////////////
    if (PCRegistro.ActivePageIndex = 1) // H010
////////////////////////////////////////////////////////////////////////////////
    or (
/////////////////////////////////  Bloco K  ////////////////////////////////////
      (PCRegistro.ActivePageIndex = 2)              // Bloco K
      and                                           // e
      (DmkPF.IntInConjunto2(1, CGRegistros.Value))   // Registro K200
////////////////////////////////////////////////////////////////////////////////
    ) then
    begin
      // H010
      case PCRegistro.ActivePageIndex of
        1: if MyObjects.FIC(EdGenPlaCta1.ValueVariant = 0, EdGenPlaCta1,
           'Informe o n�vel e o c�digo do plano de contas!') then Exit;
        2: if MyObjects.FIC(CGRegistros.Value = 0, CGRegistros,
      'Selecione pelo menos um tipo de registro a ser importado!') then Exit;
  // Nada
        else if MyObjects.FIC(True, EdGenPlaCta1,
           'Tipo de estoque indefinido para defini��o de necessidade de conta!') then Exit;
      end;
      case PCRegistro.ActivePageIndex of
        1: DataBal := EncodeDate(Geral.IMV(CBAno.Text), CBMes.ItemIndex + 1, 1);
        //2: DataBal := TPData.Date + 1;
        2: DataBal := FData + 1;
        else if MyObjects.FIC(True, EdGenPlaCta1,
           'Tipo de estoque indefinido para defini��o de data!') then Exit;
      end;
      //
      Bal_TXT := Geral.FDT(DataBal, 1);
      //
      //if InsumosSemKg(Bal_TXT) then
      if ErrUnidMedUsoConsumo(Bal_TXT) then
        Exit;
      //
      ImportaPQxSumH010(Bal_TXT, UnidMedKG);
      case PCRegistro.ActivePageIndex of
        1: ImportaPQxSumH010(Bal_TXT, UnidMedKG); // H010
        2: ImportaPQxUniK200(Bal_TXT, UnidMedKG); // K200
        else Geral.MB_Erro('"PCRegistro.ActivePageIndex" n�o implementado!');
      end;
{
      UnDmkDAC_PF.AbreMySQLQuery0(QrSumPQx, Dmod.MyDB, [
      'SELECT pqx.Insumo, pqx.CliDest,',
      'SUM(pqx.Peso) Peso, SUM(pqx.Valor) Valor ',
      'FROM pqx pqx',
      //'WHERE /*pqx.CliDest=-11 ',
      //'AND*/ pqx.Tipo=0 ',
      'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0000),
      'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
      'AND pqx.DataX="' + Bal_TXT + '" ',
      'GROUP BY pqx.Insumo, pqx.CliDest ',
      '']);
      //Geral.MB_SQL(Self, QrPQx);
      DT_EST  := Geral.FDT(FData, 2);
      if MyObjects.FIC(QrSumPQx.RecordCount = 0, nil,
      'N�o existe balan�o de uso e consumo no dia ' + DT_EST) then Exit;
      QrSumPQx.First;
      PB1.Position := 0;
      PB1.Max := QrSumPQx.RecordCount;
      QrSumPQx.DisableControls;
      PCRegistro.Visible := False;
      try
        while not QrSumPQx.Eof do
        // ver
        //Tipo_Item <> CO_SPED_TIPO_ITEM_K200
        begin
          MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
          //Reabre QrGraGruX
          if QrSumPQxPeso.Value > 0 then
          begin
            ReopenGraGruX(QrSumPQxInsumo.Value);
            UnidMed := QrGraGruXUnidMed.Value;
            if UnidMed = 0 then
            begin
              Nivel1  := QrSumPQxInsumo.Value;
              UnidMed := UnidMedKG;
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
              'UnidMed'], ['Nivel1'], [
              UnidMed], [Nivel1], True);
              //
              ReopenGraGruX(QrSumPQxInsumo.Value);
            end;
            //
            BalTab         := TEstqSPEDTabSorc.estsPQx;
            BalCod         := 0; ///SUM(....
            BalID          := Integer(TSPED_EFD_Bal.sebalPQx);

            BalNum         := FAnoMes;
            BalItm         := QrSumPQxInsumo.Value;
            BalEnt         := QrSumPQxCliDest.Value;
            GraGruX        := QrGraGruXControle.Value;
            COD_ITEM       := Geral.FF0(GraGruX);
            Grandeza       := Integer(TGrandezaUnidMed.gumPesoKG); // 2
            Pecas          := 0;
            AreaM2         := 0;
            PesoKg         := QrSumPQxPeso.Value;
            QTD            := PesoKg;
            Entidade       := QrSumPQxCliDest.Value;
            Tipo_Item      := QrGraGruXTipo_Item.Value;
            //COD_PART       := dmkPF.EscolhaDe2Str(FEmpresa = QrSumPQxCliDest.Value, '', Geral.FF0(QrSumPQxCliDest.Value));
            COD_PART       := dmkPF.EscolhaDe2Str(FEmpresa = Entidade, '', Geral.FF0(Entidade));
            if PCRegistro.ActivePageIndex = 1 then // H010
            begin
              UNID           := QrGraGruXSigla.Value;
              IND_PROP       := dmkPF.EscolhaDe2Str(FEmpresa = QrSumPQxCliDest.Value, '0', '2');
              TXT_COMPL      := '';
              COD_CTA        := UFinanceiro.MontaSPED_COD_CTA(RGNivPlaCta1.ItemIndex, EdGenPlaCta1.ValueVariant);
              VL_UNIT        := QrSumPQxValor.Value / QrSumPQxPeso.Value;
              VL_ITEM        := QrSumPQxValor.Value;
              VL_ITEM_IR     := QrSumPQxValor.Value;
              //
              InsereItemAtual_H010(BalID, BalNum, BalItm, BalEnt, COD_ITEM, UNID,
              IND_PROP, COD_PART, TXT_COMPL, COD_CTA, QTD, VL_UNIT, VL_ITEM,
              VL_ITEM_IR);
            end else
            if PCRegistro.ActivePageIndex = 2 then // K200
            begin
              DT_EST  := Geral.FDT(FData, 1);
              if FEmpresa = QrSumPQxCliDest.Value then
                IND_EST := '0'
              else
                IND_EST := '2';
              //
              InsereItemAtual_K200(BalTab, BalID, BalCod, BalNum, BalItm, DT_EST, COD_ITEM,
              QTD, IND_EST, COD_PART, GraGruX, Grandeza, Pecas, AreaM2, PesoKg,
              Entidade, Tipo_Item);
            end;
          end;
          //
          QrSumPQx.Next;
        end;
      finally
        PCRegistro.Visible := True;
        QrSumPQx.EnableControls;
      end;
}
      if NoUnid > 0 then
        Geral.MB_Aviso(Geral.FF0(NoUnid) +
        ' insumos n�o tem definido a sigla da unidade em seu cadastro de grade!' +
        slineBreak + 'Para estes insumos foi considerado a sigla "KG"!');
    end;
    if (
/////////////////////////////////  Bloco K  ////////////////////////////////////
      (PCRegistro.ActivePageIndex = 2)              // Bloco K
      and                                           // e
      (DmkPF.IntInConjunto2(2, CGRegistros.Value))   // Registro K280
////////////////////////////////////////////////////////////////////////////////
    ) then
    begin
      //K280 no K200 PQ  (K280 de entradas!)
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrUniPQx, Dmod.MyDB, [
      'SELECT pqx.DataX, pqx.DtCorrApo, ',
      'CONCAT(YEAR(pqx.DataX), LPAD(MONTH(pqx.DataX), 2, "0")) MovimCod,',
      'pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Insumo, ',
      'pqx.CliDest, pqx.Peso, pqx.Valor ',
      'FROM pqx pqx ',
      'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
      'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
      SQL_PeriodoPQ,
      //'AND pqx.DataX BETWEEN "2017-02-01" AND "2017-02-28 23:59:59" ',
      'AND pqx.DtCorrApo > "1900-01-01"',
      'AND pqx.DtCorrApo < "' + Geral.FDT(DiaIni, 1) + '"',
      'ORDER BY OrigemCodi, OrigemCtrl ',
      '']);
{
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQx, Dmod.MyDB, [
      'SELECT pqx.* ',
      'FROM pqx pqx ',
      'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
      'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
      SQL_PeriodoPQ,
      //'AND pqx.DataX BETWEEN "2017-02-01" AND "2017-02-28 23:59:59" ',
      'AND pqx.DtCorrApo > "1900-01-01"',
      'AND pqx.DtCorrApo < "' + Geral.FDT(DiaIni, 1) + '"',
      'ORDER BY OrigemCodi, OrigemCtrl ',
      '']);
      //Geral.MB_SQL(Self, QrPQx);
      QrPQx.First;
      while not QrPQx.Eof do
      begin
        ReopenGraGruX(QrPQxInsumo.Value);
        GraGruX        := QrGraGruXControle.Value;
        Grandeza       := Integer(TGrandezaUnidMed.gumPesoKG); // 2
        Pecas          := 0;
        AreaM2         := 0;
        PesoKg         := QrPQxPeso.Value;
        Qtde           := QrPQxPeso.Value;
        Entidade       := QrPQxCliDest.Value;
        Tipo_Item      := QrGraGruXTipo_Item.Value;
        COD_PART       := dmkPF.EscolhaDe2Str(FEmpresa = Entidade, '', Geral.FF0(Entidade));
        UnidMed        := QrGraGruXUnidMed.Value;
        if UnidMed = 0 then
        begin
          Nivel1  := QrSumPQxInsumo.Value;
          UnidMed := UnidMedKG;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
          'UnidMed'], ['Nivel1'], [
          UnidMed], [Nivel1], True);
          //
          ReopenGraGruX(QrSumPQxInsumo.Value);
        end;
        //
        OriBalID := TSPED_EFD_Bal.sebalEFD_K200;
        SPED_EFD_KndRegOrigem := TSPED_EFD_KndRegOrigem.sek2708oriIndef; // N�o tem para K200!
        OriESTSTabSorc := TEstqSPEDTabSorc.estsPQx;
        OriOrigemOpeProc := TOrigemOpeProc.oopBalanco;
        OriOrigemIDKnd := TOrigemIDKnd.oidk200Balanco;
        //
        SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsPQx,
        TipoPeriodoFiscal, FImporExpor,
        FAnoMes, FEmpresa, (*K100*)FRegPai, 'K200', 'K100',
        QrPQxDataX.Value, QrPQxDtCorrApo.Value,
        (*MovimID*)0, QrPQxOrigemCodi.Value, (*MovimCod*)0,
        QrPQxOrigemCtrl.Value, GraGruX, (*ClientMO*)QrPQxCliDest.Value,
        (*FornecMO*)FEmpresa, (*EntiSitio*)FEmpresa, slkPrefSitio,
        Qtde, OriBalID, SPED_EFD_KndRegOrigem, OriESTSTabSorc,
        OriOrigemOpeProc, OriOrigemIDKnd, OrigemSPEDEFDKnd, (*Agrega*)False,
        MeAviso(*, LinArq*));
        //
        QrPQx.Next;
        //
      end;
}
      QrUniPQx.First;
      while not QrUniPQx.Eof do
      begin
        ReopenGraGruX(QrUniPQxInsumo.Value);
        GraGruX        := QrGraGruXControle.Value;
        Grandeza       := Integer(TGrandezaUnidMed.gumPesoKG); // 2
        Pecas          := 0;
        AreaM2         := 0;
        PesoKg         := QrUniPQxPeso.Value;
        Qtde           := QrUniPQxPeso.Value;
        Entidade       := QrUniPQxCliDest.Value;
        Tipo_Item      := QrGraGruXTipo_Item.Value;
        COD_PART       := dmkPF.EscolhaDe2Str(FEmpresa = Entidade, '', Geral.FF0(Entidade));
        UnidMed        := QrGraGruXUnidMed.Value;
        if UnidMed = 0 then
        begin
          Nivel1  := QrSumPQxInsumo.Value;
          UnidMed := UnidMedKG;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
          'UnidMed'], ['Nivel1'], [
          UnidMed], [Nivel1], True);
          //
          ReopenGraGruX(QrSumPQxInsumo.Value);
        end;
        //
        OriBalID := TSPED_EFD_Bal.sebalEFD_K200;
        SPED_EFD_KndRegOrigem := TSPED_EFD_KndRegOrigem.sek2708oriIndef; // N�o tem para K200!
        OriESTSTabSorc := TEstqSPEDTabSorc.estsPQx;
        OriOrigemOpeProc := TOrigemOpeProc.oopBalanco;
        OriOrigemIDKnd := TOrigemIDKnd.oidk200Balanco;
        //
        SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsPQx,
        TipoPeriodoFiscal, FImporExpor,
        FAnoMes, FEmpresa, (*K100*)FRegPai, 'K200', 'K100',
        QrUniPQxDataX.Value, QrUniPQxDtCorrApo.Value,
        (*MovimID*)0, QrUniPQxOrigemCodi.Value, Geral.IMV(QrUniPQxMovimCod.Value),
        QrUniPQxOrigemCtrl.Value, GraGruX, (*ClientMO*)QrUniPQxCliDest.Value,
        (*FornecMO*)FEmpresa, (*EntiSitio*)FEmpresa, slkPrefSitio,
        Qtde, OriBalID, SPED_EFD_KndRegOrigem, OriESTSTabSorc,
        OriOrigemOpeProc, OriOrigemIDKnd, OrigemSPEDEFDKnd, (*Agrega*)False,
        MeAviso(*, LinArq*));
        //
        QrUniPQx.Next;
        //
      end;
     //
    end;
  end;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  // Couros VS
  if DmkPF.IntInConjunto2(2, CGImportar.Value) then
  begin
    Empresa        := FEmpresa;
    Filial         := DModG.ObtemFilialDeEntidade(FEmpresa);
    Terceiro       := 0;
    Ordem1         := 3; // IME-I
    Ordem2         := 0;
    Ordem3         := 0;
    Ordem4         := 0;
    Ordem5         := 0;
    Agrupa         := 0;
    DescrAgruNoItm := True;
    StqCenCad      := 0;
    ZeroNegat      := 0; // Somente positivos!
    NO_Empresa     := '';
    NO_StqCenCad   := '';
    NO_Terceiro    := '';
    SQL_PeriodoVS  := dmkPF.SQL_Periodo('WHERE vmi.DataHora ', DiaIni, DiaFim, True, True);
    SQL_PeriodoPQ  := dmkPF.SQL_Periodo('WHERE pqx.DataX ', DiaIni, DiaFim, True, True);
    EstoqueEm      := IncMonth(EncodeDate(Geral.IMV(CBAno.Text), CBMes.ItemIndex + 1, 1), 1) -1;
    DataRelativa   := EstoqueEm;
    MostraFrx      := False;
    //
/////////////////////////////////  Bloco H  ////////////////////////////////////
    if (PCRegistro.ActivePageIndex = 1) // H010
////////////////////////////////////////////////////////////////////////////////
    or (
/////////////////////////////////  Bloco K  ////////////////////////////////////
      (PCRegistro.ActivePageIndex = 2)              // Bloco K
      and                                           // e
      (DmkPF.IntInConjunto2(1, CGRegistros.Value))   // Registro K200
////////////////////////////////////////////////////////////////////////////////
    ) then
    begin
      if VS_PF.ImprimeEstoqueEm(Empresa, Filial, Terceiro, Ordem1, Ordem2,
      Ordem3, Ordem4, Ordem5, Agrupa, DescrAgruNoItm, StqCenCad, ZeroNegat,
      NO_Empresa, NO_StqCenCad,
      NO_Terceiro, DataRelativa, DataCompra, EmProcessoBH, DBG00GraGruY,
      DBG00GraGruX, DBG00CouNiv2, Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2,
      EstoqueEm, GraCusPrc,
      NFeIni, NFeFim, UsaSerie, Serie, MovimCod,
      LaAviso1, LaAviso2, False) then
      begin
        BalTab := TEstqSPEDTabSorc.estsVMI;
        BalCod := 0;// SUM(....
        // GraGru1 Sem UnidMed
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando unidades de medida! (A)');
        SQL_PSQ := 'AND gg1.UnidMed < 1 ';
        AbreSQLGraGruX(QrVMI, '', SQL_PSQ);
        QrVMI.First;
        while not QrVMI.Eof do
        begin
          Nivel1 := QrVMIGraGru1.Value;
          UnidMed := 0;
          case QrVMITipoEstq.Value of
            1(*m2*): UnidMed := UnidMedM2;
            2(*kg*): UnidMed := UnidMedKg;
            else
            begin
              Geral.MB_Erro('Importa��o abortada!' + sLineBreak +
              'ERRO! Tipo de movimenta��o de estoque diferente de kg e m2!' +
              'Tipo: ' + Geral.FF0(QrVMIGraGru1.Value));
              Erro := True;
            end;
          end;
          //
          if UnidMed > 0 then
          begin
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
            'UnidMed'], ['Nivel1'], [
            UnidMed], [Nivel1], True);
          end;
          QrVMI.Next;
        end;
        if Erro then Exit;
        //
        // GraGru1 com UnidMed sem rela��o com pe�a, kg ou m2
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando unidades de medida! (B)');
        SQL_PSQ := 'AND NOT (med.Grandeza IN (0,1,2,6,7)) ';
        AbreSQLGraGruX(QrVMI, '', SQL_PSQ);
        if QrVMI.RecordCount > 0 then
        begin
          Erro := True;
          Corda := Geral.FF0(QrVMIGraGru1.Value);
          QrVMI.Next;
          while not QrVMI.Eof do
          begin
            Corda := Corda + ',' + Geral.FF0(QrVMIGraGru1.Value);
            QrVMI.Next;
          end;
          Geral.MB_ERRO('Importa��o abortada!' + sLineBreak +
          'A unidade de medida dos produtos abaixo (Nivel 1) tem cadastrado unidades de medida inv�lidas!');
          Close;
        end;
        //
        // GraGru2 ou PrdGrupTip invalidos
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando Tipo_Item! ');
        SQL_PSQ :=  Geral.ATS([
          'AND NOT (',
          '  IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, pgt.Tipo_Item)',
          '  IN (' + CO_SPED_TIPO_ITEM_K200 + ')',
          ') ']);
        AbreSQLGraGruX(QrVMI, '', SQL_PSQ);
        //Geral.MB_SQL(Self, QrVMI);
        if QrVMI.RecordCount > 0 then
        begin
          if DBCheck.EscolheCodigoUniGrid(
          (*Aviso,*) '...',
          (* Titulo,*) 'Itens com "Tipo de Item" Indevido',
          (*Prompt*)'Importa��o abortada!!! Itens com Tipo de item (Tipo_Item do Tipo de produto ou n�vel 2) indevido',
          DsVMI, False, False) then ;
          //
          Exit;
        end;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando produtos de terceiros em m�os de terceiros!');
        SQL_PSQ := 'AND (EntiSitio <> -11 AND ClientMO <> -11)';
        AbreSQLGraGruX(QrVMI, '', SQL_PSQ);
        if QrVMI.RecordCount > 0 then
        begin
          Erro := True;
          //
          AbreSQLGraGruX(QrVMI2, 'SELECT vmi.*', SQL_PSQ);

          Corda := Geral.FF0(QrVMI2IMEI.Value);
          QrVMI2.Next;
          while not QrVMI2.Eof do
          begin
            Corda := Corda + ',' + Geral.FF0(QrVMI2IMEI.Value);
            QrVMI2.Next;
          end;
          Geral.MB_ERRO('Importa��o abortada!' + sLineBreak +
            'Os IME-Is abaixo s�o de terceiros em m�os de terceiros!' +
            sLineBreak + Corda);
          Close;
        end;
        //
        if Erro then Exit;
        //

        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando produtos com grandeza inesperada!');
        SQL_PSQ := Geral.ATS([
        'AND xco.Grandeza=-1 ',
        'AND ( ',
        'IF((vmi.GraGruY<2048 OR ',
        'vmi.CouNiv2<>1) AND vmi.SdoVrtPeso > 0, 2, ',
        'IF(vmi.CouNiv2=1 AND vmi.SdoVrtArM2 > 0, 1, -1)) <> med.Grandeza ',
        '  ) ']);
        AbreSQLGraGruX(QrVMI, '', SQL_PSQ);
        if QrVMI.RecordCount > 0 then
        begin
          Erro := True;
          //
          SQL_PSQ := Geral.ATS([
            SQL_PSQ,
            '; SELECT * FROM _couros_err_grandeza_; ']);
          SQL_ERR := Geral.ATS([
            'DROP TABLE IF EXISTS _couros_err_grandeza_; ',
            'CREATE TABLE _couros_err_grandeza_ ',
            'SELECT vmi.GraGru1 Nivel1, vmi.GraGruX Reduzido,  ',
            'CONCAT(gg1.Nome, ',
            'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
            'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
            'NO_GG1, med.Sigla, med.Grandeza, xco.Grandeza xco_Grandeza, ',
            'CAST(IF(xco.Grandeza > 0, xco.Grandeza,   ',
            '  IF((ggx.GraGruY<2048 OR   ',
            '  xco.CouNiv2<>1), IF(gg1.Nivel2<>-4, 2, -1),   ',
            '  IF(xco.CouNiv2=1, 1, -1))) AS DECIMAL(11,0)) dif_Grandeza  ',
            '']);
          AbreSQLGraGruX(DfModAppGraG1.QrErrGrandz, SQL_ERR, SQL_PSQ);
          MyObjects.frxDefineDataSets(DfModAppGraG1.frxErrGrandz, [
            DModG.frxDsDono,
            DfModAppGraG1.frxDsErrGrandz
          ]);
          MyObjects.frxMostra(DfModAppGraG1.frxErrGrandz, 'Grandezas inesperadas');
          Close;
        end;
        //
        if Erro then Exit;
        //

        // Importa��o
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Importando');
        SQL_PSQ := '';
        AbreSQLGraGruX(QrVMI, '', SQL_PSQ);
        //Geral.MB_SQL(Self, QrVMI);

        PB1.Position := 0;
        PB1.Max := QrVMI.RecordCount;
        QrVMI.DisableControls;
        PCRegistro.Visible := False;
        try
          while not QrVMI.Eof do
          begin
            //if QrVMIGraGruX.Value = 1400 then
              //Geral.MB_Aviso('1400');
            MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
            //
            Grandeza := QrVMIGrandeza.Value;
            Pecas    := QrVMISdoVrtPeca.Value;
            AreaM2   := QrVMISdoVrtArM2.Value;
            PesoKg   := QrVMISdoVrtPeso.Value;
            case TGrandezaUnidMed(Grandeza) of
              (*0*)gumPeca:
              begin
                Tipo   := TGrandezaUnidMed.gumPeca;
                Fator  := 1.0;
                MyQtd  := QrVMISdoVrtPeca.Value;
              end;
              (*1*)gumAreaM2:
              begin
                Tipo   := TGrandezaUnidMed.gumAreaM2;
                Fator  := 1.0;
                MyQtd  := QrVMISdoVrtArM2.Value;
              end;
              (*2*)gumPesoKG:
              begin
                Tipo   := TGrandezaUnidMed.gumPesoKG;
                Fator  := 1.0;
                MyQtd  := QrVMISdoVrtPeso.Value;
              end;
              //(*3*)gumVolumeM3=3,
              //(*4*)gumLinearM=4,
              //(*5*)gumOutros=5,
              (*6*)gumAreaP2:
              begin
                Tipo   := TGrandezaUnidMed.gumAreaM2;
                Fator  := 10.76391;
                MyQtd  := QrVMISdoVrtArM2.Value;
              end;
              (*7*)gumPesoTon:
              begin
                Tipo   := TGrandezaUnidMed.gumPesoKG;
                Fator  := 0.001;
                MyQtd  := QrVMISdoVrtPeso.Value;
              end;
              else
              begin
                Tipo := TGrandezaUnidMed.gumOutros;
                Fator := 0;
                MyQtd := 0;
              end;
            end;
            if Tipo = TGrandezaUnidMed.gumOutros then
            begin
              Geral.MB_Erro('Grandeza indefinida!');
              Exit;
            end;
            case Tipo of
              gumPeca    : MyBas := QrVMIPecas.Value;
              gumAreaM2  : MyBas := QrVMIAreaM2.Value;
              gumPesoKg  : MyBas := QrVMIPesoKg.Value;
              else         MyBas := 0;
            end;
            if MyBas > 0 then
              MyVlU := QrVMIValorT.Value / MyQtd
            else
              MyVlU := 0;
            //
            case Tipo of
              gumPeca    : VL_ITEM := MyVlU * QrVMISdoVrtPeca.Value;
              gumAreaM2  : VL_ITEM := MyVlU * QrVMISdoVrtArM2.Value;
              gumPesoKg  : VL_ITEM := MyVlU * QrVMISdoVrtPeso.Value;
              else         VL_ITEM := 0;
            end;
            BalID          := Integer(TSPED_EFD_Bal.sebalVSMovItX);
            BalNum         := FAnoMes;
            //BalItm         := QrVMIGraGruX.Value;
            BalItm         := QrVMIIMEI.Value;
            BalEnt         := QrVMIEmpresa.Value;
            GraGruX        := QrVMIGraGruX.Value;
            COD_ITEM       := Geral.FF0(GraGruX);
            QTD            := MyQtd * Fator;
            Tipo_Item      := QrVMITipo_Item.Value;
            // Somente se for invent�rio!
            case PCRegistro.ActivePageIndex of
              0, 2: ; // Nada!!
              1:
              begin
                if MyVlU <= 0 then
                begin
                  //Geral.MB_Erro('Valor unit�rio indefinido!');
                  MeAviso.Lines.Add('COD_ITEM ' + COD_ITEM + ' - ' +
                    QrVMINO_PRD_TAM_COR.Value + ' > Valor unit�rio indefinido!');
                  //Exit;
                end;
                if VL_ITEM <= 0 then
                begin
                  //Geral.MB_Erro('Valor total do item indefinido!');
                  MeAviso.Lines.Add('COD_ITEM ' + COD_ITEM + ' - ' +
                    QrVMINO_PRD_TAM_COR.Value + ' > Valor total do item indefinido!');
                  //Exit;
                end;
              end;
              else Geral.MB_Aviso('PageIndex n�o implementado para aviso de valores zerados!');
            end;
            if PCRegistro.ActivePageIndex = 1 then // H010
            begin
              Entidade       := QrVMIEmpresa.Value;
              COD_PART       := dmkPF.EscolhaDe2Str(FEmpresa = Entidade, '', Geral.FF0(Entidade));
              UNID           := QrVMISigla.Value;
              IND_PROP       := dmkPF.EscolhaDe2Str(FEmpresa = QrVMIEmpresa.Value, '0', '2');
              TXT_COMPL      := '';
              COD_CTA        := UFinanceiro.MontaSPED_COD_CTA(RGNivPlaCta1.ItemIndex, EdGenPlaCta1.ValueVariant);
              VL_UNIT        := MyVlU;
              //VL_ITEM        := Acima
              VL_ITEM_IR     := VL_ITEM;
              //
              InsereItemAtual_H010(BalID, BalNum, BalItm, BalEnt, COD_ITEM, UNID,
              IND_PROP, COD_PART, TXT_COMPL, COD_CTA, QTD, VL_UNIT, VL_ITEM,
              VL_ITEM_IR);
            end else
            if PCRegistro.ActivePageIndex = 2 then // K200
            begin
              DT_EST   := Geral.FDT(FData, 1);
              IND_EST  := '-';
              COD_PART := '0';
              Entidade := 0;
              //
{
              // o couro pertence a empresa declarante
              if QrVMIClientMO.Value  = FEmpresa then
              begin
                if QrVMIEntiSitio.Value = FEmpresa then
                //Da empresa em seu poder
                begin
                  IND_EST  := Geral.FF0(Integer(spedipiePropInfPoderInf));//=0,
                  COD_PART := '';
                  Entidade := FEmpresa;
                end else
                //Da empresa com terceiros
                begin
                  IND_EST  := Geral.FF0(Integer(spedipiePropInfPoderTer)); //=1,
                  COD_PART := Geral.FF0(QrVMIEntiSitio.Value);
                  Entidade := QrVMIEntiSitio.Value;
                end;
              end else
              // o couro N�O pertence a empresa declarante
              begin
                if QrVMIEntiSitio.Value = FEmpresa then
                //De terceiros c/ a empresa
                begin
                  IND_EST := Geral.FF0(Integer(spedipiePropTerPoderInf)); //=2,
                  // Errado! Numc apode ser a Empresa!
                  //COD_PART := Geral.FF0(QrVMIEntiSitio.Value);
                  //Entidade := QrVMIEntiSitio.Value;
                  COD_PART := Geral.FF0(QrVMIClientMO.Value);
                  Entidade := QrVMIClientMO.Value;
                end else
                // De terceiros com terceiros
                begin
                  IND_EST  := '-';
                  COD_PART := '0';
                  Entidade := QrVMIEntiSitio.Value;
                  //
                  MeAviso.Lines.Add('COD_ITEM: ' + COD_ITEM +
                  ' De terceiros com terceiros! COD_PART: ' + Geral.FF0(QrVMIEntiSitio.Value));
                end;
              end;
}
              Aviso := SPED_EFD_PF.ObtemIND_ESTeCOD_PART(FEmpresa,
                QrVMIClientMO.Value(*ClientMO*),
                QrVMIEntiSitio.Value(*Local*), GraGruX,
                IND_EST, COD_PART, Entidade);
              //
              if Aviso <> '' then
              begin
                //testar
                MeAviso.Lines.Add(Aviso + ' CodItem: ' + Geral.FF0(BalItm));
              end;
              //
              InsereItemAtual_K200(BalTab, BalID, BalCod, BalNum, BalItm,
              DT_EST, COD_ITEM, QTD, IND_EST, COD_PART, GraGruX,
              Grandeza, Pecas, AreaM2, PesoKg, Entidade, Tipo_Item);
            end;
            //
            QrVMI.Next;
          end;
        finally
          PCRegistro.Visible := True;
          QrVMI.EnableControls;
        end;
      end;
    end;
    if (
/////////////////////////////////  Bloco K  ////////////////////////////////////
      (PCRegistro.ActivePageIndex = 2)              // Bloco K
      and                                           // e
      (DmkPF.IntInConjunto2(2, CGRegistros.Value))   // Registro K280
////////////////////////////////////////////////////////////////////////////////
    ) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrCA_VS, Dmod.MyDB, [
      'SELECT scc.EntiSitio, vmi.DataHora, MONTH(vmi.DtCorrApo) Mes, ',
      'YEAR(vmi.DtCorrApo) Ano, vmi.MovimID, vmi.MovimNiv, vmi.Codigo, ',
      'vmi.MovimCod, vmi.Controle, vmi.ClientMO, vmi.FornecMO, vmi.GraGruX, ',
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, med.Grandeza, vmi.DtCorrApo ',
      'FROM vsmovits vmi ',
      'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
      'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
      'LEFT JOIN gragrux ggx ON ggx.GraGru1=vmi.GraGruX ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
      SQL_PeriodoVS,
      //'WHERE vmi.DataHora BETWEEN "2017-02-01" AND "2017-02-28 23:59:59"',
      'AND vmi.Empresa=' + Geral.FF0(Empresa),
      'AND vmi.DtCorrApo > "1900-01-01"',
      'AND vmi.DtCorrApo < "' + Geral.FDT(DiaIni, 1) + '"',
      'AND MovimID IN (' + CO_ALL_CODS_INN_NF_SPED_VS + ')',
      //'GROUP BY EntiSitio, Mes, Ano, GraGruX',
      '']);
      //Geral.MB_SQL(Self, QrCA_VS);
      //PeriodoFim := dmkPF.PeriodoEncode(Geral.IMV(CBAno.Text), CBMes.ItemIndex + 1) - 1;
      QrCA_VS.First;
      while not QrCA_VS.Eof do
      begin
        (*PeriodoIni := dmkPF.PeriodoEncode(QrCA_VSAno.Value, QrCA_VSMes.Value);
        for I := PeriodoIni to PeriodoFim do
        begin
          Ano := I div 100;
          Mes := I mod 100;
          Data := IncMonth(EncodeDate(Ano, Mes, 1), 1) -1;
        *)
        Grandeza := QrCA_VSGrandeza.Value;
        Qtde     := 0;
        case TGrandezaUnidMed(Grandeza) of
          (*0*)gumPeca:    Qtde := QrCA_VSPecas.Value;
          (*1*)gumAreaM2:  Qtde := QrCA_VSAreaM2.Value;
          (*2*)gumPesoKG:  Qtde := QrCA_VSPesoKg.Value;
          else Geral.MB_Aviso('"Grandeza" inv�lida para o reduzido ' +
          Geral.FF0(GraGruX));
        end;
        OriBalID := TSPED_EFD_Bal.sebalEFD_K200;
        SPED_EFD_KndRegOrigem := TSPED_EFD_KndRegOrigem.sek2708oriIndef; // N�o tem para K200!
        OriESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
        OriOrigemOpeProc := TOrigemOpeProc.oopBalanco;
        OriOrigemIDKnd := TOrigemIDKnd.oidk200Balanco;
        //
        SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
        TipoPeriodoFiscal, FImporExpor,
        FAnoMes, FEmpresa, (*K100*)FRegPai, 'K200', 'K100',
        QrCA_VSDataHora.Value, QrCA_VSDtCorrApo.Value,
        QrCA_VSMovimID.Value, QrCA_VSCodigo.Value, QrCA_VSMovimCod.Value,
        QrCA_VSControle.Value, QrCA_VSGraGruX.Value, QrCA_VSClientMO.Value,
        QrCA_VSFornecMO.Value, QrCA_VSEntiSitio.Value, slkPrefSitio,
        Qtde, OriBalID, SPED_EFD_KndRegOrigem, OriESTSTabSorc,
        OriOrigemOpeProc, OriOrigemIDKnd, OrigemSPEDEFDKnd, (*Agrega*)False,
        MeAviso(*, LinArq*));
        //
        QrCA_VS.Next;
      end;
    end;
  end;
  if Trim(MeAviso.Text) <> '' then
    Geral.MB_Aviso(MeAviso.Text);
  Close;
end;

procedure TFmSpedEfdIcmsIpiBalancos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSpedEfdIcmsIpiBalancos.CGImportarClick(Sender: TObject);
begin
  GBImportar1.Visible := DmkPF.IntInConjunto2(1, CGImportar.Value);
  GBImportar2.Visible := DmkPF.IntInConjunto2(2, CGImportar.Value);
end;

function TFmSpedEfdIcmsIpiBalancos.ErrUnidMedUsoConsumo(
  DataBal: String): Boolean;
begin
  Result := DfModAppGraG1.GrandesasInconsistentes([
  'SELECT gg1.*, pqx.Insumo Nivel1, ggx.Controle Reduzido,  ',
  'gg1.Nome NO_GG1, med.Sigla, med.Grandeza,  ',
  'xco.Grandeza xco_Grandeza,   ',
  'CAST(IF(xco.Grandeza > 0, xco.Grandeza,   ',
  '  IF((ggx.GraGruY<2048 OR   ',
  '  xco.CouNiv2<>1), IF(gg1.Nivel2<>-4, 2, -1),   ',
  '  IF(xco.CouNiv2=1, 1, -1))) AS DECIMAL(11,0)) dif_Grandeza  ',
  'FROM pqx pqx  ',
  'LEFT JOIN gragrux ggx ON ggx.GraGru1=pqx.Insumo  ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed  ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle   ',
  'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1   ',
  'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2   ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0000),
  'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
  'AND pqx.DataX="' + DataBal + '" ',
  'AND gg1.Nivel2<>-4 ',
  'AND med.Grandeza<>2   ',
  'GROUP BY pqx.Insumo, pqx.CliDest   ',
  ' ',
  'UNION ',
  ' ',
  'SELECT gg1.*, pqx.Insumo Nivel1, ggx.Controle Reduzido,  ',
  'gg1.Nome NO_GG1, med.Sigla, med.Grandeza,  ',
  'xco.Grandeza xco_Grandeza,   ',
  'CAST(IF(xco.Grandeza > 0, xco.Grandeza,   ',
  '  IF((ggx.GraGruY<2048 OR   ',
  '  xco.CouNiv2<>1), IF(gg1.Nivel2=-4, 0, -1),   ',
  '  IF(xco.CouNiv2=1, 1, -1))) AS DECIMAL(11,0)) dif_Grandeza  ',
  'FROM pqx pqx  ',
  'LEFT JOIN gragrux ggx ON ggx.GraGru1=pqx.Insumo  ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed  ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle   ',
  'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1   ',
  'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2   ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0000),
  'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
  'AND pqx.DataX="' + DataBal + '" ',
  'AND gg1.Nivel2=-4 ',
  'AND med.Grandeza<>0   ',
  'GROUP BY pqx.Insumo, pqx.CliDest   ',

  ''], Dmod.MyDB);
end;

procedure TFmSpedEfdIcmsIpiBalancos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSpedEfdIcmsIpiBalancos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PCRegistro.ActivePageIndex := 0;
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  MeAviso.Lines.Clear;
  CGRegistros.SetMaxValue;
end;

procedure TFmSpedEfdIcmsIpiBalancos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSpedEfdIcmsIpiBalancos.ImportaPQxSumH010(Bal_TXT: String;
  UnidMedKG: Integer);
var
  DT_EST, COD_PART, COD_ITEM, UNID, IND_PROP, TXT_COMPL, COD_CTA: String;
  UnidMed, Nivel1, GraGruX, BalCod, BalID, BalNum, BalItm, BalEnt, Grandeza,
  Tipo_Item, Entidade: Integer;
  BalTab: TEstqSPEDTabSorc;
  Pecas, AreaM2, PesoKg, QTD, VL_UNIT, VL_ITEM, VL_ITEM_IR: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPQx, Dmod.MyDB, [
  'SELECT pqx.Insumo, pqx.CliDest,',
  'SUM(pqx.Peso) Peso, SUM(pqx.Valor) Valor ',
  'FROM pqx pqx',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0000),
  'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
  'AND pqx.DataX="' + Bal_TXT + '" ',
  'GROUP BY pqx.Insumo, pqx.CliDest ',
  '']);
  //Geral.MB_SQL(Self, QrPQx);
  DT_EST  := Geral.FDT(FData, 2);
  if MyObjects.FIC(QrSumPQx.RecordCount = 0, nil,
  'N�o existe balan�o de uso e consumo no dia ' + DT_EST) then Exit;
  QrSumPQx.First;
  PB1.Position := 0;
  PB1.Max := QrSumPQx.RecordCount;
  QrSumPQx.DisableControls;
  PCRegistro.Visible := False;
  try
    while not QrSumPQx.Eof do
    // ver
    //Tipo_Item <> CO_SPED_TIPO_ITEM_K200
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //Reabre QrGraGruX
      if QrSumPQxPeso.Value > 0 then
      begin
        ReopenGraGruX(QrSumPQxInsumo.Value);
        UnidMed := QrGraGruXUnidMed.Value;
        if UnidMed = 0 then
        begin
          Nivel1  := QrSumPQxInsumo.Value;
          UnidMed := UnidMedKG;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
          'UnidMed'], ['Nivel1'], [
          UnidMed], [Nivel1], True);
          //
          ReopenGraGruX(QrSumPQxInsumo.Value);
        end;
        //
        BalTab         := TEstqSPEDTabSorc.estsPQx;
        BalCod         := 0; ///SUM(....
        BalID          := Integer(TSPED_EFD_Bal.sebalPQx);

        BalNum         := FAnoMes;
        BalItm         := QrSumPQxInsumo.Value;
        BalEnt         := QrSumPQxCliDest.Value;
        GraGruX        := QrGraGruXControle.Value;
        COD_ITEM       := Geral.FF0(GraGruX);
        Grandeza       := Integer(TGrandezaUnidMed.gumPesoKG); // 2
        Pecas          := 0;
        AreaM2         := 0;
        PesoKg         := QrSumPQxPeso.Value;
        QTD            := PesoKg;
        Entidade       := QrSumPQxCliDest.Value;
        Tipo_Item      := QrGraGruXTipo_Item.Value;
        //COD_PART       := dmkPF.EscolhaDe2Str(FEmpresa = QrSumPQxCliDest.Value, '', Geral.FF0(QrSumPQxCliDest.Value));
        COD_PART       := dmkPF.EscolhaDe2Str(FEmpresa = Entidade, '', Geral.FF0(Entidade));
        if PCRegistro.ActivePageIndex = 1 then // H010
        begin
          UNID           := QrGraGruXSigla.Value;
          IND_PROP       := dmkPF.EscolhaDe2Str(FEmpresa = QrSumPQxCliDest.Value, '0', '2');
          TXT_COMPL      := '';
          COD_CTA        := UFinanceiro.MontaSPED_COD_CTA(RGNivPlaCta1.ItemIndex, EdGenPlaCta1.ValueVariant);
          VL_UNIT        := QrSumPQxValor.Value / QrSumPQxPeso.Value;
          VL_ITEM        := QrSumPQxValor.Value;
          VL_ITEM_IR     := QrSumPQxValor.Value;
          //
          InsereItemAtual_H010(BalID, BalNum, BalItm, BalEnt, COD_ITEM, UNID,
          IND_PROP, COD_PART, TXT_COMPL, COD_CTA, QTD, VL_UNIT, VL_ITEM,
          VL_ITEM_IR);
        end else
(*
        if PCRegistro.ActivePageIndex = 2 then // K200
        begin
          DT_EST  := Geral.FDT(FData, 1);
          if FEmpresa = QrSumPQxCliDest.Value then
            IND_EST := '0'
          else
            IND_EST := '2';
          //
          InsereItemAtual_K200(BalTab, BalID, BalCod, BalNum, BalItm, DT_EST, COD_ITEM,
          QTD, IND_EST, COD_PART, GraGruX, Grandeza, Pecas, AreaM2, PesoKg,
          Entidade, Tipo_Item);
        end;
*)
      end;
      //
      QrSumPQx.Next;
    end;
  finally
    PCRegistro.Visible := True;
    QrSumPQx.EnableControls;
  end;
end;

procedure TFmSpedEfdIcmsIpiBalancos.ImportaPQxUniK200(Bal_TXT: String;
  UnidMedKG: Integer);
var
  DT_EST, COD_PART, COD_ITEM, UNID, IND_PROP, IND_EST: String;
  UnidMed, Nivel1, GraGruX, BalCod, BalID, BalNum, BalItm, BalEnt, Grandeza,
  Tipo_Item, Entidade: Integer;
  BalTab: TEstqSPEDTabSorc;
  Pecas, AreaM2, PesoKg, QTD, VL_UNIT, VL_ITEM, VL_ITEM_IR: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUniPQx, Dmod.MyDB, [
  'SELECT DataX, DtCorrApo, ',
  'CONCAT(YEAR(DataX), LPAD(MONTH(DataX), 2, "0")) MovimCod,',
  'pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Insumo, ',
  'pqx.CliDest, pqx.Peso, pqx.Valor ',
  'FROM pqx pqx ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0000),
  'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
  'AND pqx.DataX="' + Bal_TXT + '" ',
  'ORDER BY pqx.Insumo, pqx.CliDest ',
  '']);
  //Geral.MB_SQL(Self, QrPQx);
  DT_EST  := Geral.FDT(FData, 2);
  if MyObjects.FIC(QrUniPQx.RecordCount = 0, nil,
  'N�o existe balan�o de uso e consumo no dia ' + DT_EST) then Exit;
  QrUniPQx.First;
  PB1.Position := 0;
  PB1.Max := QrUniPQx.RecordCount;
  QrUniPQx.DisableControls;
  PCRegistro.Visible := False;
  try
    while not QrUniPQx.Eof do
    // ver
    //Tipo_Item <> CO_SPED_TIPO_ITEM_K200
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //Reabre QrGraGruX
      if QrUniPQxPeso.Value > 0 then
      begin
        ReopenGraGruX(QrUniPQxInsumo.Value);
        UnidMed := QrGraGruXUnidMed.Value;
        if UnidMed = 0 then
        begin
          Nivel1  := QrUniPQxInsumo.Value;
          UnidMed := UnidMedKG;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
          'UnidMed'], ['Nivel1'], [
          UnidMed], [Nivel1], True);
          //
          ReopenGraGruX(QrUniPQxInsumo.Value);
        end;
        //
        BalTab         := TEstqSPEDTabSorc.estsPQx;
        BalCod         := QrUniPQxOrigemCodi.Value;
        BalID          := Integer(TSPED_EFD_Bal.sebalPQx);

        BalNum         := Geral.IMV(QrUniPQxMovimCod.Value);
        BalItm         := QrUniPQxOrigemCtrl.Value;
        BalEnt         := QrUniPQxCliDest.Value;
        GraGruX        := QrGraGruXControle.Value;
        COD_ITEM       := Geral.FF0(GraGruX);
        Grandeza       := Integer(TGrandezaUnidMed.gumPesoKG); // 2
        Pecas          := 0;
        AreaM2         := 0;
        PesoKg         := QrUniPQxPeso.Value;
        QTD            := PesoKg;
        Entidade       := QrUniPQxCliDest.Value;
        Tipo_Item      := QrGraGruXTipo_Item.Value;
        //COD_PART       := dmkPF.EscolhaDe2Str(FEmpresa = QrUniPQxCliDest.Value, '', Geral.FF0(QrUniPQxCliDest.Value));
        COD_PART       := dmkPF.EscolhaDe2Str(FEmpresa = Entidade, '', Geral.FF0(Entidade));
(*
        if PCRegistro.ActivePageIndex = 1 then // H010
        begin
          UNID           := QrGraGruXSigla.Value;
          IND_PROP       := dmkPF.EscolhaDe2Str(FEmpresa = QrUniPQxCliDest.Value, '0', '2');
          TXT_COMPL      := '';
          COD_CTA        := UFinanceiro.MontaSPED_COD_CTA(RGNivPlaCta1.ItemIndex, EdGenPlaCta1.ValueVariant);
          VL_UNIT        := QrUniPQxValor.Value / QrUniPQxPeso.Value;
          VL_ITEM        := QrUniPQxValor.Value;
          VL_ITEM_IR     := QrUniPQxValor.Value;
          //
          InsereItemAtual_H010(BalID, BalNum, BalItm, BalEnt, COD_ITEM, UNID,
          IND_PROP, COD_PART, TXT_COMPL, COD_CTA, QTD, VL_UNIT, VL_ITEM,
          VL_ITEM_IR);
        end else
*)
        if PCRegistro.ActivePageIndex = 2 then // K200
        begin
          DT_EST  := Geral.FDT(FData, 1);
          if FEmpresa = QrUniPQxCliDest.Value then
            IND_EST := '0'
          else
            IND_EST := '2';
          //
          InsereItemAtual_K200(BalTab, BalID, BalCod, BalNum, BalItm, DT_EST, COD_ITEM,
          QTD, IND_EST, COD_PART, GraGruX, Grandeza, Pecas, AreaM2, PesoKg,
          Entidade, Tipo_Item);
        end;
      end;
      //
      QrUniPQx.Next;
    end;
  finally
    PCRegistro.Visible := True;
    QrUniPQx.EnableControls;
  end;
end;

function TFmSpedEfdIcmsIpiBalancos.InsereItemAtual_H010(BalID, BalNum, BalItm,
  BalEnt: Integer; COD_ITEM, UNID, IND_PROP, COD_PART, TXT_COMPL, COD_CTA:
  String; QTD, VL_UNIT, VL_ITEM, VL_ITEM_IR: Double): Boolean;
const
  REG = 'H010';
var
  ImporExpor, AnoMes, Empresa, H005, LinArq: Integer;
  SQLType: TSQLType;
begin
  Result := False;
  //SQLType        := ImgTipo.SQLType?;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  H005           := FRegPai;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrX999, Dmod.MyDB, [
  'SELECT * ',
  //'FROM efd_h010 ',
  'FROM efdicmsipih010 ',
  'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
  'AND AnoMes=' + Geral.FF0(FAnoMes),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND H005=' + Geral.FF0(H005),
  'AND BalID=' + Geral.FF0(BalID),
  'AND BalNum=' + Geral.FF0(BalNum),
  'AND BalItm=' + Geral.FF0(BalItm),
  'AND BalEnt=' + Geral.FF0(BalEnt),
  '']);
  if QrX999.RecordCount > 0 then
  begin
    LinArq  := QrX999.FieldByName('LinArq').AsInteger;
    SQLType := stUpd;
  end else
  begin
    LinArq := 0;
    LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdIcmsIpih010', 'LinArq', [
    (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
    stIns, LinArq, siPositivo, nil);
    //
    SQLType := stIns;
  end;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdIcmsIpih010', False, [
  'H005', 'REG', 'COD_ITEM',
  'UNID', 'QTD', 'VL_UNIT',
  'VL_ITEM', 'IND_PROP', 'COD_PART',
  'TXT_COMPL', 'COD_CTA', 'VL_ITEM_IR',
  'BalID', 'BalNum', 'BalItm', 'BalEnt'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  H005, REG, COD_ITEM,
  UNID, QTD, VL_UNIT,
  VL_ITEM, IND_PROP, COD_PART,
  TXT_COMPL, COD_CTA, VL_ITEM_IR,
  BalID, BalNum, BalItm, BalEnt], [
  ImporExpor, AnoMes, Empresa, LinArq], True);
end;

function TFmSpedEfdIcmsIpiBalancos.InsereItemAtual_K200(BalTab:
  TEstqSPEDTabSorc; BalID, BalCod, BalNum,
  BalItm: Integer; DT_EST, COD_ITEM: String; var QTD: Double;
  IND_EST, COD_PART: String; GraGruX, Grandeza: Integer;
  var Pecas, AreaM2, PesoKg: Double; Entidade, Tipo_Item: Integer): Boolean;
const
  ID_SEK = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
var
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq1: Integer;
  SQLType: TSQLType;
begin
  Result := False;
  SQLType        := stNil;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  PeriApu        := FRegPai;
  //
  KndTab         := Integer(BalTab);
  KndCod         := BalCod;
  KndNSU         := BalNum;
  KndItm         := BalItm;
  KndAID         := Integer(BalID);
  KndNiv         := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(QrX999, Dmod.MyDB, [
  'SELECT * ',
  'FROM efdIcmsIpik200 ',
  'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
  'AND AnoMes=' + Geral.FF0(FAnoMes),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND PeriApu=' + Geral.FF0(PeriApu),
  'AND KndTab=' + Geral.FF0(KndTab),
  'AND KndCod=' + Geral.FF0(KndCod),
  'AND KndNSU=' + Geral.FF0(KndNSU),
  'AND KndItm=' + Geral.FF0(KndItm),
  '']);
  if QrX999.RecordCount > 0 then
  begin
    MeAviso.Lines.Add('Item j� importado > ID: ' + Geral.FF0(KndItm) +
    ' Reduzido: ' + Geral.FF0(GraGruX));
    Exit;
  end else
  begin
{    LinArq := 0;
    LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdIcmsIpik200', 'LinArq', [
    (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
    stIns, LinArq, siPositivo, nil);
}
    IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik200', 'IDSeq1', [
    'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
    ImporExpor, AnoMes, Empresa, PeriApu],
    stIns, IDSeq1, siPositivo, nil);
    //
    SQLType := stIns;
  end;
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdIcmsIpik200', False, [
  'KndAID', 'KndNiv', 'DT_EST',
  'COD_ITEM', 'QTD', 'IND_EST',
  'COD_PART', 'GraGruX',
  'Grandeza', 'Pecas', 'AreaM2',
  'PesoKg', 'Entidade', 'Tipo_Item',
  'IDSeq1', 'ID_SEK'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'], [
  KndAID, KndNiv, DT_EST,
  COD_ITEM, QTD, IND_EST,
  COD_PART, GraGruX,
  Grandeza, Pecas, AreaM2,
  PesoKg, Entidade, Tipo_Item,
  IDSeq1, ID_SEK], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True);
end;

procedure TFmSpedEfdIcmsIpiBalancos.PCRegistroChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  AllowChange := False;
end;

function TFmSpedEfdIcmsIpiBalancos.ReopenGraGruX(Nivel1: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item, ggx.Controle, gg1.UnidMed, med.Sigla ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gratamcad  gtc ON gtc.Codigo=gti.Codigo ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip ',
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
  'WHERE ggx.GraGru1=' + Geral.FF0(Nivel1),
  'AND NOT gti.Codigo IS NULL ',
  'ORDER BY ggx.Controle ',
  '']);
  if QrGraGruX.RecordCount = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
    'SELECT IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
    'pgt.Tipo_Item) Tipo_Item, ggx.Controle, gg1.UnidMed, med.Sigla ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    //'LEFT JOIN gratamcad  gtc ON gtc.Codigo=gti.Codigo ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2 ',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
    'WHERE ggx.GraGru1=' + Geral.FF0(Nivel1),
    //'AND NOT gti.Codigo IS NULL ',
    'ORDER BY ggx.Controle ',
    '']);
  end;
end;

procedure TFmSpedEfdIcmsIpiBalancos.RGNivPlaCta1Click(Sender: TObject);
begin
  DModFin.ReopenNivelSel(QrNivelSel1, EdGenPlaCta1, CBGenPlaCta1, RGNivPlaCta1);
end;

procedure TFmSpedEfdIcmsIpiBalancos.RGNivPlaCta2Click(Sender: TObject);
begin
  DModFin.ReopenNivelSel(QrNivelSel2, EdGenPlaCta2, CBGenPlaCta2, RGNivPlaCta2);
end;

end.
