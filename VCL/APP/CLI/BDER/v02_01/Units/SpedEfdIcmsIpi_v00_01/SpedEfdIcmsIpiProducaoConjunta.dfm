object FmSpedEfdIcmsIpiProducaoConjunta: TFmSpedEfdIcmsIpiProducaoConjunta
  Left = 0
  Top = 0
  Caption = 'FmSpedEfdIcmsIpiProducaoConjunta'
  ClientHeight = 806
  ClientWidth = 894
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object QrDatas: TmySQLQuery
    Database = Dmod.MyDB
    Left = 40
    Top = 4
    object QrDatasMinDH: TDateTimeField
      FieldName = 'MinDH'
    end
    object QrDatasMaxDH: TDateTimeField
      FieldName = 'MaxDH'
    end
  end
  object QrOri: TmySQLQuery
    Database = Dmod.MyDB
    Left = 40
    Top = 52
  end
  object QrMae: TmySQLQuery
    Database = Dmod.MyDB
    Left = 40
    Top = 100
  end
  object QrMC: TmySQLQuery
    Database = Dmod.MyDB
    Left = 140
    Top = 4
    object QrMCMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
  end
  object QrProdRib: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _SPED_EFD_K2XX_O_P;'
      'CREATE TABLE _SPED_EFD_K2XX_O_P'
      'SELECT DATE(vmi.DataHora) Data,  cab.GGXDst GGX_Dst, '
      'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, med.Grandeza'
      'FROM bluederm_meza.vsopecab cab'
      
        'LEFT JOIN bluederm_meza.vsmovits vmi ON vmi.MovimCod=cab.MovimCo' +
        'd'
      'LEFT JOIN bluederm_meza.gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm_meza.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm_meza.unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59"'
      'AND cab.GGXDst <> vmi.GragruX'
      'AND MovimNiv=9'
      'UNION'
      'SELECT DATE(vmi.DataHora) Data,  cab.GGXDst GGX_Dst, '
      'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, med.Grandeza'
      'FROM bluederm_meza.vspwecab cab'
      
        'LEFT JOIN bluederm_meza.vsmovits vmi ON vmi.MovimCod=cab.MovimCo' +
        'd'
      'LEFT JOIN bluederm_meza.gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm_meza.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm_meza.unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59"'
      'AND cab.GGXDst <> vmi.GragruX'
      'AND MovimNiv=22'
      ';'
      'SELECT * FROM _SPED_EFD_K2XX_O_P')
    Left = 136
    Top = 52
    object QrProdRibData: TDateField
      FieldName = 'Data'
    end
    object QrProdRibGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrProdRibGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrProdRibPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrProdRibAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrProdRibPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrProdRibMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrProdRibCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdRibMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrProdRibControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProdRibEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrProdRibQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrProdRibQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrProdRibQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrProdRibsTipoEstq: TFloatField
      FieldName = 'sTipoEstq'
    end
    object QrProdRibdTipoEstq: TFloatField
      FieldName = 'dTipoEstq'
    end
    object QrProdRibMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrProdRibDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrProdRibClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrProdRibFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrProdRibEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
  end
  object QrObtCliForSite: TmySQLQuery
    Database = Dmod.MyDB
    Left = 136
    Top = 100
  end
  object QrIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 240
    Top = 4
  end
  object QrCalToCur: TmySQLQuery
    Database = Dmod.MyDB
    Left = 336
    Top = 4
    object QrCalToCurGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrConsParc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes,'
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, Sum(PesoKg) PesoKg '
      'FROM vsmovits '
      'GROUP BY Ano, Mes'
      '')
    Left = 336
    Top = 52
    object QrConsParcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrConsParcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrConsParcPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrConsParcAno: TFloatField
      FieldName = 'Ano'
    end
    object QrConsParcMes: TFloatField
      FieldName = 'Mes'
    end
  end
  object QrProdParc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes,'
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, Sum(PesoKg) PesoKg '
      'FROM vsmovits '
      'GROUP BY Ano, Mes'
      '')
    Left = 336
    Top = 100
    object QrProdParcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrProdParcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrProdParcPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrProdParcClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrProdParcFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrProdParcControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProdParcAno: TFloatField
      FieldName = 'Ano'
    end
    object QrProdParcMes: TFloatField
      FieldName = 'Mes'
    end
  end
  object QrPeriodos: TmySQLQuery
    Database = Dmod.MyDB
    Left = 336
    Top = 148
    object QrPeriodosAno: TFloatField
      FieldName = 'Ano'
    end
    object QrPeriodosMes: TFloatField
      FieldName = 'Mes'
    end
    object QrPeriodosPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
  end
  object QrVmiGArCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE(DataHora) Data, MovimCod, GraGruX, '
      'Pecas, AreaM2, PesoKg, MovimTwn '
      'FROM vsmovits '
      'WHERE MovimNiv=14 '
      'AND DATE(DataHora) BETWEEN "2016-09-01" AND "2016-09-30" '
      'AND Empresa=-11 '
      'ORDER BY Data, MovimCod, GraGruX ')
    Left = 432
    Top = 4
    object QrVmiGArCabData: TDateField
      FieldName = 'Data'
    end
    object QrVmiGArCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVmiGArCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVmiGArCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVmiGArCabPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVmiGArCabAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVmiGArCabPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVmiGArCabMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVmiGArCabUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrVmiGArCabmed_grandeza: TIntegerField
      FieldName = 'med_grandeza'
    end
    object QrVmiGArCabxco_grandeza: TIntegerField
      FieldName = 'xco_grandeza'
    end
    object QrVmiGArCabGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVmiGArCabCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrVmiGArCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVmiGArCabTipoEstq: TFloatField
      FieldName = 'TipoEstq'
    end
    object QrVmiGArCabDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVmiGArCabClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVmiGArCabFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVmiGArCabEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
    object QrVmiGArCabQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
  end
  object QrSumGAR: TmySQLQuery
    Database = Dmod.MyDB
    Left = 432
    Top = 52
    object QrSumGARPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumGARAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumGARPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object QrVmiGArIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE(DataHora) Data, MovimCod, GraGruX, '
      'Pecas, AreaM2, PesoKg, MovimTwn '
      'FROM vsmovits '
      'WHERE MovimNiv=14 '
      'AND DATE(DataHora) BETWEEN "2016-09-01" AND "2016-09-30" '
      'AND Empresa=-11 '
      'ORDER BY Data, MovimCod, GraGruX ')
    Left = 432
    Top = 100
    object QrVmiGArItsData: TDateField
      FieldName = 'Data'
    end
    object QrVmiGArItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVmiGArItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVmiGArItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVmiGArItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVmiGArItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVmiGArItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVmiGArItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVmiGArItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrVmiGArItsmed_grandeza: TIntegerField
      FieldName = 'med_grandeza'
    end
    object QrVmiGArItsxco_grandeza: TIntegerField
      FieldName = 'xco_grandeza'
    end
    object QrVmiGArItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVmiGArItsCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrVmiGArItsTipoEstq: TLargeintField
      FieldName = 'TipoEstq'
    end
    object QrVmiGArItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVmiGArItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVmiGArItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVmiGArItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVmiGArItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVmiGArItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVmiGArItsJmpGGX: TIntegerField
      FieldName = 'JmpGGX'
    end
  end
  object QrIMECs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM  _seii_imecs_')
    Left = 524
    Top = 4
    object QrIMECsMovCodPai: TIntegerField
      FieldName = 'MovCodPai'
    end
    object QrIMECsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrIMECsMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrIMECsMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrIMECsAnoCA: TIntegerField
      FieldName = 'AnoCA'
      Required = True
    end
    object QrIMECsMesCA: TIntegerField
      FieldName = 'MesCA'
      Required = True
    end
    object QrIMECsTipoTab: TWideStringField
      FieldName = 'TipoTab'
      Required = True
      Size = 15
    end
    object QrIMECsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrIMECsJmpMovID: TIntegerField
      FieldName = 'JmpMovID'
    end
    object QrIMECsJmpNivel1: TIntegerField
      FieldName = 'JmpNivel1'
    end
    object QrIMECsJmpNivel2: TIntegerField
      FieldName = 'JmpNivel2'
    end
  end
  object QrVSRibAtu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_FORNECE, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 524
    Top = 53
    object QrVSRibAtuCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRibAtuControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRibAtuMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRibAtuMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRibAtuMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRibAtuEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRibAtuTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRibAtuCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRibAtuMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRibAtuDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRibAtuPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRibAtuGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRibAtuPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibAtuPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibAtuAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtuAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtuValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtuSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRibAtuSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRibAtuSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRibAtuSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRibAtuSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRibAtuSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibAtuSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtuObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRibAtuSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRibAtuFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRibAtuMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRibAtuFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRibAtuCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSRibAtuCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSRibAtuCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtuValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtuDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRibAtuDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRibAtuDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRibAtuDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRibAtuQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRibAtuQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibAtuQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtuQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtuQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRibAtuQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibAtuQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtuQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibAtuNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSRibAtuNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRibAtuNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRibAtuNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRibAtuID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRibAtuNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRibAtuReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRibAtuCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSRibAtuCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSRibAtuNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSRibAtuMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSRibAtuPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrVSRibAtuStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSRibAtuNO_FICHA: TWideStringField
      DisplayWidth = 80
      FieldName = 'NO_FICHA'
      Size = 80
    end
    object QrVSRibAtuNO_FORNEC_MO: TWideStringField
      FieldName = 'NO_FORNEC_MO'
      Size = 100
    end
    object QrVSRibAtuClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSRibAtuCUSTO_KG: TFloatField
      FieldName = 'CUSTO_KG'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSRibAtuCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object QrVSRibDst: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 524
    Top = 101
    object QrVSRibDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRibDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRibDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRibDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRibDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRibDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRibDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRibDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRibDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRibDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRibDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRibDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRibDstPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibDstPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibDstAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDstAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRibDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRibDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRibDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRibDstSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRibDstSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibDstSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRibDstSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRibDstFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRibDstMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRibDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRibDstCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDstCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRibDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRibDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRibDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRibDstQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRibDstQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibDstQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDstQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDstQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRibDstQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibDstQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDstQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDstNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRibDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRibDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRibDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRibDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRibDstNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSRibDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRibDstPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSRibDstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSRibDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSRibDstNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 50
    end
    object QrVSRibDstNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 50
    end
    object QrVSRibDstDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
  end
  object QrProc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 524
    Top = 196
    object QrProcDtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
    end
    object QrProcDtHrFimOpe: TDateTimeField
      FieldName = 'DtHrFimOpe'
    end
  end
  object QrVmiTwn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 524
    Top = 244
  end
  object QrJmpA: TmySQLQuery
    Database = Dmod.MyDB
    Left = 524
    Top = 292
    object QrJmpAMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrJmpACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrJmpAMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrJmpAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrJmpASrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
  end
  object QrJmpB: TmySQLQuery
    Database = Dmod.MyDB
    Left = 524
    Top = 340
    object QrJmpBMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrJmpBCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrJmpBMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrJmpBControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrVSRibInd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 524
    Top = 149
    object QrVSRibIndCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRibIndControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRibIndMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRibIndMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRibIndMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRibIndEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRibIndTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRibIndCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRibIndMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRibIndDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRibIndPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRibIndGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRibIndPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibIndPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibIndAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibIndAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibIndValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibIndSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRibIndSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRibIndSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRibIndSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRibIndSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRibIndSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibIndSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibIndObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRibIndSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRibIndFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRibIndMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRibIndFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRibIndCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibIndCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibIndCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibIndValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibIndDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRibIndDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRibIndDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRibIndDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRibIndQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRibIndQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibIndQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibIndQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibIndQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRibIndQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibIndQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibIndQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibIndNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibIndNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRibIndNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRibIndNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRibIndID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRibIndNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRibIndNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSRibIndReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRibIndPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSRibIndMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSRibIndStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSRibIndNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 50
    end
    object QrVSRibIndNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 50
    end
    object QrVSRibIndDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVSRibIndJmpMovID: TLargeintField
      FieldName = 'JmpMovID'
    end
    object QrVSRibIndJmpNivel1: TLargeintField
      FieldName = 'JmpNivel1'
    end
    object QrVSRibIndJmpNivel2: TLargeintField
      FieldName = 'JmpNivel2'
    end
    object QrVSRibIndJmpGGX: TLargeintField
      FieldName = 'JmpGGX'
    end
    object QrVSRibIndRmsGGX: TLargeintField
      FieldName = 'RmsGGX'
    end
  end
  object QrFather: TmySQLQuery
    Database = Dmod.MyDB
    Left = 524
    Top = 388
    object QrFatherMovCodPai: TIntegerField
      FieldName = 'MovCodPai'
    end
  end
  object QrVSRibOriIMEI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 524
    Top = 437
    object QrVSRibOriIMEICodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRibOriIMEIControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRibOriIMEIMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRibOriIMEIMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRibOriIMEIMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRibOriIMEIEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRibOriIMEITerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRibOriIMEICliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRibOriIMEIMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRibOriIMEIDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRibOriIMEIPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRibOriIMEIGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRibOriIMEIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibOriIMEIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibOriIMEIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEIValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEISrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRibOriIMEISrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRibOriIMEISrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRibOriIMEISrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRibOriIMEISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRibOriIMEISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibOriIMEISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRibOriIMEISerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRibOriIMEIFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRibOriIMEIMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRibOriIMEIFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRibOriIMEICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEIValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEIDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRibOriIMEIDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRibOriIMEIDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRibOriIMEIDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRibOriIMEIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRibOriIMEIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibOriIMEIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRibOriIMEIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRibOriIMEIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSRibOriIMEINO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRibOriIMEINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRibOriIMEINO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRibOriIMEIID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRibOriIMEINO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRibOriIMEINO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSRibOriIMEIReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRibOriIMEICustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRibOriIMEIVSMulFrnCab: TLargeintField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSRibOriIMEIClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSRibOriIMEIRmsMovID: TLargeintField
      FieldName = 'RmsMovID'
    end
    object QrVSRibOriIMEIRmsNivel1: TLargeintField
      FieldName = 'RmsNivel1'
    end
    object QrVSRibOriIMEIRmsNivel2: TLargeintField
      FieldName = 'RmsNivel2'
    end
    object QrVSRibOriIMEIDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVSRibOriIMEIRmsGGX: TLargeintField
      FieldName = 'RmsGGX'
    end
  end
  object QrCab34: TmySQLQuery
    Database = Dmod.MyDB
    Left = 524
    Top = 484
    object QrCab34MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
  end
  object QrPQxMixOP: TmySQLQuery
    Database = Dmod.MyDB
    Left = 628
    object QrPQxMixOPOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQxMixOPCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPQxMixOPDtCorrApo: TDateField
      FieldName = 'DtCorrApo'
    end
  end
  object QrPQMCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'lse.Nome NOMESETOR, reb.Linhas RebLin, '
      'emg.Nome NO_EMITGRU, gcc.Nome NoGraCorCad, pqm.*'
      'FROM pqm pqm'
      'LEFT JOIN entidades emp ON emp.Codigo=pqm.Empresa'
      'LEFT JOIN listasetores lse ON lse.Codigo=pqm.Setor'
      'LEFT JOIN emitgru emg ON emg.Codigo=pqm.EmitGru'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=pqm.GraCorCad'
      'LEFT JOIN espessuras reb ON reb.Codigo=pqm.SemiCodEspReb'
      'WHERE pqm.Codigo > 0'
      'AND pqm.Empresa=-11')
    Left = 628
    Top = 49
    object QrPQMCabDataB: TDateField
      FieldName = 'DataB'
    end
    object QrPQMCabVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
    end
  end
  object QrPQMBxa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  '
      'pqg.Nome NOMEGRUPO '
      'FROM pqx pqx '
      'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo '
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ '
      'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico '
      'WHERE pqx.Tipo=130 '
      'AND pqx.OrigemCodi>0 '
      ' ')
    Left = 628
    Top = 145
    object QrPQMBxaOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQMBxaDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQMBxaInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQMBxaPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object QrPQMGer: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  '
      'pqg.Nome NOMEGRUPO '
      'FROM pqx pqx '
      'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo '
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ '
      'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico '
      'WHERE pqx.Tipo=130 '
      'AND pqx.OrigemCodi>0 '
      ' ')
    Left = 628
    Top = 97
    object QrPQMGerOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQMGerDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQMGerInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQMGerPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
end
