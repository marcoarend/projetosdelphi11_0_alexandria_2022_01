unit UnVS_EFD_ICMS_IPI;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  dmkDBGridZTO, UnDmkProcFunc, UnProjGroup_Vars, BlueDermConsts, TypInfo,
  System.Math, UnProjGroup_Consts, UnEntities, DBCtrls, Grids, Mask, UnVS_Tabs,
  dmkEditDateTimePicker, UnGrl_Consts, UnGrl_Geral, UnProjGroup_PF,
  mySQLDirectQuery, UnVS_PF, UnAppEnums;

type
  TUnVS_EFD_ICMS_IPI = class(TObject)
  private
    { Private declarations }
     //
  public
    { Public declarations }
    //
    function  ReopenVSOriPQx_Fast(Qry: TmySQLDirectQuery; MovimCod: Integer;
              SQL_PeriodoPQ: String; ShowSQL: Boolean): Boolean;
    procedure ReopenVSOriPQx(Qry: TmySQLQuery; MovimCod: Integer;
              SQL_PeriodoPQ: String; ShowSQL: Boolean);
    procedure ReopenVSRibItss(QrVSAtu, QrVSOriIMEI, QrVSOriPallet,
              QrVSDst, QrVSInd, QrSubPrd, QrDescl, QrForcados, QrEmit, QrPQO: TmySQLQuery;
              Codigo, MovimCod, TemIMEIMrt: Integer; MovIDEmProc, MovIDPronto,
              MovIDIndireto: TEstqMovimID; MovNivInn, MovNivSrc, MovNivDst:
              TEstqMovimNiv);
    procedure ReopenVSRibItss_Fast(DqVSAtu, DqVSOriIMEI, DqVSOriPallet,
              DqVSDst, DqVSInd, DqSubPrd, DqDescl, DqForcados, DqEmit, DqPQO:
              TmySQLDirectQuery; Codigo, MovimCod, TemIMEIMrt: Integer;
              MovIDEmProc, MovIDPronto, MovIDIndireto: TEstqMovimID;
              MovNivInn, MovNivSrc, MovNivDst: TEstqMovimNiv; TabIMEI: String;
              DB_IMEI: TmySQLDatabase; BaseDadosDiminuida: Boolean);
    procedure ReopenVSMovCod_Emit(QrEmit: TmySQLQuery; MovimCod: Integer);
    procedure ReopenVSMovCod_Emit_Fast(QrEmit: TmySQLDirectQuery; MovimCod:
              Integer; TabIMEI: String; DB_IMEI: TmySQLDatabase);
    procedure ReopenVSMovCod_PQO(QrPQO: TmySQLQuery; MovimCod, Codigo: Integer);
    procedure ReopenVSMovCod_PQO_Fast(QrPQO: TmySQLDirectQuery; MovimCod,
              Codigo: Integer; TabIMEI: String; DB_IMEI: TmySQLDatabase);
    procedure ReopenVSMovCod_PQOIts(QrPQOIts: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenVSOpePrcAtu(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt:
              Integer; MovimNiv: TEstqMovimNiv);
    procedure ReopenVSOpePrcAtu_Fast(Qry: TmySQLDirectQuery; MovimCod, (*Controle,*)
              TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; TabIMEI: String; DB_IMEI: TmySQLDatabase);
    procedure ReopenVSOpePrcForcados(Qry: TmySQLQuery; Controle, SrcNivel1,
              SrcNivel2, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID);
    procedure ReopenVSOpePrcForcados_Fast(Qry: TmySQLDirectQuery; Controle,
              SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
              TabIMEI: String; DB_IMEI: TmySQLDatabase);
    procedure ReopenVSOpePrcOriIMEI(Qry: TmySQLQuery; MovimCod, Controle,
              TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
    procedure ReopenVSOpePrcOriIMEI_Fast(Qry: TmySQLDirectQuery; MovimCod, (*Controle,*)
              TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; TabIMEI: String;
              DB_IMEI: TmySQLDatabase; BaseDadosDiminuida: Boolean);
    procedure ReopenVSOpePrcOriPallet(QrVSCalOriPallet: TmySQLQuery; MovimCod:
              Integer; MovimNiv: TEstqMovimNiv; TemIMEIMrt, LocPallet: Integer);
    procedure ReopenVSOpePrcOriPallet_Fast(QrVSCalOriPallet: TmySQLDirectQuery; MovimCod:
              Integer; MovimNiv: TEstqMovimNiv; TemIMEIMrt(*, LocPallet*):
              Integer; TabIMEI: String; DB_IMEI: TmySQLDatabase);
    procedure ReopenVSPrcPrcDst(Qry: TmySQLQuery; SrcNivel1, MovimCod, Controle,
              TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
              DstMovimNiv: TEstqMovimNiv; SQL_Limit: String = '');
    procedure ReopenVSPrcPrcDst_Fast(Qry: TmySQLDirectQuery; SrcNivel1, MovimCod,
              (*Controle,*) TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
              DstMovimNiv: TEstqMovimNiv; TabIMEI: String; DB_IMEI: TmySQLDatabase; SQL_Limit: String = '');
    procedure ReopenVSPrcPrcInd(Qry: TmySQLQuery; SrcNivel1, MovimCod, Controle,
              TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
              DstMovimNiv: TEstqMovimNiv; SQL_Limit: String = '');
    procedure ReopenVSPrcPrcInd_Fast(Qry: TmySQLDirectQuery; SrcNivel1, MovimCod,
              (*Controle,*) TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
              DstMovimNiv: TEstqMovimNiv; TabIMEI: String; DB_IMEI:
              TmySQLDatabase; BaseDadosDiminuida: Boolean);
  end;

var
  VS_EFD_ICMS_IPI: TUnVS_EFD_ICMS_IPI;

implementation

uses DmkDAC_PF, UnVS_CRC_PF, Module, ModuleGeral;

{ TUnVS_EFD_ICMS_IPI }

procedure TUnVS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSAtu, QrVSOriIMEI,
  QrVSOriPallet, QrVSDst, QrVSInd, QrSubPrd, QrDescl, QrForcados, QrEmit, QrPQO:
  TmySQLQuery; Codigo, MovimCod, TemIMEIMrt: Integer; MovIDEmProc, MovIDPronto,
  MovIDIndireto: TEstqMovimID; MovNivInn, MovNivSrc, MovNivDst: TEstqMovimNiv);
const
  Controle = 0;
  Pallet   = 0;
  //
begin
  // Em processo. Deve ser antes do Forcados!!
  ReopenVSOpePrcAtu(QrVSAtu, (*QrVSCab*)MovimCod(*.Value*), Controle,
  (*QrVSCab*)TemIMEIMrt(*.Value*), MovNivInn(*eminEmInn*));
////////////////////////////////////////////////////////////////////////////////
  // Origem
  if QrVSOriIMEI <> nil then
    ReopenVSOpePrcOriIMEI(QrVSOriIMEI,
    (*QrVSCab*)MovimCod(*.Value*), Controle,
    TemIMEIMrt, MovNivSrc(*eminSorc*), (*SQL_Limit*)'');
  if QrVSOriPallet <> nil then
    ReopenVSOpePrcOriPallet(QrVSOriPallet, MovimCod,
    MovNivSrc(*eminSorc*), TemIMEIMrt, Pallet);
////////////////////////////////////////////////////////////////////////////////
  // Destino Direto
  if QrVSDst <> nil then
    ReopenVSPrcPrcDst(QrVSDst, (*QrVSCab*)Codigo(*.Value*),
    (*QrVSCab*)MovimCod(*.Value*), Controle, (*QrVSCab*)TemIMEIMrt(*.Value*),
    MovIDPronto(*TEstqMovimID.emideado*),
    MovNivDst(*eminDest*));
////////////////////////////////////////////////////////////////////////////////
  // Destino Indireto
  if QrVSInd <> nil then
    ReopenVSPrcPrcInd(QrVSInd, (*QrVSCab*)Codigo(*.Value*),
    (*QrVSCab*)MovimCod(*.Value*), Controle,
    (*QrVSCab*)TemIMEIMrt(*.Value*),
    MovIDIndireto(*TEstqMovimID.emideado*),
    MovNivDst(*eminDest*));
////////////////////////////////////////////////////////////////////////////////
  // For�ados. Deve ser depois do Forcados!!
  if QrForcados <> nil then
    ReopenVSOpePrcForcados(QrForcados, 0, (*QrVSCab*)Codigo(*.Value*),
      (*QrVSAtuControle.Value*)QrVSAtu.FieldByName('Controle').AsInteger,
      (*QrVSCab*)TemIMEIMrt(*.Value*), MovIDEmProc(*emidEmProc*));
////////////////////////////////////////////////////////////////////////////////
  // Insumos
  if QrEmit <> nil then
    ReopenVSMovCod_Emit(QrEmit, MovimCod);
  if QrPQO <> nil then
    ReopenVSMovCod_PQO(QrPQO, MovimCod, 0);
////////////////////////////////////////////////////////////////////////////////
  //
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSRibItss_Fast(DqVSAtu, DqVSOriIMEI,
  DqVSOriPallet, DqVSDst, DqVSInd, DqSubPrd, DqDescl, DqForcados, DqEmit,
  DqPQO: TmySQLDirectQuery; Codigo, MovimCod, TemIMEIMrt: Integer; MovIDEmProc,
  MovIDPronto, MovIDIndireto: TEstqMovimID; MovNivInn, MovNivSrc,
  MovNivDst: TEstqMovimNiv; TabIMEI: String; DB_IMEI: TmySQLDatabase;
  BaseDadosDiminuida: Boolean);
{
const
  Controle = 0;
  Pallet   = 0;
  //
}
begin
  // Em processo. Deve ser antes do Forcados!!
  ReopenVSOpePrcAtu_Fast(DqVSAtu, (*DqVSCab*)MovimCod(*.Value*), (*Controle,*)
  (*DqVSCab*)TemIMEIMrt(*.Value*), MovNivInn(*eminEmInn*), TabIMEI, DB_IMEI);
////////////////////////////////////////////////////////////////////////////////
  // Origem
  if DqVSOriIMEI <> nil then
    ReopenVSOpePrcOriIMEI_Fast(DqVSOriIMEI,
    (*DqVSCab*)MovimCod(*.Value*), (*Controle,*)
    TemIMEIMrt, MovNivSrc(*eminSorc*), TabIMEI, DB_IMEI, BaseDadosDiminuida);
  if DqVSOriPallet <> nil then
    ReopenVSOpePrcOriPallet_Fast(DqVSOriPallet, MovimCod,
    MovNivSrc(*eminSorc*), TemIMEIMrt(*, Pallet*), TabIMEI, DB_IMEI);
////////////////////////////////////////////////////////////////////////////////
  // Destino Direto
  if DqVSDst <> nil then
    ReopenVSPrcPrcDst_Fast(DqVSDst, (*DqVSCab*)Codigo(*.Value*),
    (*DqVSCab*)MovimCod(*.Value*), (*Controle,*) (*DqVSCab*)TemIMEIMrt(*.Value*),
    MovIDPronto(*TEstqMovimID.emideado*),
    MovNivDst(*eminDest*), TabIMEI, DB_IMEI);
////////////////////////////////////////////////////////////////////////////////
  // Destino Indireto
  if DqVSInd <> nil then
    ReopenVSPrcPrcInd_Fast(DqVSInd, (*DqVSCab*)Codigo(*.Value*),
    (*DqVSCab*)MovimCod(*.Value*), (*Controle,*)
    (*DqVSCab*)TemIMEIMrt(*.Value*),
    MovIDIndireto(*TEstqMovimID.emideado*),
    MovNivDst(*eminDest*), TabIMEI, DB_IMEI,
    BaseDadosDiminuida);
////////////////////////////////////////////////////////////////////////////////
  // For�ados. Deve ser depois do Forcados!!
  if DqForcados <> nil then
  begin
  //'SELECT MovimID, MovimCod, Codigo, DataHora, DtCorrApo, ',
  //'Empresa, FornecMO, Controle, GraGruX, MovimNiv, MovimTwn ',
    ReopenVSOpePrcForcados_Fast(DqForcados, 0, (*DqVSCab*)Codigo(*.Value*),
      (*DqVSAtuControle.Value*)Geral.IMV(DqVSAtu.FieldValueByFieldName('Controle')),
      (*DqVSCab*)TemIMEIMrt(*.Value*), MovIDEmProc(*emidEmProc*), TabIMEI, DB_IMEI);
  end;
////////////////////////////////////////////////////////////////////////////////
  // Insumos
  if DqEmit <> nil then
    ReopenVSMovCod_Emit_Fast(DqEmit, MovimCod, TabIMEI, DB_IMEI);
  if DqPQO <> nil then
    ReopenVSMovCod_PQO_Fast(DqPQO, MovimCod, 0, TabIMEI, DB_IMEI);
////////////////////////////////////////////////////////////////////////////////
  //
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSMovCod_Emit(QrEmit: TmySQLQuery; MovimCod:
  Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
  'SELECT emi.*, ',
  'IF(emi.DtaBaixa < 2, "??/??/????", DATE_FORMAT(emi.DtaBaixa, "%d/%m/%Y") ) DtaBaixa_TXT, ',
  'IF(emi.DtCorrApo < 2, "", DATE_FORMAT(emi.DtCorrApo, "%d/%m/%Y") ) DtCorrApo_TXT ',
  'FROM emit emi ',
  'WHERE emi.VSMovCod=' + Geral.FF0((*QrVSCalCab*)MovimCod(*.Value*)),
  '']);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSMovCod_Emit_Fast(QrEmit: TmySQLDirectQuery;
  MovimCod: Integer; TabIMEI: String; DB_IMEI: TmySQLDatabase);
begin
  Geral.MB_Info('FaltaFazer: TUnVS_EFD_ICMS_IPI.ReopenVSMovCod_Emit_Fast(');
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSMovCod_PQO(QrPQO: TmySQLQuery; MovimCod,
  Codigo: Integer);
begin
{$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQO, Dmod.MyDB, [
  'SELECT lse.Nome NOMESETOR, ',
  'emg.Nome NO_EMITGRU, pqo.* ',
  'FROM pqo pqo ',
  'LEFT JOIN listasetores lse ON lse.Codigo=pqo.Setor ',
  'LEFT JOIN emitgru emg ON emg.Codigo=pqo.EmitGru ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  if Codigo <> 0 then
    QrPQO.Locate('Codigo', Codigo, []);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSMovCod_PQOIts(QrPQOIts: TmySQLQuery; Codigo,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQOIts, Dmod.MyDB, [
  'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  ',
  'pqg.Nome NOMEGRUPO ',
  'FROM pqx pqx ',
  'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo ',
  'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ ',
  'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
  'AND pqx.OrigemCodi=' + Geral.FF0(Codigo),
  '']);
  if Controle <> 0 then
    QrPQOIts.Locate('OrigemCtrl', Controle, []);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSMovCod_PQO_Fast(QrPQO: TmySQLDirectQuery;
  MovimCod, Codigo: Integer; TabIMEI: String; DB_IMEI: TmySQLDatabase);
begin
  Geral.MB_Info('FaltaFazer: TUnVS_EFD_ICMS_IPI.ReopenVSMovCod_PQO_Fast(');
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcAtu(Qry: TmySQLQuery; MovimCod,
  Controle, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  VS_CRC_PF.SQL_NO_FRN(),
  VS_CRC_PF.SQL_NO_SCL(),
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome)NO_FORNEC_MO, ',
  'IF(clo.Tipo=0, clo.RazaoSocial, clo.Nome)NO_CLIENT_MO, ',

  'IF(vmi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", vmi.Ficha)) NO_FICHA, ',
  'IF(PesoKg=0, 0, ValorT / PesoKg) CUSTO_KG, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, ',
  'vsp.Nome NO_Pallet, cou.CouNiv1, cou.CouNiv2 ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  VS_CRC_PF.SQL_LJ_FRN(),
  VS_CRC_PF.SQL_LJ_SCL(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  fmo ON fmo.Codigo=vmi.FornecMO',
  'LEFT JOIN entidades  clo ON clo.Codigo=vmi.ClientMO ',
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=vmi.GraGruX ',
  //'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2', // 2023-12-30 n�o precisa?
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //Geral.MB_Teste(nil, Qry);
  Qry.Locate('Controle', Controle, []);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcAtu_Fast(Qry: TmySQLDirectQuery;
  MovimCod, (*Controle,*) TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv;
  TabIMEI: String; DB_IMEI: TmySQLDatabase);
const
  sProcName = 'TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcAtu_Fast()';
begin
  UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DB_IMEI, [
  'SELECT MovimID, MovimCod, Codigo, DataHora, DtCorrApo, ',
  'Empresa, FornecMO, Controle, GraGruX, MovimNiv, MovimTwn ',
  'FROM ' + TabIMEI,
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  //
  if Qry.RecordCount = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DModG.MyCompressDB, [
    'SELECT MovimID, MovimCod, Codigo, DataHora, DtCorrApo, ',
    'Empresa, FornecMO, Controle, GraGruX, MovimNiv, MovimTwn ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    '']);
    //
    if Qry.RecordCount = 0 then
      Geral.MB_Info('RecordCount=0' + sLineBreak + sProcName + sLineBreak + Qry.SQL.Text);
  end;
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcForcados(Qry: TmySQLQuery; Controle,
  SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  //TemIMEIMrt: Integer;
begin
  //TemIMEIMrt := QrVSOpeCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
(*
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod), //QrVSOpeCabMovimCod.Value),
  'AND vmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)), //emidEmOperacao)),  // 11
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1), //QrVSOpeCabCodigo.Value),
*)
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)), //emidEmOperacao)),  // 11
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1), //QrVSOpeCabCodigo.Value),
  'AND vmi.SrcNivel2=' + Geral.FF0(SrcNivel2), //QrVSOpeCabCodigo.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  //Geral.MB_Teste(nil, Qry);
  Qry.Locate('Controle', Controle, []);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcForcados_Fast(Qry: TmySQLDirectQuery;
  Controle, SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
  TabIMEI: String; DB_IMEI: TmySQLDatabase);
begin
  Geral.MB_Info('FaltaFazer: TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcForcados_Fast(');
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(Qry: TmySQLQuery; MovimCod, Controle,
  TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
begin
  ProjGroup_PF.ReopenVSOpePrcOriIMEI(Qry, MovimCod, Controle, TemIMEIMrt,
  MovimNiv, SQL_Limit);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI_Fast(Qry: TmySQLDirectQuery;
  MovimCod, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; TabIMEI: String;
  DB_IMEI: TmySQLDatabase; BaseDadosDiminuida: Boolean);
const
  sProcName = 'TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI_Fast()';
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //Geral.MB_Info('FaltaFazer: TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI_Fast(');
  if BaseDadosDiminuida then
  begin
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DB_IMEI, [
    'SELECT DataHora, MovimID, MovimCod, Codigo, DtCorrApo, ',
    'Empresa, FornecMO, Controle, RmsGGX, DstGGX, MovimNiv, ',
    'MovimTwn, AreaM2, PesoKg, Pecas, QtdAntPeca, QtdAntPeso, ',
    'QtdAntArM2, ClientMO, StqCenLoc, scc_EntiSitio, TipoEstq ',
    'FROM ' + TabIMEI,
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    '']);
  end;
  //
  if (BaseDadosDiminuida =  False) or (Qry.RecordCount = 0) then
  begin
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DModG.MyCompressDB, [
    'SELECT vmi.DataHora, vmi.MovimID, vmi.MovimCod, vmi.Codigo, ',
    'vmi.DtCorrApo, vmi.Empresa, vmi.FornecMO, vmi.Controle, vmi.RmsGGX, ',
    'vmi.DstGGX, vmi.MovimNiv, vmi.MovimTwn, vmi.AreaM2, vmi.PesoKg, ',
    'vmi.Pecas, vmi.QtdAntPeca, vmi.QtdAntPeso, vmi.QtdAntArM2, vmi.ClientMO, ',
    VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
    'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'vmi.StqCenLoc, scc.EntiSitio scc_EntiSitio ', //TipoEstq ', // vai dar erro TipoEstq ver outrs como foi aberto!
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
     //
    'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
    //
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    //
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    '']);
    //if Qry.RecordCount = 0 then
      //Geral.MB_Info('RecordCount=0' + sLineBreak + sProcName + sLineBreak + Qry.SQL.Text);
  end;
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcOriPallet(QrVSCalOriPallet:
  TmySQLQuery; MovimCod: Integer; MovimNiv: TEstqMovimNiv;
  TemIMEIMrt, LocPallet: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  '']);
  SQL_Left := Geral.ATS([
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0((*QrVSCalCab*)MovimCod(*.Value*)),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer((*eminSorcCal*)MovimNiv)),
  '']);
  SQL_Group := 'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCalOriPallet, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
  QrVSCalOriPallet.Locate('Pallet', LocPallet, []);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcOriPallet_Fast(
  QrVSCalOriPallet: TmySQLDirectQuery; MovimCod: Integer;
  MovimNiv: TEstqMovimNiv; TemIMEIMrt(*, LocPallet*): Integer; TabIMEI: String; DB_IMEI: TmySQLDatabase);
begin
  Geral.MB_Info('FaltaFazer: TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcOriPallet_Fast(');
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOriPQx(Qry: TmySQLQuery; MovimCod: Integer;
  SQL_PeriodoPQ: String; ShowSQL: Boolean);
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, Peso, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
  'CliDest ClientMO ',
  'FROM pqx ',
  'WHERE ( ',
  '  Tipo=' + Geral.FF0(VAR_FATID_0110),
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM emit ',
  '    WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'OR ',
  '( ',
  '  Tipo=' + Geral.FF0(VAR_FATID_0190),
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM PQO ',
  '    WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'AND Peso <> 0',
  SQL_PeriodoPQ,
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, Peso, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
  'CliDest ClientMO ',
  'FROM pqx ',
  'WHERE ( ',
  '  ( ',
  '    Tipo=' + Geral.FF0(VAR_FATID_0110),
  '    AND OrigemCodi IN ( ',
  '      SELECT Codigo ',
  '      FROM emit ',
  '      WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '    ) ',
  '  ) ',
  '  OR ',
  '  ( ',
  '    Tipo=' + Geral.FF0(VAR_FATID_0190),
  '    AND OrigemCodi IN ( ',
  '      SELECT Codigo ',
  '      FROM PQO ',
  '      WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '    ) ',
  '  ) ',
  ') ',
  'AND Peso <> 0',
  'AND Insumo > 0',
  SQL_PeriodoPQ,
  '']);
  //
  if ShowSQL then
    Geral.MB_Info(Qry.SQL.Text);
end;

function TUnVS_EFD_ICMS_IPI.ReopenVSOriPQx_Fast(Qry: TmySQLDirectQuery;
  MovimCod: Integer; SQL_PeriodoPQ: String; ShowSQL: Boolean): Boolean;
var
  SQL_Emit, SQL_PQO, SQL_Join: String;
begin
{
  Result := UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, Dmod.MyDB, [
  'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, Peso, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
  'CliDest ClientMO ',
  'FROM pqx ',
  'WHERE ( ',
  '  ( ',
  '    Tipo=' + Geral.FF0(VAR_FATID_0110),
  '    AND OrigemCodi IN ( ',
  '      SELECT Codigo ',
  '      FROM emit ',
  '      WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '    ) ',
  '  ) ',
  '  OR ',
  '  ( ',
  '    Tipo=' + Geral.FF0(VAR_FATID_0190),
  '    AND OrigemCodi IN ( ',
  '      SELECT Codigo ',
  '      FROM PQO ',
  '      WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '    ) ',
  '  ) ',
  ') ',
  'AND Peso <> 0',
  'AND Insumo > 0',
  SQL_PeriodoPQ,
  '']);
  //
  if ShowSQL then
    Geral.MB_Info(Qry.SQL.Text);
}
////////////////////////////////////////////////////////////////////////////////
  Result   := False;
  SQL_Emit := '';
  SQL_PQO  := '';
  SQL_Join := '';
  //
  UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM emit ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  Qry.First;
  if Qry.RecordCount > 0 then
  begin
    Result := True;
    SQL_Emit := Qry.FieldValueByFieldName('Codigo');
  end;
  if Qry.RecordCount > 1 then
  begin
    Qry.Next;
    while not Qry.Eof do
    begin
      SQL_Emit := SQL_Emit + ',' + Qry.FieldValueByFieldName('Codigo');
      Qry.Next;
    end;
  end;
  //
  UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM PQO ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  Qry.First;
  if Qry.RecordCount > 0 then
  begin
    Result := True;
    SQL_PQO := Qry.FieldValueByFieldName('Codigo');
  end;
  if Qry.RecordCount > 1 then
  begin
    Qry.Next;
    while not Qry.Eof do
    begin
      SQL_PQO := SQL_PQO + ',' + Qry.FieldValueByFieldName('Codigo');
      Qry.Next;
    end;
  end;
  //
  if Result then
  begin
    if SQL_Emit <> '' then
      SQL_Emit :=
      '    Tipo=' + Geral.FF0(VAR_FATID_0110) +
      '    AND OrigemCodi IN ( ' + SQL_Emit + ') ';
    if SQL_PQO <> '' then
      SQL_PQO :=
      '    Tipo=' + Geral.FF0(VAR_FATID_0190) +
      '    AND OrigemCodi IN ( ' + SQL_PQO + ') ';
    if (SQL_Emit <> '') and (SQL_PQO <> '') then
       SQL_Join :=
      '  ) ' +
      '  OR ' +
      '  ( ';
    //
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, Dmod.MyDB, [
    'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, Peso, ',
    'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
    'CliDest ClientMO ',
    'FROM pqx ',
    'WHERE ( ',
    '  ( ',
    SQL_Emit,
    SQL_Join,
    SQL_PQO,
    '  ) ',
    ') ',
    'AND Peso <> 0',
    'AND Insumo > 0',
    SQL_PeriodoPQ,
    '']);
    //
    if ShowSQL then
      Geral.MB_Info(Qry.SQL.Text);
  end;
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSPrcPrcDst(Qry: TmySQLQuery; SrcNivel1,
  MovimCod, Controle, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
  DstMovimNiv: TEstqMovimNiv; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  VS_CRC_PF.SQL_NO_FRN(),
  VS_CRC_PF.SQL_NO_SCL(),
  ATT_MovimID,
  ATT_MovimNiv,
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  'IF(vmi.AreaM2 <> 0, vmi.ValorT/vmi.AreaM2, 0.0000) CustoM2, ',
  'IF(vmi.PesoKg <> 0, vmi.ValorT/vmi.PesoKg, 0.0000) CustoKg, ',
  'CAST(vsp.SeqSifDipoa AS SIGNED) SeqSifDipoa ',  // 2023-12-31
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  VS_CRC_PF.SQL_LJ_FRN(),
  VS_CRC_PF.SQL_LJ_SCL(),
  'LEFT JOIN vspalleta  vsp  ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf  ON vsf.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  //'WHERE (',
  'WHERE ',
  'vmi.MovimID=' + Geral.FF0(Integer(SrcMovID)),
  'AND vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
(*
  'vmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)),
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1),
  ') or ( ',
  'vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
  ') ',
*)
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //Geral.MB_Teste(Qry.SQL.Text);
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSPrcPrcDst_Fast(Qry: TmySQLDirectQuery;
  SrcNivel1, MovimCod, (*Controle,*) TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
  DstMovimNiv: TEstqMovimNiv; TabIMEI: String; DB_IMEI: TmySQLDatabase; SQL_Limit: String);
const
  sProcName = 'TUnVS_EFD_ICMS_IPI.ReopenVSPrcPrcDst_Fast()';
begin
  UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DB_IMEI, [
  'SELECT DataHora, MovimID, MovimCod, Codigo, DtCorrApo, ',
  'Empresa, FornecMO, Controle, GraGruX, MovimNiv, ',
  'MovimTwn, AreaM2, PesoKg, Pecas ',
  'FROM ' + TabIMEI,
  'WHERE MovimID=' + Geral.FF0(Integer(SrcMovID)),
  'AND MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
  '']);
  //
  //Geral.MB_Info(Qry.SQL.Text);
(* Na teoria n�o vai dar problema, pois s� se obtem dentro do per�odo,
  ent�o n�o tem como n�o estar na base compactada se existir!
  //////////////////////////////////////////////////////////////////////////////
  if Qry.RecordCount = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DModG.MyCompressDB, [
    'SELECT DataHora, MovimID, MovimCod, Codigo, DtCorrApo, ',
    'Empresa, FornecMO, Controle, GraGruX, MovimNiv, ',
    'MovimTwn, AreaM2, PesoKg, Pecas ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(SrcMovID)),
    'AND MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
    '']);
    //
    if Qry.RecordCount = 0 then
      Geral.MB_Info('RecordCount=0' + sLineBreak + sProcName + sLineBreak + Qry.SQL.Text);
  end;
*)
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSPrcPrcInd(Qry: TmySQLQuery; SrcNivel1,
  MovimCod, Controle, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
  DstMovimNiv: TEstqMovimNiv; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  //VS_CRC_PF.SQL_NO_GGX_DST(),
  VS_CRC_PF.SQL_NO_FRN(),
  VS_CRC_PF.SQL_NO_SCL(),
  ATT_MovimID,
  ATT_MovimNiv,
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  //VS_CRC_PF.SQL_LJ_GGX_DST(),
  VS_CRC_PF.SQL_LJ_FRN(),
  VS_CRC_PF.SQL_LJ_SCL(),
  'LEFT JOIN vspalleta  vsp  ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf  ON vsf.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  //'WHERE (',
  'WHERE ',
  'vmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)),
  //'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovCodPai<>vmi.MovimCod AND vmi.MovCodPai <> 0 AND vmi.MovCodPai=' + Geral.FF0(MovimCod),
{
  ') or ( ',
  'vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
  ') ',
}
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //Geral.MB_Teste(nil, Qry);
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSPrcPrcInd_Fast(Qry: TmySQLDirectQuery;
  SrcNivel1, MovimCod, (*Controle,*) TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
  DstMovimNiv: TEstqMovimNiv; TabIMEI: String; DB_IMEI: TmySQLDatabase;
  BaseDadosDiminuida: Boolean);
const
  sProcName = 'TUnVS_EFD_ICMS_IPI.ReopenVSPrcPrcInd_Fast(';
begin
  if BaseDadosDiminuida then
  begin
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DB_IMEI, [
    'SELECT  ',
    'DataHora, MovimID, MovimCod, Codigo, ',
    'DtCorrApo, Empresa, FornecMO, ',
    'Controle, GraGruX, MovimNiv, MovimTwn, ',
    'AreaM2, PesoKg, Pecas, SrcMovID,  ',
    'SrcNivel1, SrcNivel2, RmsGGX, JmpGGX, QtdGerPeca, ',
    'QtdGerPeso, QtdGerArM2, JmpMovID, JmpNivel1, ',
    'ClientMO, StqCenLoc, scc_EntiSitio, TipoEstq ',
    'FROM ' + TabIMEI,
    'WHERE SrcMovID=' + Geral.FF0(Integer(SrcMovID)),
    'AND MovCodPai<>MovimCod AND MovCodPai <> 0 AND MovCodPai=' + Geral.FF0(MovimCod),
    '']);
  end;
(* Na teoria n�o vai dar problema, pois s� se obtem dentro do per�odo,
  ent�o n�o tem como n�o estar na base compactada se existir!
  //////////////////////////////////////////////////////////////////////////////
*) // Ver se precisa desmarcar como o ReopenVSPrcPrcDst_Fast
  if (BaseDadosDiminuida = False) or (Qry.RecordCount = 0) then
  begin
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DModG.MyCompressDB, [
    'SELECT  ',
    'vmi.DataHora, vmi.MovimID, vmi.MovimCod, vmi.Codigo, ',
    'vmi.DtCorrApo, vmi.Empresa, vmi.FornecMO, ',
    'vmi.Controle, vmi.GraGruX, vmi.MovimNiv, vmi.MovimTwn, ',
    'vmi.AreaM2, vmi.PesoKg, vmi.Pecas, vmi.SrcMovID,  ',
    'vmi.SrcNivel1, vmi.SrcNivel2, vmi. RmsGGX, vmi.JmpGGX, vmi.QtdGerPeca, ',
    'vmi.QtdGerPeso, vmi.QtdGerArM2, vmi.JmpMovID, vmi.JmpNivel1, ',
    VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
    'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'vmi.ClientMO, vmi.StqCenLoc, scc.EntiSitio scc_EntiSitio ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
     //
    'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
    //
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    //
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)),
    'AND vmi.MovCodPai<>vmi.MovimCod AND vmi.MovCodPai <> 0 AND vmi.MovCodPai=' + Geral.FF0(MovimCod),
    '']);
    //if Qry.RecordCount = 0 then
      //Geral.MB_Info('RecordCount=0' + sLineBreak + sProcName + sLineBreak + Qry.SQL.Text);
  end;
end;

end.
