object FmOpcoesBlueDerm: TFmOpcoesBlueDerm
  Left = 339
  Top = 185
  Caption = 'FER-OPCAO-002 :: Op'#231#245'es Espec'#237'ficas do Aplicativo'
  ClientHeight = 657
  ClientWidth = 1063
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1063
    Height = 501
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PCGeral: TPageControl
      Left = 0
      Top = 0
      Width = 1063
      Height = 501
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Envio de email de pedidodos de insumos qu'#237'micos'
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 1055
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label20: TLabel
            Left = 12
            Top = 8
            Width = 75
            Height = 13
            Caption = 'Servidor SMTP:'
          end
          object Label24: TLabel
            Left = 312
            Top = 8
            Width = 83
            Height = 13
            Caption = 'Conex'#227'o Dial Up:'
          end
          object EdServSMTP: TdmkEdit
            Left = 12
            Top = 24
            Width = 293
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdConexaoDialUp: TdmkEdit
            Left = 312
            Top = 24
            Width = 313
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object GroupBox2: TGroupBox
            Left = 16
            Top = 52
            Width = 661
            Height = 213
            Caption = 'Ordem de Compra (OC): '
            TabOrder = 2
            object Panel7: TPanel
              Left = 12
              Top = 20
              Width = 613
              Height = 181
              BevelOuter = bvNone
              TabOrder = 0
              object Label25: TLabel
                Left = 8
                Top = 136
                Width = 119
                Height = 13
                Caption = 'Dono da conta de e-mail:'
              end
              object Label23: TLabel
                Left = 8
                Top = 92
                Width = 76
                Height = 13
                Caption = 'E-mail CC Cega:'
              end
              object Label22: TLabel
                Left = 8
                Top = 48
                Width = 122
                Height = 13
                Caption = 'E-mail (ordem de compra):'
              end
              object Label21: TLabel
                Left = 8
                Top = 4
                Width = 152
                Height = 13
                Caption = 'Nome (e-mail ordem de compra):'
              end
              object Label26: TLabel
                Left = 308
                Top = 4
                Width = 76
                Height = 13
                Caption = 'Corpo do e-mail:'
              end
              object EdDonoMailOC: TdmkEdit
                Left = 8
                Top = 152
                Width = 293
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdMailCCCega: TdmkEdit
                Left = 8
                Top = 108
                Width = 293
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdMailOC: TdmkEdit
                Left = 8
                Top = 64
                Width = 293
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdNomeMailOC: TdmkEdit
                Left = 8
                Top = 20
                Width = 293
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCorpoMailOC: TMemo
                Left = 308
                Top = 20
                Width = 293
                Height = 153
                TabOrder = 4
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Miscel'#226'nea'
        ImageIndex = 1
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1055
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 143
            Height = 13
            Caption = 'Caminho do logo da duplicata:'
          end
          object Label2: TLabel
            Left = 4
            Top = 44
            Width = 132
            Height = 13
            Caption = 'Caminho do logo da receita:'
          end
          object SpeedButton2: TSpeedButton
            Left = 724
            Top = 60
            Width = 23
            Height = 22
            OnClick = SpeedButton2Click
          end
          object SpeedButton1: TSpeedButton
            Left = 724
            Top = 20
            Width = 23
            Height = 22
            OnClick = SpeedButton1Click
          end
          object Label14: TLabel
            Left = 388
            Top = 416
            Width = 143
            Height = 13
            Caption = 'Prod. m'#225'x. m'#178'/ dia acab. PCP:'
          end
          object EdPathLogoDupl: TEdit
            Left = 4
            Top = 20
            Width = 717
            Height = 21
            TabOrder = 0
          end
          object EdPathLogoRece: TEdit
            Left = 4
            Top = 60
            Width = 717
            Height = 21
            TabOrder = 1
          end
          object CkPQNegBaixa: TCheckBox
            Left = 4
            Top = 92
            Width = 488
            Height = 17
            Caption = 
              'Permite baixa de insumos quando houver ou gerar estoque negativo' +
              '.'
            TabOrder = 2
          end
          object CkPQNegEntra: TCheckBox
            Left = 4
            Top = 112
            Width = 488
            Height = 17
            Caption = 
              'Permite exclus'#227'o de entrada de insumos mesmo quando houver ou ge' +
              'rar estoque negativo.'
            TabOrder = 3
          end
          object CkCanAltBalA: TCheckBox
            Left = 4
            Top = 132
            Width = 488
            Height = 17
            Caption = 
              'Permite alterar estoque de insumos ap'#243's balancete (invent'#225'rio). ' +
              'DEPRECADO para Novo Balan'#231'o!!!'
            TabOrder = 4
          end
          object RGImpRecRib: TRadioGroup
            Left = 4
            Top = 154
            Width = 700
            Height = 65
            Caption = ' Apresenta'#231#227'o padr'#227'o da receita de ribeira: '
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              'Procure em:'
              ''
              'FmPrincipal.VAR_RecImpApresentaCol'
              'FmPrincipal.VAR_RecImpApresentaRol')
            TabOrder = 5
          end
          object RGVendaCouro: TRadioGroup
            Left = 4
            Top = 300
            Width = 189
            Height = 105
            Caption = ' Tipo de venda de couros: '
            Items.Strings = (
              'Nenhum (n'#227'o controla)'
              'Simples (pedidos)'
              'Completo (entrada de couros)')
            TabOrder = 7
          end
          object GroupBox1: TGroupBox
            Left = 196
            Top = 300
            Width = 337
            Height = 105
            Caption = ' Direcionamento de estoque na emiss'#227'o de pesagem: '
            TabOrder = 8
            object Label8: TLabel
              Left = 8
              Top = 16
              Width = 74
              Height = 13
              Caption = 'Setor de caleiro'
            end
            object Label9: TLabel
              Left = 8
              Top = 56
              Width = 95
              Height = 13
              Caption = 'Setor de curtimento:'
            end
            object EdSetorCal: TdmkEditCB
              Left = 8
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBSetorCal
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBSetorCal: TdmkDBLookupComboBox
              Left = 64
              Top = 32
              Width = 205
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsListaSetores1
              TabOrder = 1
              dmkEditCB = EdSetorCal
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object CBSetorCur: TdmkDBLookupComboBox
              Left = 64
              Top = 72
              Width = 205
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsListaSetores2
              TabOrder = 2
              dmkEditCB = EdSetorCur
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdSetorCur: TdmkEditCB
              Left = 8
              Top = 72
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBSetorCur
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
          end
          object RGVerImprRecRib: TdmkRadioGroup
            Left = 4
            Top = 408
            Width = 377
            Height = 49
            Caption = ' Vers'#227'o de impress'#227'o de receita de ribeira: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              '0 - Atual'
              '1 - Nova')
            TabOrder = 9
            UpdType = utYes
            OldValor = 0
          end
          object RGImpRecAcabto: TRadioGroup
            Left = 4
            Top = 226
            Width = 700
            Height = 65
            Caption = ' Apresenta'#231#227'o padr'#227'o da receita de recurtimento: '
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              'Procure em:'
              ''
              'FmPrincipal.VAR_RecImpApresentaCol'
              'FmPrincipal.VAR_RecImpApresentaRol')
            TabOrder = 6
          end
          object EdM2DiaAcabPCP: TdmkEdit
            Left = 388
            Top = 432
            Width = 145
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdCampo = 'M2DiaAcabPCP'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object CkNaoRecupImposPQ: TCheckBox
            Left = 504
            Top = 92
            Width = 313
            Height = 17
            Caption = 'N'#227'o recupera impostos na entrada de mat'#233'ria-prima e insumos.'
            TabOrder = 11
          end
          object CkGruUsrEdtRecei: TdmkCheckBox
            Left = 504
            Top = 112
            Width = 333
            Height = 17
            Caption = 'Permite somente grupos de usu'#225'rios editar receitas de produ'#231#227'o.'
            TabOrder = 12
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Plano de contas'
        ImageIndex = 2
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 1055
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label53: TLabel
            Left = 3
            Top = 8
            Width = 95
            Height = 13
            Caption = 'Compra de insumos:'
          end
          object Label4: TLabel
            Left = 3
            Top = 48
            Width = 79
            Height = 13
            Caption = 'Compra de M.P.:'
          end
          object Label6: TLabel
            Left = 3
            Top = 88
            Width = 111
            Height = 13
            Caption = 'Venda de Couro Verde:'
          end
          object Label7: TLabel
            Left = 391
            Top = 88
            Width = 157
            Height = 13
            Caption = 'Frete de venda de Mat'#233'ria-Prima:'
          end
          object Label5: TLabel
            Left = 391
            Top = 48
            Width = 67
            Height = 13
            Caption = 'Frete de M.P.:'
          end
          object Label3: TLabel
            Left = 391
            Top = 8
            Width = 83
            Height = 13
            Caption = 'Frete de insumos:'
          end
          object Label15: TLabel
            Left = 3
            Top = 128
            Width = 183
            Height = 13
            Caption = 'Comiss'#227'o na compra de Mat'#233'ria-Prima:'
          end
          object EdCtaPQCompr: TdmkEditCB
            Left = 3
            Top = 24
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCtaPQCompr
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdCtaMPCompr: TdmkEditCB
            Left = 3
            Top = 64
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCtaMPCompr
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCtaMPCompr: TdmkDBLookupComboBox
            Left = 62
            Top = 64
            Width = 327
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContasMPCompr
            TabOrder = 3
            dmkEditCB = EdCtaMPCompr
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CBCtaPQCompr: TdmkDBLookupComboBox
            Left = 62
            Top = 24
            Width = 327
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContasPQCompr
            TabOrder = 1
            dmkEditCB = EdCtaPQCompr
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCtaCVVenda: TdmkEditCB
            Left = 3
            Top = 104
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCtaCVVenda
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCtaCVVenda: TdmkDBLookupComboBox
            Left = 62
            Top = 104
            Width = 327
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContasCVVenda
            TabOrder = 5
            dmkEditCB = EdCtaCVVenda
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCtaCVFrete: TdmkEditCB
            Left = 391
            Top = 104
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCtaCVFrete
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCtaCVFrete: TdmkDBLookupComboBox
            Left = 450
            Top = 104
            Width = 327
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContasCVFrete
            TabOrder = 13
            dmkEditCB = EdCtaCVFrete
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CBCtaMPFrete: TdmkDBLookupComboBox
            Left = 450
            Top = 64
            Width = 327
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContasMPFrete
            TabOrder = 11
            dmkEditCB = EdCtaMPFrete
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCtaMPFrete: TdmkEditCB
            Left = 391
            Top = 64
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCtaMPFrete
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdCtaPQFrete: TdmkEditCB
            Left = 391
            Top = 24
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCtaPQFrete
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCtaPQFrete: TdmkDBLookupComboBox
            Left = 450
            Top = 24
            Width = 327
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContasPQFrete
            TabOrder = 9
            dmkEditCB = EdCtaPQFrete
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCtaComisCMP: TdmkEditCB
            Left = 3
            Top = 144
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCtaComisCMP
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCtaComisCMP: TdmkDBLookupComboBox
            Left = 62
            Top = 144
            Width = 327
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContasComisCMP
            TabOrder = 7
            dmkEditCB = EdCtaComisCMP
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' Produtos (grade ) '
        ImageIndex = 3
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1055
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label10: TLabel
            Left = 11
            Top = 68
            Width = 179
            Height = 13
            Caption = 'Tipo de Grupo de Produtos Qu'#237'micos:'
            Enabled = False
          end
          object SpeedButton3: TSpeedButton
            Left = 440
            Top = 84
            Width = 21
            Height = 21
            Caption = '...'
            Enabled = False
            OnClick = SpeedButton3Click
          end
          object Label11: TLabel
            Left = 11
            Top = 108
            Width = 282
            Height = 13
            Caption = 'Centro de estoque dos produtos qu'#237'micos (compatibilidade):'
          end
          object SpeedButton4: TSpeedButton
            Left = 440
            Top = 124
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton4Click
          end
          object Label16: TLabel
            Left = 12
            Top = 52
            Width = 582
            Height = 13
            Caption = 
              '* Casas decimais das quantidades de produto '#233' definido no cadast' +
              'ro do Tipo de Gruopo de Produto!   '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object RGCasasProd: TdmkRadioGroup
            Left = 12
            Top = 4
            Width = 449
            Height = 45
            Caption = ' Casas decimais nos pre'#231'os dos produtos: '
            Columns = 11
            ItemIndex = 0
            Items.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5'
              '6'
              '7'
              '8'
              '9'
              '10')
            TabOrder = 0
            UpdType = utYes
            OldValor = 0
          end
          object EdPQ_PrdGruTip: TdmkEditCB
            Left = 13
            Top = 84
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPQ_PrdGruTip
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBPQ_PrdGruTip: TdmkDBLookupComboBox
            Left = 70
            Top = 84
            Width = 370
            Height = 21
            Enabled = False
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsPrdGrupTip
            TabOrder = 2
            dmkEditCB = EdPQ_PrdGruTip
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdPQ_StqCenCad: TdmkEditCB
            Left = 13
            Top = 124
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPQ_StqCenCad
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBPQ_StqCenCad: TdmkDBLookupComboBox
            Left = 70
            Top = 124
            Width = 370
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsStqCenCad
            TabOrder = 4
            dmkEditCB = EdPQ_StqCenCad
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object RGWBNivGer: TdmkRadioGroup
            Left = 12
            Top = 152
            Width = 449
            Height = 45
            Caption = ' Forma de controle do estoque de Wet Blue para semi / acabado:'
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'Somente pe'#231'as'
              'Pe'#231'as, '#225'rea e peso'
              'Pe'#231'as, '#225'rea, peso e valor')
            TabOrder = 5
            QryCampo = 'WBNivGer'
            UpdCampo = 'WBNivGer'
            UpdType = utYes
            OldValor = 0
          end
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Semi / Acabado'
        ImageIndex = 4
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 1055
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object CkOSImpInfWB: TdmkCheckBox
            Left = 12
            Top = 8
            Width = 245
            Height = 17
            Caption = 'Imprime ficha de baixa de MP junto com a OS.'
            TabOrder = 0
            QryCampo = 'OSImpInfWB'
            UpdCampo = 'OSImpInfWB'
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkUsaM2Medio: TdmkCheckBox
            Left = 12
            Top = 28
            Width = 245
            Height = 17
            Caption = 'Usa '#225'rea m'#233'dia informada por fornecedor.'
            TabOrder = 1
            QryCampo = 'UsaM2Medio'
            UpdCampo = 'UsaM2Medio'
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkUsaBRLMedM2: TdmkCheckBox
            Left = 12
            Top = 48
            Width = 245
            Height = 17
            Caption = 'Usa pre'#231'o m'#233'dio informado por MP.'
            TabOrder = 2
            QryCampo = 'UsaBRLMedM2'
            UpdCampo = 'UsaBRLMedM2'
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkUsaEmitGru: TdmkCheckBox
            Left = 12
            Top = 68
            Width = 245
            Height = 17
            Caption = 'Exigir Grupo de emiss'#227'o nas baixas de Insumos.'
            TabOrder = 3
            QryCampo = 'UsaEmitGru'
            UpdCampo = 'UsaEmitGru'
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
        end
      end
      object TabSheet6: TTabSheet
        Caption = 'Couros VS'
        ImageIndex = 5
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 1055
          Height = 473
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Label12: TLabel
            Left = 12
            Top = 48
            Width = 163
            Height = 13
            Caption = 'Contra senha impress'#227'o QR Code:'
          end
          object Label13: TLabel
            Left = 12
            Top = 284
            Width = 83
            Height = 13
            Caption = 'DB Clareco CDR:'
          end
          object Label41: TLabel
            Left = 12
            Top = 352
            Width = 126
            Height = 13
            Caption = 'Centro de estoque padr'#227'o:'
          end
          object CkInfoMulFrnImpVS: TdmkCheckBox
            Left = 12
            Top = 28
            Width = 325
            Height = 17
            Caption = 'Mostrar fornecedores nas impress'#245'es das fichas de pallets.'
            TabOrder = 0
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkCanAltBalVS: TdmkCheckBox
            Left = 12
            Top = 8
            Width = 325
            Height = 17
            Caption = 'Permite alterar estoque de produtos ap'#243's invent'#225'rio'
            TabOrder = 1
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object RGQtdBoxClas: TdmkRadioGroup
            Left = 358
            Top = 4
            Width = 407
            Height = 45
            Caption = ' Boxes iniciais na classifica'#231#227'o e reclassifica'#231#227'o:'
            Columns = 6
            ItemIndex = 0
            Items.Strings = (
              'Indefinido'
              'Perguntar'
              '6 boxes'
              '9 boxes'
              '12 boxes'
              '15 boxes')
            TabOrder = 2
            QryCampo = 'QtdBoxClas'
            UpdCampo = 'QtdBoxClas'
            UpdType = utYes
            OldValor = 0
          end
          object EdVSImpRandStr: TdmkEdit
            Left = 12
            Top = 64
            Width = 753
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object CGNObrigNFeVS: TdmkCheckGroup
            Left = 12
            Top = 91
            Width = 753
            Height = 122
            Margins.Left = 2
            Margins.Top = 2
            Margins.Right = 2
            Margins.Bottom = 2
            Caption = ' N'#227'o obrigar preenchimento de NF-e nas seguintes opera'#231#245'es '
            Columns = 3
            Items.Strings = (
              'VS_PF.ObtemListaOperacoes()')
            TabOrder = 4
            UpdType = utYes
            Value = 0
            OldValor = 0
          end
          object CkVSWarSemNF: TdmkCheckBox
            Left = 12
            Top = 220
            Width = 325
            Height = 17
            Caption = 'Avisa lan'#231'amentos sem defini'#231#227'o de NFe de origem/destino.'
            TabOrder = 5
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkVSWarNoFrn: TdmkCheckBox
            Left = 12
            Top = 240
            Width = 345
            Height = 17
            Caption = 'Avisa lan'#231'amentos sem defini'#231#227'o de fornecedor de origem.'
            TabOrder = 6
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkVSInsPalManu: TdmkCheckBox
            Left = 12
            Top = 260
            Width = 345
            Height = 17
            Caption = 'Inclui pallets informando numero manualmente.'
            TabOrder = 7
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object EdDBClarecoCDR: TdmkEdit
            Left = 12
            Top = 300
            Width = 753
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object CkVSImpMediaPall: TdmkCheckBox
            Left = 12
            Top = 328
            Width = 345
            Height = 17
            Caption = 'Imprime m'#233'dia na ficha do pallet.'
            TabOrder = 9
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
          object EdStqCenCadEstqPall_TXT: TdmkEdit
            Left = 68
            Top = 368
            Width = 697
            Height = 21
            TabStop = False
            Enabled = False
            ReadOnly = True
            TabOrder = 10
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdStqCenCadEstqPall: TdmkEditEdIdx
            Left = 12
            Top = 368
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DmkEditEdTxt = EdStqCenCadEstqPall_TXT
            IgnoradmkEdit = False
            AutoSetIfOnlyOneReg = setregOnlyManual
            LocF7CodiFldName = 'Codigo'
            LocF7NameFldName = 'Nome'
            LocF7TableName = 'stqcencad'
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
            LocF7DmkDataBase = ddluMyDB
          end
        end
      end
      object TsSPED: TTabSheet
        Caption = 'SPED EFD1'
        ImageIndex = 6
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 1055
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox3: TGroupBox
            Left = 0
            Top = 0
            Width = 1055
            Height = 437
            Align = alTop
            Caption = ' Registros SPED ICMS/IPI: '
            TabOrder = 0
            object RGSPED_II_PDA: TdmkRadioGroup
              Left = 2
              Top = 15
              Width = 1051
              Height = 38
              Align = alTop
              Caption = ' In natura >> Pr'#233'-descarne: '
              Columns = 5
              Enabled = False
              Items.Strings = (
                'Desmonte'
                'Classe/reclasse'
                'Produ'#231#227'o isolada'
                'Produ'#231#227'o conjunta'
                'Reprocessamento/reparo')
              TabOrder = 0
              UpdType = utYes
              OldValor = 0
            end
            object RGSPED_II_EmCal: TdmkRadioGroup
              Left = 2
              Top = 53
              Width = 1051
              Height = 38
              Align = alTop
              Caption = ' Pr'#233'-descarne >> Em caleiro: '
              Columns = 5
              Items.Strings = (
                'Desmonte'
                'Classe/reclasse'
                'Produ'#231#227'o isolada'
                'Produ'#231#227'o conjunta'
                'Reprocessamento/reparo')
              TabOrder = 1
              UpdType = utYes
              OldValor = 0
            end
            object RGSPED_II_Caleado: TdmkRadioGroup
              Left = 2
              Top = 91
              Width = 1051
              Height = 38
              Align = alTop
              Caption = ' Em caleiro >> Caleado: '
              Columns = 5
              Items.Strings = (
                'Desmonte'
                'Classe/reclasse'
                'Produ'#231#227'o isolada'
                'Produ'#231#227'o conjunta'
                'Reprocessamento/reparo')
              TabOrder = 2
              UpdType = utYes
              OldValor = 0
            end
            object RGSPED_II_DTA: TdmkRadioGroup
              Left = 2
              Top = 129
              Width = 1051
              Height = 38
              Align = alTop
              Caption = ' Caleado >> Tripa p'#243's divisora: '
              Columns = 5
              Enabled = False
              Items.Strings = (
                'Desmonte'
                'Classe/reclasse'
                'Produ'#231#227'o isolada'
                'Produ'#231#227'o conjunta'
                'Reprocessamento/reparo')
              TabOrder = 3
              UpdType = utYes
              OldValor = 0
            end
            object RGSPED_II_EmCur: TdmkRadioGroup
              Left = 2
              Top = 167
              Width = 1051
              Height = 38
              Align = alTop
              Caption = '  Tripa p'#243's divisora >> Em curtimento: '
              Columns = 5
              Items.Strings = (
                'Desmonte'
                'Classe/reclasse'
                'Produ'#231#227'o isolada'
                'Produ'#231#227'o conjunta'
                'Reprocessamento/reparo')
              TabOrder = 4
              UpdType = utYes
              OldValor = 0
            end
            object RGSPED_II_ArtCur: TdmkRadioGroup
              Left = 2
              Top = 243
              Width = 1051
              Height = 38
              Align = alTop
              Caption = '  Curtido >> Artigo gerado: '
              Columns = 5
              Items.Strings = (
                'Desmonte'
                'Classe/reclasse'
                'Produ'#231#227'o isolada'
                'Produ'#231#227'o conjunta'
                'Reprocessamento/reparo')
              TabOrder = 5
              UpdType = utYes
              OldValor = 0
            end
            object RGSPED_II_Operacao: TdmkRadioGroup
              Left = 2
              Top = 281
              Width = 1051
              Height = 38
              Align = alTop
              Caption = '  Opera'#231#245'es: '
              Columns = 5
              Items.Strings = (
                'Desmonte'
                'Classe/reclasse'
                'Produ'#231#227'o isolada'
                'Produ'#231#227'o conjunta'
                'Reprocessamento/reparo')
              TabOrder = 6
              UpdType = utYes
              OldValor = 0
            end
            object RGSPED_II_Recurt: TdmkRadioGroup
              Left = 2
              Top = 319
              Width = 1051
              Height = 38
              Align = alTop
              Caption = '  Semi / Acabado: '
              Columns = 5
              Items.Strings = (
                'Desmonte'
                'Classe/reclasse'
                'Produ'#231#227'o isolada'
                'Produ'#231#227'o conjunta'
                'Reprocessamento/reparo')
              TabOrder = 7
              UpdType = utYes
              OldValor = 0
            end
            object RGSPED_II_Curtido: TdmkRadioGroup
              Left = 2
              Top = 205
              Width = 1051
              Height = 38
              Align = alTop
              Caption = '  Em curtimento >> Curtido: '
              Columns = 5
              Items.Strings = (
                'Desmonte'
                'Classe/reclasse'
                'Produ'#231#227'o isolada'
                'Produ'#231#227'o conjunta'
                'Reprocessamento/reparo')
              TabOrder = 8
              UpdType = utYes
              OldValor = 0
            end
            object RGSPED_II_SubProd: TdmkRadioGroup
              Left = 2
              Top = 357
              Width = 1051
              Height = 38
              Align = alTop
              Caption = '  Processamento de sub-produtos: '
              Columns = 5
              Items.Strings = (
                'Desmonte'
                'Classe/reclasse'
                'Produ'#231#227'o isolada'
                'Produ'#231#227'o conjunta'
                'Reprocessamento/reparo')
              TabOrder = 9
              UpdType = utYes
              OldValor = 0
            end
            object RGSPED_II_Reparo: TdmkRadioGroup
              Left = 2
              Top = 395
              Width = 1051
              Height = 38
              Align = alTop
              Caption = '  Reprocessamento / reparo '
              Columns = 5
              Enabled = False
              Items.Strings = (
                'Desmonte'
                'Classe/reclasse'
                'Produ'#231#227'o isolada'
                'Produ'#231#227'o conjunta'
                'Reprocessamento/reparo')
              TabOrder = 10
              UpdType = utYes
              OldValor = 0
            end
          end
          object BtPadroesSPED_II: TBitBtn
            Left = 280
            Top = 439
            Width = 229
            Height = 32
            Caption = 'Padr'#245'es registros SPED ICMS/IPI'
            TabOrder = 1
            OnClick = BtPadroesSPED_IIClick
          end
        end
      end
      object TabSheet7: TTabSheet
        Caption = 'SPED EFD 2'
        ImageIndex = 7
        object Panel12: TPanel
          Left = 0
          Top = 0
          Width = 1055
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object PageControl1: TPageControl
            Left = 0
            Top = 0
            Width = 1055
            Height = 473
            ActivePage = TabSheet8
            Align = alClient
            TabOrder = 0
            object TabSheet8: TTabSheet
              Caption = ' Entrada de mat'#233'ria-prima'
              object Panel13: TPanel
                Left = 0
                Top = 0
                Width = 1047
                Height = 445
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Panel14: TPanel
                  Left = 0
                  Top = 0
                  Width = 1047
                  Height = 81
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label66: TLabel
                    Left = 10
                    Top = 1
                    Width = 66
                    Height = 13
                    Caption = 'CFOP interno:'
                  end
                  object SbCreTrib_MP_CFOP_DE: TSpeedButton
                    Left = 853
                    Top = 15
                    Width = 23
                    Height = 22
                    Caption = '...'
                    OnClick = SbCreTrib_MP_CFOP_DEClick
                  end
                  object Label29: TLabel
                    Left = 10
                    Top = 41
                    Width = 94
                    Height = 13
                    Caption = 'CFOP interestadual:'
                  end
                  object SbCreTrib_MP_CFOP_FE: TSpeedButton
                    Left = 853
                    Top = 55
                    Width = 23
                    Height = 22
                    Caption = '...'
                    OnClick = SbCreTrib_MP_CFOP_FEClick
                  end
                  object EdCreTrib_MP_CFOP_DE: TdmkEditCB
                    Left = 10
                    Top = 16
                    Width = 55
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBCreTrib_MP_CFOP_DE
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBCreTrib_MP_CFOP_DE: TdmkDBLookupComboBox
                    Left = 65
                    Top = 16
                    Width = 784
                    Height = 21
                    Color = clWhite
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsCreTrib_MP_CFOP_DE
                    TabOrder = 1
                    dmkEditCB = EdCreTrib_MP_CFOP_DE
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                    LocF7PreDefProc = f7pNone
                  end
                  object EdCreTrib_MP_CFOP_FE: TdmkEditCB
                    Left = 10
                    Top = 56
                    Width = 55
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBCreTrib_MP_CFOP_FE
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBCreTrib_MP_CFOP_FE: TdmkDBLookupComboBox
                    Left = 65
                    Top = 56
                    Width = 784
                    Height = 21
                    Color = clWhite
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsCreTrib_MP_CFOP_FE
                    TabOrder = 3
                    dmkEditCB = EdCreTrib_MP_CFOP_FE
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                    LocF7PreDefProc = f7pNone
                  end
                end
                object GBRecuImpost: TGroupBox
                  Left = 0
                  Top = 81
                  Width = 1047
                  Height = 228
                  Align = alTop
                  Caption = ' Recupera'#231#227'o de impostos: '
                  TabOrder = 1
                  object Panel15: TPanel
                    Left = 2
                    Top = 15
                    Width = 1043
                    Height = 211
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label17: TLabel
                      Left = 9
                      Top = 160
                      Width = 191
                      Height = 13
                      Caption = 'ICMSST (retorno: BC , % e valor):'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label18: TLabel
                      Left = 53
                      Top = 80
                      Width = 165
                      Height = 13
                      Caption = 'PIS (retorno: BC , % e valor):'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label19: TLabel
                      Left = 53
                      Top = 120
                      Width = 190
                      Height = 13
                      Caption = 'COFINS (retorno: BC , % e valor):'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label27: TLabel
                      Left = 53
                      Top = 40
                      Width = 130
                      Height = 13
                      Caption = 'IPI (retorno: BC , % e valor):'
                    end
                    object Label476: TLabel
                      Left = 10
                      Top = 0
                      Width = 35
                      Height = 13
                      Caption = 'ICMS:'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label477: TLabel
                      Left = 8
                      Top = 40
                      Width = 13
                      Height = 13
                      Caption = 'IPI'
                    end
                    object Label478: TLabel
                      Left = 8
                      Top = 80
                      Width = 21
                      Height = 13
                      Caption = 'PIS'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label480: TLabel
                      Left = 8
                      Top = 120
                      Width = 46
                      Height = 13
                      Caption = 'COFINS'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label28: TLabel
                      Left = 57
                      Top = 0
                      Width = 160
                      Height = 13
                      Caption = 'ICMS ST (retorno: BC , % e valor):'
                    end
                    object EdCreTrib_MP_ICMS_ALIQ_ST: TdmkEdit
                      Left = 56
                      Top = 176
                      Width = 40
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 2
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object EdCreTrib_MP_PIS_ALIQ: TdmkEdit
                      Left = 56
                      Top = 96
                      Width = 40
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 7
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object EdCreTrib_MP_COFINS_ALIQ: TdmkEdit
                      Left = 56
                      Top = 136
                      Width = 40
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 10
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object EdCreTrib_MP_IPI_ALIQ: TdmkEdit
                      Left = 56
                      Top = 56
                      Width = 40
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 4
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object EdCreTrib_MP_ICMS_CST: TdmkEdit
                      Left = 8
                      Top = 16
                      Width = 45
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '0'
                      ValMax = '90'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      QryName = 'QrNFeItsN'
                      QryCampo = 'UC_ICMS_CST_B'
                      UpdCampo = 'UC_ICMS_CST_B'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnChange = EdCreTrib_MP_ICMS_CSTChange
                      OnKeyDown = EdCreTrib_MP_ICMS_CSTKeyDown
                    end
                    object EdCreTrib_MP_IPI_CST: TdmkEdit
                      Left = 8
                      Top = 56
                      Width = 45
                      Height = 21
                      TabOrder = 3
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 2
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '0'
                      ValMax = '99'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '00'
                      QryName = 'QrNFeItsO'
                      QryCampo = 'UC_IPI_CST'
                      UpdCampo = 'UC_IPI_CST'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = '00'
                      ValWarn = False
                      OnChange = EdCreTrib_MP_IPI_CSTChange
                      OnKeyDown = EdCreTrib_MP_IPI_CSTKeyDown
                    end
                    object EdCreTrib_MP_IPI_CST_TXT: TdmkEdit
                      Left = 98
                      Top = 56
                      Width = 740
                      Height = 21
                      TabStop = False
                      ReadOnly = True
                      TabOrder = 5
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdCreTrib_MP_PIS_CST: TdmkEdit
                      Left = 8
                      Top = 96
                      Width = 45
                      Height = 21
                      TabOrder = 6
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 2
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMax = '99'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryName = 'QrNFeItsQ'
                      QryCampo = 'UC_PIS_CST'
                      UpdCampo = 'UC_PIS_CST'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                      OnChange = EdCreTrib_MP_PIS_CSTChange
                      OnKeyDown = EdCreTrib_MP_PIS_CSTKeyDown
                    end
                    object EdCreTrib_MP_PIS_CST_TXT: TdmkEdit
                      Left = 98
                      Top = 96
                      Width = 740
                      Height = 21
                      TabStop = False
                      ReadOnly = True
                      TabOrder = 8
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdCreTrib_MP_COFINS_CST: TdmkEdit
                      Left = 8
                      Top = 136
                      Width = 45
                      Height = 21
                      TabOrder = 9
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 2
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMax = '99'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryName = 'QrNFeItsS'
                      QryCampo = 'UC_COFINS_CST'
                      UpdCampo = 'UC_COFINS_CST'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                      OnChange = EdCreTrib_MP_COFINS_CSTChange
                      OnKeyDown = EdCreTrib_MP_COFINS_CSTKeyDown
                    end
                    object EdCreTrib_MP_COFINS_CST_TXT: TdmkEdit
                      Left = 98
                      Top = 136
                      Width = 740
                      Height = 21
                      TabStop = False
                      ReadOnly = True
                      TabOrder = 11
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdCreTrib_MP_ICMS_ALIQ: TdmkEdit
                      Left = 56
                      Top = 16
                      Width = 40
                      Height = 21
                      TabStop = False
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object EdCreTrib_MP_ICMS_CST_TXT: TdmkEdit
                      Left = 98
                      Top = 16
                      Width = 740
                      Height = 21
                      TabStop = False
                      ReadOnly = True
                      TabOrder = 12
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdCreTrib_MP_TES_ICMS_TXT: TdmkEdit
                      Left = 865
                      Top = 16
                      Width = 168
                      Height = 21
                      TabStop = False
                      ReadOnly = True
                      TabOrder = 13
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdCreTrib_MP_TES_ICMS: TdmkEdit
                      Left = 840
                      Top = 16
                      Width = 21
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 14
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 1
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '0'
                      ValMax = '2'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryName = 'QrNFeItsN'
                      QryCampo = 'UC_ICMS_CST_B'
                      UpdCampo = 'TES_ICMS'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnChange = EdCreTrib_MP_TES_ICMSChange
                      OnKeyDown = EdCreTrib_MP_TES_ICMSKeyDown
                    end
                    object EdCreTrib_MP_TES_IPI: TdmkEdit
                      Left = 840
                      Top = 56
                      Width = 21
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 15
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 1
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '0'
                      ValMax = '2'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryName = 'QrNFeItsN'
                      QryCampo = 'UC_ICMS_CST_B'
                      UpdCampo = 'TES_IPI'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnChange = EdCreTrib_MP_TES_IPIChange
                      OnKeyDown = EdCreTrib_MP_TES_IPIKeyDown
                    end
                    object EdCreTrib_MP_TES_IPI_TXT: TdmkEdit
                      Left = 865
                      Top = 56
                      Width = 168
                      Height = 21
                      TabStop = False
                      ReadOnly = True
                      TabOrder = 16
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdCreTrib_MP_TES_PIS: TdmkEdit
                      Left = 840
                      Top = 96
                      Width = 21
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 17
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 1
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '0'
                      ValMax = '2'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryName = 'QrNFeItsN'
                      QryCampo = 'UC_ICMS_CST_B'
                      UpdCampo = 'TES_PIS'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnChange = EdCreTrib_MP_TES_PISChange
                      OnKeyDown = EdCreTrib_MP_TES_PISKeyDown
                    end
                    object EdCreTrib_MP_TES_PIS_TXT: TdmkEdit
                      Left = 865
                      Top = 96
                      Width = 168
                      Height = 21
                      TabStop = False
                      ReadOnly = True
                      TabOrder = 18
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdCreTrib_MP_TES_COFINS: TdmkEdit
                      Left = 840
                      Top = 136
                      Width = 21
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 19
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 1
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '0'
                      ValMax = '2'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryName = 'QrNFeItsN'
                      QryCampo = 'UC_ICMS_CST_B'
                      UpdCampo = 'TES_COFINS'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnChange = EdCreTrib_MP_TES_COFINSChange
                      OnKeyDown = EdCreTrib_MP_TES_COFINSKeyDown
                    end
                    object EdCreTrib_MP_TES_COFINS_TXT: TdmkEdit
                      Left = 865
                      Top = 136
                      Width = 168
                      Height = 21
                      TabStop = False
                      ReadOnly = True
                      TabOrder = 20
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                  end
                end
              end
            end
            object TabSheet9: TTabSheet
              Caption = 'Remessa de mat'#233'ria-prima'
              ImageIndex = 1
              object Panel16: TPanel
                Left = 0
                Top = 0
                Width = 1047
                Height = 445
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Panel17: TPanel
                  Left = 0
                  Top = 0
                  Width = 1047
                  Height = 81
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label30: TLabel
                    Left = 10
                    Top = 1
                    Width = 66
                    Height = 13
                    Caption = 'CFOP interno:'
                  end
                  object Label31: TLabel
                    Left = 10
                    Top = 41
                    Width = 94
                    Height = 13
                    Caption = 'CFOP interestadual:'
                  end
                  object EdRemTrib_MP_CFOP_DE: TdmkEditCB
                    Left = 10
                    Top = 16
                    Width = 55
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBRemTrib_MP_CFOP_DE
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBRemTrib_MP_CFOP_DE: TdmkDBLookupComboBox
                    Left = 65
                    Top = 16
                    Width = 784
                    Height = 21
                    Color = clWhite
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsRemTrib_MP_CFOP_DE
                    TabOrder = 1
                    dmkEditCB = EdRemTrib_MP_CFOP_DE
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                    LocF7PreDefProc = f7pNone
                  end
                  object EdRemTrib_MP_CFOP_FE: TdmkEditCB
                    Left = 10
                    Top = 56
                    Width = 55
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBRemTrib_MP_CFOP_FE
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBRemTrib_MP_CFOP_FE: TdmkDBLookupComboBox
                    Left = 65
                    Top = 56
                    Width = 784
                    Height = 21
                    Color = clWhite
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsRemTrib_MP_CFOP_FE
                    TabOrder = 3
                    dmkEditCB = EdRemTrib_MP_CFOP_FE
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                    LocF7PreDefProc = f7pNone
                  end
                end
                object GroupBox4: TGroupBox
                  Left = 0
                  Top = 81
                  Width = 1047
                  Height = 228
                  Align = alTop
                  Caption = ' Recupera'#231#227'o de impostos: '
                  TabOrder = 1
                  object Panel18: TPanel
                    Left = 2
                    Top = 15
                    Width = 1043
                    Height = 211
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label32: TLabel
                      Left = 9
                      Top = 160
                      Width = 191
                      Height = 13
                      Caption = 'ICMSST (retorno: BC , % e valor):'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label33: TLabel
                      Left = 53
                      Top = 80
                      Width = 165
                      Height = 13
                      Caption = 'PIS (retorno: BC , % e valor):'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label34: TLabel
                      Left = 53
                      Top = 120
                      Width = 190
                      Height = 13
                      Caption = 'COFINS (retorno: BC , % e valor):'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label35: TLabel
                      Left = 53
                      Top = 40
                      Width = 130
                      Height = 13
                      Caption = 'IPI (retorno: BC , % e valor):'
                    end
                    object Label36: TLabel
                      Left = 10
                      Top = 0
                      Width = 35
                      Height = 13
                      Caption = 'ICMS:'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label37: TLabel
                      Left = 8
                      Top = 40
                      Width = 13
                      Height = 13
                      Caption = 'IPI'
                    end
                    object Label38: TLabel
                      Left = 8
                      Top = 80
                      Width = 21
                      Height = 13
                      Caption = 'PIS'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label39: TLabel
                      Left = 8
                      Top = 120
                      Width = 46
                      Height = 13
                      Caption = 'COFINS'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label40: TLabel
                      Left = 57
                      Top = 0
                      Width = 160
                      Height = 13
                      Caption = 'ICMS ST (retorno: BC , % e valor):'
                    end
                    object EdRemTrib_MP_ICMS_ALIQ_ST: TdmkEdit
                      Left = 56
                      Top = 176
                      Width = 40
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 2
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object EdRemTrib_MP_PIS_ALIQ: TdmkEdit
                      Left = 56
                      Top = 96
                      Width = 40
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 7
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object EdRemTrib_MP_COFINS_ALIQ: TdmkEdit
                      Left = 56
                      Top = 136
                      Width = 40
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 10
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object EdRemTrib_MP_IPI_ALIQ: TdmkEdit
                      Left = 56
                      Top = 56
                      Width = 40
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 4
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object EdRemTrib_MP_ICMS_CST: TdmkEdit
                      Left = 8
                      Top = 16
                      Width = 45
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '0'
                      ValMax = '90'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      QryName = 'QrNFeItsN'
                      QryCampo = 'UC_ICMS_CST_B'
                      UpdCampo = 'UC_ICMS_CST_B'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnChange = EdRemTrib_MP_ICMS_CSTChange
                      OnKeyDown = EdRemTrib_MP_ICMS_CSTKeyDown
                    end
                    object EdRemTrib_MP_IPI_CST: TdmkEdit
                      Left = 8
                      Top = 56
                      Width = 45
                      Height = 21
                      TabOrder = 3
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 2
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '0'
                      ValMax = '99'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '00'
                      QryName = 'QrNFeItsO'
                      QryCampo = 'UC_IPI_CST'
                      UpdCampo = 'UC_IPI_CST'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = '00'
                      ValWarn = False
                      OnChange = EdRemTrib_MP_IPI_CSTChange
                      OnKeyDown = EdRemTrib_MP_IPI_CSTKeyDown
                    end
                    object EdRemTrib_MP_IPI_CST_TXT: TdmkEdit
                      Left = 98
                      Top = 56
                      Width = 940
                      Height = 21
                      TabStop = False
                      ReadOnly = True
                      TabOrder = 5
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdRemTrib_MP_PIS_CST: TdmkEdit
                      Left = 8
                      Top = 96
                      Width = 45
                      Height = 21
                      TabOrder = 6
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 2
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMax = '99'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryName = 'QrNFeItsQ'
                      QryCampo = 'UC_PIS_CST'
                      UpdCampo = 'UC_PIS_CST'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                      OnChange = EdRemTrib_MP_PIS_CSTChange
                      OnKeyDown = EdRemTrib_MP_PIS_CSTKeyDown
                    end
                    object EdRemTrib_MP_PIS_CST_TXT: TdmkEdit
                      Left = 98
                      Top = 96
                      Width = 940
                      Height = 21
                      TabStop = False
                      ReadOnly = True
                      TabOrder = 8
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdRemTrib_MP_COFINS_CST: TdmkEdit
                      Left = 8
                      Top = 136
                      Width = 45
                      Height = 21
                      TabOrder = 9
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 2
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMax = '99'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryName = 'QrNFeItsS'
                      QryCampo = 'UC_COFINS_CST'
                      UpdCampo = 'UC_COFINS_CST'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                      OnChange = EdRemTrib_MP_COFINS_CSTChange
                      OnKeyDown = EdRemTrib_MP_COFINS_CSTKeyDown
                    end
                    object EdRemTrib_MP_COFINS_CST_TXT: TdmkEdit
                      Left = 98
                      Top = 136
                      Width = 940
                      Height = 21
                      TabStop = False
                      ReadOnly = True
                      TabOrder = 11
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdRemTrib_MP_ICMS_ALIQ: TdmkEdit
                      Left = 56
                      Top = 16
                      Width = 40
                      Height = 21
                      TabStop = False
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object EdRemTrib_MP_ICMS_CST_TXT: TdmkEdit
                      Left = 98
                      Top = 16
                      Width = 940
                      Height = 21
                      TabStop = False
                      ReadOnly = True
                      TabOrder = 12
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1063
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1015
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 967
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 401
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 401
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 401
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 549
    Width = 1063
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 1059
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 593
    Width = 1063
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 1059
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 915
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 6
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 14
        Top = 3
        Width = 119
        Height = 40
        Caption = '&Salva'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 
      'All (*.gif;*.png;*.jpg;*.jpeg;*.bmp;*.tif;*.tiff;*.ico;*.emf;*.w' +
      'mf)|*.gif;*.png;*.jpg;*.jpeg;*.bmp;*.tif;*.tiff;*.ico;*.emf;*.wm' +
      'f|GIF Image (*.gif)|*.gif|Portable Network Graphics (*.png)|*.pn' +
      'g|JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|' +
      'Bitmaps (*.bmp)|*.bmp|TIFF Images (*.tif)|*.tif|TIFF Images (*.t' +
      'iff)|*.tiff|Icons (*.ico)|*.ico|Enhanced Metafiles (*.emf)|*.emf' +
      '|Metafiles (*.wmf)|*.wmf'
    Left = 1029
    Top = 69
  end
  object QrContasPQCompr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 213
    Top = 69
    object QrContasPQComprCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object QrContasPQComprNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
  end
  object DsContasPQCompr: TDataSource
    DataSet = QrContasPQCompr
    Left = 213
    Top = 117
  end
  object QrListaSetores1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM listasetores'
      'ORDER BY Nome')
    Left = 696
    Top = 272
    object QrListaSetores1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaSetores1Nome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object DsListaSetores1: TDataSource
    DataSet = QrListaSetores1
    Left = 696
    Top = 316
  end
  object QrListaSetores2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM listasetores'
      'ORDER BY Nome')
    Left = 696
    Top = 360
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
    end
  end
  object DsListaSetores2: TDataSource
    DataSet = QrListaSetores2
    Left = 696
    Top = 404
  end
  object QrPrdGrupTip: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM prdgruptip'
      'WHERE TipPrd=3'
      'ORDER BY Nome')
    Left = 772
    Top = 272
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 772
    Top = 320
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 772
    Top = 364
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 776
    Top = 408
  end
  object PMPadroesSPED_II: TPopupMenu
    Left = 376
    Top = 560
    object Isolada1: TMenuItem
      Tag = 1
      Caption = '&Isolada'
      OnClick = Isolada1Click
    end
    object Conjunta1: TMenuItem
      Tag = 2
      Caption = '&Conjunta'
      OnClick = Conjunta1Click
    end
  end
  object QrContasMPCompr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 317
    Top = 69
    object QrContasMPComprCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object QrContasMPComprNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
  end
  object DsContasMPCompr: TDataSource
    DataSet = QrContasMPCompr
    Left = 317
    Top = 117
  end
  object QrContasCVVenda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 213
    Top = 169
    object QrContasCVVendaCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object QrContasCVVendaNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
  end
  object DsContasCVVenda: TDataSource
    DataSet = QrContasCVVenda
    Left = 213
    Top = 217
  end
  object QrContasComisCMP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 317
    Top = 173
    object QrContasComisCMPCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object QrContasComisCMPNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
  end
  object DsContasComisCMP: TDataSource
    DataSet = QrContasComisCMP
    Left = 317
    Top = 221
  end
  object QrContasPQFrete: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 569
    Top = 73
    object QrContasPQFreteCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object QrContasPQFreteNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
  end
  object DsContasPQFrete: TDataSource
    DataSet = QrContasPQFrete
    Left = 569
    Top = 121
  end
  object QrContasMPFrete: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 685
    Top = 73
    object QrContasMPFreteCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object QrContasMPFreteNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
  end
  object DsContasMPFrete: TDataSource
    DataSet = QrContasMPFrete
    Left = 685
    Top = 121
  end
  object QrContasCVFrete: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 569
    Top = 169
    object QrContasCVFreteCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object QrContasCVFreteNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
  end
  object DsContasCVFrete: TDataSource
    DataSet = QrContasCVFrete
    Left = 569
    Top = 217
  end
  object QrCreTrib_MP_CFOP_DE: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 912
    Top = 17
    object QrCreTrib_MP_CFOP_DECodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrCreTrib_MP_CFOP_DENome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCreTrib_MP_CFOP_DE: TDataSource
    DataSet = QrCreTrib_MP_CFOP_DE
    Left = 912
    Top = 66
  end
  object QrCreTrib_MP_CFOP_FE: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 916
    Top = 121
    object QrCreTrib_MP_CFOP_FECodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrCreTrib_MP_CFOP_FENome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCreTrib_MP_CFOP_FE: TDataSource
    DataSet = QrCreTrib_MP_CFOP_FE
    Left = 916
    Top = 170
  end
  object QrRemTrib_MP_CFOP_FE: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 916
    Top = 321
    object QrRemTrib_MP_CFOP_FECodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrRemTrib_MP_CFOP_FENome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsRemTrib_MP_CFOP_FE: TDataSource
    DataSet = QrRemTrib_MP_CFOP_FE
    Left = 916
    Top = 370
  end
  object QrRemTrib_MP_CFOP_DE: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 916
    Top = 221
    object QrRemTrib_MP_CFOP_DECodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrRemTrib_MP_CFOP_DENome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsRemTrib_MP_CFOP_DE: TDataSource
    DataSet = QrRemTrib_MP_CFOP_DE
    Left = 916
    Top = 270
  end
end
