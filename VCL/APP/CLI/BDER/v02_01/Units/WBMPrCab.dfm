object FmWBMPrCab: TFmWBMPrCab
  Left = 368
  Top = 194
  Caption = 
    'WET-RECUR-013 :: Configura'#231#227'o de Mat'#233'ria-prima para Semi / Acaba' +
    'do'
  ClientHeight = 537
  ClientWidth = 919
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 919
    Height = 441
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 172
    ExplicitTop = 212
    object GBConfirma: TGroupBox
      Left = 0
      Top = 378
      Width = 919
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 779
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 919
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 48
        Height = 13
        Caption = 'Reduzido:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 129
        Height = 13
        Caption = 'Descri'#231#227'o / tamanho / cor:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 772
        Top = 16
        Width = 34
        Height = 13
        Caption = 'Ordem:'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 668
        Top = 16
        Width = 59
        Height = 13
        Caption = '$ /m'#178' m'#233'dio:'
        FocusControl = DBEdit1
      end
      object EdGraGruX: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GraGruX'
        UpdCampo = 'GraGruX'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdTxtNomeTamCor: TdmkEdit
        Left = 76
        Top = 32
        Width = 589
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NO_PRD_TAM_COR'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdOrdem: TdmkEdit
        Left = 772
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Ordem'
        UpdCampo = 'Ordem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdBRLMedM2: TdmkEdit
        Left = 668
        Top = 32
        Width = 100
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'BRLMedM2'
        UpdCampo = 'BRLMedM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 919
    Height = 441
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 296
    ExplicitTop = 248
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 919
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 772
        Top = 16
        Width = 34
        Height = 13
        Caption = 'Ordem:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 668
        Top = 16
        Width = 59
        Height = 13
        Caption = '$ /m'#178' m'#233'dio:'
        FocusControl = DBEdit2
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'GraGruX'
        DataSource = DsWBMPrCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 589
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsWBMPrCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 772
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Ordem'
        DataSource = DsWBMPrCab
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 668
        Top = 32
        Width = 100
        Height = 21
        DataField = 'BRLMedM2'
        DataSource = DsWBMPrCab
        TabOrder = 3
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 377
      Width = 919
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 396
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtFornece: TBitBtn
          Tag = 10062
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fornecedor'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtForneceClick
        end
        object BtMP: TBitBtn
          Tag = 224
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&M.Prima'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtMPClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object DBGWBMPrFrn: TdmkDBGridZTO
      Left = 0
      Top = 61
      Width = 919
      Height = 132
      Align = alTop
      DataSource = DsWBMPrFrn
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fornece'
          Title.Caption = 'Fornecedor'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Nome do fornecedor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'M2Medio'
          Title.Caption = 'M'#233'dia m'#178' / pe'#231'a'
          Width = 84
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 919
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 871
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 655
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 635
        Height = 32
        Caption = 'Configura'#231#227'o de Mat'#233'ria-prima para Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 635
        Height = 32
        Caption = 'Configura'#231#227'o de Mat'#233'ria-prima para Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 635
        Height = 32
        Caption = 'Configura'#231#227'o de Mat'#233'ria-prima para Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 919
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 915
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrWBMPrCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrWBMPrCabBeforeOpen
    AfterOpen = QrWBMPrCabAfterOpen
    BeforeClose = QrWBMPrCabBeforeClose
    AfterScroll = QrWBMPrCabAfterScroll
    SQL.Strings = (
      'SELECT flu.Nome NO_Fluxo, fo1.Nome NO_ReceiRecu,'
      'fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab,'
      'wac.*, '
      'ggx.GraGru1, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM wbartcab wac'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wac.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN fluxos  flu ON flu.Codigo=wac.Fluxo'
      'LEFT JOIN formulas fo1 ON fo1.Numero=wac.ReceiRecu'
      'LEFT JOIN formulas fo2 ON fo2.Numero=wac.ReceiRefu'
      'LEFT JOIN formulas ti1 ON ti1.Numero=wac.ReceiAcab')
    Left = 92
    Top = 20
    object QrWBMPrCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrWBMPrCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWBMPrCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWBMPrCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWBMPrCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWBMPrCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWBMPrCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWBMPrCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWBMPrCabGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrWBMPrCabNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrWBMPrCabOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrWBMPrCabBRLMedM2: TFloatField
      FieldName = 'BRLMedM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsWBMPrCab: TDataSource
    DataSet = QrWBMPrCab
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtMP
    CanUpd01 = BtFornece
    Left = 168
    Top = 20
  end
  object frxWET_RECUR_013: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41690.713452719910000000
    ReportOptions.LastChange = 41690.713452719910000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 360
    Top = 124
    Datasets = <
      item
        DataSet = frxDsCad
        DataSetName = 'frxDsCad'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 64.251997800000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line_PH_01: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Configura'#231#227'o de Mat'#233'ria-prima para Semi / Acabado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 529.134199999999900000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 45.354360000000000000
          Width = 68.031496060000000000
          Height = 18.897637800000000000
          DataSet = frxDsCad
          DataSetName = 'frxDsCad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ordem')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 136.063080000000000000
          Top = 45.354360000000000000
          Width = 476.220472440944800000
          Height = 18.897637800000000000
          DataSet = frxDsCad
          DataSetName = 'frxDsCad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descr'#231#227'o')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 68.031540000000010000
          Top = 45.354360000000000000
          Width = 68.031496060000000000
          Height = 18.897637800000000000
          DataSet = frxDsCad
          DataSetName = 'frxDsCad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          Left = 612.283903940000000000
          Top = 45.354360000000000000
          Width = 68.031496060000000000
          Height = 18.897637800000000000
          DataSet = frxDsCad
          DataSetName = 'frxDsCad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ m'#233'dio m'#178)
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897637800000000000
        Top = 143.622140000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCad
        DataSetName = 'frxDsCad'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 68.031496060000000000
          Height = 18.897637800000000000
          DataField = 'Ordem'
          DataSet = frxDsCad
          DataSetName = 'frxDsCad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCad."Ordem"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 136.063080000000000000
          Width = 476.220472440000000000
          Height = 18.897637800000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsCad
          DataSetName = 'frxDsCad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCad."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 68.031540000000010000
          Width = 68.031496060000000000
          Height = 18.897637800000000000
          DataField = 'GraGruX'
          DataSet = frxDsCad
          DataSetName = 'frxDsCad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCad."GraGruX"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 18.897637800000000000
          DataField = 'BRLMedM2'
          DataSet = frxDsCad
          DataSetName = 'frxDsCad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCad."BRLMedM2"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsCad: TfrxDBDataset
    UserName = 'frxDsCad'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GraGruX=GraGruX'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Ordem=Ordem'
      'BRLMedM2=BRLMedM2')
    DataSet = QrCad
    BCDToCurrency = False
    Left = 360
    Top = 172
  end
  object QrCad: TmySQLQuery
    Database = Dmod.MyDB
    Left = 180
    Top = 88
    object QrCadGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrCadLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCadUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCadAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCadGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrCadNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrCadOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCadBRLMedM2: TFloatField
      FieldName = 'BRLMedM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object PMMP: TPopupMenu
    OnPopup = PMMPPopup
    Left = 436
    Top = 432
    object IncluiMP1: TMenuItem
      Caption = '&Inclui nova mat'#233'ria-prima'
      OnClick = IncluiMP1Click
    end
    object AlteraMP1: TMenuItem
      Caption = '&Altera a mat'#233'ria-prima selecionada'
      OnClick = AlteraMP1Click
    end
    object ExcluiMP1: TMenuItem
      Caption = '&Exclui a mat'#233'ria-prima selecionada'
      Enabled = False
      OnClick = ExcluiMP1Click
    end
  end
  object PMFornece: TPopupMenu
    OnPopup = PMFornecePopup
    Left = 556
    Top = 436
    object IncluiFornece1: TMenuItem
      Caption = '&Inclui novo fornecedor'
      OnClick = IncluiFornece1Click
    end
    object AlteraFornece1: TMenuItem
      Caption = '&Altera fornecedor atual'
      OnClick = AlteraFornece1Click
    end
    object ExcluiFornece1: TMenuItem
      Caption = '&Exclui fornecedor atual'
      OnClick = ExcluiFornece1Click
    end
  end
  object QrWBMPrFrn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frn.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE'
      'FROM wbmprfrn frn'
      'LEFT JOIN entidades ent ON ent.Codigo=frn.Fornece'
      'WHERE frn.GraGruX=0'
      '')
    Left = 460
    Top = 124
    object QrWBMPrFrnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWBMPrFrnGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrWBMPrFrnFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrWBMPrFrnM2Medio: TFloatField
      FieldName = 'M2Medio'
      EditFormat = '#,###,###,##0.00'
    end
    object QrWBMPrFrnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWBMPrFrnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWBMPrFrnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWBMPrFrnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWBMPrFrnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWBMPrFrnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWBMPrFrnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWBMPrFrnNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
  end
  object DsWBMPrFrn: TDataSource
    DataSet = QrWBMPrFrn
    Left = 460
    Top = 172
  end
end
