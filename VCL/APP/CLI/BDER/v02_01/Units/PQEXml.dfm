object FmPQEXml: TFmPQEXml
  Left = 339
  Top = 185
  Caption = 'QUI-ENTRA-010 :: Entrada de Uso e Consumo por XMLde NFe'
  ClientHeight = 344
  ClientWidth = 854
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 854
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 806
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 758
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 535
        Height = 32
        Caption = 'Entrada de Uso e Consumo por XMLde NFe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 535
        Height = 32
        Caption = 'Entrada de Uso e Consumo por XMLde NFe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 535
        Height = 32
        Caption = 'Entrada de Uso e Consumo por XMLde NFe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 230
    Width = 854
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 850
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 274
    Width = 854
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 708
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 706
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 854
    Height = 182
    Align = alClient
    TabOrder = 3
    object PnMensagens: TPanel
      Left = 533
      Top = 1
      Width = 320
      Height = 180
      Align = alClient
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Label6: TLabel
        Left = 0
        Top = 0
        Width = 35
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Alertas:'
      end
      object Label29: TLabel
        Left = 0
        Top = 73
        Width = 58
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Mensagens:'
      end
      object MeWarn: TMemo
        Left = 0
        Top = 13
        Width = 320
        Height = 60
        TabStop = False
        Align = alTop
        ReadOnly = True
        TabOrder = 0
      end
      object MeInfo: TMemo
        Left = 0
        Top = 86
        Width = 320
        Height = 94
        TabStop = False
        Align = alClient
        ReadOnly = True
        TabOrder = 1
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 532
      Height = 180
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object LaEmpresa: TLabel
        Left = 68
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label17: TLabel
        Left = 408
        Top = 4
        Width = 65
        Height = 13
        Caption = 'Data entrada:'
      end
      object LaLancamento: TLabel
        Left = 12
        Top = 4
        Width = 40
        Height = 13
        Caption = 'N'#250'mero:'
      end
      object Label31: TLabel
        Left = 12
        Top = 44
        Width = 59
        Height = 13
        Caption = 'Regra fiscal:'
      end
      object LaCondicaoPG: TLabel
        Left = 12
        Top = 84
        Width = 119
        Height = 13
        Caption = 'Condi'#231#227'o de pagamento:'
      end
      object BtCondicaoPG: TSpeedButton
        Left = 501
        Top = 99
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = BtCondicaoPGClick
      end
      object EdEmpresa: TdmkEditCB
        Left = 68
        Top = 20
        Width = 55
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 123
        Top = 20
        Width = 282
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        TabStop = False
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPEntrada: TdmkEditDateTimePicker
        Left = 409
        Top = 20
        Width = 112
        Height = 21
        Date = 40060.000000000000000000
        Time = 0.661368159722769600
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object RGMadeBy: TdmkRadioGroup
        Left = 12
        Top = 128
        Width = 513
        Height = 45
        Caption = ' Modo de fornecimento dos produtos: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          'Fabrica'#231#227'o Pr'#243'pria'
          'Revenda/outros')
        TabOrder = 4
        QryCampo = 'MadeBy'
        UpdCampo = 'MadeBy'
        UpdType = utYes
        OldValor = 0
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 20
        Width = 52
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdRegrFiscal: TdmkEditCB
        Left = 12
        Top = 60
        Width = 44
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBRegrFiscal
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBRegrFiscal: TdmkDBLookupComboBox
        Left = 56
        Top = 60
        Width = 465
        Height = 21
        DropDownRows = 2
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFisRegCad
        TabOrder = 6
        dmkEditCB = EdRegrFiscal
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCondicaoPG: TdmkEditCB
        Left = 12
        Top = 100
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCondicaoPG
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCondicaoPG: TdmkDBLookupComboBox
        Left = 56
        Top = 100
        Width = 441
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsPediPrzCab
        TabOrder = 8
        dmkEditCB = EdCondicaoPG
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65515
  end
  object QrFisRegCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.ModeloNF,'
      'frc.Nome, frc.Financeiro, imp.Nome NO_MODELO_NF,'
      'frc.GenCtbD, frc.GenCtbC'
      'FROM fisregcad frc'
      'LEFT JOIN imprime imp ON imp.Codigo=frc.ModeloNF'
      'ORDER BY Nome')
    Left = 424
    Top = 44
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFisRegCadModeloNF: TIntegerField
      FieldName = 'ModeloNF'
      Required = True
    end
    object QrFisRegCadFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
    object QrFisRegCadGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
      Required = True
    end
    object QrFisRegCadGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
      Required = True
    end
    object QrFisRegCadNO_MODELO_NF: TWideStringField
      FieldName = 'NO_MODELO_NF'
      Size = 100
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 424
    Top = 89
  end
  object QrA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 596
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrAversao: TFloatField
      FieldName = 'versao'
    end
    object QrAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrAide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrAide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrAide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrAide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrAide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrAide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrAide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrAide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrAemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrAemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrAemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrAemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrAemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrAemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrAemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrAemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrAemit_cPais: TIntegerField
      FieldName = 'emit_cPais'
    end
    object QrAemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Size = 60
    end
    object QrAemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 10
    end
    object QrAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrAemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Size = 14
    end
    object QrAemit_IM: TWideStringField
      FieldName = 'emit_IM'
      Size = 15
    end
    object QrAemit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Size = 7
    end
    object QrAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrAdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrAdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrAdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrAdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrAdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
    end
    object QrAdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrAdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrAdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Size = 8
    end
    object QrAdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
    end
    object QrAdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Size = 60
    end
    object QrAdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 10
    end
    object QrAdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrAdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Size = 9
    end
    object QrAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrAICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrAISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
    end
    object QrAISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
    end
    object QrAISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
    end
    object QrAISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
    end
    object QrAISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
    end
    object QrARetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
    end
    object QrARetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
    end
    object QrARetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
    end
    object QrARetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
    end
    object QrARetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
    end
    object QrARetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
    end
    object QrARetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
    end
    object QrAModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrATransporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Size = 14
    end
    object QrATransporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Size = 11
    end
    object QrATransporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Size = 60
    end
    object QrATransporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
      Size = 14
    end
    object QrATransporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Size = 60
    end
    object QrATransporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Size = 60
    end
    object QrATransporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Size = 2
    end
    object QrARetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
    end
    object QrARetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
    end
    object QrARetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
    end
    object QrARetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
    end
    object QrARetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Size = 4
    end
    object QrARetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Size = 7
    end
    object QrAVeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Size = 8
    end
    object QrAVeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Size = 2
    end
    object QrAVeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
    end
    object QrACobr_Fat_nFat: TWideStringField
      FieldName = 'Cobr_Fat_nFat'
      Size = 60
    end
    object QrACobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
    end
    object QrACobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
    end
    object QrACobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
    end
    object QrAInfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 255
    end
    object QrAInfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrAExporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Size = 2
    end
    object QrAExporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Size = 60
    end
    object QrACompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrACompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrACompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
    object QrAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrAinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrAinfProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrAinfProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Size = 30
    end
    object QrAinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrAinfProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrAinfCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Size = 30
    end
    object QrAinfCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
    end
    object QrAinfCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Size = 30
    end
    object QrAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrAinfCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Size = 28
    end
    object QrAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrAinfCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
    end
    object QrAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrA_Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrAFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
    end
    object QrACartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrATabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrACondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrAFreteExtra: TFloatField
      FieldName = 'FreteExtra'
    end
    object QrASegurExtra: TFloatField
      FieldName = 'SegurExtra'
    end
    object QrALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrAICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrAICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
    end
    object QrAICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrAICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
    end
    object QrAIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrAIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
    end
    object QrAIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrAIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
    end
    object QrAPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrAPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
    end
    object QrAPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrAPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
    end
    object QrACOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object QrACOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
    end
    object QrACOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrACOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
    end
    object QrACodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrACodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object QrAide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrAide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrAide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrAemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
    object QrAdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrAVagao: TWideStringField
      FieldName = 'Vagao'
    end
    object QrABalsa: TWideStringField
      FieldName = 'Balsa'
    end
    object QrAprotNFe_versao: TFloatField
      FieldName = 'protNFe_versao'
      Required = True
    end
    object QrAretCancNFe_versao: TFloatField
      FieldName = 'retCancNFe_versao'
      Required = True
    end
    object QrADataFiscal: TDateField
      FieldName = 'DataFiscal'
      Required = True
    end
    object QrASINTEGRA_ExpDeclNum: TWideStringField
      FieldName = 'SINTEGRA_ExpDeclNum'
      Size = 11
    end
    object QrASINTEGRA_ExpDeclDta: TDateField
      FieldName = 'SINTEGRA_ExpDeclDta'
      Required = True
    end
    object QrASINTEGRA_ExpNat: TWideStringField
      FieldName = 'SINTEGRA_ExpNat'
      Size = 1
    end
    object QrASINTEGRA_ExpRegNum: TWideStringField
      FieldName = 'SINTEGRA_ExpRegNum'
      Size = 12
    end
    object QrASINTEGRA_ExpRegDta: TDateField
      FieldName = 'SINTEGRA_ExpRegDta'
      Required = True
    end
    object QrASINTEGRA_ExpConhNum: TWideStringField
      FieldName = 'SINTEGRA_ExpConhNum'
      Size = 16
    end
    object QrASINTEGRA_ExpConhDta: TDateField
      FieldName = 'SINTEGRA_ExpConhDta'
      Required = True
    end
    object QrASINTEGRA_ExpConhTip: TWideStringField
      FieldName = 'SINTEGRA_ExpConhTip'
      Required = True
      Size = 2
    end
    object QrASINTEGRA_ExpPais: TWideStringField
      FieldName = 'SINTEGRA_ExpPais'
      Required = True
      Size = 4
    end
    object QrASINTEGRA_ExpAverDta: TDateField
      FieldName = 'SINTEGRA_ExpAverDta'
      Required = True
    end
    object QrACriAForca: TSmallintField
      FieldName = 'CriAForca'
      Required = True
    end
    object QrACodInfoTrsp: TIntegerField
      FieldName = 'CodInfoTrsp'
      Required = True
    end
    object QrAOrdemServ: TIntegerField
      FieldName = 'OrdemServ'
      Required = True
    end
    object QrASituacao: TSmallintField
      FieldName = 'Situacao'
      Required = True
    end
    object QrAAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrANFG_Serie: TWideStringField
      FieldName = 'NFG_Serie'
      Size = 3
    end
    object QrANF_ICMSAlq: TFloatField
      FieldName = 'NF_ICMSAlq'
      Required = True
    end
    object QrANF_CFOP: TWideStringField
      FieldName = 'NF_CFOP'
      Size = 4
    end
    object QrAImportado: TSmallintField
      FieldName = 'Importado'
      Required = True
    end
    object QrANFG_SubSerie: TWideStringField
      FieldName = 'NFG_SubSerie'
      Size = 3
    end
    object QrANFG_ValIsen: TFloatField
      FieldName = 'NFG_ValIsen'
      Required = True
    end
    object QrANFG_NaoTrib: TFloatField
      FieldName = 'NFG_NaoTrib'
      Required = True
    end
    object QrANFG_Outros: TFloatField
      FieldName = 'NFG_Outros'
      Required = True
    end
    object QrACOD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrACOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
      Required = True
    end
    object QrAVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Required = True
    end
    object QrAEFD_INN_AnoMes: TIntegerField
      FieldName = 'EFD_INN_AnoMes'
      Required = True
    end
    object QrAEFD_INN_Empresa: TIntegerField
      FieldName = 'EFD_INN_Empresa'
      Required = True
    end
    object QrAEFD_INN_LinArq: TIntegerField
      FieldName = 'EFD_INN_LinArq'
      Required = True
    end
    object QrAICMSRec_vBCST: TFloatField
      FieldName = 'ICMSRec_vBCST'
      Required = True
    end
    object QrAICMSRec_vICMSST: TFloatField
      FieldName = 'ICMSRec_vICMSST'
      Required = True
    end
    object QrAEFD_EXP_REG: TWideStringField
      FieldName = 'EFD_EXP_REG'
      Required = True
      Size = 4
    end
    object QrAICMSRec_pAliqST: TFloatField
      FieldName = 'ICMSRec_pAliqST'
      Required = True
    end
    object QrAeveMDe_Id: TWideStringField
      FieldName = 'eveMDe_Id'
      Size = 17
    end
    object QrAeveMDe_tpAmb: TSmallintField
      FieldName = 'eveMDe_tpAmb'
      Required = True
    end
    object QrAeveMDe_verAplic: TWideStringField
      FieldName = 'eveMDe_verAplic'
    end
    object QrAeveMDe_cOrgao: TSmallintField
      FieldName = 'eveMDe_cOrgao'
      Required = True
    end
    object QrAeveMDe_cStat: TIntegerField
      FieldName = 'eveMDe_cStat'
      Required = True
    end
    object QrAeveMDe_xMotivo: TWideStringField
      FieldName = 'eveMDe_xMotivo'
      Size = 255
    end
    object QrAeveMDe_chNFe: TWideStringField
      FieldName = 'eveMDe_chNFe'
      Size = 44
    end
    object QrAeveMDe_tpEvento: TIntegerField
      FieldName = 'eveMDe_tpEvento'
      Required = True
    end
    object QrAeveMDe_xEvento: TWideStringField
      FieldName = 'eveMDe_xEvento'
      Size = 60
    end
    object QrAeveMDe_nSeqEvento: TSmallintField
      FieldName = 'eveMDe_nSeqEvento'
      Required = True
    end
    object QrAeveMDe_CNPJDest: TWideStringField
      FieldName = 'eveMDe_CNPJDest'
      Size = 18
    end
    object QrAeveMDe_CPFDest: TWideStringField
      FieldName = 'eveMDe_CPFDest'
      Size = 18
    end
    object QrAeveMDe_emailDest: TWideStringField
      FieldName = 'eveMDe_emailDest'
      Size = 60
    end
    object QrAeveMDe_dhRegEvento: TDateTimeField
      FieldName = 'eveMDe_dhRegEvento'
      Required = True
    end
    object QrAeveMDe_TZD_UTC: TFloatField
      FieldName = 'eveMDe_TZD_UTC'
      Required = True
    end
    object QrAeveMDe_nProt: TWideStringField
      FieldName = 'eveMDe_nProt'
      Size = 15
    end
    object QrAcSitNFe: TSmallintField
      FieldName = 'cSitNFe'
    end
    object QrAcSitConf: TSmallintField
      FieldName = 'cSitConf'
    end
    object QrANFeNT2013_003LTT: TSmallintField
      FieldName = 'NFeNT2013_003LTT'
      Required = True
    end
    object QrAvTotTrib: TFloatField
      FieldName = 'vTotTrib'
      Required = True
    end
    object QrAide_hEmi: TTimeField
      FieldName = 'ide_hEmi'
      Required = True
    end
    object QrAide_dhEmiTZD: TFloatField
      FieldName = 'ide_dhEmiTZD'
      Required = True
    end
    object QrAide_dhSaiEntTZD: TFloatField
      FieldName = 'ide_dhSaiEntTZD'
      Required = True
    end
    object QrAide_idDest: TSmallintField
      FieldName = 'ide_idDest'
    end
    object QrAide_indFinal: TSmallintField
      FieldName = 'ide_indFinal'
    end
    object QrAide_indPres: TSmallintField
      FieldName = 'ide_indPres'
    end
    object QrAide_dhContTZD: TFloatField
      FieldName = 'ide_dhContTZD'
      Required = True
    end
    object QrAEstrangDef: TSmallintField
      FieldName = 'EstrangDef'
      Required = True
    end
    object QrAdest_idEstrangeiro: TWideStringField
      FieldName = 'dest_idEstrangeiro'
    end
    object QrAdest_indIEDest: TSmallintField
      FieldName = 'dest_indIEDest'
      Required = True
    end
    object QrAdest_IM: TWideStringField
      FieldName = 'dest_IM'
      Size = 15
    end
    object QrAICMSTot_vICMSDeson: TFloatField
      FieldName = 'ICMSTot_vICMSDeson'
    end
    object QrAISSQNtot_dCompet: TDateField
      FieldName = 'ISSQNtot_dCompet'
      Required = True
    end
    object QrAISSQNtot_vDeducao: TFloatField
      FieldName = 'ISSQNtot_vDeducao'
    end
    object QrAISSQNtot_vOutro: TFloatField
      FieldName = 'ISSQNtot_vOutro'
    end
    object QrAISSQNtot_vDescIncond: TFloatField
      FieldName = 'ISSQNtot_vDescIncond'
    end
    object QrAISSQNtot_vDescCond: TFloatField
      FieldName = 'ISSQNtot_vDescCond'
    end
    object QrAISSQNtot_vISSRet: TFloatField
      FieldName = 'ISSQNtot_vISSRet'
    end
    object QrAISSQNtot_cRegTrib: TSmallintField
      FieldName = 'ISSQNtot_cRegTrib'
      Required = True
    end
    object QrAinfProt_dhRecbtoTZD: TFloatField
      FieldName = 'infProt_dhRecbtoTZD'
      Required = True
    end
    object QrAinfCanc_dhRecbtoTZD: TFloatField
      FieldName = 'infCanc_dhRecbtoTZD'
      Required = True
    end
    object QrAvBasTrib: TFloatField
      FieldName = 'vBasTrib'
    end
    object QrApTotTrib: TFloatField
      FieldName = 'pTotTrib'
    end
    object QrAinfCCe_verAplic: TWideStringField
      FieldName = 'infCCe_verAplic'
      Size = 30
    end
    object QrAinfCCe_cOrgao: TSmallintField
      FieldName = 'infCCe_cOrgao'
      Required = True
    end
    object QrAinfCCe_tpAmb: TSmallintField
      FieldName = 'infCCe_tpAmb'
      Required = True
    end
    object QrAinfCCe_CNPJ: TWideStringField
      FieldName = 'infCCe_CNPJ'
      Size = 18
    end
    object QrAinfCCe_CPF: TWideStringField
      FieldName = 'infCCe_CPF'
      Size = 18
    end
    object QrAinfCCe_chNFe: TWideStringField
      FieldName = 'infCCe_chNFe'
      Size = 44
    end
    object QrAinfCCe_dhEvento: TDateTimeField
      FieldName = 'infCCe_dhEvento'
      Required = True
    end
    object QrAinfCCe_dhEventoTZD: TFloatField
      FieldName = 'infCCe_dhEventoTZD'
      Required = True
    end
    object QrAinfCCe_tpEvento: TIntegerField
      FieldName = 'infCCe_tpEvento'
      Required = True
    end
    object QrAinfCCe_nSeqEvento: TIntegerField
      FieldName = 'infCCe_nSeqEvento'
      Required = True
    end
    object QrAinfCCe_verEvento: TFloatField
      FieldName = 'infCCe_verEvento'
      Required = True
    end
    object QrAinfCCe_xCorrecao: TWideMemoField
      FieldName = 'infCCe_xCorrecao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrAinfCCe_cStat: TIntegerField
      FieldName = 'infCCe_cStat'
      Required = True
    end
    object QrAinfCCe_dhRegEvento: TDateTimeField
      FieldName = 'infCCe_dhRegEvento'
      Required = True
    end
    object QrAinfCCe_dhRegEventoTZD: TFloatField
      FieldName = 'infCCe_dhRegEventoTZD'
      Required = True
    end
    object QrAinfCCe_nProt: TWideStringField
      FieldName = 'infCCe_nProt'
      Size = 15
    end
    object QrAinfCCe_nCondUso: TIntegerField
      FieldName = 'infCCe_nCondUso'
      Required = True
    end
    object QrAInfCpl_totTrib: TWideStringField
      FieldName = 'InfCpl_totTrib'
      Size = 255
    end
    object QrAICMSTot_vFCPUFDest: TFloatField
      FieldName = 'ICMSTot_vFCPUFDest'
    end
    object QrAICMSTot_vICMSUFDest: TFloatField
      FieldName = 'ICMSTot_vICMSUFDest'
    end
    object QrAICMSTot_vICMSUFRemet: TFloatField
      FieldName = 'ICMSTot_vICMSUFRemet'
    end
    object QrAExporta_XLocDespacho: TWideStringField
      FieldName = 'Exporta_XLocDespacho'
      Size = 60
    end
    object QrACodInfoCliI: TIntegerField
      FieldName = 'CodInfoCliI'
      Required = True
    end
    object QrAICMSTot_vFCP: TFloatField
      FieldName = 'ICMSTot_vFCP'
    end
    object QrAICMSTot_vFCPST: TFloatField
      FieldName = 'ICMSTot_vFCPST'
    end
    object QrAICMSTot_vFCPSTRet: TFloatField
      FieldName = 'ICMSTot_vFCPSTRet'
    end
    object QrAICMSTot_vIPIDevol: TFloatField
      FieldName = 'ICMSTot_vIPIDevol'
    end
    object QrAAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrAAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrAURL_QRCode: TWideStringField
      FieldName = 'URL_QRCode'
      Size = 255
    end
    object QrAURL_Consulta: TWideStringField
      FieldName = 'URL_Consulta'
      Size = 255
    end
    object QrAAllTxtLink: TWideStringField
      FieldName = 'AllTxtLink'
      Size = 510
    end
    object QrACNPJCPFAvulso: TWideStringField
      FieldName = 'CNPJCPFAvulso'
      Size = 14
    end
    object QrARazaoNomeAvulso: TWideStringField
      FieldName = 'RazaoNomeAvulso'
      Size = 60
    end
    object QrAide_indIntermed: TSmallintField
      FieldName = 'ide_indIntermed'
      Required = True
    end
    object QrAInfIntermedEnti: TIntegerField
      FieldName = 'InfIntermedEnti'
      Required = True
    end
    object QrAInfIntermed_CNPJ: TWideStringField
      FieldName = 'InfIntermed_CNPJ'
      Size = 14
    end
    object QrAInfIntermed_idCadIntTran: TWideStringField
      FieldName = 'InfIntermed_idCadIntTran'
      Size = 60
    end
    object QrAEmiteAvulso: TSmallintField
      FieldName = 'EmiteAvulso'
      Required = True
    end
    object QrAAtrelaFatID: TIntegerField
      FieldName = 'AtrelaFatID'
      Required = True
    end
    object QrAAtrelaFatNum: TIntegerField
      FieldName = 'AtrelaFatNum'
      Required = True
    end
    object QrAAtrelaStaLnk: TSmallintField
      FieldName = 'AtrelaStaLnk'
      Required = True
    end
  end
  object QrI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 624
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrINivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrIprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrIprod_vTotItm: TFloatField
      FieldName = 'prod_vTotItm'
    end
    object QrIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrIFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrIFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrIEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrIprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
      Required = True
    end
    object QrIprod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrIprod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
    end
    object QrI_Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrIInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Required = True
    end
    object QrIEhServico: TIntegerField
      FieldName = 'EhServico'
      Required = True
    end
    object QrIICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      Required = True
    end
    object QrIICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
      Required = True
    end
    object QrIICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      Required = True
    end
    object QrIICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
      Required = True
    end
    object QrIIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      Required = True
    end
    object QrIIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
      Required = True
    end
    object QrIIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      Required = True
    end
    object QrIIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
      Required = True
    end
    object QrIPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      Required = True
    end
    object QrIPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
      Required = True
    end
    object QrIPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      Required = True
    end
    object QrIPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
      Required = True
    end
    object QrICOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      Required = True
    end
    object QrICOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
      Required = True
    end
    object QrICOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      Required = True
    end
    object QrICOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
      Required = True
    end
    object QrIMeuID: TIntegerField
      FieldName = 'MeuID'
      Required = True
    end
    object QrIUnidMedCom: TIntegerField
      FieldName = 'UnidMedCom'
      Required = True
    end
    object QrIUnidMedTrib: TIntegerField
      FieldName = 'UnidMedTrib'
      Required = True
    end
    object QrIICMSRec_vBCST: TFloatField
      FieldName = 'ICMSRec_vBCST'
      Required = True
    end
    object QrIICMSRec_vICMSST: TFloatField
      FieldName = 'ICMSRec_vICMSST'
      Required = True
    end
    object QrIICMSRec_pAliqST: TFloatField
      FieldName = 'ICMSRec_pAliqST'
      Required = True
    end
    object QrITem_II: TSmallintField
      FieldName = 'Tem_II'
    end
    object QrILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrIAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrIprod_nFCI: TWideStringField
      FieldName = 'prod_nFCI'
      Size = 36
    end
    object QrIprod_CEST: TIntegerField
      FieldName = 'prod_CEST'
      Required = True
    end
    object QrIStqMovValA: TIntegerField
      FieldName = 'StqMovValA'
      Required = True
    end
    object QrIprod_indEscala: TWideStringField
      FieldName = 'prod_indEscala'
      Size = 1
    end
    object QrIprod_CNPJFab: TWideStringField
      FieldName = 'prod_CNPJFab'
      Size = 14
    end
    object QrIprod_cBenef: TWideStringField
      FieldName = 'prod_cBenef'
      Size = 10
    end
    object QrIAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrIprod_cBarra: TWideStringField
      FieldName = 'prod_cBarra'
      Size = 30
    end
    object QrIprod_cBarraTrib: TWideStringField
      FieldName = 'prod_cBarraTrib'
      Size = 30
    end
    object QrIUsaSubsTrib: TSmallintField
      FieldName = 'UsaSubsTrib'
      Required = True
    end
    object QrIAtrelaID: TIntegerField
      FieldName = 'AtrelaID'
      Required = True
    end
    object QrIFatorQtd: TFloatField
      FieldName = 'FatorQtd'
    end
  end
  object QrN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 652
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrNICMS_CSOSN: TIntegerField
      FieldName = 'ICMS_CSOSN'
    end
    object QrNICMS_UFST: TWideStringField
      FieldName = 'ICMS_UFST'
      Size = 2
    end
    object QrNICMS_pBCOp: TFloatField
      FieldName = 'ICMS_pBCOp'
    end
    object QrNICMS_vBCSTRet: TFloatField
      FieldName = 'ICMS_vBCSTRet'
    end
    object QrNICMS_vICMSSTRet: TFloatField
      FieldName = 'ICMS_vICMSSTRet'
    end
    object QrNICMS_motDesICMS: TSmallintField
      FieldName = 'ICMS_motDesICMS'
    end
    object QrNICMS_pCredSN: TFloatField
      FieldName = 'ICMS_pCredSN'
    end
    object QrNICMS_vCredICMSSN: TFloatField
      FieldName = 'ICMS_vCredICMSSN'
    end
  end
  object QrO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 680
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
    end
    object QrOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
  end
  object QrQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsq'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 708
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrQPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrQPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
    end
    object QrQPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
    end
    object QrQPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
    end
    object QrQPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
    end
    object QrQPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
    end
  end
  object QrR: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsr'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 736
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrRPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
    end
    object QrRPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
    end
    object QrRPISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
    end
    object QrRPISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
    end
    object QrRPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
    end
    object QrRPISST_fatorBCST: TFloatField
      FieldName = 'PISST_fatorBCST'
    end
  end
  object QrS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitss'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 738
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrSCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrSCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
    end
    object QrSCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
    end
    object QrSCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
    end
    object QrSCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
    end
  end
  object QrT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitst'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 766
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrTCOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
    end
    object QrTCOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
    end
    object QrTCOFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
    end
    object QrTCOFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
    end
    object QrTCOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
    end
    object QrTCOFINSST_fatorBCST: TFloatField
      FieldName = 'COFINSST_fatorBCST'
    end
  end
  object QrV: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeitsv'
      'WHERE FatID=53 '
      'AND FatNum=27 '
      'AND Empresa =-11 '
      'AND nItem=1'
      ' ')
    Left = 792
    object QrVnItem: TIntegerField
      FieldName = 'nItem'
      Required = True
    end
    object QrVInfAdProd: TWideMemoField
      FieldName = 'InfAdProd'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 290
    Top = 92
  end
  object QrPediPrzCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, MaxDesco,'
      'JurosMes, Parcelas, Percent1, Percent2'
      'FROM pediprzcab'
      'WHERE Aplicacao & :P0 <> 0'
      'ORDER BY Nome')
    Left = 290
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrPediPrzCabCondPg: TSmallintField
      FieldName = 'CondPg'
    end
  end
  object QrI80s: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '  '#39'SELECT *  '#39','
      '  '#39'FROM nfeitsi80 '#39','
      '  '#39'WHERE FatID>0 '#39','
      '  '#39'AND FatNum>0 '#39','
      '  '#39'AND nItem>0 '#39','
      '  '#39'AND Empresa=-11 '#39',')
    Left = 581
    Top = 181
  end
  object QrItsI80: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi80 '
      'WHERE FatID>0 '
      'AND FatNum>0 '
      'AND nItem>0 '
      'AND Empresa=-11 ')
    Left = 581
    Top = 233
    object QrItsI80rastro_nLote: TWideStringField
      FieldName = 'rastro_nLote'
      Required = True
    end
    object QrItsI80rastro_qLote: TFloatField
      FieldName = 'rastro_qLote'
      Required = True
    end
    object QrItsI80rastro_dFab: TDateField
      FieldName = 'rastro_dFab'
      Required = True
    end
    object QrItsI80rastro_dVal: TDateField
      FieldName = 'rastro_dVal'
      Required = True
    end
    object QrItsI80rastro_cAgreg: TWideStringField
      FieldName = 'rastro_cAgreg'
    end
  end
end
