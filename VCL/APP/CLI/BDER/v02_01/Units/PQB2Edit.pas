unit PQB2Edit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnInternalConsts,
  UnMsgInt, ExtCtrls, UnInternalConsts2, mySQLDbTables, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, UMySQLModule, dmkGeral, dmkLabel, dmkImage, UnDmkEnums,
  UnDmkProcFunc, DmkDAC_PF;

type
  TFmPQB2Edit = class(TForm)
    PainelDados: TPanel;
    LaInsumo: TLabel;
    Label4: TLabel;
    EdPecas: TdmkEdit;
    Label10: TLabel;
    EdPreco: TdmkEdit;
    Label6: TLabel;
    EdVTota: TdmkEdit;
    QrPQ: TmySQLQuery;
    DsPQ: TDataSource;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    Label2: TLabel;
    QrCI: TmySQLQuery;
    QrCINome: TWideStringField;
    QrCICodigo: TIntegerField;
    DsCI: TDataSource;
    EdInsumo: TdmkEditCB;
    CBInsumo: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    LaEmpresa: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdPecasExit(Sender: TObject);
    procedure EdPrecoExit(Sender: TObject);
    procedure EdVTotaExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdInsumoEnter(Sender: TObject);
    procedure EdClienteEnter(Sender: TObject);
    procedure EdInsumoExit(Sender: TObject);
    procedure EdClienteExit(Sender: TObject);
    procedure EdInsumoChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
  private
    { Private declarations }
    FPQ, FCI: Integer;
    procedure BuscaPrecoCadastro();
    procedure CalculaOnEdit(Tipo: Integer);
  public
    { Public declarations }
    FBalanco: Integer;
  end;

var
  FmPQB2Edit: TFmPQB2Edit;

implementation

uses UnMyObjects, Module, PQB2, PQx, UnPQ_PF, ModuleGeral;

{$R *.DFM}


procedure TFmPQB2Edit.CalculaOnEdit(Tipo: Integer);
var
  Pecas, Preco, VTota: Double;
begin
  Pecas := Geral.DMV(EdPecas.Text);
  Preco := Geral.DMV(EdPreco.Text);
  VTota := Geral.DMV(EdVTota.Text);
  //
  if Tipo <> 1 then VTota := Pecas * Preco
  else if Pecas <> 0 then Preco := VTota / Pecas
  else Preco := 0;
  //
  EdPecas.ValueVariant := Pecas;
  EdPreco.ValueVariant := Preco;
  EdVTota.ValueVariant := VTota;
end;

procedure TFmPQB2Edit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQB2Edit.BuscaPrecoCadastro();
begin
  if ImgTipo.SQLType = stIns then
  begin
    FPQ := EdInsumo.ValueVariant;
    FCI := EdCliente.ValueVariant;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT CustoPadrao FROM pqcli');
    Dmod.QrAux.SQL.Add('WHERE PQ=:P0');
    Dmod.QrAux.SQL.Add('AND CI=:P1');
    Dmod.QrAux.Params[00].AsInteger := FPQ;
    Dmod.QrAux.Params[01].AsInteger := FCI;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    EdPreco.ValueVariant := Dmod.QrAux.FieldByName('CustoPadrao').AsFloat;
    CalculaOnEdit(0);
    //
  end;
end;

procedure TFmPQB2Edit.BtConfirmaClick(Sender: TObject);
const
  Unitario = True;
var
  Controle: Integer;
  KG, RS, SF: Double;
  //
  DataX, DtCorrApo: String;
  OriCodi, OriCtrl, OriTipo, CliOrig, CliDest, Insumo, CliInt, Empresa: Integer;
  Peso, Valor: Double;
var
  Cursor : TCursor;
  Preco, Conta, Pecas, VTota: Double;
  Cliente: Integer;
begin
  DtCorrApo := Geral.FDT(0, 1);
  DataX := dmkPF.PrimeiroDiaDoPeriodo(FmPQB2.QrBalancosPeriodo.Value, dtSystem);
  Pecas := Geral.DMV(EdPecas.Text);
  VTota := Geral.DMV(EdVTota.Text);
  //
  Cliente := Geral.IMV(EdCliente.Text);
  Preco := Geral.DMV(EdPreco.Text);
  Insumo := Geral.IMV(EdInsumo.Text);
  //
  if MyObjects.FIC((Preco <= 0) and (Cliente < 1), EdPreco,
    'Pre�o n�o pode ser negativo para esta empresa.') then Exit;
  if MyObjects.FIC((VTota = 0) and (Cliente < 1), EdPreco,
    'Defina o valor.') then Exit;
  if MyObjects.FIC(Insumo = 0, EdInsumo, 'Defina o insumo.') then Exit;
  if MyObjects.FIC(Pecas <= 0, EdPecas, 'Quantidade inv�lida!') then Exit;
  //
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    OriCodi := FBalanco;
    OriTipo := VAR_FATID_0000;
    CliOrig := Cliente;
    CliDest := Cliente;
    //Insumo  := Insumo;
    Peso    := Pecas;
    Valor   := VTota;
    if ImgTipo.SQLType = stIns then
    begin
      Conta := UMyMod.BuscaEmLivreY_Double(
      Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta');
      OriCtrl := Trunc(Conta);
(*
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('INSERT INTO p q x SET Tipo=0, Peso=:P0, Valor=:P1, ');
      Dmod.QrUpdU.SQL.Add('Insumo=:P2, CliOrig=:P3, CliDest=:P4, Datax=:P5, ');
      Dmod.QrUpdU.SQL.Add('OrigemCodi=:P6, OrigemCtrl=:P7 ');
*)
      PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
      OriCtrl, OriTipo, Unitario, DtCorrApo, Empresa);
    end else begin
      Conta := FmPQB2.QrBalancosItsOrigemCtrl.Value;
      OriCtrl := Trunc(Conta);
(*
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE PQx SET Peso=:P0, Valor=:P1, ');
      Dmod.QrUpdU.SQL.Add('Insumo=:P2, CliOrig=:P3, CliDest=:P4, DataX=:P5 ');
      Dmod.QrUpdU.SQL.Add('WHERE OrigemCodi=:P6 AND OrigemCtrl=:P7 AND Tipo=0');
*)
      CLiInt := Cliente;
      PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, True, Empresa);
      //
      PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
      OriCtrl, OriTipo, Unitario, DtCorrApo, Empresa);
    end;
    (*
    Dmod.QrUpdU.Params[00].AsFloat   := Pecas;
    Dmod.QrUpdU.Params[01].AsFloat   := VTota;
    Dmod.QrUpdU.Params[02].AsInteger := Insumo;
    Dmod.QrUpdU.Params[03].AsInteger := Cliente;
    Dmod.QrUpdU.Params[04].AsInteger := Cliente;
    Dmod.QrUpdU.Params[05].AsString  := DataX;
    Dmod.QrUpdU.Params[06].AsInteger := FBalanco;
    Dmod.QrUpdU.Params[07].AsFloat   := Conta;
    //Dmod.QrUpdU.Params[08].AsInteger := 0;
    Dmod.QrUpdU.ExecSQL;
    *)
  except
    Screen.Cursor := Cursor;
    Exit;
  end;
  if CkContinuar.Checked then
  begin
    ImgTipo.SQLType        := stIns;
    FPQ                    := 0;
    FCI                    := 0;
    LaInsumo.Enabled       := True;
    EdInsumo.Enabled       := True;
    CBInsumo.Enabled       := True;
    EdInsumo.ValueVariant  := EdInsumo.ValueVariant + 1;
    EdPreco.ValueVariant   := 0;
    EdPecas.ValueVariant   := 0;
    //
    CalculaOnEdit(0);
    EdInsumo.SetFocus;
    FmPQB2.SomaBalanco();
    FmPQB2.ReindexaTabela();
    FmPQB2.QrBalancosIts.Locate('OrigemCtrl', Conta, []);
    Screen.Cursor := crDefault;
  end else Close;
end;

procedure TFmPQB2Edit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  CalculaOnEdit(1);
  EdPreco.SetFocus;
  EdPecas.SetFocus;
  if ImgTipo.SQLType = stIns then
  begin
    EdInsumo.SetFocus;
  end;
end;

procedure TFmPQB2Edit.EdClienteChange(Sender: TObject);
begin
  if not EdCliente.Focused then
    BuscaPrecoCadastro();
end;

procedure TFmPQB2Edit.EdClienteEnter(Sender: TObject);
begin
  FCI := EdCliente.ValueVariant;
end;

procedure TFmPQB2Edit.EdClienteExit(Sender: TObject);
begin
  if FCI <> EdCliente.ValueVariant then
    BuscaPrecoCadastro();
end;

procedure TFmPQB2Edit.EdInsumoChange(Sender: TObject);
begin
  if not EdInsumo.Focused then
    BuscaPrecoCadastro();
end;

procedure TFmPQB2Edit.EdInsumoEnter(Sender: TObject);
begin
  FPQ := EdInsumo.ValueVariant;
end;

procedure TFmPQB2Edit.EdInsumoExit(Sender: TObject);
begin
  if FPQ <> EdInsumo.ValueVariant then
    BuscaPrecoCadastro();
end;

procedure TFmPQB2Edit.EdPecasExit(Sender: TObject);
begin
  CalculaOnEdit(0);
end;

procedure TFmPQB2Edit.EdPrecoExit(Sender: TObject);
begin
  CalculaOnEdit(0);
end;

procedure TFmPQB2Edit.EdVTotaExit(Sender: TObject);
begin
  CalculaOnEdit(1);
end;

procedure TFmPQB2Edit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQB2Edit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
end;

end.
