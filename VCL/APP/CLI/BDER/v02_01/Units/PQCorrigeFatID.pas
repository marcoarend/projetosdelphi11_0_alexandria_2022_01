unit PQCorrigeFatID;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, DB,
  mySQLDbTables, dmkGeral, ComCtrls, dmkImage, UnDmkEnums;

type
  TFmPQCorrigeFatID = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    QrPQE: TmySQLQuery;
    DsPQE: TDataSource;
    QrLct: TmySQLQuery;
    DsLct: TDataSource;
    DBGrid2: TDBGrid;
    QrLctData: TDateField;
    QrLctDebito: TFloatField;
    QrPQENO_FORNECE: TWideStringField;
    QrPQECodigo: TIntegerField;
    QrPQEIQ: TIntegerField;
    QrPQECI: TIntegerField;
    QrPQEData: TDateField;
    QrPQENF: TIntegerField;
    QrLctControle: TIntegerField;
    QrLctCarteira: TIntegerField;
    QrPQEValorNF: TFloatField;
    QrLctFatID: TIntegerField;
    QrPQETransportadora: TIntegerField;
    QrPQEConhecimento: TIntegerField;
    QrLctNotaFiscal: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctSub: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtAbre: TBitBtn;
    BtCorrige: TBitBtn;
    BtSaida: TBitBtn;
    QrPQECodCliInt: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure QrPQEAfterScroll(DataSet: TDataSet);
    procedure QrPQEBeforeClose(DataSet: TDataSet);
    procedure QrPQEAfterOpen(DataSet: TDataSet);
    procedure BtCorrigeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmPQCorrigeFatID: TFmPQCorrigeFatID;

implementation

uses UnMyObjects, UnInternalConsts, Module, UnFinanceiro, ModuleGeral,
DmkDAC_PF;

{$R *.DFM}

procedure TFmPQCorrigeFatID.BtAbreClick(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQE, Dmod.MyDB, [
  'SELECT IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'pqe.Codigo, pqe.IQ, pqe.CI, pqe.Data, pqe.NF, pqe.ValorNF, ',
  'pqe.Transportadora, pqe.Conhecimento, ei.CodCliInt ',
  'FROM pqe pqe ',
  'LEFT JOIN enticliint ei ON ei.CodEnti=pqe.CI ',
  'LEFT JOIN entidades frn ON frn.Codigo=pqe.IQ ',
  'ORDER BY Codigo ',
  '']);
end;

procedure TFmPQCorrigeFatID.BtCorrigeClick(Sender: TObject);
var
  FatID, FatNum, FatParcela, Controle, Sub: Integer;
  TabLctA: String;
begin
  QrPQE.First;
  while not QrPQE.Eof do
  begin
    if QrPQECodCliInt.Value <> 0 then
      TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrPQECodCliInt.Value)
    else
      TabLctA := sTabLctErr;
    //
    if (QrLct.State = dsBrowse) then
    begin
      QrLct.First;
      while not QrLct.Eof do
      begin
        if QrLctFatID.Value = 0 then
        begin
          FatNum     := QrPQECodigo.Value;
          FatParcela := QrLct.RecNo;
          Controle   := QrLctControle.Value;
          Sub        := QrLctSub.Value;
          //
          if (QrLctNotaFiscal.Value = QrPQENF.Value) and
             (QrLctFornecedor.Value = QrPQEIQ.Value) then
          begin
            FatID := 1;
          end else begin
            FatID := 2;
          end;
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'FatID', 'FatNum', 'FatParcela'], ['Controle', 'Sub'],
          [FatID, FatNum, FatParcela], [Controle, Sub], False, '',
          TabLctA);
          //
        end;
        //
        QrLct.Next;
      end;
    end;
    //
    QrPQE.Next;
  end;
end;

procedure TFmPQCorrigeFatID.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQCorrigeFatID.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQCorrigeFatID.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmPQCorrigeFatID.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQCorrigeFatID.QrPQEAfterOpen(DataSet: TDataSet);
begin
  BtCorrige.Enabled := QrPQE.RecordCount > 0;
end;

procedure TFmPQCorrigeFatID.QrPQEAfterScroll(DataSet: TDataSet);
var
  Fornecedor, NotaFiscal, Transporta, Conhecimento: String;
begin
  Fornecedor   := FormatFloat('0', QrPQEIQ.Value);
  NotaFiscal   := FormatFloat('0', QrPQENF.Value);
  Transporta   := FormatFloat('0', QrPQETransportadora.Value);
  Conhecimento := FormatFloat('0', QrPQEConhecimento.Value);
  //
  QrLct.Close;
  if QrPQENF.Value > 0 then
  begin
    QrLct.SQL.Clear;
    QrLct.SQL.Add('SELECT Data, Controle, Carteira, Debito, FatID, NotaFiscal, Fornecedor, Sub');
    QrLct.SQL.Add('FROM ' + VAR_LCT);
    {
    QrLct.SQL.Add('WHERE FatID=0');
    QrLct.SQL.Add('AND Tipo in (0,2)');
    }
    QrLct.SQL.Add('WHERE Tipo in (0,2)');
    //
    QrLct.SQL.Add('AND (');
    QrLct.SQL.Add('  (Fornecedor=' + Fornecedor + ' AND NotaFiscal=' + NotaFiscal + ')');
    if QrPQEConhecimento.Value > 0 then
    begin
      QrLct.SQL.Add('  OR');
      QrLct.SQL.Add(' (Fornecedor=' + Transporta + ' AND NotaFiscal=' + Conhecimento + ')');
    end;
    //
    QrLct.SQL.Add(')');
    UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
  end;
end;

procedure TFmPQCorrigeFatID.QrPQEBeforeClose(DataSet: TDataSet);
begin
  QrLct.Close;
  BtCorrige.Enabled := False;
end;

end.
