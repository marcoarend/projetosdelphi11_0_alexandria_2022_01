object FmPQENew: TFmPQENew
  Left = 377
  Top = 210
  Caption = 'QUI-ENTRA-002 :: Entrada de Uso e Consumo'
  ClientHeight = 552
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 91
    Width = 1008
    Height = 82
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label17: TLabel
      Left = 64
      Top = 0
      Width = 65
      Height = 13
      Caption = 'Data entrada:'
    end
    object Label11: TLabel
      Left = 180
      Top = 0
      Width = 67
      Height = 13
      Caption = 'Data emiss'#227'o:'
    end
    object Label6: TLabel
      Left = 8
      Top = 40
      Width = 57
      Height = 13
      Caption = 'Fornecedor:'
    end
    object LaCI: TLabel
      Left = 296
      Top = 0
      Width = 70
      Height = 13
      Caption = 'Cliente interno:'
    end
    object Label10: TLabel
      Left = 8
      Top = 0
      Width = 36
      Height = 13
      Caption = 'Pedido:'
    end
    object LaSitPedido: TLabel
      Left = 108
      Top = 0
      Width = 6
      Height = 13
      Caption = '0'
      Visible = False
    end
    object TPEntrada: TdmkEditDateTimePicker
      Left = 65
      Top = 16
      Width = 112
      Height = 21
      Date = 40060.000000000000000000
      Time = 0.661368159722769600
      TabOrder = 1
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object TPEmissao: TdmkEditDateTimePicker
      Left = 180
      Top = 16
      Width = 112
      Height = 21
      Date = 40060.000000000000000000
      Time = 0.661368159722769600
      TabOrder = 2
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdFornecedor: TdmkEditCB
      Left = 8
      Top = 56
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBFornecedor
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBFornecedor: TdmkDBLookupComboBox
      Left = 64
      Top = 56
      Width = 497
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOME_E_DOC_ENTIDADE'
      ListSource = DsFornecedores
      TabOrder = 6
      dmkEditCB = EdFornecedor
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object CBCI: TdmkDBLookupComboBox
      Left = 352
      Top = 16
      Width = 641
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOME'
      ListSource = DsCI
      TabOrder = 4
      dmkEditCB = EdCI
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCI: TdmkEditCB
      Left = 296
      Top = 16
      Width = 55
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdCIChange
      DBLookupComboBox = CBCI
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdPedido: TdmkEdit
      Left = 8
      Top = 16
      Width = 52
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object RGMadeBy: TdmkRadioGroup
      Left = 568
      Top = 44
      Width = 425
      Height = 37
      Caption = ' Modo de fornecimento dos produtos: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Indefinido'
        'Fabrica'#231#227'o Pr'#243'pria'
        'Revenda/outros')
      TabOrder = 7
      QryCampo = 'MadeBy'
      UpdCampo = 'MadeBy'
      UpdType = utYes
      OldValor = 0
    end
  end
  object PnNFs: TPanel
    Left = 0
    Top = 173
    Width = 1008
    Height = 128
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object LarefNFe: TLabel
      Left = 224
      Top = -1
      Width = 150
      Height = 13
      Caption = 'Chave de acesso da NF-e (VP):'
    end
    object LaModNF: TLabel
      Left = 508
      Top = -2
      Width = 38
      Height = 13
      Caption = 'Modelo:'
    end
    object LaSerie: TLabel
      Left = 551
      Top = -2
      Width = 27
      Height = 13
      Caption = 'S'#233'rie:'
    end
    object LaNF: TLabel
      Left = 584
      Top = -1
      Width = 34
      Height = 13
      Caption = 'NF VP:'
    end
    object LaNFe_CC: TLabel
      Left = 224
      Top = 39
      Width = 150
      Height = 13
      Caption = 'Chave de acesso da NF-e (CC):'
    end
    object LamodCC: TLabel
      Left = 508
      Top = 38
      Width = 38
      Height = 13
      Caption = 'Modelo:'
    end
    object LaSerCC: TLabel
      Left = 551
      Top = 38
      Width = 27
      Height = 13
      Caption = 'S'#233'rie:'
    end
    object LaNF_CC: TLabel
      Left = 583
      Top = 37
      Width = 34
      Height = 13
      Caption = 'NF CC:'
    end
    object SbNF_VP: TSpeedButton
      Left = 652
      Top = 11
      Width = 25
      Height = 25
      Caption = '<>'
      OnClick = SbNF_VPClick
    end
    object LaNFe_RP: TLabel
      Left = 224
      Top = 79
      Width = 151
      Height = 13
      Caption = 'Chave de acesso da NF-e (RP):'
    end
    object LamodRP: TLabel
      Left = 508
      Top = 78
      Width = 38
      Height = 13
      Caption = 'Modelo:'
    end
    object LaSerRP: TLabel
      Left = 551
      Top = 78
      Width = 27
      Height = 13
      Caption = 'S'#233'rie:'
    end
    object LaNF_RP: TLabel
      Left = 588
      Top = 77
      Width = 35
      Height = 13
      Caption = 'NF RP:'
    end
    object Label4: TLabel
      Left = 684
      Top = -2
      Width = 125
      Height = 13
      Caption = 'Atrelamento com XML BD:'
    end
    object Label9: TLabel
      Left = 684
      Top = 38
      Width = 125
      Height = 13
      Caption = 'Atrelamento com XML BD:'
    end
    object Label15: TLabel
      Left = 684
      Top = 78
      Width = 125
      Height = 13
      Caption = 'Atrelamento com XML BD:'
      Enabled = False
    end
    object SbNF_CC: TSpeedButton
      Left = 652
      Top = 51
      Width = 25
      Height = 25
      Caption = '<>'
      OnClick = SbNF_CCClick
    end
    object SpeedButton2: TSpeedButton
      Left = 652
      Top = 91
      Width = 25
      Height = 25
      Caption = '<>'
      Enabled = False
      OnClick = SpeedButton2Click
    end
    object EdrefNFe: TdmkEdit
      Left = 224
      Top = 13
      Width = 280
      Height = 21
      MaxLength = 44
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdrefNFeExit
      OnRedefinido = EdrefNFeRedefinido
    end
    object EdModNF: TdmkEdit
      Left = 508
      Top = 13
      Width = 39
      Height = 21
      Alignment = taRightJustify
      MaxLength = 2
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSerie: TdmkEdit
      Left = 551
      Top = 13
      Width = 31
      Height = 21
      Alignment = taRightJustify
      MaxLength = 3
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNF: TdmkEdit
      Left = 584
      Top = 13
      Width = 66
      Height = 21
      Alignment = taRightJustify
      MaxLength = 9
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFe_CC: TdmkEdit
      Left = 224
      Top = 53
      Width = 280
      Height = 21
      MaxLength = 44
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdNFe_CCExit
      OnRedefinido = EdNFe_CCRedefinido
    end
    object EdmodCC: TdmkEdit
      Left = 508
      Top = 53
      Width = 39
      Height = 21
      Alignment = taRightJustify
      MaxLength = 2
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSerCC: TdmkEdit
      Left = 551
      Top = 53
      Width = 31
      Height = 21
      Alignment = taRightJustify
      MaxLength = 3
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNF_CC: TdmkEdit
      Left = 583
      Top = 53
      Width = 67
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 6
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000000'
      QryCampo = 'NF_CC'
      UpdCampo = 'NF_CC'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFe_RP: TdmkEdit
      Left = 224
      Top = 93
      Width = 280
      Height = 21
      MaxLength = 44
      TabOrder = 15
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdNFe_RPExit
      OnRedefinido = EdNFe_RPRedefinido
    end
    object EdmodRP: TdmkEdit
      Left = 508
      Top = 93
      Width = 39
      Height = 21
      Alignment = taRightJustify
      MaxLength = 2
      TabOrder = 16
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSerRP: TdmkEdit
      Left = 551
      Top = 93
      Width = 31
      Height = 21
      Alignment = taRightJustify
      MaxLength = 3
      TabOrder = 17
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNF_RP: TdmkEdit
      Left = 584
      Top = 93
      Width = 67
      Height = 21
      Alignment = taRightJustify
      TabOrder = 18
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 6
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000000'
      QryCampo = 'NFRef'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFVP_StaLnk: TdmkEdit
      Left = 684
      Top = 13
      Width = 28
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      MaxLength = 9
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFVP_FatID: TdmkEdit
      Left = 716
      Top = 13
      Width = 39
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      MaxLength = 2
      ReadOnly = True
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFVP_FatNum: TdmkEdit
      Left = 759
      Top = 13
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      MaxLength = 3
      ReadOnly = True
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdNFVP_FatNumChange
    end
    object EdNFCC_StaLnk: TdmkEdit
      Left = 684
      Top = 53
      Width = 28
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      MaxLength = 9
      ReadOnly = True
      TabOrder = 12
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFCC_FatID: TdmkEdit
      Left = 716
      Top = 53
      Width = 39
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      MaxLength = 2
      ReadOnly = True
      TabOrder = 13
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFCC_FatNum: TdmkEdit
      Left = 759
      Top = 53
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      MaxLength = 3
      ReadOnly = True
      TabOrder = 14
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdNFCC_FatNumChange
    end
    object EdNFRP_StaLnk: TdmkEdit
      Left = 684
      Top = 93
      Width = 28
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      MaxLength = 9
      ReadOnly = True
      TabOrder = 19
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFRP_FatID: TdmkEdit
      Left = 716
      Top = 93
      Width = 39
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      MaxLength = 2
      ReadOnly = True
      TabOrder = 20
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNFRP_FatNum: TdmkEdit
      Left = 759
      Top = 93
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      MaxLength = 3
      ReadOnly = True
      TabOrder = 21
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CkTipoNF: TdmkCheckBox
      Left = 12
      Top = 12
      Width = 162
      Height = 17
      Caption = 'Nota Fiscal Eletr'#244'nica (NF-e)'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = CkTipoNFClick
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
  end
  object PnInativo: TPanel
    Left = 0
    Top = 301
    Width = 1008
    Height = 0
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    Visible = False
    object Label2: TLabel
      Left = 270
      Top = 0
      Width = 57
      Height = 13
      Caption = '%RICMS F.:'
      Enabled = False
      Visible = False
    end
    object Label12: TLabel
      Left = 202
      Top = -1
      Width = 52
      Height = 13
      Caption = 'Valor juros:'
      Enabled = False
      Visible = False
    end
    object Label1: TLabel
      Left = 409
      Top = 0
      Width = 62
      Height = 13
      Caption = '%RICMS NF:'
      Enabled = False
      Visible = False
    end
    object Label13: TLabel
      Left = 342
      Top = 0
      Width = 54
      Height = 13
      Caption = '%ICMS NF:'
      Enabled = False
      Visible = False
    end
    object EdRICMSF: TdmkEdit
      Left = 270
      Top = 16
      Width = 68
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 1
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdJuros: TdmkEdit
      Left = 202
      Top = 16
      Width = 64
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdRICMS: TdmkEdit
      Left = 409
      Top = 16
      Width = 68
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdICMS: TdmkEdit
      Left = 342
      Top = 16
      Width = 60
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object GB_R: TGroupBox
      Left = 961
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 914
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 317
        Height = 31
        Caption = 'Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 317
        Height = 31
        Caption = 'Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 317
        Height = 31
        Caption = 'Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 489
    Width = 1008
    Height = 63
    Align = alBottom
    TabOrder = 4
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 46
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 861
        Top = 0
        Width = 143
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 14
          Top = 3
          Width = 119
          Height = 39
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 7
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 445
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 6
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 945
        Height = 16
        Caption = 
          'VP: NFe de venda do fornecedor para a empresa ou cliente.       ' +
          '  RP:  Simples remessa do formecedor do cliente.        CC: NFe ' +
          'do cliente interno para a empresa.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 945
        Height = 16
        Caption = 
          'VP: NFe de venda do fornecedor para a empresa ou cliente.       ' +
          '  RP:  Simples remessa do formecedor do cliente.        CC: NFe ' +
          'do cliente interno para a empresa.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 1008
    Height = 44
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object LaEmpresa: TLabel
      Left = 64
      Top = 4
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object LaLancamento: TLabel
      Left = 8
      Top = 4
      Width = 40
      Height = 13
      Caption = 'N'#250'mero:'
    end
    object Label31: TLabel
      Left = 372
      Top = 4
      Width = 59
      Height = 13
      Caption = 'Regra fiscal:'
    end
    object LaCondicaoPG: TLabel
      Left = 696
      Top = 4
      Width = 119
      Height = 13
      Caption = 'Condi'#231#227'o de pagamento:'
    end
    object BtCondicaoPG: TSpeedButton
      Left = 972
      Top = 19
      Width = 23
      Height = 23
      Caption = '...'
      OnClick = BtCondicaoPGClick
    end
    object EdEmpresa: TdmkEditCB
      Left = 64
      Top = 20
      Width = 55
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Empresa'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEmpresaChange
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 119
      Top = 20
      Width = 250
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 2
      TabStop = False
      dmkEditCB = EdEmpresa
      QryCampo = 'Empresa'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCodigo: TdmkEdit
      Left = 8
      Top = 20
      Width = 52
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CBRegrFiscal: TdmkDBLookupComboBox
      Left = 416
      Top = 20
      Width = 278
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsFisRegCad
      TabOrder = 4
      dmkEditCB = EdRegrFiscal
      QryCampo = 'Empresa'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdRegrFiscal: TdmkEditCB
      Left = 372
      Top = 20
      Width = 44
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Empresa'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEmpresaChange
      DBLookupComboBox = CBRegrFiscal
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdCondicaoPG: TdmkEditCB
      Left = 696
      Top = 20
      Width = 44
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCondicaoPG
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCondicaoPG: TdmkDBLookupComboBox
      Left = 740
      Top = 20
      Width = 229
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsPediPrzCab
      TabOrder = 6
      dmkEditCB = EdCondicaoPG
      UpdType = utNil
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object PnDados3: TPanel
    Left = 0
    Top = 301
    Width = 1008
    Height = 144
    Align = alClient
    TabOrder = 3
    object Label16: TLabel
      Left = 8
      Top = -1
      Width = 171
      Height = 13
      Caption = 'Indicador do tipo de pagamento [F4]'
      FocusControl = EdIND_PGTO
    end
    object Label18: TLabel
      Left = 353
      Top = -1
      Width = 54
      Height = 13
      Caption = '$ Produtos:'
    end
    object Label20: TLabel
      Left = 445
      Top = -1
      Width = 74
      Height = 13
      Caption = '$ Frete na NFe:'
    end
    object Label22: TLabel
      Left = 537
      Top = -1
      Width = 46
      Height = 13
      Caption = '$ Seguro:'
    end
    object Label28: TLabel
      Left = 629
      Top = -1
      Width = 75
      Height = 13
      Caption = '$ Desp. Acess.:'
    end
    object Label29: TLabel
      Left = 721
      Top = -1
      Width = 58
      Height = 13
      Caption = '$ Desconto:'
    end
    object Label30: TLabel
      Left = 813
      Top = -1
      Width = 25
      Height = 13
      Caption = '$ IPI:'
    end
    object Label3: TLabel
      Left = 905
      Top = -1
      Width = 53
      Height = 13
      Caption = '$ Total NF:'
    end
    object Label5: TLabel
      Left = 8
      Top = 40
      Width = 61
      Height = 13
      Caption = 'Qtde l'#237'quido:'
    end
    object Label7: TLabel
      Left = 102
      Top = 40
      Width = 43
      Height = 13
      Caption = 'kg Bruto:'
    end
    object Label108: TLabel
      Left = 196
      Top = 40
      Width = 139
      Height = 13
      Caption = 'Indicador do tipo de frete [F4]'
      FocusControl = EdIND_FRT
    end
    object Label19: TLabel
      Left = 510
      Top = 40
      Width = 75
      Height = 13
      Caption = 'Transportadora:'
    end
    object EdIND_PGTO_TXT: TdmkEdit
      Left = 30
      Top = 15
      Width = 319
      Height = 21
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdIND_PGTO: TdmkEdit
      Left = 8
      Top = 15
      Width = 21
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '9'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'IND_PGTO'
      UpdCampo = 'IND_PGTO'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdIND_PGTOChange
      OnKeyDown = EdIND_PGTOKeyDown
    end
    object EdICMSTot_vProd: TdmkEdit
      Left = 353
      Top = 15
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = CalculaTotalNF
    end
    object EdICMSTot_vFrete: TdmkEdit
      Left = 445
      Top = 15
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = CalculaTotalNF
    end
    object EdICMSTot_vSeg: TdmkEdit
      Left = 537
      Top = 15
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = CalculaTotalNF
    end
    object EdICMSTot_vOutro: TdmkEdit
      Left = 629
      Top = 15
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = CalculaTotalNF
    end
    object EdICMSTot_vDesc: TdmkEdit
      Left = 721
      Top = 15
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = CalculaTotalNF
    end
    object EdIPI: TdmkEdit
      Left = 813
      Top = 15
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = CalculaTotalNF
    end
    object EdValorNF: TdmkEdit
      Left = 905
      Top = 15
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdkgLiquido: TdmkEdit
      Left = 8
      Top = 56
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdkgBruto: TdmkEdit
      Left = 102
      Top = 56
      Width = 91
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdIND_FRT: TdmkEdit
      Left = 196
      Top = 56
      Width = 21
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '9'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'IND_PGTO'
      UpdCampo = 'IND_PGTO'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdIND_FRTChange
      OnKeyDown = EdIND_FRTKeyDown
    end
    object EdIND_FRT_TXT: TdmkEdit
      Left = 218
      Top = 56
      Width = 291
      Height = 21
      ReadOnly = True
      TabOrder = 12
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdTransportador: TdmkEditCB
      Left = 510
      Top = 56
      Width = 55
      Height = 21
      Alignment = taRightJustify
      TabOrder = 13
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBTransportador
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBTransportador: TdmkDBLookupComboBox
      Left = 569
      Top = 56
      Width = 424
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOME_E_DOC_ENTIDADE'
      ListSource = DsTransportadores
      TabOrder = 14
      dmkEditCB = EdTransportador
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 78
      Width = 993
      Height = 59
      Caption = 'Conhecimento do Frete: e restitui'#231#227'o de tributos:'
      TabOrder = 15
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 989
        Height = 42
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 461
          Height = 42
          Align = alLeft
          BevelOuter = bvNone
          Caption = 'Panel4'
          TabOrder = 0
          object Label27: TLabel
            Left = 4
            Top = 0
            Width = 124
            Height = 13
            Caption = 'Chave de acesso da CTe:'
          end
          object Label21: TLabel
            Left = 312
            Top = 0
            Width = 38
            Height = 13
            Caption = 'Modelo:'
          end
          object Label23: TLabel
            Left = 355
            Top = 0
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object Label25: TLabel
            Left = 388
            Top = 0
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
          end
          object EdCte_Id: TdmkEdit
            Left = 3
            Top = 14
            Width = 306
            Height = 21
            MaxLength = 44
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdrefNFeExit
          end
          object EdCTe_mod: TdmkEdit
            Left = 312
            Top = 14
            Width = 39
            Height = 21
            Alignment = taRightJustify
            MaxLength = 2
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCTe_serie: TdmkEdit
            Left = 355
            Top = 14
            Width = 31
            Height = 21
            Alignment = taRightJustify
            MaxLength = 3
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdConhecim: TdmkEdit
            Left = 388
            Top = 14
            Width = 70
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 9
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object Panel6: TPanel
          Left = 461
          Top = 0
          Width = 528
          Height = 42
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Label8: TLabel
            Left = 4
            Top = 0
            Width = 62
            Height = 13
            Caption = '$ Frete CT-e:'
          end
          object Label14: TLabel
            Left = 102
            Top = 0
            Width = 75
            Height = 13
            Caption = 'ICMS % e valor:'
          end
          object Label24: TLabel
            Left = 243
            Top = 0
            Width = 66
            Height = 13
            Caption = 'PIS % e valor:'
          end
          object Label26: TLabel
            Left = 385
            Top = 0
            Width = 88
            Height = 13
            Caption = 'COFINS % e valor:'
          end
          object EdFrete: TdmkEdit
            Left = 4
            Top = 14
            Width = 96
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdFreteChange
          end
          object EdFreteRpICMS: TdmkEdit
            Left = 102
            Top = 14
            Width = 48
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdFreteRpICMSChange
          end
          object EdFreteRvICMS: TdmkEdit
            Left = 153
            Top = 14
            Width = 88
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdFreteRpPIS: TdmkEdit
            Left = 243
            Top = 14
            Width = 48
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdFreteRpPISChange
          end
          object EdFreteRvPIS: TdmkEdit
            Left = 294
            Top = 14
            Width = 88
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdFreteRpCOFINS: TdmkEdit
            Left = 385
            Top = 14
            Width = 48
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdFreteRpCOFINSChange
          end
          object EdFreteRvCOFINS: TdmkEdit
            Left = 436
            Top = 14
            Width = 88
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
  end
  object QrFornecedores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Tipo, ent.CNPJ, ent.CPF,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,'
      'CONCAT(IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome),'
      '  " - ", IF(ent.Tipo=0, ent.CNPJ, ent.CPF)) NOME_E_DOC_ENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece2="V"'
      'ORDER BY NOME')
    Left = 12
    Top = 4
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedoresNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrFornecedoresTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrFornecedoresCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrFornecedoresCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrFornecedoresNOME_E_DOC_ENTIDADE: TWideStringField
      FieldName = 'NOME_E_DOC_ENTIDADE'
      Size = 121
    end
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 40
    Top = 4
  end
  object QrTransportadores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Tipo, ent.CNPJ, ent.CPF,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,'
      'CONCAT(IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome),'
      '  " - ", IF(ent.Tipo=0, ent.CNPJ, ent.CPF)) NOME_E_DOC_ENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece3='#39'V'#39
      'ORDER BY NOME')
    Left = 68
    Top = 4
    object QrTransportadoresTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrTransportadoresCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTransportadoresCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTransportadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportadoresNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrTransportadoresNOME_E_DOC_ENTIDADE: TWideStringField
      FieldName = 'NOME_E_DOC_ENTIDADE'
      Size = 121
    end
  end
  object DsTransportadores: TDataSource
    DataSet = QrTransportadores
    Left = 96
    Top = 4
  end
  object QrItens: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *FROM pqpedits'
      'WHERE Codigo=:P0')
    Left = 464
    Top = 4
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItensCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pqpedits.Codigo'
    end
    object QrItensControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'DBMBWET.pqpedits.Controle'
    end
    object QrItensConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'DBMBWET.pqpedits.Conta'
    end
    object QrItensInsumo: TIntegerField
      FieldName = 'Insumo'
      Origin = 'DBMBWET.pqpedits.Insumo'
    end
    object QrItensVolumes: TIntegerField
      FieldName = 'Volumes'
      Origin = 'DBMBWET.pqpedits.Volumes'
    end
    object QrItensPesoVB: TFloatField
      FieldName = 'PesoVB'
      Origin = 'DBMBWET.pqpedits.PesoVB'
    end
    object QrItensPesoVL: TFloatField
      FieldName = 'PesoVL'
      Origin = 'DBMBWET.pqpedits.PesoVL'
    end
    object QrItensValorItem: TFloatField
      FieldName = 'ValorItem'
      Origin = 'DBMBWET.pqpedits.ValorItem'
    end
    object QrItensIPI: TFloatField
      FieldName = 'IPI'
      Origin = 'DBMBWET.pqpedits.IPI'
    end
    object QrItensTotalCusto: TFloatField
      FieldName = 'TotalCusto'
      Origin = 'DBMBWET.pqpedits.TotalCusto'
    end
    object QrItensTotalPeso: TFloatField
      FieldName = 'TotalPeso'
      Origin = 'DBMBWET.pqpedits.TotalPeso'
    end
    object QrItensPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'DBMBWET.pqpedits.Preco'
    end
    object QrItensICMS: TFloatField
      FieldName = 'ICMS'
      Origin = 'DBMBWET.pqpedits.ICMS'
    end
    object QrItensEntrada: TFloatField
      FieldName = 'Entrada'
      Origin = 'DBMBWET.pqpedits.Entrada'
    end
    object QrItensPrazo: TWideStringField
      FieldName = 'Prazo'
      Origin = 'DBMBWET.pqpedits.Prazo'
      Size = 128
    end
    object QrItensCFin: TFloatField
      FieldName = 'CFin'
      Origin = 'DBMBWET.pqpedits.CFin'
    end
  end
  object QrInsIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'INSERT INTO pqeits SET'
      'Insumo=:P0,'
      'Volumes=:P1,'
      'PesoVB=:P2,'
      'PesoVL=:P3,'
      'ValorItem=:P4,'
      'IPI=:P5,'
      'RIPI=:P6,'
      'TotalCusto=:P7,'
      'TotalPeso=:P8,'
      'CFin=:P9,'
      'Codigo=:P10,'
      'Conta=:11'
      ''
      '')
    Left = 520
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P8'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P9'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P10'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = '11'
        ParamType = ptUnknown
      end>
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NOME'
      'FROM entidades'
      'WHERE Cliente2='#39'V'#39
      'ORDER BY Nome')
    Left = 408
    Top = 4
    object QrCITipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCICNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrCICPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 408
    Top = 52
  end
  object Qr00_NFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, IDCtrl,'
      'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk,'
      'ide_mod, ide_serie, ide_nNF  '
      'FROM nfecaba '
      'WHERE FatID=53 '
      'AND Empresa=-11 '
      'AND ide_mod>0 '
      'AND ide_serie>0 '
      'AND ide_nNF>0 ')
    Left = 678
    Top = 385
    object Qr00_NFeCabAFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object Qr00_NFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object Qr00_NFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object Qr00_NFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object Qr00_NFeCabAAtrelaFatID: TIntegerField
      FieldName = 'AtrelaFatID'
      Required = True
    end
    object Qr00_NFeCabAAtrelaFatNum: TIntegerField
      FieldName = 'AtrelaFatNum'
      Required = True
    end
    object Qr00_NFeCabAAtrelaStaLnk: TSmallintField
      FieldName = 'AtrelaStaLnk'
      Required = True
    end
    object Qr00_NFeCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
      Required = True
    end
    object Qr00_NFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      Required = True
    end
    object Qr00_NFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Required = True
    end
    object Qr00_NFeCabAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
  end
  object QrPediVdaIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pedivdaits')
    Left = 768
    Top = 385
    object QrPediVdaItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediVdaItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediVdaItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrPediVdaItsPrecoO: TFloatField
      FieldName = 'PrecoO'
      Required = True
    end
    object QrPediVdaItsPrecoR: TFloatField
      FieldName = 'PrecoR'
      Required = True
    end
    object QrPediVdaItsQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrPediVdaItsQuantC: TFloatField
      FieldName = 'QuantC'
      Required = True
    end
    object QrPediVdaItsQuantV: TFloatField
      FieldName = 'QuantV'
      Required = True
    end
    object QrPediVdaItsValBru: TFloatField
      FieldName = 'ValBru'
      Required = True
    end
    object QrPediVdaItsDescoP: TFloatField
      FieldName = 'DescoP'
      Required = True
    end
    object QrPediVdaItsDescoV: TFloatField
      FieldName = 'DescoV'
      Required = True
    end
    object QrPediVdaItsValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrPediVdaItsPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrPediVdaItsMedidaC: TFloatField
      FieldName = 'MedidaC'
      Required = True
    end
    object QrPediVdaItsMedidaL: TFloatField
      FieldName = 'MedidaL'
      Required = True
    end
    object QrPediVdaItsMedidaA: TFloatField
      FieldName = 'MedidaA'
      Required = True
    end
    object QrPediVdaItsMedidaE: TFloatField
      FieldName = 'MedidaE'
      Required = True
    end
    object QrPediVdaItsPercCustom: TFloatField
      FieldName = 'PercCustom'
      Required = True
    end
    object QrPediVdaItsCustomizad: TSmallintField
      FieldName = 'Customizad'
      Required = True
    end
    object QrPediVdaItsInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Required = True
    end
    object QrPediVdaItsOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrPediVdaItsReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 11
    end
    object QrPediVdaItsMulti: TIntegerField
      FieldName = 'Multi'
      Required = True
    end
    object QrPediVdaItsvProd: TFloatField
      FieldName = 'vProd'
      Required = True
    end
    object QrPediVdaItsvFrete: TFloatField
      FieldName = 'vFrete'
      Required = True
    end
    object QrPediVdaItsvSeg: TFloatField
      FieldName = 'vSeg'
      Required = True
    end
    object QrPediVdaItsvOutro: TFloatField
      FieldName = 'vOutro'
      Required = True
    end
    object QrPediVdaItsvDesc: TFloatField
      FieldName = 'vDesc'
      Required = True
    end
    object QrPediVdaItsvBC: TFloatField
      FieldName = 'vBC'
      Required = True
    end
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ICMSRec_pAliq, PISRec_pAliq,  '
      'COFINSRec_pAliq  '
      'FROM gragru1 '
      'WHERE Nivel1=1 ')
    Left = 584
    Top = 4
    object QrGraGru1ICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrGraGru1PISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrGraGru1COFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
  end
  object QrItsI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=53'
      'AND FatNum=27'
      'AND Empresa =-11'
      'ORDER BY nItem')
    Left = 616
    Top = 276
    object QrItsIFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrItsIFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrItsIEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrItsInItem: TIntegerField
      FieldName = 'nItem'
      Required = True
    end
    object QrItsIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrItsIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrItsIprod_cBarra: TWideStringField
      FieldName = 'prod_cBarra'
      Size = 30
    end
    object QrItsIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrItsIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrItsIprod_CEST: TIntegerField
      FieldName = 'prod_CEST'
      Required = True
    end
    object QrItsIprod_indEscala: TWideStringField
      FieldName = 'prod_indEscala'
      Size = 1
    end
    object QrItsIprod_CNPJFab: TWideStringField
      FieldName = 'prod_CNPJFab'
      Size = 14
    end
    object QrItsIprod_cBenef: TWideStringField
      FieldName = 'prod_cBenef'
      Size = 10
    end
    object QrItsIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrItsIprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrItsIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
      Required = True
    end
    object QrItsIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrItsIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
      Required = True
    end
    object QrItsIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
      Required = True
    end
    object QrItsIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      Required = True
    end
    object QrItsIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrItsIprod_cBarraTrib: TWideStringField
      FieldName = 'prod_cBarraTrib'
      Size = 30
    end
    object QrItsIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrItsIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
      Required = True
    end
    object QrItsIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
      Required = True
    end
    object QrItsIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Required = True
    end
    object QrItsIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Required = True
    end
    object QrItsIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
      Required = True
    end
    object QrItsIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrItsIprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
      Required = True
    end
    object QrItsIprod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrItsIprod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrItsITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
      Required = True
    end
    object QrItsI_Ativo_: TSmallintField
      FieldName = '_Ativo_'
      Required = True
    end
    object QrItsIInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Required = True
    end
    object QrItsIEhServico: TIntegerField
      FieldName = 'EhServico'
      Required = True
    end
    object QrItsIUsaSubsTrib: TSmallintField
      FieldName = 'UsaSubsTrib'
      Required = True
    end
    object QrItsIICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      Required = True
    end
    object QrItsIICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
      Required = True
    end
    object QrItsIICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      Required = True
    end
    object QrItsIICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
      Required = True
    end
    object QrItsIIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      Required = True
    end
    object QrItsIIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
      Required = True
    end
    object QrItsIIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      Required = True
    end
    object QrItsIIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
      Required = True
    end
    object QrItsIPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      Required = True
    end
    object QrItsIPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
      Required = True
    end
    object QrItsIPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      Required = True
    end
    object QrItsIPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
      Required = True
    end
    object QrItsICOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      Required = True
    end
    object QrItsICOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
      Required = True
    end
    object QrItsICOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      Required = True
    end
    object QrItsICOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
      Required = True
    end
    object QrItsIMeuID: TIntegerField
      FieldName = 'MeuID'
      Required = True
    end
    object QrItsINivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrItsIGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrItsIUnidMedCom: TIntegerField
      FieldName = 'UnidMedCom'
      Required = True
    end
    object QrItsIUnidMedTrib: TIntegerField
      FieldName = 'UnidMedTrib'
      Required = True
    end
    object QrItsIICMSRec_vBCST: TFloatField
      FieldName = 'ICMSRec_vBCST'
      Required = True
    end
    object QrItsIICMSRec_vICMSST: TFloatField
      FieldName = 'ICMSRec_vICMSST'
      Required = True
    end
    object QrItsIICMSRec_pAliqST: TFloatField
      FieldName = 'ICMSRec_pAliqST'
      Required = True
    end
    object QrItsITem_II: TSmallintField
      FieldName = 'Tem_II'
      Required = True
    end
    object QrItsIprod_nFCI: TWideStringField
      FieldName = 'prod_nFCI'
      Size = 36
    end
    object QrItsIStqMovValA: TIntegerField
      FieldName = 'StqMovValA'
      Required = True
    end
    object QrItsIAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrItsIAtrelaID: TIntegerField
      FieldName = 'AtrelaID'
      Required = True
    end
    object QrItsIprod_vTotItm: TFloatField
      FieldName = 'prod_vTotItm'
    end
  end
  object QrItsV: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeitsv'
      'WHERE FatID=53 '
      'AND FatNum=27 '
      'AND Empresa =-11 '
      'AND nItem=1'
      ' ')
    Left = 616
    Top = 328
    object QrItsVnItem: TIntegerField
      FieldName = 'nItem'
      Required = True
    end
    object QrItsVInfAdProd: TWideMemoField
      FieldName = 'InfAdProd'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrItsN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeitsn'
      'WHERE FatID=53 '
      'AND FatNum=27 '
      'AND Empresa =-11 '
      'AND nItem=1'
      ' ')
    Left = 664
    Top = 276
    object QrItsNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
      Required = True
    end
    object QrItsNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
      Required = True
    end
    object QrItsNICMS_vBCEfet: TFloatField
      FieldName = 'ICMS_vBCEfet'
      Required = True
    end
    object QrItsNICMS_pICMSEfet: TFloatField
      FieldName = 'ICMS_pICMSEfet'
      Required = True
    end
    object QrItsNICMS_vICMSEfet: TFloatField
      FieldName = 'ICMS_vICMSEfet'
      Required = True
    end
    object QrItsNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
      Required = True
    end
    object QrItsNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
      Required = True
    end
    object QrItsNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
      Required = True
    end
    object QrItsNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
      Required = True
    end
    object QrItsNICMS_vICMSOp: TFloatField
      FieldName = 'ICMS_vICMSOp'
      Required = True
    end
    object QrItsNICMS_pDif: TFloatField
      FieldName = 'ICMS_pDif'
      Required = True
    end
    object QrItsNICMS_vICMSDif: TFloatField
      FieldName = 'ICMS_vICMSDif'
      Required = True
    end
    object QrItsNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
      Required = True
    end
    object QrItsNICMS_vBCFCP: TFloatField
      FieldName = 'ICMS_vBCFCP'
      Required = True
    end
    object QrItsNICMS_pFCP: TFloatField
      FieldName = 'ICMS_pFCP'
      Required = True
    end
    object QrItsNICMS_vFCP: TFloatField
      FieldName = 'ICMS_vFCP'
      Required = True
    end
    object QrItsNICMS_pFCPDif: TFloatField
      FieldName = 'ICMS_pFCPDif'
      Required = True
    end
    object QrItsNICMS_vFCPDif: TFloatField
      FieldName = 'ICMS_vFCPDif'
      Required = True
    end
    object QrItsNICMS_vFCPEfet: TFloatField
      FieldName = 'ICMS_vFCPEfet'
      Required = True
    end
    object QrItsNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
      Required = True
    end
    object QrItsNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
      Required = True
    end
    object QrItsNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
      Required = True
    end
    object QrItsNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
      Required = True
    end
    object QrItsNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
      Required = True
    end
    object QrItsNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
      Required = True
    end
    object QrItsNICMS_vBCFCPST: TFloatField
      FieldName = 'ICMS_vBCFCPST'
      Required = True
    end
    object QrItsNICMS_pFCPST: TFloatField
      FieldName = 'ICMS_pFCPST'
      Required = True
    end
    object QrItsNICMS_vFCPST: TFloatField
      FieldName = 'ICMS_vFCPST'
      Required = True
    end
    object QrItsNICMS_CSOSN: TIntegerField
      FieldName = 'ICMS_CSOSN'
      Required = True
    end
    object QrItsNICMS_UFST: TWideStringField
      FieldName = 'ICMS_UFST'
      Size = 2
    end
    object QrItsNICMS_pBCOp: TFloatField
      FieldName = 'ICMS_pBCOp'
      Required = True
    end
    object QrItsNICMS_vBCSTRet: TFloatField
      FieldName = 'ICMS_vBCSTRet'
      Required = True
    end
    object QrItsNICMS_pST: TFloatField
      FieldName = 'ICMS_pST'
      Required = True
    end
    object QrItsNICMS_vICMSSTRet: TFloatField
      FieldName = 'ICMS_vICMSSTRet'
      Required = True
    end
    object QrItsNICMS_vICMSDeson: TFloatField
      FieldName = 'ICMS_vICMSDeson'
      Required = True
    end
    object QrItsNICMS_vBCFCPSTRet: TFloatField
      FieldName = 'ICMS_vBCFCPSTRet'
      Required = True
    end
    object QrItsNICMS_pFCPSTRet: TFloatField
      FieldName = 'ICMS_pFCPSTRet'
      Required = True
    end
    object QrItsNICMS_vFCPSTRet: TFloatField
      FieldName = 'ICMS_vFCPSTRet'
      Required = True
    end
    object QrItsNICMS_motDesICMS: TSmallintField
      FieldName = 'ICMS_motDesICMS'
      Required = True
    end
    object QrItsNICMS_pCredSN: TFloatField
      FieldName = 'ICMS_pCredSN'
      Required = True
    end
    object QrItsNICMS_vCredICMSSN: TFloatField
      FieldName = 'ICMS_vCredICMSSN'
      Required = True
    end
    object QrItsNCOD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrItsNICMS_vICMSSubstituto: TFloatField
      FieldName = 'ICMS_vICMSSubstituto'
      Required = True
    end
    object QrItsNICMS_vICMSSTDeson: TFloatField
      FieldName = 'ICMS_vICMSSTDeson'
      Required = True
    end
    object QrItsNICMS_motDesICMSST: TSmallintField
      FieldName = 'ICMS_motDesICMSST'
      Required = True
    end
    object QrItsNICMS_pRedBCEfet: TFloatField
      FieldName = 'ICMS_pRedBCEfet'
      Required = True
    end
  end
  object QrItsO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeitso'
      'WHERE FatID=53 '
      'AND FatNum=27 '
      'AND Empresa =-11 '
      'AND nItem=1'
      ' ')
    Left = 712
    Top = 276
    object QrItsOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrItsOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrItsOIPI_clEnq: TWideStringField
      FieldName = 'IPI_clEnq'
      Size = 5
    end
    object QrItsOIPI_CNPJProd: TWideStringField
      FieldName = 'IPI_CNPJProd'
      Size = 14
    end
    object QrItsOIPI_cSelo: TWideStringField
      FieldName = 'IPI_cSelo'
      Size = 60
    end
    object QrItsOIPI_qSelo: TFloatField
      FieldName = 'IPI_qSelo'
    end
    object QrItsOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
      Required = True
    end
    object QrItsOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
      Required = True
    end
    object QrItsOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
      Required = True
    end
    object QrItsOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
      Required = True
    end
    object QrItsOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
      Required = True
    end
    object QrItsOIND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
  end
  object QrItsS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeitss'
      'WHERE FatID=53 '
      'AND FatNum=27 '
      'AND Empresa =-11 '
      'AND nItem=1'
      ' ')
    Left = 904
    Top = 276
    object QrItsSCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
      Required = True
    end
    object QrItsSCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
      Required = True
    end
    object QrItsSCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
      Required = True
    end
    object QrItsSCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
      Required = True
    end
    object QrItsSCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
      Required = True
    end
    object QrItsSCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
      Required = True
    end
    object QrItsSCOFINS_fatorBC: TFloatField
      FieldName = 'COFINS_fatorBC'
      Required = True
    end
  end
  object QrItsQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeitsq'
      'WHERE FatID=53 '
      'AND FatNum=27 '
      'AND Empresa =-11 '
      'AND nItem=1'
      ' ')
    Left = 796
    Top = 280
    object QrItsQPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
      Required = True
    end
    object QrItsQPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
      Required = True
    end
    object QrItsQPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
      Required = True
    end
    object QrItsQPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
      Required = True
    end
    object QrItsQPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
      Required = True
    end
    object QrItsQPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
      Required = True
    end
    object QrItsQPIS_fatorBC: TFloatField
      FieldName = 'PIS_fatorBC'
      Required = True
    end
  end
  object QrItsR: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeitsr'
      'WHERE FatID=53 '
      'AND FatNum=27 '
      'AND Empresa =-11 '
      'AND nItem=1'
      ' ')
    Left = 848
    Top = 276
    object QrItsRPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
      Required = True
    end
    object QrItsRPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
      Required = True
    end
    object QrItsRPISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
      Required = True
    end
    object QrItsRPISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
      Required = True
    end
    object QrItsRPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
      Required = True
    end
    object QrItsRPISST_indSomaPISST: TSmallintField
      FieldName = 'PISST_indSomaPISST'
      Required = True
    end
    object QrItsRPISST_fatorBCST: TFloatField
      FieldName = 'PISST_fatorBCST'
      Required = True
    end
  end
  object QrItsT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeitst'
      'WHERE FatID=53 '
      'AND FatNum=27 '
      'AND Empresa =-11 '
      'AND nItem=1'
      ' ')
    Left = 956
    Top = 276
    object QrItsTCOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
      Required = True
    end
    object QrItsTCOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
      Required = True
    end
    object QrItsTCOFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
      Required = True
    end
    object QrItsTCOFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
      Required = True
    end
    object QrItsTCOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
      Required = True
    end
    object QrItsTCOFINSST_indSomaCOFINSST: TSmallintField
      FieldName = 'COFINSST_indSomaCOFINSST'
      Required = True
    end
    object QrItsTCOFINSST_fatorBCST: TFloatField
      FieldName = 'COFINSST_fatorBCST'
      Required = True
    end
  end
  object QrCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=53'
      'AND FatNum=27'
      'AND Empresa =-11'
      '')
    Left = 576
    Top = 276
    object QrCabAemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
      Required = True
    end
    object QrCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrCabACodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrCabAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
      Required = True
    end
    object QrCabAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
      Required = True
    end
    object QrCabAICMSTot_vICMSDeson: TFloatField
      FieldName = 'ICMSTot_vICMSDeson'
      Required = True
    end
    object QrCabAICMSTot_vFCPUFDest: TFloatField
      FieldName = 'ICMSTot_vFCPUFDest'
      Required = True
    end
    object QrCabAICMSTot_vICMSUFDest: TFloatField
      FieldName = 'ICMSTot_vICMSUFDest'
      Required = True
    end
    object QrCabAICMSTot_vICMSUFRemet: TFloatField
      FieldName = 'ICMSTot_vICMSUFRemet'
      Required = True
    end
    object QrCabAICMSTot_vFCP: TFloatField
      FieldName = 'ICMSTot_vFCP'
      Required = True
    end
    object QrCabAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
      Required = True
    end
    object QrCabAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
      Required = True
    end
    object QrCabAICMSTot_vFCPST: TFloatField
      FieldName = 'ICMSTot_vFCPST'
      Required = True
    end
    object QrCabAICMSTot_vFCPSTRet: TFloatField
      FieldName = 'ICMSTot_vFCPSTRet'
      Required = True
    end
    object QrCabAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
      Required = True
    end
    object QrCabAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
      Required = True
    end
    object QrCabAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
      Required = True
    end
    object QrCabAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
      Required = True
    end
    object QrCabAICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
      Required = True
    end
    object QrCabAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
      Required = True
    end
    object QrCabAICMSTot_vIPIDevol: TFloatField
      FieldName = 'ICMSTot_vIPIDevol'
      Required = True
    end
    object QrCabAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
      Required = True
    end
    object QrCabAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
      Required = True
    end
    object QrCabAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
      Required = True
    end
    object QrCabAide_indPag: TSmallintField
      FieldName = 'ide_indPag'
      Required = True
    end
    object QrCabAModFrete: TSmallintField
      FieldName = 'ModFrete'
      Required = True
    end
  end
  object QrFisRegCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.ModeloNF,'
      'frc.Nome, frc.Financeiro, imp.Nome NO_MODELO_NF,'
      'frc.GenCtbD, frc.GenCtbC'
      'FROM fisregcad frc'
      'LEFT JOIN imprime imp ON imp.Codigo=frc.ModeloNF'
      'ORDER BY Nome')
    Left = 660
    Top = 4
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFisRegCadModeloNF: TIntegerField
      FieldName = 'ModeloNF'
      Required = True
    end
    object QrFisRegCadNO_MODELO_NF: TWideStringField
      FieldName = 'NO_MODELO_NF'
      Size = 100
    end
    object QrFisRegCadFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
    object QrFisRegCadGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
      Required = True
    end
    object QrFisRegCadGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
      Required = True
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 660
    Top = 49
  end
  object QrPediVda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Tipo TipoCart, '
      'frc.GenCtbD, frc.GenCtbC,'
      'pvd.CodUsu, pvd.MadeBy, pvd.CartEmis,'
      'pvd.RegrFiscal '
      'FROM pedivda pvd '
      'LEFT JOIN carteiras car ON car.Codigo=pvd.CartEmis '
      'LEFT JOIN fisregcad frc ON frc.Codigo=pvd.RegrFiscal')
    Left = 772
    Top = 333
    object QrPediVdaTipoCart: TIntegerField
      FieldName = 'TipoCart'
    end
    object QrPediVdaGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrPediVdaGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
    object QrPediVdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrPediVdaMadeBy: TSmallintField
      FieldName = 'MadeBy'
      Required = True
    end
    object QrPediVdaCartEmis: TIntegerField
      FieldName = 'CartEmis'
      Required = True
    end
    object QrPediVdaRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Required = True
    end
    object QrPediVdaRepresen: TIntegerField
      FieldName = 'Represen'
    end
    object QrPediVdaFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
    object QrPediVdaCliente: TIntegerField
      FieldName = 'Cliente'
    end
  end
  object QrPediPrzCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, MaxDesco,'
      'JurosMes, Parcelas, Percent1, Percent2'
      'FROM pediprzcab'
      'WHERE Aplicacao & :P0 <> 0'
      'ORDER BY Nome')
    Left = 872
    Top = 65532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrPediPrzCabCondPg: TSmallintField
      FieldName = 'CondPg'
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 872
    Top = 44
  end
  object QrPediPrzIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Dias,  Percent1 '
      'FROM pediprzits ')
    Left = 264
    Top = 271
    object QrPediPrzItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediPrzItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrPediPrzItsPercent1: TFloatField
      FieldName = 'Percent1'
      Required = True
    end
  end
  object QrLct: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.Data, la.Tipo, la.Carteira,  '
      'la.Controle, la.Sub '
      'FROM lct0001a la '
      'WHERE la.FatID=1001 '
      'AND la.FatParcRef=24 '
      'AND la.ID_Pgto = 0  ')
    Left = 172
    Top = 279
    object QrLctData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
  end
  object QrNFeCabY: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaby'
      'WHERE FatID=53'
      'AND FatNum>0'
      'AND Empresa=-11')
    Left = 956
    Top = 337
    object QrNFeCabYControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrNFeCabYnDup: TWideStringField
      FieldName = 'nDup'
      Required = True
      Size = 60
    end
    object QrNFeCabYdVenc: TDateField
      FieldName = 'dVenc'
      Required = True
    end
    object QrNFeCabYvDup: TFloatField
      FieldName = 'vDup'
      Required = True
    end
    object QrNFeCabYLancto: TIntegerField
      FieldName = 'Lancto'
      Required = True
    end
    object QrNFeCabYSub: TIntegerField
      FieldName = 'Sub'
      Required = True
    end
    object QrNFeCabYEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
  end
  object QrSumY: TMySQLQuery
    Database = DmProd.MySQLDatabase1
    Left = 932
    Top = 401
    object QrSumYvDup: TFloatField
      FieldName = 'vDup'
      Required = True
    end
  end
end
