unit OSsAbertas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, frxClass, frxDBSet,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UndmkProcFunc, UnDmkEnums,
  UnProjGroup_Vars, dmkDBGridZTO;

type
  TFmOSsAbertas = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    frxDsOSs: TfrxDBDataset;
    frxOSs_: TfrxReport;
    QrOSs: TMySQLQuery;
    QrOSsDataPedido: TDateField;
    QrOSsCliente: TIntegerField;
    QrOSsVendedor: TIntegerField;
    QrOSsOS: TIntegerField;
    QrOSsTexto: TWideStringField;
    QrOSsEntrega: TDateField;
    QrOSsFluxo: TIntegerField;
    QrOSsClasse: TWideStringField;
    QrOSsPedido: TIntegerField;
    QrOSsEspesTxt: TWideStringField;
    QrOSsCorTxt: TWideStringField;
    QrOSsM2Pedido: TFloatField;
    QrOSsObserv: TWideMemoField;
    QrOSsNOMECLI: TWideStringField;
    QrOSsNOMEVEN: TWideStringField;
    QrOSsNOMEMP: TWideStringField;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVendedores: TDataSource;
    QrMP: TmySQLQuery;
    QrMPCodigo: TIntegerField;
    QrMPNome: TWideStringField;
    DsMP: TDataSource;
    QrTransp: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsTransp: TDataSource;
    Panel5: TPanel;
    Panel6: TPanel;
    Label10: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    CBMP: TdmkDBLookupComboBox;
    EdMP: TdmkEditCB;
    EdVendedor: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    EdTexto: TdmkEdit;
    RgFiltro: TRadioGroup;
    CkComissoes: TCheckBox;
    GroupBox2: TGroupBox;
    CkFimDataF: TCheckBox;
    CkIniDataF: TCheckBox;
    TPIniDataF: TDateTimePicker;
    TPFimDataF: TDateTimePicker;
    GroupBox3: TGroupBox;
    CkFimEntrega: TCheckBox;
    CkIniEntrega: TCheckBox;
    TPIniEntrega: TDateTimePicker;
    TPFimEntrega: TDateTimePicker;
    Panel7: TPanel;
    DBGOSs: TdmkDBGridZTO;
    PnImprime: TPanel;
    RGAgrupa: TRadioGroup;
    RGOrdem4: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem1: TRadioGroup;
    RGOrdem5: TRadioGroup;
    frxOSS_ABERT_001_01: TfrxReport;
    Panel8: TPanel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label40: TLabel;
    Label5: TLabel;
    EdCor: TdmkEdit;
    EdClasse: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    EdEspessura: TdmkEdit;
    Label3: TLabel;
    EdTransp: TdmkEditCB;
    CBTransp: TdmkDBLookupComboBox;
    DsOSs: TDataSource;
    QrOSsPedidCli: TWideStringField;
    BtPesquisa: TBitBtn;
    BtImpSel: TBitBtn;
    QrSels: TMySQLQuery;
    QrSelsDataPedido: TDateField;
    QrSelsCliente: TIntegerField;
    QrSelsVendedor: TIntegerField;
    QrSelsOS: TIntegerField;
    QrSelsTexto: TWideStringField;
    QrSelsEntrega: TDateField;
    QrSelsFluxo: TIntegerField;
    QrSelsClasse: TWideStringField;
    QrSelsPedido: TIntegerField;
    QrSelsEspesTxt: TWideStringField;
    QrSelsCorTxt: TWideStringField;
    QrSelsM2Pedido: TFloatField;
    QrSelsObserv: TWideMemoField;
    QrSelsNOMECLI: TWideStringField;
    QrSelsNOMEVEN: TWideStringField;
    QrSelsNOMEMP: TWideStringField;
    QrSelsPedidCli: TWideStringField;
    QrOSsNO_CondicaoPG: TWideStringField;
    QrSelsNO_CondicaoPG: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxOSS_ABERT_001_01GetValue(const VarName: string;
      var Value: Variant);
    procedure DBGOSsDblClick(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtImpSelClick(Sender: TObject);
  private
    { Private declarations }
    function OrdemBy1(Item: Integer): String;
    function OrdemBy2(Item: Integer): String;
    procedure Pesquisa();
    procedure MostraRelatorio(Query: TMySQLQuery);
    function SQLBasePesq(): String;
    function SQLOrderBy(): String;
  public
    { Public declarations }
    FRetorna: Boolean;
    FSelOS: Integer;
  end;

  var
  FmOSsAbertas: TFmOSsAbertas;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral;

{$R *.DFM}

procedure TFmOSsAbertas.BtImpSelClick(Sender: TObject);
var
  Corda: String;
begin
  Corda := MyObjects.CordaDeZTOChecks(DBGOSs, QrOSs, 'OS', True);
  //
  if Corda <> EmptyStr then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSels, DMod.MyDB, [
    SQLBasePesq(),
    'AND mi.Controle IN (' + Corda + ')',
    SQLOrderBy]);
    //
    MostraRelatorio(QrSels);
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

procedure TFmOSsAbertas.BtOKClick(Sender: TObject);
begin
  Pesquisa();
  //
  MostraRelatorio(QrOSs);
end;

procedure TFmOSsAbertas.BtPesquisaClick(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmOSsAbertas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSsAbertas.DBGOSsDblClick(Sender: TObject);
begin
  if FRetorna then
  begin
    FSelOS := QrOSsOS.Value;
    Close;
  end;
end;

procedure TFmOSsAbertas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSsAbertas.FormCreate(Sender: TObject);
begin
  FRetorna := False;
  FSelOS := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrVendedores, Dmod.MyDB);
  if FM_MASTER = 'F' then
  begin
    EdVendedor.Text     := IntToStr(VAR_FUNCILOGIN);
    CBVendedor.KeyValue := VAR_FUNCILOGIN;
(*  2013-07-18
    EdVendedor.Enabled := False;
    CBVendedor.Enabled := False;
    FIM 2013-07-18
*)
  end;
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransp, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMP, Dmod.MyDB);
  //
  TPIniDataF.Date := Date-30;
  TPFimDataF.Date := Date;
  //
  TPIniEntrega.Date := Date;
  TPFimEntrega.Date := Date + 180;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa);
end;

procedure TFmOSsAbertas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSsAbertas.frxOSS_ABERT_001_01GetValue(const VarName: string;
  var Value: Variant);
var
  Pre, Pos: String;
begin
  if VarName = 'VARF_PERIODO' then Value :=
    dmkPF.PeriodoImp(TPIniDataF.Date, TPFimDataF.Date, 0, 0, CkIniDataF.Checked,
      CkFimDataF.Checked, False, False, 'Emiss�o:', '') + ' - ' +
    dmkPF.PeriodoImp(TPIniEntrega.Date, TPFimEntrega.Date, 0, 0, CkIniEntrega.Checked,
      CkFimEntrega.Checked, False, False, 'Emiss�o:', '')
  else if VarName = 'VARF_CLIENTE' then
    Value := dmkPF.CampoReportTxt(CBCliente.Text, 'TODOS')
  else if VarName = 'VARF_MP' then
    Value := dmkPF.CampoReportTxt(CBMP.Text, 'TODOS')
  else if VarName = 'VARF_VENDEDOR' then
    Value := dmkPF.CampoReportTxt(CBVendedor.Text, 'TODOS')
  else if VarName = 'VARF_DESCRICAO' then
  begin
    if EdTexto.Text <> '' then
    begin
      if RgFiltro.ItemIndex in ([1,2]) then Pre := '%' else Pre := '';
      if RgFiltro.ItemIndex in ([1,3]) then Pos := '%' else Pos := '';
      Value := Pre+EdTexto.Text+Pos;
    end else Value := ' ';
  end
  else if VarName = 'VARF_NO_EMPRESA' then
    Value := CBEmpresa.Text
  else if VarName ='VARF_DATA' then
    Value := Date
end;

procedure TFmOSsAbertas.MostraRelatorio(Query: TMySQLQuery);
  function Condicao(Agrupa: Boolean; Item: Integer): String;
  begin
    if Agrupa then
      Result := '''' + 'frxDsOSs."' + OrdemBy2(Item) + '"'''
    else Result := '';
  end;
begin
  frxDsOSs.DataSet := Query;
  //
  MyObjects.frxDefineDataSets(frxOSS_ABERT_001_01, [
  DmodG.frxDsDono,
  frxDsOSs
  ]);

  frxOSS_ABERT_001_01.Variables['VARF_COMI' ]   := CkComissoes.Checked;

  frxOSS_ABERT_001_01.Variables['VARF_VISI1']   := RGAgrupa.ItemIndex > 0;
  frxOSS_ABERT_001_01.Variables['VARF_VISI2']   := RGAgrupa.ItemIndex > 1;
  frxOSS_ABERT_001_01.Variables['VARF_VISI3']   := RGAgrupa.ItemIndex > 2;

  frxOSS_ABERT_001_01.Variables['VARF_COND1']   := Condicao((*RGAgrupa.ItemIndex > 0*)True, RGOrdem1.ItemIndex);
  frxOSS_ABERT_001_01.Variables['VARF_COND2']   := Condicao((*RGAgrupa.ItemIndex > 1*)True, RGOrdem2.ItemIndex);
  frxOSS_ABERT_001_01.Variables['VARF_COND3']   := Condicao((*RGAgrupa.ItemIndex > 2*)True, RGOrdem3.ItemIndex);

  frxOSS_ABERT_001_01.Variables['VARF_MEMO1']   := '''' + RGOrdem1.Items[RGOrdem1.ItemIndex] +
    ': ' + '[frxDsOSs."' + OrdemBy2(RGOrdem1.ItemIndex) + '"]''';
  frxOSS_ABERT_001_01.Variables['VARF_MEMO2']   := '''' + RGOrdem2.Items[RGOrdem2.ItemIndex] +
    ': ' + '[frxDsOSs."' + OrdemBy2(RGOrdem2.ItemIndex) + '"]''';
  frxOSS_ABERT_001_01.Variables['VARF_MEMO3']   := '''' + RGOrdem3.Items[RGOrdem3.ItemIndex] +
    ': ' + '[frxDsOSs."' + OrdemBy2(RGOrdem3.ItemIndex) + '"]''';
  //
  MyObjects.frxMostra(frxOSS_ABERT_001_01, 'Lista de OSs abertas');
end;

function TFmOSsAbertas.OrdemBy1(Item: Integer): String;
begin
  case Item of
    0: Result := 'NOMECLI';
    1: Result := 'mi.MP';
    2: Result := 'NOMEVEN';
    3: Result := 'mi.Texto';
    4: Result := 'pp.DataF';
    5: Result := 'mi.Entrega';
    6: Result := 'mi.CorTxt';
    7: Result := 'mi.Classe';
    8: Result := 'mi.EspesTxt';
    else Result := '**EROR OrdemBy **';
  end;
end;

function TFmOSsAbertas.OrdemBy2(Item: Integer): String;
begin
  case Item of
    0: Result := 'NOMECLI';
    1: Result := 'NOMEMP';
    2: Result := 'NOMEVEN';
    3: Result := 'Texto';
    4: Result := 'DataPedido';
    5: Result := 'Entrega';
    6: Result := 'CorTxt';
    7: Result := 'Classe';
    8: Result := 'EspesTxt';
    else Result := '**EROR OrdemBy **';
  end;
end;

procedure TFmOSsAbertas.Pesquisa();
var
  Pre, Pos, Txt, Cor, Classe, Espessura: String;
  Cliente, MP, Vendedor, Transp: Integer;
begin
  // Parei Aqui
  Cliente   := EdCliente.ValueVariant;
  MP        := EdMP.ValueVariant;
  Vendedor  := EdVendedor.ValueVariant;
  Transp    := EdTransp.ValueVariant;
  Txt       := UpperCase(EdTexto.ValueVariant);
  Cor       := UpperCase(EdCor.ValueVariant);
  Classe    := UpperCase(EdClasse.ValueVariant);
  Espessura := UpperCase(EdEspessura.ValueVariant);
  //
  if (Txt <> '') or (Cor <> '') or (Classe <> '') or (Espessura <> '') then
  begin
    if RgFiltro.ItemIndex in ([1,2]) then Pre := '%' else Pre := '';
    if RgFiltro.ItemIndex in ([1,3]) then Pos := '%' else Pos := '';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSs, DMod.MyDB, [
  SQLBasePesq(),
  dmkPF.SQL_Periodo('AND pp.DataF ', TPIniDataF.Date,
      TPFimDataF.Date, CkIniDataF.Checked, CkFimDataF.Checked),
  dmkPF.SQL_Periodo('AND mi.Entrega ', TPIniEntrega.Date,
      TPFimEntrega.Date, CkIniEntrega.Checked, CkFimEntrega.Checked),
  '',
  Geral.ATS_IF(Cliente <> 0, ['AND pp.Cliente = ' + Geral.FF0(Cliente)]),
  Geral.ATS_IF(MP <> 0, ['AND mi.MP = ' + Geral.FF0(MP)]),
  Geral.ATS_IF(Vendedor <> 0, ['AND pp.Vendedor = ' + Geral.FF0(Vendedor)]),
  Geral.ATS_IF(Transp <> 0, ['AND pp.Transp = ' + Geral.FF0(Transp)]),
  Geral.ATS_IF(Txt <> '', ['AND UPPER(mi.Texto) LIKE "' + Pre + Txt + Pos + '"']),
  Geral.ATS_IF(Cor <> '', ['AND UPPER(mi.CorTxt) LIKE "' + Pre + Cor + Pos + '"']),
  Geral.ATS_IF(Classe <> '', ['AND UPPER(mi.Classe) LIKE "' + Pre + Classe + Pos + '"']),
  Geral.ATS_IF(Espessura <> '', ['AND UPPER(mi.EspesTxt) LIKE "' + Pre + Espessura + Pos + '"']),
  //////////////////////////////////////////////////////////////////////////////
  SQLOrderBy]);
  //dmkPF.LeMeuTexto(QrOSs.SQL.Text);
end;

function TFmOSsAbertas.SQLBasePesq(): String;
begin
  Result := Geral.ATS([
  'SELECT pp.DataF DataPedido, pp.Cliente, pp.Vendedor,  ',
  'mi.Controle OS, UPPER(mi.Texto) Texto, mi.Entrega, mi.Fluxo, ',
  'UPPER(mi.Classe) Classe, mi.Pedido, UPPER(mi.EspesTxt) EspesTxt, ',
  'UPPER(mi.CorTxt) CorTxt, mi.M2Pedido, mi.Observ, ',
  'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLI, ',
  'IF(ve.Tipo=0, ve.RazaoSocial, ve.Nome) NOMEVEN, ',
  'ag.Nome NOMEMP, pp.PedidCli, pc.Nome NO_CONDICAOPG ',
  'FROM mpvits mi ',
  'LEFT JOIN mpp pp ON pp.Codigo=mi.Pedido  ',
  'LEFT JOIN artigosgrupos ag ON ag.Codigo=mi.MP  ',
  'LEFT JOIN entidades cl ON cl.Codigo=pp.Cliente  ',
  'LEFT JOIN entidades ve ON ve.Codigo=pp.Vendedor  ',
  'LEFT JOIN pediprzcab pc ON pc.Codigo=pp.CondicaoPg ',
  'WHERE mi.Codigo=0 '
  ]);
end;

function TFmOSsAbertas.SQLOrderBy(): String;
begin
  Result := Geral.ATS(['ORDER BY ' +
  OrdemBy1(RGOrdem1.ItemIndex)+', '+
  OrdemBy1(RGOrdem2.ItemIndex)+', '+
  OrdemBy1(RGOrdem3.ItemIndex)+', '+
  OrdemBy1(RGOrdem4.ItemIndex)+', '+
  OrdemBy1(RGOrdem5.ItemIndex)
  ]);
end;

end.
