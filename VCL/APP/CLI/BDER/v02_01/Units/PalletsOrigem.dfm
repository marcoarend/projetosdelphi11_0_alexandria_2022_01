object FmPalletsOrigem: TFmPalletsOrigem
  Left = 334
  Top = 225
  Caption = 'CAD-BDERM-006 :: Escolha de Lote'
  ClientHeight = 438
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 104
    Width = 784
    Height = 226
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 0
      Width = 784
      Height = 226
      Align = alClient
      DataSource = DsLoc
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Marca'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PecasOut'
          Title.Caption = 'Sa'#237'da'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PecasSal'
          Title.Caption = 'Saldo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLIENTEI'
          Title.Caption = 'Cliente Interno'
          Width = 160
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPROCEDENCIA'
          Title.Caption = 'Proced'#234'ncia'
          Width = 145
          Visible = True
        end>
    end
  end
  object PainelTitulo1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 56
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label14: TLabel
      Left = 8
      Top = 4
      Width = 33
      Height = 13
      Caption = 'Marca:'
    end
    object Label1: TLabel
      Left = 124
      Top = 4
      Width = 24
      Height = 13
      Caption = 'Lote:'
    end
    object Label2: TLabel
      Left = 104
      Top = 24
      Width = 12
      Height = 13
      Caption = 'ou'
    end
    object EdMarca: TdmkEdit
      Left = 8
      Top = 20
      Width = 92
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdMarcaExit
    end
    object EdLote: TdmkEdit
      Left = 124
      Top = 20
      Width = 92
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdLoteExit
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 196
        Height = 32
        Caption = 'Escolha de Lote'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 196
        Height = 32
        Caption = 'Escolha de Lote'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 196
        Height = 32
        Caption = 'Escolha de Lote'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 330
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 374
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtCancela: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Cancela'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtCancelaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECLIENTEI, '
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOMEPROCEDENCIA, wi.Controle,'
      'wi.Data, wi.ClienteI, wi.Procedencia, wi.Marca,'
      'wi.Pecas, wi.Lote, wi.PecasOut, wi.PecasSal'
      'FROM mpin wi'
      'LEFT JOIN entidades ci ON ci.Codigo=wi.ClienteI'
      'LEFT JOIN entidades fo ON fo.Codigo=wi.Procedencia'
      'WHERE Marca=:P0'
      'OR Lote=:P1')
    Left = 164
    Top = 149
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocNOMECLIENTEI: TWideStringField
      FieldName = 'NOMECLIENTEI'
      Size = 100
    end
    object QrLocNOMEPROCEDENCIA: TWideStringField
      FieldName = 'NOMEPROCEDENCIA'
      Size = 100
    end
    object QrLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLocData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrLocClienteI: TIntegerField
      FieldName = 'ClienteI'
      Required = True
    end
    object QrLocProcedencia: TIntegerField
      FieldName = 'Procedencia'
      Required = True
    end
    object QrLocMarca: TWideStringField
      FieldName = 'Marca'
      Required = True
    end
    object QrLocPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrLocPecasOut: TFloatField
      FieldName = 'PecasOut'
      Required = True
    end
    object QrLocPecasSal: TFloatField
      FieldName = 'PecasSal'
      Required = True
    end
    object QrLocLote: TWideStringField
      FieldName = 'Lote'
    end
  end
  object DsLoc: TDataSource
    DataSet = QrLoc
    Left = 192
    Top = 149
  end
  object QrPalet: TMySQLQuery
    SQL.Strings = (
      'SELECT * FROM palet'
      'WHERE OS=:P0')
    Left = 336
    Top = 181
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPaletPalet: TIntegerField
      FieldName = 'Palet'
    end
    object QrPaletData: TDateField
      FieldName = 'Data'
    end
    object QrPaletOS: TIntegerField
      FieldName = 'OS'
    end
    object QrPaletCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPaletProcede: TIntegerField
      FieldName = 'Procede'
    end
    object QrPaletLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrPaletMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrPaletNOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Size = 255
    end
    object QrPaletNOMEPROCEDE: TWideStringField
      FieldName = 'NOMEPROCEDE'
      Size = 255
    end
    object QrPaletPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrPaletSaida1: TFloatField
      FieldName = 'Saida1'
    end
    object QrPaletSaida2: TFloatField
      FieldName = 'Saida2'
    end
    object QrPaletSaldo: TFloatField
      FieldName = 'Saldo'
    end
  end
end
