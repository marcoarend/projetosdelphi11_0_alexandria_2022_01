unit PQImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, ComCtrls, DBCtrls,
  Grids, DBGrids, frxClass, frxDBSet, Variants, dmkGeral, dmkCheckGroup,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnDmkProcFunc, dmkLabel, dmkImage,
  dmkEditDateTimePicker, AppListas, UnDmkEnums, dmkCheckBox, UnInternalConsts,
  UnEmpresas, dmkDBGridZTO, Vcl.Menus, UnGOTOy;

type
  TFmPQImp = class(TForm)
    Panel1: TPanel;
    QrEstq: TmySQLQuery;
    DsEstq: TDataSource;
    PCRelatorio: TPageControl;
    TabSheet1: TTabSheet;
    QrCI: TMySQLQuery;
    DsCI: TDataSource;
    QrPQ: TmySQLQuery;
    DsPQ: TDataSource;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    QrEstqNOMECI: TWideStringField;
    QrEstqNOMEPQ: TWideStringField;
    QrEstqControle: TIntegerField;
    QrEstqPQ: TIntegerField;
    QrEstqCI: TIntegerField;
    QrEstqMinEstq: TFloatField;
    QrEstqLk: TIntegerField;
    QrEstqDataCad: TDateField;
    QrEstqDataAlt: TDateField;
    QrEstqUserCad: TIntegerField;
    QrEstqUserAlt: TIntegerField;
    QrEstqCustoPadrao: TFloatField;
    QrEstqMoedaPadrao: TSmallintField;
    QrEstqEstSegur: TIntegerField;
    QrEstqPeso: TFloatField;
    QrEstqValor: TFloatField;
    QrEstqCusto: TFloatField;
    DsCI1: TDataSource;
    QrSE: TmySQLQuery;
    DsSE: TDataSource;
    QrSECodigo: TIntegerField;
    QrSENome: TWideStringField;
    QrSE1_: TmySQLQuery;
    Panel8: TPanel;
    QrEstqNOMESE: TWideStringField;
    TabSheet3: TTabSheet;
    DsCI2B: TDataSource;
    QrUsoT: TmySQLQuery;
    QrUsoTNOMEORI: TWideStringField;
    QrUsoTDataX: TDateField;
    QrUsoTTipo: TSmallintField;
    QrUsoTCliOrig: TIntegerField;
    QrUsoTCliDest: TIntegerField;
    QrUsoTInsumo: TIntegerField;
    QrUsoTPeso: TFloatField;
    QrUsoTValor: TFloatField;
    QrUsoTOrigemCodi: TIntegerField;
    QrUsoTOrigemCtrl: TIntegerField;
    QrUsoTNOMESE: TWideStringField;
    QrUsoTNOMEPQ: TWideStringField;
    QrPedComprar: TMySQLQuery;
    Qr3Bal: TmySQLQuery;
    Qr3Out: TmySQLQuery;
    Qr3Inn: TmySQLQuery;
    Qr3PQ: TmySQLQuery;
    Qr3PQInsumo: TIntegerField;
    Qr3PQNOMEPQ: TWideStringField;
    Qr3PQINNPESO: TFloatField;
    Qr3PQINNVALOR: TFloatField;
    Qr3PQOUTPESO: TFloatField;
    Qr3PQOUTVALOR: TFloatField;
    Qr3OutInsumo: TIntegerField;
    Qr3OutPeso: TFloatField;
    Qr3OutValor: TFloatField;
    Qr3InnInsumo: TIntegerField;
    Qr3InnPeso: TFloatField;
    Qr3InnValor: TFloatField;
    Qr3BalInsumo: TIntegerField;
    Qr3BalPeso: TFloatField;
    Qr3BalValor: TFloatField;
    QrEstqNOMEFO: TWideStringField;
    QrUsoTNOMEFO: TWideStringField;
    QrSE1_Codigo: TIntegerField;
    Qr4: TmySQLQuery;
    TabSheet5: TTabSheet;
    Qr4NOMECLIDESTINO: TWideStringField;
    Qr4NOMECLIORIGEM: TWideStringField;
    Qr4NOMEPQ: TWideStringField;
    Qr4Peso: TFloatField;
    Qr4Valor: TFloatField;
    Qr4CUSTO_MEDIO: TFloatField;
    Qr4NOMESETOR: TWideStringField;
    Qr4Insumo: TIntegerField;
    Qr3PQNOMEIQ: TWideStringField;
    QrMoviPQ: TmySQLQuery;
    Qr3PQNOMESETOR: TWideStringField;
    QrMoviPQInsumo: TIntegerField;
    QrMoviPQNomePQ: TWideStringField;
    QrMoviPQNomeFO: TWideStringField;
    QrMoviPQNomeCI: TWideStringField;
    QrMoviPQNomeSE: TWideStringField;
    QrMoviPQInnPes: TFloatField;
    QrMoviPQInnVal: TFloatField;
    QrMoviPQOutPes: TFloatField;
    QrMoviPQOutVal: TFloatField;
    Qr4SETOR: TFloatField;
    TabSheet6: TTabSheet;
    frxMoviPQ: TfrxReport;
    frxDsMoviPQ: TfrxDBDataset;
    frxEstoqueEm: TfrxReport;
    QrMoviPQAntPes: TFloatField;
    QrMoviPQAntVal: TFloatField;
    QrMoviPQFimPes: TFloatField;
    QrMoviPQFimVal: TFloatField;
    Qr3PQBALPESO: TFloatField;
    Qr3PQBALVALOR: TFloatField;
    QrMoviPQBalPes: TFloatField;
    QrMoviPQBalVal: TFloatField;
    frxEstq1: TfrxReport;
    frxDsEstq: TfrxDBDataset;
    frxUsoT: TfrxReport;
    frxDsUsoT: TfrxDBDataset;
    frxConSetor: TfrxReport;
    frxDs4: TfrxDBDataset;
    TabSheet7: TTabSheet;
    Panel12: TPanel;
    QrPQx: TmySQLQuery;
    QrPQxNO_CLI_ORIG: TWideStringField;
    QrPQxNO_CLI_DEST: TWideStringField;
    QrPQxNO_PQ: TWideStringField;
    QrPQxDataX: TDateField;
    QrPQxOrigemCodi: TIntegerField;
    QrPQxOrigemCtrl: TIntegerField;
    QrPQxTipo: TIntegerField;
    QrPQxCliOrig: TIntegerField;
    QrPQxCliDest: TIntegerField;
    QrPQxInsumo: TIntegerField;
    QrPQxPeso: TFloatField;
    QrPQxValor: TFloatField;
    QrPQxAlterWeb: TSmallintField;
    QrPQxAtivo: TSmallintField;
    QrPQxRetorno: TSmallintField;
    DsPQx: TDataSource;
    DBGLancamentos: TDBGrid;
    QrOrfaos: TmySQLQuery;
    TabSheet8: TTabSheet;
    DsOrfaos: TDataSource;
    QrOrfaosInsumo: TIntegerField;
    QrOrfaosNO_PQ: TWideStringField;
    QrOrfaosNO_GG1: TWideStringField;
    PnOrfaosNao: TPanel;
    PnOrfaosSim: TPanel;
    DBGrid3: TDBGrid;
    Panel14: TPanel;
    Panel13: TPanel;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    Panel16: TPanel;
    RGFiltro5: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel17: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    Panel18: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    QrSE1_Ativo: TSmallintField;
    QrSE1_Nome: TWideStringField;
    QrSEAtivo: TSmallintField;
    CkNovaForma5: TCheckBox;
    Qr3Bal_: TmySQLQuery;
    IntegerField1: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    QrEstqCodProprio: TWideStringField;
    frxEstoqueEm_: TfrxReport;
    frxQUI_INSUM_002_009_A: TfrxReport;
    QrPQRSum: TmySQLQuery;
    QrPQRIts: TmySQLQuery;
    QrPQRSumInsumo: TIntegerField;
    QrPQRSumNomePQ: TWideStringField;
    QrPQRSumAbertoKg: TFloatField;
    QrPQRSumPrecoMedio: TFloatField;
    QrPQRItsOrigemCodi: TIntegerField;
    QrPQRItsOrigemCtrl: TIntegerField;
    QrPQRItsTipo: TIntegerField;
    QrPQRItsDataX: TDateField;
    QrPQRItsAbertoKg: TFloatField;
    QrPQRItsPRECO: TFloatField;
    QrPQRItsRetQtd: TFloatField;
    QrPQRItsNF_CC: TIntegerField;
    frxDsPQRSum: TfrxDBDataset;
    frxDsPQRIts: TfrxDBDataset;
    QrPQRSumCodProprio: TWideStringField;
    QrPQRSumValAberto: TFloatField;
    QrPQRSumNOMEFO: TWideStringField;
    QrPQRSumNOMECI: TWideStringField;
    QrPQRSumNOMESE: TWideStringField;
    frxQUI_INSUM_002_009_B: TfrxReport;
    Panel7: TPanel;
    Ck009_Individual: TCheckBox;
    QrPQRSumCI: TIntegerField;
    frxEstqNFs: TfrxReport;
    frxDsEstqNFs: TfrxDBDataset;
    QrEstqNFs: TmySQLQuery;
    QrEstqNFsNOMEFO: TWideStringField;
    QrEstqNFsNOMECI: TWideStringField;
    QrEstqNFsNOMEPQ: TWideStringField;
    QrEstqNFsNOMESE: TWideStringField;
    QrEstqNFsCodProprio: TWideStringField;
    QrEstqNFsprod_CFOP: TWideStringField;
    QrEstqNFsDataX: TDateField;
    QrEstqNFsCliOrig: TIntegerField;
    QrEstqNFsInsumo: TIntegerField;
    QrEstqNFsPeso: TFloatField;
    QrEstqNFsValor: TFloatField;
    QrEstqNFsSdoPeso: TFloatField;
    QrEstqNFsSdoValr: TFloatField;
    QrEstqNFsCusto: TFloatField;
    Panel23: TPanel;
    PnSetores: TPanel;
    DBGrid2: TDBGrid;
    Panel5: TPanel;
    Label8: TLabel;
    BtTodosSetores: TBitBtn;
    BtNenhumSetor: TBitBtn;
    Panel2: TPanel;
    Panel9: TPanel;
    LaCI1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdCI1: TdmkEditCB;
    CBCI1: TdmkDBLookupComboBox;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    Panel11: TPanel;
    LaPQ: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    Panel10: TPanel;
    LaCI: TLabel;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    Panel6: TPanel;
    RGAgrupa: TRadioGroup;
    RGOrdem1: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem2: TRadioGroup;
    PB1: TProgressBar;
    PB2: TProgressBar;
    PB3: TProgressBar;
    Panel24: TPanel;
    CkGrade: TCheckBox;
    TabSheet11: TTabSheet;
    QrListaPesagem: TmySQLQuery;
    frxListaPesagem: TfrxReport;
    QrListaPesagemDataEmis: TDateTimeField;
    QrListaPesagemDataEmis_TXT: TWideStringField;
    QrListaPesagemNumero: TIntegerField;
    QrListaPesagemNOME: TWideStringField;
    QrListaPesagemPeso: TFloatField;
    QrListaPesagemQtde: TFloatField;
    QrListaPesagemFulao: TWideStringField;
    QrListaPesagemSetor: TSmallintField;
    frxDsListaPesagem: TfrxDBDataset;
    QrListaPesagemNOMESETOR: TWideStringField;
    QrListaPesagemObs: TWideStringField;
    LaEmpresa: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    CkNovaForma6: TCheckBox;
    frxEstoqueEm3: TfrxReport;
    frxDsMoviPQ3: TfrxDBDataset;
    QrMoviPQ3: TmySQLQuery;
    QrMoviPQ3Insumo: TIntegerField;
    QrMoviPQ3NomePQ: TWideStringField;
    QrMoviPQ3NomeFO: TWideStringField;
    QrMoviPQ3NomeCI: TWideStringField;
    QrMoviPQ3NomeSE: TWideStringField;
    QrMoviPQ3InnPes: TFloatField;
    QrMoviPQ3InnVal: TFloatField;
    QrMoviPQ3OutPes: TFloatField;
    QrMoviPQ3OutVal: TFloatField;
    QrMoviPQ3AntPes: TFloatField;
    QrMoviPQ3AntVal: TFloatField;
    QrMoviPQ3FimPes: TFloatField;
    QrMoviPQ3FimVal: TFloatField;
    QrMoviPQ3BalPes: TFloatField;
    QrMoviPQ3BalVal: TFloatField;
    QrMoviPQ3DvlPes: TFloatField;
    QrMoviPQ3DvlVal: TFloatField;
    QrMoviPQ3AjuPes: TFloatField;
    QrMoviPQ3AjuVal: TFloatField;
    frxEstq2: TfrxReport;
    QrEstqCustoEstoquePadrao: TFloatField;
    CkCustoPadrao5: TCheckBox;
    frxEstoqueEm3CustoPadrao: TfrxReport;
    QrMoviPQ3InnPad: TFloatField;
    QrMoviPQ3OutPad: TFloatField;
    QrMoviPQ3AntPad: TFloatField;
    QrMoviPQ3FimPad: TFloatField;
    QrMoviPQ3BalPad: TFloatField;
    QrMoviPQ3DvlPad: TFloatField;
    frxPedOpen: TfrxReport;
    frxDsPedOpen: TfrxDBDataset;
    QrPedOpen: TMySQLQuery;
    QrPedOpenCodigo: TIntegerField;
    QrPedOpenControle: TIntegerField;
    QrPedOpenInsumo: TIntegerField;
    QrPedOpenPesoEstq: TFloatField;
    QrPedOpenPesoUso: TFloatField;
    QrPedOpenPesoFut: TFloatField;
    QrPedOpenPesoNeed: TFloatField;
    QrPedOpenCliInt: TIntegerField;
    QrPedOpenRepet: TFloatField;
    QrPedOpenNomePQ: TWideStringField;
    QrPedOpenNomeCI: TWideStringField;
    QrPedOpenLk: TIntegerField;
    QrPedOpenDataCad: TDateField;
    QrPedOpenDataAlt: TDateField;
    QrPedOpenUserCad: TIntegerField;
    QrPedOpenUserAlt: TIntegerField;
    QrPedOpenAlterWeb: TSmallintField;
    QrPedOpenAWServerID: TIntegerField;
    QrPedOpenAWStatSinc: TSmallintField;
    QrPedOpenAtivo: TSmallintField;
    QrPedOpenNivel: TIntegerField;
    QrPedOpenKgLiqEmb: TFloatField;
    QrPedOpenPesoCompra: TFloatField;
    QrPedPedEPQ: TMySQLQuery;
    QrPedPedEPQInsumo: TIntegerField;
    QrPedPedEPQPesoUso: TFloatField;
    QrPedPedEPQPedidoCod: TIntegerField;
    QrPedPedEPQPedidoTxt: TWideStringField;
    QrPedPedEPQNomePQ: TWideStringField;
    QrPedPedEPQNomeCI: TWideStringField;
    QrPedPedEPQCliInt: TIntegerField;
    QrPedPedEPQPedidoOrd: TIntegerField;
    frxPedSeq: TfrxReport;
    QrPedSeq: TMySQLQuery;
    QrPedSeqCodigo: TIntegerField;
    QrPedSeqControle: TIntegerField;
    QrPedSeqInsumo: TIntegerField;
    QrPedSeqPesoEstq: TFloatField;
    QrPedSeqPesoUso: TFloatField;
    QrPedSeqPesoFut: TFloatField;
    QrPedSeqPesoNeed: TFloatField;
    QrPedSeqCliInt: TIntegerField;
    QrPedSeqRepet: TFloatField;
    QrPedSeqNomePQ: TWideStringField;
    QrPedSeqNomeCI: TWideStringField;
    QrPedSeqLk: TIntegerField;
    QrPedSeqDataCad: TDateField;
    QrPedSeqDataAlt: TDateField;
    QrPedSeqUserCad: TIntegerField;
    QrPedSeqUserAlt: TIntegerField;
    QrPedSeqAlterWeb: TSmallintField;
    QrPedSeqAWServerID: TIntegerField;
    QrPedSeqAWStatSinc: TSmallintField;
    QrPedSeqAtivo: TSmallintField;
    QrPedSeqNivel: TIntegerField;
    QrPedSeqKgLiqEmb: TFloatField;
    QrPedSeqPesoCompra: TFloatField;
    QrPedSeqPedidoCod: TIntegerField;
    QrPedSeqPedidoTxt: TWideStringField;
    QrPedSeqPedidoOrd: TIntegerField;
    QrPedSeqDtPedido: TDateField;
    frxDsPedSeq: TfrxDBDataset;
    frxDsPQUCab: TfrxDBDataset;
    QrPedSeqInsumM2PercRetr: TFloatField;
    QrPedSeqInsumM2PedKgM2: TFloatField;
    QrPedSeqInsumM2PesoKg: TFloatField;
    QrPedPedEPQInsumM2PesoKg: TFloatField;
    QrPedOpenInsumM2PesoKg: TFloatField;
    QrSumCab: TMySQLQuery;
    QrSumCabCodigo: TIntegerField;
    QrSumCabPedPdArM2: TFloatField;
    QrPedSeqDtIniProd: TDateTimeField;
    QrPedSeqDtEntrega: TDateTimeField;
    QrPedOpenIQ: TIntegerField;
    QrPedOpenNOMEIQ: TWideStringField;
    frxDsPedComprar: TfrxDBDataset;
    QrPedComprarInsumM2PesoKg: TFloatField;
    QrPedComprarCodigo: TIntegerField;
    QrPedComprarControle: TIntegerField;
    QrPedComprarInsumo: TIntegerField;
    QrPedComprarPesoEstq: TFloatField;
    QrPedComprarPesoUso: TFloatField;
    QrPedComprarPesoFut: TFloatField;
    QrPedComprarPesoNeed: TFloatField;
    QrPedComprarCliInt: TIntegerField;
    QrPedComprarRepet: TFloatField;
    QrPedComprarNomePQ: TWideStringField;
    QrPedComprarNomeCI: TWideStringField;
    QrPedComprarLk: TIntegerField;
    QrPedComprarDataCad: TDateField;
    QrPedComprarDataAlt: TDateField;
    QrPedComprarUserCad: TIntegerField;
    QrPedComprarUserAlt: TIntegerField;
    QrPedComprarAlterWeb: TSmallintField;
    QrPedComprarAWServerID: TIntegerField;
    QrPedComprarAWStatSinc: TSmallintField;
    QrPedComprarAtivo: TSmallintField;
    QrPedComprarNivel: TIntegerField;
    QrPedComprarKgLiqEmb: TFloatField;
    QrPedComprarPesoCompra: TFloatField;
    QrPedComprarIQ: TIntegerField;
    QrPedComprarNOMEIQ: TWideStringField;
    QrPedPedEPQIQ: TIntegerField;
    QrPedPedEPQNOMEIQ: TWideStringField;
    Panel33: TPanel;
    Label18: TLabel;
    EdFornece0: TdmkEditCB;
    CBFornece0: TdmkDBLookupComboBox;
    QrFornece: TMySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNomeFO: TWideStringField;
    DsFornece: TDataSource;
    QrEstqKgUsoDd: TFloatField;
    QrEstqDiasEsrq: TWideStringField;
    QrEstqKgEstqSegur: TFloatField;
    frxEstq3: TfrxReport;
    QrEstqDdEstq: TFloatField;
    QrListaPesagemSemiTxtEspReb: TWideStringField;
    QrListaPesagemDtaBaixa: TDateField;
    QrListaPesagemDtCorrApo: TDateTimeField;
    QrListaPesagemEmpresa: TIntegerField;
    QrListaPesagemHoraIni: TTimeField;
    QrListaPesagemGraCorCad: TIntegerField;
    QrListaPesagemNoGraCorCad: TWideStringField;
    QrListaPesagemSemiCodEspReb: TIntegerField;
    QrListaPesagemGrandeza: TSmallintField;
    QrListaPesagemToSumPeso: TFloatField;
    QrListaPesagemToSumAreaM2: TFloatField;
    QrListaPesagemToSumPecas: TFloatField;
    QrListaPesagemCodigo: TIntegerField;
    QrListaPesagemStatus: TSmallintField;
    QrListaPesagemNOMECI: TWideStringField;
    QrListaPesagemTecnico: TWideStringField;
    QrListaPesagemClienteI: TIntegerField;
    QrListaPesagemTipificacao: TSmallintField;
    QrListaPesagemTempoR: TIntegerField;
    QrListaPesagemTempoP: TIntegerField;
    QrListaPesagemTipific: TSmallintField;
    QrListaPesagemEspessura: TWideStringField;
    QrListaPesagemDefPeca: TWideStringField;
    QrListaPesagemCusto: TFloatField;
    QrListaPesagemAreaM2: TFloatField;
    QrListaPesagemSetrEmi: TSmallintField;
    QrListaPesagemSourcMP: TSmallintField;
    QrListaPesagemCod_Espess: TIntegerField;
    QrListaPesagemCodDefPeca: TIntegerField;
    QrListaPesagemCustoTo: TFloatField;
    QrListaPesagemCustoKg: TFloatField;
    QrListaPesagemCustoM2: TFloatField;
    QrListaPesagemCusUSM2: TFloatField;
    QrListaPesagemEmitGru: TIntegerField;
    QrListaPesagemRetrabalho: TSmallintField;
    QrListaPesagemSemiCodPeca: TIntegerField;
    QrListaPesagemSemiTxtPeca: TWideStringField;
    QrListaPesagemSemiPeso: TFloatField;
    QrListaPesagemSemiQtde: TFloatField;
    QrListaPesagemSemiAreaM2: TFloatField;
    QrListaPesagemSemiRendim: TFloatField;
    QrListaPesagemSemiCodEspe: TIntegerField;
    QrListaPesagemSemiTxtEspe: TWideStringField;
    QrListaPesagemBRL_USD: TFloatField;
    QrListaPesagemBRL_EUR: TFloatField;
    QrListaPesagemDtaCambio: TDateField;
    QrListaPesagemVSMovCod: TIntegerField;
    QrListaPesagemDtaBaixa_TXT: TWideStringField;
    QrListaPesagemObsNoTrim: TWideStringField;
    QrEstqNCM: TWideStringField;
    Panel3: TPanel;
    CGTipoCad: TdmkCheckGroup;
    Panel4: TPanel;
    Label1: TLabel;
    BtTodosTipos: TBitBtn;
    BtNenhumTipo: TBitBtn;
    QrEstqDiasProd: TFloatField;
    frxEstq4: TfrxReport;
    QrEstqKgDdProd: TFloatField;
    QrOrfaosEmpresa: TIntegerField;
    QrOrfaosCliOrig: TIntegerField;
    QrOrfaosCodigo: TIntegerField;
    QrOrfaosNivel1: TIntegerField;
    QrOrfaosCI: TIntegerField;
    Timer1: TTimer;
    Panel20: TPanel;
    BtOrfaos: TBitBtn;
    Panel21: TPanel;
    Panel22: TPanel;
    Panel27: TPanel;
    PMOrfaos: TPopupMenu;
    VerificacadastrodeUsoeConsumo1: TMenuItem;
    Substituimovimentosporoutroinsumo1: TMenuItem;
    CriaClienteInterno1: TMenuItem;
    Doitemselecionado1: TMenuItem;
    Detodospossveis1: TMenuItem;
    QrErrPqxDad: TMySQLQuery;
    PC00OrigemInfo: TPageControl;
    Panel15: TPanel;
    RGRelEstq: TRadioGroup;
    Bt0RecalUsoPQDiario: TBitBtn;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    TPRefProdIni: TdmkEditDateTimePicker;
    TPRefProdFim: TdmkEditDateTimePicker;
    TabSheet2: TTabSheet;
    TabSheet4: TTabSheet;
    Panel19: TPanel;
    Label6: TLabel;
    MeObservacoes: TMemo;
    TabSheet12: TTabSheet;
    DBGrid1: TDBGrid;
    QrDifPqxPqw: TMySQLQuery;
    DsDifPqxPqw: TDataSource;
    QrDifPqxPqwInsumo: TIntegerField;
    QrDifPqxPqwSdoPeso: TFloatField;
    QrDifPqxPqwSdoValr: TFloatField;
    QrDifPqxPqwPeso: TFloatField;
    QrDifPqxPqwValor: TFloatField;
    QrDifPqxPqwNO_PQ: TWideStringField;
    QrDifPqxPqwCI: TIntegerField;
    QrDifPqxPqwNO_CI: TWideStringField;
    QrEstqNFsTipo: TIntegerField;
    QrEstqNFsNO_Tipo: TWideStringField;
    frxDsDifPqxPqw: TfrxDBDataset;
    frxDifPqxPqw: TfrxReport;
    DBGrid4: TDBGrid;
    DsEstqNFs: TDataSource;
    QrEstqNFsOrigemCodi: TIntegerField;
    QrEstqNFsNF: TFloatField;
    CGPositivo1: TdmkCheckGroup;
    CGAtivo1: TdmkCheckGroup;
    Excluilanamentossemmovimentosefortexto1: TMenuItem;
    QrOrfaosGGXNiv2: TIntegerField;
    QrOrfaosNO_GGXNiv2: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid2CellClick(Column: TColumn);
    procedure BtTodosSetoresClick(Sender: TObject);
    procedure BtNenhumSetorClick(Sender: TObject);
    procedure frxMoviPQGetValue(const VarName: String; var Value: Variant);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PCRelatorioChange(Sender: TObject);
    procedure DBGrid3DblClick(Sender: TObject);
    procedure QrOrfaosAfterOpen(DataSet: TDataSet);
    procedure QrPQRSumBeforeClose(DataSet: TDataSet);
    procedure QrPQRSumAfterScroll(DataSet: TDataSet);
    procedure frxListaPesagemGetValue(const VarName: string;
      var Value: Variant);
    procedure BtTodosTipCadClick(Sender: TObject);
    procedure BtNenhumTipCadClick(Sender: TObject);
    procedure DBGLancamentosDblClick(Sender: TObject);
    procedure CkNovaForma6Click(Sender: TObject);
    procedure CkNovaForma5Click(Sender: TObject);
    procedure QrMoviPQ3CalcFields(DataSet: TDataSet);
    procedure Bt0RecalUsoPQDiarioClick(Sender: TObject);
    procedure QrListaPesagemCalcFields(DataSet: TDataSet);
    procedure BtNenhumTipoClick(Sender: TObject);
    procedure BtTodosTiposClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure VerificacadastrodeUsoeConsumo1Click(Sender: TObject);
    procedure Substituimovimentosporoutroinsumo1Click(Sender: TObject);
    procedure BtOrfaosClick(Sender: TObject);
    procedure PMOrfaosPopup(Sender: TObject);
    procedure Doitemselecionado1Click(Sender: TObject);
    procedure Detodospossveis1Click(Sender: TObject);
    procedure Excluilanamentossemmovimentosefortexto1Click(Sender: TObject);
  private
    { Private declarations }
    FMoviPQ, FSetores: String;
    FCI, FCD, FPQ, FFO, FSetoresQtd, FDiasProd: Integer;
    FSE: String;
    FSetoresCod, FSetoresTxt: String;
    FDataI, FDataF, FTC: String;
    F009_Individual: Boolean;
    FDI, FDF: TDateTime;
    FEmpresa: Integer;
    //

    procedure ImprimeEstoqueAtual();
    procedure ImprimeUsoTerceiros();
    procedure ImprimeConsumoSetor();
    procedure ImprimeEstoqueEm();
    procedure ImprimeEstoquePorNFCC();
    procedure ImprimeListaPesagem();
    procedure ReopenSetor(Setor: Integer);
    procedure Reopen3PQ(PQ: Integer);
    function ObtemSetores: Boolean;
    function PreparaMoviPQ_EstqEm: Boolean;
    procedure ImprimeLancamentos();
    procedure VerificaOrfaos(Avisa: Boolean);
    procedure AtivarTodosSetores(Ativo: Integer);
    procedure ReopenPQRIts();
    procedure IncluiClienteInternoInsumoAtual();

  public
    { Public declarations }
  end;

  var
  FmPQImp: TFmPQImp;

implementation

{$R *.DFM}

uses UnMyObjects, Module, MyVCLSkin, UMySQLModule, UCreate, PQx, Principal,
  MyDBCheck, PQ, ModuleGeral, CreateBlueDerm, DmkDAC_PF, PQUCab, PQUCaH, PQUFrm,
  UnPQ_PF, UnApp_Jan;

const
  CO_ProducaoDiaria = 1;
  CO_PedidoEmAberto = 2;
  //
  PCRelatorio_PAGEINDEX_00_Estoque  = 0;
  PCRelatorio_PAGEINDEX_01_UsoTerc  = 1;
  PCRelatorio_PAGEINDEX_02_ConsSetr = 2;
  PCRelatorio_PAGEINDEX_03_EstoquEm = 3;
  PCRelatorio_PAGEINDEX_04_Lanctos  = 4;
  PCRelatorio_PAGEINDEX_05_Orfaos   = 5;
  PCRelatorio_PAGEINDEX_06_ErrEstq  = 6;
  PCRelatorio_PAGEINDEX_07_EstqNfCC = 7;
  PCRelatorio_PAGEINDEX_08_LstPesqg = 8;

var
  VAR_LAST_Prev_Prev  : Integer = 0;
  VAR_LAST_Prev_PdOpn : Integer = 0;

procedure TFmPQImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQImp.BtOKClick(Sender: TObject);
const
  Setores: array[0..3] of String = ('NOMECI', 'NOMESE', 'NOMEPQ', 'NOMEFO');
var  
  I, K: Integer;
begin
  FDataI := Geral.FDT(TPIni.Date, 1);
  FDataF := Geral.FDT(TPFim.Date, 1);
  //
  FCI := Geral.IMV(EdCI.Text);
  FCD := Geral.IMV(EdCI1.Text);
  FPQ := Geral.IMV(EdPQ.Text);
  FSE := 'ORDER BY ' +
         Setores[RGOrdem1.ItemIndex] + ', ' +
         Setores[RGOrdem2.ItemIndex] + ', ' +
         Setores[RGOrdem3.ItemIndex];
  FDI := TPIni.Date;
  FDF := TPFim.Date;
  FTC := '';
  F009_Individual := Ck009_Individual.Checked;
  FFO := EdFornece0.ValueVariant;
  //
  for I := 0 to CGTipoCad.Items.Count -1 do
  begin
    K := CGTipoCad.GetIndexOfCheckedStatus(I);
    if K > -1 then
      FTC := FTC + ',' + FormatFloat('0', K);
  end;
  if FTC <> '' then
    FTC := Copy(FTC, 2);
  //
  case PCRelatorio.ActivePageIndex of
    PCRelatorio_PAGEINDEX_00_Estoque : ImprimeEstoqueAtual();
    PCRelatorio_PAGEINDEX_01_UsoTerc : ImprimeUsoTerceiros();
    PCRelatorio_PAGEINDEX_02_ConsSetr: ImprimeConsumoSetor();
    PCRelatorio_PAGEINDEX_03_EstoquEm: ImprimeEstoqueEm();
    PCRelatorio_PAGEINDEX_04_Lanctos : ImprimeLancamentos();
    PCRelatorio_PAGEINDEX_05_Orfaos  : VerificaOrfaos(True);
    PCRelatorio_PAGEINDEX_06_ErrEstq : App_Jan.MostraFormPQImpErr(); //ImprimeErros();
    PCRelatorio_PAGEINDEX_07_EstqNfCC: ImprimeEstoquePorNFCC();
    PCRelatorio_PAGEINDEX_08_LstPesqg: ImprimeListaPesagem();
    //
    else Geral.MB_Erro('Relat�rio n�o definido!');
  end;
end;

procedure TFmPQImp.FormCreate(Sender: TObject);
var
  Data: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  Data := DModG.ObtemAgora();
  PC00OrigemInfo.ActivePageIndex := 0;
  //
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  FSetores := UnCreateBlueDerm.RecriaTempTableNovo(ntrttSetores, DModG.QrUpdPID1, False);
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + FSetores,
    'SELECT Codigo, 1 Ativo ',
    'FROM ' + TMeuDB + '.listasetores ',
    '']);
  ReopenSetor(0);
  //
  MyObjects.PreencheComponente(CGTipoCad, sListaTipoCadPQ, 2);
  CGTipoCad.Value := 55;
  //
  TPIni.Date := Data;
  TPFim.Date := Data;
  PCRelatorio.ActivePageIndex := 0;
  UnDMkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  //
  EdCI.Text := dmkPF.ObtemInfoRegEdit('CI', Caption, '', ktString);
  FCI := Geral.IMV(EdCI.Text);
  if FCI <> 0 then CBCI.KeyValue := FCI;
  TPIni.Date := 0;
  TPFim.Date := Data;
  TPRefProdIni.Date := Trunc(Data) - 29;
  TPRefProdFim.Date := Data - 1;
  //
  PnOrfaosNao.Align := alClient;
  //
  CGPositivo1.Value := 1;
  CGAtivo1.Value := 1;
  //
  Timer1.Enabled := True;
end;

procedure TFmPQImp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmkPF.SalvaInfoRegEdit('CI', Caption, EdCI.Text, ktString);
  dmkPF.SalvaInfoRegEdit('Ini', Caption, TPIni.Date, ktDate);
  dmkPF.SalvaInfoRegEdit('Fim', Caption, TPFim.Date, ktDate);
end;

function TFmPQImp.ObtemSetores: Boolean;
var
  Liga1, Liga2: String;
begin
  FSetoresQtd := 0;
  UnDMkDAC_PF.AbreMySQLQuery0(QrSE1_, DModG.MyPID_DB, [
    'SELECT str.*, lse.Nome  ',
    'FROM ' + FSetores + ' str ',
    'LEFT JOIN ' + TMeuDB + '.listasetores lse ON str.Codigo=lse.Codigo ',
    'WHERE str.Ativo=1 ',
    '']);
  if QrSE1_.RecordCount = 0 then
  begin
    Result := False;
    Geral.MB_Aviso('Defina pelo menos um setor!');
  end else begin
    Liga1 := '';
    Liga2 := '';
    FSetoresCod := '';
    FSetoresTxt := '';
    while not QrSE1_.Eof do
    begin
      if QrSE1_Ativo.Value = 1 then
      begin
        FSetoresQtd := FSetoresQtd + 1;
        FSetoresCod := FSetoresCod + Liga1 + Geral.FF0(QrSE1_Codigo.Value);
        FSetoresTxt := FSetoresTxt + Liga2 + QrSE1_Nome.Value;
        Liga1 := ',';
        Liga2 := ', ';
      end;
      QrSE1_.Next;
    end;
    if QrSE1_.RecordCount = QrSE.RecordCount then FSetoresTxt := 'TODOS';
    Result := True;
  end;
end;

procedure TFmPQImp.ImprimeEstoqueAtual();
var
  SQL_SE, SQL_PQ, SQL_CI, SQL_Ativo, SQL_FO, SQL_Periodo, sDias: String;
  Report: TfrxReport;
begin
  if not ObtemSetores then Exit;
  if MyObjects.FIC(FTC = '', CGTipoCad, 'Informe o(s) tipo(s) de cadastro!') then
    Exit;
  SQL_SE := '';
  SQL_PQ := '';
  SQL_CI := '';
  SQL_FO := '';
  //
(*
  if CkPositivo1.Checked then
    //SQL_SE := 'AND pcl.Peso>0 AND pq_.Setor in (' + FSetoresCod + ') ';
    SQL_SE := 'AND pcl.Peso>0 AND pq_.Setor in (' + FSetoresCod + ') ';
*)
  // ini 2023-10-25
  (*
  case RGPositivo1.ItemIndex of
    //0: SQL_SE := '';
    1: SQL_SE := 'AND pcl.Peso <> 0 ';
    2: SQL_SE := 'AND pcl.Peso > 0 ';
  end;
  *)
  case CGPositivo1.Value of
    0: SQL_SE := '';
    1: SQL_SE := 'AND pcl.Peso > 0 ';
    2: SQL_SE := 'AND pcl.Peso = 0 ';
    3: SQL_SE := 'AND pcl.Peso >= 0 ';
    4: SQL_SE := 'AND pcl.Peso < 0 ';
    5: SQL_SE := 'AND pcl.Peso <> 0 ';
    6: SQL_SE := 'AND pcl.Peso <= 0 ';
    7: SQL_SE := ''; // todos
  end;
  // fim 2023-10-25
  if FPQ <> 0 then
    SQL_PQ := 'AND pcl.PQ=' + Geral.FF0(FPQ);
  if FCI <> 0 then
    SQL_CI := 'AND pcl.CI=' + Geral.FF0(FCI);
  if FFO <> 0 then
    SQL_FO := 'AND pq_.IQ=' + Geral.FF0(FFO);
  //
  // ini 2023-10-25
  (*
  case RGAtivo.ItemIndex of
    0: SQL_Ativo := '';
    1: SQL_Ativo := 'AND pq_.Ativo = 1';
    2: SQL_Ativo := 'AND pq_.Ativo = 0';
  end;
  *)
  case CGAtivo1.ItemIndex of
    0: SQL_Ativo := '';
    1: SQL_Ativo := 'AND pq_.Ativo = 1';
    2: SQL_Ativo := 'AND pq_.Ativo = 0';
    3: SQL_Ativo := '';
  end;
  // fim 2023-10-25
  //
  //case RG00OrigemInfo.ItemIndex of
  case PC00OrigemInfo.ActivePageIndex of
    0:
    begin
      if RGRelEstq.ItemIndex = 3 then
      begin
        if FSetoresQtd <> 1 then
        begin
          Geral.MB_Aviso(
          'Selecione apenas um setor, pois � utilizada a produ��o setorial para c�lculo dos dias de estoque!');
          Exit;
        end;
        SQL_Periodo := dmkPF.SQL_Periodo('WHERE DtaBaixa ',
          TPRefProdIni.Date, TPRefProdFim.Date, True, True);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
        'SELECT COUNT(DISTINCT(DtaBaixa)) Dias  ',
        'FROM emit emi  ',
        SQL_Periodo,
        'AND emi.Setor in (' + FSetoresCod + ') ',
        'AND emi.Empresa=' + Geral.FF0(FEmpresa),
        '']);
        FDiasProd := Dmod.QrAux.Fields[0].AsInteger;
        sDias := Geral.FF0(FDiasProd);
        SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataX ',
          TPRefProdIni.Date, TPRefProdFim.Date, True, True);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrEstq, DModG.MyPID_DB, [
        'DROP TABLE IF EXISTS _estq_pq_dd_esq_1;',
        'CREATE TABLE _estq_pq_dd_esq_1',
        'SELECT gg1.NCM, ',
        'CASE WHEN ind.Tipo=0 THEN ind.RazaoSocial ',
        'ELSE ind.Nome END NOMEFO, ',
        'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ',
        'ELSE cli.Nome END NOMECI, pq_.Nome NOMEPQ, pcl.*, ',
        'lse.Nome NOMESE, pcl.CustoPadrao * pcl.Peso CustoEstoquePadrao, ',
        'pq_.KgUsoDd, pq_.KgEstqSegur, ',
        'IF(pq_.KgUsoDd<=0, "N/D", FORMAT(IF(pq_.KgUsoDd<=0, 0.000, IF(((pcl.Peso - pq_.KgEstqSegur) / pq_.KgUsoDd) > 999, 999, (pcl.Peso - pq_.KgEstqSegur) / pq_.KgUsoDd)), 1)) DiasEstq, ',
        'IF(pq_.KgUsoDd<=0, 0.000, IF(((pcl.Peso - pq_.KgEstqSegur) / pq_.KgUsoDd) > 999, 999,(pcl.Peso - pq_.KgEstqSegur) / pq_.KgUsoDd)) DdEstq ',
        'FROM ' + TMeuDB + '.pqcli pcl ',
        'LEFT JOIN ' + TMeuDB + '.pq pq_ ON pq_.Codigo=pcl.PQ ',
        'LEFT JOIN ' + TMeuDB + '.entidades    cli ON cli.Codigo=pcl.CI ',
        'LEFT JOIN ' + TMeuDB + '.listasetores lse ON lse.Codigo=pq_.Setor ',
        'LEFT JOIN ' + TMeuDB + '.entidades    ind ON ind.Codigo=pq_.IQ ',
        'LEFT JOIN ' + TMeuDB + '.gragru1      gg1 ON gg1.Nivel1=pq_.Codigo ',
        'WHERE pq_.GGXNiv2 IN (' + FTC + ') ',
        'AND pcl.Empresa=' + Geral.FF0(FEmpresa),
        'AND pq_.Setor in (' + FSetoresCod + ') ',
        SQL_SE,
        SQL_PQ,
        SQL_CI,
        SQL_FO,
        SQL_Ativo,
        //
        ';',
        'DROP TABLE IF EXISTS _estq_pq_dd_esq_2;',
        'CREATE TABLE _estq_pq_dd_esq_2',
        'SELECT pqx.Insumo, SUM(Peso) Peso',
        'FROM ' + TMeuDB + '.pqx pqx',
        SQL_Periodo,
        'AND Tipo > 100',
        'GROUP BY pqx.Insumo',
        '; ',
        '',
        'SELECT ep1.*, ABS(IF(ep2.Peso=0, 0.0, ',
        '  IF(ABS(ep1.Peso/ep2.Peso*' + sDias + ') >= 999.9, 999.9, ',
        '    ROUND(ep1.Peso/ep2.Peso*' + sDias + ', 1)))) DiasProd, ',
        'ABS(IF(20=0, 0.0, ROUND(ep2.Peso/' + sDias + ', 3))) KgDdProd ',
        'FROM _estq_pq_dd_esq_1 ep1 ',
        'LEFT JOIN _estq_pq_dd_esq_2 ep2 ON ep1.PQ=ep2.Insumo',
        FSE, // ORDEM
        '']);
        //Geral.MB_Teste(QrEstq.SQL.Text);
      end else
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrEstq, Dmod.MyDB, [
        'SELECT 0.0 DiasProd, 0.0 KgDdProd, gg1.NCM, ',
        'CASE WHEN ind.Tipo=0 THEN ind.RazaoSocial ',
        'ELSE ind.Nome END NOMEFO, ',
        'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ',
        'ELSE cli.Nome END NOMECI, pq_.Nome NOMEPQ, pcl.*, ',
        'lse.Nome NOMESE, pcl.CustoPadrao * pcl.Peso CustoEstoquePadrao, ',
        //'pq_.KgUsoDd, IF(pq_.KgUsoDd<=0, "N/D", FORMAT(IF(pq_.KgUsoDd<=0, 0.000, pcl.Peso / pq_.KgUsoDd), 1)) DiasEstq ',
        'pq_.KgUsoDd, pq_.KgEstqSegur, ',
        'IF(pq_.KgUsoDd<=0, "N/D", FORMAT(IF(pq_.KgUsoDd<=0, 0.000, IF(((pcl.Peso - pq_.KgEstqSegur) / pq_.KgUsoDd) > 999, 999, (pcl.Peso - pq_.KgEstqSegur) / pq_.KgUsoDd)), 1)) DiasEstq, ',
        'IF(pq_.KgUsoDd<=0, 0.000, IF(((pcl.Peso - pq_.KgEstqSegur) / pq_.KgUsoDd) > 999, 999,(pcl.Peso - pq_.KgEstqSegur) / pq_.KgUsoDd)) DdEstq ',
        'FROM pqcli pcl ',
        'LEFT JOIN pq pq_ ON pq_.Codigo=pcl.PQ ',
        'LEFT JOIN entidades    cli ON cli.Codigo=pcl.CI ',
        'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor ',
        'LEFT JOIN entidades    ind ON ind.Codigo=pq_.IQ ',
        'LEFT JOIN gragru1      gg1 ON gg1.Nivel1=pq_.Codigo ',
        //
        //'WHERE pq_.Ativo IN (' + FTC + ') ',
        'WHERE pq_.GGXNiv2 IN (' + FTC + ') ',
        'AND pcl.Empresa=' + Geral.FF0(FEmpresa),
        'AND pq_.Setor in (' + FSetoresCod + ') ',
        SQL_SE,
        SQL_PQ,
        SQL_CI,
        SQL_FO,
        SQL_Ativo,
        //
        FSE, // ORDEM
        '']);
        //
        //Geral.MB_Teste(Self, QrEstq);
        //
      end;
      case RGRelEstq.ItemIndex of
        0: Report := frxEstq1; // Normal
        1: Report := frxEstq2; // custo padr�o
        2: Report := frxEstq3; // Dura��o estoque cadatro
        3: Report := frxEstq4; // Dura��o estoque produ��o
        else Geral.MB_Erro('Relat�rio de estoque n�o definido!');
      end;
      //
      MyObjects.frxDefineDataSets(Report, [
        DModG.frxDsDono,
        frxDsEstq
      ]);
      MyObjects.frxMostra(Report, 'Estoque de Insumos');
    end;
    1:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrEstqNFs, Dmod.MyDB, [
(*
      'SELECT IF(ind.Tipo=0, ind.RazaoSocial, ind.Nome) NOMEFO, ',
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECI,  ',
      'pq_.Nome NOMEPQ, lse.Nome NOMESE, pcl.CodProprio, ',
      'pqe.NF, pqi.prod_CFOP, ',
      'pqx.DataX, pqx.CliOrig, pqx.Insumo, pqx.Peso, pqx.Valor, ',
      'pqx.SdoPeso, pqx.SdoValr, (pqx.Valor / pqx.Peso) Custo ',
      'FROM pqx pqx ',
      'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo ',
      'LEFT JOIN pqeits pqi ',
      '  ON pqi.Codigo=pqx.OrigemCodi ',
      '  AND pqi.Controle=pqx.OrigemCtrl ',
      'LEFT JOIN pqe ON pqe.Codigo=pqi.Codigo ',
      'LEFT JOIN entidades cli ON cli.Codigo=pqx.CliOrig ',
      'LEFT JOIN entidades ind ON ind.Codigo=pq_.IQ ',
      'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor ',
      'LEFT JOIN pqcli pcl ON pcl.PQ=pqx.Insumo AND pcl.CI=pqx.CliOrig ',
*)
      'SELECT IF(ind.Tipo=0, ind.RazaoSocial, ind.Nome) NOMEFO,  ',
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECI,   ',
      'pq_.Nome NOMEPQ, lse.Nome NOMESE, pcl.CodProprio, ',
      'IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0010) +
      ', pqe.NF, pqx.NF) + 0.0 NF, pqi.prod_CFOP, pqx.DataX, pqx.CliOrig,  ',
      'pqx.Insumo, pqx.Peso, pqx.Valor, pqx.SdoPeso,  ',
      'pqx.SdoValr, (pqx.Valor / pqx.Peso) Custo, ',
      'pqx.Tipo, vfi.Nome NO_Tipo, pqx.OrigemCodi  ',
      'FROM pqx pqx  ',
      'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo  ',
      'LEFT JOIN pqeits pqi  ',
      '  ON pqi.Codigo=pqx.OrigemCodi  ',
      '  AND pqi.Controle=pqx.OrigemCtrl  ',
      'LEFT JOIN pqe ON pqe.Codigo=pqi.Codigo  ',
      'LEFT JOIN entidades cli ON cli.Codigo=pqx.CliOrig  ',
      'LEFT JOIN entidades ind ON ind.Codigo=pq_.IQ  ',
      'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor  ',
      'LEFT JOIN pqcli pcl ON pcl.PQ=pqx.Insumo AND pcl.CI=pqx.CliOrig  ',
      'LEFT JOIN varfatid vfi ON vfi.Codigo=pqx.Tipo ',
      'WHERE pqx.SdoPeso>0  ',
      // Deve mostrar todos saldos de lan�amentos com o valor do peso positivo!
      // Noroeste deu entrada a negativo na baixa de outros (pqo) que no BD fica
      // na pr�tica como entrada a positivo (-1 * -1 = 1!!!!)
      '/*AND pqx.Tipo<>0*/ ', // 'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
      //'AND pq_.Ativo IN (' + FTC + ') ',
      'AND pq_.GGXNiv2 IN (' + FTC + ') ',
      'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
      SQL_SE,
      SQL_PQ,
      SQL_CI,
      SQL_FO,
      SQL_Ativo,
      //
      FSE, // ORDEM
      '']);
      //
      //Geral.MB_Teste(QrEstqNFs.SQL.Text);
      //
      try
        QrEstqNFs.DisableControls;
        //
        MyObjects.frxDefineDataSets(frxEstqNFs, [
          DModG.frxDsDono,
          frxDsEstqNFs
        ]);
        MyObjects.frxMostra(frxEstqNFs, 'Estoque de Insumos por NFs');
      finally
        QrEstqNFs.EnableControls;
      end;
    end;
    2:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrDifPqxPqw, DModG.MyPID_DB, [
      'DROP TABLE IF EXISTS _pqx_pqw_compara_pqx;',
      '',
      'CREATE TABLE _pqx_pqw_compara_pqx',
      'SELECT CliOrig, Insumo, SUM(SdoPeso) SdoPeso, ',
      'SUM(SdoValr) SdoValr',
      'FROM ' + TMeuDB + '.pqx',
      'WHERE SdoPeso<>0',
      'GROUP BY CliOrig, Insumo',
      ';',
      'SELECT pqx.Insumo, pqx.SdoPeso, pqx.SdoValr, ',
      'pqc.Peso, pqc.Valor, pq.Nome NO_PQ, pqc.CI,',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CI',
      'FROM _pqx_pqw_compara_pqx pqx',
      'LEFT JOIN ' + TMeuDB + '.pqcli pqc ON ',
      '  pqc.CI=pqx.CliOrig',
      '  AND pqc.PQ=pqx.Insumo',
      'LEFT JOIN ' + TMeuDB + '.pq pq ON pq.Codigo=pqc.PQ',
      'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=pqc.CI',
      'WHERE pqx.SdoPeso<>pqc.Peso',
      ';',
      '']);
      //
      //Geral.MB_Teste(QrEstqNFs.SQL.Text);
      //
      try
        QrDifPqxPqw.DisableControls;
        //
        MyObjects.frxDefineDataSets(frxDifPqxPqw, [
          DModG.frxDsDono,
          frxDsDifPqxPqw
        ]);
      finally
        QrDifPqxPqw.EnableControls;
      end;
      MyObjects.frxMostra(frxDifPqxPqw, 'Diverg�ncias de Estoque de Insumos Cliente x NFs');
    end;
    else Geral.MB_Erro('"Origem das informa��es" n�o implementado!');
  end;
end;

procedure TFmPQImp.ImprimeUsoTerceiros;
const
  Ordem: array[0..3] of String = ('NOMEORI', 'NOMESE', 'NOMEPQ', 'NOMEFO');
begin
  if not ObtemSetores then Exit;
  if FCI = 0 then
  begin
    Geral.MB_Aviso('Defina o cliente interno!');
    Exit;
  end;
  //
  QrUsoT.Close;
  QrUsoT.SQL.Clear;
  QrUsoT.SQL.Add('SELECT CASE WHEN ori.Tipo=0 THEN ori.RazaoSocial');
  QrUsoT.SQL.Add('ELSE ori.Nome END NOMEORI, pqx.*, lse.Nome NOMESE ,');
  QrUsoT.SQL.Add('pq_.Nome NOMEPQ, CASE WHEN iq_.Tipo = 2 THEN');
  QrUsoT.SQL.Add('iq_.RazaoSocial ELSE iq_.Nome END NOMEFO');
  QrUsoT.SQL.Add('FROM pqx pqx');
  QrUsoT.SQL.Add('LEFT JOIN entidades    ori ON ori.Codigo=pqx.CliOrig');
  QrUsoT.SQL.Add('LEFT JOIN pq           pq_ ON pq_.Codigo=pqx.Insumo');
  QrUsoT.SQL.Add('LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor');
  QrUsoT.SQL.Add('LEFT JOIN entidades    iq_ ON iq_.Codigo=pq_.IQ');
  //
  QrUsoT.SQL.Add('WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0110));
  QrUsoT.SQL.Add('AND pqx.CliDest='+FormatFloat('0', FCI));
  QrUsoT.SQL.Add('AND pqx.CliOrig<>'+FormatFloat('0', FCI));
  QrUsoT.SQL.Add('AND pq_.Setor in ('+FSetoresCod+')');
  QrUsoT.SQL.Add('AND pqx.Empresa=' + Geral.FF0(FEmpresa));
  QrUsoT.SQL.Add(dmkPF.SQL_Periodo('AND pqx.DataX ', TPIni.Date, TPFim.Date, True, True));
  //
  if FCD <> 0 then
    QrUsoT.SQL.Add('AND pqx.CliOrig='+FormatFloat('0', FCD));
  //
  if FPQ <> 0 then
    QrUsoT.SQL.Add('AND pqx.Insumo='+FormatFloat('0', FPQ));
  //

  QrUsoT.SQL.Add('ORDER BY ' +
         Ordem[RGOrdem1.ItemIndex] + ', ' +
         Ordem[RGOrdem2.ItemIndex] + ', ' +
         Ordem[RGOrdem3.ItemIndex]);
  UnDMkDAC_PF.AbreQuery(QrUsoT, Dmod.MyDB);
  // ini 2022-02-25
  MyObjects.frxDefineDatasets(frxUsoT, [
    frxDsUsoT
  ]);
  // fim 2022-02-25
  //
  MyObjects.frxMostra(frxUsoT, 'Uso de insumos qu�micos de terceiros');
end;

procedure TFmPQImp.IncluiClienteInternoInsumoAtual();
var
  Nivel1, Insumo, Codigo, Empresa, CliOrig, CI, CtrlPQCli: Integer;
begin
  Nivel1 := QrOrfaosNivel1.Value;
  Empresa := QrOrfaosEmpresa.Value;
  CliOrig := QrOrfaosCliOrig.Value;
  Insumo  := QrOrfaosInsumo.Value;
  CI  := QrOrfaosCI.Value;
  if Nivel1 <> 0 then
    Codigo := Nivel1
  else
    Codigo := Insumo;
  if (Codigo <> 0) and (Empresa <> 0) and (CliOrig <> 0) and (CI = 0) then
  begin
    if UnPQX.PQ_IncluiClienteInterno(stIns, Codigo, Empresa, CliOrig,
    (*MoedaPadrao*)3, (*CustoPadraoAtz*)0, (*MoedaMan*)0,
    (*EstSegur*)10, (*EdtCtrl*)0, (*MinEstq*)0, (*CustoPadrao*)0,
    (*CustoMan*)0, (*KgLiqEmb*)0, (*NO_CI*)EmptyStr,
    (*CodProprio*)EmptyStr, CtrlPQCli) then
    begin
      if CtrlPQCli  <> 0 then
      begin
        UnPQx.AtualizaEstoquePQ(CliOrig, Codigo, Empresa, aeMsg, CO_VAZIO);
        CI := CliOrig;
      end;
    end;
  end;
end;

procedure TFmPQImp.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGrid2, Rect, 1, QrSEAtivo.Value);
end;

procedure TFmPQImp.DBGrid3DblClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
  Continua: Boolean;
  //
  Empresa, CliOrig, Insumo, CI, Controle, CtrlPQCli: Integer;
begin
  if QrOrfaos.RecordCount > 0 then
  begin
    Continua := True;
    Codigo := QrOrfaosNivel1.Value;
    Empresa := QrOrfaosEmpresa.Value;
    CliOrig := QrOrfaosCliOrig.Value;
    Insumo  := QrOrfaosInsumo.Value;
    CI  := QrOrfaosCI.Value;
    if QrOrfaosCodigo.Value > 0 then
    begin
      // nada
    end else if Codigo > 0 then
    begin
      // Cadastra PQ na tabela 'pq'
      Nome := QrOrfaosNO_GG1.Value;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pq', False, [
      'Nome'], ['Codigo'], [Nome], [Codigo], True);
      // verifica se existe o cliente interno...
      VerificaOrfaos(False);
      if (Empresa <> 0) and (CliOrig <> 0) and (Insumo <> 0) then
      begin
        if QrOrfaos.Locate('Empresa;CliOrig;Insumo',
        VarArrayOf([Empresa, CliOrig, Insumo]), []) then
        begin
          CI  := QrOrfaosCI.Value;
          if CI = 0 then
          begin
            CtrlPQCli := 0;
            if UnPQX.PQ_IncluiClienteInterno(stIns, Codigo, Empresa, CliOrig,
            (*MoedaPadrao*)3, (*CustoPadraoAtz*)0, (*MoedaMan*)0,
            (*EstSegur*)10, (*EdtCtrl*)0, (*MinEstq*)0, (*CustoPadrao*)0,
            (*CustoMan*)0, (*KgLiqEmb*)0, (*NO_CI*)EmptyStr,
            (*CodProprio*)EmptyStr, CtrlPQCli) then
            begin
              if CtrlPQCli  <> 0 then
              begin
                UnPQx.AtualizaEstoquePQ(CliOrig, Codigo, Empresa, aeMsg, CO_VAZIO);
                CI := CliOrig;
              end;
            end;
          end;
        end;
      end;
    end else Continua := False;
    if CI = 0 then
    begin
      Geral.MB_Aviso('� necess�rio cadastrar o cliente interno ' +
      Geral.FF0(CliOrig) + ' no insumo ' + Geral.FF0(Insumo) + '.');
      //
      Continua := True;
    end;
    if Continua then
    begin
      if DBCheck.CriaFm(TFmPQ, FmPQ, afmoNegarComAviso) then
      begin
        FmPQ.FCodLoc := Codigo;
        FmPQ.ShowModal;
        FmPQ.Destroy;
        //
      end;
    end else Geral.MB_Aviso('Imposs�vel verificar PQ!');
  end;
  VerificaOrfaos(False);
end;

procedure TFmPQImp.Detodospossveis1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    QrOrfaos.First;
    while not QrOrfaos.Eof do
    begin
      IncluiClienteInternoInsumoAtual();
      QrOrfaos.Next;
    end;
    //
    VerificaOrfaos(True);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPQImp.Doitemselecionado1Click(Sender: TObject);
begin
  IncluiClienteInternoInsumoAtual();
  VerificaOrfaos(True);
end;

procedure TFmPQImp.Excluilanamentossemmovimentosefortexto1Click(
  Sender: TObject);
begin
  if QrOrfaosGGXNiv2.Value = 3 then // Texto
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o dos lan�amentos de estoque sem movimento do insumo ' +
    Geral.FF0(QrOrfaosInsumo.Value) + '?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM pqx WHERE Insumo=' +
        Geral.FF0(QrOrfaosInsumo.Value) + ' AND Peso = 0 AND Valor=0');
        VerificaOrfaos(True);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmPQImp.VerificacadastrodeUsoeConsumo1Click(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
  Continua: Boolean;
  //
  Empresa, CliOrig, Insumo, CI, Controle, CtrlPQCli: Integer;
begin
  if QrOrfaos.RecordCount > 0 then
  begin
    Continua := True;
    Codigo := QrOrfaosNivel1.Value;
    Empresa := QrOrfaosEmpresa.Value;
    CliOrig := QrOrfaosCliOrig.Value;
    Insumo  := QrOrfaosInsumo.Value;
    CI  := QrOrfaosCI.Value;
    if QrOrfaosCodigo.Value > 0 then
    begin
      // nada
    end else if Codigo > 0 then
    begin
      // Cadastra PQ na tabela 'pq'
      Nome := QrOrfaosNO_GG1.Value;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pq', False, [
      'Nome'], ['Codigo'], [Nome], [Codigo], True);
      // verifica se existe o cliente interno...
      VerificaOrfaos(False);
      if (Empresa <> 0) and (CliOrig <> 0) and (Insumo <> 0) then
      begin
        if QrOrfaos.Locate('Empresa;CliOrig;Insumo',
        VarArrayOf([Empresa, CliOrig, Insumo]), []) then
        begin
          CI  := QrOrfaosCI.Value;
          if CI = 0 then
          begin
            CtrlPQCli := 0;
            if UnPQX.PQ_IncluiClienteInterno(stIns, Codigo, Empresa, CliOrig,
            (*MoedaPadrao*)3, (*CustoPadraoAtz*)0, (*MoedaMan*)0,
            (*EstSegur*)10, (*EdtCtrl*)0, (*MinEstq*)0, (*CustoPadrao*)0,
            (*CustoMan*)0, (*KgLiqEmb*)0, (*NO_CI*)EmptyStr,
            (*CodProprio*)EmptyStr, CtrlPQCli) then
            begin
              if CtrlPQCli  <> 0 then
              begin
                UnPQx.AtualizaEstoquePQ(CliOrig, Codigo, Empresa, aeMsg, CO_VAZIO);
                CI := CliOrig;
              end;
            end;
          end;
        end;
      end;
    end else Continua := False;
    if CI = 0 then
    begin
      Geral.MB_Aviso('� necess�rio cadastrar o cliente interno ' +
      Geral.FF0(CliOrig) + ' no insumo ' + Geral.FF0(Insumo) + '.');
      //
      Continua := True;
    end;
  end;
  VerificaOrfaos(False);
end;

procedure TFmPQImp.VerificaOrfaos(Avisa: Boolean);
var
  Mensagem, ATT_GGXNiv2: String;
begin
(*
  UnDMkDAC_PF.AbreMySQLQuery0(QrOrfaos, Dmod.MyDB, [
  'SELECT DISTINCT pqx.Insumo, pq.Codigo, pq.Nome NO_PQ, ',
  'gg1.Nome NO_GG1 ',
  'FROM pqx pqx ',
  'LEFT JOIN pq pq ON pq.Codigo=pqx.Insumo ',
  'LEFT JOIN pqcli pcl ON pcl.PQ=pqx.Insumo ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pqx.Insumo ',
  'WHERE pcl.Controle IS NULL ',
  'AND pq.Codigo > 0 ',
  'AND pq.Ativo in (1,2,4,5) ',
  'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
  '']);
*)
  ATT_GGXNiv2 := dmkPF.ArrayToTexto('pq.GGXNiv2', 'NO_GGXNiv2', pvPos, True,
    sListaTipoCadPQ);
  UnDMkDAC_PF.AbreMySQLQuery0(QrOrfaos, DmodG.MyPID_DB, [
  'DROP TABLE IF EXISTS _pqcli_pqx_err_pqcli_;',
  'CREATE TABLE _pqcli_pqx_err_pqcli_',
  'SELECT DISTINCT Empresa, CI, PQ',
  'FROM ' + TMeuDB + '.pqcli',
  'WHERE PQ > 0',
  ';',
  'DROP TABLE IF EXISTS _pqcli_pqx_err_pqx_;',
  'CREATE TABLE _pqcli_pqx_err_pqx_',
  'SELECT DISTINCT Empresa, CliOrig, Insumo',
  'FROM ' + TMeuDB + '.pqx',
  'WHERE Insumo>0',
  ';',
  'SELECT pqx.Empresa, pqx.CliOrig, pcl.CI,',
  'pq.Codigo, pq.GGXNiv2, gg1.Nivel1, pqx.Insumo, ',
  ATT_GGXNiv2,
  'pq.Nome NO_PQ, gg1.Nome NO_GG1 ',
  'FROM _pqcli_pqx_err_pqx_ pqx',
  'LEFT JOIN _pqcli_pqx_err_pqcli_ pqc',
  '  ON pqx.Empresa=pqc.Empresa',
  '  AND pqx.CliOrig=pqc.CI',
  '  AND pqx.Insumo=pqc.PQ',
  'LEFT JOIN ' + TMeuDB + '.pq pq ON pq.Codigo=pqx.Insumo',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=pqx.Insumo ',
  'LEFT JOIN ' + TMeuDB + '.pqcli pcl ON pcl.PQ=pqx.Insumo',
  '  AND pcl.CI=pqx.CliOrig ',
  'WHERE pqc.PQ IS NULL',
  'OR pqx.Insumo IS NULL',
  'ORDER BY pqx.Empresa, pqx.CliOrig, pqx.Insumo',
  ';',
  '']);
  //Geral.MB_Teste(QrOrfaos.SQL.Text);
  if QrOrfaos.RecordCount > 0 then
  begin
    if Avisa then
    begin
      if QrOrfaos.RecordCount = 1 then
        Mensagem := 'Foi localizado um insumo orf�o!'
      else
        Mensagem := 'Foram localizados ' +
        IntToStr(QrOrfaos.RecordCount) +' insumos orf�os!';
      Geral.MB_Aviso('CUIDADO! ' + Mensagem);
    end;
    //
    PCRelatorio.ActivePageIndex := PCRelatorio_PAGEINDEX_05_Orfaos; // 5
  end;
end;

procedure TFmPQImp.ReopenSetor(Setor: Integer);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrSE, DModG.MyPID_DB, [
  'SELECT str.*, lse.Nome  ',
  'FROM ' + FSetores + ' str ',
  'LEFT JOIN ' + TMeuDB + '.listasetores lse ON str.Codigo=lse.Codigo ',
  //'WHERE str.Ativo=1 ',
  '']);
  QrSE.Locate('Codigo', Setor, []);
end;

procedure TFmPQImp.Substituimovimentosporoutroinsumo1Click(Sender: TObject);
begin
(*
SELECT PQ, Count(PQ) ITENS
FROM pqcli
WHERE PQ=469

SELECT PQ, Count(PQ) ITENS
FROM pqcot
WHERE PQ=469

SELECT Insumo, Count(Insumo) ITENS
FROM pqeits
WHERE Insumo=469

SELECT Produto, Count(Produto) ITENS
FROM formulasits
WHERE Produto=469

SELECT Insumo, Count(Insumo) ITENS
FROM pqx
WHERE Insumo=469

SELECT Insumo, Count(Insumo) ITENS
FROM pqpedits
WHERE Insumo=469

SELECT Insumo, Count(Insumo) ITENS
FROM pqretits
WHERE Insumo=469

SELECT Insumo, Count(Insumo) ITENS
FROM pqrits
WHERE Insumo=469

SELECT Insumo, Count(Insumo) ITENS
FROM pquclc
WHERE Insumo=469

SELECT Insumo, Count(Insumo) ITENS
FROM pquits
WHERE Insumo=469

SELECT Insumo, Count(Insumo) ITENS
FROM pqurec
WHERE Insumo=469

SELECT Insumo, Count(Insumo) ITENS
FROM pqbadx
WHERE Insumo=469

SELECT Insumo, Count(Insumo) ITENS
FROM pqbbxa
WHERE Insumo=469
*)

end;

procedure TFmPQImp.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  //
  VerificaOrfaos(True);
end;

procedure TFmPQImp.DBGLancamentosDblClick(Sender: TObject);
begin
  if (QrPQx.State <> dsInactive) and (QrPQx.RecordCount > 0) then
    UnPQX.GerenciaPesagem(QrPQXOrigemCodi.Value)
  else
    Geral.MB_Aviso('N�o h� dados para localizar pesagem!')
end;

procedure TFmPQImp.DBGrid2CellClick(Column: TColumn);
var
  Setor, Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Setor := QrSECodigo.Value;
    if QrSEAtivo.Value = 0 then
      Ativo := 1
    else
      Ativo := 0;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'UPDATE ' + FSetores,
    'SET Ativo=' + Geral.FF0(Ativo),
    'WHERE Codigo=' + Geral.FF0(Setor),
    '']);
    //
    ReopenSetor(Setor);
  end;
end;

procedure TFmPQImp.BtTodosSetoresClick(Sender: TObject);
begin
  AtivarTodosSetores(1);
end;

procedure TFmPQImp.BtTodosTipCadClick(Sender: TObject);
begin
  CGTipoCad.SetMaxValue;
end;

procedure TFmPQImp.CkNovaForma5Click(Sender: TObject);
begin
  if CkNovaForma5.Checked then
    CkNovaForma6.Checked := False;
end;

procedure TFmPQImp.CkNovaForma6Click(Sender: TObject);
begin
  if CkNovaForma6.Checked then
    CkNovaForma5.Checked := False;
end;

procedure TFmPQImp.BtNenhumSetorClick(Sender: TObject);
begin
  AtivarTodosSetores(0);
end;

procedure TFmPQImp.BtNenhumTipCadClick(Sender: TObject);
begin
  CGTipoCad.Value := 0;
end;

procedure TFmPQImp.Reopen3PQ(PQ: Integer);
begin
{
  Qr3PQ.Close;
  Qr3PQ.SQL.Clear;
  Qr3PQ.SQL.Add('SELECT DISTINCT Insumo, pq_.Nome NOMEPQ,');
  Qr3PQ.SQL.Add('CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial');
  Qr3PQ.SQL.Add('ELSE frn.Nome END NOMEIQ, lse.Nome NOMESETOR');
  Qr3PQ.SQL.Add('FROM pqx pqx');
  Qr3PQ.SQL.Add('LEFT JOIN pq           pq_ ON pq_.Codigo=pqx.Insumo');
  Qr3PQ.SQL.Add('LEFT JOIN entidades    frn ON frn.Codigo=pq_.IQ');
  Qr3PQ.SQL.Add('LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor');
  Qr3PQ.SQL.Add('');
  if PQ <> 0 then
    Qr3PQ.SQL.Add('WHERE pqx.Insumo=' + FormatFloat('0', PQ));
  UnDMkDAC_PF.AbreQuery(Qr3PQ, Dmod.MyDB);
  //
}
  UnDmkDAC_PF.AbreMySQLQuery0(Qr3PQ, Dmod.MyDB, [
  'SELECT DISTINCT Insumo, pq_.Nome NOMEPQ, ',
  'CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial ',
  'ELSE frn.Nome END NOMEIQ, lse.Nome NOMESETOR ',
  'FROM pqx pqx ',
  'LEFT JOIN pq           pq_ ON pq_.Codigo=pqx.Insumo ',
  'LEFT JOIN entidades    frn ON frn.Codigo=pq_.IQ ',
  'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor ',
{  Nao usa?
  'WHERE Tipo>' + Geral.FF0(VAR_FATID_0000) + ' AND Tipo>' + Geral.FF0(VAR_FATID_0100),
  'AND DataX BETWEEN "' + FDataI + '" AND "' + FDataF + '" ',
  'AND CliOrig=' + Geral.FF0(FCI),
}
  'WHERE pqx.Insumo > 0 ', // N�o mostrar [agua, reciclo e PQ zero
  Geral.ATS_if(PQ <> 0, ['AND pqx.Insumo=' + Geral.FF0(PQ)]),
  '']);
  //dmkPF.LeMeuTexto(Qr3PQ.SQL.Text);
end;

procedure TFmPQImp.ReopenPQRIts();
begin
  if not F009_Individual then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRIts, Dmod.MyDB, [
  'SELECT pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Tipo, ',
  'pqx.DataX, (pqx.Peso + pqx.RetQtd) AbertoKg,  ',
  'IF(pqx.Peso = 0, 0, pqx.Valor / pqx.Peso) PRECO, ',
  'IF(pqx.Peso = 0, 0, pqx.Valor / pqx.Peso)  ',
  '* (pqx.Peso + pqx.RetQtd) ValAberto, ',
  'pqx.RetQtd, pqe.NF_CC  ',
  'FROM pqx ',
  'LEFT JOIN pqe pqe ON pqe.Codigo=pqx.OrigemCodi ',
  'WHERE pqx.CliDest=' + Geral.FF0(QrPQRSumCI.Value),
  'AND pqx.Insumo=' + Geral.FF0(QrPQRSumInsumo.Value),
  'AND pqx.RetQtd + pqx.Peso <> 0 ',
  'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
  'AND pqx.Peso > 0  ',
  'ORDER BY pqx.DataX ',
  '']);
end;
procedure TFmPQImp.BtTodosTiposClick(Sender: TObject);
begin
  CGTipoCad.Value := CGTipoCad.MaxValue;
end;

procedure TFmPQImp.BtNenhumTipoClick(Sender: TObject);
begin
  CGTipoCad.Value := 0;
end;

procedure TFmPQImp.Bt0RecalUsoPQDiarioClick(Sender: TObject);
begin
  App_Jan.MostraFormPQUsoDiaAtz();
end;

procedure TFmPQImp.BtOrfaosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOrfaos, BtOrfaos);
end;

procedure TFmPQImp.AtivarTodosSetores(Ativo: Integer);
var
  Setor: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Setor := QrSECodigo.Value;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DmodG.MyPID_DB, [
    'UPDATE ' + FSetores + ' SET Ativo=' + Geral.FF0(Ativo),
    '']);
    //
    ReopenSetor(Setor);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPQImp.QrListaPesagemCalcFields(DataSet: TDataSet);
begin
  QrListaPesagemObsNoTrim.Value := Geral.Substitui(QrListaPesagemObs.Value, sLineBreak, ' ');
end;

procedure TFmPQImp.QrMoviPQ3CalcFields(DataSet: TDataSet);
begin
  QrMoviPQ3AjuPes.Value :=
    QrMoviPQ3FimPes.Value
    -
    (
      QrMoviPQ3AntPes.Value +
      QrMoviPQ3InnPes.Value +
      QrMoviPQ3OutPes.Value +
      QrMoviPQ3DvlPes.Value
    );
  //
  QrMoviPQ3AjuVal.Value :=
    QrMoviPQ3FimVal.Value
    -
    (
      QrMoviPQ3AntVal.Value +
      QrMoviPQ3InnVal.Value +
      QrMoviPQ3OutVal.Value +
      QrMoviPQ3DvlVal.Value
    );
end;

procedure TFmPQImp.QrOrfaosAfterOpen(DataSet: TDataSet);
begin
  PnOrfaosNao.Visible := QrOrfaos.RecordCount = 0;
  PnOrfaosSim.Visible := QrOrfaos.RecordCount > 0;
end;

procedure TFmPQImp.QrPQRSumAfterScroll(DataSet: TDataSet);
begin
  ReopenPQRIts();
end;

procedure TFmPQImp.QrPQRSumBeforeClose(DataSet: TDataSet);
begin
  QrPQRIts.Close;
end;

procedure TFmPQImp.PCRelatorioChange(Sender: TObject);
var
  Visi: Boolean;
begin
  Visi := PCRelatorio.ActivePageIndex <> PCRelatorio_PAGEINDEX_08_LstPesqg; //Lista pesagem
  //
  LaPQ.Visible     := Visi;
  EdPQ.Visible     := Visi;
  CBPQ.Visible     := Visi;
  LaCI.Visible     := Visi;
  EdCI.Visible     := Visi;
  CBCI.Visible     := Visi;
  LaCI1.Visible    := Visi;
  EdCI1.Visible    := Visi;
  CBCI1.Visible    := Visi;
  RGAgrupa.Visible := Visi;
  RGOrdem1.Visible := Visi;
  RGOrdem2.Visible := Visi;
  RGOrdem3.Visible := Visi;
  //

  case PCRelatorio.ActivePageIndex of
    PCRelatorio_PAGEINDEX_00_Estoque: //Estoque atual
      PnSetores.Visible := True;
(*
    ?: // Previs�o de consumo
      ReopenPQUCab(0);
*)
    PCRelatorio_PAGEINDEX_02_ConsSetr: //Consumo setor
      PnSetores.Visible := True;
    PCRelatorio_PAGEINDEX_03_EstoquEm:
    begin
      if TPIni.Date < 2 then
        TPIni.Date := Geral.PrimeiroDiaDoMes(DmodG.ObtemAgora());
    end;
    PCRelatorio_PAGEINDEX_08_LstPesqg: //Lista pesagem
    begin
      TPIni.Date        := Date;
      TPFim.Date        := Date;
      PnSetores.Visible := True;
    end
    else
      PnSetores.Visible := False;
  end;
end;

procedure TFmPQImp.PMOrfaosPopup(Sender: TObject);
begin
  Doitemselecionado1.Enabled := QrOrfaosCI.Value = 0;
end;

procedure TFmPQImp.ImprimeConsumoSetor();
const
  Ordem: array[0..2] of String = ('NOMEORI', 'NOMESE', 'NOMEPQ');
var
  SQL_Periodo, SQL_FCI, SQL_FCD, SQL_FPQ, SQL_Setores: String;
begin
  if not ObtemSetores then Exit;
  //
  SQL_Setores := '';
  SQL_FCI     := '';
  SQL_FCD     := '';
  SQL_FPQ     := '';
  SQL_Periodo :=
    dmkPF.SQL_Periodo('AND pqx.DataX ', TPIni.Date, TPFim.Date, True, True);
  if FCI <> 0 then SQL_FCI := 'AND pqx.CliOrig = '+FormatFloat('0', FCI);
  if FCD <> 0 then SQL_FCD := 'AND pqx.CliDest = '+FormatFloat('0', FCD);
  if FPQ <> 0 then SQL_FPQ := 'AND pqx.Insumo  = '+FormatFloat('0', FPQ);
  if FSetoresCod <> '' then
      SQL_Setores := Geral.ATS([
    'AND (',
    '    IF(pqx.Tipo IN (' + Geral.FF0(VAR_FATID_0000) + ', ' + Geral.FF0(VAR_FATID_0110) + '), emi.Setor, ',
    '    IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0150) + ', pqt.Setor, ',
    '    IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0170) + ', pqd.Setor, ',
    '    IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0180) + ', pqi.Setor, ',
    '    IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0130) + ', pqm.Setor, ',
    '    IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0185) + ', pqn.Setor, ',
    '    IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0190) + ', pqo.Setor, ',
    '       0))))))) IN (' + FSetoresCod + ')',
    ')']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr4, Dmod.MyDB, [
  'SELECT CASE WHEN cld.Tipo=0 THEN cld.RazaoSocial ',
  'ELSE cld.Nome END NOMECLIDESTINO, CASE WHEN pqx.Tipo=' + Geral.FF0(VAR_FATID_0150),
  'THEN 4.0000 ELSE emi.Setor END SETOR, CASE WHEN pqx.Tipo=' + Geral.FF0(VAR_FATID_0150),
  'THEN "ETE"  ELSE lse.Nome END NOMESETOR, ',
  'CASE WHEN clo.Tipo=0 THEN clo.RazaoSocial ',
  'ELSE clo.Nome END NOMECLIORIGEM, ',
  'pq_.Nome NOMEPQ, -SUM(pqx.Peso) Peso, -SUM(pqx.Valor) Valor, ',
  'SUM(pqx.Valor) / SUM(pqx.Peso) CUSTO_MEDIO, pqx.Insumo ',
  'FROM pqx pqx ',
  'LEFT JOIN emit         emi ON emi.Codigo=pqx.OrigemCodi ',
  'LEFT JOIN pq           pq_ ON pq_.Codigo=pqx.Insumo ',
  'LEFT JOIN entidades    cld ON cld.Codigo=pqx.CliDest ',
  'LEFT JOIN entidades    clo ON clo.Codigo=pqx.CliOrig ',
  'LEFT JOIN listasetores lse ON lse.Codigo=emi.Setor ',
  'LEFT JOIN pqd          pqd ON pqd.Codigo=pqx.OrigemCodi  ',
  'LEFT JOIN pqo          pqo ON pqo.Codigo=pqx.OrigemCodi  ',
  'LEFT JOIN pqi          pqi ON pqi.Codigo=pqx.OrigemCodi  ',
  'LEFT JOIN pqm          pqm ON pqm.Codigo=pqx.OrigemCodi  ',
  'LEFT JOIN pqn          pqn ON pqn.Codigo=pqx.OrigemCodi  ',
  'LEFT JOIN pqt          pqt ON pqt.Codigo=pqx.OrigemCodi  ',
  'WHERE pqx.Tipo>' + Geral.FF0(VAR_FATID_0100),
  'AND pqx.Tipo<>' + Geral.FF0(VAR_FATID_0170),
  'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
  SQL_Periodo,
  SQL_FCI,
  SQL_FCD,
  SQL_FPQ,
  SQL_Setores,
  'GROUP BY SETOR, NOMEPQ, NOMECLIDESTINO, NOMECLIORIGEM ',
  'ORDER BY SETOR, NOMEPQ, NOMECLIDESTINO, NOMECLIORIGEM ',
  '']);
  //Geral.MB_Teste(Self, Qr4);
  //
  MyObjects.frxDefineDataSets(frxConSetor, [
    DmodG.frxDsMaster,
    frxDs4
  ]);
  MyObjects.frxMostra(frxConSetor, 'Consumo de insumos por setor');
end;

procedure TFmPQImp.frxListaPesagemGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_NO_EMPRESA') = 0 then
    Value := CBEmpresa.Text
  else
  if AnsiCompareText(VarName, 'VAR_DATA_HORA') = 0 then
    Value := Geral.Maiusculas(Geral.FDT(Now(), 8), False)
  else if AnsiCompareText(VarName, 'VAR_SETORESTXT') = 0 then
    Value := FSetoresTxt
  else if AnsiCompareText(VarName, 'VAR_PERIODO') = 0 then
    Value := dmkPF.PeriodoImp(TPIni.Date, TPFim.Date, 0, 0, True, True, False, False, '', '')
end;

procedure TFmPQImp.frxMoviPQGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_NO_EMPRESA') = 0 then
    Value := CBEmpresa.Text
  else
  if AnsiCompareText(VarName, 'VFR_AGR1NOME') = 0 then
  begin
    case PCRelatorio.ActivePageIndex of
      PCRelatorio_PAGEINDEX_00_Estoque: //0:
      begin
        case PC00OrigemInfo.ActivePageIndex of
          0:
          begin
            case RGOrdem1.ItemIndex of
              0: Value := QrEstqNOMECI.Value;
              1: Value := QrEstqNOMESE.Value;
              2: Value := QrEstqNOMEPQ.Value;
              3: Value := QrEstqNOMEFO.Value;
              else Value := '';
            end;
          end;
          1:
          begin
            case RGOrdem1.ItemIndex of
              0: Value := QrEstqNFsNOMECI.Value;
              1: Value := QrEstqNFsNOMESE.Value;
              2: Value := QrEstqNFsNOMEPQ.Value;
              3: Value := QrEstqNFsNOMEFO.Value;
              else Value := '';
            end;
          end;
        end;
      end;
      PCRelatorio_PAGEINDEX_01_UsoTerc: //2:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrUsoTNOMEORI.Value;
          1: Value := QrUsoTNOMESE.Value;
          2: Value := QrUsoTNOMEPQ.Value;
          3: Value := QrUsoTNOMEFO.Value;
          else Value := '';
        end;
      end;
      PCRelatorio_PAGEINDEX_02_ConsSetr,
      PCRelatorio_PAGEINDEX_03_EstoquEm: //   3,5:
      begin
        if CkNovaForma6.Checked and (PCRelatorio.ActivePageIndex = 5) then
        begin
          case RGOrdem1.ItemIndex of
            0: Value := QrMoviPQNomeCI.Value;
            1: Value := QrMoviPQNomeSE.Value;
            2: Value := QrMoviPQNomePQ.Value;
            3: Value := QrMoviPQNomeFO.Value;
            else Value := '';
          end;
        end else
        begin
          case RGOrdem1.ItemIndex of
            0: Value := QrMoviPQ3NomeCI.Value;
            1: Value := QrMoviPQ3NomeSE.Value;
            2: Value := QrMoviPQ3NomePQ.Value;
            3: Value := QrMoviPQ3NomeFO.Value;
            else Value := '';
          end;
        end;
      end;
      PCRelatorio_PAGEINDEX_07_EstqNfCC: //     9:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrPQRSumNOMECI.Value;
          1: Value := QrPQRSumNOMESE.Value;
          2: Value := QrPQRSumNOMEPQ.Value;
          3: Value := QrPQRSumNOMEFO.Value;
          else Value := '';
        end;
      end;
    end;
  end else if AnsiCompareText(VarName, 'VFR_AGR2NOME') = 0 then
  begin
    case PCRelatorio.ActivePageIndex of
      PCRelatorio_PAGEINDEX_00_Estoque: //0:
      begin
        case PC00OrigemInfo.ActivePageIndex of
          0:
          begin
            case RGOrdem2.ItemIndex of
              0: Value := QrEstqNOMECI.Value;
              1: Value := QrEstqNOMESE.Value;
              2: Value := QrEstqNOMEPQ.Value;
              3: Value := QrEstqNOMEFO.Value;
              else Value := '';
            end;
          end;
          1:
          begin
            case RGOrdem2.ItemIndex of
              0: Value := QrEstqNFsNOMECI.Value;
              1: Value := QrEstqNFsNOMESE.Value;
              2: Value := QrEstqNFsNOMEPQ.Value;
              3: Value := QrEstqNFsNOMEFO.Value;
              else Value := '';
            end;
          end;
        end;
      end;
      PCRelatorio_PAGEINDEX_01_UsoTerc: //2:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrUsoTNOMEORI.Value;
          1: Value := QrUsoTNOMESE.Value;
          2: Value := QrUsoTNOMEPQ.Value;
          3: Value := QrUsoTNOMEFO.Value;
          else Value := '';
        end;
      end;
      PCRelatorio_PAGEINDEX_02_ConsSetr,
      PCRelatorio_PAGEINDEX_03_EstoquEm: //   3,5:
      begin
        if CkNovaForma6.Checked and (PCRelatorio.ActivePageIndex = 5) then
        begin
          case RGOrdem1.ItemIndex of
            0: Value := QrMoviPQ3NomeCI.Value;
            1: Value := QrMoviPQ3NomeSE.Value;
            2: Value := QrMoviPQ3NomePQ.Value;
            3: Value := QrMoviPQ3NomeFO.Value;
            else Value := '';
          end;
        end else
        begin
          case RGOrdem2.ItemIndex of
            0: Value := QrMoviPQNomeCI.Value;
            1: Value := QrMoviPQNomeSE.Value;
            2: Value := QrMoviPQNomePQ.Value;
            3: Value := QrMoviPQNomeFO.Value;
            else Value := '';
          end;
        end;
      end;
      PCRelatorio_PAGEINDEX_07_EstqNfCC: //     9:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrPQRSumNOMECI.Value;
          1: Value := QrPQRSumNOMESE.Value;
          2: Value := QrPQRSumNOMEPQ.Value;
          3: Value := QrPQRSumNOMEFO.Value;
          else Value := '';
        end;
      end;
    end;
  end else if AnsiCompareText(VarName, 'VAR_DATA_HORA') = 0 then
    Value := Geral.Maiusculas(Geral.FDT(Now(), 8), False)
  else if AnsiCompareText(VarName, 'VAR_PQNOME') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBPQ.Text, EdPQ.ValueVariant)
  else if AnsiCompareText(VarName, 'VAR_CINOME') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBCI.Text, EdCI.ValueVariant)
  else if AnsiCompareText(VarName, 'VAR_AMOSTRAS') = 0 then
    Value := ''
  else if AnsiCompareText(VarName, 'VAR_SETORESTXT') = 0 then
    Value := FSetoresTxt
  else if AnsiCompareText(VarName, 'VAR_PERIODO') = 0 then
    Value := dmkPF.PeriodoImp(TPIni.Date, TPFim.Date, 0, 0, True, True,
    False, False, '', '')


  else if AnsiCompareText(VarName, 'VFR_AGR')  = 0 then Value := RGAgrupa.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD1') = 0 then Value := RGOrdem1.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD2') = 0 then Value := RGOrdem2.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD3') = 0 then Value := RGOrdem3.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_GRD') = 0 then
    Value := dmkPF.BoolToInt2(CkGrade.Checked, 15, 0)
  //else if AnsiCompareText(VarName, 'VARF_PECA') = 0 then
    //Value := CBUnidade.Text;

  else if AnsiCompareText(VarName, 'VFR_EMPRESA') = 0 then Value := CBCI.Text
(*
  else if AnsiCompareText(VarName, 'VARF_GRUPO') = 0 then
    Value := QrPQUCabNome.Value
*)
  else if AnsiCompareText(VarName, 'VARF_EDP') = 0 then
    Value := Geral.FF0(FdiasProd)
  else if AnsiCompareText(VarName, 'VARF_PERIODO_PROD') = 0 then
     Value := dmkPF.PeriodoImp1(TPRefProdIni.Date, TPRefProdFim.Date, True, True, '', ' a ', '')
  else if AnsiCompareText(VarName, 'VARF_OBSERVACAO') = 0 then
     Value := MeObservacoes.Text
  else
end;

function TFmPQImp.PreparaMoviPQ_EstqEm: Boolean;
var
  AntPes, AntVal, BalPes, BalVal, FimPes, FimVal: Double;
  PQ: Integer;
  SQL: String;
var
  NomePQ, NomeFO, NomeCI, NomeSE: String;
  Insumo: Integer;
  InnPes, InnVal, OutPes, OutVal: Double;
begin
  PQ := EdPQ.ValueVariant;
  //
  if MyObjects.FIC(FCI = 0, EdCI, 'Defina o cliente interno!') then
    Exit;
  //
  FMoviPQ :=
    UnCreateBlueDerm.RecriaTempTableNovo(ntrttMoviPQ, DmodG.QrUpdPID1, False);
  //
  //
  Screen.Cursor := crHourGlass;
  try
    {
    if Tipo = 1 then
    begin
      Qr3Ant.Close;
      Qr3Ant.Params[0].AsString  := FDataI;
      Qr3Ant.Params[1].AsInteger := FCI;
      UnDMkDAC_PF.AbreQuery(Qr3Ant, Dmod.MyDB);
    end;
    Qr3Bal.Close;
    Qr3Bal.Params[0].AsString  := FDataI;
    Qr3Bal.Params[1].AsString  := FDataF;
    Qr3Bal.Params[2].AsInteger := FCI;
    UnDMkDAC_PF.AbreQuery(Qr3Bal, Dmod.MyDB);
    }

    //
    Qr3Inn.Close;
    Qr3Inn.Params[0].AsString  := FDataI;
    Qr3Inn.Params[1].AsString  := FDataF;
    Qr3Inn.Params[2].AsInteger := FCI;
    Qr3Inn.Params[3].AsInteger := FEmpresa;
    UnDMkDAC_PF.AbreQuery(Qr3Inn, Dmod.MyDB);
    //
    Qr3Out.Close;
    Qr3Out.Params[0].AsString  := FDataI;
    Qr3Out.Params[1].AsString  := FDataF;
    Qr3Out.Params[2].AsInteger := FCI;
    Qr3Out.Params[3].AsInteger := FEmpresa;
    UnDMkDAC_PF.AbreQuery(Qr3Out, Dmod.MyDB);
    //
    Reopen3PQ(PQ);
    //
    {
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM movipq');
    Dmod.QrUpdL.ExecSQL;
    }
    UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DmodG.MyPID_DB, [
    'DELETE FROM ' + FMoviPQ,
    '']);
    //
    PB1.Position := 0;
    PB1.Max := Qr3PQ.RecordCount;
    Qr3PQ.First;
{
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('INSERT INTO movipq SET ');
    Dmod.QrUpdL.SQL.Add('Insumo=:P00, NomePQ=:P01, NomeFO=:P02, NomeCI=:P03, ');
    Dmod.QrUpdL.SQL.Add('InnPes=:P04, InnVal=:P05, OutPes=:P06, OutVal=:P07, ');
    Dmod.QrUpdL.SQL.Add('AntPes=:P08, AntVal=:P09, BalPes=:P10, BalVal=:P11, ');
    Dmod.QrUpdL.SQL.Add('FimPes=:P12, FimVal=:P13, NomeSE=:P14');
}
    while not Qr3PQ.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      //if Qr3PQInsumo.Value = 15 then
        //ShowMessage(Qr3PQNOMEPQ.Value);
      UnPQx.VerificaEstoquePQ(FCI, Qr3PQInsumo.Value, Int(TPIni.Date) -1, AntPes, AntVal);
      UnPQx.VerificaEstoquePQ(FCI, Qr3PQInsumo.Value, Int(TPFim.Date), FimPes, FimVal);
      BalPes := FimPes - (AntPes + Qr3PQINNPESO.Value + Qr3PQOUTPESO.Value);
      BalVal := FimVal - (AntVal + Qr3PQINNVALOR.Value + Qr3PQOUTVALOR.Value);
      //
{
      Dmod.QrUpdL.Params[00].AsInteger := Qr3PQInsumo.Value;
      Dmod.QrUpdL.Params[01].AsString  := Qr3PQNOMEPQ.Value;
      Dmod.QrUpdL.Params[02].AsString  := Qr3PQNOMEIQ.Value;
      Dmod.QrUpdL.Params[03].AsString  := CBCI.Text;
      Dmod.QrUpdL.Params[04].AsFloat   := Qr3PQINNPESO.Value;
      Dmod.QrUpdL.Params[05].AsFloat   := Qr3PQINNVALOR.Value;
      Dmod.QrUpdL.Params[06].AsFloat   := Qr3PQOUTPESO.Value;
      Dmod.QrUpdL.Params[07].AsFloat   := Qr3PQOUTVALOR.Value;
      Dmod.QrUpdL.Params[08].AsFloat   := AntPes;
      Dmod.QrUpdL.Params[09].AsFloat   := AntVal;
      Dmod.QrUpdL.Params[10].AsFloat   := BalPes;
      Dmod.QrUpdL.Params[11].AsFloat   := BalVal;
      Dmod.QrUpdL.Params[12].AsFloat   := FimPes;
      Dmod.QrUpdL.Params[13].AsFloat   := FimVal;
      Dmod.QrUpdL.Params[14].AsString  := Qr3PQNOMESETOR.Value;
      Dmod.QrUpdL.ExecSQL;
      //
}
      Insumo         := Qr3PQInsumo.Value;
      NomePQ         := Qr3PQNOMEPQ.Value;
      NomeFO         := Qr3PQNOMEIQ.Value;
      NomeCI         := CBCI.Text;
      NomeSE         := Qr3PQNOMESETOR.Value;
      //AntPes         := 0;
      //AntVal         := 0;
      InnPes         := Qr3PQINNPESO.Value;
      InnVal         := Qr3PQINNVALOR.Value;
      OutPes         := Qr3PQOUTPESO.Value;
      OutVal         := Qr3PQOUTVALOR.Value;
      BalPes         := Qr3PQBALPESO.Value;
      BalVal         := Qr3PQBALVALOR.Value;
      //FimPes         := 0;
      //FimVal         := 0;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FMoviPQ, False, [
      'Insumo', 'NomePQ', 'NomeFO',
      'NomeCI', 'NomeSE', 'AntPes',
      'AntVal', 'InnPes', 'InnVal',
      'OutPes', 'OutVal', 'BalPes',
      'BalVal', 'FimPes', 'FimVal'], [
      ], [
      Insumo, NomePQ, NomeFO,
      NomeCI, NomeSE, AntPes,
      AntVal, InnPes, InnVal,
      OutPes, OutVal, BalPes,
      BalVal, FimPes, FimVal], [
      ], False);
      //DmkPF.LeMeuTexto(DModG.QrUpdPID1.SQL.Text);
      //
      Qr3PQ.Next;
    end;
    //
    case  RGFiltro5.ItemIndex of
      0: SQL := '';
      1: SQL := 'WHERE FimPes > 0 OR DvlPes <> 0';
      2: SQL := 'WHERE InnPes <> 0 OR OutPes <> 0 OR DvlPes <> 0';
      3: SQL := 'WHERE FimPes > 0 OR InnPes <> 0 OR OutPes <> 0 OR DvlPes <> 0';
      else SQL := '? SQL ?';
    end;
    //
{
    QrMoviPQ.Close;
    QrMoviPQ.SQL.Clear;
    QrMoviPQ.SQL.Add('SELECT * FROM movipq');
    QrMoviPQ.SQL.Add(FSE);
}
    UnDMkDAC_PF.AbreMySQLQuery0(QrMoviPQ, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + FMoviPQ,
    SQL,
    FSE,
    '']);
    //
    PB1.Position := 0;
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;


procedure TFmPQImp.ImprimeEstoqueEm();
var
  Continua: Boolean;
  PQ: Integer;
  DtIni, DtFim: TDateTime;
  NomeCI, _WHERE, ORDERBY: String;
begin
  if CkNovaForma5.Checked then
  begin
    DtIni  := Int(TPIni.Date);
    DtFim  := Int(TPFim.Date);
    PQ     := EdPQ.ValueVariant;
    NomeCI := CBCI.Text;
    case  RGFiltro5.ItemIndex of
      0: _WHERE := '';
      1: _WHERE := 'WHERE FimPes > 0';
      2: _WHERE := 'WHERE InnPes <> 0 OR OutPes <> 0';
      3: _WHERE := 'WHERE FimPes > 0 OR InnPes <> 0 OR OutPes <> 0 OR DvlPes <> 0';
      else _WHERE := '? SQL ?';
    end;
    ORDERBY := FSE;
    Continua :=  Dmod.PreparaMoviPQ_EstqEm_v2014(ntrttMoviPQ, QrMoviPQ, FCI, FPQ,
    DtIni, DtFim, NomeCI, _WHERE, ORDERBY, LaAviso1, LaAviso2, PB1);
  end else
  if CkNovaForma6.Checked then
  begin
    DtIni  := Int(TPIni.Date);
    DtFim  := Int(TPFim.Date);
    PQ     := EdPQ.ValueVariant;
    NomeCI := CBCI.Text;
    case  RGFiltro5.ItemIndex of
      0: _WHERE := '';
      1: _WHERE := 'WHERE FimPes > 0 OR DvlPes <> 0';
      2: _WHERE := 'WHERE InnPes <> 0 OR OutPes <> 0 OR DvlPes <> 0';
      3: _WHERE := 'WHERE FimPes > 0 OR InnPes <> 0 OR OutPes <> 0 OR DvlPes <> 0';
      else _WHERE := '? SQL ?';
    end;
    ORDERBY := FSE;
    Continua :=  Dmod.PreparaMoviPQ_EstqEm_v2018(ntrttMoviPQ3, QrMoviPQ3, FCI, FPQ,
    DtIni, DtFim, NomeCI, _WHERE, ORDERBY, LaAviso1, LaAviso2, PB1);
  end else
    Continua := PreparaMoviPQ_EstqEm();
  if Continua then
  begin
    if CkNovaForma6.Checked then
    begin
      if not CkCustoPadrao5.Checked then
      begin
        MyObjects.frxDefineDataSets(frxEstoqueEm3, [
          DModG.frxDsMaster,
          frxDsMoviPQ3
        ]);
        MyObjects.frxMostra(frxEstoqueEm3, 'Movimento de Insumos Qu�micos em...');
      end else
      begin
        MyObjects.frxDefineDataSets(frxEstoqueEm3CustoPadrao, [
          DModG.frxDsMaster,
          frxDsMoviPQ3
        ]);
        MyObjects.frxMostra(frxEstoqueEm3CustoPadrao, 'Movimento de Insumos Qu�micos em...');
      end;
    end else
    begin
      MyObjects.frxDefineDataSets(frxEstoqueEm, [
        DModG.frxDsMaster,
        frxDsMoviPQ
      ]);
      MyObjects.frxMostra(frxEstoqueEm, 'Movimento de Insumos Qu�micos em...');
    end;
  end;
end;

procedure TFmPQImp.ImprimeEstoquePorNFCC();
begin
  if not ObtemSetores then Exit;
  if MyObjects.FIC(FTC = '', CGTipoCad, 'Informe o(s) tipo(s) de cadastro!') then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRSum, Dmod.MyDB, [
  'SELECT pqx.Insumo, pq.Nome NOMEPQ, ',
  'pcl.CodProprio, pcl.CI, ',
  '(SUM(pqx.Peso) + SUM(pqx.RetQtd)) AbertoKg,',
  'SUM(IF(pqx.Peso = 0, 0, pqx.Valor / pqx.Peso) ',
  '* (pqx.Peso + pqx.RetQtd)) ValAberto,',
  'SUM(IF(pqx.Peso = 0, 0, pqx.Valor / pqx.Peso) ',
  '* (pqx.Peso + pqx.RetQtd)) /  ',
  '(SUM(pqx.Peso) + SUM(pqx.RetQtd)) PrecoMedio,',
  '',
  'CASE WHEN ind.Tipo=0 THEN ind.RazaoSocial',
  'ELSE ind.Nome END NOMEFO,',
  'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial',
  'ELSE cli.Nome END NOMECI, ',
  'lse.Nome NOMESE',
  '',
  'FROM pqcli pcl',
  'LEFT JOIN pqx pqx ON pcl.PQ=pqx.Insumo',
  'LEFT JOIN pq pq ON pq.Codigo=pcl.PQ',
  '',
  'LEFT JOIN entidades    cli ON cli.Codigo=pcl.CI',
  'LEFT JOIN listasetores lse ON lse.Codigo=pq.Setor',
  'LEFT JOIN entidades    ind ON ind.Codigo=pq.IQ',
  '',
  //'WHERE pq.Ativo IN (' + FTC + ') ',
  'WHERE pq.GGXNiv2 IN (' + FTC + ') ',
  'AND pqx.CliDest=' + Geral.FF0(FCI),
  'AND pcl.CI=' + Geral.FF0(FCI),
  'AND pqx.Insumo>0',
  'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
  'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
  'AND pqx.RetQtd + pqx.Peso <> 0',
  'AND pq.Setor in (' + FSetoresCod + ') ',
  Geral.ATS_if( FPQ <> 0, ['AND pcl.PQ=' + Geral.FF0(FPQ)]),
  'GROUP BY pqx.Insumo ',
  FSE,
  '']);
  //
  if Ck009_Individual.Checked then
  begin
    MyObjects.frxDefineDataSets(frxQUI_INSUM_002_009_B, [
      DModG.frxDsDono,
      frxDsPQRSum,
      frxDsPQRIts
    ]);
    MyObjects.frxMostra(frxQUI_INSUM_002_009_B, 'Estoque de Insumos por NF CC');
  end else
  begin
    MyObjects.frxDefineDataSets(frxQUI_INSUM_002_009_A, [
      DModG.frxDsDono,
      frxDsPQRSum,
      frxDsPQRIts
    ]);
    MyObjects.frxMostra(frxQUI_INSUM_002_009_A, 'Estoque de Insumos por NF CC');
  end;
end;

procedure TFmPQImp.ImprimeLancamentos();
begin
  if MyObjects.FIC(FPQ = 0, EdPQ, 'Informe o Insumo Qu�mico') then Exit;
  //
  QrPQx.Close;
  QrPQx.SQL.Clear;
  QrPQx.SQL.Add('SELECT');
  QrPQx.SQL.Add('IF(clo.Tipo=0, clo.RazaoSocial, clo.Nome) NO_CLI_ORIG,');
  QrPQx.SQL.Add('IF(cld.Tipo=0, cld.RazaoSocial, clo.Nome) NO_CLI_DEST,');
  QrPQx.SQL.Add('pq.Nome NO_PQ, pqx.*');
  QrPQx.SQL.Add('FROM pqx pqx');
  QrPQx.SQL.Add('LEFT JOIN pq pq ON pq.Codigo=pqx.Insumo');
  QrPQx.SQL.Add('LEFT JOIN entidades clo ON clo.Codigo=pqx.CliOrig');
  QrPQx.SQL.Add('LEFT JOIN entidades cld ON cld.Codigo=pqx.CliDest');
  QrPQx.SQL.Add('WHERE pqx.Insumo=' + Geral.FF0(FPQ));
  QrPQx.SQL.Add('AND pqx.Empresa=' + Geral.FF0(FEmpresa));
  QrPQx.SQL.Add(dmkPF.SQL_Periodo('AND pqx.DataX ', FDI, FDF, True, True));
  QrPQx.SQL.Add('ORDER BY pqx.DataX, pqx.Tipo');
  if FCI <> 0 then QrPQx.SQL.Add('AND pqx.CliOrig = '+FormatFloat('0', FCI));
  if FCD <> 0 then QrPQx.SQL.Add('AND pqx.CliDest = '+FormatFloat('0', FCD));
  if FPQ <> 0 then QrPQx.SQL.Add('AND pqx.Insumo  = '+FormatFloat('0', FPQ));
  UnDMkDAC_PF.AbreQuery(QrPQx, Dmod.MyDB);
  //Geral.MB_Teste(Self, QrPQx);
  //MyObjects.frxMostra(frxLanc_tos, 'Lan�amentos de Insumo');
end;

procedure TFmPQImp.ImprimeListaPesagem();
var
  SQL_Periodo, SQL_Setor: String;
begin
  if not ObtemSetores then Exit;
  //
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE emi.DtaBaixa ', TPIni.Date, TPFim.Date, True, True);
  SQL_Setor   := 'AND emi.Setor in (' + FSetoresCod + ') ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrListaPesagem, Dmod.MyDB, [
  'SELECT dfp.Grandeza,  ',
  'IF(dfp.Grandeza=1, emi.Peso, 0.000) ToSumPeso, ',
  'IF(dfp.Grandeza=2, emi.AreaM2, 0.00) ToSumAreaM2, ',
  'IF(dfp.Grandeza=3, emi.Qtde, 0.0) ToSumPecas, ',
  'emi.*,  ',
  'DATE_FORMAT(emi.DataEmis, "%d/%m/%y") DataEmis_TXT,  ',
  'DATE_FORMAT(emi.DtaBaixa, "%d/%m/%y") DtaBaixa_TXT  ',
  'FROM emit emi ',
  'LEFT JOIN defpecas dfp ON dfp.Codigo=emi.CodDefPeca ',
  SQL_Periodo,
  SQL_Setor,
  'AND emi.empresa=' + Geral.FF0(FEmpresa),
  'ORDER BY DtaBaixa_TXT, Setor  ',
  '']);
  //Geral.MB_Teste(Self, QrListaPesagem);
  if QrListaPesagem.RecordCount > 0 then
  begin
    MyObjects.frxDefineDataSets(frxListaPesagem, [
      DModG.frxDsDono,
      frxDsListaPesagem
      ]);
    MyObjects.frxMostra(frxListaPesagem, 'Lista pesagem');
  end else
    Geral.MB_Aviso('Nenhum registro foi localizado!');
end;

end.

