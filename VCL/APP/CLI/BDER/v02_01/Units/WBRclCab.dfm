object FmWBRclCab: TFmWBRclCab
  Left = 368
  Top = 194
  Caption = 
    'WET-RECUR-014 :: Reclassifica'#231#227'o de Mat'#233'ria-prima para Semi / Ac' +
    'abado'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 101
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label52: TLabel
        Left = 476
        Top = 16
        Width = 87
        Height = 13
        Caption = 'Data / hora in'#237'cio:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 628
        Top = 16
        Width = 95
        Height = 13
        Caption = 'Data / hora t'#233'rmino:'
        Color = clBtnFace
        ParentColor = False
      end
      object LaPecas: TLabel
        Left = 16
        Top = 56
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object LaAreaM2: TLabel
        Left = 104
        Top = 56
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object LaAreaP2: TLabel
        Left = 192
        Top = 56
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object LaPeso: TLabel
        Left = 280
        Top = 56
        Width = 27
        Height = 13
        Caption = 'Peso:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 932
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDtHrIni: TdmkEditDateTimePicker
        Left = 476
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrIni'
        UpdCampo = 'DtHrIni'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrIni: TdmkEdit
        Left = 584
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtHrIni'
        UpdCampo = 'DtHrIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtHrFim: TdmkEditDateTimePicker
        Left = 628
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrFim'
        UpdCampo = 'DtHrFim'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrFim: TdmkEdit
        Left = 736
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtHrFim'
        UpdCampo = 'DtHrFim'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPecas: TdmkEdit
        Left = 16
        Top = 72
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAreaM2: TdmkEditCalc
        Left = 104
        Top = 72
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdAreaP2: TdmkEditCalc
        Left = 192
        Top = 72
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaP2'
        UpdCampo = 'AreaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
      object EdPesoKg: TdmkEdit
        Left = 280
        Top = 72
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoKg'
        UpdCampo = 'PesoKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 932
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 337
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 502
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 419
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 358
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 105
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label10: TLabel
        Left = 476
        Top = 16
        Width = 87
        Height = 13
        Caption = 'Data / hora in'#237'cio:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 628
        Top = 16
        Width = 95
        Height = 13
        Caption = 'Data / hora t'#233'rmino:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 932
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 16
        Top = 60
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label17: TLabel
        Left = 104
        Top = 60
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
      end
      object Label18: TLabel
        Left = 192
        Top = 60
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
      end
      object Label19: TLabel
        Left = 280
        Top = 60
        Width = 27
        Height = 13
        Caption = 'Peso:'
      end
      object Label9: TLabel
        Left = 368
        Top = 60
        Width = 50
        Height = 13
        Caption = 'Valor total:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsWBRclCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsWBRclCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 341
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsWBRclCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 476
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtHrIni'
        DataSource = DsWBRclCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 628
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtHrFim'
        DataSource = DsWBRclCab
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 932
        Top = 32
        Width = 64
        Height = 21
        DataField = 'MovimCod'
        DataSource = DsWBRclCab
        TabOrder = 5
      end
      object DBEdit11: TDBEdit
        Left = 16
        Top = 76
        Width = 84
        Height = 21
        DataField = 'Pecas'
        DataSource = DsWBRclCab
        TabOrder = 6
      end
      object DBEdit12: TDBEdit
        Left = 280
        Top = 76
        Width = 84
        Height = 21
        DataField = 'PesoKg'
        DataSource = DsWBRclCab
        TabOrder = 7
      end
      object DBEdit13: TDBEdit
        Left = 104
        Top = 76
        Width = 84
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsWBRclCab
        TabOrder = 8
      end
      object DBEdit14: TDBEdit
        Left = 192
        Top = 76
        Width = 84
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsWBRclCab
        TabOrder = 9
      end
      object DBEdit15: TDBEdit
        Left = 368
        Top = 76
        Width = 84
        Height = 21
        DataField = 'ValorT'
        DataSource = DsWBRclCab
        TabOrder = 10
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 501
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 224
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtItsClick
        end
        object BtReclasif: TBitBtn
          Tag = 224
          Left = 252
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Reclasse'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtReclasifClick
        end
      end
    end
    object GBIts: TGroupBox
      Left = 0
      Top = 105
      Width = 1008
      Height = 172
      Align = alTop
      Caption = ' Itens da entrada: '
      TabOrder = 2
      object DGDados: TDBGrid
        Left = 2
        Top = 15
        Width = 1004
        Height = 155
        Align = alClient
        DataSource = DsWBRclIts
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Pallet'
            Title.Caption = 'ID Pallet'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PALLET'
            Title.Caption = 'Pallet'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaP2'
            Title.Caption = #193'rea ft'#178
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso kg'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorT'
            Title.Caption = 'Valor total'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID item'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PC_RECLAS'
            Title.Caption = 'P'#231' reclas.'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtPeca'
            Title.Caption = 'Sdo virtual p'#231
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Observ'
            Title.Caption = 'Observa'#231#245'es'
            Width = 300
            Visible = True
          end>
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 424
      Width = 1008
      Height = 77
      Align = alBottom
      Caption = 'Itens reclassificados do item de entrada selecionado: '
      TabOrder = 3
      object DBGrid1: TDBGrid
        Left = 2
        Top = 15
        Width = 1004
        Height = 60
        Align = alClient
        DataSource = DsReclasif
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Pallet'
            Title.Caption = 'ID Pallet'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Pallet'
            Title.Caption = 'Pallet'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
            Width = 384
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaP2'
            Title.Caption = #193'rea ft'#178
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso kg'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorT'
            Title.Caption = 'Valor total'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID Pallet'
            Width = 72
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 664
        Height = 32
        Caption = 'Reclassifica'#231#227'o de Mat'#233'ria-prima para Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 664
        Height = 32
        Caption = 'Reclassifica'#231#227'o de Mat'#233'ria-prima para Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 664
        Height = 32
        Caption = 'Reclassifica'#231#227'o de Mat'#233'ria-prima para Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 600
    Top = 44
  end
  object QrWBRclCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrWBRclCabBeforeOpen
    AfterOpen = QrWBRclCabAfterOpen
    BeforeClose = QrWBRclCabBeforeClose
    AfterScroll = QrWBRclCabAfterScroll
    SQL.Strings = (
      'SELECT wic.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA'
      'FROM wbinncab wic'
      'LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor'
      'LEFT JOIN entidades trn ON trn.Codigo=wic.transporta')
    Left = 684
    Top = 1
    object QrWBRclCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWBRclCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrWBRclCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrWBRclCabDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
    object QrWBRclCabDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
    object QrWBRclCabPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBRclCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBRclCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBRclCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBRclCabValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBRclCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWBRclCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWBRclCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWBRclCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWBRclCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWBRclCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWBRclCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWBRclCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
  end
  object DsWBRclCab: TDataSource
    DataSet = QrWBRclCab
    Left = 688
    Top = 45
  end
  object QrWBRclIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrWBRclItsBeforeClose
    AfterScroll = QrWBRclItsAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM wbmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 768
    Top = 1
    object QrWBRclItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWBRclItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrWBRclItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrWBRclItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrWBRclItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrWBRclItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrWBRclItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBRclItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBRclItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBRclItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBRclItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWBRclItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWBRclItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWBRclItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWBRclItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWBRclItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWBRclItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWBRclItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrWBRclItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrWBRclItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrWBRclItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrWBRclItsPC_RECLAS: TFloatField
      FieldKind = fkLookup
      FieldName = 'PC_RECLAS'
      LookupDataSet = QrSumWII
      LookupKeyFields = 'SrcNivel2'
      LookupResultField = 'Pecas'
      KeyFields = 'Controle'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
      Lookup = True
    end
    object QrWBRclItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrWBRclItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrWBRclItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBRclItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBRclItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrWBRclItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBRclItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
  end
  object DsWBRclIts: TDataSource
    DataSet = QrWBRclIts
    Left = 772
    Top = 45
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 656
    Top = 580
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 536
    Top = 584
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrReclasif: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM wbmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 28
    Top = 349
    object QrReclasifCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrReclasifControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrReclasifMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrReclasifEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrReclasifMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrReclasifGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrReclasifPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrReclasifPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrReclasifAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrReclasifAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrReclasifLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrReclasifDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrReclasifDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrReclasifUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrReclasifUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrReclasifAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrReclasifAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrReclasifNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrReclasifPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrReclasifNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrReclasifValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrReclasifSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrReclasifSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrReclasifSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrReclasifMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
  end
  object DsReclasif: TDataSource
    DataSet = QrReclasif
    Left = 28
    Top = 397
  end
  object QrSumWII: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM wbmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 92
    Top = 349
    object QrSumWIIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrSumWIISrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
  end
  object PMImprime: TPopupMenu
    Left = 12
    Top = 16
    object Classificao1: TMenuItem
      Caption = '&Resumo entrada / Classifica'#231#227'o'
      OnClick = Classificao1Click
    end
    object Fichas1: TMenuItem
      Caption = '&Fichas de Pallets'
      object Entradascomestoquevirtual1: TMenuItem
        Caption = '&Entradas com estoque virtual'
        OnClick = Entradascomestoquevirtual1Click
      end
      object Palletsdosclassificados1: TMenuItem
        Caption = '&Classificados nesta entrada'
        OnClick = Palletsdosclassificados1Click
      end
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Estoque1: TMenuItem
      Caption = '&Estoque (mostrar'#225' outra janela)'
      OnClick = Estoque1Click
    end
  end
  object QrResulClas: TmySQLQuery
    Database = Dmod.MyDB
    Left = 180
    Top = 352
    object QrResulClasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrResulClasControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrResulClasMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrResulClasEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrResulClasMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrResulClasGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrResulClasPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrResulClasPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrResulClasAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrResulClasAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrResulClasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrResulClasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrResulClasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrResulClasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrResulClasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrResulClasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrResulClasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrResulClasNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrResulClasPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrResulClasNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrResulClasSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrResulClasGGX_REFER: TWideStringField
      FieldName = 'GGX_REFER'
      Size = 25
    end
    object QrResulClasNO_ORI_PALLET: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_ORI_PALLET'
      LookupDataSet = QrWBRclIts
      LookupKeyFields = 'Controle'
      LookupResultField = 'NO_PALLET'
      KeyFields = 'SrcNivel2'
      Size = 60
      Lookup = True
    end
    object QrResulClasPALLET_M2: TFloatField
      FieldKind = fkLookup
      FieldName = 'PALLET_M2'
      LookupDataSet = QrWBRclIts
      LookupKeyFields = 'Controle'
      LookupResultField = 'AreaM2'
      KeyFields = 'SrcNivel2'
      Lookup = True
    end
    object QrResulClasPERCENT_CLASS: TFloatField
      FieldKind = fkLookup
      FieldName = 'PERCENT_CLASS'
      LookupDataSet = QrResulTota
      LookupKeyFields = 'GraGruX'
      LookupResultField = 'PERCENT_CLASS'
      KeyFields = 'GraGruX'
      Lookup = True
    end
  end
  object frxWET_RECUR_001_01: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412040000000
    ReportOptions.LastChange = 41608.425381412040000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      
        'procedure DBCross1ColumnTotal0OnBeforePrint(Sender: TfrxComponen' +
        't);'
      'begin'
      
        '  DBCross1ColumnTotal0.Memo.Text := '#39'teste'#39';                    ' +
        '                         '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_RECUR_001_01GetValue
    Left = 180
    Top = 308
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsResulClas
        DataSetName = 'frxDsResulClas'
      end
      item
        DataSet = frxDsResulTota
        DataSetName = 'frxDsResulTota'
      end
      item
        DataSet = frxDsWBRclCab
        DataSetName = 'frxDsWBRclCab'
      end
      item
        DataSet = frxDsWBRclIts
        DataSetName = 'frxDsWBRcllts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 120.944950240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle Entrada de Mat'#233'ria-Prima para Semi / Acabado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 529.134199999999900000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 113.385900000000000000
          Top = 105.826840000000000000
          Width = 207.874003540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Top = 105.826840000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsWBRclCab."NO_EMPRESA"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 419.527830000000000000
          Top = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo2: TfrxMemoView
          Top = 64.252010000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo entrada:')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 86.929190000000000000
          Top = 64.252010000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWBRclCab."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Top = 83.149660000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 86.929190000000000000
          Top = 83.149660000000000000
          Width = 593.386210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWBRclCab."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 188.976500000000000000
          Top = 64.252010000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data entrada:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 260.787570000000000000
          Top = 64.252010000000000000
          Width = 173.858380000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWBRclCab."DtEntrada"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 370.393940000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 476.220780000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 321.260050000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 37.795300000000000000
          Top = 105.826840000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 525.354670000000100000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 623.622450000000000000
          Top = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 574.488560000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        DataSet = frxDsWBRclIts
        DataSetName = 'frxDsWBRcllts'
        RowCount = 0
        object Memo55: TfrxMemoView
          Left = 113.385900000000000000
          Width = 207.874003540000000000
          Height = 15.118110240000000000
          DataSet = frxDsWBRclIts
          DataSetName = 'frxDsWBRcllts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWBRclIts."GraGruX"] - [frxDsWBRclIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Width = 37.795275590551180000
          Height = 15.118110240000000000
          DataField = 'Pallet'
          DataSet = frxDsWBRclIts
          DataSetName = 'frxDsWBRcllts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsWBRclIts."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 419.527830000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsWBRclIts
          DataSetName = 'frxDsWBRcllts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsWBRclIts."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 370.393940000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDsWBRclIts
          DataSetName = 'frxDsWBRcllts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsWBRclIts."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 476.220780000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsWBRclIts
          DataSetName = 'frxDsWBRcllts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsWBRclIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 321.260050000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataSet = frxDsWBRclIts
          DataSetName = 'frxDsWBRcllts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsWBRclIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 37.795300000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'NO_PALLET'
          DataSet = frxDsWBRclIts
          DataSetName = 'frxDsWBRcllts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWBRclIts."NO_PALLET"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 574.488560000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataSet = frxDsWBRclIts
          DataSetName = 'frxDsWBRcllts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsWBRclIts."SdoVrtArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 623.622450000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsWBRclIts
          DataSetName = 'frxDsWBRcllts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWBRclIts."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 525.354670000000100000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataSet = frxDsWBRclIts
          DataSetName = 'frxDsWBRcllts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsWBRclIts."SdoVrtPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 150.220404090000000000
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object DBCross1: TfrxDBCrossView
          Width = 411.000000000000000000
          Height = 128.000000000000000000
          DownThenAcross = False
          MaxWidth = 1800
          RowLevels = 2
          OnPrintCell = 'DBCross1OnPrintCell'
          OnPrintColumnHeader = 'DBCross1OnPrintColumnHeader'
          CellFields.Strings = (
            'Pecas')
          ColumnFields.Strings = (
            'GGX_REFER')
          DataSet = frxDsResulClas
          DataSetName = 'frxDsResulClas'
          RowFields.Strings = (
            'NO_ORI_PALLET'
            'PALLET_M2')
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D3822207374616E64616C6F6E653D226E6F223F3E3C63726F73733E3C6365
            6C6C6D656D6F733E3C546672784D656D6F56696577204C6566743D2232333122
            20546F703D223337302C3134313933222057696474683D223936222048656967
            68743D22323222205265737472696374696F6E733D22323422204F6E4265666F
            72655072696E743D22444243726F73733143656C6C304F6E4265666F72655072
            696E742220416C6C6F7745787072657373696F6E733D2246616C73652220466F
            6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F
            6E742E4865696768743D222D31332220466F6E742E4E616D653D22417269616C
            2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D223136
            37373732313522204672616D652E5479703D223135222046696C6C2E4261636B
            436F6C6F723D2231353739303332302220476170583D22332220476170593D22
            33222048416C69676E3D22686152696768742220506172656E74466F6E743D22
            46616C7365222056416C69676E3D22766143656E7465722220546578743D2230
            222F3E3C546672784D656D6F56696577204C6566743D223235342220546F703D
            223636222057696474683D22393622204865696768743D223232222052657374
            72696374696F6E733D2232342220416C6C6F7745787072657373696F6E733D22
            46616C73652220446973706C6179466F726D61742E466F726D61745374723D22
            25322E326E2220446973706C6179466F726D61742E4B696E643D22666B4E756D
            657269632220466F6E742E436861727365743D22312220466F6E742E436F6C6F
            723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D
            653D22417269616C2220466F6E742E5374796C653D223022204672616D652E43
            6F6C6F723D22313637373732313522204672616D652E5479703D223135222046
            696C6C2E4261636B436F6C6F723D2231353739303332302220476170583D2233
            2220476170593D2233222048416C69676E3D2268615269676874222050617265
            6E74466F6E743D2246616C7365222056416C69676E3D22766143656E74657222
            20546578743D22302C3030222F3E3C546672784D656D6F56696577204C656674
            3D223233312220546F703D223339322C3134313933222057696474683D223936
            22204865696768743D22323222205265737472696374696F6E733D2232342220
            416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E4368
            61727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865
            696768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E
            742E5374796C653D223022204672616D652E436F6C6F723D2231363737373231
            3522204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F72
            3D2231353739303332302220476170583D22332220476170593D223322204841
            6C69676E3D22686152696768742220506172656E74466F6E743D2246616C7365
            222056416C69676E3D22766143656E7465722220546578743D2230222F3E3C54
            6672784D656D6F56696577204C6566743D223332372220546F703D223337302C
            3134313933222057696474683D22363422204865696768743D22323222205265
            737472696374696F6E733D2232342220416C6C6F7745787072657373696F6E73
            3D2246616C73652220466F6E742E436861727365743D22312220466F6E742E43
            6F6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E
            4E616D653D22417269616C2220466F6E742E5374796C653D223022204672616D
            652E436F6C6F723D22313637373732313522204672616D652E5479703D223135
            222046696C6C2E4261636B436F6C6F723D223135373930333230222047617058
            3D22332220476170593D2233222048416C69676E3D2268615269676874222050
            6172656E74466F6E743D2246616C7365222056416C69676E3D22766143656E74
            65722220546578743D2230222F3E3C546672784D656D6F56696577204C656674
            3D223235342220546F703D223838222057696474683D22393622204865696768
            743D22323222205265737472696374696F6E733D2232342220416C6C6F774578
            7072657373696F6E733D2246616C73652220466F6E742E436861727365743D22
            312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D
            31332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C65
            3D223022204672616D652E436F6C6F723D22313637373732313522204672616D
            652E5479703D223135222046696C6C2E4261636B436F6C6F723D223135373930
            3332302220476170583D22332220476170593D2233222048416C69676E3D2268
            6152696768742220506172656E74466F6E743D2246616C7365222056416C6967
            6E3D22766143656E7465722220546578743D2230222F3E3C546672784D656D6F
            56696577204C6566743D223332372220546F703D223339322C31343139332220
            57696474683D22363422204865696768743D2232322220526573747269637469
            6F6E733D2232342220416C6C6F7745787072657373696F6E733D2246616C7365
            2220446973706C6179466F726D61742E466F726D61745374723D2225322E326E
            2220446973706C6179466F726D61742E4B696E643D22666B4E756D6572696322
            20466F6E742E436861727365743D22312220466F6E742E436F6C6F723D223022
            20466F6E742E4865696768743D222D31332220466F6E742E4E616D653D224172
            69616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D
            22313637373732313522204672616D652E5479703D223135222046696C6C2E42
            61636B436F6C6F723D2231353739303332302220476170583D22332220476170
            593D2233222048416C69676E3D22686152696768742220506172656E74466F6E
            743D2246616C7365222056416C69676E3D22766143656E746572222054657874
            3D2230222F3E3C2F63656C6C6D656D6F733E3C63656C6C6865616465726D656D
            6F733E3C546672784D656D6F56696577204C6566743D223231312220546F703D
            223434222057696474683D22343322204865696768743D223232222052657374
            72696374696F6E733D22382220416C6C6F7745787072657373696F6E733D2246
            616C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F
            723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D
            653D22417269616C2220466F6E742E5374796C653D223022204672616D652E43
            6F6C6F723D22313637373732313522204672616D652E5479703D223135222046
            696C6C2E4261636B436F6C6F723D2231343231313238382220476170583D2233
            2220476170593D22332220506172656E74466F6E743D2246616C736522205641
            6C69676E3D22766143656E7465722220546578743D225065636173222F3E3C54
            6672784D656D6F56696577204C6566743D223231312220546F703D2236362220
            57696474683D22343322204865696768743D2232322220526573747269637469
            6F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C736522
            20466F6E742E436861727365743D22312220466F6E742E436F6C6F723D223022
            20466F6E742E4865696768743D222D31332220466F6E742E4E616D653D224172
            69616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D
            22313637373732313522204672616D652E5479703D223135222046696C6C2E42
            61636B436F6C6F723D2231343231313238382220476170583D22332220476170
            593D22332220506172656E74466F6E743D2246616C7365222056416C69676E3D
            22766143656E7465722220546578743D225065636173222F3E3C546672784D65
            6D6F56696577204C6566743D223132322220546F703D22383822205769647468
            3D22353322204865696768743D22323222205265737472696374696F6E733D22
            382220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E74
            2E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E74
            2E4865696768743D222D31332220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D2231343231313238382220476170583D22332220476170593D223322
            20506172656E74466F6E743D2246616C7365222056416C69676E3D2276614365
            6E7465722220546578743D225065636173222F3E3C2F63656C6C686561646572
            6D656D6F733E3C636F6C756D6E6D656D6F733E3C546672784D656D6F56696577
            204C6566743D223233312220546F703D223334382C3134313933222057696474
            683D22393622204865696768743D22323222205265737472696374696F6E733D
            2232342220416C6C6F7745787072657373696F6E733D2246616C73652220466F
            6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F
            6E742E4865696768743D222D31332220466F6E742E4E616D653D22417269616C
            2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D223136
            37373732313522204672616D652E5479703D223135222046696C6C2E4261636B
            436F6C6F723D2231343231313238382220476170583D22332220476170593D22
            33222048416C69676E3D22686143656E7465722220506172656E74466F6E743D
            2246616C7365222056416C69676E3D22766143656E7465722220546578743D22
            222F3E3C2F636F6C756D6E6D656D6F733E3C636F6C756D6E746F74616C6D656D
            6F733E3C546672784D656D6F56696577204C6566743D223332372220546F703D
            223334382C3134313933222057696474683D22363422204865696768743D2232
            3222205265737472696374696F6E733D223822204F6E4265666F72655072696E
            743D22444243726F737331436F6C756D6E546F74616C304F6E4265666F726550
            72696E742220416C6C6F7745787072657373696F6E733D2246616C7365222046
            6F6E742E436861727365743D22312220466F6E742E436F6C6F723D2230222046
            6F6E742E4865696768743D222D31332220466F6E742E4E616D653D2241726961
            6C2220466F6E742E5374796C653D223122204672616D652E436F6C6F723D2231
            3637373732313522204672616D652E5479703D223135222046696C6C2E426163
            6B436F6C6F723D2231343231313238382220476170583D22332220476170593D
            2233222048416C69676E3D22686143656E7465722220506172656E74466F6E74
            3D2246616C7365222056416C69676E3D22766143656E7465722220546578743D
            225375622D746F74616C222F3E3C2F636F6C756D6E746F74616C6D656D6F733E
            3C636F726E65726D656D6F733E3C546672784D656D6F56696577204C6566743D
            2232302220546F703D223332362C3134313933222057696474683D2232313122
            204865696768743D22323222205265737472696374696F6E733D22382220416C
            6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43686172
            7365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48656967
            68743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E
            5374796C653D223022204672616D652E436F6C6F723D22313637373732313522
            204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22
            31343231313238382220476170583D22332220476170593D2233222048416C69
            676E3D22686143656E7465722220506172656E74466F6E743D2246616C736522
            2056416C69676E3D22766143656E7465722220546578743D225175616E746964
            616465222F3E3C546672784D656D6F56696577204C6566743D22323331222054
            6F703D223332362C3134313933222057696474683D2231363022204865696768
            743D22323222205265737472696374696F6E733D22382220416C6C6F77457870
            72657373696F6E733D2246616C73652220466F6E742E436861727365743D2231
            2220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31
            332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D
            223022204672616D652E436F6C6F723D22313637373732313522204672616D65
            2E5479703D223135222046696C6C2E4261636B436F6C6F723D22313432313132
            38382220476170583D22332220476170593D2233222048416C69676E3D226861
            43656E7465722220506172656E74466F6E743D2246616C7365222056416C6967
            6E3D22766143656E7465722220546578743D22434C4153534946494341C387C3
            834F222F3E3C546672784D656D6F56696577204C6566743D223231312220546F
            703D223232222057696474683D22343322204865696768743D22323222205265
            737472696374696F6E733D2238222056697369626C653D2246616C7365222041
            6C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E436861
            727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569
            6768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E74
            2E5374796C653D223022204672616D652E436F6C6F723D223136373737323135
            22204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D
            2231343231313238382220476170583D22332220476170593D2233222048416C
            69676E3D22686143656E7465722220506172656E74466F6E743D2246616C7365
            222056416C69676E3D22766143656E7465722220546578743D22222F3E3C5466
            72784D656D6F56696577204C6566743D2232302220546F703D223334382C3134
            313933222057696474683D2231323222204865696768743D2232322220526573
            7472696374696F6E733D22382220416C6C6F7745787072657373696F6E733D22
            46616C73652220466F6E742E436861727365743D22312220466F6E742E436F6C
            6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E61
            6D653D22417269616C2220466F6E742E5374796C653D223022204672616D652E
            436F6C6F723D22313637373732313522204672616D652E5479703D2231352220
            46696C6C2E4261636B436F6C6F723D2231343231313238382220476170583D22
            332220476170593D2233222048416C69676E3D22686143656E74657222205061
            72656E74466F6E743D2246616C7365222056416C69676E3D22766143656E7465
            722220546578743D2250414C4C4554222F3E3C546672784D656D6F5669657720
            4C6566743D223134322220546F703D223334382C313431393322205769647468
            3D22383922204865696768743D22323222205265737472696374696F6E733D22
            382220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E74
            2E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E74
            2E4865696768743D222D31332220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D2231343231313238382220476170583D22332220476170593D223322
            2048416C69676E3D22686143656E7465722220506172656E74466F6E743D2246
            616C7365222056416C69676E3D22766143656E7465722220546578743D22C381
            726561206DC2B2222F3E3C2F636F726E65726D656D6F733E3C726F776D656D6F
            733E3C546672784D656D6F56696577204C6566743D2232302220546F703D2233
            37302C3134313933222057696474683D2231323222204865696768743D223232
            22205265737472696374696F6E733D2232342220416C6C6F7745787072657373
            696F6E733D2246616C73652220466F6E742E436861727365743D22312220466F
            6E742E436F6C6F723D22302220466F6E742E4865696768743D222D3133222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22302220
            4672616D652E436F6C6F723D22313637373732313522204672616D652E547970
            3D223135222046696C6C2E4261636B436F6C6F723D2231343231313238382220
            476170583D22332220476170593D2233222048416C69676E3D22686143656E74
            65722220506172656E74466F6E743D2246616C7365222056416C69676E3D2276
            6143656E7465722220546578743D22222F3E3C546672784D656D6F5669657720
            4C6566743D223134322220546F703D223337302C313431393322205769647468
            3D22383922204865696768743D22323222205265737472696374696F6E733D22
            32342220416C6C6F7745787072657373696F6E733D2246616C73652220446973
            706C6179466F726D61742E466F726D61745374723D2225322E326E2220446973
            706C6179466F726D61742E4B696E643D22666B4E756D657269632220466F6E74
            2E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E74
            2E4865696768743D222D31332220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D2231343231313238382220476170583D22332220476170593D223322
            2048416C69676E3D22686143656E7465722220506172656E74466F6E743D2246
            616C7365222056416C69676E3D22766143656E7465722220546578743D22222F
            3E3C2F726F776D656D6F733E3C726F77746F74616C6D656D6F733E3C54667278
            4D656D6F56696577204C6566743D2232302220546F703D223339322C31343139
            33222057696474683D2232313122204865696768743D22323222205265737472
            696374696F6E733D22382220416C6C6F7745787072657373696F6E733D224661
            6C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F72
            3D22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D65
            3D22417269616C2220466F6E742E5374796C653D223122204672616D652E436F
            6C6F723D22313637373732313522204672616D652E5479703D22313522204669
            6C6C2E4261636B436F6C6F723D2231343231313238382220476170583D223322
            20476170593D2233222048416C69676E3D22686143656E746572222050617265
            6E74466F6E743D2246616C7365222056416C69676E3D22766143656E74657222
            20546578743D22544F54414C222F3E3C546672784D656D6F56696577204C6566
            743D2237342220546F703D223636222057696474683D22313131222048656967
            68743D22323222205265737472696374696F6E733D2238222056697369626C65
            3D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C7365
            2220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D2230
            2220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D2241
            7269616C2220466F6E742E5374796C653D223122204672616D652E436F6C6F72
            3D22313637373732313522204672616D652E5479703D223135222046696C6C2E
            4261636B436F6C6F723D2231343231313238382220476170583D223322204761
            70593D2233222048416C69676E3D22686143656E7465722220506172656E7446
            6F6E743D2246616C7365222056416C69676E3D22766143656E74657222205465
            78743D22546F74616C222F3E3C2F726F77746F74616C6D656D6F733E3C63656C
            6C66756E6374696F6E733E3C6974656D20312F3E3C2F63656C6C66756E637469
            6F6E733E3C636F6C756D6E736F72743E3C6974656D20322F3E3C2F636F6C756D
            6E736F72743E3C726F77736F72743E3C6974656D20302F3E3C6974656D20302F
            3E3C2F726F77736F72743E3C2F63726F73733E}
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 653.858690000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 112.425104090000000000
        Top = 480.000310000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object DBCross2: TfrxDBCrossView
          Top = 6.425104089999991000
          Width = 286.000000000000000000
          Height = 106.000000000000000000
          DownThenAcross = False
          MaxWidth = 1800
          RowLevels = 2
          ShowColumnTotal = False
          ShowRowTotal = False
          OnPrintCell = 'DBCross1OnPrintCell'
          OnPrintColumnHeader = 'DBCross1OnPrintColumnHeader'
          CellFields.Strings = (
            'PERCENT_CLASS')
          ColumnFields.Strings = (
            'GGX_REFER')
          DataSet = frxDsResulTota
          DataSetName = 'frxDsResulTota'
          RowFields.Strings = (
            'NO_Pallet'
            'CalcM2')
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D3822207374616E64616C6F6E653D226E6F223F3E3C63726F73733E3C6365
            6C6C6D656D6F733E3C546672784D656D6F56696577204C6566743D2231353322
            20546F703D223535302C3432353431343039222057696474683D223131332220
            4865696768743D22323222205265737472696374696F6E733D22323422204F6E
            4265666F72655072696E743D22444243726F73733143656C6C304F6E4265666F
            72655072696E742220416C6C6F7745787072657373696F6E733D2246616C7365
            2220446973706C6179466F726D61742E466F726D61745374723D2225322E326E
            2220446973706C6179466F726D61742E4B696E643D22666B4E756D6572696322
            20466F6E742E436861727365743D22312220466F6E742E436F6C6F723D223022
            20466F6E742E4865696768743D222D31332220466F6E742E4E616D653D224172
            69616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D
            22313637373732313522204672616D652E5479703D223135222046696C6C2E42
            61636B436F6C6F723D2231353739303332302220476170583D22332220476170
            593D2233222048416C69676E3D22686152696768742220506172656E74466F6E
            743D2246616C7365222056416C69676E3D22766143656E746572222054657874
            3D22302C3030222F3E3C546672784D656D6F56696577204C6566743D22313935
            2220546F703D223636222057696474683D2231313322204865696768743D2232
            3222205265737472696374696F6E733D2232342220416C6C6F77457870726573
            73696F6E733D2246616C73652220446973706C6179466F726D61742E466F726D
            61745374723D2225322E326E2220446973706C6179466F726D61742E4B696E64
            3D22666B4E756D657269632220466F6E742E436861727365743D22312220466F
            6E742E436F6C6F723D22302220466F6E742E4865696768743D222D3133222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22302220
            4672616D652E436F6C6F723D22313637373732313522204672616D652E547970
            3D223135222046696C6C2E4261636B436F6C6F723D2231353739303332302220
            476170583D22332220476170593D2233222048416C69676E3D22686152696768
            742220506172656E74466F6E743D2246616C7365222056416C69676E3D227661
            43656E7465722220546578743D22302C3030222F3E3C546672784D656D6F5669
            6577204C6566743D223137302220546F703D223434222057696474683D223634
            22204865696768743D22323222205265737472696374696F6E733D2232342220
            416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E4368
            61727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865
            696768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E
            742E5374796C653D223022204672616D652E436F6C6F723D2231363737373231
            3522204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F72
            3D2231353739303332302220476170583D22332220476170593D223322204841
            6C69676E3D22686152696768742220506172656E74466F6E743D2246616C7365
            222056416C69676E3D22766143656E7465722220546578743D2230222F3E3C54
            6672784D656D6F56696577204C6566743D223137302220546F703D2236362220
            57696474683D22363422204865696768743D2232322220526573747269637469
            6F6E733D2232342220416C6C6F7745787072657373696F6E733D2246616C7365
            2220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D2230
            2220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D2241
            7269616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F72
            3D22313637373732313522204672616D652E5479703D223135222046696C6C2E
            4261636B436F6C6F723D2231353739303332302220476170583D223322204761
            70593D2233222048416C69676E3D22686152696768742220506172656E74466F
            6E743D2246616C7365222056416C69676E3D22766143656E7465722220546578
            743D2230222F3E3C546672784D656D6F56696577204C6566743D223235342220
            546F703D223838222057696474683D22393622204865696768743D2232322220
            5265737472696374696F6E733D2232342220416C6C6F7745787072657373696F
            6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E74
            2E436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E
            742E4E616D653D22417269616C2220466F6E742E5374796C653D223022204672
            616D652E436F6C6F723D22313637373732313522204672616D652E5479703D22
            3135222046696C6C2E4261636B436F6C6F723D22313537393033323022204761
            70583D22332220476170593D2233222048416C69676E3D226861526967687422
            20506172656E74466F6E743D2246616C7365222056416C69676E3D2276614365
            6E7465722220546578743D2230222F3E3C546672784D656D6F56696577204C65
            66743D223330372220546F703D223636222057696474683D2236342220486569
            6768743D22323222205265737472696374696F6E733D2232342220416C6C6F77
            45787072657373696F6E733D2246616C73652220446973706C6179466F726D61
            742E466F726D61745374723D2225322E326E2220446973706C6179466F726D61
            742E4B696E643D22666B4E756D657269632220466F6E742E436861727365743D
            22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D22
            2D31332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C
            653D223022204672616D652E436F6C6F723D2231363737373231352220467261
            6D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2231353739
            303332302220476170583D22332220476170593D2233222048416C69676E3D22
            686152696768742220506172656E74466F6E743D2246616C7365222056416C69
            676E3D22766143656E7465722220546578743D2230222F3E3C2F63656C6C6D65
            6D6F733E3C63656C6C6865616465726D656D6F733E3C546672784D656D6F5669
            6577204C6566743D2237342220546F703D223434222057696474683D22313231
            22204865696768743D22323222205265737472696374696F6E733D2238222041
            6C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E436861
            727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569
            6768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E74
            2E5374796C653D223022204672616D652E436F6C6F723D223136373737323135
            22204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D
            2231343231313238382220476170583D22332220476170593D22332220506172
            656E74466F6E743D2246616C7365222056416C69676E3D22766143656E746572
            2220546578743D2250455243454E545F434C415353222F3E3C546672784D656D
            6F56696577204C6566743D2237342220546F703D223636222057696474683D22
            31323122204865696768743D22323222205265737472696374696F6E733D2238
            2220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E
            436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E
            4865696768743D222D31332220466F6E742E4E616D653D22417269616C222046
            6F6E742E5374796C653D223022204672616D652E436F6C6F723D223136373737
            32313522204672616D652E5479703D223135222046696C6C2E4261636B436F6C
            6F723D2231343231313238382220476170583D22332220476170593D22332220
            506172656E74466F6E743D2246616C7365222056416C69676E3D22766143656E
            7465722220546578743D2250455243454E545F434C415353222F3E3C54667278
            4D656D6F56696577204C6566743D223132322220546F703D2238382220576964
            74683D22353322204865696768743D22323222205265737472696374696F6E73
            3D22382220416C6C6F7745787072657373696F6E733D2246616C73652220466F
            6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F
            6E742E4865696768743D222D31332220466F6E742E4E616D653D22417269616C
            2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D223136
            37373732313522204672616D652E5479703D223135222046696C6C2E4261636B
            436F6C6F723D2231343231313238382220476170583D22332220476170593D22
            332220506172656E74466F6E743D2246616C7365222056416C69676E3D227661
            43656E7465722220546578743D2250455243454E545F434C415353222F3E3C2F
            63656C6C6865616465726D656D6F733E3C636F6C756D6E6D656D6F733E3C5466
            72784D656D6F56696577204C6566743D223135332220546F703D223532382C34
            32353431343039222057696474683D2231313322204865696768743D22323222
            205265737472696374696F6E733D2232342220416C6C6F774578707265737369
            6F6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E
            742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F
            6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230222046
            72616D652E436F6C6F723D22313637373732313522204672616D652E5479703D
            223135222046696C6C2E4261636B436F6C6F723D223134323131323838222047
            6170583D22332220476170593D2233222048416C69676E3D22686143656E7465
            722220506172656E74466F6E743D2246616C7365222056416C69676E3D227661
            43656E7465722220546578743D22222F3E3C2F636F6C756D6E6D656D6F733E3C
            636F6C756D6E746F74616C6D656D6F733E3C546672784D656D6F56696577204C
            6566743D223137302220546F703D223232222057696474683D22363422204865
            696768743D22323222205265737472696374696F6E733D223822205669736962
            6C653D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C
            73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D
            22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D
            22417269616C2220466F6E742E5374796C653D223122204672616D652E436F6C
            6F723D22313637373732313522204672616D652E5479703D223135222046696C
            6C2E4261636B436F6C6F723D2231343231313238382220476170583D22332220
            476170593D2233222048416C69676E3D22686143656E7465722220506172656E
            74466F6E743D2246616C7365222056416C69676E3D22766143656E7465722220
            546578743D225375622D746F74616C222F3E3C2F636F6C756D6E746F74616C6D
            656D6F733E3C636F726E65726D656D6F733E3C546672784D656D6F5669657720
            4C6566743D2232302220546F703D223530362C34323534313430392220576964
            74683D2231333322204865696768743D22323222205265737472696374696F6E
            733D22382220416C6C6F7745787072657373696F6E733D2246616C7365222046
            6F6E742E436861727365743D22312220466F6E742E436F6C6F723D2230222046
            6F6E742E4865696768743D222D31332220466F6E742E4E616D653D2241726961
            6C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231
            3637373732313522204672616D652E5479703D223135222046696C6C2E426163
            6B436F6C6F723D2231343231313238382220476170583D22332220476170593D
            2233222048416C69676E3D22686143656E7465722220506172656E74466F6E74
            3D2246616C7365222056416C69676E3D22766143656E7465722220546578743D
            2250657263656E7475616C222F3E3C546672784D656D6F56696577204C656674
            3D223135332220546F703D223530362C3432353431343039222057696474683D
            2231313322204865696768743D22323222205265737472696374696F6E733D22
            382220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E74
            2E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E74
            2E4865696768743D222D31332220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D2231343231313238382220476170583D22332220476170593D223322
            2048416C69676E3D22686143656E7465722220506172656E74466F6E743D2246
            616C7365222056416C69676E3D22766143656E7465722220546578743D22434C
            4153534946494341C387C3834F222F3E3C546672784D656D6F56696577204C65
            66743D2237342220546F703D223232222057696474683D223132312220486569
            6768743D22323222205265737472696374696F6E733D2238222056697369626C
            653D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C73
            652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D22
            302220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D22
            417269616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F
            723D22313637373732313522204672616D652E5479703D223135222046696C6C
            2E4261636B436F6C6F723D2231343231313238382220476170583D2233222047
            6170593D2233222048416C69676E3D22686143656E7465722220506172656E74
            466F6E743D2246616C7365222056416C69676E3D22766143656E746572222054
            6578743D22222F3E3C546672784D656D6F56696577204C6566743D2232302220
            546F703D223532382C3432353431343039222057696474683D22373422204865
            696768743D22323222205265737472696374696F6E733D22382220416C6C6F77
            45787072657373696F6E733D2246616C73652220466F6E742E43686172736574
            3D22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D
            222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E537479
            6C653D223022204672616D652E436F6C6F723D22313637373732313522204672
            616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22313432
            31313238382220476170583D22332220476170593D2233222048416C69676E3D
            22686143656E7465722220506172656E74466F6E743D2246616C736522205641
            6C69676E3D22766143656E7465722220546578743D2250414C4C4554222F3E3C
            546672784D656D6F56696577204C6566743D2239342220546F703D223532382C
            3432353431343039222057696474683D22353922204865696768743D22323222
            205265737472696374696F6E733D22382220416C6C6F7745787072657373696F
            6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E74
            2E436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E
            742E4E616D653D22417269616C2220466F6E742E5374796C653D223022204672
            616D652E436F6C6F723D22313637373732313522204672616D652E5479703D22
            3135222046696C6C2E4261636B436F6C6F723D22313432313132383822204761
            70583D22332220476170593D2233222048416C69676E3D22686143656E746572
            2220506172656E74466F6E743D2246616C7365222056416C69676E3D22766143
            656E7465722220546578743D22C381726561206DC2B2222F3E3C2F636F726E65
            726D656D6F733E3C726F776D656D6F733E3C546672784D656D6F56696577204C
            6566743D2232302220546F703D223535302C3432353431343039222057696474
            683D22373422204865696768743D22323222205265737472696374696F6E733D
            2232342220416C6C6F7745787072657373696F6E733D2246616C73652220466F
            6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F
            6E742E4865696768743D222D31332220466F6E742E4E616D653D22417269616C
            2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D223136
            37373732313522204672616D652E5479703D223135222046696C6C2E4261636B
            436F6C6F723D2231343231313238382220476170583D22332220476170593D22
            33222048416C69676E3D22686143656E7465722220506172656E74466F6E743D
            2246616C7365222056416C69676E3D22766143656E7465722220546578743D22
            222F3E3C546672784D656D6F56696577204C6566743D2239342220546F703D22
            3535302C3432353431343039222057696474683D22353922204865696768743D
            22323222205265737472696374696F6E733D2232342220416C6C6F7745787072
            657373696F6E733D2246616C73652220446973706C6179466F726D61742E466F
            726D61745374723D2225322E326E2220446973706C6179466F726D61742E4B69
            6E643D22666B4E756D657269632220466F6E742E436861727365743D22312220
            466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D313322
            20466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230
            22204672616D652E436F6C6F723D22313637373732313522204672616D652E54
            79703D223135222046696C6C2E4261636B436F6C6F723D223134323131323838
            2220476170583D22332220476170593D2233222048416C69676E3D2268614365
            6E7465722220506172656E74466F6E743D2246616C7365222056416C69676E3D
            22766143656E7465722220546578743D22222F3E3C2F726F776D656D6F733E3C
            726F77746F74616C6D656D6F733E3C546672784D656D6F56696577204C656674
            3D22302220546F703D223636222057696474683D22373422204865696768743D
            22323222205265737472696374696F6E733D2238222056697369626C653D2246
            616C73652220416C6C6F7745787072657373696F6E733D2246616C7365222046
            6F6E742E436861727365743D22312220466F6E742E436F6C6F723D2230222046
            6F6E742E4865696768743D222D31332220466F6E742E4E616D653D2241726961
            6C2220466F6E742E5374796C653D223122204672616D652E436F6C6F723D2231
            3637373732313522204672616D652E5479703D223135222046696C6C2E426163
            6B436F6C6F723D2231343231313238382220476170583D22332220476170593D
            2233222048416C69676E3D22686143656E7465722220506172656E74466F6E74
            3D2246616C7365222056416C69676E3D22766143656E7465722220546578743D
            22544F54414C222F3E3C546672784D656D6F56696577204C6566743D22373422
            20546F703D223636222057696474683D2231313122204865696768743D223232
            22205265737472696374696F6E733D2238222056697369626C653D2246616C73
            652220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E74
            2E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E74
            2E4865696768743D222D31332220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223122204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D2231343231313238382220476170583D22332220476170593D223322
            2048416C69676E3D22686143656E7465722220506172656E74466F6E743D2246
            616C7365222056416C69676E3D22766143656E7465722220546578743D22546F
            74616C222F3E3C2F726F77746F74616C6D656D6F733E3C63656C6C66756E6374
            696F6E733E3C6974656D20312F3E3C2F63656C6C66756E6374696F6E733E3C63
            6F6C756D6E736F72743E3C6974656D20322F3E3C2F636F6C756D6E736F72743E
            3C726F77736F72743E3C6974656D20302F3E3C6974656D20302F3E3C2F726F77
            736F72743E3C2F63726F73733E}
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 260.787570000000000000
        Width = 680.315400000000000000
        object Memo17: TfrxMemoView
          Width = 321.259854720000000000
          Height = 15.118110240000000000
          DataSet = FmVSMovImp.frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 321.260050000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = FmVSMovImp.frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsWBRclIts."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 370.393940000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = FmVSMovImp.frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsWBRclIts."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 419.527830000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = FmVSMovImp.frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsWBRclIts."AreaP2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 476.220780000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = FmVSMovImp.frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsWBRclIts."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 525.354670000000100000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = FmVSMovImp.frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsWBRclIts."SdoVrtPeca">,MD002,1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 574.488560000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = FmVSMovImp.frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsWBRclIts."SdoVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsWBRclIts."Codigo"'
      end
    end
  end
  object frxDsResulClas: TfrxDBDataset
    UserName = 'frxDsResulClas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'MovimID=MovimID'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pallet=Pallet'
      'NO_Pallet=NO_Pallet'
      'SrcNivel2=SrcNivel2'
      'GGX_REFER=GGX_REFER'
      'NO_ORI_PALLET=NO_ORI_PALLET'
      'PALLET_M2=PALLET_M2'
      'PALLET_ITENS=PALLET_ITENS'
      'PERCENT_CLASS=PERCENT_CLASS')
    DataSet = QrResulClas
    BCDToCurrency = False
    Left = 180
    Top = 396
  end
  object frxDsResulTota: TfrxDBDataset
    UserName = 'frxDsResulTota'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_Pallet=NO_Pallet'
      'Pecas=Pecas'
      'GGX_REFER=GGX_REFER'
      'GraGruX=GraGruX'
      'PERCENT_CLASS=PERCENT_CLASS'
      'AreaM2=AreaM2'
      'CalcM2=CalcM2')
    DataSet = QrResulTota
    BCDToCurrency = False
    Left = 264
    Top = 396
  end
  object QrResulTota: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrResulTotaCalcFields
    SQL.Strings = (
      'SELECT "TOTAL" NO_Pallet, SUM(wmi.Pecas) Pecas,'
      ' SUM(wmi.AreaM2) AreaM2, '
      'IF(gg1.Referencia <> "", gg1.Referencia, CONCAT("#", '
      'ggx.Controle)) GGX_REFER, wmi.GraGruX'
      'FROM wbmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet '
      'WHERE wmi.SrcMovID=38'
      'AND wmi.SrcNivel1=37'
      'AND MovimNiv=2'
      'GROUP BY GraGruX')
    Left = 264
    Top = 352
    object QrResulTotaNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Required = True
      Size = 5
    end
    object QrResulTotaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrResulTotaGGX_REFER: TWideStringField
      FieldName = 'GGX_REFER'
      Size = 25
    end
    object QrResulTotaGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrResulTotaPERCENT_CLASS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERCENT_CLASS'
      Calculated = True
    end
    object QrResulTotaAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrResulTotaCalcM2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CalcM2'
      Calculated = True
    end
  end
  object QrPalletInn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE'
      'FROM wbmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro'
      'WHERE wmi.MovimCod=38'
      'AND SdoVrtPeca>=0.001'
      'ORDER BY wmi.Controle')
    Left = 424
    Top = 356
    object QrPalletInnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPalletInnControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPalletInnMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrPalletInnMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrPalletInnEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPalletInnTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrPalletInnMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrPalletInnDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrPalletInnPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrPalletInnGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrPalletInnPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrPalletInnPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrPalletInnAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrPalletInnAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrPalletInnSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrPalletInnSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrPalletInnSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrPalletInnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPalletInnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPalletInnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPalletInnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPalletInnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPalletInnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPalletInnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPalletInnSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrPalletInnObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrPalletInnNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPalletInnNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrPalletInnNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrPalletInnNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
  end
  object QrWBInnSum: TmySQLQuery
    Database = Dmod.MyDB
    Left = 344
    Top = 396
    object QrWBInnSumAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrWBInnSumPecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object frxDsWBRclIts: TfrxDBDataset
    UserName = 'frxDsWBRcllts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'MovimID=MovimID'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'PC_RECLAS=PC_RECLAS'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'SdoVrtArM2=SdoVrtArM2'
      'SdoVrtPeca=SdoVrtPeca'
      'Observ=Observ'
      'ValorT=ValorT')
    DataSet = QrWBRclIts
    BCDToCurrency = False
    Left = 772
    Top = 88
  end
  object frxDsWBRclCab: TfrxDBDataset
    UserName = 'frxDsWBRclCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'DtCompra=DtCompra'
      'DtViagem=DtViagem'
      'DtEntrada=DtEntrada'
      'Fornecedor=Fornecedor'
      'Transporta=Transporta'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_FORNECE=NO_FORNECE'
      'NO_TRANSPORTA=NO_TRANSPORTA'
      'ValorT=ValorT')
    DataSet = QrWBRclCab
    BCDToCurrency = False
    Left = 688
    Top = 92
  end
  object frxWET_RECUR_001_02: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41608.425381412000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Imprime: Boolean;'
      '    '
      'procedure Line3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Line3.Visible := Imprime;                         '
      
        '  Imprime := not Imprime;                                       ' +
        '                                                                ' +
        '                  '
      'end;'
      ''
      'begin'
      '  Imprime := True;                                  '
      'end.')
    OnGetValue = frxWET_RECUR_001_01GetValue
    Left = 424
    Top = 312
    Datasets = <
      item
        DataSet = frxDsPalletInn
        DataSetName = 'frxDsPalletInn'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 559.370078740157500000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPalletInn
        DataSetName = 'frxDsPalletInn'
        RowCount = 0
        object Shape1: TfrxShapeView
          Top = 37.897650000000000000
          Width = 680.315400000000000000
          Height = 483.779840000000000000
          Shape = skRoundRectangle
        end
        object Memo47: TfrxMemoView
          Left = 200.315090000000000000
          Top = 90.811070000000000000
          Width = 283.464750000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-Prima para Semi / Acabado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 18.897650000000000000
          Top = 90.811070000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 483.779840000000000000
          Top = 90.811070000000000000
          Width = 177.637846540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Item [Line#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 20.015770000000000000
          Top = 128.795300000000000000
          Width = 411.968330630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'PALLET  [frxDsPalletInn."Pallet"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 1.118120000000000000
          Top = 176.574830000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          Left = 1.118120000000000000
          Top = 173.015770000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          Top = 268.448828660000000000
          Width = 188.976377950000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 188.976377950000000000
          Top = 268.448828660000000000
          Width = 491.338582680000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Proced'#234'ncia')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 226.574830000000000000
          Width = 120.944881890000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-prima:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 340.157480310000000000
          Top = 362.937034720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Top = 362.937034720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 510.236220470000000000
          Top = 362.937034720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 170.078740160000000000
          Top = 362.937034720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 306.244106690000000000
          Width = 188.976377950000000000
          Height = 52.913395590000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPalletInn."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 188.976377950000000000
          Top = 306.244106690000000000
          Width = 491.338582680000000000
          Height = 52.913395590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPalletInn."Terceiro"] - [frxDsPalletInn."NO_FORNECE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 120.944881890000000000
          Top = 226.574830000000000000
          Width = 559.370078740000100000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPalletInn."GraGruX"] - [frxDsPalletInn."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 340.157480310000000000
          Top = 400.732312760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPalletInn."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 400.732312760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsPalletInn."' +
              'Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 510.236220470000000000
          Top = 400.732312760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsPalletInn."' +
              'PesoKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 170.078740160000000000
          Top = 400.732312760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPalletInn."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 120.944881890000000000
          Top = 180.795300000000000000
          Width = 559.370078740000100000
          Height = 40.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPalletInn."NO_EMPRESA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 180.795300000000000000
          Width = 120.944881890000000000
          Height = 40.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente Interno: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Top = 56.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 226.771800000000000000
          Top = 128.606370000000000000
          Width = 434.645510630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPalletInn."NO_Pallet"]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 559.370103150000000000
          Width = 680.315400000000000000
          OnBeforePrint = 'Line3OnBeforePrint'
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
        object Memo10: TfrxMemoView
          Left = 37.795300000000000000
          Top = 438.425480000000000000
          Width = 604.724800000000000000
          Height = 83.149660000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPalletInn."Observ"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPalletInn: TfrxDBDataset
    UserName = 'frxDsPalletInn'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SdoVrtPeca=SdoVrtPeca'
      'Observ=Observ'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_Pallet'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_FORNECE=NO_FORNECE')
    DataSet = QrPalletInn
    BCDToCurrency = False
    Left = 424
    Top = 400
  end
  object QrPalletCla: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Pallet, MAX(DataHora) DataHora,  '
      'SUM(wmi.Pecas) Pecas,  '
      'IF(MIN(AreaM2) > 0.01, 0, SUM(wmi.AreaM2)) AreaM2,  '
      'IF(MIN(AreaP2) > 0.01, 0, SUM(wmi.AreaP2)) AreaP2,  '
      'IF(MIN(PesoKg) > 0.001, 0, SUM(wmi.PesoKg)) PesoKg,  '
      'CONCAT(gg1.Nome,   '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,   '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,  '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE  '
      'FROM wbmovits wmi   '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX   '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet   '
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa '
      'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro  '
      'WHERE wmi.Pallet IN (17, 18, 19, 20, 21)  '
      'GROUP BY wmi.Pallet, wmi.GraGruX, wmi.Empresa, wmi.Terceiro  '
      '   ')
    Left = 564
    Top = 356
    object QrPalletClaPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrPalletClaDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrPalletClaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrPalletClaAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrPalletClaAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrPalletClaPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrPalletClaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPalletClaNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrPalletClaNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrPalletClaNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrPalletClaTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrPalletClaGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object frxWET_RECUR_001_03: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41608.425381412000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Imprime: Boolean;'
      '    '
      'procedure Line3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Line3.Visible := Imprime;                         '
      
        '  Imprime := not Imprime;                                       ' +
        '                                                                ' +
        '                  '
      'end;'
      ''
      'begin'
      '  Imprime := True;                                  '
      'end.')
    OnGetValue = frxWET_RECUR_001_01GetValue
    Left = 564
    Top = 308
    Datasets = <
      item
        DataSet = frxDsPalletCla
        DataSetName = 'frxDsPalletCla'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 559.370078740000100000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPalletCla
        DataSetName = 'frxDsPalletCla'
        RowCount = 0
        object Shape1: TfrxShapeView
          Top = 37.897650000000000000
          Width = 680.315400000000000000
          Height = 483.779840000000000000
          Shape = skRoundRectangle
        end
        object Memo47: TfrxMemoView
          Left = 200.315090000000000000
          Top = 90.811070000000000000
          Width = 283.464750000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-Prima para Semi / Acabado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 18.897650000000000000
          Top = 90.811070000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 483.779840000000000000
          Top = 90.811070000000000000
          Width = 177.637846540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Item [Line#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 20.015770000000000000
          Top = 128.795300000000000000
          Width = 411.968330630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'PALLET [frxDsPalletCla."Pallet"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 1.118120000000000000
          Top = 176.574830000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          Left = 1.118120000000000000
          Top = 173.015770000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          Top = 268.448828660000000000
          Width = 188.976377950000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 188.976377950000000000
          Top = 268.448828660000000000
          Width = 491.338582680000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Proced'#234'ncia')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 226.574830000000000000
          Width = 120.944881890000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-prima:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 340.157480310000000000
          Top = 359.157504720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Top = 359.157504720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 510.236220470000000000
          Top = 359.157504720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 170.078740160000000000
          Top = 359.157504720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 306.244106690000000000
          Width = 188.976377950000000000
          Height = 52.913385826771660000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPalletCla."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 188.976377950000000000
          Top = 306.244106690000000000
          Width = 491.338582680000000000
          Height = 52.913385830000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPalletCla."Terceiro"] - [frxDsPalletCla."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 120.944881890000000000
          Top = 226.574830000000000000
          Width = 559.370078740000100000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPalletCla."GraGruX"] - [frxDsPalletCla."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 340.157480310000000000
          Top = 396.952782760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsPalletCla."Ar' +
              'eaP2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 396.952782760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsPalletCla."' +
              'Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 510.236220470000000000
          Top = 396.952782760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsPalletCla."' +
              'PesoKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 170.078740160000000000
          Top = 396.952782760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsPalletCla."Ar' +
              'eaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 120.944881890000000000
          Top = 180.795300000000000000
          Width = 559.370078740000100000
          Height = 40.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPalletCla."NO_EMPRESA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 180.795300000000000000
          Width = 120.944881890000000000
          Height = 40.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente Interno: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Top = 56.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 226.771800000000000000
          Top = 128.606370000000000000
          Width = 434.645510630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPalletCla."NO_Pallet"]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 559.370103150000000000
          Width = 680.315400000000000000
          OnBeforePrint = 'Line3OnBeforePrint'
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
        object Line2: TfrxLineView
          Top = 464.882190000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Line4: TfrxLineView
          Left = 11.338590000000000000
          Top = 495.118430000000000000
          Width = 657.638220000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
      end
    end
  end
  object frxDsPalletCla: TfrxDBDataset
    UserName = 'frxDsPalletCla'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Pallet=Pallet'
      'DataHora=DataHora'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'PesoKg=PesoKg'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_Pallet'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_FORNECE=NO_FORNECE'
      'Terceiro=Terceiro'
      'GraGruX=GraGruX')
    DataSet = QrPalletCla
    BCDToCurrency = False
    Left = 564
    Top = 404
  end
  object PMReclasif: TPopupMenu
    OnPopup = PMReclasifPopup
    Left = 772
    Top = 580
    object Incluireclassificao1: TMenuItem
      Caption = '&Inclui reclassifica'#231#227'o'
      OnClick = Incluireclassificao1Click
    end
    object Alterareclassificao1: TMenuItem
      Caption = '&Altera reclassifica'#231#227'o'
      OnClick = Alterareclassificao1Click
    end
    object Excluireclassificao1: TMenuItem
      Caption = '&Exclui reclassifica'#231#227'o'
      OnClick = Excluireclassificao1Click
    end
  end
end
