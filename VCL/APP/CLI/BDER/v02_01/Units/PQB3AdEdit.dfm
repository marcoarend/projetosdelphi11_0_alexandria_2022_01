object FmPQB3AdEdit: TFmPQB3AdEdit
  Left = 426
  Top = 332
  Caption = 'QUI-BALAN-010 :: Edi'#231#227'o de Entrada'
  ClientHeight = 360
  ClientWidth = 482
  Color = clBtnFace
  Constraints.MaxHeight = 535
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 482
    Height = 207
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 225
    object CkContinuar: TCheckBox
      Left = 12
      Top = 216
      Width = 113
      Height = 17
      Caption = 'Continuar inserindo'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 482
      Height = 125
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 16
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label7: TLabel
        Left = 16
        Top = 44
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label8: TLabel
        Left = 16
        Top = 84
        Width = 37
        Height = 13
        Caption = 'Insumo:'
      end
      object EdEmpresa: TdmkEdit
        Left = 16
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNO_Empresa: TdmkEdit
        Left = 76
        Top = 20
        Width = 400
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCliInt: TdmkEdit
        Left = 16
        Top = 60
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNO_CliInt: TdmkEdit
        Left = 76
        Top = 60
        Width = 400
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdInsumo: TdmkEdit
        Left = 16
        Top = 100
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNO_Insumo: TdmkEdit
        Left = 76
        Top = 100
        Width = 400
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 125
      Width = 482
      Height = 82
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      ExplicitLeft = 84
      ExplicitTop = 152
      ExplicitWidth = 185
      ExplicitHeight = 41
      object Label5: TLabel
        Left = 120
        Top = 40
        Width = 84
        Height = 13
        Caption = 'Data de validade:'
      end
      object Label3: TLabel
        Left = 16
        Top = 40
        Width = 94
        Height = 13
        Caption = 'Data de fabrica'#231#227'o:'
      end
      object SpeedButton1: TSpeedButton
        Left = 448
        Top = 16
        Width = 23
        Height = 22
        Caption = '?'
        OnClick = SpeedButton1Click
      end
      object Label12: TLabel
        Left = 126
        Top = 0
        Width = 24
        Height = 13
        Caption = 'Lote:'
      end
      object Label1: TLabel
        Left = 16
        Top = 0
        Width = 76
        Height = 13
        Caption = 'S'#233'rie e NF (VP):'
      end
      object Edide_serie: TdmkEdit
        Left = 16
        Top = 16
        Width = 37
        Height = 21
        Alignment = taRightJustify
        MaxLength = 9
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPdVal: TdmkEditDateTimePicker
        Left = 120
        Top = 56
        Width = 100
        Height = 21
        Date = 45219.000000000000000000
        Time = 0.383424131941865200
        TabOrder = 4
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPdFab: TdmkEditDateTimePicker
        Left = 16
        Top = 56
        Width = 100
        Height = 21
        Date = 45219.000000000000000000
        Time = 0.383424131941865200
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdxLote: TdmkEdit
        Left = 125
        Top = 16
        Width = 316
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edide_nNF: TdmkEdit
        Left = 56
        Top = 16
        Width = 66
        Height = 21
        Alignment = taRightJustify
        MaxLength = 9
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 482
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 434
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 386
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 224
        Height = 32
        Caption = 'Edi'#231#227'o de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 224
        Height = 32
        Caption = 'Edi'#231#227'o de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 224
        Height = 32
        Caption = 'Edi'#231#227'o de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 255
    Width = 482
    Height = 41
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 300
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 478
      Height = 24
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 27
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 296
    Width = 482
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 317
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 478
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 334
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object Qry: TMySQLQuery
    Database = DmProd.MySQLDatabase1
    Left = 408
    Top = 52
  end
end
