object FmPQB2: TFmPQB2
  Left = 340
  Top = 174
  Caption = 'QUI-BALAN-001 :: Balan'#231'os de Insumos'
  ClientHeight = 640
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 471
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelDados1: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 50
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 424
        Top = 6
        Width = 68
        Height = 13
        Caption = 'Balan'#231'o Qtde:'
      end
      object Label2: TLabel
        Left = 516
        Top = 6
        Width = 69
        Height = 13
        Caption = 'Balan'#231'o Valor:'
      end
      object Label4: TLabel
        Left = 8
        Top = 6
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit3
      end
      object DBEdit1: TDBEdit
        Left = 424
        Top = 22
        Width = 89
        Height = 21
        TabStop = False
        DataField = 'EstqQ'
        DataSource = DsBalancos
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 516
        Top = 22
        Width = 89
        Height = 21
        TabStop = False
        DataField = 'EstqV'
        DataSource = DsBalancos
        TabOrder = 1
      end
      object DBCheckBox1: TDBCheckBox
        Left = 612
        Top = 24
        Width = 97
        Height = 17
        Caption = 'Confirmado.'
        DataField = 'Confirmado'
        DataSource = DsBalancos
        TabOrder = 2
        ValueChecked = 'V'
        ValueUnchecked = 'F'
      end
      object GroupBox1: TGroupBox
        Left = 715
        Top = 0
        Width = 293
        Height = 50
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alRight
        Caption = ' Pesquisa pelo nome: '
        TabOrder = 3
        object Label10: TLabel
          Left = 8
          Top = 24
          Width = 56
          Height = 13
          Caption = 'Mercadoria:'
        end
        object EdPesquisa: TdmkEdit
          Left = 73
          Top = 22
          Width = 203
          Height = 20
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdPesquisaChange
        end
      end
      object DBEdit3: TDBEdit
        Left = 8
        Top = 22
        Width = 56
        Height = 21
        DataField = 'Filial'
        DataSource = DsBalancos
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 64
        Top = 22
        Width = 357
        Height = 21
        DataField = 'NO_Emp'
        DataSource = DsBalancos
        TabOrder = 5
      end
    end
    object PainelDados2: TPanel
      Left = 0
      Top = 451
      Width = 1008
      Height = 20
      Align = alBottom
      TabOrder = 1
      Visible = False
      object Progress1: TProgressBar
        Left = 469
        Top = 1
        Width = 538
        Height = 18
        Align = alClient
        Position = 50
        Smooth = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 1
        Top = 1
        Width = 228
        Height = 18
        Align = alLeft
        BevelOuter = bvLowered
        Caption = 'Aguarde...    Atualizando estoque...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = True
        ParentFont = False
        TabOrder = 1
      end
      object Panel2: TPanel
        Left = 229
        Top = 1
        Width = 60
        Height = 18
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Caption = '  Registros:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 2
      end
      object PnRegistros: TPanel
        Left = 289
        Top = 1
        Width = 76
        Height = 18
        Align = alLeft
        Alignment = taRightJustify
        BevelOuter = bvLowered
        Caption = '0  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 3
      end
      object Panel4: TPanel
        Left = 365
        Top = 1
        Width = 48
        Height = 18
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Caption = '  Tempo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 4
      end
      object PnTempo: TPanel
        Left = 413
        Top = 1
        Width = 56
        Height = 18
        Align = alLeft
        Alignment = taRightJustify
        BevelOuter = bvLowered
        Caption = '0:00:00  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 5
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 387
      Width = 1008
      Height = 64
      Align = alBottom
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 859
          Top = 0
          Width = 145
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
        end
        object BtTrava: TBitBtn
          Tag = 14
          Left = 8
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Trava'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtTravaClick
        end
        object BtRefresh: TBitBtn
          Tag = 18
          Left = 152
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Faz / Desfaz'
          TabOrder = 2
          OnClick = BtRefreshClick
        end
        object BtIncluiIts: TBitBtn
          Tag = 10
          Left = 324
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Insere'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtIncluiItsClick
        end
        object BtAlteraIts: TBitBtn
          Tag = 11
          Left = 416
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'Edit&a'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtAlteraItsClick
        end
        object BtExcluiIts: TBitBtn
          Tag = 12
          Left = 508
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtExcluiItsClick
        end
        object BtPrivativo: TBitBtn
          Tag = 182
          Left = 600
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Zera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = BtPrivativoClick
        end
        object BtInicial: TBitBtn
          Left = 692
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'I&nicial'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          OnClick = BtInicialClick
        end
      end
    end
    object DBGCI: TdmkDBGridZTO
      Left = 0
      Top = 50
      Width = 357
      Height = 337
      Align = alLeft
      DataSource = DsCliDest
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ENT'
          Title.Caption = 'Cliente interno'
          Width = 252
          Visible = True
        end>
    end
    object Panel9: TPanel
      Left = 357
      Top = 50
      Width = 651
      Height = 337
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 4
      object Label3: TLabel
        Left = 0
        Top = 0
        Width = 572
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 
          'Para localizar um insumo espec'#237'fico use a pesquisa acima ou "Ctr' +
          'l" + "F" para localizar pelo c'#243'digo!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clFuchsia
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBGBalancosIts: TdmkDBGridZTO
        Left = 0
        Top = 13
        Width = 651
        Height = 324
        Align = alClient
        DataSource = DsBalancosIts
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        OnKeyDown = DBGBalancosItsKeyDown
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Insumo'
            Title.Alignment = taCenter
            Title.Caption = 'C'#243'digo'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEPQ'
            Title.Caption = 'Mercadoria'
            Width = 366
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Title.Alignment = taRightJustify
            Title.Caption = 'Quantidade'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Title.Alignment = taRightJustify
            Title.Caption = 'Valor total'
            Width = 92
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 261
        Height = 32
        Caption = 'Balan'#231'os de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 261
        Height = 32
        Caption = 'Balan'#231'os de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 261
        Height = 32
        Caption = 'Balan'#231'os de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 971
        Height = 17
        Caption = 
          'Para retornar insumos de clientes internos (QUI-RETOR-001), crie' +
          ' balan'#231'os mensais (manual ou resgata) e logo gere as diferen'#231'as ' +
          'do m'#234's anterior!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 971
        Height = 17
        Caption = 
          'Para retornar insumos de clientes internos (QUI-RETOR-001), crie' +
          ' balan'#231'os mensais (manual ou resgata) e logo gere as diferen'#231'as ' +
          'do m'#234's anterior!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB2: TProgressBar
        Left = 0
        Top = 19
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBCntrl: TGroupBox
    Left = 0
    Top = 576
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 172
      Height = 47
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
    end
    object LaRegistro: TStaticText
      Left = 174
      Top = 15
      Width = 30
      Height = 17
      Align = alClient
      BevelInner = bvLowered
      BevelKind = bkFlat
      Caption = '[N]: 0'
      TabOrder = 2
    end
    object Panel3: TPanel
      Left = 296
      Top = 15
      Width = 710
      Height = 47
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object Panel7: TPanel
        Left = 577
        Top = 0
        Width = 133
        Height = 47
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 188
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtExcluiClick
      end
      object BtResgata: TBitBtn
        Left = 280
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Resgata'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtResgataClick
      end
      object BtGeraDif: TBitBtn
        Left = 372
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Gera dif m'#234's ant'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtGeraDifClick
      end
      object BtAjusta: TBitBtn
        Left = 464
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = 'Aj&usta'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        Visible = False
        OnClick = BtAjustaClick
      end
    end
  end
  object DsBalancos: TDataSource
    DataSet = QrBalancos
    Left = 56
    Top = 209
  end
  object DsBalancosIts: TDataSource
    DataSet = QrBalancosIts
    Left = 608
    Top = 233
  end
  object DsArtigo: TDataSource
    DataSet = QrArtigo
    Left = 64
    Top = 381
  end
  object QrBalancos: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrBalancosAfterOpen
    BeforeClose = QrBalancosBeforeClose
    AfterScroll = QrBalancosAfterScroll
    OnCalcFields = QrBalancosCalcFields
    SQL.Strings = (
      'SELECT * FROM balancos')
    Left = 56
    Top = 161
    object QrBalancosPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrBalancosEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrBalancosEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrBalancosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBalancosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBalancosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBalancosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBalancosConfirmado: TWideStringField
      FieldName = 'Confirmado'
      Size = 1
    end
    object QrBalancosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBalancosPeriodo2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Periodo2'
      Size = 50
      Calculated = True
    end
    object QrBalancosEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrBalancosNO_Emp: TWideStringField
      FieldName = 'NO_Emp'
      Size = 100
    end
    object QrBalancosFilial: TIntegerField
      FieldName = 'Filial'
    end
  end
  object QrBalancosIts: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrBalancosItsAfterOpen
    BeforeClose = QrBalancosItsBeforeClose
    OnCalcFields = QrBalancosItsCalcFields
    SQL.Strings = (
      'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico, '
      'pqg.Nome NOMEGRUPO,'
      'IF(pqx.Valor=0, 0, pqx.Valor/pqx.Peso) CUSTOKG'
      'FROM pqx pqx'
      'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo'
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ'
      'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico'
      'WHERE pqx.Tipo=0'
      'AND pqx.OrigemCodi=:P0'
      'ORDER BY NOMEPQ')
    Left = 608
    Top = 185
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBalancosItsDataX: TDateField
      FieldName = 'DataX'
    end
    object QrBalancosItsTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrBalancosItsCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrBalancosItsCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrBalancosItsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrBalancosItsPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrBalancosItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrBalancosItsOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrBalancosItsOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrBalancosItsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrBalancosItsGrupoQuimico: TIntegerField
      FieldName = 'GrupoQuimico'
    end
    object QrBalancosItsNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 100
    end
    object QrBalancosItsCUSTOKG: TFloatField
      FieldName = 'CUSTOKG'
    end
    object QrBalancosItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrArtigo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM produtos'
      'WHERE Codigo=:P0')
    Left = 36
    Top = 381
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArtigoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrArtigoNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrAtualiza: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT PQ, CI, Empresa'
      'FROM pqcli'
      'ORDER BY PQ, CI')
    Left = 36
    Top = 413
    object QrAtualizaPQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrAtualizaCI: TIntegerField
      FieldName = 'CI'
    end
    object QrAtualizaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrMax: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Periodo) Periodo'
      'FROM balancos'
      'WHERE Empresa=?')
    Left = 92
    Top = 413
    object QrMaxPeriodo: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'Periodo'
    end
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(EstqQ) EstqQ, SUM(EstqV) EstqV'
      'FROM balancosits'
      'WHERE Periodo=:P0')
    Left = 64
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaEstqQ: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqQ'
    end
    object QrSomaEstqV: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqV'
    end
  end
  object QrProdutos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT PQ, CI, Peso, Valor'
      'FROM pqcli'
      'WHERE Peso<>0'
      'OR Valor<>0')
    Left = 448
    Top = 144
    object QrProdutosPQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrProdutosCI: TIntegerField
      FieldName = 'CI'
    end
    object QrProdutosPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrProdutosValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrPQCli: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Insumo, CliDest'
      'FROM pqx'
      'WHERE DataX BETWEEN :P0 AND :P1'
      '')
    Left = 48
    Top = 276
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPQCliInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQCliCliDest: TIntegerField
      FieldName = 'CliDest'
    end
  end
  object PMImprime: TPopupMenu
    Left = 236
    Top = 228
    object EsteBalano1: TMenuItem
      Caption = '&Este Balan'#231'o'
      OnClick = EsteBalano1Click
    end
    object Diferenasgeradas1: TMenuItem
      Caption = 'Diferen'#231'as geradas'
      OnClick = Diferenasgeradas1Click
    end
    object Outrasimpressoes1: TMenuItem
      Caption = 'Outras impress'#245'es'
      OnClick = Outrasimpressoes1Click
    end
  end
  object QrCliDest: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCliDestBeforeClose
    AfterScroll = QrCliDestAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT'
      'FROM pqx pqx'
      'LEFT JOIN entidades ent ON ent.Codigo=pqx.CliDest'
      'WHERE pqx.Tipo=0'
      'AND ent.Codigo <> 0'
      'AND pqx.OrigemCodi>0'
      'ORDER BY NO_ENT')
    Left = 120
    Top = 160
    object QrCliDestCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliDestNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsCliDest: TDataSource
    DataSet = QrCliDest
    Left = 120
    Top = 208
  end
  object frxDsBalancosIts: TfrxDBDataset
    UserName = 'frxDsBalancosIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataX=DataX'
      'Tipo=Tipo'
      'CliOrig=CliOrig'
      'CliDest=CliDest'
      'Insumo=Insumo'
      'Peso=Peso'
      'Valor=Valor'
      'OrigemCodi=OrigemCodi'
      'OrigemCtrl=OrigemCtrl'
      'NOMEPQ=NOMEPQ'
      'GrupoQuimico=GrupoQuimico'
      'NOMEGRUPO=NOMEGRUPO'
      'CUSTOKG=CUSTOKG')
    DataSet = QrBalancosIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 608
    Top = 284
  end
  object frxQUI_BALAN_001_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41537.466215289350000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxQUI_BALAN_001_01GetValue
    Left = 608
    Top = 332
    Datasets = <
      item
        DataSet = frxDsBalancosIts
        DataSetName = 'frxDsBalancosIts'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 108.708554020000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 22.677170240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708554020000000000
          Width = 408.188976380000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188971500000000000
          Top = 90.708554020000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897691500000000000
          Top = 90.708554020000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606411500000000000
          Top = 90.708554020000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'INVENT'#193'RIO MENSAL DE USO E CONSUMO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_MES]')
          ParentFont = False
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527559055083000
          Width = 381.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188956850000000000
          Top = 3.779527559999991000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBalancosIts."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Top = 3.779527559999991000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBalancosIts."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Top = 3.779527559999991000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsBalancosIts."Peso">)=0,0,(SUM(<frxDsBalancosIts."' +
              'Valor">) / SUM(<frxDsBalancosIts."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsBalancosIts
        DataSetName = 'frxDsBalancosIts'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 355.275590550000000000
          Height = 15.118110240000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsBalancosIts
          DataSetName = 'frxDsBalancosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBalancosIts."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188976380000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'Peso'
          DataSet = frxDsBalancosIts
          DataSetName = 'frxDsBalancosIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBalancosIts."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsBalancosIts
          DataSetName = 'frxDsBalancosIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBalancosIts."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataSet = frxDsBalancosIts
          DataSetName = 'frxDsBalancosIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBalancosIts."CUSTOKG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsBalancosIts
          DataSetName = 'frxDsBalancosIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBalancosIts."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrMoviPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM movipq'
      '')
    Left = 764
    Top = 216
    object QrMoviPQInsumo: TIntegerField
      FieldName = 'Insumo'
      Origin = 'movipq.Insumo'
    end
    object QrMoviPQNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Origin = 'movipq.NomePQ'
      Size = 100
    end
    object QrMoviPQNomeFO: TWideStringField
      FieldName = 'NomeFO'
      Origin = 'movipq.NomeFO'
      Size = 100
    end
    object QrMoviPQNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Origin = 'movipq.NomeCI'
      Size = 100
    end
    object QrMoviPQNomeSE: TWideStringField
      FieldName = 'NomeSE'
      Origin = 'movipq.NomeSE'
      Size = 100
    end
    object QrMoviPQInnPes: TFloatField
      FieldName = 'InnPes'
      Origin = 'movipq.InnPes'
    end
    object QrMoviPQInnVal: TFloatField
      FieldName = 'InnVal'
      Origin = 'movipq.InnVal'
    end
    object QrMoviPQOutPes: TFloatField
      FieldName = 'OutPes'
      Origin = 'movipq.OutPes'
    end
    object QrMoviPQOutVal: TFloatField
      FieldName = 'OutVal'
      Origin = 'movipq.OutVal'
    end
    object QrMoviPQAntPes: TFloatField
      FieldName = 'AntPes'
      Origin = 'movipq.AntPes'
    end
    object QrMoviPQAntVal: TFloatField
      FieldName = 'AntVal'
      Origin = 'movipq.AntVal'
    end
    object QrMoviPQFimPes: TFloatField
      FieldName = 'FimPes'
      Origin = 'movipq.FimPes'
    end
    object QrMoviPQFimVal: TFloatField
      FieldName = 'FimVal'
      Origin = 'movipq.FimVal'
    end
    object QrMoviPQBalPes: TFloatField
      FieldName = 'BalPes'
      Origin = 'movipq.BalPes'
    end
    object QrMoviPQBalVal: TFloatField
      FieldName = 'BalVal'
      Origin = 'movipq.BalVal'
    end
  end
  object QrDif2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Insumo , QtdBal - Peso Peso, '
      'IF(Peso=0, 0, Valor / Peso) Preco'
      'FROM _pqb2_resgata_'
      'WHERE QtdBal - Peso <> 0')
    Left = 400
    Top = 348
    object QrDif2Insumo: TIntegerField
      FieldName = 'Insumo'
      ReadOnly = True
    end
    object QrDif2Peso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrDif2Preco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object QrDifGer: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Nome NO_PQ, pqx.* '
      'FROM pqx pqx'
      'LEFT JOIN pq pq ON pq.Codigo=pqx.Insumo'
      'WHERE pqx.OrigemCodi=174'
      'AND pqx.Tipo=-1'
      'AND pqx.CliDest=9'
      'ORDER BY NO_PQ')
    Left = 776
    Top = 388
    object QrDifGerNO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrDifGerDataX: TDateField
      FieldName = 'DataX'
    end
    object QrDifGerOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrDifGerOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrDifGerTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrDifGerCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrDifGerCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrDifGerInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrDifGerPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrDifGerValor: TFloatField
      FieldName = 'Valor'
    end
    object QrDifGerRetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrDifGerStqMovIts: TIntegerField
      FieldName = 'StqMovIts'
    end
    object QrDifGerAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrDifGerAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrDifGerRetQtd: TFloatField
      FieldName = 'RetQtd'
    end
  end
  object frxDsDifGer: TfrxDBDataset
    UserName = 'frxDsDifGer'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PQ=NO_PQ'
      'DataX=DataX'
      'OrigemCodi=OrigemCodi'
      'OrigemCtrl=OrigemCtrl'
      'Tipo=Tipo'
      'CliOrig=CliOrig'
      'CliDest=CliDest'
      'Insumo=Insumo'
      'Peso=Peso'
      'Valor=Valor'
      'Retorno=Retorno'
      'StqMovIts=StqMovIts'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'RetQtd=RetQtd')
    DataSet = QrDifGer
    BCDToCurrency = False
    DataSetOptions = []
    Left = 700
    Top = 388
  end
  object frxQUI_BALAN_001_02: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41537.466215289350000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxQUI_BALAN_001_01GetValue
    Left = 608
    Top = 388
    Datasets = <
      item
        DataSet = frxDsBalancosIts
        DataSetName = 'frxDsBalancosIts'
      end
      item
        DataSet = frxDsDifGer
        DataSetName = 'frxDsDifGer'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 108.708554020000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 22.677170240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708554020000000000
          Width = 498.897696380000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897691500000000000
          Top = 90.708554020000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606411500000000000
          Top = 90.708554020000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'DIFEREN'#199'AS GERADAS EM BALAN'#199'O ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_MES_MES]')
          ParentFont = False
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527559999991000
          Width = 468.472480000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118146850000000000
          Top = 3.779527559999991000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDifGer."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826808270000000000
          Top = 3.779527559999991000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDifGer."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsDifGer
        DataSetName = 'frxDsDifGer'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 445.984310550000000000
          Height = 15.118110240000000000
          DataField = 'NO_PQ'
          DataSet = frxDsDifGer
          DataSetName = 'frxDsDifGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDifGer."NO_PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897696380000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataSet = frxDsDifGer
          DataSetName = 'frxDsDifGer'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDifGer."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606338270000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsDifGer
          DataSetName = 'frxDsDifGer'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDifGer."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsDifGer
          DataSetName = 'frxDsDifGer'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDifGer."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
end
