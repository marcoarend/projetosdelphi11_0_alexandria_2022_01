object FmListaSetores: TFmListaSetores
  Left = 368
  Top = 194
  Caption = 'IND-GERAL-001 :: Cadastro de Setores'
  ClientHeight = 475
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 379
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitHeight = 341
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 273
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 448
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Sigla:'
        FocusControl = DBEdit1
      end
      object Label14: TLabel
        Left = 20
        Top = 224
        Width = 178
        Height = 13
        Caption = 'Descri'#231#227'o da opera'#231#227'o subsequente:'
        FocusControl = DBEdit5
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 364
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsListaSetores
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsListaSetores
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 448
        Top = 32
        Width = 36
        Height = 21
        DataField = 'Sigla'
        DataSource = DsListaSetores
        TabOrder = 2
      end
      object dmkRadioGroup1: TDBRadioGroup
        Left = 20
        Top = 124
        Width = 465
        Height = 49
        Caption = ' Tipo de receita utilizada: '
        Columns = 4
        DataField = 'TpReceita'
        DataSource = DsListaSetores
        Items.Strings = (
          'Nenhuma'
          'Ribeira'
          'Recurtimento'
          'Acabamento')
        TabOrder = 3
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 20
        Top = 172
        Width = 465
        Height = 49
        Caption = ' Setor espec'#237'fico (uso interno): '
        Columns = 4
        DataField = 'TpSetor'
        DataSource = DsListaSetores
        Items.Strings = (
          'N'#227'o aplicavel'
          'Caleiro'
          'Curtimento'
          'Recurtimento')
        TabOrder = 4
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object GroupBox2: TGroupBox
        Left = 20
        Top = 60
        Width = 465
        Height = 65
        Caption = 'Produ'#231#227'o m'#225'xima por dia trabalhado:'
        TabOrder = 5
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 461
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label11: TLabel
            Left = 11
            Top = 4
            Width = 126
            Height = 13
            Caption = 'Produ'#231#227'o m'#225'xima m'#178' / dia:'
          end
          object Label12: TLabel
            Left = 151
            Top = 4
            Width = 134
            Height = 13
            Caption = 'Produ'#231#227'o m'#225'x. couros / dia:'
          end
          object Label13: TLabel
            Left = 291
            Top = 4
            Width = 128
            Height = 13
            Caption = 'Produ'#231#227'o m'#225'xima Kg / dia:'
          end
          object DBEdit2: TDBEdit
            Left = 11
            Top = 19
            Width = 134
            Height = 21
            DataField = 'm2Dia'
            DataSource = DsListaSetores
            TabOrder = 0
          end
          object DBEdit3: TDBEdit
            Left = 151
            Top = 19
            Width = 134
            Height = 21
            DataField = 'CourosDia'
            DataSource = DsListaSetores
            TabOrder = 1
          end
          object DBEdit4: TDBEdit
            Left = 291
            Top = 19
            Width = 134
            Height = 21
            DataField = 'KgDia'
            DataSource = DsListaSetores
            TabOrder = 2
          end
        end
      end
      object DBEdit5: TDBEdit
        Left = 20
        Top = 240
        Width = 465
        Height = 21
        DataField = 'OperPosProcDescr'
        DataSource = DsListaSetores
        TabOrder = 6
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 315
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 277
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 379
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitHeight = 341
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 281
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 444
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Sigla:'
      end
      object Label6: TLabel
        Left = 16
        Top = 228
        Width = 422
        Height = 13
        Caption = 
          'Descri'#231#227'o da opera'#231#227'o subsequente: Ex: Redescarne, Enxuga e mede' +
          ', Estirae seca, etc'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 364
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSigla: TdmkEdit
        Left = 444
        Top = 32
        Width = 36
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 3
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Sigla'
        UpdCampo = 'Sigla'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGTpReceita: TdmkRadioGroup
        Left = 16
        Top = 124
        Width = 465
        Height = 49
        Caption = ' Tipo de receita utilizada: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Nenhuma'
          'Ribeira'
          'Recurtimento'
          'Acabamento')
        TabOrder = 3
        QryCampo = 'TpReceita'
        UpdCampo = 'TpReceita'
        UpdType = utYes
        OldValor = 0
      end
      object RGTpSetor: TdmkRadioGroup
        Left = 16
        Top = 176
        Width = 465
        Height = 49
        Caption = ' Setor de setor (para uso de receita): '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o aplicavel'
          'Caleiro'
          'Curtimento'
          'Recurtimento')
        TabOrder = 4
        QryCampo = 'TpSetor'
        UpdCampo = 'TpSetor'
        UpdType = utYes
        OldValor = 0
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 56
        Width = 465
        Height = 65
        Caption = 'Produ'#231#227'o m'#225'xima por dia trabalhado:'
        TabOrder = 5
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 461
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label5: TLabel
            Left = 11
            Top = 4
            Width = 126
            Height = 13
            Caption = 'Produ'#231#227'o m'#225'xima m'#178' / dia:'
          end
          object Label8: TLabel
            Left = 151
            Top = 4
            Width = 134
            Height = 13
            Caption = 'Produ'#231#227'o m'#225'x. couros / dia:'
          end
          object Label10: TLabel
            Left = 291
            Top = 4
            Width = 128
            Height = 13
            Caption = 'Produ'#231#227'o m'#225'xima Kg / dia:'
          end
          object Edm2Dia: TdmkEdit
            Left = 11
            Top = 20
            Width = 134
            Height = 21
            Alignment = taRightJustify
            CharCase = ecUpperCase
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'm2Dia'
            UpdCampo = 'm2Dia'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCourosDia: TdmkEdit
            Left = 151
            Top = 20
            Width = 134
            Height = 21
            Alignment = taRightJustify
            CharCase = ecUpperCase
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'CourosDia'
            UpdCampo = 'CourosDia'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdKgDia: TdmkEdit
            Left = 291
            Top = 20
            Width = 134
            Height = 21
            Alignment = taRightJustify
            CharCase = ecUpperCase
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'KgDia'
            UpdCampo = 'KgDia'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
      object EdOperPosProcDescr: TdmkEdit
        Left = 16
        Top = 244
        Width = 465
        Height = 21
        MaxLength = 60
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'OperPosProcDescr'
        UpdCampo = 'OperPosProcDescr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 316
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 278
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 251
        Height = 32
        Caption = 'Cadastro de Setores'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 251
        Height = 32
        Caption = 'Cadastro de Setores'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 251
        Height = 32
        Caption = 'Cadastro de Setores'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrListaSetores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM listasetores'
      'WHERE Codigo > 0')
    Left = 64
    Top = 64
    object QrListaSetoresLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrListaSetoresDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrListaSetoresDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrListaSetoresUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrListaSetoresUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrListaSetoresCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object QrListaSetoresNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrListaSetoresSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrListaSetoresm2Dia: TFloatField
      FieldName = 'm2Dia'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrListaSetoresTpReceita: TSmallintField
      FieldName = 'TpReceita'
    end
    object QrListaSetoresTpSetor: TSmallintField
      FieldName = 'TpSetor'
    end
    object QrListaSetoresCourosDia: TFloatField
      FieldName = 'CourosDia'
    end
    object QrListaSetoresKgDia: TFloatField
      FieldName = 'KgDia'
    end
    object QrListaSetoresOperPosProcDescr: TWideStringField
      FieldName = 'OperPosProcDescr'
      Size = 60
    end
  end
  object DsListaSetores: TDataSource
    DataSet = QrListaSetores
    Left = 92
    Top = 64
  end
end
