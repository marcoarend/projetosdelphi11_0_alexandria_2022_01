object FmFormulaV: TFmFormulaV
  Left = 368
  Top = 194
  Caption = 'QUI-RECEI-201 :: Receita - Vers'#227'o Obsoleta'
  ClientHeight = 598
  ClientWidth = 1068
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 52
    Width = 1068
    Height = 546
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1068
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 483
      Width = 1068
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 929
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 52
    Width = 1068
    Height = 546
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 482
      Width = 1068
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 372
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 546
        Top = 15
        Width = 520
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 22
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 21
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Panel2: TPanel
          Left = 387
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 1068
      Height = 85
      Align = alTop
      TabOrder = 1
      object Label16: TLabel
        Left = 6
        Top = 4
        Width = 40
        Height = 13
        Caption = 'N'#250'mero:'
        FocusControl = DBEdit12
      end
      object Label18: TLabel
        Left = 69
        Top = 4
        Width = 36
        Height = 13
        Caption = 'Vers'#227'o:'
      end
      object Label17: TLabel
        Left = 121
        Top = 4
        Width = 39
        Height = 13
        Caption = 'Cria'#231#227'o:'
        FocusControl = DBEdit13
      end
      object Label30: TLabel
        Left = 185
        Top = 4
        Width = 81
        Height = 13
        Caption = 'Nome da receita:'
        FocusControl = DBEdit26
      end
      object Label2: TLabel
        Left = 6
        Top = 44
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label1: TLabel
        Left = 71
        Top = 44
        Width = 27
        Height = 13
        Caption = 'Peso:'
      end
      object Label24: TLabel
        Left = 162
        Top = 44
        Width = 72
        Height = 13
        Caption = 'Tempo parado:'
        FocusControl = DBEdit20
      end
      object Label25: TLabel
        Left = 270
        Top = 44
        Width = 101
        Height = 13
        Caption = 'Tempo em opera'#231#227'o:'
        FocusControl = DBEdit21
      end
      object Label26: TLabel
        Left = 378
        Top = 44
        Width = 59
        Height = 13
        Caption = 'Tempo total:'
        FocusControl = DBEdit22
      end
      object Label4: TLabel
        Left = 570
        Top = 44
        Width = 166
        Height = 13
        Caption = 'Cliente interno (Dono dos insumos):'
      end
      object Label23: TLabel
        Left = 856
        Top = 44
        Width = 102
        Height = 13
        Caption = 'T'#233'cnico respons'#225'vel:'
        FocusControl = DBEdit19
      end
      object Label6: TLabel
        Left = 667
        Top = 4
        Width = 42
        Height = 13
        Caption = 'Rebaixe:'
      end
      object Label8: TLabel
        Left = 767
        Top = 4
        Width = 52
        Height = 13
        Caption = 'Espessura:'
      end
      object Label22: TLabel
        Left = 883
        Top = 4
        Width = 28
        Height = 13
        Caption = 'Setor:'
      end
      object Label10: TLabel
        Left = 454
        Top = 4
        Width = 122
        Height = 13
        Caption = 'Cor padr'#227'o (recurtimento):'
      end
      object DBEdit12: TDBEdit
        Left = 6
        Top = 20
        Width = 59
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'NumCODIF'
        DataSource = DsFormulaV
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object DBEdit4: TDBEdit
        Left = 70
        Top = 20
        Width = 39
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'Versao'
        DataSource = DsFormulaV
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
      object DBEdit13: TDBEdit
        Left = 121
        Top = 20
        Width = 60
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'DataCad'
        DataSource = DsFormulaV
        ReadOnly = True
        TabOrder = 2
      end
      object DBEdit26: TDBEdit
        Left = 185
        Top = 20
        Width = 264
        Height = 21
        DataField = 'Nome'
        DataSource = DsFormulaV
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 6
        Top = 60
        Width = 61
        Height = 21
        DataField = 'Qtde'
        DataSource = DsFormulaV
        TabOrder = 4
      end
      object DBEdit1: TDBEdit
        Left = 71
        Top = 60
        Width = 86
        Height = 21
        DataField = 'Peso'
        DataSource = DsFormulaV
        TabOrder = 5
      end
      object DBEdit20: TDBEdit
        Left = 162
        Top = 60
        Width = 107
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'HHMM_P'
        DataSource = DsFormulaV
        ReadOnly = True
        TabOrder = 6
      end
      object DBEdit21: TDBEdit
        Left = 270
        Top = 60
        Width = 108
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'HHMM_R'
        DataSource = DsFormulaV
        ReadOnly = True
        TabOrder = 7
      end
      object DBEdit22: TDBEdit
        Left = 378
        Top = 60
        Width = 108
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'HHMM_T'
        DataSource = DsFormulaV
        ReadOnly = True
        TabOrder = 8
      end
      object EdDBClienteI: TDBEdit
        Left = 570
        Top = 60
        Width = 35
        Height = 21
        DataField = 'ClienteI'
        TabOrder = 9
      end
      object DBEdit19: TDBEdit
        Left = 856
        Top = 60
        Width = 137
        Height = 21
        DataField = 'Tecnico'
        DataSource = DsFormulaV
        TabOrder = 10
      end
      object EdDBGraCorCad: TDBEdit
        Left = 454
        Top = 20
        Width = 40
        Height = 21
        DataField = 'GraCorCad'
        DataSource = DsFormulaV
        TabOrder = 11
      end
      object DBEdit6: TDBEdit
        Left = 924
        Top = 20
        Width = 132
        Height = 21
        DataField = 'NO_SETOR'
        DataSource = DsFormulaV
        TabOrder = 12
      end
      object DBEdit7: TDBEdit
        Left = 884
        Top = 20
        Width = 40
        Height = 21
        DataField = 'Setor'
        DataSource = DsFormulaV
        TabOrder = 13
      end
      object DBEdit8: TDBEdit
        Left = 768
        Top = 20
        Width = 40
        Height = 21
        DataField = 'Espessura'
        DataSource = DsFormulaV
        TabOrder = 14
      end
      object DBEdit9: TDBEdit
        Left = 668
        Top = 20
        Width = 40
        Height = 21
        DataField = 'EspRebaixe'
        DataSource = DsFormulaV
        TabOrder = 15
      end
      object DBEdit10: TDBEdit
        Left = 708
        Top = 20
        Width = 57
        Height = 21
        DataField = 'LinhasReb'
        DataSource = DsFormulaV
        TabOrder = 16
      end
      object DBEdit11: TDBEdit
        Left = 808
        Top = 20
        Width = 73
        Height = 21
        DataField = 'LinhasSemi'
        DataSource = DsFormulaV
        TabOrder = 17
      end
      object DBEdit14: TDBEdit
        Left = 496
        Top = 20
        Width = 169
        Height = 21
        DataField = 'NO_GraCorCad'
        DataSource = DsFormulaV
        TabOrder = 18
      end
      object DBEdit15: TDBEdit
        Left = 604
        Top = 60
        Width = 249
        Height = 21
        DataField = 'NO_ClienteI'
        DataSource = DsFormulaV
        TabOrder = 19
      end
    end
    object DBGrid2: TDBGrid
      Left = 0
      Top = 85
      Width = 77
      Height = 397
      Align = alLeft
      DataSource = DsFormulaV
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Versao'
          Title.Caption = 'Vers'#227'o'
          Width = 39
          Visible = True
        end>
    end
    object PCVerSel: TPageControl
      Left = 77
      Top = 85
      Width = 991
      Height = 397
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 3
      object TabSheet1: TTabSheet
        Caption = ' Receita '
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 983
          Height = 369
          Align = alClient
          Color = clWhite
          DataSource = DsFormulaVIts
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Title.Caption = 'Seq.'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Processo'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Porcent'
              Title.Alignment = taRightJustify
              Title.Caption = ' % de uso'
              Width = 54
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Produto'
              Title.Alignment = taRightJustify
              Title.Caption = 'PQ [F4]'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPQ'
              Title.Caption = 'Descri'#231#227'o (Produto ou Texto) [F7]'
              Width = 170
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Boca'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -21
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'B?'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Diluicao'
              Title.Alignment = taRightJustify
              Title.Caption = 'Dil.1:x'
              Width = 38
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Graus'
              Title.Alignment = taRightJustify
              Title.Caption = #186' C'
              Width = 20
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'TempoR'
              Title.Alignment = taRightJustify
              Title.Caption = 'Roda'
              Width = 34
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'TempoP'
              Title.Alignment = taRightJustify
              Title.Caption = 'P'#225'ra'
              Width = 35
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'pH_Min'
              Title.Alignment = taRightJustify
              Title.Caption = 'pH min'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'pH_Max'
              Title.Caption = 'pH m'#225'x'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Be_Min'
              Title.Alignment = taRightJustify
              Title.Caption = #186' B'#233' min'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Be_Max'
              Title.Caption = #186' B'#233' m'#225'x'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GC_Min'
              Title.Caption = #186'C Min'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GC_Max'
              Title.Caption = #186'C Max'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Obs'
              Title.Caption = 'Observa'#231#245'es'
              Width = 114
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ObsProces'
              Title.Caption = 'Observa'#231#227'o processso'
              Width = 214
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Minimo'
              Title.Caption = 'M'#237'n.'
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Maximo'
              Title.Caption = 'M'#225'x.'
              Width = 30
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Descri'#231#227'o das altera'#231#245'es'
        ImageIndex = 1
        object DBMemo1: TDBMemo
          Left = 0
          Top = 0
          Width = 983
          Height = 369
          Align = alClient
          DataField = 'HistAlter'
          DataSource = DsFormulaV
          TabOrder = 0
          ExplicitLeft = 76
          ExplicitTop = 44
          ExplicitWidth = 185
          ExplicitHeight = 89
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1068
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1020
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 804
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 319
        Height = 32
        Caption = 'Receita - Vers'#227'o Obsoleta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 319
        Height = 32
        Caption = 'Receita - Vers'#227'o Obsoleta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 319
        Height = 32
        Caption = 'Receita - Vers'#227'o Obsoleta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 65532
    Top = 65528
  end
  object QrFormulaVIts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFormulaVItsCalcFields
    SQL.Strings = (
      'SELECT pq_.Nome NOMEPQ, its.*'
      'FROM formulavits its'
      'LEFT JOIN pq pq_ ON pq_.Codigo=its.Produto'
      'ORDER BY Ordem, Reordem, Controle')
    Left = 136
    Top = 268
    object QrFormulaVItsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrFormulaVItsNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrFormulaVItsOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrFormulaVItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFormulaVItsReordem: TIntegerField
      FieldName = 'Reordem'
      Required = True
    end
    object QrFormulaVItsProcesso: TWideStringField
      FieldName = 'Processo'
      Size = 30
    end
    object QrFormulaVItsPorcent: TFloatField
      FieldName = 'Porcent'
      Required = True
    end
    object QrFormulaVItsProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrFormulaVItsTempoR: TIntegerField
      FieldName = 'TempoR'
      Required = True
    end
    object QrFormulaVItsTempoP: TIntegerField
      FieldName = 'TempoP'
      Required = True
    end
    object QrFormulaVItspH: TFloatField
      FieldName = 'pH'
      Required = True
    end
    object QrFormulaVItspH_Min: TFloatField
      FieldName = 'pH_Min'
      Required = True
    end
    object QrFormulaVItspH_Max: TFloatField
      FieldName = 'pH_Max'
      Required = True
    end
    object QrFormulaVItsBe: TFloatField
      FieldName = 'Be'
      Required = True
    end
    object QrFormulaVItsBe_Min: TFloatField
      FieldName = 'Be_Min'
      Required = True
    end
    object QrFormulaVItsBe_Max: TFloatField
      FieldName = 'Be_Max'
      Required = True
    end
    object QrFormulaVItsGC_Min: TFloatField
      FieldName = 'GC_Min'
      Required = True
    end
    object QrFormulaVItsGC_Max: TFloatField
      FieldName = 'GC_Max'
      Required = True
    end
    object QrFormulaVItsObs: TWideStringField
      FieldName = 'Obs'
    end
    object QrFormulaVItsObsProces: TWideStringField
      FieldName = 'ObsProces'
      Size = 100
    end
    object QrFormulaVItsSC: TIntegerField
      FieldName = 'SC'
      Required = True
    end
    object QrFormulaVItsReciclo: TIntegerField
      FieldName = 'Reciclo'
      Required = True
    end
    object QrFormulaVItsDiluicao: TFloatField
      FieldName = 'Diluicao'
      Required = True
    end
    object QrFormulaVItsCProd: TFloatField
      FieldName = 'CProd'
      Required = True
    end
    object QrFormulaVItsCSolu: TFloatField
      FieldName = 'CSolu'
      Required = True
    end
    object QrFormulaVItsCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrFormulaVItsMinimo: TFloatField
      FieldName = 'Minimo'
      Required = True
    end
    object QrFormulaVItsMaximo: TFloatField
      FieldName = 'Maximo'
      Required = True
    end
    object QrFormulaVItsGraus: TFloatField
      FieldName = 'Graus'
    end
    object QrFormulaVItsBoca: TWideStringField
      FieldName = 'Boca'
      Size = 1
    end
    object QrFormulaVItsEhGGX: TSmallintField
      FieldName = 'EhGGX'
      Required = True
    end
    object QrFormulaVItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrFormulaVItsDensidade: TFloatField
      FieldName = 'Densidade'
      Required = True
    end
    object QrFormulaVItsUsarDensid: TSmallintField
      FieldName = 'UsarDensid'
      Required = True
    end
    object QrFormulaVItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrFormulaVItsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrFormulaVItsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrFormulaVItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrFormulaVItsUserDel: TIntegerField
      FieldName = 'UserDel'
      Required = True
    end
    object QrFormulaVItsDataDel: TDateTimeField
      FieldName = 'DataDel'
      Required = True
    end
    object QrFormulaVItsMotvDel: TIntegerField
      FieldName = 'MotvDel'
      Required = True
    end
    object QrFormulaVItsVersao: TIntegerField
      FieldName = 'Versao'
      Required = True
    end
    object QrFormulaVItsVerOri: TIntegerField
      FieldName = 'VerOri'
      Required = True
    end
    object QrFormulaVItsSeq: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Seq'
      Calculated = True
    end
  end
  object DsFormulaVIts: TDataSource
    DataSet = QrFormulaVIts
    Left = 136
    Top = 316
  end
  object QrFormulaV: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFormulaVBeforeOpen
    AfterOpen = QrFormulaVAfterOpen
    BeforeClose = QrFormulaVBeforeClose
    AfterScroll = QrFormulaVAfterScroll
    OnCalcFields = QrFormulaVCalcFields
    SortFieldNames = 'Versao DESC'
    SQL.Strings = (
      'SELECT frv.*,'
      'lse.Nome NO_SETOR, gcc.Nome NO_GraCorCad,'
      'esr.Linhas LinhasReb, ess.Linhas LinhasSemi,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_ClienteI '
      'FROM formulav frv'
      'LEFT JOIN listasetores lse ON lse.Codigo=frv.Setor'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=frv.GraCorCad'
      'LEFT JOIN espessuras esr ON esr.Codigo=frv.Espessura'
      'LEFT JOIN espessuras ess ON ess.Codigo=frv.EspRebaixe'
      'LEFT JOIN entidades cli ON cli.Codigo=frv.ClienteI')
    Left = 40
    Top = 268
    object QrFormulaVNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'formulav.Numero'
      Required = True
    end
    object QrFormulaVNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'formulav.Nome'
      Size = 30
    end
    object QrFormulaVClienteI: TIntegerField
      FieldName = 'ClienteI'
      Origin = 'formulav.ClienteI'
      Required = True
    end
    object QrFormulaVTipificacao: TIntegerField
      FieldName = 'Tipificacao'
      Origin = 'formulav.Tipificacao'
      Required = True
    end
    object QrFormulaVDataI: TDateField
      FieldName = 'DataI'
      Origin = 'formulav.DataI'
    end
    object QrFormulaVDataA: TDateField
      FieldName = 'DataA'
      Origin = 'formulav.DataA'
    end
    object QrFormulaVTecnico: TWideStringField
      FieldName = 'Tecnico'
      Origin = 'formulav.Tecnico'
    end
    object QrFormulaVTempoR: TIntegerField
      FieldName = 'TempoR'
      Origin = 'formulav.TempoR'
    end
    object QrFormulaVTempoP: TIntegerField
      FieldName = 'TempoP'
      Origin = 'formulav.TempoP'
    end
    object QrFormulaVTempoT: TIntegerField
      FieldName = 'TempoT'
      Origin = 'formulav.TempoT'
    end
    object QrFormulaVHorasR: TIntegerField
      FieldName = 'HorasR'
      Origin = 'formulav.HorasR'
    end
    object QrFormulaVHorasP: TIntegerField
      FieldName = 'HorasP'
      Origin = 'formulav.HorasP'
    end
    object QrFormulaVHorasT: TIntegerField
      FieldName = 'HorasT'
      Origin = 'formulav.HorasT'
    end
    object QrFormulaVHidrica: TIntegerField
      FieldName = 'Hidrica'
      Origin = 'formulav.Hidrica'
    end
    object QrFormulaVLinhaTE: TIntegerField
      FieldName = 'LinhaTE'
      Origin = 'formulav.LinhaTE'
    end
    object QrFormulaVCaldeira: TIntegerField
      FieldName = 'Caldeira'
      Origin = 'formulav.Caldeira'
    end
    object QrFormulaVSetor: TIntegerField
      FieldName = 'Setor'
      Origin = 'formulav.Setor'
    end
    object QrFormulaVEspessura: TIntegerField
      FieldName = 'Espessura'
      Origin = 'formulav.Espessura'
    end
    object QrFormulaVPeso: TFloatField
      FieldName = 'Peso'
      Origin = 'formulav.Peso'
    end
    object QrFormulaVQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'formulav.Qtde'
    end
    object QrFormulaVGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'formulav.Grupo'
      Required = True
    end
    object QrFormulaVVersao: TIntegerField
      FieldName = 'Versao'
      Origin = 'formulav.Versao'
      Required = True
    end
    object QrFormulaVEhGGX: TSmallintField
      FieldName = 'EhGGX'
      Origin = 'formulav.EhGGX'
      Required = True
    end
    object QrFormulaVLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'formulav.Lk'
    end
    object QrFormulaVDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'formulav.DataCad'
    end
    object QrFormulaVDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'formulav.DataAlt'
    end
    object QrFormulaVUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'formulav.UserCad'
    end
    object QrFormulaVUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'formulav.UserAlt'
    end
    object QrFormulaVAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'formulav.AlterWeb'
      Required = True
    end
    object QrFormulaVAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'formulav.Ativo'
      Required = True
    end
    object QrFormulaVRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
      Origin = 'formulav.Retrabalho'
      Required = True
    end
    object QrFormulaVBxaEstqVS: TSmallintField
      FieldName = 'BxaEstqVS'
      Origin = 'formulav.BxaEstqVS'
      Required = True
    end
    object QrFormulaVGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'formulav.GraGruX'
      Required = True
    end
    object QrFormulaVGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'formulav.GraCorCad'
      Required = True
    end
    object QrFormulaVEspRebaixe: TIntegerField
      FieldName = 'EspRebaixe'
      Origin = 'formulav.EspRebaixe'
    end
    object QrFormulaVAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Origin = 'formulav.AWServerID'
      Required = True
    end
    object QrFormulaVAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Origin = 'formulav.AWStatSinc'
      Required = True
    end
    object QrFormulaVFormulasGru: TIntegerField
      FieldName = 'FormulasGru'
      Origin = 'formulav.FormulasGru'
      Required = True
    end
    object QrFormulaVHistAlter: TWideMemoField
      FieldName = 'HistAlter'
      Origin = 'formulav.HistAlter'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFormulaVUserDel: TIntegerField
      FieldName = 'UserDel'
      Origin = 'formulav.UserDel'
      Required = True
    end
    object QrFormulaVDataDel: TDateTimeField
      FieldName = 'DataDel'
      Origin = 'formulav.DataDel'
      Required = True
    end
    object QrFormulaVMotvDel: TIntegerField
      FieldName = 'MotvDel'
      Origin = 'formulav.MotvDel'
      Required = True
    end
    object QrFormulaVNO_SETOR: TWideStringField
      FieldName = 'NO_SETOR'
      Origin = 'listasetores.Nome'
    end
    object QrFormulaVNO_GraCorCad: TWideStringField
      FieldName = 'NO_GraCorCad'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrFormulaVLinhasReb: TWideStringField
      FieldName = 'LinhasReb'
      Origin = 'espessuras.Linhas'
      Size = 7
    end
    object QrFormulaVLinhasSemi: TWideStringField
      FieldName = 'LinhasSemi'
      Origin = 'espessuras.Linhas'
      Size = 7
    end
    object QrFormulaVHHMM_P: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_P'
      Size = 10
      Calculated = True
    end
    object QrFormulaVHHMM_R: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_R'
      Size = 10
      Calculated = True
    end
    object QrFormulaVHHMM_T: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_T'
      Size = 10
      Calculated = True
    end
    object QrFormulaVMEDIAPESO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MEDIAPESO'
      DisplayFormat = '#,###,##0.0'
      Calculated = True
    end
    object QrFormulaVNumCODIF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NumCODIF'
      Size = 11
      Calculated = True
    end
    object QrFormulaVNO_ClienteI: TWideStringField
      FieldName = 'NO_ClienteI'
      Size = 100
    end
  end
  object DsFormulaV: TDataSource
    DataSet = QrFormulaV
    Left = 40
    Top = 316
  end
  object frxDsFormulaV: TfrxDBDataset
    UserName = 'frxDsFormulaV'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Numero=Numero'
      'Nome=Nome'
      'ClienteI=ClienteI'
      'Tipificacao=Tipificacao'
      'DataI=DataI'
      'DataA=DataA'
      'Tecnico=Tecnico'
      'TempoR=TempoR'
      'TempoP=TempoP'
      'TempoT=TempoT'
      'HorasR=HorasR'
      'HorasP=HorasP'
      'HorasT=HorasT'
      'Hidrica=Hidrica'
      'LinhaTE=LinhaTE'
      'Caldeira=Caldeira'
      'Setor=Setor'
      'Espessura=Espessura'
      'Peso=Peso'
      'Qtde=Qtde'
      'Grupo=Grupo'
      'Versao=Versao'
      'EhGGX=EhGGX'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Retrabalho=Retrabalho'
      'BxaEstqVS=BxaEstqVS'
      'GraGruX=GraGruX'
      'GraCorCad=GraCorCad'
      'EspRebaixe=EspRebaixe'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'FormulasGru=FormulasGru'
      'HistAlter=HistAlter'
      'UserDel=UserDel'
      'DataDel=DataDel'
      'MotvDel=MotvDel'
      'NO_SETOR=NO_SETOR'
      'NO_GraCorCad=NO_GraCorCad'
      'LinhasReb=LinhasReb'
      'LinhasSemi=LinhasSemi'
      'HHMM_P=HHMM_P'
      'HHMM_R=HHMM_R'
      'HHMM_T=HHMM_T'
      'MEDIAPESO=MEDIAPESO'
      'NumCODIF=NumCODIF'
      'NO_ClienteI=NO_ClienteI')
    DataSet = QrFormulaV
    BCDToCurrency = False
    DataSetOptions = []
    Left = 40
    Top = 368
  end
  object frxDsFormulaVIts: TfrxDBDataset
    UserName = 'frxDsFormulaVIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEPQ=NOMEPQ'
      'Numero=Numero'
      'Ordem=Ordem'
      'Controle=Controle'
      'Reordem=Reordem'
      'Processo=Processo'
      'Porcent=Porcent'
      'Produto=Produto'
      'TempoR=TempoR'
      'TempoP=TempoP'
      'pH=pH'
      'pH_Min=pH_Min'
      'pH_Max=pH_Max'
      'Be=Be'
      'Be_Min=Be_Min'
      'Be_Max=Be_Max'
      'GC_Min=GC_Min'
      'GC_Max=GC_Max'
      'Obs=Obs'
      'ObsProces=ObsProces'
      'SC=SC'
      'Reciclo=Reciclo'
      'Diluicao=Diluicao'
      'CProd=CProd'
      'CSolu=CSolu'
      'Custo=Custo'
      'Minimo=Minimo'
      'Maximo=Maximo'
      'Graus=Graus'
      'Boca=Boca'
      'EhGGX=EhGGX'
      'GraGruX=GraGruX'
      'Densidade=Densidade'
      'UsarDensid=UsarDensid'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'UserDel=UserDel'
      'DataDel=DataDel'
      'MotvDel=MotvDel'
      'Versao=Versao'
      'VerOri=VerOri'
      'Seq=Seq')
    DataSet = QrFormulaVIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 136
    Top = 368
  end
  object frxQUI_RECEI_201_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38017.353737407400000000
    ReportOptions.LastChange = 41853.389075254600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 260
    Top = 276
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsFormulaV
        DataSetName = 'frxDsFormulaV'
      end
      item
        DataSet = frxDsFormulaVIts
        DataSetName = 'frxDsFormulaVIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      object Band1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.385826770000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsFormulaVIts
        DataSetName = 'frxDsFormulaVIts'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 657.637761100000000000
          Width = 22.677165350000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulaVIts."Seq"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913327240000000000
          Width = 30.236220470000000000
          Height = 17.385826770000000000
          DataSet = frxDsFormulaVIts
          DataSetName = 'frxDsFormulaVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulaVIts."Produto' +
              '">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149547720000000000
          Width = 181.417322830000000000
          Height = 17.385826770000000000
          DataSet = frxDsFormulaVIts
          DataSetName = 'frxDsFormulaVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFormulaVIts."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685005200000000000
          Width = 18.897637800000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulaVIts."Diluica' +
              'o">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582642990000000000
          Width = 18.897637800000000000
          Height = 17.385826770000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '##0; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulaVIts."Graus"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480280790000000000
          Width = 26.456692910000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulaVIts."TempoR"' +
              '>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 343.936973700000000000
          Width = 26.456692910000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulaVIts."TempoP"' +
              '>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393656850000000000
          Width = 22.677165350000000000
          Height = 17.385826770000000000
          DataSet = frxDsFormulaVIts
          DataSetName = 'frxDsFormulaVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39',<frxDsFormulaVIts."p' +
              'H_Min">)]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748016850000000000
          Width = 22.677165350000000000
          Height = 17.385826770000000000
          DataSet = frxDsFormulaVIts
          DataSetName = 'frxDsFormulaVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39',<frxDsFormulaVIts."B' +
              'e_Min">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102330470000000000
          Width = 136.062953070000000000
          Height = 17.385826770000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFormulaVIts."Obs"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = -0.000058580000000000
          Width = 52.913385830000000000
          Height = 17.385826770000000000
          DataField = 'Porcent'
          DataSet = frxDsFormulaVIts
          DataSetName = 'frxDsFormulaVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,##0.000; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulaVIts."Porcent"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 264.566894960000000000
          Width = 15.118110240000000000
          Height = 17.385826770000000000
          DataField = 'Boca'
          DataSet = frxDsFormulaVIts
          DataSetName = 'frxDsFormulaVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulaVIts."Boca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Width = 22.677165350000000000
          Height = 17.385826770000000000
          DataSet = frxDsFormulaVIts
          DataSetName = 'frxDsFormulaVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39',<frxDsFormulaVIts."p' +
              'H_Max">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Width = 22.677165350000000000
          Height = 17.385826770000000000
          DataSet = frxDsFormulaVIts
          DataSetName = 'frxDsFormulaVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39',<frxDsFormulaVIts."B' +
              'e_Max">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 30.236220470000000000
          Height = 17.385826770000000000
          DataSet = frxDsFormulaVIts
          DataSetName = 'frxDsFormulaVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39',<frxDsFormulaVIts.' +
              '"Minimo">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 627.402263150000000000
          Width = 30.236220470000000000
          Height = 17.385826770000000000
          DataSet = frxDsFormulaVIts
          DataSetName = 'frxDsFormulaVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39',<frxDsFormulaVIts.' +
              '"Maximo">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object Band3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007874020000000000
        Top = 192.756030000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149547720000000000
          Width = 181.417322834646000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Nome do produto ou texto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685005200000000000
          Width = 18.897637800000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '1:x')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582642990000000000
          Width = 18.897637800000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #186'C')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480280790000000000
          Width = 26.456692910000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Roda')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 343.936973700000000000
          Width = 26.456692910000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'ra')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393656850000000000
          Width = 45.354330710000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'pH')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748016850000000000
          Width = 45.354330710000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'B'#233)
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102330470000000000
          Width = 136.062953070000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913327240000000000
          Width = 30.236220470000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd.')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = -0.000058580000000000
          Width = 52.913385830000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Uso %')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 657.637761100000000000
          Width = 22.677165350000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 264.566894960000000000
          Width = 15.118110240000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'B')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Interv. kg/p'#231)
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677165350000000000
        Top = 313.700990000000000000
        Width = 680.315400000000000000
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 17.007876460000000000
          DataSet = frxDsFormulaVIts
          DataSetName = 'frxDsFormulaVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Width = 178.708720000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 636.031850000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 69.921230550000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 680.314960629921300000
          Height = 64.252010000000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 0.157700000000000000
          Top = 22.677180000000000000
          Width = 60.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita de ')
          ParentFont = False
          WordWrap = False
        end
        object meSetor: TfrxMemoView
          AllowVectorExport = True
          Left = 63.937230000000000000
          Top = 22.677180000000000000
          Width = 95.779530000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulaV."NO_SETOR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 0.157700000000000000
          Top = 41.574815350000000000
          Width = 44.000000000000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object meCodReceita: TfrxMemoView
          AllowVectorExport = True
          Left = 44.157700000000000000
          Top = 41.574815350000000000
          Width = 631.181173150000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            
              '[frxDsFormulaV."NumCODIF"] v.[frxDsFormulaV."Versao"] - [frxDsFo' +
              'rmulaV."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 159.716760000000000000
          Top = 22.677180000000000000
          Width = 68.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 227.716760000000000000
          Top = 22.677180000000000000
          Width = 112.220470000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulaV."Tecnico"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Top = 3.834570000000010000
          Width = 567.779530000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 449.764070000000000000
          Top = 22.677180000000000000
          Width = 53.102350000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' Rebaixe:')
          ParentFont = False
          WordWrap = False
        end
        object meEspessura: TfrxMemoView
          AllowVectorExport = True
          Left = 503.086890000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulaV."LinhasReb"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000000000
          Top = 22.677180000000000000
          Width = 53.102350000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' Esp.Final:')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 616.472790000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulaV."LinhasSemi"]')
          ParentFont = False
          WordWrap = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 151.181200000000000000
        Width = 680.315400000000000000
        RowCount = 1
        Stretched = True
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataField = 'HistAlter'
          DataSet = frxDsFormulaV
          DataSetName = 'frxDsFormulaV'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFormulaV."HistAlter"]')
          ParentFont = False
        end
      end
    end
  end
  object PMImprime: TPopupMenu
    Left = 81
    Top = 9
    object Imprimeaversoselecionada1: TMenuItem
      Caption = 'A vers'#227'o selecionada'
      OnClick = Imprimeaversoselecionada1Click
    end
    object Listadasdescries1: TMenuItem
      Caption = 'Lista das descri'#231#245'es das altera'#231#245'es'
      OnClick = Listadasdescries1Click
    end
  end
  object frxQUI_RECEI_201_B: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38017.353737407400000000
    ReportOptions.LastChange = 41853.389075254600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 260
    Top = 328
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsFormulaV
        DataSetName = 'frxDsFormulaV'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677165350000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 17.007876460000000000
          DataSet = frxDsFormulaVIts
          DataSetName = 'frxDsFormulaVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Width = 178.708720000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 636.031850000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 124.724490000000000000
          Top = 18.897650000000000000
          Width = 408.189240000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DAS DESCRI'#199#213'ES DAS ALTERA'#199#213'ES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 18.897650000000000000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 117.165430000000000000
        Width = 680.315400000000000000
        DataSet = frxDsFormulaV
        DataSetName = 'frxDsFormulaV'
        RowCount = 0
        Stretched = True
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataSet = frxDsFormulaV
          DataSetName = 'frxDsFormulaV'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFormulaV."HistAlter"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object meCodReceita: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354330710000000000
          Top = 3.779530000000000000
          Width = 430.866083150000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              '[frxDsFormulaV."NumCODIF"] v.[frxDsFormulaV."Versao"] - [frxDsFo' +
              'rmulaV."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Top = 3.779530000000000000
          Width = 90.708661420000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data do cadastro:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929231500000000000
          Top = 3.779530000000000000
          Width = 113.385729130000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[<frxDsFormulaV."DataCad">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
end
