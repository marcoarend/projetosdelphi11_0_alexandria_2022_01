unit FormulasImpOSs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkEditCalc, UnDmkEnums;

type
  TFmFormulasImpOSs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    QrMPVIts: TmySQLQuery;
    DsMPVIts: TDataSource;
    DBGMPVIts: TDBGrid;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_ENT: TWideStringField;
    DsClientes: TDataSource;
    Panel5: TPanel;
    RGFiltroOSs: TRadioGroup;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    Panel6: TPanel;
    Panel7: TPanel;
    Label2: TLabel;
    EdMPVIts: TdmkEdit;
    PnMedidas: TPanel;
    Label3: TLabel;
    EdPecas: TdmkEdit;
    Label4: TLabel;
    EdPeso: TdmkEdit;
    Label5: TLabel;
    EdAreaM2: TdmkEditCalc;
    Label6: TLabel;
    EdAreaP2: TdmkEditCalc;
    BtOK: TBitBtn;
    QrMPVItsControle: TIntegerField;
    QrMPVItsM2Pedido: TFloatField;
    QrMPVItsNOME_OS: TWideStringField;
    Label7: TLabel;
    EdUniOS: TdmkEdit;
    Label8: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure RGFiltroOSsClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdMPVItsChange(Sender: TObject);
    procedure DBGMPVItsDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FEmit: Boolean;
    F_EmitCus: String;
    FCodigo, FControle: Integer;
    FQryExe, FQryLote: TmySQLQuery;
    FTipoReceitas: TTipoReceitas;
    //
    procedure ReopenMPVIts(Controle: Integer);
    procedure FechaPesquisa();
    procedure ReopenQry();
  end;

  var
  FmFormulasImpOSs: TFmFormulasImpOSs;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmFormulasImpOSs.BitBtn1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if QrMPVIts.State <> dsInactive then
    Controle := QrMPVItsControle.Value
  else
    Controle := 0;
  //
  ReopenMPVIts(Controle);
end;

procedure TFmFormulasImpOSs.BtOKClick(Sender: TObject);
var
  DataEmis: String;
  Codigo, Controle, MPIn, Formula, MPVIts: Integer;
  Peso, Custo, Pecas, AreaM2, AreaP2: Double;
begin
  Codigo         := FCodigo;
  Controle       := FControle;
  MPIn           := 0;
  Formula        := 0;            // Ser� definida FmFormulasImpWE.BtConfirmaClick()
  DataEmis       := '0000-00-00'; // Ser� definida FmFormulasImpWE.BtConfirmaClick()
  Peso           := EdPeso.ValueVariant;
  Custo          := 0;            // Ser� definido em DmModEmit.AtualizaCustosEmit()
  Pecas          := EdPecas.ValueVariant;
  MPVIts         := EdMPVIts.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  //
  if MyObjects.FIC(Pecas < 0.1, EdPecas, 'Informe a quantidade de pe�as!') then
    Exit;
  case FTipoReceitas of
    dmktfrmSetrEmi_MOLHADO:
      if MyObjects.FIC(Peso < 0.001, EdPeso, 'Informe o peso!') then
        Exit;
    dmktfrmSetrEmi_ACABATO:
      if MyObjects.FIC(AreaM2 < 0.01, EdAreaM2, 'Informe a �rea!') then
        Exit;
    //dmktfrmSetrEmi_INDEFIN
    else
    begin
      Geral.MB_Erro('Tipo de emiss�o setorial indefinido!');
      Exit;
    end;
  end;

  //
  if FEmit then
    Controle := UMyMod.BuscaEmLivreY_Def('emitcus', 'Controle', stIns, Controle);
  if UMyMod.SQLInsUpd(FQryExe, ImgTipo.SQLType, F_EmitCus, False, [
  'Codigo', 'MPIn', 'Formula',
  'DataEmis', 'Peso', 'Custo',
  'Pecas', 'MPVIts', 'AreaM2',
  'AreaP2'], [
  'Controle'], [
  Codigo, MPIn, Formula,
  DataEmis, Peso, Custo,
  Pecas, MPVIts, AreaM2,
  AreaP2], [
  Controle], True) then
  begin
    FControle := Controle;
    ReopenQry();
    Close;
  end;
end;

procedure TFmFormulasImpOSs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulasImpOSs.DBGMPVItsDblClick(Sender: TObject);
begin
  if (QrMPVIts.State <> dsInactive) and (QrMPVIts.RecordCount > 0) then
    EdMPVIts.ValueVariant := QrMPVItsControle.Value;
end;

procedure TFmFormulasImpOSs.EdClienteChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmFormulasImpOSs.EdMPVItsChange(Sender: TObject);
begin
  PnMedidas.Visible := EdMPVIts.ValueVariant <> 0;
  if PnMedidas.Visible then
    EdPecas.SetFocus;
end;

procedure TFmFormulasImpOSs.FechaPesquisa();
begin
  QrMPVIts.Close;
end;

procedure TFmFormulasImpOSs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormulasImpOSs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrClientes, Dmod.MyDB, [
  'SELECT DISTINCT ent.Codigo,  ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  'FROM entidades ent ',
  'ORDER BY NO_ENT ',
  '']);
  //
  ReopenMPVIts(0);
end;

procedure TFmFormulasImpOSs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulasImpOSs.ReopenMPVIts(Controle: Integer);
var
  SQL_All, SQL_Cli, SQL_Fat: String;
  Cliente, UniOS: Integer;
begin
  Cliente := EdCliente.ValueVariant;
  UniOS := EdUniOS.ValueVariant;
  if UniOS <> 0 then
  begin
    SQL_All := 'AND mpi.Controle=' + Geral.FF0(UniOS);
    SQL_Cli := '';
    SQL_Fat := '';
  end else
  begin
    SQL_All := '';
    SQL_Cli := Geral.ATS_If(Cliente <> 0, [
      'AND mpp.Cliente=' + Geral.FF0(Cliente)]);
    case RGFiltroOSs.ItemIndex of
      0: SQL_Fat := 'AND mpi.Codigo=0 ';
      1: SQL_Fat := '';
      else SQL_Fat := 'AND mpi.Codigo=**ERRO**';
    end;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMPVIts, Dmod.MyDB, [
  'SELECT mpi.Controle, M2Pedido, ',
  'CONCAT(mpi.Texto, " ", mpi.CorTxt, " >>> ", ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NOME_OS ',
  'FROM mpvits mpi ',
  'LEFT JOIN mpp mpp ON mpp.Codigo=mpi.Pedido ',
  'LEFT JOIN entidades ent ON ent.Codigo=mpp.Cliente ',
  'WHERE mpi.Pedido>0 ',
  SQL_All,
  SQL_Fat,
  SQL_Cli,
  'ORDER BY mpi.Controle ',
  '']);
  //
  QrMPVIts.Locate('Controle', Controle, []);
end;

procedure TFmFormulasImpOSs.ReopenQry;
begin
  if FQryLote <> nil then
  begin
    FQryLote.Close;
    UnDmkDAC_PF.AbreQuery(FQryLote, FQryLote.Database);
    FQryLote.Last;
  end;
end;

procedure TFmFormulasImpOSs.RGFiltroOSsClick(Sender: TObject);
begin
  FechaPesquisa();
end;

end.
