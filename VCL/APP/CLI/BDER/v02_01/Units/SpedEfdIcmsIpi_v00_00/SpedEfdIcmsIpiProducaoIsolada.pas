u n i t   S p e d E f d I c m s I p i P r o d u c a o I s o l a d a ;  
  
 i n t e r f a c e  
  
 u s e s  
     W i n a p i . W i n d o w s ,   W i n a p i . M e s s a g e s ,   S y s t e m . S y s U t i l s ,   S y s t e m . V a r i a n t s ,  
     S y s t e m . C l a s s e s ,   V c l . G r a p h i c s ,   V c l . C o n t r o l s ,   V c l . F o r m s ,   V c l . D i a l o g s ,   D a t a . D B ,  
     m y S Q L D b T a b l e s ,  
     / /  
     S t d C t r l s ,   E x t C t r l s ,   M e n u s ,   U n I n t e r n a l C o n s t s 2 ,   C o m C t r l s ,   R e g i s t r y ,   P r i n t e r s ,  
     C o m m C t r l ,   C o n s t s ,   U n I n t e r n a l C o n s t s ,   Z C F 2 ,   S t r U t i l s ,   d m k G e r a l ,  
     U n D m k E n u m s ,   U n M s g I n t ,   D b T a b l e s ,   D m k C o d i n g ,   d m k E d i t ,  
     d m k R a d i o G r o u p ,   d m k M e m o ,   A d v T o o l B a r ,   d m k C h e c k G r o u p ,   U n M y O b j e c t s ,   M y D B C h e c k ,  
     U n D m k P r o c F u n c ,   U n P r o j G r o u p _ C o n s t s ,   A p p L i s t a s ,  
     d m k L a b e l ,   d m k I m a g e ,  
     D B C t r l s ,  
     d m k D B L o o k u p C o m b o B o x ,   d m k E d i t C B ,   d m k E d i t D a t e T i m e P i c k e r ,  
     d m k D B G r i d Z T O ,   S P E D _ L i s t a s ,   T y p I n f o ,   U n V S _ E F D _ I C M S _ I P I ;  
  
 t y p e  
     T F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a   =   c l a s s ( T F o r m )  
         Q r D a t a s :   T m y S Q L Q u e r y ;  
         Q r D a t a s M i n D H :   T D a t e T i m e F i e l d ;  
         Q r D a t a s M a x D H :   T D a t e T i m e F i e l d ;  
         Q r O r i :   T m y S Q L Q u e r y ;  
         Q r M a e :   T m y S Q L Q u e r y ;  
         Q r M C :   T m y S Q L Q u e r y ;  
         Q r M C M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b :   T m y S Q L Q u e r y ;  
         Q r P r o d R i b D a t a :   T D a t e F i e l d ;  
         Q r P r o d R i b G G X _ D s t :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b G G X _ S r c :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b P e c a s :   T F l o a t F i e l d ;  
         Q r P r o d R i b A r e a M 2 :   T F l o a t F i e l d ;  
         Q r P r o d R i b P e s o K g :   T F l o a t F i e l d ;  
         Q r P r o d R i b M o v i m I D :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b C o d i g o :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b E m p r e s a :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b Q t d A n t P e c a :   T F l o a t F i e l d ;  
         Q r P r o d R i b Q t d A n t A r M 2 :   T F l o a t F i e l d ;  
         Q r P r o d R i b Q t d A n t P e s o :   T F l o a t F i e l d ;  
         Q r P r o d R i b s T i p o E s t q :   T F l o a t F i e l d ;  
         Q r P r o d R i b d T i p o E s t q :   T F l o a t F i e l d ;  
         Q r P r o d R i b M o v i m N i v :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b D t C o r r A p o :   T D a t e T i m e F i e l d ;  
         Q r P r o d R i b C l i e n t M O :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b F o r n e c M O :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b E n t i S i t i o :   T I n t e g e r F i e l d ;  
         Q r O b t C l i F o r S i t e :   T m y S Q L Q u e r y ;  
         Q r I t s :   T m y S Q L Q u e r y ;  
         Q r C a l T o C u r :   T m y S Q L Q u e r y ;  
         Q r C a l T o C u r D s t G G X :   T I n t e g e r F i e l d ;  
         Q r C o n s P a r c :   T m y S Q L Q u e r y ;  
         Q r C o n s P a r c P e c a s :   T F l o a t F i e l d ;  
         Q r C o n s P a r c A r e a M 2 :   T F l o a t F i e l d ;  
         Q r C o n s P a r c P e s o K g :   T F l o a t F i e l d ;  
         Q r C o n s P a r c A n o :   T F l o a t F i e l d ;  
         Q r C o n s P a r c M e s :   T F l o a t F i e l d ;  
         Q r P r o d P a r c :   T m y S Q L Q u e r y ;  
         Q r P r o d P a r c P e c a s :   T F l o a t F i e l d ;  
         Q r P r o d P a r c A r e a M 2 :   T F l o a t F i e l d ;  
         Q r P r o d P a r c P e s o K g :   T F l o a t F i e l d ;  
         Q r P r o d P a r c C l i e n t M O :   T I n t e g e r F i e l d ;  
         Q r P r o d P a r c F o r n e c M O :   T I n t e g e r F i e l d ;  
         Q r P r o d P a r c C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r P r o d P a r c A n o :   T F l o a t F i e l d ;  
         Q r P r o d P a r c M e s :   T F l o a t F i e l d ;  
         Q r P e r i o d o s :   T m y S Q L Q u e r y ;  
         Q r P e r i o d o s A n o :   T F l o a t F i e l d ;  
         Q r P e r i o d o s M e s :   T F l o a t F i e l d ;  
         Q r P e r i o d o s P e r i o d o :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b :   T m y S Q L Q u e r y ;  
         Q r V m i G A r C a b D a t a :   T D a t e F i e l d ;  
         Q r V m i G A r C a b C o d i g o :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b G r a G r u X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b P e c a s :   T F l o a t F i e l d ;  
         Q r V m i G A r C a b A r e a M 2 :   T F l o a t F i e l d ;  
         Q r V m i G A r C a b P e s o K g :   T F l o a t F i e l d ;  
         Q r V m i G A r C a b M o v i m T w n :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b U n i d M e d :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b m e d _ g r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b x c o _ g r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b G r a G r u Y :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b C o u N i v 2 :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b T i p o E s t q :   T F l o a t F i e l d ;  
         Q r V m i G A r C a b D t C o r r A p o :   T D a t e T i m e F i e l d ;  
         Q r V m i G A r C a b C l i e n t M O :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b F o r n e c M O :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b E n t i S i t i o :   T I n t e g e r F i e l d ;  
         Q r S u m G A R :   T m y S Q L Q u e r y ;  
         Q r S u m G A R P e c a s :   T F l o a t F i e l d ;  
         Q r S u m G A R A r e a M 2 :   T F l o a t F i e l d ;  
         Q r S u m G A R P e s o K g :   T F l o a t F i e l d ;  
         Q r V m i G A r I t s :   T m y S Q L Q u e r y ;  
         Q r V m i G A r I t s D a t a :   T D a t e F i e l d ;  
         Q r V m i G A r I t s C o d i g o :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s G r a G r u X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s P e c a s :   T F l o a t F i e l d ;  
         Q r V m i G A r I t s A r e a M 2 :   T F l o a t F i e l d ;  
         Q r V m i G A r I t s P e s o K g :   T F l o a t F i e l d ;  
         Q r V m i G A r I t s M o v i m T w n :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s U n i d M e d :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s m e d _ g r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s x c o _ g r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s G r a G r u Y :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s C o u N i v 2 :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s T i p o E s t q :   T L a r g e i n t F i e l d ;  
         Q r V m i G A r I t s C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s D s t G G X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s S r c G G X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s S r c N i v e l 2 :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s S r c M o v I D :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s D t C o r r A p o :   T D a t e T i m e F i e l d ;  
     p r i v a t e  
         {   P r i v a t e   d e c l a r a t i o n s   }  
  
     p u b l i c  
         {   P u b l i c   d e c l a r a t i o n s   }  
         F I m p o r E x p o r ,   F E m p r e s a ,   F A n o M e s ,   F P e r i A p u :   I n t e g e r ;  
         F D i a I n i ,   F D i a F i m ,   F D i a P o s ( * ,   F D a t a * ) :   T D a t e T i m e ;  
         F T i p o P e r i o d o F i s c a l :   T T i p o P e r i o d o F i s c a l ;  
         P B 2 :   T P r o g r e s s B a r ;  
         L a A v i s o 1 ,   L a A v i s o 2 ,   L a A v i s o 3 ,   L a A v i s o 4 :   T L a b e l ;  
         M e A v i s o :   T M e m o ;  
         / /  
         f u n c t i o n     I m p o r t a G e r A r t ( S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e :  
                             S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
         f u n c t i o n     I m p o r t a O p e P r o c A ( Q r C a b :   T M y S Q L Q u e r y ;   M o v i m I D :   T E s t q M o v i m I D ;  
                             S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   S Q L _ D t H r F i m O p e :  
                             S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
         f u n c t i o n     I m p o r t a O p e P r o c B ( Q r C a b :   T M y S Q L Q u e r y ;   P s q M o v i m N i v :   T E s t q M o v i m N i v ;  
                             S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   S Q L _ D t H r F i m O p e :  
                             S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e _ O r i g e P r o c P Q ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;   M o v i m I D :  
                             T E s t q M o v i m I D ;   M o v i m N i v :   T E s t q M o v i m N i v ;   M o v i m C o d :   I n t e g e r ;  
                             I D S e q 1 :   I n t e g e r ;   S Q L _ P e r i o d o P Q :   S t r i n g ;   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ;   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o :   I n t e g e r ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e _ O r i g e R i b D T A _ 2 1 0 ( c o n s t   T i p o R e g S P E D P r o d :  
                             T T i p o R e g S P E D P r o d ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t   M o v i m N i v :  
                             T E s t q M o v i m N i v ;   c o n s t   S Q L _ P e r i o d o V S :   S t r i n g ;   c o n s t   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e _ O r i g e R i b P D A _ 2 1 0 ( c o n s t   T i p o R e g S P E D P r o d :  
                             T T i p o R e g S P E D P r o d ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t   M o v i m N i v :  
                             T E s t q M o v i m N i v ;   c o n s t   S Q L _ P e r i o d o V S :   S t r i n g ;   c o n s t   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e _ O r i g e P r o c V S _ 2 1 0 ( c o n s t   T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;  
                             c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t   M o v i m N i v I n n ,   M o v i m N i v B x a :  
                             T E s t q M o v i m N i v ;   c o n s t   Q r C a b :   T m y S Q L Q u e r y ;   v a r   I D S e q 1 :   I n t e g e r ;  
                             c o n s t   S Q L _ P e r i o d o V S :   S t r i n g ;   c o n s t   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :  
                             B o o l e a n ;  
         f u n c t i o n     I n s e r e _ E m O p e P r o c _ 2 1 5 ( c o n s t   T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;  
                             c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   M o v i m N i v :   T E s t q M o v i m N i v ;   c o n s t   Q r C a b :  
                             T m y S Q L Q u e r y ;   ( * c o n s t   F a t o r :   D o u b l e ; * )   c o n s t   S Q L _ P e r i o d o :   S t r i n g ;  
                             c o n s t   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;   v a r   I D S e q 1 :   I n t e g e r ) :  
                             B o o l e a n ;  
         p r o c e d u r e   I n s e r e _ E m O p e P r o c _ P r d ( c o n s t   T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;  
                             c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t   Q r y :   T m y S Q L Q u e r y ;   c o n s t   F a t o r :  
                             D o u b l e ;   c o n s t   S Q L _ P e r i o d o ,   S Q L _ P e r i o d o P Q :   S t r i n g ;   c o n s t  
                             O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;   c o n s t   M o v i m N i v :   T E s t q M o v i m N i v ) ;  
         / /  
         f u n c t i o n     G r a n d e z a s I n c o n s i s t e n t e s ( ) :   B o o l e a n ;  
         p r o c e d u r e   M e n s a g e m ( P r o c N a m e :   S t r i n g ;   M o v i m C o d ,   M o v i m I D :   I n t e g e r ;   T e x t o B a s e :  
                             S t r i n g ;   Q u e r y :   T m y S Q L Q u e r y ) ;  
     e n d ;  
  
 v a r  
     F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a :   T F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a ;  
  
 i m p l e m e n t a t i o n  
 u s e s  
     M o d u l e ,   U M y S Q L M o d u l e ,   M o d u l e G e r a l ,   U n V S _ P F ,   M o d A p p G r a G 1 ,   P Q x ,   D m k D A C _ P F ,  
     U n S P E D _ E F D _ P F ,   G r a G r u X ,   U n G r a d e _ P F ;  
  
  
 { $ R   * . d f m }  
  
 {   T F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a   }  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a . G r a n d e z a s I n c o n s i s t e n t e s :   B o o l e a n ;  
 v a r  
     S Q L _ P e r i o d o V M I :   S t r i n g ;  
 b e g i n  
     R e s u l t   : =   F a l s e ;  
     S Q L _ P e r i o d o V M I   : =   d m k P F . S Q L _ P e r i o d o ( ' W H E R E   v m i . D a t a H o r a   ' ,   F D i a I n i ,   F D i a F i m ,   T r u e ,   T r u e ) ;  
     / /  
     i f   D f M o d A p p G r a G 1 . G r a n d e s a s I n c o n s i s t e n t e s ( [  
     ' S E L E C T   D I S T I N C T   g g x . G r a G r u 1   N i v e l 1 ,   g g x . C o n t r o l e   R e d u z i d o ,     ' ,  
     ' g g 1 . N o m e   N O _ G G 1 ,   m e d . S i g l a ,   m e d . G r a n d e z a ,   ' ,  
     ' x c o . G r a n d e z a   x c o _ G r a n d e z a ,     ' ,  
     ' C A S T ( I F ( x c o . G r a n d e z a   >   0 ,   x c o . G r a n d e z a ,     ' ,  
     '     I F ( ( g g x . G r a G r u Y < 2 0 4 8   O R     ' ,  
     '     x c o . C o u N i v 2 < > 1 ) ,   2 ,     ' ,  
     '     I F ( x c o . C o u N i v 2 = 1 ,   1 ,   - 1 ) ) )   A S   D E C I M A L ( 1 1 , 0 ) )   d i f _ G r a n d e z a   ' ,  
     '   ' ,  
     ' F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   v m i   ' ,  
     ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v m i . G r a G r u X   ' ,  
     ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1   ' ,  
     ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e     ' ,  
     ' L E F T   J O I N   c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1     ' ,  
     ' L E F T   J O I N   c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2     ' ,  
     ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d   ' ,  
     S Q L _ P e r i o d o V M I ,  
     ' A N D   v m i . G r a G r u X   < >   0   ' ,  
     ' A N D   ( ( I F ( x c o . G r a n d e z a   >   0 ,   x c o . G r a n d e z a ,     ' ,  
     '     I F ( ( g g x . G r a G r u Y < 2 0 4 8   O R     ' ,  
     '     x c o . C o u N i v 2 < > 1 ) ,   2 ,     ' ,  
     '     I F ( x c o . C o u N i v 2 = 1 ,   1 ,   - 1 ) ) )   < >   m e d . G r a n d e z a ) )   ' ,  
     ' ' ] ,   D m o d . M y D B )   t h e n  
     b e g i n  
         R e s u l t   : =   T r u e ;  
         C l o s e ;  
     e n d ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a . I m p o r t a G e r A r t ( S Q L _ P e r i o d o C o m p l e x o ,  
     S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e :   S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
 c o n s t  
     M o v i m I D   =   T E s t q M o v i m I D . e m i d I n d s V S ;  
     E S T S T a b S o r c   =   e s t s V M I ;  
     s P r o c N a m e   =   ' F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a . P r o d u c a o S i n g u l a r _ I m p o r t a G e r A r t ( ) ' ;  
     / /  
     f u n c t i o n   F i l t r o R e g ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
     b e g i n  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 3 X :   R e s u l t   : =   ' = ' ;  
             t r s p 2 5 X :   R e s u l t   : =   ' < > ' ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ E r r o ( ' " F i l t r o R e g "   i n d e f i n i d o :   '   +  
                     G e r a l . F F 0 ( I n t e g e r ( T i p o R e g S P E D P r o d ) )   +   ' !   ( 1 ) ' ) ;  
                 R e s u l t   : =   ' # E R R ' ;  
             e n d ;  
         e n d ;  
     e n d ;  
     f u n c t i o n   F i l t r o P e r i o d o ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
     b e g i n  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 3 X :   R e s u l t   : =   S Q L _ P e r i o d o ;  
             / / t r s p 2 5 X :   R e s u l t   : =   S Q L _ D t H r F i m O p e ;  
             / / t r s p 2 5 X :   R e s u l t   : =   S Q L _ P e r i o d o ;  
             t r s p 2 5 X :   R e s u l t   : =   ' ' ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ E r r o ( ' " F i l t r o R e g "   i n d e f i n i d o ! ' ) ;  
                 R e s u l t   : =   ' # E R R _ P E R I O D O _ ' ;  
             e n d ;  
         e n d ;  
     e n d ;  
     / /  
     f u n c t i o n   I n s e r e R e g i s t r o s ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   B o o l e a n ;  
     v a r  
         M o v i m C o d ,   M o v i m T w n ,   C o d i g o ,   G r a G r u X ,   T i p o E s t q ,   M y F a t o r C a b ,   M y F A t o r I t s ,  
         I D S e q 1 ,   I D _ I t e m ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o :   I n t e g e r ;  
         D t H r A b e r t o ,   D t H r F i m O p e :   T D a t e T i m e ;  
         A r e a M 2 ,   P e s o K g ,   P e c a s ,   Q t d e :   D o u b l e ;  
         C O D _ I N S _ S U B S T :   S t r i n g ;  
         / /  
         D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;  
     b e g i n  
         C O D _ I N S _ S U B S T   : =   ' ' ;  
         M y F a t o r C a b   : =   1 ;  
         M y F a t o r I t s   : =   - 1 ;  
         / /  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
 / / /     C a b e c a l h o   >   R e g i s t r o   K 2 3 0   o u   K 2 5 0  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r V m i G A r C a b ,   D m o d . M y D B ,   [  
         ' S E L E C T   g g 1 . U n i d M e d ,   m e d . G r a n d e z a   m e d _ g r a n d e z a ,   ' ,  
         ' x c o . G r a n d e z a   x c o _ G r a n d e z a ,   g g x . G r a G r u Y ,   x c o . C o u N i v 2 ,   ' ,  
         V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( T r u e ,   ' T i p o E s t q ' ,   T r u e ,  
         ' v m i . A r e a M 2 ' ,   ' v m i . P e s o K g ' ,   ' v m i . P e c a s ' ) ,  
         ' D A T E ( v m i . D a t a H o r a )   D a t a ,   v m i . C o d i g o ,   v m i . M o v i m C o d ,   ' ,  
         ' v m i . G r a G r u X ,   v m i . P e c a s ,   v m i . A r e a M 2 ,   v m i . P e s o K g ,   ' ,  
         ' v m i . M o v i m T w n ,   v m i . C o n t r o l e ,   D t C o r r A p o ,   ' ,  
         ' v m i . C l i e n t M O ,   v m i . F o r n e c M O ,   s c c . E n t i S i t i o   ' ,  
         ' F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '                 v m i   ' ,  
         ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v m i . G r a G r u X   ' ,  
         ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
         ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
         ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
         ' L E F T   J O I N   s t q c e n l o c     s c l   O N   s c l . C o n t r o l e = v m i . S t q C e n L o c   ' ,  
         ' L E F T   J O I N   s t q c e n c a d     s c c   O N   s c c . C o d i g o = s c l . C o d i g o   ' ,  
         ' W H E R E   v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( e m i n S o r c C u r t i V S ) ) ,   / /   1 4  
         S Q L _ P e r i o d o ,  
         ' A N D   v m i . E m p r e s a = '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         ' A N D   v m i . M o v i m C o d   I N   (   ' ,  
         '     S E L E C T   M o v i m C o d   ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   ' ,  
         '     W H E R E   M o v i m N i v = 1 3   ' ,  
         '     A N D   F o r n e c M O '   +   F i l t r o R e g ( T i p o R e g S P E D P r o d )   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         ' )   ' ,  
         ' O R D E R   B Y   v m i . D t C o r r A p o ,   v m i . M o v i m C o d ,   v m i . G r a G r u X   ' ,  
         ' ' ] ) ;  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q r V m i G A r C a b ) ;  
         P B 2 . P o s i t i o n   : =   0 ;  
         P B 2 . M a x   : =   Q r V m i G A r C a b . R e c o r d C o u n t ;  
         Q r V m i G A r C a b . F i r s t ;  
         w h i l e   n o t   Q r V m i G A r C a b . E o f   d o  
         b e g i n  
             M y O b j e c t s . U p d P B ( P B 2 ,   L a A v i s o 3 ,   L a A v i s o 4 ) ;  
             / /  
             C o d i g o           : =   Q r V m i G A r C a b C o d i g o . V a l u e ;  
             M o v i m C o d       : =   Q r V m i G A r C a b M o v i m C o d . V a l u e ;  
             M o v i m T w n       : =   Q r V m i G A r C a b M o v i m T w n . V a l u e ;  
             D t H r A b e r t o   : =   Q r V m i G A r C a b D a t a . V a l u e ;  
             D t H r F i m O p e   : =   Q r V m i G A r C a b D a t a . V a l u e ;  
             D t M o v i m         : =   Q r V m i G A r C a b D a t a . V a l u e ;  
             D t C o r r A p o     : =   Q r V m i G A r C a b D t C o r r A p o . V a l u e ;  
             G r a G r u X         : =   Q r V m i G A r C a b G r a G r u X . V a l u e ;  
             T i p o E s t q       : =   T r u n c ( Q r V m i G A r C a b T i p o E s t q . V a l u e ) ;  
             C l i e n t M O       : =   Q r V m i G A r C a b C l i e n t M O . V a l u e ;  
             F o r n e c M O       : =   Q r V m i G A r C a b F o r n e c M O . V a l u e ;  
             E n t i S i t i o     : =   Q r V m i G A r C a b E n t i S i t i o . V a l u e ;  
             C o n t r o l e       : =   Q r V m i G A r C a b C o n t r o l e . V a l u e ;  
             / /  
             U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r S u m G A R ,   D m o d . M y D B ,   [  
             ' S E L E C T   S U M ( P e c a s )   P e c a s ,   S U M ( P e s o K g )   P e s o K g ,     ' ,  
             ' S U M ( A r e a M 2 )   A r e a M 2   ' ,  
             ' F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   ' ,  
             ' W H E R E   M o v i m C o d = '   +   G e r a l . F F 0 ( M o v i m C o d ) ,  
             ' A N D   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( e m i n D e s t C u r t i V S ) ) ,  
             ' A N D   G r a G r u X = '   +   G e r a l . F F 0 ( G r a G r u X ) ,  
             ' ' ] ) ;  
             A r e a M 2           : =   Q r S u m G A R A r e a M 2 . V a l u e ;  
             P e s o K g           : =   Q r S u m G A R P e s o K g . V a l u e ;  
             P e c a s             : =   Q r S u m G A R P e c a s . V a l u e ;  
             / /  
             i f   ( ( P e c a s   < >   0 )   o r   ( M o v i m I D = T E s t q M o v i m I D . e m i d E m P r o c S P ) )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
             b e g i n  
                 c a s e   T i p o E s t q   o f  
                     1 :   Q t d e   : =   A r e a M 2 ;  
                     2 :   Q t d e   : =   P e s o K g ;  
                     e l s e  
                     b e g i n  
                         G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                         +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ T i p o E s t q ] ) ;  
                         / /  
                         Q t d e   : =   P e c a s ;  
                     e n d ;  
                 e n d ;  
                 Q t d e   : =   Q t d e   *   M y F a t o r C a b ;  
             e n d   e l s e  
                 Q t d e   : =   0 ;  
             / /  
             i f   Q t d e   <   0   t h e n  
                 M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                 ' Q u a n t i d a d e   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 2 ) '   +   s L i n e B r e a k   +  
                 ' Q t d e :   '   +   G e r a l . F F T ( Q t d e ,   3 ,   s i N e g a t i v o ) ,   Q r V m i G A r C a b )  
             e l s e   i f   ( Q t d e   =   0 )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
             b e g i n  
                 c a s e   M o v i m I D   o f  
                     e m i d I n d s V S :   D t H r F i m O p e   : =   0 ;  
                     e l s e   M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                     ' Q u a n t i d a d e   z e r a d a   p a r a   O P   e n c e r r a d a ! ! !   ( 2 ) ' ,   Q r V m i G A r C a b ) ;  
                 e n d ;  
             e n d ;  
             / /  
             c a s e   T i p o R e g S P E D P r o d   o f  
                 t r s p 2 3 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                     C o n t r o l e ,  
                     D t H r A b e r t o ,   D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o ,   G r a G r u X ,   C l i e n t M O ,  
                     F o r n e c M O ,   E n t i S i t i o ,   Q t d e ,   O r i g e m O p e P r o c ,  
                     F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   I D S e q 1 ) ;  
                 e n d ;  
                 t r s p 2 5 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                     C o n t r o l e ,  
                     D t H r F i m O p e ,  
                     D t M o v i m ,   D t C o r r A p o ,   G r a G r u X ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,  
                     Q t d e ,   O r i g e m O p e P r o c ,   F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   ( * L i n A r q P a i * ) I D S e q 1 ) ;  
                 e n d ;  
                 e l s e  
                 G e r a l . M B _ E r r o ( ' T i p o   d e   r e g i s t r o   S P E D   E F D   ( '   +   G e r a l . F F 0 ( I n t e g e r (  
                 T i p o R e g S P E D P r o d ) )   +   ' )   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e   +   ' ( 2 ) ' ) ;  
             e n d ;  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
 / / /     I t e m   >   R e g i s t r o   K 2 3 5   o u   K 2 5 5  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
             / /  
             U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r V m i G A r I t s ,   D m o d . M y D B ,   [  
             ' S E L E C T   v m i . S r c M o v I D ,   v m i . S r c N i v e l 2 ,     v m i . S r c G G X ,   v m i . D s t G G X ,   ' ,  
             ' g g 1 . U n i d M e d ,   m e d . G r a n d e z a   m e d _ g r a n d e z a ,   ' ,  
             ' x c o . G r a n d e z a   x c o _ G r a n d e z a ,   g g x . G r a G r u Y ,   x c o . C o u N i v 2 ,   ' ,  
             V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( F a l s e ,   ' T i p o E s t q ' ,   T r u e ,  
             ' v m i . A r e a M 2 ' ,   ' v m i . P e s o K g ' ,   ' v m i . P e c a s ' ) ,  
             ' D A T E ( v m i . D a t a H o r a )   D a t a ,   v m i . D t C o r r A p o ,   v m i . C o d i g o ,   v m i . M o v i m C o d ,     ' ,  
             ' v m i . G r a G r u X ,   v m i . P e c a s ,   v m i . A r e a M 2 ,   v m i . P e s o K g ,     ' ,  
             ' v m i . M o v i m T w n ,   v m i . C o n t r o l e     ' ,  
             ' F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '                 v m i     ' ,  
             ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v m i . G r a G r u X   ' ,  
             ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
             ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
             ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
             ' W H E R E   v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( e m i n B a i x C u r t i V S ) ) ,   / /   1 5  
             ' A N D   v m i . M o v i m C o d = '   +   G e r a l . F F 0 ( M o v i m C o d ) ,  
             ' A N D   v m i . M o v i m T w n = '   +   G e r a l . F F 0 ( M o v i m T w n ) ,  
             ' A N D   v m i . E m p r e s a = '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
             ' A N D   ( v m i . F o r n e c M O = 0   ' ,  
             '   O R   v m i . F o r n e c M O '   +   F i l t r o R e g ( T i p o R e g S P E D P r o d )   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
             ' ) ' ,  
             F i l t r o P e r i o d o ( T i p o R e g S P E D P r o d ) ,  
             ' O R D E R   B Y   D a t a ,   v m i . M o v i m C o d ,   v m i . G r a G r u X   ' ,  
             ' ' ] ) ;  
             / / i f   Q r V m i G A r I t s . R e c o r d C o u n t   >   1   t h e n  
             / / G e r a l . M B _ S Q L ( S e l f ,   Q r V m i G A r I t s ) ;  
             Q r V m i G A r I t s . F i r s t ;  
             w h i l e   n o t   Q r V m i G A r I t s . E o f   d o  
             b e g i n  
                 i f   T E s t q M o v i m I D ( Q r V m i G A r I t s S r c M o v I D . V a l u e )   =   e m i d E m P r o c C u r   t h e n  
                 b e g i n  
                     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r C a l T o C u r ,   D m o d . M y D B ,   [  
                     ' S E L E C T   D s t G G X   ' ,  
                     ' F R O M   '   +   C O _ S E L _ T A B _ V M I ,  
                     ' W H E R E   C o n t r o l e = '   +   G e r a l . F F 0 ( Q r V m i G A r I t s S r c N i v e l 2 . V a l u e ) ,  
                     ' ' ] ) ;  
                     G r a G r u X   : =   Q r C a l T o C u r D s t G G X . V a l u e ;  
                 e n d  
                 e l s e  
                     G r a G r u X   : =   Q r V m i G A r I t s G r a G r u X . V a l u e ;  
                 / /  
                 C o d i g o           : =   Q r V m i G A r I t s C o d i g o . V a l u e ;  
                 M o v i m C o d       : =   Q r V m i G A r I t s M o v i m C o d . V a l u e ;  
                 M o v i m T w n       : =   Q r V m i G A r I t s M o v i m T w n . V a l u e ;  
                 D t H r A b e r t o   : =   Q r V m i G A r I t s D a t a . V a l u e ;  
                 D t H r F i m O p e   : =   Q r V m i G A r I t s D a t a . V a l u e ;  
                 T i p o E s t q       : =   Q r V m i G A r I t s T i p o E s t q . V a l u e ;  
                 C o n t r o l e       : =   Q r V m i G A r I t s C o n t r o l e . V a l u e ;  
                 I D _ I t e m         : =   C o n t r o l e ;  
                 / /  
                 A r e a M 2           : =   Q r V m i G A r I t s A r e a M 2 . V a l u e ;  
                 P e s o K g           : =   Q r V m i G A r I t s P e s o K g . V a l u e ;  
                 P e c a s             : =   Q r V m i G A r I t s P e c a s . V a l u e ;  
                 / /  
                 D t C o r r A p o     : =   Q r V m i G A r I t s D t C o r r A p o . V a l u e ;  
                 / /  
                 i f   ( ( P e c a s   < >   0 )   o r   ( M o v i m I D = T E s t q M o v i m I D . e m i d E m P r o c S P ) )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
                 b e g i n  
                     c a s e   T i p o E s t q   o f  
                         1 :   Q t d e   : =   A r e a M 2 ;  
                         2 :   Q t d e   : =   P e s o K g ;  
                         e l s e  
                         b e g i n  
                             G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                             +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ T i p o E s t q ] ) ;  
                             / /  
                             Q t d e   : =   P e c a s ;  
                         e n d ;  
                     e n d ;  
                     Q t d e   : =   Q t d e   *   M y F a t o r I t s ;  
                 e n d   e l s e  
                     Q t d e   : =   0 ;  
                 / /  
                 i f   Q t d e   <   0   t h e n  
                     M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                     ' Q u a n t i d a d e   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 3 ) '   +   s L i n e B r e a k   +  
                     ' Q t d e :   '   +   G e r a l . F F T ( Q t d e ,   3 ,   s i N e g a t i v o ) ,   Q r V m i G A r I t s )  
                 e l s e   i f   ( Q t d e   =   0 )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
                 b e g i n  
                     c a s e   M o v i m I D   o f  
                         e m i d I n d s V S :   D t H r F i m O p e   : =   0 ;  
                         e l s e   M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                         ' Q u a n t i d a d e   z e r a d a   p a r a   O P   e n c e r r a d a ! ! !   ( 3 ) ' ,   Q r V m i G A r I t s ) ;  
                     e n d ;  
                 e n d ;  
                 / /  
                 c a s e   T i p o R e g S P E D P r o d   o f  
                     / / t r s p 2 1 X :  
                     t r s p 2 3 X :  
                     b e g i n  
                         S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                         F P e r i A p u ,   ( * L i n A r q P a i * ) I D S e q 1 ,   D t H r A b e r t o ,   D t C o r r A p o ,   G r a G r u X ,  
                         Q t d e ,   C O D _ I N S _ S U B S T ,   I D _ I t e m ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                         C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   O r i g e m O p e P r o c ,  
                         E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                     e n d ;  
                     t r s p 2 5 X :  
                     b e g i n  
                         S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                         F P e r i A p u ,   ( * L i n A r q P a i * ) I D S e q 1 ,   D t H r A b e r t o ,   D t C o r r A p o ,   G r a G r u X ,  
                         Q t d e ,   C O D _ I N S _ S U B S T ,   I D _ I t e m ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                         C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   O r i g e m O p e P r o c ,  
                         E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                     e n d ;  
                     e l s e  
                         G e r a l . M B _ E r r o ( ' T i p o   d e   r e g i s t r o   S P E D   E F D   ( '   +   G e r a l . F F 0 ( I n t e g e r (  
                         T i p o R e g S P E D P r o d ) )   +   ' )   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e   +   ' ( 3 ) ' ) ;  
                 e n d ;  
                 / /  
                 Q r V m i G A r I t s . N e x t ;  
             e n d ;  
             / /  
             Q r V m i G A r C a b . N e x t ;  
         e n d ;  
     e n d ;  
 b e g i n  
     U n D m k D A C _ P F . E x e c u t a M y S Q L Q u e r y 0 ( D M o d G . Q r U p d P I D 1 ,   D M o d G . M y P I D _ D B ,   [  
     ' D R O P   T A B L E   I F   E X I S T S   _ S P E D _ E F D _ K 2 X X _ G A R ; ' ,  
     ' C R E A T E   T A B L E   _ S P E D _ E F D _ K 2 X X _ G A R ' ,  
     ' S E L E C T   v m i . D s t G G X ,   v m i . S r c G G X ,   ' ,  
     ' g g 1 . U n i d M e d ,   m e d . G r a n d e z a   m e d _ g r a n d e z a ,   ' ,  
     ' x c o . G r a n d e z a   x c o _ G r a n d e z a ,   g g x . G r a G r u Y ,   x c o . C o u N i v 2 ,   ' ,  
     V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( F a l s e ,   ' T i p o E s t q ' ,   T r u e ,  
     ' v m i . A r e a M 2 ' ,   ' v m i . P e s o K g ' ,   ' v m i . P e c a s ' ) ,  
     ' D A T E ( v m i . D a t a H o r a )   D a t a ,   v m i . C o d i g o ,   v m i . M o v i m C o d ,     ' ,  
     ' v m i . G r a G r u X ,   v m i . P e c a s ,   v m i . A r e a M 2 ,   v m i . P e s o K g ,     ' ,  
     ' v m i . M o v i m T w n     ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . '   +   C O _ S E L _ T A B _ V M I   +   '                 v m i       ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x         g g x   O N   g g x . C o n t r o l e = v m i . G r a G r u X     ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1   ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d   ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e     ' ,  
     ' W H E R E   v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( e m i n B a i x C u r t i V S ) ) ,             / / 1 5  
     S Q L _ P e r i o d o ,  
     ' O R D E R   B Y   D a t a ,   v m i . M o v i m C o d ,   v m i . G r a G r u X   ; ' ,  
     ' ' ] ) ;  
     i f   S P E D _ E F D _ P F . E r r G r a n d e z a ( ' _ S P E D _ E F D _ K 2 X X _ G A R ' ,   ' S r c G G X ' ,   ' D s t G G X ' )   t h e n  
         E x i t ;  
     / /  
     I n s e r e R e g i s t r o s ( t r s p 2 3 X ) ;  
     I n s e r e R e g i s t r o s ( t r s p 2 5 X ) ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a . I m p o r t a O p e P r o c A ( Q r C a b :   T M y S Q L Q u e r y ;  
     M o v i m I D :   T E s t q M o v i m I D ;   S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,  
     S Q L _ D t H r F i m O p e :   S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
 v a r  
     T a b S r c ,   S Q L _ F L D ,   S Q L _ A N D :   S t r i n g ;  
     M o v N i v I n n ,   M o v N i v B x a :   T E s t q M o v i m N i v ;  
     / /  
     f u n c t i o n   F i l t r o R e g ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
     b e g i n  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 3 X :   R e s u l t   : =   ' = ' ;  
             t r s p 2 5 X :   R e s u l t   : =   ' < > ' ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ E r r o ( ' " F i l t r o R e g "   i n d e f i n i d o :   '   +  
                     G e r a l . F F 0 ( I n t e g e r ( T i p o R e g S P E D P r o d ) )   +   ' !   ( 2 ) ' ) ;  
                 R e s u l t   : =   ' # E R R ' ;  
             e n d ;  
         e n d ;  
     e n d ;  
     / /  
     f u n c t i o n   F i l t r o C o n d ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
     b e g i n  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 3 X :   R e s u l t   : =   '   O R   ' ;  
             t r s p 2 5 X :   R e s u l t   : =   '   A N D   ' ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ E r r o ( ' " F i l t r o C o n d "   i n d e f i n i d o :   '   +  
                     G e r a l . F F 0 ( I n t e g e r ( T i p o R e g S P E D P r o d ) )   +   ' !   ( 2 a ) ' ) ;  
                 R e s u l t   : =   ' # E R R ' ;  
             e n d ;  
         e n d ;  
     e n d ;  
     / /  
     f u n c t i o n   R e a b r e E m P r o c e s s o 2 1 X ( Q u e r y :   T m y S Q L Q u e r y ) :   B o o l e a n ;  
     b e g i n  
         R e s u l t   : =   F a l s e ;  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q u e r y ,   D m o d . M y D B ,   [  
         S Q L _ F L D ,  
         ' F R O M   '   +   T a b S r c   +     '   v s x ' ,  
         ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v s x . G G X D s t   ' ,  
         ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
         ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
         ' L E F T   J O I N   c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1   ' ,  
         ' L E F T   J O I N   c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2   ' ,  
         ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
         S Q L _ P e r i o d o C o m p l e x o ,  
         ' O R   M o v i m C o d   I N   (   ' ,  
         '     S E L E C T   M o v i m C o d   ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   ' ,  
         '     W H E R E   M o v i m I D = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) ) ,  
         '     '   +   S Q L _ P e r i o d o V S ,  
         ' ) )   ' ,  
         S Q L _ A N D ,  
  
         '   ' ,  
         ' A N D   M o v i m C o d   I N   (     ' ,  
         '     S E L E C T   M o v i m C o d     ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '     ' ,  
         '     W H E R E   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v N i v I n n ) ) ,  
         '     A N D   ( E m p r e s a   =   '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         / / '     A N D   F o r n e c M O '   +   F i l t r o R e g ( t r s p 2 1 X )   +   G e r a l . F F 0 ( F E m p r e s a )   +  
         '     ) ' ,  
         ' )   ' ,  
         '   ' ,  
         / / O p e r a c a o   q u e   n a o   t e m   t e m   i t e n s   d e   o r i g e m   n e m   d e s t i n o ,   o u   s e j a ,   e s t a h   v a z i a  
         ' A N D   ( P e c a s S r c   < >   0   ' ,  
         '           O R   ' ,  
         '           P e c a s D s t   < >   0   ' ,  
         '           O R   ' ,  
         '           ( P e s o K g S r c   < > 0   A N D   g g x . G r a G r u Y   <   1 0 2 4 ) )   ' ,  
         ' ' ] ) ;  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
         R e s u l t   : =   T r u e ;  
     e n d ;  
     / /  
     f u n c t i o n   R e a b r e E m P r o c e s s o 2 3 X ( Q u e r y :   T m y S Q L Q u e r y ) :   B o o l e a n ;  
     b e g i n  
         R e s u l t   : =   F a l s e ;  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q u e r y ,   D m o d . M y D B ,   [  
         S Q L _ F L D ,  
         ' F R O M   '   +   T a b S r c   +     '   v s x ' ,  
         / / ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v s x . G r a G r u X   ' ,  
         ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v s x . G G X D s t   ' ,  
         ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
         ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
         ' L E F T   J O I N   c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1   ' ,  
         ' L E F T   J O I N   c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2   ' ,  
         ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
         S Q L _ P e r i o d o C o m p l e x o ,  
         ' O R   M o v i m C o d   I N   (   ' ,  
         '     S E L E C T   M o v i m C o d   ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   ' ,  
         '     W H E R E   M o v i m I D = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) ) ,  
         '     '   +   S Q L _ P e r i o d o V S ,  
         ' ) )   ' ,  
         S Q L _ A N D ,  
  
         '   ' ,  
         ' A N D   M o v i m C o d   I N   (     ' ,  
         '     S E L E C T   M o v i m C o d     ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '     ' ,  
         '     W H E R E   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v N i v I n n ) ) ,  
         '     A N D   ( E m p r e s a   =   '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         '     A N D   ( F o r n e c M O '   +   F i l t r o R e g ( t r s p 2 3 X )   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         '     '   +   F i l t r o C o n d ( t r s p 2 3 X )   +   '   F o r n e c M O '   +   F i l t r o R e g ( t r s p 2 3 X )   +   ' 0 )   )   ' ,  
         ' )   ' ,  
         '   ' ,  
  
         / / O p e r a c a o   q u e   n a o   t e m   t e m   i t e n s   d e   o r i g e m   n e m   d e s t i n o ,   o u   s e j a ,   e s t a h   v a z i a  
         ' A N D   ( P e c a s S r c   < >   0   ' ,  
         '           O R   ' ,  
         '           P e c a s D s t   < >   0   ' ,  
         '           O R   ' ,  
         '           ( P e s o K g S r c   < > 0   A N D   g g x . G r a G r u Y   <   1 0 2 4 ) )   ' ,  
         ' ' ] ) ;  
 ( *  
         i f   Q u e r y . R e c o r d C o u n t   >   0   t h e n   G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
 * )  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
         R e s u l t   : =   T r u e ;  
     e n d ;  
     / /  
     f u n c t i o n   R e a b r e E m P r o c e s s o 2 5 X ( Q u e r y :   T m y S Q L Q u e r y ) :   B o o l e a n ;  
     b e g i n  
         R e s u l t   : =   F a l s e ;  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q u e r y ,   D m o d . M y D B ,   [  
         S Q L _ F L D ,  
         ' F R O M   '   +   T a b S r c   +     '   v s x ' ,  
         ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v s x . G G X D s t   ' ,  
         ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
         ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
         ' L E F T   J O I N   c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1   ' ,  
         ' L E F T   J O I N   c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2   ' ,  
         ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
         ' W H E R E     M o v i m C o d   I N   (     ' ,  
         '     S E L E C T   M o v i m C o d     ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '     ' ,  
         '     W H E R E   M o v i m N i v   I N   ( '   +   G e r a l . F F 0 ( I n t e g e r ( M o v N i v I n n ) )   +   ' , '   +  
         G e r a l . F F 0 ( I n t e g e r ( M o v N i v B x a ) )   +   ' )   ' ,  
         / /  
         '     A N D   ( E m p r e s a   =   '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         '     A N D   ( F o r n e c M O '   +   F i l t r o R e g ( t r s p 2 5 X )   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         '     '   +   F i l t r o C o n d ( t r s p 2 5 X )   +   '   F o r n e c M O '   +   F i l t r o R e g ( t r s p 2 5 X )   +   ' 0 )   )   ' ,  
         '   '   +   S Q L _ P e r i o d o V S ,  
         ' )   ' ,  
         '   ' ,  
         / / O p e r a c a o   q u e   n a o   t e m   t e m   i t e n s   d e   o r i g e m   n e m   d e s t i n o ,   o u   s e j a ,   e s t a h   v a z i a  
         ' A N D   ( P e c a s S r c   < >   0   ' ,  
         '           O R   ' ,  
         '           P e c a s D s t   < >   0   ' ,  
         '           O R   ' ,  
         '           ( P e s o K g S r c   < > 0   A N D   g g x . G r a G r u Y   <   1 0 2 4 ) )   ' ,  
         ' ' ] ) ;  
 ( *  
         i f   Q u e r y . R e c o r d C o u n t   >   0   t h e n   G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
 * )  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
         R e s u l t   : =   T r u e ;  
     e n d ;  
     / /  
     f u n c t i o n   R e a b r e E m P r o c e s s o 2 6 X ( Q u e r y :   T m y S Q L Q u e r y ) :   B o o l e a n ;  
     b e g i n  
         R e s u l t   : =   F a l s e ;  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q u e r y ,   D m o d . M y D B ,   [  
         S Q L _ F L D ,  
         ' F R O M   '   +   T a b S r c   +     '   v s x ' ,  
         ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v s x . G G X D s t   ' ,  
         ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
         ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
         ' L E F T   J O I N   c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1   ' ,  
         ' L E F T   J O I N   c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2   ' ,  
         ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
         S Q L _ P e r i o d o C o m p l e x o ,  
         ' O R   M o v i m C o d   I N   (   ' ,  
         '     S E L E C T   M o v i m C o d   ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   ' ,  
         '     W H E R E   M o v i m I D = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) ) ,  
         '     '   +   S Q L _ P e r i o d o V S ,  
         ' ) )   ' ,  
         S Q L _ A N D ,  
  
         '   ' ,  
         ' A N D   M o v i m C o d   I N   (     ' ,  
         '     S E L E C T   M o v i m C o d     ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '     ' ,  
         '     W H E R E   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v N i v I n n ) ) ,  
         '     A N D   ( E m p r e s a   =   '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         '     ) ' ,  
         ' )   ' ,  
         '   ' ,  
         / / O p e r a c a o   q u e   n a o   t e m   t e m   i t e n s   d e   o r i g e m   n e m   d e s t i n o ,   o u   s e j a ,   e s t a h   v a z i a  
         ' A N D   ( P e c a s S r c   < >   0   ' ,  
         '           O R   ' ,  
         '           P e c a s D s t   < >   0   ' ,  
         '           O R   ' ,  
         '           ( P e s o K g S r c   < > 0   A N D   g g x . G r a G r u Y   <   1 0 2 4 ) )   ' ,  
         ' ' ] ) ;  
 ( *  
         i f   Q u e r y . R e c o r d C o u n t   >   0   t h e n   G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
 * )  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
         R e s u l t   : =   T r u e ;  
     e n d ;  
     / /  
     f u n c t i o n   I n s e r e R e g i s t r o s ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   B o o l e a n ;  
     v a r  
         L i n A r q P a i ,   M o v i m C o d :   I n t e g e r ;  
         F a t o r :   D o u b l e ;  
         A b r i u :   B o o l e a n ;  
     b e g i n  
         R e s u l t   : =   F a l s e ;  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 1 X :   A b r i u   : =   R e a b r e E m P r o c e s s o 2 1 X ( Q r C a b ) ;  
             t r s p 2 3 X :   A b r i u   : =   R e a b r e E m P r o c e s s o 2 3 X ( Q r C a b ) ;  
             t r s p 2 5 X :   A b r i u   : =   R e a b r e E m P r o c e s s o 2 5 X ( Q r C a b ) ;  
             t r s p 2 6 X :   A b r i u   : =   R e a b r e E m P r o c e s s o 2 6 X ( Q r C a b ) ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ A v i s o ( ' T i p o   d e   r e g i s t r o   i n d e f i n i d o   e m   " I n s e r e R e g i s t r o s ( ) " ' ) ;  
                 A b r i u   : =   F a l s e ;  
             e n d ;  
         e n d ;  
         i f   n o t   A b r i u   t h e n  
             E x i t ;  
         / /  
         P B 2 . M a x   : =   Q r C a b . R e c o r d C o u n t ;  
         P B 2 . P o s i t i o n   : =   0 ;  
         Q r C a b . F i r s t ;  
         w h i l e   n o t   Q r C a b . E o f   d o  
         b e g i n  
             M y O b j e c t s . U p d P B ( P B 2 ,   n i l ,   n i l ) ;  
             M o v i m C o d     : =   Q r C a b . F i e l d B y N a m e ( ' M o v i m C o d ' ) . A s I n t e g e r ;  
             i f   T i p o R e g S P E D P r o d   =   t r s p 2 1 X   t h e n  
             b e g i n  
                 c a s e   M o v i m I D   o f  
 ( *                   J a   t e m   p r o p r i o :   P r o d u c a o S i n g u l a r _ I n s e r e _ O r i g e R i b P D A _ 2 1 0 ( )  
                     e m i d E m R i b P D A :  
                     b e g i n  
                         F a t o r   : =   1 ;  
                         P r o d u c a o S i n g u l a r _ I n s e r e _ O r i g e R i b P D A _ 2 1 0 ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   T E s t q M o v i m N i v . e m i n D e s t P D A ,   Q r C a b ,   L i n A r q P a i ,   S Q L _ P e r i o d o V S ,   O r i g e m O p e P r o c ) ;  
                     e n d ;  
 * )  
                     e m i d E m P r o c C a l :   ;   / /   n a d a ! !  
                     e m i d E m P r o c C u r :   ;   / /   n a d a ! !  
                     e m i d E m O p e r a c a o :  
                     b e g i n  
                         F a t o r   : =   1 ;  
                         I n s e r e _ O r i g e P r o c V S _ 2 1 0 ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   T E s t q M o v i m N i v . e m i n E m O p e r I n n ,   T E s t q M o v i m N i v . e m i n E m O p e r B x a ,   Q r C a b ,   L i n A r q P a i ,   S Q L _ P e r i o d o V S ,   O r i g e m O p e P r o c ) ;  
                         I n s e r e _ E m O p e P r o c _ 2 1 5 ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   T E s t q M o v i m N i v . e m i n D e s t O p e r ,   Q r C a b ,   ( * F a t o r , * )   S Q L _ P e r i o d o V S ,   O r i g e m O p e P r o c ,   L i n A r q P a i ) ;  
                     e n d ;  
                     e m i d E m P r o c W E :   ;   / /   n a d a ! !  
                     e l s e  
                     b e g i n  
                         R e s u l t   : =   F a l s e ;  
                         G e r a l . M B _ E r r o ( ' M o v i m I D   '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) )   +   '   n � o   i m p l e m e n t a d o   e m   " I m p o r t a O p e P r o c ( ) . 2 1 x " ' ) ;  
                         E x i t ;  
                     e n d ;  
                 e n d ;  
             e n d   e l s e  
             b e g i n  
                 c a s e   M o v i m I D   o f  
                     e m i d E m P r o c C a l :  
                     b e g i n  
                         F a t o r   : =   - 1 ;  
                         I n s e r e _ E m O p e P r o c _ P r d ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   Q r C a b ,   F a t o r ,  
                             S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   T E s t q M o v i m N i v . e m i n E m C a l B x a ) ;  
                     e n d ;  
                     e m i d E m P r o c C u r :  
                     b e g i n  
                         F a t o r   : =   1 ;  
                         I n s e r e _ E m O p e P r o c _ P r d ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   Q r C a b ,   F a t o r ,  
                             S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   T E s t q M o v i m N i v . e m i n E m C u r B x a ) ;  
                     e n d ;  
                     e m i d E m P r o c W E :  
                     b e g i n  
                         F a t o r   : =   1 ;  
  
                         / / c r i a r   q u e r y   q u e   s e p a r a   p e r i o d o s   +   P e r i o d o s   d e   c o r r e c a o !  
                         I n s e r e _ E m O p e P r o c _ P r d ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   Q r C a b ,   F a t o r ,  
                             S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   T E s t q M o v i m N i v . e m i n E m W E n d B x a ) ;  
                     e n d ;  
                     e m i d E m P r o c S P :  
                     b e g i n  
                         F a t o r   : =   1 ;  
                         I n s e r e _ E m O p e P r o c _ P r d ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   Q r C a b ,   F a t o r ,  
                             S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   T E s t q M o v i m N i v . e m i n E m P S P B x a ) ;  
                     e n d ;  
                     e m i d E m R e p r R M :  
                     b e g i n  
                         F a t o r   : =   1 ;  
                         I n s e r e _ E m O p e P r o c _ P r d ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   Q r C a b ,   F a t o r ,  
                             S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   T E s t q M o v i m N i v . e m i n E m R R M B x a ) ;  
                       / /   G e r a l . M B _ S Q L ( S e l f ,   Q r C a b ) ;  
                     e n d ;  
                     e l s e  
                     b e g i n  
                         R e s u l t   : =   F a l s e ;  
                         / /   M o v i m I D   3 0   n � o   i m p l e m e n t a d o   e m   " I m p o r t a O p e P r o c ( ) . 2 x x "  
                         G e r a l . M B _ E r r o ( ' M o v i m I D   '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) )   +   '   n � o   i m p l e m e n t a d o   e m   " I m p o r t a O p e P r o c ( ) . 2 x x " ' ) ;  
                         E x i t ;  
                     e n d ;  
                 e n d ;  
             e n d ;  
             Q r C a b . N e x t ;  
         e n d ;  
         R e s u l t   : =   T r u e ;  
     e n d ;  
 b e g i n  
     / / P B 1 . P o s i t i o n   : =   P B 1 . P o s i t i o n   +   1 ;  
     R e s u l t   : =   F a l s e ;  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' A t u a l i z a n d o   " D t H r F i m O p e "   d o   M o v i m I D :   '   +  
         G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) ) ) ;  
     V S _ P F . A t u a l i z a D t H r F i m O p e _ M o v i m I D ( M o v i m I D ) ;  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' I m p o r t a n d o   M o v i m I D :   '   +  
         G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) ) ) ;  
     T a b S r c         : =   V S _ P F . O b t e m N o m e T a b e l a V S X x x C a b ( M o v i m I D ) ;  
     / /   R e c u r t i m e n t o ! ! !  
     M o v N i v I n n   : =   V S _ P F . O b t e m M o v i m N i v I n n D e M o v i m I D ( M o v i m I D ) ;  
     M o v N i v B x a   : =   V S _ P F . O b t e m M o v i m N i v B x a D e M o v i m I D ( M o v i m I D ) ;  
     / /   ? ? ? ? ?  
     / / M o v N i v I n n   : =   V S _ P F . O b t e m M o v i m N i v B x a D e M o v i m I D ( M o v i m I D ) ;  
     i f   M o v N i v I n n   =   e m i n S e m N i v   t h e n  
         E x i t ;  
     / /  
     S Q L _ F L D   : =   G e r a l . A T S ( [  
     ' S E L E C T   D I S T I N C T   g g x . G r a G r u 1   N i v e l 1 ,   g g x . C o n t r o l e   R e d u z i d o ,   ' ,  
     ' g g 1 . N o m e   N O _ G G 1 ,   m e d . S i g l a ,   m e d . G r a n d e z a   ' ,  
     ' ' ] ) ;  
     S Q L _ A N D   : =   G e r a l . A T S ( [  
     ' A N D   ( ( I F ( x c o . G r a n d e z a   >   0 ,   x c o . G r a n d e z a ,   ' ,  
     '     I F ( ( g g x . G r a G r u Y < 2 0 4 8   O R   ' ,  
     '     x c o . C o u N i v 2 < > 1 ) ,   2 ,   ' ,  
     '     I F ( x c o . C o u N i v 2 = 1 ,   1 ,   - 1 ) ) )   < >   m e d . G r a n d e z a ) ) ' ,  
     ' ' ] ) ;  
     i f   G r a n d e z a s I n c o n s i s t e n t e s ( )   t h e n  
         E x i t ;  
     / /  
     S Q L _ F L D   : =   G e r a l . A T S ( [  
     ' S E L E C T     ' ,  
     / /  
     V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( F a l s e ,   ' T i p o E s t q ' ,   T r u e ,  
     ' v s x . A r e a D s t M 2 ' ,   ' v s x . P e s o K g D s t ' ,   ' v s x . P e c a s D s t ' ) ,  
     / /  
     ' v s x . *   ' ,  
     ' ' ] ) ;  
     S Q L _ A N D   : =   ' ' ;  
     / /  
     c a s e   M o v i m I D   o f  
         / / e m i d E m R i b P D A ,   J a   t e m   p r o p r i o :   P r o d u c a o S i n g u l a r _ I n s e r e _ O r i g e R i b P D A _ 2 1 0 ( )  
         / / e m i d E m R i b D T A ,  
         e m i d E m O p e r a c a o :  
         b e g i n  
             I n s e r e R e g i s t r o s ( t r s p 2 1 X ) ;  
         e n d ;  
         e m i d E m P r o c C a l ,  
         e m i d E m P r o c C u r ,  
         e m i d E m P r o c W E ,  
         e m i d E m P r o c S P :  
         b e g i n  
             I n s e r e R e g i s t r o s ( t r s p 2 3 X ) ;  
             I n s e r e R e g i s t r o s ( t r s p 2 5 X ) ;  
         e n d ;  
         e m i d E m R e p r R M :  
         b e g i n  
             I n s e r e R e g i s t r o s ( t r s p 2 6 X ) ;  
         e n d ;  
         e l s e   G e r a l . M B _ E r r o ( ' " M o v i m I D "   n � o   i m p l e m e n t a d o   e m   " P r o d u c a o S i n g u l a r _ I m p o r t a O p e P r o c A ( ) " ' ) ;  
     e n d ;  
     / /  
     R e s u l t   : =   T r u e ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a . I m p o r t a O p e P r o c B ( Q r C a b :   T M y S Q L Q u e r y ;  
     P s q M o v i m N i v :   T E s t q M o v i m N i v ;   S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,  
     S Q L _ D t H r F i m O p e :   S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
     / /  
     f u n c t i o n   P a r t e S Q L ( T a b e l a ,   F l d S r c ,   F l d D s t ,   s A r e a ,   s P e s o ,   s P e c a ,   d A r e a ,   d P e s o ,   d P e c a :   S t r i n g ;   M o v i m N i v :  
     T E s t q m o v i m N i v ) :   S t r i n g ;  
     v a r  
         S Q L _ I F :   S t r i n g ;  
     b e g i n  
         S Q L _ I F   : =   ' I F ( '   +   C o p y ( S Q L _ P e r i o d o V S ,   4 )   +   ' ,   ' ;  
         / /  
         R e s u l t   : =   G e r a l . A T S ( [  
         ' S E L E C T   v m i . E m p r e s a ,   v m i . M o v i m I D ,   v m i . C o d i g o ,   v m i . M o v i m C o d ,     ' ,  
         ' v m i . M o v i m N i v ,   v m i . C o n t r o l e ,   '   +  
         S Q L _ I F   +   s P e c a   +   ' ,   0 . 0 0 0 )   P e c a s ,   '   +  
         S Q L _ I F   +   s A r e a   +   ' ,   0 . 0 0 0 )   A r e a M 2 ,   '   +  
         S Q L _ I F   +   s P e s o   +   ' ,   0 . 0 0 0 )   P e s o K g ,   ' ,  
         ' D A T E ( v m i . D a t a H o r a )   D a t a ,     '   +   F l d D s t   +   '   G G X _ D s t ,   ' ,   / / +   F l d O r i   +   ' ,   ' ,  
         F l d S r c   +   '   G G X _ S r c ,   ' ,  
         '     ' ,  
         / /  
         V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( T r u e ,   ' s T i p o E s t q ' ,   T r u e ,  
         s A r e a ,   s P e s o ,   s P e c a ,   ' s ' ) ,  
         / /  
         V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( T r u e ,   ' d T i p o E s t q ' ,   T r u e ,  
         d A r e a ,   d P e s o ,   d P e c a ,   ' d ' ) ,  
         / /  
         '   ' ,  
         S Q L _ I F   +   d P e c a   +   ' ,   0 . 0 0 0 )   Q t d A n t P e c a ,   '   +  
         S Q L _ I F   +   d A r e a   +   ' ,   0 . 0 0 0 )   Q t d A n t A r M 2 ,   '   +  
         S Q L _ I F   +   d P e s o   +   ' ,   0 . 0 0 0 )   Q t d A n t P e s o ,   ' ,  
         ' v m i . D t C o r r A p o ,   v m i . C l i e n t M O ,   v m i . F o r n e c M O ,   s c c . E n t i S i t i o   ' ,  
         ' F R O M   '   +   T M e u D B   +   ' . '   +   T a b e l a   +   '   c a b   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . '   +   C O _ S E L _ T A B _ V M I   +   '       v m i   O N   v m i . M o v i m C o d = c a b . M o v i m C o d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . s t q c e n l o c   s c l   O N   s c l . C o n t r o l e = v m i . S t q C e n L o c   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . s t q c e n c a d   s c c   O N   s c c . C o d i g o = s c l . C o d i g o   ' ,  
         '   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x         s g g x   O N   s g g x . C o n t r o l e = v m i . G r a G r u X   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1         s g g 1   O N   s g g 1 . N i v e l 1 = s g g x . G r a G r u 1   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d         s m e d   O N   s m e d . C o d i g o = s g g 1 . U n i d M e d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u c         s g g c   O N   s g g c . C o n t r o l e = s g g x . G r a G r u C   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a c o r c a d     s g c c   O N   s g c c . C o d i g o = s g g c . G r a C o r C a d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a t a m i t s     s g t i   O N   s g t i . C o n t r o l e = s g g x . G r a T a m I   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x c o u   s x c o   O N   s x c o . G r a G r u X = s g g x . C o n t r o l e     ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 1         s n v 1   O N   s n v 1 . C o d i g o = s x c o . C o u N i v 1       ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 2         s n v 2   O N   s n v 2 . C o d i g o = s x c o . C o u N i v 2       ' ,  
         '   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x         d g g x   O N   d g g x . C o n t r o l e = c a b . G r a G r u X   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1         d g g 1   O N   d g g 1 . N i v e l 1 = d g g x . G r a G r u 1   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d         d m e d   O N   d m e d . C o d i g o = d g g 1 . U n i d M e d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u c         d g g c   O N   d g g c . C o n t r o l e = d g g x . G r a G r u C   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a c o r c a d     d g c c   O N   d g c c . C o d i g o = d g g c . G r a C o r C a d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a t a m i t s     d g t i   O N   d g t i . C o n t r o l e = d g g x . G r a T a m I   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x c o u   d x c o   O N   d x c o . G r a G r u X = d g g x . C o n t r o l e     ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 1         d n v 1   O N   d n v 1 . C o d i g o = d x c o . C o u N i v 1       ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 2         d n v 2   O N   d n v 2 . C o d i g o = d x c o . C o u N i v 2       ' ,  
         '   ' ,  
         ' W H E R E   ( v m i . E m p r e s a = '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         ' A N D   v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v ) ) ,  
         S Q L _ P e r i o d o V S ,  
     ' )   O R   ' ,  
     ' (   ' ,  
     ' v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v ) ) ,  
     ' A N D   c a b . M o v i m C o d   I N   (   ' ,  
     ' S E L E C T   D I S T I N C T   V S M o v C o d     ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . e m i t     ' ,  
     ' W H E R E   C o d i g o   I N   (       ' ,  
     '     S E L E C T   O r i g e m C o d i     ' ,  
     '     F R O M   '   +   T M e u D B   +   ' . p q x     ' ,  
     '     W H E R E   T i p o = 1 1 0           ' ,  
     '     '   +   S Q L _ P e r i o d o P Q ,  
     ' )       ' ,  
     ' A N D   V S M o v C o d   < >   0     ' ,  
     '     ' ,  
     ' U N I O N       ' ,  
     '     ' ,  
     ' S E L E C T   D I S T I N C T   V S M o v C o d     ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . p q o     ' ,  
     ' W H E R E   C o d i g o   I N   (       ' ,  
     '     S E L E C T   O r i g e m C o d i     ' ,  
     '     F R O M   '   +   T M e u D B   +   ' . p q x     ' ,  
     '     W H E R E   T i p o = 1 1 0           ' ,  
     '     '   +   S Q L _ P e r i o d o P Q ,  
     ' )       ' ,  
     ' A N D   V S M o v C o d   < >   0     ' ,  
     / / / /  
     ' )   ' ,  
     ' )   ' ,  
     ' ' ] ) ;  
     e n d ;  
 c o n s t  
     s P r o c N a m e   =   ' F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a . P r o d u c a o S i n g u l a r _ I m p o r t a O p e P r o c B ( ) ' ;  
     / / S r c F a t o r   =   - 1 ;  
     D s t F a t o r   =   1 ;  
     E S T S T a b S o r c   =   e s t s V M I ;  
 v a r  
     P e c a s ,   A r e a M 2 ,   P e s o K g ,   Q t d e S r c ,   Q t d e D s t ,   Q t d A n t A r M 2 ,   Q t d A n t P e s o ,   Q t d A n t P e c a :  
     D o u b l e ;  
     D a t a ,   D t H r A b e r t o ,   D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;  
     S r c T i p o E s t q ,   D s t T i p o E s t q ,   G G X S r c ,   G G X D s t ,   C o d i g o ,   M o v i m C o d ,   I D S e q 1 ,  
     I D _ I t e m ,   C o n t r o l e ,   S r c F a t o r ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o :   I n t e g e r ;  
     M o v i m N i v :   T E s t q M o v i m N i v ;  
     T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;  
     C O D _ I N S _ S U B S T ,   S Q L x :   S t r i n g ;  
     M o v i m I D :   T E s t q M o v i m I D ;  
     M o v i m N i v I n n :   T E s t q M o v i m N i v ;  
 b e g i n  
     C O D _ I N S _ S U B S T   : =   ' ' ;  
     / /  
     c a s e   P s q M o v i m N i v   o f  
         T E s t q m o v i m N i v . e m i n S o r c C a l :   S r c F a t o r   : =   1 ;  
         T E s t q m o v i m N i v . e m i n D e s t C a l :   S r c F a t o r   : =   - 1 ;  
         T E s t q m o v i m N i v . e m i n S o r c C u r :   S r c F a t o r   : =   1 ;  
         e l s e  
         b e g i n  
             S r c F a t o r   : =   - 1 ;  
             G e r a l . M B _ E r r o ( ' " S r c F a t o r "   n � o   i m p l e m e n t a d o   e m   " P r o d u c a o S i n g u l a r _ I m p o r t a O p e P r o c B ( ) " ' ) ;  
         e n d ;  
     e n d ;  
     c a s e   P s q M o v i m N i v   o f  
         T E s t q m o v i m N i v . e m i n S o r c C a l :   S Q L x   : =  
             P a r t e S Q L ( ' v s c a l c a b ' ,   ' v m i . R m s G G X ' ,   ' c a b . G r a G r u X ' ,  
             ' v m i . Q t d A n t A r M 2 ' ,   ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   ' v m i . Q t d A n t A r M 2 ' ,  
             ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   T E s t q m o v i m N i v . e m i n S o r c C a l ) ;  
         T E s t q m o v i m N i v . e m i n D e s t C a l :   S Q L x   : =  
             P a r t e S Q L ( ' v s c a l j m p ' ,   ' v m i . J m p G G X ' ,   ' v m i . G r a G r u X ' ,   ' - v m i . Q t d G e r A r M 2 ' ,  
             ' - v m i . Q t d G e r P e s o ' ,   ' - v m i . Q t d G e r P e c a ' ,     ' v m i . Q t d G e r A r M 2 ' ,  
             ' v m i . Q t d G e r P e s o ' ,   ' v m i . Q t d G e r P e c a ' ,   T E s t q m o v i m N i v . e m i n D e s t C a l ) ;  
         T E s t q m o v i m N i v . e m i n S o r c C u r :   S Q L x   : =  
             P a r t e S Q L ( ' v s c u r c a b ' ,   ' v m i . R m s G G X ' ,   ' c a b . G r a G r u X ' ,  
             ' v m i . Q t d A n t A r M 2 ' ,   ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   ' v m i . Q t d A n t A r M 2 ' ,  
             ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   T E s t q m o v i m N i v . e m i n S o r c C u r ) ;  
         e l s e  
         b e g i n  
             S Q L x   : =   ' S E L E C T   ? ? ? ?   F R O M   ? ? ? ? ' ;  
             G e r a l . M B _ E r r o ( ' " M o v i m N i v "   n � o   i m p l e m e n t a d o   e m   " P r o d u c a o S i n g u l a r _ I m p o r t a O p e P r o c B ( ) " ' ) ;  
         e n d ;  
     e n d ;  
  
     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r M C ,   D M o d G . M y P I D _ D B ,   [  
     ' D R O P   T A B L E   I F   E X I S T S   _ S P E D _ E F D _ K 2 X X _ O _ P ;     ' ,  
     ' C R E A T E   T A B L E   _ S P E D _ E F D _ K 2 X X _ O _ P     ' ,  
     / /  
     S Q L x ,  
     ' ;   ' ,  
     '   ' ,  
     ' S E L E C T   D I S T I N C T   M o v i m C o d     ' ,  
     ' F R O M   _ S P E D _ E F D _ K 2 X X _ O _ P   ' ,  
     ' O R D E R   B Y   M o v i m C o d ;     ' ,  
     ' ' ] ) ;  
     / / G e r a l . M B _ S Q L ( S e l f ,   Q r M C ) ;   / / P a r e i   a q u i  
     / /  
     i f   S P E D _ E F D _ P F . E r r G r a n d e z a ( ' _ S P E D _ E F D _ K 2 X X _ O _ P ' ,   ' G G x _ S r c ' ,   ' G G X _ D s t '   )   t h e n  
         E x i t ;  
     / /  
     P B 2 . P o s i t i o n   : =   0 ;  
     P B 2 . M a x   : =   Q r M C . R e c o r d C o u n t ;  
     Q r M C . F i r s t ;  
     / / i f   Q r M C . R e c o r d C o u n t   >   0   t h e n  
         / / G e r a l . M B _ A v i s o ( ' D E P R R C A R ! ! !   O p e P r o c A   -   1 ' ) ;  
     w h i l e   n o t   Q r M C . E o f   d o  
     b e g i n  
         M y O b j e c t s . U p d P B ( P B 2 ,   L a A v i s o 3 ,   L a A v i s o 4 ) ;  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r P r o d R i b ,   D M o d G . M y P I D _ D B ,   [  
         ' S E L E C T   *   ' ,  
         ' F R O M   _ S P E D _ E F D _ K 2 X X _ O _ P   ' ,  
         ' W H E R E   M o v i m C o d = '   +   G e r a l . F F 0 ( Q r M C M o v i m C o d . V a l u e ) ,  
         ' ' ] ) ;  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q r P r o d R i b ) ;   / / P a r e i   a q u i !  
         Q r P r o d R i b . F i r s t ;  
         w h i l e   n o t   Q r P r o d R i b . E o f   d o  
         b e g i n  
             M o v i m I D           : =   T E s t q M o v i m I D ( Q r P r o d R i b M o v i m I D . V a l u e ) ;  
             A r e a M 2             : =   Q r P r o d R i b A r e a M 2 . V a l u e ;  
             P e s o K g             : =   Q r P r o d R i b P e s o K g . V a l u e ;  
             P e c a s               : =   Q r P r o d R i b P e c a s . V a l u e ;  
             Q t d A n t A r M 2     : =   Q r P r o d R i b Q t d A n t A r M 2 . V a l u e ;  
             Q t d A n t P e s o     : =   Q r P r o d R i b Q t d A n t P e s o . V a l u e ;  
             Q t d A n t P e c a     : =   Q r P r o d R i b Q t d A n t P e c a . V a l u e ;  
             D a t a                 : =   Q r P r o d R i b D a t a . V a l u e ;  
             S r c T i p o E s t q   : =   T r u n c ( Q r P r o d R i b s T i p o E s t q . V a l u e ) ;  
             D s t T i p o E s t q   : =   T r u n c ( Q r P r o d R i b d T i p o E s t q . V a l u e ) ;  
             G G X S r c             : =   Q r P r o d R i b G G X _ S r c . V a l u e ;  
             G G X D s t             : =   Q r P r o d R i b G G X _ D s t . V a l u e ;  
             M o v i m C o d         : =   Q r P r o d R i b M o v i m C o d . V a l u e ;  
             M o v i m N i v         : =   T E s t q M o v i m N i v ( Q r P r o d R i b M o v i m N i v . V a l u e ) ;  
             C o d i g o             : =   Q r P r o d R i b C o d i g o . V a l u e ;  
             C o n t r o l e         : =   Q r P r o d R i b C o n t r o l e . V a l u e ;  
             I D _ I t e m           : =   C o n t r o l e ;  
             / /  
             D t M o v i m           : =   D a t a ;  
             D t C o r r A p o       : =   Q r P r o d R i b D t C o r r A p o . V a l u e ;  
             / /  
             C l i e n t M O         : =   Q r P r o d R i b C l i e n t M O . V a l u e ;  
             F o r n e c M O         : =   Q r P r o d R i b F o r n e c M O . V a l u e ;  
             E n t i S i t i o       : =   Q r P r o d R i b E n t i S i t i o . V a l u e ;  
             / /  
             i f   ( C l i e n t M O   = 0 )   o r   ( F o r n e c M O = 0 )   o r   ( E n t i S i t i o = 0 )   t h e n  
             b e g i n  
                 M o v i m N i v I n n   : =   V S _ P F . O b t e m M o v i m N i v I n n D e M o v i m I D ( M o v i m I D ) ;  
                 U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r O b t C l i F o r S i t e ,   D m o d . M y D B ,   [  
                 ' S E L E C T   v m i . C l i e n t M O ,   v m i . F o r n e c M O ,   s c c . E n t i S i t i o     ' ,  
                 ' F R O M   v s m o v i t s   v m i   ' ,  
                 ' L E F T   J O I N   s t q c e n l o c   s c l   O N   s c l . C o n t r o l e = v m i . S t q C e n L o c     ' ,  
                 ' L E F T   J O I N   s t q c e n c a d   s c c   O N   s c c . C o d i g o = s c l . C o d i g o     ' ,  
                 ' W H E R E   v m i . M o v i m C o d = '   +   G e r a l . F F 0 ( M o v i m C o d ) ,  
                 ' A N D   v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v I n n ) ) ,  
                 ' ' ] ) ;  
                 C l i e n t M O     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' C l i e n t M O ' ) . A s I n t e g e r ;  
                 F o r n e c M O     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' F o r n e c M O ' ) . A s I n t e g e r ;  
                 E n t i S i t i o     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' E n t i S i t i o ' ) . A s I n t e g e r ;  
             e n d ;  
             / /  
             / / i f   ( P e c a s   < >   0 )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
             i f   ( ( P e c a s   < >   0 )   o r   ( M o v i m I D = T E s t q M o v i m I D . e m i d E m P r o c S P ) )   a n d   ( D a t a   >   1 )   t h e n  
             b e g i n  
                 c a s e   S r c T i p o E s t q   o f  
                     1 :   Q t d e S r c   : =   A r e a M 2 ;  
                     2 :   Q t d e S r c   : =   P e s o K g ;  
                     e l s e  
                     b e g i n  
                         G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( S r c T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                         +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ S r c T i p o E s t q ]   +   s L i n e B r e a k   +  
                         ' R e d u z i d o   =   '   +   G e r a l . F F 0 ( G G X D s t )   +   s L i n e B r e a k   +  
                         ' M o v i m C o d   =   '   +   G e r a l . F F 0 ( M o v i m C o d )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
                         Q r P r o d R i b . S Q L . T e x t ) ;  
                         / /  
                         Q t d e D s t   : =   P e c a s ;  
                     e n d ;  
                 e n d ;  
                 Q t d e S r c   : =   Q t d e S r c   *   S r c F a t o r ;  
  
                 / /  
                 c a s e   D s t T i p o E s t q   o f  
                     1 :   Q t d e D s t   : =   Q t d A n t A r M 2 ;  
                     2 :   Q t d e D s t   : =   Q t d A n t P e s o ;  
                     e l s e  
                     b e g i n  
                         G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( S r c T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                         +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ S r c T i p o E s t q ]   +   s L i n e B r e a k   +  
                         ' R e d u z i d o   =   '   +   G e r a l . F F 0 ( G G X D s t )   +   s L i n e B r e a k   +  
                         ' M o v i m C o d   =   '   +   G e r a l . F F 0 ( M o v i m C o d )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
                         Q r P r o d R i b . S Q L . T e x t ) ;  
                         / /  
                         Q t d e D s t   : =   Q t d A n t P e c a ;  
                     e n d ;  
                 e n d ;  
                 Q t d e D s t   : =   Q t d e D s t   *   D s t F a t o r ;  
             e n d   e l s e  
             b e g i n  
                 Q t d e S r c   : =   0 ;  
                 Q t d e D s t   : =   0 ;  
             e n d ;  
             / /  
             i f   Q t d e S r c   <   0   t h e n  
                 M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                 ' Q u a n t i d a d e   " S r c "   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 5 ) '   +   s L i n e B r e a k   +  
                 ' Q t d e :   '   +   G e r a l . F F T ( Q t d e S r c ,   3 ,   s i N e g a t i v o ) ,   Q r P r o d R i b ) ;  
             i f   Q t d e D s t   <   0   t h e n  
                 M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                 ' Q u a n t i d a d e   " D s t "   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 6 ) '   +   s L i n e B r e a k   +  
                 ' Q t d e :   '   +   G e r a l . F F T ( Q t d e D s t ,   3 ,   s i N e g a t i v o ) ,   Q r P r o d R i b )  
             e l s e   i f   ( Q t d e D s t   =   0 )   a n d   ( D a t a   >   1 )   t h e n  
             b e g i n  
                 c a s e   M o v i m I D   o f  
                     e m i d E m P r o c C a l :   D a t a   : =   0 ;  
                     e m i d E m P r o c C u r :   D a t a   : =   0 ;  
                     e l s e   M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                     ' Q u a n t i d a d e   z e r a d a   p a r a   O P   e n c e r r a d a ! ! !   ( 4 ) ' ,   Q r P r o d R i b ) ;  
                 e n d ;  
             e n d ;  
             / /  
             / / i f   Q r S u b P r d O P E m p r e s a . V a l u e   =   Q r F M O F o r n e c M O . V a l u e   t h e n  
  
             i f   F E m p r e s a   =   F o r n e c M O   t h e n  
                 T i p o R e g S P E D P r o d   : =   t r s p 2 3 X  
             e l s e  
                 T i p o R e g S P E D P r o d   : =   t r s p 2 5 X ;  
             / /  
             D t H r A b e r t o   : =   D a t a ;  
             c a s e   T i p o R e g S P E D P r o d   o f  
                 t r s p 2 3 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                     C o n t r o l e ,   D t H r A b e r t o ,   D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o ,   G G X D s t ,  
                     C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   Q t d e D s t ,   O r i g e m O p e P r o c ,  
                     F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   I D S e q 1 ) ;  
                     / /  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   ( * L i n A r q P a i * ) I D S e q 1 ,   D t H r A b e r t o ,  
                     D t C o r r A p o ,   G G X S r c ,   Q t d e S r c ,   C O D _ I N S _ S U B S T ,   I D _ I t e m ,   I n t e g e r ( M o v i m I D ) ,  
                     C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,  
                     O r i g e m O p e P r o c ,   E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                     / /  
                     I n s e r e _ O r i g e P r o c P Q ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   M o v i m N i v ,   M o v i m C o d ,  
                     ( * L i n A r q P a i * ) I D S e q 1 ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ) ;  
                 e n d ;  
                 t r s p 2 5 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,  
                     D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o ,   G G X D s t ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,  
                     Q t d e D s t ,   O r i g e m O p e P r o c ,   F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   ( * L i n A r q P a i * ) I D S e q 1 ) ;  
                     / /  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   ( * L i n A r q P a i * ) I D S e q 1 ,   D t H r A b e r t o ,   D t C o r r A p o ,   G G X S r c ,  
                     Q t d e S r c ,   C O D _ I N S _ S U B S T ,   I D _ I t e m ,  
                     I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,  
                     E n t i S i t i o ,   O r i g e m O p e P r o c ,   E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                     / /  
                     I n s e r e _ O r i g e P r o c P Q ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   M o v i m N i v ,   M o v i m C o d ,  
                     ( * L i n A r q P a i * ) I D S e q 1 ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ) ;  
                 e n d ;  
                 e l s e   G e r a l . M B _ E r r o ( ' T i p o   d e   r e g i s t r o   S P E D   E F D   ( '   +   G e r a l . F F 0 ( I n t e g e r (  
                 T i p o R e g S P E D P r o d ) )   +   ' )   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e   +   ' ( 5 ) ' ) ;  
             e n d ;  
             Q r P r o d R i b . N e x t ;  
         e n d ;  
         Q r M C . N e x t ;  
     e n d ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a . I n s e r e _ E m O p e P r o c _ 2 1 5 (  
     c o n s t   T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;  
     M o v i m N i v :   T E s t q M o v i m N i v ;   c o n s t   Q r C a b :   T m y S Q L Q u e r y ;   c o n s t   S Q L _ P e r i o d o :   S t r i n g ;  
     c o n s t   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;   v a r   I D S e q 1 :   I n t e g e r ) :   B o o l e a n ;  
 c o n s t  
     E S T S T a b S o r c   =   e s t s V M I ;  
     s P r o c N a m e   =   ' F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a . I n s e r e _ O r i g e P r o c _ 2 1 5 ' ;  
 v a r  
     D t H r A b e r t o ,   D t H r F i m O p e ,   D a t a ,   D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;  
     G r a G r u X ,   G G X C a b D s t ,   I D _ I t e m ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,  
     E n t i S i t i o :   I n t e g e r ;  
     M y Q t d ,   Q t d e ,   F a t o r :   D o u b l e ;  
     C O D _ I N S _ S U B S T ,   T a b C a b :   S t r i n g ;  
     / /  
     f u n c t i o n   F i l t r o P e r i o d o ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
     b e g i n  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 1 X :   R e s u l t   : =   S Q L _ P e r i o d o ;  
             / / t r s p 2 3 X :   R e s u l t   : =   S Q L _ P e r i o d o ;  
             / / t r s p 2 5 X :   R e s u l t   : =   S Q L _ P e r i o d o ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ E r r o ( ' " F i l t r o R e g "   i n d e f i n i d o :   '   +  
                     G e r a l . F F 0 ( I n t e g e r ( T i p o R e g S P E D P r o d ) )   +   ' !   ( 3 ) ' ) ;  
                 R e s u l t   : =   ' # E R R _ P E R I O D O _ ' ;  
             e n d ;  
         e n d ;  
     e n d ;  
 b e g i n  
     R e s u l t   : =   T r u e ;  
     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r O r i ,   D m o d . M y D B ,   [  
     ' S E L E C T   c a b . G r a G r u X   G G X S r c ,   c a b . G G X D s t ,     ' ,  
     ' v m i . C o d i g o ,   v m i . M o v i m C o d ,   v m i . C o n t r o l e ,   v m i . G r a G r u X ,   v m i . S r c N i v e l 2 ,     ' ,  
     ' P e c a s ,   P e s o K g ,   A r e a M 2 ,   D A T E ( D a t a H o r a )   D A T A ,   v m i . D t C o r r A p o ,   ' ,  
             V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( F a l s e ,   ' T i p o E s t q ' ,   T r u e ,  
             ' A r e a M 2 ' ,   ' P e s o K g ' ,   ' P e c a s ' ) ,  
     ' v m i . C l i e n t M O ,   v m i . F o r n e c M O ,   s c c . E n t i S i t i o ,   m e d . G r a n d e z a   ' ,  
     ' F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   v m i         ' ,  
     ' L E F T   J O I N   v s o p e c a b       c a b   O N   c a b . M o v i m C o d = v m i . M o v i m C o d     ' ,  
     ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v m i . G r a G r u X   ' ,  
     / / ' L E F T   J O I N   g r a g r u x   g g x   O N   g g x . C o n t r o l e = c a b . G r a G r u X   ' ,  
     ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1   ' ,  
     ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d   ' ,  
     ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
     ' L E F T   J O I N   s t q c e n l o c     s c l   O N   s c l . C o n t r o l e = v m i . S t q C e n L o c   ' ,  
     ' L E F T   J O I N   s t q c e n c a d     s c c   O N   s c c . C o d i g o = s c l . C o d i g o   ' ,  
     ' W H E R E   v m i . M o v i m C o d = '   +   G e r a l . F F 0 ( Q r C a b . F i e l d B y N a m e ( ' M o v i m C o d ' ) . A s I n t e g e r ) ,  
     S Q L _ P e r i o d o ,  
     ' A N D   v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v ) ) ,  
     ' ' ] ) ;  
     / / G e r a l . M B _ S Q L ( S e l f ,   Q r O r i ) ;  
     Q r O r i . F i r s t ;  
     w h i l e   n o t   Q r O r i . E o f   d o  
     b e g i n  
         C o d i g o         : =   Q r O r i . F i e l d B y N a m e ( ' C o d i g o ' ) . A s I n t e g e r ;  
         M o v i m C o d     : =   Q r O r i . F i e l d B y N a m e ( ' M o v i m C o d ' ) . A s I n t e g e r ;  
         C o n t r o l e     : =   Q r O r i . F i e l d B y N a m e ( ' C o n t r o l e ' ) . A s I n t e g e r ;  
         I D _ I t e m       : =   C o n t r o l e ;  
         / /  
         F a t o r                   : =   1 ;  
         D a t a                     : =   Q r O r i . F i e l d B y N a m e ( ' D A T A ' ) . A s D a t e T i m e ;  
         G G X C a b D s t           : =   Q r O r i . F i e l d B y N a m e ( ' G G X D s t ' ) . A s I n t e g e r ;  
         D t M o v i m               : =   D a t a ;  
         D t C o r r A p o           : =   Q r O r i . F i e l d B y N a m e ( ' D t C o r r A p o ' ) . A s D a t e T i m e ;  
         C l i e n t M O             : =   Q r O r i . F i e l d B y N a m e ( ' C l i e n t M O ' ) . A s I n t e g e r ;  
         F o r n e c M O             : =   Q r O r i . F i e l d B y N a m e ( ' F o r n e c M O ' ) . A s I n t e g e r ;  
         E n t i S i t i o           : =   Q r O r i . F i e l d B y N a m e ( ' E n t i S i t i o ' ) . A s I n t e g e r ;  
         i f   M o v i m I D   =   e m i d E m P r o c C u r   t h e n  
         b e g i n  
             U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r C a l T o C u r ,   D m o d . M y D B ,   [  
             ' S E L E C T   D s t G G X   ' ,  
             ' F R O M   '   +   C O _ S E L _ T A B _ V M I ,  
             ' W H E R E   C o n t r o l e = '   +   G e r a l . F F 0 ( Q r O r i . F i e l d B y N a m e ( ' S r c N i v e l 2 ' ) . A s I n t e g e r ) ,  
             ' ' ] ) ;  
             G r a G r u X   : =   Q r C a l T o C u r D s t G G X . V a l u e ;  
         e n d  
         e l s e  
             G r a G r u X           : =   Q r O r i . F i e l d B y N a m e ( ' G r a G r u X ' ) . A s I n t e g e r ;  
         C O D _ I N S _ S U B S T   : =   ' ' ;  
         c a s e   M o v i m N i v   o f  
             / / e m i n E m O p e r B x a ,  
             / / e m i n E m W E n d B x a ,  
             e m i n D e s t O p e r :  
             b e g i n  
                 F a t o r   : =   1 ;  
                 i f   G G X C a b D s t   < >   G r a G r u X   t h e n  
                     i f   G G X C a b D s t   < >   0   t h e n  
                         C O D _ I N S _ S U B S T   : =   G e r a l . F F 0 ( G G X C a b D s t ) ;  
             e n d ;  
 ( *  
             e m i n D e s t C a l ,  
             e m i n D e s t C u r :  
             b e g i n  
               C O D _ I N S _ S U B S T   : =   ' ' ;   / /   N a d a ? ? ?   E h   o   G r a G r u X !  
               F a t o r   : =   ? ;  
             e n d ;  
 * )  
             e l s e  
             b e g i n  
                 R e s u l t   : =   F a l s e ;  
                 G e r a l . M B _ E r r o ( ' " M o v i m N i v " :   '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v ) )   +  
                 '   n � o   i m p l e m e t a d o   e m   " I n s e r e _ O r i g e P r o c ( ) "   ( 1 ) ' ) ;  
             e n d ;  
         e n d ;  
         / / q u a l   u s a r ?   a r e a   o u   p e s o ?  
         M y Q t d   : =   0 ;  
         c a s e     Q r O r i . F i e l d B y N a m e ( ' G r a n d e z a ' ) . A s I n t e g e r   o f  
             0 :   M y Q t d   : =   Q r O r i . F i e l d B y N a m e ( ' P e c a s ' ) . A s F l o a t ;  
             1 :   M y Q t d   : =   Q r O r i . F i e l d B y N a m e ( ' A r e a M 2 ' ) . A s F l o a t ;  
             2 :   M y Q t d   : =   Q r O r i . F i e l d B y N a m e ( ' P e s o K g ' ) . A s F l o a t ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ A v i s o ( ' G r a n d e z a   n � o   i m p l e m e n t a d a :   '   +  
                 s G r a n d e z a U n i d M e d [ Q r O r i . F i e l d B y N a m e ( ' G r a n d e z a ' ) . A s I n t e g e r ]   +   s L i n e B r e a k   +  
             ' R e d u z i d o :   '   +   G e r a l . F F 0 ( G r a G r u X )   +   s L i n e B r e a k ) ;  
             e n d ;  
         e n d ;  
         / /  
         Q t d e   : =   M y Q t d   *   F a t o r ;  
         i f   Q t d e   <   0   t h e n  
             G e r a l . M B _ E R R O ( ' Q u a n t i d a d e   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 5 ) '   +   s L i n e B r e a k   +  
             ' M o v i m N i v :   '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v ) )   +   s L i n e B r e a k   +  
             s p r o c N a m e   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
             ' G r a n d e z a :   '   +   s G r a n d e z a U n i d M e d [ Q r O r i . F i e l d B y N a m e ( ' G r a n d e z a ' ) . A s I n t e g e r ]   +   s L i n e B r e a k   +  
             ' R e d u z i d o :   '   +   G e r a l . F F 0 ( G r a G r u X )   +   s L i n e B r e a k   +  
             ' P e � a s :   '   +   G e r a l . F F T ( Q r O r i . F i e l d B y N a m e ( ' P e c a s ' ) . A s F l o a t ,   3 ,   s i N e g a t i v o )   +   s L i n e B r e a k   +  
             ' � r e a   m � :   '   +   G e r a l . F F T ( Q r O r i . F i e l d B y N a m e ( ' A r e a M 2 ' ) . A s F l o a t ,   3 ,   s i N e g a t i v o )   +   s L i n e B r e a k   +  
             ' P e s o   k g :   '   +   G e r a l . F F T ( Q r O r i . F i e l d B y N a m e ( ' P e s o K g ' ) . A s F l o a t ,   3 ,   s i N e g a t i v o )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
             ' Q t d e :   '   +   G e r a l . F F T ( M y Q t d ,   3 ,   s i N e g a t i v o )   +   '   *   '   +  
             G e r a l . F F T ( F a t o r ,   3 ,   s i N e g a t i v o )   +   '   =   '   +  
             G e r a l . F F T ( Q t d e ,   3 ,   s i N e g a t i v o )   +  
             s L i n e B r e a k   +   ' A v i s e   a   D E R M A T E K ! ! ! ' ) ;  
             I D _ I t e m   : =   Q r O r i . F i e l d B y N a m e ( ' C o n t r o l e ' ) . A s I n t e g e r ;  
         i f   Q t d e   <   0 . 0 0 1   t h e n  
             G e r a l . M B _ E R R O ( ' Q u a n t i d a d e   n � o   p o d e   s e r   z e r o ! ! !   ( 3 ) '   +   s L i n e B r e a k   +  
             ' M o v i m N i v :   '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v ) )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
             ' G r a n d e z a :   '   +   s G r a n d e z a U n i d M e d [ Q r O r i . F i e l d B y N a m e ( ' G r a n d e z a ' ) . A s I n t e g e r ]   +   s L i n e B r e a k   +  
             ' R e d u z i d o :   '   +   G e r a l . F F 0 ( G r a G r u X )   +   s L i n e B r e a k   +  
             ' P e � a s :   '   +   G e r a l . F F T ( Q r O r i . F i e l d B y N a m e ( ' P e c a s ' ) . A s F l o a t ,   3 ,   s i N e g a t i v o )   +   s L i n e B r e a k   +  
             ' � r e a   m � :   '   +   G e r a l . F F T ( Q r O r i . F i e l d B y N a m e ( ' A r e a M 2 ' ) . A s F l o a t ,   3 ,   s i N e g a t i v o )   +   s L i n e B r e a k   +  
             ' P e s o   k g :   '   +   G e r a l . F F T ( Q r O r i . F i e l d B y N a m e ( ' P e s o K g ' ) . A s F l o a t ,   3 ,   s i N e g a t i v o )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
             ' Q t d e :   '   +   G e r a l . F F T ( M y Q t d ,   3 ,   s i N e g a t i v o )   +   '   *   '   +  
             G e r a l . F F T ( F a t o r ,   3 ,   s i N e g a t i v o )   +   '   =   '   +  
             G e r a l . F F T ( Q t d e ,   3 ,   s i N e g a t i v o )   +   s L i n e B r e a k   +  
             s p r o c N a m e   +   s L i n e B r e a k   +  
             s L i n e B r e a k   +   ' A v i s e   a   D E R M A T E K ! ! ! ' ) ;  
         / /  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 1 X :  
             b e g i n  
                 S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 1 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                 F P e r i A p u ,   ( * L i n A r q P a i * ) I D S e q 1 ,   D a t a ,   D t M o v i m ,   D t C o r r A p o ,   G r a G r u X ,  
                 Q t d e ,   C O D _ I N S _ S U B S T ,   I D _ I t e m ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                 C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   O r i g e m O p e P r o c ,   o i d k 2 1 0 P r o c V S ,  
                 E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
             e n d ;  
             e l s e  
                 G e r a l . M B _ E r r o ( ' T i p o   d e   r e g i s t r o   S P E D   E F D   ( '   +   G e r a l . F F 0 ( I n t e g e r (  
                 T i p o R e g S P E D P r o d ) )   +   ' )   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e   +   ' ( 6 ) ' ) ;  
         e n d ;  
         / /  
         Q r O r i . N e x t ;  
     e n d ;  
 e n d ;  
  
 p r o c e d u r e   T F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a . I n s e r e _ E m O p e P r o c _ P r d (  
     c o n s t   T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;  
     c o n s t   Q r y :   T m y S Q L Q u e r y ;   c o n s t   F a t o r :   D o u b l e ;   c o n s t   S Q L _ P e r i o d o ,  
     S Q L _ P e r i o d o P Q :   S t r i n g ;   c o n s t   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;  
     c o n s t   M o v i m N i v :   T E s t q M o v i m N i v ) ;  
     / /  
     p r o c e d u r e   I n s e r e _ E m O p e P r o c ( c o n s t   T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;   c o n s t  
                         M o v i m I D :   T E s t q M o v i m I D ;   c o n s t   Q r y :   T m y S Q L Q u e r y ;   c o n s t   F a t o r :  
                         D o u b l e ;   c o n s t   S Q L _ P e r i o d o ,   S Q L _ C o r r A p o :   S t r i n g ;  
                         c o n s t   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;  
                         c o n s t   I n s u m o S e m P r o d u c a o :   B o o l e a n ;   v a r   I D S e q 1 :   I n t e g e r ) ;  
     c o n s t  
         s P r o c N a m e   =   ' F m S p e d E f d I c m s I p i P r o d u c a o I s o l a d a . I n s e r e _ E m O p e P r o c _ P r d   >   I n s e r e _ E m O p e P r o c ( ) ' ;  
     v a r  
         D t H r A b e r t o ,   D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o P r o d ,   D t C o r r A p o C o n s :   T D a t e T i m e ;  
         Q t d e P r o d ,   Q t d e C o n s ,   P e c a s ,   P e s o K g ,   A r e a M 2 :   D o u b l e ;  
         M y F a t o r ,   T i p o E s t q ,   C o d i g o ,   M o v i m C o d ,   G G X D s t ,   C l i e n t M O ,   F o r n e c M O ,  
         E n t i S i t i o :   I n t e g e r ;   E S T S T a b S o r c :   T E s t q S P E D T a b S o r c ;   I D _ I t e m ,   C o n t r o l e :   I n t e g e r ;  
         / /  
         f u n c t i o n   F i l t r o P e r i o d o ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
         b e g i n  
             c a s e   T i p o R e g S P E D P r o d   o f  
                 t r s p 2 1 X :   R e s u l t   : =   ' ' ;  
                 t r s p 2 3 X :   R e s u l t   : =   S Q L _ P e r i o d o ;  
                 / / t r s p 2 5 X :   R e s u l t   : =   ' ' ;  
                 t r s p 2 5 X :   R e s u l t   : =   S Q L _ P e r i o d o ;  
                 t r s p 2 6 X :   R e s u l t   : =   S Q L _ P e r i o d o ;  
                 e l s e  
                 b e g i n  
                     G e r a l . M B _ E r r o ( ' " F i l t r o R e g "   i n d e f i n i d o :   '   +  
                         G e r a l . F F 0 ( I n t e g e r ( T i p o R e g S P E D P r o d ) )   +   ' !   ( 4 ) ' ) ;  
                     R e s u l t   : =   ' # E R R _ P E R I O D O _ ' ;  
                 e n d ;  
             e n d ;  
         e n d ;  
  
         p r o c e d u r e   I n s e r e A t u a l _ 2 X 0 ( ) ;  
         b e g i n  
             D t M o v i m         : =   F D i a I n i ;   / /   v e r   a q u i   q u a n d o   p e r i o d o   n a o   f o r   m e n s a l !  
             / / P a r e i   a q u i   ! ! !     �   a s i m m ? ? ?  
             i f   ( P e c a s   < >   0 )   o r   ( M o v i m I D = T E s t q M o v i m I D . e m i d E m P r o c S P )   ( * a n d   ( D t H r F i m O p e   >   1 ) * )   t h e n  
             b e g i n  
                 c a s e   T i p o E s t q   o f  
                     1 :   Q t d e P r o d   : =   A r e a M 2 ;  
                     2 :   Q t d e P r o d   : =   P e s o K g ;  
                     e l s e  
                     b e g i n  
                         G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                         +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ T i p o E s t q ]   +   s L i n e B r e a k   +  
                         ' R e d u z i d o   =   '   +   G e r a l . F F 0 ( G G X D s t )   +   s L i n e B r e a k   +  
                         ' M o v i m C o d   =   '   +   G e r a l . F F 0 ( M o v i m C o d )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
                         Q r y . S Q L . T e x t ) ;  
                         / /  
                         Q t d e P r o d   : =   P e c a s ;  
                     e n d ;  
                 e n d ;  
                 Q t d e P r o d   : =   Q t d e P r o d   *   M y F a t o r ;  
             e n d   e l s e  
                 Q t d e P r o d   : =   0 ;  
             / /  
             i f   Q t d e P r o d   <   0   t h e n  
                 M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                 ' Q u a n t i d a d e   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 8 ) '   +   s L i n e B r e a k   +  
                 ' Q t d e :   '   +   G e r a l . F F T ( Q t d e P r o d ,   3 ,   s i N e g a t i v o ) ,   n i l )  
             e l s e   i f   ( Q t d e P r o d   =   0 )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
             b e g i n  
                 c a s e   M o v i m I D   o f  
                     e m i d E m P r o c C a l :   D t H r F i m O p e   : =   0 ;  
                     e m i d E m P r o c C u r :   D t H r F i m O p e   : =   0 ;  
                     e l s e   M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                 ' Q u a n t i d a d e   z e r a d a   p a r a   O P   e n c e r r a d a ! ! !   ( 6 ) ' ,   n i l ) ;  
                 e n d ;  
             e n d ;  
             / /  
             c a s e   T i p o R e g S P E D P r o d   o f  
         {  
                 t r s p 2 1 X :  
                 b e g i n  
             E S T S T a b S o r c   : =   T E s t q S P E D T a b S o r c . e s t s N D ;  
             I D _ I t e m           : =   0 ;  
             C o n t r o l e         : =   0 ;  
                     I n s e r e I t e m A t u a l _ K 2 1 5 ( L i n A r q P a i ,   D t H r A b e r t o ( * D a t a * ) ,   G G X D s t ( * G r a G r u X * ) ,  
                     Q t d e ,   ' ' ( * C O D _ I N S _ S U B S T * ) ,   I D _ I t e m ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                     C o n t r o l e ,   E S T S T a b S o r c ) ;  
                     / /  
                 e n d ;  
         }  
                 t r s p 2 3 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,  
                     D t H r A b e r t o ,   D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o P r o d ,   G G X D s t ,   C l i e n t M O ,  
                     F o r n e c M O ,   E n t i S i t i o ,   Q t d e P r o d ,   O r i g e m O p e P r o c ,  
                     F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   I D S e q 1 ) ;  
                 e n d ;  
                 t r s p 2 5 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,  
                     D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o P r o d ,   G G X D s t ,   C l i e n t M O ,   F o r n e c M O ,  
                     E n t i S i t i o ,   Q t d e P r o d ,   O r i g e m O p e P r o c ,   F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,  
                     M e A v i s o ,   ( * L i n A r q P a i * ) I D S e q 1 ) ;  
                 e n d ;  
                 t r s p 2 6 X :  
                 b e g i n  
                     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r C o n s P a r c ,   D m o d . M y D B ,   [  
                     ' S E L E C T   C A S T ( Y E A R ( D t C o r r A p o )   A S   D E C I M A L ( 1 1 , 0 ) )   A n o ,   ' ,  
                     ' C A S T ( M O N T H ( D t C o r r A p o )   A S   D E C I M A L ( 1 1 , 0 ) )   M e s ,   ' ,  
                     ' P e c a s ,   A r e a M 2 ,   P e s o K g   ' ,  
                     ' F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   ' ,  
                     ' W H E R E   M o v i m C o d = '   +   G e r a l . F F 0 ( M o v i m C o d ) ,  
                     ' A N D   M o v i m N i v   I N   ( 5 7 )   ' ,   / / e m i n E m R R M B x a = 5 7 ,  
                     / / ' A N D   M o v i m T w n = '   +   G e r a l . F F 0 ( ) ,   /  
                     F i l t r o P e r i o d o ( T i p o R e g S P E D P r o d ) ,  
                     S Q L _ C o r r A p o ,  
                     ' G R O U P   B Y   A n o ,   M e s ' ,   / /   M o v i m N i v   > > >   5 4   o u   5 6   p a r a   R R M ! ! !  
                     ' ' ] ) ;  
                   / /   G e r a l . M B _ S Q L ( S e l f ,   Q r C o n s P a r c ) ;  
                     / /  
                     i f   Q r C o n s P a r c . R e c o r d C o u n t   >   1   t h e n  
                         G e r a l . M B _ E r r o ( ' E R R O !   A v i s e   a   D e r m a t e k !   Q r C o n s P a r c . R e c o r d C o u n t   >   1 ! ! ! ' ) ;  
                     / / w h i l e   n o t   Q r C o n s P a r c . E o f   d o  
                     b e g i n  
                         i f   Q r C o n s P a r c A n o . V a l u e   =   0   t h e n  
                             D t C o r r A p o C o n s   : =   0  
                         e l s e  
                             D t C o r r A p o C o n s   : =   E n c o d e D a t e ( T r u n c ( Q r C o n s P a r c A n o . V a l u e ) ,   T r u n c ( Q r C o n s P a r c M e s . V a l u e ) ,   1 ) ;  
                         / /  
                         A r e a M 2   : =   Q r C o n s P a r c A r e a M 2 . V a l u e ;  
                         P e s o K g   : =   Q r C o n s P a r c P e s o K g . V a l u e ;  
                         P e c a s     : =   Q r C o n s P a r c P e c a s . V a l u e ;  
                         / /  
                         / / P a r e i   a q u i   ! ! !     �   a s i m m ? ? ?  
                         i f   ( P e c a s   < >   0 )   o r   ( M o v i m I D = T E s t q M o v i m I D . e m i d E m P r o c S P )   ( * a n d   ( D t H r F i m O p e   >   1 ) * )   t h e n  
                         b e g i n  
                             c a s e   T i p o E s t q   o f  
                                 1 :   Q t d e C o n s   : =   A r e a M 2 ;  
                                 2 :   Q t d e C o n s   : =   P e s o K g ;  
                                 e l s e  
                                 b e g i n  
                                     G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                                     +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ T i p o E s t q ]   +   s L i n e B r e a k   +  
                                     ' R e d u z i d o   =   '   +   G e r a l . F F 0 ( G G X D s t )   +   s L i n e B r e a k   +  
                                     ' M o v i m C o d   =   '   +   G e r a l . F F 0 ( M o v i m C o d )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
                                     Q r y . S Q L .