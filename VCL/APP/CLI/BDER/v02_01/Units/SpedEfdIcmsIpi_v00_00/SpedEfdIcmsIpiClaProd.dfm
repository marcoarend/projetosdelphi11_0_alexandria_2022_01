object FmSpedEfdIcmsIpiClaProd: TFmSpedEfdIcmsIpiClaProd
  Left = 339
  Top = 185
  Caption = 'SPE-D_K2X-001 :: Importa'#231#227'o de Movimenta'#231#245'es'
  ClientHeight = 597
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 372
        Height = 32
        Caption = 'Importa'#231#227'o de Movimenta'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 372
        Height = 32
        Caption = 'Importa'#231#227'o de Movimenta'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 372
        Height = 32
        Caption = 'Importa'#231#227'o de Movimenta'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 372
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 372
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 305
        Align = alTop
        TabOrder = 0
        object PnPeriodo: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 42
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LaMes: TLabel
            Left = 48
            Top = 0
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
          end
          object LaAno: TLabel
            Left = 240
            Top = 0
            Width = 22
            Height = 13
            Caption = 'Ano:'
          end
          object LaData: TLabel
            Left = 336
            Top = 0
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object CBMes: TComboBox
            Left = 48
            Top = 17
            Width = 182
            Height = 21
            Color = clWhite
            DropDownCount = 12
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            Text = 'CBMes'
          end
          object CBAno: TComboBox
            Left = 240
            Top = 17
            Width = 90
            Height = 21
            Color = clWhite
            DropDownCount = 3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Text = 'CBAno'
          end
          object TPData: TdmkEditDateTimePicker
            Left = 336
            Top = 16
            Width = 112
            Height = 21
            Date = 42471.359051006950000000
            Time = 42471.359051006950000000
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object PCRegistro: TPageControl
          Left = 2
          Top = 57
          Width = 1004
          Height = 246
          ActivePage = TabSheet3
          Align = alClient
          TabOrder = 1
          OnChanging = PCRegistroChanging
          object TabSheet1: TTabSheet
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
          end
          object TabSheet2: TTabSheet
            Caption = 'K220 - Outras mov. internas entre mercadorias'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object PageControl1: TPageControl
              Left = 0
              Top = 97
              Width = 996
              Height = 121
              ActivePage = TabSheet13
              Align = alClient
              TabOrder = 0
              object TabSheet4: TTabSheet
                Caption = 'Classe e reclasse unit'#225'ria'
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 190
                object dmkDBGridZTO1: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 988
                  Height = 133
                  Align = alClient
                  DataSource = DsCacIts
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet5: TTabSheet
                Caption = 'Classe M'#250'ltipla'
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO2: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 216
                  Align = alClient
                  DataSource = DsClasMul
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet6: TTabSheet
                Caption = 'Reclasse M'#250'ltipla'
                ImageIndex = 2
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO3: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 261
                  Align = alClient
                  DataSource = DsReclMul
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet7: TTabSheet
                Caption = 'Em opera'#231#227'o e processo'
                ImageIndex = 3
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 509
                ExplicitHeight = 257
                object dmkDBGridZTO5: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 988
                  Height = 190
                  Align = alClient
                  DataSource = DsSecO_P
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet13: TTabSheet
                Caption = 'Reclasse direta NFe'
                ImageIndex = 4
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO10: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 988
                  Height = 93
                  Align = alClient
                  DataSource = DsGGXRcl
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet14: TTabSheet
                Caption = 'Desclasse de opera'#231#227'o e processo'
                ImageIndex = 5
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 190
                object dmkDBGridZTO11: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 988
                  Height = 133
                  Align = alClient
                  DataSource = DsDesclasse
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
            end
            object CGItensK220: TdmkCheckGroup
              Left = 0
              Top = 0
              Width = 996
              Height = 57
              Align = alTop
              Caption = ' Itens a importar: '
              Columns = 3
              ItemIndex = 2
              Items.Strings = (
                'Classe e reclasse'
                'Couro posto para ope'#231#227'o/processo'
                'Coprodu'#231#227'o de produtos'
                'Coprodu'#231#227'o de subprodutos'
                'Redirecionamento'
                'Desclassifica'#231#227'o em ope'#231#227'o/processo')
              TabOrder = 1
              UpdType = utYes
              Value = 4
              OldValor = 0
            end
            object Panel8: TPanel
              Left = 0
              Top = 57
              Width = 996
              Height = 40
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 2
              object BtTodos220: TBitBtn
                Tag = 127
                Left = 134
                Top = 0
                Width = 120
                Height = 40
                Caption = '&Todos'
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtTodos220Click
              end
              object BtNenhum220: TBitBtn
                Tag = 128
                Left = 260
                Top = 0
                Width = 120
                Height = 40
                Caption = '&Nenhum'
                NumGlyphs = 2
                TabOrder = 1
                OnClick = BtNenhum220Click
              end
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'K210/15/30/35/50/55 Desmonte e produ'#231#227'o'
            ImageIndex = 2
            object PageControl2: TPageControl
              Left = 0
              Top = 137
              Width = 996
              Height = 81
              ActivePage = TabSheet12
              Align = alClient
              TabOrder = 0
              object TabSheet8: TTabSheet
                Caption = 'Caleiro'
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO4: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 216
                  Align = alClient
                  DataSource = DsVSCalCab
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet9: TTabSheet
                Caption = 'Curtimento'
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO6: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 216
                  Align = alClient
                  DataSource = DsVSCurCab
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet12: TTabSheet
                Caption = 'Gera'#231#227'o WB'
                ImageIndex = 2
                object dmkDBGridZTO9: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 988
                  Height = 53
                  Align = alClient
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet10: TTabSheet
                Caption = 'Opera'#231#245'es'
                ImageIndex = 3
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO7: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 216
                  Align = alClient
                  DataSource = DsVSOpeCab
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet11: TTabSheet
                Caption = 'Recurtimento / acabamento'
                ImageIndex = 4
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO8: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 216
                  Align = alClient
                  DataSource = DsVSPWECab
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
            end
            object Panel7: TPanel
              Left = 0
              Top = 97
              Width = 996
              Height = 40
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object BtTudo: TBitBtn
                Tag = 127
                Left = 134
                Top = 0
                Width = 120
                Height = 40
                Caption = '&Todos'
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtTudoClick
              end
              object BtNenhum: TBitBtn
                Tag = 128
                Left = 260
                Top = 0
                Width = 120
                Height = 40
                Caption = '&Nenhum'
                NumGlyphs = 2
                TabOrder = 1
                OnClick = BtNenhumClick
              end
            end
            object Panel9: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 97
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 2
              object CGImportar2xx: TdmkCheckGroup
                Left = 89
                Top = 0
                Width = 907
                Height = 97
                Align = alClient
                Caption = ' Itens a importar: '
                Columns = 3
                ItemIndex = 2
                Items.Strings = (
                  'Pr'#233'-descarnado'
                  'Em Caleiro'
                  'Caleado'
                  'Tripa'
                  'Em Curtimento'
                  'Curtido'
                  'Gera'#231#227'o WB'
                  'Opera'#231#245'es'
                  'Recurtimento'
                  'Proc. sub-prod.'
                  'Reproces./reparo')
                TabOrder = 0
                UpdType = utYes
                Value = 4
                OldValor = 0
              end
              object RGProducao: TRadioGroup
                Left = 0
                Top = 0
                Width = 89
                Height = 97
                Align = alLeft
                Caption = '  Produ'#231#227'o: '
                ItemIndex = 1
                Items.Strings = (
                  'Isolada'
                  'Conjunta')
                TabOrder = 1
              end
            end
          end
        end
      end
      object MeAviso: TMemo
        Left = 0
        Top = 305
        Width = 1008
        Height = 67
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Lines.Strings = (
          'MeAviso')
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 420
    Width = 1008
    Height = 107
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 90
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 1004
        Height = 37
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object PB1: TProgressBar
          Left = 0
          Top = 20
          Width = 1004
          Height = 17
          Align = alBottom
          TabOrder = 0
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 37
        Width = 1004
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object LaAviso3: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso4: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object PB2: TProgressBar
          Left = 0
          Top = 36
          Width = 1004
          Height = 17
          Align = alBottom
          TabOrder = 0
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 527
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 16
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrIDNiv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MovimID, MovimNiv, COUNT(Controle) IMEIs  '
      'FROM vsmovits '
      ' '
      'GROUP BY MovimID, MovimNiv '
      'ORDER BY MovimID, MovimNiv ')
    Left = 120
    Top = 224
    object QrIDNivMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrIDNivMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
  end
  object QrConsParc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes,'
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, Sum(PesoKg) PesoKg '
      'FROM vsmovits '
      'GROUP BY Ano, Mes'
      '')
    Left = 692
    Top = 356
    object QrConsParcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrConsParcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrConsParcPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrConsParcAno: TFloatField
      FieldName = 'Ano'
    end
    object QrConsParcMes: TFloatField
      FieldName = 'Mes'
    end
  end
  object QrReclMul: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE(DataHora) Data,  GraGruX GGX_Dest, DstGGX GGX_Orig, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, SUM(PesoKg) PesoKg '
      'FROM vsmovits '
      'WHERE DataHora BETWEEN   "2014-01-01" AND "2016-10-31" '
      'AND Empresa = -11 '
      'AND MovimID=14 '
      'AND MovimNiv=2 '
      'GROUP BY Data, GraGruX, DstGGX '
      'ORDER BY Data, GraGruX, DstGGX ')
    Left = 184
    Top = 272
    object QrReclMulData: TDateField
      FieldName = 'Data'
    end
    object QrReclMulGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrReclMulGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrReclMulPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrReclMulAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrReclMulPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object QrClasMul: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE(DataHora) Data,  GraGruX GGX_Dest, DstGGX GGX_Orig, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, SUM(PesoKg) PesoKg '
      'FROM vsmovits '
      'WHERE DataHora BETWEEN   "2014-01-01" AND "2016-10-31" '
      'AND Empresa = -11 '
      'AND MovimID=14 '
      'AND MovimNiv=2 '
      'GROUP BY Data, GraGruX, DstGGX '
      'ORDER BY Data, GraGruX, DstGGX ')
    Left = 256
    Top = 272
    object QrClasMulData: TDateField
      FieldName = 'Data'
    end
    object QrClasMulGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrClasMulGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrClasMulPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrClasMulAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrClasMulPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object DsCacIts: TDataSource
    DataSet = QrConsParc
    Left = 116
    Top = 320
  end
  object DsReclMul: TDataSource
    DataSet = QrReclMul
    Left = 184
    Top = 320
  end
  object DsClasMul: TDataSource
    DataSet = QrClasMul
    Left = 256
    Top = 320
  end
  object QrVSCalCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'IF((ggx.GraGruY<2048 OR '
      'cou.CouNiv2<>1) AND vsx.PesoKgDst <> 0, 1, '
      'IF(cou.CouNiv2=1 AND vsx.AreaDstM2 <> 0, 2, '
      '  IF(vsx.PecasDst=0, 9, -1))) TipoEstq, vsx.* '
      'FROM vscalcab vsx '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2 '
      'WHERE  ( '
      '(DtHrAberto BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      ' OR (DtHrFimOpe BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      
        ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe > "2016-01-31 23:5' +
        '9:59") '
      ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe < "1900-01-01") '
      ') '
      ' '
      'OR MovimCod IN (  '
      '  SELECT MovimCod  '
      '  FROM vsmovits  '
      '  WHERE MovimID=26 '
      '  AND DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59" '
      ')  ')
    Left = 504
    Top = 116
  end
  object DsVSCalCab: TDataSource
    DataSet = QrVSCalCab
    Left = 504
    Top = 164
  end
  object QrVSCurCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'IF((ggx.GraGruY<2048 OR '
      'cou.CouNiv2<>1) AND vsx.PesoKgDst <> 0, 1, '
      'IF(cou.CouNiv2=1 AND vsx.AreaDstM2 <> 0, 2, '
      '  IF(vsx.PecasDst=0, 9, -1))) TipoEstq, vsx.* '
      'FROM vscalcab vsx '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2 '
      'WHERE  ( '
      '(DtHrAberto BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      ' OR (DtHrFimOpe BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      
        ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe > "2016-01-31 23:5' +
        '9:59") '
      ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe < "1900-01-01") '
      ') '
      ' '
      'OR MovimCod IN (  '
      '  SELECT MovimCod  '
      '  FROM vsmovits  '
      '  WHERE MovimID=26 '
      '  AND DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59" '
      ')  ')
    Left = 580
    Top = 116
  end
  object DsVSCurCab: TDataSource
    DataSet = QrVSCurCab
    Left = 580
    Top = 164
  end
  object QrVSOpeCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'IF((ggx.GraGruY<2048 OR '
      'cou.CouNiv2<>1) AND vsx.PesoKgDst <> 0, 1, '
      'IF(cou.CouNiv2=1 AND vsx.AreaDstM2 <> 0, 2, '
      '  IF(vsx.PecasDst=0, 9, -1))) TipoEstq, vsx.* '
      'FROM vscalcab vsx '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2 '
      'WHERE  ( '
      '(DtHrAberto BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      ' OR (DtHrFimOpe BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      
        ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe > "2016-01-31 23:5' +
        '9:59") '
      ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe < "1900-01-01") '
      ') '
      ' '
      'OR MovimCod IN (  '
      '  SELECT MovimCod  '
      '  FROM vsmovits  '
      '  WHERE MovimID=26 '
      '  AND DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59" '
      ')  ')
    Left = 652
    Top = 116
  end
  object DsVSOpeCab: TDataSource
    DataSet = QrVSOpeCab
    Left = 652
    Top = 164
  end
  object QrSecO_P: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _SPED_EFD_K2XX_O_P;'
      'CREATE TABLE _SPED_EFD_K2XX_O_P'
      'SELECT DATE(vmi.DataHora) Data,  cab.GGXDst GGX_Dst, '
      'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, med.Grandeza'
      'FROM bluederm_meza.vsopecab cab'
      
        'LEFT JOIN bluederm_meza.vsmovits vmi ON vmi.MovimCod=cab.MovimCo' +
        'd'
      'LEFT JOIN bluederm_meza.gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm_meza.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm_meza.unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59"'
      'AND cab.GGXDst <> vmi.GragruX'
      'AND MovimNiv=9'
      'UNION'
      'SELECT DATE(vmi.DataHora) Data,  cab.GGXDst GGX_Dst, '
      'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, med.Grandeza'
      'FROM bluederm_meza.vspwecab cab'
      
        'LEFT JOIN bluederm_meza.vsmovits vmi ON vmi.MovimCod=cab.MovimCo' +
        'd'
      'LEFT JOIN bluederm_meza.gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm_meza.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm_meza.unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59"'
      'AND cab.GGXDst <> vmi.GragruX'
      'AND MovimNiv=22'
      ';'
      'SELECT * FROM _SPED_EFD_K2XX_O_P')
    Left = 696
    Top = 248
    object QrSecO_PData: TDateField
      FieldName = 'Data'
    end
    object QrSecO_PGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrSecO_PGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrSecO_PPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSecO_PAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSecO_PPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrSecO_PGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrSecO_PMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrSecO_PCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSecO_PMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrSecO_PControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSecO_PEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSecO_PMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrSecO_PDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrSecO_PClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrSecO_PFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrSecO_PEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
  end
  object DsSecO_P: TDataSource
    DataSet = QrSecO_P
    Left = 696
    Top = 296
  end
  object QrVSPWECab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'IF((ggx.GraGruY<2048 OR '
      'cou.CouNiv2<>1) AND vsx.PesoKgDst <> 0, 1, '
      'IF(cou.CouNiv2=1 AND vsx.AreaDstM2 <> 0, 2, '
      '  IF(vsx.PecasDst=0, 9, -1))) TipoEstq, vsx.* '
      'FROM vscalcab vsx '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2 '
      'WHERE  ( '
      '(DtHrAberto BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      ' OR (DtHrFimOpe BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      
        ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe > "2016-01-31 23:5' +
        '9:59") '
      ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe < "1900-01-01") '
      ') '
      ' '
      'OR MovimCod IN (  '
      '  SELECT MovimCod  '
      '  FROM vsmovits  '
      '  WHERE MovimID=26 '
      '  AND DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59" '
      ')  ')
    Left = 768
    Top = 72
  end
  object DsVSPWECab: TDataSource
    DataSet = QrVSPWECab
    Left = 768
    Top = 120
  end
  object QrCacIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _SPED_EFD_K2XX;'
      ''
      'CREATE TABLE _SPED_EFD_K2XX'
      ''
      'SELECT cac.*, src.GraGruX GGX_Src, dst.GraGruX GGX_Dst '
      'FROM _sped_efd_k2xx_cacits_ cac'
      
        'LEFT JOIN _sped_efd_k2xx_vmi_src_ src ON cac.VMI_Sorc=src.Contro' +
        'le'
      
        'LEFT JOIN _sped_efd_k2xx_vmi_dst_ dst ON cac.VMI_Dest=dst.Contro' +
        'le'
      ';'
      ''
      'SELECT Data, GGX_Src, GGX_Dst, SUM(Pecas) Pecas,'
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2  '
      'FROM _SPED_EFD_K2XX'
      'GROUP BY Data, GGX_Src, GGX_Dst '
      'ORDER BY Data, GGX_Src, GGX_Dst ')
    Left = 116
    Top = 276
    object QrCacItsData: TDateField
      FieldName = 'Data'
    end
    object QrCacItsGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrCacItsGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrCacItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrCacItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrCacItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrCacItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCacItsVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrCacItsCacID: TIntegerField
      FieldName = 'CacID'
    end
  end
  object DsVmiGArIts: TDataSource
    Left = 388
    Top = 48
  end
  object QrGGXRcl: TmySQLQuery
    Database = Dmod.MyDB
    Left = 324
    Top = 272
    object QrGGXRclGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrGGXRclGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrGGXRclGrandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrGGXRclPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrGGXRclAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrGGXRclPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrGGXRclData: TDateField
      FieldName = 'Data'
    end
    object QrGGXRclMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrGGXRclCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGGXRclMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrGGXRclControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXRclMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrGGXRclDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrGGXRclClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrGGXRclFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrGGXRclEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
  end
  object DsGGXRcl: TDataSource
    DataSet = QrGGXRcl
    Left = 324
    Top = 320
  end
  object QrDesclasse: TmySQLQuery
    Database = Dmod.MyDB
    Left = 396
    Top = 272
    object QrDesclasseGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrDesclasseGdz_Dst: TIntegerField
      FieldName = 'Gdz_Dst'
    end
    object QrDesclassePecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrDesclasseAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrDesclassePesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrDesclasseData: TDateField
      FieldName = 'Data'
    end
    object QrDesclasseMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrDesclasseCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDesclasseMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrDesclasseControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrDesclasseDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrDesclasseDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrDesclasseDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrDesclasseGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrDesclasseGdz_Src: TIntegerField
      FieldName = 'Gdz_Src'
    end
    object QrDesclasseMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrDesclasseDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrDesclasseClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrDesclasseFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrDesclasseEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
  end
  object DsDesclasse: TDataSource
    DataSet = QrDesclasse
    Left = 396
    Top = 320
  end
  object QrCeR2: TmySQLQuery
    Database = Dmod.MyDB
    Left = 604
    Top = 228
  end
  object QrSubPrdOP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _SPED_EFD_K2XX_O_P;'
      'CREATE TABLE _SPED_EFD_K2XX_O_P'
      'SELECT DATE(vmi.DataHora) Data,  cab.GGXDst GGX_Dst, '
      'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, med.Grandeza'
      'FROM bluederm_meza.vsopecab cab'
      
        'LEFT JOIN bluederm_meza.vsmovits vmi ON vmi.MovimCod=cab.MovimCo' +
        'd'
      'LEFT JOIN bluederm_meza.gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm_meza.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm_meza.unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59"'
      'AND cab.GGXDst <> vmi.GragruX'
      'AND MovimNiv=9'
      'UNION'
      'SELECT DATE(vmi.DataHora) Data,  cab.GGXDst GGX_Dst, '
      'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, med.Grandeza'
      'FROM bluederm_meza.vspwecab cab'
      
        'LEFT JOIN bluederm_meza.vsmovits vmi ON vmi.MovimCod=cab.MovimCo' +
        'd'
      'LEFT JOIN bluederm_meza.gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm_meza.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm_meza.unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59"'
      'AND cab.GGXDst <> vmi.GragruX'
      'AND MovimNiv=22'
      ';'
      'SELECT * FROM _SPED_EFD_K2XX_O_P')
    Left = 564
    Top = 308
    object QrSubPrdOPData: TDateField
      FieldName = 'Data'
    end
    object QrSubPrdOPGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrSubPrdOPGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrSubPrdOPPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSubPrdOPAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSubPrdOPPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrSubPrdOPGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrSubPrdOPMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrSubPrdOPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSubPrdOPMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrSubPrdOPControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSubPrdOPEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSubPrdOPDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrSubPrdOPClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrSubPrdOPFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
  end
  object QrFMO: TmySQLQuery
    Database = Dmod.MyDB
    Left = 460
    Top = 184
    object QrFMOFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
  end
  object QrIDs: TmySQLQuery
    Database = Dmod.MyDB
    Left = 848
    Top = 168
  end
  object QrVSPSPCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'IF((ggx.GraGruY<2048 OR '
      'cou.CouNiv2<>1) AND vsx.PesoKgDst <> 0, 1, '
      'IF(cou.CouNiv2=1 AND vsx.AreaDstM2 <> 0, 2, '
      '  IF(vsx.PecasDst=0, 9, -1))) TipoEstq, vsx.* '
      'FROM vscalcab vsx '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2 '
      'WHERE  ( '
      '(DtHrAberto BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      ' OR (DtHrFimOpe BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      
        ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe > "2016-01-31 23:5' +
        '9:59") '
      ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe < "1900-01-01") '
      ') '
      ' '
      'OR MovimCod IN (  '
      '  SELECT MovimCod  '
      '  FROM vsmovits  '
      '  WHERE MovimID=26 '
      '  AND DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59" '
      ')  ')
    Left = 768
    Top = 164
  end
  object DsVSPSPCab: TDataSource
    DataSet = QrVSPSPCab
    Left = 768
    Top = 208
  end
  object QrVSRRMCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'IF((ggx.GraGruY<2048 OR '
      'cou.CouNiv2<>1) AND vsx.PesoKgDst <> 0, 1, '
      'IF(cou.CouNiv2=1 AND vsx.AreaDstM2 <> 0, 2, '
      '  IF(vsx.PecasDst=0, 9, -1))) TipoEstq, vsx.* '
      'FROM vscalcab vsx '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2 '
      'WHERE  ( '
      '(DtHrAberto BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      ' OR (DtHrFimOpe BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      
        ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe > "2016-01-31 23:5' +
        '9:59") '
      ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe < "1900-01-01") '
      ') '
      ' '
      'OR MovimCod IN (  '
      '  SELECT MovimCod  '
      '  FROM vsmovits  '
      '  WHERE MovimID=26 '
      '  AND DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59" '
      ')  ')
    Left = 764
    Top = 360
  end
  object QrProdParc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes,'
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, Sum(PesoKg) PesoKg '
      'FROM vsmovits '
      'GROUP BY Ano, Mes'
      '')
    Left = 692
    Top = 404
    object QrProdParcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrProdParcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrProdParcPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrProdParcClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrProdParcFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrProdParcControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProdParcAno: TFloatField
      FieldName = 'Ano'
    end
    object QrProdParcMes: TFloatField
      FieldName = 'Mes'
    end
  end
  object QrBuscaCtrl: TmySQLQuery
    Database = Dmod.MyDB
    Left = 292
    Top = 168
    object QrBuscaCtrlControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSumGAR: TmySQLQuery
    Database = Dmod.MyDB
    Left = 844
    Top = 364
    object QrSumGARPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumGARAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumGARPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
end
