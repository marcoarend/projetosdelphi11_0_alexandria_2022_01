unit UnVS_EFD_ICMS_IPI;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  dmkDBGridZTO, UnDmkProcFunc, UnProjGroup_Vars, BlueDermConsts, TypInfo,
  System.Math, UnProjGroup_Consts, UnEntities, DBCtrls, Grids, Mask, UnVS_Tabs,
  AdvToolBar, dmkEditDateTimePicker, UnGrl_Consts, UnGrl_Geral, UnVS_PF;

type
  TUnVS_EFD_ICMS_IPI = class(TObject)
  private
    { Private declarations }
     //
  public
    { Public declarations }
    //
    procedure ReopenVSOriPQx(Qry: TmySQLQuery; MovimCod: Integer; SQL_PeriodoPQ:
              String; ShowSQL: Boolean);
    procedure ReopenVSRibItss(QrVSAtu, QrVSOriIMEI, QrVSOriPallet,
              QrVSDst, QrVSInd, QrSubPrd, QrDescl, QrForcados, QrEmit, QrPQO: TmySQLQuery;
              Codigo, MovimCod, TemIMEIMrt: Integer; MovIDEmProc, MovIDPronto,
              MovIDIndireto: TEstqMovimID; MovNivInn, MovNivSrc, MovNivDst:
              TEstqMovimNiv);
    procedure ReopenVSMovCod_Emit(QrEmit: TmySQLQuery; MovimCod: Integer);
    procedure ReopenVSMovCod_PQO(QrPQO: TmySQLQuery; MovimCod, Codigo: Integer);
    procedure ReopenVSMovCod_PQOIts(QrPQOIts: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenVSOpePrcAtu(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt:
              Integer; MovimNiv: TEstqMovimNiv);
    procedure ReopenVSOpePrcForcados(Qry: TmySQLQuery; Controle, SrcNivel1,
              SrcNivel2, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID);
    procedure ReopenVSOpePrcOriIMEI(Qry: TmySQLQuery; MovimCod, Controle,
              TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
    procedure ReopenVSOpePrcOriPallet(QrVSCalOriPallet: TmySQLQuery; MovimCod:
              Integer; MovimNiv: TEstqMovimNiv; TemIMEIMrt, LocPallet: Integer);
    procedure ReopenVSPrcPrcDst(Qry: TmySQLQuery; SrcNivel1, MovimCod, Controle,
              TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
              DstMovimNiv: TEstqMovimNiv; SQL_Limit: String = '');
    procedure ReopenVSPrcPrcInd(Qry: TmySQLQuery; SrcNivel1, MovimCod, Controle,
              TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
              DstMovimNiv: TEstqMovimNiv; SQL_Limit: String = '');
  end;

var
  VS_EFD_ICMS_IPI: TUnVS_EFD_ICMS_IPI;

implementation

uses DmkDAC_PF, Module;

{ TUnVS_EFD_ICMS_IPI }

procedure TUnVS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSAtu, QrVSOriIMEI,
  QrVSOriPallet, QrVSDst, QrVSInd, QrSubPrd, QrDescl, QrForcados, QrEmit, QrPQO:
  TmySQLQuery; Codigo, MovimCod, TemIMEIMrt: Integer; MovIDEmProc, MovIDPronto,
  MovIDIndireto: TEstqMovimID; MovNivInn, MovNivSrc, MovNivDst: TEstqMovimNiv);
const
  Controle = 0;
  Pallet   = 0;
  //
begin
  // Em processo. Deve ser antes do Forcados!!
  ReopenVSOpePrcAtu(QrVSAtu, (*QrVSCab*)MovimCod(*.Value*), Controle,
  (*QrVSCab*)TemIMEIMrt(*.Value*), MovNivInn(*eminEmInn*));
////////////////////////////////////////////////////////////////////////////////
  // Origem
  if QrVSOriIMEI <> nil then
    ReopenVSOpePrcOriIMEI(QrVSOriIMEI,
    (*QrVSCab*)MovimCod(*.Value*), Controle,
    TemIMEIMrt, MovNivSrc(*eminSorc*), (*SQL_Limit*)'');
  if QrVSOriPallet <> nil then
    ReopenVSOpePrcOriPallet(QrVSOriPallet, MovimCod,
    MovNivSrc(*eminSorc*), TemIMEIMrt, Pallet);
////////////////////////////////////////////////////////////////////////////////
  // Destino Direto
  if QrVSDst <> nil then
    ReopenVSPrcPrcDst(QrVSDst, (*QrVSCab*)Codigo(*.Value*),
    (*QrVSCab*)MovimCod(*.Value*), Controle, (*QrVSCab*)TemIMEIMrt(*.Value*),
    MovIDPronto(*TEstqMovimID.emideado*),
    MovNivDst(*eminDest*));
////////////////////////////////////////////////////////////////////////////////
  // Destino Indireto
  if QrVSInd <> nil then
    ReopenVSPrcPrcInd(QrVSInd, (*QrVSCab*)Codigo(*.Value*),
    (*QrVSCab*)MovimCod(*.Value*), Controle,
    (*QrVSCab*)TemIMEIMrt(*.Value*),
    MovIDIndireto(*TEstqMovimID.emideado*),
    MovNivDst(*eminDest*));
////////////////////////////////////////////////////////////////////////////////
  // For�ados. Deve ser depois do Forcados!!
  if QrForcados <> nil then
    ReopenVSOpePrcForcados(QrForcados, 0, (*QrVSCab*)Codigo(*.Value*),
      (*QrVSAtuControle.Value*)QrVSAtu.FieldByName('Controle').AsInteger,
      (*QrVSCab*)TemIMEIMrt(*.Value*), MovIDEmProc(*emidEmProc*));
////////////////////////////////////////////////////////////////////////////////
  // Insumos
  if QrEmit <> nil then
    ReopenVSMovCod_Emit(QrEmit, MovimCod);
  if QrPQO <> nil then
    ReopenVSMovCod_PQO(QrPQO, MovimCod, 0);
////////////////////////////////////////////////////////////////////////////////
  //
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSMovCod_Emit(QrEmit: TmySQLQuery; MovimCod:
  Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
  'SELECT emi.*, ',
  'IF(emi.DtaBaixa < 2, "??/??/????", DATE_FORMAT(emi.DtaBaixa, "%d/%m/%Y") ) DtaBaixa_TXT, ',
  'IF(emi.DtCorrApo < 2, "", DATE_FORMAT(emi.DtCorrApo, "%d/%m/%Y") ) DtCorrApo_TXT ',
  'FROM emit emi ',
  'WHERE emi.VSMovCod=' + Geral.FF0((*QrVSCalCab*)MovimCod(*.Value*)),
  '']);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSMovCod_PQO(QrPQO: TmySQLQuery; MovimCod,
  Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQO, Dmod.MyDB, [
  'SELECT lse.Nome NOMESETOR, ',
  'emg.Nome NO_EMITGRU, pqo.* ',
  'FROM pqo pqo ',
  'LEFT JOIN listasetores lse ON lse.Codigo=pqo.Setor ',
  'LEFT JOIN emitgru emg ON emg.Codigo=pqo.EmitGru ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  if Codigo <> 0 then
    QrPQO.Locate('Codigo', Codigo, []);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSMovCod_PQOIts(QrPQOIts: TmySQLQuery; Codigo,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQOIts, Dmod.MyDB, [
  'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  ',
  'pqg.Nome NOMEGRUPO ',
  'FROM pqx pqx ',
  'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo ',
  'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ ',
  'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
  'AND pqx.OrigemCodi=' + Geral.FF0(Codigo),
  '']);
  if Controle <> 0 then
    QrPQOIts.Locate('OrigemCtrl', Controle, []);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcAtu(Qry: TmySQLQuery; MovimCod,
  Controle, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  VS_PF.SQL_NO_FRN(),
  VS_PF.SQL_NO_SCL(),
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome)NO_FORNEC_MO, ',
  'IF(vmi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", vmi.Ficha)) NO_FICHA, ',
  'IF(PesoKg=0, 0, ValorT / PesoKg) CUSTO_KG, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  VS_PF.SQL_LJ_FRN(),
  VS_PF.SQL_LJ_SCL(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  fmo ON fmo.Codigo=vmi.FornecMO',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //Geral.MB_SQL(nil, Qry);
  Qry.Locate('Controle', Controle, []);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcForcados(Qry: TmySQLQuery; Controle,
  SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  //TemIMEIMrt: Integer;
begin
  //TemIMEIMrt := QrVSOpeCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
(*
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod), //QrVSOpeCabMovimCod.Value),
  'AND vmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)), //emidEmOperacao)),  // 11
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1), //QrVSOpeCabCodigo.Value),
*)
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)), //emidEmOperacao)),  // 11
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1), //QrVSOpeCabCodigo.Value),
  'AND vmi.SrcNivel2=' + Geral.FF0(SrcNivel2), //QrVSOpeCabCodigo.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  //Geral.MB_SQL(nil, Qry);
  Qry.Locate('Controle', Controle, []);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(Qry: TmySQLQuery; MovimCod, Controle,
  TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //TemIMEIMrt := QrVSOpeCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  VS_PF.SQL_NO_FRN(),
  VS_PF.SQL_NO_CMO(),
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  '-1.000 FatorImp ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  VS_PF.SQL_LJ_FRN(),
  VS_PF.SQL_LJ_CMO(),
  'LEFT JOIN vspalleta   vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch    vsf ON vsf.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  //'WHERE vmi.MovimCod=' + Geral.FF0(QrVSOpeCabMovimCod.Value),
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  SQL_Group := '';
  //UnDmkDAC_PF.AbreMySQLQuery0(QrVSOpeOriIMEI, Dmod.MyDB, [
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //Geral.MB_SQL(nil, Qry);
  //
  Qry.Locate('Controle', Controle, []);
  //
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOpePrcOriPallet(QrVSCalOriPallet:
  TmySQLQuery; MovimCod: Integer; MovimNiv: TEstqMovimNiv;
  TemIMEIMrt, LocPallet: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  '']);
  SQL_Left := Geral.ATS([
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0((*QrVSCalCab*)MovimCod(*.Value*)),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer((*eminSorcCal*)MovimNiv)),
  '']);
  SQL_Group := 'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCalOriPallet, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
  QrVSCalOriPallet.Locate('Pallet', LocPallet, []);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSOriPQx(Qry: TmySQLQuery; MovimCod: Integer;
  SQL_PeriodoPQ: String; ShowSQL: Boolean);
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, Peso, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
  'CliDest ClientMO ',
  'FROM pqx ',
  'WHERE ( ',
  '  Tipo=' + Geral.FF0(VAR_FATID_0110),
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM emit ',
  '    WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'OR ',
  '( ',
  '  Tipo=' + Geral.FF0(VAR_FATID_0190),
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM PQO ',
  '    WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'AND Peso <> 0',
  SQL_PeriodoPQ,
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, Peso, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
  'CliDest ClientMO ',
  'FROM pqx ',
  'WHERE ( ',
  '  ( ',
  '    Tipo=' + Geral.FF0(VAR_FATID_0110),
  '    AND OrigemCodi IN ( ',
  '      SELECT Codigo ',
  '      FROM emit ',
  '      WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '    ) ',
  '  ) ',
  '  OR ',
  '  ( ',
  '    Tipo=' + Geral.FF0(VAR_FATID_0190),
  '    AND OrigemCodi IN ( ',
  '      SELECT Codigo ',
  '      FROM PQO ',
  '      WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '    ) ',
  '  ) ',
  ') ',
  'AND Peso <> 0',
  'AND Insumo > 0',
  SQL_PeriodoPQ,
  '']);
  //
  if ShowSQL then
    Geral.MB_Info(Qry.SQL.Text);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSPrcPrcDst(Qry: TmySQLQuery; SrcNivel1,
  MovimCod, Controle, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
  DstMovimNiv: TEstqMovimNiv; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  VS_PF.SQL_NO_FRN(),
  VS_PF.SQL_NO_SCL(),
  ATT_MovimID,
  ATT_MovimNiv,
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  VS_PF.SQL_LJ_FRN(),
  VS_PF.SQL_LJ_SCL(),
  'LEFT JOIN vspalleta  vsp  ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf  ON vsf.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  //'WHERE (',
  'WHERE ',
  'vmi.MovimID=' + Geral.FF0(Integer(SrcMovID)),
  'AND vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
(*
  'vmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)),
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1),
  ') or ( ',
  'vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
  ') ',
*)
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //Geral.MB_SQL(nil, Qry);
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

procedure TUnVS_EFD_ICMS_IPI.ReopenVSPrcPrcInd(Qry: TmySQLQuery; SrcNivel1,
  MovimCod, Controle, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
  DstMovimNiv: TEstqMovimNiv; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  VS_PF.SQL_NO_FRN(),
  VS_PF.SQL_NO_SCL(),
  ATT_MovimID,
  ATT_MovimNiv,
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  VS_PF.SQL_LJ_FRN(),
  VS_PF.SQL_LJ_SCL(),
  'LEFT JOIN vspalleta  vsp  ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf  ON vsf.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  //'WHERE (',
  'WHERE ',
  'vmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)),
  //'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovCodPai<>vmi.MovimCod AND vmi.MovCodPai <> 0 AND vmi.MovCodPai=' + Geral.FF0(MovimCod),
{
  ') or ( ',
  'vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
  ') ',
}
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //Geral.MB_SQL(nil, Qry);
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

end.
