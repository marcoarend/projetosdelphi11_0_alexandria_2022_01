u n i t   S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a ;  
  
 i n t e r f a c e  
  
 u s e s  
     W i n a p i . W i n d o w s ,   W i n a p i . M e s s a g e s ,   S y s t e m . S y s U t i l s ,   S y s t e m . V a r i a n t s ,  
     S y s t e m . C l a s s e s ,   V c l . G r a p h i c s ,   V c l . C o n t r o l s ,   V c l . F o r m s ,   V c l . D i a l o g s ,   D a t a . D B ,  
     m y S Q L D b T a b l e s ,  
     / /  
     S t d C t r l s ,   E x t C t r l s ,   M e n u s ,   U n I n t e r n a l C o n s t s 2 ,   C o m C t r l s ,   R e g i s t r y ,   P r i n t e r s ,  
     C o m m C t r l ,   C o n s t s ,   U n I n t e r n a l C o n s t s ,   Z C F 2 ,   S t r U t i l s ,   d m k G e r a l ,   U n D m k E n u m s ,  
     U n M s g I n t ,   D b T a b l e s ,   D m k C o d i n g ,   d m k E d i t ,   d m k R a d i o G r o u p ,   d m k M e m o ,   A d v T o o l B a r ,  
     d m k C h e c k G r o u p ,   U n M y O b j e c t s ,   M y D B C h e c k ,   U n D m k P r o c F u n c ,   U n P r o j G r o u p _ C o n s t s ,  
     A p p L i s t a s ,   d m k L a b e l ,   d m k I m a g e ,   D B C t r l s ,   d m k D B L o o k u p C o m b o B o x ,   d m k E d i t C B ,  
     d m k E d i t D a t e T i m e P i c k e r ,   d m k D B G r i d Z T O ,   S P E D _ L i s t a s ,   T y p I n f o ;  
  
 t y p e  
     T O p e n C l i F o r S i t e   =   ( o c f s I n d e f = 0 ,   o c f s C o n t r o l e = 1 ,   o c f s M o v I d N i v C o d = 2 ,   o c f s S r c N i v e l 2 = 3 ) ;  
     T F o r m a I n s e r c a o I t e m P r o d u c a o   =   ( f i p I n d e f = 0 ,   f i p I s o l a d a = 1 ,   f i p C o n j u n t a = 2 ) ;  
     T I n s e r c a o I t e m P r o d u c a o C o n j u n t a   =   ( i i p c I n d e f = 0 ,   i i p c S i m p l e s = 1 ,   i i p c G e m e o s = 2 ,  
                                                                 i i p c I n d i r e t a = 3 ,   i i p c U n i A n t = 4 ,   i i p c U n i G e r = 5 ) ;  
     T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a   =   c l a s s ( T F o r m )  
         Q r D a t a s :   T m y S Q L Q u e r y ;  
         Q r D a t a s M i n D H :   T D a t e T i m e F i e l d ;  
         Q r D a t a s M a x D H :   T D a t e T i m e F i e l d ;  
         Q r O r i :   T m y S Q L Q u e r y ;  
         Q r M a e :   T m y S Q L Q u e r y ;  
         Q r M C :   T m y S Q L Q u e r y ;  
         Q r M C M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b :   T m y S Q L Q u e r y ;  
         Q r P r o d R i b D a t a :   T D a t e F i e l d ;  
         Q r P r o d R i b G G X _ D s t :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b G G X _ S r c :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b P e c a s :   T F l o a t F i e l d ;  
         Q r P r o d R i b A r e a M 2 :   T F l o a t F i e l d ;  
         Q r P r o d R i b P e s o K g :   T F l o a t F i e l d ;  
         Q r P r o d R i b M o v i m I D :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b C o d i g o :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b E m p r e s a :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b Q t d A n t P e c a :   T F l o a t F i e l d ;  
         Q r P r o d R i b Q t d A n t A r M 2 :   T F l o a t F i e l d ;  
         Q r P r o d R i b Q t d A n t P e s o :   T F l o a t F i e l d ;  
         Q r P r o d R i b s T i p o E s t q :   T F l o a t F i e l d ;  
         Q r P r o d R i b d T i p o E s t q :   T F l o a t F i e l d ;  
         Q r P r o d R i b M o v i m N i v :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b D t C o r r A p o :   T D a t e T i m e F i e l d ;  
         Q r P r o d R i b C l i e n t M O :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b F o r n e c M O :   T I n t e g e r F i e l d ;  
         Q r P r o d R i b E n t i S i t i o :   T I n t e g e r F i e l d ;  
         Q r O b t C l i F o r S i t e :   T m y S Q L Q u e r y ;  
         Q r I t s :   T m y S Q L Q u e r y ;  
         Q r C a l T o C u r :   T m y S Q L Q u e r y ;  
         Q r C o n s P a r c :   T m y S Q L Q u e r y ;  
         Q r C o n s P a r c P e c a s :   T F l o a t F i e l d ;  
         Q r C o n s P a r c A r e a M 2 :   T F l o a t F i e l d ;  
         Q r C o n s P a r c P e s o K g :   T F l o a t F i e l d ;  
         Q r C o n s P a r c A n o :   T F l o a t F i e l d ;  
         Q r C o n s P a r c M e s :   T F l o a t F i e l d ;  
         Q r P r o d P a r c :   T m y S Q L Q u e r y ;  
         Q r P r o d P a r c P e c a s :   T F l o a t F i e l d ;  
         Q r P r o d P a r c A r e a M 2 :   T F l o a t F i e l d ;  
         Q r P r o d P a r c P e s o K g :   T F l o a t F i e l d ;  
         Q r P r o d P a r c C l i e n t M O :   T I n t e g e r F i e l d ;  
         Q r P r o d P a r c F o r n e c M O :   T I n t e g e r F i e l d ;  
         Q r P r o d P a r c C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r P r o d P a r c A n o :   T F l o a t F i e l d ;  
         Q r P r o d P a r c M e s :   T F l o a t F i e l d ;  
         Q r P e r i o d o s :   T m y S Q L Q u e r y ;  
         Q r P e r i o d o s A n o :   T F l o a t F i e l d ;  
         Q r P e r i o d o s M e s :   T F l o a t F i e l d ;  
         Q r P e r i o d o s P e r i o d o :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b :   T m y S Q L Q u e r y ;  
         Q r V m i G A r C a b D a t a :   T D a t e F i e l d ;  
         Q r V m i G A r C a b C o d i g o :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b G r a G r u X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b P e c a s :   T F l o a t F i e l d ;  
         Q r V m i G A r C a b A r e a M 2 :   T F l o a t F i e l d ;  
         Q r V m i G A r C a b P e s o K g :   T F l o a t F i e l d ;  
         Q r V m i G A r C a b M o v i m T w n :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b U n i d M e d :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b m e d _ g r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b x c o _ g r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b G r a G r u Y :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b C o u N i v 2 :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b T i p o E s t q :   T F l o a t F i e l d ;  
         Q r V m i G A r C a b D t C o r r A p o :   T D a t e T i m e F i e l d ;  
         Q r V m i G A r C a b C l i e n t M O :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b F o r n e c M O :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b E n t i S i t i o :   T I n t e g e r F i e l d ;  
         Q r S u m G A R :   T m y S Q L Q u e r y ;  
         Q r S u m G A R P e c a s :   T F l o a t F i e l d ;  
         Q r S u m G A R A r e a M 2 :   T F l o a t F i e l d ;  
         Q r S u m G A R P e s o K g :   T F l o a t F i e l d ;  
         Q r V m i G A r I t s :   T m y S Q L Q u e r y ;  
         Q r V m i G A r I t s D a t a :   T D a t e F i e l d ;  
         Q r V m i G A r I t s C o d i g o :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s G r a G r u X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s P e c a s :   T F l o a t F i e l d ;  
         Q r V m i G A r I t s A r e a M 2 :   T F l o a t F i e l d ;  
         Q r V m i G A r I t s P e s o K g :   T F l o a t F i e l d ;  
         Q r V m i G A r I t s M o v i m T w n :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s U n i d M e d :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s m e d _ g r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s x c o _ g r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s G r a G r u Y :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s C o u N i v 2 :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s T i p o E s t q :   T L a r g e i n t F i e l d ;  
         Q r V m i G A r I t s C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s D s t G G X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s S r c G G X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s S r c N i v e l 2 :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s S r c M o v I D :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s D t C o r r A p o :   T D a t e T i m e F i e l d ;  
         Q r I M E C s :   T m y S Q L Q u e r y ;  
         Q r I M E C s C o d i g o :   T I n t e g e r F i e l d ;  
         Q r I M E C s M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r I M E C s M o v i m I D :   T I n t e g e r F i e l d ;  
         Q r I M E C s A n o C A :   T I n t e g e r F i e l d ;  
         Q r I M E C s M e s C A :   T I n t e g e r F i e l d ;  
         Q r I M E C s T i p o T a b :   T S t r i n g F i e l d ;  
         Q r I M E C s A t i v o :   T S m a l l i n t F i e l d ;  
         Q r V S R i b A t u :   T m y S Q L Q u e r y ;  
         Q r V S R i b A t u C o d i g o :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u C o n t r o l e :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u M o v i m C o d :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u M o v i m N i v :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u M o v i m T w n :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u E m p r e s a :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u T e r c e i r o :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u C l i V e n d a :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u M o v i m I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u D a t a H o r a :   T D a t e T i m e F i e l d ;  
         Q r V S R i b A t u P a l l e t :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u G r a G r u X :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u P e c a s :   T F l o a t F i e l d ;  
         Q r V S R i b A t u P e s o K g :   T F l o a t F i e l d ;  
         Q r V S R i b A t u A r e a M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b A t u A r e a P 2 :   T F l o a t F i e l d ;  
         Q r V S R i b A t u V a l o r T :   T F l o a t F i e l d ;  
         Q r V S R i b A t u S r c M o v I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u S r c N i v e l 1 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u S r c N i v e l 2 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u S r c G G X :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u S d o V r t P e c a :   T F l o a t F i e l d ;  
         Q r V S R i b A t u S d o V r t P e s o :   T F l o a t F i e l d ;  
         Q r V S R i b A t u S d o V r t A r M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b A t u O b s e r v :   T S t r i n g F i e l d ;  
         Q r V S R i b A t u S e r i e F c h :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u F i c h a :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u M i s t u r o u :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u F o r n e c M O :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u C u s t o M O K g :   T F l o a t F i e l d ;  
         Q r V S R i b A t u C u s t o M O M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b A t u C u s t o M O T o t :   T F l o a t F i e l d ;  
         Q r V S R i b A t u V a l o r M P :   T F l o a t F i e l d ;  
         Q r V S R i b A t u D s t M o v I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u D s t N i v e l 1 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u D s t N i v e l 2 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u D s t G G X :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u Q t d G e r P e c a :   T F l o a t F i e l d ;  
         Q r V S R i b A t u Q t d G e r P e s o :   T F l o a t F i e l d ;  
         Q r V S R i b A t u Q t d G e r A r M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b A t u Q t d G e r A r P 2 :   T F l o a t F i e l d ;  
         Q r V S R i b A t u Q t d A n t P e c a :   T F l o a t F i e l d ;  
         Q r V S R i b A t u Q t d A n t P e s o :   T F l o a t F i e l d ;  
         Q r V S R i b A t u Q t d A n t A r M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b A t u Q t d A n t A r P 2 :   T F l o a t F i e l d ;  
         Q r V S R i b A t u N o t a M P A G :   T F l o a t F i e l d ;  
         Q r V S R i b A t u N O _ P A L L E T :   T S t r i n g F i e l d ;  
         Q r V S R i b A t u N O _ P R D _ T A M _ C O R :   T S t r i n g F i e l d ;  
         Q r V S R i b A t u N O _ T T W :   T S t r i n g F i e l d ;  
         Q r V S R i b A t u I D _ T T W :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u N O _ F O R N E C E :   T S t r i n g F i e l d ;  
         Q r V S R i b A t u R e q M o v E s t q :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u C U S T O _ M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b A t u C U S T O _ P 2 :   T F l o a t F i e l d ;  
         Q r V S R i b A t u N O _ L O C _ C E N :   T S t r i n g F i e l d ;  
         Q r V S R i b A t u M a r c a :   T S t r i n g F i e l d ;  
         Q r V S R i b A t u P e d I t s L i b :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u S t q C e n L o c :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u N O _ F I C H A :   T S t r i n g F i e l d ;  
         Q r V S R i b A t u N O _ F O R N E C _ M O :   T S t r i n g F i e l d ;  
         Q r V S R i b A t u C l i e n t M O :   T L a r g e i n t F i e l d ;  
         Q r V S R i b A t u C U S T O _ K G :   T F l o a t F i e l d ;  
         Q r V S R i b A t u C u s t o P Q :   T F l o a t F i e l d ;  
         Q r V S R i b D s t :   T m y S Q L Q u e r y ;  
         Q r V S R i b D s t C o d i g o :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t C o n t r o l e :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t M o v i m C o d :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t M o v i m N i v :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t M o v i m T w n :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t E m p r e s a :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t T e r c e i r o :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t C l i V e n d a :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t M o v i m I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t D a t a H o r a :   T D a t e T i m e F i e l d ;  
         Q r V S R i b D s t P a l l e t :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t G r a G r u X :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t P e c a s :   T F l o a t F i e l d ;  
         Q r V S R i b D s t P e s o K g :   T F l o a t F i e l d ;  
         Q r V S R i b D s t A r e a M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b D s t A r e a P 2 :   T F l o a t F i e l d ;  
         Q r V S R i b D s t V a l o r T :   T F l o a t F i e l d ;  
         Q r V S R i b D s t S r c M o v I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t S r c N i v e l 1 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t S r c N i v e l 2 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t S r c G G X :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t S d o V r t P e c a :   T F l o a t F i e l d ;  
         Q r V S R i b D s t S d o V r t P e s o :   T F l o a t F i e l d ;  
         Q r V S R i b D s t S d o V r t A r M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b D s t O b s e r v :   T S t r i n g F i e l d ;  
         Q r V S R i b D s t S e r i e F c h :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t F i c h a :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t M i s t u r o u :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t F o r n e c M O :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t C u s t o M O K g :   T F l o a t F i e l d ;  
         Q r V S R i b D s t C u s t o M O M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b D s t C u s t o M O T o t :   T F l o a t F i e l d ;  
         Q r V S R i b D s t V a l o r M P :   T F l o a t F i e l d ;  
         Q r V S R i b D s t D s t M o v I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t D s t N i v e l 1 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t D s t N i v e l 2 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t D s t G G X :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t Q t d G e r P e c a :   T F l o a t F i e l d ;  
         Q r V S R i b D s t Q t d G e r P e s o :   T F l o a t F i e l d ;  
         Q r V S R i b D s t Q t d G e r A r M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b D s t Q t d G e r A r P 2 :   T F l o a t F i e l d ;  
         Q r V S R i b D s t Q t d A n t P e c a :   T F l o a t F i e l d ;  
         Q r V S R i b D s t Q t d A n t P e s o :   T F l o a t F i e l d ;  
         Q r V S R i b D s t Q t d A n t A r M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b D s t Q t d A n t A r P 2 :   T F l o a t F i e l d ;  
         Q r V S R i b D s t N o t a M P A G :   T F l o a t F i e l d ;  
         Q r V S R i b D s t N O _ P A L L E T :   T S t r i n g F i e l d ;  
         Q r V S R i b D s t N O _ P R D _ T A M _ C O R :   T S t r i n g F i e l d ;  
         Q r V S R i b D s t N O _ T T W :   T S t r i n g F i e l d ;  
         Q r V S R i b D s t I D _ T T W :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t N O _ F O R N E C E :   T S t r i n g F i e l d ;  
         Q r V S R i b D s t N O _ S e r i e F c h :   T S t r i n g F i e l d ;  
         Q r V S R i b D s t R e q M o v E s t q :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t P e d I t s F i n :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t M a r c a :   T S t r i n g F i e l d ;  
         Q r V S R i b D s t S t q C e n L o c :   T L a r g e i n t F i e l d ;  
         Q r V S R i b D s t N O _ M o v i m N i v :   T S t r i n g F i e l d ;  
         Q r V S R i b D s t N O _ M o v i m I D :   T S t r i n g F i e l d ;  
         Q r P r o c :   T m y S Q L Q u e r y ;  
         Q r P r o c D t H r A b e r t o :   T D a t e T i m e F i e l d ;  
         Q r P r o c D t H r F i m O p e :   T D a t e T i m e F i e l d ;  
         Q r V S R i b D s t D t C o r r A p o :   T D a t e T i m e F i e l d ;  
         Q r V m i T w n :   T m y S Q L Q u e r y ;  
         Q r I M E C s J m p M o v I D :   T I n t e g e r F i e l d ;  
         Q r I M E C s J m p N i v e l 1 :   T I n t e g e r F i e l d ;  
         Q r I M E C s J m p N i v e l 2 :   T I n t e g e r F i e l d ;  
         Q r J m p A :   T m y S Q L Q u e r y ;  
         Q r J m p A S r c N i v e l 2 :   T I n t e g e r F i e l d ;  
         Q r J m p A M o v i m I D :   T I n t e g e r F i e l d ;  
         Q r J m p A C o d i g o :   T I n t e g e r F i e l d ;  
         Q r J m p A M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r J m p A C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r J m p B :   T m y S Q L Q u e r y ;  
         Q r J m p B M o v i m I D :   T I n t e g e r F i e l d ;  
         Q r J m p B C o d i g o :   T I n t e g e r F i e l d ;  
         Q r J m p B M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r J m p B C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r V S R i b I n d :   T m y S Q L Q u e r y ;  
         Q r V S R i b I n d C o d i g o :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d C o n t r o l e :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d M o v i m C o d :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d M o v i m N i v :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d M o v i m T w n :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d E m p r e s a :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d T e r c e i r o :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d C l i V e n d a :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d M o v i m I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d D a t a H o r a :   T D a t e T i m e F i e l d ;  
         Q r V S R i b I n d P a l l e t :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d G r a G r u X :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d P e c a s :   T F l o a t F i e l d ;  
         Q r V S R i b I n d P e s o K g :   T F l o a t F i e l d ;  
         Q r V S R i b I n d A r e a M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b I n d A r e a P 2 :   T F l o a t F i e l d ;  
         Q r V S R i b I n d V a l o r T :   T F l o a t F i e l d ;  
         Q r V S R i b I n d S r c M o v I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d S r c N i v e l 1 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d S r c N i v e l 2 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d S r c G G X :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d S d o V r t P e c a :   T F l o a t F i e l d ;  
         Q r V S R i b I n d S d o V r t P e s o :   T F l o a t F i e l d ;  
         Q r V S R i b I n d S d o V r t A r M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b I n d O b s e r v :   T S t r i n g F i e l d ;  
         Q r V S R i b I n d S e r i e F c h :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d F i c h a :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d M i s t u r o u :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d F o r n e c M O :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d C u s t o M O K g :   T F l o a t F i e l d ;  
         Q r V S R i b I n d C u s t o M O M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b I n d C u s t o M O T o t :   T F l o a t F i e l d ;  
         Q r V S R i b I n d V a l o r M P :   T F l o a t F i e l d ;  
         Q r V S R i b I n d D s t M o v I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d D s t N i v e l 1 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d D s t N i v e l 2 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d D s t G G X :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d Q t d G e r P e c a :   T F l o a t F i e l d ;  
         Q r V S R i b I n d Q t d G e r P e s o :   T F l o a t F i e l d ;  
         Q r V S R i b I n d Q t d G e r A r M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b I n d Q t d G e r A r P 2 :   T F l o a t F i e l d ;  
         Q r V S R i b I n d Q t d A n t P e c a :   T F l o a t F i e l d ;  
         Q r V S R i b I n d Q t d A n t P e s o :   T F l o a t F i e l d ;  
         Q r V S R i b I n d Q t d A n t A r M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b I n d Q t d A n t A r P 2 :   T F l o a t F i e l d ;  
         Q r V S R i b I n d N o t a M P A G :   T F l o a t F i e l d ;  
         Q r V S R i b I n d N O _ P A L L E T :   T S t r i n g F i e l d ;  
         Q r V S R i b I n d N O _ P R D _ T A M _ C O R :   T S t r i n g F i e l d ;  
         Q r V S R i b I n d N O _ T T W :   T S t r i n g F i e l d ;  
         Q r V S R i b I n d I D _ T T W :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d N O _ F O R N E C E :   T S t r i n g F i e l d ;  
         Q r V S R i b I n d N O _ S e r i e F c h :   T S t r i n g F i e l d ;  
         Q r V S R i b I n d R e q M o v E s t q :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d P e d I t s F i n :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d M a r c a :   T S t r i n g F i e l d ;  
         Q r V S R i b I n d S t q C e n L o c :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d N O _ M o v i m N i v :   T S t r i n g F i e l d ;  
         Q r V S R i b I n d N O _ M o v i m I D :   T S t r i n g F i e l d ;  
         Q r V S R i b I n d D t C o r r A p o :   T D a t e T i m e F i e l d ;  
         Q r V S R i b I n d J m p M o v I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d J m p N i v e l 1 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d J m p N i v e l 2 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d J m p G G X :   T L a r g e i n t F i e l d ;  
         Q r F a t h e r :   T m y S Q L Q u e r y ;  
         Q r F a t h e r M o v C o d P a i :   T I n t e g e r F i e l d ;  
         Q r I M E C s M o v C o d P a i :   T I n t e g e r F i e l d ;  
         Q r V S R i b O r i I M E I :   T m y S Q L Q u e r y ;  
         Q r V S R i b O r i I M E I C o d i g o :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I C o n t r o l e :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I M o v i m C o d :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I M o v i m N i v :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I M o v i m T w n :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I E m p r e s a :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I T e r c e i r o :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I C l i V e n d a :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I M o v i m I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I D a t a H o r a :   T D a t e T i m e F i e l d ;  
         Q r V S R i b O r i I M E I P a l l e t :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I G r a G r u X :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I P e c a s :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I P e s o K g :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I A r e a M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I A r e a P 2 :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I V a l o r T :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I S r c M o v I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I S r c N i v e l 1 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I S r c N i v e l 2 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I S r c G G X :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I S d o V r t P e c a :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I S d o V r t P e s o :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I S d o V r t A r M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I O b s e r v :   T S t r i n g F i e l d ;  
         Q r V S R i b O r i I M E I S e r i e F c h :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I F i c h a :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I M i s t u r o u :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I F o r n e c M O :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I C u s t o M O K g :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I C u s t o M O M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I C u s t o M O T o t :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I V a l o r M P :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I D s t M o v I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I D s t N i v e l 1 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I D s t N i v e l 2 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I D s t G G X :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I Q t d G e r P e c a :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I Q t d G e r P e s o :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I Q t d G e r A r M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I Q t d G e r A r P 2 :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I Q t d A n t P e c a :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I Q t d A n t P e s o :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I Q t d A n t A r M 2 :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I Q t d A n t A r P 2 :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I N o t a M P A G :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I N O _ P A L L E T :   T S t r i n g F i e l d ;  
         Q r V S R i b O r i I M E I N O _ P R D _ T A M _ C O R :   T S t r i n g F i e l d ;  
         Q r V S R i b O r i I M E I N O _ T T W :   T S t r i n g F i e l d ;  
         Q r V S R i b O r i I M E I I D _ T T W :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I N O _ F O R N E C E :   T S t r i n g F i e l d ;  
         Q r V S R i b O r i I M E I N O _ S e r i e F c h :   T S t r i n g F i e l d ;  
         Q r V S R i b O r i I M E I R e q M o v E s t q :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I C u s t o P Q :   T F l o a t F i e l d ;  
         Q r V S R i b O r i I M E I V S M u l F r n C a b :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I C l i e n t M O :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I R m s M o v I D :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I R m s N i v e l 1 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I R m s N i v e l 2 :   T L a r g e i n t F i e l d ;  
         Q r V S R i b O r i I M E I D t C o r r A p o :   T D a t e T i m e F i e l d ;  
         Q r V S R i b O r i I M E I R m s G G X :   T L a r g e i n t F i e l d ;  
         Q r V S R i b I n d R m s G G X :   T L a r g e i n t F i e l d ;  
         Q r C a b 3 4 :   T m y S Q L Q u e r y ;  
         Q r C a b 3 4 M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r C a l T o C u r G r a G r u X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s J m p G G X :   T I n t e g e r F i e l d ;  
         p r o c e d u r e   F o r m C r e a t e ( S e n d e r :   T O b j e c t ) ;  
     p r i v a t e  
         {   P r i v a t e   d e c l a r a t i o n s   }  
         F S E I I _ I M E c s :   S t r i n g ;  
         / /  
         f u n c t i o n     I m p o r t a O p e P r o c B _ I s o l a d a ( Q r C a b :   T M y S Q L Q u e r y ;   P s q M o v i m N i v :  
                             T E s t q M o v i m N i v ;   S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,  
                             S Q L _ D t H r F i m O p e :   S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
         f u n c t i o n     I m p o r t a O p e P r o c B _ C o n j u n t a ( Q r C a b :   T M y S Q L Q u e r y ;   M o v i m I D ,  
                             P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X :   T E s t q M o v i m I D ;   M o v i m N i v ,  
                             P s q M o v i m N i v _ V M I :   T E s t q M o v i m N i v ;   S Q L _ P e r i o d o C o m p l e x o ,  
                             S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   S Q L _ D t H r F i m O p e :  
                             S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
         f u n c t i o n     R e o p e n V m i T w n ( M o v i m I D :   T E s t q M o v i m I D ;   M o v i m N i v :   T E s t q M o v i m N i v ;  
                             M o v i m C o d ,   M o v i m T w n :   I n t e g e r ) :   B o o l e a n ;  
                             / /   G A r  
         f u n c t i o n     F i l t r o R e g G A R ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
         f u n c t i o n     F i l t r o P e r i o d o G A R ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;   S Q L _ P e r i o d o :  
                             S t r i n g ) :   S t r i n g ;  
         f u n c t i o n     G e r a G A R ( S Q L _ P e r i o d o :   S t r i n g ) :   B o o l e a n ;  
         p r o c e d u r e   R e o p e n O b t C l i F o r S i t e ( F o r m a :   T O p e n C l i F o r S i t e ;   C o n t r o l e ,   M o v i m C o d ,  
                             M o v i m N i v :   I n t e g e r ) ;  
         p r o c e d u r e   R e o p e n V m i G A r C a b ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;   S Q L _ P e r i o d o :  
                             S t r i n g ) ;  
         p r o c e d u r e   R e o p e n V m i G A r I t s ( M o v i m C o d ,   M o v i m T w n :   I n t e g e r ;   S Q L _ P e r i o d o :   S t r i n g ;  
                             T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) ;  
                             / /   F i m   G A r  
         p r o c e d u r e   V e r i f i c a T i p o E s t q Q t d e ( T i p o E s t q :   I n t e g e r ;   A r e a M 2 ,   P e s o K g ,   P e c a s :  
                             D o u b l e ;   G r a G r u X ,   M o v i m C o d :   I n t e g e r ;   P r o c N a m e :   S t r i n g ) ;  
     p u b l i c  
         {   P u b l i c   d e c l a r a t i o n s   }  
         F S Q L _ P e r i o d o P Q :   S t r i n g ;  
         F I m p o r E x p o r ,   F E m p r e s a ,   F A n o M e s ,   F P e r i A p u ,   F S P E D _ E F D _ P r o d u c a o :   I n t e g e r ;  
         F D i a I n i ,   F D i a F i m ,   F D i a P o s ( * ,   F D a t a * ) :   T D a t e T i m e ;  
         F T i p o P e r i o d o F i s c a l :   T T i p o P e r i o d o F i s c a l ;  
         P B 2 :   T P r o g r e s s B a r ;  
         L a A v i s o 1 ,   L a A v i s o 2 ,   L a A v i s o 3 ,   L a A v i s o 4 :   T L a b e l ;  
         M e A v i s o :   T M e m o ;  
         / /  
         f u n c t i o n     D e f i n e G G X C a l T o C u r ( C a m p o :   S t r i n g ;   C o n t r o l e :   I n t e g e r ) :   I n t e g e r ;  
         f u n c t i o n     I m p o r t a G e r A r t I s o l a d o ( S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e :  
                             S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
         f u n c t i o n     I m p o r t a G e r A r t C o n j u n t o ( S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e :  
                             S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
         f u n c t i o n     I m p o r t a O p e P r o c A ( Q r C a b :   T M y S Q L Q u e r y ;   M o v i m I D :   T E s t q M o v i m I D ;  
                             S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   S Q L _ D t H r F i m O p e :  
                             S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
         f u n c t i o n     I m p o r t a O p e P r o c B ( Q r C a b :   T M y S Q L Q u e r y ;   M o v i m I D ,   P s q M o v i m I D _ V M I ,  
                             P s q M o v i m I D _ P Q X :   T E s t q M o v i m I D ;   M o v i m N i v ,   P s q M o v i m N i v _ V M I :  
                             T E s t q M o v i m N i v ;   S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,  
                             S Q L _ D t H r F i m O p e :   S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e _ O r i g e P r o c P Q ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;   M o v i m I D :  
                             T E s t q M o v i m I D ;   M o v i m N i v :   T E s t q M o v i m N i v ;   M o v i m C o d :   I n t e g e r ;  
                             I D S e q 1 :   I n t e g e r ;   S Q L _ P e r i o d o P Q :   S t r i n g ;   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ;   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o :   I n t e g e r ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e _ O r i g e R i b D T A _ 2 1 0 ( c o n s t   T i p o R e g S P E D P r o d :  
                             T T i p o R e g S P E D P r o d ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t   M o v i m N i v :  
                             T E s t q M o v i m N i v ;   c o n s t   S Q L _ P e r i o d o V S :   S t r i n g ;   c o n s t   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e _ O r i g e R i b P D A _ 2 1 0 ( c o n s t   T i p o R e g S P E D P r o d :  
                             T T i p o R e g S P E D P r o d ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t   M o v i m N i v :  
                             T E s t q M o v i m N i v ;   c o n s t   S Q L _ P e r i o d o V S :   S t r i n g ;   c o n s t   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e _ O r i g e P r o c V S _ 2 1 0 ( c o n s t   T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;  
                             c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t   M o v i m N i v I n n ,   M o v i m N i v B x a :  
                             T E s t q M o v i m N i v ;   c o n s t   Q r C a b :   T m y S Q L Q u e r y ;   v a r   I D S e q 1 :   I n t e g e r ;  
                             c o n s t   S Q L _ P e r i o d o V S :   S t r i n g ;   c o n s t   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :  
                             B o o l e a n ;  
         f u n c t i o n     I n s e r e _ E m O p e P r o c _ 2 1 5 ( c o n s t   T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;  
                             c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   M o v i m N i v :   T E s t q M o v i m N i v ;   c o n s t   Q r C a b :  
                             T m y S Q L Q u e r y ;   ( * c o n s t   F a t o r :   D o u b l e ; * )   c o n s t   S Q L _ P e r i o d o :   S t r i n g ;  
                             c o n s t   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;   v a r   I D S e q 1 :   I n t e g e r ) :  
                             B o o l e a n ;  
         p r o c e d u r e   I n s e r e _ E m O p e P r o c _ P r d ( c o n s t   T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;  
                             c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t   Q r y :   T m y S Q L Q u e r y ;   c o n s t   F a t o r :  
                             D o u b l e ;   c o n s t   S Q L _ P e r i o d o ,   S Q L _ P e r i o d o P Q :   S t r i n g ;   c o n s t  
                             O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;   c o n s t   M o v i m N i v :   T E s t q M o v i m N i v ) ;  
         / /  
         f u n c t i o n     G r a n d e z a s I n c o n s i s t e n t e s ( ) :   B o o l e a n ;  
         p r o c e d u r e   M e n s a g e m ( P r o c N a m e :   S t r i n g ;   M o v i m C o d ,   M o v i m I D :   I n t e g e r ;   T e x t o B a s e :  
                             S t r i n g ;   Q u e r y :   T m y S Q L Q u e r y ) ;  
         p r o c e d u r e   P e s q u i s a I M E C s R e l a c i o n a d o s ( T a r g e t M o v i m I D ,   P s q M o v i m I D _ V M I ,  
                             P s q M o v i m I D _ P Q X :   T E s t q M o v i m I D ;   P s q M o v i m N i v _ V M I :   T E s t q M o v i m N i v ;  
                             S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q :   S t r i n g ;   I n c l u i P Q x :   B o o l e a n ) ;  
         p r o c e d u r e   R e o p e n O r i P Q x ( M o v i m C o d :   I n t e g e r ;   S Q L _ P e r i o d o P Q :   S t r i n g ;  
                             S h o w S Q L :   B o o l e a n ) ;  
         / /  
         p r o c e d u r e   I n s e r e B _ P r o d u c a o I s o l a d a ( S Q L x ,   S Q L _ P e r i o d o P Q :   S t r i n g ;   P s q M o v i m I D :  
                             T E s t q M o v i m I D ;   P s q M o v i m N i v :   T E s t q M o v i m N i v ;   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ) ;  
         p r o c e d u r e   I n s e r e B _ P r o d u c a o C o n j u n t a ( T a b C a b :   S t r i n g ;   T a r g e t M o v i m I D ,  
                             P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X :   T E s t q M o v i m I D ;   P s q M o v i m N i v :  
                             T E s t q M o v i m N i v ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;  
                             F o r m a I n s e r c a o I t e m P r o d u c a o :   T F o r m a I n s e r c a o I t e m P r o d u c a o ) ;  
         f u n c t i o n     I n s e r e B _ O r i g e P r o c P Q _ C o n j u n t a ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;  
                             M o v i m I D :   T E s t q M o v i m I D ;   M o v i m N i v :   T E s t q M o v i m N i v ;   M o v i m C o d :   I n t e g e r ;  
                             I D S e q 1 :   I n t e g e r ;   S Q L _ P e r i o d o P Q :   S t r i n g ;   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ;   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o :   I n t e g e r ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e B _ O r i g e P r o c P Q _ I s o l a d a ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;  
                             M o v i m I D :   T E s t q M o v i m I D ;   M o v i m N i v :   T E s t q M o v i m N i v ;   M o v i m C o d :   I n t e g e r ;  
                             I D S e q 1 :   I n t e g e r ;   S Q L _ P e r i o d o P Q :   S t r i n g ;   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ;   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o :   I n t e g e r ;  
                             D t H r A b e r t o :   T D a t e T i m e ) :   B o o l e a n ;  
         p r o c e d u r e   I n s e r e I t e m P r o d u c a o C o n j u n t a G e m e o s ( c o n s t   D t H r A b e r t o ,   D t H r F i m O p e ,  
                             D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t  
                             E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,   C o d i g o ,   C o n t r o l e ,   G r a G r u X :   I n t e g e r ;  
                             c o n s t   M o v i m N i v :   T E s t q M o v i m N i v ;   c o n s t   M o v i m T w n :   I n t e g e r ;   c o n s t  
                             _ A r e a M 2 ,   _ P e s o K g ,   _ P e c a s :   D o u b l e ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;  
                             c o n s t   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,   P a i M o v I D ,  
                             M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 :   I n t e g e r ;   v a r   I D S e q 1 _ K 2 9 0 ,  
                             I D S e q 1 _ K 3 0 0 :   I n t e g e r ;   v a r   I n s e r i u P Q :   B o o l e a n ) ;  
         p r o c e d u r e   I n s e r e I t e m P r o d u c a o I s o l a d a G e m e o s ( c o n s t   D t H r A b e r t o ,   D t H r F i m O p e ,  
                             D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t  
                             E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,   C o d i g o ,   C o n t r o l e ,   G r a G r u X :   I n t e g e r ;  
                             c o n s t   M o v i m N i v :   T E s t q M o v i m N i v ;   c o n s t   M o v i m T w n :   I n t e g e r ;   c o n s t  
                             _ A r e a M 2 ,   _ P e s o K g ,   _ P e c a s :   D o u b l e ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;  
                             c o n s t   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,   P a i M o v I D ,  
                             M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 :   I n t e g e r ;   v a r   I D S e q 1 _ K 2 9 0 ,  
                             I D S e q 1 _ K 3 0 0 :   I n t e g e r ;   v a r   I n s e r i u P Q :   B o o l e a n ) ;  
         p r o c e d u r e   I n s e r e I t e m P r o d u c a o C o n j u n t a I n d i r e t a ( c o n s t   D t H r A b e r t o ,   D t H r F i m O p e ,  
                             D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t  
                             E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,   C o d i g o ,   C o n t r o l e ,   G r a G r u X :   I n t e g e r ;  
                             c o n s t   M o v i m N i v :   T E s t q M o v i m N i v ;   c o n s t   M o v i m T w n :   I n t e g e r ;   c o n s t  
                             _ A r e a M 2 ,   _ P e s o K g ,   _ P e c a s :   D o u b l e ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;  
                             c o n s t   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,   P a i M o v I D ,  
                             M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,   G e r M o v I D ,   G e r N i v e l 1 ,   G e r N i v e l 2 ,  
                             G e r G G X :   I n t e g e r ;   c o n s t   Q t d G e r P e c a ,   Q t d G e r P e s o ,   Q t d G e r A r M 2 :   D o u b l e ;  
                             v a r   I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 :   I n t e g e r ;   v a r  
                             I n s e r i u P Q :   B o o l e a n ) ;  
         p r o c e d u r e   I n s e r e I t e m P r o d u c a o I s o l a d a I n d i r e t a ( c o n s t   D t H r A b e r t o ,   D t H r F i m O p e ,  
                             D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t  
                             E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,   C o d i g o ,   C o n t r o l e ,   G r a G r u X :   I n t e g e r ;  
                             c o n s t   M o v i m N i v :   T E s t q M o v i m N i v ;   c o n s t   M o v i m T w n :   I n t e g e r ;   c o n s t  
                             _ A r e a M 2 ,   _ P e s o K g ,   _ P e c a s :   D o u b l e ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;  
                             c o n s t   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,   P a i M o v I D ,  
                             M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,   G e r M o v I D ,   G e r N i v e l 1 ,   G e r N i v e l 2 ,  
                             G e r G G X :   I n t e g e r ;   c o n s t   Q t d G e r P e c a ,   Q t d G e r P e s o ,   Q t d G e r A r M 2 :   D o u b l e ;  
                             v a r   I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 :   I n t e g e r ;   v a r  
                             I n s e r i u P Q :   B o o l e a n ) ;  
         p r o c e d u r e   I n s e r e I t e m P r o d u c a o C o n j u n t a U n i A n t ( c o n s t   D t H r A b e r t o ,   D t H r F i m O p e ,  
                             D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t  
                             E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,   C o d i g o ,   C o n t r o l e ,   G G X O r i ,   G G X D s t :  
                             I n t e g e r ;   c o n s t   M o v i m N i v :   T E s t q M o v i m N i v ;   c o n s t   M o v i m T w n :   I n t e g e r ;  
                             c o n s t   _ A r e a M 2 ,   _ P e s o K g ,   _ P e c a s :   D o u b l e ;   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ;   c o n s t   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,  
                             P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 :   I n t e g e r ;   c o n s t  
                             Q t d A n t P e c a ,   Q t d A n t P e s o ,   Q t d A n t A r M 2 :   D o u b l e ;   v a r   I D S e q 1 _ K 2 9 0 ,  
                             I D S e q 1 _ K 3 0 0 :   I n t e g e r ;   v a r   I n s e r i u P Q :   B o o l e a n ) ;  
         p r o c e d u r e   I n s e r e I t e m P r o d u c a o I s o l a d a U n i A n t ( c o n s t   D t H r A b e r t o ,   D t H r F i m O p e ,  
                             D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t  
                             E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,   C o d i g o ,   C o n t r o l e ,   G G X O r i ,   G G X D s t :  
                             I n t e g e r ;   c o n s t   M o v i m N i v :   T E s t q M o v i m N i v ;   c o n s t   M o v i m T w n :   I n t e g e r ;  
                             c o n s t   _ A r e a M 2 ,   _ P e s o K g ,   _ P e c a s :   D o u b l e ;   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ;   c o n s t   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,  
                             P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 :   I n t e g e r ;   c o n s t  
                             Q t d A n t P e c a ,   Q t d A n t P e s o ,   Q t d A n t A r M 2 :   D o u b l e ;   v a r   I D S e q 1 _ K 2 9 0 ,  
                             I D S e q 1 _ K 3 0 0 :   I n t e g e r ;   v a r   I n s e r i u P Q :   B o o l e a n ) ;  
         p r o c e d u r e   I n s e r e I t e m P r o d u c a o C o n j u n t a U n i G e r ( c o n s t   D t H r A b e r t o ,   D t H r F i m O p e ,  
                             D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t  
                             E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,   C o d i g o ,   C o n t r o l e ,   G G X O r i ,   G G X D s t :  
                             I n t e g e r ;   c o n s t   M o v i m N i v :   T E s t q M o v i m N i v ;   c o n s t   M o v i m T w n :   I n t e g e r ;  
                             c o n s t   _ A r e a M 2 ,   _ P e s o K g ,   _ P e c a s :   D o u b l e ;   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ;   c o n s t   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,  
                             P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 :   I n t e g e r ;   c o n s t  
                             Q t d G e r P e c a ,   Q t d G e r P e s o ,   Q t d G e r A r M 2 :   D o u b l e ;   v a r   I D S e q 1 _ K 2 9 0 ,  
                             I D S e q 1 _ K 3 0 0 :   I n t e g e r ;   v a r   I n s e r i u P Q :   B o o l e a n ) ;  
         p r o c e d u r e   I n s e r e I t e m P r o d u c a o I s o l a d a U n i G e r ( c o n s t   D t H r A b e r t o ,   D t H r F i m O p e ,  
                             D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t  
                             E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,   C o d i g o ,   C o n t r o l e ,   G G X O r i ,   G G X D s t :  
                             I n t e g e r ;   c o n s t   M o v i m N i v :   T E s t q M o v i m N i v ;   c o n s t   M o v i m T w n :   I n t e g e r ;  
                             c o n s t   _ A r e a M 2 ,   _ P e s o K g ,   _ P e c a s :   D o u b l e ;   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ;   c o n s t   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,  
                             P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 :   I n t e g e r ;   c o n s t  
                             Q t d G e r P e c a ,   Q t d G e r P e s o ,   Q t d G e r A r M 2 :   D o u b l e ;   v a r   I D S e q 1 _ K 2 9 0 ,  
                             I D S e q 1 _ K 3 0 0 :   I n t e g e r ;   v a r   I n s e r i u P Q :   B o o l e a n ) ;  
         p r o c e d u r e   D e f i n e C l i F o r S i t e O n l y ( c o n s t   C o n t r o l e :   I n t e g e r ;   v a r   C l i e n t M O ,   F o r n e c M O ,  
                             E n t i S i t i o :   I n t e g e r ) ;  
         p r o c e d u r e   D e f i n e C l i F o r S i t e T i p E s t q ( c o n s t   C o n t r o l e :   I n t e g e r ;   v a r   C l i e n t M O ,   F o r n e c M O ,  
                             E n t i S i t i o ,   T i p o E s t q :   I n t e g e r ) ;  
         p r o c e d u r e   I n s e r e I t e m P r o d u c a o C o n j u n t a S i m p l e s ( c o n s t   D t H r A b e r t o ,   D t H r F i m O p e ,  
                             D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;   c o n s t   M o v i m I D :   T E s t q M o v i m I D ;   c o n s t  
                             E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,   C o d i g o ,   C o n t r o l e ,   G r a G r u X :  
                             I n t e g e r ;   c o n s t   M o v i m N i v :   T E s t q M o v i m N i v ;   c o n s t   M o v i m T w n :   I n t e g e r ;  
                             c o n s t   A r e a M 2 ,   P e s o K g ,   P e c a s :   D o u b l e ;   O r i g e m O p e P r o c :  
                             T O r i g e m O p e P r o c ;   c o n s t   P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,  
                             F a t o r :   I n t e g e r ;   c o n s t   O r i E S T S T a b S o r c :   T E s t q S P E D T a b S o r c ;  
                             c o n s t   S P E D _ E F D _ G e r B x a :   T S P E D _ E F D _ G e r B x a ;  
                             v a r   I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 :   I n t e g e r ) ;  
         p r o c e d u r e   V e r i f i c a S e T e m G r a G r u X ( I D L i n P a s ,   G r a G r u X :   I n t e g e r ;   P r o c N a m e :   S t r i n g ;  
                             Q r y :   T m y S Q L Q u e r y ) ;  
     e n d ;  
  
 v a r  
     F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a :   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a ;  
  
 i m p l e m e n t a t i o n  
 u s e s  
     M o d u l e ,   U M y S Q L M o d u l e ,   M o d u l e G e r a l ,   U n V S _ P F ,   M o d A p p G r a G 1 ,   P Q x ,   D m k D A C _ P F ,  
     U n S P E D _ E F D _ P F ,   G r a G r u X ,   U n G r a d e _ P F ,   U n S P E D _ C r e a t e ,   U n V S _ E F D _ I C M S _ I P I ;  
  
  
 { $ R   * . d f m }  
  
 {   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a   }  
  
 p r o c e d u r e   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . D e f i n e C l i F o r S i t e O n l y (  
     c o n s t   C o n t r o l e :   I n t e g e r ;   v a r   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o :   I n t e g e r ) ;  
 c o n s t  
     s P r o c N a m e   =   ' F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . D e f i n e C l i F o r S i t e O n l y ( ) ' ;  
 v a r  
     M o v i m I D ,   P s q M o v i m N i v ,   M o v i m C o d :   I n t e g e r ;  
     / /  
     p r o c e d u r e   M s g ( T e x t o :   S t r i n g ) ;  
     b e g i n  
         G e r a l . M B _ E r r o ( T e x t o   +   '   e m   '   +   s p r o c N a m e   +   s L i n e B r e a k   +  
         ' I M E - I   =   '   +   G e r a l . F F 0 ( C o n t r o l e ) ) ;  
     e n d ;  
 b e g i n  
     R e o p e n O b t C l i F o r S i t e ( o c f s C o n t r o l e ,   C o n t r o l e ,   0 ,   0 ) ;  
     / / N � o   f e c h a r   Q r O b t C l i F o r S i t e ! ! !  
     C l i e n t M O         : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' C l i e n t M O ' ) . A s I n t e g e r ;  
     F o r n e c M O         : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' F o r n e c M O ' ) . A s I n t e g e r ;  
     E n t i S i t i o       : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' E n t i S i t i o ' ) . A s I n t e g e r ;  
     / /  
     i f   ( F o r n e c M O   =   0 )   o r   ( C l i e n t M O   =   0 )   o r   ( E n t i S i t i o   =   0 )   t h e n  
     b e g i n  
         M o v i m C o d         : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' M o v i m C o d ' ) . A s I n t e g e r ;  
         M o v i m I D           : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' M o v i m I D ' ) . A s I n t e g e r ;  
         P s q M o v i m N i v   : =   I n t e g e r ( V S _ P F . O b t e m M o v i m N i v C l i F o r L o c D e M o v i m I D ( T E s t q M o v i m I D ( M o v i m I D ) ) ) ;  
         / /  
         R e o p e n O b t C l i F o r S i t e ( o c f s M o v I d N i v C o d ,   0 ,   M o v i m C o d ,   P s q M o v i m N i v ) ;  
         i f   C l i e n t M O   =   0   t h e n  
             C l i e n t M O     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' C l i e n t M O ' ) . A s I n t e g e r ;  
         i f   F o r n e c M O   =   0   t h e n  
             F o r n e c M O     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' F o r n e c M O ' ) . A s I n t e g e r ;  
         i f   E n t i S i t i o   =   0   t h e n  
             E n t i S i t i o   : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' E n t i S i t i o ' ) . A s I n t e g e r ;  
     e n d ;  
     i f   C l i e n t M O   =   0   t h e n  
         M s g ( ' C l i e n t M O   n a o   d e f i n i d o ' ) ;  
     i f   ( F o r n e c M O   =   0 )   a n d   ( E n t i S i t i o   =   0 )   t h e n  
         M s g ( ' F o r n e c M O   e   E n t i S i t i o   n a o   d e f i n i d o s ' ) ;  
 e n d ;  
  
 p r o c e d u r e   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . D e f i n e C l i F o r S i t e T i p E s t q ( c o n s t   C o n t r o l e :  
     I n t e g e r ;   v a r   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   T i p o E s t q :   I n t e g e r ) ;  
 c o n s t  
     s P r o c N a m e   =   ' F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . D e f i n e C l i F o r S i t e T i p E s t q ( ) ' ;  
 v a r  
     M o v i m I D ,   P s q M o v i m N i v ,   M o v i m C o d :   I n t e g e r ;  
     / /  
     p r o c e d u r e   M s g ( T e x t o :   S t r i n g ) ;  
     b e g i n  
         G e r a l . M B _ E r r o ( T e x t o   +   '   e m   '   +   s p r o c N a m e   +   s L i n e B r e a k   +  
         ' I M E - I   =   '   +   G e r a l . F F 0 ( C o n t r o l e ) ) ;  
     e n d ;  
 b e g i n  
     R e o p e n O b t C l i F o r S i t e ( o c f s C o n t r o l e ,   C o n t r o l e ,   0 ,   0 ) ;  
     / / N � o   f e c h a r   Q r O b t C l i F o r S i t e ! ! !  
     C l i e n t M O         : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' C l i e n t M O ' ) . A s I n t e g e r ;  
     F o r n e c M O         : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' F o r n e c M O ' ) . A s I n t e g e r ;  
     E n t i S i t i o       : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' E n t i S i t i o ' ) . A s I n t e g e r ;  
     T i p o E s t q   : =   T r u n c ( Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' T i p o E s t q ' ) . A s F l o a t ) ;  
     / /  
     i f   ( F o r n e c M O   =   0 )   o r   ( C l i e n t M O   =   0 )   o r   ( E n t i S i t i o   =   0 )   t h e n  
     b e g i n  
         M o v i m C o d         : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' M o v i m C o d ' ) . A s I n t e g e r ;  
         M o v i m I D           : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' M o v i m I D ' ) . A s I n t e g e r ;  
         P s q M o v i m N i v   : =   I n t e g e r ( V S _ P F . O b t e m M o v i m N i v C l i F o r L o c D e M o v i m I D ( T E s t q M o v i m I D ( M o v i m I D ) ) ) ;  
         / /  
         R e o p e n O b t C l i F o r S i t e ( o c f s M o v I d N i v C o d ,   0 ,   M o v i m C o d ,   P s q M o v i m N i v ) ;  
         i f   C l i e n t M O   =   0   t h e n  
             C l i e n t M O     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' C l i e n t M O ' ) . A s I n t e g e r ;  
         i f   F o r n e c M O   =   0   t h e n  
             F o r n e c M O     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' F o r n e c M O ' ) . A s I n t e g e r ;  
         i f   E n t i S i t i o   =   0   t h e n  
             E n t i S i t i o   : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' E n t i S i t i o ' ) . A s I n t e g e r ;  
     e n d ;  
     i f   C l i e n t M O   =   0   t h e n  
         M s g ( ' C l i e n t M O   n a o   d e f i n i d o ' ) ;  
     i f   ( F o r n e c M O   =   0 )   a n d   ( E n t i S i t i o   =   0 )   t h e n  
         M s g ( ' F o r n e c M O   e   E n t i S i t i o   n a o   d e f i n i d o s ' ) ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . D e f i n e G G X C a l T o C u r ( C a m p o :   S t r i n g ;  
     C o n t r o l e :   I n t e g e r ) :   I n t e g e r ;  
 b e g i n  
     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r C a l T o C u r ,   D m o d . M y D B ,   [  
     / / ' S E L E C T   D s t G G X   ' ,  
     ' S E L E C T   '   +   C a m p o   +   '   G r a G r u X   ' ,  
     ' F R O M   '   +   C O _ S E L _ T A B _ V M I ,  
     ' W H E R E   C o n t r o l e = '   +   G e r a l . F F 0 ( C o n t r o l e ) ,  
     ' ' ] ) ;  
     / / G e r a l . M B _ S Q L ( S e l f ,   Q r C a l T o C u r ) ;  
     R e s u l t   : =   Q r C a l T o C u r G r a G r u X . V a l u e ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . F i l t r o P e r i o d o G A R (  
     T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;   S Q L _ P e r i o d o :   S t r i n g ) :   S t r i n g ;  
 b e g i n  
     c a s e   T i p o R e g S P E D P r o d   o f  
         t r s p 2 3 X ,   t r s p 2 9 X :   R e s u l t   : =   S Q L _ P e r i o d o ;  
         t r s p 2 5 X ,   t r s p 3 0 X :   R e s u l t   : =   ' ' ;  
         e l s e  
         b e g i n  
             G e r a l . M B _ E r r o ( ' " F i l t r o R e g "   i n d e f i n i d o ! ' ) ;  
             R e s u l t   : =   ' # E R R _ P E R I O D O _ ' ;  
         e n d ;  
     e n d ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . F i l t r o R e g G A R (  
     T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
 b e g i n  
     c a s e   T i p o R e g S P E D P r o d   o f  
         t r s p 2 3 X ,   t r s p 2 9 X :   R e s u l t   : =   ' = ' ;  
         t r s p 2 5 X ,   t r s p 3 0 X :   R e s u l t   : =   ' < > ' ;  
         e l s e  
         b e g i n  
             G e r a l . M B _ E r r o ( ' " F i l t r o P e r i o d o G A R "   i n d e f i n i d o :   '   +  
                 G e r a l . F F 0 ( I n t e g e r ( T i p o R e g S P E D P r o d ) )   +   ' ! ' ) ;  
             R e s u l t   : =   ' # E R R ' ;  
         e n d ;  
     e n d ;  
 e n d ;  
  
 p r o c e d u r e   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . F o r m C r e a t e ( S e n d e r :   T O b j e c t ) ;  
 b e g i n  
     F S E I I _ I M E c s   : =   S P E D _ C r e a t e . R e c r i a T e m p T a b l e N o v o ( n t r t t S P E D E F D _ I M E C s ,   D M o d G . Q r U p d P I D 1 ,   F a l s e ) ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . G e r a G A R (  
     S Q L _ P e r i o d o :   S t r i n g ) :   B o o l e a n ;  
 b e g i n  
     U n D m k D A C _ P F . E x e c u t a M y S Q L Q u e r y 0 ( D M o d G . Q r U p d P I D 1 ,   D M o d G . M y P I D _ D B ,   [  
     ' D R O P   T A B L E   I F   E X I S T S   _ S P E D _ E F D _ K 2 X X _ G A R ; ' ,  
     ' C R E A T E   T A B L E   _ S P E D _ E F D _ K 2 X X _ G A R ' ,  
     ' S E L E C T   v m i . D s t G G X ,   v m i . S r c G G X ,   ' ,  
     ' g g 1 . U n i d M e d ,   m e d . G r a n d e z a   m e d _ g r a n d e z a ,   ' ,  
     ' x c o . G r a n d e z a   x c o _ G r a n d e z a ,   g g x . G r a G r u Y ,   x c o . C o u N i v 2 ,   ' ,  
     V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( F a l s e ,   ' T i p o E s t q ' ,   T r u e ,  
     ' v m i . A r e a M 2 ' ,   ' v m i . P e s o K g ' ,   ' v m i . P e c a s ' ) ,  
     ' D A T E ( v m i . D a t a H o r a )   D a t a ,   v m i . C o d i g o ,   v m i . M o v i m C o d ,     ' ,  
     ' v m i . G r a G r u X ,   v m i . P e c a s ,   v m i . A r e a M 2 ,   v m i . P e s o K g ,     ' ,  
     ' v m i . M o v i m T w n     ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . '   +   C O _ S E L _ T A B _ V M I   +   '                 v m i       ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x         g g x   O N   g g x . C o n t r o l e = v m i . G r a G r u X     ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1   ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d   ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e     ' ,  
     ' W H E R E   v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( e m i n B a i x C u r t i V S ) ) ,             / / 1 5  
     S Q L _ P e r i o d o ,  
     ' O R D E R   B Y   D a t a ,   v m i . M o v i m C o d ,   v m i . G r a G r u X   ; ' ,  
     ' ' ] ) ;  
     R e s u l t   : =  
         n o t   S P E D _ E F D _ P F . E r r G r a n d e z a ( ' _ S P E D _ E F D _ K 2 X X _ G A R ' ,   ' S r c G G X ' ,   ' D s t G G X ' ) ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . G r a n d e z a s I n c o n s i s t e n t e s :   B o o l e a n ;  
 v a r  
     S Q L _ P e r i o d o V M I :   S t r i n g ;  
 b e g i n  
     R e s u l t   : =   F a l s e ;  
     S Q L _ P e r i o d o V M I   : =   d m k P F . S Q L _ P e r i o d o ( ' W H E R E   v m i . D a t a H o r a   ' ,   F D i a I n i ,   F D i a F i m ,   T r u e ,   T r u e ) ;  
     / /  
     i f   D f M o d A p p G r a G 1 . G r a n d e s a s I n c o n s i s t e n t e s ( [  
     ' S E L E C T   D I S T I N C T   g g x . G r a G r u 1   N i v e l 1 ,   g g x . C o n t r o l e   R e d u z i d o ,     ' ,  
     ' g g 1 . N o m e   N O _ G G 1 ,   m e d . S i g l a ,   m e d . G r a n d e z a ,   ' ,  
     ' x c o . G r a n d e z a   x c o _ G r a n d e z a ,     ' ,  
     ' C A S T ( I F ( x c o . G r a n d e z a   >   0 ,   x c o . G r a n d e z a ,     ' ,  
     '     I F ( ( g g x . G r a G r u Y < 2 0 4 8   O R     ' ,  
     '     x c o . C o u N i v 2 < > 1 ) ,   2 ,     ' ,  
     '     I F ( x c o . C o u N i v 2 = 1 ,   1 ,   - 1 ) ) )   A S   D E C I M A L ( 1 1 , 0 ) )   d i f _ G r a n d e z a   ' ,  
     '   ' ,  
     ' F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   v m i   ' ,  
     ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v m i . G r a G r u X   ' ,  
     ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1   ' ,  
     ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e     ' ,  
     ' L E F T   J O I N   c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1     ' ,  
     ' L E F T   J O I N   c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2     ' ,  
     ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d   ' ,  
     S Q L _ P e r i o d o V M I ,  
     ' A N D   v m i . G r a G r u X   < >   0   ' ,  
     ' A N D   ( ( I F ( x c o . G r a n d e z a   >   0 ,   x c o . G r a n d e z a ,     ' ,  
     '     I F ( ( g g x . G r a G r u Y < 2 0 4 8   O R     ' ,  
     '     x c o . C o u N i v 2 < > 1 ) ,   2 ,     ' ,  
     '     I F ( x c o . C o u N i v 2 = 1 ,   1 ,   - 1 ) ) )   < >   m e d . G r a n d e z a ) )   ' ,  
     ' ' ] ,   D m o d . M y D B )   t h e n  
     b e g i n  
         R e s u l t   : =   T r u e ;  
         C l o s e ;  
     e n d ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I m p o r t a G e r A r t C o n j u n t o (  
     S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e :   S t r i n g ;  
     O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
 c o n s t  
     M o v i m I D   =   T E s t q M o v i m I D . e m i d I n d s V S ;  
     E S T S T a b S o r c   =   e s t s V M I ;  
     s P r o c N a m e   =   ' F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I m p o r t a G e r A r t ( ) ' ;  
     / /  
     J m p M o v I D     =   0 ;  
     J m p M o v C o d   =   0 ;  
     J m p N i v e l 1   =   0 ;  
     J m p N i v e l 2   =   0 ;  
     P a i M o v I D     =   0 ;  
     M o v C o d P a i   =   0 ;  
     P a i N i v e l 1   =   0 ;  
     P a i N i v e l 2   =   0 ;  
     / /  
     f u n c t i o n   I n s e r e R e g i s t r o s ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   B o o l e a n ;  
     v a r  
         M o v i m C o d ,   M o v i m T w n ,   C o d i g o ,   G r a G r u X ,   T i p o E s t q ,   M y F a t o r C a b ,   M y F A t o r I t s ,  
         I D S e q 1 ,   I D _ I t e m ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   I D S e q 1 _ K 2 9 0 ,  
         I D S e q 1 _ K 3 0 0 ,   C o d D o c O P :   I n t e g e r ;  
         D t H r A b e r t o ,   D t H r F i m O p e :   T D a t e T i m e ;  
         A r e a M 2 ,   P e s o K g ,   P e c a s ,   Q t d e :   D o u b l e ;  
         C O D _ I N S _ S U B S T :   S t r i n g ;  
         / /  
         D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;  
         O r i E S T S T a b S o r c :   T E s t q S P E D T a b S o r c ;  
     b e g i n  
         C O D _ I N S _ S U B S T   : =   ' ' ;  
         M y F a t o r C a b   : =   1 ;  
         M y F a t o r I t s   : =   - 1 ;  
         / /  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
 / / /     C a b e c a l h o   >   R e g i s t r o   K 2 3 0   o u   K 2 5 0  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         R e o p e n V m i G A r C a b ( T i p o R e g S P E D P r o d ,   S Q L _ P e r i o d o ) ;  
         P B 2 . P o s i t i o n   : =   0 ;  
         P B 2 . M a x   : =   Q r V m i G A r C a b . R e c o r d C o u n t ;  
         Q r V m i G A r C a b . F i r s t ;  
         w h i l e   n o t   Q r V m i G A r C a b . E o f   d o  
         b e g i n  
             M y O b j e c t s . U p d P B ( P B 2 ,   L a A v i s o 3 ,   L a A v i s o 4 ) ;  
             / /  
             I D S e q 1 _ K 2 9 0   : =   - 1 ;  
             I D S e q 1 _ K 3 0 0   : =   - 1 ;  
             C o d D o c O P         : =   Q r V m i G A r C a b M o v i m C o d . V a l u e ;  
             / /  
             C o d i g o             : =   Q r V m i G A r C a b C o d i g o . V a l u e ;  
             M o v i m C o d         : =   Q r V m i G A r C a b M o v i m C o d . V a l u e ;  
             M o v i m T w n         : =   Q r V m i G A r C a b M o v i m T w n . V a l u e ;  
             D t H r A b e r t o     : =   Q r V m i G A r C a b D a t a . V a l u e ;  
             D t H r F i m O p e     : =   Q r V m i G A r C a b D a t a . V a l u e ;  
             D t M o v i m           : =   Q r V m i G A r C a b D a t a . V a l u e ;  
             D t C o r r A p o       : =   Q r V m i G A r C a b D t C o r r A p o . V a l u e ;  
             G r a G r u X           : =   Q r V m i G A r C a b G r a G r u X . V a l u e ;  
             T i p o E s t q         : =   T r u n c ( Q r V m i G A r C a b T i p o E s t q . V a l u e ) ;  
             C l i e n t M O         : =   Q r V m i G A r C a b C l i e n t M O . V a l u e ;  
             F o r n e c M O         : =   Q r V m i G A r C a b F o r n e c M O . V a l u e ;  
             E n t i S i t i o       : =   Q r V m i G A r C a b E n t i S i t i o . V a l u e ;  
             C o n t r o l e         : =   Q r V m i G A r C a b C o n t r o l e . V a l u e ;  
             / /  
 ( *  
             U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r S u m G A R ,   D m o d . M y D B ,   [  
             ' S E L E C T   S U M ( P e c a s )   P e c a s ,   S U M ( P e s o K g )   P e s o K g ,     ' ,  
             ' S U M ( A r e a M 2 )   A r e a M 2   ' ,  
             ' F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   ' ,  
             ' W H E R E   M o v i m C o d = '   +   G e r a l . F F 0 ( M o v i m C o d ) ,  
             ' A N D   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( e m i n D e s t C u r t i V S ) ) ,  
             ' A N D   G r a G r u X = '   +   G e r a l . F F 0 ( G r a G r u X ) ,  
             ' ' ] ) ;  
             A r e a M 2           : =   Q r S u m G A R A r e a M 2 . V a l u e ;  
             P e s o K g           : =   Q r S u m G A R P e s o K g . V a l u e ;  
             P e c a s             : =   Q r S u m G A R P e c a s . V a l u e ;  
 * )  
             A r e a M 2           : =   Q r V m i G A r C a b A r e a M 2 . V a l u e ;  
             P e s o K g           : =   Q r V m i G A r C a b P e s o K g . V a l u e ;  
             P e c a s             : =   Q r V m i G A r C a b P e c a s . V a l u e ;  
             / /  
             i f   ( ( P e c a s   < >   0 )   o r   ( T i p o E s t q   =   2 ( * k g * ) )  
             o r   ( M o v i m I D = T E s t q M o v i m I D . e m i d E m P r o c S P ) )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
             b e g i n  
                 c a s e   T i p o E s t q   o f  
                     1 :   Q t d e   : =   A r e a M 2 ;  
                     2 :   Q t d e   : =   P e s o K g ;  
                     e l s e  
                     b e g i n  
                         G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                         +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ T i p o E s t q ] ) ;  
                         / /  
                         Q t d e   : =   P e c a s ;  
                     e n d ;  
                 e n d ;  
                 Q t d e   : =   Q t d e   *   M y F a t o r C a b ;  
             e n d   e l s e  
                 Q t d e   : =   0 ;  
             / /  
             i f   ( * T e m T w n   a n d * )   ( Q t d e   =   0 )   t h e n  
                 V e r i f i c a T i p o E s t q Q t d e ( T i p o E s t q ,   A r e a M 2 ,   P e s o K g ,   P e c a s ,  
                 G r a G r u X ,   M o v i m C o d ,   s P r o c N a m e ) ;  
             / /  
             i f   Q t d e   <   0   t h e n  
                 M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                 ' Q u a n t i d a d e   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 2 ) '   +   s L i n e B r e a k   +  
                 ' Q t d e :   '   +   G e r a l . F F T ( Q t d e ,   3 ,   s i N e g a t i v o ) ,   Q r V m i G A r C a b )  
             e l s e   i f   ( Q t d e   =   0 )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
             b e g i n  
                 c a s e   M o v i m I D   o f  
                     e m i d I n d s V S :   D t H r F i m O p e   : =   0 ;  
                     e l s e   M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                     ' Q u a n t i d a d e   z e r a d a   p a r a   O P   e n c e r r a d a ! ! !   ( 2 ) ' ,   Q r V m i G A r C a b ) ;  
                 e n d ;  
             e n d ;  
             / /  
             c a s e   T i p o R e g S P E D P r o d   o f  
                 t r s p 2 9 X :  
                 b e g i n  
                     i f   I D S e q 1 _ K 2 9 0   =   - 1   t h e n  
                     b e g i n  
                         S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 9 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                         F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o d D o c O P ,   ( * C o n t r o l e , * )  
                         ( * D a t a I n i * ) D t H r A b e r t o ,   ( * D a t a F i m * ) D t H r F i m O p e ,   ( * D t M o v i m , * )  
                         D t C o r r A p o ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   O r i g e m O p e P r o c ,   F D i a F i m ,  
                         F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   I D S e q 1 _ K 2 9 0 ) ;  
                     e n d ;  
                     / /   K 2 9 1   -   I t e n s   P r o d u z i d o s  
                     O r i E S T S T a b S o r c   : =   e s t s V M I ;  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 9 1 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 _ K 2 9 0 ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,  
                     P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,   D t M o v i m ,   D t C o r r A p o ,  
                     G r a G r u X ,   Q t d e ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,  
                     C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   O r i g e m O p e P r o c ,   O r i E S T S T a b S o r c ,  
                     F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                 e n d ;  
                 t r s p 3 0 X :  
                 b e g i n  
                     / /   K 2 3 0   -   C a b e � a l h o   p r o d u � � o   e m   t e r c e i r o s  
                     i f   I D S e q 1 _ K 3 0 0   =   - 1   t h e n  
                     b e g i n  
                         S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 3 0 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                         F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o d D o c O P ,   ( * C o n t r o l e , * )  
                         ( * D a t a I n i * ) D t H r A b e r t o ,   ( * D a t a F i m * ) D t H r F i m O p e ,   D t M o v i m ,  
                         D t C o r r A p o ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   O r i g e m O p e P r o c ,   F D i a F i m ,  
                         F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   I D S e q 1 _ K 3 0 0 ) ;  
                     e n d ;  
                     / /   K 3 0 1   -   I t e n s   P r o d u z i d o s  
                     O r i E S T S T a b S o r c   : =   e s t s V M I ;  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 3 0 1 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 _ K 3 0 0 ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,  
                     P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,   D t M o v i m ,   D t C o r r A p o ,  
                     G r a G r u X ,   Q t d e ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,  
                     C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   O r i g e m O p e P r o c ,   O r i E S T S T a b S o r c ,  
                     F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                 e n d ;  
                 e l s e  
                 G e r a l . M B _ E r r o ( ' T i p o   d e   r e g i s t r o   S P E D   E F D   ( '   +   G e r a l . F F 0 ( I n t e g e r (  
                 T i p o R e g S P E D P r o d ) )   +   ' )   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e   +   ' ( 2 ) ' ) ;  
             e n d ;  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
 / / /     I t e m   >   R e g i s t r o   K 2 3 5   o u   K 2 5 5  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
             / /  
             R e o p e n V m i G A r I t s ( M o v i m C o d ,   M o v i m T w n ,   S Q L _ P e r i o d o ,   T i p o R e g S P E D P r o d ) ;  
             Q r V m i G A r I t s . F i r s t ;  
             w h i l e   n o t   Q r V m i G A r I t s . E o f   d o  
             b e g i n  
                 i f   T E s t q M o v i m I D ( Q r V m i G A r I t s S r c M o v I D . V a l u e )   =   e m i d E m P r o c C u r   t h e n  
                 b e g i n  
                     / / G r a G r u X   : =   D e f i n e G G X C a l T o C u r ( ' J m p G G X ' ,   Q r V m i G A r I t s S r c N i v e l 2 . V a l u e ) ;  
                     G r a G r u X   : =   Q r V m i G A r I t s J m p G G X . V a l u e ;  
                 e n d  
                 e l s e  
                     G r a G r u X   : =   Q r V m i G A r I t s G r a G r u X . V a l u e ;  
                 / /  
                 C o d i g o           : =   Q r V m i G A r I t s C o d i g o . V a l u e ;  
                 M o v i m C o d       : =   Q r V m i G A r I t s M o v i m C o d . V a l u e ;  
                 M o v i m T w n       : =   Q r V m i G A r I t s M o v i m T w n . V a l u e ;  
                 D t H r A b e r t o   : =   Q r V m i G A r I t s D a t a . V a l u e ;  
                 D t H r F i m O p e   : =   Q r V m i G A r I t s D a t a . V a l u e ;  
                 T i p o E s t q       : =   Q r V m i G A r I t s T i p o E s t q . V a l u e ;  
                 C o n t r o l e       : =   Q r V m i G A r I t s C o n t r o l e . V a l u e ;  
                 I D _ I t e m         : =   C o n t r o l e ;  
                 / /  
                 A r e a M 2           : =   Q r V m i G A r I t s A r e a M 2 . V a l u e ;  
                 P e s o K g           : =   Q r V m i G A r I t s P e s o K g . V a l u e ;  
                 P e c a s             : =   Q r V m i G A r I t s P e c a s . V a l u e ;  
                 / /  
                 D t C o r r A p o     : =   Q r V m i G A r I t s D t C o r r A p o . V a l u e ;  
                 / /  
                 i f   ( ( P e c a s   < >   0 )   o r   ( T i p o E s t q   =   2 ( * k g * ) )  
                 o r   ( M o v i m I D = T E s t q M o v i m I D . e m i d E m P r o c S P ) )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
                 b e g i n  
                     c a s e   T i p o E s t q   o f  
                         1 :   Q t d e   : =   A r e a M 2 ;  
                         2 :   Q t d e   : =   P e s o K g ;  
                         e l s e  
                         b e g i n  
                             G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                             +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ T i p o E s t q ] ) ;  
                             / /  
                             Q t d e   : =   P e c a s ;  
                         e n d ;  
                     e n d ;  
                     Q t d e   : =   Q t d e   *   M y F a t o r I t s ;  
                 e n d   e l s e  
                     Q t d e   : =   0 ;  
                 i f   ( * T e m T w n   a n d * )   ( Q t d e   =   0 )   t h e n  
                     V e r i f i c a T i p o E s t q Q t d e ( T i p o E s t q ,   A r e a M 2 ,   P e s o K g ,   P e c a s ,  
                     G r a G r u X ,   M o v i m C o d ,   s P r o c N a m e ) ;  
                 / /  
                 i f   Q t d e   <   0   t h e n  
                     M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                     ' Q u a n t i d a d e   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 3 ) '   +   s L i n e B r e a k   +  
                     ' Q t d e :   '   +   G e r a l . F F T ( Q t d e ,   3 ,   s i N e g a t i v o ) ,   Q r V m i G A r I t s )  
                 e l s e   i f   ( Q t d e   =   0 )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
                 b e g i n  
                     c a s e   M o v i m I D   o f  
                         e m i d I n d s V S :   D t H r F i m O p e   : =   0 ;  
                         e l s e   M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                         ' Q u a n t i d a d e   z e r a d a   p a r a   O P   e n c e r r a d a ! ! !   ( 3 ) ' ,   Q r V m i G A r I t s ) ;  
                     e n d ;  
                 e n d ;  
                 / /  
                 c a s e   T i p o R e g S P E D P r o d   o f  
                     / / t r s p 2 1 X :  
                     t r s p 2 9 X :  
                     b e g i n  
                         / /   K 2 9 2   -   I t e n s   c o n s u m i d o s  
                         O r i E S T S T a b S o r c   : =   e s t s V M I ;  
                         S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 9 2 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                         F P e r i A p u ,   I D S e q 1 _ K 2 9 0 ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,  
                         P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,   D t M o v i m ,   D t C o r r A p o ,  
                         G r a G r u X ,   Q t d e ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,  
                         C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   O r i g e m O p e P r o c ,   O r i E S T S T a b S o r c ,  
                         F T i p o P e r i o d o F i s c a l ,   T E s t q S P E D T a b S o r c . e s t s V M I ,   M e A v i s o ) ;  
                     e n d ;  
                     t r s p 3 0 X :  
                     b e g i n  
                         / /   K 3 0 2   -   I t e n s   C o n s u m i d o s  
                         O r i E S T S T a b S o r c   : =   e s t s V M I ;  
                         S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 3 0 2 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                         F P e r i A p u ,   I D S e q 1 _ K 3 0 0 ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,  
                         P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,   D t M o v i m ,   D t C o r r A p o ,  
                         G r a G r u X ,   Q t d e ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,  
                         C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   O r i g e m O p e P r o c ,   O r i E S T S T a b S o r c ,  
                         F T i p o P e r i o d o F i s c a l ,   T E s t q S P E D T a b S o r c . e s t s V M I ,   M e A v i s o ) ;  
                     e n d ;  
                     e l s e  
                         G e r a l . M B _ E r r o ( ' T i p o   d e   r e g i s t r o   S P E D   E F D   ( '   +   G e r a l . F F 0 ( I n t e g e r (  
                         T i p o R e g S P E D P r o d ) )   +   ' )   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e   +   ' ( 3 ) ' ) ;  
                 e n d ;  
                 / /  
                 Q r V m i G A r I t s . N e x t ;  
             e n d ;  
             / /  
             Q r V m i G A r C a b . N e x t ;  
         e n d ;  
     e n d ;  
 b e g i n  
     i f   n o t   G e r a G A R ( S Q L _ P e r i o d o )   t h e n  
         E x i t ;  
     / /  
     I n s e r e R e g i s t r o s ( t r s p 2 9 X ) ;  
     I n s e r e R e g i s t r o s ( t r s p 3 0 X ) ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I m p o r t a G e r A r t I s o l a d o ( S Q L _ P e r i o d o C o m p l e x o ,  
     S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e :   S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
 c o n s t  
     M o v i m I D   =   T E s t q M o v i m I D . e m i d I n d s V S ;  
     E S T S T a b S o r c   =   e s t s V M I ;  
     s P r o c N a m e   =   ' F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I m p o r t a G e r A r t ( ) ' ;  
     / /  
     f u n c t i o n   F i l t r o R e g ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
     b e g i n  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 3 X :   R e s u l t   : =   ' = ' ;  
             t r s p 2 5 X :   R e s u l t   : =   ' < > ' ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ E r r o ( ' " F i l t r o R e g "   i n d e f i n i d o :   '   +  
                     G e r a l . F F 0 ( I n t e g e r ( T i p o R e g S P E D P r o d ) )   +   ' !   ( 1 ) ' ) ;  
                 R e s u l t   : =   ' # E R R ' ;  
             e n d ;  
         e n d ;  
     e n d ;  
     f u n c t i o n   F i l t r o P e r i o d o ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
     b e g i n  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 3 X :   R e s u l t   : =   S Q L _ P e r i o d o ;  
             / / t r s p 2 5 X :   R e s u l t   : =   S Q L _ D t H r F i m O p e ;  
             / / t r s p 2 5 X :   R e s u l t   : =   S Q L _ P e r i o d o ;  
             t r s p 2 5 X :   R e s u l t   : =   ' ' ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ E r r o ( ' " F i l t r o R e g "   i n d e f i n i d o ! ' ) ;  
                 R e s u l t   : =   ' # E R R _ P E R I O D O _ ' ;  
             e n d ;  
         e n d ;  
     e n d ;  
     / /  
     f u n c t i o n   I n s e r e R e g i s t r o s ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   B o o l e a n ;  
     v a r  
         M o v i m C o d ,   M o v i m T w n ,   C o d i g o ,   G r a G r u X ,   T i p o E s t q ,   M y F a t o r C a b ,   M y F A t o r I t s ,  
         I D S e q 1 ,   I D _ I t e m ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o :   I n t e g e r ;  
         D t H r A b e r t o ,   D t H r F i m O p e :   T D a t e T i m e ;  
         A r e a M 2 ,   P e s o K g ,   P e c a s ,   Q t d e :   D o u b l e ;  
         C O D _ I N S _ S U B S T :   S t r i n g ;  
         / /  
         D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;  
     b e g i n  
         C O D _ I N S _ S U B S T   : =   ' ' ;  
         M y F a t o r C a b   : =   1 ;  
         M y F a t o r I t s   : =   - 1 ;  
         / /  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
 / / /     C a b e c a l h o   >   R e g i s t r o   K 2 3 0   o u   K 2 5 0  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         R e o p e n V m i G A r C a b ( T i p o R e g S P E D P r o d ,   S Q L _ P e r i o d o ) ;  
         P B 2 . P o s i t i o n   : =   0 ;  
         P B 2 . M a x   : =   Q r V m i G A r C a b . R e c o r d C o u n t ;  
         Q r V m i G A r C a b . F i r s t ;  
         w h i l e   n o t   Q r V m i G A r C a b . E o f   d o  
         b e g i n  
             M y O b j e c t s . U p d P B ( P B 2 ,   L a A v i s o 3 ,   L a A v i s o 4 ) ;  
             / /  
             C o d i g o           : =   Q r V m i G A r C a b C o d i g o . V a l u e ;  
             M o v i m C o d       : =   Q r V m i G A r C a b M o v i m C o d . V a l u e ;  
             M o v i m T w n       : =   Q r V m i G A r C a b M o v i m T w n . V a l u e ;  
             D t H r A b e r t o   : =   Q r V m i G A r C a b D a t a . V a l u e ;  
             D t H r F i m O p e   : =   Q r V m i G A r C a b D a t a . V a l u e ;  
             D t M o v i m         : =   Q r V m i G A r C a b D a t a . V a l u e ;  
             D t C o r r A p o     : =   Q r V m i G A r C a b D t C o r r A p o . V a l u e ;  
             G r a G r u X         : =   Q r V m i G A r C a b G r a G r u X . V a l u e ;  
             T i p o E s t q       : =   T r u n c ( Q r V m i G A r C a b T i p o E s t q . V a l u e ) ;  
             C l i e n t M O       : =   Q r V m i G A r C a b C l i e n t M O . V a l u e ;  
             F o r n e c M O       : =   Q r V m i G A r C a b F o r n e c M O . V a l u e ;  
             E n t i S i t i o     : =   Q r V m i G A r C a b E n t i S i t i o . V a l u e ;  
             C o n t r o l e       : =   Q r V m i G A r C a b C o n t r o l e . V a l u e ;  
             / /  
 ( *  
             U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r S u m G A R ,   D m o d . M y D B ,   [  
             ' S E L E C T   S U M ( P e c a s )   P e c a s ,   S U M ( P e s o K g )   P e s o K g ,     ' ,  
             ' S U M ( A r e a M 2 )   A r e a M 2   ' ,  
             ' F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   ' ,  
             ' W H E R E   M o v i m C o d = '   +   G e r a l . F F 0 ( M o v i m C o d ) ,  
             ' A N D   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( e m i n D e s t C u r t i V S ) ) ,  
             ' A N D   G r a G r u X = '   +   G e r a l . F F 0 ( G r a G r u X ) ,  
             ' ' ] ) ;  
             A r e a M 2           : =   Q r S u m G A R A r e a M 2 . V a l u e ;  
             P e s o K g           : =   Q r S u m G A R P e s o K g . V a l u e ;  
             P e c a s             : =   Q r S u m G A R P e c a s . V a l u e ;  
 * )  
             A r e a M 2           : =   Q r V m i G A r C a b A r e a M 2 . V a l u e ;  
             P e s o K g           : =   Q r V m i G A r C a b P e s o K g . V a l u e ;  
             P e c a s             : =   Q r V m i G A r C a b P e c a s . V a l u e ;  
             / /  
             i f   ( ( P e c a s   < >   0 )   o r   ( T i p o E s t q   =   2 ( * k g * ) )  
             o r   ( M o v i m I D = T E s t q M o v i m I D . e m i d E m P r o c S P ) )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
             b e g i n  
                 c a s e   T i p o E s t q   o f  
                     1 :   Q t d e   : =   A r e a M 2 ;  
                     2 :   Q t d e   : =   P e s o K g ;  
                     e l s e  
                     b e g i n  
                         G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                         +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ T i p o E s t q ] ) ;  
                         / /  
                         Q t d e   : =   P e c a s ;  
                     e n d ;  
                 e n d ;  
                 Q t d e   : =   Q t d e   *   M y F a t o r C a b ;  
             e n d   e l s e  
                 Q t d e   : =   0 ;  
             i f   ( * T e m T w n   a n d * )   ( Q t d e   =   0 )   t h e n  
                 V e r i f i c a T i p o E s t q Q t d e ( T i p o E s t q ,   A r e a M 2 ,   P e s o K g ,   P e c a s ,  
                 G r a G r u X ,   M o v i m C o d ,   s P r o c N a m e ) ;  
             / /  
             i f   Q t d e   <   0   t h e n  
                 M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                 ' Q u a n t i d a d e   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 2 ) '   +   s L i n e B r e a k   +  
                 ' Q t d e :   '   +   G e r a l . F F T ( Q t d e ,   3 ,   s i N e g a t i v o ) ,   Q r V m i G A r C a b )  
             e l s e   i f   ( Q t d e   =   0 )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
             b e g i n  
                 c a s e   M o v i m I D   o f  
                     e m i d I n d s V S :   D t H r F i m O p e   : =   0 ;  
                     e l s e   M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                     ' Q u a n t i d a d e   z e r a d a   p a r a   O P   e n c e r r a d a ! ! !   ( 2 ) ' ,   Q r V m i G A r C a b ) ;  
                 e n d ;  
             e n d ;  
             / /  
             c a s e   T i p o R e g S P E D P r o d   o f  
                 t r s p 2 3 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                     C o n t r o l e ,  
                     D t H r A b e r t o ,   D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o ,   G r a G r u X ,   C l i e n t M O ,  
                     F o r n e c M O ,   E n t i S i t i o ,   Q t d e ,   O r i g e m O p e P r o c ,  
                     F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   I D S e q 1 ) ;  
                 e n d ;  
                 t r s p 2 5 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                     C o n t r o l e ,  
                     D t H r F i m O p e ,  
                     D t M o v i m ,   D t C o r r A p o ,   G r a G r u X ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,  
                     Q t d e ,   O r i g e m O p e P r o c ,   F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   I D S e q 1 ) ;  
                 e n d ;  
                 e l s e  
                 G e r a l . M B _ E r r o ( ' T i p o   d e   r e g i s t r o   S P E D   E F D   ( '   +   G e r a l . F F 0 ( I n t e g e r (  
                 T i p o R e g S P E D P r o d ) )   +   ' )   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e   +   ' ( 2 ) ' ) ;  
             e n d ;  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
 / / /     I t e m   >   R e g i s t r o   K 2 3 5   o u   K 2 5 5  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
             / /  
             R e o p e n V m i G A r I t s ( M o v i m C o d ,   M o v i m T w n ,   S Q L _ P e r i o d o ,   T i p o R e g S P E D P r o d ) ;  
             Q r V m i G A r I t s . F i r s t ;  
             w h i l e   n o t   Q r V m i G A r I t s . E o f   d o  
             b e g i n  
                 i f   T E s t q M o v i m I D ( Q r V m i G A r I t s S r c M o v I D . V a l u e )   =   e m i d E m P r o c C u r   t h e n  
                 b e g i n  
                     / / G r a G r u X   : =   D e f i n e G G X C a l T o C u r ( ' J m p G G X ' ,   Q r V m i G A r I t s S r c N i v e l 2 . V a l u e ) ;  
                     G r a G r u X   : =   Q r V m i G A r I t s J m p G G X . V a l u e ;  
                 e n d  
                 e l s e  
                     G r a G r u X   : =   Q r V m i G A r I t s G r a G r u X . V a l u e ;  
                 / /  
                 C o d i g o           : =   Q r V m i G A r I t s C o d i g o . V a l u e ;  
                 M o v i m C o d       : =   Q r V m i G A r I t s M o v i m C o d . V a l u e ;  
                 M o v i m T w n       : =   Q r V m i G A r I t s M o v i m T w n . V a l u e ;  
                 D t H r A b e r t o   : =   Q r V m i G A r I t s D a t a . V a l u e ;  
                 D t H r F i m O p e   : =   Q r V m i G A r I t s D a t a . V a l u e ;  
                 T i p o E s t q       : =   Q r V m i G A r I t s T i p o E s t q . V a l u e ;  
                 C o n t r o l e       : =   Q r V m i G A r I t s C o n t r o l e . V a l u e ;  
                 I D _ I t e m         : =   C o n t r o l e ;  
                 / /  
                 A r e a M 2           : =   Q r V m i G A r I t s A r e a M 2 . V a l u e ;  
                 P e s o K g           : =   Q r V m i G A r I t s P e s o K g . V a l u e ;  
                 P e c a s             : =   Q r V m i G A r I t s P e c a s . V a l u e ;  
                 / /  
                 D t C o r r A p o     : =   Q r V m i G A r I t s D t C o r r A p o . V a l u e ;  
                 / /  
                 i f   ( ( P e c a s   < >   0 )   o r   ( T i p o E s t q   =   2 ( * k g * ) )  
                 o r   ( M o v i m I D = T E s t q M o v i m I D . e m i d E m P r o c S P ) )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
                 b e g i n  
                     c a s e   T i p o E s t q   o f  
                         1 :   Q t d e   : =   A r e a M 2 ;  
                         2 :   Q t d e   : =   P e s o K g ;  
                         e l s e  
                         b e g i n  
                             G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                             +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ T i p o E s t q ] ) ;  
                             / /  
                             Q t d e   : =   P e c a s ;  
                         e n d ;  
                     e n d ;  
                     Q t d e   : =   Q t d e   *   M y F a t o r I t s ;  
                 e n d   e l s e  
                     Q t d e   : =   0 ;  
                 i f   ( * T e m T w n   a n d * )   ( Q t d e   =   0 )   t h e n  
                     V e r i f i c a T i p o E s t q Q t d e ( T i p o E s t q ,   A r e a M 2 ,   P e s o K g ,   P e c a s ,  
                     G r a G r u X ,   M o v i m C o d ,   s P r o c N a m e ) ;  
                 / /  
                 i f   Q t d e   <   0   t h e n  
                     M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                     ' Q u a n t i d a d e   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 3 ) '   +   s L i n e B r e a k   +  
                     ' Q t d e :   '   +   G e r a l . F F T ( Q t d e ,   3 ,   s i N e g a t i v o ) ,   Q r V m i G A r I t s )  
                 e l s e   i f   ( Q t d e   =   0 )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
                 b e g i n  
                     c a s e   M o v i m I D   o f  
                         e m i d I n d s V S :   D t H r F i m O p e   : =   0 ;  
                         e l s e   M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                         ' Q u a n t i d a d e   z e r a d a   p a r a   O P   e n c e r r a d a ! ! !   ( 3 ) ' ,   Q r V m i G A r I t s ) ;  
                     e n d ;  
                 e n d ;  
                 / /  
                 c a s e   T i p o R e g S P E D P r o d   o f  
                     / / t r s p 2 1 X :  
                     t r s p 2 3 X :  
                     b e g i n  
                         S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                         F P e r i A p u ,   I D S e q 1 ,   D t H r A b e r t o ,   D t C o r r A p o ,   G r a G r u X ,  
                         Q t d e ,   C O D _ I N S _ S U B S T ,   I D _ I t e m ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                         C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   O r i g e m O p e P r o c ,  
                         E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                     e n d ;  
                     t r s p 2 5 X :  
                     b e g i n  
                         S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                         F P e r i A p u ,   I D S e q 1 ,   D t H r A b e r t o ,   D t C o r r A p o ,   G r a G r u X ,  
                         Q t d e ,   C O D _ I N S _ S U B S T ,   I D _ I t e m ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                         C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   O r i g e m O p e P r o c ,  
                         E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                     e n d ;  
                     e l s e  
                         G e r a l . M B _ E r r o ( ' T i p o   d e   r e g i s t r o   S P E D   E F D   ( '   +   G e r a l . F F 0 ( I n t e g e r (  
                         T i p o R e g S P E D P r o d ) )   +   ' )   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e   +   ' ( 3 ) ' ) ;  
                 e n d ;  
                 / /  
                 Q r V m i G A r I t s . N e x t ;  
             e n d ;  
             / /  
             Q r V m i G A r C a b . N e x t ;  
         e n d ;  
     e n d ;  
 b e g i n  
     i f   n o t   G e r a G A R ( S Q L _ P e r i o d o )   t h e n  
         E x i t ;  
     / /  
     I n s e r e R e g i s t r o s ( t r s p 2 3 X ) ;  
     I n s e r e R e g i s t r o s ( t r s p 2 5 X ) ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I m p o r t a O p e P r o c A ( Q r C a b :   T M y S Q L Q u e r y ;  
     M o v i m I D :   T E s t q M o v i m I D ;   S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,  
     S Q L _ D t H r F i m O p e :   S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
 v a r  
     T a b S r c ,   S Q L _ F L D ,   S Q L _ A N D :   S t r i n g ;  
     M o v N i v I n n ,   M o v N i v B x a :   T E s t q M o v i m N i v ;  
     / /  
     f u n c t i o n   F i l t r o R e g ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
     b e g i n  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 3 X :   R e s u l t   : =   ' = ' ;  
             t r s p 2 5 X :   R e s u l t   : =   ' < > ' ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ E r r o ( ' " F i l t r o R e g "   i n d e f i n i d o :   '   +  
                     G e r a l . F F 0 ( I n t e g e r ( T i p o R e g S P E D P r o d ) )   +   ' !   ( 2 ) ' ) ;  
                 R e s u l t   : =   ' # E R R ' ;  
             e n d ;  
         e n d ;  
     e n d ;  
     / /  
     f u n c t i o n   F i l t r o C o n d ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
     b e g i n  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 3 X :   R e s u l t   : =   '   O R   ' ;  
             t r s p 2 5 X :   R e s u l t   : =   '   A N D   ' ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ E r r o ( ' " F i l t r o C o n d "   i n d e f i n i d o :   '   +  
                     G e r a l . F F 0 ( I n t e g e r ( T i p o R e g S P E D P r o d ) )   +   ' !   ( 2 a ) ' ) ;  
                 R e s u l t   : =   ' # E R R ' ;  
             e n d ;  
         e n d ;  
     e n d ;  
     / /  
     f u n c t i o n   R e a b r e E m P r o c e s s o 2 1 X ( Q u e r y :   T m y S Q L Q u e r y ) :   B o o l e a n ;  
     b e g i n  
         R e s u l t   : =   F a l s e ;  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q u e r y ,   D m o d . M y D B ,   [  
         S Q L _ F L D ,  
         ' F R O M   '   +   T a b S r c   +     '   v s x ' ,  
         ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v s x . G G X D s t   ' ,  
         ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
         ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
         ' L E F T   J O I N   c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1   ' ,  
         ' L E F T   J O I N   c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2   ' ,  
         ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
         S Q L _ P e r i o d o C o m p l e x o ,  
         ' O R   M o v i m C o d   I N   (   ' ,  
         '     S E L E C T   M o v i m C o d   ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   ' ,  
         '     W H E R E   M o v i m I D = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) ) ,  
         '     '   +   S Q L _ P e r i o d o V S ,  
         ' ) )   ' ,  
         S Q L _ A N D ,  
  
         '   ' ,  
         ' A N D   M o v i m C o d   I N   (     ' ,  
         '     S E L E C T   M o v i m C o d     ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '     ' ,  
         '     W H E R E   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v N i v I n n ) ) ,  
         '     A N D   ( E m p r e s a   =   '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         / / '     A N D   F o r n e c M O '   +   F i l t r o R e g ( t r s p 2 1 X )   +   G e r a l . F F 0 ( F E m p r e s a )   +  
         '     ) ' ,  
         ' )   ' ,  
         '   ' ,  
         / / O p e r a c a o   q u e   n a o   t e m   t e m   i t e n s   d e   o r i g e m   n e m   d e s t i n o ,   o u   s e j a ,   e s t a h   v a z i a  
         ' A N D   ( P e c a s S r c   < >   0   ' ,  
         '           O R   ' ,  
         '           P e c a s D s t   < >   0   ' ,  
         '           O R   ' ,  
         '           ( P e s o K g S r c   < > 0   A N D   g g x . G r a G r u Y   <   1 0 2 4 ) )   ' ,  
         ' ' ] ) ;  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
         R e s u l t   : =   T r u e ;  
     e n d ;  
     / /  
     f u n c t i o n   R e a b r e E m P r o c e s s o 2 3 X ( Q u e r y :   T m y S Q L Q u e r y ) :   B o o l e a n ;  
     b e g i n  
         R e s u l t   : =   F a l s e ;  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q u e r y ,   D m o d . M y D B ,   [  
         S Q L _ F L D ,  
         ' F R O M   '   +   T a b S r c   +     '   v s x ' ,  
         / / ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v s x . G r a G r u X   ' ,  
         ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v s x . G G X D s t   ' ,  
         ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
         ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
         ' L E F T   J O I N   c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1   ' ,  
         ' L E F T   J O I N   c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2   ' ,  
         ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
         S Q L _ P e r i o d o C o m p l e x o ,  
         ' O R   M o v i m C o d   I N   (   ' ,  
         '     S E L E C T   M o v i m C o d   ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   ' ,  
         '     W H E R E   M o v i m I D = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) ) ,  
         '     '   +   S Q L _ P e r i o d o V S ,  
         ' ) )   ' ,  
         S Q L _ A N D ,  
  
         '   ' ,  
         ' A N D   M o v i m C o d   I N   (     ' ,  
         '     S E L E C T   M o v i m C o d     ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '     ' ,  
         '     W H E R E   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v N i v I n n ) ) ,  
         '     A N D   ( E m p r e s a   =   '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         '     A N D   ( F o r n e c M O '   +   F i l t r o R e g ( t r s p 2 3 X )   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         '     '   +   F i l t r o C o n d ( t r s p 2 3 X )   +   '   F o r n e c M O '   +   F i l t r o R e g ( t r s p 2 3 X )   +   ' 0 )   )   ' ,  
         ' )   ' ,  
         '   ' ,  
  
         / / O p e r a c a o   q u e   n a o   t e m   t e m   i t e n s   d e   o r i g e m   n e m   d e s t i n o ,   o u   s e j a ,   e s t a h   v a z i a  
         ' A N D   ( P e c a s S r c   < >   0   ' ,  
         '           O R   ' ,  
         '           P e c a s D s t   < >   0   ' ,  
         '           O R   ' ,  
         '           ( P e s o K g S r c   < > 0   A N D   g g x . G r a G r u Y   <   1 0 2 4 ) )   ' ,  
         ' ' ] ) ;  
 ( *  
         i f   Q u e r y . R e c o r d C o u n t   >   0   t h e n   G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
 * )  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
         R e s u l t   : =   T r u e ;  
     e n d ;  
     / /  
     f u n c t i o n   R e a b r e E m P r o c e s s o 2 5 X ( Q u e r y :   T m y S Q L Q u e r y ) :   B o o l e a n ;  
     b e g i n  
         R e s u l t   : =   F a l s e ;  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q u e r y ,   D m o d . M y D B ,   [  
         S Q L _ F L D ,  
         ' F R O M   '   +   T a b S r c   +     '   v s x ' ,  
         ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v s x . G G X D s t   ' ,  
         ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
         ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
         ' L E F T   J O I N   c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1   ' ,  
         ' L E F T   J O I N   c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2   ' ,  
         ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
         ' W H E R E     M o v i m C o d   I N   (     ' ,  
         '     S E L E C T   M o v i m C o d     ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '     ' ,  
         '     W H E R E   M o v i m N i v   I N   ( '   +   G e r a l . F F 0 ( I n t e g e r ( M o v N i v I n n ) )   +   ' , '   +  
         G e r a l . F F 0 ( I n t e g e r ( M o v N i v B x a ) )   +   ' )   ' ,  
         / /  
         '     A N D   ( E m p r e s a   =   '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         '     A N D   ( F o r n e c M O '   +   F i l t r o R e g ( t r s p 2 5 X )   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         '     '   +   F i l t r o C o n d ( t r s p 2 5 X )   +   '   F o r n e c M O '   +   F i l t r o R e g ( t r s p 2 5 X )   +   ' 0 )   )   ' ,  
         '   '   +   S Q L _ P e r i o d o V S ,  
         ' )   ' ,  
         '   ' ,  
         / / O p e r a c a o   q u e   n a o   t e m   t e m   i t e n s   d e   o r i g e m   n e m   d e s t i n o ,   o u   s e j a ,   e s t a h   v a z i a  
         ' A N D   ( P e c a s S r c   < >   0   ' ,  
         '           O R   ' ,  
         '           P e c a s D s t   < >   0   ' ,  
         '           O R   ' ,  
         '           ( P e s o K g S r c   < > 0   A N D   g g x . G r a G r u Y   <   1 0 2 4 ) )   ' ,  
         ' ' ] ) ;  
 ( *  
         i f   Q u e r y . R e c o r d C o u n t   >   0   t h e n   G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
 * )  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
         R e s u l t   : =   T r u e ;  
     e n d ;  
     / /  
     f u n c t i o n   R e a b r e E m P r o c e s s o 2 6 X ( Q u e r y :   T m y S Q L Q u e r y ) :   B o o l e a n ;  
     b e g i n  
         R e s u l t   : =   F a l s e ;  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q u e r y ,   D m o d . M y D B ,   [  
         S Q L _ F L D ,  
         ' F R O M   '   +   T a b S r c   +     '   v s x ' ,  
         ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v s x . G G X D s t   ' ,  
         ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
         ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
         ' L E F T   J O I N   c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1   ' ,  
         ' L E F T   J O I N   c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2   ' ,  
         ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
         S Q L _ P e r i o d o C o m p l e x o ,  
         ' O R   M o v i m C o d   I N   (   ' ,  
         '     S E L E C T   M o v i m C o d   ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '   ' ,  
         '     W H E R E   M o v i m I D = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) ) ,  
         '     '   +   S Q L _ P e r i o d o V S ,  
         ' ) )   ' ,  
         S Q L _ A N D ,  
  
         '   ' ,  
         ' A N D   M o v i m C o d   I N   (     ' ,  
         '     S E L E C T   M o v i m C o d     ' ,  
         '     F R O M   '   +   C O _ S E L _ T A B _ V M I   +   '     ' ,  
         '     W H E R E   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v N i v I n n ) ) ,  
         '     A N D   ( E m p r e s a   =   '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         '     ) ' ,  
         ' )   ' ,  
         '   ' ,  
         / / O p e r a c a o   q u e   n a o   t e m   t e m   i t e n s   d e   o r i g e m   n e m   d e s t i n o ,   o u   s e j a ,   e s t a h   v a z i a  
         ' A N D   ( P e c a s S r c   < >   0   ' ,  
         '           O R   ' ,  
         '           P e c a s D s t   < >   0   ' ,  
         '           O R   ' ,  
         '           ( P e s o K g S r c   < > 0   A N D   g g x . G r a G r u Y   <   1 0 2 4 ) )   ' ,  
         ' ' ] ) ;  
 ( *  
         i f   Q u e r y . R e c o r d C o u n t   >   0   t h e n   G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
 * )  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
         R e s u l t   : =   T r u e ;  
     e n d ;  
     / /  
     f u n c t i o n   I n s e r e R e g i s t r o s ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   B o o l e a n ;  
     v a r  
         L i n A r q P a i ,   M o v i m C o d :   I n t e g e r ;  
         F a t o r :   D o u b l e ;  
         A b r i u :   B o o l e a n ;  
     b e g i n  
         R e s u l t   : =   F a l s e ;  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 1 X :   A b r i u   : =   R e a b r e E m P r o c e s s o 2 1 X ( Q r C a b ) ;  
             t r s p 2 3 X :   A b r i u   : =   R e a b r e E m P r o c e s s o 2 3 X ( Q r C a b ) ;  
             t r s p 2 5 X :   A b r i u   : =   R e a b r e E m P r o c e s s o 2 5 X ( Q r C a b ) ;  
             t r s p 2 6 X :   A b r i u   : =   R e a b r e E m P r o c e s s o 2 6 X ( Q r C a b ) ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ A v i s o ( ' T i p o   d e   r e g i s t r o   i n d e f i n i d o   e m   " I n s e r e R e g i s t r o s ( ) " ' ) ;  
                 A b r i u   : =   F a l s e ;  
             e n d ;  
         e n d ;  
         i f   n o t   A b r i u   t h e n  
             E x i t ;  
         / /  
         P B 2 . M a x   : =   Q r C a b . R e c o r d C o u n t ;  
         P B 2 . P o s i t i o n   : =   0 ;  
         Q r C a b . F i r s t ;  
         w h i l e   n o t   Q r C a b . E o f   d o  
         b e g i n  
             M y O b j e c t s . U p d P B ( P B 2 ,   n i l ,   n i l ) ;  
             M o v i m C o d     : =   Q r C a b . F i e l d B y N a m e ( ' M o v i m C o d ' ) . A s I n t e g e r ;  
             i f   T i p o R e g S P E D P r o d   =   t r s p 2 1 X   t h e n  
             b e g i n  
                 c a s e   M o v i m I D   o f  
 ( *                   J a   t e m   p r o p r i o :   I n s e r e _ O r i g e R i b P D A _ 2 1 0 ( )  
                     e m i d E m R i b P D A :  
                     b e g i n  
                         F a t o r   : =   1 ;  
                         I n s e r e _ O r i g e R i b P D A _ 2 1 0 ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   T E s t q M o v i m N i v . e m i n D e s t P D A ,   Q r C a b ,   L i n A r q P a i ,   S Q L _ P e r i o d o V S ,   O r i g e m O p e P r o c ) ;  
                     e n d ;  
 * )  
                     e m i d E m P r o c C a l :   ;   / /   n a d a ! !  
                     e m i d E m P r o c C u r :   ;   / /   n a d a ! !  
                     e m i d E m O p e r a c a o :  
                     b e g i n  
                         F a t o r   : =   1 ;  
                         I n s e r e _ O r i g e P r o c V S _ 2 1 0 ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   T E s t q M o v i m N i v . e m i n E m O p e r I n n ,   T E s t q M o v i m N i v . e m i n E m O p e r B x a ,   Q r C a b ,   L i n A r q P a i ,   S Q L _ P e r i o d o V S ,   O r i g e m O p e P r o c ) ;  
                         I n s e r e _ E m O p e P r o c _ 2 1 5 ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   T E s t q M o v i m N i v . e m i n D e s t O p e r ,   Q r C a b ,   ( * F a t o r , * )   S Q L _ P e r i o d o V S ,   O r i g e m O p e P r o c ,   L i n A r q P a i ) ;  
                     e n d ;  
                     e m i d E m P r o c W E :   ;   / /   n a d a ! !  
                     e l s e  
                     b e g i n  
                         R e s u l t   : =   F a l s e ;  
                         G e r a l . M B _ E r r o ( ' M o v i m I D   '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) )   +   '   n � o   i m p l e m e n t a d o   e m   " I m p o r t a O p e P r o c ( ) . 2 1 x " ' ) ;  
                         E x i t ;  
                     e n d ;  
                 e n d ;  
             e n d   e l s e  
             b e g i n  
                 c a s e   M o v i m I D   o f  
                     e m i d E m P r o c C a l :  
                     b e g i n  
                         F a t o r   : =   - 1 ;  
                         I n s e r e _ E m O p e P r o c _ P r d ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   Q r C a b ,   F a t o r ,  
                             S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   T E s t q M o v i m N i v . e m i n E m C a l B x a ) ;  
                     e n d ;  
                     e m i d E m P r o c C u r :  
                     b e g i n  
                         F a t o r   : =   1 ;  
                         I n s e r e _ E m O p e P r o c _ P r d ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   Q r C a b ,   F a t o r ,  
                             S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   T E s t q M o v i m N i v . e m i n E m C u r B x a ) ;  
                     e n d ;  
                     e m i d E m P r o c W E :  
                     b e g i n  
                         F a t o r   : =   1 ;  
  
                         / / c r i a r   q u e r y   q u e   s e p a r a   p e r i o d o s   +   P e r i o d o s   d e   c o r r e c a o !  
                         I n s e r e _ E m O p e P r o c _ P r d ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   Q r C a b ,   F a t o r ,  
                             S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   T E s t q M o v i m N i v . e m i n E m W E n d B x a ) ;  
                     e n d ;  
                     e m i d E m P r o c S P :  
                     b e g i n  
                         F a t o r   : =   1 ;  
                         I n s e r e _ E m O p e P r o c _ P r d ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   Q r C a b ,   F a t o r ,  
                             S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   T E s t q M o v i m N i v . e m i n E m P S P B x a ) ;  
                     e n d ;  
                     e m i d E m R e p r R M :  
                     b e g i n  
                         F a t o r   : =   1 ;  
                         I n s e r e _ E m O p e P r o c _ P r d ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   Q r C a b ,   F a t o r ,  
                             S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   T E s t q M o v i m N i v . e m i n E m R R M B x a ) ;  
                       / /   G e r a l . M B _ S Q L ( S e l f ,   Q r C a b ) ;  
                     e n d ;  
                     e l s e  
                     b e g i n  
                         R e s u l t   : =   F a l s e ;  
                         / /   M o v i m I D   3 0   n � o   i m p l e m e n t a d o   e m   " I m p o r t a O p e P r o c ( ) . 2 x x "  
                         G e r a l . M B _ E r r o ( ' M o v i m I D   '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) )   +   '   n � o   i m p l e m e n t a d o   e m   " I m p o r t a O p e P r o c ( ) . 2 x x " ' ) ;  
                         E x i t ;  
                     e n d ;  
                 e n d ;  
             e n d ;  
             Q r C a b . N e x t ;  
         e n d ;  
         R e s u l t   : =   T r u e ;  
     e n d ;  
 b e g i n  
 / / f a z e r   n o v o   a q u i !  
     / / P B 1 . P o s i t i o n   : =   P B 1 . P o s i t i o n   +   1 ;  
     R e s u l t   : =   F a l s e ;  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' A t u a l i z a n d o   " D t H r F i m O p e "   d o   M o v i m I D :   '   +  
         G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) ) ) ;  
     V S _ P F . A t u a l i z a D t H r F i m O p e _ M o v i m I D ( M o v i m I D ) ;  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' I m p o r t a n d o   M o v i m I D :   '   +  
         G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) ) ) ;  
     T a b S r c         : =   V S _ P F . O b t e m N o m e T a b e l a V S X x x C a b ( M o v i m I D ) ;  
     / /   R e c u r t i m e n t o ! ! !  
     M o v N i v I n n   : =   V S _ P F . O b t e m M o v i m N i v I n n D e M o v i m I D ( M o v i m I D ) ;  
     M o v N i v B x a   : =   V S _ P F . O b t e m M o v i m N i v B x a D e M o v i m I D ( M o v i m I D ) ;  
     / /   ? ? ? ? ?  
     / / M o v N i v I n n   : =   V S _ P F . O b t e m M o v i m N i v B x a D e M o v i m I D ( M o v i m I D ) ;  
     i f   M o v N i v I n n   =   e m i n S e m N i v   t h e n  
         E x i t ;  
     / /  
     S Q L _ F L D   : =   G e r a l . A T S ( [  
     ' S E L E C T   D I S T I N C T   g g x . G r a G r u 1   N i v e l 1 ,   g g x . C o n t r o l e   R e d u z i d o ,   ' ,  
     ' g g 1 . N o m e   N O _ G G 1 ,   m e d . S i g l a ,   m e d . G r a n d e z a   ' ,  
     ' ' ] ) ;  
     S Q L _ A N D   : =   G e r a l . A T S ( [  
     ' A N D   ( ( I F ( x c o . G r a n d e z a   >   0 ,   x c o . G r a n d e z a ,   ' ,  
     '     I F ( ( g g x . G r a G r u Y < 2 0 4 8   O R   ' ,  
     '     x c o . C o u N i v 2 < > 1 ) ,   2 ,   ' ,  
     '     I F ( x c o . C o u N i v 2 = 1 ,   1 ,   - 1 ) ) )   < >   m e d . G r a n d e z a ) ) ' ,  
     ' ' ] ) ;  
     i f   G r a n d e z a s I n c o n s i s t e n t e s ( )   t h e n  
         E x i t ;  
     / /  
     S Q L _ F L D   : =   G e r a l . A T S ( [  
     ' S E L E C T     ' ,  
     / /  
     V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( F a l s e ,   ' T i p o E s t q ' ,   T r u e ,  
     ' v s x . A r e a D s t M 2 ' ,   ' v s x . P e s o K g D s t ' ,   ' v s x . P e c a s D s t ' ) ,  
     / /  
     ' v s x . *   ' ,  
     ' ' ] ) ;  
     S Q L _ A N D   : =   ' ' ;  
     / /  
     c a s e   M o v i m I D   o f  
         / / e m i d E m R i b P D A ,   J a   t e m   p r o p r i o :   I n s e r e _ O r i g e R i b P D A _ 2 1 0 ( )  
         / / e m i d E m R i b D T A ,  
         e m i d E m O p e r a c a o :  
         b e g i n  
             I n s e r e R e g i s t r o s ( t r s p 2 1 X ) ;  
         e n d ;  
         e m i d E m P r o c C a l ,  
         e m i d E m P r o c C u r ,  
         e m i d E m P r o c W E ,  
         e m i d E m P r o c S P :  
         b e g i n  
             I n s e r e R e g i s t r o s ( t r s p 2 3 X ) ;  
             I n s e r e R e g i s t r o s ( t r s p 2 5 X ) ;  
         e n d ;  
         e m i d E m R e p r R M :  
         b e g i n  
             I n s e r e R e g i s t r o s ( t r s p 2 6 X ) ;  
         e n d ;  
         e l s e   G e r a l . M B _ E r r o ( ' " M o v i m I D "   n � o   i m p l e m e n t a d o   e m   " I m p o r t a O p e P r o c A ( ) " ' ) ;  
     e n d ;  
     / /  
     R e s u l t   : =   T r u e ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I m p o r t a O p e P r o c B ( Q r C a b :   T M y S Q L Q u e r y ;  
     M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X :   T E s t q M o v i m I D ;   M o v i m N i v ,  
     P s q M o v i m N i v _ V M I :   T E s t q M o v i m N i v ;   S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o V S ,  
     S Q L _ P e r i o d o P Q ,   S Q L _ D t H r F i m O p e :   S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :  
     B o o l e a n ;  
 c o n s t  
     s P r o c N a m e   =   ' T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I m p o r t a O p e P r o c B ( ) " ' ;  
 b e g i n  
 / /  
     c a s e   F S P E D _ E F D _ P r o d u c a o   o f  
         0 :   I m p o r t a O p e P r o c B _ I s o l a d a ( Q r C a b ,   P s q M o v i m N i v _ V M I ,   S Q L _ P e r i o d o C o m p l e x o ,  
               S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,   S Q L _ D t H r F i m O p e ,   O r i g e m O p e P r o c ) ;  
         1 :   I m p o r t a O p e P r o c B _ C o n j u n t a ( Q r C a b ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,  
               M o v i m N i v ,   P s q M o v i m N i v _ V M I ,   S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o V S ,  
               S Q L _ P e r i o d o P Q ,   S Q L _ D t H r F i m O p e ,   O r i g e m O p e P r o c ) ;  
         e l s e   G e r a l . M B _ E r r o ( ' " F S P E D _ E F D _ P r o d u c a o "   i n d e f i n i d o   e m   '   +   s P r o c N a m e ) ;  
     e n d  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I m p o r t a O p e P r o c B _ C o n j u n t a ( Q r C a b :  
     T M y S Q L Q u e r y ;   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X :   T E s t q M o v i m I D ;   M o v i m N i v ,  
     P s q M o v i m N i v _ V M I :   T E s t q M o v i m N i v ;   S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o V S ,  
     S Q L _ P e r i o d o P Q ,   S Q L _ D t H r F i m O p e :   S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :  
     B o o l e a n ;  
 c o n s t  
     s P r o c N a m e   =   ' T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I m p o r t a O p e P r o c B _ C o n j u n t a ( ) " ' ;  
     / /  
     f u n c t i o n   P a r t e S Q L ( T a b e l a ,   F l d S r c ,   F l d D s t ,   s A r e a ,   s P e s o ,   s P e c a ,   d A r e a ,   d P e s o ,   d P e c a :   S t r i n g ;   M o v i m N i v :  
     T E s t q m o v i m N i v ) :   S t r i n g ;  
     v a r  
         S Q L _ I F :   S t r i n g ;  
     b e g i n  
         S Q L _ I F   : =   ' I F ( '   +   C o p y ( S Q L _ P e r i o d o V S ,   4 )   +   ' ,   ' ;  
         / /  
         R e s u l t   : =   G e r a l . A T S ( [  
         ' S E L E C T   v m i . E m p r e s a ,   v m i . M o v i m I D ,   v m i . C o d i g o ,   v m i . M o v i m C o d ,     ' ,  
         ' v m i . M o v i m N i v ,   v m i . C o n t r o l e ,   '   +  
         S Q L _ I F   +   s P e c a   +   ' ,   0 . 0 0 0 )   P e c a s ,   '   +  
         S Q L _ I F   +   s A r e a   +   ' ,   0 . 0 0 0 )   A r e a M 2 ,   '   +  
         S Q L _ I F   +   s P e s o   +   ' ,   0 . 0 0 0 )   P e s o K g ,   ' ,  
         ' D A T E ( v m i . D a t a H o r a )   D a t a ,     '   +   F l d D s t   +   '   G G X _ D s t ,   ' ,   / / +   F l d O r i   +   ' ,   ' ,  
         F l d S r c   +   '   G G X _ S r c ,   ' ,  
         '     ' ,  
         / /  
         V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( T r u e ,   ' s T i p o E s t q ' ,   T r u e ,  
         s A r e a ,   s P e s o ,   s P e c a ,   ' s ' ) ,  
         / /  
         V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( T r u e ,   ' d T i p o E s t q ' ,   T r u e ,  
         d A r e a ,   d P e s o ,   d P e c a ,   ' d ' ) ,  
         / /  
         '   ' ,  
         S Q L _ I F   +   d P e c a   +   ' ,   0 . 0 0 0 )   Q t d A n t P e c a ,   '   +  
         S Q L _ I F   +   d A r e a   +   ' ,   0 . 0 0 0 )   Q t d A n t A r M 2 ,   '   +  
         S Q L _ I F   +   d P e s o   +   ' ,   0 . 0 0 0 )   Q t d A n t P e s o ,   ' ,  
         ' v m i . D t C o r r A p o ,   v m i . C l i e n t M O ,   v m i . F o r n e c M O ,   s c c . E n t i S i t i o   ' ,  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         / / ' F R O M   '   +   T M e u D B   +   ' . '   +   T a b e l a   +   '   c a b   ' ,  
         ' F R O M   '   +   F S E I I _ I M E c s   +   '   i m c   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . '   +   T a b e l a   +   '   c a b   O N   c a b . M o v i m C o d = i m c . M o v i m C o d   ' ,  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . '   +   C O _ S E L _ T A B _ V M I   +   '       v m i   O N   v m i . M o v i m C o d = c a b . M o v i m C o d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . s t q c e n l o c   s c l   O N   s c l . C o n t r o l e = v m i . S t q C e n L o c   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . s t q c e n c a d   s c c   O N   s c c . C o d i g o = s c l . C o d i g o   ' ,  
         '   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x         s g g x   O N   s g g x . C o n t r o l e = v m i . G r a G r u X   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1         s g g 1   O N   s g g 1 . N i v e l 1 = s g g x . G r a G r u 1   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d         s m e d   O N   s m e d . C o d i g o = s g g 1 . U n i d M e d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u c         s g g c   O N   s g g c . C o n t r o l e = s g g x . G r a G r u C   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a c o r c a d     s g c c   O N   s g c c . C o d i g o = s g g c . G r a C o r C a d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a t a m i t s     s g t i   O N   s g t i . C o n t r o l e = s g g x . G r a T a m I   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x c o u   s x c o   O N   s x c o . G r a G r u X = s g g x . C o n t r o l e     ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 1         s n v 1   O N   s n v 1 . C o d i g o = s x c o . C o u N i v 1       ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 2         s n v 2   O N   s n v 2 . C o d i g o = s x c o . C o u N i v 2       ' ,  
         '   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x         d g g x   O N   d g g x . C o n t r o l e = c a b . G r a G r u X   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1         d g g 1   O N   d g g 1 . N i v e l 1 = d g g x . G r a G r u 1   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d         d m e d   O N   d m e d . C o d i g o = d g g 1 . U n i d M e d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u c         d g g c   O N   d g g c . C o n t r o l e = d g g x . G r a G r u C   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a c o r c a d     d g c c   O N   d g c c . C o d i g o = d g g c . G r a C o r C a d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a t a m i t s     d g t i   O N   d g t i . C o n t r o l e = d g g x . G r a T a m I   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x c o u   d x c o   O N   d x c o . G r a G r u X = d g g x . C o n t r o l e     ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 1         d n v 1   O N   d n v 1 . C o d i g o = d x c o . C o u N i v 1       ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 2         d n v 2   O N   d n v 2 . C o d i g o = d x c o . C o u N i v 2       ' ,  
         '   ' ,  
         / / ' W H E R E   i m c . M o v i m I D = '   +   G e r a l . F F 0 ( I n t e g e r ( P s q M o v i m I D ) ) ,  
     ' ' ] ) ;  
     e n d ;  
     p r o c e d u r e   A v i s o O p c E s p e c i f ( T x t :   S t r i n g ) ;  
     b e g i n  
         G e r a l . M B _ A v i s o ( ' C o n f i g u r a � � o   i n v � l i d a   e m   o p � � e s   e s p e c � f i c a s   p a r a   '   +  
         s P r o c N a m e   +   '   p a r a   o   i t e m :   '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v ) )   +  
         '   -   '   +   s E s t q M o v i m N i v [ I n t e g e r ( M o v i m N i v ) ]   +   T x t ) ;  
     e n d ;  
 v a r  
     S Q L x :   S t r i n g ;  
     I n c l u i P Q x :   B o o l e a n ;  
 b e g i n  
     c a s e   M o v i m N i v   o f  
         / /     C o u r o   e m   c a l e i r o   ( o r i g e m :   C o u r o   P D A )  
         T E s t q m o v i m N i v . e m i n S o r c C a l :  
         b e g i n  
             I n c l u i P Q x   : =   F a l s e ;  
             S Q L x   : =   P a r t e S Q L ( ' v s c a l c a b ' ,   ' v m i . R m s G G X ' ,   ' c a b . G r a G r u X ' ,  
             ' v m i . Q t d A n t A r M 2 ' ,   ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   ' v m i . Q t d A n t A r M 2 ' ,  
             ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   T E s t q m o v i m N i v . e m i n S o r c C a l ) ;  
             / /  
             P e s q u i s a I M E C s R e l a c i o n a d o s ( M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,   S Q L _ P e r i o d o V S ,  
             S Q L _ P e r i o d o P Q ,   I n c l u i P Q x ) ;  
             c a s e   D m o d . Q r C o n t r o l e S P E D _ I I _ E m C a l . V a l u e   o f  
 ( *  
                 2 :   I n s e r e B _ P r o d u c a o I s o l a d a ( S Q L x ,   S Q L _ P e r i o d o P Q ,   P s q M o v i m I D _ V M I ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ) ;  
 * )  
                 2 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s c a l c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p I s o l a d a ) ;  
                 3 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s c a l c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p C o n j u n t a ) ;  
                 e l s e   A v i s o O p c E s p e c i f ( ' ' ) ;  
             e n d ;  
             / /  
         e n d ;  
         T E s t q m o v i m N i v . e m i n D e s t C a l :  
         b e g i n  
 ( *  
 2 6 . 3 1   n � o   a c h a !  
 2 9 . 3 1   �   o   c e r t o   p a r a   C o u r o   c a l e a d o !  
 * )  
             I n c l u i P Q x   : =   T r u e ;  
             S Q L x   : =   P a r t e S Q L ( ' v s c a l j m p ' ,   ' v m i . J m p G G X ' ,   ' v m i . G r a G r u X ' ,   ' - v m i . Q t d G e r A r M 2 ' ,  
             ' - v m i . Q t d G e r P e s o ' ,   ' - v m i . Q t d G e r P e c a ' ,     ' v m i . Q t d G e r A r M 2 ' ,  
             ' v m i . Q t d G e r P e s o ' ,   ' v m i . Q t d G e r P e c a ' ,   T E s t q m o v i m N i v . e m i n D e s t C a l ) ;  
             / /  
             P e s q u i s a I M E C s R e l a c i o n a d o s ( M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,   S Q L _ P e r i o d o V S ,  
             S Q L _ P e r i o d o P Q ,   I n c l u i P Q x ) ;  
             / /  
             c a s e   D m o d . Q r C o n t r o l e S P E D _ I I _ C a l e a d o . V a l u e   o f  
 ( *  
                 2 :   I n s e r e B _ P r o d u c a o I s o l a d a ( S Q L x ,   S Q L _ P e r i o d o P Q ,   P s q M o v i m I D _ V M I ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ) ;  
 * )  
                 2 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s c a l c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p I s o l a d a ) ;  
                 3 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s c a l c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p C o n j u n t a ) ;  
                 e l s e   A v i s o O p c E s p e c i f ( ' ' ) ;  
             e n d ;  
         e n d ;  
         T E s t q m o v i m N i v . e m i n S o r c C u r :  
         b e g i n  
             I n c l u i P Q x   : =   F a l s e ;  
             S Q L x   : =   P a r t e S Q L ( ' v s c u r c a b ' ,   ' v m i . R m s G G X ' ,   ' c a b . G r a G r u X ' ,  
             ' v m i . Q t d A n t A r M 2 ' ,   ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   ' v m i . Q t d A n t A r M 2 ' ,  
             ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   T E s t q m o v i m N i v . e m i n S o r c C u r ) ;  
             / /  
             P e s q u i s a I M E C s R e l a c i o n a d o s ( M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,   S Q L _ P e r i o d o V S ,  
             S Q L _ P e r i o d o P Q ,   I n c l u i P Q x ) ;  
             / / I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s c u r c a b ' ,   P s q M o v i m I D ,   P s q M o v i m N i v ,   O r i g e m O p e P r o c ) ;  
             / / t e s t a r  
             c a s e   D m o d . Q r C o n t r o l e S P E D _ I I _ E m C u r . V a l u e   o f  
 ( *  
                 2 :   I n s e r e B _ P r o d u c a o I s o l a d a ( S Q L x ,   S Q L _ P e r i o d o P Q ,   P s q M o v i m I D _ V M I ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ) ;  
 * )  
                 2 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s c u r c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p I s o l a d a ) ;  
                 3 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s c u r c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p C o n j u n t a ) ;  
                 e l s e   A v i s o O p c E s p e c i f ( ' ' ) ;  
             e n d ;  
         e n d ;  
         T E s t q m o v i m N i v . e m i n D e s t C u r :  
         b e g i n  
             I n c l u i P Q x   : =   T r u e ;  
 ( *  
             S Q L x   : =   P a r t e S Q L ( ' v s c a l j m p ' ,   ' v m i . J m p G G X ' ,   ' v m i . G r a G r u X ' ,   ' - v m i . Q t d G e r A r M 2 ' ,  
             ' - v m i . Q t d G e r P e s o ' ,   ' - v m i . Q t d G e r P e c a ' ,     ' v m i . Q t d G e r A r M 2 ' ,  
             ' v m i . Q t d G e r P e s o ' ,   ' v m i . Q t d G e r P e c a ' ,   T E s t q m o v i m N i v . e m i n D e s t C a l ) ;  
 * )  
             S Q L x   : =   P a r t e S Q L ( ' v s c u r c a b ' ,   ' v m i . R m s G G X ' ,   ' c a b . G r a G r u X ' ,  
             ' v m i . Q t d A n t A r M 2 ' ,   ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   ' v m i . Q t d A n t A r M 2 ' ,  
             ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   T E s t q m o v i m N i v . e m i n S o r c C u r ) ;  
             P e s q u i s a I M E C s R e l a c i o n a d o s ( M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,   S Q L _ P e r i o d o V S ,  
             S Q L _ P e r i o d o P Q ,   I n c l u i P Q x ) ;  
             / /  
             c a s e   D m o d . Q r C o n t r o l e S P E D _ I I _ C a l e a d o . V a l u e   o f  
 ( *  
                 2 :   I n s e r e B _ P r o d u c a o I s o l a d a ( S Q L x ,   S Q L _ P e r i o d o P Q ,   P s q M o v i m I D _ V M I ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ) ;  
 * )  
                 2 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s c u r c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p I s o l a d a ) ;  
                 3 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s c u r c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p C o n j u n t a ) ;  
                 e l s e   A v i s o O p c E s p e c i f ( ' ' ) ;  
             e n d ;  
         e n d ;  
         T E s t q m o v i m N i v . e m i n D e s t O p e r :  
         b e g i n  
             I n c l u i P Q x   : =   T r u e ;  
             S Q L x   : =   ' S E L E C T   ? ? ? ?   F R O M   ? ? ? ? ' ;  
             P e s q u i s a I M E C s R e l a c i o n a d o s ( M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,   S Q L _ P e r i o d o V S ,  
             S Q L _ P e r i o d o P Q ,   I n c l u i P Q x ) ;  
             / /  
             c a s e   D m o d . Q r C o n t r o l e S P E D _ I I _ O p e r a c a o . V a l u e   o f  
                 2 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s o p e c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p I s o l a d a ) ;  
                 3 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s o p e c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p C o n j u n t a ) ;  
                 e l s e   A v i s o O p c E s p e c i f ( ' ' ) ;  
             e n d ;  
         e n d ;  
         T E s t q m o v i m N i v . e m i n D e s t W E n d :  
         b e g i n  
             I n c l u i P Q x   : =   T r u e ;  
             S Q L x   : =   ' S E L E C T   ? ? ? ?   F R O M   ? ? ? ? ' ;  
             P e s q u i s a I M E C s R e l a c i o n a d o s ( M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,   S Q L _ P e r i o d o V S ,  
             S Q L _ P e r i o d o P Q ,   I n c l u i P Q x ) ;  
             / /  
             c a s e   D m o d . Q r C o n t r o l e S P E D _ I I _ R e c u r t . V a l u e   o f  
                 2 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s p w e c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p I s o l a d a ) ;  
                 3 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s p w e c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p C o n j u n t a ) ;  
                 e l s e   A v i s o O p c E s p e c i f ( ' ' ) ;  
             e n d ;  
         e n d ;  
         T E s t q m o v i m N i v . e m i n D e s t P S P :  
         b e g i n  
             I n c l u i P Q x   : =   T r u e ;  
             S Q L x   : =   ' S E L E C T   ? ? ? ?   F R O M   ? ? ? ? ' ;  
             P e s q u i s a I M E C s R e l a c i o n a d o s ( M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,   S Q L _ P e r i o d o V S ,  
             S Q L _ P e r i o d o P Q ,   I n c l u i P Q x ) ;  
             / /  
             c a s e   D m o d . Q r C o n t r o l e S P E D _ I I _ S u b P r o d . V a l u e   o f  
                 2 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s p s p c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p I s o l a d a ) ;  
                 3 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s p s p c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p C o n j u n t a ) ;  
                 e l s e   A v i s o O p c E s p e c i f ( ' ' ) ;  
             e n d ;  
         e n d ;  
         T E s t q m o v i m N i v . e m i n D e s t R R M :  
         b e g i n  
             I n c l u i P Q x   : =   T r u e ;  
             S Q L x   : =   ' S E L E C T   ? ? ? ?   F R O M   ? ? ? ? ' ;  
             P e s q u i s a I M E C s R e l a c i o n a d o s ( M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,   S Q L _ P e r i o d o V S ,  
             S Q L _ P e r i o d o P Q ,   I n c l u i P Q x ) ;  
             / /  
             c a s e   D m o d . Q r C o n t r o l e S P E D _ I I _ R e p a r o . V a l u e   o f  
                 2 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s r r m c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p I s o l a d a ) ;  
                 3 :   I n s e r e B _ P r o d u c a o C o n j u n t a ( ' v s r r m c a b ' ,   M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X ,   P s q M o v i m N i v _ V M I ,  
                       O r i g e m O p e P r o c ,   f i p C o n j u n t a ) ;  
                 e l s e   A v i s o O p c E s p e c i f ( ' ' ) ;  
             e n d ;  
         e n d ;  
         / /  
         e l s e  
         b e g i n  
             I n c l u i P Q x   : =   F a l s e ;  
             S Q L x   : =   ' S E L E C T   ? ? ? ?   F R O M   ? ? ? ? ' ;  
             G e r a l . M B _ E r r o ( ' " M o v i m N i v "   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e ) ;  
         e n d ;  
     e n d ;  
 {  
     / /  
     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r M C ,   D M o d G . M y P I D _ D B ,   [  
     ' D R O P   T A B L E   I F   E X I S T S   _ S P E D _ E F D _ K 2 X X _ O _ P ;     ' ,  
     ' C R E A T E   T A B L E   _ S P E D _ E F D _ K 2 X X _ O _ P     ' ,  
     / /  
     S Q L x ,  
     ' ;   ' ,  
     '   ' ,  
     ' S E L E C T   D I S T I N C T   M o v i m C o d     ' ,  
     ' F R O M   _ S P E D _ E F D _ K 2 X X _ O _ P   ' ,  
     ' O R D E R   B Y   M o v i m C o d ;     ' ,  
     ' ' ] ) ;  
     / / G e r a l . M B _ S Q L ( S e l f ,   Q r M C ) ;  
     / /  
     i f   S P E D _ E F D _ P F . E r r G r a n d e z a ( ' _ S P E D _ E F D _ K 2 X X _ O _ P ' ,   ' G G x _ S r c ' ,   ' G G X _ D s t '   )   t h e n  
         E x i t ;  
     / /  
     P B 2 . P o s i t i o n   : =   0 ;  
     P B 2 . M a x   : =   Q r M C . R e c o r d C o u n t ;  
     Q r M C . F i r s t ;  
     / / i f   Q r M C . R e c o r d C o u n t   >   0   t h e n  
         / / G e r a l . M B _ A v i s o ( ' D E P R R C A R ! ! !   O p e P r o c A   -   1 ' ) ;  
     w h i l e   n o t   Q r M C . E o f   d o  
     b e g i n  
         M y O b j e c t s . U p d P B ( P B 2 ,   L a A v i s o 3 ,   L a A v i s o 4 ) ;  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r P r o d R i b ,   D M o d G . M y P I D _ D B ,   [  
         ' S E L E C T   *   ' ,  
         ' F R O M   _ S P E D _ E F D _ K 2 X X _ O _ P   ' ,  
         ' W H E R E   M o v i m C o d = '   +   G e r a l . F F 0 ( Q r M C M o v i m C o d . V a l u e ) ,  
         ' A N D   M o v i m I D = '   +   G e r a l . F F 0 ( I n t e g e r ( P s q M o v i m I D ) ) ,  
         ' ' ] ) ;  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q r P r o d R i b ) ;   / / P a r e i   a q u i !  
         Q r P r o d R i b . F i r s t ;  
         w h i l e   n o t   Q r P r o d R i b . E o f   d o  
         b e g i n  
             M o v i m I D           : =   T E s t q M o v i m I D ( Q r P r o d R i b M o v i m I D . V a l u e ) ;  
             A r e a M 2             : =   Q r P r o d R i b A r e a M 2 . V a l u e ;  
             P e s o K g             : =   Q r P r o d R i b P e s o K g . V a l u e ;  
             P e c a s               : =   Q r P r o d R i b P e c a s . V a l u e ;  
             Q t d A n t A r M 2     : =   Q r P r o d R i b Q t d A n t A r M 2 . V a l u e ;  
             Q t d A n t P e s o     : =   Q r P r o d R i b Q t d A n t P e s o . V a l u e ;  
             Q t d A n t P e c a     : =   Q r P r o d R i b Q t d A n t P e c a . V a l u e ;  
             D a t a                 : =   Q r P r o d R i b D a t a . V a l u e ;  
             S r c T i p o E s t q   : =   T r u n c ( Q r P r o d R i b s T i p o E s t q . V a l u e ) ;  
             D s t T i p o E s t q   : =   T r u n c ( Q r P r o d R i b d T i p o E s t q . V a l u e ) ;  
             G G X S r c             : =   Q r P r o d R i b G G X _ S r c . V a l u e ;  
             G G X D s t             : =   Q r P r o d R i b G G X _ D s t . V a l u e ;  
             M o v i m C o d         : =   Q r P r o d R i b M o v i m C o d . V a l u e ;  
             M o v i m N i v         : =   T E s t q M o v i m N i v ( Q r P r o d R i b M o v i m N i v . V a l u e ) ;  
             C o d i g o             : =   Q r P r o d R i b C o d i g o . V a l u e ;  
             C o n t r o l e         : =   Q r P r o d R i b C o n t r o l e . V a l u e ;  
             I D _ I t e m           : =   C o n t r o l e ;  
             / /  
             D t M o v i m           : =   D a t a ;  
             D t C o r r A p o       : =   Q r P r o d R i b D t C o r r A p o . V a l u e ;  
             / /  
             C l i e n t M O         : =   Q r P r o d R i b C l i e n t M O . V a l u e ;  
             F o r n e c M O         : =   Q r P r o d R i b F o r n e c M O . V a l u e ;  
             E n t i S i t i o       : =   Q r P r o d R i b E n t i S i t i o . V a l u e ;  
             / /  
             i f   ( C l i e n t M O   = 0 )   o r   ( F o r n e c M O = 0 )   o r   ( E n t i S i t i o = 0 )   t h e n  
             b e g i n  
                 M o v i m N i v I n n   : =   V S _ P F . O b t e m M o v i m N i v I n n D e M o v i m I D ( M o v i m I D ) ;  
                 U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r O b t C l i F o r S i t e ,   D m o d . M y D B ,   [  
                 ' S E L E C T   v m i . C l i e n t M O ,   v m i . F o r n e c M O ,   s c c . E n t i S i t i o     ' ,  
                 ' F R O M   v s m o v i t s   v m i   ' ,  
                 ' L E F T   J O I N   s t q c e n l o c   s c l   O N   s c l . C o n t r o l e = v m i . S t q C e n L o c     ' ,  
                 ' L E F T   J O I N   s t q c e n c a d   s c c   O N   s c c . C o d i g o = s c l . C o d i g o     ' ,  
                 ' W H E R E   v m i . M o v i m C o d = '   +   G e r a l . F F 0 ( M o v i m C o d ) ,  
                 ' A N D   v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v I n n ) ) ,  
                 ' ' ] ) ;  
                 C l i e n t M O     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' C l i e n t M O ' ) . A s I n t e g e r ;  
                 F o r n e c M O     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' F o r n e c M O ' ) . A s I n t e g e r ;  
                 E n t i S i t i o     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' E n t i S i t i o ' ) . A s I n t e g e r ;  
             e n d ;  
             / /  
             / / i f   ( P e c a s   < >   0 )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
             i f   ( ( P e c a s   < >   0 )   o r   ( M o v i m I D = T E s t q M o v i m I D . e m i d E m P r o c S P ) )   a n d   ( D a t a   >   1 )   t h e n  
             b e g i n  
                 c a s e   S r c T i p o E s t q   o f  
                     1 :   Q t d e S r c   : =   A r e a M 2 ;  
                     2 :   Q t d e S r c   : =   P e s o K g ;  
                     e l s e  
                     b e g i n  
                         G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( S r c T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                         +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ S r c T i p o E s t q ]   +   s L i n e B r e a k   +  
                         ' R e d u z i d o   =   '   +   G e r a l . F F 0 ( G G X D s t )   +   s L i n e B r e a k   +  
                         ' M o v i m C o d   =   '   +   G e r a l . F F 0 ( M o v i m C o d )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
                         Q r P r o d R i b . S Q L . T e x t ) ;  
                         / /  
                         Q t d e D s t   : =   P e c a s ;  
                     e n d ;  
                 e n d ;  
                 Q t d e S r c   : =   Q t d e S r c   *   S r c F a t o r ;  
  
                 / /  
                 c a s e   D s t T i p o E s t q   o f  
                     1 :   Q t d e D s t   : =   Q t d A n t A r M 2 ;  
                     2 :   Q t d e D s t   : =   Q t d A n t P e s o ;  
                     e l s e  
                     b e g i n  
                         G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( S r c T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                         +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ S r c T i p o E s t q ]   +   s L i n e B r e a k   +  
                         ' R e d u z i d o   =   '   +   G e r a l . F F 0 ( G G X D s t )   +   s L i n e B r e a k   +  
                         ' M o v i m C o d   =   '   +   G e r a l . F F 0 ( M o v i m C o d )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
                         Q r P r o d R i b . S Q L . T e x t ) ;  
                         / /  
                         Q t d e D s t   : =   Q t d A n t P e c a ;  
                     e n d ;  
                 e n d ;  
                 Q t d e D s t   : =   Q t d e D s t   *   D s t F a t o r ;  
             e n d   e l s e  
             b e g i n  
                 Q t d e S r c   : =   0 ;  
                 Q t d e D s t   : =   0 ;  
             e n d ;  
             / /  
             i f   Q t d e S r c   <   0   t h e n  
                 M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                 ' Q u a n t i d a d e   " S r c "   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 5 ) '   +   s L i n e B r e a k   +  
                 ' Q t d e :   '   +   G e r a l . F F T ( Q t d e S r c ,   3 ,   s i N e g a t i v o ) ,   Q r P r o d R i b ) ;  
             i f   Q t d e D s t   <   0   t h e n  
                 M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                 ' Q u a n t i d a d e   " D s t "   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 6 ) '   +   s L i n e B r e a k   +  
                 ' Q t d e :   '   +   G e r a l . F F T ( Q t d e D s t ,   3 ,   s i N e g a t i v o ) ,   Q r P r o d R i b )  
             e l s e   i f   ( Q t d e D s t   =   0 )   a n d   ( D a t a   >   1 )   t h e n  
             b e g i n  
                 c a s e   M o v i m I D   o f  
                     e m i d E m P r o c C a l :   D a t a   : =   0 ;  
                     e m i d E m P r o c C u r :   D a t a   : =   0 ;  
                     e l s e   M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                     ' Q u a n t i d a d e   z e r a d a   p a r a   O P   e n c e r r a d a ! ! !   ( 4 ) ' ,   Q r P r o d R i b ) ;  
                 e n d ;  
             e n d ;  
             / /  
             / / i f   Q r S u b P r d O P E m p r e s a . V a l u e   =   Q r F M O F o r n e c M O . V a l u e   t h e n  
  
             i f   F E m p r e s a   =   F o r n e c M O   t h e n  
                 T i p o R e g S P E D P r o d   : =   t r s p 2 3 X  
             e l s e  
                 T i p o R e g S P E D P r o d   : =   t r s p 2 5 X ;  
             / /  
             D t H r A b e r t o   : =   D a t a ;  
             D t H r A b e r t o   : =   D a t a ;  
             c a s e   T i p o R e g S P E D P r o d   o f  
                 t r s p 2 3 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                     C o n t r o l e ,   D t H r A b e r t o ,   D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o ,   G G X D s t ,  
                     C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   Q t d e D s t ,   O r i g e m O p e P r o c ,  
                     F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   I D S e q 1 ) ;  
                     / /  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 ,   D t H r A b e r t o ,  
                     D t C o r r A p o ,   G G X S r c ,   Q t d e S r c ,   C O D _ I N S _ S U B S T ,   I D _ I t e m ,   I n t e g e r ( M o v i m I D ) ,  
                     C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,  
                     O r i g e m O p e P r o c ,   E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                     / /  
                     I n s e r e _ O r i g e P r o c P Q ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   M o v i m N i v ,   M o v i m C o d ,  
                     I D S e q 1 ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ) ;  
                 e n d ;  
                 t r s p 2 5 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,  
                     D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o ,   G G X D s t ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,  
                     Q t d e D s t ,   O r i g e m O p e P r o c ,   F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   I D S e q 1 ) ;  
                     / /  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 ,   D t H r A b e r t o ,   D t C o r r A p o ,   G G X S r c ,  
                     Q t d e S r c ,   C O D _ I N S _ S U B S T ,   I D _ I t e m ,  
                     I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,  
                     E n t i S i t i o ,   O r i g e m O p e P r o c ,   E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                     / /  
                     I n s e r e _ O r i g e P r o c P Q ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   M o v i m N i v ,   M o v i m C o d ,  
                     I D S e q 1 ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ) ;  
                 e n d ;  
                 e l s e   G e r a l . M B _ E r r o ( ' T i p o   d e   r e g i s t r o   S P E D   E F D   ( '   +   G e r a l . F F 0 ( I n t e g e r (  
                 T i p o R e g S P E D P r o d ) )   +   ' )   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e   +   ' ( 4 ) ' ) ;  
             e n d ;  
             Q r P r o d R i b . N e x t ;  
         e n d ;  
         Q r M C . N e x t ;  
     e n d ;  
 }  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I m p o r t a O p e P r o c B _ I s o l a d a ( Q r C a b :   T M y S Q L Q u e r y ;  
     P s q M o v i m N i v :   T E s t q M o v i m N i v ;   S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o V S ,   S Q L _ P e r i o d o P Q ,  
     S Q L _ D t H r F i m O p e :   S t r i n g ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) :   B o o l e a n ;  
     / /  
     f u n c t i o n   P a r t e S Q L ( T a b e l a ,   F l d S r c ,   F l d D s t ,   s A r e a ,   s P e s o ,   s P e c a ,   d A r e a ,   d P e s o ,   d P e c a :   S t r i n g ;   M o v i m N i v :  
     T E s t q m o v i m N i v ) :   S t r i n g ;  
     v a r  
         S Q L _ I F :   S t r i n g ;  
     b e g i n  
         S Q L _ I F   : =   ' I F ( '   +   C o p y ( S Q L _ P e r i o d o V S ,   4 )   +   ' ,   ' ;  
         / /  
         R e s u l t   : =   G e r a l . A T S ( [  
         ' S E L E C T   v m i . E m p r e s a ,   v m i . M o v i m I D ,   v m i . C o d i g o ,   v m i . M o v i m C o d ,     ' ,  
         ' v m i . M o v i m N i v ,   v m i . C o n t r o l e ,   '   +  
         S Q L _ I F   +   s P e c a   +   ' ,   0 . 0 0 0 )   P e c a s ,   '   +  
         S Q L _ I F   +   s A r e a   +   ' ,   0 . 0 0 0 )   A r e a M 2 ,   '   +  
         S Q L _ I F   +   s P e s o   +   ' ,   0 . 0 0 0 )   P e s o K g ,   ' ,  
         ' D A T E ( v m i . D a t a H o r a )   D a t a ,     '   +   F l d D s t   +   '   G G X _ D s t ,   ' ,   / / +   F l d O r i   +   ' ,   ' ,  
         F l d S r c   +   '   G G X _ S r c ,   ' ,  
         '     ' ,  
         / /  
         V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( T r u e ,   ' s T i p o E s t q ' ,   T r u e ,  
         s A r e a ,   s P e s o ,   s P e c a ,   ' s ' ) ,  
         / /  
         V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( T r u e ,   ' d T i p o E s t q ' ,   T r u e ,  
         d A r e a ,   d P e s o ,   d P e c a ,   ' d ' ) ,  
         / /  
         '   ' ,  
         S Q L _ I F   +   d P e c a   +   ' ,   0 . 0 0 0 )   Q t d A n t P e c a ,   '   +  
         S Q L _ I F   +   d A r e a   +   ' ,   0 . 0 0 0 )   Q t d A n t A r M 2 ,   '   +  
         S Q L _ I F   +   d P e s o   +   ' ,   0 . 0 0 0 )   Q t d A n t P e s o ,   ' ,  
         ' v m i . D t C o r r A p o ,   v m i . C l i e n t M O ,   v m i . F o r n e c M O ,   s c c . E n t i S i t i o   ' ,  
         ' F R O M   '   +   T M e u D B   +   ' . '   +   T a b e l a   +   '   c a b   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . '   +   C O _ S E L _ T A B _ V M I   +   '       v m i   O N   v m i . M o v i m C o d = c a b . M o v i m C o d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . s t q c e n l o c   s c l   O N   s c l . C o n t r o l e = v m i . S t q C e n L o c   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . s t q c e n c a d   s c c   O N   s c c . C o d i g o = s c l . C o d i g o   ' ,  
         '   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x         s g g x   O N   s g g x . C o n t r o l e = v m i . G r a G r u X   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1         s g g 1   O N   s g g 1 . N i v e l 1 = s g g x . G r a G r u 1   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d         s m e d   O N   s m e d . C o d i g o = s g g 1 . U n i d M e d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u c         s g g c   O N   s g g c . C o n t r o l e = s g g x . G r a G r u C   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a c o r c a d     s g c c   O N   s g c c . C o d i g o = s g g c . G r a C o r C a d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a t a m i t s     s g t i   O N   s g t i . C o n t r o l e = s g g x . G r a T a m I   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x c o u   s x c o   O N   s x c o . G r a G r u X = s g g x . C o n t r o l e     ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 1         s n v 1   O N   s n v 1 . C o d i g o = s x c o . C o u N i v 1       ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 2         s n v 2   O N   s n v 2 . C o d i g o = s x c o . C o u N i v 2       ' ,  
         '   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x         d g g x   O N   d g g x . C o n t r o l e = c a b . G r a G r u X   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1         d g g 1   O N   d g g 1 . N i v e l 1 = d g g x . G r a G r u 1   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d         d m e d   O N   d m e d . C o d i g o = d g g 1 . U n i d M e d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u c         d g g c   O N   d g g c . C o n t r o l e = d g g x . G r a G r u C   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a c o r c a d     d g c c   O N   d g c c . C o d i g o = d g g c . G r a C o r C a d   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a t a m i t s     d g t i   O N   d g t i . C o n t r o l e = d g g x . G r a T a m I   ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x c o u   d x c o   O N   d x c o . G r a G r u X = d g g x . C o n t r o l e     ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 1         d n v 1   O N   d n v 1 . C o d i g o = d x c o . C o u N i v 1       ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 2         d n v 2   O N   d n v 2 . C o d i g o = d x c o . C o u N i v 2       ' ,  
         '   ' ,  
         ' W H E R E   ( v m i . E m p r e s a = '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         ' A N D   v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v ) ) ,  
         S Q L _ P e r i o d o V S ,  
     ' )   O R   ' ,  
     ' (   ' ,  
     ' v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v ) ) ,  
     ' A N D   c a b . M o v i m C o d   I N   (   ' ,  
     ' S E L E C T   D I S T I N C T   V S M o v C o d     ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . e m i t     ' ,  
     ' W H E R E   C o d i g o   I N   (       ' ,  
     '     S E L E C T   O r i g e m C o d i     ' ,  
     '     F R O M   '   +   T M e u D B   +   ' . p q x     ' ,  
     '     W H E R E   T i p o = 1 1 0           ' ,  
     '     '   +   S Q L _ P e r i o d o P Q ,  
     ' )       ' ,  
     ' A N D   V S M o v C o d   < >   0     ' ,  
     '     ' ,  
     ' U N I O N       ' ,  
     '     ' ,  
     ' S E L E C T   D I S T I N C T   V S M o v C o d     ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . p q o     ' ,  
     ' W H E R E   C o d i g o   I N   (       ' ,  
     '     S E L E C T   O r i g e m C o d i     ' ,  
     '     F R O M   '   +   T M e u D B   +   ' . p q x     ' ,  
     '     W H E R E   T i p o = 1 1 0           ' ,  
     '     '   +   S Q L _ P e r i o d o P Q ,  
     ' )       ' ,  
     ' A N D   V S M o v C o d   < >   0     ' ,  
     / / / /  
     ' )   ' ,  
     ' )   ' ,  
     ' ' ] ) ;  
     e n d ;  
 c o n s t  
     s P r o c N a m e   =   ' " F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I m p o r t a O p e P r o c B _ I s o l a d a ( ) " ' ;  
     / / S r c F a t o r   =   - 1 ;  
     D s t F a t o r   =   1 ;  
     E S T S T a b S o r c   =   e s t s V M I ;  
 v a r  
     P e c a s ,   A r e a M 2 ,   P e s o K g ,   Q t d e S r c ,   Q t d e D s t ,   Q t d A n t A r M 2 ,   Q t d A n t P e s o ,   Q t d A n t P e c a :  
     D o u b l e ;  
     D a t a ,   D t H r A b e r t o ,   D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;  
     S r c T i p o E s t q ,   D s t T i p o E s t q ,   G G X S r c ,   G G X D s t ,   C o d i g o ,   M o v i m C o d ,   I D S e q 1 ,  
     I D _ I t e m ,   C o n t r o l e ,   S r c F a t o r ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o :   I n t e g e r ;  
     M o v i m N i v :   T E s t q M o v i m N i v ;  
     T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;  
     C O D _ I N S _ S U B S T ,   S Q L x :   S t r i n g ;  
     M o v i m I D :   T E s t q M o v i m I D ;  
     M o v i m N i v I n n :   T E s t q M o v i m N i v ;  
 b e g i n  
     C O D _ I N S _ S U B S T   : =   ' ' ;  
     / /  
     c a s e   P s q M o v i m N i v   o f  
         T E s t q m o v i m N i v . e m i n S o r c C a l :   S r c F a t o r   : =   1 ;  
         T E s t q m o v i m N i v . e m i n D e s t C a l :   S r c F a t o r   : =   - 1 ;  
         T E s t q m o v i m N i v . e m i n S o r c C u r :   S r c F a t o r   : =   1 ;  
         e l s e  
         b e g i n  
             S r c F a t o r   : =   - 1 ;  
             G e r a l . M B _ E r r o ( ' " S r c F a t o r "   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e ) ;  
         e n d ;  
     e n d ;  
     c a s e   P s q M o v i m N i v   o f  
         T E s t q m o v i m N i v . e m i n S o r c C a l :   S Q L x   : =  
             P a r t e S Q L ( ' v s c a l c a b ' ,   ' v m i . R m s G G X ' ,   ' c a b . G r a G r u X ' ,  
             ' v m i . Q t d A n t A r M 2 ' ,   ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   ' v m i . Q t d A n t A r M 2 ' ,  
             ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   T E s t q m o v i m N i v . e m i n S o r c C a l ) ;  
         T E s t q m o v i m N i v . e m i n D e s t C a l :   S Q L x   : =  
             P a r t e S Q L ( ' v s c a l j m p ' ,   ' v m i . J m p G G X ' ,   ' v m i . G r a G r u X ' ,   ' - v m i . Q t d G e r A r M 2 ' ,  
             ' - v m i . Q t d G e r P e s o ' ,   ' - v m i . Q t d G e r P e c a ' ,     ' v m i . Q t d G e r A r M 2 ' ,  
             ' v m i . Q t d G e r P e s o ' ,   ' v m i . Q t d G e r P e c a ' ,   T E s t q m o v i m N i v . e m i n D e s t C a l ) ;  
         T E s t q m o v i m N i v . e m i n S o r c C u r :   S Q L x   : =  
             P a r t e S Q L ( ' v s c u r c a b ' ,   ' v m i . R m s G G X ' ,   ' c a b . G r a G r u X ' ,  
             ' v m i . Q t d A n t A r M 2 ' ,   ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   ' v m i . Q t d A n t A r M 2 ' ,  
             ' v m i . Q t d A n t P e s o ' ,   ' v m i . Q t d A n t P e c a ' ,   T E s t q m o v i m N i v . e m i n S o r c C u r ) ;  
         e l s e  
         b e g i n  
             S Q L x   : =   ' S E L E C T   ? ? ? ?   F R O M   ? ? ? ? ' ;  
             G e r a l . M B _ E r r o ( ' " M o v i m N i v "   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e ) ;  
         e n d ;  
     e n d ;  
  
     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r M C ,   D M o d G . M y P I D _ D B ,   [  
     ' D R O P   T A B L E   I F   E X I S T S   _ S P E D _ E F D _ K 2 X X _ O _ P ;     ' ,  
     ' C R E A T E   T A B L E   _ S P E D _ E F D _ K 2 X X _ O _ P     ' ,  
     / /  
     S Q L x ,  
     ' ;   ' ,  
     '   ' ,  
     ' S E L E C T   D I S T I N C T   M o v i m C o d     ' ,  
     ' F R O M   _ S P E D _ E F D _ K 2 X X _ O _ P   ' ,  
     ' O R D E R   B Y   M o v i m C o d ;     ' ,  
     ' ' ] ) ;  
     / / G e r a l . M B _ S Q L ( S e l f ,   Q r M C ) ;   / / P a r e i   a q u i  
     / /  
     i f   S P E D _ E F D _ P F . E r r G r a n d e z a ( ' _ S P E D _ E F D _ K 2 X X _ O _ P ' ,   ' G G x _ S r c ' ,   ' G G X _ D s t '   )   t h e n  
         E x i t ;  
     / /  
     P B 2 . P o s i t i o n   : =   0 ;  
     P B 2 . M a x   : =   Q r M C . R e c o r d C o u n t ;  
     Q r M C . F i r s t ;  
     / / i f   Q r M C . R e c o r d C o u n t   >   0   t h e n  
         / / G e r a l . M B _ A v i s o ( ' D E P R R C A R ! ! !   O p e P r o c A   -   1 ' ) ;  
     w h i l e   n o t   Q r M C . E o f   d o  
     b e g i n  
         M y O b j e c t s . U p d P B ( P B 2 ,   L a A v i s o 3 ,   L a A v i s o 4 ) ;  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r P r o d R i b ,   D M o d G . M y P I D _ D B ,   [  
         ' S E L E C T   *   ' ,  
         ' F R O M   _ S P E D _ E F D _ K 2 X X _ O _ P   ' ,  
         ' W H E R E   M o v i m C o d = '   +   G e r a l . F F 0 ( Q r M C M o v i m C o d . V a l u e ) ,  
         ' ' ] ) ;  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q r P r o d R i b ) ;   / / P a r e i   a q u i !  
         Q r P r o d R i b . F i r s t ;  
         w h i l e   n o t   Q r P r o d R i b . E o f   d o  
         b e g i n  
             M o v i m I D           : =   T E s t q M o v i m I D ( Q r P r o d R i b M o v i m I D . V a l u e ) ;  
             A r e a M 2             : =   Q r P r o d R i b A r e a M 2 . V a l u e ;  
             P e s o K g             : =   Q r P r o d R i b P e s o K g . V a l u e ;  
             P e c a s               : =   Q r P r o d R i b P e c a s . V a l u e ;  
             Q t d A n t A r M 2     : =   Q r P r o d R i b Q t d A n t A r M 2 . V a l u e ;  
             Q t d A n t P e s o     : =   Q r P r o d R i b Q t d A n t P e s o . V a l u e ;  
             Q t d A n t P e c a     : =   Q r P r o d R i b Q t d A n t P e c a . V a l u e ;  
             D a t a                 : =   Q r P r o d R i b D a t a . V a l u e ;  
             S r c T i p o E s t q   : =   T r u n c ( Q r P r o d R i b s T i p o E s t q . V a l u e ) ;  
             D s t T i p o E s t q   : =   T r u n c ( Q r P r o d R i b d T i p o E s t q . V a l u e ) ;  
             G G X S r c             : =   Q r P r o d R i b G G X _ S r c . V a l u e ;  
             G G X D s t             : =   Q r P r o d R i b G G X _ D s t . V a l u e ;  
             M o v i m C o d         : =   Q r P r o d R i b M o v i m C o d . V a l u e ;  
             M o v i m N i v         : =   T E s t q M o v i m N i v ( Q r P r o d R i b M o v i m N i v . V a l u e ) ;  
             C o d i g o             : =   Q r P r o d R i b C o d i g o . V a l u e ;  
             C o n t r o l e         : =   Q r P r o d R i b C o n t r o l e . V a l u e ;  
             I D _ I t e m           : =   C o n t r o l e ;  
             / /  
             D t M o v i m           : =   D a t a ;  
             D t C o r r A p o       : =   Q r P r o d R i b D t C o r r A p o . V a l u e ;  
             / /  
             C l i e n t M O         : =   Q r P r o d R i b C l i e n t M O . V a l u e ;  
             F o r n e c M O         : =   Q r P r o d R i b F o r n e c M O . V a l u e ;  
             E n t i S i t i o       : =   Q r P r o d R i b E n t i S i t i o . V a l u e ;  
             / /  
             i f   ( C l i e n t M O   = 0 )   o r   ( F o r n e c M O = 0 )   o r   ( E n t i S i t i o = 0 )   t h e n  
             b e g i n  
                 M o v i m N i v I n n   : =   V S _ P F . O b t e m M o v i m N i v I n n D e M o v i m I D ( M o v i m I D ) ;  
                 U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r O b t C l i F o r S i t e ,   D m o d . M y D B ,   [  
                 ' S E L E C T   v m i . C l i e n t M O ,   v m i . F o r n e c M O ,   s c c . E n t i S i t i o     ' ,  
                 ' F R O M   v s m o v i t s   v m i   ' ,  
                 ' L E F T   J O I N   s t q c e n l o c   s c l   O N   s c l . C o n t r o l e = v m i . S t q C e n L o c     ' ,  
                 ' L E F T   J O I N   s t q c e n c a d   s c c   O N   s c c . C o d i g o = s c l . C o d i g o     ' ,  
                 ' W H E R E   v m i . M o v i m C o d = '   +   G e r a l . F F 0 ( M o v i m C o d ) ,  
                 ' A N D   v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v I n n ) ) ,  
                 ' ' ] ) ;  
                 C l i e n t M O     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' C l i e n t M O ' ) . A s I n t e g e r ;  
                 F o r n e c M O     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' F o r n e c M O ' ) . A s I n t e g e r ;  
                 E n t i S i t i o     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' E n t i S i t i o ' ) . A s I n t e g e r ;  
             e n d ;  
             / /  
             / / i f   ( P e c a s   < >   0 )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
             i f   ( ( P e c a s   < >   0 )   o r   ( M o v i m I D = T E s t q M o v i m I D . e m i d E m P r o c S P ) )   a n d   ( D a t a   >   1 )   t h e n  
             b e g i n  
                 c a s e   S r c T i p o E s t q   o f  
                     1 :   Q t d e S r c   : =   A r e a M 2 ;  
                     2 :   Q t d e S r c   : =   P e s o K g ;  
                     e l s e  
                     b e g i n  
                         G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( S r c T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                         +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ S r c T i p o E s t q ]   +   s L i n e B r e a k   +  
                         ' R e d u z i d o   =   '   +   G e r a l . F F 0 ( G G X D s t )   +   s L i n e B r e a k   +  
                         ' M o v i m C o d   =   '   +   G e r a l . F F 0 ( M o v i m C o d )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
                         Q r P r o d R i b . S Q L . T e x t ) ;  
                         / /  
                         Q t d e D s t   : =   P e c a s ;  
                     e n d ;  
                 e n d ;  
                 Q t d e S r c   : =   Q t d e S r c   *   S r c F a t o r ;  
  
                 / /  
                 c a s e   D s t T i p o E s t q   o f  
                     1 :   Q t d e D s t   : =   Q t d A n t A r M 2 ;  
                     2 :   Q t d e D s t   : =   Q t d A n t P e s o ;  
                     e l s e  
                     b e g i n  
                         G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( S r c T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                         +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ S r c T i p o E s t q ]   +   s L i n e B r e a k   +  
                         ' R e d u z i d o   =   '   +   G e r a l . F F 0 ( G G X D s t )   +   s L i n e B r e a k   +  
                         ' M o v i m C o d   =   '   +   G e r a l . F F 0 ( M o v i m C o d )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
                         Q r P r o d R i b . S Q L . T e x t ) ;  
                         / /  
                         Q t d e D s t   : =   Q t d A n t P e c a ;  
                     e n d ;  
                 e n d ;  
                 Q t d e D s t   : =   Q t d e D s t   *   D s t F a t o r ;  
             e n d   e l s e  
             b e g i n  
                 Q t d e S r c   : =   0 ;  
                 Q t d e D s t   : =   0 ;  
             e n d ;  
             i f   ( * T e m T w n   a n d * )   ( Q t d e S r c   =   0 )   t h e n  
                 V e r i f i c a T i p o E s t q Q t d e ( S r c T i p o E s t q ,   A r e a M 2 ,   P e s o K g ,   P e c a s ,  
                 G G X S r c ,   M o v i m C o d ,   s P r o c N a m e ) ;  
             i f   ( * T e m T w n   a n d * )   ( Q t d e D s t   =   0 )   t h e n  
                 V e r i f i c a T i p o E s t q Q t d e ( D s t T i p o E s t q ,   A r e a M 2 ,   P e s o K g ,   P e c a s ,  
                 G G X D s t ,   M o v i m C o d ,   s P r o c N a m e ) ;  
             / /  
             i f   Q t d e S r c   <   0   t h e n  
                 M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                 ' Q u a n t i d a d e   " S r c "   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 5 ) '   +   s L i n e B r e a k   +  
                 ' Q t d e :   '   +   G e r a l . F F T ( Q t d e S r c ,   3 ,   s i N e g a t i v o ) ,   Q r P r o d R i b ) ;  
             i f   Q t d e D s t   <   0   t h e n  
                 M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                 ' Q u a n t i d a d e   " D s t "   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 6 ) '   +   s L i n e B r e a k   +  
                 ' Q t d e :   '   +   G e r a l . F F T ( Q t d e D s t ,   3 ,   s i N e g a t i v o ) ,   Q r P r o d R i b )  
             e l s e   i f   ( Q t d e D s t   =   0 )   a n d   ( D a t a   >   1 )   t h e n  
             b e g i n  
                 c a s e   M o v i m I D   o f  
                     e m i d E m P r o c C a l :   D a t a   : =   0 ;  
                     e m i d E m P r o c C u r :   D a t a   : =   0 ;  
                     e l s e   M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                     ' Q u a n t i d a d e   z e r a d a   p a r a   O P   e n c e r r a d a ! ! !   ( 4 ) ' ,   Q r P r o d R i b ) ;  
                 e n d ;  
             e n d ;  
             / /  
             / / i f   Q r S u b P r d O P E m p r e s a . V a l u e   =   Q r F M O F o r n e c M O . V a l u e   t h e n  
  
             i f   F E m p r e s a   =   F o r n e c M O   t h e n  
                 T i p o R e g S P E D P r o d   : =   t r s p 2 3 X  
             e l s e  
                 T i p o R e g S P E D P r o d   : =   t r s p 2 5 X ;  
             / /  
             D t H r A b e r t o   : =   D a t a ;  
             D t H r A b e r t o   : =   D a t a ;  
             c a s e   T i p o R e g S P E D P r o d   o f  
                 t r s p 2 3 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                     C o n t r o l e ,   D t H r A b e r t o ,   D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o ,   G G X D s t ,  
                     C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   Q t d e D s t ,   O r i g e m O p e P r o c ,  
                     F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   I D S e q 1 ) ;  
                     / /  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 ,   D t H r A b e r t o ,  
                     D t C o r r A p o ,   G G X S r c ,   Q t d e S r c ,   C O D _ I N S _ S U B S T ,   I D _ I t e m ,   I n t e g e r ( M o v i m I D ) ,  
                     C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,  
                     O r i g e m O p e P r o c ,   E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                     / /  
 ( *                 D e s a b i l i t a r ? .   P Q   S e r �   i n c l u i d o   n o   c o u r o   p r o c e s s a d o   e   n � o   e m   p r o c e s s o ! * )  
                     I n s e r e _ O r i g e P r o c P Q ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   M o v i m N i v ,   M o v i m C o d ,  
                     I D S e q 1 ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ) ;  
                 e n d ;  
                 t r s p 2 5 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,  
                     D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o ,   G G X D s t ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,  
                     Q t d e D s t ,   O r i g e m O p e P r o c ,   F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   I D S e q 1 ) ;  
                     / /  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 ,   D t H r A b e r t o ,   D t C o r r A p o ,   G G X S r c ,  
                     Q t d e S r c ,   C O D _ I N S _ S U B S T ,   I D _ I t e m ,  
                     I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,  
                     E n t i S i t i o ,   O r i g e m O p e P r o c ,   E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                     / /  
 ( *                 D e s a b i l i t a r ? .   P Q   S e r �   i n c l u i d o   n o   c o u r o   p r o c e s s a d o   e   n � o   e m   p r o c e s s o ! * )  
                     I n s e r e _ O r i g e P r o c P Q ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   M o v i m N i v ,   M o v i m C o d ,  
                     I D S e q 1 ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ) ;  
  
                 e n d ;  
                 e l s e   G e r a l . M B _ E r r o ( ' T i p o   d e   r e g i s t r o   S P E D   E F D   ( '   +   G e r a l . F F 0 ( I n t e g e r (  
                 T i p o R e g S P E D P r o d ) )   +   ' )   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e   +   ' ( 5 ) ' ) ;  
             e n d ;  
             Q r P r o d R i b . N e x t ;  
         e n d ;  
         Q r M C . N e x t ;  
     e n d ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I n s e r e B _ O r i g e P r o c P Q _ C o n j u n t a (  
     T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;   M o v i m I D :   T E s t q M o v i m I D ;  
     M o v i m N i v :   T E s t q M o v i m N i v ;   M o v i m C o d ,   I D S e q 1 :   I n t e g e r ;   S Q L _ P e r i o d o P Q :   S t r i n g ;  
     O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;   C l i e n t M O ,   F o r n e c M O ,  
     E n t i S i t i o :   I n t e g e r ) :   B o o l e a n ;  
 c o n s t  
     F a t o r   =   - 1 ;  
     O r i E S T S T a b S o r c   =   e s t s P Q x ;  
     s P r o c N a m e   =   ' F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I n s e r e B _ O r i g e P r o c P Q _ C o n j u n t a ( ) ' ;  
     J m p M o v I D   =   0 ;  
     J m p M o v C o d   =   0 ;  
     J m p N i v e l 1   =   0 ;  
     J m p N i v e l 2   =   0 ;  
     P a i M o v I D   =   0 ;  
     M o v C o d P a i   =   0 ;  
     P a i N i v e l 1   =   0 ;  
     P a i N i v e l 2   =   0 ;  
 v a r  
     D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;  
     C o d i g o ,   C o n t r o l e ,   G r a G r u X ( * ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o * ) :   I n t e g e r ;  
     M y Q t d ,   Q t d e :   D o u b l e ;  
 b e g i n  
     R e s u l t   : =   T r u e ;  
     V S _ E F D _ I C M S _ I P I . R e o p e n V S O r i P Q x ( Q r O r i ,   M o v i m C o d ,   S Q L _ P e r i o d o P Q ,   F a l s e ) ;  
     / /  
     / / G e r a l . M B _ S Q L ( S e l f ,   Q r O r i ) ;  
     Q r O r i . F i r s t ;  
     w h i l e   n o t   Q r O r i . E o f   d o  
     b e g i n  
         C o d i g o         : =   Q r O r i . F i e l d B y N a m e ( ' O r i g e m C o d i ' ) . A s I n t e g e r ;  
         C o n t r o l e     : =   Q r O r i . F i e l d B y N a m e ( ' O r i g e m C t r l ' ) . A s I n t e g e r ;  
         G r a G r u X       : =   U n P Q x . O b t e m G G X d e I n s u m o ( Q r O r i . F i e l d B y N a m e ( ' I n s u m o ' ) . A s I n t e g e r ) ;  
         M y Q t d           : =   Q r O r i . F i e l d B y N a m e ( ' P e s o ' ) . A s F l o a t ;  
         Q t d e             : =   M y Q t d   *   F a t o r ;  
         D t M o v i m       : =   Q r O r i . F i e l d B y N a m e ( ' D a t a X ' ) . A s D a t e T i m e ;  
         D t C o r r A p o   : =   Q r O r i . F i e l d B y N a m e ( ' D t C o r r A p o ' ) . A s D a t e T i m e ;  
         / /  
         i f   Q t d e   <   0   t h e n  
             G e r a l . M B _ E R R O ( ' Q u a n t i d a d e   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 2 1 ) '   +   s L i n e B r e a k   +  
             ' M o v i m N i v :   '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v ) )   +   s L i n e B r e a k   +  
             s p r o c N a m e   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
             / / ' G r a n d e z a :   '   +   s G r a n d e z a U n i d M e d [ Q r O r i . F i e l d B y N a m e ( ' G r a n d e z a ' ) . A s I n t e g e r ]   +   s L i n e B r e a k   +  
             ' I D   I t e m :   '   +   G e r a l . F F 0 ( C o n t r o l e )   +   s L i n e B r e a k   +  
             ' R e d u z i d o :   '   +   G e r a l . F F 0 ( G r a G r u X )   +   s L i n e B r e a k   +  
             ' P e s o :   '   +   G e r a l . F F T ( M y Q t d ,   3 ,   s i N e g a t i v o )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
             ' Q t d e :   '   +   G e r a l . F F T ( M y Q t d ,   3 ,   s i N e g a t i v o )   +   '   *   '   +  
             G e r a l . F F T ( F a t o r ,   3 ,   s i N e g a t i v o )   +   '   =   '   +  
             G e r a l . F F T ( Q t d e ,   3 ,   s i N e g a t i v o )   +   s L i n e B r e a k   +  
             s p r o c N a m e   +   s L i n e B r e a k   +  
             s L i n e B r e a k   +   ' A v i s e   a   D E R M A T E K ! ! ! ' ) ;  
         i f   Q t d e   >   0   t h e n  
         b e g i n  
             c a s e   T i p o R e g S P E D P r o d   o f  
                 t r s p 2 9 X :  
                 b e g i n  
                     / /   K 2 9 2   -   I n s u m o s   c o n s u m i d o s  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 9 2 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,   P a i M o v I D ,  
                     M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,   D t M o v i m ,   D t C o r r A p o ,   G r a G r u X ,   Q t d e ,  
                     I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,  
                     E n t i S i t i o ,   O r i g e m O p e P r o c ,   O r i E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,  
                     T E s t q S P E D T a b S o r c . e s t s P Q X ,   M e A v i s o ) ;  
                 e n d ;  
                 t r s p 3 0 X :  
                 b e g i n  
                     / /   K 3 0 2   -   I n s u m o s   c o n s u m i d o s  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 3 0 2 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,   P a i M o v I D ,  
                     M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,   D t M o v i m ,   D t C o r r A p o ,   G r a G r u X ,   Q t d e ,  
                     I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,  
                     E n t i S i t i o ,   O r i g e m O p e P r o c ,   O r i E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,  
                     T E s t q S P E D T a b S o r c . e s t s P Q X ,   M e A v i s o ) ;  
                 e n d ;  
                 e l s e  
                     G e r a l . M B _ E r r o ( ' T i p o   d e   r e g i s t r o   S P E D   E F D   ( '   +   G e r a l . F F 0 ( I n t e g e r (  
                     T i p o R e g S P E D P r o d ) )   +   ' )   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e   +   ' ( 2 1 ) ' ) ;  
             e n d ;  
         e n d ;  
         / /  
         Q r O r i . N e x t ;  
     e n d ;  
 e n d ;  
  
 f u n c t i o n   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I n s e r e B _ O r i g e P r o c P Q _ I s o l a d a (  
     T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;   M o v i m I D :   T E s t q M o v i m I D ;  
     M o v i m N i v :   T E s t q M o v i m N i v ;   M o v i m C o d ,   I D S e q 1 :   I n t e g e r ;   S Q L _ P e r i o d o P Q :   S t r i n g ;  
     O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;   C l i e n t M O ,   F o r n e c M O ,  
     E n t i S i t i o :   I n t e g e r ;   D t H r A b e r t o :   T D a t e T i m e ) :   B o o l e a n ;  
 c o n s t  
     F a t o r   =   - 1 ;  
     O r i E S T S T a b S o r c   =   e s t s P Q x ;  
     s P r o c N a m e   =   ' F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I n s e r e B _ O r i g e P r o c P Q _ C o n j u n t a ( ) ' ;  
     J m p M o v I D   =   0 ;  
     J m p M o v C o d   =   0 ;  
     J m p N i v e l 1   =   0 ;  
     J m p N i v e l 2   =   0 ;  
     P a i M o v I D   =   0 ;  
     M o v C o d P a i   =   0 ;  
     P a i N i v e l 1   =   0 ;  
     P a i N i v e l 2   =   0 ;  
     C O D _ I N S _ S U B S T   =   ' ' ;  
 v a r  
     D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;  
     C o d i g o ,   C o n t r o l e ,   G r a G r u X ( * ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o * ) :   I n t e g e r ;  
     M y Q t d ,   Q t d e :   D o u b l e ;  
 b e g i n  
     R e s u l t   : =   T r u e ;  
     V S _ E F D _ I C M S _ I P I . R e o p e n V S O r i P Q x ( Q r O r i ,   M o v i m C o d ,   S Q L _ P e r i o d o P Q ,   F a l s e ) ;  
     / /  
     / / G e r a l . M B _ S Q L ( S e l f ,   Q r O r i ) ;  
     Q r O r i . F i r s t ;  
     w h i l e   n o t   Q r O r i . E o f   d o  
     b e g i n  
         C o d i g o         : =   Q r O r i . F i e l d B y N a m e ( ' O r i g e m C o d i ' ) . A s I n t e g e r ;  
         C o n t r o l e     : =   Q r O r i . F i e l d B y N a m e ( ' O r i g e m C t r l ' ) . A s I n t e g e r ;  
         G r a G r u X       : =   U n P Q x . O b t e m G G X d e I n s u m o ( Q r O r i . F i e l d B y N a m e ( ' I n s u m o ' ) . A s I n t e g e r ) ;  
         M y Q t d           : =   Q r O r i . F i e l d B y N a m e ( ' P e s o ' ) . A s F l o a t ;  
         Q t d e             : =   M y Q t d   *   F a t o r ;  
         D t M o v i m       : =   Q r O r i . F i e l d B y N a m e ( ' D a t a X ' ) . A s D a t e T i m e ;  
         D t C o r r A p o   : =   Q r O r i . F i e l d B y N a m e ( ' D t C o r r A p o ' ) . A s D a t e T i m e ;  
         / /  
         i f   Q t d e   <   0   t h e n  
             G e r a l . M B _ E R R O ( ' Q u a n t i d a d e   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 2 1 ) '   +   s L i n e B r e a k   +  
             ' M o v i m N i v :   '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v ) )   +   s L i n e B r e a k   +  
             s p r o c N a m e   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
             / / ' G r a n d e z a :   '   +   s G r a n d e z a U n i d M e d [ Q r O r i . F i e l d B y N a m e ( ' G r a n d e z a ' ) . A s I n t e g e r ]   +   s L i n e B r e a k   +  
             ' I D   I t e m :   '   +   G e r a l . F F 0 ( C o n t r o l e )   +   s L i n e B r e a k   +  
             ' R e d u z i d o :   '   +   G e r a l . F F 0 ( G r a G r u X )   +   s L i n e B r e a k   +  
             ' P e s o :   '   +   G e r a l . F F T ( M y Q t d ,   3 ,   s i N e g a t i v o )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
             ' Q t d e :   '   +   G e r a l . F F T ( M y Q t d ,   3 ,   s i N e g a t i v o )   +   '   *   '   +  
             G e r a l . F F T ( F a t o r ,   3 ,   s i N e g a t i v o )   +   '   =   '   +  
             G e r a l . F F T ( Q t d e ,   3 ,   s i N e g a t i v o )   +   s L i n e B r e a k   +  
             s p r o c N a m e   +   s L i n e B r e a k   +  
             s L i n e B r e a k   +   ' A v i s e   a   D E R M A T E K ! ! ! ' ) ;  
         i f   Q t d e   >   0   t h e n  
         b e g i n  
             c a s e   T i p o R e g S P E D P r o d   o f  
                 t r s p 2 3 X :  
                 b e g i n  
 ( *  
                     / /   K 2 9 2   -   I n s u m o s   c o n s u m i d o s  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 9 2 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,   P a i M o v I D ,  
                     M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,   D t M o v i m ,   D t C o r r A p o ,   G r a G r u X ,   Q t d e ,  
                     I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,  
                     E n t i S i t i o ,   O r i g e m O p e P r o c ,   O r i E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,  
                     T E s t q S P E D T a b S o r c . e s t s P Q X ,   M e A v i s o ) ;  
 * )  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 ,   D t H r A b e r t o ,  
                     D t C o r r A p o ,   G r a G r u X ,   Q t d e ,   C O D _ I N S _ S U B S T ,   C o n t r o l e ,   I n t e g e r ( M o v i m I D ) ,  
                     C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,  
                     O r i g e m O p e P r o c ,   O r i E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                 e n d ;  
                 t r s p 2 5 X :  
                 b e g i n  
 ( *  
                     / /   K 3 0 2   -   I n s u m o s   c o n s u m i d o s  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 3 0 2 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,   P a i M o v I D ,  
                     M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,   D t M o v i m ,   D t C o r r A p o ,   G r a G r u X ,   Q t d e ,  
                     I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,  
                     E n t i S i t i o ,   O r i g e m O p e P r o c ,   O r i E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,  
                     T E s t q S P E D T a b S o r c . e s t s P Q X ,   M e A v i s o ) ;  
 * )  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 ,   D t H r A b e r t o ,   D t C o r r A p o ,   G r a G r u X ,   Q t d e ,   C O D _ I N S _ S U B S T ,  
                     C o n t r o l e ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,  
                     F o r n e c M O ,   E n t i S i t i o ,   O r i g e m O p e P r o c ,   O r i E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,  
                     M e A v i s o ) ;  
                 e n d ;  
                 e l s e  
                     G e r a l . M B _ E r r o ( ' T i p o   d e   r e g i s t r o   S P E D   E F D   ( '   +   G e r a l . F F 0 ( I n t e g e r (  
                     T i p o R e g S P E D P r o d ) )   +   ' )   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e   +   ' ( 2 1 ) ' ) ;  
             e n d ;  
         e n d ;  
         / /  
         Q r O r i . N e x t ;  
     e n d ;  
 e n d ;  
  
 p r o c e d u r e   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I n s e r e B _ P r o d u c a o C o n j u n t a ( T a b C a b :  
     S t r i n g ;   T a r g e t M o v i m I D ,   P s q M o v i m I D _ V M I ,   P s q M o v i m I D _ P Q X :   T E s t q M o v i m I D ;   P s q M o v i m N i v :  
       T E s t q M o v i m N i v ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;   F o r m a I n s e r c a o I t e m P r o d u c a o :  
       T F o r m a I n s e r c a o I t e m P r o d u c a o ) ;  
 c o n s t  
     s P r o c N a m e   =   ' F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I n s e r e B _ P r o d u c a o C o n j u n t a ( ) ' ;  
     D s t F a t o r     =   1 ;  
     O r i F a t o r     =   1 ;  
 v a r  
     I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 :   I n t e g e r ;  
     T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;  
     / /  
     M o v i m I D :   T E s t q M o v i m I D ;  
     C o d i g o ,   M o v i m C o d ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   C t r l D s t ,   G G X D s t ,   T i p o E s t q D s t ,  
     T i p o E s t q O r i ,   C t r l O r i ,   G G X O r i ,   C o d D o c O P :   I n t e g e r ;  
     D t H r A b e r t o ,   D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;  
     A r e a M 2 ,   P e s o K g ,   P e c a s ,   Q t d e D s t ,   Q t d e O r i :   D o u b l e ;  
     O r i E S T S T a b S o r c :   T E s t q S P E D T a b S o r c ;  
     / /  
     M o v i m N i v :   T E s t q M o v i m N i v ;  
     M o v i m T w n ,   C t r l P s q ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,   J m p N i v e l 2 ,   P a i M o v I D ,  
     M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,   E m p r e s a ,   F o r n e c e M O ,   C o n t r o l e ,   G r a G r u X ,  
     G e r M o v I D ,   G e r N i v e l 1 ,   G e r N i v e l 2 ,   G e r G G X :   I n t e g e r ;  
     I n s e r i u P Q :   B o o l e a n ;  
     Q t d G e r P e c a ,   Q t d G e r P e s o ,   Q t d G e r A r M 2 ,   Q t d A n t P e c a ,   Q t d A n t P e s o ,   Q t d A n t A r M 2 :   D o u b l e ;  
     I n s e r c a o I t e m P r o d u c a o C o n j u n t a :   T I n s e r c a o I t e m P r o d u c a o C o n j u n t a ;  
     / /  
     p r o c e d u r e   A v i s o F i p ( P r o c N o m e :   S t r i n g ) ;  
     b e g i n  
         G e r a l . M B _ A v i s o ( ' F o r m a I n s e r c a o I t e m P r o d u c a o   n � o   d e f i n i d a   e m   '   +   s P r o c N a m e   +  
         ' . '   +   P r o c N o m e ) ;  
     e n d ;  
 b e g i n  
     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r F a t h e r ,   D M o d G . M y P I D _ D B ,   [  
     ' S E L E C T   D I S T I N C T   M o v C o d P a i   ' ,  
     ' F R O M     '   +   F S E I I _ I M E c s ,  
     ' O R D E R   B Y   M o v C o d P a i ' ,  
     ' ' ] ) ;  
     / / G e r a l . M B _ S Q L ( S e l f ,   Q r F a t h e r ) ;  
     P B 2 . P o s i t i o n   : =   0 ;  
     P B 2 . M a x   : =   Q r F a t h e r . R e c o r d C o u n t ;  
     Q r F a t h e r . F i r s t ;  
     w h i l e   n o t   Q r F a t h e r . E o f   d o  
     b e g i n  
         M y O b j e c t s . U p d P B ( P B 2 ,   L a A v i s o 3 ,   L a A v i s o 4 ) ;  
         / /  
         I n s e r i u P Q   : =   F a l s e ;  
         I D S e q 1 _ K 2 9 0   : =   - 1 ;  
         I D S e q 1 _ K 3 0 0   : =   - 1 ;  
         / /  
         c a s e   T a r g e t M o v i m I D   o f  
             T E S t q M o v i m I D . e m i d E m O p e r a c a o ,  
             T E S t q M o v i m I D . e m i d E m P r o c W E ,  
             T E S t q M o v i m I D . e m i d E m P r o c S P ,  
             T E S t q M o v i m I D . e m i d E m R e p r R M :  
             b e g i n  
                 U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r I M E C s ,   D M o d G . M y P I D _ D B ,   [  
                 ' S E L E C T   *   ' ,  
                 ' F R O M     '   +   F S E I I _ I M E c s ,  
                 ' W H E R E   M o v C o d P a i = '   +   G e r a l . F F 0 ( Q r F a t h e r M o v C o d P a i . V a l u e ) ,  
                 / / ' O R D E R   B Y   '  
                 ' L I M I T   1   ' ,  
                 ' ' ] ) ;  
             e n d ;  
             T E S t q M o v i m I D . e m i d E m P r o c C a l ,  
             T E S t q M o v i m I D . e m i d C a l e a d o ,  
             T E S t q M o v i m I D . e m i d E m P r o c C u r ,  
             T E S t q M o v i m I D . e m i d C u r t i d o :  
             b e g i n  
                 U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r I M E C s ,   D M o d G . M y P I D _ D B ,   [  
                 ' S E L E C T   *   ' ,  
                 ' F R O M     '   +   F S E I I _ I M E c s ,  
                 ' W H E R E   M o v C o d P a i = '   +   G e r a l . F F 0 ( Q r F a t h e r M o v C o d P a i . V a l u e ) ,  
                 ' ' ] ) ;  
             e n d ;  
             e l s e   b e g i n  
                 G e r a l . M B _ E r r o (  
                 G e t E n u m N a m e ( T y p e I n f o ( T E S t q M o v i m I D ) ,   I n t e g e r ( T a r g e t M o v i m I D ) )   +  
                 '   i n d e f i n i d o   e m   '   +   s p r o c N a m e   +   '   ( 1 ) ' ) ;  
                 / /  
                 U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r I M E C s ,   D M o d G . M y P I D _ D B ,   [  
                 ' S E L E C T   *   ' ,  
                 ' F R O M     '   +   F S E I I _ I M E c s ,  
                 ' W H E R E   M o v C o d P a i = '   +   G e r a l . F F 0 ( Q r F a t h e r M o v C o d P a i . V a l u e ) ,  
                 ' ' ] ) ;  
             e n d ;  
         e n d ;  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q r I M E C s ) ;  
         w h i l e   n o t   Q r I M E C s . E o f   d o  
         b e g i n  
             I n s e r c a o I t e m P r o d u c a o C o n j u n t a   : =   i i p c I n d e f ;  
             C o d D o c O P     : =   Q r I M E C s M o v C o d P a i . V a l u e ;  
             U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r P r o c ,   D m o d . M y D B ,   [  
             ' S E L E C T   D t H r A b e r t o ,   D t H r F i m O p e   ' ,  
             ' F R O M   '   +   L o w e r C a s e ( T a b C a b ) ,  
             ' W H E R E   M o v i m C o d = '   +   G e r a l . F F 0 ( C o d D o c O P ) ,  
             ' ' ] ) ;  
             / /  
             / / c a s e   P s q M o v i m I D   o f  
             c a s e   T a r g e t M o v i m I D   o f  
                 T E S t q M o v i m I D . e m i d E m P r o c C a l :  
                 b e g i n  
                     J m p M o v I D     : =   0 ;  
                     J m p M o v C o d   : =   0 ;  
                     J m p N i v e l 1   : =   0 ;  
                     J m p N i v e l 2   : =   0 ;  
                     P a i M o v I D     : =   Q r I M E C s M o v i m I D . V a l u e ;  
                     M o v C o d P a i   : =   Q r I M E C s M o v C o d P a i . V a l u e ;  
                     P a i N i v e l 1   : =   Q r I M E C s C o d i g o . V a l u e ;  
                     P a i N i v e l 2   : =   0 ;  
                     / /  
                     V S _ E F D _ I C M S _ I P I . R e o p e n V S R i b I t s s ( Q r V S R i b A t u ,   Q r V S R i b O r i I M E I ,  
                     ( * Q r V S R i b O r i P a l l e t * ) n i l ,   ( * Q r V S R i b D s t * ) n i l ,   ( * Q r V S R i b I n d * ) n i l ,  
                     ( * Q r S u b P r d * ) n i l ,   ( * Q r D e s c l * ) n i l ,   ( * Q r F o r c a d o s * ) n i l ,  
                     ( * Q r E m i t * ) n i l ,   ( * Q r P Q O * ) n i l ,   ( * Q r I M E C s C o d i g o . V a l u e * ) P a i N i v e l 1 ,  
                     ( * Q r I M E C s M o v i m C o d . V a l u e * )   M o v C o d P a i ,   ( * Q r I M E C s T e m I M E I M r t . V a l u e * ) 1 ,  
                     / / M o v I D E m P r o c ,   M o v I D P r o n t o ,   M o v I D I n d i r e t o ,  
                     e m i d E m P r o c C a l ,   e m i d C a l e a d o ,   e m i d C a l e a d o ,  
                     / / M o v N i v I n n ,   M o v N i v S r c ,   M o v N i v D s t :  
                     e m i n E m C a l I n n ,   e m i n S o r c C a l ,   e m i n D e s t C a l ) ;  
                     I n s e r c a o I t e m P r o d u c a o C o n j u n t a   : =   i i p c U n i A n t ;  
                 e n d ;  
                 T E S t q M o v i m I D . e m i d C a l e a d o :  
                 b e g i n  
                     / /     C o u r o   c a l e a d o   t i r a d o   d i r e t o   d o   c a l e i r o  
                     i f   Q r I M E C s J m p N i v e l 2 . V a l u e   =   0   t h e n  
                     b e g i n  
                         J m p M o v I D     : =   0 ;  
                         J m p M o v C o d   : =   0 ;  
                         J m p N i v e l 1   : =   0 ;  
                         J m p N i v e l 2   : =   0 ;  
                         P a i M o v I D     : =   Q r I M E C s M o v i m I D . V a l u e ;  
                         M o v C o d P a i   : =   Q r I M E C s M o v C o d P a i . V a l u e ;  
                         P a i N i v e l 1   : =   Q r I M E C s C o d i g o . V a l u e ;  
                         P a i N i v e l 2   : =   0 ;  
                         I n s e r c a o I t e m P r o d u c a o C o n j u n t a   : =   i i p c G e m e o s ;  
                     e n d   e l s e  
                     b e g i n  
                         C t r l P s q   : =   Q r I M E C s J m p N i v e l 2 . V a l u e ;  
                         / /  
                         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r J m p A ,   D m o d . M y D B ,   [  
                         ' S E L E C T   *     ' ,  
                         ' F R O M   v s m o v i t s   ' ,  
                         ' W H E R E   C o n t r o l e = '   +   G e r a l . F F 0 ( C t r l P s q ) ,  
                         ' ' ] ) ;  
                         / /  
                         C t r l P s q       : =   Q r J m p A S r c N i v e l 2 . V a l u e ;  
                         J m p M o v I D     : =   Q r J m p A M o v i m I D . V a l u e ;  
                         J m p M o v C o d   : =   Q r J m p A M o v i m C o d . V a l u e ;  
                         J m p N i v e l 1   : =   Q r J m p A C o d i g o . V a l u e ;  
                         J m p N i v e l 2   : =   Q r J m p A C o n t r o l e . V a l u e ;  
                         / /  
                         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r J m p B ,   D m o d . M y D B ,   [  
                         ' S E L E C T   *     ' ,  
                         ' F R O M   v s m o v i t s   ' ,  
                         ' W H E R E   C o n t r o l e = '   +   G e r a l . F F 0 ( C t r l P s q ) ,  
                         ' ' ] ) ;  
                         / /  
                         P a i M o v I D     : =   Q r J m p B M o v i m I D . V a l u e ;  
                         M o v C o d P a i   : =   Q r J m p B M o v i m C o d . V a l u e ;  
                         P a i N i v e l 1   : =   Q r J m p B C o d i g o . V a l u e ;  
                         P a i N i v e l 2   : =   Q r J m p B C o n t r o l e . V a l u e ;  
                         I n s e r c a o I t e m P r o d u c a o C o n j u n t a   : =   i i p c I n d i r e t a ;  
                     e n d ;  
                     / /  
                     V S _ E F D _ I C M S _ I P I . R e o p e n V S R i b I t s s ( Q r V S R i b A t u ,   ( * Q r V S R i b O r i I M E I * ) n i l ,  
                     ( * Q r V S R i b O r i P a l l e t * ) n i l ,   Q r V S R i b D s t ,   Q r V S R i b I n d ,  
                     ( * Q r S u b P r d * ) n i l ,   ( * Q r D e s c l * ) n i l ,   ( * Q r F o r c a d o s * ) n i l ,  
                     ( * Q r E m i t * ) n i l ,   ( * Q r P Q O * ) n i l ,   ( * Q r I M E C s C o d i g o . V a l u e * ) P a i N i v e l 1 ,  
                     ( * Q r I M E C s M o v i m C o d . V a l u e * )   M o v C o d P a i ,   ( * Q r I M E C s T e m I M E I M r t . V a l u e * ) 1 ,  
                     / / M o v I D E m P r o c ,   M o v I D P r o n t o ,   M o v I D I n d i r e t o ,  
                     e m i d E m P r o c C a l ,   e m i d C a l e a d o ,   e m i d C a l e a d o ,  
                     / / M o v N i v I n n ,   M o v N i v S r c ,   M o v N i v D s t :  
                     e m i n E m C a l I n n ,   e m i n S o r c C a l ,   e m i n D e s t C a l ) ;  
                 e n d ;  
                 T E S t q M o v i m I D . e m i d E m P r o c C u r :  
                 b e g i n  
                     J m p M o v I D     : =   0 ;  
                     J m p M o v C o d   : =   0 ;  
                     J m p N i v e l 1   : =   0 ;  
                     J m p N i v e l 2   : =   0 ;  
                     P a i M o v I D     : =   Q r I M E C s M o v i m I D . V a l u e ;  
                     M o v C o d P a i   : =   Q r I M E C s M o v C o d P a i . V a l u e ;  
                     P a i N i v e l 1   : =   Q r I M E C s C o d i g o . V a l u e ;  
                     P a i N i v e l 2   : =   0 ;  
                     / /  
                     V S _ E F D _ I C M S _ I P I . R e o p e n V S R i b I t s s ( Q r V S R i b A t u ,   Q r V S R i b O r i I M E I ,  
                     ( * Q r V S R i b O r i P a l l e t * ) n i l ,   ( * Q r V S R i b D s t * ) n i l ,   ( * Q r V S R i b I n d * ) n i l ,  
                     ( * Q r S u b P r d * ) n i l ,   ( * Q r D e s c l * ) n i l ,   ( * Q r F o r c a d o s * ) n i l ,  
                     ( * Q r E m i t * ) n i l ,   ( * Q r P Q O * ) n i l ,   ( * Q r I M E C s C o d i g o . V a l u e * ) P a i N i v e l 1 ,  
                     ( * Q r I M E C s M o v i m C o d . V a l u e * )   M o v C o d P a i ,   ( * Q r I M E C s T e m I M E I M r t . V a l u e * ) 1 ,  
                     / / M o v I D E m P r o c ,   M o v I D P r o n t o ,   M o v I D I n d i r e t o ,  
                     e m i d E m P r o c C u r ,   e m i d I n d s V S ,   e m i d E m P r o c C u r ,  
                     / / M o v N i v I n n ,   M o v N i v S r c ,   M o v N i v D s t :  
                     e m i n E m C u r I n n ,   e m i n S o r c C u r ,   e m i n D e s t C u r ) ;  
                     I n s e r c a o I t e m P r o d u c a o C o n j u n t a   : =   i i p c U n i A n t ;  
                 e n d ;  
                 T E S t q M o v i m I D . e m i d C u r t i d o :  
                 b e g i n  
                     J m p M o v I D     : =   0 ;  
                     J m p M o v C o d   : =   0 ;  
                     J m p N i v e l 1   : =   0 ;  
                     J m p N i v e l 2   : =   0 ;  
                     P a i M o v I D     : =   Q r I M E C s M o v i m I D . V a l u e ;  
                     M o v C o d P a i   : =   Q r I M E C s M o v C o d P a i . V a l u e ;  
                     P a i N i v e l 1   : =   Q r I M E C s C o d i g o . V a l u e ;  
                     P a i N i v e l 2   : =   0 ;  
                     / /  
                     V S _ E F D _ I C M S _ I P I . R e o p e n V S R i b I t s s ( Q r V S R i b A t u ,   ( * Q r V S R i b O r i I M E I * ) n i l ,  
                     ( * Q r V S R i b O r i P a l l e t * ) n i l ,   Q r V S R i b D s t ,   Q r V S R i b I n d ,  
                     ( * Q r S u b P r d * ) n i l ,   ( * Q r D e s c l * ) n i l ,   ( * Q r F o r c a d o s * ) n i l ,  
                     ( * Q r E m i t * ) n i l ,   ( * Q r P Q O * ) n i l ,   ( * Q r I M E C s C o d i g o . V a l u e * ) P a i N i v e l 1 ,  
                     ( * Q r I M E C s M o v i m C o d . V a l u e * )   M o v C o d P a i ,   ( * Q r I M E C s T e m I M E I M r t . V a l u e * ) 1 ,  
                     / / M o v I D E m P r o c ,   M o v I D P r o n t o ,   M o v I D I n d i r e t o  
                     e m i d E m P r o c C u r ,   e m i d I n d s V S ,   e m i d E m P r o c C u r ,  
                     / / M o v N i v I n n ,   M o v N i v S r c ,   M o v N i v D s t :  
                     e m i n E m C u r I n n ,   e m i n S o r c C u r ,   e m i n D e s t C u r ) ;  
                     I n s e r c a o I t e m P r o d u c a o C o n j u n t a   : =   i i p c U n i G e r ;  
                 e n d ;  
                 T E S t q M o v i m I D . e m i d E m O p e r a c a o :  
                 b e g i n  
                     J m p M o v I D     : =   0 ;  
                     J m p M o v C o d   : =   0 ;  
                     J m p N i v e l 1   : =   0 ;  
                     J m p N i v e l 2   : =   0 ;  
                     P a i M o v I D     : =   Q r I M E C s M o v i m I D . V a l u e ;  
                     M o v C o d P a i   : =   Q r I M E C s M o v C o d P a i . V a l u e ;  
                     P a i N i v e l 1   : =   Q r I M E C s C o d i g o . V a l u e ;  
                     P a i N i v e l 2   : =   0 ;  
                     / /  
                     V S _ E F D _ I C M S _ I P I . R e o p e n V S R i b I t s s ( Q r V S R i b A t u ,   ( * Q r V S R i b O r i I M E I * ) n i l ,  
                     ( * Q r V S R i b O r i P a l l e t * ) n i l ,   Q r V S R i b D s t ,   ( * Q r V S R i b I n d * ) n i l ,  
                     ( * Q r S u b P r d * ) n i l ,   ( * Q r D e s c l * ) n i l ,   ( * Q r F o r c a d o s * ) n i l ,  
                     ( * Q r E m i t * ) n i l ,   ( * Q r P Q O * ) n i l ,   ( * Q r V S R i b C a b C o d i g o . V a l u e * ) P a i N i v e l 1 ,  
                     ( * Q r V S R i b C a b M o v i m C o d . V a l u e * ) M o v C o d P a i , ( * Q r V S R i b C a b T e m I M E I M r t . V a l u e * ) 1 ,  
                     / / M o v I D E m P r o c ,   M o v I D P r o n t o ,   M o v I D I n d i r e t o  
                     e m i d E m O p e r a c a o ,   e m i d E m O p e r a c a o ,   e m i d E m O p e r a c a o ,  
                     / / M o v N i v I n n ,   M o v N i v S r c ,   M o v N i v D s t :  
                     e m i n E m O p e r I n n ,   e m i n S o r c O p e r ,   e m i n D e s t O p e r ) ;  
                     I n s e r c a o I t e m P r o d u c a o C o n j u n t a   : =   i i p c G e m e o s ;  
                 e n d ;  
                 T E S t q M o v i m I D . e m i d E m P r o c W E :  
                 b e g i n  
                     J m p M o v I D     : =   0 ;  
                     J m p M o v C o d   : =   0 ;  
                     J m p N i v e l 1   : =   0 ;  
                     J m p N i v e l 2   : =   0 ;  
                     P a i M o v I D     : =   Q r I M E C s M o v i m I D . V a l u e ;  
                     M o v C o d P a i   : =   Q r I M E C s M o v C o d P a i . V a l u e ;  
                     P a i N i v e l 1   : =   Q r I M E C s C o d i g o . V a l u e ;  
                     P a i N i v e l 2   : =   0 ;  
                     / /  
                     V S _ E F D _ I C M S _ I P I . R e o p e n V S R i b I t s s ( Q r V S R i b A t u ,   ( * Q r V S R i b O r i I M E I * ) n i l ,  
                     ( * Q r V S R i b O r i P a l l e t * ) n i l ,   Q r V S R i b D s t ,   ( * Q r V S R i b I n d * ) n i l ,  
                     ( * Q r S u b P r d * ) n i l ,   ( * Q r D e s c l * ) n i l ,   ( * Q r F o r c a d o s * ) n i l ,  
                     ( * Q r E m i t * ) n i l ,   ( * Q r P Q O * ) n i l ,   ( * Q r V S R i b C a b C o d i g o . V a l u e * ) P a i N i v e l 1 ,  
                     ( * Q r V S R i b C a b M o v i m C o d . V a l u e * ) M o v C o d P a i , ( * Q r V S R i b C a b T e m I M E I M r t . V a l u e * ) 1 ,  
                     / / M o v I D E m P r o c ,   M o v I D P r o n t o ,   M o v I D I n d i r e t o  
                     e m i d E m P r o c W E ,   e m i d F i n i s h e d ,   e m i d E m P r o c W E ,  
                     / / M o v N i v I n n ,   M o v N i v S r c ,   M o v N i v D s t :  
                     e m i n E m W E n d I n n ,   e m i n S o r c W E n d ,   e m i n D e s t W E n d ) ;  
                     I n s e r c a o I t e m P r o d u c a o C o n j u n t a   : =   i i p c G e m e o s ;  
                 e n d ;  
                 T E S t q M o v i m I D . e m i d E m P r o c S P :  
                 b e g i n  
                     J m p M o v I D     : =   0 ;  
                     J m p M o v C o d   : =   0 ;  
                     J m p N i v e l 1   : =   0 ;  
                     J m p N i v e l 2   : =   0 ;  
                     P a i M o v I D     : =   Q r I M E C s M o v i m I D . V a l u e ;  
                     M o v C o d P a i   : =   Q r I M E C s M o v C o d P a i . V a l u e ;  
                     P a i N i v e l 1   : =   Q r I M E C s C o d i g o . V a l u e ;  
                     P a i N i v e l 2   : =   0 ;  
                     / /  
                     V S _ E F D _ I C M S _ I P I . R e o p e n V S R i b I t s s ( Q r V S R i b A t u ,   ( * Q r V S R i b O r i I M E I * ) n i l ,  
                     ( * Q r V S R i b O r i P a l l e t * ) n i l ,   Q r V S R i b D s t ,   ( * Q r V S R i b I n d * ) n i l ,  
                     ( * Q r S u b P r d * ) n i l ,   ( * Q r D e s c l * ) n i l ,   ( * Q r F o r c a d o s * ) n i l ,  
                     ( * Q r E m i t * ) n i l ,   ( * Q r P Q O * ) n i l ,   ( * Q r V S R i b C a b C o d i g o . V a l u e * ) P a i N i v e l 1 ,  
                     ( * Q r V S R i b C a b M o v i m C o d . V a l u e * ) M o v C o d P a i , ( * Q r V S R i b C a b T e m I M E I M r t . V a l u e * ) 1 ,  
                     / / M o v I D E m P r o c ,   M o v I D P r o n t o ,   M o v I D I n d i r e t o  
                     e m i d E m P r o c S P ,   e m i d E m P r o c S P ,   e m i d E m P r o c S P ,  
                     / / M o v N i v I n n ,   M o v N i v S r c ,   M o v N i v D s t :  
                     e m i n E m P S P I n n ,   e m i n S o r c P S P ,   e m i n D e s t P S P ) ;  
                     I n s e r c a o I t e m P r o d u c a o C o n j u n t a   : =   i i p c G e m e o s ;  
                 e n d ;  
                 T E S t q M o v i m I D . e m i d E m R e p r R M :  
                 b e g i n  
                     J m p M o v I D     : =   0 ;  
                     J m p M o v C o d   : =   0 ;  
                     J m p N i v e l 1   : =   0 ;  
                     J m p N i v e l 2   : =   0 ;  
                     P a i M o v I D     : =   Q r I M E C s M o v i m I D . V a l u e ;  
                     M o v C o d P a i   : =   Q r I M E C s M o v C o d P a i . V a l u e ;  
                     P a i N i v e l 1   : =   Q r I M E C s C o d i g o . V a l u e ;  
                     P a i N i v e l 2   : =   0 ;  
                     / /  
                     V S _ E F D _ I C M S _ I P I . R e o p e n V S R i b I t s s ( Q r V S R i b A t u ,   ( * Q r V S R i b O r i I M E I * ) n i l ,  
                     ( * Q r V S R i b O r i P a l l e t * ) n i l ,   Q r V S R i b D s t ,   ( * Q r V S R i b I n d * ) n i l ,  
                     ( * Q r S u b P r d * ) n i l ,   ( * Q r D e s c l * ) n i l ,   ( * Q r F o r c a d o s * ) n i l ,  
                     ( * Q r E m i t * ) n i l ,   ( * Q r P Q O * ) n i l ,   ( * Q r V S R i b C a b C o d i g o . V a l u e * ) P a i N i v e l 1 ,  
                     ( * Q r V S R i b C a b M o v i m C o d . V a l u e * ) M o v C o d P a i , ( * Q r V S R i b C a b T e m I M E I M r t . V a l u e * ) 1 ,  
                     / / M o v I D E m P r o c ,   M o v I D P r o n t o ,   M o v I D I n d i r e t o  
                     e m i d E m R e p r R M ,   e m i d E m R e p r R M ,   e m i d E m R e p r R M ,  
                     / / M o v N i v I n n ,   M o v N i v S r c ,   M o v N i v D s t :  
                     e m i n E m R R M I n n ,   e m i n S o r c R R M ,   e m i n D e s t R R M ) ;  
                     I n s e r c a o I t e m P r o d u c a o C o n j u n t a   : =   i i p c G e m e o s ;  
                 e n d ;  
                 e l s e   G e r a l . M B _ E r r o ( ' M o v i m I D   '   +   G e r a l . F F 0 ( I n t e g e r ( T a r g e t M o v i m I D ) )   +  
                 '   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e   +   '   ( 2 ) ' ) ;  
             e n d ;  
             / /  
             / / G e r a l . M B _ S Q L ( S e l f ,   Q r V S R i b D s t ) ;  
             c a s e   I n s e r c a o I t e m P r o d u c a o C o n j u n t a   o f  
                 / / i i p c I n d e f = 0 ,  
                 i i p c G e m e o s :  
                 b e g i n  
                     Q r V S R i b D s t . F i r s t ;  
                     w h i l e   n o t   Q r V S R i b D s t . E o f   d o  
                     b e g i n  
                         i f   ( Q r V S R i b D s t D a t a H o r a . V a l u e   > =   F D i a I n i )  
                         a n d   ( Q r V S R i b D s t D a t a H o r a . V a l u e   <   F D i a P o s )   t h e n  
                         b e g i n  
                             M o v i m I D           : =   T E s t q M o v i m I D ( Q r V S R i b D s t M o v i m I D . V a l u e ) ;  
                             M o v i m C o d         : =   Q r V S R i b D s t M o v i m C o d . V a l u e ;  
                             C o d i g o             : =   Q r V S R i b D s t C o d i g o . V a l u e ;  
                             D t H r A b e r t o     : =   T r u n c ( Q r P r o c D t H r A b e r t o . V a l u e ) ;  
                             D t H r F i m O p e     : =   T r u n c ( Q r P r o c D t H r F i m O p e . V a l u e ) ;  
                             D t M o v i m           : =   T r u n c ( Q r V S R i b D s t D a t a H o r a . V a l u e ) ;  
                             D t C o r r A p o       : =   T r u n c ( Q r V S R i b D s t D t C o r r A p o . V a l u e ) ;  
                             E m p r e s a           : =   Q r V S R i b D s t E m p r e s a . V a l u e ;  
                             F o r n e c e M O       : =   Q r V S R i b D s t F o r n e c M O . V a l u e ;  
                             C o n t r o l e         : =   Q r V S R i b D s t C o n t r o l e . V a l u e ;  
                             G r a G r u X           : =   Q r V S R i b D s t G r a G r u X . V a l u e ;  
                             M o v i m N i v         : =   T E s t q M o v i m N i v ( Q r V S R i b D s t M o v i m N i v . V a l u e ) ;  
                             M o v i m T w n         : =   Q r V S R i b D s t M o v i m T w n . V a l u e ;  
                             A r e a M 2             : =   Q r V S R i b D s t A r e a M 2 . V a l u e ;  
                             P e s o K g             : =   Q r V S R i b D s t P e s o K g . V a l u e ;  
                             P e c a s               : =   Q r V S R i b D s t P e c a s . V a l u e ;  
                             V e r i f i c a S e T e m G r a G r u X ( 2 9 8 6 ,   G r a G r u X ,   s P r o c N a m e ,   Q r V S R i b D s t ) ;  
                             / /  
                             c a s e   F o r m a I n s e r c a o I t e m P r o d u c a o   o f  
                                 f i p I s o l a d a :  
                                     I n s e r e I t e m P r o d u c a o I s o l a d a G e m e o s ( D t H r A b e r t o ,   D t H r F i m O p e ,  
                                     D t M o v i m ,   D t C o r r A p o ,   M o v i m I D ,   E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,  
                                     C o d i g o ,   C o n t r o l e ,   G r a G r u X ,   M o v i m N i v ,   M o v i m T w n ,   A r e a M 2 ,   P e s o K g ,  
                                     P e c a s ,   O r i g e m O p e P r o c ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,  
                                     J m p N i v e l 2 ,   P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,  
                                     I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 ,   I n s e r i u P Q ) ;  
                                 f i p C o n j u n t a :  
                                     I n s e r e I t e m P r o d u c a o C o n j u n t a G e m e o s ( D t H r A b e r t o ,   D t H r F i m O p e ,  
                                     D t M o v i m ,   D t C o r r A p o ,   M o v i m I D ,   E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,  
                                     C o d i g o ,   C o n t r o l e ,   G r a G r u X ,   M o v i m N i v ,   M o v i m T w n ,   A r e a M 2 ,   P e s o K g ,  
                                     P e c a s ,   O r i g e m O p e P r o c ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,  
                                     J m p N i v e l 2 ,   P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,  
                                     I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 ,   I n s e r i u P Q ) ;  
                                 e l s e  
                                     A v i s o F i p ( ' G e m e o s ' ) ;  
                             e n d ;  
                         e n d ;  
                         / /  
                         Q r V S R i b D s t . N e x t ;  
                     e n d ;  
                 e n d ;  
                 i i p c I n d i r e t a :  
                 b e g i n  
                     Q r V S R i b I n d . F i r s t ;  
                     w h i l e   n o t   Q r V S R i b I n d . E o f   d o  
                     b e g i n  
                         i f   ( Q r V S R i b I n d D a t a H o r a . V a l u e   > =   F D i a I n i )  
                         a n d   ( Q r V S R i b I n d D a t a H o r a . V a l u e   <   F D i a P o s )   t h e n  
                         b e g i n  
                             M o v i m I D           : =   T E s t q M o v i m I D ( Q r V S R i b I n d M o v i m I D . V a l u e ) ;  
                             M o v i m C o d         : =   Q r V S R i b I n d M o v i m C o d . V a l u e ;  
                             C o d i g o             : =   Q r V S R i b I n d C o d i g o . V a l u e ;  
                             D t H r A b e r t o     : =   T r u n c ( Q r P r o c D t H r A b e r t o . V a l u e ) ;  
                             D t H r F i m O p e     : =   T r u n c ( Q r P r o c D t H r F i m O p e . V a l u e ) ;  
                             D t M o v i m           : =   T r u n c ( Q r V S R i b I n d D a t a H o r a . V a l u e ) ;  
                             D t C o r r A p o       : =   T r u n c ( Q r V S R i b I n d D t C o r r A p o . V a l u e ) ;  
                             E m p r e s a           : =   Q r V S R i b I n d E m p r e s a . V a l u e ;  
                             F o r n e c e M O       : =   Q r V S R i b I n d F o r n e c M O . V a l u e ;  
                             C o n t r o l e         : =   Q r V S R i b I n d C o n t r o l e . V a l u e ;  
                             G r a G r u X           : =   Q r V S R i b I n d G r a G r u X . V a l u e ;  
                             M o v i m N i v         : =   T E s t q M o v i m N i v ( Q r V S R i b I n d M o v i m N i v . V a l u e ) ;  
                             M o v i m T w n         : =   Q r V S R i b I n d M o v i m T w n . V a l u e ;  
                             A r e a M 2             : =   Q r V S R i b I n d A r e a M 2 . V a l u e ;  
                             P e s o K g             : =   Q r V S R i b I n d P e s o K g . V a l u e ;  
                             P e c a s               : =   Q r V S R i b I n d P e c a s . V a l u e ;  
                             G e r M o v I D         : =   Q r V S R i b I n d S r c M o v I D . V a l u e ;  
                             G e r N i v e l 1       : =   Q r V S R i b I n d S r c N i v e l 1 . V a l u e ;  
                             G e r N i v e l 2       : =   Q r V S R i b I n d S r c N i v e l 2 . V a l u e ;  
                             G e r G G X             : =   Q r V S R i b I n d J m p G G X . V a l u e ;  
                             Q t d G e r P e c a     : =   Q r V S R i b I n d Q t d G e r P e c a . V a l u e ;  
                             Q t d G e r P e s o     : =   Q r V S R i b I n d Q t d G e r P e s o . V a l u e ;  
                             Q t d G e r A r M 2     : =   Q r V S R i b I n d Q t d G e r A r M 2 . V a l u e ;  
                             i f   G e r G G X   =   0   t h e n  
                             b e g i n  
                                 / / G e r a l . M B _ E r r o ( ' M o v C o d P a i   =   '   +   G e r a l . F F 0 ( M o v C o d P a i ) ) ;  
                                 G e r G G X   : =   V S _ P F . O b t e m J m p G G X d e M o v C o d P a i ( M o v C o d P a i ) ;  
                                 i f   M e A v i s o   < >   n i l   t h e n  
                                     M e A v i s o . L i n e s . A d d ( ' " G e r G G X "   o b t i d o   d o   M o v C o d P a i   =   '   +  
                                         G e r a l . F F 0 ( M o v C o d P a i ) ) ;  
 ( *  
                                 i f   M e A v i s o   < >   n i l   t h e n  
                                     M e A v i s o . L i n e s . A d d ( ' " G e r G G X "   i n d e f i n i d o   p a r a   G e r M o v I D   =   '   +  
                                     G e r a l . F F 0 ( G e r M o v I D )   +   '   e   G e r N i v e l 1   =   '   +   G e r a l . F F 0 ( G e r N i v e l 1 ) ) ;  
                                 V S _ P F . A t u a l i z a J m p G G X ( T E s t q M o v i m I D ( G e r M o v I D ) ,   G e r N i v e l 1 ) ;  
                                 U n D m k D A C _ P F . A b r e Q u e r y ( Q r V S R i b I n d ,   D m o d . M y D B ) ;  
                                 G e r G G X   : =   Q r V S R i b I n d J m p G G X . V a l u e ;  
                                 i f   M e A v i s o   < >   n i l   t h e n  
                                 b e g i n  
                                     i f   G e r G G X   =   0   t h e n  
                                         M e A v i s o . L i n e s . A d d ( ' " G e r G G X "   n a o   c o r r i g i d o   p a r a   G e r M o v I D   =   '   +  
                                         G e r a l . F F 0 ( G e r M o v I D )   +   '   e   G e r N i v e l 1   =   '   +   G e r a l . F F 0 ( G e r N i v e l 1 ) )  
                                     e l s e  
                                         M e A v i s o . L i n e s . A d d ( ' " G e r G G X "   c o r r i g i d o   p a r a   G e r M o v I D   =   '   +  
                                         G e r a l . F F 0 ( G e r M o v I D )   +   '   e   G e r N i v e l 1   =   '   +   G e r a l . F F 0 ( G e r N i v e l 1 ) ) ;  
                                 e n d ;  
 * )  
                             e n d ;  
                             V e r i f i c a S e T e m G r a G r u X ( 3 0 4 2 ,   G r a G r u X ,   s P r o c N a m e ,   Q r V S R i b I n d ) ;  
                             V e r i f i c a S e T e m G r a G r u X ( 3 0 4 3 ,   G e r G G X ,   s P r o c N a m e ,   Q r V S R i b I n d ) ;  
                             / / f a z e r   p e l o   J m p G G X !   s e m  
                             / /  
                             c a s e   F o r m a I n s e r c a o I t e m P r o d u c a o   o f  
                                 f i p I s o l a d a :  
                                     I n s e r e I t e m P r o d u c a o I s o l a d a I n d i r e t a ( D t H r A b e r t o ,   D t H r F i m O p e ,  
                                     D t M o v i m ,   D t C o r r A p o ,   M o v i m I D ,   E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,  
                                     C o d i g o ,   C o n t r o l e ,   G r a G r u X ,   M o v i m N i v ,   M o v i m T w n ,   A r e a M 2 ,   P e s o K g ,  
                                     P e c a s ,   O r i g e m O p e P r o c ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,  
                                     J m p N i v e l 2 ,   P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,  
                                     G e r M o v I D ,   G e r N i v e l 1 ,   G e r N i v e l 2 ,   G e r G G X ,   Q t d G e r P e c a ,  
                                     Q t d G e r P e s o ,   Q t d G e r A r M 2 ,   I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 ,   I n s e r i u P Q ) ;  
                                 f i p C o n j u n t a :  
                                     I n s e r e I t e m P r o d u c a o C o n j u n t a I n d i r e t a ( D t H r A b e r t o ,   D t H r F i m O p e ,  
                                     D t M o v i m ,   D t C o r r A p o ,   M o v i m I D ,   E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,  
                                     C o d i g o ,   C o n t r o l e ,   G r a G r u X ,   M o v i m N i v ,   M o v i m T w n ,   A r e a M 2 ,   P e s o K g ,  
                                     P e c a s ,   O r i g e m O p e P r o c ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,  
                                     J m p N i v e l 2 ,   P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,  
                                     G e r M o v I D ,   G e r N i v e l 1 ,   G e r N i v e l 2 ,   G e r G G X ,   Q t d G e r P e c a ,  
                                     Q t d G e r P e s o ,   Q t d G e r A r M 2 ,   I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 ,   I n s e r i u P Q ) ;  
                                 e l s e  
                                     A v i s o F i p ( ' I n d i r e t a ' ) ;  
                             e n d ;  
                         e n d ;  
                         / /  
                         Q r V S R i b I n d . N e x t ;  
                     e n d ;  
                 e n d ;  
                 i i p c U n i A n t :  
                 b e g i n  
                     Q r V S R i b O r i I M E I . F i r s t ;  
                     w h i l e   n o t   Q r V S R i b O r i I M E I . E o f   d o  
                     b e g i n  
                         i f   ( Q r V S R i b O r i I M E I D a t a H o r a . V a l u e   > =   F D i a I n i )  
                         a n d   ( Q r V S R i b O r i I M E I D a t a H o r a . V a l u e   <   F D i a P o s )   t h e n  
                         b e g i n  
                             M o v i m I D           : =   T E s t q M o v i m I D ( Q r V S R i b O r i I M E I M o v i m I D . V a l u e ) ;  
                             M o v i m C o d         : =   Q r V S R i b O r i I M E I M o v i m C o d . V a l u e ;  
                             C o d i g o             : =   Q r V S R i b O r i I M E I C o d i g o . V a l u e ;  
                             D t H r A b e r t o     : =   T r u n c ( Q r P r o c D t H r A b e r t o . V a l u e ) ;  
                             D t H r F i m O p e     : =   T r u n c ( Q r P r o c D t H r F i m O p e . V a l u e ) ;  
                             D t M o v i m           : =   T r u n c ( Q r V S R i b O r i I M E I D a t a H o r a . V a l u e ) ;  
                             D t C o r r A p o       : =   T r u n c ( Q r V S R i b O r i I M E I D t C o r r A p o . V a l u e ) ;  
                             E m p r e s a           : =   Q r V S R i b O r i I M E I E m p r e s a . V a l u e ;  
                             F o r n e c e M O       : =   Q r V S R i b O r i I M E I F o r n e c M O . V a l u e ;  
                             C o n t r o l e         : =   Q r V S R i b O r i I M E I C o n t r o l e . V a l u e ;  
                             G G X O r i             : =   Q r V S R i b O r i I M E I R m s G G X . V a l u e ;  
                             G G X D s t             : =   Q r V S R i b O r i I M E I D s t G G X . V a l u e ;  
                             M o v i m N i v         : =   T E s t q M o v i m N i v ( Q r V S R i b O r i I M E I M o v i m N i v . V a l u e ) ;  
                             M o v i m T w n         : =   Q r V S R i b O r i I M E I M o v i m T w n . V a l u e ;  
                             A r e a M 2             : =   Q r V S R i b O r i I M E I A r e a M 2 . V a l u e ;  
                             P e s o K g             : =   Q r V S R i b O r i I M E I P e s o K g . V a l u e ;  
                             P e c a s               : =   Q r V S R i b O r i I M E I P e c a s . V a l u e ;  
                             Q t d A n t P e c a     : =   Q r V S R i b O r i I M E I Q t d A n t P e c a . V a l u e ;  
                             Q t d A n t P e s o     : =   Q r V S R i b O r i I M E I Q t d A n t P e s o . V a l u e ;  
                             Q t d A n t A r M 2     : =   Q r V S R i b O r i I M E I Q t d A n t A r M 2 . V a l u e ;  
                             / /  
                             V e r i f i c a S e T e m G r a G r u X ( 3 1 0 0 ,   G G X O r i ,   s P r o c N a m e ,   Q r V S R i b O r i I M E I ) ;  
                             V e r i f i c a S e T e m G r a G r u X ( 3 1 0 1 ,   G G X D s t ,   s P r o c N a m e ,   Q r V S R i b O r i I M E I ) ;  
                             / /  
                             c a s e   F o r m a I n s e r c a o I t e m P r o d u c a o   o f  
                                 f i p I s o l a d a :  
                                     I n s e r e I t e m P r o d u c a o I s o l a d a U n i A n t ( D t H r A b e r t o ,   D t H r F i m O p e ,  
                                     D t M o v i m ,   D t C o r r A p o ,   M o v i m I D ,   E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,  
                                     C o d i g o ,   C o n t r o l e ,   G G X O r i ,   G G X D s t ,   M o v i m N i v ,   M o v i m T w n ,   A r e a M 2 ,  
                                     P e s o K g ,   P e c a s ,   O r i g e m O p e P r o c ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,  
                                     J m p N i v e l 2 ,   P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,  
                                     Q t d A n t P e c a ,   Q t d A n t P e s o ,   Q t d A n t A r M 2 ,   I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 ,  
                                     I n s e r i u P Q ) ;  
                                 f i p C o n j u n t a :  
                                     I n s e r e I t e m P r o d u c a o C o n j u n t a U n i A n t ( D t H r A b e r t o ,   D t H r F i m O p e ,  
                                     D t M o v i m ,   D t C o r r A p o ,   M o v i m I D ,   E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,  
                                     C o d i g o ,   C o n t r o l e ,   G G X O r i ,   G G X D s t ,   M o v i m N i v ,   M o v i m T w n ,   A r e a M 2 ,  
                                     P e s o K g ,   P e c a s ,   O r i g e m O p e P r o c ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,  
                                     J m p N i v e l 2 ,   P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,  
                                     Q t d A n t P e c a ,   Q t d A n t P e s o ,   Q t d A n t A r M 2 ,   I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 ,  
                                     I n s e r i u P Q ) ;  
                                 e l s e  
                                     A v i s o F i p ( ' U n i d A n t ' ) ;  
                             e n d ;  
                         e n d ;  
                         / /  
                         Q r V S R i b O r i I M E I . N e x t ;  
                     e n d ;  
                 e n d ;  
                 i i p c U n i G e r :  
                 b e g i n  
                     Q r V S R i b I n d . F i r s t ;  
                     w h i l e   n o t   Q r V S R i b I n d . E o f   d o  
                     b e g i n  
                         i f   ( Q r V S R i b I n d D a t a H o r a . V a l u e   > =   F D i a I n i )  
                         a n d   ( Q r V S R i b I n d D a t a H o r a . V a l u e   <   F D i a P o s )   t h e n  
                         b e g i n  
                             M o v i m I D           : =   T E s t q M o v i m I D ( Q r V S R i b I n d M o v i m I D . V a l u e ) ;  
                             M o v i m C o d         : =   Q r V S R i b I n d M o v i m C o d . V a l u e ;  
                             C o d i g o             : =   Q r V S R i b I n d C o d i g o . V a l u e ;  
                             D t H r A b e r t o     : =   T r u n c ( Q r P r o c D t H r A b e r t o . V a l u e ) ;  
                             D t H r F i m O p e     : =   T r u n c ( Q r P r o c D t H r F i m O p e . V a l u e ) ;  
                             D t M o v i m           : =   T r u n c ( Q r V S R i b I n d D a t a H o r a . V a l u e ) ;  
                             D t C o r r A p o       : =   T r u n c ( Q r V S R i b I n d D t C o r r A p o . V a l u e ) ;  
                             E m p r e s a           : =   Q r V S R i b I n d E m p r e s a . V a l u e ;  
                             F o r n e c e M O       : =   Q r V S R i b I n d F o r n e c M O . V a l u e ;  
                             C o n t r o l e         : =   Q r V S R i b I n d C o n t r o l e . V a l u e ;  
                             G G X O r i             : =   Q r V S R i b I n d R m s G G X . V a l u e ;  
                             G G X D s t             : =   Q r V S R i b I n d J m p G G X . V a l u e ;  
                             M o v i m N i v         : =   T E s t q M o v i m N i v ( Q r V S R i b I n d M o v i m N i v . V a l u e ) ;  
                             M o v i m T w n         : =   Q r V S R i b I n d M o v i m T w n . V a l u e ;  
                             A r e a M 2             : =   Q r V S R i b I n d A r e a M 2 . V a l u e ;  
                             P e s o K g             : =   Q r V S R i b I n d P e s o K g . V a l u e ;  
                             P e c a s               : =   Q r V S R i b I n d P e c a s . V a l u e ;  
                             Q t d G e r P e c a     : =   P e c a s ;  
                             Q t d G e r P e s o     : =   P e s o K g ;  
                             Q t d G e r A r M 2     : =   A r e a M 2 ;  
                             / /  
                             V e r i f i c a S e T e m G r a G r u X ( 3 1 5 7 ,   G G X O r i ,   s P r o c N a m e ,   Q r V S R i b I n d ) ;  
                             V e r i f i c a S e T e m G r a G r u X ( 3 1 5 8 ,   G G X D s t ,   s P r o c N a m e ,   Q r V S R i b I n d ) ;  
                             / /  
                             J m p M o v I D         : =   Q r V S R i b I n d J m p M o v I D . V a l u e ;  
                             J m p N i v e l 1       : =   Q r V S R i b I n d J m p N i v e l 1 . V a l u e ;  
                             / / i f   M o v i m I D   =   T E s t q M o v i m I D . e m i d C u r t i d o   t h e n  
                             i f   ( M o v i m I D   =   T E s t q M o v i m I D . e m i d C u r t i d o )   a n d  
                             ( T E s t q M o v i m I D ( J m p M o v I D )   =   T E s t q M o v i m I D . e m i d C u r t i d o )   t h e n  
                             b e g i n  
                                 M o v i m I D   : =   T E s t q M o v i m I D ( J m p M o v I D ) ;  
                                 i f   J m p N i v e l 1   < >   0   t h e n  
                                 b e g i n  
                                     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r C a b 3 4 ,   D m o d . M y D B ,   [  
                                     ' S E L E C T   M o v i m C o d ' ,  
                                     ' F R O M   v s c u r j m p ' ,  
                                     ' W H E R E   C o d i g o = '   +   G e r a l . F F 0 ( J m p N i v e l 1 ) ,  
                                     ' ' ] ) ;  
                                     M o v i m C o d   : =   Q r C a b 3 4 M o v i m C o d . V a l u e ;  
                                 e n d   e l s e  
                                 b e g i n  
                                     M o v i m C o d   : =   0 ;  
                                     G e r a l . M B _ A v i s o ( ' M o v i m C o d   i n d e f i n i d o   e m   '   +   s P r o c N a m e   +   '   ( 3 ) ' ) ;  
                                 e n d ;  
                             e n d ;  
                             / /  
                             c a s e   F o r m a I n s e r c a o I t e m P r o d u c a o   o f  
                                 f i p I s o l a d a :  
                                     I n s e r e I t e m P r o d u c a o I s o l a d a U n i G e r ( D t H r A b e r t o ,   D t H r F i m O p e ,  
                                     D t M o v i m ,   D t C o r r A p o ,   M o v i m I D ,   E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,  
                                     C o d i g o ,   C o n t r o l e ,   G G X O r i ,   G G X D s t ,   M o v i m N i v ,   M o v i m T w n ,   A r e a M 2 ,  
                                     P e s o K g ,   P e c a s ,   O r i g e m O p e P r o c ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,  
                                     J m p N i v e l 2 ,   P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,  
                                     Q t d G e r P e c a ,   Q t d G e r P e s o ,   Q t d G e r A r M 2 ,   I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 ,  
                                     I n s e r i u P Q ) ;  
                                 f i p C o n j u n t a :  
                                     I n s e r e I t e m P r o d u c a o C o n j u n t a U n i G e r ( D t H r A b e r t o ,   D t H r F i m O p e ,  
                                     D t M o v i m ,   D t C o r r A p o ,   M o v i m I D ,   E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,  
                                     C o d i g o ,   C o n t r o l e ,   G G X O r i ,   G G X D s t ,   M o v i m N i v ,   M o v i m T w n ,   A r e a M 2 ,  
                                     P e s o K g ,   P e c a s ,   O r i g e m O p e P r o c ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,  
                                     J m p N i v e l 2 ,   P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,  
                                     Q t d A n t P e c a ,   Q t d A n t P e s o ,   Q t d A n t A r M 2 ,   I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 ,  
                                     I n s e r i u P Q ) ;  
                                 e l s e  
                                     A v i s o F i p ( ' U n i d G e r ' ) ;  
                             e n d ;  
                         e n d ;  
                         / /  
                         Q r V S R i b I n d . N e x t ;  
                     e n d ;  
                 e n d ;  
                 i i p c S i m p l e s :  
                 b e g i n  
                     / /   G e r a � � o !  
                     Q r V S R i b D s t . F i r s t ;  
                     w h i l e   n o t   Q r V S R i b D s t . E o f   d o  
                     b e g i n  
                         i f   ( Q r V S R i b D s t D a t a H o r a . V a l u e   > =   F D i a I n i )  
                         a n d   ( Q r V S R i b D s t D a t a H o r a . V a l u e   <   F D i a P o s )   t h e n  
                         b e g i n  
                             M o v i m I D           : =   T E s t q M o v i m I D ( Q r V S R i b D s t M o v i m I D . V a l u e ) ;  
                             M o v i m C o d         : =   Q r V S R i b D s t M o v i m C o d . V a l u e ;  
                             C o d i g o             : =   Q r V S R i b D s t C o d i g o . V a l u e ;  
                             D t H r A b e r t o     : =   T r u n c ( Q r P r o c D t H r A b e r t o . V a l u e ) ;  
                             D t H r F i m O p e     : =   T r u n c ( Q r P r o c D t H r F i m O p e . V a l u e ) ;  
                             D t M o v i m           : =   T r u n c ( Q r V S R i b D s t D a t a H o r a . V a l u e ) ;  
                             D t C o r r A p o       : =   T r u n c ( Q r V S R i b D s t D t C o r r A p o . V a l u e ) ;  
                             E m p r e s a           : =   Q r V S R i b D s t E m p r e s a . V a l u e ;  
                             F o r n e c e M O       : =   Q r V S R i b D s t F o r n e c M O . V a l u e ;  
                             C o n t r o l e         : =   Q r V S R i b D s t C o n t r o l e . V a l u e ;  
                             G r a G r u X           : =   Q r V S R i b D s t G r a G r u X . V a l u e ;  
                             M o v i m N i v         : =   T E s t q M o v i m N i v ( Q r V S R i b D s t M o v i m N i v . V a l u e ) ;  
                             M o v i m T w n         : =   Q r V S R i b D s t M o v i m T w n . V a l u e ;  
                             A r e a M 2             : =   Q r V S R i b D s t A r e a M 2 . V a l u e ;  
                             P e s o K g             : =   Q r V S R i b D s t P e s o K g . V a l u e ;  
                             P e c a s               : =   Q r V S R i b D s t P e c a s . V a l u e ;  
                             / /  
                             V e r i f i c a S e T e m G r a G r u X ( 3 2 3 3 ,   G r a G r u X ,   s P r o c N a m e ,   Q r V S R i b D s t ) ;  
                             c a s e   F o r m a I n s e r c a o I t e m P r o d u c a o   o f  
                                 {  
                                 f i p I s o l a d a :  
                                     I n s e r e I t e m P r o d u c a o I s o l a d a S i m p l e s ( D t H r A b e r t o ,   D t H r F i m O p e ,  
                                     D t M o v i m ,   D t C o r r A p o ,   M o v i m I D ,   E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,  
                                     C o d i g o ,   C o n t r o l e ,   G G X O r i ,   G G X D s t ,   M o v i m N i v ,   M o v i m T w n ,   A r e a M 2 ,  
                                     P e s o K g ,   P e c a s ,   O r i g e m O p e P r o c ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,  
                                     J m p N i v e l 2 ,   P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,  
                                     Q t d G e r P e c a ,   Q t d G e r P e s o ,   Q t d G e r A r M 2 ,   I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 ,  
                                     I n s e r i u P Q ) ;  
                                 }  
                                 f i p C o n j u n t a :  
                                     I n s e r e I t e m P r o d u c a o C o n j u n t a S i m p l e s ( D t H r A b e r t o ,   D t H r F i m O p e ,  
                                     D t M o v i m ,   D t C o r r A p o ,   M o v i m I D ,   E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,  
                                     C o d i g o ,   C o n t r o l e ,   G r a G r u X ,   M o v i m N i v ,   M o v i m T w n ,   A r e a M 2 ,  
                                     P e s o K g ,   P e c a s ,   O r i g e m O p e P r o c ,   P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,  
                                     P a i N i v e l 2 ,   ( * F a t o r * ) 1 ,   T E s t q S P E D T a b S o r c . e s t s V M I ,  
                                     T S P E D _ E F D _ G e r B x a . s e g b G e r a ,   I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 ) ;  
                                 e l s e  
                                     A v i s o F i p ( ' S i m p l e s ' ) ;  
                             e n d ;  
                         e n d ;  
                         / /  
                         Q r V S R i b D s t . N e x t ;  
                     e n d ;  
                     / /  
                     / /   B a i x a !  
                     Q r V S R i b O r i I M E I . F i r s t ;  
                     w h i l e   n o t   Q r V S R i b O r i I M E I . E o f   d o  
                     b e g i n  
                         i f   ( Q r V S R i b O r i I M E I D a t a H o r a . V a l u e   > =   F D i a I n i )  
                         a n d   ( Q r V S R i b O r i I M E I D a t a H o r a . V a l u e   <   F D i a P o s )   t h e n  
                         b e g i n  
                             M o v i m I D           : =   T E s t q M o v i m I D ( Q r V S R i b O r i I M E I M o v i m I D . V a l u e ) ;  
                             M o v i m C o d         : =   Q r V S R i b O r i I M E I M o v i m C o d . V a l u e ;  
                             C o d i g o             : =   Q r V S R i b O r i I M E I C o d i g o . V a l u e ;  
                             D t H r A b e r t o     : =   T r u n c ( Q r P r o c D t H r A b e r t o . V a l u e ) ;  
                             D t H r F i m O p e     : =   T r u n c ( Q r P r o c D t H r F i m O p e . V a l u e ) ;  
                             D t M o v i m           : =   T r u n c ( Q r V S R i b O r i I M E I D a t a H o r a . V a l u e ) ;  
                             D t C o r r A p o       : =   T r u n c ( Q r V S R i b O r i I M E I D t C o r r A p o . V a l u e ) ;  
                             E m p r e s a           : =   Q r V S R i b O r i I M E I E m p r e s a . V a l u e ;  
                             F o r n e c e M O       : =   Q r V S R i b O r i I M E I F o r n e c M O . V a l u e ;  
                             C o n t r o l e         : =   Q r V S R i b O r i I M E I C o n t r o l e . V a l u e ;  
                             G r a G r u X           : =   Q r V S R i b A t u G r a G r u X . V a l u e ;  
                             M o v i m N i v         : =   T E s t q M o v i m N i v ( Q r V S R i b O r i I M E I M o v i m N i v . V a l u e ) ;  
                             M o v i m T w n         : =   Q r V S R i b O r i I M E I M o v i m T w n . V a l u e ;  
                             A r e a M 2             : =   Q r V S R i b O r i I M E I A r e a M 2 . V a l u e ;  
                             P e s o K g             : =   Q r V S R i b O r i I M E I P e s o K g . V a l u e ;  
                             P e c a s               : =   Q r V S R i b O r i I M E I P e c a s . V a l u e ;  
                             / /  
                             V e r i f i c a S e T e m G r a G r u X ( 3 2 8 4 ,   G r a G r u X ,   s P r o c N a m e ,   Q r V S R i b O r i I M E I ) ;  
                             / /  
                             c a s e   F o r m a I n s e r c a o I t e m P r o d u c a o   o f  
                                 {  
                                 f i p I s o l a d a :  
                                     I n s e r e I t e m P r o d u c a o I s o l a d a S i m p l e s ( D t H r A b e r t o ,   D t H r F i m O p e ,  
                                     D t M o v i m ,   D t C o r r A p o ,   M o v i m I D ,   E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,  
                                     C o d i g o ,   C o n t r o l e ,   G G X O r i ,   G G X D s t ,   M o v i m N i v ,   M o v i m T w n ,   A r e a M 2 ,  
                                     P e s o K g ,   P e c a s ,   O r i g e m O p e P r o c ,   J m p M o v I D ,   J m p M o v C o d ,   J m p N i v e l 1 ,  
                                     J m p N i v e l 2 ,   P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,   P a i N i v e l 2 ,  
                                     Q t d G e r P e c a ,   Q t d G e r P e s o ,   Q t d G e r A r M 2 ,   I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 ,  
                                     I n s e r i u P Q ) ;  
                                 }  
                                 f i p C o n j u n t a :  
                                     I n s e r e I t e m P r o d u c a o C o n j u n t a S i m p l e s ( D t H r A b e r t o ,   D t H r F i m O p e ,  
                                     D t M o v i m ,   D t C o r r A p o ,   M o v i m I D ,   E m p r e s a ,   M o v i m C o d ,   C o d D o c O P ,  
                                     C o d i g o ,   C o n t r o l e ,   G r a G r u X ,   M o v i m N i v ,   M o v i m T w n ,   A r e a M 2 ,  
                                     P e s o K g ,   P e c a s ,   O r i g e m O p e P r o c ,   P a i M o v I D ,   M o v C o d P a i ,   P a i N i v e l 1 ,  
                                     P a i N i v e l 2 ,   ( * F a t o r * ) - 1 ,   T E s t q S P E D T a b S o r c . e s t s V M I ,  
                                     T S P E D _ E F D _ G e r B x a . s e g b B a i x a ,   I D S e q 1 _ K 2 9 0 ,   I D S e q 1 _ K 3 0 0 ) ;  
                                 e l s e  
                                     A v i s o F i p ( ' S i m p l e s ' ) ;  
                             e n d ;  
                         e n d ;  
                         / /  
                         Q r V S R i b O r i I M E I . N e x t ;  
                     e n d ;  
                     / / b a i x a   ! ! ! !  
 {  
             / /   K 2 9 2   -   I n s u m o s   c o n s u m i d o s  
             i f   n o t   I n s e r i u P Q   t h e n  
                 I n s e r i u P Q   : =   I n s e r e B _ O r i g e P r o c P Q _ C o n j u n t a ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   M o v i m N i v ,  
                 ( * M o v i m C o d * ) M o v C o d P a i ,   I D S e q 1 _ K 2 9 0 ,   F S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   C l i e n t M O ,  
                 F o r n e c M O ,   E n t i S i t i o ) ;  
 }  
  
                 e n d ;  
                 e l s e   b e g i n  
                     G e r a l . M B _ E r r o ( G e t E n u m N a m e ( T y p e I n f o ( T I n s e r c a o I t e m P r o d u c a o C o n j u n t a ) ,  
                     I n t e g e r ( I n s e r c a o I t e m P r o d u c a o C o n j u n t a ) )   +   '   i n d e f i n i d o   e m   '   +  
                     s p r o c N a m e   +   '   ( 4 ) ' ) ;  
                 e n d ;  
             e n d ;  
             Q r I M E C s . N e x t ;  
         e n d ;  
         Q r F a t h e r . N e x t ;  
     e n d ;  
 e n d ;  
  
 p r o c e d u r e   T F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I n s e r e B _ P r o d u c a o I s o l a d a ( S Q L x ,  
     S Q L _ P e r i o d o P Q :   S t r i n g ;   P s q M o v i m I D :   T E s t q M o v i m I D ;   P s q M o v i m N i v :  
     T E s t q M o v i m N i v ;   O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ) ;  
 c o n s t  
     s P r o c N a m e   =   ' " F m S p e d E f d I c m s I p i P r o d u c a o C o n j u n t a . I n s e r e B _ P r o d u c a o I s o l a d a ( ) ' ;  
     D s t F a t o r   =   1 ;  
     E S T S T a b S o r c   =   e s t s V M I ;  
 v a r  
     P e c a s ,   A r e a M 2 ,   P e s o K g ,   Q t d A n t A r M 2 ,   Q t d A n t P e s o ,   Q t d A n t P e c a ,   Q t d e S r c ,   Q t d e D s t :  
     D o u b l e ;  
     D a t a ,   D t H r A b e r t o ,   D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o :   T D a t e T i m e ;  
  
     S r c T i p o E s t q ,   D s t T i p o E s t q ,   G G X S r c ,   G G X D s t ,   C o d i g o ,   M o v i m C o d ,   I D S e q 1 ,  
     I D _ I t e m ,   C o n t r o l e ,   S r c F a t o r ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o :   I n t e g e r ;  
     M o v i m N i v :   T E s t q M o v i m N i v ;  
     T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;  
     C O D _ I N S _ S U B S T ( * ,   S Q L x * ) :   S t r i n g ;  
     M o v i m I D :   T E s t q M o v i m I D ;  
     M o v i m N i v I n n :   T E s t q M o v i m N i v ;  
     / /  
 ( * I n c l u i P Q x :   B o o l e a n ; * )  
 b e g i n  
     C O D _ I N S _ S U B S T   : =   ' ' ;  
     c a s e   P s q M o v i m N i v   o f  
         T E s t q m o v i m N i v . e m i n S o r c C a l :   S r c F a t o r   : =   1 ;  
         T E s t q m o v i m N i v . e m i n D e s t C a l :   S r c F a t o r   : =   - 1 ;  
         T E s t q m o v i m N i v . e m i n S o r c C u r :   S r c F a t o r   : =   1 ;  
         e l s e  
         b e g i n  
             S r c F a t o r   : =   - 1 ;  
             G e r a l . M B _ E r r o ( ' " S r c F a t o r "   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e ) ;  
         e n d ;  
     e n d ;  
     / /  
     D t H r F i m O p e   : =   0 ;  
     / /  
     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r M C ,   D M o d G . M y P I D _ D B ,   [  
     ' D R O P   T A B L E   I F   E X I S T S   _ S P E D _ E F D _ K 2 X X _ O _ P ;     ' ,  
     ' C R E A T E   T A B L E   _ S P E D _ E F D _ K 2 X X _ O _ P     ' ,  
     / /  
     S Q L x ,  
     ' ;   ' ,  
     '   ' ,  
     ' S E L E C T   D I S T I N C T   M o v i m C o d     ' ,  
     ' F R O M   _ S P E D _ E F D _ K 2 X X _ O _ P   ' ,  
     ' O R D E R   B Y   M o v i m C o d ;     ' ,  
     ' ' ] ) ;  
     / /  
     i f   S P E D _ E F D _ P F . E r r G r a n d e z a ( ' _ S P E D _ E F D _ K 2 X X _ O _ P ' ,   ' G G x _ S r c ' ,   ' G G X _ D s t '   )   t h e n  
         E x i t ;  
     / /  
     P B 2 . P o s i t i o n   : =   0 ;  
     P B 2 . M a x   : =   Q r M C . R e c o r d C o u n t ;  
     Q r M C . F i r s t ;  
     / / i f   Q r M C . R e c o r d C o u n t   >   0   t h e n  
         / / G e r a l . M B _ A v i s o ( ' D E P R R C A R ! ! !   O p e P r o c A   -   1 ' ) ;  
     w h i l e   n o t   Q r M C . E o f   d o  
     b e g i n  
         M y O b j e c t s . U p d P B ( P B 2 ,   L a A v i s o 3 ,   L a A v i s o 4 ) ;  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r P r o d R i b ,   D M o d G . M y P I D _ D B ,   [  
         ' S E L E C T   *   ' ,  
         ' F R O M   _ S P E D _ E F D _ K 2 X X _ O _ P   ' ,  
         ' W H E R E   M o v i m C o d = '   +   G e r a l . F F 0 ( Q r M C M o v i m C o d . V a l u e ) ,  
         ' A N D   M o v i m I D = '   +   G e r a l . F F 0 ( I n t e g e r ( P s q M o v i m I D ) ) ,  
         ' ' ] ) ;  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q r P r o d R i b ) ;   / / P a r e i   a q u i !  
         Q r P r o d R i b . F i r s t ;  
         w h i l e   n o t   Q r P r o d R i b . E o f   d o  
         b e g i n  
             M o v i m I D           : =   T E s t q M o v i m I D ( Q r P r o d R i b M o v i m I D . V a l u e ) ;  
             A r e a M 2             : =   Q r P r o d R i b A r e a M 2 . V a l u e ;  
             P e s o K g             : =   Q r P r o d R i b P e s o K g . V a l u e ;  
             P e c a s               : =   Q r P r o d R i b P e c a s . V a l u e ;  
             Q t d A n t A r M 2     : =   Q r P r o d R i b Q t d A n t A r M 2 . V a l u e ;  
             Q t d A n t P e s o     : =   Q r P r o d R i b Q t d A n t P e s o . V a l u e ;  
             Q t d A n t P e c a     : =   Q r P r o d R i b Q t d A n t P e c a . V a l u e ;  
             D a t a                 : =   Q r P r o d R i b D a t a . V a l u e ;  
             S r c T i p o E s t q   : =   T r u n c ( Q r P r o d R i b s T i p o E s t q . V a l u e ) ;  
             D s t T i p o E s t q   : =   T r u n c ( Q r P r o d R i b d T i p o E s t q . V a l u e ) ;  
             G G X S r c             : =   Q r P r o d R i b G G X _ S r c . V a l u e ;  
             G G X D s t             : =   Q r P r o d R i b G G X _ D s t . V a l u e ;  
             M o v i m C o d         : =   Q r P r o d R i b M o v i m C o d . V a l u e ;  
             M o v i m N i v         : =   T E s t q M o v i m N i v ( Q r P r o d R i b M o v i m N i v . V a l u e ) ;  
             C o d i g o             : =   Q r P r o d R i b C o d i g o . V a l u e ;  
             C o n t r o l e         : =   Q r P r o d R i b C o n t r o l e . V a l u e ;  
             I D _ I t e m           : =   C o n t r o l e ;  
             / /  
             D t M o v i m           : =   D a t a ;  
             D t C o r r A p o       : =   Q r P r o d R i b D t C o r r A p o . V a l u e ;  
             / /  
             C l i e n t M O         : =   Q r P r o d R i b C l i e n t M O . V a l u e ;  
             F o r n e c M O         : =   Q r P r o d R i b F o r n e c M O . V a l u e ;  
             E n t i S i t i o       : =   Q r P r o d R i b E n t i S i t i o . V a l u e ;  
             / /  
             i f   ( C l i e n t M O   = 0 )   o r   ( F o r n e c M O = 0 )   o r   ( E n t i S i t i o = 0 )   t h e n  
             b e g i n  
                 i f   M o v i m I D   =   e m i d C a l e a d o   t h e n  
                     / /   N o v o ! !  
                     D e f i n e C l i F o r S i t e T i p E s t q ( C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   S r c T i p o E s t q )  
                 e l s e  
                 b e g i n  
                     / /   A n t i g o !   U s a r   n o v o   n o s   o u t r o s ?  
                     M o v i m N i v I n n   : =   V S _ P F . O b t e m M o v i m N i v I n n D e M o v i m I D ( M o v i m I D ) ;  
                     / /  
                     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r O b t C l i F o r S i t e ,   D m o d . M y D B ,   [  
                     ' S E L E C T   v m i . C l i e n t M O ,   v m i . F o r n e c M O ,   s c c . E n t i S i t i o     ' ,  
                     ' F R O M   v s m o v i t s   v m i   ' ,  
                     ' L E F T   J O I N   s t q c e n l o c   s c l   O N   s c l . C o n t r o l e = v m i . S t q C e n L o c     ' ,  
                     ' L E F T   J O I N   s t q c e n c a d   s c c   O N   s c c . C o d i g o = s c l . C o d i g o     ' ,  
                     ' W H E R E   v m i . M o v i m C o d = '   +   G e r a l . F F 0 ( M o v i m C o d ) ,  
                     ' A N D   v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m N i v I n n ) ) ,  
                     ' ' ] ) ;  
                     / / G e r a l . M B _ S Q L ( S e l f ,   Q r O b t C l i F o r S i t e ) ;  
                     C l i e n t M O     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' C l i e n t M O ' ) . A s I n t e g e r ;  
                     F o r n e c M O     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' F o r n e c M O ' ) . A s I n t e g e r ;  
                     E n t i S i t i o     : =   Q r O b t C l i F o r S i t e . F i e l d B y N a m e ( ' E n t i S i t i o ' ) . A s I n t e g e r ;  
                 e n d ;  
             e n d ;  
             / /  
             / / i f   ( P e c a s   < >   0 )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
             i f   ( ( P e c a s   < >   0 )   o r   ( M o v i m I D = T E s t q M o v i m I D . e m i d E m P r o c S P ) )   a n d   ( D a t a   >   1 )   t h e n  
             b e g i n  
                 c a s e   S r c T i p o E s t q   o f  
                     1 :   Q t d e S r c   : =   A r e a M 2 ;  
                     2 :   Q t d e S r c   : =   P e s o K g ;  
                     e l s e  
                     b e g i n  
                         G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( S r c T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                         +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ S r c T i p o E s t q ]   +   s L i n e B r e a k   +  
                         ' R e d u z i d o   =   '   +   G e r a l . F F 0 ( G G X D s t )   +   s L i n e B r e a k   +  
                         ' M o v i m C o d   =   '   +   G e r a l . F F 0 ( M o v i m C o d )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
                         Q r P r o d R i b . S Q L . T e x t ) ;  
                         / /  
                         Q t d e D s t   : =   P e c a s ;  
                     e n d ;  
                 e n d ;  
                 Q t d e S r c   : =   Q t d e S r c   *   S r c F a t o r ;  
  
                 / /  
                 c a s e   D s t T i p o E s t q   o f  
                     1 :   Q t d e D s t   : =   Q t d A n t A r M 2 ;  
                     2 :   Q t d e D s t   : =   Q t d A n t P e s o ;  
                     e l s e  
                     b e g i n  
                         G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( S r c T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                         +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ S r c T i p o E s t q ]   +   s L i n e B r e a k   +  
                         ' R e d u z i d o   =   '   +   G e r a l . F F 0 ( G G X D s t )   +   s L i n e B r e a k   +  
                         ' M o v i m C o d   =   '   +   G e r a l . F F 0 ( M o v i m C o d )   +   s L i n e B r e a k   +   s L i n e B r e a k   +  
                         Q r P r o d R i b . S Q L . T e x t ) ;  
                         / /  
                         Q t d e D s t   : =   Q t d A n t P e c a ;  
                     e n d ;  
                 e n d ;  
                 Q t d e D s t   : =   Q t d e D s t   *   D s t F a t o r ;  
             e n d   e l s e  
             b e g i n  
                 Q t d e S r c   : =   0 ;  
                 Q t d e D s t   : =   0 ;  
             e n d ;  
             i f   ( * T e m T w n   a n d * )   ( Q t d e S r c   =   0 )   t h e n  
                 V e r i f i c a T i p o E s t q Q t d e ( S r c T i p o E s t q ,   A r e a M 2 ,   P e s o K g ,   P e c a s ,  
                 G G X S r c ,   M o v i m C o d ,   s P r o c N a m e ) ;  
             i f   ( * T e m T w n   a n d * )   ( Q t d e D s t   =   0 )   t h e n  
                 V e r i f i c a T i p o E s t q Q t d e ( D s t T i p o E s t q ,   A r e a M 2 ,   P e s o K g ,   P e c a s ,  
                 G G X D s t ,   M o v i m C o d ,   s P r o c N a m e ) ;  
             / /  
             i f   Q t d e S r c   <   0   t h e n  
                 M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                 ' Q u a n t i d a d e   " S r c "   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 5 ) '   +   s L i n e B r e a k   +  
                 ' Q t d e :   '   +   G e r a l . F F T ( Q t d e S r c ,   3 ,   s i N e g a t i v o ) ,   Q r P r o d R i b ) ;  
             i f   Q t d e D s t   <   0   t h e n  
                 M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                 ' Q u a n t i d a d e   " D s t "   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 6 ) '   +   s L i n e B r e a k   +  
                 ' Q t d e :   '   +   G e r a l . F F T ( Q t d e D s t ,   3 ,   s i N e g a t i v o ) ,   Q r P r o d R i b )  
             e l s e   i f   ( Q t d e D s t   =   0 )   a n d   ( D a t a   >   1 )   t h e n  
             b e g i n  
                 c a s e   M o v i m I D   o f  
                     e m i d E m P r o c C a l :   D a t a   : =   0 ;  
                     e m i d E m P r o c C u r :   D a t a   : =   0 ;  
                     e l s e   M e n s a g e m ( s P r o c N a m e ,   M o v i m C o d ,   I n t e g e r ( M o v i m I D ) ,  
                     ' Q u a n t i d a d e   z e r a d a   p a r a   O P   e n c e r r a d a ! ! !   ( 4 ) ' ,   Q r P r o d R i b ) ;  
                 e n d ;  
             e n d ;  
             / /  
             / / i f   Q r S u b P r d O P E m p r e s a . V a l u e   =   Q r F M O F o r n e c M O . V a l u e   t h e n  
  
             i f   F E m p r e s a   =   F o r n e c M O   t h e n  
                 T i p o R e g S P E D P r o d   : =   t r s p 2 3 X  
             e l s e  
                 T i p o R e g S P E D P r o d   : =   t r s p 2 5 X ;  
             / /  
             D t H r A b e r t o   : =   D a t a ;  
             D t H r A b e r t o   : =   D a t a ;  
             c a s e   T i p o R e g S P E D P r o d   o f  
                 t r s p 2 3 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,  
                     C o n t r o l e ,   D t H r A b e r t o ,   D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o ,   G G X D s t ,  
                     C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,   Q t d e D s t ,   O r i g e m O p e P r o c ,  
                     F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   I D S e q 1 ) ;  
                     / /  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 3 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 ,   D t H r A b e r t o ,  
                     D t C o r r A p o ,   G G X S r c ,   Q t d e S r c ,   C O D _ I N S _ S U B S T ,   I D _ I t e m ,   I n t e g e r ( M o v i m I D ) ,  
                     C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,  
                     O r i g e m O p e P r o c ,   E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                     / /  
 ( *                 D e s a b i l i t a d o .   P Q   S e r �   i n c l u i d o   n o   c o u r o   p r o c e s s a d o   e   n � o   e m   p r o c e s s o !  
                     I n s e r e _ O r i g e P r o c P Q ( T i p o R e g S P E D P r o d ,   M o v i m I D ,   M o v i m N i v ,   M o v i m C o d ,  
                     I D S e q 1 ,   S Q L _ P e r i o d o P Q ,   O r i g e m O p e P r o c ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ) ;  
 * )  
                 e n d ;  
                 t r s p 2 5 X :  
                 b e g i n  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 0 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,  
                     D t H r F i m O p e ,   D t M o v i m ,   D t C o r r A p o ,   G G X D s t ,   C l i e n t M O ,   F o r n e c M O ,   E n t i S i t i o ,  
                     Q t d e D s t ,   O r i g e m O p e P r o c ,   F D i a F i m ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ,   I D S e q 1 ) ;  
                     / /  
                     S P E D _ E F D _ P F . I n s e r e I t e m A t u a l _ K 2 5 5 ( F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,  
                     F P e r i A p u ,   I D S e q 1 ,   D t H r A b e r t o ,   D t C o r r A p o ,   G G X S r c ,  
                     Q t d e S r c ,   C O D _ I N S _ S U B S T ,   I D _ I t e m ,  
                     I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   C o n t r o l e ,   C l i e n t M O ,   F o r n e c M O ,  
                     E n t i S i t i o ,   O r i g e m O p e P r o c ,   E S T S T a b S o r c ,   F T i p o P e r i o d o F i s c a l ,   M e A v i s o ) ;  
                     / /  
 ( *                 D e s a b i l i t a d o .   P Q   S e r �   i n c l u i d o   n o   c o u r o   