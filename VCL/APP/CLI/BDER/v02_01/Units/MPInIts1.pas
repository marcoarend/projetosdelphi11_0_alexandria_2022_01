unit MPInIts1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkEdit, Db,
  mySQLDbTables, Mask, DBCtrls, Menus, ComCtrls, Variants, dmkEditCB,
  dmkDBLookupComboBox, dmkGeral, dmkLabel, dmkImage, dmkEditDateTimePicker,
  UnDmkEnums, DmkDAC_PF;

type
  TPS_ComoCalc = (psccPeso, psccValU, psccValT);
  TFmMPInIts1 = class(TForm)
    Panel1: TPanel;
    DBGrid2: TDBGrid;
    Splitter1: TSplitter;
    QrEntiMP: TmySQLQuery;
    QrEntiMPCodigo: TIntegerField;
    QrEntiMPLk: TIntegerField;
    QrEntiMPDataCad: TDateField;
    QrEntiMPDataAlt: TDateField;
    QrEntiMPUserCad: TIntegerField;
    QrEntiMPUserAlt: TIntegerField;
    QrEntiMPAlterWeb: TSmallintField;
    QrEntiMPAtivo: TSmallintField;
    QrEntiMPCMPUnida: TSmallintField;
    QrEntiMPCMPMoeda: TSmallintField;
    QrEntiMPCMPPerio: TSmallintField;
    QrEntiMPCMPPreco: TFloatField;
    QrEntiMPCMPFrete: TFloatField;
    QrEntiMPCPLUnida: TSmallintField;
    QrEntiMPCPLMoeda: TSmallintField;
    QrEntiMPCPLPerio: TSmallintField;
    QrEntiMPCPLPreco: TFloatField;
    QrEntiMPCPLFrete: TFloatField;
    QrEntiMPAGIUnida: TSmallintField;
    QrEntiMPAGIMoeda: TSmallintField;
    QrEntiMPAGIPerio: TSmallintField;
    QrEntiMPAGIPreco: TFloatField;
    QrEntiMPAGIFrete: TFloatField;
    QrEntiMPCMIUnida: TSmallintField;
    QrEntiMPCMIMoeda: TSmallintField;
    QrEntiMPCMIPerio: TSmallintField;
    QrEntiMPCMIPreco: TFloatField;
    QrEntiMPCMIFrete: TFloatField;
    QrEntiMPPreDescarn: TSmallintField;
    QrCambios: TmySQLQuery;
    QrCambiosData: TDateField;
    QrCambiosDolar: TFloatField;
    QrCambiosEuro: TFloatField;
    QrCambiosIndexador: TFloatField;
    DsCambios: TDataSource;
    QrEntiMPMaskLetras: TWideStringField;
    QrEntiMPMaskFormat: TWideStringField;
    QrEntiMPCPLSit: TSmallintField;
    QrEntiMPCMP_LPQV: TFloatField;
    QrEntiMPCPL_LPQV: TFloatField;
    QrEntiMPAGI_LPQV: TFloatField;
    QrEntiMPCMI_LPQV: TFloatField;
    QrEntiMPCMP_IDQV: TSmallintField;
    QrEntiMPCPL_IDQV: TSmallintField;
    QrEntiMPAGI_IDQV: TSmallintField;
    QrEntiMPCMI_IDQV: TSmallintField;
    QrFornece: TmySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTIDADE: TWideStringField;
    QrFornecePreDescarn: TSmallintField;
    QrForneceMaskLetras: TWideStringField;
    QrForneceMaskFormat: TWideStringField;
    QrForneceCorretor: TIntegerField;
    QrForneceComissao: TFloatField;
    QrForneceDescAdiant: TFloatField;
    QrForneceCondPagto: TWideStringField;
    QrForneceCondComis: TWideStringField;
    QrForneceLocalEntrg: TWideStringField;
    QrForneceAbate: TIntegerField;
    QrForneceTransporte: TIntegerField;
    DsFornece: TDataSource;
    PMFornece: TPopupMenu;
    Entidades1: TMenuItem;
    FornecedoresdeMP1: TMenuItem;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Label1: TLabel;
    Label10: TLabel;
    Label8: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label35: TLabel;
    Label37: TLabel;
    EdCaminhao: TdmkEdit;
    EdPecasNF: TdmkEdit;
    EdPecas: TdmkEdit;
    EdPLE: TdmkEdit;
    EdPDA: TdmkEdit;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    BitBtn1: TBitBtn;
    EdPNF: TdmkEdit;
    Panel8: TPanel;
    Panel9: TPanel;
    GroupBox5: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    Label30: TLabel;
    EdCMPValor: TdmkEdit;
    EdCMPFrete: TdmkEdit;
    EdCMPDevol: TdmkEdit;
    GroupBox2: TGroupBox;
    LaCPLValor: TLabel;
    Label9: TLabel;
    Label32: TLabel;
    EdCPLValor: TdmkEdit;
    EdCPLFrete: TdmkEdit;
    EdCPLDevol: TdmkEdit;
    Panel11: TPanel;
    Panel12: TPanel;
    GroupBox6: TGroupBox;
    SpeedButton1: TSpeedButton;
    Panel13: TPanel;
    Label23: TLabel;
    Label22: TLabel;
    Label21: TLabel;
    Label20: TLabel;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    GroupBox7: TGroupBox;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    SpeedButton2: TSpeedButton;
    EdDolar: TdmkEdit;
    EdEuro: TdmkEdit;
    EdIndexador: TdmkEdit;
    GroupBox8: TGroupBox;
    Label28: TLabel;
    Label18: TLabel;
    Label29: TLabel;
    Label34: TLabel;
    EdCMPQuebra: TdmkEdit;
    EdCPLQuebra: TdmkEdit;
    EdCMIQuebra: TdmkEdit;
    EdAGIQuebra: TdmkEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel10: TPanel;
    GroupBox3: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label31: TLabel;
    EdAGIValor: TdmkEdit;
    EdAgiFrete: TdmkEdit;
    EdAGIDevol: TdmkEdit;
    GroupBox4: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label33: TLabel;
    EdCMIValor: TdmkEdit;
    EdCMIFrete: TdmkEdit;
    EdCMIDevol: TdmkEdit;
    DBGrid1: TDBGrid;
    PnInsUpd: TPanel;
    Label36: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    EdProcede: TdmkEditCB;
    CBProcede: TdmkDBLookupComboBox;
    EdPNF_Fat2: TdmkEdit;
    EdCusto: TdmkEdit;
    EdPreco: TdmkEdit;
    Panel14: TPanel;
    PnControla: TPanel;
    BtRateia: TBitBtn;
    BtInsere: TBitBtn;
    Panel15: TPanel;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    PnConfirma: TPanel;
    Panel16: TPanel;
    BitBtn4: TBitBtn;
    BitBtn7: TBitBtn;
    StaticText1: TStaticText;
    DsProcede: TDataSource;
    QrProcede: TmySQLQuery;
    QrMPInRat: TmySQLQuery;
    QrMPInRatProcedeCod: TIntegerField;
    QrMPInRatProcedeNom: TWideStringField;
    QrMPInRatPecas: TFloatField;
    QrMPInRatPecasNF: TFloatField;
    QrMPInRatPNF: TFloatField;
    QrMPInRatPLE: TFloatField;
    QrMPInRatPDA: TFloatField;
    QrMPInRatPreco: TFloatField;
    QrMPInRatCusto: TFloatField;
    QrMPInRatControle: TAutoIncField;
    QrMPInRatPNF_Fat: TFloatField;
    DsMPInRat: TDataSource;
    QrB: TmySQLQuery;
    QrBProcedeCod: TIntegerField;
    QrBProcedeNom: TWideStringField;
    QrBPecas: TFloatField;
    QrBPecasNF: TFloatField;
    QrBPNF: TFloatField;
    QrBPLE: TFloatField;
    QrBPDA: TFloatField;
    QrBPreco: TFloatField;
    QrBCusto: TFloatField;
    QrBControle: TAutoIncField;
    QrA: TmySQLQuery;
    QrAProcedeCod: TIntegerField;
    QrAProcedeNom: TWideStringField;
    QrAPecas: TFloatField;
    QrAPecasNF: TFloatField;
    QrAPNF: TFloatField;
    QrAPLE: TFloatField;
    QrAPDA: TFloatField;
    QrAPreco: TFloatField;
    QrACusto: TFloatField;
    QrAControle: TAutoIncField;
    LaTipoRat: TLabel;
    QrSumRat: TmySQLQuery;
    QrSumRatCusto: TFloatField;
    EdFrete: TdmkEdit;
    Label41: TLabel;
    EdNF2: TdmkEdit;
    Label42: TLabel;
    EdConheci2: TdmkEdit;
    Label43: TLabel;
    QrMPInRatFrete: TFloatField;
    QrMPInRatNF: TIntegerField;
    QrMPInRatConheci: TIntegerField;
    QrSumRatFrete: TFloatField;
    Panel17: TPanel;
    Label6: TLabel;
    TPEmissao: TDateTimePicker;
    TPVencto: TDateTimePicker;
    Label45: TLabel;
    Label2: TLabel;
    EdNF: TdmkEdit;
    EdConheci: TdmkEdit;
    Label44: TLabel;
    TPEmissao2: TDateTimePicker;
    Label46: TLabel;
    Label47: TLabel;
    TPVencto2: TDateTimePicker;
    TPEmisFrete2: TDateTimePicker;
    Label48: TLabel;
    Label49: TLabel;
    TPVctoFrete2: TDateTimePicker;
    Label50: TLabel;
    Label51: TLabel;
    Panel18: TPanel;
    Memo1: TMemo;
    GroupBox9: TGroupBox;
    Label52: TLabel;
    EdPS_ValUni: TdmkEdit;
    EdPS_QtdTot: TdmkEdit;
    Label53: TLabel;
    EdPS_ValTot: TdmkEdit;
    Label54: TLabel;
    CkNF_Inn_Fut: TCheckBox;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label5: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    Label27: TLabel;
    EdCustoInfo: TdmkEdit;
    EdFreteInfo: TdmkEdit;
    EdTotalInfo: TdmkEdit;
    EdFinalInfo: TdmkEdit;
    EdQuebraViagm: TdmkEdit;
    Panel19: TPanel;
    GroupBox10: TGroupBox;
    Label55: TLabel;
    dmkEdit1: TdmkEdit;
    Label56: TLabel;
    dmkEdit2: TdmkEdit;
    Label57: TLabel;
    dmkEdit3: TdmkEdit;
    Label58: TLabel;
    dmkEdit4: TdmkEdit;
    GroupBox11: TGroupBox;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    dmkEdit5: TdmkEdit;
    dmkEdit6: TdmkEdit;
    dmkEdit7: TdmkEdit;
    dmkEdit8: TdmkEdit;
    QrProcedeCodigo: TIntegerField;
    QrProcedeNOMEENTIDADE: TWideStringField;
    BitBtn2: TBitBtn;
    EdPecas_Fat2: TdmkEdit;
    Label63: TLabel;
    EdNF_Modelo: TdmkEdit;
    Label64: TLabel;
    EdNF_Serie: TdmkEdit;
    Label65: TLabel;
    EdCFOP: TdmkEdit;
    Label66: TLabel;
    Label67: TLabel;
    EdEmitNFAvul: TdmkEditCB;
    CBEmitNFAvul: TdmkDBLookupComboBox;
    BitBtn3: TBitBtn;
    QrEmit: TmySQLQuery;
    DsEmit: TDataSource;
    QrEmitCodigo: TIntegerField;
    QrEmitNOMEENTIDADE: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel20: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel21: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    TPEmisFrete: TdmkEditDateTimePicker;
    TPVctoFrete: TdmkEditDateTimePicker;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPNF_FatxxxChange(Sender: TObject);
    procedure EdPecasNFChange(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure QrCambiosAfterOpen(DataSet: TDataSet);
    procedure EdDevolInfoExit(Sender: TObject);
    procedure EdPecasChange(Sender: TObject);
    procedure EdPLEChange(Sender: TObject);
    procedure EdDolarChange(Sender: TObject);
    procedure EdEuroChange(Sender: TObject);
    procedure EdIndexadorChange(Sender: TObject);
    procedure Entidades1Click(Sender: TObject);
    procedure FornecedoresdeMP1Click(Sender: TObject);
    procedure EdForneceChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure EdProcedeChange(Sender: TObject);
    procedure BtInsereClick(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure EdPDAChange(Sender: TObject);
    procedure EdIndexadorExit(Sender: TObject);
    procedure EdConheciExit(Sender: TObject);
    procedure EdPS_ValUniChange(Sender: TObject);
    procedure EdPS_QtdTotChange(Sender: TObject);
    procedure EdPS_ValTotChange(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure EdPecas_Fat2Change(Sender: TObject);
    procedure EdCustoChange(Sender: TObject);
    procedure EdPrecoChange(Sender: TObject);
    procedure EdPecas_Fat2Exit(Sender: TObject);
    procedure EdCustoExit(Sender: TObject);
    procedure EdPrecoExit(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
    FPS_ComoCalc: TPS_ComoCalc;
    procedure CalculaCouroInfo;
    procedure CalculaTotalInfo;
    function CalculaPreco(Preco: Double; Moeda: Integer): Double;
    function CalculaCusto(Unidade, Fonte: Integer; Preco: Double): Double;
    procedure RedefineMoedas;
    procedure ReopenEntiMP();
    procedure MostraEdicaoRat(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure ReopenMPInRat(Controle: Integer);
    procedure Rateia(Controle: Integer);
    procedure InsereOne();
    procedure InsereMul();
    procedure CalculaValorPrestacaoDeServico(ComoCalc: TPS_ComoCalc);
    procedure CalculaQtdePreco(QuemChamou: Integer);
  public
    { Public declarations }
    FAtivou: Boolean;
    FCodigo, FConta, FControle: Integer;
    FEntradaPorXML: Boolean;
    //
    procedure ReopenCambio;
  end;

  var
  FmMPInIts1: TFmMPInIts1;

implementation

uses UnMyObjects, MPIn, UmySQLModule, Module, UnInternalConsts, Principal, EntiMP, MyDBCheck,
  ModuleGeral;

{$R *.DFM}

procedure TFmMPInIts1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPInIts1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  PageControl1.Pages[1].Visible := False;
  if not FAtivou then
  begin
    FAtivou := True;
    if ImgTipo.SQLType = stIns then
    begin
      EdCMPFrete.ValueVariant := QrEntiMPCMPFrete.Value;
      EdCPLFrete.ValueVariant := QrEntiMPCPLFrete.Value;
      EdAGIFrete.ValueVariant := QrEntiMPAGIFrete.Value;
      EdCMIFrete.ValueVariant := QrEntiMPCMIFrete.Value;
      CalculaTotalInfo;
      ReopenMPInRat(0);
    end;
  end;
end;

procedure TFmMPInIts1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPInIts1.BtOKClick(Sender: TObject);
begin
  if (EdNF.ValueVariant = 0) and (CkNF_Inn_Fut.Checked = False) then
  begin
    Geral.MB_Aviso('Inclus�o / altera��o cancelada!' + sLineBreak +
    'O n�mero da NF dever� ser informada, caso n�o seja marcada a op��o "' +
    CkNF_Inn_Fut.Caption + '"');
    Exit;
  end;
  if QrMPInRat.RecordCount = 0 then
    InsereOne()
  else
    InsereMul();
  DMod.AtualizaMPIn(FmMPIn.QrMPInControle.Value);
  if FEntradaPorXML then Close;
end;

procedure TFmMPInIts1.InsereOne();
var
  Conta: Integer;
  Cotacao, Reais: Double;
begin
  if FConta = 0 then
    Conta := UMyMod.BuscaEmLivreY_Def('MPInIts', 'Conta', ImgTipo.SQLType, FConta)
  else
    Conta := FConta;  
  case QrEntiMPCMPMoeda.Value of
    1: Cotacao := QrCambiosDolar.Value;
    2: Cotacao := QrCambiosEuro.Value;
    3: Cotacao := QrCambiosIndexador.Value;
    else Cotacao := 1;
  end;
  Reais := Cotacao * QrEntiMPCMPPreco.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'MPInIts', False, [
    'Codigo', 'Controle', 'Pecas',
    'PecasNF', 'M2', 'PNF', //'PNF_Fat',
    'PLE', 'PDA',
    'CMPValor', 'CMPFrete',
    'CPLValor', 'CPLFrete',
    'AGIValor', 'AGIFrete',
    'CMIValor', 'CMIFrete',
    'CMPDevol', 'CPLDevol',
    'AGIDevol', 'CMIDevol',
    'CustoInfo', 'FreteInfo',
    'TotalInfo', 'FinalInfo',
    'Caminhao',
    'CMPUnida', 'CMPPreco',
    'CMPMoeda', 'CMPCotac',
    'CMPReais', 'Fornece',
    'NF', 'Conheci',
    'Emissao', 'Vencto',
    'EmisFrete', 'VctoFrete',
    'PS_ValUni', 'PS_QtdTot',
    'PS_ValTot', 'NF_Inn_Fut',
    'NF_modelo', 'NF_Serie',
    'CFOP', 'EmitNFAvul'
  ], ['Conta'], [
    FCodigo, FControle, EdPecas.ValueVariant,
    EdPecasNF.ValueVariant, 0, EdPNF.ValueVariant, //EdPNF_Fat.ValueVariant,
    EdPLE.ValueVariant, EdPDA.ValueVariant,
    EdCMPValor.ValueVariant, EdCMPFrete.ValueVariant,
    EdCPLValor.ValueVariant, EdCPLFrete.ValueVariant,
    EdAGIValor.ValueVariant, EdAGIFrete.ValueVariant,
    EdCMIValor.ValueVariant, EdCMIFrete.ValueVariant,
    EdCMPDevol.ValueVariant, EdCMPDevol.ValueVariant,
    EdAGIDevol.ValueVariant, EdCMIDevol.ValueVariant,
    EdCustoInfo.ValueVariant, EdFreteInfo.ValueVariant,
    EdTotalInfo.ValueVariant, EdFinalInfo.ValueVariant,
    EdCaminhao.ValueVariant,
    QrEntiMPCMPUnida.Value, QrEntiMPCMPPreco.Value,
    QrEntiMPCMPMoeda.Value, Cotacao,
    Reais, Geral.IMV(EdFornece.Text),
    EdNF.ValueVariant, EdConheci.ValueVariant,
    Geral.FDT(TPEmissao.Date, 1), Geral.FDT(TPVencto.Date, 1),
    Geral.FDT(TPEmisFrete.Date, 1), Geral.FDT(TPVctoFrete.Date, 1),
    EdPS_ValUni.ValueVariant, EdPS_QtdTot.ValueVariant,
    EdPS_ValTot.ValueVariant, Geral.BoolToInt(CkNF_Inn_Fut.Checked),
    EdNF_modelo.Text, EdNF_Serie.Text,
    FormatFloat('0000', EdCFOP.ValueVariant), EdEmitNFAvul.ValueVariant
  ], [Conta], True) then
  begin
    FmMPIn.FControle := FControle;
    FmMPIn.FConta    := Conta;
    FmMPIn.AtualizaMPIn(FControle);
    if ImgTipo.SQLType = stIns then
    begin
      EdCaminhao.ValueVariant   := EdCaminhao.ValueVariant + 1;
      EdPecasNF.ValueVariant    := 0;
      EdPNF.ValueVariant        := 0;
      EdPecas.ValueVariant      := 0;
      EdPLE.ValueVariant        := 0;
      EdPDA.ValueVariant        := 0;
      EdCustoInfo.ValueVariant  := 0;
      EdNF.ValueVariant         := 0;
      EdNF_Modelo.Text          := '';
      EdNF_Serie.Text           := '';
      EdConheci.ValueVariant    := 0;
      // deixar o mesmo valor
      //EdFreteInfo.ValueVariant    := 0;
      //
      EdPecasNF.SetFocus;
    end else Close;
  end;
end;

procedure TFmMPInIts1.InsereMul();
var
  Conta: Integer;
  Cotacao, Reais: Double;
begin
  Conta := 0;
  case QrEntiMPCMPMoeda.Value of
    1: Cotacao := QrCambiosDolar.Value;
    2: Cotacao := QrCambiosEuro.Value;
    3: Cotacao := QrCambiosIndexador.Value;
    else Cotacao := 1;
  end;
  Reais := Cotacao * QrEntiMPCMPPreco.Value;

  //


  QrMPInRat.First;
  while not QrMPInRat.Eof do
  begin
    {
    EdCustoInfo.ValueVariant :=
      EdCMPValor.ValueVariant +
      EdCPLValor.ValueVariant +
      EdAGIValor.ValueVariant +
      EdCMIValor.ValueVariant;
    //
    EdFreteInfo.ValueVariant :=
      EdCMPFrete.ValueVariant +
      EdCPLFrete.ValueVariant +
      EdAGIFrete.ValueVariant +
      EdCMIFrete.ValueVariant;
    //
    EdTotalInfo.ValueVariant :=
    EdCustoInfo.ValueVariant +
    EdFreteInfo.ValueVariant;
    //
    EdFinalInfo.ValueVariant :=
    EdTotalInfo.ValueVariant -
    //EdDevolInfo.ValueVariant;
      EdCMPDevol.ValueVariant;
    }

    Conta := UMyMod.BuscaEmLivreY_Def('MPInIts', 'Conta', ImgTipo.SQLType, FConta);
    // if
    UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'MPInIts', False, [
      'Codigo', 'Controle', 'Pecas',
      'PecasNF', 'M2', 'PNF', 'PNF_Fat',
      'PLE', 'PDA',
      'CMPValor', 'CMPFrete',
      'CPLValor', 'CPLFrete',
      'AGIValor', 'AGIFrete',
      'CMIValor', 'CMIFrete',
      'CMPDevol', 'CPLDevol',
      'AGIDevol', 'CMIDevol',
      'CustoInfo', 'FreteInfo',
      'TotalInfo', 'FinalInfo',
      'Caminhao',
      'CMPUnida', 'CMPPreco',
      'CMPMoeda', 'CMPCotac',
      'CMPReais', 'Fornece',
      'NF', 'Conheci',
      'Emissao', 'Vencto',
      'EmisFrete', 'VctoFrete'
    ], ['Conta'], [
      FCodigo, FControle, QrMPInRatPecas.Value,
      QrMPInRatPecasNF.Value, 0, QrMPInRatPNF.Value, QrMPInRatPNF_Fat.Value,
      QrMPInRatPLE.Value, QrMPInRatPDA.Value,
      QrMPInRatCusto.Value, QrMPInRatFrete.Value,
      //EdCPLValor.ValueVariant, EdCPLFrete.ValueVariant,
      0, 0,
      //EdAGIValor.ValueVariant, EdAGIFrete.ValueVariant,
      0, 0,
      //EdCMIValor.ValueVariant, EdCMIFrete.ValueVariant,
      0, 0,
      //EdCMPDevol.ValueVariant, EdCMPDevol.ValueVariant,
      0, 0,
      //EdAGIDevol.ValueVariant, EdCMIDevol.ValueVariant, *)
      0, 0,
      //EdCustoInfo.ValueVariant, EdFreteInfo.ValueVariant,
      QrMPInRatCusto.Value, QrMPInRatFrete.Value,
      //EdTotalInfo.ValueVariant, EdFinalInfo.ValueVariant,
      QrMPInRatCusto.Value + QrMPInRatFrete.Value, QrMPInRatCusto.Value + QrMPInRatFrete.Value,
      EdCaminhao.ValueVariant,
      //QrEntiMPCMPUnida.Value, QrEntiMPCMPPreco.Value,
      1, QrMPInRatPreco.Value,
      QrEntiMPCMPMoeda.Value, Cotacao,
      Reais, QrMPInRatProcedeCod.Value,
      EdNF2.ValueVariant, EdConheci2.ValueVariant,
      //QrMPInRatNF.Value, QrMPInRatConheci.Value,
      Geral.FDT(TPEmissao2.Date, 1), Geral.FDT(TPVencto2.Date, 1),
      //QrMPInRatEmi.Value, QrMPInRatConheci.Value,
      Geral.FDT(TPEmisFrete2.Date, 1), Geral.FDT(TPVctoFrete2.Date, 1)
    ], [Conta], True);
    QrMPInRat.Next;
  end;
  begin
    FmMPIn.FControle := FControle;
    FmMPIn.FConta    := Conta;
    FmMPIn.AtualizaMPIn(FControle);
    if ImgTipo.SQLType = stIns then
    begin
      EdCaminhao.ValueVariant := EdCaminhao.ValueVariant + 1;
      EdPecasNF.ValueVariant  := 0;
      EdPNF.ValueVariant      := 0;
      EdPecas.ValueVariant    := 0;
      EdPLE.ValueVariant      := 0;
      EdPDA.ValueVariant      := 0;
      EdCustoInfo.ValueVariant  := 0;
      // deixar o mesmo valor
      //EdFreteInfo.ValueVariant    := 0;
      //
      EdPecasNF.SetFocus;
    end else Close;
  end;
end;

procedure TFmMPInIts1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FEntradaPorXML := False;
  FPS_ComoCalc := psccPeso;
  FAtivou := False;
  TPEmissao.Date     := Date;
  TPVencto.Date      := Date;
  TPEmissao2.Date    := Date + 1;
  TPVencto2.Date     := Date + 1;
  //
  TPEmisFrete.Date   := 0;
  TPVctoFrete.Date   := 0;
  TPEmisFrete2.Date  := 0;
  TPVctoFrete2.Date  := 0;
  //
  PageControl1.ActivePageIndex := 0;
  // As moedas precisam estar definidas antes de
  // conferir valores a couros e fretes
  ReopenCambio;
  RedefineMoedas;
  //
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcede, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEmit, Dmod.MyDB);
  //
end;

procedure TFmMPInIts1.EdPecasNFChange(Sender: TObject);
begin
  CalculaCouroInfo;
end;

procedure TFmMPInIts1.EdPNF_FatxxxChange(Sender: TObject);
begin
  CalculaCouroInfo;
  // Mudado em 2014-02-20 a pedido do Eder do Territoriodo couro
  //EdPS_QtdTot.ValueVariant := EdPNF.ValueVariant;
  //EdPS_QtdTot.ValueVariant := EdPLE.ValueVariant;
  //
  CalculaValorPrestacaoDeServico(psccPeso);
end;

procedure TFmMPInIts1.EdPecasChange(Sender: TObject);
begin
  CalculaCouroInfo;
end;

procedure TFmMPInIts1.EdPLEChange(Sender: TObject);
begin
  CalculaCouroInfo;
  // Mudado em 2014-02-20 a pedido do Eder do Territoriodo couro
  //EdPS_QtdTot.ValueVariant := EdPNF.ValueVariant;
  EdPS_QtdTot.ValueVariant := EdPLE.ValueVariant;
  //
  CalculaValorPrestacaoDeServico(psccPeso);
end;

function TFmMPInIts1.CalculaPreco(Preco: Double; Moeda: Integer): Double;
var
  Index: Double;
begin
  case Moeda of
    1: Index := EdDolar.ValueVariant;
    2: Index := EdEuro.ValueVariant;
    3: Index := EdIndexador.ValueVariant;
    else Index := 1; // Real
  end;
  Result := Index * Preco;
end;

procedure TFmMPInIts1.CalculaQtdePreco(QuemChamou: Integer);
var
  Pecas, Preco, Valor: Double;
begin
  Pecas := EdPecas_Fat2.ValueVariant;
  Preco := EdPreco.ValueVariant;
  Valor := EdCusto.ValueVariant;
  //
  case QuemChamou of
    0:// Pecas
    begin
      if Preco > 0 then
        Valor := Preco * Pecas
      else begin
        if Pecas = 0 then
          Preco := 0
        else
          Preco := Valor / Pecas;
      end;
    end;
    1: // Valor
    begin
      if Pecas > 0 then
        Preco := Valor / Pecas
      else
      begin
        if Preco = 0 then
          Pecas := 0
        else
          Pecas := Valor / Preco;
      end;
    end;
    2: // Pre�o
    begin
      if Pecas > 0 then
        Valor := Preco * Pecas
      else
      begin
        if Preco = 0 then
          Pecas := 0
        else
          Pecas := Valor / Preco;
      end;
    end;
  end;
  EdPecas_Fat2.ValueVariant := Pecas;
  EdPreco.ValueVariant      := Preco;
  EdCusto.ValueVariant      := Valor;
end;

procedure TFmMPInIts1.ReopenCambio;
begin
  { Tabela c�mbios n�o existe mais!
  QrCambios.Close;
  QrCambios.Params[0].AsString := Geral.FDT(FmMPIn.QrMPInData.Value, 1);
  UnDmkDAC_PF.AbreQuery(QrCambios, Dmod.MyDB);
  }
end;

procedure TFmMPInIts1.SpeedButton1Click(Sender: TObject);
begin
  //FmPrincipal.CadastroDeCambio;
  //ReopenCambio;
end;

procedure TFmMPInIts1.RedefineMoedas;
begin
  EdDolar.ValueVariant     := QrCambiosDolar.Value;
  EdEuro.ValueVariant      := QrCambiosEuro.Value;
  EdIndexador.ValueVariant := QrCambiosIndexador.Value;
end;

procedure TFmMPInIts1.SpeedButton2Click(Sender: TObject);
begin
  RedefineMoedas;
end;

procedure TFmMPInIts1.QrCambiosAfterOpen(DataSet: TDataSet);
begin
  RedefineMoedas;
end;

function TFmMPInIts1.CalculaCusto(Unidade, Fonte: Integer; Preco: Double): Double;
var
  Fator: Double;
begin
  case Fonte of
    0:
    case Unidade of
      0: Fator := EdPNF.ValueVariant; // kg NF
      1: Fator := EdPecasNF.ValueVariant; // couro NF
      else Fator := 0; // m2 e outros NF
    end;
    1:
    case Unidade of
      0: Fator := EdPLE.ValueVariant; // kg entrada
      1: Fator := EdPecas.ValueVariant; // couro entrada
      else Fator := 0; // m2 e outros entrada
    end;
    else Fator := 0;
  end;
  Result := Fator * Preco;
end;

procedure TFmMPInIts1.CalculaCouroInfo;
var
  Fator: Double;
  Fonte: Integer;
begin
  Fonte := QrEntiMPCPLSit.Value;
  EdCMPQuebra.ValueVariant := 0;
  EdCPLQuebra.ValueVariant := 0;
  EdAGIQuebra.ValueVariant := 0;
  EdCMIQuebra.ValueVariant := 0;
  //
  if (QrMPInRat.State = dsInactive) or (QrMPInRat.RecordCount = 0) then
  begin
    EdCMPValor.ValueVariant := CalculaCusto(QrEntiMPCMPUnida.Value,
      0,     CalculaPreco(QrEntiMPCMPPreco.Value, QrEntiMPCMPMoeda.Value));
    EdCMPValor.Enabled := True;
    EdCMPFrete.Enabled := True;
    EdCPLValor.Enabled := True;
  end else begin
    QrSumRat.Close;
    UnDmkDAC_PF.AbreQuery(QrSumRat, DModG.MyPID_DB);
    EdCMPValor.ValueVariant := QrSumRatCusto.Value;
    EdCMPFrete.ValueVariant := QrSumRatFrete.Value;
    EdCPLValor.ValueVariant := 0;
    EdCMPValor.Enabled := False;
    EdCMPFrete.Enabled := False;
    EdCPLValor.Enabled := False;
  end;
  //
  EdCPLValor.ValueVariant := CalculaCusto(QrEntiMPCPLUnida.Value,
    Fonte, CalculaPreco(QrEntiMPCPLPreco.Value, QrEntiMPCPLMoeda.Value));
  //
  EdAGIValor.ValueVariant := CalculaCusto(QrEntiMPAGIUnida.Value,
    0,     CalculaPreco(QrEntiMPAGIPreco.Value, QrEntiMPAGIMoeda.Value));
  //
  EdCMIValor.ValueVariant := CalculaCusto(QrEntiMPCMIUnida.Value,
    0,     CalculaPreco(QrEntiMPCMIPreco.Value, QrEntiMPCMIMoeda.Value));

  {  Devolu��o de quebra de viagem }
  // Calcula % de quebra de viagem
  EdCMPDevol.ValueVariant := 0;
  if EdPNF.ValueVariant = 0 then Fator := 0
  else
    Fator := 100 *
    (EdPNF.ValueVariant - EdPLE.ValueVariant) / EdPNF.ValueVariant;
  EdQuebraViagm.ValueVariant := Fator;
  { calcula devolu��o conforme IDQV (Indice de devolu-
    ��o de quebra de viagem: Nada, tudo ou excedente}
  //  CMP
  if Fator > QrEntiMPCMP_LPQV.Value then
  begin
    case QrEntiMPCMP_IDQV.Value of
      0: Fator := 0;
      //1: Fator := Fator;
      2: Fator := Fator - QrEntiMPCMP_LPQV.Value;
    end;
    EdCMPQuebra.ValueVariant := Fator;
    if Fator > 0 then
    //EdDevolInfo.ValueVariant := Fator * EdCustoInfo.ValueVariant / 100;
      EdCMPDevol.ValueVariant := Fator * EdCMPValor.ValueVariant / 100;
  end;
  //  CPL
  if Fator > QrEntiMPCPL_LPQV.Value then
  begin
    case QrEntiMPCPL_IDQV.Value of
      0: Fator := 0;
      //1: Fator := Fator;
      2: Fator := Fator - QrEntiMPCPL_LPQV.Value;
    end;
    EdCPLQuebra.ValueVariant := Fator;
    if Fator > 0 then
    //EdDevolInfo.ValueVariant := Fator * EdCustoInfo.ValueVariant / 100;
      EdCPLDevol.ValueVariant := Fator * EdCPLValor.ValueVariant / 100;
  end;
  //
  //  AGI
  if Fator > QrEntiMPAGI_LPQV.Value then
  begin
    case QrEntiMPAGI_IDQV.Value of
      0: Fator := 0;
      //1: Fator := Fator;
      2: Fator := Fator - QrEntiMPAGI_LPQV.Value;
    end;
    EdAGIQuebra.ValueVariant := Fator;
    if Fator > 0 then
    //EdDevolInfo.ValueVariant := Fator * EdCustoInfo.ValueVariant / 100;
      EdAGIDevol.ValueVariant := Fator * EdAGIValor.ValueVariant / 100;
  end;
  //  CMI
  if Fator > QrEntiMPCMI_LPQV.Value then
  begin
    case QrEntiMPCMI_IDQV.Value of
      0: Fator := 0;
      //1: Fator := Fator;
      2: Fator := Fator - QrEntiMPCMI_LPQV.Value;
    end;
    EdCMIQuebra.ValueVariant := Fator;
    if Fator > 0 then
    //EdDevolInfo.ValueVariant := Fator * EdCustoInfo.ValueVariant / 100;
      EdCMIDevol.ValueVariant := Fator * EdCMIValor.ValueVariant / 100;
  end;
  CalculaTotalInfo;
  Rateia(QrMPInRatControle.Value);
end;

procedure TFmMPInIts1.EdDevolInfoExit(Sender: TObject);
begin
  CalculaTotalInfo;
end;

procedure TFmMPInIts1.CalculaTotalInfo;
begin
  EdCustoInfo.ValueVariant :=
    EdCMPValor.ValueVariant +
    EdCPLValor.ValueVariant +
    EdAGIValor.ValueVariant +
    EdCMIValor.ValueVariant;
  //
  EdFreteInfo.ValueVariant :=
    EdCMPFrete.ValueVariant +
    EdCPLFrete.ValueVariant +
    EdAGIFrete.ValueVariant +
    EdCMIFrete.ValueVariant;
  //
  EdTotalInfo.ValueVariant :=
  EdCustoInfo.ValueVariant +
  EdFreteInfo.ValueVariant;
  //
  EdFinalInfo.ValueVariant :=
  EdTotalInfo.ValueVariant -
  //EdDevolInfo.ValueVariant;
    EdCMPDevol.ValueVariant;
end;

procedure TFmMPInIts1.EdDolarChange(Sender: TObject);
begin
  CalculaCouroInfo;
end;

procedure TFmMPInIts1.EdEuroChange(Sender: TObject);
begin
  CalculaCouroInfo;
end;

procedure TFmMPInIts1.EdIndexadorChange(Sender: TObject);
begin
  CalculaCouroInfo;
end;

procedure TFmMPInIts1.Entidades1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  QrFornece.Close;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdFornece.Text := IntToStr(VAR_ENTIDADE);
    CBFornece.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmMPInIts1.FornecedoresdeMP1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiMP, FmEntiMP, afmoNegarComAviso) then
  begin
    FmEntiMP.ShowModal;
    FmEntiMP.Destroy;
  end;
  QrFornece.Close;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdFornece.Text := IntToStr(VAR_ENTIDADE);
    CBFornece.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmMPInIts1.EdForneceChange(Sender: TObject);
begin
  ReopenEntiMP;
end;

procedure TFmMPInIts1.ReopenEntiMP();
begin
  QrEntiMP.Close;
  QrEntiMP.Params[0].AsInteger := Geral.IMV(EdFornece.Text);
  UnDmkDAC_PF.AbreQuery(QrEntiMP, Dmod.MyDB);
  //
  if ImgTipo.SQLType = stIns then
  begin
    EdCMPFrete.ValueVariant := QrEntiMPCMPFrete.Value;
    EdCPLFrete.ValueVariant := QrEntiMPCPLFrete.Value;
    EdAGIFrete.ValueVariant := QrEntiMPAGIFrete.Value;
    EdCMIFrete.ValueVariant := QrEntiMPCMIFrete.Value;
    //
    CalculaTotalInfo;
  end;
end;

procedure TFmMPInIts1.BitBtn1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFornece, BitBtn1);
end;

procedure TFmMPInIts1.EdPrecoChange(Sender: TObject);
begin
  CalculaCouroInfo;
end;

procedure TFmMPInIts1.EdPrecoExit(Sender: TObject);
begin
  CalculaQtdePreco(2);
end;

procedure TFmMPInIts1.EdProcedeChange(Sender: TObject);
begin
  //EdPreco.ValueVariant := QrProcedeCMPPreco.Value;
end;

procedure TFmMPInIts1.EdPS_QtdTotChange(Sender: TObject);
begin
  if EdPS_QtdTot.Focused then
    CalculaValorPrestacaoDeServico(psccPeso);
end;

procedure TFmMPInIts1.EdPS_ValTotChange(Sender: TObject);
begin
  if EdPS_ValTot.Focused then
    CalculaValorPrestacaoDeServico(psccValT);
end;

procedure TFmMPInIts1.EdPS_ValUniChange(Sender: TObject);
begin
  if EdPS_ValUni.Focused then
    CalculaValorPrestacaoDeServico(psccValU);
end;

procedure TFmMPInIts1.BtInsereClick(Sender: TObject);
begin
  if FEntradaPorXML then
    Geral.MB_Aviso('Entrada por XML n�o permite sub-fornecedores!')
  else MostraEdicaoRat(1, stIns, 0);
end;

procedure TFmMPInIts1.MostraEdicaoRat(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      Rateia(QrMPInRatControle.Value);
      //DBGrid1.Enabled      := True;
      PnControla.Visible   := True;
      PnConfirma.Visible   := False;
      PnInsUpd.Visible     := False;
    end;
    1:
    begin
      PnConfirma.Visible := True;
      PnInsUpd.Visible   := True;
      PnControla.Visible := False;
      if SQLType = stIns then
      begin
        EdProcede.Text              := '';
        CBProcede.KeyValue          := NULL;
        //
        EdPecas_Fat2.ValueVariant := 0;
        EdPNF_Fat2.ValueVariant   := 0;
        EdCusto.ValueVariant      := 0;
        EdPreco.ValueVariant      := 0;
        //
        EdFrete.ValueVariant      := 0;
        EdNF2.ValueVariant        := 0;
        EdConheci2.ValueVariant   := 0;
        //
      end else begin
        EdProcede.Text            := IntToStr(QrMPInRatProcedeCod.Value);
        CBProcede.KeyValue        := QrMPInRatProcedeCod.Value;
        //
        EdPNF_Fat2.ValueVariant   := QrMPInRatPNF_Fat.Value;
        EdCusto.ValueVariant      := QrMPInRatCusto.Value;
        EdPreco.ValueVariant      := QrMPInRatPreco.Value;
        //
        EdFrete.ValueVariant      := QrMPInRatFrete.Value;
        EdNF2.ValueVariant        := QrMPInRatNF.Value;
        EdConheci2.ValueVariant   := QrMPInRatConheci.Value;
        //
      end;
      EdProcede.SetFocus;
    end;
  end;
  LaTipoRat.Caption := DmkEnums.NomeTipoSQL(SQLType);
  //GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo > 0 then
    ReopenMPInRat(Codigo)
end;

procedure TFmMPInIts1.ReopenMPInRat(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMPInRat, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + FmMPIn.FTabLoc_ntrttMPInRat,
    'ORDER BY Controle ',
    '']);
  //
  if Controle <> 0 then
    QrMPInRat.Locate('Controle', Controle, []);
end;

procedure TFmMPInIts1.Rateia(Controle: Integer);
var
  Codigo, CNFt, CLEt, CNFs, CNFi, i: Integer;
  PNFt, PLEt, PDAt, PLEi, PDAi, kgPNF, kgPLE, kgPDA, PNFi, PNFs, PLEs, PDAs: Double;
begin
  PNFs := 0;
  PLEs := 0;
  PDAs := 0;
  ReopenMPInRat(0);
  //
  if QrMPInRat.RecordCount > 0 then
  begin
    Codigo := Geral.IMV(EdFornece.Text);
    CNFt := EdPecasNF.ValueVariant;
    CLEt := EdPecas.ValueVariant;
    PNFt := EdPNF.ValueVariant;
    PLEt := EdPLE.ValueVariant;
    PDAt := EdPDA.ValueVariant;
    //
    if CNFt = 0 then kgPNF := 0 else kgPNF := PNFt / CNFt;
    if CNFt = 0 then kgPLE := 0 else kgPLE := PLEt / CNFt;
    if CNFt = 0 then kgPDA := 0 else kgPDA := PDAt / CNFt;
    //
    CNFs := 0;
    //CLEs := 0;
    //
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Clear;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('UPDATE ' + FmMPIn.FTabLoc_ntrttMPInRat + ' SET ');
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('PecasNF=:P0, Pecas=:P1, PLE=:P2, PDA=:P3, ');
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('PNF=:P4 ');
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('WHERE Controle=:Pa');
    //
    QrA.Close;
    QrA.Params[0].AsInteger := Codigo;
    UnDmkDAC_PF.AbreQuery(QrA, DModG.MyPID_DB);
    //
    QrA.First;
    while not QrA.Eof do
    begin
      if QrAPreco.Value = 0 then CNFi := 0 else
        CNFi := Round(QrACusto.Value / QrAPreco.Value);
      PNFi := Round(kgPNF * CNFi);
      PLEi := Round(kgPLE * CNFi);
      PDAi := Round(kgPDA * CNFi);
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[00].AsFloat   := CNFi;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[01].AsFloat   := CNFi;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[02].AsFloat   := PLEi;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[03].AsFloat   := PDAi;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[04].AsFloat   := PNFi;
      //
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[05].AsInteger := QrAControle.Value;
      (*Dmod.QrUpdL*)DModG.QrUpdPID1.ExecSQL;
      //
      CNFs := CNFs + CNFi;
      //CLEs := CLEs + CLEi;
      PNFs := PNFs + PNFi;
      PLEs := PLEs + PLEi;
      PDAs := PDAs + PDAi;
      QrA.Next;
    end;
    //
    QrB.Close;
    QrB.Params[0].AsInteger := Codigo;
    UnDmkDAC_PF.AbreQuery(QrB, DModG.MyPID_DB);
    i := 0;
    QrB.First;
    while not QrB.Eof do
    begin
      inc(i, 1);
      if i < QrB.RecordCount then
      begin
        if QrBPreco.Value = 0 then CNFi := 0 else
          CNFi := Round(QrBCusto.Value / QrBPreco.Value);
        PNFi := Round(kgPNF * CNFi);
        PLEi := Round(kgPLE * CNFi);
        PDAi := Round(kgPDA * CNFi);
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[00].AsFloat   := CNFi;
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[01].AsFloat   := CNFi;
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[02].AsFloat   := PLEi;
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[03].AsFloat   := PDAi;
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[04].AsFloat   := PNFi;
        //
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[05].AsInteger := QrBControle.Value;
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.ExecSQL;
        //
        CNFs := CNFs + CNFi;
        //CLEs := CLEs + CLEi;
        PNFs := PNFs + PNFi;
        PLEs := PLEs + PLEi;
        PDAs := PDAs + PDAi;
      end else begin
        //
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[00].AsFloat   := CNFt - CNFs;
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[01].AsFloat   := CLEt - CNFs;
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[02].AsFloat   := PLEt - PLEs;
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[03].AsFloat   := PDAt - PDAs;
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[04].AsFloat   := PNFt - PNFs;
        //
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[05].AsInteger := QrBControle.Value;
        (*Dmod.QrUpdL*)DModG.QrUpdPID1.ExecSQL;
        //
      end;
      QrB.Next;
    end;
  end;
  ReopenMPInRat(Controle);
end;

procedure TFmMPInIts1.BitBtn5Click(Sender: TObject);
begin
  MostraEdicaoRat(1, stUpd, QrMPInRatControle.Value);
end;

procedure TFmMPInIts1.BitBtn6Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a retirada do fornecedor selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Clear;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('DELETE FROM ' + FmMPIn.FTabLoc_ntrttMPInRat);
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('WHERE Controle=:P0');
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.SQL.Add('');
    //
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.Params[0].AsInteger := QrMPInRatControle.Value;
    (*Dmod.QrUpdL*)DModG.QrUpdPID1.ExecSQL;
  end;
  Rateia(QrMPInRatControle.Value);
  //
  ReopenMPInRat(0);
end;

procedure TFmMPInIts1.BitBtn7Click(Sender: TObject);
var
  Fornece: Integer;
  x: TSQLType;
begin
  Fornece := Geral.IMV(EdProcede.Text);
  if Fornece = 0 then
  begin
    Application.MessageBox('Informe o fornecedor!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if EdPreco.ValueVariant = 0 then
  begin
    Application.MessageBox('Informe o pre�o por couro!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    EdPreco.SetFocus;
    Exit;
  end;
  //UMyMod.BuscaEmLivreY_Def(Table, Field, LaTipoRat: String; Atual: Integer): Integer;
  if UMyMod.SQLInsUpdL((*Dmod.QrUpdL*)DModG.QrUpdPID1, LaTipoRat.Caption, FmMPIn.FTabLoc_ntrttMPInRat, True, [
    'PNF_Fat', 'Custo',
    'Preco', 'ProcedeNom', 'ProcedeCod',
    'Frete', 'NF',
    'Conheci',
    'Emissao', 'Vencto',
    'EmisFrete', 'VctoFrete',
    'Pecas', 'PecasNF'
    ], ['Controle'], [
    EdPNF_Fat2.ValueVariant, EdCusto.ValueVariant,
    Edpreco.ValueVariant, CBProcede.Text, Fornece,
    EdFrete.ValueVariant, EdNF2.ValueVariant,
    EdConheci2.ValueVariant,
    EdNF2.ValueVariant, EdConheci2.ValueVariant,
    Geral.FDT(TPEmissao2.Date, 1), Geral.FDT(TPVencto2.Date, 1),
    Geral.FDT(TPEmisFrete2.Date, 1), Geral.FDT(TPVctoFrete2.Date, 1),
    EdPecas_Fat2.ValueVariant, EdPecas_Fat2.ValueVariant
    ], [QrMPInRatControle.Value]) then
    begin
     // Antes de abrir para calcular correto
     //Rateia();
     x := ImgTipo.SQLType;
     MostraEdicaoRat(0, stLok, QrMPInRatControle.Value);
     if x = stIns then
       MostraEdicaoRat(1, stIns, 0);
    end;
end;

procedure TFmMPInIts1.BitBtn2Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  QrProcede.Close;
  UnDmkDAC_PF.AbreQuery(QrProcede, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdProcede.Text := IntToStr(VAR_ENTIDADE);
    CBProcede.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmMPInIts1.BitBtn3Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmMPInIts1, FmMPInIts1, afmoNegarComAviso) then
  begin
    FmMPInIts1.ShowModal;
    FmMPInIts1.Destroy;
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodigoPesquisado(EdEmitNFAvul,
      CBEmitNFAvul, QrEmit, VAR_CADASTRO);
  end;
end;

procedure TFmMPInIts1.BitBtn4Click(Sender: TObject);
begin
  MostraEdicaoRat(0, stLok, 0);
end;

procedure TFmMPInIts1.EdPDAChange(Sender: TObject);
begin
  CalculaCouroInfo;
end;

procedure TFmMPInIts1.EdIndexadorExit(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: TPEmissao.SetFocus;
    1: BtInsere.SetFocus;
  end;
end;

procedure TFmMPInIts1.EdConheciExit(Sender: TObject);
begin
  BtOk.SetFocus;
end;

procedure TFmMPInIts1.EdCustoChange(Sender: TObject);
begin
  CalculaCouroInfo;
end;

procedure TFmMPInIts1.EdCustoExit(Sender: TObject);
begin
  CalculaQtdePreco(1);
end;

procedure TFmMPInIts1.CalculaValorPrestacaoDeServico(ComoCalc: TPS_ComoCalc);
var
  Qtde, ValU, ValT: Double;
begin
  Qtde := EdPS_QtdTot.ValueVariant;
  ValU := EdPS_ValUni.ValueVariant;
  ValT := EdPS_ValTot.ValueVariant;
  //
  case ComoCalc of
    psccPeso: ValT := ValU * Qtde;
    psccValU: ValT := ValU * Qtde;
    psccValT:
    begin
      if Qtde > 0  then
        ValU := ValT / Qtde
      else
      if ValU > 0  then
        Qtde := ValT / ValU;
    end;
  end;
  //
  if ComoCalc <> psccPeso then
    EdPS_QtdTot.ValueVariant := Qtde;
  if ComoCalc <> psccValU then
    EdPS_ValUni.ValueVariant := ValU;
  if ComoCalc <> psccValT then
    EdPS_ValTot.ValueVariant := ValT;
end;
  
procedure TFmMPInIts1.EdPecas_Fat2Change(Sender: TObject);
begin
  CalculaCouroInfo;
end;

procedure TFmMPInIts1.EdPecas_Fat2Exit(Sender: TObject);
begin
  CalculaQtdePreco(0);
end;

end.

