unit OpcoesBlueDerm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ExtDlgs, DBCtrls, Db,
  mySQLDbTables, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkRadioGroup,
  dmkGeral, UnDmkProcFunc, dmkImage, AppListas, UnDmkEnums, dmkCheckBox,
  UnGrl_Consts, UnProjGroup_Consts, dmkCheckGroup, Vcl.Menus, dmkEditEdIdx,
  dmkEditEdTxt;

type
  TFmOpcoesBlueDerm = class(TForm)
    Panel1: TPanel;
    PCGeral: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    OpenPictureDialog1: TOpenPictureDialog;
    TabSheet3: TTabSheet;
    QrContasPQCompr: TMySQLQuery;
    QrContasPQComprCodigo: TIntegerField;
    QrContasPQComprNome: TWideStringField;
    DsContasPQCompr: TDataSource;
    QrListaSetores1: TmySQLQuery;
    DsListaSetores1: TDataSource;
    QrListaSetores1Codigo: TIntegerField;
    QrListaSetores1Nome: TWideStringField;
    QrListaSetores2: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsListaSetores2: TDataSource;
    TabSheet4: TTabSheet;
    QrPrdGrupTip: TmySQLQuery;
    DsPrdGrupTip: TDataSource;
    Panel4: TPanel;
    RGCasasProd: TdmkRadioGroup;
    Label10: TLabel;
    EdPQ_PrdGruTip: TdmkEditCB;
    CBPQ_PrdGruTip: TdmkDBLookupComboBox;
    SpeedButton3: TSpeedButton;
    Panel5: TPanel;
    Label53: TLabel;
    EdCtaPQCompr: TdmkEditCB;
    Label4: TLabel;
    EdCtaMPCompr: TdmkEditCB;
    CBCtaMPCompr: TdmkDBLookupComboBox;
    CBCtaPQCompr: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdCtaCVVenda: TdmkEditCB;
    CBCtaCVVenda: TdmkDBLookupComboBox;
    EdCtaCVFrete: TdmkEditCB;
    CBCtaCVFrete: TdmkDBLookupComboBox;
    Label7: TLabel;
    CBCtaMPFrete: TdmkDBLookupComboBox;
    EdCtaMPFrete: TdmkEditCB;
    Label5: TLabel;
    EdCtaPQFrete: TdmkEditCB;
    Label3: TLabel;
    CBCtaPQFrete: TdmkDBLookupComboBox;
    Panel6: TPanel;
    Label1: TLabel;
    EdPathLogoDupl: TEdit;
    Label2: TLabel;
    EdPathLogoRece: TEdit;
    SpeedButton2: TSpeedButton;
    SpeedButton1: TSpeedButton;
    CkPQNegBaixa: TCheckBox;
    CkPQNegEntra: TCheckBox;
    CkCanAltBalA: TCheckBox;
    RGImpRecRib: TRadioGroup;
    RGVendaCouro: TRadioGroup;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    EdSetorCal: TdmkEditCB;
    CBSetorCal: TdmkDBLookupComboBox;
    CBSetorCur: TdmkDBLookupComboBox;
    EdSetorCur: TdmkEditCB;
    Panel3: TPanel;
    EdServSMTP: TdmkEdit;
    Label20: TLabel;
    Label24: TLabel;
    EdConexaoDialUp: TdmkEdit;
    Label11: TLabel;
    EdPQ_StqCenCad: TdmkEditCB;
    CBPQ_StqCenCad: TdmkDBLookupComboBox;
    SpeedButton4: TSpeedButton;
    QrStqCenCad: TmySQLQuery;
    DsStqCenCad: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    Label25: TLabel;
    Label23: TLabel;
    Label22: TLabel;
    Label21: TLabel;
    Label26: TLabel;
    EdDonoMailOC: TdmkEdit;
    EdMailCCCega: TdmkEdit;
    EdMailOC: TdmkEdit;
    EdNomeMailOC: TdmkEdit;
    EdCorpoMailOC: TMemo;
    RGWBNivGer: TdmkRadioGroup;
    TabSheet5: TTabSheet;
    Panel2: TPanel;
    CkOSImpInfWB: TdmkCheckBox;
    CkUsaM2Medio: TdmkCheckBox;
    CkUsaBRLMedM2: TdmkCheckBox;
    CkUsaEmitGru: TdmkCheckBox;
    RGVerImprRecRib: TdmkRadioGroup;
    TabSheet6: TTabSheet;
    Panel10: TPanel;
    CkInfoMulFrnImpVS: TdmkCheckBox;
    CkCanAltBalVS: TdmkCheckBox;
    RGQtdBoxClas: TdmkRadioGroup;
    Label12: TLabel;
    EdVSImpRandStr: TdmkEdit;
    CGNObrigNFeVS: TdmkCheckGroup;
    TsSPED: TTabSheet;
    Panel11: TPanel;
    GroupBox3: TGroupBox;
    RGSPED_II_PDA: TdmkRadioGroup;
    RGSPED_II_EmCal: TdmkRadioGroup;
    RGSPED_II_Caleado: TdmkRadioGroup;
    RGSPED_II_DTA: TdmkRadioGroup;
    RGSPED_II_EmCur: TdmkRadioGroup;
    RGSPED_II_ArtCur: TdmkRadioGroup;
    RGSPED_II_Operacao: TdmkRadioGroup;
    RGSPED_II_Recurt: TdmkRadioGroup;
    RGSPED_II_Curtido: TdmkRadioGroup;
    BtPadroesSPED_II: TBitBtn;
    RGSPED_II_SubProd: TdmkRadioGroup;
    RGSPED_II_Reparo: TdmkRadioGroup;
    CkVSWarSemNF: TdmkCheckBox;
    CkVSWarNoFrn: TdmkCheckBox;
    CkVSInsPalManu: TdmkCheckBox;
    Label13: TLabel;
    EdDBClarecoCDR: TdmkEdit;
    RGImpRecAcabto: TRadioGroup;
    PMPadroesSPED_II: TPopupMenu;
    Isolada1: TMenuItem;
    Conjunta1: TMenuItem;
    CkVSImpMediaPall: TdmkCheckBox;
    EdM2DiaAcabPCP: TdmkEdit;
    Label14: TLabel;
    Label15: TLabel;
    EdCtaComisCMP: TdmkEditCB;
    CBCtaComisCMP: TdmkDBLookupComboBox;
    QrContasMPCompr: TMySQLQuery;
    QrContasMPComprCodigo: TIntegerField;
    QrContasMPComprNome: TWideStringField;
    DsContasMPCompr: TDataSource;
    QrContasCVVenda: TMySQLQuery;
    QrContasCVVendaCodigo: TIntegerField;
    QrContasCVVendaNome: TWideStringField;
    DsContasCVVenda: TDataSource;
    QrContasComisCMP: TMySQLQuery;
    QrContasComisCMPCodigo: TIntegerField;
    QrContasComisCMPNome: TWideStringField;
    DsContasComisCMP: TDataSource;
    QrContasPQFrete: TMySQLQuery;
    QrContasPQFreteCodigo: TIntegerField;
    QrContasPQFreteNome: TWideStringField;
    DsContasPQFrete: TDataSource;
    QrContasMPFrete: TMySQLQuery;
    QrContasMPFreteCodigo: TIntegerField;
    QrContasMPFreteNome: TWideStringField;
    DsContasMPFrete: TDataSource;
    QrContasCVFrete: TMySQLQuery;
    QrContasCVFreteCodigo: TIntegerField;
    QrContasCVFreteNome: TWideStringField;
    DsContasCVFrete: TDataSource;
    Label16: TLabel;
    CkNaoRecupImposPQ: TCheckBox;
    TabSheet7: TTabSheet;
    Panel12: TPanel;
    QrCreTrib_MP_CFOP_DE: TMySQLQuery;
    QrCreTrib_MP_CFOP_DECodigo: TLargeintField;
    QrCreTrib_MP_CFOP_DENome: TWideStringField;
    DsCreTrib_MP_CFOP_DE: TDataSource;
    PageControl1: TPageControl;
    TabSheet8: TTabSheet;
    Panel13: TPanel;
    Panel14: TPanel;
    GBRecuImpost: TGroupBox;
    Panel15: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label27: TLabel;
    Label476: TLabel;
    Label477: TLabel;
    Label478: TLabel;
    Label480: TLabel;
    Label28: TLabel;
    EdCreTrib_MP_ICMS_ALIQ_ST: TdmkEdit;
    EdCreTrib_MP_PIS_ALIQ: TdmkEdit;
    EdCreTrib_MP_COFINS_ALIQ: TdmkEdit;
    EdCreTrib_MP_IPI_ALIQ: TdmkEdit;
    EdCreTrib_MP_ICMS_CST: TdmkEdit;
    EdCreTrib_MP_IPI_CST: TdmkEdit;
    EdCreTrib_MP_IPI_CST_TXT: TdmkEdit;
    EdCreTrib_MP_PIS_CST: TdmkEdit;
    EdCreTrib_MP_PIS_CST_TXT: TdmkEdit;
    EdCreTrib_MP_COFINS_CST: TdmkEdit;
    EdCreTrib_MP_COFINS_CST_TXT: TdmkEdit;
    EdCreTrib_MP_ICMS_ALIQ: TdmkEdit;
    Label66: TLabel;
    EdCreTrib_MP_CFOP_DE: TdmkEditCB;
    CBCreTrib_MP_CFOP_DE: TdmkDBLookupComboBox;
    SbCreTrib_MP_CFOP_DE: TSpeedButton;
    Label29: TLabel;
    EdCreTrib_MP_CFOP_FE: TdmkEditCB;
    CBCreTrib_MP_CFOP_FE: TdmkDBLookupComboBox;
    SbCreTrib_MP_CFOP_FE: TSpeedButton;
    EdCreTrib_MP_ICMS_CST_TXT: TdmkEdit;
    QrCreTrib_MP_CFOP_FE: TMySQLQuery;
    QrCreTrib_MP_CFOP_FECodigo: TLargeintField;
    QrCreTrib_MP_CFOP_FENome: TWideStringField;
    DsCreTrib_MP_CFOP_FE: TDataSource;
    TabSheet9: TTabSheet;
    Panel16: TPanel;
    Panel17: TPanel;
    Label30: TLabel;
    Label31: TLabel;
    EdRemTrib_MP_CFOP_DE: TdmkEditCB;
    CBRemTrib_MP_CFOP_DE: TdmkDBLookupComboBox;
    EdRemTrib_MP_CFOP_FE: TdmkEditCB;
    CBRemTrib_MP_CFOP_FE: TdmkDBLookupComboBox;
    GroupBox4: TGroupBox;
    Panel18: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    EdRemTrib_MP_ICMS_ALIQ_ST: TdmkEdit;
    EdRemTrib_MP_PIS_ALIQ: TdmkEdit;
    EdRemTrib_MP_COFINS_ALIQ: TdmkEdit;
    EdRemTrib_MP_IPI_ALIQ: TdmkEdit;
    EdRemTrib_MP_ICMS_CST: TdmkEdit;
    EdRemTrib_MP_IPI_CST: TdmkEdit;
    EdRemTrib_MP_IPI_CST_TXT: TdmkEdit;
    EdRemTrib_MP_PIS_CST: TdmkEdit;
    EdRemTrib_MP_PIS_CST_TXT: TdmkEdit;
    EdRemTrib_MP_COFINS_CST: TdmkEdit;
    EdRemTrib_MP_COFINS_CST_TXT: TdmkEdit;
    EdRemTrib_MP_ICMS_ALIQ: TdmkEdit;
    EdRemTrib_MP_ICMS_CST_TXT: TdmkEdit;
    QrRemTrib_MP_CFOP_FE: TMySQLQuery;
    QrRemTrib_MP_CFOP_FECodigo: TLargeintField;
    QrRemTrib_MP_CFOP_FENome: TWideStringField;
    DsRemTrib_MP_CFOP_FE: TDataSource;
    QrRemTrib_MP_CFOP_DE: TMySQLQuery;
    QrRemTrib_MP_CFOP_DECodigo: TLargeintField;
    QrRemTrib_MP_CFOP_DENome: TWideStringField;
    DsRemTrib_MP_CFOP_DE: TDataSource;
    EdCreTrib_MP_TES_ICMS_TXT: TdmkEdit;
    EdCreTrib_MP_TES_ICMS: TdmkEdit;
    EdCreTrib_MP_TES_IPI: TdmkEdit;
    EdCreTrib_MP_TES_IPI_TXT: TdmkEdit;
    EdCreTrib_MP_TES_PIS: TdmkEdit;
    EdCreTrib_MP_TES_PIS_TXT: TdmkEdit;
    EdCreTrib_MP_TES_COFINS: TdmkEdit;
    EdCreTrib_MP_TES_COFINS_TXT: TdmkEdit;
    EdStqCenCadEstqPall_TXT: TdmkEdit; // TdmkEditedTxt
    Label41: TLabel;
    EdStqCenCadEstqPall: TdmkEditEdIdx;
    CkGruUsrEdtRecei: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtPadroesSPED_IIClick(Sender: TObject);
    procedure Isolada1Click(Sender: TObject);
    procedure Conjunta1Click(Sender: TObject);
    procedure SbCreTrib_MP_CFOP_DEClick(Sender: TObject);
    procedure SbCreTrib_MP_CFOP_FEClick(Sender: TObject);
    procedure EdCreTrib_MP_IPI_CSTChange(Sender: TObject);
    procedure EdCreTrib_MP_IPI_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCreTrib_MP_PIS_CSTChange(Sender: TObject);
    procedure EdCreTrib_MP_PIS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCreTrib_MP_COFINS_CSTChange(Sender: TObject);
    procedure EdCreTrib_MP_COFINS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCreTrib_MP_ICMS_CSTChange(Sender: TObject);
    procedure EdCreTrib_MP_ICMS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdRemTrib_MP_ICMS_CSTChange(Sender: TObject);
    procedure EdRemTrib_MP_ICMS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdRemTrib_MP_IPI_CSTChange(Sender: TObject);
    procedure EdRemTrib_MP_IPI_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdRemTrib_MP_PIS_CSTChange(Sender: TObject);
    procedure EdRemTrib_MP_PIS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdRemTrib_MP_COFINS_CSTChange(Sender: TObject);
    procedure EdRemTrib_MP_COFINS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCreTrib_MP_TES_ICMSChange(Sender: TObject);
    procedure EdCreTrib_MP_TES_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCreTrib_MP_TES_IPIChange(Sender: TObject);
    procedure EdCreTrib_MP_TES_IPIKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCreTrib_MP_TES_PISChange(Sender: TObject);
    procedure EdCreTrib_MP_TES_PISKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCreTrib_MP_TES_COFINSChange(Sender: TObject);
    procedure EdCreTrib_MP_TES_COFINSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    F_TES_ITENS: MyArrayLista;
    //
    procedure PadroesRegistrosSPED_ICMS_IPI(Sender: TObject);
    procedure ReopenCFOP_DE();
    procedure ReopenCFOP_FE();

  public
    { Public declarations }
  end;

  var
  FmOpcoesBlueDerm: TFmOpcoesBlueDerm;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule, UnInternalConsts, PrdGrupTip,
  MyDBCheck, StqCenCad, DmkDAC_PF, UnVS_PF, UnGrade_Jan, UnFinanceiro,
  NFe_PF;

{$R *.DFM}

procedure TFmOpcoesBlueDerm.BtPadroesSPED_IIClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPadroesSPED_II, BtPadroesSPED_II);
end;

procedure TFmOpcoesBlueDerm.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesBlueDerm.Conjunta1Click(Sender: TObject);
begin
  PadroesRegistrosSPED_ICMS_IPI(Sender);
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_COFINS_CSTChange(Sender: TObject);
begin
  EdCreTrib_MP_COFINS_CST_TXT.Text := UFinanceiro.CST_COFINS_Get(TDmkEdit(Sender).Text);
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_COFINS_CSTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    TDmkEdit(Sender).Text := UFinanceiro.ListaDeCST_COFINS();
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_ICMS_CSTChange(Sender: TObject);
begin
  EdCreTrib_MP_ICMS_CST_TXT.Text := UFinanceiro.CST_B_Get(Geral.IMV(TDmkEdit(Sender).Text));
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_ICMS_CSTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    TDmkEdit(Sender).Text := UFinanceiro.ListaDeTributacaoPeloICMS();
(*if Key = VK_F3 then
    EdCSOSN.Text := UFinanceiro.ListaDeCSOSN();*)
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_IPI_CSTChange(Sender: TObject);
begin
  EdCreTrib_MP_IPI_CST_TXT.Text := UFinanceiro.CST_IPI_Get(TDmkEdit(Sender).Text);
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_IPI_CSTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    TDmkEdit(Sender).Text := UFinanceiro.ListaDeCST_IPI();
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_PIS_CSTChange(Sender: TObject);
begin
  EdCreTrib_MP_PIS_CST_TXT.Text := UFinanceiro.CST_PIS_Get(TDmkEdit(Sender).Text);
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_PIS_CSTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    TDmkEdit(Sender).Text := UFinanceiro.ListaDeCST_PIS();
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_TES_COFINSChange(Sender: TObject);
begin
  EdCreTrib_MP_TES_COFINS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(
    spedTES_BC_ITENS,  EdCreTrib_MP_TES_COFINS.ValueVariant, F_TES_ITENS);
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_TES_COFINSKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdCreTrib_MP_TES_COFINS.Text := Geral.SelecionaItem(F_TES_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_TES_ICMSChange(Sender: TObject);
begin
  EdCreTrib_MP_TES_ICMS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(
    spedTES_BC_ITENS,  EdCreTrib_MP_TES_ICMS.ValueVariant, F_TES_ITENS);
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_TES_ICMSKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdCreTrib_MP_TES_ICMS.Text := Geral.SelecionaItem(F_TES_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_TES_IPIChange(Sender: TObject);
begin
  EdCreTrib_MP_TES_IPI_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,
    EdCreTrib_MP_TES_IPI.ValueVariant, F_TES_ITENS);
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_TES_IPIKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdCreTrib_MP_TES_IPI.Text := Geral.SelecionaItem(F_TES_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_TES_PISChange(Sender: TObject);
begin
  EdCreTrib_MP_TES_PIS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(
    spedTES_BC_ITENS,  EdCreTrib_MP_TES_PIS.ValueVariant, F_TES_ITENS);
end;

procedure TFmOpcoesBlueDerm.EdCreTrib_MP_TES_PISKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdCreTrib_MP_TES_PIS.Text := Geral.SelecionaItem(F_TES_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmOpcoesBlueDerm.EdRemTrib_MP_COFINS_CSTChange(Sender: TObject);
begin
  EdRemTrib_MP_COFINS_CST_TXT.Text := UFinanceiro.CST_COFINS_Get(TDmkEdit(Sender).Text);
end;

procedure TFmOpcoesBlueDerm.EdRemTrib_MP_COFINS_CSTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    TDmkEdit(Sender).Text := UFinanceiro.ListaDeCST_COFINS();
end;

procedure TFmOpcoesBlueDerm.EdRemTrib_MP_ICMS_CSTChange(Sender: TObject);
begin
  EdRemTrib_MP_ICMS_CST_TXT.Text := UFinanceiro.CST_B_Get(Geral.IMV(TDmkEdit(Sender).Text));
end;

procedure TFmOpcoesBlueDerm.EdRemTrib_MP_ICMS_CSTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    TDmkEdit(Sender).Text := UFinanceiro.ListaDeTributacaoPeloICMS();
end;

procedure TFmOpcoesBlueDerm.EdRemTrib_MP_IPI_CSTChange(Sender: TObject);
begin
  EdRemTrib_MP_IPI_CST_TXT.Text := UFinanceiro.CST_IPI_Get(TDmkEdit(Sender).Text);
end;

procedure TFmOpcoesBlueDerm.EdRemTrib_MP_IPI_CSTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    TDmkEdit(Sender).Text := UFinanceiro.ListaDeCST_IPI();
end;

procedure TFmOpcoesBlueDerm.EdRemTrib_MP_PIS_CSTChange(Sender: TObject);
begin
  EdRemTrib_MP_PIS_CST_TXT.Text := UFinanceiro.CST_PIS_Get(TDmkEdit(Sender).Text);
end;

procedure TFmOpcoesBlueDerm.EdRemTrib_MP_PIS_CSTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    TDmkEdit(Sender).Text := UFinanceiro.ListaDeCST_PIS();
end;

procedure TFmOpcoesBlueDerm.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesBlueDerm.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesBlueDerm.Isolada1Click(Sender: TObject);
begin
  PadroesRegistrosSPED_ICMS_IPI(Sender);
end;

procedure TFmOpcoesBlueDerm.PadroesRegistrosSPED_ICMS_IPI(Sender: TObject);
var
  I: Integer;
begin
  case TMenuItem(Sender).Tag of
    (*Isolada*)  1: I := 2;
    (*Conjunta*) 2: I := 3;
    else(*Isolada*) I := 2;
  end;
  RGSPED_II_PDA.ItemIndex      := 0; // 210/215
  RGSPED_II_EmCal.ItemIndex    := I; // 29x/30x
  RGSPED_II_Caleado.ItemIndex  := I; // 29x/30x
  RGSPED_II_DTA.ItemIndex      := 0; // 210/215
  RGSPED_II_EmCur.ItemIndex    := I; // 29x/30x
  RGSPED_II_Curtido.ItemIndex  := I; // 29x/30x
  RGSPED_II_ArtCur.ItemIndex   := I; // 29x/30x
  RGSPED_II_Operacao.ItemIndex := 0; // 210/215
  RGSPED_II_Recurt.ItemIndex   := I; // 29x/30x
  RGSPED_II_SubProd.ItemIndex  := I; // 29x/30x
  RGSPED_II_Reparo.ItemIndex   := 4; // 260/265
end;

procedure TFmOpcoesBlueDerm.ReopenCFOP_DE();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCreTrib_MP_CFOP_DE, Dmod.MyDB, [
  'SELECT CAST(CONCAT(SUBSTRING(cfp.Codigo, 1, 1), SUBSTRING(cfp.Codigo, 3, 3)) AS SIGNED) Codigo,',
  'cfp.Nome',
  'FROM cfop2003 cfp',
  'ORDER BY cfp.Nome',
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrRemTrib_MP_CFOP_DE, Dmod.MyDB, [
  'SELECT CAST(CONCAT(SUBSTRING(cfp.Codigo, 1, 1), SUBSTRING(cfp.Codigo, 3, 3)) AS SIGNED) Codigo,',
  'cfp.Nome',
  'FROM cfop2003 cfp',
  'ORDER BY cfp.Nome',
  '']);
end;

procedure TFmOpcoesBlueDerm.ReopenCFOP_FE();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCreTrib_MP_CFOP_FE, Dmod.MyDB, [
  'SELECT CAST(CONCAT(SUBSTRING(cfp.Codigo, 1, 1), SUBSTRING(cfp.Codigo, 3, 3)) AS SIGNED) Codigo,',
  'cfp.Nome',
  'FROM cfop2003 cfp',
  'ORDER BY cfp.Nome',
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrRemTrib_MP_CFOP_FE, Dmod.MyDB, [
  'SELECT CAST(CONCAT(SUBSTRING(cfp.Codigo, 1, 1), SUBSTRING(cfp.Codigo, 3, 3)) AS SIGNED) Codigo,',
  'cfp.Nome',
  'FROM cfop2003 cfp',
  'ORDER BY cfp.Nome',
  '']);
end;

procedure TFmOpcoesBlueDerm.BtOKClick(Sender: TObject);
var
  StqCenCad, WBNivGer, OSImpInfWB, UsaBRLMedM2, UsaM2Medio, UsaEmitGru,
  VerImprRecRib, CanAltBalVS, InfoMulFrnImpVS, QtdBoxClas, NObrigNFeVS,
  VSWarSemNF, VSWarNoFrn, VSInsPalManu: Integer;
  VSImpRandStr, DBClarecoCDR: String;
  SPED_II_PDA, SPED_II_EmCal, SPED_II_Caleado, SPED_II_DTA, SPED_II_EmCur,
  SPED_II_Curtido, SPED_II_ArtCur, SPED_II_Operacao, SPED_II_Recurt,
  SPED_II_SubProd, SPED_II_Reparo: Integer;
  VSImpMediaPall, NaoRecupImposPQ: Integer;
  //
  Cancelado: Boolean;
  M2DiaAcabPCP: Double;
  //
  CtaComisCMP, GruUsrEdtRecei: Integer;
  //
var
  CreTrib_MP_ICMS_CST, CreTrib_MP_IPI_CST, CreTrib_MP_PIS_CST, CreTrib_MP_COFINS_CST: String;
  CreTrib_MP_CFOP_DE, CreTrib_MP_CFOP_FE: Integer;
  CreTrib_MP_ICMS_ALIQ, CreTrib_MP_ICMS_ALIQ_ST, CreTrib_MP_IPI_ALIQ, CreTrib_MP_PIS_ALIQ, CreTrib_MP_COFINS_ALIQ: Double;

  RemTrib_MP_ICMS_CST, RemTrib_MP_IPI_CST, RemTrib_MP_PIS_CST, RemTrib_MP_COFINS_CST: String;
  RemTrib_MP_CFOP_DE, RemTrib_MP_CFOP_FE: Integer;
  RemTrib_MP_ICMS_ALIQ, RemTrib_MP_ICMS_ALIQ_ST, RemTrib_MP_IPI_ALIQ, RemTrib_MP_PIS_ALIQ, RemTrib_MP_COFINS_ALIQ: Double;

  CreTrib_MP_TES_ICMS, CreTrib_MP_TES_IPI, CreTrib_MP_TES_PIS, CreTrib_MP_TES_COFINS: Integer;

  StqCenCadEstqPall: Integer;

  procedure AvisaSoDesmonte(RG: TRadioGroup);
  begin
    Geral.MB_Aviso('O campo "' + RG.Caption + '" s� aceita o item "' +
    RG.Items[0] + '"');
  end;
  //
  procedure AvisaSohProducao(RG: TRadioGroup);
  begin
    Geral.MB_Aviso('O campo "' + RG.Caption + '" s� aceita os ites "' +
    RG.Items[2] + '" e "' +RG.Items[3] + '"');
    //
    Cancelado := True;
  end;

begin
  Cancelado := False;
{
  if not UMyMod.ObtemCodigoDeCodUsu(EdPQ_PrdGruTip, PrdGrupTip,
    'Informe o tipo de grupos de produtos (Grade)!',
    'Codigo', 'CodUsu') then Exit;
}
  //
  WBNivGer        := RGWBNivGer.ItemIndex;
  OSImpInfWB      := Geral.BoolToInt(CkOSImpInfWB.Checked);
  UsaBRLMedM2     := Geral.BoolToInt(CkUsaBRLMedM2.Checked);
  UsaM2Medio      := Geral.BoolToInt(CkUsaM2Medio.Checked);
  UsaEmitGru      := Geral.BoolToInt(CkUsaEmitGru.Checked);
  VerImprRecRib   := RGVerImprRecRib.ItemIndex;
  CanAltBalVS     := Geral.BoolToInt(CkCanAltBalVS.Checked);
  InfoMulFrnImpVS := Geral.BoolToInt(CkInfoMulFrnImpVS.Checked);
  QtdBoxClas      := RGQtdBoxClas.ItemIndex;
  VSImpRandStr    := EdVSImpRandStr.Text;
  NObrigNFeVS     := CGNObrigNFeVS.Value;
  SPED_II_PDA     := RGSPED_II_PDA.ItemIndex;
  SPED_II_EmCal   := RGSPED_II_EmCal.ItemIndex;
  SPED_II_Caleado := RGSPED_II_Caleado.ItemIndex;
  SPED_II_DTA     := RGSPED_II_DTA.ItemIndex;
  SPED_II_EmCur   := RGSPED_II_EmCur.ItemIndex;
  SPED_II_Curtido := RGSPED_II_Curtido.ItemIndex;
  SPED_II_ArtCur  := RGSPED_II_ArtCur.ItemIndex;
  SPED_II_Operacao:= RGSPED_II_Operacao.ItemIndex;
  SPED_II_Recurt  := RGSPED_II_Recurt.ItemIndex;
  SPED_II_SubProd := RGSPED_II_SubProd.ItemIndex;
  SPED_II_Reparo  := RGSPED_II_Reparo.ItemIndex;
  VSWarSemNF      := Geral.BoolToInt(CkVSWarSemNF.Checked);
  VSWarNoFrn      := Geral.BoolToInt(CkVSWarNoFrn.Checked);
  VSInsPalManu    := Geral.BoolToInt(CkVSInsPalManu.Checked);
  DBClarecoCDR    := EdDBClarecoCDR.Text;
  VSImpMediaPall  := Geral.BoolToInt(CkVSImpMediaPall.Checked);
  //
  M2DiaAcabPCP    := EdM2DiaAcabPCP.ValueVariant;
  //
  CtaComisCMP     := EdCtaComisCMP.ValueVariant;
  //
  NaoRecupImposPQ := Geral.BoolToInt(CkNaoRecupImposPQ.Checked);
  //
  CreTrib_MP_CFOP_DE      := EdCreTrib_MP_CFOP_DE.ValueVariant;
  CreTrib_MP_CFOP_FE      := EdCreTrib_MP_CFOP_FE.ValueVariant;
  CreTrib_MP_ICMS_CST     := EdCreTrib_MP_ICMS_CST.Text;
  CreTrib_MP_IPI_CST      := EdCreTrib_MP_IPI_CST.Text;
  CreTrib_MP_PIS_CST      := EdCreTrib_MP_PIS_CST.Text;
  CreTrib_MP_COFINS_CST   := EdCreTrib_MP_COFINS_CST.Text;
  CreTrib_MP_ICMS_ALIQ    := EdCreTrib_MP_ICMS_ALIQ.ValueVariant;
  CreTrib_MP_ICMS_ALIQ_ST := EdCreTrib_MP_ICMS_ALIQ_ST.ValueVariant;
  CreTrib_MP_IPI_ALIQ     := EdCreTrib_MP_IPI_ALIQ.ValueVariant;
  CreTrib_MP_PIS_ALIQ     := EdCreTrib_MP_PIS_ALIQ.ValueVariant;
  CreTrib_MP_COFINS_ALIQ  := EdCreTrib_MP_COFINS_ALIQ.ValueVariant;
  //
  RemTrib_MP_CFOP_DE      := EdRemTrib_MP_CFOP_DE.ValueVariant;
  RemTrib_MP_CFOP_FE      := EdRemTrib_MP_CFOP_FE.ValueVariant;
  RemTrib_MP_ICMS_CST     := EdRemTrib_MP_ICMS_CST.Text;
  RemTrib_MP_IPI_CST      := EdRemTrib_MP_IPI_CST.Text;
  RemTrib_MP_PIS_CST      := EdRemTrib_MP_PIS_CST.Text;
  RemTrib_MP_COFINS_CST   := EdRemTrib_MP_COFINS_CST.Text;
  RemTrib_MP_ICMS_ALIQ    := EdRemTrib_MP_ICMS_ALIQ.ValueVariant;
  RemTrib_MP_ICMS_ALIQ_ST := EdRemTrib_MP_ICMS_ALIQ_ST.ValueVariant;
  RemTrib_MP_IPI_ALIQ     := EdRemTrib_MP_IPI_ALIQ.ValueVariant;
  RemTrib_MP_PIS_ALIQ     := EdRemTrib_MP_PIS_ALIQ.ValueVariant;
  RemTrib_MP_COFINS_ALIQ  := EdRemTrib_MP_COFINS_ALIQ.ValueVariant;
  CreTrib_MP_TES_ICMS     := EdCreTrib_MP_TES_ICMS.ValueVariant;
  CreTrib_MP_TES_IPI      := EdCreTrib_MP_TES_IPI.ValueVariant;
  CreTrib_MP_TES_PIS      := EdCreTrib_MP_TES_PIS.ValueVariant;
  CreTrib_MP_TES_COFINS   := EdCreTrib_MP_TES_COFINS.ValueVariant;

  StqCenCadEstqPall       := EdStqCenCadEstqPall.ValueVariant;

  GruUsrEdtRecei          := Geral.BoolToInt(CkGruUsrEdtRecei.Checked);

  if Length(VSImpRandStr) > 0 then
  begin
    if MyObjects.FIC(Length(VSImpRandStr) < 8, EdVSImpRandStr,
    'A "Contra senha impress�o QR Code" deve ter no m[iximo 8 caracteres!') then Exit;
    if MyObjects.FIC(Length(VSImpRandStr) > 30, EdVSImpRandStr,
    'A "Contra senha impress�o QR Code" deve ter no m�ximo 30 caracteres!') then Exit;
  end;
  //
  VSImpRandStr := DmkPF.HexCripto(VSImpRandStr, CO_RandStrWeb01);
  if not UMyMod.ObtemCodigoDeCodUsu(EdPQ_StqCenCad, StqCencad,
    'Informe o Centro de estoque de Produtos Qu�micos!',
    'Codigo', 'CodUsu') then Exit;
    //
  if RGSPED_II_PDA.ItemIndex <> 0 then
    AvisaSoDesmonte(RGSPED_II_PDA);
  if RGSPED_II_EmCal.ItemIndex < 2 then
    AvisaSohProducao(RGSPED_II_EmCal);
  if RGSPED_II_Caleado.ItemIndex < 2 then
    AvisaSohProducao(RGSPED_II_Caleado);
  if RGSPED_II_DTA.ItemIndex <> 0 then
    AvisaSoDesmonte(RGSPED_II_DTA);
  if RGSPED_II_EmCur.ItemIndex < 2 then
    AvisaSohProducao(RGSPED_II_EmCur);
  if RGSPED_II_Curtido.ItemIndex < 2 then
    AvisaSohProducao(RGSPED_II_Curtido);

 //...
  if Cancelado then
    Exit;
  //
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'controle', False, [
    'ServSMTP', 'NomeMailOC', 'MailOC',
    'MailCCCega', 'ConexaoDialUp', 'CorpoMailOC',
    'DonoMailOC', 'PathLogoDupl', 'PathLogoRece',
    'PQNegBaixa',
    'PQNegEntra',
    'CanAltBalA',
    'ImpRecRib',
    'ImpRecAcabto',
    'CtaPQCompr', 'CtaPQFrete',
    'CtaMPCompr', 'CtaMPFrete',
    'CtaCVVenda', 'CtaCVFrete',
    //'CtaCCVenda', 'CtaCCFrete',
    //'CtaCAVenda', 'CtaCAFrete',
    'VendaCouro', 'SetorCal', 'SetorCur',
    'CasasProd', (*'PQ_PrdGrup Tip,'*) 'PQ_StqCenCad',
    (*'PQ_Amostra', 'PQ_PQ',
    'PQ_Material', 'PQ_Outros'*)
    'WBNivGer',
    'OSImpInfWB', 'UsaBRLMedM2', 'UsaM2Medio',
    'UsaEmitGru', 'VerImprRecRib', 'CanAltBalVS',
    'InfoMulFrnImpVS', 'QtdBoxClas', 'VSImpRandStr',
    'NObrigNFeVS',
    'SPED_II_PDA', 'SPED_II_EmCal', 'SPED_II_Caleado',
    'SPED_II_DTA', 'SPED_II_EmCur', 'SPED_II_Curtido',
    'SPED_II_ArtCur', 'SPED_II_Operacao', 'SPED_II_Recurt',
    'SPED_II_SubProd', 'SPED_II_Reparo', 'VSWarSemNF',
    'VSWarNoFrn', 'VSInsPalManu', 'DBClarecoCDR',
    'VSImpMediaPall', 'M2DiaAcabPCP', 'CtaComisCMP',
    'NaoRecupImposPQ',
    'CreTrib_MP_CFOP_DE', 'CreTrib_MP_CFOP_FE', 'CreTrib_MP_ICMS_CST',
    'CreTrib_MP_IPI_CST', 'CreTrib_MP_PIS_CST', 'CreTrib_MP_COFINS_CST',
    'CreTrib_MP_ICMS_ALIQ', 'CreTrib_MP_ICMS_ALIQ_ST', 'CreTrib_MP_IPI_ALIQ',
    'CreTrib_MP_PIS_ALIQ', 'CreTrib_MP_COFINS_ALIQ',
    'RemTrib_MP_CFOP_DE', 'RemTrib_MP_CFOP_FE', 'RemTrib_MP_ICMS_CST',
    'RemTrib_MP_IPI_CST', 'RemTrib_MP_PIS_CST', 'RemTrib_MP_COFINS_CST',
    'RemTrib_MP_ICMS_ALIQ', 'RemTrib_MP_ICMS_ALIQ_ST', 'RemTrib_MP_IPI_ALIQ',
    'RemTrib_MP_PIS_ALIQ', 'RemTrib_MP_COFINS_ALIQ',
    'CreTrib_MP_TES_ICMS', 'CreTrib_MP_TES_IPI',
    'CreTrib_MP_TES_PIS', 'CreTrib_MP_TES_COFINS',
    'StqCenCadEstqPall', 'GruUsrEdtRecei'
    ], ['Codigo'], [
    EdServSMTP.Text, EdNomeMailOC.Text, EdMailOC.Text,
    EdMailCCCega.Text, EdConexaoDialUp.Text, EdCorpoMailOC.Text,
    EdDonoMAilOC.Text, dmkPF.DuplicaBarras(EdPathLogoDupl.Text),
    // ???.DuplicaBarras(EdPathLogoRece.Text),
    EdPathLogoRece.Text,
    Geral.BoolToInt(CkPQNegBaixa.Checked),
    Geral.BoolToInt(CkPQNegEntra.Checked),
    Geral.BoolToInt(CkCanAltBalA.Checked),
    RGImpRecRib.ItemIndex,
    RGImpRecAcabto.ItemIndex,
    Geral.IMV(EdCtaPQCompr.Text), Geral.IMV(EdCtaPQFrete.Text),
    Geral.IMV(EdCtaMPCompr.Text), Geral.IMV(EdCtaMPFrete.Text),
    Geral.IMV(EdCtaCVVenda.Text), Geral.IMV(EdCtaCVFrete.Text),
    //EdCtaCCVenda.Text, EdCtaCCFrete.Text,
    //EdCtaCAVenda.Text, EdCtaCAFrete.Text,
    RGVendaCouro.ItemIndex,
    Geral.IMV(EdSetorCal.Text), Geral.IMV(EdSetorCur.Text),
    RGCasasProd.ItemIndex, (*PrdGrupTip,*) StqCenCad,
    (*EdPQ_Amostra.ValueVariant, EdPQ_PQ.ValueVariant,
    EdPQ_Material.ValueVariant, EdPQ_Outros.ValueVariant*)
    WBNivGer,
    OSImpInfWB, UsaBRLMedM2, UsaM2Medio,
    UsaEmitGru, VerImprRecRib, CanAltBalVS,
    InfoMulFrnImpVS, QtdBoxClas, VSImpRandStr,
    NObrigNFeVS,
    SPED_II_PDA, SPED_II_EmCal, SPED_II_Caleado,
    SPED_II_DTA, SPED_II_EmCur, SPED_II_Curtido,
    SPED_II_ArtCur, SPED_II_Operacao, SPED_II_Recurt,
    SPED_II_SubProd, SPED_II_Reparo, VSWarSemNF,
    VSWarNoFrn, VSInsPalManu, DBClarecoCDR,
    VSImpMediaPall, M2DiaAcabPCP, CtaComisCMP,
    NaoRecupImposPQ,
    CreTrib_MP_CFOP_DE, CreTrib_MP_CFOP_FE, CreTrib_MP_ICMS_CST,
    CreTrib_MP_IPI_CST, CreTrib_MP_PIS_CST, CreTrib_MP_COFINS_CST,
    CreTrib_MP_ICMS_ALIQ, CreTrib_MP_ICMS_ALIQ_ST, CreTrib_MP_IPI_ALIQ,
    CreTrib_MP_PIS_ALIQ, CreTrib_MP_COFINS_ALIQ,
    RemTrib_MP_CFOP_DE, RemTrib_MP_CFOP_FE, RemTrib_MP_ICMS_CST,
    RemTrib_MP_IPI_CST, RemTrib_MP_PIS_CST, RemTrib_MP_COFINS_CST,
    RemTrib_MP_ICMS_ALIQ, RemTrib_MP_ICMS_ALIQ_ST, RemTrib_MP_IPI_ALIQ,
    RemTrib_MP_PIS_ALIQ, RemTrib_MP_COFINS_ALIQ,
    CreTrib_MP_TES_ICMS, CreTrib_MP_TES_IPI,
    CreTrib_MP_TES_PIS, CreTrib_MP_TES_COFINS,
    StqCenCadEstqPall, GruUsrEdtRecei
    ], [1], True);
  //
  UnDmkDAC_PF.AbreQuery(Dmod.QrControle, Dmod.MyDB);
  Close;
end;

procedure TFmOpcoesBlueDerm.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TabSheet6.Caption     := CO_PODE_ALTERAR_ESTOQUE_PRODUTO_ABA;
  CkCanAltBalVS.Caption := CO_PODE_ALTERAR_ESTOQUE_PRODUTO_CHK;
  //CkVSWarSemNF.Caption  := ??
  //CkVSWarNoFrn
  CGNObrigNFeVS.Items := VS_PF.ObtemListaOperacoes();
  //
  PCGeral.ActivePageIndex := 0;
  UnDmkDAC_PF.AbreQuery(QrContasPQCompr, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContasMPCompr, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContasCVVenda, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContasComisCMP, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContasPQFrete, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContasMPFrete, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContasCVFrete, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrListaSetores1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrListaSetores2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrdGrupTip, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  ReopenCFOP_DE();
  ReopenCFOP_FE();
  //
  UnDmkDAC_PF.AbreQuery(Dmod.QrControle, Dmod.MyDB);
  //
  MyObjects.PreencheComponente(RGImpRecRib, sListaRecImpApresenta, 6);
  MyObjects.PreencheComponente(RGImpRecAcabto, sListaRecImpApresenta, 6);
  //
  EdServSMTP.Text          := Dmod.QrControle.FieldByName('PathLogoDupl').AsString;
  EdNomeMailOC.Text        := Dmod.QrControle.FieldByName('NomeMailOC').AsString;
  EdMailOC.Text            := Dmod.QrControle.FieldByName('MailOC').AsString;
  EdMailCCCega.Text        := Dmod.QrControle.FieldByName('MailCCCega').AsString;
  EdConexaoDialUp.Text     := Dmod.QrControle.FieldByName('ConexaoDialUp').AsString;
  EdCorpoMailOC.Text       := Dmod.QrControle.FieldByName('CorpoMailOC').AsString;
  EdDonoMailOC.Text        := Dmod.QrControle.FieldByName('DonoMailOC').AsString;
  EdPathLogoDupl.Text      := Dmod.QrControle.FieldByName('PathLogoDupl').AsString;
  EdPathLogoRece.Text      := Dmod.QrControle.FieldByName('PathLogoRece').AsString;
  CkPQNegBaixa.Checked     := Geral.IntToBool(Dmod.QrControlePQNegBaixa.Value);
  CkPQNegEntra.Checked     := Geral.IntToBool(Dmod.QrControlePQNegEntra.Value);
  CkCanAltBalA.Checked     := Geral.IntToBool(Dmod.QrControleCanAltBalA.Value);
  RGImpRecRib.ItemIndex    := Dmod.QrControleImpRecRib.Value;
  RGImpRecAcabto.ItemIndex := Dmod.QrControleImpRecAcabto.Value;
  //
  EdCtaPQCompr.Text       := IntToStr(Dmod.QrControleCtaPQCompr.Value);
  CBCtaPQCompr.KeyValue   := Dmod.QrControleCtaPQCompr.Value;
  EdCtaPQFrete.Text       := IntToStr(Dmod.QrControleCtaPQFrete.Value);
  CBCtaPQFrete.KeyValue   := Dmod.QrControleCtaPQFrete.Value;
  EdCtaMPCompr.Text       := IntToStr(Dmod.QrControleCtaMPCompr.Value);
  CBCtaMPCompr.KeyValue   := Dmod.QrControleCtaMPCompr.Value;
  EdCtaMPFrete.Text       := IntToStr(Dmod.QrControleCtaMPFrete.Value);
  CBCtaMPFrete.KeyValue   := Dmod.QrControleCtaMPFrete.Value;
  EdCtaCVVenda.Text       := IntToStr(Dmod.QrControleCtaCVVenda.Value);
  CBCtaCVVenda.KeyValue   := Dmod.QrControleCtaCVVenda.Value;
  EdCtaCVFrete.Text       := IntToStr(Dmod.QrControleCtaCVFrete.Value);
  CBCtaCVFrete.KeyValue   := Dmod.QrControleCtaCVFrete.Value;
  EdCtaComisCMP.Text      := IntToStr(Dmod.QrControleCtaComisCMP.Value);
  CBCtaComisCMP.KeyValue  := Dmod.QrControleCtaComisCMP.Value;
  {
  EdPQ_Amostra.Text       := IntToStr(Dmod.QrControlePQ_Amostra.Value);
  CBPQ_Amostra.KeyValue   := Dmod.QrControlePQ_Amostra.Value;
  }
  //
  EdSetorCal.Text       := IntToStr(Dmod.QrControleSetorCal.Value);
  CBSetorCal.KeyValue   := Dmod.QrControleSetorCal.Value;
  EdSetorCur.Text       := IntToStr(Dmod.QrControleSetorCur.Value);
  CBSetorCur.KeyValue   := Dmod.QrControleSetorCur.Value;
  //
  RGVendaCouro.ItemIndex := Dmod.QrControleVendaCouro.Value;
  RGCasasProd.ItemIndex  := Dmod.QrControleCasasProd.Value;
  //
  {
  UMyMod.SetaCodUsuDeCodigo(EdPQ_PrdGruTip, CBPQ_PrdGruTip, QrPrdGrupTip,
    Dmod.QrControlePQ_Prdgrup Tip.Value);
  }
  //
  UMyMod.SetaCodUsuDeCodigo(EdPQ_StqCenCad, CBPQ_StqCenCad, QrStqCenCad,
    Dmod.QrControlePQ_StqCenCad.Value);
  //
  RGWBNivGer.ItemIndex  := Dmod.QrControleWBNivGer.Value;
  //
  CkOSImpInfWB.Checked         := Geral.IntToBool(Dmod.QrControleOSImpInfWB.Value);
  CkUsaBRLMedM2.Checked        := Geral.IntToBool(Dmod.QrControleUsaBRLMedM2.Value);
  CkUsaM2Medio.Checked         := Geral.IntToBool(Dmod.QrControleUsaM2Medio.Value);
  CkUsaEmitGru.Checked         := Geral.IntToBool(Dmod.QrControleUsaEmitGru.Value);
  RGVerImprRecRib.ItemIndex    := Dmod.QrControleVerImprRecRib.Value;
  CkCanAltBalVS.Checked        := Geral.IntToBool(Dmod.QrControleCanAltBalVS.Value);
  CkInfoMulFrnImpVS.Checked    := Geral.IntToBool(Dmod.QrControleInfoMulFrnImpVS.Value);
  RGQtdBoxClas.ItemIndex       := Dmod.QrControleQtdBoxClas.Value;
  //EdVSImpRandStr.Text       := Dmod.QrControleVSImpRandStr.Value;
  EdVSImpRandStr.Text          := DmkPF.HexDecripto(Dmod.QrControle.FieldByName('VSImpRandStr').AsString, CO_RandStrWeb01);
  CGNObrigNFeVS.Value          := Dmod.QrControleNObrigNFeVS.Value;
  RGSPED_II_PDA.ItemIndex      := Dmod.QrControleSPED_II_PDA.Value;
  RGSPED_II_EmCal.ItemIndex    := Dmod.QrControleSPED_II_EmCal.Value;
  RGSPED_II_Caleado.ItemIndex  := Dmod.QrControleSPED_II_Caleado.Value;
  RGSPED_II_DTA.ItemIndex      := Dmod.QrControleSPED_II_DTA.Value;
  RGSPED_II_EmCur.ItemIndex    := Dmod.QrControleSPED_II_EmCur.Value;
  RGSPED_II_ArtCur.ItemIndex   := Dmod.QrControleSPED_II_ArtCur.Value;
  RGSPED_II_Curtido.ItemIndex  := Dmod.QrControleSPED_II_Curtido.Value;
  RGSPED_II_Operacao.ItemIndex := Dmod.QrControleSPED_II_Operacao.Value;
  RGSPED_II_Recurt.ItemIndex   := Dmod.QrControleSPED_II_Recurt.Value;
  RGSPED_II_SubProd.ItemIndex  := Dmod.QrControleSPED_II_SubProd.Value;
  RGSPED_II_Reparo.ItemIndex   := Dmod.QrControleSPED_II_Reparo.Value;
  CkVSWarSemNF.Checked         := Geral.IntToBool(Dmod.QrControleVSWarSemNF.Value);
  CkVSWarNoFrn.Checked         := Geral.IntToBool(Dmod.QrControleVSWarNoFrn.Value);
  CkVSInsPalManu.Checked       := Geral.IntToBool(Dmod.QrControleVSInsPalManu.Value);
  EdDBClarecoCDR.Text          := Dmod.QrControle.FieldByName('DBClarecoCDR').AsString;
  //
  CkVSImpMediaPall.Checked     := Geral.IntToBool(Dmod.QrControleVSImpMediaPall.Value);
  //
  EdM2DiaAcabPCP.ValueVariant  := Dmod.QrControleM2DiaAcabPCP.Value;
  //
  CkNaoRecupImposPQ.Checked    := Geral.IntToBool(Dmod.QrControleNaoRecupImposPQ.Value);
  //
  EdCreTrib_MP_CFOP_DE.ValueVariant       := Dmod.QrControleCreTrib_MP_CFOP_DE.Value;
  EdCreTrib_MP_CFOP_FE.ValueVariant       := Dmod.QrControleCreTrib_MP_CFOP_FE.Value;
  EdCreTrib_MP_ICMS_CST.ValueVariant      := Geral.IMV(Dmod.QrControleCreTrib_MP_ICMS_CST.VAlue);
  EdCreTrib_MP_IPI_CST.ValueVariant       := Dmod.QrControleCreTrib_MP_IPI_CST.VAlue;
  EdCreTrib_MP_PIS_CST.ValueVariant       := Dmod.QrControleCreTrib_MP_PIS_CST.VAlue;
  EdCreTrib_MP_COFINS_CST.ValueVariant    := Dmod.QrControleCreTrib_MP_COFINS_CST.VAlue;
  EdCreTrib_MP_ICMS_ALIQ.ValueVariant     := Dmod.QrControleCreTrib_MP_ICMS_ALIQ.Value;
  EdCreTrib_MP_ICMS_ALIQ_ST.ValueVariant  := Dmod.QrControleCreTrib_MP_ICMS_ALIQ_ST.Value;
  EdCreTrib_MP_IPI_ALIQ.ValueVariant      := Dmod.QrControleCreTrib_MP_IPI_ALIQ.Value;
  EdCreTrib_MP_PIS_ALIQ.ValueVariant      := Dmod.QrControleCreTrib_MP_PIS_ALIQ.Value;
  EdCreTrib_MP_COFINS_ALIQ.ValueVariant   := Dmod.QrControleCreTrib_MP_COFINS_ALIQ.Value;

  EdCreTrib_MP_ICMS_CST_TXT.ValueVariant      := UFinanceiro.CST_B_Get(Geral.IMV(EdCreTrib_MP_ICMS_CST.Text));
  EdCreTrib_MP_IPI_CST_TXT.ValueVariant       := UFinanceiro.CST_IPI_Get(EdCreTrib_MP_IPI_CST.Text);
  EdCreTrib_MP_PIS_CST_TXT.ValueVariant       := UFinanceiro.CST_PIS_Get(EdCreTrib_MP_PIS_CST.Text);
  EdCreTrib_MP_COFINS_CST_TXT.ValueVariant    := UFinanceiro.CST_COFINS_Get(EdCreTrib_MP_COFINS_CST.Text);

  //
  EdRemTrib_MP_CFOP_DE.ValueVariant       := Dmod.QrControleRemTrib_MP_CFOP_DE.Value;
  EdRemTrib_MP_CFOP_FE.ValueVariant       := Dmod.QrControleRemTrib_MP_CFOP_FE.Value;
  EdRemTrib_MP_ICMS_CST.ValueVariant      := Geral.IMV(Dmod.QrControleRemTrib_MP_ICMS_CST.VAlue);
  EdRemTrib_MP_IPI_CST.ValueVariant       := Dmod.QrControleRemTrib_MP_IPI_CST.VAlue;
  EdRemTrib_MP_PIS_CST.ValueVariant       := Dmod.QrControleRemTrib_MP_PIS_CST.VAlue;
  EdRemTrib_MP_COFINS_CST.ValueVariant    := Dmod.QrControleRemTrib_MP_COFINS_CST.VAlue;
  EdRemTrib_MP_ICMS_ALIQ.ValueVariant     := Dmod.QrControleRemTrib_MP_ICMS_ALIQ.Value;
  EdRemTrib_MP_ICMS_ALIQ_ST.ValueVariant  := Dmod.QrControleRemTrib_MP_ICMS_ALIQ_ST.Value;
  EdRemTrib_MP_IPI_ALIQ.ValueVariant      := Dmod.QrControleRemTrib_MP_IPI_ALIQ.Value;
  EdRemTrib_MP_PIS_ALIQ.ValueVariant      := Dmod.QrControleRemTrib_MP_PIS_ALIQ.Value;
  EdRemTrib_MP_COFINS_ALIQ.ValueVariant   := Dmod.QrControleRemTrib_MP_COFINS_ALIQ.Value;

  EdRemTrib_MP_ICMS_CST_TXT.ValueVariant      := UFinanceiro.CST_B_Get(Geral.IMV(EdRemTrib_MP_ICMS_CST.Text));
  EdRemTrib_MP_IPI_CST_TXT.ValueVariant       := UFinanceiro.CST_IPI_Get(EdRemTrib_MP_IPI_CST.Text);
  EdRemTrib_MP_PIS_CST_TXT.ValueVariant       := UFinanceiro.CST_PIS_Get(EdRemTrib_MP_PIS_CST.Text);
  EdRemTrib_MP_COFINS_CST_TXT.ValueVariant    := UFinanceiro.CST_COFINS_Get(EdRemTrib_MP_COFINS_CST.Text);

  F_TES_ITENS := UnNFe_PF.ListaTES_ITENS();

  EdCreTrib_MP_TES_ICMS.ValueVariant        := Dmod.QrControleCreTrib_MP_TES_ICMS.Value;
  EdCreTrib_MP_TES_IPI.ValueVariant         := Dmod.QrControleCreTrib_MP_TES_IPI.Value;
  EdCreTrib_MP_TES_PIS.ValueVariant         := Dmod.QrControleCreTrib_MP_TES_PIS.Value;
  EdCreTrib_MP_TES_COFINS.ValueVariant      := Dmod.QrControleCreTrib_MP_TES_COFINS.Value;

  EdCreTrib_MP_TES_ICMS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,  EdCreTrib_MP_TES_ICMS.ValueVariant, F_TES_ITENS);
  EdCreTrib_MP_TES_IPI_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,  EdCreTrib_MP_TES_IPI.ValueVariant, F_TES_ITENS);
  EdCreTrib_MP_TES_PIS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,  EdCreTrib_MP_TES_PIS.ValueVariant, F_TES_ITENS);
  EdCreTrib_MP_TES_COFINS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,  EdCreTrib_MP_TES_COFINS.ValueVariant, F_TES_ITENS);

  EdStqCenCadEstqPall.ValueVariant := Dmod.QrControleStqCenCadEstqPall.Value;

  CkGruUsrEdtRecei.Checked := Geral.IntToBool(Dmod.QrControleGruUsrEdtRecei.Value);

end;

procedure TFmOpcoesBlueDerm.SbCreTrib_MP_CFOP_DEClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormCFOP2003();
  if VAR_CADASTRO <> 0 then
  begin
    ReopenCFOP_DE();
    UMyMod.SetaCodigoPesquisado(EdCreTrib_MP_CFOP_DE, CBCreTrib_MP_CFOP_DE, QrCreTrib_MP_CFOP_DE, VAR_CADASTRO);
  end;
end;

procedure TFmOpcoesBlueDerm.SbCreTrib_MP_CFOP_FEClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormCFOP2003();
  if VAR_CADASTRO <> 0 then
  begin
    ReopenCFOP_FE();
    UMyMod.SetaCodigoPesquisado(EdCreTrib_MP_CFOP_FE, CBCreTrib_MP_CFOP_FE, QrCreTrib_MP_CFOP_FE, VAR_CADASTRO);
  end;
end;

procedure TFmOpcoesBlueDerm.SpeedButton1Click(Sender: TObject);
begin
  OpenPictureDialog1.InitialDir := ExtractFilePath(EdPathLogoDupl.Text);
  if OpenPictureDialog1.Execute then
    EdPathLogoDupl.Text := OpenPictureDialog1.FileName;
end;

procedure TFmOpcoesBlueDerm.SpeedButton2Click(Sender: TObject);
begin
  OpenPictureDialog1.InitialDir := ExtractFilePath(EdPathLogoRece.Text);
  if OpenPictureDialog1.Execute then
    EdPathLogoRece.Text := OpenPictureDialog1.FileName;
end;

procedure TFmOpcoesBlueDerm.SpeedButton3Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmPrdGrupTip, FmPrdGrupTip, afmoNegarComAviso) then
  begin
    FmPrdGrupTip.ShowModal;
    FmPrdGrupTip.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodUsuDeCodigo(EdPQ_PrdGruTip, CBPQ_PrdGruTip, QrPrdGrupTip,
    VAR_CADASTRO)
end;

procedure TFmOpcoesBlueDerm.SpeedButton4Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmStqCenCad, FmStqCenCad, afmoNegarComAviso) then
  begin
    FmStqCenCad.ShowModal;
    FmStqCenCad.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodUsuDeCodigo(EdPQ_StqCenCad, CBPQ_StqCenCad, QrStqCenCad,
    VAR_CADASTRO)
end;

end.
