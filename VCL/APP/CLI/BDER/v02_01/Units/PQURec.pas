unit PQURec;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, UnAppEnums;

type
  TFmPQURec = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrPQ: TMySQLQuery;
    DsPQ: TDataSource;
    Panel3: TPanel;
    LaPQ: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    SbPQ: TSpeedButton;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    QrPQIQ: TIntegerField;
    QrPQNomeIQ: TWideStringField;
    QrPQKgLiqEmb: TFloatField;
    QrPQPesoEstq: TFloatField;
    PnInsumo: TPanel;
    DBEdIQ: TDBEdit;
    Label5: TLabel;
    DBEdPesoEstq: TDBEdit;
    DBEdKgLiqEmb: TDBEdit;
    Label4: TLabel;
    DBEdNomeIQ: TDBEdit;
    Label2: TLabel;
    EdPesComprPreDef: TdmkEdit;
    Label3: TLabel;
    QrDuplic: TMySQLQuery;
    QrDuplicInsumo: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbPQClick(Sender: TObject);
    procedure EdPQRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure PreDefinePesoItem(Insumo: Integer; PesComprPreDef: Double);
  public
    { Public declarations }
    //FNivel: Integer;
    //
    FAplicacao, FNivel, FCliIntCod: Integer;
    FCliIntNom: String;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    procedure ReopenPQ(CliInt: Integer);
  end;

  var
  FmPQURec: TFmPQURec;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  PQUGer, UnApp_Jan;

{$R *.DFM}

procedure TFmPQURec.BtOKClick(Sender: TObject);
var
  NomePQ, NomeCI, PedidoTxt, NomeIQ: String;
  Nivel, Codigo, Controle, Insumo, CliInt, PedidoCod, PedidoOrd, IQ, Aplicacao:
  Integer;
  KgLiqEmb, PesoEstq, PesoUso, PesoFut, PesoNeed, PesoCompra, Repet,
  InsumM2PercRetr, InsumM2PedKgM2, InsumM2PesoKg, AreaM2Pedido, QtEmbalagens,
  PesComprCarga, PesComprPreDef, CargaKgUso, CargaKgNeed, CargaKgCompra,
  CargaKgQtEmb, CargaPercInc, PesoPedido: Double;
  SQLType: TSQLType;
  Duplicado, Altera: Boolean;
begin
  SQLType        := ImgTipo.SQLType;
  Nivel          := FNivel;
  Codigo         := 0;
  Controle       := 0;
  Insumo         := EdPQ.ValueVariant;
  KgLiqEmb       := QrPQKgLiqEmb.Value;
  PesoEstq       := QrPQPesoEstq.Value;
  PesoUso        := 0;
  PesoFut        := 0;
  PesoNeed       := 0;
  PesoCompra     := 0;
  CliInt         := FCliIntCod;
  Repet          := 0;
  NomePQ         := CBPQ.Text;
  NomeCI         := FCliIntNom;
  PedidoCod      := 0;
  PedidoTxt      := '?';
  PedidoOrd      := 0;
  InsumM2PercRetr:= 0.00;
  InsumM2PedKgM2 := 0.000;
  InsumM2PesoKg  := 0.000000;
  AreaM2Pedido   := 0.00;
  IQ             := QrPQIQ.Value;
  NomeIQ         := QrPQNomeIQ.Value;
  QtEmbalagens   := 0;
  PesComprCarga  := 0;
  PesComprPreDef := EdPesComprPreDef.ValueVariant;
  CargaKgUso     := 0;
  CargaKgNeed    := 0;
  CargaKgCompra  := PesComprPreDef;
  if KgLiqEmb > 0 then
    CargaKgQtEmb   := CargaKgCompra / KgLiqEmb
  else
    CargaKgQtEmb   := 0;
  CargaPercInc   := 0;
  Aplicacao      := FAplicacao;
  PesoPedido     := PesComprPreDef;
  //
  if MyObjects.FIC(Insumo = 0, EdPQ, 'Informe o insumo!') then
    Exit;
  //
  Altera := False;
  if SQLType = stIns then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDuplic, Dmod.MyDB, [
    'SELECT Insumo',
    'FROM pqurec ',
    'WHERE Aplicacao=' + Geral.FF0(FAplicacao),
    'AND Nivel=2 ',
    'AND Insumo=' + Geral.FF0(Insumo),
    '']);
    Duplicado := (QrDuplic.RecordCount > 0) and (QrDuplicInsumo.Value = Insumo);
  end else
  begin
    Duplicado := False;
    Altera := True;
  end;
  //
  if Duplicado or Altera then
  begin
    if Duplicado then
      Altera := Geral.MB_Pergunta('Insumo j� incluido na compra!') = ID_YES;
    if Altera then
      PreDefinePesoItem(Insumo, PesComprPreDef);
  end else
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqurec', False, [
    'Codigo', 'Controle',
    'KgLiqEmb', 'PesoEstq',
    'PesoUso', 'PesoFut', 'PesoNeed',
    'PesoCompra', 'CliInt', 'Repet',
    'NomePQ', 'NomeCI', 'PedidoCod',
    'PedidoTxt', 'PedidoOrd', 'InsumM2PercRetr',
    'InsumM2PedKgM2', 'InsumM2PesoKg', 'AreaM2Pedido',
    'IQ', 'NomeIQ', 'QtEmbalagens',
    'PesComprCarga', 'PesComprPreDef', 'CargaKgUso',
    'CargaKgNeed', 'CargaKgCompra', 'CargaKgQtEmb',
    'CargaPercInc', 'PesoPedido'], [
    'Aplicacao', 'Nivel', 'Insumo'
    ], [
    Codigo, Controle,
    KgLiqEmb, PesoEstq,
    PesoUso, PesoFut, PesoNeed,
    PesoCompra, CliInt, Repet,
    NomePQ, NomeCI, PedidoCod,
    PedidoTxt, PedidoOrd, InsumM2PercRetr,
    InsumM2PedKgM2, InsumM2PesoKg, AreaM2Pedido,
    IQ, NomeIQ, QtEmbalagens,
    PesComprCarga, PesComprPreDef, CargaKgUso,
    CargaKgNeed, CargaKgCompra, CargaKgQtEmb,
    CargaPercInc, PesoPedido], [
    Aplicacao, Nivel, Insumo
    ], True) then
    begin
    end;
  end;
  ReopenCadastro_Com_Itens_ITS(Controle);
  FmPQUGer.ReopenQrPQURecCarga(Insumo);
  Close;
end;

procedure TFmPQURec.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQURec.EdPQRedefinido(Sender: TObject);
begin
  PnInsumo.Visible := EdPQ.ValueVariant <> 0;
end;

procedure TFmPQURec.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQURec.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPQURec.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQURec.PreDefinePesoItem(Insumo: Integer; PesComprPreDef: Double);
const
  Aplicacao = Integer(TPQUAplicacao.pquaplicRecurtimento);
  Nivel     = 2;
var
  sPeso: String;
  CargaKgCompra, CargaKgQtEmb, PesoPedido: Double;
  SQLType: TSQLType;
begin
  SQLType        := stUpd;
  CargaKgCompra  := PesComprPreDef;
  CargaKgQtEmb   := PesComprPreDef;
  PesoPedido     := PesComprPreDef;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqurec', False, [
  'PesComprPreDef', 'CargaKgCompra', 'CargaKgQtEmb',
  'PesoPedido'], [
  'Aplicacao', 'Nivel', 'Insumo'], [
  PesComprPreDef, CargaKgCompra, CargaKgQtEmb,
  PesoPedido], [
  Aplicacao, Nivel, Insumo], True) then
  begin
    Dmod.MyDB.Execute(Geral.ATS([
    'UPDATE pqurec SET ',
    'CargaKgQtEmb=CargaKgCompra/KgLiqEmb ',
    'WHERE Aplicacao=' + Geral.FF0(FAplicacao),
    'AND Nivel=2 ',
    'AND PesComprPreDef>0 ',
    'AND KgLiqEmb>0 ',
    '']));
  end;
end;

procedure TFmPQURec.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmPQURec.ReopenPQ(CliInt: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQ, Dmod.MyDB, [
  'SELECT pq_.*, pq_.Codigo, pq_.Nome, pq_.IQ, ',
  'IF(iq_.Tipo=0, iq_.RazaoSocial, iq_.Nome) NomeIQ, ',
  'pqc.KgLiqEmb, pqc.Peso PesoEstq ',
  'FROM pq pq_ ',
  'LEFT JOIN entidades iq_ ON pq_.IQ=iq_.Codigo ',
  'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo AND pqc.CI=' + Geral.FF0(CliInt),
  'WHERE pq_.Codigo>0 ',
  //'AND pq_.GrupoQuimico & 55 ',
  'ORDER BY pq_.Nome ',
  '  ']);
  //
end;

procedure TFmPQURec.SbPQClick(Sender: TObject);
var
  Insumo: Integer;
begin
  Insumo       := EdPQ.ValueVariant;
  VAR_CADASTRO := 0;
  App_Jan.MostraFormPQ(Insumo);
  UMyMod.SetaCodigoPesquisado(EdPQ, CBPQ, QrPQ, VAR_CADASTRO);
end;

end.
