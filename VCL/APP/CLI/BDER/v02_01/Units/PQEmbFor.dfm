object FmPQEmbFor: TFmPQEmbFor
  Left = 339
  Top = 185
  Caption = 'QUI-INSUM-004 :: Defini'#231#227'o de Embalagem'
  ClientHeight = 365
  ClientWidth = 583
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 81
    Width = 583
    Height = 176
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 84
      Width = 58
      Height = 13
      Caption = 'Embalagem:'
    end
    object Label2: TLabel
      Left = 8
      Top = 4
      Width = 85
      Height = 13
      Caption = 'C'#243'd. Fornecedor: '
      Enabled = False
    end
    object Label3: TLabel
      Left = 8
      Top = 44
      Width = 66
      Height = 13
      Caption = 'Observa'#231#245'es:'
    end
    object SpeedButton1: TSpeedButton
      Left = 552
      Top = 100
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object Label4: TLabel
      Left = 8
      Top = 124
      Width = 40
      Height = 13
      Caption = 'Produto:'
    end
    object SbPQ: TSpeedButton
      Left = 552
      Top = 140
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbPQClick
    end
    object Label5: TLabel
      Left = 172
      Top = 4
      Width = 54
      Height = 13
      Caption = 'Descri'#231#227'o: '
      Enabled = False
    end
    object CBEmbalagem: TdmkDBLookupComboBox
      Left = 64
      Top = 100
      Width = 485
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsEmbalagens
      TabOrder = 3
      dmkEditCB = EdEmbalagem
      QryCampo = 'Embalagem'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdEmbalagem: TdmkEditCB
      Left = 8
      Top = 100
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Embalagem'
      UpdCampo = 'Embalagem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEmbalagem
      IgnoraDBLookupComboBox = False
    end
    object EdcProd: TdmkEdit
      Left = 8
      Top = 20
      Width = 161
      Height = 21
      Enabled = False
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'GraGruX'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdObservacao: TdmkEdit
      Left = 8
      Top = 60
      Width = 565
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'GraGruX'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdPQ: TdmkEditCB
      Left = 8
      Top = 140
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Embalagem'
      UpdCampo = 'Embalagem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPQ
      IgnoraDBLookupComboBox = False
    end
    object CBPQ: TdmkDBLookupComboBox
      Left = 64
      Top = 140
      Width = 485
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPQ
      TabOrder = 5
      dmkEditCB = EdPQ
      QryCampo = 'Embalagem'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdxProd: TdmkEdit
      Left = 172
      Top = 20
      Width = 401
      Height = 21
      Enabled = False
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'GraGruX'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 48
    Width = 583
    Height = 33
    Align = alTop
    Alignment = taCenter
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Lines.Strings = (
      'N'#227'o foi encontrado cadastro para o item abaixo.'
      'Selecione o cadastro correspondente para associ'#225'-lo!')
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 583
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 535
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 487
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 301
        Height = 32
        Caption = 'Defini'#231#227'o de Embalagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 301
        Height = 32
        Caption = 'Defini'#231#227'o de Embalagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 301
        Height = 32
        Caption = 'Defini'#231#227'o de Embalagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 257
    Width = 583
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 579
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 301
    Width = 583
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 579
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 435
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object DsEmbalagens: TDataSource
    DataSet = QrEmbalagens
    Left = 36
    Top = 12
  end
  object QrEmbalagens: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM embalagens'
      'ORDER BY Nome')
    Left = 8
    Top = 12
    object QrEmbalagensCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmbalagensNome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object QrPQ: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM PQ'
      'ORDER BY Nome')
    Left = 68
    Top = 12
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 96
    Top = 12
  end
  object PMPQ: TPopupMenu
    Left = 508
    Top = 136
    object CadastroAutomtico1: TMenuItem
      Caption = 'Cadastro &Autom'#225'tico'
      Enabled = False
    end
    object CadastroManual1: TMenuItem
      Caption = 'Cadastro &Manual'
      OnClick = CadastroManual1Click
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM pqfor'
      'WHERE CI=:P0'
      'AND IQ=:P1'
      'AND PQ=:P2')
    Left = 464
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLocControle: TIntegerField
      FieldName = 'Controle'
    end
  end
end
