object FmFormulasPesq: TFmFormulasPesq
  Left = 339
  Top = 185
  Caption = 'QUI-RECEI-002 :: Pesquisa de Receitas por Nome'
  ClientHeight = 521
  ClientWidth = 752
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 752
    Height = 365
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object LaTotal: TLabel
      Left = 0
      Top = 352
      Width = 752
      Height = 13
      Align = alBottom
      ExplicitWidth = 3
    end
    object DBGrid1: TdmkDBGrid
      Left = 0
      Top = 157
      Width = 752
      Height = 195
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'NumCODIF'
          Title.Caption = 'N'#250'mero'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 450
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Setor_TXT'
          Title.Caption = 'Setor'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Numero'
          Title.Caption = 'ID'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsFormulas
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'NumCODIF'
          Title.Caption = 'N'#250'mero'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 450
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Setor_TXT'
          Title.Caption = 'Setor'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Numero'
          Title.Caption = 'ID'
          Visible = True
        end>
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 752
      Height = 157
      Align = alTop
      TabOrder = 1
      object LaPrompt: TLabel
        Left = 12
        Top = 92
        Width = 88
        Height = 13
        Caption = 'Cliente espec'#237'fico:'
      end
      object Panel3: TPanel
        Left = 1
        Top = 1
        Width = 750
        Height = 50
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 8
          Width = 101
          Height = 13
          Caption = 'Digite parte do nome:'
        end
        object Edit1: TEdit
          Left = 8
          Top = 24
          Width = 321
          Height = 21
          TabOrder = 0
          OnChange = Edit1Change
        end
        object RGMascara: TRadioGroup
          Left = 337
          Top = 0
          Width = 413
          Height = 50
          Align = alRight
          Caption = ' M'#225'scara de pesquisa: '
          Columns = 3
          ItemIndex = 1
          Items.Strings = (
            'Nenhum'
            'Prefixo, entre e sufixo'
            'Prefixo e sufixo'
            'Prefixo'
            'Entre'
            'Sufixo')
          TabOrder = 1
        end
      end
      object RGAtivo: TRadioGroup
        Left = 9
        Top = 52
        Width = 220
        Height = 37
        Caption = ' Situa'#231#227'o: '
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Ambos'
          'Ativas'
          'Inativas')
        TabOrder = 1
      end
      object RGArquivo: TRadioGroup
        Left = 233
        Top = 52
        Width = 176
        Height = 37
        Caption = ' Arquivo: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Em uso'
          'Morto')
        TabOrder = 2
        OnClick = RGArquivoClick
      end
      object RGAmbAplic: TRadioGroup
        Left = 413
        Top = 52
        Width = 332
        Height = 37
        Caption = ' Ambiente de aplica'#231#227'o: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Ambos'
          'Produ'#231#227'o'
          'Amostra')
        TabOrder = 3
        OnClick = RGArquivoClick
      end
      object EdCliente: TdmkEditCB
        Left = 12
        Top = 108
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 80
        Top = 108
        Width = 557
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_CLIENTE'
        ListSource = DsEntidades
        TabOrder = 5
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 752
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 704
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 656
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 390
        Height = 32
        Caption = 'Pesquisa de Receitas por Nome'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 390
        Height = 32
        Caption = 'Pesquisa de Receitas por Nome'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 390
        Height = 32
        Caption = 'Pesquisa de Receitas por Nome'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 413
    Width = 752
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 748
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 457
    Width = 752
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 748
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 605
        Top = 0
        Width = 143
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtPesquisa: TBitBtn
        Tag = 22
        Left = 12
        Top = 3
        Width = 122
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtLocaliza: TBitBtn
        Tag = 7
        Left = 134
        Top = 3
        Width = 122
        Height = 40
        Caption = '&Localiza'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtLocalizaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 256
        Top = 3
        Width = 122
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtImprimeClick
      end
      object BtResgata: TBitBtn
        Left = 380
        Top = 3
        Width = 122
        Height = 40
        Cursor = crHandPoint
        Caption = '&Resgata'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtResgataClick
      end
    end
  end
  object QrFormulas: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFormulasAfterOpen
    BeforeClose = QrFormulasBeforeClose
    AfterScroll = QrFormulasAfterScroll
    OnCalcFields = QrFormulasCalcFields
    SQL.Strings = (
      'SELECT fml.Numero, fml.Nome, '
      'lse.Nome Setor_TXT, fml.Ativo'
      'FROM formulas fml'
      'LEFT JOIN listasetores lse ON lse.Codigo=fml.Setor'
      'WHERE fml.Nome LIKE :P0'
      'AND fml.Numero<> 0')
    Left = 429
    Top = 53
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFormulasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrFormulasAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrFormulasNumCODIF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NumCODIF'
      Size = 11
      Calculated = True
    end
    object QrFormulasSetor_TXT: TWideStringField
      FieldName = 'Setor_TXT'
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 457
    Top = 53
  end
  object frxDsFormulas: TfrxDBDataset
    UserName = 'frxDsFormulas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Numero=Numero'
      'Nome=Nome'
      'Ativo=Ativo'
      'Setor_TXT=Setor_TXT')
    DataSet = QrFormulas
    BCDToCurrency = False
    DataSetOptions = []
    Left = 284
    Top = 208
  end
  object frxFormulas: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.407328206020000000
    ReportOptions.LastChange = 39949.407328206020000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxFormulasGetValue
    Left = 256
    Top = 208
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader5: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 75.590590240000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Width = 1046.929810000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 702.992125984252000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 18.897650000000000000
          Width = 377.952755905511800000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'PESQUISA DE RECEITAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 37.795300000000000000
          Width = 702.992125980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Top = 60.472479999999990000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ativo')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 60.472479999999990000
          Width = 468.661639450000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 60.472479999999990000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'N'#250'mero')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031849999999900000
          Top = 18.897650000000000000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 60.472479999999990000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Setor')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 154.960730000000000000
        Width = 718.110700000000000000
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
        RowCount = 0
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Width = 468.661666300000000000
          Height = 15.118110240000000000
          DataSet = frxDsFormulas
          DataSetName = 'frxDsFormulas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFormulas."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 56.692920710000000000
          Height = 15.118110240000000000
          DataSet = frxDsFormulas
          DataSetName = 'frxDsFormulas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulas."Numero"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object CheckBox1: TfrxCheckBoxView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'Ativo'
          DataSet = frxDsFormulas
          DataSetName = 'frxDsFormulas'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Width = 132.283464566929100000
          Height = 15.118110240000000000
          DataField = 'Setor_TXT'
          DataSet = frxDsFormulas
          DataSetName = 'frxDsFormulas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFormulas."Setor_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 230.551330000000000000
        Width = 718.110700000000000000
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 702.992580000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object MePagina: TfrxMemoView
        AllowVectorExport = True
        Left = 895.748610000000000000
        Top = 37.795300000000000000
        Width = 143.622140000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'P'#225'gina [Page#] de [TotalPages#]')
        ParentFont = False
        VAlign = vaCenter
      end
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END Descricao, Codigo'
      'FROM entidades '
      'ORDER BY Descricao')
    Left = 208
    Top = 66
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 120
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 208
    Top = 118
  end
end
