unit Pallets;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, Grids,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  DBGrids, Menus, Math, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkCheckGroup, UnDmkProcFunc, dmkLabel, dmkImage, UnDmkEnums,
  UnDeprecated_PF, DmkDAC_PF;

type
  TFmPallets = class(TForm)
    PainelDados: TPanel;
    DsPallets: TDataSource;
    QrPallets: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    QrPalletsCodigo: TIntegerField;
    QrPalletsPallet: TWideStringField;
    QrPalletsLk: TIntegerField;
    QrPalletsDataCad: TDateField;
    QrPalletsDataAlt: TDateField;
    QrPalletsUserCad: TIntegerField;
    QrPalletsUserAlt: TIntegerField;
    Label2: TLabel;
    QrPalletsDataI: TDateField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    PnCouros: TPanel;
    PnCourosIn: TPanel;
    QrPalletsIts: TmySQLQuery;
    DsPalletsIts: TDataSource;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    DBGrid2: TDBGrid;
    QrDonos: TmySQLQuery;
    QrDonosCodigo: TIntegerField;
    QrDonosNOMEENTIDADE: TWideStringField;
    DsDonos: TDataSource;
    QrPalletsCliInt: TIntegerField;
    QrPalletsNOMECLIINT: TWideStringField;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    Panel2: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    Label4: TLabel;
    TPDataI: TDateTimePicker;
    EdClienteI: TdmkEditCB;
    Label5: TLabel;
    CBClienteI: TdmkDBLookupComboBox;
    PnAdicoes: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit01: TDBEdit;
    DBEdit02: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Panel4: TPanel;
    Label13: TLabel;
    EdMPArti: TdmkEditCB;
    CBMPArti: TdmkDBLookupComboBox;
    Label14: TLabel;
    EdMPGrup: TdmkEditCB;
    CBMPGrup: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdMPClas: TdmkEditCB;
    CBMPClas: TdmkDBLookupComboBox;
    QrMPArti: TmySQLQuery;
    DsMPArti: TDataSource;
    QrMPGrup: TmySQLQuery;
    DsMPGrup: TDataSource;
    QrMPArtiCodigo: TIntegerField;
    QrMPArtiNome: TWideStringField;
    QrMPGrupCodigo: TIntegerField;
    QrMPGrupNome: TWideStringField;
    Label16: TLabel;
    EdPecas: TdmkEdit;
    Label17: TLabel;
    EdM2: TdmkEdit;
    Label18: TLabel;
    EdP2: TdmkEdit;
    FM2: TdmkEdit;
    FP2: TdmkEdit;
    PMPallet: TPopupMenu;
    CrianovoPallet1: TMenuItem;
    AlteraPalletatual1: TMenuItem;
    ExcluiPalletatual1: TMenuItem;
    RGMutaveis: TdmkCheckGroup;
    QrUpd: TmySQLQuery;
    DBGOS: TDBGrid;
    QrPalet: TmySQLQuery;
    QrPaletPalet: TIntegerField;
    QrPaletData: TDateField;
    QrPaletOS: TIntegerField;
    QrPaletCliInt: TIntegerField;
    QrPaletProcede: TIntegerField;
    QrPaletMarca: TWideStringField;
    QrPaletNOMECLIINT: TWideStringField;
    QrPaletNOMEPROCEDE: TWideStringField;
    QrPaletPecas: TFloatField;
    QrPaletSaida1: TFloatField;
    QrPaletSaida2: TFloatField;
    QrPaletSaldo: TFloatField;
    DsPalet: TDataSource;
    QrPaletLote: TWideStringField;
    QrMPClas: TmySQLQuery;
    DsMPClas: TDataSource;
    QrMPClasCodigo: TIntegerField;
    QrMPClasNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel9: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel10: TPanel;
    BtSaida: TBitBtn;
    BtPallet: TBitBtn;
    BtCouros: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel11: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GroupBox1: TGroupBox;
    Panel12: TPanel;
    Panel13: TPanel;
    BtConfirma2: TBitBtn;
    BitBtn1: TBitBtn;
    BtSaida2: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtPalletClick(Sender: TObject);
    procedure BtCourosClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPalletsAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrPalletsAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPalletsBeforeOpen(DataSet: TDataSet);
    procedure QrPalletsBeforeClose(DataSet: TDataSet);
    procedure EdMPArtiExit(Sender: TObject);
    procedure EdMPGrupExit(Sender: TObject);
    procedure EdMPClasExit(Sender: TObject);
    procedure EdM2Exit(Sender: TObject);
    procedure EdP2Exit(Sender: TObject);
    procedure CrianovoPallet1Click(Sender: TObject);
    procedure AlteraPalletatual1Click(Sender: TObject);
    procedure BtSaida2Click(Sender: TObject);
    procedure RGMutaveisChange(Sender: TObject; ButtonIndex: Integer);
    procedure RGMutaveisClick(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrPaletAfterOpen(DataSet: TDataSet);
    procedure EdPecasExit(Sender: TObject);
    procedure CBMPArtiExit(Sender: TObject);
    procedure CBMPGrupExit(Sender: TObject);
    procedure CBMPClasExit(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    FPalletsIts: Integer;
    FMaxMutavel: Integer;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //////////////////////
    procedure ReopenPalletsIts;
    procedure ConfiguraTabStops;
    procedure InsereCouroArea;
  public
    { Public declarations }
    FSeq: Integer;
    procedure ReopenLPalet;
    procedure VeSeConfirma(ID: Integer);
  end;

var
  FmPallets: TFmPallets;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, BlueDermConsts, PalletsOrigem, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPallets.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPallets.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPalletsCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPallets.DefParams;
begin
  VAR_GOTOTABELA := 'Pallets';
  VAR_GOTOMYSQLTABLE := QrPallets;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := 'Pallet';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial');
  VAR_SQLx.Add('ELSE cli.Nome END NOMECLIINT, pal.*');
  VAR_SQLx.Add('FROM pallets pal');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=pal.CliInt');
  VAR_SQLx.Add('WHERE pal.Codigo > -1000');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND pal.Codigo=:P0');
  //
  VAR_SQLa.Add('AND pal.Pallet Like :P0');
  //
end;

procedure TFmPallets.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PnAdicoes.Visible   := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      if SQLType = stIns then
      begin
        EdCodigo.Text := '';
        EdNome.Text   := '';
        TPDataI.Date  := Date;
        EdClienteI.Text := '';
        CBClienteI.KeyValue := Null;
        EdClienteI.Enabled := True;
        CBClienteI.Enabled := True;
      end else begin
        EdCodigo.Text := IntToStr(QrPalletsCodigo.Value);
        EdNome.Text   := QrPalletsPallet.Value;
        TPDataI.Date  := QrPalletsDataI.Value;
        EdClienteI.Text := IntToStr(QrPalletsCliInt.Value);
        CBClienteI.KeyValue := QrPalletsCliInt.Value;
        if (QrPalletsIts.State <> dsBrowse) or (QrPalletsIts.RecordCount > 0) then
        begin
          EdClienteI.Enabled := False;
          CBClienteI.Enabled := False;
        end;
      end;
      EdNome.SetFocus;
    end;
    2:
    begin
      PnAdicoes.Visible   := True;
      PainelDados.Visible := False;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmPallets.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPallets.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPallets.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPallets.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPallets.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPallets.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPallets.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPallets.BtPalletClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPallet, BtPallet);
end;

procedure TFmPallets.BtCourosClick(Sender: TObject);
begin
  //MyObjects.MostraPopUpDeBotao(PMCouros, BtCouros);
  MostraEdicao(2, stIns, 0);
end;

procedure TFmPallets.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPalletsCodigo.Value;
  Close;
end;

procedure TFmPallets.BtConfirmaClick(Sender: TObject);
var
  Codigo, CliInt: Integer;
  Nome : String;
begin
  Nome   := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    EdNome.SetFocus;
    Exit;
  end;
  CliInt := Geral.IMV(EdClienteI.Text);
  if CliInt = 0 then
  begin
    Application.MessageBox('Defina o cliente interno.', 'Erro', MB_OK+MB_ICONERROR);
    if EdClienteI.Enabled then EdClienteI.SetFocus;
    Exit;
  end;
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO pallets SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Pallets', 'Pallets', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE pallets SET ');
    Codigo := QrPalletsCodigo.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Pallet=:P0, DataI=:P1, CliInt=:P2, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsString  := Geral.FDT(TPDataI.Date, 1);
  Dmod.QrUpdU.Params[02].AsInteger := CliInt;
  //
  Dmod.QrUpdU.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[04].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[05].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Pallets', 'Codigo');
  MostraEdicao(0, stLok, 0);
  LocCod(Codigo,Codigo);
  if FSeq = 1 then Close;
end;

procedure TFmPallets.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Pallets', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Pallets', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Pallets', 'Codigo');
end;

procedure TFmPallets.FormCreate(Sender: TObject);
var
  Padrao: Integer;
begin
  ImgTipo.SQLType := stLok;
  //
  Padrao := Trunc((Power(2, RGMutaveis.Items.Count-1))*2-1);
  RGMutaveis.Value := dmkPF.ObtemInfoRegEdit('Mutaveis', Caption,
    Padrao, ktInteger);
  FSeq := 0;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PnCouros.Align    := alClient;
  //
  TPDataI.Date := Date;
  //
  UnDmkDAC_PF.AbreQuery(QrDonos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMPArti, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMPGrup, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMPClas, Dmod.MyDB);
  //
  CriaOForm;
  //
  QrUpd.SQL.Clear;
  QrUpd.SQL.Add('INSERT INTO palletsits SET');
  QrUpd.SQL.Add('  MPIn=:P0');
  QrUpd.SQL.Add(', MPArti=:P1');
  QrUpd.SQL.Add(', MPGrup=:P2');
  QrUpd.SQL.Add(', MPClas=:P3');
  QrUpd.SQL.Add(', Pecas=:P4');
  QrUpd.SQL.Add(', M2=:P5');
  QrUpd.SQL.Add(', P2=:P6');
  QrUpd.SQL.Add('');
  QrUpd.SQL.Add('');
  QrUpd.SQL.Add(', Codigo=:Pa');
  QrUpd.SQL.Add(', Controle=:Pb');
  //
  ReopenLPalet;
end;

procedure TFmPallets.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPalletsCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPallets.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmPallets.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPallets.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmPallets.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dmkPF.SalvaInfoRegEdit('Mutaveis', Caption, RGMutaveis.Value, ktInteger);
  VAR_PALLET_BDC := QrPalletsPallet.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPallets.QrPalletsAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPallets.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Pallets', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, stIns, 0);
  ConfiguraTabStops;
end;

procedure TFmPallets.QrPalletsAfterScroll(DataSet: TDataSet);
begin
  BtCouros.Enabled := GOTOy.BtEnabled(QrPalletsCodigo.Value, False);
  ReopenPalletsIts;
end;

procedure TFmPallets.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPalletsCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, 'Pallet', 'Pallets', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPallets.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPallets.QrPalletsBeforeOpen(DataSet: TDataSet);
begin
  QrPalletsCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPallets.QrPalletsBeforeClose(DataSet: TDataSet);
begin
  QrPalletsIts.Close;
end;

procedure TFmPallets.ReopenPalletsIts;
begin
  QrPalletsIts.Close;
  QrPalletsIts.Params[0].AsInteger := QrPalletsCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPalletsIts, Dmod.MyDB);
  //
  if FPalletsIts <> 0 then QrPalletsIts.Locate('Controle', FPalletsIts, []);
end;

procedure TFmPallets.EdMPArtiExit(Sender: TObject);
begin
  VeSeConfirma(8);
end;

procedure TFmPallets.EdMPGrupExit(Sender: TObject);
begin
  VeSeConfirma(32);
end;

procedure TFmPallets.EdMPClasExit(Sender: TObject);
begin
  VeSeConfirma(128);
end;

procedure TFmPallets.EdM2Exit(Sender: TObject);
begin
  Deprecated_PF.VerificaAlteracaoMetrica(cgMtoFT, EdM2, EdP2, FM2, FP2, 2);
  VeSeConfirma(2);
end;

procedure TFmPallets.EdP2Exit(Sender: TObject);
begin
  Deprecated_PF.VerificaAlteracaoMetrica(cgFTtoM, EdM2, EdP2, FM2, FP2, 2);
  VeSeConfirma(4);
end;

procedure TFmPallets.CrianovoPallet1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmPallets.AlteraPalletatual1Click(Sender: TObject);
var
  Pallets : Integer;
begin
  Pallets := QrPalletsCodigo.Value;
  if not UMyMod.SelLockY(Pallets, Dmod.MyDB, 'Pallets', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Pallets, Dmod.MyDB, 'Pallets', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPallets.BtSaida2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmPallets.RGMutaveisChange(Sender: TObject;
  ButtonIndex: Integer);
begin
  ConfiguraTabStops;
end;

procedure TFmPallets.ConfiguraTabStops;
var
  i, j: Integer;
begin
  EdPecas.TabStop     := dmkPF.IntInConjunto2(001, RGMutaveis.Value);
  EdM2.TabStop        := dmkPF.IntInConjunto2(002, RGMutaveis.Value);
  EdP2.TabStop        := dmkPF.IntInConjunto2(004, RGMutaveis.Value);
  EdMPArti.TabStop    := dmkPF.IntInConjunto2(008, RGMutaveis.Value);
  CBMPArti.TabStop    := dmkPF.IntInConjunto2(016, RGMutaveis.Value);
  EdMPGrup.TabStop    := dmkPF.IntInConjunto2(032, RGMutaveis.Value);
  CBMPGrup.TabStop    := dmkPF.IntInConjunto2(064, RGMutaveis.Value);
  EdMPClas.TabStop    := dmkPF.IntInConjunto2(128, RGMutaveis.Value);
  CBMPClas.TabStop    := dmkPF.IntInConjunto2(256, RGMutaveis.Value);
  BtConfirma2.TabStop := dmkPF.IntInConjunto2(512, RGMutaveis.Value);
  FMaxMutavel := -1;
  j := RGMutaveis.Items.Count;
  //Trunc((Power(2, RGMutaveis.Items.Count-1))*2-1);
  while FMaxMutavel = -1 do
  begin
    i := Trunc(Power(2, j));
    if dmkPF.IntInConjunto2(i, RGMutaveis.Value) then
    begin
      FMaxMutavel := i;
      Exit;
    end;
    j := j - 1;
  end;
end;

procedure TFmPallets.RGMutaveisClick(Sender: TObject);
begin
  ConfiguraTabStops;
end;

procedure TFmPallets.BtConfirma2Click(Sender: TObject);
begin
  InsereCouroArea;
end;

procedure TFmPallets.InsereCouroArea;
var
  Codigo, Controle, MPIn, MPArti, MPGrup, MPClass, Pecas: Integer;
  M2, P2: Double;
begin
  MPIn := QrPaletOS.Value;  //MPInControle
  if Deprecated_PF.SQLZeroI(MPIn, 'Defina a OS de origem!', nil) then exit;
  //
  MPArti := Geral.IMV(EdMPArti.Text);
  if Deprecated_PF.SQLZeroI(MPArti, 'Defina o artigo!', nil) then exit;
  //
  MPGrup := Geral.IMV(EdMPGrup.Text);
  if Deprecated_PF.SQLZeroI(MPGrup, 'Defina o tamanho!', nil) then exit;
  //
  MPClass := Geral.IMV(EdMPClas.Text);
  if Deprecated_PF.SQLZeroI(MPClass, 'Defina a classe!', nil) then exit;
  //
  Pecas := Geral.IMV(EdPecas.Text);
  if Deprecated_PF.SQLZeroI(Pecas, 'Defina a quantidade de pe�as!', nil) then exit;
  //
  M2 := Geral.DMV(EdM2.Text);
  if Deprecated_PF.SQLZeroD(M2, 'Defina a �rea!', nil) then exit;
  //
  P2 := Geral.DMV(EdP2.Text);
  if Deprecated_PF.SQLZeroD(P2, 'Defina a �rea!', nil) then exit;
  //
  Codigo   := QrPalletsCodigo.Value;
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PalletsIts', 'PalletsIts', 'Controle');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO palletsits SET ');
  Dmod.QrUpd.SQL.Add('MPIn=:P0, MPArti=:P1, MPGrup=:P2, Pecas=:P3, ');
  Dmod.QrUpd.SQL.Add('MPClass=:P4, M2=:P5, P2=:P6, ');
  Dmod.QrUpd.SQL.Add('Codigo=:Pa, Controle=:Pb');
  Dmod.QrUpd.SQL.Add('');
  //
  Dmod.QrUpd.Params[00].AsInteger := MPIn;
  Dmod.QrUpd.Params[01].AsInteger := MPArti;
  Dmod.QrUpd.Params[02].AsInteger := MPGrup;
  Dmod.QrUpd.Params[03].AsInteger := Pecas;
  Dmod.QrUpd.Params[04].AsInteger := MPClass;
  Dmod.QrUpd.Params[05].AsFloat   := M2;
  Dmod.QrUpd.Params[06].AsFloat   := P2;
  Dmod.QrUpd.Params[07].AsInteger := Codigo;
  Dmod.QrUpd.Params[08].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  if dmkPF.IntInConjunto2(001, RGMutaveis.Value) then EdPecas.Text := '';
  if dmkPF.IntInConjunto2(002, RGMutaveis.Value) then EdM2.Text := '';
  if dmkPF.IntInConjunto2(004, RGMutaveis.Value) then EdP2.Text := '';
  if dmkPF.IntInConjunto2(008, RGMutaveis.Value) then EdMPArti.Text := '';
  if dmkPF.IntInConjunto2(008, RGMutaveis.Value) then CBMPArti.KeyValue := NULL;
  if dmkPF.IntInConjunto2(016, RGMutaveis.Value) then EdMPArti.Text := '';
  if dmkPF.IntInConjunto2(016, RGMutaveis.Value) then CBMPArti.KeyValue := NULL;
  if dmkPF.IntInConjunto2(032, RGMutaveis.Value) then EdMPGrup.Text := '';
  if dmkPF.IntInConjunto2(032, RGMutaveis.Value) then CBMPGrup.KeyValue := NULL;
  if dmkPF.IntInConjunto2(064, RGMutaveis.Value) then EdMPGrup.Text := '';
  if dmkPF.IntInConjunto2(064, RGMutaveis.Value) then CBMPGrup.KeyValue := NULL;
  if dmkPF.IntInConjunto2(128, RGMutaveis.Value) then EdMPClas.Text := '';
  if dmkPF.IntInConjunto2(128, RGMutaveis.Value) then CBMPClas.KeyValue := NULL;
  if dmkPF.IntInConjunto2(256, RGMutaveis.Value) then EdMPClas.Text := '';
  if dmkPF.IntInConjunto2(256, RGMutaveis.Value) then CBMPClas.KeyValue := NULL;
  //
  if dmkPF.IntInConjunto2(001, RGMutaveis.Value) then EdPecas.SetFocus  else
  if dmkPF.IntInConjunto2(002, RGMutaveis.Value) then EdM2.SetFocus     else
  if dmkPF.IntInConjunto2(004, RGMutaveis.Value) then EdP2.SetFocus     else
  if dmkPF.IntInConjunto2(008, RGMutaveis.Value) then EdMPArti.SetFocus else
  if dmkPF.IntInConjunto2(016, RGMutaveis.Value) then CBMPArti.SetFocus else
  if dmkPF.IntInConjunto2(032, RGMutaveis.Value) then EdMPGrup.SetFocus else
  if dmkPF.IntInConjunto2(064, RGMutaveis.Value) then CBMPGrup.SetFocus else
  if dmkPF.IntInConjunto2(128, RGMutaveis.Value) then EdMPClas.SetFocus else
  if dmkPF.IntInConjunto2(226, RGMutaveis.Value) then CBMPClas.SetFocus
end;

procedure TFmPallets.BitBtn1Click(Sender: TObject);
begin
  Application.CreateForm(TFmPalletsOrigem, FmPalletsOrigem);
  FmPalletsOrigem.FPallet := QrPalletsCodigo.Value;
  FmPalletsOrigem.ShowModal;
  FmPalletsOrigem.Destroy;
end;

procedure TFmPallets.ReopenLPalet;
begin
  QrPalet.Close;
  UnDmkDAC_PF.AbreQuery(QrPalet, DModG.MyPID_DB);
end;

procedure TFmPallets.QrPaletAfterOpen(DataSet: TDataSet);
var
  Lin: Integer;
begin
  Lin := QrPalet.RecordCount;
  if Lin = 0 then Lin := 1;
  DBGOS.Height := 20 + (Lin * 19);
end;

procedure TFmPallets.EdPecasExit(Sender: TObject);
begin
  EdPecas.Text := Geral.TFT(EdPecas.Text, 0, siPositivo);
  VeSeConfirma(1);
end;

procedure TFmPallets.CBMPArtiExit(Sender: TObject);
begin
  VeSeConfirma(16);
end;

procedure TFmPallets.CBMPGrupExit(Sender: TObject);
begin
  VeSeConfirma(64);
end;

procedure TFmPallets.CBMPClasExit(Sender: TObject);
begin
  VeSeConfirma(256);
end;

procedure TFmPallets.VeSeConfirma(ID: Integer);
begin
  if ID = FMaxMutavel then BtConfirma2Click(Self);
end;

end.

