unit WBMPrCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkMemo, frxClass,
  frxDBSet, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO;

type
  TFmWBMPrCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtFornece: TBitBtn;
    BtMP: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrWBMPrCab: TmySQLQuery;
    DsWBMPrCab: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdGraGruX: TdmkEdit;
    Label9: TLabel;
    EdTxtNomeTamCor: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrWBMPrCabGraGruX: TIntegerField;
    QrWBMPrCabLk: TIntegerField;
    QrWBMPrCabDataCad: TDateField;
    QrWBMPrCabDataAlt: TDateField;
    QrWBMPrCabUserCad: TIntegerField;
    QrWBMPrCabUserAlt: TIntegerField;
    QrWBMPrCabAlterWeb: TSmallintField;
    QrWBMPrCabAtivo: TSmallintField;
    QrWBMPrCabGraGru1: TIntegerField;
    QrWBMPrCabNO_PRD_TAM_COR: TWideStringField;
    QrWBMPrCabOrdem: TIntegerField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    EdOrdem: TdmkEdit;
    EdBRLMedM2: TdmkEdit;
    Label5: TLabel;
    QrWBMPrCabBRLMedM2: TFloatField;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    frxWET_RECUR_013: TfrxReport;
    frxDsCad: TfrxDBDataset;
    QrCad: TmySQLQuery;
    QrCadGraGruX: TIntegerField;
    QrCadLk: TIntegerField;
    QrCadDataCad: TDateField;
    QrCadDataAlt: TDateField;
    QrCadUserCad: TIntegerField;
    QrCadUserAlt: TIntegerField;
    QrCadAlterWeb: TSmallintField;
    QrCadAtivo: TSmallintField;
    QrCadGraGru1: TIntegerField;
    QrCadNO_PRD_TAM_COR: TWideStringField;
    QrCadOrdem: TIntegerField;
    QrCadBRLMedM2: TFloatField;
    PMMP: TPopupMenu;
    IncluiMP1: TMenuItem;
    AlteraMP1: TMenuItem;
    ExcluiMP1: TMenuItem;
    PMFornece: TPopupMenu;
    IncluiFornece1: TMenuItem;
    AlteraFornece1: TMenuItem;
    ExcluiFornece1: TMenuItem;
    QrWBMPrFrn: TmySQLQuery;
    DsWBMPrFrn: TDataSource;
    QrWBMPrFrnCodigo: TIntegerField;
    QrWBMPrFrnGraGruX: TIntegerField;
    QrWBMPrFrnFornece: TIntegerField;
    QrWBMPrFrnM2Medio: TFloatField;
    QrWBMPrFrnLk: TIntegerField;
    QrWBMPrFrnDataCad: TDateField;
    QrWBMPrFrnDataAlt: TDateField;
    QrWBMPrFrnUserCad: TIntegerField;
    QrWBMPrFrnUserAlt: TIntegerField;
    QrWBMPrFrnAlterWeb: TSmallintField;
    QrWBMPrFrnAtivo: TSmallintField;
    QrWBMPrFrnNO_FORNECE: TWideStringField;
    DBGWBMPrFrn: TdmkDBGridZTO;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWBMPrCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWBMPrCabBeforeOpen(DataSet: TDataSet);
    procedure BtMPClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PMMPPopup(Sender: TObject);
    procedure IncluiMP1Click(Sender: TObject);
    procedure AlteraMP1Click(Sender: TObject);
    procedure ExcluiMP1Click(Sender: TObject);
    procedure BtForneceClick(Sender: TObject);
    procedure IncluiFornece1Click(Sender: TObject);
    procedure AlteraFornece1Click(Sender: TObject);
    procedure ExcluiFornece1Click(Sender: TObject);
    procedure PMFornecePopup(Sender: TObject);
    procedure QrWBMPrCabBeforeClose(DataSet: TDataSet);
    procedure QrWBMPrCabAfterScroll(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, GraGruX: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure MostraWBMPrFrn(SQLType: TSQLType);
    procedure ReopenWBMPrFrn(Codigo: Integer);

  public
    { Public declarations }
  end;

var
  FmWBMPrCab: TFmWBMPrCab;
const
  FFormatFloat = '00000';
  FCo_Codigo = 'GraGruX';
  FCo_Nome = 'CONCAT(gg1.Nome, ' + sLineBreak +
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ' + sLineBreak +
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))';

implementation

uses UnMyObjects, Module, MeuDBUses, DMkDAC_PF, ModuleGeral, MyDBCheck,
  WBMPrFrn;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWBMPrCab.LocCod(Atual, GraGruX: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, GraGruX);
end;

procedure TFmWBMPrCab.MostraWBMPrFrn(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmWBMPrFrn, FmWBMPrFrn, afmoNegarComAviso) then
  begin
    FmWBMPrFrn.ImgTipo.SQLType := SQLType;
    FmWBMPrFrn.FQrCab := QrWBMPrCab;
    FmWBMPrFrn.FDsCab := DsWBMPrCab;
    FmWBMPrFrn.FQrIts := QrWBMPrFrn;
    if SQLType = stIns then
    begin
      FmWBMPrFrn.EdFornece.Enabled      := True;
      FmWBMPrFrn.CBFornece.Enabled      := True;
    end else
    begin
      FmWBMPrFrn.EdCodigo.ValueVariant := QrWBMPrFrnCodigo.Value;
      //
      FmWBMPrFrn.EdM2Medio.ValueVariant := QrWBMPrFrnM2Medio.Value;
      FmWBMPrFrn.EdFornece.ValueVariant := QrWBMPrFrnFornece.Value;
      FmWBMPrFrn.CBFornece.KeyValue     := QrWBMPrFrnFornece.Value;
      //
      FmWBMPrFrn.EdFornece.Enabled      := False;
      FmWBMPrFrn.CBFornece.Enabled      := False;
      //
    end;
    FmWBMPrFrn.ShowModal;
    FmWBMPrFrn.Destroy;
  end;
end;

procedure TFmWBMPrCab.PMFornecePopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(AlteraFornece1, QrWBMPrCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraFornece1, QrWBMPrFrn);
  MyObjects.HabilitaMenuItemItsDel(AlteraFornece1, QrWBMPrFrn);
end;

procedure TFmWBMPrCab.PMMPPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(AlteraMP1, QrWBMPrCab);
  //MyObjects.HabilitaMenuItemCabDel(AlteraMP1, QrWBMPrCab);
end;

procedure TFmWBMPrCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWBMPrCabGraGruX.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWBMPrCab.DefParams;
begin
  VAR_GOTOTABELA := 'wbmprcab';
  VAR_GOTOMYSQLTABLE := QrWBMPrCab;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := FCo_Codigo;
  VAR_GOTONOME := FCo_Nome;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wmp.*,');
  VAR_SQLx.Add('ggx.GraGru1, CONCAT(gg1.Nome,');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))');
  VAR_SQLx.Add('NO_PRD_TAM_COR');
  VAR_SQLx.Add('FROM wbmprcab wmp');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  VAR_SQLx.Add('');
  //
  VAR_SQLx.Add('WHERE wmp.GraGruX > 0');
  //
  VAR_SQL1.Add('AND wmp.GraGruX=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE ' + FCo_Nome + ' LIKE :P0 ');
  //
end;

procedure TFmWBMPrCab.ExcluiFornece1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a retirada do fornecedor selecionado?',
  'wbmprfrn', 'Codigo', QrWBMPrFrnCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrWBMPrFrn,
      QrWBMPrFrnCodigo, QrWBMPrFrnCodigo.Value);
    ReopenWBMPrFrn(Codigo);
  end;
end;

procedure TFmWBMPrCab.ExcluiMP1Click(Sender: TObject);
begin
//
end;

procedure TFmWBMPrCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWBMPrCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWBMPrCab.ReopenWBMPrFrn(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBMPrFrn, Dmod.MyDB, [
  'SELECT frn.*, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE ',
  'FROM wbmprfrn frn ',
  'LEFT JOIN entidades ent ON ent.Codigo=frn.Fornece ',
  'WHERE frn.GraGruX=' + Geral.FF0(QrWBMPrCabGraGruX.Value),
  '']);
end;

procedure TFmWBMPrCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWBMPrCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWBMPrCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWBMPrCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWBMPrCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWBMPrCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWBMPrCab.AlteraFornece1Click(Sender: TObject);
begin
  MostraWBMPrFrn(stUpd);
end;

procedure TFmWBMPrCab.AlteraMP1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWBMPrCab, [PnDados],
  [PnEdita], EdBRLMedM2, ImgTipo, 'wbmprcab');
end;

procedure TFmWBMPrCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWBMPrCabGraGruX.Value;
  Close;
end;

procedure TFmWBMPrCab.BtConfirmaClick(Sender: TObject);
var
  GraGruX, Ordem: Integer;
  BRLMedM2: Double;
begin
  GraGruX        := EdGraGruX.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  BRLMedM2       := EdBRLMedM2.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'wbmprcab', False, [
  'Ordem', 'BRLMedM2'], [
  'GraGruX'], [
  Ordem, BRLMedM2], [
  GraGruX], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(GraGruX, GraGruX);
  end;
end;

procedure TFmWBMPrCab.BtDesisteClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(GraGruX, Dmod.MyDB, 'wbmprcab', FCo_Codigo);
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmWBMPrCab.BtForneceClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFornece, BtFornece);
end;

procedure TFmWBMPrCab.BtMPClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMP, BtMP);
end;

procedure TFmWBMPrCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DBGWBMPrFrn.Align := alClient;
  CriaOForm;
end;

procedure TFmWBMPrCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWBMPrCabGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmWBMPrCab.SbImprimeClick(Sender: TObject);
var
  Ordem: Integer;
  Ord_Txt: String;
begin
  Ordem := MyObjects.SelRadioGroup('Ordem de Impress�o',
  'Selecione a ordem de Impress�o', [
  'Ordem',
  'Nome',
  'Pre�o'], 2);
  case Ordem of
    0: Ord_Txt := 'Ordem';
    1: Ord_Txt := 'NO_PRD_TAM_COR';
    2: Ord_Txt := 'BRLMedM2';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCad, Dmod.MyDB, [
  'SELECT wmp.*, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM wbmprcab wmp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'ORDER BY ' + Ord_Txt,
  '']);
  MyObjects.frxDefineDataSets(frxWET_RECUR_013, [
  DModG.frxDsDono,
  frxDsCad]);
  MyObjects.frxMostra(frxWET_RECUR_013,
    'Configura��o de Mat�ria-prima para Semi / Acabado');
end;

procedure TFmWBMPrCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWBMPrCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrWBMPrCabGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmWBMPrCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWBMPrCab.QrWBMPrCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWBMPrCab.QrWBMPrCabAfterScroll(DataSet: TDataSet);
begin
  ReopenWBMPrFrn(0);
end;

procedure TFmWBMPrCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWBMPrCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWBMPrCabGraGruX.Value,
  CuringaLoc.CriaForm(FCo_Codigo, FCo_Nome, 'wbmprcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmWBMPrCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWBMPrCab.IncluiFornece1Click(Sender: TObject);
begin
  MostraWBMPrFrn(stIns);
end;

procedure TFmWBMPrCab.IncluiMP1Click(Sender: TObject);
var
  DefinedSQL: String;
  Qry: TmySQLQuery;
  GraGruX: Integer;
begin
{
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWBMPrCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'wbmprcab');
}
  DefinedSQL := Geral.ATS([
  'SELECT ggx.Controle _Codigo, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) _Nome ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) LIKE %s ',
  'ORDER BY _Nome ',
  '']);
  FmMeuDBUses.PesquisaNome('', '', '', DefinedSQL);
  GraGruX := VAR_CADASTRO;
  if GraGruX <> 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      //ShowMessage(Geral.FF0(VAR_CADASTRO));
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT GraGruX ',
      'FROM wbmprcab ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      '']);
      if Qry.FieldByName('GraGruX').AsInteger = GraGruX then
      begin
        Geral.MB_Aviso('O reduzido ' + Geral.FF0(GraGruX) +
        ' j� estava adicionado � configura��o de mat�ria-prima!');
      end else
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'wbmprcab', False, [
        ], [
        'GraGruX'], [
        ], [
        GraGruX], True);
      end;
      //
      LocCod(GraGruX, GraGruX);
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmWBMPrCab.QrWBMPrCabBeforeClose(DataSet: TDataSet);
begin
  QrWBMPrFrn.Close;
end;

procedure TFmWBMPrCab.QrWBMPrCabBeforeOpen(DataSet: TDataSet);
begin
  QrWBMPrCabGraGruX.DisplayFormat := FFormatFloat;
end;

end.

