unit PQEPed3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DBCtrls, Db, (*DBTables,*) Grids, DBGrids, ExtCtrls,
  dmkGeral, mySQLDbTables, Variants, dmkImage, UnDmkEnums, DmkDAC_PF, dmkEdit,
  Vcl.ComCtrls, dmkDBGrid, dmkCheckBox, dmkDBLookupComboBox, dmkEditCB,
  UnInternalConsts, UnGrl_Vars;

type
  TFmPQEPed3 = class(TForm)
    QrFornece: TmySQLQuery;
    QrPedidos: TmySQLQuery;
    DsFornece: TDataSource;
    DsPedidos: TDataSource;
    QrPedidosCodigo: TIntegerField;
    Panel2: TPanel;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTIDADE: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PnCfgPesq: TPanel;
    TabSheet2: TTabSheet;
    PainelDados: TPanel;
    Label1: TLabel;
    DBGrid1: TDBGrid;
    Panel5: TPanel;
    EdCodUsu: TdmkEdit;
    Label2: TLabel;
    EdReferencia: TdmkEdit;
    SbPesqCod: TSpeedButton;
    SbPesqRef: TSpeedButton;
    QrPediVda: TMySQLQuery;
    QrPediVdaCodigo: TIntegerField;
    QrPediVdaCodUsu: TIntegerField;
    QrPediVdaCliente: TIntegerField;
    QrPediVdaDtaEntra: TDateField;
    QrPediVdaDtaPrevi: TDateField;
    QrPediVdaObserva: TWideMemoField;
    QrPediVdaQuantP: TFloatField;
    QrPediVdaValLiq: TFloatField;
    QrPediVdaReferenPedi: TWideStringField;
    QrPediVdaNO_ENT: TWideStringField;
    DsPediVda: TDataSource;
    DBMemo1: TDBMemo;
    QrPediVdaGru: TMySQLQuery;
    QrPediVdaGruCodUsu: TIntegerField;
    QrPediVdaGruNome: TWideStringField;
    QrPediVdaGruNivel1: TIntegerField;
    QrPediVdaGruGRATAMCAD: TIntegerField;
    QrPediVdaGruQuantP: TFloatField;
    QrPediVdaGruValLiq: TFloatField;
    QrPediVdaGruItensCustomizados: TFloatField;
    QrPediVdaGruFracio: TSmallintField;
    QrPediVdaGruControle: TIntegerField;
    QrPediVdaGruValBru: TFloatField;
    QrPediVdaGruPrecoF: TFloatField;
    DsPediVdaGru: TDataSource;
    DBGGru: TDBGrid;
    QrPediVdaGruSaldoQtd: TFloatField;
    QrPediVdaIts: TMySQLQuery;
    QrPediVdaItsCodigo: TIntegerField;
    QrPediVdaItsControle: TIntegerField;
    QrPediVdaItsGraGruX: TIntegerField;
    QrPediVdaItsPrecoO: TFloatField;
    QrPediVdaItsPrecoR: TFloatField;
    QrPediVdaItsQuantP: TFloatField;
    QrPediVdaItsQuantC: TFloatField;
    QrPediVdaItsQuantV: TFloatField;
    QrPediVdaItsValBru: TFloatField;
    QrPediVdaItsDescoP: TFloatField;
    QrPediVdaItsDescoV: TFloatField;
    QrPediVdaItsValLiq: TFloatField;
    QrPediVdaItsPrecoF: TFloatField;
    QrPediVdaItsMedidaC: TFloatField;
    QrPediVdaItsMedidaL: TFloatField;
    QrPediVdaItsMedidaA: TFloatField;
    QrPediVdaItsMedidaE: TFloatField;
    QrPediVdaItsPercCustom: TFloatField;
    QrPediVdaItsCustomizad: TSmallintField;
    QrPediVdaItsInfAdCuztm: TIntegerField;
    QrPediVdaItsOrdem: TIntegerField;
    QrPediVdaItsReferencia: TWideStringField;
    QrPediVdaItsMulti: TIntegerField;
    QrPediVdaItsvProd: TFloatField;
    QrPediVdaItsvFrete: TFloatField;
    QrPediVdaItsvSeg: TFloatField;
    QrPediVdaItsvOutro: TFloatField;
    QrPediVdaItsvDesc: TFloatField;
    QrPediVdaItsvBC: TFloatField;
    BtAtzSdos: TBitBtn;
    PB1: TProgressBar;
    Panel6: TPanel;
    Panel7: TPanel;
    CkTipoNF: TdmkCheckBox;
    PnNFs: TPanel;
    LarefNFe: TLabel;
    LaModNF: TLabel;
    LaSerie: TLabel;
    LaNF: TLabel;
    LaNFe_CC: TLabel;
    LamodCC: TLabel;
    LaSerCC: TLabel;
    LaNF_CC: TLabel;
    SbNF_VP: TSpeedButton;
    Label4: TLabel;
    Label9: TLabel;
    SbNF_CC: TSpeedButton;
    EdrefNFe: TdmkEdit;
    EdModNF: TdmkEdit;
    EdSerie: TdmkEdit;
    EdNF: TdmkEdit;
    EdNFe_CC: TdmkEdit;
    EdmodCC: TdmkEdit;
    EdSerCC: TdmkEdit;
    EdNF_CC: TdmkEdit;
    EdNFVP_StaLnk: TdmkEdit;
    EdNFVP_FatID: TdmkEdit;
    EdNFVP_FatNum: TdmkEdit;
    EdNFCC_StaLnk: TdmkEdit;
    EdNFCC_FatID: TdmkEdit;
    EdNFCC_FatNum: TdmkEdit;
    QrFornecedores: TMySQLQuery;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOME: TWideStringField;
    QrFornecedoresTipo: TSmallintField;
    QrFornecedoresCNPJ: TWideStringField;
    QrFornecedoresCPF: TWideStringField;
    DsFornecedores: TDataSource;
    QrCI: TMySQLQuery;
    QrCITipo: TSmallintField;
    QrCICNPJ: TWideStringField;
    QrCICPF: TWideStringField;
    QrCICodigo: TIntegerField;
    QrCINOME: TWideStringField;
    DsCI: TDataSource;
    Label6: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    LaCI: TLabel;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    Qr00_NFeCabA: TMySQLQuery;
    Qr00_NFeCabAFatID: TIntegerField;
    Qr00_NFeCabAFatNum: TIntegerField;
    Qr00_NFeCabAEmpresa: TIntegerField;
    Qr00_NFeCabAIDCtrl: TIntegerField;
    Qr00_NFeCabAAtrelaFatID: TIntegerField;
    Qr00_NFeCabAAtrelaFatNum: TIntegerField;
    Qr00_NFeCabAAtrelaStaLnk: TSmallintField;
    Qr00_NFeCabAide_mod: TSmallintField;
    Qr00_NFeCabAide_serie: TIntegerField;
    Qr00_NFeCabAide_nNF: TIntegerField;
    Qr00_NFeCabAId: TWideStringField;
    LaEmpresa: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrItsI: TMySQLQuery;
    QrItsIFatID: TIntegerField;
    QrItsIFatNum: TIntegerField;
    QrItsIEmpresa: TIntegerField;
    QrItsInItem: TIntegerField;
    QrItsIprod_cProd: TWideStringField;
    QrItsIprod_cEAN: TWideStringField;
    QrItsIprod_cBarra: TWideStringField;
    QrItsIprod_xProd: TWideStringField;
    QrItsIprod_NCM: TWideStringField;
    QrItsIprod_CEST: TIntegerField;
    QrItsIprod_indEscala: TWideStringField;
    QrItsIprod_CNPJFab: TWideStringField;
    QrItsIprod_cBenef: TWideStringField;
    QrItsIprod_EXTIPI: TWideStringField;
    QrItsIprod_genero: TSmallintField;
    QrItsIprod_CFOP: TIntegerField;
    QrItsIprod_uCom: TWideStringField;
    QrItsIprod_qCom: TFloatField;
    QrItsIprod_vUnCom: TFloatField;
    QrItsIprod_vProd: TFloatField;
    QrItsIprod_cEANTrib: TWideStringField;
    QrItsIprod_cBarraTrib: TWideStringField;
    QrItsIprod_uTrib: TWideStringField;
    QrItsIprod_qTrib: TFloatField;
    QrItsIprod_vUnTrib: TFloatField;
    QrItsIprod_vFrete: TFloatField;
    QrItsIprod_vSeg: TFloatField;
    QrItsIprod_vDesc: TFloatField;
    QrItsIprod_vOutro: TFloatField;
    QrItsIprod_indTot: TSmallintField;
    QrItsIprod_xPed: TWideStringField;
    QrItsIprod_nItemPed: TIntegerField;
    QrItsITem_IPI: TSmallintField;
    QrItsI_Ativo_: TSmallintField;
    QrItsIInfAdCuztm: TIntegerField;
    QrItsIEhServico: TIntegerField;
    QrItsIUsaSubsTrib: TSmallintField;
    QrItsIICMSRec_pRedBC: TFloatField;
    QrItsIICMSRec_vBC: TFloatField;
    QrItsIICMSRec_pAliq: TFloatField;
    QrItsIICMSRec_vICMS: TFloatField;
    QrItsIIPIRec_pRedBC: TFloatField;
    QrItsIIPIRec_vBC: TFloatField;
    QrItsIIPIRec_pAliq: TFloatField;
    QrItsIIPIRec_vIPI: TFloatField;
    QrItsIPISRec_pRedBC: TFloatField;
    QrItsIPISRec_vBC: TFloatField;
    QrItsIPISRec_pAliq: TFloatField;
    QrItsIPISRec_vPIS: TFloatField;
    QrItsICOFINSRec_pRedBC: TFloatField;
    QrItsICOFINSRec_vBC: TFloatField;
    QrItsICOFINSRec_pAliq: TFloatField;
    QrItsICOFINSRec_vCOFINS: TFloatField;
    QrItsIMeuID: TIntegerField;
    QrItsINivel1: TIntegerField;
    QrItsIGraGruX: TIntegerField;
    QrItsIUnidMedCom: TIntegerField;
    QrItsIUnidMedTrib: TIntegerField;
    QrItsIICMSRec_vBCST: TFloatField;
    QrItsIICMSRec_vICMSST: TFloatField;
    QrItsIICMSRec_pAliqST: TFloatField;
    QrItsITem_II: TSmallintField;
    QrItsIprod_nFCI: TWideStringField;
    QrItsIStqMovValA: TIntegerField;
    QrItsIAtivo: TSmallintField;
    QrItsIAtrelaID: TIntegerField;
    QrItsS: TMySQLQuery;
    QrItsV: TMySQLQuery;
    QrItsVnItem: TIntegerField;
    QrItsVInfAdProd: TWideMemoField;
    QrItsN: TMySQLQuery;
    QrItsNICMS_CST: TSmallintField;
    QrItsNICMS_Orig: TSmallintField;
    QrItsO: TMySQLQuery;
    QrItsOIPI_CST: TSmallintField;
    QrItsOIPI_cEnq: TWideStringField;
    QrItsQ: TMySQLQuery;
    QrItsQPIS_CST: TSmallintField;
    QrItsSCOFINS_CST: TSmallintField;
    TabSheet3: TTabSheet;
    QrPedXNFe: TMySQLQuery;
    QrPedXNFeDiferencaItm: TFloatField;
    QrPedXNFeQuantP: TFloatField;
    QrPedXNFevProd: TFloatField;
    QrPedXNFevFrete: TFloatField;
    QrPedXNFevSeg: TFloatField;
    QrPedXNFevDesc: TFloatField;
    QrPedXNFevOutro: TFloatField;
    QrPedXNFevTotItm: TFloatField;
    QrPedXNFeValLiq: TFloatField;
    QrPedXNFeValBru: TFloatField;
    QrPedXNFeGraGruX: TIntegerField;
    QrPedXNFeGraGru1: TIntegerField;
    QrPedXNFeNome: TWideStringField;
    QrPedXNFePrecoF: TFloatField;
    QrPedXNFeSaldoQtd: TFloatField;
    QrPedXNFeprod_xProd: TWideStringField;
    QrPedXNFeprod_qCom: TFloatField;
    QrPedXNFeprod_vProd: TFloatField;
    QrPedXNFeprod_vFrete: TFloatField;
    QrPedXNFeprod_vSeg: TFloatField;
    QrPedXNFeprod_vDesc: TFloatField;
    QrPedXNFeprod_vOutro: TFloatField;
    QrPedXNFeprod_vTotItm: TFloatField;
    DsPedXNFe: TDataSource;
    DBGPedXNFe: TDBGrid;
    QrPedXNFeprod_cProd: TWideStringField;
    QrPedXNFePrecoP: TFloatField;
    QrPediVdaRegrFiscal: TIntegerField;
    QrPediVdaCentroCusto: TIntegerField;
    Qr00_NFeCabAemit_CRT: TSmallintField;
    QrCabA: TMySQLQuery;
    QrCabAemit_CRT: TSmallintField;
    QrItsNICMS_CSOSN: TIntegerField;
    QrPediVdaMadeBy: TSmallintField;
    QrPediVdaCondicaoPG: TIntegerField;
    QrCabAide_dEmi: TDateField;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBEncerradosClick(Sender: TObject);
    procedure CBFornecedorClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure QrPedidosAfterScroll(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbPesqCodClick(Sender: TObject);
    procedure SbPesqRefClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure QrPediVdaGruAfterOpen(DataSet: TDataSet);
    procedure QrPediVdaGruBeforeClose(DataSet: TDataSet);
    procedure BtAtzSdosClick(Sender: TObject);
    procedure SbNF_VPClick(Sender: TObject);
    procedure SbNF_CCClick(Sender: TObject);
    procedure EdrefNFeRedefinido(Sender: TObject);
    procedure EdrefNFeExit(Sender: TObject);
    procedure EdNFe_CCExit(Sender: TObject);
    procedure EdNFe_CCRedefinido(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdNF_CCChange(Sender: TObject);
    procedure EdNFChange(Sender: TObject);
  private
    { Private declarations }
    FConfirma: Boolean;
    //FFatID, FFatNum: Integer;

    procedure ReopenPedidos(CodUsu: Integer; Referencia: String);
    procedure ReopenPQEItsDePediVda(Codigo: Integer);
    procedure DefineDFe_Atrela(const TipoNFeEntrada: TTipoNFeEntrada);
    function  VerificaDadosCabecalho(var FatID, FatNum, Empresa,
              CodInfoEmit,
              IQ, CI: Integer;
              var refNFe: String;
              var ModNF, Serie, NF: Integer;

              var NFe_CC: String;
              var modCC,SerCC, NF_CC,
              //
              NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
              NFCC_FatID, NFCC_FatNum, NFCC_StaLnk: Integer): Boolean;
    function  VerificaChaveNFeDeEntrada(Empresa, CliInt: Integer; ChaveEmpresa,
              ChaveCliInt: String): Boolean;
    function  CadastroItensOK(const FatID, FatNum, Empresa, CodInfoEmit: Integer): Boolean;
    function  TodosItensFaturadosCorretamente(FatID, FatNum, Empresa: Integer):
              Boolean;
    procedure ReopenCabA(FatID, FatNum, Empresa: Integer);
    procedure ReopenItsI(FatID, FatNum, Empresa: Integer);
    procedure ReopenItsN(FatID, FatNum, Empresa: Integer);


  public
    { Public declarations }
  end;

var
  FmPQEPed3: TFmPQEPed3;

implementation

uses UnMyObjects, PQE, Module, ModPediVda,
  ModuleNFe_0000, ModuleGeral, UnSPED_PF,
//PQx, NFe_PF,  ModProd,
  UMySQLModule, NFeXMLGerencia, UnGrade_Jan, MyDBCheck;


{$R *.DFM}

const
  FEFDInnNFSMainFatID = VAR_FATID_0010;
  CO_MovimCod_ZERO    = 0;


procedure TFmPQEPed3.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQEPed3.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FConfirma := False;
  //
  QrFornece.Params[0].AsInteger := 2;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornecedores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPQEPed3.DefineDFe_Atrela(const TipoNFeEntrada: TTipoNFeEntrada);
const
  sProcName = 'TFmPQEPed3.DefineDFe_Atrela()';
var
  Empresa, CI, IQ, ide_mod, ide_Serie, ide_nNF, CodInfoEmit,
  NFe_FatID, NFe_FatNum, NFe_StaLnk: Integer;
  SQL_CNPJ_CPF_Emit, SQL_CNPJ_CPF_Dest: String;
  Continua: Boolean;
  Ed_modCC: TdmkEdit;
begin
  Ed_modCC := nil;
  Continua := True;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  IQ := EdFornecedor.ValueVariant;
  CI := EdCI.ValueVariant;
  case TipoNFeEntrada of
    TTipoNFeEntrada.tneVP:
      if EMpresa <> CI then
        Continua := Geral.MB_Pergunta(
        'Para esta pesquisa o cliente interno deve ser o mesmo que a empresa!'
        + sLineBreak + 'Mas deseja pesquisar assim mesmo?') = ID_YES;
    TTipoNFeEntrada.tneCC:
      if Empresa = CI then
        Continua := Geral.MB_Pergunta(
        'Para esta pesquisa o cliente interno deve ser diferente da emprea!'
        + sLineBreak + 'Mas deseja pesquisar assim mesmo?') = ID_YES;
    TTipoNFeEntrada.tneRP:
      if Empresa <> CI then
        Continua := Geral.MB_Pergunta(
        'Para esta pesquisa o cliente interno deve ser diferente da emprea!'
        + sLineBreak + 'Mas deseja pesquisar assim mesmo?') = ID_YES;
  end;
  //
  if MyObjects.FIC(IQ = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  if MyObjects.FIC(CI = 0, EdCI, 'Informe o cliente interno') then Exit;
  if Continua then
  begin
  case TipoNFeEntrada of
    TTipoNFeEntrada.tneVP:
    begin
      CodInfoEmit := QrFornecedoresCodigo.Value;
      case QrFornecedoresTipo.Value of
        0: SQL_CNPJ_CPF_Emit := 'AND emit_CNPJ="' + Geral.SoNumero_TT(QrFornecedoresCNPJ.Value) + '"';
        1: SQL_CNPJ_CPF_Emit := 'AND emit_CPF="' + Geral.SoNumero_TT(QrFornecedoresCPF.Value) + '"';
        else SQL_CNPJ_CPF_Emit := 'AND emit_CPF_CPF=????';
      end;
      //
      case QrCITipo.Value of
        0: SQL_CNPJ_CPF_Dest := 'AND dest_CNPJ="' + Geral.SoNumero_TT(QrCICNPJ.Value) + '"';
        1: SQL_CNPJ_CPF_Dest := 'AND dest_CPF="' + Geral.SoNumero_TT(QrCICPF.Value) + '"';
        else SQL_CNPJ_CPF_Dest := 'AND dest_CPF_CPF=????';
      end;
    end;
    TTipoNFeEntrada.tneCC:
    begin
      CodInfoEmit := QrFornecedoresCodigo.Value;
      case QrFornecedoresTipo.Value of
        0: SQL_CNPJ_CPF_Emit := 'AND emit_CNPJ="' + Geral.SoNumero_TT(QrFornecedoresCNPJ.Value) + '"';
        1: SQL_CNPJ_CPF_Emit := 'AND emit_CPF="' + Geral.SoNumero_TT(QrFornecedoresCPF.Value) + '"';
        else SQL_CNPJ_CPF_Emit := 'AND emit_CPF_CPF=????';
      end;
      //
      case DModG.QrEmpresasTipo.Value of
        0: SQL_CNPJ_CPF_Dest := 'AND dest_CNPJ="' + Geral.SoNumero_TT(DModG.QrEmpresasCNPJ_CPF.Value) + '"';
        1: SQL_CNPJ_CPF_Dest := 'AND dest_CPF="' + Geral.SoNumero_TT(DModG.QrEmpresasCNPJ_CPF.Value) + '"';
        else SQL_CNPJ_CPF_Dest := 'AND dest_CPF_CPF=????';
      end;
    end;
    else
    begin
      Geral.MB_Aviso('Tipo de entrada n�o implementada em ' + sProcName);
    end;
    //
  end;
    //
    case TipoNFeEntrada of
      //tneND: //=0,
      tneVP:
      begin
        // NF de venda
        ide_mod   := EdmodNF.ValueVariant;
        ide_Serie := EdSerie.ValueVariant;
        ide_nNF   := EdNF.ValueVariant;
        Ed_modCC  := EdmodNF;
      end;
       //=1,
      (*tneRP: //=2,
      begin
        // NF de remessa produ��o
        ide_mod   := EdmodRP.ValueVariant;
        ide_Serie := EdSerRP.ValueVariant;
        ide_nNF   := EdNF_RP.ValueVariant;
        Ed_modCC  := EdModRP;
      end;*)
      tneCC: //=3)
      begin
        // NF de remessa cobertura cliente
        ide_mod   := EdmodCC.ValueVariant;
        ide_Serie := EdSerCC.ValueVariant;
        ide_nNF   := EdNF_CC.ValueVariant;
        Ed_modCC  := EdModCC;
      end;
    end;
    //
    if MyObjects.FIC(ide_mod = 0, Ed_modCC,
      'Informe o modelo do documento fiscal!') then Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr00_NFeCabA, Dmod.MyDB, [
    'SELECT FatID, FatNum, Empresa, IDCtrl,',
    'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk,',
    'ide_mod, ide_serie, ide_nNF, Id, emit_CRT ',
    'FROM nfecaba ',
    'WHERE FatID=' + Geral.FF0(VAR_FATID_0053),
    'AND Empresa=' + Geral.FF0(Empresa),
    SQL_CNPJ_CPF_Emit,
    SQL_CNPJ_CPF_Dest,
    'AND ide_mod IN (0,' + Geral.FF0(ide_mod) + ')',
    'AND ide_serie >= 0 ', // + Geral.FF0(ide_Serie),
    'AND ide_nNF=' + Geral.FF0(ide_nNF),
    'ORDER BY ide_mod DESC ',
    '']);
    //Geral.MB_Teste(Qr00_NFeCabA.SQL.Text);
    if Qr00_NFeCabA.RecordCount > 0 then
    begin
      if not Qr00_NFeCabA.Locate('ide_mod;ide_serie', VarArrayOf([ide_mod, ide_serie]), []) then
      begin
        if not Qr00_NFeCabA.Locate('ide_serie', ide_serie, []) then
        begin
          if not Qr00_NFeCabA.Locate('ide_mod', ide_mod, []) then
          begin

          end;
        end;
      end;
      //Geral.MB_Teste(Qr00_NFeCabA.SQL.Text);
      //
      if Qr00_NFeCabA.RecordCount > 0 then
      begin
        if ide_Serie <> Qr00_NFeCabAide_serie.Value then
        begin
          case TipoNFeEntrada of
            tneVP: EdSerie.ValueVariant := Qr00_NFeCabAide_serie.Value;
            //tneRP: EdSerRP.ValueVariant := Qr00_NFeCabAide_serie.Value;
            tneCC: EdSerCC.ValueVariant := Qr00_NFeCabAide_serie.Value;
          end;
        end;
        //
        case TipoNFeEntrada of
          tneVP:
          begin
            EdrefNFe.ValueVariant       := Qr00_NFeCabAId.Value;
            EdNFVP_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
            EdNFVP_FatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
            EdNFVP_FatNum.ValueVariant  := Qr00_NFeCabAFatNum.Value;
          end;
          (*tneRP:
          begin
            EdNFe_RP.ValueVariant       := Qr00_NFeCabAId.Value;
            EdNFRP_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
            EdNFRP_FatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
            EdNFRP_FatNum.ValueVariant  := Qr00_NFeCabAFatNum.Value;
          end;*)
          tneCC:
          begin
            EdNFe_CC.ValueVariant       := Qr00_NFeCabAId.Value;
            EdNFCC_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
            EdNFCC_FatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
            EdNFCC_FatNum.ValueVariant  := Qr00_NFeCabAFatNum.Value;
          end;
        end;
      end;
      // N�o sabe o PQE.Codigo ainda!!
      //MovFatID  := FEFDInnNFSMainFatID;
      //MovFatNum := Codigo;
      NFe_FatID  := Qr00_NFeCabAFatID.Value;
      NFe_FatNum := Qr00_NFeCabAFatNum.Value;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
      'CodInfoEmit'
      //'AtrelaFatID', 'AtrelaFatNum', 'AtrelaStaLnk'
      ], [
      'FatID', 'FatNum', 'Empresa'], [
      CodInfoEmit
      //MovFatID, MovFatNum, NFe_StaLnk
      ], [
      NFe_FatID, NFe_FatNum, Empresa], False);
    end else
      Geral.MB_Info('N�o foi poss�vel encontrar a NFe!');
  end;
end;

procedure TFmPQEPed3.CBEncerradosClick(Sender: TObject);
begin
{
  QrFornece.Close;
  QrPedidos.Close;
  if CBEncerrados.Checked then QrFornece.Params[0].AsInteger := 3
  else QrFornece.Params[0].AsInteger := 2;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
}
end;

procedure TFmPQEPed3.CBFornecedorClick(Sender: TObject);
begin
{
  QrPedidos.Close;
  if CBFornecedor.KeyValue = NULL then CBFornecedor.KeyValue := 0;
  QrPedidos.Params[0].AsInteger := CBFornecedor.KeyValue;
  if CBEncerrados.Checked then QrPedidos.Params[1].AsInteger := 3
  else QrPedidos.Params[1].AsInteger := 2;
  UnDmkDAC_PF.AbreQuery(QrPedidos, Dmod.MyDB);
}
end;

procedure TFmPQEPed3.DBGrid1DblClick(Sender: TObject);
var
  FatID, FatNum, Empresa,
  CodInfoEmit,
  IQ, CI: Integer;
  var refNFe: String;
  var ModNF, Serie, NF: Integer;

  var NFe_CC: String;
  var modCC,SerCC, NF_CC,
  //
  NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
  NFCC_FatID, NFCC_FatNum, NFCC_StaLnk: Integer;
begin
  if (QrPediVda.State <> dsInactive) and (QrPediVda.RecordCount > 0) then
  begin
    FConfirma := True;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPediVdaGru, Dmod.MyDB, [
    'SELECT pvi.Controle, SUM(QuantP) QuantP, ',
    'SUM(ValLiq) ValLiq, SUM(ValBru) ValBru, ',
    'SUM(Customizad) ItensCustomizados, ',
    'gti.Codigo GRATAMCAD, ',
    'gg1.CodUsu, gg1.Nome, gg1.Nivel1, pgt.Fracio, ',
    'IF(QuantP <= 0, 0.00, vProd / QuantP) PrecoF, ',
    'SUM(QuantP - QuantV) SaldoQtd ',
    'FROM pedivdaits pvi ',
    'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX ',
    'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN prdgruptip   pgt ON pgt.Codigo=gg1.PrdGrupTip ',
    'WHERE pvi.Codigo=' + Geral.FF0(QrPediVdaCodigo.Value),
    'GROUP BY gg1.Nivel1 ',
    'ORDER BY pvi.Controle ',
    '']);
    //
    if QrPediVdaGru.RecordCount > 0 then
    begin
      //PnCfgPesq.Enabled := False; desmarcar!
      PageControl1.ActivePageIndex := 1;
      //
      //if VerificaDadosCabecalho(FatID, FatNum, Empresa, CodInfoEmit) then
      if VerificaDadosCabecalho(FatID, FatNum, Empresa,
  CodInfoEmit,
  IQ, CI,
  refNFe,
  ModNF, Serie, NF,
  NFe_CC,
  modCC,SerCC, NF_CC,
  //
  NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
  NFCC_FatID, NFCC_FatNum, NFCC_StaLnk) then
        if CadastroItensOK(FatID, FatNum, Empresa, CodInfoEmit) then
          if TodosItensFaturadosCorretamente(FatID, FatNum, Empresa) then
            ;
    end;
  end;
end;

procedure TFmPQEPed3.EdNFChange(Sender: TObject);
begin
  if (EdNF.ValueVariant > 0) and (EdModNF.ValueVariant = 0) then
    EdModNF.ValueVariant := 55;
end;

procedure TFmPQEPed3.EdNFe_CCExit(Sender: TObject);
begin
  if not DmNFe_0000.VerificaChavedeAcesso(EdNFe_CC.Text, True) then
    EdNFe_CC.SetFocus;
end;

procedure TFmPQEPed3.EdNFe_CCRedefinido(Sender: TObject);
var
  NFe_CC: String;
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
begin
  NFe_CC := EdNFe_CC.ValueVariant;
  if Length(Trim(NFe_CC)) >= 44 then
  begin
    //UnNFe_PF.ObtemSerieNumeroByID(refNFe, Serie, NF);
    NFeXMLGeren.DesmontaChaveDeAcesso(NFe_CC, 2.00,
    UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
    EdModCC.ValueVariant := ModNFe;
    EdSerCC.ValueVariant := SerNFe;
    EdNF_CC.ValueVariant := NumNFe;
  end;
end;

procedure TFmPQEPed3.EdNF_CCChange(Sender: TObject);
begin
  if (EdNF_CC.ValueVariant > 0) and (EdModCC.ValueVariant = 0) then
    EdModCC.ValueVariant.ValueVariant := 55;
end;

procedure TFmPQEPed3.EdrefNFeExit(Sender: TObject);
begin
  if not DmNFe_0000.VerificaChavedeAcesso(EdrefNFe.Text, True) then
    EdrefNFe.SetFocus;
end;

procedure TFmPQEPed3.EdrefNFeRedefinido(Sender: TObject);
var
  refNFe: String;
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
begin
  refNFe := EdrefNFe.ValueVariant;
  if Length(Trim(refNFe)) >= 44 then
  begin
    //UnNFe_PF.ObtemSerieNumeroByID(refNFe, Serie, NF);
    NFeXMLGeren.DesmontaChaveDeAcesso(refNFe, 2.00,
    UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
    EdModNF.ValueVariant := ModNFe;
    EdSerie.ValueVariant := SerNFe;
    EdNF.ValueVariant := NumNFe;
  end;
end;

procedure TFmPQEPed3.BtAtzSdosClick(Sender: TObject);
var
  Pedido: Integer;
begin
  Pedido := QrPediVdaCodigo.Value;
  DmPediVda.AtualizaTodosItensPediVda_(Pedido, PB1);
  ReopenPQEItsDePediVda(Pedido);
  if QrPediVdaGru.State <> dsInactive then
  begin
    QrPediVdaGru.Close;
    UnDmkDAC_PF.AbreQuery(QrPediVdaGru, Dmod.MyDB);
  end;
end;

procedure TFmPQEPed3.BtConfirmaClick(Sender: TObject);
var
  Pedido: Integer;
  var FatID, FatNum, Empresa,
  CodInfoEmit,
  IQ, CI, RegrFiscal, CondicaoPG: Integer;
  var refNFe: String;
  var ModNF, Serie, NF: Integer;

  var NFe_CC: String;
  var modCC,SerCC, NF_CC,
  //
  NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
  NFCC_FatID, NFCC_FatNum, NFCC_StaLnk: Integer;
begin
  //if VerificaDadosCabecalho(FatID, FatNum, Empresa, CodInfoEmit) = False then
  if VerificaDadosCabecalho(FatID, FatNum, Empresa,
  CodInfoEmit,
  IQ, CI,
  refNFe,
  ModNF, Serie, NF,

  NFe_CC,
  modCC,SerCC, NF_CC,
  //
  NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
  NFCC_FatID, NFCC_FatNum, NFCC_StaLnk) = False then
    Exit;
  //////////////////////////////////////////////////////////////////////////////
  if not CadastroItensOK(FatID, FatNum, Empresa, CodInfoEmit) then Exit;
  //////////////////////////////////////////////////////////////////////////////
  ///
  //ver aqui se totais por produto batem entre pedido e NFe
  if not TodosItensFaturadosCorretamente(FatID, FatNum, Empresa) then
    if not DBCheck.LiberaPelaSenhaBoss() then
     Exit;
  ///
  ///
  ///
  if (QrPediVda.State <> dsInactive) and (QrPediVda.RecordCount > 0) and
  (QrPediVdaGru.State <> dsInactive) and (QrPediVdaGru.RecordCount > 0) then
    Pedido := QrPediVdaCodigo.Value
  else
    Pedido := 0;
  //
  if Pedido = 0 then
  begin
    Geral.MB_Aviso('Defina o Pedido!');
    PageControl1.ActivePageIndex := 0;
    Exit;
  end else
  begin
    ReopenPQEItsDePediVda(Pedido);
    if QrPediVdaIts.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Este pedido n�o tem mais nenhum item com saldo!');
      Exit;
    end;
    Hide;
    //
(*
    FmPQE.QrPedXNFe.Database := QrPedXNFe.Database;
    FmPQE.QrPedXNFe.SQL.Text := QrPedXNFe.SQL.Text;
    FmPQE.QrPedXNFe.Open;
*)
    //
    RegrFiscal := QrPediVdaRegrFiscal.Value;
    CondicaoPG := QrPediVdaCondicaoPG.Value;
    //
    FmPQE.InsereRegCondicionado3(Pedido,
      FatID, FatNum, Empresa,
      CodInfoEmit,
      IQ, CI, RegrFiscal, CondicaoPG,
      refNFe,
      ModNF, Serie, NF,

      NFe_CC,
      modCC,SerCC, NF_CC,
      //
      NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
      NFCC_FatID, NFCC_FatNum, NFCC_StaLnk,
      QrCabAide_dEmi.Value);
    //
    Close;
  end;
end;

procedure TFmPQEPed3.QrPedidosAfterScroll(DataSet: TDataSet);
begin
{
  QrPedidosIts.Close;
  if CBPedido.KeyValue <> NULL then
  begin
    QrPedidosIts.Params[0].AsInteger := QrPedidosCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrPedidosIts, Dmod.MyDB);
  end;
}
end;

procedure TFmPQEPed3.QrPediVdaGruAfterOpen(DataSet: TDataSet);
begin
  BtConfirma.Enabled := QrPediVdaGru.RecordCount > 0;
  BtAtzSdos.Enabled  := QrPediVdaGru.RecordCount > 0;
end;

procedure TFmPQEPed3.QrPediVdaGruBeforeClose(DataSet: TDataSet);
begin
  BtConfirma.Enabled := False;
  BtAtzSdos.Enabled  := False;
end;

procedure TFmPQEPed3.ReopenCabA(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCabA, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfecaba ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TFmPQEPed3.ReopenItsI(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsI, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitsi ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'ORDER BY nItem ',
  '']);
end;

procedure TFmPQEPed3.ReopenItsN(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsV, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitsv',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsN, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitsn',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsO, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitso',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsQ, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitsq',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsS, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitss',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  ' ']);
end;

procedure TFmPQEPed3.ReopenPedidos(CodUsu: Integer; Referencia: String);
var
  SQL_Pesq: String;
begin
  if CodUsu <> 0 then
    SQL_Pesq := 'AND pvd.CodUsu=' + Geral.FF0(CodUsu)
  else
    SQL_Pesq := 'AND ReferenPedi LIKE "' + Referencia + '" ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediVda, Dmod.MyDB, [
  'SELECT pvd.Codigo, pvd.CodUsu, pvd.Cliente,  ',
  'pvd.DtaEntra, pvd.DtaPrevi, pvd.Observa,  ',
  'pvd.QuantP, pvd.ValLiq, pvd.ReferenPedi, ',
  'pvd.RegrFiscal, pvd.CondicaoPG, ',
  'pvd.CentroCusto, pvd.MadeBy, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT  ',
  'FROM pedivda pvd ',
  'LEFT JOIN entidades ent ON ent.Codigo=pvd.Cliente ',
  'WHERE pvd.EntSai=0 ',
  SQL_Pesq,
  '']);
end;

procedure TFmPQEPed3.ReopenPQEItsDePediVda(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediVdaIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM pedivdaits',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND QuantP > QuantV ',
  'ORDER BY Controle ',
  '']);
end;

procedure TFmPQEPed3.SbNF_CCClick(Sender: TObject);
begin
  DefineDFe_Atrela(tneCC);
end;

procedure TFmPQEPed3.SbNF_VPClick(Sender: TObject);
begin
  DefineDFe_Atrela(tneVP);
end;

procedure TFmPQEPed3.SbPesqCodClick(Sender: TObject);
begin
  ReopenPedidos(EdCodUsu.ValueVariant, '');
end;

procedure TFmPQEPed3.SbPesqRefClick(Sender: TObject);
begin
  ReopenPedidos(0, EdReferencia.ValueVariant);
end;

function TFmPQEPed3.TodosItensFaturadosCorretamente(FatID, FatNum, Empresa:
  Integer): Boolean;
var
  ItensErr: Integer;
  fDifMax: Double;
  sDifMax: String;
begin
  Result := False;
  ItensErr := 0;
  fDifMax := DModG.QrCtrlGeralDifMaxPedNFe.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPedXNFe, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _INN_PED_X_NFe_PED_;',
  'CREATE TABLE _INN_PED_X_NFe_PED_',
  'SELECT SUM(pvi.QuantP) QuantP, ',
  'SUM(vProd) vProd, SUM(vFrete) vFrete, SUM(vSeg) vSeg, ',
  'SUM(vDesc) vDesc, SUM(vOutro) vOutro, ',
  'SUM(vProd + vFrete + vSeg - vDesc + vOutro) vTotItm,',
  'SUM(pvi.ValLiq) ValLiq, SUM(pvi.ValBru) ValBru, ',
  'pvi.GraGruX, ggx.GraGru1, gg1.Nome, ',
  'IF(pvi.QuantP <= 0, 0.00, pvi.vProd / pvi.QuantP) PrecoP, ',
  'SUM(pvi.QuantP - pvi.QuantV) SaldoQtd ',
  'FROM ' + TMeuDB +  '.pedivdaits pvi ',
  'LEFT JOIN ' + TMeuDB +  '.gragrux ggx ON ggx.Controle=pvi.GraGruX ',
  'LEFT JOIN ' + TMeuDB +  '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'WHERE pvi.Codigo=' + Geral.FF0(QrPediVdaCodigo.Value),
  'GROUP BY pvi.GraGruX;',
  '',
  'DROP TABLE IF EXISTS _INN_PED_X_NFe_NFe_;',
  'CREATE TABLE _INN_PED_X_NFe_NFe_',
  'SELECT nfi.GraGruX, nfi.prod_cProd, nfi.prod_xProd,',
  'IF(SUM(prod_qCom) <= 0, 0.00, SUM(prod_vProd) / SUM(prod_qCom)) PrecoF, ',
  'SUM(prod_qCom) prod_qCom, SUM(prod_vProd) prod_vProd,',
  'SUM(prod_vFrete) prod_vFrete, SUM(prod_vSeg) prod_vSeg,',
  'SUM(prod_vDesc) prod_vDesc, SUM(prod_vOutro) prod_vOutro,',
  'SUM(prod_vProd + prod_vFrete + prod_vSeg - prod_vDesc + ',
  'prod_vOutro) prod_vTotItm',
  '',
  ' ',
  'FROM ' + TMeuDB +  '.nfeitsi nfi',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'GROUP BY nfi.GraGruX',
  ';',
  '',
  'SELECT nfi.prod_vTotItm - ped.vTotItm DiferencaItm, ped.*, nfi.* ',
  'FROM _INN_PED_X_NFe_PED_ ped',
  'LEFT JOIN _INN_PED_X_NFe_NFe_ nfi ON ped.GraGrux=nfi.GraGruX',
  ';',
  '']);
  QrPedXNFe.First;
  while not QrPedXNFe.Eof do
  begin
    if (QrPedXNFeDiferencaItm.Value > fDifMax) then
      ItensErr := ItensErr + 1;
    //
    QrPedXNFe.Next;
  end;
  Result := ItensErr = 0;
  if Result = False then
  begin
    sDifMax := Geral.FFT(fDifMax, 2, siNegativo);
    //
    if ItensErr = 1 then
      Geral.MB_Aviso('Existe um item faturado acima do valor do pedido em mais de $ ' + sDifMax)
    else
      Geral.MB_Aviso('Existem ' + Geral.FF0(ItensErr) +
        ' itens faturados acima do valor do pedido em mais de $ ' + sDifMax);
  end;
end;

function TFmPQEPed3.VerificaChaveNFeDeEntrada(Empresa, CliInt: Integer;
  ChaveEmpresa, ChaveCliInt: String): Boolean;
var
  ChaveNFe: String;
begin
  Result := False;
  if (Empresa = CliInt) then
    ChaveNFe := ChaveEmpresa
  else
    ChaveNFe := ChaveCliInt;
  //
  if ChaveNFe = EmptyStr then
  begin
    Geral.MB_Aviso('Chave de acesso de entrada n�o definida corretamente!');
    Exit;
  end else
  begin
    if not DmNFe_0000.VerificaChavedeAcesso(ChaveNFe, False) then
      Exit;
  end;
  //
  Result := True;
end;

function TFmPQEPed3.VerificaDadosCabecalho(var FatID, FatNum, Empresa,
  CodInfoEmit,
  IQ, CI: Integer;
  var refNFe: String;
  var ModNF, Serie, NF: Integer;

  var NFe_CC: String;
  var modCC,SerCC, NF_CC,
  //
  NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
  NFCC_FatID, NFCC_FatNum, NFCC_StaLnk: Integer): Boolean;
var
  PrecisaNF: Boolean;
  UsaEntradaFiscal: Boolean;
begin
  FatID   := 0;
  FatNum  := 0;
  Empresa := 0;
  CodInfoEmit := 0;
  //
  Result := False;
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  IQ             := EdFornecedor.ValueVariant;
  CI             := EdCI.ValueVariant;
  if MyObjects.FIC(CI = 0, EdCI, 'Informe o cliente interno') then Exit;
  if MyObjects.FIC(CBFornecedor.KeyValue = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  //
  UsaEntradaFiscal := DModG.QrCtrlGeralUsarEntraFiscal.Value = 1;

  NFVP_FatID     := EdNFVP_FatID.ValueVariant;
  NFVP_StaLnk    := EdNFVP_StaLnk.ValueVariant;
  NFVP_FatNum    := EdNFVP_FatNum.ValueVariant;

  NFCC_FatID     := EdNFCC_FatID.ValueVariant;
  NFCC_FatNum    := EdNFCC_FatNum.ValueVariant;
  NFCC_StaLnk    := EdNFCC_StaLnk.ValueVariant;
  //
  // NF de venda
  refNFe := EdrefNFe.Text;
  modNF  := EdmodNF.ValueVariant;
  Serie  := EdSerie.ValueVariant;
  NF     := EdNF.ValueVariant;
  PrecisaNF := (CI = Empresa) and (
    (refNFe = '') or ( (NF =  0) and (modNF = 0)));
  if MyObjects.FIC(UsaEntradaFiscal and PrecisaNF, EdrefNFe, 'Informe a chave da NF VP!') then Exit;
  if MyObjects.FIC(PrecisaNF and (NFVP_FatNum = 0), EdrefNFe, '� necess�rio o atrelamento com o XML (VP)!') then Exit;
  if CI = Empresa then
  begin
    FatID  := EdNFVP_FatID.ValueVariant;
    FatNum := NFVP_FatNum;
    CodInfoEmit := EdFornecedor.ValueVariant;
  end;
  //

  (*// NF de Remessa para Produ��o (Industrializa��o)
  NFe_RP    := EdNFe_RP.ValueVariant;
  modRP     := EdmodRP.ValueVariant;
  SerRP     := EdSerRP.ValueVariant;
  NF_RP     := EdNF_RP.ValueVariant;
  PrecisaNF := (CI <> Empresa) and (
    (NFe_RP = '') or ( (NF_RP = 0) and (modRP = 0)));
  if MyObjects.FIC(UsaEntradaFiscal and PrecisaNF, EdNFe_RP, 'Informe a chave da NF RP!') then; // Exit;
  //*)

  // NF de Cobertura do Cliente
  NFe_CC    := EdNFe_CC.ValueVariant;
  modCC     := EdmodCC.ValueVariant;
  SerCC     := EdSerCC.ValueVariant;
  NF_CC     := EdNF_CC.ValueVariant;
  PrecisaNF := (CI <> Empresa) and (
    (NFe_CC = '') or ( (NF_CC = 0) and (modCC = 0)));
  if MyObjects.FIC(UsaEntradaFiscal and PrecisaNF, EdNFe_CC, 'Informe a chave da NF CC!') then Exit;
  if MyObjects.FIC(PrecisaNF and (NFCC_FatNum = 0), EdNFe_CC, '� necess�rio o atrelamento com o XML (CC)!') then Exit;
  if CI <> Empresa then
  begin
    FatID  := EdNFCC_FatID.ValueVariant;
    FatNum := NFCC_FatNum;
    CodInfoEmit := CI;
  end;

  //

  (*// CTe - Conhecimento de Frete
  Cte_Id    := EdCte_Id.ValueVariant;
  CTe_mod   := EdCTe_mod.ValueVariant;
  CTe_serie := EdCTe_serie.ValueVariant;

  //
  NFVP_FatID     := EdNFVP_FatID.ValueVariant;*)
  (*NFVP_StaLnk    := EdNFVP_StaLnk.ValueVariant;
  NFRP_FatID     := EdNFRP_FatID.ValueVariant;
  NFRP_FatNum    := EdNFRP_FatNum.ValueVariant;
  NFRP_StaLnk    := EdNFRP_StaLnk.ValueVariant;
  NFCC_FatID     := EdNFCC_FatID.ValueVariant;*)
  (*NFCC_StaLnk    := EdNFCC_StaLnk.ValueVariant;

  IND_PGTO       := EdIND_PGTO.ValueVariant;
  IND_FRT        := EdIND_FRT.ValueVariant;

  ICMSTot_vProd  := EdICMSTot_vProd.ValueVariant;
  ICMSTot_vFrete := EdICMSTot_vFrete.ValueVariant;
  ICMSTot_vSeg   := EdICMSTot_vSeg.ValueVariant;
  ICMSTot_vDesc  := EdICMSTot_vDesc.ValueVariant;
  ICMSTot_vOutro := EdICMSTot_vOutro.ValueVariant;
  IPI            := EdIPI.ValueVariant;

  Codigo := EdCodigo.ValueVariant;*)
  //
  if VerificaChaveNFeDeEntrada(Empresa, CI, refNFe, NFe_CC) = False
    then Exit;
  //
  Result := True;
end;

procedure TFmPQEPed3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQEPed3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQEPed3.FormShow(Sender: TObject);
begin
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  EdCI.ValueVariant := DModG.QrEmpresasCodigo.Value;
  CBCI.KeyValue     := DModG.QrEmpresasCodigo.Value;
  try
    EdCI.SetFocus;
  except
    //nada
  end;
end;

function TFmPQEPed3.CadastroItensOK(const FatID, FatNum, Empresa, CodInfoEmit: Integer): Boolean;
const
  sProcName = 'TFmPQEPed3.CadastroItensOK()';
var
  prod_cProd, prod_xProd, prod_cEAN, prod_NCM, prod_uCom, infAdProd,
  prod_EXTIPI, IPI_cEnq: String;
  prod_CFOP, ICMS_Orig, ICMS_CST, IPI_CST, PIS_CST, COFINS_CST, Nivel1: Integer;
  //
  GraGruEIts_NomeGGX: String;
  GraGruEIts_GraGruX: Integer;
  //
  ItensTot, ItensOkA, ItensOkB: Integer;
  EhServico: Boolean;
  RegrFiscal, CRT_Emitido, CST_A_Emitido, CST_B_Emitido, CSOSN_Emitido,
  CFOP_Emitido: Integer;
  CFOP_Entrada: String; (*var ICMS_Aliq: Double;*)
  CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer;
  InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST: String;
  OriTES, EFD_II_C195, GenCtbD, GenCtbC: Integer;
  TES_ICMS, TES_IPI, TES_PIS, TES_COFINS, PVD_MadeBy,
  TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS: Integer;
  ICMSAliq, PISAliq, COFINSAliq: Double;
  FisRegGenCtbD, FisRegGenCtbC: Integer;
begin
  Result := False;
  //
  ReopenCabA(FatID, FatNum, Empresa);
  ReopenItsI(FatID, FatNum, Empresa);
  //
  if QrItsI.RecordCount = 0 then
  begin
    Geral.MB_Aviso('Nenhum item foi lozalizado na NFe baixada');
    Exit;
  end;
  // ver se est�o todos produtos atrelados!
  ItensTot     := QrItsI.RecordCount;
  ItensOkA     := 0;
  ItensOkB     := 0;
  while not QrItsI.Eof do
  begin
    ReopenItsN(FatID, FatNum, Empresa);
    //
    prod_cProd   := QrItsIprod_cProd.Value;
    prod_xProd   := QrItsIprod_xProd.Value;
    prod_cEAN    := QrItsIprod_cEAN.Value;
    prod_NCM     := QrItsIprod_NCM.Value;
    prod_uCom    := QrItsIprod_uCom.Value;
    infAdProd    := QrItsVinfAdProd.Value;
    prod_CFOP    := QrItsIprod_CFOP.Value;
    ICMS_Orig    := QrItsNICMS_Orig.Value;
    ICMS_CST     := QrItsNICMS_CST.Value;
    IPI_CST      := QrItsOIPI_CST.Value;
    PIS_CST      := QrItsQPIS_CST.Value;
    COFINS_CST   := QrItsSCOFINS_CST.Value;
    prod_EXTIPI  := QrItsIprod_EXTIPI.Value;
    IPI_cEnq     := QrItsOIPI_cEnq.Value;
    //
    DmNFe_0000.QrCod.Close;
    DmNFe_0000.QrCod.Params[00].AsInteger := CodInfoEmit;
    DmNFe_0000.QrCod.Params[01].AsString  := Trim(prod_cProd);
    UMyMod.AbreQuery(DmNFe_0000.QrCod, Dmod.MyDB, sProcName);
    //
    Nivel1 := 0;
    case DmNFe_0000.QrCod.RecordCount of
      0:
      begin
        //if CO_DMKID_APP <> 2 then //Bluederm => O Marco n�o quer que configure o produto
        //begin
          VAR_NOME_NOVO_GG1 := prod_xProd;
          Grade_Jan.MostraFormGraGruEIts(stIns, CodInfoEmit,
            prod_cProd, prod_xProd, prod_cEAN, prod_NCM, prod_uCom,
            infAdProd, prod_CFOP, ICMS_Orig, ICMS_CST, IPI_CST,
            PIS_CST, COFINS_CST, prod_EXTIPI, IPI_cEnq,
            GraGruEIts_GraGruX, GraGruEIts_NomeGGX);
          //
          VAR_NOME_NOVO_GG1 := EmptyStr;
          //
          DmNFe_0000.QrCod.Close;
          DmNFe_0000.QrCod.Params[00].AsInteger := CodInfoEmit;
          DmNFe_0000.QrCod.Params[01].AsString  := Trim(prod_cProd);
          UMyMod.AbreQuery(DmNFe_0000.QrCod, Dmod.MyDB, sProcName);
          //
          Nivel1 := DmNFe_0000.QrCodNivel1.Value;
        //end else
          //Nivel1 := 0;
      end;
      else
        Nivel1 := DmNFe_0000.QrCodNivel1.Value;
    end;
    if Nivel1 <> 0 then
    begin
      ItensOkA := ItensOkA + 1;
      //
      if GraGruEIts_GraGruX <> 0 then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE nfeitsi ',
        'SET GraGruX=' + Geral.FF0(GraGruEIts_GraGruX),
        ', Nivel1=' + Geral.FF0(Nivel1),
        'WHERE FatID=' + Geral.FF0(FatID),
        'AND FatNum=' + Geral.FF0(FatNum),
        'AND Empresa=' + Geral.FF0(Empresa),
        'AND nItem=' + Geral.FF0(QrItsInItem.Value),
        'AND FatID>1', // evitar erro nas NFes emitidas!
        '']);
      end;
    end;

    /////////////  CFOP ////////////////////////////////////////////////////////

    RegrFiscal       := QrPediVdaRegrFiscal.Value;
    EhServico        := QrItsIEhServico.Value = 1;
    CFOP_Emitido     := QrItsIprod_CFOP.Value;
    CRT_Emitido      := QrCabAemit_CRT.Value;
    CST_A_Emitido    := QrItsNICMS_Orig.Value;
    CST_B_Emitido    := QrItsNICMS_CST.Value;
    CSOSN_Emitido    := QrItsNICMS_CSOSN.Value;
    PVD_MadeBy       := QrPediVdaMadeBy.Value;

    //
    Result := DmNFe_0000.ObtemNumeroCFOP_Entrada(
    (*Tabela_FatPedFis: String; Parametros: array of integer;*)
    Empresa, CodInfoEmit, PVD_MadeBy, RegrFiscal, EhServico,
    CRT_Emitido, CST_A_Emitido, CST_B_Emitido, CSOSN_Emitido,
    Geral.FormataCFOP(Geral.FF0(CFOP_Emitido)),
    CFOP_Entrada, (*var ICMS_Aliq: Double;*)
    CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib,
    InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST,
    OriTES, EFD_II_C195, GenCtbD, GenCtbC,
    TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
    TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS,
    ICMSAliq, PISAliq, COFINSAliq, FisRegGenCtbD, FisRegGenCtbC);
    if Result then
    begin
      ItensOkB := ItensOkB + 1;
    end;
    //
    QrItsI.Next;
  end;
  //
  // ...
  Result := (ItensTot = ItensOkA) and (ItensTot = ItensOkB);
  if ItensTot <> ItensOkA then
    Geral.MB_Aviso('N�o foi poss�vel verificar todos itens da NFe X cadastro!');
  if ItensTot <> ItensOkB then
    Geral.MB_Aviso('N�o foi poss�vel verificar os dados fiscais de todos itens da NFe!');
end;

end.
