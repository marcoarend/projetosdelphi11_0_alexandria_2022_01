object FmWBPallet: TFmWBPallet
  Left = 368
  Top = 194
  Caption = 
    'WET-RECUR-015 :: Cadastro de Pallet de Mat'#233'ria-prima para Semi /' +
    ' Acabado'
  ClientHeight = 451
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 355
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 500
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 420
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 500
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 560
        Top = 32
        Width = 432
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 292
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 355
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 500
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsWBPallet
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 420
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsWBPallet
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 500
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsWBPallet
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 560
        Top = 32
        Width = 432
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsWBPallet
        TabOrder = 3
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 291
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pallet'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 224
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&MP'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtFrn: TBitBtn
          Tag = 10129
          Left = 252
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fornece'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtFrnClick
        end
      end
    end
    object DBGArt: TDBGrid
      Left = 0
      Top = 65
      Width = 501
      Height = 226
      Align = alLeft
      DataSource = DsWBPalArt
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
          Width = 400
          Visible = True
        end
        item
          Expanded = False
          Visible = True
        end>
    end
    object DBGFrn: TDBGrid
      Left = 728
      Top = 77
      Width = 226
      Height = 100
      DataSource = DsWBPalFrn
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Fornece'
          Title.Caption = 'Fornecedor'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Nome'
          Width = 400
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 698
        Height = 32
        Caption = 'Cadastro de Pallet de Mat'#233'ria-prima para Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 698
        Height = 32
        Caption = 'Cadastro de Pallet de Mat'#233'ria-prima para Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 698
        Height = 32
        Caption = 'Cadastro de Pallet de Mat'#233'ria-prima para Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 64
    Top = 56
  end
  object QrWBPallet: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrWBPalletBeforeOpen
    AfterOpen = QrWBPalletAfterOpen
    BeforeClose = QrWBPalletBeforeClose
    AfterScroll = QrWBPalletAfterScroll
    SQL.Strings = (
      'SELECT let.*,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA'
      'FROM wbpallet let'
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa')
    Left = 236
    Top = 69
    object QrWBPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWBPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrWBPalletLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWBPalletDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWBPalletDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWBPalletUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWBPalletUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWBPalletAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWBPalletAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWBPalletEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrWBPalletNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
  end
  object DsWBPallet: TDataSource
    DataSet = QrWBPallet
    Left = 236
    Top = 113
  end
  object QrWBPalArt: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrWBPalArtBeforeClose
    AfterScroll = QrWBPalArtAfterScroll
    SQL.Strings = (
      'SELECT art.*, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR'
      'FROM wbpalart art'
      'LEFT JOIN gragrux ggx ON ggx.Controle=art.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      '')
    Left = 300
    Top = 69
    object QrWBPalArtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWBPalArtControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrWBPalArtGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrWBPalArtLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWBPalArtDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWBPalArtDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWBPalArtUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWBPalArtUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWBPalArtAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWBPalArtAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWBPalArtNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsWBPalArt: TDataSource
    DataSet = QrWBPalArt
    Left = 300
    Top = 113
  end
  object PMArt: TPopupMenu
    OnPopup = PMArtPopup
    Left = 676
    Top = 368
    object ArtInclui1: TMenuItem
      Caption = '&Adiciona uma MP'
      Enabled = False
      OnClick = ArtInclui1Click
    end
    object ArtAltera1: TMenuItem
      Caption = '&Muda a MP selecionada'
      Enabled = False
      OnClick = ArtAltera1Click
    end
    object ArtExclui1: TMenuItem
      Caption = '&Removea a MP selecionada'
      Enabled = False
      OnClick = ArtExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 544
    Top = 364
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrWBPalFrn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frn.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE'
      'FROM wbpalfrn frn'
      'LEFT JOIN entidades ent ON ent.Codigo=frn.Fornece')
    Left = 368
    Top = 69
    object QrWBPalFrnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWBPalFrnControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrWBPalFrnConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrWBPalFrnFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrWBPalFrnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWBPalFrnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWBPalFrnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWBPalFrnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWBPalFrnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWBPalFrnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWBPalFrnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWBPalFrnNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
  end
  object DsWBPalFrn: TDataSource
    DataSet = QrWBPalFrn
    Left = 368
    Top = 113
  end
  object PMFrn: TPopupMenu
    OnPopup = PMFrnPopup
    Left = 780
    Top = 364
    object FrnInclui1: TMenuItem
      Caption = '&Adiciona um fornecdor na MP selecionada'
      Enabled = False
      OnClick = FrnInclui1Click
    end
    object FrnAltera1: TMenuItem
      Caption = '&Muda o fornecedor selecionado'
      Enabled = False
      OnClick = FrnAltera1Click
    end
    object FrnExclui1: TMenuItem
      Caption = '&Remove o fornecedor selecionado'
      Enabled = False
      OnClick = FrnExclui1Click
    end
  end
end
