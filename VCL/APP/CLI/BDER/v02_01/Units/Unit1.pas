unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  dmkEdit, dmkGeral, UnDmkProcFunc, Vcl.ComCtrls, dmkEditDateTimePicker,
  Vcl.ExtCtrls, dmkRadioGroup, UnDmkEnums, dmkMemo;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdPecas: TdmkEdit;
    Label3: TLabel;
    Label8: TLabel;
    EdNota: TdmkEdit;
    Button2: TButton;
    Button1: TButton;
    EdPesoKg: TdmkEdit;
    Label2: TLabel;
    Label5: TLabel;
    EdFatorMP: TdmkEdit;
    EdFatorAR: TdmkEdit;
    Label6: TLabel;
    Label4: TLabel;
    EdNotaRMP: TdmkEdit;
    Label7: TLabel;
    EdAreaIdeal: TdmkEdit;
    EdAreaIdealTotal: TdmkEdit;
    EdAreaM2: TdmkEdit;
    Panel2: TPanel;
    Label9: TLabel;
    EdAnoMes: TdmkEdit;
    Label10: TLabel;
    EdIncremento: TdmkEdit;
    EdAnoMesNovo: TdmkEdit;
    Label11: TLabel;
    Button3: TButton;
    Label12: TLabel;
    TPDataSPED: TdmkEditDateTimePicker;
    Button4: TButton;
    EdPerDoTip: TdmkEdit;
    RGSPED_EFD_Peri: TdmkRadioGroup;
    dmkEdit1: TdmkEdit;
    Memo1: TMemo;
    Label13: TLabel;
    EdKgM2: TdmkEdit;
    EdKgM2Nota: TdmkEdit;
    Label14: TLabel;
    Label15: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure EdAreaIdealChange(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure dmkEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    function MediaKgM2(Peso, AreaM2: Double): Double;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  UnVS_PF, UnEfdIcmsIpi_PF;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  Pecas, Peso, AreaM2, FatorMP, FatorAR, Nota: Double;
begin
  Pecas   :=  EdPecas.ValueVariant;
  Peso    :=  EdPesoKg.ValueVariant;
  AreaM2  :=  EdAreaM2.ValueVariant;
  //FatorMP :=  1 / (1 - (EdQuebra.ValueVariant / 100));
  FatorMP :=  EdFatorMP.ValueVariant;
  FatorAR :=  EdFatorAR.ValueVariant;
  //
  Nota := VS_PF.NotaCouroRibeiraApuca(Pecas, Peso, AreaM2, FatorMP, FatorAR);
  EdNotaRMP.ValueVariant := Nota;
  EdKgM2.ValueVariant := MediaKgM2(Peso, AreaM2);
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  Pecas, Peso, AreaM2, FatorMP, FatorAR, Nota: Double;
begin
  Pecas   :=  EdPecas.ValueVariant;
  Peso    :=  EdPesoKg.ValueVariant;
  //FatorMP :=  1 / (1 - (EdQuebra.ValueVariant / 100));
  FatorMP :=  EdFatorMP.ValueVariant;
  FatorAR :=  EdFatorAR.ValueVariant;
  //
  Nota := EdNota.ValueVariant;
  //
  AreaM2 := VS_PF.NotaCouroRibeiraApuca_Area(Pecas, Peso, FatorMP, FatorAR, Nota);
  EdAreaIdeal.ValueVariant := AreaM2;
  EdKgM2Nota.ValueVariant := MediaKgM2(Peso, EdAreaIdealTotal.ValueVariant);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  EdAnoMesNovo.ValueVariant := DmkPF.IncrementaAnoMes(
    EdAnoMes.ValueVariant, EdIncremento.ValueVariant);
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  EdPerDoTip.ValueVariant := EfdIcmsIpi_PF.ObtemPeriodoSPEDdeData(TPDataSPED.Date,
     TTipoPeriodoFiscal(RGSPED_EFD_Peri.ItemIndex));
end;

procedure TForm1.dmkEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Linha: String;
begin
  Linha := '';
  if ssShift in Shift then
    Linha := '[Shift] ';
  Memo1.Lines.Add(Linha + Char(Key) + ' ' + Geral.FF0(Key));
end;

procedure TForm1.EdAreaIdealChange(Sender: TObject);
begin
  EdAreaIdealTotal.ValueVariant :=
    EdAreaIdeal.ValueVariant * EdPecas.ValueVariant;
end;

function TForm1.MediaKgM2(Peso, AreaM2: Double): Double;
begin
  if AreaM2 = 0 then
    Result := 0
  else
    Result := Peso / AreaM2;
end;

end.
