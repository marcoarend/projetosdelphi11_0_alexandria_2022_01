unit PQI;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, DmkDAC_PF,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, DBGrids, Menus, frxClass, frxDBSet, Variants, dmkGeral, UnDmkProcFunc,
  dmkDBLookupComboBox, dmkEditCB, dmkDBEdit, dmkEdit, dmkEditDateTimePicker,
  dmkLabel, dmkImage, UnDmkEnums, UnEmpresas;

type
  TFmPQI = class(TForm)
    PnDados: TPanel;
    DsPQI: TDataSource;
    QrPQI: TMySQLQuery;
    PnEdita: TPanel;
    PnEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    PnData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrPQICodigo: TIntegerField;
    QrPQIDataB: TDateField;
    QrPQILk: TIntegerField;
    QrPQIDataCad: TDateField;
    QrPQIDataAlt: TDateField;
    QrPQIUserCad: TIntegerField;
    QrPQIUserAlt: TIntegerField;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    Label2: TLabel;
    TPDataB: TdmkEditDateTimePicker;
    QrPQIIts: TMySQLQuery;
    QrPQIItsDataX: TDateField;
    QrPQIItsTipo: TSmallintField;
    QrPQIItsCliOrig: TIntegerField;
    QrPQIItsCliDest: TIntegerField;
    QrPQIItsInsumo: TIntegerField;
    QrPQIItsPeso: TFloatField;
    QrPQIItsValor: TFloatField;
    QrPQIItsOrigemCodi: TIntegerField;
    QrPQIItsOrigemCtrl: TIntegerField;
    QrPQIItsNOMEPQ: TWideStringField;
    QrPQIItsGrupoQuimico: TIntegerField;
    QrPQIItsNOMEGRUPO: TWideStringField;
    QrPQIItsEmpresa: TIntegerField;
    DsPQIIts: TDataSource;
    DBGIts: TDBGrid;
    QrPQ: TmySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsPQ: TDataSource;
    DsCI: TDataSource;
    QrCI: TmySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    QrSaldo: TmySQLQuery;
    QrSaldoPeso: TFloatField;
    QrSaldoValor: TFloatField;
    QrSaldoCUSTO: TFloatField;
    QrSaldoCI: TIntegerField;
    QrSaldoPQ: TIntegerField;
    DsSaldo: TDataSource;
    PnInsumos: TPanel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    EdPesoAdd: TdmkEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    EdSaldoFut: TdmkEdit;
    PMInclui: TPopupMenu;
    IncluiNovaBaixa1: TMenuItem;
    IncluiInsumobaixaatual1: TMenuItem;
    QrSumPQ: TmySQLQuery;
    QrSumPQCUSTO: TFloatField;
    PMExclui: TPopupMenu;
    ExcluiItemdabaixa1: TMenuItem;
    ExcluiBaixa1: TMenuItem;
    PMAltera: TPopupMenu;
    AlteraBaixa1: TMenuItem;
    PMImprime: TPopupMenu;
    Estabaixa1: TMenuItem;
    Outros1: TMenuItem;
    frxDsPQIIts: TfrxDBDataset;
    frxDsPQI: TfrxDBDataset;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBConfirm2: TGroupBox;
    Panel6: TPanel;
    Panel7: TPanel;
    BtConfirma2: TBitBtn;
    BtDesiste2: TBitBtn;
    GBControle: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    CkDtCorrApo: TCheckBox;
    Label14: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrSE: TMySQLQuery;
    QrSECodigo: TIntegerField;
    QrSENome: TWideStringField;
    DsSE: TDataSource;
    Label3: TLabel;
    EdSetor: TdmkEditCB;
    CBSetor: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdFunci: TdmkEditCB;
    CBFunci: TdmkDBLookupComboBox;
    SpeedButton6: TSpeedButton;
    Label5: TLabel;
    EdNome: TdmkEdit;
    QrFunci: TMySQLQuery;
    DsFunci: TDataSource;
    QrFunciCodigo: TIntegerField;
    QrFunciNO_Funci: TWideStringField;
    QrPQINome: TWideStringField;
    QrPQISetor: TIntegerField;
    QrPQIFunci: TIntegerField;
    QrPQINO_FUNCI: TWideStringField;
    QrPQINO_SETOR: TWideStringField;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    frxUSO_CONSU_001_A: TfrxReport;
    QrPQITipoEnt: TSmallintField;
    SintticoCustos1: TMenuItem;
    EdCAprov: TdmkEdit;
    Label15: TLabel;
    Label16: TLabel;
    TPDtaDevol: TdmkEditDateTimePicker;
    QrPQII: TMySQLQuery;
    QrPQIICodigo: TIntegerField;
    QrPQIIControle: TIntegerField;
    QrPQIICAprov: TWideStringField;
    QrPQIIDtaDevol: TDateField;
    QrPQIIDtaDevol_TXT: TWideStringField;
    QrPQIItsCAprov: TWideStringField;
    QrPQIItsDtaDevol_TXT: TWideStringField;
    Datadedevoluo1: TMenuItem;
    frxUSO_CONSU_001_A_01: TfrxReport;
    RecibodeentrgamodeloA1: TMenuItem;
    N1: TMenuItem;
    QrPQICNPJCPF: TWideStringField;
    QryI: TMySQLQuery;
    QryITEXTO: TWideStringField;
    TPDtCorrApo: TdmkEditDateTimePicker;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPQIAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrPQIAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPQIBeforeOpen(DataSet: TDataSet);
    procedure QrPQIAfterClose(DataSet: TDataSet);
    procedure EdPQChange(Sender: TObject);
    procedure EdCIChange(Sender: TObject);
    procedure EdPesoAddExit(Sender: TObject);
    procedure QrSaldoAfterOpen(DataSet: TDataSet);
    procedure BtDesiste2Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure IncluiNovaBaixa1Click(Sender: TObject);
    procedure IncluiInsumobaixaatual1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure ExcluiItemdabaixa1Click(Sender: TObject);
    procedure AlteraBaixa1Click(Sender: TObject);
    procedure Estabaixa1Click(Sender: TObject);
    procedure Outros1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frBaixaAtualUserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    procedure frxUSO_CONSU_001_AGetValue(const VarName: String;
      var Value: Variant);
    procedure SbNovoClick(Sender: TObject);
    procedure ExcluiBaixa1Click(Sender: TObject);
    procedure CkDtCorrApoClick(Sender: TObject);
    procedure SintticoCustos1Click(Sender: TObject);
    procedure Datadedevoluo1Click(Sender: TObject);
    procedure RecibodeentrgamodeloA1Click(Sender: TObject);
    procedure EdCAprovKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    FPQTIts: Integer;
    FEmpresa: Integer;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenPQIIts;
    procedure CalculaSaldoFuturo;
    procedure AtualizaCusto;
    function  InserePQIIts(Codigo, Controle: Integer): Boolean;
    procedure ExcluiItem(Insumo, CliInt, Empresa, Controle: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmPQI: TFmPQI;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, PQx, Principal, GetPeriodo, UnPQ_PF, ModuleGeral,
  MyDBCheck;

{$R *.DFM}


const
  FThis_FATID = VAR_FATID_0180;


/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPQI.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPQI.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPQICodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPQI.DefParams;
begin
  VAR_GOTOTABELA := 'pqi';
  VAR_GOTOMYSQLTABLE := QrPQI;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := 'Empresa=' + Geral.FF0(FEmpresa);

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FUNCI,');
  VAR_SQLx.Add('ent.Tipo TipoEnt, IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJCPF, ');
  VAR_SQLx.Add('lse.Nome NO_SETOR, pqi.*');
  VAR_SQLx.Add('FROM pqi pqi');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=pqi.Funci');
  VAR_SQLx.Add('LEFT JOIN listasetores lse ON lse.Codigo=pqi.Setor');
  VAR_SQLx.Add('WHERE pqi.Codigo > -1000');
  VAR_SQLx.Add('AND pqi.Empresa=' + Geral.FF0(FEmpresa));
  //
  VAR_SQL1.Add('AND pqi.Codigo=:P0');
  //
  VAR_SQLa.Add('AND pqi.Nome Like :P0');
  //
end;

procedure TFmPQI.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      GBControle.Visible := True;
      PnDados.Visible    := True;
      PnEdita.Visible    := False;
      PnInsumos.Visible  := False;
      GBConfirm2.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      GBControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant  := '';
        TPDataB.Date           := Date;
        EdNome.Text            := '';
        EdSetor.ValueVariant   := 0;
        CBSetor.KeyValue       := null;
        EdFunci.ValueVariant   := 0;
        CBFunci.KeyValue       := null;
      end else begin
        EdCodigo.ValueVariant  := QrPQICodigo.Value;
        //
        TPDataB.Date           := QrPQIDataB.Value;
        EdNome.Text            := QrPQINome.Value;
        EdSetor.ValueVariant   := QrPQISetor.Value;
        CBSetor.KeyValue       := QrPQISetor.Value;
        EdFunci.ValueVariant   := QrPQIFunci.Value;
        CBFunci.KeyValue       := QrPQIFunci.Value;
      end;
      TPDataB.SetFocus;
    end;
    2:
    begin
      // 2023-09-09
      if UnPQx.ImpedePeloBalanco(QrPQIDataB.Value) then Exit;
      // 2023-09-09
      PnInsumos.Visible  := True;
      GBConfirm2.Visible := True;
      GBControle.Visible := False;
      //
      EdCI.Text := IntToStr(Dmod.QrMasterDono.Value);
      CBCI.KeyValue := Dmod.QrMasterDono.Value;
      EdCI.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmPQI.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPQI.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPQI.Datadedevoluo1Click(Sender: TObject);
var
  Data, Hora: TDateTime;
  Controle: Integer;
  DtaDevol: String;
begin
  //if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  if not DBCheck.ObtemData(Date, Data, Date, Hora, True) then Exit;
  DtaDevol := Geral.FDT(Data, 1);
  //
  Controle := QrPQIItsOrigemCtrl.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pqiits', True, [
  'DtaDevol'], [
  'Controle'], [
  DtaDevol], [
  Controle], True) then
    ReopenPQIIts();
end;

procedure TFmPQI.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPQI.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPQI.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPQI.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPQI.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPQI.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmPQI.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmPQI.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPQICodigo.Value;
  Close;
end;

procedure TFmPQI.BtConfirmaClick(Sender: TObject);
var
  Nome, DataB: String;
  Codigo, Empresa, Funci, Setor: Integer;
  SQLType: TSQLType;
begin
  if UnPQx.ImpedePeloBalanco(TPDataB.Date) then Exit;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  Funci          := EdFunci.ValueVariant;
  Setor          := EdSetor.ValueVariant;
  DataB          := Geral.FDT(TPDataB.Date, 1);

  //
  Codigo := UMyMod.BPGS1I32('pqi', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqi', False, [
  'Empresa', 'DataB',
  'Nome', 'Funci', 'Setor'], [
  'Codigo'], [
  Empresa, DataB,
  Nome, Funci, Setor], [
  Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqi', 'Codigo');
    AtualizaCusto;
    MostraEdicao(0, stLok, Codigo);
  end;
end;

procedure TFmPQI.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'pqi', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqi', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqi', 'Codigo');
end;

procedure TFmPQI.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //
  TPDtCorrApo.Date  := 0;
  TPDataB.Date      := Date;
  PnEdit.Align      := alClient;
  PnDados.Align     := alClient;
  PnEdita.Align     := alClient;
  DBGIts.Align      := alClient;
  LaRegistro.Align  := alClient;
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSE, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFunci, Dmod.MyDB);
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  CriaOForm;
end;

procedure TFmPQI.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPQICodigo.Value,LaRegistro.Caption);
end;

procedure TFmPQI.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPQI.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmPQI.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQI.QrPQIAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPQI.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'pqi', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmPQI.QrPQIAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrPQICodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrPQICodigo.Value, False);
  ReopenPQIIts;
end;

procedure TFmPQI.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPQICodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'pqi', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPQI.SintticoCustos1Click(Sender: TObject);
begin
  PQ_PF.MostraFormUsoConsImp(FThis_FATID);
end;

procedure TFmPQI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQI.QrPQIBeforeOpen(DataSet: TDataSet);
begin
  QrPQICodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPQI.QrPQIAfterClose(DataSet: TDataSet);
begin
  QrPQIIts.Close;
end;

procedure TFmPQI.RecibodeentrgamodeloA1Click(Sender: TObject);
begin
  MyObjects.frxDefineDatasets(frxUSO_CONSU_001_A_01, [
  DModG.frxDSDono,
  frxDsPQI,
  frxDsPQIIts
  ]);
  MyObjects.frxMostra(frxUSO_CONSU_001_A_01, 'Recibo de entrga de EPI');
end;

procedure TFmPQI.ReopenPQIIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQII, Dmod.MyDB, [
  'SELECT pii.*, IF(pii.DtaDevol < 2, "", ',
  '  DATE_FORMAT(pii.DtaDevol, "%d/%m/%y")) DtaDevol_TXT ',
  'FROM pqiits pii ',
  'WHERE pii.Codigo=' + Geral.FF0(QrPQICodigo.Value),
  ' ']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQIIts, Dmod.MyDB, [
  'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  ',
  'pqg.Nome NOMEGRUPO ',
  'FROM pqx pqx ',
  'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo ',
  'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ ',
  'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico ',
  'WHERE pqx.Tipo=' + Geral.FF0(FThis_FATID),
  'AND pqx.OrigemCodi=' + Geral.FF0(QrPQICodigo.Value),
  '']);
  //Geral.MB_Teste(QrPQIIts.SQL.Text);
  //
  if FPQTIts <> 0 then
    QrPQIIts.Locate('OrigemCtrl', FPQTIts, []);
end;

procedure TFmPQI.EdPQChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQI.EdCAprovKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  PQ: Integer;
  SQL, TextoSelected: String;
begin
  if Key in ([VK_F3, VK_F4]) then
  begin
    PQ := EdPQ.ValueVariant;
    if MyObjects.FIC(PQ = 0, EdPQ, 'Informe o material!') then Exit;
    //
    SQL := Geral.ATS([
    'SELECT pii.CAprov TEXTO, MAX(DataX) ULTDTA, ',
    'MAX(pqx.OrigemCtrl) ULTCTRL ',
    'FROM pqx pqx ',
    'LEFT JOIN pqiits pii ON pii.Codigo=pqx.OrigemCodi ',
    '  AND pii.Controle=pqx.OrigemCtrl ',
    'WHERE pqx.Insumo=' + Geral.FF0(PQ),
    'AND pqx.Tipo=' + Geral.FF0(FThis_FATID),
    'GROUP BY TEXTO ',
    'ORDER BY ULTDTA DESC, ULTCTRL DESC  ',
    '']);
    //
    //Geral.MB_Teste(SQL);
    if Key = VK_F3 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QryI, Dmod.MyDB, [SQL]);
      if QryITEXTO.Value <> EmptyStr then
        EdCAprov.ValueVariant := QryITEXTO.Value;
    end else
    if Key = VK_F4 then
    begin
      if DModG.ObtemTextoDeLista(Dmod.MyDB, SQL, TextoSelected) then
        EdCAprov.ValueVariant := TextoSelected;
    end;
  end;
end;

procedure TFmPQI.EdCIChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQI.EdPesoAddExit(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQI.CalculaSaldoFuturo;
var
  CI, PQ: Integer;
  PesoAdd, SaldoFut: Double;
begin
  CI := Geral.IMV(EdCI.Text);
  PQ := Geral.IMV(EdPQ.Text);
(*
  QrSaldo.Close;
  QrSaldo.Params[0].AsInteger := CI;
  QrSaldo.Params[1].AsInteger := PQ;
  UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrSaldo, Dmod.MyDB, [
  'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO ',
  'FROM pqcli ',
  'WHERE CI=' + Geral.FF0(CI),
  'AND PQ=' + Geral.FF0(PQ),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  '']);
  //
  PesoAdd := Geral.DMV(EdPesoAdd.Text);
  SaldoFut := QrSaldoPeso.Value - PesoAdd;
  EdSaldoFut.Text := Geral.FFT(SaldoFut, 3, siNegativo);
end;

procedure TFmPQI.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmPQI.QrSaldoAfterOpen(DataSet: TDataSet);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQI.BtDesiste2Click(Sender: TObject);
begin
  AtualizaCusto;
  MostraEdicao(0, stLok, 0);
end;

procedure TFmPQI.BtConfirma2Click(Sender: TObject);
const
  Unitario = True;
var
  Controle: Integer;
  KG, RS, SF: Double;
  //
  DataX: String;
  OriCodi, OriCtrl, OriTipo, CliOrig, CliDest, Insumo, Empresa: Integer;
  Peso, Valor: Double;
  DtCorrApo: String;
begin
  if PnInsumos.Visible then
  begin
    DataX      := Geral.FDT(QrPQIDataB.Value, 1);
    OriCodi    := QrPQICodigo.Value;
    OriTipo    := FThis_FATID;
    CliOrig    := QrSaldoCI.Value;
    CliDest    := Dmod.QrMasterDono.Value;
    Insumo     := EdPQ.ValueVariant;
    //
    KG := Geral.DMV(EdPesoAdd.Text);
    RS := KG * QrSaldoCUSTO.Value;
    Peso       := -KG;
    Valor      := -RS;
    DtCorrApo  := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
    //
    if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
    //
    if MyObjects.FIC(Insumo = 0, EdPQ, 'Informe o material!') then Exit;
    if MyObjects.FIC(KG <= 0, EdPesoAdd, 'Quantidade inv�lida!') then Exit;
    if MyObjects.FIC(QrSaldo.RecordCount = 0, EdPQ, 'Saldo zerado!') then Exit;
    SF := Geral.DMV(EdSaldoFut.Text);
    if MyObjects.FIC(SF < 0, EdPesoAdd, 'Saldo insuficiente!') then Exit;
    Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'EmitIts',
      'EmitIts', 'Controle');
    //
    OriCtrl    := Controle;
    if PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
    OriCtrl, OriTipo, Unitario, DtCorrApo, Empresa) then
    begin
      if not InserePQIIts(OriCodi, OriCtrl) then
      begin
        //Insumo   := QrPQIItsInsumo.Value;
        //CliInt   := QrPQIItsCliOrig.Value;
        //Empresa  := QrPQIItsEmpresa.Value;
        //Controle := QrPQIItsOrigemCtrl.Value;
        ExcluiItem(Insumo, CliOrig, Empresa, Controle);
        Exit;
      end;
    end;
    //
    FPQTIts := Controle;
    UnPQx.AtualizaEstoquePQ(QrSaldoCI.Value, QrSaldoPQ.Value, Empresa, aeMsg, '');
    QrSaldo.Close;
    UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
    ReopenPQIIts;
    AtualizaCusto;
    EdPesoAdd.ValueVariant := 0.000;
    EdPQ.ValueVariant := 0;
    CBPQ.KeyValue     := NULL;
    EdCAprov.Text     := '';
    //
    EdPQ.SetFocus;
  end;
end;

procedure TFmPQI.IncluiNovaBaixa1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

function TFmPQI.InserePQIIts(Codigo, Controle: Integer): Boolean;
var
  CAprov, DtaDevol: String;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  CAprov         := EdCAprov.ValueVariant;
  DtaDevol       := Geral.FDT(TPDtaDevol.Date, 1);
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqiits', False, [
  'Codigo', 'CAprov', 'DtaDevol'], [
  'Controle'], [
  Codigo, CAprov, DtaDevol], [
  Controle], True);
end;

procedure TFmPQI.IncluiInsumobaixaatual1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
  TPDtaDevol.Date := 0;
end;

procedure TFmPQI.AtualizaCusto;
var
  CustoHora: Double;
begin
(*
  LocCod(QrPQICodigo.Value, QrPQICodigo.Value);
  CustoHora := QrPQIHorasTrab.Value * QrPQIValorHora.Value;
  //
  QrSumPQ.Close;
  QrSumPQ.Params[0].AsInteger := QrPQICodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSumPQ, Dmod.MyDB);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE pqi SET CustoHora=:P0, CustoInsumo=:P1, ');
  Dmod.QrUpd.SQL.Add('CustoTotal=:P2 WHERE Codigo=:Pa ');
  Dmod.QrUpd.Params[00].AsFloat   := CustoHora;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPQCUSTO.Value;
  Dmod.QrUpd.Params[02].AsFloat   := CustoHora + QrSumPQCUSTO.Value;
  Dmod.QrUpd.Params[03].AsInteger := QrPQICodigo.Value;
  Dmod.QrUpd.ExecSQL;
*)
  LocCod(QrPQICodigo.Value, QrPQICodigo.Value);
end;

procedure TFmPQI.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmPQI.PMExcluiPopup(Sender: TObject);
begin
  ExcluiBaixa1.Enabled := not Geral.IntToBool_0(QrPQIIts.RecordCount);
  ExcluiItemdabaixa1.Enabled := Geral.IntToBool_0(QrPQIIts.RecordCount);
end;

procedure TFmPQI.ExcluiBaixa1Click(Sender: TObject);
begin
  // quando fazer cuidar controle de estoque por item de entrada de NFe!
  // Ver se d� para fazer como no PQO
end;

procedure TFmPQI.ExcluiItem(Insumo, CliInt, Empresa, Controle: Integer);
var
  //, Insumo, CliInt, Empresa
  Atual, OriCodi, OriCtrl, OriTipo: Integer;
begin
  if Application.MessageBox(PChar('Confirma a exclus�o do item selecionado '+
  'nesta baixa?'), 'Pergunta', MB_YESNO+MB_ICONQUESTION)= ID_YES then
  begin
    //Insumo  := QrPQIItsInsumo.Value;
    //CliInt  := QrPQIItsCliOrig.Value;
    //Empresa := QrPQIItsEmpresa.Value;
    //Atual   := QrPQIItsOrigemCtrl.Value;
    Atual  := Controle;
    FPQTIts := UMyMod.ProximoRegistro(QrPQIIts, 'OrigemCtrl', Atual);
    OriCodi := QrPQICodigo.Value;
    OriCtrl := Atual;
    OriTipo := FThis_FATID;
    PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, True, Empresa);
    //
    AtualizaCusto;
    UnPQx.AtualizaEstoquePQ(CliInt, Insumo, Empresa, aeMsg, '');
  end;
end;

procedure TFmPQI.ExcluiItemdabaixa1Click(Sender: TObject);
var
  Insumo, CliInt, Empresa, Controle: Integer;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQIDataB.Value) then Exit;
  // 2023-09-09
  Insumo   := QrPQIItsInsumo.Value;
  CliInt   := QrPQIItsCliOrig.Value;
  Empresa  := QrPQIItsEmpresa.Value;
  Controle := QrPQIItsOrigemCtrl.Value;
  ExcluiItem(Insumo, CliInt, Empresa, Controle);
end;

procedure TFmPQI.AlteraBaixa1Click(Sender: TObject);
var
  PQT : Integer;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQIDataB.Value) then Exit;
  // 2023-09-09
  PQT := QrPQICodigo.Value;
  if not UMyMod.SelLockY(PQT, Dmod.MyDB, 'pqi', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PQT, Dmod.MyDB, 'pqi', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPQI.Estabaixa1Click(Sender: TObject);
begin
  MyObjects.frxDefineDatasets(frxUSO_CONSU_001_A, [
  DModG.frxDSDono,
  frxDsPQI,
  frxDsPQIIts
  ]);
  MyObjects.frxMostra(frxUSO_CONSU_001_A, 'Baixa de EPI');
end;

procedure TFmPQI.Outros1Click(Sender: TObject);
begin
  PQ_PF.MostraFormPQImp();
end;

procedure TFmPQI.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmPQI.frBaixaAtualUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  if AnsiCompareText(Name, 'VFR_GRD') = 0 then Val := 15//Geral.BoolToInt2(CkGrade.Checked, 15, 0)
end;

procedure TFmPQI.frxUSO_CONSU_001_AGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VFR_GRD' then Value := 15 else
  if VarName = 'VARF_DATA' then Value := Now() else
  if VarName ='VARF_FUNCI_TERC' then
  begin
    if QrPQITipoEnt.Value = 0 then
      Value := 'Empresa'
    else
      Value := 'Funcion�rio';
  end else
  if VarName ='VARF_DOC' then
  begin
    if QrPQITipoEnt.Value = 0 then
      Value := 'CNPJ'
    else
      Value := 'CPF';
  end else
  if VarName = 'VARF_CNPJ_CPF' then
    Value := Geral.FormataCNPJ_TT(QrPQICNPJCPF.Value)
end;

end.

