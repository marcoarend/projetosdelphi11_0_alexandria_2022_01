�
 TFMFORMULASIMPFI 0�^  TPF0TFmFormulasImpFIFmFormulasImpFILeftSTop� Caption4   QUI-RECEI-105 :: Impressão de Receita de AcabamentoClientHeightqClientWidth^Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenter
OnActivateFormActivateOnCreate
FormCreate	OnDestroyFormDestroyOnResize
FormResizePixelsPerInch`
TextHeight TPanelPnIUserLeft Top0Width^Height�AlignalClient
BevelOuterbvNoneTabOrder  TPanelPanel3Left Top Width^HeightQAlignalTop
BevelOuterbvNoneTabOrder  TLabelLabel1LeftTopWidthuHeightCaptionReceita de acabamento:  TLabelLabel2LeftTop,WidthFHeightCaptionCliente interno:  TLabelLabel001Left�Top,WidthCHeightCaption   Data emissão:  
TdmkEditCBEdTintasCabLeftTopWidth8Height	AlignmenttaRightJustifyTabOrder 
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnOnChangeEdTintasCabChangeDBLookupComboBoxCBTintasCabIgnoraDBLookupComboBoxAutoSetIfOnlyOneRegsetregOnlyManual  TdmkDBLookupComboBoxCBTintasCabLeftDTopWidth�HeightKeyFieldNumero	ListFieldNome
ListSourceDsTintasCabTabOrder	dmkEditCBEdTintasCabUpdTypeutYesLocF7SQLMasc$#LocF7PreDefProcf7pNone  
TdmkEditCBEdCliIntLeftTop<Width8Height	AlignmenttaRightJustifyTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnDBLookupComboBoxCBCliIntIgnoraDBLookupComboBoxAutoSetIfOnlyOneRegsetregOnlyManual  TdmkDBLookupComboBoxCBCliIntLeftDTop<WidthEHeightKeyFieldCodigo	ListFieldNOMECI
ListSourceDsCliIntTabOrder	dmkEditCBEdCliIntUpdTypeutYesLocF7SQLMasc$#LocF7PreDefProcf7pNone  TdmkEditDateTimePicker
TPDataEmisLeft�Top<WidthpHeightDate      ?�@Time   X�����?TabOrderReadOnlyDefaultEditMask!99/99/99;1;_AutoApplyEditMask	UpdTypeutYesDatePurposedmkdpInsumMovimMin   TPanelPanel4Left TopQWidth^HeightGAlignalClient
BevelOuterbvNoneCaptionPanel4TabOrder TPanelPanel1Left Top Width^Height� AlignalClient
BevelOuterbvNoneTabOrder  TPanelPanel5Left Top Width^Height-AlignalTop
BevelOuterbvNoneTabOrder  TBitBtnBtTodosTagLeftTopWidthxHeight(CursorcrHandPointHintSai da janela atualCaption&Todos	NumGlyphsParentShowHintShowHint	TabOrder OnClickBtTodosClick  TBitBtnBtNenhumTag� Left� TopWidthxHeight(CursorcrHandPointHintSai da janela atualCaption&Nenhum	NumGlyphsParentShowHintShowHint	TabOrderOnClickBtNenhumClick  TRadioGroupRGTipoPrecoLefthTop Width� Height-AlignalRightCaption    Origem preços: Columns	ItemIndex Items.StringsEstoqueCadastro	A definir TabOrder  TRadioGroupRGModeloImpLeft�Top Width� Height-AlignalRightCaption    Modelo de impressão: Columns	ItemIndex Items.StringsPesagemCustos TabOrder   TDBGridDBGImpriFluLeft Top-Width^Height� AlignalClient
DataSource
DsImpriFluOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnCellClickDBGImpriFluCellClick
OnColEnterDBGImpriFluColEnter	OnColExitDBGImpriFluColExitOnDrawColumnCellDBGImpriFluDrawColumnCellColumnsExpanded	FieldNameAtivoReadOnly	Title.CaptionokWidthVisible	 Expanded	FieldNameOrdemFluWidth$Visible	 Expanded	FieldNameNomeFluTitle.CaptionFluxoWidth� Visible	 Expanded	FieldNameNomeTinTitle.CaptionProcessoWidth� Visible	 Expanded	FieldNameGramasM2Title.Caption   g/m²Visible	 Expanded	FieldNameCustoKgTitle.Caption$/kgVisible	 Expanded	FieldNameCustoToTitle.Caption$ totalVisible	 Expanded	FieldNameGramasToTitle.CaptionPeso gVisible	     TPanelPanel2Left Top� Width^HeightkAlignalBottom
BevelOuterbvNoneTabOrder TPanelPanel8Left Top WidtheHeightkAlignalLeft
BevelOuterbvNoneParentBackgroundTabOrder  TLabelLabel6LeftTopWidthOHeightCaptionLotes de couros:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TBitBtn
BtAdicionaTag
LeftTopWidthZHeight(HintConfirma a senha digitadaCaption	&AdicionaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	NumGlyphs
ParentFontTabOrder OnClickBtAdicionaClick  TBitBtnBtExcluiTagLeftTop@WidthZHeight(HintConfirma a senha digitadaCaptionE&xcluiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	NumGlyphs
ParentFontTabOrderOnClickBtExcluiClick   TDBGridDBGrid1LefteTop WidthHeightkAlignalClient
DataSource	DsEmitCusFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNamePecasTitle.Caption   PeçasWidth8Visible	 Expanded	FieldNameAreaM2Title.Caption	   Área m²Width8Visible	 Expanded	FieldName	TextoECorTitle.CaptionArtigo e cor (Texto livre)Width� Visible	 Expanded	FieldNameNOME_CLITitle.CaptionCliente finalWidthVVisible	    TPageControlPageControl2Left}Top Width� Heightk
ActivePage	TabSheet4AlignalRight	MultiLine	TabOrderTabPositiontpLeft 	TTabSheet	TabSheet4Caption   Observações TMemoEdMemoLeft Top Width� HeightcTabStopAlignalClientColorclWhiteTabOrder       TPanelPanel9Left Top�Width^Height0AlignalBottom
BevelOuterbvNoneTabOrder TLabelLabel29LeftTopWidth-HeightCaption   Área (m²):  TLabelLabel30LeftTTopWidth:HeightCaptionQuantidade:  TLabelLabel3Left� TopWidthHeightCaption   Peça:  TLabelLabel24Left`TopWidth4HeightCaption
Espessura:  TdmkEditEdAreaM2LeftTopWidthHHeight	AlignmenttaRightJustifyTabOrder 
FormatTypedmktfDoubleMskTypefmtNoneDecimalSize	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0,00UpdTypeutYesObrigatorioPermiteNuloValueVariant          ValWarn  TdmkEditEdPecasLeftTTopWidth@Height	AlignmenttaRightJustifyTabOrder
FormatTypedmktfDoubleMskTypefmtNoneDecimalSize	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0,0UpdTypeutYesObrigatorioPermiteNuloValueVariant          ValWarn  
TdmkEditCB	EdDefPecaLeft� TopWidth(Height	AlignmenttaRightJustifyTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnDBLookupComboBox	CBDefPecaIgnoraDBLookupComboBoxAutoSetIfOnlyOneRegsetregOnlyManual  TdmkDBLookupComboBox	CBDefPecaLeft� TopWidth� HeightKeyFieldCodigo	ListFieldNome
ListSource
DsDefPecasTabOrder	dmkEditCB	EdDefPecaUpdTypeutYesLocF7SQLMasc$#LocF7PreDefProcf7pNone  
TdmkEditCBEdEspessuraLeft`TopWidth(Height	AlignmenttaRightJustifyTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnDBLookupComboBoxCBEspessuraIgnoraDBLookupComboBoxAutoSetIfOnlyOneRegsetregOnlyManual  TdmkDBLookupComboBoxCBEspessuraLeft�TopWidth� HeightKeyFieldCodigo	ListFieldLinhas
ListSourceDsEspessurasTabOrder	dmkEditCBEdEspessuraUpdTypeutYesLocF7SQLMasc$#LocF7PreDefProcf7pNone  	TCheckBoxCkDtCorrApoLeftTopWidth� HeightCaption#   É correção de apontamento. Data:TabOrderOnClickCkDtCorrApoClick  TdmkEditDateTimePickerTPDtCorrApoLeft�TopWidth� HeightDate      �@Time   �P��?EnabledTabOrderReadOnlyDefaultEditMask!99/99/99;1;_AutoApplyEditMask	UpdTypeutYesDatePurposedmkdpSPED_EFD_MIN_MAX    TPanelPnCabecaLeft Top Width^Height0AlignalTop
BevelOuterbvNoneTabOrder 	TGroupBoxGB_RLeft.Top Width0Height0AlignalRightTabOrder  	TdmkImageImgTipoLeftTopWidth Height Transparent	SQLTypestNil   	TGroupBoxGB_LLeft Top Width0Height0AlignalLeftTabOrder  	TGroupBoxGB_MLeft0Top Width�Height0AlignalClientTabOrder TLabel
LaTitulo1ALeftTop	Width�Height Caption#   Impressão de Receita de AcabamentoColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclSilverFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFontVisible  TLabel
LaTitulo1BLeft	TopWidth�Height Caption#   Impressão de Receita de AcabamentoColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclGradientActiveCaptionFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFont  TLabel
LaTitulo1CLeftTop
Width�Height Caption#   Impressão de Receita de AcabamentoColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.Color
clHotLightFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFont    	TGroupBox	GBAvisos1Left Top�Width^Height9AlignalBottomCaption	 Avisos: TabOrder TPanelPanel6LeftTopWidthZHeight(AlignalClient
BevelOuterbvNoneTabOrder  TLabelLaAviso1LeftTopWidthxHeightCaption..............................Font.CharsetDEFAULT_CHARSET
Font.ColorclSilverFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	  TLabelLaAviso2LeftTopWidthxHeightCaption..............................Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	  TProgressBarPB1Left TopWidthZHeightAlignalBottomTabOrder     	TGroupBoxGBRodaPeLeft Top1Width^Height@AlignalBottomTabOrder TPanelPanel7LeftTopWidthZHeight/AlignalClient
BevelOuterbvNoneTabOrder  TLabelLabel13Left� Top Width,HeightCaptionEmpresa:  TPanel
PnSaiDesisLeft�Top Width� Height/AlignalRight
BevelOuterbvNoneTabOrder TBitBtnBtSaidaTagLeftTopWidthxHeight(CursorcrHandPointHintSai da janela atualCaption   &Saída	NumGlyphsParentShowHintShowHint	TabOrder OnClickBtSaidaClick   TBitBtnBtOKTagLeftTopWidthxHeight(Caption&OK	NumGlyphsTabOrder OnClick	BtOKClick  
TdmkEditCB	EdEmpresaLeft� TopWidth8HeightTabStop	AlignmenttaRightJustifyTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0QryCampoEmpresaUpdCampoEmpresaUpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnDBLookupComboBox	CBEmpresaIgnoraDBLookupComboBoxAutoSetIfOnlyOneRegsetregOnlyManual  TdmkDBLookupComboBox	CBEmpresaLeft� TopWidth�HeightKeyFieldFilial	ListField
NOMEFILIALTabOrderTabStop	dmkEditCB	EdEmpresaQryCampoEmpresaUpdTypeutYesLocF7SQLMasc$#LocF7PreDefProcf7pNone    TMySQLQueryQrTintasCabDatabase	Dmod.MyDBSQL.Strings0SELECT IF(cli.Tipo=0, Cli.RazaoSocial, cli.Nome)/NOMECLII, lse.Nome NOMESETOR, esp.LINHAS, cab.*FROM tintascab cab3LEFT JOIN entidades cli ON cli.Codigo=cab.ClienteI
3LEFT JOIN listasetores lse ON lse.Codigo=cab.Setor
5LEFT JOIN espessuras esp ON esp.Codigo=cab.Espessura
WHERE cab.Numero>0     LeftTop�  TWideStringFieldQrTintasCabNOMECLII	FieldNameNOMECLIISized  TWideStringFieldQrTintasCabNOMESETOR	FieldName	NOMESETOR  TWideStringFieldQrTintasCabLINHAS	FieldNameLINHASSize  TIntegerFieldQrTintasCabNumero	FieldNameNumeroRequired	  TWideStringFieldQrTintasCabTxtUsu	FieldNameTxtUsuRequired	Size
  TWideStringFieldQrTintasCabNomeDisplayWidthd	FieldNameNomeRequired	Sized  TIntegerFieldQrTintasCabClienteI	FieldNameClienteIRequired	  TIntegerFieldQrTintasCabTipificacao	FieldNameTipificacaoRequired	  
TDateFieldQrTintasCabDataI	FieldNameDataI  
TDateFieldQrTintasCabDataA	FieldNameDataA  TWideStringFieldQrTintasCabTecnico	FieldNameTecnico  TIntegerFieldQrTintasCabSetor	FieldNameSetor  TIntegerFieldQrTintasCabEspessura	FieldName	Espessura  TFloatFieldQrTintasCabAreaM2	FieldNameAreaM2DisplayFormat#,###,##0.00  TFloatFieldQrTintasCabQtde	FieldNameQtdeDisplayFormat#,###,##0.0  TIntegerFieldQrTintasCabLk	FieldNameLk  
TDateFieldQrTintasCabDataCad	FieldNameDataCadDisplayFormatdd/mm/yy  
TDateFieldQrTintasCabDataAlt	FieldNameDataAltDisplayFormatdd/mm/yy  TIntegerFieldQrTintasCabUserCad	FieldNameUserCad  TIntegerFieldQrTintasCabUserAlt	FieldNameUserAlt  TSmallintFieldQrTintasCabAlterWeb	FieldNameAlterWebRequired	  TSmallintFieldQrTintasCabAtivo	FieldNameAtivoRequired	   TDataSourceDsTintasCabDataSetQrTintasCabLeftTop�   TMySQLQueryQrCliIntDatabase	Dmod.MyDBSQL.Strings.SELECT CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial"ELSE ci.Nome END NOMECI, ci.CodigoFROM entidades ciWHERE ci.Cliente2="V" Left Topd TWideStringFieldQrCliIntNOMECI	FieldNameNOMECISized  TIntegerFieldQrCliIntCodigo	FieldNameCodigoRequired	   TDataSourceDsCliIntDataSetQrCliIntLeftTopd  TMySQLQueryQrEspessurasDatabase	Dmod.MyDBSQL.StringsSELECT Codigo, Linhas, EMCMFROM espessurasORDER BY EMCM Left Top TIntegerFieldQrEspessurasCodigo	FieldNameCodigo  TWideStringFieldQrEspessurasLinhas	FieldNameLinhasSize  TFloatFieldQrEspessurasEMCM	FieldNameEMCM   TDataSourceDsEspessurasDataSetQrEspessurasLeftTop  TDataSource
DsImpriFluDataSet
QrImpriFluLeft� Top�   TMySQLQuery
QrImpriItsDatabase	Dmod.MyDBSQL.Strings8SELECT pq.Ativo, pq.Nome, pcl.PQ PQCI, pcl.CustoPadrao, .pcl.MoedaPadrao, pcl.Controle PRODUTOCI, fi.* FROM _tintas_impri_its_ fi	4LEFT JOIN bluederm.PQ    pq  ON pq.Codigo=fi.Produto@LEFT JOIN bluederm.PQCli pcl ON pcl.PQ=fi.Produto AND pcl.CI=:P0 Left4Top,	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown   TIntegerFieldQrImpriItsOrdemFlu	FieldNameOrdemFlu  TIntegerFieldQrImpriItsOrdemIts	FieldNameOrdemIts  TIntegerFieldQrImpriItsTintas	FieldNameTintas  TIntegerFieldQrImpriItsProduto	FieldNameProduto  TWideStringFieldQrImpriItsNome	FieldNameNomeSize  TFloatFieldQrImpriItsGramasTi	FieldNameGramasTi  TFloatFieldQrImpriItsGramasKg	FieldNameGramasKg  TFloatFieldQrImpriItsGramasTo	FieldNameGramasTo  TFloatFieldQrImpriItsPrecoMo_Kg	FieldName
PrecoMo_Kg  TFloatFieldQrImpriItsCotacao_Mo	FieldName
Cotacao_Mo  TFloatFieldQrImpriItsCustoRS_Kg	FieldName
CustoRS_Kg  TFloatFieldQrImpriItsCustoRS_To	FieldName
CustoRS_To  TWideStringFieldQrImpriItsSiglaMoeda	FieldName
SiglaMoedaSize
  TIntegerFieldQrImpriItsPQCI	FieldNamePQCI  TFloatFieldQrImpriItsCustoPadrao	FieldNameCustoPadrao  TSmallintFieldQrImpriItsMoedaPadrao	FieldNameMoedaPadrao  TIntegerFieldQrImpriItsPRODUTOCI	FieldName	PRODUTOCIRequired	  TIntegerFieldQrImpriItsControle	FieldNameControle  TSmallintFieldQrImpriItsAtivo	FieldNameAtivo  TFloatFieldQrImpriItsCustoMedio	FieldName
CustoMedio   TMySQLQueryQrTintasFluDatabase	Dmod.MyDBSQL.Strings7SELECT tin.Nome NOMEPROCESSO, Tin.Ordem ORDEMTIN, flu.*FROM tintasflu flu3LEFT JOIN tintastin tin ON tin.Codigo=flu.TintasTinWHERE flu.Numero=:P0ORDER BY flu.Ordem LeftXTop� 	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown   TWideStringFieldQrTintasFluNOMEPROCESSO	FieldNameNOMEPROCESSOSize  TIntegerFieldQrTintasFluOrdem	FieldNameOrdem  TIntegerFieldQrTintasFluReordem	FieldNameReordem  TIntegerFieldQrTintasFluNumero	FieldNameNumero  TIntegerFieldQrTintasFluCodigo	FieldNameCodigo  TWideStringFieldQrTintasFluNome	FieldNameNomeSized  TIntegerFieldQrTintasFluTintasTin	FieldName	TintasTin  TFloatFieldQrTintasFluGramasM2	FieldNameGramasM2DisplayFormat#,###,##0.000  TIntegerFieldQrTintasFluORDEMTIN	FieldNameORDEMTIN  TFloatFieldQrTintasFluInfoCargM2	FieldName
InfoCargM2  TWideStringFieldQrTintasFluDescri	FieldNameDescriSize�   TSmallintFieldQrTintasFluOpProc	FieldNameOpProc   TMySQLQuery
QrImpriFluDatabase	Dmod.MyDBAfterScrollQrImpriFluAfterScrollRequestLive	Left� Top�  TSmallintFieldQrImpriFluAtivo	FieldNameAtivo  TIntegerFieldQrImpriFluOrdemTin	FieldNameOrdemTinMaxValue  TIntegerFieldQrImpriFluCodigo	FieldNameCodigo  TWideStringFieldQrImpriFluNomeFlu	FieldNameNomeFluSized  TWideStringFieldQrImpriFluNomeTin	FieldNameNomeTinSize  TIntegerFieldQrImpriFluTintasTin	FieldName	TintasTin  TIntegerFieldQrImpriFluOrdemFlu	FieldNameOrdemFluMaxValue  TFloatFieldQrImpriFluGramasM2	FieldNameGramasM2DisplayFormat#,###,##0.000  TFloatFieldQrImpriFluGramasTo	FieldNameGramasToDisplayFormat	#,###,##0  TFloatFieldQrImpriFluCustoTo	FieldNameCustoToDisplayFormat#,###,##0.00  TFloatFieldQrImpriFluCustoKg	FieldNameCustoKgDisplayFormat#,###,##0.0000  TFloatFieldQrImpriFluAreaM2	FieldNameAreaM2  TFloatFieldQrImpriFluCustoM2	FieldNameCustoM2  TFloatFieldQrImpriFluCusUSM2	FieldNameCusUSM2  TFloatFieldQrImpriFluInfoCargM2	FieldName
InfoCargM2  TWideStringFieldQrImpriFluDescriFlu	FieldName	DescriFluSize�   TSmallintFieldQrImpriFluOpProc	FieldNameOpProc   TMySQLQueryQrTintasItsDatabase	Dmod.MyDBOnCalcFieldsQrTintasItsCalcFieldsSQL.StringsSELECT pq_.Nome NOMEPQ,    (IF(pqc.CustoPadrao > 0, pqc.CustoPadrao, pqc.Valor/pqc.Peso) CustoPadrao,=IF((pqc.Valor/pqc.Peso>0) AND (pqc.Valor>0) AND (pqc.Peso>0),0pqc.Valor/pqc.Peso, pqc.CustoPadrao) CustoMedio,        &pqc.MoedaPadrao, tii.Numero TintasCab,)tii.Codigo TintasTin, tii.Ordem ORDEMITS,(tii.Controle, tii.Produto, tii.GramasTi, tii.Gramaskg, tii.Obs, tii.AtivoFROM tintasits tii    *LEFT JOIN pq pq_ ON pq_.Codigo=tii.Produto(LEFT JOIN pqcli pqc ON pqc.pq=pq_.CodigoWHERE tii.Codigo=:P0                             LeftTop,	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown   TWideStringFieldQrTintasItsNOMEPQ	FieldNameNOMEPQSize2  TFloatFieldQrTintasItsCustoPadrao	FieldNameCustoPadrao  TFloatFieldQrTintasItsCustoMedio	FieldName
CustoMedio  TSmallintFieldQrTintasItsMoedaPadrao	FieldNameMoedaPadrao  TIntegerFieldQrTintasItsTintasCab	FieldName	TintasCabRequired	  TIntegerFieldQrTintasItsTintasTin	FieldName	TintasTinRequired	  TIntegerFieldQrTintasItsControle	FieldNameControleRequired	  TIntegerFieldQrTintasItsProduto	FieldNameProduto  TFloatFieldQrTintasItsGramasTi	FieldNameGramasTi  TFloatFieldQrTintasItsGramaskg	FieldNameGramaskg  TWideStringFieldQrTintasItsObs	FieldNameObsSize  TIntegerFieldQrTintasItsORDEMITS	FieldNameORDEMITSRequired	  TWideStringFieldQrTintasItsSIGLAMOEDA	FieldKindfkCalculated	FieldName
SIGLAMOEDASize

Calculated	  TSmallintFieldQrTintasItsAtivo	FieldNameAtivo   TDataSource
DsImpriItsDataSet
QrImpriItsLeftPTop,  TDataSource
DsDefPecasDataSet
QrDefPecasLeftTop4  TMySQLQuery
QrDefPecasDatabase	Dmod.MyDBSQL.StringsSELECT * From DefPecas Left Top4 TIntegerFieldQrDefPecasCodigo	FieldNameCodigoOriginDBMBWET.defpecas.Codigo  TWideStringFieldQrDefPecasNome	FieldNameNomeOriginDBMBWET.defpecas.NomeSize  TSmallintFieldQrDefPecasGrandeza	FieldNameGrandezaOriginDBMBWET.defpecas.Grandeza   TMySQLQueryQrLotesDatabase	Dmod.MyDBLeft�Top  TMySQLQueryQrPreusoDatabase	Dmod.MyDBSQL.Strings+SELECT pq.Ativo, ems.Codigo, ems.Controle, pqc.MoedaPadrao, ems.ProdutoCI,0SUM(ems.Peso_PQ) Peso_PQ, ems.Produto, ems.PQCI,5ems.NomePQ, pqc.Peso, (pqc.Peso+999999999999) EXISTE,    (IF(pqc.CustoPadrao > 0, pqc.CustoPadrao, pqc.Valor/pqc.Peso) CustoPadrao,    =IF((pqc.Valor/pqc.Peso>0) AND (pqc.Valor>0) AND (pqc.Peso>0),0pqc.Valor/pqc.Peso, pqc.CustoPadrao) CustoMedio,    4ems.Cli_Orig, pqc.Peso-SUM(ems.Peso_PQ) PESOFUTURO, 5IF(cli.Tipo=0, cli.RazaoSocial,cli.Nome) NOMECLI_ORIGFROM emitits ems5LEFT JOIN PQCli     pqc ON pqc.Controle=ems.ProdutoCI'LEFT JOIN PQ     pq ON pq.Codigo=pqc.PQ2LEFT JOIN Entidades cli ON cli.Codigo=ems.Cli_OrigWHERE ems.Ativo=2AND ems.Codigo=:P0GROUP BY ems.Produto, Cli_Orig         Left�Top� 	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown   TIntegerFieldQrPreusoCodigo	FieldNameCodigoRequired	  TIntegerFieldQrPreusoControle	FieldNameControleRequired	  TFloatFieldQrPreusoPeso_PQ	FieldNamePeso_PQ  TIntegerFieldQrPreusoProduto	FieldNameProdutoRequired	  TIntegerFieldQrPreusoPQCI	FieldNamePQCIRequired	  TIntegerFieldQrPreusoProdutoCI	FieldName	ProdutoCIRequired	  TWideStringFieldQrPreusoNomePQ	FieldNameNomePQSize2  TFloatFieldQrPreusoPeso	FieldNamePeso  TFloatFieldQrPreusoPESOFUTURO	FieldName
PESOFUTURO  TIntegerFieldQrPreusoCli_Orig	FieldNameCli_OrigRequired	  TWideStringFieldQrPreusoNOMECLI_ORIG	FieldNameNOMECLI_ORIGSized  TFloatFieldQrPreusoDEFASAGEM	FieldKindfkCalculated	FieldName	DEFASAGEM
Calculated	  TFloatFieldQrPreusoCustoPadrao	FieldNameCustoPadrao  TSmallintFieldQrPreusoMoedaPadrao	FieldNameMoedaPadrao  TFloatFieldQrPreusoCustoMedio	FieldName
CustoMedio  TFloatFieldQrPreusoEXISTE	FieldNameEXISTE  TSmallintFieldQrPreusoAtivo	FieldNameAtivo   TMySQLQueryQrSumCusDatabase	Dmod.MyDBSQL.StringsSELECT SUM(CustoTo) CustoTo, SUM(CustoKg) CustoKg, SUM(CustoM2) CustoM2, SUM(CusUSM2) CusUSM2FROM _tintas_impri_flu_ Left�Top( TFloatFieldQrSumCusCustoKg	FieldNameCustoKg  TFloatFieldQrSumCusCustoTo	FieldNameCustoTo  TFloatFieldQrSumCusCustoM2	FieldNameCustoM2  TFloatFieldQrSumCusCusUSM2	FieldNameCusUSM2   TMySQLQuery	QrEmitCusDatabase	Dmod.MyDBSQL.Strings4SELECT CONCAT(mpi.Texto, " ", mpi.CorTxt) TextoECor,3IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_CLI,ecu.* FROM emitcus ecu/LEFT JOIN mpvits mpi ON mpi.Controle=ecu.MPVIts+LEFT JOIN mpp mpp ON mpp.Codigo=mpi.Pedido 2LEFT JOIN entidades ent ON ent.Codigo=mpp.Cliente WHERE ecu.Controle>0  Left�Top9 TWideStringFieldQrEmitCusTextoECor	FieldName	TextoECorSizeQ  TWideStringFieldQrEmitCusNOME_CLI	FieldNameNOME_CLISized  TIntegerFieldQrEmitCusCodigo	FieldNameCodigo  TIntegerFieldQrEmitCusControle	FieldNameControle  TIntegerFieldQrEmitCusMPIn	FieldNameMPIn  TIntegerFieldQrEmitCusFormula	FieldNameFormula  TDateTimeFieldQrEmitCusDataEmis	FieldNameDataEmis  TFloatFieldQrEmitCusPeso	FieldNamePeso  TFloatFieldQrEmitCusCusto	FieldNameCusto  TFloatFieldQrEmitCusPecas	FieldNamePecas  TSmallintFieldQrEmitCusAtivo	FieldNameAtivo  TIntegerFieldQrEmitCusMPVIts	FieldNameMPVIts  TFloatFieldQrEmitCusAreaM2	FieldNameAreaM2  TFloatFieldQrEmitCusAreaP2	FieldNameAreaP2  TFloatFieldQrEmitCusPercTotCus	FieldName
PercTotCus   TDataSource	DsEmitCusDataSet	QrEmitCusLeft�Top9  TMySQLQueryQrValDatabase	Dmod.MyDBLeft�Top�  TFloatFieldQrValCustoRS_To	FieldName
CustoRS_To   TMySQLQueryQrSqrDatabase	Dmod.MyDBLeft�Top�  TFloatFieldQrSqrAreaM2	FieldNameAreaM2    