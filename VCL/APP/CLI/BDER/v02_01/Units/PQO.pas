unit PQO;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, DBGrids, Menus, frxClass, frxDBSet, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, dmkEditDateTimePicker, UnDmkProcFunc,
  dmkLabel, dmkImage, UnDmkEnums, UnProjGroup_Consts, UnEmpresas;

type
  TFmPQO = class(TForm)
    PnDados: TPanel;
    DsPQO: TDataSource;
    QrPQO: TmySQLQuery;
    PnEdita: TPanel;
    PnEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    PnData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label2: TLabel;
    TPDataB: TdmkEditDateTimePicker;
    QrPQOIts: TmySQLQuery;
    QrPQOItsDataX: TDateField;
    QrPQOItsTipo: TSmallintField;
    QrPQOItsCliOrig: TIntegerField;
    QrPQOItsCliDest: TIntegerField;
    QrPQOItsInsumo: TIntegerField;
    QrPQOItsPeso: TFloatField;
    QrPQOItsValor: TFloatField;
    QrPQOItsOrigemCodi: TIntegerField;
    QrPQOItsOrigemCtrl: TIntegerField;
    QrPQOItsNOMEPQ: TWideStringField;
    QrPQOItsGrupoQuimico: TIntegerField;
    QrPQOItsNOMEGRUPO: TWideStringField;
    DsPQOIts: TDataSource;
    DBGIts: TDBGrid;
    QrPQ: TmySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsPQ: TDataSource;
    DsCI: TDataSource;
    QrCI: TmySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    QrSaldo: TmySQLQuery;
    QrSaldoPeso: TFloatField;
    QrSaldoValor: TFloatField;
    QrSaldoCUSTO: TFloatField;
    QrSaldoCI: TIntegerField;
    QrSaldoPQ: TIntegerField;
    DsSaldo: TDataSource;
    PnInsumos: TPanel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    EdPesoAdd: TdmkEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    EdSaldoFut: TdmkEdit;
    PMInclui: TPopupMenu;
    IncluiNovaBaixa1: TMenuItem;
    IncluiInsumobaixaatual1: TMenuItem;
    QrSumPQ: TmySQLQuery;
    QrSumPQCUSTO: TFloatField;
    PMExclui: TPopupMenu;
    ExcluiItemdabaixa1: TMenuItem;
    ExcluiBaixa1: TMenuItem;
    PMAltera: TPopupMenu;
    AlteraBaixa1: TMenuItem;
    PMImprime: TPopupMenu;
    Estabaixa1: TMenuItem;
    Outros1: TMenuItem;
    QrPeriodo: TmySQLQuery;
    EdSetor: TdmkEditCB;
    Label3: TLabel;
    CBSetor: TdmkDBLookupComboBox;
    QrSE: TmySQLQuery;
    QrSECodigo: TIntegerField;
    QrSENome: TWideStringField;
    DsSE: TDataSource;
    QrPQOCodigo: TIntegerField;
    QrPQOSetor: TIntegerField;
    QrPQOCustoInsumo: TFloatField;
    QrPQOCustoTotal: TFloatField;
    QrPQODataB: TDateField;
    QrPQOLk: TIntegerField;
    QrPQODataCad: TDateField;
    QrPQODataAlt: TDateField;
    QrPQOUserCad: TIntegerField;
    QrPQOUserAlt: TIntegerField;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    QrPeriodoCodigo: TIntegerField;
    QrPeriodoSetor: TIntegerField;
    QrPeriodoCustoInsumo: TFloatField;
    QrPeriodoCustoTotal: TFloatField;
    QrPeriodoDataB: TDateField;
    QrPeriodoLk: TIntegerField;
    QrPeriodoDataCad: TDateField;
    QrPeriodoDataAlt: TDateField;
    QrPeriodoUserCad: TIntegerField;
    QrPeriodoUserAlt: TIntegerField;
    QrPQONOMESETOR: TWideStringField;
    QrPeriodoNOMESETOR: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBControle: TGroupBox;
    PnNaviDwn: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBConfirm2: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma2: TBitBtn;
    BtDesiste2: TBitBtn;
    GBConfirma: TGroupBox;
    Panel1: TPanel;
    Panel7: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Alteraitematual1: TMenuItem;
    QrEmitGru: TmySQLQuery;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    DsEmitGru: TDataSource;
    Label14: TLabel;
    EdEmitGru: TdmkEditCB;
    CBEmitGru: TdmkDBLookupComboBox;
    QrPQONO_EMITGRU: TWideStringField;
    QrPQOAlterWeb: TSmallintField;
    QrPQOAtivo: TSmallintField;
    QrPQOEmitGru: TIntegerField;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    QrPQONome: TWideStringField;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    EdNome: TdmkEdit;
    Label11: TLabel;
    frxQUI_RECEI_011_A: TfrxReport;
    frxDsPQOIts: TfrxDBDataset;
    frxDsPQO: TfrxDBDataset;
    SpeedButton5: TSpeedButton;
    QrGraCorCad: TmySQLQuery;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    DsGraCorCad: TDataSource;
    Label15: TLabel;
    QrPQOGraCorCad: TIntegerField;
    QrPQONoGraCorCad: TWideStringField;
    DBEdit10: TDBEdit;
    Label16: TLabel;
    EdGraCorCad: TdmkEditCB;
    CBGraCorCad: TdmkDBLookupComboBox;
    QrRebaixe: TmySQLQuery;
    QrRebaixeCodigo: TIntegerField;
    QrRebaixeLinhas: TWideStringField;
    QrRebaixeEMCM: TFloatField;
    DsRebaixe: TDataSource;
    Label24: TLabel;
    EdSemiCodEspReb: TdmkEditCB;
    CBSemiCodEspReb: TdmkDBLookupComboBox;
    QrPQOSemiCodEspReb: TIntegerField;
    QrPQORebLin: TWideStringField;
    Label21: TLabel;
    DBEdit6: TDBEdit;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    Label22: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrPQOItsEmpresa: TIntegerField;
    QrFormulasGru: TMySQLQuery;
    QrFormulasGruCodigo: TIntegerField;
    QrFormulasGruNome: TWideStringField;
    DsFormulasGru: TDataSource;
    Label23: TLabel;
    EdFormulasGru: TdmkEditCB;
    CBFormulasGru: TdmkDBLookupComboBox;
    SbFormulasGru: TSpeedButton;
    QrPQOFormulasGru: TIntegerField;
    QrPQONO_FormulasGru: TWideStringField;
    DBEdit11: TDBEdit;
    Label25: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPQOAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrPQOAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPQOBeforeOpen(DataSet: TDataSet);
    procedure QrPQOAfterClose(DataSet: TDataSet);
    procedure EdPQChange(Sender: TObject);
    procedure EdCIChange(Sender: TObject);
    procedure EdPesoAddExit(Sender: TObject);
    procedure BtDesiste2Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure IncluiNovaBaixa1Click(Sender: TObject);
    procedure IncluiInsumobaixaatual1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure AlteraBaixa1Click(Sender: TObject);
    procedure Outros1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frBaixaAtualUserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    procedure ExcluiBaixa1Click(Sender: TObject);
    procedure ExcluiItemdabaixa1Click(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure Alteraitematual1Click(Sender: TObject);
    procedure PMAlteraPopup(Sender: TObject);
    procedure Estabaixa1Click(Sender: TObject);
    procedure frxQUI_RECEI_011_AGetValue(const VarName: string;
      var Value: Variant);
    procedure SpeedButton5Click(Sender: TObject);
    procedure CkDtCorrApoClick(Sender: TObject);
    procedure SbFormulasGruClick(Sender: TObject);
  private
    FPQOIts: Integer;
    FEmpresa: Integer;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenPQOIts;
    procedure CalculaSaldoFuturo();
    procedure AtualizaCusto;
  public
    { Public declarations }
    procedure ExcluiItemSelecionado(AtzCusto: Boolean);
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmPQO: TFmPQO;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, PQx, Principal, ModuleGeral, PQOEdit, MyDBCheck,
  DmkDAC_PF, UnPQ_PF, UnApp_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPQO.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPQO.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPQOCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPQO.DefParams;
begin
  VAR_GOTOTABELA := 'PQO';
  VAR_GOTOMYSQLTABLE := QrPQO;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := 'Empresa=' + Geral.FF0(FEmpresa);

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('lse.Nome NOMESETOR, reb.Linhas RebLin, ');
  VAR_SQLx.Add('emg.Nome NO_EMITGRU, gcc.Nome NoGraCorCad, ');
  VAR_SQLx.Add('frg.Nome NO_FormulasGru, pqo.*');
  VAR_SQLx.Add('FROM pqo pqo');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=pqo.Empresa');
  VAR_SQLx.Add('LEFT JOIN listasetores lse ON lse.Codigo=pqo.Setor');
  VAR_SQLx.Add('LEFT JOIN emitgru emg ON emg.Codigo=pqo.EmitGru');
  VAR_SQLx.Add('LEFT JOIN gracorcad gcc ON gcc.Codigo=pqo.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN espessuras reb ON reb.Codigo=pqo.SemiCodEspReb');
  VAR_SQLx.Add('LEFT JOIN formulasgru frg ON frg.Codigo=pqo.FormulasGru');
  VAR_SQLx.Add('WHERE pqo.Codigo > 0');
  VAR_SQLx.Add('AND pqo.Empresa=' + Geral.FF0(FEmpresa));
  //
  VAR_SQL1.Add('AND pqo.Codigo=:P0');
  //
  VAR_SQLa.Add('AND lse.Nome Like :P0');
  //
end;

procedure TFmPQO.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      GBControle.Visible := True;
      PnDados.Visible    := True;
      PnEdita.Visible    := False;
      PnInsumos.Visible  := False;
      GBConfirm2.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      GBControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant  := 0;
        EdNome.Text            := '';
        TPDataB.Date           := Date;
        EdSetor.ValueVariant   := 0;
        CBSetor.KeyValue       := Null;
        EdEmitGru.ValueVariant := 0;
        CBEmitGru.KeyValue     := Null;
        EdGraCorCad.ValueVariant := 0;
        CBGraCorCad.KeyValue     := Null;
        EdSemiCodEspReb.ValueVariant := 0;
        CBSemiCodEspReb.KeyValue     := Null;
        EdFormulasGru.ValueVariant   := 0;
        CBFormulasGru.KeyValue       := 0;
      end else begin
        EdCodigo.ValueVariant  := QrPQOCodigo.Value;
        EdNome.ValueVariant    := QrPQONome.Value;
        TPDataB.Date           := QrPQODataB.Value;
        EdSetor.ValueVariant   := QrPQOSetor.Value;
        CBSetor.KeyValue       := QrPQOSetor.Value;
        EdEmitGru.ValueVariant := QrPQOEmitGru.Value;
        CBEmitGru.KeyValue     := QrPQOEmitGru.Value;
        EdGraCorCad.ValueVariant := QrPQOGraCorCad.Value;
        CBGraCorCad.KeyValue     := QrPQOGraCorCad.Value;
        EdSemiCodEspReb.ValueVariant := QrPQOSemiCodEspReb.Value;
        CBSemiCodEspReb.KeyValue     := QrPQOSemiCodEspReb.Value;
        EdFormulasGru.ValueVariant   := QrPQOFormulasGru.Value;
        CBFormulasGru.KeyValue       := QrPQOFormulasGru.Value;
      end;
      TPDataB.SetFocus;
    end;
    2:
    begin
      // 2023-09-09
      if UnPQx.ImpedePeloBalanco(QrPQODataB.Value) then Exit;
      // 2023-09-09
      PnInsumos.Visible  := True;
      GBConfirm2.Visible := True;
      GBControle.Visible := False;
      //
      EdCI.ValueVariant  := Dmod.QrMasterDono.Value;
      CBCI.KeyValue      := Dmod.QrMasterDono.Value;
      //
      EdCI.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmPQO.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPQO.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPQO.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPQO.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPQO.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPQO.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPQO.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPQO.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  App_Jan.MostraFormEmitGru(EdEmitGru.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEmitGru, CBEmitGru, QrEmitGru, VAR_CADASTRO);
    //
    EdEmitGru.SetFocus;
  end;
end;

procedure TFmPQO.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmPQO.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmPQO.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPQOCodigo.Value;
  Close;
end;

procedure TFmPQO.BtConfirmaClick(Sender: TObject);
const
  Obriga = True;
var
  DataB, Nome: String;
  Codigo, Setor, EmitGru, GraCorCad, SemiCodEspReb, Empresa, FormulasGru: Integer;
  //CustoInsumo, CustoTotal: Double;
begin
  if UnPQx.ImpedePeloBalanco(TPDataB.Date) then Exit;
  //
  Codigo         := QrPQOCodigo.Value;
  Setor          := EdSetor.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //CustoInsumo    := ;
  //CustoTotal     := ;
  DataB          := Geral.FDT(TPDataB.Date, 1);
  EmitGru        := EdEmitGru.ValueVariant;
  if MyObjects.FIC(Setor = 0, EdSetor, 'Informe o setor!') then
    Exit;
  if Dmod.ObrigaInformarEmitGru(Obriga, EmitGru, EdEmitGru) then
    Exit;
  GraCorCad := EdGraCorCad.ValueVariant;
  SemiCodEspReb := EdSemiCodEspReb.ValueVariant;
  FormulasGru   := EdFormulasGru.ValueVariant;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('pqo', 'Codigo', ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pqo', False, [
  'Setor', (*'CustoInsumo', 'CustoTotal',*)
  'Nome', 'DataB', 'EmitGru',
  'GraCorCad', 'SemiCodEspReb', 'Empresa',
  'FormulasGru'], [
  'Codigo'], [
  Setor, (*CustoInsumo, CustoTotal,*)
  Nome, DataB, EmitGru,
  GraCorCad, SemiCodEspReb, Empresa,
  FormulasGru], [
  Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PQO', 'Codigo');
    AtualizaCusto;
    MostraEdicao(0, stLok, Codigo);
  end;
end;

procedure TFmPQO.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PQO', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PQO', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PQO', 'Codigo');
end;

procedure TFmPQO.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //
  TPDtCorrApo.Date  := 0;
  TPDataB.Date      := Date;
  PnEdit.Align      := alClient;
  PnDados.Align     := alClient;
  PnEdita.Align     := alClient;
  DBGIts.Align      := alClient;
  LaRegistro.Align  := alClient;
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSE, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEmitGru, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrRebaixe, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFormulasGru, Dmod.MyDB);
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  CriaOForm;
end;

procedure TFmPQO.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPQOCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPQO.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPQO.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmPQO.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQO.QrPQOAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPQO.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'PQO', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmPQO.QrPQOAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrPQOCodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrPQOCodigo.Value, False);
  ReopenPQOIts;
end;

procedure TFmPQO.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPQOCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PQO', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPQO.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQO.QrPQOBeforeOpen(DataSet: TDataSet);
begin
  QrPQOCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPQO.QrPQOAfterClose(DataSet: TDataSet);
begin
  QrPQOIts.Close;
end;

procedure TFmPQO.ReopenPQOIts;
begin
  QrPQOIts.Close;
  QrPQOIts.Params[0].AsInteger := QrPQOCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPQOIts, Dmod.MyDB);
  if FPQOIts <> 0 then QrPQOIts.Locate('OrigemCtrl', FPQOIts, []);
end;

procedure TFmPQO.EdPQChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQO.Estabaixa1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxQUI_RECEI_011_A, 'Outras Baixas de Insumos Qu�micos');
end;

procedure TFmPQO.EdCIChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQO.EdPesoAddExit(Sender: TObject);
begin

  CalculaSaldoFuturo;
end;

procedure TFmPQO.CalculaSaldoFuturo();
var
  CI, PQ: Integer;
  PesoAdd, SaldoFut: Double;
begin
  CI := Geral.IMV(EdCI.Text);
  PQ := Geral.IMV(EdPQ.Text);
(*
  QrSaldo.Close;
  QrSaldo.Params[0].AsInteger := CI;
  QrSaldo.Params[1].AsInteger := PQ;
  UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrSaldo, Dmod.MyDB, [
  'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO ',
  'FROM pqcli ',
  'WHERE CI=' + Geral.FF0(CI),
  'AND PQ=' + Geral.FF0(PQ),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  '']);
  //
  PesoAdd := Geral.DMV(EdPesoAdd.Text);
  SaldoFut := QrSaldoPeso.Value - PesoAdd;
  EdSaldoFut.Text := Geral.FFT(SaldoFut, 3, siNegativo);
end;

procedure TFmPQO.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmPQO.BtDesiste2Click(Sender: TObject);
begin
  AtualizaCusto;
  MostraEdicao(0, stLok, 0);
end;

procedure TFmPQO.BtConfirma2Click(Sender: TObject);
const
  Unitario = True;
var
  Controle: Integer;
  KG, RS, SF: Double;
  //
  DataX, DtCorrApo: String;
  OriCodi, OriCtrl, OriTipo, CliOrig, CliDest, Insumo, Empresa: Integer;
  Peso, Valor: Double;
begin
  if PnInsumos.Visible then
  begin
    if QrSaldo.RecordCount = 0 then
    begin
      Geral.MB_Erro('Insumo / cliente interno n�o definido!');
      Exit;
    end;
    SF := Geral.DMV(EdSaldoFut.Text);
    //
    if (SF < 0) and (EdCI.ValueVariant < 0) then
    begin
      Geral.MB_Erro('Quantidade insuficiente no estoque!');
      EdPesoAdd.SetFocus;
      Exit;
    end;
    KG      := Geral.DMV(EdPesoAdd.Text);
    RS      := KG * QrSaldoCUSTO.Value;
    CliOrig := EdCI.ValueVariant;
    CliDest := EdCI.ValueVariant;
    if MyObjects.FIC(KG <= 0, EdPesoAdd, 'Quantidade inv�lida!') then Exit;
    //Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'EmitIts',
      //'EmitIts', 'Controle');
    //
    if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
    //
    Controle := DModG.BuscaProximoInteiro(CO_FLD_TAB_PQX, 'OrigemCtrl', 'Tipo', VAR_FATID_0190);
    //
(*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO p q x SET Tipo=' + Geral.FF0(VAR_FATID_0190) + ', DataX=:P0, ');
    Dmod.QrUpd.SQL.Add('CliOrig=:P1, CliDest=:P2, Insumo=:P3, Peso=:P4, ');
    Dmod.QrUpd.SQL.Add('Valor=:P5, OrigemCodi=:P6, OrigemCtrl=:P7');
    Dmod.QrUpd.Params[00].AsString  := Geral.FDT(QrPQODataB.Value, 1);
    Dmod.QrUpd.Params[01].AsInteger := CliOrig;
    Dmod.QrUpd.Params[02].AsInteger := CliDest;
    Dmod.QrUpd.Params[03].AsInteger := QrSaldoPQ.Value;
    Dmod.QrUpd.Params[04].AsFloat   := -KG;
    Dmod.QrUpd.Params[05].AsFloat   := -RS;
    Dmod.QrUpd.Params[06].AsInteger := QrPQOCodigo.Value;
    Dmod.QrUpd.Params[07].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
*)
    DataX      := Geral.FDT(QrPQODataB.Value, 1);
    OriCodi    := QrPQOCodigo.Value;
    OriCtrl    := Controle;
    OriTipo    := VAR_FATID_0190;
    CliOrig    := EdCI.ValueVariant;
    CliDest    := EdCI.ValueVariant;
    Insumo     := QrSaldoPQ.Value;
    Peso       := -KG;
    Valor      := -RS;
    DtCorrApo  := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
    //
    PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
      OriCtrl, OriTipo, Unitario, DtCorrApo, Empresa);
    //
    FPQOIts := Controle;
    UnPQx.AtualizaEstoquePQ(QrSaldoCI.Value, QrSaldoPQ.Value, Empresa, aeMsg, '');
    QrSaldo.Close;
    UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
    ReopenPQOIts;
    AtualizaCusto;
    EdPesoAdd.Text := '';
    EdPQ.Text := '';
    CBPQ.KeyValue := NULL;
    EdPQ.SetFocus;
  end;
end;

procedure TFmPQO.IncluiNovaBaixa1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmPQO.IncluiInsumobaixaatual1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmPQO.Alteraitematual1Click(Sender: TObject);
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQODataB.Value) then Exit;
  // 2023-09-09
  if DBCheck.CriaFm(TFmPQOEdit, FmPQOEdit, afmoNegarComAviso) then
  begin
    FmPQOEdit.EdPeso.ValueVariant  := -QrPQOItsPeso.Value;
    FmPQOEdit.EdValor.ValueVariant := -QrPQOItsValor.Value;
    //
    FmPQOEdit.FEmpresa    := QrPQOItsEmpresa.Value;
    FmPQOEdit.FInsumo     := QrPQOItsInsumo.Value;
    FmPQOEdit.FCliOrig    := QrPQOItsCliOrig.Value;
    FmPQOEdit.FCliDest    := QrPQOItsCliDest.Value;
    FmPQOEdit.FDataX      := QrPQOItsDataX.Value;
    FmPQOEdit.FCliInt     := QrPQOItsCliOrig.Value;
    FmPQOEdit.FOrigemCodi := QrPQOItsOrigemCodi.Value;
    FmPQOEdit.FOrigemCtrl := QrPQOItsOrigemCtrl.Value;
    FmPQOEdit.FTipo       := QrPQOItsTipo.Value;
    //
    FmPQOEdit.ShowModal;
    FmPQOEdit.Destroy;
    //
    QrPQOIts.Close;
    UnDmkDAC_PF.AbreQuery(QrPQOIts, Dmod.MyDB);
  end;
end;

procedure TFmPQO.AtualizaCusto;
begin
  LocCod(QrPQOCodigo.Value, QrPQOCodigo.Value);
  //
  QrSumPQ.Close;
  QrSumPQ.Params[0].AsInteger := QrPQOCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSumPQ, Dmod.MyDB);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE pqo SET CustoInsumo=:P0, ');
  Dmod.QrUpd.SQL.Add('CustoTotal=:P1 WHERE Codigo=:Pa ');
  Dmod.QrUpd.Params[00].AsFloat   := QrSumPQCUSTO.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPQCUSTO.Value;
  Dmod.QrUpd.Params[02].AsInteger := QrPQOCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  LocCod(QrPQOCodigo.Value, QrPQOCodigo.Value);
end;

procedure TFmPQO.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmPQO.PMAlteraPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPQO.State <> dsInactive) and (QrPQO.RecordCount > 0);
  Enab2 := (QrPQOIts.State <> dsInactive) and (QrPQOIts.RecordCount > 0);
  //
  Alteraitematual1.Enabled := Enab and Enab2;
  AlteraBaixa1.Enabled     := Enab;
end;

procedure TFmPQO.PMExcluiPopup(Sender: TObject);
begin
  ExcluiBaixa1.Enabled       := not Geral.IntToBool_0(QrPQOIts.RecordCount);
  ExcluiItemdabaixa1.Enabled := Geral.IntToBool_0(QrPQOIts.RecordCount);
end;

procedure TFmPQO.ExcluiBaixa1Click(Sender: TObject);
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQODataB.Value) then Exit;
  // 2023-09-09
  if Geral.MB_Pergunta('Confirma a exclus�o de TODOS ITENS '+
  'desta baixa e a pr�pria baixa?')= ID_YES then
  begin
    QrPQOIts.First;
    while not QrPQOIts.Eof do
    begin
      ExcluiItemSelecionado(False);
      QrPQOIts.Next;
    end;
    //AtualizaCusto;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM pqo WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrPQOCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    LocCod(QrPQOCodigo.Value, QrPQOCodigo.Value);
  end;
end;

procedure TFmPQO.ExcluiItemdabaixa1Click(Sender: TObject);
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQODataB.Value) then Exit;
  // 2023-09-09
  if Geral.MB_Pergunta('Confirma a exclus�o do item selecionado '+
  'nesta baixa?')= ID_YES then
     ExcluiItemSelecionado(True);
end;

procedure TFmPQO.ExcluiItemSelecionado(AtzCusto: Boolean);
var
  Atual, OriCodi, OriCtrl, OriTipo, Insumo, CliInt, Empresa: Integer;
begin
  Insumo  := QrPQOItsInsumo.Value;
  CliInt  := QrPQOItsCliOrig.Value;
  Empresa := QrPQOItsEmpresa.Value;
  Atual := QrPQOItsOrigemCtrl.Value;
(*
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM p q x WHERE Tipo=' + Geral.FF0(VAR_FATID_0190) + ' AND OrigemCtrl=:P0');
  Dmod.QrUpd.SQL.Add('AND OrigemCodi=:P1');
  Dmod.QrUpd.Params[00].AsInteger := Atual;
  Dmod.QrUpd.Params[01].AsInteger := QrPQOCodigo.Value;
  Dmod.QrUpd.ExecSQL;
*)
    OriCodi := QrPQOCodigo.Value;
    OriCtrl := Atual;
    OriTipo := VAR_FATID_0190;
    PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, True, Empresa);
    //
  if AtzCusto then
    AtualizaCusto;
  UnPQx.AtualizaEstoquePQ(CliInt, Insumo, Empresa, aeNenhum, '');
  //FPQOIts := UMyMod.ProximoRegistro(QrPQOIts, 'OrigemCtrl', Atual);
  //
end;

procedure TFmPQO.AlteraBaixa1Click(Sender: TObject);
var
  PQO : Integer;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQODataB.Value) then Exit;
  // 2023-09-09
  PQO := QrPQOCodigo.Value;
  if not UMyMod.SelLockY(PQO, Dmod.MyDB, 'PQO', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PQO, Dmod.MyDB, 'PQO', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPQO.Outros1Click(Sender: TObject);
begin
  FmPrincipal.Relatrios1Click(Self);
end;

procedure TFmPQO.SbFormulasGruClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  App_Jan.MostraFormFormulasGru();
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdFormulasGru, CBFormulasGru, QrFormulasGru, VAR_CADASTRO);
    //
    EdFormulasGru.SetFocus;
  end;
  App_Jan.MostraFormFormulasGru();
end;

procedure TFmPQO.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmPQO.frBaixaAtualUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  if AnsiCompareText(Name, 'VFR_GRD') = 0 then Val := 15//Geral.BoolToInt2(CkGrade.Checked, 15, 0)
end;

procedure TFmPQO.frxQUI_RECEI_011_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VFR_GRD' then Value := 15 else
  if VarName = 'VARF_DATA' then Value := Now() else
end;

end.

