object FmMPRecebImp: TFmMPRecebImp
  Left = 339
  Top = 185
  Caption = 'IMP-MPINN-001 :: Recebimento de Mat'#233'ria-Prima'
  ClientHeight = 341
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 528
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 375
        Height = 32
        Caption = 'Recebimento de Mat'#233'ria-Prima'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 375
        Height = 32
        Caption = 'Recebimento de Mat'#233'ria-Prima'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 375
        Height = 32
        Caption = 'Recebimento de Mat'#233'ria-Prima'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 624
    Height = 179
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 179
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 624
        Height = 179
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 620
          Height = 162
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label18: TLabel
            Left = 8
            Top = 0
            Width = 71
            Height = 13
            Caption = 'Cliente Interno:'
          end
          object Label11: TLabel
            Left = 8
            Top = 40
            Width = 63
            Height = 13
            Caption = 'Proced'#234'ncia:'
          end
          object Label10: TLabel
            Left = 224
            Top = 80
            Width = 18
            Height = 13
            Caption = 'OS:'
          end
          object Label19: TLabel
            Left = 292
            Top = 80
            Width = 17
            Height = 13
            Caption = 'NF:'
          end
          object Label12: TLabel
            Left = 360
            Top = 80
            Width = 24
            Height = 13
            Caption = 'Lote:'
          end
          object Label13: TLabel
            Left = 432
            Top = 80
            Width = 33
            Height = 13
            Caption = 'Marca:'
          end
          object Label1: TLabel
            Left = 8
            Top = 80
            Width = 57
            Height = 13
            Caption = 'Data Abate:'
          end
          object Label2: TLabel
            Left = 116
            Top = 80
            Width = 66
            Height = 13
            Caption = 'Data Entrada:'
          end
          object Label3: TLabel
            Left = 8
            Top = 120
            Width = 41
            Height = 13
            Caption = 'Qtd. Boi:'
          end
          object Label4: TLabel
            Left = 92
            Top = 120
            Width = 51
            Height = 13
            Caption = 'Qtd. Vaca:'
          end
          object Label5: TLabel
            Left = 176
            Top = 120
            Width = 77
            Height = 13
            Caption = 'Qtd. Degolados:'
          end
          object Label6: TLabel
            Left = 260
            Top = 120
            Width = 75
            Height = 13
            Caption = 'Total avaliados:'
          end
          object Label7: TLabel
            Left = 344
            Top = 120
            Width = 65
            Height = 13
            Caption = 'N'#186' Caminh'#227'o:'
          end
          object EdClienteI: TdmkEditCB
            Left = 8
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBClienteI
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBClienteI: TdmkDBLookupComboBox
            Left = 64
            Top = 16
            Width = 489
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMECLIENTEI'
            ListSource = DsClientesI
            TabOrder = 1
            dmkEditCB = EdClienteI
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdProcedencia: TdmkEditCB
            Left = 8
            Top = 56
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBProcedencia
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBProcedencia: TdmkDBLookupComboBox
            Left = 64
            Top = 56
            Width = 489
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMECLIENTEI'
            ListSource = DsProcedencias
            TabOrder = 3
            dmkEditCB = EdProcedencia
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdOS: TdmkEdit
            Left = 224
            Top = 96
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNF: TdmkEdit
            Left = 292
            Top = 96
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdLote: TEdit
            Left = 360
            Top = 96
            Width = 68
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 8
          end
          object EdMarca: TEdit
            Left = 432
            Top = 96
            Width = 121
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 9
          end
          object TPIni: TdmkEditDateTimePicker
            Left = 8
            Top = 96
            Width = 105
            Height = 21
            Date = 38547.409162858800000000
            Time = 38547.409162858800000000
            TabOrder = 4
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPFim: TdmkEditDateTimePicker
            Left = 116
            Top = 96
            Width = 105
            Height = 21
            Date = 38547.409162858800000000
            Time = 38547.409162858800000000
            TabOrder = 5
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object EdQtdB: TdmkEdit
            Left = 8
            Top = 136
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdQtdV: TdmkEdit
            Left = 92
            Top = 136
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdQtdD: TdmkEdit
            Left = 176
            Top = 136
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdQtdT: TdmkEdit
            Left = 260
            Top = 136
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 13
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCaminhao: TdmkEdit
            Left = 344
            Top = 136
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 14
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 227
    Width = 624
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 271
    Width = 624
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 478
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 476
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object frxIMP_MPINN_001_001_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41207.355384305600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  if <PictureTitL1_Existe> = True then'
      '    PictureTitL1.LoadFromFile(<PictureTitL1_Load>);'
      'end.')
    OnGetValue = frxIMP_MPINN_001_001_AGetValue
    Left = 56
    Top = 80
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 154.960720240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Rich1: TfrxRichView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 113.385826770000000000
          DataField = 'TxtTitRelC'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C205461686F6D613B7D7D0D0A7B5C2A5C67656E657261746F7220
            52696368656432302031302E302E31383336327D5C766965776B696E64345C75
            6331200D0A5C706172645C66305C667331365C7061720D0A7D0D0A00}
        end
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 151.181200000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Line_PH_01: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 132.283550000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RECEBIMENTO DE MAT'#201'RIA-PRIMA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 18.897650000000000000
          Top = 132.283550000000000000
          Width = 132.283486540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 529.134200000000000000
          Top = 132.283550000000000000
          Width = 132.283486540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLI_INT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object PictureTitR1: TfrxPictureView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 113.385802360000000000
          Height = 113.385826770000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Stretched = False
          HightQuality = True
          Transparent = True
          TransparentColor = clWhite
        end
        object PictureTitL1: TfrxPictureView
          Top = 18.897650000000000000
          Width = 113.385802360000000000
          Height = 113.385826770000000000
          Center = True
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HightQuality = True
          Transparent = True
          TransparentColor = clWhite
        end
        object Memo3: TfrxMemoView
          Left = 113.385900000000000000
          Top = 41.574830000000000000
          Width = 453.543600000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_FORNECE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 113.385900000000000000
          Top = 64.252010000000000000
          Width = 302.362400000000000000
          Height = 30.236240000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  [VARF_MARCA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 113.385900000000000000
          Top = 94.488250000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Abate: [VARF_DATAI]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 226.771800000000000000
          Top = 94.488250000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Boi: [VARF_QTD_B]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 340.157700000000000000
          Top = 94.488250000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vaca: [VARF_QTD_V]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 453.543600000000000000
          Top = 94.488250000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'O.S.: [VARF_OS]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 113.385900000000000000
          Top = 113.385900000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NF: [VARF_NF]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 226.771800000000000000
          Top = 113.385900000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Degol.: [VARF_QTD_D]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 340.157700000000000000
          Top = 113.385900000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Avali.: [VARF_QTD_T]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 453.543600000000000000
          Top = 113.385900000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lote: [VARF_LOTE]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 340.157700000000000000
          Top = 64.252010000000000000
          Width = 226.771800000000000000
          Height = 30.236240000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_DATAF]  ')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 1186.772420000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 642.520100000000000000
        Top = 483.779840000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Picture1: TfrxPictureView
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 638.740570000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Picture.Data = {
            0A544A504547496D616765B5B30000FFD8FFE000104A46494600010101006000
            600000FFE1005A4578696600004D4D002A000000080004013100020000001400
            00003E511000010000000101000000511100040000000100001EC25112000400
            00000100001EC20000000041646F62652046697265776F726B732043533500FF
            DB00430002010102010102020202020202020305030303030306040403050706
            07070706070708090B0908080A0807070A0D0A0A0B0C0C0C0C07090E0F0D0C0E
            0B0C0C0CFFDB004301020202030303060303060C0807080C0C0C0C0C0C0C0C0C
            0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
            0C0C0C0C0C0C0C0C0CFFC0001108027D02A603012200021101031101FFC4001F
            0000010501010101010100000000000000000102030405060708090A0BFFC400
            B5100002010303020403050504040000017D0102030004110512213141061351
            6107227114328191A1082342B1C11552D1F02433627282090A161718191A2526
            2728292A3435363738393A434445464748494A535455565758595A6364656667
            68696A737475767778797A838485868788898A92939495969798999AA2A3A4A5
            A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DA
            E1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101
            010101010000000000000102030405060708090A0BFFC400B511000201020404
            0304070504040001027700010203110405213106124151076171132232810814
            4291A1B1C109233352F0156272D10A162434E125F11718191A262728292A3536
            3738393A434445464748494A535455565758595A636465666768696A73747576
            7778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2
            B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7
            E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00FDFCAF9F7FE0
            A01FF0539F847FF04D8F8791EB5F11F5E71AA6A20AE8BE1BD323177ADEBF3745
            8ADADC104E5B0BBDCAC60900B02467E62FDA83FE0B49E27F8FBF17350F81DFB1
            2F866D7E2EFC4AB6716FAE78D26E7C1FE0956254CB2DC0F92E245C1215495257
            03CD6063AF41FF00827FFF00C1167C3DFB37FC499FE317C60F136A1F1DFF0068
            9D642C97BE30F107EFA1D20E3FD469B037CB6F1283B55B1B80185F2D494A00F0
            3D33F636FDA4FF00E0B8FAAC3E26FDA3355F15FECE9F00CAFDA341F861E1BBEF
            B2788755CF31DC6AB3B21284707C974C82388E23976DE9BF635FDB97FE099B22
            DD7C07F8B36BFB4E7C35B224FF00C205F13245875EB5872311DA6A808F31C282
            0798F1C6BC6217E95FA7945007C21FB28FFC17FF00E12FC5FF008891FC38F8B5
            A66BBFB37FC5F8D8452F85FC7A9F6186E243C016D7AE1229431C04DFE5B4848D
            AAD5F77AB075041041E411C835E57FB5A7EC43F09FF6E8F8772785BE2C781740
            F1A69455840D7B6E3ED560CC3064B6B85C4B6EFF00ED44EA48E092322BE06BEF
            D88FF6AFFF00823B5EAEAFFB3478AB50FDA13E0959E5AEFE14F8CAF776B3A540
            3076E95798E7037623C0C703CA998E4007EA6D15F2BFFC13CBFE0AFF00F097FE
            0A266F742D0EE751F06FC4ED07726B9E02F1345F60D7B4B910E24FDCB63CE453
            D5E3CEDC8DE109DB5F5450014514500145145001451450014514500145145001
            4514500145145001451450014514500145145001451450014514500145145001
            4514500145145001451450014514500145145001451450014514500145145001
            4514500145145001451450014514500145145001451450014514500145145001
            45145001451450014514500145145001451450014515575BD72CBC33A3DD6A3A
            95E5AE9FA7D8C4D3DCDD5CCAB0C36F1A8CB3BBB10154019249C014016AB84FDA
            27F69EF87BFB24FC37BAF17FC4BF18683E0BF0ED983BAF353BA5844AC013E5C6
            BF7E59081C246198F606BE13F8C5FF0005B8F18FED5FF14356F853FB0E78023F
            8BBE25D325FB26B1F10B560F6DE09F0D31E0B79DC1BA75E480A406DB9413AE45
            6C7ECE1FF0405D075DF1FDA7C51FDABFC6DAB7ED39F174FEF41D73E4F0C68AC4
            EEF2ACF4E188F629C8F9D7CB6FBC228CD0072D73FF000596F8E5FF000509BE7D
            23F629F81D7FA9F876591E06F8A9F11EDE5D27C3298255A4B584112DCED3CF19
            75230D09154B54FF00837B3E227ED33A749E2DF8FF00FB5BFC5EF137C5D80ADE
            68379E1899348F0F7842F001B65B6B1500395618DE86DCBA93950C770FD3FD37
            4DB6D1B4EB7B3B3B782D2D2D6358A1821411C70A28C2AAA8E0280000070054D4
            01F979F0B3FE0AA1F17FFE096FE3ED1FE15FEDC9A74177E1AD46E469FE19F8DB
            A1DB48DA36AD9CF971EA6817F717180773055000C95650D31FD34F0B78AB4BF1
            C7872C759D1352B0D6348D4E15B9B3BEB1B84B8B6BB89865648E44255D48E432
            9208ACFF008A5F0A7C33F1BFC01AA7853C63A0693E27F0D6B509B7BED3353B54
            B9B5BA43D991C10707041EA080460806BF32FC5DFB027C7CFF00822B789756F1
            DFEC8D717BF14FE0A4F335F6BBF04F5ABD925B8B05249925D1AE1B732B0193E5
            F2E700159CED0A01FAA7457CE5FF0004F1FF0082A57C27FF0082957822EAF7C0
            9AACF63E26D1004D7FC27ABA0B5D73C3F2E76B2CF0139D9BF2A245CA13C64365
            41401F1037EC71F1D7FE0DF4D5B56F137ECE565A97C70FD9A353D45F55F117C3
            3B95127887C37BC012DD69D3AAEF9D55500D8416C01B958EE987E827EC31FB7F
            FC2EFF00828B7C1A8BC6DF0BBC409AB5846E20D42C275106A5A2DC6326DEEA0C
            9314839EE5580CAB30E6BDA2BF3EFF006EAFF8231EA173F1926FDA07F653F11D
            B7C18FDA0A073717EB182BE1FF001C2FDE7B7D42DC650348402650A72C496058
            891003F4128AF89FFE09B3FF000587D3BF6AEF1CDFFC1EF8B3E1AB8F837FB497
            85CB45ABF837520D1C5AA04524DDE9B231227819417003160BF303226243F6C5
            00145145007C9DFF000518FF00823D7C30FF0082851B5F124FF6EF87DF17B420
            B2E81F10BC36DF64D6B4D9907EEBCC742A6E225207C8E72A33B190926BE71F84
            5FF054AF8C1FF04C4F899A1FC24FDB92C2CE6F0EEAD21B2F0C7C6DD1E32746D5
            C8FB91EA51AA8FB34C4757217D4A940D357E9FD733F18BE0CF84FF00683F86FA
            B783FC71E1ED27C53E18D72136F7DA6EA56CB3DBCEA7D55BA303C86186520104
            100D006DE83AF58F8A744B3D4F4BBDB4D4B4DD4214B8B5BBB5996682E62750C9
            223A92ACAC0820824107356EBF26B5FF0081FF001C3FE0DE7D6EEBC47F086DFC
            41F1C3F6426964BDD6BC072B9B8D7FE1EC658B4971A7C8C4B4D6EB92ECA78003
            6F00E67AFD15FD90BF6CAF871FB75FC14D33C7FF000C3C4B63E24F0F6A4A3718
            982DC584B8CB417111F9A19973CA3007A119041201EA14514500145145001451
            4500145145001451450014514500145145001451450014514500145145001451
            4500145145001451450014514500145145001451450014514500145145001451
            4500145145001451450014514500145145001451450014514500145145001451
            4500145145001451450014514500145145001451450014515F0BFF00C144FF00
            E0B1A9F033E2A5A7C0AF809E1A1F1A3F695F10064B6F0ED9B16D3FC34B807ED5
            A9CCA42C68A0EE31EF56C0CBB460A9600F70FDBEFF00E0A47F0A3FE09B5F0B13
            C4DF1335FF00B2CF7E5A2D1B43B24FB46AFAFCE31FB9B580105CE59416255177
            0DCCB919F87FC35FB1EFED15FF0005CCBDB1F14FED2F77ACFC0DFD9E1E65BCD2
            BE1168B70D6FAB788E20C1A37D62E301D51B0A4C4403DD5216C487D97FE09F1F
            F04663F0B3E2A27C78FDA37C4EDF1BBF692D4503B6B17C3CCD2BC283395B7D32
            0202C613240976A91CEC58C16DDF79D00727F043E047837F66BF869A67837C03
            E1AD23C27E17D1E3F2ED34ED36DC430C63B938E598F52CC4B31E49279AEB28A2
            800A28ACBF1BF8DF46F869E0FD4FC43E22D574FD0F42D16DA4BDBFD42FA75B7B
            6B28114B3CB248C42AAAA824927000A00D4AFCFF00FDB9BFE0B3D79A67C649FE
            00FECAFE19B5F8DDFB41CE0C776B0CD9F0FF0082D780D71A85C29084A138312B
            A956E1995B08DE49E3BFDB0BE377FC1767C6BABFC3DFD992F355F851FB39584E
            6C3C4FF182E2168350F10E0E24B4D2233B5D411FF2D41071F78C608493EF1FD8
            57FE09F9F0BBFE09D1F0722F067C30F0FC7A5DB4A565D4F529F6CBA9EBB38047
            DA2EE7C032C872D8E02AEE2155471401E11FF04B9FF82425EFEC79F18BC5BF1D
            BE2B78E6EBE267ED17F12EC0D8F88F5B857ECDA5D9C0F2432BDA5AC01543207B
            784798EA1B1128558C160C57DC34500145145007CC1FF052EFF8254FC3FF00F8
            294781B4F3ABC973E12F88FE14905E7847C73A4831EABE1CBA560E8EACACA658
            83804C4CC0679528D861E03FB08FFC1527C7FF00013E3E69FF00B2F7ED916F6B
            E1DF8AF2A88FC21E3A8D563D0FE24C3BF6C651D4048AECE4029F2866F948490A
            A3FE8ED78A7EDEFF00B00FC37FF828F7C01BFF0087DF12748FB6D8CADF68D3F5
            08088EFF0044BA03E4B9B69719475EE3EEB0CAB02091401ED7457E687FC13EBF
            6E9F88DFB0D7ED1F61FB21FED5FAB7F696BB3AECF863F13674F22CFC7D681B11
            DACCCC485BD40550024B310158B394926FD2FA0028A28A004650EA5580656182
            08C835F9B5FB607FC1273C7BFB2AFC6CD53F68CFD896EB4FF0A78FEE53CDF157
            C36982C5E1AF1F44A4B3058B2A96F747270C0AA96390D1B1667FD26A2803E53F
            F826CFFC15BBC03FF0511D3F53D096CEF7E1E7C5DF09B183C53F0FB5E7116AFA
            3CCBC394560A6E200DC79AA83195DEA85803F5657C7DFF000526FF00823AF823
            F6F5BFB3F1C685AB6A3F0A3E3AF8702C9E1FF885E1E1E4EA16EE8311C77214A9
            B9840E36960CA0E1580254F85FC1AFF82C7FC46FD84BE26D87C24FDBB7C39078
            4AFAE985BE83F16F46B62DE13F147500DC1450B6931C02C02AA8CE59225C1600
            FD35A2B3FC2BE2CD2BC77E1CB2D6743D4AC358D23528967B4BDB2B84B8B7BA8D
            8655D244255948E84120D6850014514500145145001451450014514500145145
            00145792FED37FB7A7C17FD8CB4A7BBF8A5F13FC17E082B179E96BA96A71A5F5
            C267198AD8133CBF48D18D7C777FFF000732FC23F88BAC5EE95F037E177C7EFD
            A1B51B55CA4BE0CF054EF639FF00A6924C5258D73C6E3091401FA3D457E70DBF
            FC14CFF6E9F8ABA19BBF04FEC0375A340ED88EE3C59F12F4EB2948FF006AD244
            8655FCEA3D2FE31FFC1547C576CF7B1FC22FD937C2B1E7E5D3F58D6352BBBB03
            D0C96D72D11FCC5007E91515F9B507EDC5FF00051AF8342E6E3C71FB1DFC3EF8
            8DA75AAEE79FC09E3A86C6523B9482E649E690FF00B2A99356741FF83967E167
            C3DD56CF49F8FDF0BBE38FECE1ABDC8F99FC5FE12B87D358F6F2A6855A4917FD
            AF2545007E8E515E49FB377EDEFF00053F6C0B38A5F863F14BC0DE359648FCD3
            69A6EAD0C97D0AFAC96C489A3FA3A29AF5BA0028A28A0028A28A0028A28A002B
            C3BFE0A3DFB717873FE09D7FB1C78CFE2B788E481C6836663D2EC5DC2BEADA8C
            9F25B5AA0EA4BC8467192A8AEDD14D7AB7C49F895E1FF83BE01D5FC55E2AD634
            FF000FF87340B57BDD4751BE9961B7B3850659DD8F000FD4E00E4D7E5CFC04D2
            35DFF8381FF6DBD13E33F8974ABAD37F646F82BA9B4FF0FB47D42168CFC40D62
            362A7549E16EB044CA42AB0C70130774E28036BE067ECD1FF0530F8DBF07FC39
            E31D77F6B3F087C3CD53C51631EA937865FE18E99752687E70DEB6CF2345B8BA
            2B28607255B20962327AB3FB0C7FC146B8C7EDCBE0CFFC34FA57FF001AAFD1CA
            2803F393FE1867FE0A35FF0047CBE0CFFC34FA57FF001BA3FE1867FE0A35FF00
            47CBE0CFFC34FA57FF001BAFD1BA2803F2CFC17FB4D7ED47FF0004CDFF00828A
            7C2AF06FED4BF18B45F8B3F07FE395B5C787F47F10D9F85ECB4283C3DE215746
            8229FC88836255C46BBDF6B1999B0042E6BF532BC43FE0A29FB0A7857FE0A39F
            B26789BE1678ACB5A45ABC62E34CD4E38F7CFA26A11E4DBDE45C8F9918F2011B
            919D72031AF987FE0937FF000521F13E91E3CB8FD943F69890F87FF686F00A7D
            934CD4AF5825A7C48D3914F937D672B63CE9BCB4CC8A065B696FBC2558C03F43
            68A28A0028A28A0028A28A0028A2A1D4752B7D1EC26BBBBB882D6D6D90C92CD3
            3848E250325998F0001DCD004D457C6FFB46FF00C1C01FB227ECC93C969ACFC6
            9F0CEBFAB2A9D9A7F857CCF104F238E3CBDD68B2471BE78C48E95E4365FF0005
            D4F8BFFB425C41FF000A07F622F8E1E36D2EF9336BAD78BE683C21A7CDFEDA49
            2ACB1BA77CF98A4FB5007E93515F9B9A7FC74FF82A5F8DEE1A6B4F823FB2EF82
            2D9CE52DFC41E20BDD4268C7A33D9DC9527DC014D83F6A6FF829DF81B54316B3
            FB2F7C0BF1DDBC6D932F86BC6ABA52C83D17EDB72CC0FB95FC2803F4968AFCE1
            BDFF0082D5FED05F0835768FE2B7EC05F1C744D3A05DD7179E0BD52DFC662350
            396FF478A38F1FF6D062BB2F809FF07227EC93F1BB5A8F46D47E215CFC31F123
            398E4D23C79A64DA1CB6C476927706D53E866CD007DD94566783FC6BA37C43F0
            DDA6B3E1FD5B4CD7747BF4125B5F69F749756D70A7A324884AB0F704D69D0014
            514500145145001451450014514500145145001515FDFC1A558CD75753C36D6D
            6C8D2CB2CAE1238900C96663C0007249E95E1DFB757FC149FE0E7FC1397C0035
            DF8A7E2EB3D227BA42DA6E8B6E45CEB1ACB038D96D6AA77C9F3606E3845246E6
            5EB5F0BC7F027F695FF82F7DE45A87C5C8B5CFD9BBF6559E7135AF81AD6E0C5E
            2BF1CDBA9051B5070079103F5313018E308F849E80377F683FF829AFC52FF829
            F7C57D5FE05FEC472C76FA3E9971F61F1BFC6AB989FF00B23C3D1FFCB4874D70
            3F7F744676B8C83D530A44E9F5B7FC1393FE0985F0D3FE099DF0B6E344F05DAD
            CEA9E23D724FB57893C57AA379FAC7892E892CD2CF29E42EE24AC6B855C93CB1
            666F59F805FB3DF827F65AF855A5781FE1E786B4BF097853448FCAB3D3AC22D9
            1A7AB313967763CB3B92CC7249279AECA800A28A2800A28AE07F69EFDA73C13F
            B1D7C0CF107C46F887AE5BF87FC29E1AB73717775265998F448A341CC92BB10A
            A8B92CC401400BFB4CFED39E06FD8F7E0B6B5F107E237886C7C33E14D062F32E
            6EEE5B1BD8F09146BD6495DBE554505989C015F9A9E08F82BF163FE0E26F1A58
            F8DFE2E5AF88FE137EC7BA75C8B9F0D78044CD67ACFC43D8D98AF7512BF72DDB
            82101C118F2FB4E6F7ECB7FB2D78DFFE0B9DF1B348FDA37F68FD22F742F813A1
            4E6EBE15FC29B96FDC6A71E418B58D52339594C8872A87EF718FDD7137EAC410
            A5B429144891C71A855551855006000074140195E00F87FA17C29F04E95E1AF0
            CE8FA6E81E1FD0ED92CF4FD3B4FB75B7B5B285061638E35015540EC056BD1450
            01451450014514500145145007827FC146FF00E09E5E07FF008294FECE3A8780
            BC6309B4BC8DBED9A06BD6E83EDFE1BBF51FBBBAB76E0820E0328203AE41C704
            7CF7FF00047BFDBE7C6DA978E7C51FB2B7ED1335BDA7ED0BF08618C457DB8F93
            E3BD236FEE752B7620798EA9E5F9A7827786C6EF3027DFF5F007FC17ABF601D7
            3E3F7C17D3FE367C29B7107C77F81B0CDAA688F0AB0935DD3B637DB34C7D843B
            8689A478D41C97CA0C79AC6803EFFA2BE1BFF827E7C1DF843FB777ECAFE02F8C
            7E12D73C7F1D8789ED12EE4B393C40CEFA75DC6E52E2D64C28CB45323A678DC1
            430E1857D29F1CFF0064AF0DFED03AA69F79ACEA3E26B3974D83ECF10D37526B
            6465C9396001CB64F5A00F50A2BC6FE10FEC3DE13F82DE38B6F10695AAF8BEEA
            F6D51D163BED59A7808618394C0CD52F885FB007837E2578D752D7AFF58F1AC1
            79AA4CD3CB1DAEB2F142AC7B2AE0ED1ED401EE35CE7C5AF83DE14F8F5E00D47C
            29E36F0E68BE2BF0DEAD1F9777A6EAB689756D38ED94704641E41EA0E08208CD
            72DF087F655F0F7C16F0DEBBA5695A87892EADFC41198AE1AFB5269E48C142B9
            8DB0361C31E477C7A5702DFF0004C9F01331275DF88193CFFC879FFF0089A00F
            95F57FF821BFC4CFD88FC477DE22FD88FE39EADF0C2C6EA66B99FE1C78BCC9AD
            F846E9D8824445F7CB6F9031BF6C921E82451525A7FC1613F69FFD9308B2FDA5
            3F636F1DDED9DAA33CFE2CF84B22F88F4E9514E3CC36BBCB40BDFF007B38383F
            7474AFB8F59FD95BC3DAE7C12B2F014DA87891347B091648E78F5265BD620B10
            1A5C648F98F18C74F4AE16C3FE09A9E04D3AFE0B88F5CF1F33DBC8B2286D75C8
            241C8CFCBD2803C77E027FC1C89FB1DFC7ABAB7B1FF85B369E07D7253B66D33C
            636171A24966D9C6C927993ECA1BFDD98E2BEC1F85FF001A7C1BF1BF433AA782
            FC5BE19F17E98081F6BD13548350839FF6E2665FD6B03E36FEC87F0A7F696B28
            ADFE22FC36F02F8F12088C3136BFA15AEA3242A463E47951990FBA90735F0E7E
            D6DFF06DEFEC83A7FC26F16F8B7C39F0DB51F04788B45D32E752B7BCF0EF8975
            1B32B245133AA88DA67895491C854071D08A00FD28A2BF2EBE18FF00C10B7C6D
            2FC19F076AFF000CBF6D5FDA9FC04BA9E9169A8369DA87890EB9A6DAB4B0A485
            21809802A02D8018B1C0E49AEC57FE09BFFB77F827495B7F0E7FC140FF00B4D1
            3EEC7AE7C23D2657FC67692591BF1A00FD14A2BF3647EC5BFF00052C32EC3FB6
            6FC3558B38F307C36B02F8F5DBF67C67F1AD2FF87717EDE3E36D2A4B5F11FF00
            C140534B865E19343F847A4C5211ED3ABC5221FA5007E8A578AFED2FFF00051E
            F80DFB1DC1747E267C5BF02784AEECD43C9A75D6AB13EA447FB167196B87FF00
            80C66BE435FF0083772F3E2BE9325A7C6AFDAEFF006A4F8A3693306974D8FC51
            FD97A4CE3B87B522607FE02CB8AF6AFD9D3FE0833FB23FECBD731DDF873E0878
            4750D4E300FDBFC44926BF71BC1CF98A6F5A558DF3CE630BED8A00F11BFF00F8
            387E7FDA3F527D23F64CFD9D3E2EFED05786736DFDBF3591F0EF85A16C70CD7D
            3A9DA73CEC9521C8E8DE9560FD8B7F6F9FDBD0A4FF001B3E3CE85FB3AF83EEF0
            D2784FE14405F57D9D0C726A4EC5A3723A98E5950FF77B57E97D8D8C1A5D9456
            D6B0C56D6D0208E28A240891A8180A00E0003B0A96803E26FD99BFE0DE8FD94F
            F669D446ACFF000E2DFE23F8A5D8BDC6BDE3D9CF886EEEA4273E634730FB3ABE
            79DC90A9F7CD7DA3A469169A06996F63616B6D63656A82386DEDE358A2850745
            55500003D0558A2800A28A2800AABADE8765E26D22E74FD4ACED750B0BC8CC53
            DB5CC4B2C33A1EAAC8C08607D08C55AA2803E2FF00DA57FE0DF0FD91BF69FB89
            AF752F841A1F8535C75C45AAF83DDFC3F716EF9C894476C5207901E774913E7B
            E6BC7C7FC11FFF006AAFD93FCD97F673FDB4FC657BA543B12D7C2FF15EC53C45
            68231FF2CFED8559E151E90DBA9C77AFD30A2803F3625FDBF7F6FCFD958CC9F1
            6BF648F0DFC5DD26DDD63FEDDF849E226DEE07575D3EE3CDBA909FF76300D697
            873FE0E78FD9CB48D7DB45F8A9A47C5FF80FADC40092CFC75E0ABBB770FDC016
            C276C67BB2AFE15FA07E33D79BC2BE0ED5B544884EFA6D9CD74B1938121442DB
            7F1C62B8FF00839E2FD3FF006A2FD9F746D6B5CD134EB8B2F11DB17B8D3AEA25
            B9B7387652A55C10C3E5EE2803CBBE197FC163BF654F8BBA625DE8BFB42FC24D
            921C2C3A87892DB4CB93FF006C6E5A393FF1DAF4BD33F6C8F843AD43E6D97C55
            F86F7718FE387C4D6522FE624AF38F897FF047EFD963E2EC322EBBFB3E7C239A
            4973BE7B4F0CDAD85C367A932C08927FE3D5E5377FF06D87EC477B26F7F811A4
            29FF00A67AF6AD18FC96E80A00F7AF1EFF00C14A3F677F85F6B34DE21F8EBF08
            7481029668EE3C5D60B29C7658FCDDEC7D94126BE51F899FF072C7C1BD7FC40F
            E13FD9EBC2BF11BF69CF88532B7D9F49F07683731D9C2C0E375C5D4D1AF9710E
            A658E39540EB81C8F5EF865FF0412FD8EBE12EA0B75A4FECFBE00BA953A0D62D
            E5D6907BECBC79573EF8AFA87C01F0DBC39F09FC33068BE15D0344F0D68D6BC4
            361A558C5656B17FBB1C6AAA3F01401F9ADA0FFC1327F683FF0082ADFC42D1FC
            5FFB6B6B1A6F84FE1868F742FF004AF829E15BD66B599C1CA36AB771B7EF9970
            0ED476EA769881656FD33F09F84F4BF01F85F4ED0F44D3ACB48D1B48B68ECEC6
            CAD21586DED218D42A471A280155540000180056851400514514005145140057
            CE7FF0515FF82617C36FF8293FC3FB0B0F17C5A8687E2BF0DC86EBC31E30D124
            16DADF86EE32183C13633B0B2A968CF0DB41F9582B0FA328A00FCBAD0FF6BAFD
            B33FE09316ABA1FC79F875AA7ED49F0A74DCA5AFC49F01401BC476B6EB9C1D43
            4E3CC8CAA065F2A060969A5624D7B8FECF9FF070E7EC7BFB4508A0B2F8D1E1DF
            09EA8C81A7D3FC5EB2787A5B56EF1B4974A90338E988E461EE6BED4AF2AF8EDF
            B0AFC15FDA7EEE4B9F88BF097E1CF8DEF648FC9FB66B5E1DB4BCBB45F4599D0C
            8B8EDB58628026D0BF6DCF82FE28B613E99F177E186A30B0C892D7C53633291E
            B95948AA5E26FF008280FC06F056EFED9F8DBF08B48DBD7EDBE30D3ADF1FF7DC
            C2BE78D63FE0DB3FD88F5DBB334FF017448DC92716FAD6AB6C9CFF00B31DD2AF
            E95A1E12FF0083763F62BF055E24F67F007C2933C67205FDE5F6A087EAB3CEEA
            7F11400FF8BBFF00070DFEC63F056EE6B6D53E3D784F53B88895DBE1F86EB5D5
            91BD164B28A58CE7D7763DEBC993FE0E25BCF8E76ABFF0CF5FB287ED17F18FCC
            98C29A9DC68A342D09CE383F6D6F382E7AFCE89815F6C7C22FD883E0BFECFF00
            A9C57DE04F845F0CBC197D00C25D687E17B1D3E75E31FEB22895B3EF9ABD63F1
            C25BDFDA96FBE1E0B18C4165E1C4D6CDDEF3BCBB4E22F2F1D31839CD007C3223
            FF00829E7ED72C15DFE067ECA9E1F9A5C12A3FE12AF114311FFBFB68E40FFAE4
            73E952697FF06DC784FE32DFD96A7FB4BFC73F8E3FB486A16B234ADA7EB3E209
            74CD0013FF003CACE0632423DA39C0F6AFD25A2803C6BF669FF82787C0BFD8EE
            D6DD3E197C27F02783AE6DA3F296FEC7498BFB4645F492ED819E4FABBB1AF65A
            28A0028A28A002B81F8F5FB2AFC32FDA9B401A5FC49F87FE0EF1DD8A232451EB
            9A4417C6DC30E4C6D22968DBFDA42083C835DF51401F9BFE2EFF00836EFC0FF0
            AFC4179E24FD983E2CFC58FD987C4B72CB2183C3FAD4DA8E8374EA739B8B29E4
            DF3027F80CDB07F731C566BFED0DFF000518FD8390A7C40F851E04FDABFC1D64
            097D73C0975FD8FE2329BB8692C9936CB263FE59DBC047FB7DEBF4CA8A00F817
            E08FFC1C99FB317C45F169F0AF8E359F14FC09F1BC52F913E81F123439B459AD
            DB1C979FE7B78D73C7EF65427D057DB5F0D3E2D7853E3478622D6FC1DE26F0F7
            8B34598E23BFD1B5186FED643ED244CCA7F03599F1B7F673F87FFB4AF861345F
            889E07F0978EB48898BC769AFE9306A30C4C460BA2CCAC15B1FC4B83EF5F157C
            45FF0083633F656F10F8846B5E0AD27C77F06F5E0E64FED1F0278AAEAC250D9C
            8DAB319A38C03D0468A05007E84515F9BD6DFF000453FDA27E1B6B067F879FF0
            505F8E7A6DA45C41078BB4C87C5850760C679D11BFEF81F4A9F56FD867FE0A31
            A549B74AFDB93C1BABC43A36A1F09F4BB46FCA38DFF9D007E8D515F9C1A67EC4
            7FF0521D46E963D47F6DAF00E976E7ACB63F0BB4DBA907FC0648507EB4BE22FF
            008240FED65F12AE50F8B3FE0A21F1264833974F0DF81ECFC38E7D70D6D7200F
            FBE6803F47ABC5FE3B7FC146FE01FECC6D79178FBE327C36F0B5ED8AEE9AC2F7
            C416CB7E3D85B073331F65426BF3E7C67FF0411F01F8C3F6B3F07781FE2B7C62
            FDA37E33DA6AFA35C6A577278A7C7534A8648CB0D91A2202919DBD3793CFDEAF
            AD3E08FF00C105BF63EFD9F6512F87FE017812EE618C49AFC12F881D48EEA6FD
            E6DA7DD71401E3FE26FF00839BBE06F89F5EB8D0FE0AF82FE357ED11AF468C63
            83C0DE0DB99600E3A091E711C8ABEAEB1381D79AC5B8F88DFF00051EFF008283
            42B6FE1DF04F807F636F035F2217D4F5FBF5F1278B248DB218C30A288A26C73B
            268A17538C495F777C4EFD96BC29F137C11A57870C57BE1DD1F466DD696DA0CA
            34E8E218C6D0A8BB428EC00E2B8FF09FFC13D7C17E0DF1358EAB6BACF8DE5B8D
            3E659E349B5A778D994E4061B791ED401E43FB147FC10B3E10FECA1F11CFC49F
            145DF88BE377C67B8713DCF8E7C7774752BE49801F3DB46F948318F95BE69147
            1E611C57DA95E4BF19BF635F0C7C73F179D6F57D4FC556976625876586A8D6F0
            E14601DA0119A9BE097EC83E1AF80BE279B56D1F52F145DDCCD09819350D4DAE
            620A4E721481CFBD007AA515E09AF7FC13ABC13E22D6EEEFE7D6BC74935E4AD3
            3AC7ADBAA02C72401B7815D97837F65AF0FF0081FE156ADE0FB4D43C472E99AC
            92669A7D45A4BA4CE3EE498CAF4EC2803D268AF9E3FE1DA1E04FFA0E78FBFF00
            07ADFF00C4D77DF113F65AF0FF00C4CF87DA2F86AFF50F11C161A1002DE4B5D4
            5A2B8930B8FDE3E0EFE077A00EF3C49E23D3FC1DE1DD4357D5AF2DB4ED2F4AB6
            92F2F2EEE2411C36B0C6A5DE4763C2AAA82493C000D7E51FC1BF066A9FF07137
            ED811FC56F19595E5A7EC7DF077589ADFC0BE1EB8DC91FC45D5606DADAA5CC64
            0DF6ABC855231D63EBE783CFFF00C14C7F676B4F8EDFB6A7C3DFD8C7E147883C
            60B7DE37D3DFC47F14B5697567B81E1EF0C24814C6A00DA26B861B407040DF00
            236CD91FACBF07BE11786FE017C2CF0FF82BC21A4DB687E18F0BD8C5A7699616
            E088EDA08D42AAF3C93DCB124924924924D007471C6B0C6A88AA888300018007
            A52D1450014514500145145001451450014514500145145007E657FC12A6C1FF
            0060DFF82B9FED3BFB2CC51CB07823C4F143F18BC090902386CEDEE9E3835082
            35FEE2DC3C71A01D16D18F5638FD35AFCD5FDAFA01E1BFF839BFF646D46C9FCB
            BAF137803C4BA3EA414F32DADBDB5E5CC4AC3D04CC48F71ED5FA554005145140
            051451400514514005735F19FC1A7E22FC22F14680B7515936B7A55CD88B897E
            E406489937B7B0CE4D74B5E77FB5C4AD0FECBBF105D1991D7C3F7A4107047EE5
            A803A3F849E136F00FC29F0C684D7115DB68BA4DAD819E3FB937950A26F1EC76
            E47D6BA1AE2BF66B91A5FD9D3C00EECCCCDE1CD38924E493F658F9AED6800A28
            A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A280333C6DA5C9AE
            783357B289774B796534083D4B46CA07E66B81FD8B3C0DAB7C35FD987C27A1EB
            B68F63AAE9D04A93C0C4168C99E461D3FD920FE35EA1202D1B053B588E0FA578
            FF00EC19F12F57F8B5FB2EF87B5AD7AF24BFD5E692EA2B99E4C6E90A5CCAAB9C
            7A2051F85007B0D1451400514514005145140051451400514514005145140051
            45140051451400578DF86BE1A6B76BFB74F897C5B3D93A68575E17834EB7B924
            6D7944CAECA075E306BD92BC63E1D7C4BD6B5CFDB63E2278667BD924D0B43D26
            C25B6B6206D86491159987B9C9A00F67A28A2800A28A2800A28A2800A28A2800
            A28A2800A28A2800A28A2800A28A2803CCFC4DF05DB5AFDAA3C33E3AFED3B589
            746D1EE34FFB0B7FAD98B963BC7B0DDCD7A657CF9F122EA54FF828EFC3884492
            089FC357E4A063B49FDE738FC2BE83A0028A28A0028A28A0028A28A002AAEBDA
            ED9F85F43BDD4F51B986CF4FD3A07BAB9B895B6C70448A59DD8F6014124FA0AB
            55E05FF0559D5EE741FF0082607ED197B69B85CDB7C33F11BC6CA7050FF665C7
            CDF875FC2803E54FF83773C1F79FB40685F18FF6BFF145AB0F147ED1DE2DBA7D
            20CD87934FF0FD84AD6D696CAC79003C72211D196DE13D857E9457CD5FF046CF
            0669BE02FF00824FFECE561A4C4915ACFF000F345D41C29C833DD59C5733B7D4
            CD3487EA6BE95A0028A28A0028A28A0028A28A0028A28A0028A28A0028A2BE0A
            FF0082B27FC151B5AF857E23B1FD9D7F679B55F1AFED3FF116236DA75A5A32C9
            07832D987CFA95F372B1EC8F73A2B74C0761B405700F3FF80D7F07EDDDFF0007
            217C43F883A647F6BF067ECB9E075F015BDF23E629B5EBB9A57B80BEA638A4BB
            8580E86253DC57E9AD7CEFFF0004BCFF00827AE85FF04D2FD93349F879A65FCD
            AF6B9713C9ABF89F5EB8C9B8D7F559F067B972792B901103648445C92DB98FD1
            14005145140051451400514514005731F1AD7437F845E255F133C91F879F4D9D
            75164C87580A10E4639CEDCD74F5E6FF00B62297FD95FE20850589D0AEB00727
            FD59A00EAFE16C5A541F0CBC391E82CEFA1A6976CBA733E4B35B8897CA273DF6
            6DADDAE2FF0066F52BFB3BF80810411E1CD3C107A8FF00468EBB4A0028A28A00
            28A28A0028A28A0028A28A0028A28A0028A28A0028A28A002BCD7F64DF83B1FC
            08F83B6FE1C8B56B6D692DEF2E6617306367EF252DB3827919C1AF4AAF9F7FE0
            9B92C87E0A7882091DDCDA78AF52846E392006438FD6803E82A28A2800A28A28
            00A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800AF35F87FF07A3F
            0D7ED23F103C64355B6BB93C4B058406C93064B110C3B3E7FF007F191ED5E955
            E05FB30DDBEA1FB597C7D9999995750D2A0504E40D96D22F1401EFB451450014
            51450014514500145145001451450014514500145145001451450079BF8AAD3C
            147F69DF0ACF7F2CCBE391A5DCA696809F2DADF932923A67AE33EF5E915F3EFC
            4E89CFFC1457E193856283C3DA88271C0F95EBE82A0028A28A0028A28A0028A2
            8A002B8FFDA17E105A7ED09F00BC73E01D4267B6B0F1C787EFFC3F732A0CB451
            DDDB4903B01DC85909AEC28A00FCFBFF00836DBE3E5C78CBFE09F117C24F1296
            B4F895FB3A6B17BE01F13E9F238692DCDBDC4BF66703AF97E4E2256E85ADE4C7
            02BF412BF31FFE0A4FF057C77FF04D2FDB3CFEDAFF0006743BBF13786358B58B
            4CF8D9E0DB104CDAA69D12E17588107CA66B7441B8F51B431F95E661F7A7ECA9
            FB56F80FF6D5F819A1FC45F871AF5AF887C2FAFC2248668CED9607FE386643F3
            452A1E191B041F6C1201E894514500145145001451450014515E21FB7C7FC141
            FE1C7FC1383E098F1BFC46BCD44C177769A7695A46956E2EB56D7AEDCFCB6D69
            09651248464F2CAA00E5871900F6FAE33E3D7ED15E04FD96FE1C5DF8BFE2378B
            BC3FE0AF0D58F12EA1ABDE25AC25882446A58E5E46C1DA8A0B31E0026BF3F9FF
            006C1FDBFF00F6FD66B5F83DF02F41FD98BC1B72CE83C59F14E7371AE1407878
            B4C54DD0C847F0CD0CB193FF002D0751D4FC0EFF00837A3C0BA87C49B5F88DFB
            4B78E3C57FB53FC49B76F3239FC5EE5740D3C93BB65BE9819A211824FEEE42F1
            F711A9E801C078B3FE0A97F1E3FE0AC3ADDC782BF627F09DDF857C0324E6D356
            F8DDE2DB26B7B1B68C6449FD996922EE9A5C636B30241382917120FAB7FE09B3
            FF0004A9F87DFF0004DAF07EA6FA3CFA878CBE22F8AE56BBF15F8EF5D3E7EB5E
            22B8762EECF231631C5B892230C79E58BBE5CFD29A2E8B67E1CD26DB4FD3AD2D
            6C2C2CA3586DEDADA258A18114615111400AA07000181566800A28A2800A28A2
            800A28A2800A28A2800AE53E3A78E62F867F077C4BE209EC62D4A1D234F96E5A
            D64C6CB80AA7E439CF07A57575C0FED4DE16D43C6FFB39F8CF48D2ADA4BCD475
            1D2A682DE04FBD2B95E147B9A00E8BE1878913C65F0D7C3BAC476B1D947AAE99
            6D78B6E98DB00922570831D9738FC2B72B98F823A25D786BE0C78434DBE85ADE
            F74FD16CEDAE226EB1C89022B29F704115D3D001451450014514500145145001
            45145001451450014514500145145004777722CED2598A3B8890B9541966C0CE
            00EE6BE4FF00839FB72FC35F00E9FAF58785FC0BF1485B0D6EEA4BF2BA209556
            F095F34644BC745F94E08AFAD2BC07F61A592D3C43F1A2D5D1D153E21EA53202
            0804395C11F82D0076DE32FDA7748F047C2BD27C5B75A1F8B67B2D60811DA5BE
            9A64BD8739FF00591EEF97A7AD71BA0FFC142FC2BE20D6AD2C63F0AFC46824BC
            9561579B422B1A1638058EF381EF5EF54500795FC6BFDAEFC3BF023C510E93AB
            E93E2BBD9E6844E25D3B4C3730804E305811CF1D2A3F835FB637867E38F8B868
            BA4E99E2AB4BA3134DBEFF004B6B7870BD46EC9E7DABD628A00F09F167FC1433
            C0FE0EF135F69575A678CE4B9D3E6682468746778CB29C12A73C8F7AEBBE1EFE
            D4BE19F899F0FB59F1369D06B91E9DA1026E56E34F78A6385DDF221E5B8F4AF4
            8A2803E7BFF879A7C34EF1F8B01FFB02CB5DEF8C3F6A9F09F817E16693E31D45
            B564D1B5A205B6CB091E7C9CFDE8C0CAF43D6BD1E8A00F09D07FE0A37F0C3C45
            ACDA585BDEEB82EAF25586357D22E146E638009DBC735D3FC65FDB0BC09F013C
            4B1691E25D42FAD6FA6844EA90E9F3CEBB09C03B9148EDD2BD3E8A00F2BF841F
            B677C3EF8E9E2C1A1F87355BCBAD4DA2698452E9F3C036A8C93B9D02FEB58FE2
            5FF82877C25F087886F74AD4BC49736D7DA7CCD04D1FF655DB85753820158883
            CF706BDB28A00F3CF01FED55E04F897E04D67C4DA3EB4F73A268009BFB86B2B8
            8CC002EECED640CDC0FE106B8D5FF82967C126CE3C6C38F5D22FC7FED0AF7474
            1229560194F504641AACFA158CB8DF6568D8F5894FF4A00E2BC51FB51F80FC17
            F0DF4BF176A7AFA5AF8775A20595D9B49D84C4E71F22A171D0F5515CBE8BFF00
            050AF83BE21D5ADAC6CFC670CD757722C5127F675E2EF6638032620073EA6BD7
            EE749B4BCB65826B5B79614FBB1BC6ACABF4078AAE9E12D2A3915D74BD395D0E
            548B64054FA8E28038AF8AFF00B59FC3CF81FE228F49F1578921D275096213AC
            2D6B3CA4A13C1CA230EDEB58FF00B2F78EBE1B7C50D7FC71E21F87D752DF4DA8
            EA318D62E1A3923496654214A8700E304F6AF50D4FC33A6EB7207BDD3EC6F1C0
            C069A049081E9C8AF06FD86F4A5D3BE217C6E68ADD6DA03E35B88A2448F62054
            C818038C73DA803E86A28A2800A28A2800A28A2800A28A2800A28A2800A28A28
            00A28A2800A28A2803CDBC5FF182DBC3FF00B4E7843C18FA3DBCF73AFE9D7775
            1EA271E65A8881250719C3639E6BD26BC5BE22FC38D6B57FDB73E1D7896DAC26
            9744D2348D42DEEEE87DC81DD18229F7248AF69A0028A28A0028A28A0028A28A
            0028A28A004745910AB2865618208C822BF35BF685FF008245FC4AFD8D3E386B
            1F1C7F61BD6B4BF0AEBDAD37DA3C51F0B35694A785BC5E412C5A15242DADC124
            8077220DC76BC40B06FD2AA2803E10FD8D7FE0BDFF000D3E3678E17E1AFC62D2
            754FD9CBE375A3082EFC27E33CDAC373213806D2F1D523991F8DA1B63367E50E
            30C7EEF560CA082083C823A1AF23FDAF7F610F845FB79F801BC35F16BC07A178
            CB4E45616F25D4463BCB027AB5BDCC6566858E064C6EB9C7391C57C4507FC127
            3F6A3FF8278C665FD91FF687B8F10F82ECB0D07C35F8ACA753B08A35C9F22D6F
            507990273854458877690F26803F4E68AFCDDF047FC1777C61FB3CFC40D0BC21
            FB60FECF9E2EF80936B77916976DE35B3B84D67C1D7174E0ED0F771F1006C7DD
            0F295192E554161FA410CC9710A491BAC91C8032B29CAB03D083DC5003A8A28A
            002BF2E7FE0B865BF669FF0082837ECAFF00B4CF8E3C35AA78C7E06FC30B8D43
            4DF11C5696A6F07856F2E936DAEACF0F395493CB6DFF00C2D6E807CEC80FEA35
            54D7B40B0F1568B77A66A96569A969B7F1341736B750ACD05C46C30C8E8C0AB2
            91C10410680307E0C7C6EF087ED15F0E34CF187813C49A3F8B3C31AC47E6D9EA
            5A65CADC4130EE372F460782A70CA410402315D4D7E6CFC5FF00F8219F89BF66
            1F88FA9FC4FF00D883E245C7C11F145FBFDA753F025FB3DDF827C46E39DAF6E7
            71B663920328654071188464D6B7ECD1FF0005DE8FC2DF15ACBE10FED6FE01D4
            7F671F8B333082D6F7506F33C27E246C851259DFE4A28663D199901F97CD2DF2
            800FD10A2911D6440CA432B0C820E4114B400514514005145140051451400514
            514005707FB5178CAFFE1EFECF1E31D6F4BB836BA8E99A5CD3DB4C00263900F9
            4E0FBD7795C7FC7FF87CDF15FE0BF897C36B790E9EDACD8BDB0B997FD5C24FF1
            37B5005BF831AEDDF8A3E0F784F53BF90CD7DA8E8D677570F8C6F91E04663F89
            26BA5AC4F869E1A3E0BF871E1FD18CF1DD1D274DB6B3F393EECBE5C4A9B87B1C
            67F1ADBA0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A
            002BCCFE04FC664F899E3DF88BA3AE956DA6B78475AFB0B4917DEBCCA93E6B7F
            B476D7A6578D7ECDFF000CB5AF02FC70F8C1A96A364F6DA7788F58B7BBD3A524
            1170A237DC47D0902803D968A28A0028A28A0028A28A0028A28A0028A28A0028
            A28A0028A28A0028A28A002BCE3F671F8BABF182D3C6570BA5DAE98341F14DF6
            8B987FE5EC41B3F7CDFED36EE7E95E8F5E39FB15FC36D6FE19F82BC5B0EBD64F
            6577AB78B350D4E24620978A429B1F8F5DA6803D8E8A28A0028A28A0028A28A0
            028A28A0028A28A0028A28A0028A28A0028A28A00F1CF889F1435BD0FF006D0F
            879E16B7BB31E85AE695A84F776FB41F3A48D0B21CF518C0AF63AF34F1B7C186
            F12FED35E09F1A8D4ED605F0DD8DEDB1B26FF5B73E7215DCBECB9C9AF4BA0028
            A28A0028A28A0028A28A0028A28A0028A28A0028AF32FDACBF6C8F867FB0DFC2
            5BAF1BFC53F17E95E11F0F5B12892DD39335E4B82443044B992694807091A938
            04E3009AF8013F6B4FDAF3FE0B2970F67F00FC3D79FB317C06BD5C37C48F15DA
            6FF136BF01C7CFA6D983888329387C90460ACEADF2500775FF00070AFED9BF0B
            F42FD903C5DFB3FDC595B7C48F8C7F17B4F3A1F85BC0DA728BBD4A3BC9F8B6D4
            248D72615824DB323360BB4602E46E2BF5EFEC29F097C43F013F628F845E07F1
            6DDADEF8A7C1FE0DD2746D5E6597CE57BBB7B38A2970FF00C603AB00C796001E
            F5E55FF04F5FF823D7C21FF82774D7DAF68567A8F8C7E266BA5A4D6FC77E279B
            EDFAEEA723E4C844ADFEA51893958F1BB8DE5C806BEAAA0028A28A0028A28A00
            2BCEBF6A2FD92FE1C7EDA5F09AFF00C0FF00143C23A478C3C35A8290D6D7B17C
            F6EE411E6C32AE24865009C491B2B8EC6BD168A00FC92B5F13FC5AFF00836FFC
            67A769DE25D4B5FF008BDFB126A77515859EAB323DDEBDF0A5A470B1A4DB7FD6
            580C851818E81423E125FD5AF05F8CF49F88DE10D2FC41A0EA367ABE89AD5AC7
            7D617D6B28960BB82450E92230E195948208F5A8BE217C3ED13E2C781758F0C7
            8974BB3D6BC3FAFDA4961A8585DC62482EE0914ABC6CA7A820915F9B3FF049FD
            7353FF0082637EDF7E3AFD877C4FAA5D5EF822F6D25F1D7C19BFBF9B74B26992
            48CD75A5863F7DE1712B8039FDCDC39C0651401FA7D451450014514500145145
            001451450015E5BFB6CDC3DB7EC9DE3E911DA375D225C329C11D3BD7A9571FFB
            4043E1CB8F831E234F173CB1F869ACD86A0D1921D62C8C918E739C5003BE003B
            49F01FC12CE599DB40B12C49C927ECF1F26BAEAC7F8789A6C7E00D0D746666D2
            174FB71625B24983CB5F2C9CF7DB8AD8A0028A28A0028A28A0028A28A0028A28
            A0028A28A0028A28A0028A28A002BC7BE0AFC46D5F5FFDAA3E31F87350BE92E3
            4FF0F3690FA6C071B6DD66B567931F56DA6BD86BCD7C03F07E3F09FED25E3FF1
            72EAD6D70FE2CB7B047B05C79B69E443E5866E7A30191401E954514500145145
            0014514500145145001451450014514500145145001451450015E31FB08FC4BD
            6FE2CFC14BAD635EBE92FEEDF5ABD8639180056249005518EC39AF67AF34FD92
            FE0E45F02FE0DDBE850EAD6DADA0BBB9BAFB5DBE3CB7F3252D81827A743EE280
            3D2E8A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28
            A00F00F8B9732C7FF0506F8471092458A4D2355CA062158885BB77ED5EFF005E
            73E38B7F063FED1FE0797549675F1A47697C3454527634463FDFEEEDC2F4CD7A
            35001451450014514500145145001451450015F2C7FC152BFE0A87A07FC138BE
            1AE910DAE913F8EFE2C78EEE4697E08F045839FB6EBD76C42862141648119977
            BE3A90A393C7D11F15FE2768BF053E17F88FC63E23BC4D3FC3FE15D32E357D4A
            E5B9105BC1134B2363BE154F1DEBF3A3FE089FF01B58FDB5FE29F893F6EDF8C3
            A7799E2EF88F3CD67F0CF48BA1E647E0CF0D445A188C4A7849A701F73AE37296
            707FD2185006D7EC69FF000467F107C5CF8AF63F1FFF006D1D5EDBE2A7C6577F
            B4691E1662B2F85BC05193B92DEDEDC6639A54E32E72BB867F78C04A7F471542
            A8000007000E828A2800A28A2800A28A2800A28A2800A28A2800AFCD3FF8388E
            DEDBE04788BF652FDA32DCCF6FAAFC23F8B5A7E9F7D343852FA36A2ACB7D1B37
            5C32DBA20CF004AFEB5FA595F9ABFF00070F449F1CFC5DFB23FECFB24534B67F
            17FE2DD95D6AC236C33E97A6856BB4FF00BE6E91F3DBCBA00FD2AA28A2800A28
            A2800A28A2800A28A2800AF28FDB951A4FD91FC7CAAA598E94FC0193F796BD5E
            B8DFDA17E2143F0A7E0AF88FC4571A7C3AAC3A55A199AD25C6CB8190369C8231
            CFA5004DF0154AFC0CF0582082342B1041EA3FD1D2BACAC8F006BC9E29F01E89
            A9C76E96B1EA56105D2C2BF7610F1AB0418EC338FC2B5E800A28A2800A28A280
            0A28A2800A28A2800A28A2800A28A2800A28A2800AF9EFE1BCB25BFF00C1483E
            23425DCC773E19B09C2927682A635E057D095E71A59F065B7ED55AA456F1CEBE
            3DBAF0F473DD3107CB6B113045C76CEFC5007A3D145140051451400514514005
            145140051451400514514005145140051451401575EBCFECFD0EF6E33B7C881E
            4CFA6149AF18FF00826E46D1FEC59E0A2ECCEEE978C598E49CDECFFD2BD87C59
            756763E15D4E7D47234F86D257B9C727CA084BFF00E3B9AE5BF66B5F0B2FC0EF
            0F1F0524D1F85DA066B05973BC2991CB673CE7796A00EE68A28A0028A28A0028
            A28A0028A28A0028A28A0028A28A0028A28A0028A28A00F00F8C71B37FC1403E
            0DB0562ABA66B1938E07FA39AF7FAF38F1E7C5BB7F0C7ED19E02F093E916F757
            1E27B7BF963BF6C79964208B7955E3387E870457A3D001451450014514500145
            14500145145007E79FFC1CD5E37D72DFFE09BF61F0EBC38D247AC7C75F1D687F
            0F2168C9120175334ECABEBBC5AF964775908EF5F77FC2DF86DA47C1BF867E1D
            F08787ED56C742F0B69B6FA4E9D6EBC886DE089628D3DF08A066BE0EFF00839E
            FC1D7CFF00F04B9BAF88BA2CF2DB7893E07F8C342F1DE912C79DC9710DE2DAEE
            FF0080A5DBBFFC02BEF5F873E3BD3FE297C3DD07C4FA44A66D2BC47A75BEA965
            21182F0CF12CB1B7E2AC0D006CD1451400514514005145140051451400514514
            0057E6DFED9BACCDE36FF8394BF63CF0D4C15EC7C19E0AF14789235233FBDBBB
            5B8B527F0FB3C647D2BF492BF34BF69290697FF074B7ECEB2CAA766A9F08F58B
            385BA0DE925FCADF53B7F9D007E96D1451400514514005145140051451400579
            C7ED79E0FD4BC7FF00B3578C346D22D64BDD4B50B031DBC09F7A56DEA703DF00
            D7A3D79BFED7DE3AD53E19FECD7E2ED7B46B8369AA69B6624B79828631B19117
            383EC4D00751F09B49B8D03E15F866C2EE2686EECB4AB58268CF5474851594FD
            0822BA0AC2F85DAC5C788BE19F87350BB7F32EAFB4CB6B899F18DCEF12B31FC4
            935BB40051451400514514005145140051451400514514005145140051451400
            5781CE9258FF00C14D6091637F2AFBE1C98D9F69DBB97502719F5C28AF7CAF32
            D7FE348D1FF6B0D07C0A74BB57FED8D0A6D446A07FD74652461E50FF0064EDCF
            D6803D368A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A002
            8A28A00E4FE3DCAD07C0BF1A4880B3A6837CCA00C9245BBD72BFB0DD83E9BFB2
            4780A17468DC6968E558608DCCCDFD6BB3F8C5E321F0EBE12F89FC40D6F1DD8D
            134ABABEF224FB9398E267087D988C7E34DF833E2C7F1EFC22F0C6BB259C3A7B
            6B5A5DB5F7D9A2FF0057079B12BED5F61BA803A5A28A2800A28A2800A28A2800
            A28A2800A28A2800A28A2800A28A2800A28A2803C6FE2AFC3CD675CFDB1BE14F
            886D2C669B47D0ED3558EFAE57EE5BB4B6FB6307FDE3C0AF64AF21F8ADF12F59
            F0EFED6DF0A3C37677662D1FC456FAB3DFC1B4113986D83C673D461B9E2BD7A8
            00A28A2800A28A2800A28A2800A28A2803E6FF00F82C3F812D7E23FF00C129FF
            0068BD2EEE3F3631F0EF5BBD8D7FE9B5B594B7109FC25890FE14DFF823978F53
            E257FC1297F676D550B337FC2BED1ACE42DD5A4B7B38EDE43F8BC4C6BA0FF829
            EDEAE9BFF04D4FDA1EE5C0296FF0CFC49230F61A55C9FE95E77FF041CF0FCDE1
            9FF823BFECF36D3AB23CBE0FB6BB01B93B662D329FA15901A00FADA8A28A0028
            A28A0028A28A0028A28A0028A28A002BF37BFE0ABF791FC15FF82C6FEC05F130
            DAB1B6BFF106BDE03BE9FA297D4AD22B7B4427D7CC9A6603BED35FA435F9F9FF
            00072F7C31D5FC41FF0004CEB8F885E1AF3878ABE0478AB48F889A4989373AC9
            6971E548DC721522B892527D22A00FD03A2B9AF833F1534BF8E9F07BC27E37D0
            DDA4D17C63A359EB9A7B37DE6B7B981268C9F7D8EB5D2D001451450014514500
            1451450015C47ED21F0D1BE317C0EF11F8656FA0D35B57B5F245CCDFEAE1C3AB
            64FB715DBD7907EDF13C96DFB2078E5E3778DD6C930CA4823F7D1F71401E93E0
            2D00F853C0DA2E96664B83A6D8416BE6AFDD93646ABB87B1C66B5AB9CF83CC5F
            E127859989663A45A124F24FEE52BA3A0028A28A0028A28A0028A28A0028A28A
            0028A28A0028A28A0028A28A002BC67C77F0BF5AD4BF6DCF02F8B6D6CDA4D134
            ED0EF6CAF6E030C44EDB8A291D792D5ECD5E39F16BE216B3E1CFDAFF00E12E83
            6D7D2C3A2F88EDF5717B6C31B27786D84884FB8245007B1D1451400514514005
            14514005145140051451400514514005145140051451401C37ED33E18D47C6DF
            B3E78CF47D22DCDD6A5AA691716B6F0820191DD0A85E7D735A3F043C3D75E11F
            82DE10D26F62305E697A2595A4F193931C91C088CBF81045737FB63F8E752F86
            DFB3278C35CD22E9ECB53B0B30D6F3A7DE898C88B91EFF003575FF000BB52B9D
            63E19F872F2F2469AF2EF4CB69A790F5776894B1FC493401BB45145001451450
            014514500145145001451450014514500145145001451450079C7C41F83CFE2C
            FDA2FE1EF8BC6A56B6EBE1387518DAD1FF00D6DDFDA2011829FEEF535E8F5E11
            F1D27922FDB8BE04A2BBAA4B16BE1D4120362C948C8EF835EEF4005145140051
            4514005145140051451401F16FFC1C41F1764F82BFF0462F8F5AA43288E7D534
            28FC3E83761A41A85D4162EA3D7F7770E48F406BE80FD873E135EFC04FD8ABE1
            07817528843A8F833C15A36877683F826B6B1861907FDF486BE25FF8383153F6
            8FF899FB26FECCD6F1CD7527C59F89D6DAD6B7042DF30D134B4325E6E1E9B67D
            E09E336E7D2BF4B2800A28A2800A28A2800A28A2800A28A2800A28A2800AE6BE
            347C28D23E3CFC1EF15F81BC41134FA178CB47BBD0F518D4E19EDEE61786500F
            63B1DB9AE968A00FC93FF820AFC0EB7F8C3FB3778BBE127C42F13F8D6C7E257E
            CD7E27BAF00EAD6DA76B6F0C325A45239B2B8442A76C4C82489307056DB3DEBF
            483E0A7ECB5A2FC0ABCD467D335BF16EA4DA9C1E448354D4CDD2C6339CA0DA36
            B7BD7C27FB415DBFFC134BFE0BF7E06F896C5ED7E18FED7FA743E07F1138CF93
            69E25B40A9A74CE07CA0CA9E544A4FF7EE58F4AFD38A00F00BCFF8276F86AF6E
            E599BC69F1455A672E42F884800939C0F93A5771A7FECCBA569BF05EE7C0E9AF
            78BE4B0BA90C8D7CFA996D410920E166DBC0E3A63B9AF47A2803E7D8FF00E09D
            3E1A85D5D3C71F151594E411E2239FFD175DD7C5BFD99F4EF8C1A368D6377E24
            F19E929A245E5472695AA1B692E06D0B994ED3BDB8CE78E49AF48A2803C5FE1E
            FEC45A37C39F1969FADDBF8CFE24EA1369F28956DEFF005D335B4A47674D8370
            F6CD59F8A5FB1B695F157C6D75AEDC78C7E22E937174141B7D2F5B36D6D1E063
            E54D8719EFCF5AF5FA2803CDBE0C7ECCD63F052F3509AD7C55E39D74EA30790C
            BAD6ADF6B484673BA31B46D6F7AE0FC4BFF04E3F0DF8BAD6EEDB51F1DFC57BBB
            4BD24CD6F278843C2E09CE3698C8233EBE95F42D1401E6DA2FECCBA7681F04A5
            F02DB7897C68B62ED94D40EA9FF132806461526DBF2A8DA001B7A66B8D4FD80B
            4F460C3E27FC67241CF3E27FFED75EF7450079D7C57FD9C6DBE2CE8FA359CDE2
            DF1DE86BA345E52CBA3EAE6D65BBF940DD336D3BDB8CE78E49AC0F00FEC6365F
            0FFC6161ACC5F107E2A6A8F6128945AEA3E20F3ED67C7F0C89E58DCBED9AF64A
            2803C93E277EC930FC4EF195D6B2FF0010FE29E86D74147D8F48D7FECB671606
            3E48FCB38CF53CF5AD4F839FB3845F072F3509A3F1AFC42F121D420F20A6BDAC
            7DB52DF9CEF8C6C5DADEFCD7A3D1401E1177FB08C373712CABF167E36C2F2316
            013C5185424E781E57415D8D87ECF7358FC1BB9F081F1DF8FE792E2532FF006E
            49AA96D5A2C9076ACDB785E318C7426BD1A8A00F015FD84EED581FF85D3F1BCE
            0E707C4CD83FF8E576FF00163F6799BE2968FA2D9C7E3BF1FF00868E8F179467
            D1B5636B2DF7CA06E99B69DEDC673C724D7A3D1401E2DF0FFF00640BEF02F8CB
            4FD5E5F8B1F1575B8EC25129B1D475A335ADCE3F8645C7CCBED567E29FECA17B
            F12FC6B75AC43F147E2778723BA0B8B0D275836F690E063E54C719EA7DEBD828
            A00F34F82DFB3B5D7C20BDD4669FC7FE3DF160BF83C958F5CD4CDD25B739DF18
            23E56F7AE2EEFF0061FD62E6EA5957E367C5F8848E5822EB242A64E7038E82BD
            FE8A00F38D3FE005ED8FC17B9F08B78F7C6F3DD5C48641AEC97F9D4E2C9076AC
            98E178C63D09AE113F61AD655C13F1BBE30900E71FDB479FD2BE82A2803CDBE2
            EFC01D47E2768BA2DA59F8FF00C67E177D222F2E49F4BBCF2A5BFF00940DD29F
            E26E339F526BCDF42FD8A2F3C23FB40780FC5BA8FC55F147889FC3925E79363A
            EDD899A7F3A0F2C8872783D0B6339C0F4AFA46BE7BFDAFE67B3FDA2BE024CACC
            AA7C43710B60E01DF1C631401F42514514005145140051451400514514005145
            1400514514005145140051451401E7BFB567C2C8FE367ECFDE24F0B4BACC5E1F
            8F588A38CEA121016DF6CD1BE4E48EBB76F5EF5E6FA1FEC25AE691A6D9DBC7F1
            B7E2A08AD6248D523D4F6C602800003B0C0E07A5697FC14BAEDEDFF634F15468
            CC8F732D8C408383CDEC07F90AF74B4B75B3B58A14C9589020CF5C018A00F3AF
            8BDFB3F5F7C53D3F4782DBC7FE37F0BB6951794F2E917FE43DE9C01BA5E3E66E
            339F73583F0EFF00649D53C07E33B0D5A7F8B1F1375D8ACA4F31AC750D54CB6D
            71FECBAE3915ECF450078EFC4FFD94753F88BE34BCD5EDFE2A7C4AF0F4576411
            61A66A861B583031F22E38CF5AD7F835FB3D5F7C2693536BAF881E39F158D460
            F2546B37FE78B53FDF8F8F95BDEBD2E8A00F02B8FD86B519EE24907C69F8CD18
            762C157C40405C9E83E5E95DB5BFECFB736FF0665F081F1E78EE49E594C9FDB8
            DA913AAA720ED12E385E318C7426BD1E8A00F018BF616BE570CFF1A7E3538539
            C0F11100FD7E5AEDBE2DFECF137C54D3F47B78BC79E3FF000C9D261F24C9A2EA
            A6D64BDE00DD310A77B719CFA935E91450078D7C3DFD90AE7C05E31B0D5E4F8A
            9F1635C5B193CC363A9EBC67B4B8FF006644DBF30F6A9FE247EC92DF127C6B79
            AC49F12BE2A68D15D90469FA4EBC6D6D202063E44D871EBF5AF5EA2803CE7E10
            7ECE517C20975278FC69F107C47FDA50F9246BBAC7DB05B7FB717C8BB5BDF9AE
            3EE3F611B7B89DE43F15FE36A1918B6D5F1461464F41FBAE95EED450079DC1FB
            3AC117C1E97C1CDE30F1E4D14B2F987569357DDAAAF20ED13ECE178C636F426B
            898FF604D3E391587C4FF8CC4A9CE0F89F83FF0090ABDEA8A00F39F8B3FB385B
            7C5BD3F48B79BC5DE3BD086911794AFA3EAFF657BAE00DD29D877B71D78EA6B0
            BE1F7EC6963F0F7C6363ACC7E3FF008A5AB49612798B6BA96BFF0068B597D9D3
            CB1B87B66BD8E8A00F13F8B3FB0B787BE307C4B5F15DFF008ABE2069FAA43BBE
            CCBA7EB0208ACB7284711031928180C1C1E6BA4F839FB34D97C1993526B6F157
            8E75CFED387C861ACEADF6B100FEF47F28DADEF5E9145007825D7FC13FB47BC9
            E477F889F1748918B14FF8493E51939C01E5F4AED6DFF66AD3ADBE0CCBE091E2
            3F19B59CB2190EA2DAA13A92F20E04DB781C74C74AF46A2803C023FF008279E8
            3148AC3C7DF1649539C1F119C7FE8BAEDFE2FF00ECC9A67C65D3B48B5BEF1178
            CB4B8F478BC943A5EA86D9AE0600CCA769DEDC75E3A9AF48A2803C63E1CFEC41
            A0FC34F19D86B969E2BF8857D71A7C9E62417DAD99ADE43E8E9B06E1ED9A97E2
            87EC57A17C56F1A5DEB979E29F1FE9F71798DD069FAD182DD3031F2A6C38FCEB
            D8A8A00F34F831FB2F693F0464D4DAC35FF186ABFDA90F9120D5754374221EA9
            F28DADEF5C4DD7FC139BC27777324ADE2CF8941A562E40D7D80049CF1F257D03
            5F3F7FC152BF6D6B3FF827CFEC1DF11BE29CCF09D5343D31E0D0EDE40185E6A9
            3FEE6CE2DBFC4BE73A3301C84573DA803E28FF0082757C30B0FDA4FF00E0BA7F
            1A3E2058DDEA9AD7813F660D193E197862F3529DAE259757B87793519448782D
            1137501C7F04B11F7AFD58AF917FE086FF00B145DFEC2DFF0004E1F03F8735E4
            99BC75E2957F1778BE79F26E26D56FF12C8B2924E648A3F2A0273C9833DEBEBA
            A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A00F973FE0B27FB0
            B7FC3C2FFE09F5E38F01D844A3C616712EBFE11BA53B26B3D62D332DB98DF236
            34837C05FB2DC39A9BFE08FBFB762FFC1437F60CF0878EAF4F95E2FD3D5BC3FE
            2FB464F2E4B2D6AD02A5D232606C2F94982F6599457D3B5F977E1F53FF000495
            FF0082EFDF698DBAC7E0B7EDAEDF6BB3C922DB49F19427F7918ECA6E8C99EDBD
            EE631D22E003F5128A28A0028A28A0028A28A0028A28A0028A28A0028A28A002
            8A28A0028A28A0028A28A0028A28A0028A28A0028A28A002BCDFE3BC9E0BB5F1
            67C3F9FC571CEFA87F6F470E82D18242DEB8C2EEC76E3BD7A457827EDD70B457
            3F07AF5637905A7C46D2B7ED52485225C9FA7CB401EF74514500145145001451
            4500145145001451450014514500145145001451450079C7ED5371E0B8FE1434
            3E3D59DFC3F797D6D095881DCD319018FA738DC39F6AF47AF9F3FE0A496D2DF7
            C14F0F5B451C92B5C78AF4E8C85524E3739CF1F4AFA0E800A28A2800A28A2800
            A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800
            A28A2800A28A2800AFCBEFDB7247FF0082A57FC169FE197ECEF6911BEF859FB3
            6887E23FC4827E6B6BDD51D47F66E9F20236B10AEAE549F99279C6331D7DC1FB
            7DFED8DE1FFD80FF00640F1DFC5AF126D96CBC21A734F6F6BBB6B6A376E4476D
            6CA7B196678D33D8313D01AF9EFF00E081BFB207887F67DFD90AF7E227C478E5
            93E31FED05AA49E3DF18DC4E9B6E236B92CF6B6AC0F2A228A4DC633F71E6957B
            5007DCB451450014514500145145001451450014514500145145001451450015
            F2A7FC166BF60D9BFE0A0FFB097893C2BA24B358FC40F0D4B1F8A7C13A840FE5
            4F65ACD98678363820A1901787767E5F3B775515F55D1401F337FC121FF6F04F
            F828AFEC21E0FF0088376B1DB78AE057D13C5964ABB1AC358B5C25CA14FE0DD9
            59554F21264AFA66BF2F3C168FFF000499FF0082F0EA3E1D2ED67F05BF6D557D
            574D0DF2DB691E3180FEFA25E70A6E43E7A02EF730A8E22AFD43A0028A28A002
            8A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A002
            8A28A0028A28A002BCCFF6A5F8CFFF000A3BC25A06A5FD976BAA8D47C4167A61
            49FA41E697FDEAFF00B4BB78FAD7A6578CFEDCDF0BB5BF8B1F0974AB1D02CDAF
            AFEC7C41657E630C14F971B36E6E7D0366803D9A8A28A0028A28A0028A28A002
            8A28A0028A28A0028A28A0028A28A0028A28A00F35FDA67E319F837A4F84651A
            5DAEAA7C45E28B2D0F64FD21F3BCC3E68FF697671F5AF4AAF1FF00DACBE1B6B3
            F12754F85EBA559B5E5B687E35B0D5B50C10043045BF739CF50377EB5EC14005
            1451400514514005145140051451400514514005145140051451400514514005
            145140051451400514514005145790FEDEDFB60681FB04FEC81E3DF8B5E2428F
            61E0ED2DEE61B6662A750BB6223B6B5523A196778E3CF6DF93C03401F0C7EDF5
            2BFF00C156BFE0AF9F0EBF65FB156BDF853F018DBFC46F8A4C14B5B5F5F607F6
            7E9727183959159973F324F2F198B8FD45AF86BFE0815FB236BFF017F63EBCF8
            91F111249BE317ED0BAA3F8FBC61753A15B843724C96B68C0F2AB145216F2CFD
            C79E55E800AFB96800A28A2800A28A2800A28A2800A28A2800A28A2800A28A28
            00A28A2800A28A2803E44FF82DDFEC317BFB767EC13E22D2FC3267B6F895E059
            63F18F81EF6DB22EADF57B20648E389810434C9BE107380D22B7F08AED7FE095
            5FB705A7FC1443F60DF87DF1490C116B1ABD80B5D7ED631B4596AB01F2AEE2DB
            D557CD5675079D8E87BD7D0D5F987FB1015FF826AFFC171BE2FF00ECF7229B1F
            871FB45597FC2D0F00C649105AEA6819753B28801B416D9349B47DD8ED611FC5
            C807E9E514514005145140051451400514514005145140051451400514514005
            1451400514514005145140051451400578DFEDE9F10F5AF857FB37EA5AEE817D
            2E9D7F677B663CE8FEF046B84461F886C57B2579DFED5FF0961F8E3F0175CF0C
            CFAA5B68A9A81B76FB6CF8F2E0F2EE2393272475D9B7FE05401E894532DF3F67
            8F2E243B47CC3A371D69F4005145140051451400514514005145140051451400
            5145140051451401E2DFB527C4AD6BC15F153E10699A4DF4B670788BC49F66BF
            44E97102AA9287DBE6AF69AF33F8D7F0720F895F14FE1BEAF2EB16FA7C9E12D4
            A6BE8ED5F1BEFC9451B5327AAEDC9C67835E9940051451400514514005145140
            0514514005145140051451400514514005145140051451400514514005145140
            057E607FC14B08FF00829CFF00C159BE107EC9B67E5DF7C3DF855E57C52F8A69
            F7A19FCB205869B28EFE6798A590F54BB561F738FD15F8E7F18F44FD9E7E0BF8
            B3C79E24B816BA0783748BAD6B5097201582DE2695C0CF56214803B92077AF86
            7FE0DDBF82DAEEB1F01FC79FB4C78F6D9E3F88BFB52F88A5F164DE6125ACB474
            674D36D573D2354691D08EB1CB10FE11401FA214514500145145001451450014
            514500145145001451450014514500145145001451450015F9D9FF00071A7C29
            D63C37FB3AF807F693F065AB4DE3CFD973C5769E2D8026775E696F2C715FDB36
            39F2D9442EFE91C527A9AFD13AC0F8ADF0CF47F8D3F0BFC49E0EF10DA8BDD07C
            57A5DCE8FA95B938F3ADAE2268A54CF6CA3B0CD0037E117C51D1BE387C29F0CF
            8D3C3B722F340F16E976DAC69D3E31E6DBDC44B2C6D8EC4A38E3B57435F9D5FF
            0006DB7C4AD6341FD947C77FB3DF8BAF22BAF1A7ECBFE36D47C1172C0B16B9B0
            F39E5B3B8F9BFE59B133C718FF009E76EBED5FA2B40051451400514514005145
            140051451400514514005145140051451400514514005145140051451400578B
            FF00C143519FF635F1CEC66565B68181070462E613FD2BDA2B8EFDA0E2F0DCDF
            05BC463C5E92C9E1A4B36935058F3BCC6A43718E7390280357E185D1BDF86BE1
            D9C924CDA65B3E4F539894D6E5647C3FB9D3AF7C05A1CDA3861A4CD6103D96EE
            0F9063531E7DF6E2B5E800A28A2800A28A2800A28A2800A28A2800A28A2800A2
            8A2800A28A2803C0BF68FDF79FB677C00B58D980136B73B8070085B48C8FE46B
            DF6BCE3C7175E0C97F690F03DBEA8B3B78CE2B4BE97452A0F969198F6CE5BB64
            A8E335E8F4005145140051451400514514005145140051451400514514005145
            140051451400514514005145140051451401F9B7FF00070EF8BF57F8F1A4FC14
            FD913C25733C3AFF00ED2BE2D86DF5A96DF0D258787AC1D2E6FA623A8C111B8E
            CCB04CBCD7E887823C19A5FC37F05E8FE1DD12CE1D3B45D02CA1D3AC2D22188E
            D6DE18D638E351D9551540F615F9C7FB0EC63F6E9FF82F4FED13F1B2E51AEBC2
            FF00B3F69D07C24F093C91E6317BB9E4D4E546E9BD25F3D323AC774BDB15FA61
            4005145140051451400514514005145140051451400514514005145140051451
            4005145140051451401F9A96CB27EC65FF0007344D14666B7F0B7ED73F0F44AC
            8008E0935ED194F3E8596CE263EA5AECF5CD7E95D7E6D7FC1C796CFF0005FC19
            FB3B7ED1B6B6F3CB2FC00F8A9A66A1A9BC47694D1EF1D62BC427B091E3B68FFE
            075FA491C8B346AE8CAE8E320839047AD002D145140051451400514514005145
            1400514514005145140051451400514514005145140051451400579CFED7BA73
            6ADFB2D7C42811599CE8178E001924AC2CDC7E55E8D583F14FC4CDE0AF861E24
            D656DE2BC6D234BB9BD1049F72631C2CFB1BD8E307EB4018FF00B344A66FD9CB
            C00CC1831F0E69F9046083F668F20FBE6BB6AE43E017C433F163E0BF867C486D
            21B13AC5847706DE2FF570923955F60462BAFA0028A28A0028A28A0028A28A00
            28A28A0028A28A0028A28A0028A28A00F9F3E235ACB79FF051EF874C2394C367
            E19BF90B853B416322F5F5E95F41D799EA5F199EDBF6B7D3BC0034CB5912E3C3
            4FAD9BF3FEBA3C5C345E58FF0064EDCFE35E9940051451400514514005145140
            051451400514514005145140051451400514514005145140051451400579C7ED
            87FB41DA7EC9FF00B297C47F8997CB0C907813C377FAD88A47D8B7324103C91C
            39F591C2A0F52E057A3D7E757FC1CDFE2BD4B5BFF827D683F0774008FE24FDA1
            BC7DA0F80AC137ED75F36E96E4B8FF006775B471B76C4DEF401D77FC1B95FB3C
            5DFC05FF00824EFC3DD4758591FC51F149AE3C7FADDC3B167BB9B527F3619589
            E771B416B9CF706BEE7ACBF03F8334DF871E0AD1FC3BA35AC763A3E83630E9D6
            36E9C2DBC10C6B1C683D95140FC2B52800A28A2800A28A2800A28A2800A28A28
            00A28A2800A28A2800A28A2800A28A2800A28A2800A28A2803E56FF82E17C148
            FF00680FF82487ED01E1D7C978BC1F77AD4200C979B4F03508D47BB3DAA8FC6B
            AAFF0082547C6A1FB43FFC135FE05F8C5AE5EF2EB57F04E962F6673967BB8AD9
            21B9C9EE7CF8E41F857B6F8CFC2361E3FF0007EADA0EAB00B9D2F5BB39AC2F21
            3C0961950C6EBF8AB11F8D7E7DFF00C1AC5E3D7F10FF00C11FFC2DE17B8464D4
            7E19F88F5CF0B5EEE24932ADFCB7607B612ED171FECD007E8B51451400514514
            0051451400514514005145140051451400514514005145140051451400514514
            00573FF167419FC55F0AFC4DA5DAA79973A9695756B12671B9DE17551F8922BA
            0ACAF1DEA32E91E07D66EE16749AD6C6795190659596362081DCE450071FFB24
            F82F54F875FB37F84343D6ADCDA6A9A658886E212C18C6C19B0323DB15E8B5F1
            E7ECC1FF00052FD125F811E1E3E28B3F1C6ADAF08E5177770E90D324CC267008
            60C33F2ED1F515EF7E35FDA93C3BE02F85FA478B6FACBC42DA6EB440822874E7
            92E53209F9E31CAF4EF401E9145783787BFE0A25E05F12EBB67A7DBE99E3449E
            F6558633268AEA8198E064E781EF5D2FC69FDB0BC27F01BC511E91AE5BF8825B
            B961138365A6BDC47B4FFB438CFB5007AA515E51F067F6C8F08FC75F169D1744
            B7F10C5782269B37BA6BDBC7B57AFCC78CFB56178A3FE0A1FF000F7C23E22BDD
            2EED3C4E6EAC266825F2F4795D3729C1C1EE3DE803DD28AF38F00FED4DE16F89
            1F0EF59F1469ABAC0D2F42C9BA13E9F24737033F2A1196E3D2B84FF87997C301
            F7A4F1321F43A2CF9FE5401F41515E71E34FDAA7C25E02F863A478BB5093541A
            3EB640B531584B24C7209F9900CAF4EF5C8F87FF00E0A33F0C7C49ADDA69D6F7
            9AEADD5ECAB0C6B26913A8DCC703276F1CD007BAD15E5BF19BF6C6F03FC05F14
            47A3F88AEB5386FA4844E16DF4F9A75DA7A7CCA08CFB537E0EFED9BE03F8EDE2
            BFEC4F0EDF6A336A5E534DE5CFA74D00DABD4EE6503F5A00F54A2BC3FC4BFF00
            0512F859E12F105EE977DAB6A91DE584CD04CABA4DCB0565382010983CF715D5
            F80FF6ABF057C48F87DAC78A34AD42EE4D1B41C9BD964B19A368F033C215DCDC
            7F741A00F45A2BC13FE1E65F077FE862BFFF00C13DE7FF001BAEDFC5FF00B55F
            81BC07F0DB49F176ADABCD6BA0EB840B3B8FB0CEED2E413CA2A165E87A814018
            9FF0AC758B8FDBBFFE13192CD86816FE08FEC98AE770C1B937A64298EBF70939
            AF5FAF9BBF67AFDAAEDBF685FDB17C4F67E1BD76E353F0769FE1B8658A168248
            112E4CCA19F6C8AAD9C12338AFA46800A28A2800A28A2800A28A2800A28A2800
            A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800AFCD8FF82882C3FB
            427FC17EBF62DF86334134FA7FC3CD375DF897A92A9F94308CC563211FEC5D59
            8E7FE9A57E93D7E707ECF32DE7C5EFF839DFF682D6E6413597C23F84DA2783A0
            6C6442D7F2C1A981EC4E67FC33401FA3F4514500145145001451450014514500
            1451450014514500145145001451450014514500145145001451450015F9BFFF
            0006F4EA563E16F177EDAFF0FAD22F20784FF688F125E431E7FD5DADC98E3857
            E98B46FCEBF482BF34BFE08C1A7B786BFE0AB9FF000518D2064449E39D0F5303
            A7CD7705FCAC7F1E2803F4B68A28A0028A28A0028A28A0028A28A0028A28A002
            8A28A0028A28A0028A28A0028A28A0028A28A002A3BCB65BDB3961700ACC850F
            D08C549450079EFECABF0C2DFE0BFC05D07C2F6DABDB6BB0E93E7A8BD808292E
            FB8924C0C1238DFB7AF6AF42AF9EFF00E098F3BB7ECB56F0BB16369ABDFC3C9C
            918989FEB5F42500145145001451450014514500145145001451450014514500
            1451450079A7853E0F5B691FB51F8ABC6CBAC5BDCDD6AFA55B69EDA72E3CCB45
            4DA77B739C36DC8C8AF4BAF9FBE086FBBFDBF3E37C8598A5A59E890819C81BAD
            037F4AFA06800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28
            A2800A28A2800A28A2800A28A2800AFCDBFF008230EA93F8FF00FE0A75FF0005
            0DF16DC3191A7F885A5F87158FF774C82EEDC2FE0A5457E9257E787FC100AC20
            D4BC4DFB6BF89212AFFDAFFB4A78AED924EEF1426064FC3F7C7F3A00FD0FA28A
            2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A
            2800A28A2800A28A2800AFCEDFF826069A742FF82DCFFC145ECD80467D43C077
            617A644BA45DC99FFC787E75FA255F9DDFB003CA3FE0E0BFF8280AA8CC0D61F0
            FCC871D18687F28FC8B5007E88D1451400514514005145140051451400514514
            00514514005145140051451400514514005145140051451401E6DFB2E5C782A4
            F01EA50F81219A0D26CF59BB82E12504117418197193D32462BD26BC0BFE09F1
            652E99E0CF8876B2C6F19B6F1F6AB180CA4703CAE7E95EFB4005145140051451
            4005145140051451400514514005145140051451401E73F0A6F7C1BA8FC6AF89
            126811CE3C4D1CF650F88A470423BA42C200B938E137038EF5E8D5F3EFEC896F
            29FDA1FE3CDCC91488B37886DE34665203848E41C7AF5AFA0A800A28A2800A28
            A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28
            A2800AFCE3FF00836AEFCEB1F00FF690D409DC352FDA13C59721BFBDB92C79AF
            D1CAFCD4FF00835CD8CBFB187C609588679BE357891DC8E324A59D007E95D145
            1400514514005145140051451400514514005145140051451400514514005145
            1400514514005145140057E78FFC133F5583C4DFF05C6FF828AEA1105610DD78
            02C038EC61D1AE6271FF007D47CFD2BF43ABF34FFE084DA5DC6BFF00B71FFC14
            1FC6728778B53F8C92F875246E7274D3729B73FECADC27D011401FA594514500
            1451450014514500145145001451450014514500145145001451450014514500
            1451450014514500799FECF1F19DFE2C6B7F102C9F4CB4D38F84BC4D75A38307
            FCBD2A11895BFDA6EF5E995E35FB2D7C2DD6FE1D7C43F8B377AADA1B6B3F11F8
            9E4D474F7DC0F9F1303F371D3A8EB5ECB4005145140051451400514514005145
            1400514514005145140051451401E67FB3EFC687F8B5E29F88766DA5DAE9EBE1
            3F114DA3ACB17DEBB11FFCB47FF6ABD32BC7BF64BF85FACFC3AD53E285D6B368
            6D1BC47E36D4353B20583196D5CAF96FC74CF3C7B57B0D001451450014514500
            1451450014514500145145001451450014514500145145001451450014514500
            15F9C3FF0006D0E90FE16FD9D3F687D09D4A3787BE3FF8AB4D2A7EF2F96965C1
            F7E6BF47ABF383FE0845AFCDA37ED4BFB7B782251B1748F8EBA978891318206A
            46420FD0ADB2D007E8FD14514005145140051451400514514005145140051451
            4005145140051451400514514005145140051451400D9E74B581E595D638E352
            CCCC7014019249F4AFCE1FF835E61D4BC5BFB0078DBE26EA8A4CDF1AFE2A7893
            C6C8EC3E691669A2B6627FEDADACB5F4EFFC1587E3745FB39FFC1343E3AF8C1E
            E9ACE7D2FC15A9C56528382B793DBB5BDA8FC6E25887E35F3AFF00C1273F650F
            8DFF00047FE09A9F04742F0AF8F3C33A0E8D27846CB574D3EEF47F3A6B59AFD3
            EDD3A3B3292489AE64CFA74A00FD0DA2BCD7C2DE15F8A369F07B54B1D5BC53E1
            FBBF1ACCCDF61D4A2D3F65AC032301A3C7CC71BBB7715C01F86FFB4BA9C2FC46
            F00B8F56D1D81FD12803E89A2BCDBE277877E29EA1E08D0E0F09F88BC31A7EBF
            02E355B9BEB37920B93B4731A8076FCD93C8E845729E0BF05FED0767E2CD3E5D
            77C67E02BBD1A39D4DE436DA6C8934B1E7E65525460E3BE6803DD28AF22F8C7E
            1AF8D7AA78D2497C0DE24F04E99A098D0241A9D9CB25C07C7CC4B2A91827A55C
            F81FA0FC5FD27C4372DF1035DF06EABA5B4388134AB59629965CF525940DB8CF
            BD007A8D15E05AAF85FF006933AADCBD9789BE160B36918C29359DCEE54CFCA0
            E17AE3DEBB5F0C699F15A1F845AA43ABEA7E0B9BC70EC7FB3EE2DE09C69C8B95
            C798A46F271BBA0F4A00F48A2BE7EFEC4FDA77FE837F070FFDBBDF7FF135DDFC
            49B4F8AF27833444F08DDF8123F102AFFC4D9F558EE4DABB6D1FEA047F301BB7
            7DEED8A00F46A2BC4BC1F61FB4445E28B06D76FBE114BA30997ED8B671DF8B83
            167E6F2F70DBBB1D33C56BFC5B87E37B78C256F02CDF0C174011A796BADADE9B
            B2F8F9B3E57C98CE71ED401EAD457987C149FE324BE22B81F1120F873169221F
            DC1D00DE1B832E47DEF3895DB8CF4E735C9EABA97ED3516A570B67A7FC1896D0
            48DE4B3BEA01CA67E5DDF375C75C5007BDD15E6DE19BDF8B47E10EA92EB167E0
            51E39563FD9F0DA3DCFF0066B2E571E6963BF38DDD0FA5707FDBBFB517FD013E
            0DFF00DFFBEFFE2E803E85A2BCD7E276A7F166CFC11A1C9E11D33C1579E2175F
            F89BC7A84B3A5AC6DB47FA92A4311BB77DE3D315CAF833C49FB44CDE2CD3E2D7
            FC39F0CE1D19E75179359DD5C99922CFCC5033905B1D322803DD28AF21F8C9E2
            1F8DBA678D648FC0DE1FF02EA7A008D0A4BA9DD4D1DC97C7CC08560B807A7B55
            CF81DE20F8BFAB788AE53E20E81E0ED2B4B5833049A4DD492CAD2E47043311B7
            19A00F52A2BE7FD5FC71FB48C5AADCA597823E1DCB66B2B085E4D4A50EC99F94
            9F9C738AEDBC2DE22F8A93FC1ED52F358F0F78620F1B44CDF60B0B7BC736732E
            571BDC9241C6EEFD850060FECE1F1175BF15FC7DF8CBA36A97F35DD9787755B4
            8F4F89FA5B47244EC557D89506BDA2BCA7E00FC2B9FC19F137C77E23D4350D3D
            F5AF197F67DCDFE996D207FECB9628194AE739218B120903815EAD4005145140
            0514514005145140051451400514514005145140051451401E2FFB10FC47D67E
            27F81FC5D7FAD5F4D7EF6FE2ED46D2D1A439315BA18F6463D864E3EB5ED15E19
            FB3C7C32F127C0D9EDB45D0A5D0FC49E13D5753BDD4B53D505C6D9EDA59186D8
            D15490D8DA013EB9E9537C46F18FC7BD3FC6BA843E18F08781AFF418E522CEE2
            F3509127953B1650E003F85007B6D15E67F07BC43F15B56D235A7F1B787BC2DA
            4DEC31674C4D3EEDE549E4DA789324ED19C723DEB857F1A7ED3464609E0DF862
            173C13A84E78FF00BEE803E86A2BCDAF758F8ACBF0560B9B7D23C1EDE3E320F3
            AD1EE25FECE0993921B3BB38C77C75AE1ED3C4DFB4CC975179DE1BF8511C25C6
            FC5D5D160B9E7F8FAE2803E81A2BCCFE346A3F172CEE74DFF84034FF00035DC2
            D066FBFB664B8564973D23F2C8F971EBCD64FC2ED5BE3BDD78D6D13C63A57C35
            B6F0F9DDF69934B96E9AE871C6C0EC57AE3AD007B1515E2DF10B51FDA0A1F196
            A0BE16D3FE154DA0094FD89F5192F45D18FB799B182EEFA715D07C24B9F8B93E
            93ACB78E2DFC0105E887FE254BA39BA3199307FD779849DB9DBF779C66803D26
            8AF03DFF00B4F5CC8CA13E085B28E8C7FB48E7DBA9AEDEF60F8B0DF07604B79F
            C029E3E328F39D96E8E93E5E4E768FF5BBB18EBC673401E8B457835B69BFB4C0
            B98CCDA97C1931061BF6C5A86EDB9E71C75C575FF1974FF8B77771A69F015FF8
            1AD2158317C3598AE199A5CF58FCB07E5C7AF3401E954578FF00C30D27E3ADBF
            8D2D1FC61AAFC37B8F0FAEEFB4C7A5C174B74DC71B4B80A39F5AAFF11341F8FB
            77E31D41FC2FAEFC37B4D05E43F638EFADAE5AE513B6F2AA54B7D38A00F68A2B
            CD7E10E8DF15F4FD23595F1BEB3E0FD42F648B1A5B6996F2A47149B4F32EE032
            B9C703B66B8597C19FB4CBC8C57C65F0C5149E00D3E7381F8A5007D094579B5E
            E81F1525F82B05A41AFF0085A2F1E2C80CB7C6D1CD83264E404C6738C738AE1A
            D3C09FB49A5D4466F1CFC3B784382E0699264AE7903E4F4A00FA0A8AF31F8D9E
            18F8ADAE5C69A7C09E26F0D68914506DBD5D42C5A7334B9FBCA707031DAB23E1
            57823E37693E36B4B9F17F8DBC29AAE849BBED16965A618A5938E36BED18E680
            3D96BF37FF0061BD487C25FF00838B3F6CCF0425AADBDAFC44F0BF85FC6F6BC6
            33F65B68ED2661EA1A6BB9093EA2BEB4F88FF0FF00E3AEAFE34D427F0D78FF00
            C29A4E852484DA5B5C690269A24ECACC54E4FBE6BE18F89BA5F8E3F653FF0083
            857F666F17F8F3C47A46A67E35F82B5DF87B757D676A2D6293EC64DFDBC4CA00
            05DA796DD41EA781DA803F54E8A28A0028A28A0028A28A0028A28A0028A28A00
            28A28A0028A28A0028A28A0028A28A0028A28A0028A28A00FCDDFF008395757B
            8F8A5FB3D7C1FF00D9E74ABB30EB1FB45FC4CD1FC392C6A093FD9D0CEB3DCCC7
            1FC314BF6466F635FA39A669B6FA369D6F67690C76F6B691AC30C51AED489140
            0AA0760000315F9B5AF487F6CEFF00839A348D3C34F3F85FF648F87725FCAAC8
            1E04D7F58015573D01367346E0F50D6A7D2BF4AE800A28A2800A28A2800A28A2
            800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2
            803E7DF82F3B41FF000506F8D10166DB3E9BA3CC173C7CB6CAB9FD6BE82AF36F
            0BDD782A2FDA83C516F6114C9E3A9B4AB69B53720F9725B02163C1CE3238CD7A
            4D00145145001451450014514500145145001451450014514500145151DE5DA5
            859CB3CA76C502191CFA00327F4A00F00FF8262B3CFF00B2769D70ECCE6EB52B
            E9724E49FF004861FD2BE83AF3CFD9593C1FFF000A274497C070CF0785AE3CE9
            2CD660449CCF20727249FBE1ABD0E800A28A2800A28A2800A28A2800A28A2800
            A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800AFCDDFF8399744BC
            F879FB2B7C2BF8F9A45825D6ADFB397C4FD0FC5F2BFF0017D84CE209A2F75799
            ED370F45F4CD7E9157957EDCDFB365AFED87FB1CFC4DF85F76B011E38F0E5EE9
            56F24C3296F72F137D9E6FAC7308DC7BA0A00F49F0F7882CBC59A058EABA6DD4
            37BA76A76F1DDDADC4477473C522864753DC329041F43572BE26FF008378BF69
            2B8FDA47FE0935F0C0EA8E07897E1F412F8175A80A9592D27D31FECF12499FE3
            36A2D9DBDE435F6CD00145145001451450014514500145145001451450014514
            50014514500145145001451450014D9E74B581E595D638A252CECC70AA00C924
            F618A75705FB55F87356F187ECBDF12748D004875DD57C2DA9D9E9DE59C3FDA6
            4B49522C1EC7795A00F84FFE0DB2D2A6F8C9F0BFE3CFED2DA95A5CC3A97ED1BF
            13B53D5AC1E690B9FEC6B495E0B38467B4523DDC63D91476AFD29AFCCEFF0082
            06FED9BF087E017FC107BE166B3E2DF1F784FC29A6F83ED753B7D685FEA31452
            D8DC0D4EF2468DA2CF986470C1D502977122E01C8AE76D3F6ADFDAA7FE0B75A9
            DF45FB3BDD4FFB37FECE693B5A7FC2C8D6B4C67F12F8A906433E996EC408E127
            A48191863FD6AB0688007EA078B7C6BA37803459352D7B56D3344D3A1FF59757
            F7496D027D5DC851F9D79F45FB75FC109EF16DD3E327C2A7B866DA225F165817
            27D31E6E735F1F782BFE0D82FD9A27D55F5AF8A737C4EF8F3E2ABA50D75AC78E
            3C617B3CF2CB81B9C7D9DE1E09E81CB9030327AD774FFF0006E37EC5125B988F
            C05F0F852BB72355D48363FDE1739CFBE73401F68685E20B0F1469915F6997D6
            7A8D94C331DC5ACCB34520F50CA483F855BAFCB4FF00836EBE1C68FF000CBE2D
            FED9DA4FC3DB6B8B0F82FA27C523A1F846CC5E4B776B0496892A5D1864919998
            32B5A9DC589236726BF52E800A28A2800A28A2800A28A2800A28A2800A28A280
            0A28A2800A28A2800A28A2800A28A2803E7CD06C6E6DFF00E0A49E2A7890A7DA
            FC090BA48CA7CBDE2E51464FE1D2A4B8F087ED262EE5687C5BF0C5A1673B15EC
            2E0155CF1D17AE2BB4BBF8CCF67FB5CDAF804E9B69E55DF858EB42FF009F3F70
            B968BCAFF770BBBEB5E97401E7363A6FC5587E0CCF04FA978326F1EF987C9B9F
            2675D37664637281BF7633D06338AE262D2BF69A8DD776A9F06645079CC37E09
            1F80AF7CA2803CE3E2DCDF16A1D3347FF841A2F004D7862FF89A7F6D1BA11F99
            81FEA7CA20EDCEEFBDCE315CFF00C3ED43F6807F1958278A6C3E15A6826502F1
            F4C7BDFB484EE63DEC573F515ECF450078E7C53D77E3AD978D6ED3C1DA27C3CB
            DF0FAEDFB3C9A95C5C25D371CEE0AC14739E95AFF0535AF8B5A95C6A43C7DA37
            83B4E86383363FD93712C8D24B9E8FBD8E171DC57A651401F3EDD78F3F69286E
            E5117813E1ECD0072108D5240C573C1397F4AEEACBC4FF00143FE14B4F7F73E1
            8F0F1F1CA48445A6477C45A3A64609909E0E32719ED5E9145007CF91FC54FDA2
            D6450FF0B3C24EA4F2575F4181F8B5777F173C6FF12BC35A568D27847C11A6F8
            96EEE62DDA8C536AD1DA0B37DA3E552DF7C672323D2BD228A00F19F879F153E3
            3EB3E31B0B4F117C2AD2F46D1E6902DD5FC5E238276B74FEF08C72DF4156BE25
            FC59F8AFE19F195DD9F873E1241E27D1E2DBE46A07C4F6B6666E39FDD38DCB83
            C735EB9450079AFC1BF8A9E3EF1C5D6A49E2BF8672782E3B4837DB48DAEC17FF
            006C933FEAC08D46CF5C9C8AF36F885FB4F7C59B7F0A6B91BFC06D5A3812D275
            370BE22B770ABB1BE7DA23C9E39C0AFA4EB17E2578A3FE108F875AFEB4614B81
            A3E9B717BE537DD93CB899F69F63B71401E59FF04E486E20FD8AFC08B75692D8
            CE6DA766864FBC99BA988FCC107F1AF6DAE37F679F884FF15FE09F86BC48F636
            FA6BEB364B726DA1FF00570E49E17DABB2A0028A28A0028A28A0028A28A0028A
            28A0028A28A0028A28A0028A28A0028A28A0028A28A00E53E21FC78F037C2265
            5F1678CFC27E17671B946ADABDBD9161EA3CD75E2B1BC2BFB5EFC26F1D6A11D9
            E87F143E1DEB3772B04486C7C4967712393D005490926BF28BFE09EBFF0004D2
            F803FB747EDFBFB6F5A7C79F877FF0997C4DF08FC5EBEBCB59353D56FA1F2B41
            BE677D395238A64529B227209538568C038C0AFAF75AFF00836CBF624D7ADDA3
            9BE04E9110618DD6DAE6AB6EC3DF297439A00FB9010C01041068AFCC9F107FC1
            03FC6DFB204126BFFB1A7ED0DF113E17EB166ED7117837C55A836B9E10D4CF1F
            B8789D4B4208C8F3596671DB69F9876FFB187FC16DA3D47E2F4BF03FF6A8F0CD
            97ECFDF1E74F8D4C70DF5D05F0EF8AD09205C69D76EC530C41C46D2312785776
            0CAA01CA7FC12D73FB2CFF00C166BF6CEF80CAF32E87E28BBB1F8B7E1F84A848
            A3FB72A0D40AA8EDE7CF04631DA0AFD2AAFCD3D13C67A6FC52FF0083A70DCF82
            B54B1D52D7C2BF004E9BE2C9ACA559628A66D60CB140ECB91E6E25B67C1E768F
            6AFD2CA0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0
            028A28A0028A28A0028A28A00F8F3C43FF000402FD8F3C55F1B2E7E216A1F02F
            C3375E27BCBE6D4A766BCBD16334ECDB99DAC84FF6520B124A98B6927A57D77A
            469169E1FD2ADAC2C2D6DAC6C6CE2586DEDEDE358A282351854455002A800000
            0C002AC5140057C85FF05ACFDBEAFBF610FD8E2E9FC20B25F7C5CF893769E10F
            0069B00125CDC6AB75FBB49910E7708436FE4152FE5A9FBE2BEA4F88BF10F44F
            849E02D67C51E25D4ED346F0FF0087ACE5D4351BEBA7090DA411A9779189E802
            826BF33FFE09ABE01D5FFE0AE3FB74EA5FB6C78FF49BDB1F86DE117B8F0F7C0D
            D0750428C2D51D926D6E48F38124ADBC2E7386DD8244313900FAFBFE0943FB09
            DA7FC1393F612F037C2F5963BCD6F4FB66D43C457CA777DBF55B83E6DD49BB00
            B2876F2D09E7CB8A307A57D17451400514514005145140051451400514514005
            145140051451400514514005145140051451401E35E21F859AE5CFEDD9E1FF00
            194169BB40B6F0A4DA5DCDC6F0364A677755C75390C2BD96BC53E21FC45D6F45
            FDB93E1E78720BF9E3D0B5AD16FE5B8B40711CB2C6ACCAC7DC6057B5D0014514
            500145145001451450014514500145145001451450015CB7C73D02F7C59F04BC
            63A569B119F51D4F43BDB5B58F207992C96EEA8B93D32C40AEA6B81FDA9FC5B7
            BE04FD9CBC6BAC69B73259EA1A7E913CB6D3A1C34526C21587B8241A0093F664
            F05DF7C3AFD9EFC1BA1EA709B7D474BD26082E62C86F2E408372E475C1CD7755
            CB7C0CD62EFC43F04BC1DA85FCEF737D7DA1D95C5C4CFCB4B23DBA3331F72C49
            FC6BA9A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0
            028A28A0028A28A00FCBCFF829C4F3FF00C12C3FE0A8FF000DBF6C2B48675F86
            1F112087E1C7C5BF210B0B24620586A920009C47B23566C70B6EA839940AFD3D
            D3F508356B082EED668AE6D6EA3596196260E92A30CAB291C10410411D6B8FFD
            A43F67CF0BFED5DF023C57F0E3C69A7A6A7E17F18E9F269D7F01C6EDAC3E5743
            FC322305746EAAC8A4722BE09FF82387ED1FE2AFD8FBE366B9FB0C7C70D552E7
            C5FF000FE0377F0CFC413928BE34F0E619A2442DF7A7B74041404E111D39F219
            9803F4B2BCA3F6B4FD86BE11FEDD7E058BC39F16FC07A178DF4AB67325B0BD8D
            92E2CD8F5686E232B3424E0026375C8E0E6BD5E8A00F0DFD8BBFE09ABF037FE0
            9E3A6EAD6DF06FE1E695E0B1AE94FED09E2B8B8BCBABC08494579EE249252AA5
            9885DDB4127039AF72A28A0028A28A0028A28A0028A28A0028A28A0028A28A00
            28A28A0028A28A0028A28A0028A28A0028A28A0028A2BF393FE0AC9FB6EF8E3E
            39FC67B0FD8D3F66ABF89BE2CF8D6063E37F134323187E1BE8CD1AB492C8E9F7
            2E6589BE4190C032E3E79632003CF7F6BEF1DEB5FF0005E5FDB36F3F668F879A
            96A363FB35FC2CBE86E3E2F78AEC25D91F89AF6298326856B2293B943206671C
            074662311C7E67DB1FF04C402DBF658B5B2882C563A5EA77763630200B1DA5BC
            4E123891470A8AA3000E00AE9BF614FD887C0BFF0004F3FD9A3C3DF0BBE1F69F
            F64D1B448F75C5CC98373AB5DB01E75DCEDFC52C8C327B280AAA02AA81ADFB2A
            45E0A8BE13A9F00BCD2787DEFAE5819492DE77987CCEBCE3774F6A00F49A28A2
            800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2
            800A28A2803CDBC7BF08ECF5FF00DA4BC07E319758B7B3BBF0FDB5FDB4360E54
            49A80962DA4A64E4EC072700FE15E935F3EFED413B5A7ED81FB3ECCAC429BDD6
            223E877DB422BE82A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28
            A002B8CFDA1FC076FF0013FE09F893C3F75A9C3A35BEAD686DDEF652025B8247
            CC7240C76EBDEBB3AF14FF00828B5D359FEC5FE3975254982DD320E0FCD750AF
            F5A00F54F87DE1D8FC1FE01D0F488A75B98B4AD3E0B34997A4A238D5030FA819
            FC6B5EB1BE1C5B1B2F87BA0C2C496874EB743F844A2B66800A28A2800A28A280
            0A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800AFCD1FF
            00828A7EC0BAEFEDFBF1DBE28DD782BC4127863E33FC1AB9D0FC4BF0D35C2FB4
            69F78B661E4B46639DB05C32AEFE080CA8C4300CADFA5D5E71E008FC191FED17
            E3F3A4BCE7C652C36075D562762A08716FB7B7DCEB8A00F13FF8246FFC14CED3
            FE0A27F032EE2F105847E14F8CDF0FA7FEC6F883E13911A19F48BF42C86558DC
            96104A5199324ED2190925493F59D7E71FFC159FF62BF1D7C04F8D169FB67FEC
            DB68EDF157C176A23F1C7856DE2CC3F11743403CE8DD1412F751C6A36100B108
            BB7E78E30DF5FF00EC35FB6CF817FE0A0DFB36681F13BE1F6A1F6CD1B598C25C
            DB498175A45DAAA99ACEE147DD9A22C011C820865255958807AED14514005145
            1400514514005145140051451400514514005145140051451400514514005145
            140051457C9FFF00055EFF0082A1693FF04E8F853A65A68FA5B78D7E327C409C
            695E02F06DB2B4B71AD5EB32A07754F996042E0B1E0B1C28209C800E4BFE0AE9
            FF00053BD6BF65C5F0FF00C1AF82DA67FC267FB4D7C58CDAF8534786359E3D12
            22487D52F14E42431A8765DE36B18D8B7C88F5DBFF00C129FF00E098FA37FC13
            7FE0BDEC379A9BF8CBE2AF8E271AB78EFC65740BDE7882FD8B311BDB2FE446CE
            E23527F89988DCED5C67FC125BFE097BAA7ECA92F883E317C65D561F1CFED31F
            14F173E2AD7D88923D2A26C14D36CFB243180AAC5000C5140F91100FB66800AF
            9F7FE09930BC3FB2B599756469354BE6208C7FCB761FD2BE82AF35FD933E3043
            F1D3E0A58F88E0D22DB448EEAE2E22FB2418D8852564CF0072719FA9A00F4AA2
            8A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A2
            8A2800A28A2803CD7E35DEF82AC3E26FC397F13413C9AE49A9CB0F87A4452562
            B86450DBB078046DEB9AF4AAF9FBF6D4B199FE29FC0DBA8A29241078CE18DCAA
            9214381927D07CB5F40D00145145001451450014514500145145001451450014
            51450015E79FB55EA5E0FD33E056B4FE3C867B8F0B48D6F1DDC50826472678FC
            B03041FF0059B0F5E80D7A1D7807FC14BE09AF3F65BBAB68229267BAD5AC23DA
            8A58E3ED0A7B7D2803DF2DADD2CEDA386350B1C4A1147A0030053E8A2800A28A
            2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A
            2800AF9FBE0C46F17FC140BE33928C125D3B476DC47048B6515F40D79A7823E3
            141E22FDA67C6FE0D4D22DADA7F0ED9595CBDFAE3CCBBF3630C15B8CE17381CD
            007A5D7E557EDABF023C57FF00044BFDA8F55FDABBE0868B75AC7C15F185C2BF
            C68F005913B2D159CB3EBD651FDD478F24BA8C60B3748DDCC5FAAB50EA5A6DB6
            B3A75C59DE5BC17769751B433413209239918619194F0CA412083C10680396F8
            07F1EBC25FB4F7C1DF0F78FBC0BADD9F887C2BE28B34BED3EF6DDB2B2230CED6
            1D51D4E559180646054804115D7D7E49F8DBC37E21FF00836FFF0069DBAF1978
            6ED356D7BF629F8ABAC799E23D1EDD4CCFF0AB54B8703ED9020E7EC4D8008030
            14043F3AC5E67EAC781FC71A37C4CF06E95E22F0EEA963ADE83AE5A477DA7EA1
            6532CD6F7B048A1E3963752432B290411D41A00D4A28A2800A28A2800A28A280
            0A28A2800A28A2800A28A2800A28A2800A28A2800A28AF0BFF008286FF00C141
            FE1FFF00C1363F674D47E20F8F6F5B683F64D1B48B7F9AFBC417CCA4C5696E83
            24B311CB636A2E58F028031BFE0A6BFF000529F05FFC132FF67E7F16788A39F5
            CF12EB130D3BC29E16B2CB6A3E27D418AAA5BC2A0310A0B02EF821578019D911
            BC37FE0955FF0004D8F1A69DF15353FDA97F69B9EDBC41FB4778EE0C5A69EB96
            D3FE1DE9CC084D3ECD092124F2DB1230248CB28662D2492E0FFC133FFE09FF00
            F103F684F8FB1FED81FB555B17F8A7AA464F80FC14EE5EC3E1AE9AE1B6288CF1
            F6B6460493F32124B7EF0909FA3B40051451400578D7EC0DF0E359F853FB32E8
            FA36BF632E9DAAC3737724B0498DCA1EE1D94F1EAA41FC6BD9735E3DFB067C49
            D6BE2DFECBBE1ED7BC4178F7FAADE4974B2CECA01709732A2F4E385503F0A00F
            61A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A28
            00A28A2800A28A2803CD7F693F8CD27C1683C133A69D6BA827887C5765A14866
            CE6D527126654FF697671F5AF4AAF1CFDB43E16EB7F153C29E0C8742B417973A
            378BF4FD5665DE1764317981DF9F4DC2BD8E800A28A2800A28A2800A28A2800A
            28A2800A28A2800A28A2800AF31FDAB7E364BF027C05A56A716996BAABEA5ADD
            A697E4DC67628959B2FF0051B78AF4EAF1BFDB53E15EB5F173C1DE10B0D12D3E
            D6F63E2DD3EFEF06F0BE55B47E66F7E7AE32BC5007B251451400514514005145
            140051451400514514005145140051451400514514005145140051451400578B
            FC38F86DADE8BFB6CFC47F135C58CB1687AD695A7C36B7271B679238D5580FA6
            0D7B4578D7807E27EB7AC7EDB1E3DF0ADCDE349A1E91A358DD5A5B6D00452385
            2ED9EBCE4D007B2D14514018FF00107E1FE87F15FC0DAC7863C4BA558EB9E1ED
            7ECE5B0D474FBC884B6F7B6F2294923753C156524115F957E00F17F897FE0DBF
            FDA3EDBC0BE2EBED5BC45FB14FC4BD558785B5F9D4CF2FC2DD4E77CFD82E64E5
            8D9B0048278C65C0DC260FFAD95C9FC74F81BE13FDA57E126BFE04F1CE8965E2
            2F0AF89AD1ECB50B0BA5CA4D1B0EA08E55D4E195D4865601810403401D2E97AA
            5AEB9A65BDED95CC17967791ACD04F048248A68D8655D5864329041047041A9E
            BF26FF00674F8E1E30FF00837F3E3FE8BF013E356B77FE23FD97BC6576D6DF0C
            BE225F018F08484961A46A527448C6708E7803E6188C3AC3FAC71C8B2A2BA32B
            2B0C820E411EB400B45145001451450014514500145145001451450014514500
            14515E6BFB5C7ED6FE03FD877E026BBF127E246B71687E17D062DD24846F9AEA
            539F2EDE14EB24D2118541D4F5C004800CAFDB9BF6E2F87FFF0004F3FD9CF5BF
            899F11B555D3F45D25365BDBC637DDEAD74C0F95696E9D5E5908C0ECA32CC555
            5987C59FF04F2FD88FE20FEDE9FB46E9DFB61FED4DA6B596AB0AF9BF0ABE1C4A
            CCD67E07B06612437B3C6C066FD861B24020E1980611A4393FB0BFEC95E3CFF8
            2B2FED11A1FED6FF00B4C6912E8FE10D19CDD7C1FF00861704B5BE8F6C71B354
            BD8DB87B8942A48A187242BE0288D07EA4500145145001451450032E13CDB791
            73B772919F4E2BCFFF00652F84BFF0A33E01681E163A8DAEAE74B13FFA5DBFFA
            B9BCCB8965E3E9BF1F8575FE38F1058F857C1DAA6A5A9DF5A69B61656B24B3DD
            5CCCB0C3028539667620281EA4D7E6B7C21FF8380BF661FD8C7F672F0DF837C5
            7E3DBFD73C7DA624C27F0F787F47BBD52F4992E659117CC54FB38628E876B4A0
            E1871401FA7B457E6FDD7FC1C2BAB78BF4B82F7E1F7EC59FB63F8BACAE0663BC
            9FC0C6C6D26F4292A3CC187BF149A57FC1C01E31D0C4B3F8D3F619FDB0BC3FA7
            C29BE4BAD3FC1CDA92440752DBBC90147AE6803F4868AF82FE06FF00C1CA9FB2
            4FC62D6E2D1B56F1DEA7F0BFC47249E53695E3AD1AE3479613EB24E43DB20FF7
            A615F6FF00827C77A1FC4BF0CDA6B7E1CD674AF1068D7EBBEDAFF4DBB8EEED6E
            17FBC92464AB0F704D006AD14514005145140051451400514514005145140051
            4514005145140051451401E27FB7AFC46D6BE177C18D3F54D0B509F4DBAFEDEB
            1825922382D0BB90C87D8F15ED95E6BFB587C20B4F8DDF0826D12F759B7D0605
            BDB6BAFB64DB42218E504039207CDD073D48AF4AA0028A28A0028A28A0028A28
            A0028A28A0028A28A0028A28A002BC67F6B9F887AC78235EF85169A45FCF63FD
            BBE37B1B2BDF2CE3ED16C77799137AAB719FA57B3579AFC7AF84569F147C5FF0
            F6EEE759B7D2E4F0BEBD1EAB0C1215DD7EE83FD5AE48E7BF19A00F4AA28A2800
            A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28AF1FF
            00DA8FFE0A03F053F62AD30DCFC54F89DE0FF053347E6C7697F7E9F6EB85F58A
            D5374F2FFC011A803D828AFCE64FF8399FE0C7C41D76E74EF849F0C7F68BF8E7
            3403E49FC13E029AEADE43FF006D9E29547B98EA8BFF00C17A3E2BBCE1E1FD80
            FF006AF6B2079793409239B1EBE5F94727DB3401FA4F5E67E15F831FD85FB52F
            8ABC71FDA96D37F6EE956D63F605FF005B0797B7E76F63B78AF8BAEFFE0E64F8
            4FF0D750B5B4F8B7F07BF696F826F3604973E2EF01C905A45EA434723C8CA3D4
            479F6AF41FD88FF6F5F84BFB647EDBDE28F11FC38F881A3789343D7BC376F169
            C3CC7B59EE2489D3CD45B79C24A19792414071CF4E6803EDCA28A2800A28A280
            382FDA77F663F047ED8BF03B5FF873F11342B5F11784FC490793776B30C1520E
            5258D8731CA8C032BAE0AB00457E747EC77FB4CF8E3FE08A5FB4368DFB2F7ED1
            3AEDDEBFF077C4738B5F843F142F51628624CF1A3EA321E11D37054763F2F03F
            D5B2F95FAA95E5FF00B63FEC77E02FDBBFF67DD7BE1AFC47D160D67C3BAEC254
            1200B8D3E700F97756EF8CC73464EE561EE082A594807A82B075041041E411C8
            3457E5F7EC27FB62F8F7FE0973FB45691FB237ED3FAD4DAB685A91307C23F8A3
            783C9B4F10D9A958E1D32EDC9212ED06D51B989C944258344CFF00A834005145
            14005145140051451400514514005145731F1A3E33F85FF678F857AEF8DFC6BA
            D58F877C2BE1AB56BCD4751BB7DB15B4630327B92490A1402599800092050066
            FED27FB48F82FF00646F825E21F889F1035BB6F0FF00853C336AD757B773649C
            0FBB1C683E692473855450599880064D7E707EC9DFB3778CFF00E0B8FF00B436
            8DFB4CFED01A25CE85F023C3531B8F847F0CEF0B6DD4D0E0A6B5A94646D90C80
            AB46B9C3607FCB35066A1F007E1178B7FE0E17FDA0748F8DFF0016F49BFF000E
            FEC9FE05D43ED5F0DBC05781A29BC657713328D5B514070D103B82A7DD652506
            57CC69BF592DEDE3B3B78E186348A289422222855450300003A003B5003E8A28
            A0028A2BE2CFF8282FFC16ABC11FB21F8F2DFE16F80F45D47E36FC7FD64F93A6
            F80FC32C279EDA42321AFA540C2D500C310C0BEDF9B685F9C007D73F117E2478
            7BE10F82352F1378AF5CD23C35E1DD1A13717FA9EA7771DA5A59C6081BE49642
            1546481927A902BF39BC6FFF0005BFF88BFB6CF8AAF7C17FB0D7C23BFF0089AD
            6F3FD8EFBE26789E2934BF07690D9019A3DFB24BA65073B4146E32B1CA08CC3F
            0C7FE08D7F13FF00E0A0BE29D2BE247EDDFE3893C52D6D32DF693F08BC3572F6
            7E12F0F9EAA2E7636EBA940383F31E854CB2A1C57E92781FC09A1FC31F09D868
            1E1AD1749F0F685A54420B2D3B4CB48ED2D2CE31D1238A3011147A2802803F3A
            3C2BFF0006FF006B3FB4EEBD67E2AFDB27E3AF8E3E3C6B713ADCAF8634CB87D0
            BC25A6C98FB91DBC1B59C0E30EA212D8F994E4E7EE2FD9CFF63EF86BFB27E82D
            63E02F07683E1F79862E6F6DECA25BEBFE7833CE144929030017248000E8057A
            55140051451401C4FC72FD9ABE1E7ED37E18FEC5F88BE07F0A78E34A19D96DAE
            6970DF24448C164F314EC6C7F12E08F5AF84BE207FC1BA9E1EF839E25B8F18FE
            C93F15BC79FB3278CD8F9A6D74EBD9756F0E6A2C32425CD95C39DCA4E072CC8A
            3A447A57E91D1401F985A57FC1617E3A7FC136F54B4D03F6E2F8546DBC2A644B
            483E2FF802DE5D47C3F3312155AF6D947996ECC7FBAAACC784831CD7E8BFC1EF
            8D3E11FDA0BE1EE9FE2CF03789744F16F86B554DF6BA9695789756D30EE03A12
            0303C15382A72080462B6FC41E1FB0F1668579A5EAB6369A9699A8C2F6D75697
            50ACD05CC4EA55A3746055958120820820D7E6E7C72FF822678C3F643F8877FF
            0016BF615F188F857E299E4FB4EB1F0E75090CDE0DF1605E4C421638B5908C85
            23E55DD8430F2D401FA5D457C5BFF04E7FF82C8681FB5C78E6F7E137C4BF0D6A
            3F05BF68AF0E6E8B56F046BA0C46FCA025AE74E95B8B981941618F9B6E586F4C
            48DF695001451450014514500145145001451450014514500145145007857FC1
            4B10B7EC55E3375255E2FB13A91C1045F5BFF4CD7B5E857A751D12CAE09C9B88
            12427D72A0D71DFB4CBF8562F817E217F1B4135CF85A3851EFE38812ECA2542B
            8C1073BC29EB5D6F85AF2D350F0C69B716008B09ED6292DB2307CB280AFF00E3
            B8A00BD4514500145145001451450014514500145145001451450015F3CFED7A
            EF2FED2BF006DD59807D7AEE5201C6764519FEB5F43579C7C53B8F064DF1CBE1
            BD96BD6D3CFE289A5BF9BC3CC8A4A42D1C2AD39620F1F26DC641E45007A3D145
            14005145140051451400514514005145140051451400514563FC40F883A17C28
            F056A9E25F13EB1A6F87FC3FA25BB5DDFEA3A85C25BDAD9C2A32D2492390AAA0
            772680362BE59FF8280FFC161BE0BFFC13B4DAE91E27D5EF7C4FF10B56648B49
            F03F8620FED2D7F5291C808A205388831230D295DDC85DC78AF967C57FF050FF
            008F7FF058EF1A6ABE04FD8EEDDFE1C7C20B299ACB5EF8DDAED9BAB5C81C4916
            8D6EC0179319C487E61C12603B59BEA0FF00827BFF00C11DBE0F7FC13C84DAE6
            8B617BE33F89DAA969759F1F789E5FED0D7B549A4C99184AF9F255893F2C7824
            637B39F98807CD69E00FDBD3FE0ABB10B8F136BD0FEC53F07EFB94D1F45637DE
            3CD4E0383FBEB8F93EC648C8F94C4E878689C735EDFF00B2AFFC1BFBFB2DFECA
            B7A3568BE1E41F113C5D2B192EBC49E3C97FE121D46EE5273E6B0987909267F8
            E2890FBD7DA345004561616FA559436D6B0436D6D6EA1228A240891A8180AA07
            000F4152D145002491ACD1B23AABA38C1046411E95F217ED6BFF00042BFD9A7F
            6CA3793F897C069A26A97120B98351F0CDC1D16E2CAE867FD29040046F31C9CB
            4A9203D48C8047D7D45007E615E7ECBBFB737FC12EE76D43E0DFC443FB59FC2D
            B3F9A4F047C41BC1078A6D611FC369A91C095950600720765818E2BDD3F611FF
            0082E07C1FFDB4FC567C0BA98D6BE117C63B3616F7FE01F1B5B9D37558E70016
            480BE16719CE00DB21032635AFB26BE79FDBE3FE096BF05FFE0A45E0BFECDF89
            7E14827D5ED531A6F8934E22D35CD2181CAB4172A37601E763EE8C9E4A938A00
            FA1A8AFCA83F19BF6A8FF82155E456FF0013C6BBFB527ECBD6CDE5AF8C2C22F3
            3C65E0A833F29BD889CDD42A320B9246064C91FCB11FD12FD96FF6B4F875FB69
            FC23B1F1D7C30F16695E2FF0CDF9D82E6CA4CB5BC8002D0CD19C3C52A82328E0
            30C8E3045007A2D1451401E35FB78FEC27F0FF00FE0A29FB39EB1F0DBE22698B
            77A66A03CEB2BD8940BCD1AED41F2EEED9C8CA4887D38652CAD95620FC6DFF00
            04EEFDBB3E207EC45FB4359FEC81FB576A9F69F1301B3E18FC48B93E558FC41B
            11C25BCAEC7097B18D881589676F9092FB1E6FD2EAF08FF8287FFC13D3C05FF0
            526FD9EEF7C07E37B67B79E36FB5E87AEDAA28D47C377CBF72EAD9CF2AC08019
            7203AE41EC4007BBD15F9CBFF04C7FF828478F7E087C777FD907F6AABA8ADBE2
            EE836E1BC17E3099C4765F12F4C076C4F1BB01BAEC2A90C3259CAB83FBC46DDF
            A3540051451400514514005145140143C55E2AD33C0DE19D435AD6AFED34AD23
            4AB77BBBCBCBA956282D61452CF23BB70AAAA0924F402BF277C2BA36B9FF0007
            2A7ED231F8935A8752D1BF61FF0085FAB1FEC4D3E412DA4FF1635288956B8954
            E1859C4EA46D38383B47CED2795EE3FF0007197ECCFF0019FF006A0FD882CF49
            F8536F77E22D0F4AD621D53C6FE0FD3EF4D8EA5E32D26221DACEDE6DAD8208DE
            5304B6D5DA1D8046F61FF82537EDCFF04FF6C8FD9AB4BB4F8371C3E1AB5F03DB
            45A3DFF82AE225B4D4FC24D1AEC16F3DBE72141560B20CAB956E77060003E97D
            2749B5D074AB5B0B1B682CECACA2582DEDE08C471411AA8554551C2A80000070
            00AB14514005617C4DF8A1E1CF82FE02D53C53E2ED774AF0D786F4480DCDFEA5
            A95CA5B5ADA463AB3BB9000EDEE4803935E09FF0517FF82AD7C2CFF826CF852C
            7FE12BBCBBD7FC73E216583C37E0AD0D3ED5AE7882776DB1A470AE4A217F97CC
            7C2E781B9B0A7E4CF863FF0004D2F8CDFF000573F1CE97F147F6D7B993C37F0F
            AC2F3FB43C2BF03B4A99E3B3B451FEAA6D5A50434B3E09DD1F5E48CC40B43401
            5BC5DFB767ED01FF0005B3D7F56F047EC9715CFC27F81904ED61AE7C6AD6ECE4
            8EF3545076C9168D6C76B1246479870C3232D6ED8DDF65FF00C13DFF00E0973F
            08FF00E09ADE059B4EF87DA179BE21D553FE279E2AD4C8B9D735F909DCCD3DC1
            1BB697F9846B84079C6724FBCF84FC25A4F80BC3361A2E85A5E9DA2E8DA5C2B6
            D6561616C96D6B69128C2C71C6802A281C055000AD0A0028A28A0028A28A0028
            A28A0028A28A0028A28A00F98FFE0A51FF0004A8F871FF00052BF0159C5AFA4D
            E18F881E1A916F3C2BE38D257CAD63C39748DBE3911D594C9187C31899B6E7E6
            051C2B8F08FF0082737FC1497E20FC27FDA0E3FD93FF006B45B6D33E32D94064
            F0978C54AC7A67C4DB20C424911002ADD051829C172AC08570437E89D7CD9FF0
            545FF826D7863FE0A5BFB3ACBE18D46E1BC3DE34D065FED5F06F8AAD815BDF0D
            6A683314C8EB87F2CB2A891011B8004619519403E93A2BE23FF82317FC1433C4
            9FB4FF00823C51F093E315B9D1BF68BF81B70BA2F8CACA5010EAD18F960D5610
            000D1CCA0162836EE21861644CFDB94005145140051451400514514005145140
            051451401E4DFB7569F26A9FB2378F21891A473A6970AA09276BAB7F4AEC3E06
            48D2FC13F073BAB2BB687645830C107ECE9C1F7A77C6BF18C9F0EFE0FF008A35
            F8AD21BF9344D2AE6FC5BCBFEAE6F2A267DADEC76F352FC21F197FC2C5F851E1
            9F101862B73ADE956B7E628FEE4465895CA0F604E3F0A00E8A8A28A0028A28A0
            028A28A0028A28A0028A28A0028A28A002BC0BE33C535E7EDF3F05C2C5234365
            A7EB32970A4AA97B6DBC9E83A0AF7DAF31F14FC6C9346FDAB3C2FE025D36D668
            F58D1EE7527BD6CF9D6FB0B00ABEC76F3401E9D4514500145145001451450014
            514500145145001451450072BF1C7E37F857F66DF847E20F1DF8DF5AB3F0F785
            3C2F68D7BA96A172D88EDE3181DB966662155402CCCCAA01240AFCBEF86DF067
            E22FFC1C6BF102CFE22FC5AB5D7BE1D7EC75A3DC89FC1FE02123D9EA5F105E37
            25350D48A9E2DCFF000AA9C11809DE6776B76971FF000713FEDEF7FA34B24CFF
            00B1A7ECF7ABF93782199D21F89DE218FAC7B948DF690FB13F29C8399D4C7FAC
            5A5E976DA1E996D65656F05A59D9C4B0410428238E18D40554551C2A80000070
            00A00A5E08F0368BF0CFC21A6F87FC39A469BA0E85A3C0B6B63A769F6C96D6B6
            7128C2C71C6802A281D000056A51450014514500145145001451450014514500
            365892789E39115E3705595864303D411DC57E727ED4FF00F0458F117C0EF8AF
            7FF1C7F628F1341F077E284AE27D67C1E70BE0FF001BA29C9866B5FB9048DCE1
            D46D04E408D89947E8ED1401F117FC13F3FE0B45A07ED2BF122E7E0F7C5EF0C6
            A1F027F689D102C77DE0ED7CF950EAE719F3F4D9DB8B88980DCAB9DC54E57CC5
            1BEBEDDAF9DFFE0A15FF0004BFF84FFF00052AF8751E8FE3FD1E4B7D774CFDE6
            85E29D2DC5AEB7E1F981DCB25BCE0670180251C3213CEDC8047C69E0CFDBD3E3
            C7FC1157C5DA7F813F6B94D43E287C0FBA956CBC3BF1AF4AB77B8B9D3F2711C1
            ACC232FBF18CCBCB120E0CE771400FD53A2B1BE1DFC45D03E2E781B4AF13F85B
            59D37C43E1DD72D92F34FD4B4FB85B8B5BC858655E37525594FA835B3401F36F
            FC14F3FE09ABE12FF8296FC03FF8477569A4F0EF8CFC3D37F6AF837C5B66BB75
            0F0CEA68098A78DD70E62DE177C618060A0821D51D7C77FE0935FF000523F16F
            8E7C73AD7ECCFF00B45DA8F0E7ED27F0D20C492C8163B5F1EE9A802C7AB59B70
            1D9C7CCE8A00C7CE00F9D22FBD2BF1A7FE0AEDF1334EFF0082A07EDC1F0E7E19
            FECA5A4C9AFF00ED0DF05F5F8750D47E2A58CCD0E8DF0FED9598C9697370AACB
            7219F93100C032322EE66952803F65A8A8B4F8E686C204B89567B848D565902E
            D12301CB63B64F38A96800A28A2800A28A2800AF833FE0A23FF0480D43E22FC5
            A87F680FD9B3C4517C20FDA47466F3A4BE80797A478DA21CB5A6A90A8C485F6A
            8F3483C0C386C2347F79D1401F137FC134FF00E0B0BA77ED61E2FBEF843F167C
            3B37C1EFDA4BC2AAD1EB3E0DD4F3147A988D496BBD3A46389E06505F682CCAB9
            20BA0F30F9B7ED45FF000590F187ED27F182F7E057EC43A0DA7C4BF883013078
            83C7F7015BC23E075CE0C8D39CA5CCCB86210654950144C7720FA33FE0A25FF0
            495F829FF053FD0748B7F8A1E1FBB6D5B407274ED7748B9FB0EAD69193F3C026
            0A7744FCE51C32F248C3735EA3FB2D7EC97F0EBF62BF83FA6F80FE1878534BF0
            978674C5012DED10992E1F00196695B2F34AD8E6491998FAF02803E76FF8272F
            FC11A3C25FB1978AEEBE26F8EB5DD47E32FED07E21DD36B3E3DF106669E17718
            686C636C8B68429DA304B95C8CAA6235FB3A8A2800A28A2800A28A2800A28A28
            00A28A2800A28A2800A28A2800A28A2803F317FE0B79E11D43F609FDA47E157E
            DD3E0AB298FF00C209790784BE29DA5A800EB3E1ABC956212BAE40792195D554
            9C9DCF013F2C431FA5DE1EF1058F8B740B1D574CBB82FF004DD4EDE3BBB4B981
            C3C571148A1924561C156520823A835C7FED49F00347FDAAFF00670F1CFC36D7
            911B49F1C68975A3CECCA18C3E744C8B2A83FC48C55D4F66506BE1DFF8209FED
            CDA568DFF04B9F0AF86FE29EBB1687E28F849AEEA5F0CF5037C5B734DA6BAF97
            18C02408EDA6B78F9EE86803F47A8AF3FF00097ED59F0D3C77AA5BD8E91E3AF0
            BDEDF5DB88E0B65D42359A663D155090CC4FA019AE9F54F88DE1ED0F566B0BED
            7B46B3BE5018DBCF7B1473004641DA581C11ED401B3454365A8DBEA50896DA78
            6E233D1A370EA7F115350014514500145145001451450072BF1D3C3575E34F82
            5E31D1AC63F3AF756D0EF6CADE3CE37C925BBA28C9E9924551FD99FC2BA97817
            F67DF06E8BABC06DB53D2B49B7B5B98B706F2DD1029191C718AD9F8B1A8DD691
            F0B3C4B77652B41796BA55D4D048BF7A3916162AC3DC100D723FB1B78D351F88
            7FB31783B5AD5EEE5BFD4AFAC8B5C5C4872F2B091D727DF8A00F4DA28A2800A2
            8A2800A28A2800A28A2800A28A2800A28A2800AF1AF127C2AD6F53FDBABC39E3
            04B4DDE1FD33C2D3D8C973BC7CB70D339098EBF7581CD7B2D78D45E3ED62EBF6
            FF0097C32BA85C2E816BE041A83D967F74D726FB67998FEF6C207D05007B2D14
            514005145140051451400514514005159DAAF8C748D0B3F6ED574DB3DBD7CFB9
            48F1F99155352F89BE1BD1FC3035BBCF10E876BA296D9F6F96FA24B52DE9E616
            DB9F6CD006E57C27FF0005F7FDABFC4FF077F657D17E137C349243F183F68FD5
            97C09E17589CA4B6B1CFB56F6F030E5045148177820A34E8FF00C35F4E5FFEDA
            DF09F4E9D227F1FF0086E577608A20BA13E4938FE0CD7C3BF0C48FDBA7FE0E51
            F1AF8925517DE0DFD91BC0769A2694C93EE8575DD650CD25C05FEF7D95EE616F
            436D19EA05007DB3FB0C7EC7DE18FD82BF651F057C28F0944ABA578474F4B792
            E366D9351B93F3DC5D49FEDCB2B3B9EC37607000AF59A28A0028A28A0028A28A
            0028A28A0028A28A0028A28A0028A28A002B33C6BE09D1BE24F84752F0FF0088
            74AD3B5DD0B58B77B4BED3EFEDD2E2D6F2171868E48DC157520E0820835A7450
            07E59FC42FF82707C6CFF823E78EF54F899FB17CAFE32F85F79235EF89FE076B
            1772490C9DE49F48998968E6DBD23FBD9007EF86D887D4FF00B0DFFC1617E09F
            EDCDF06B58F14E9BE26B4F066A9E0EB779BC61E1DF13CE9A6EA5E11319225374
            B215022520FEF81D9D89560CA3EA5AF8F7F6E7FF008214FECE7FF0509F8B7A37
            8E7C77E14BCB3F1369D2A9BFBCD0AEFF00B39BC470291FE8D7FB54F9F1F006E1
            B65000024038A00F987E22FED59F183FE0BE7E37D57E1B7ECE57DACFC2EFD992
            C657B1F177C5A92030DFF8A36B012586931B61C230E0CA08246779404472FE83
            FEC5FF00B117C35FD807E0869FF0FF00E17F876DF41D0ECF124F27FACBBD4E7D
            A035CDCCA7E69656C724F0060285501477FF000EBE1CE81F08BC0DA57863C2DA
            3699E1DF0EE876EB69A7E9BA7DBA5BDAD9C4A30A91C6A02A81EC2B6A800A28A2
            800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2
            800A28A2800A28A2800A28A2800A28A2800AFCCDFF00825E780744F02FFC15D7
            F6FBF853AADA69FAAE9AFE26D0BC7BA6D9DE5AA4D12C9AAD9CB717922060403B
            E4B74247F7457E9957E697ECEF13685FF074EFED036F1B1116BBF0774AD4655E
            CCF14F61029FC173F9D007DFB67FB3CF8074ED62DB51B6F04784AD750B3944D0
            5CC3A45BC7342E3A32BAA0208F6358FF0013BF643F86DF19BC492EB1E26F09E9
            FAAEA9322C6F72EF247232A8C282518740315E91450079BFC1CFD91BE1E7C00F
            125DEADE10F0F2E8D7D7B07D9A675BCB8995D3706C6D91D80E40E4015C1EA3FF
            0004DBF055C6A7717765E22F889A34B73234ADF61D7DE30189C9C6E56EE6BE84
            A2803CE749FD9EE5F0D7C16BFF0007E9BE36F1B09AEDD9E2D6AF350FB56A56A4
            95384936AE146DC018FE23EB5C043FB23FC4DD3181B1F8FF00E2B4C74FB5E950
            DDFF00E84FCD7D0B450079B7C5DF057C4BD5BC31A1DBF82BC69A5E91A9D8A6CD
            46EAFF004C49D75121146EDB822325831C2FF7B1DAB9BF879E18F8FF00A3F8CB
            4F6F11789FE1E6B1A00947DB562B29E1BB68FBF97850BBBD33C57B6D1401E3DF
            173C4FF1BF46F1BDC2F837C33E08D63C3A150C0D7B7B2C376CDB7E60DF305EB9
            C6074AD5F821E39F89DE25D4AFA1F1DF81B4BF0D5BC106FB6B9B3D5E3BB17326
            40D9B064AF193935E9945007CB3F1D3F6E5F117C29F86BE29D4FC79F07BC41A0
            F8474CB1B86D4B56FED781EDEDAD42B07959F680A36F3C9EE2BE3AFD9CBE25FE
            D87FF0508F01E9DA5FECED77E1BFD9CFF677D2775AE95E3AF10D9C7AEF88FC53
            1976679ED2D997CA58C3310090A0E3E599B040F42FF8382CEA5FB467C44FD963
            F658B592EA0D0FE3F78F5AE7C52D6B318E59F45D2161B9BAB7E3B3098480F66B
            65F7AFD1CF0D786F4FF06F8734FD1F49B2B6D374AD2ADA3B3B3B4B78C470DAC3
            1A848E3451C2AAA80001C0005007E701FF008218FC7CD4035DEA1FF0508FDA2A
            4D633B965B406D2C837A9B65B92A57FD9C81562F3F611FF8289FC1A8ADE3F01F
            ED9BE08F88B6B0722D3C79E0282C777FB2D716EB3CCFF52C0D7E9251401F9BB2
            7ED0FF00F053FF0084BADC76FACFECF7FB3D7C5AB18480F71E11F1649A234E3D
            41D4271B49FF00AE58F6A65DFF00C16BBF68FF0087BAB9B4F1D7FC13D7E39D97
            9671249E14D522F14A7B95682DD51BF06AFD25A2803F39B51FF838F3C37E17C4
            5E21FD953F6D4D02F1066486EBE1AA284F7CB5D0247BE052E9DFF071B787FC5D
            048BE17FD94BF6D4F12DDAAE563B3F86AAE99F4665BA62A3DF69AFD18A2803F3
            6348FF0082F7FC50B5D54C9E21FD81BF6B6B0D0E4CB4373A6F86DF51BB2BEAF6
            E523F2CFB190D4B73FF07307806C2EDAD6E7F66BFDB2AD6F14E0DB4BF0DE3594
            1F4DBF6CEB5FA4545007E71DE7FC1C2D7FE26D2BCFF03FEC5BFB69789E46E15A
            E3E1F9B2B52DD8195259B03DF6D52B2FF82A87EDC1F19B469CFC3FFF00827EEB
            3A24B91E5DEF8D3C7F67A6AC63D5AD668E095BFE02FC57E94D1401F9AC6D7FE0
            AABF1DF4552F75FB297C0F8DDF9F29350D5F558D7D7E6FB4DAB7E629F7BFF048
            3FDAE3E319B7B9F893FF000504F88D04BB7F7967E07F0B5B786E38CFA2CB6F2A
            171EED1826BF49A8A00FCD07FF00824B7ED91F00A0B8D53E137EDDFE31F126A5
            10062D1FE23E851EAF63798EA8F7123CCF103FDE8E2DDEE3AD637ECD3FF052BF
            1EF83FF6CB93C1BF1EFE11C9A17ED2779A31D1ACB4ED0B5456D2BC5B65113706
            F6C1640CC46D462555E4DA11F3B4AB22FEA457E79FFC1C9FF0465D4FF60D87E3
            778647D87E27FECE1ADD978CFC33AA4483CF8116EA18EEA12DD7C968CAC8EBD1
            8DB203C66803EC1F833F18BC65F1262D55F5BF86DA8F8396D21DF67F6CD46394
            DEBF3F2602829DB939AE26F3E27FED157F70EB65F0C7C1FA74618856BCD7567C
            8EC7F76C2BD63E06FC54B2F8EBF053C1FE37D351A3D3BC65A2596B96AAC72562
            B98126407DF6B8AEA6803CDD25F8AFAB7C190DE5782B4AF1EBCBF75CCF2E9B1C
            79F6CB96C7E19AE162F877FB47DFDCA35CFC43F0269F1EE05D2D3476978CF207
            9895F4151401E6DF18BE1378DBE2243A5A683F122EFC1AB6B0ECBC16BA545706
            F64FEFEE66053BF02B0BE197ECC9E2EF0678DACB57D63E2F78BFC4B05AB167B0
            9E3586DAE38E8CA18F15ECD450078D7C49FD8AF4AF8A3E2EBED5F50F1B7C4BB7
            17CFBCD8DA6BA61B387FD948F61DA3DB35B3F077F652F0CFC14B2D62DF4EBAD7
            F508F5D87C8BBFED2D41EE0B26082074DA482791CD7A651401E256FF00F04E5F
            82F6CC587822DE46639264BFBB7C9FC65AF40B3F80FE0DB1F87D078517C35A4C
            BE1BB77F323D3A7804D006CE776D7CE4E4F535D6D1401C8E9BFB3FF80B46915E
            CFC13E11B574E4343A3DBA11F884AF80BFE0DBA4B4F89FE1BFDA9BE3225B98AE
            FE29FC6FD726849C122C2DFCB36B1E7B84371328ED5FA575F9D1FF0006ABE823
            4EFF0082327817532BFBCF126B7AEEA5237791BFB4E78377E5081F85007E8BD1
            4514005145140051451400514514005145140051451400514514005145140051
            4514005145140051451400514514005145140051451400514514005145140051
            45140051451400514514005145140051451400514514005145140057E6BFC189
            85E7FC1D55F180A81FE87F02AC606C7AB6A362FCFE06BF4A2BF343F67A063FF8
            3A8FE3E8939327C1AD2DA3FF006545C69E08FCE803F4BE8A28A0028A28A0028A
            28A0028A28A0028A28A00FCE8FF8282412693FF07077EC07A8DD4812C2F34FF1
            DD9C1B8F0271A3F3F8B09631F957E8BD7E6B7FC1C33E57C13F1D7EC81F1FE7B9
            9AD6C7E127C5FB1B1D6258D49F234AD482ADE484FA05B454C7732815FA534005
            1451400514514005145140051451400514514005145140057CB5FF0005BBD62C
            F43FF82467ED133DF951049E06D46DD73D3CD96231C5FF00911D2BEA5AFCF3FF
            00839EBC717563FF0004B0D4BC03A542D3F887E3478AF43F046931A1F99A79AF
            52E7000E4EE4B474C7FB62803E98FF00825DDBC967FF0004CEFD9DA19811345F
            0C7C348E0F0430D2AD81FD6BDD2B1FE1EF822C3E19780343F0D697198B4CF0F6
            9F6FA65A27F72186358D07E0AA2B62800A28A2800A28A2800A28A2800A28A280
            0AF80BFE0D76756FF82177C0F5041643AF2B0EE0FF00C241A91C7E4457DFB5F9
            DFFF0006AF339FF8229FC340DBBCB5D4F5D11E7A6DFED6BAE9ED9CD007E88514
            5140051451400514514005145140051451400514514005145140051451400514
            5140051451400514514005145140051451400514514005145140051451400514
            5140051451400514514005145140051451400514514005145140057E6DEB7736
            BF037FE0EA0D127BB496283E367C0C9B4CB19586127D42CB50371246A7B95B5B
            30C476DC3D6BF492BF3A3FE0E3BF851ABF86FF00673F87FF00B4A7832D1A5F1E
            FECBBE2CB4F1640D196125D69524B1C57F6A76F58DC081DF3C08E293B13401FA
            2F4573DF093E2868DF1BBE15F86FC67E1DBA5BED03C59A5DB6AFA75C2F49ADE7
            89658DBD89561C76AE86800A28A2800A28A2800A28A2800A28A2803E65FF0082
            C9FECAF27ED9FF00F04C3F8CBF0FAD2DE4BAD5B51F0FC9A86951469BA496FAC9
            96F2DA35F7796044E3B39AB1FF000482FDAC57F6D9FF00826CFC21F8852DDB5E
            EADA8E810D96B323B03236A36B9B5BA66F42D344EFCF6707BD7D255F98DFF04A
            7793FE09EFFF000555FDA27F64CBF5165E12F17CBFF0B77E1A2B011C46CEE984
            77D6B173C88A50A88A3B5A4CDD0D007E9CD14514005145140051451400514514
            00514514005145140057E667EDCF30FDB67FE0BFBFB34FC18B65FB5F873E0169
            777F16FC53E54DC47765962D351D7A078E74B7703A94BB35FA43E33F18699F0F
            3C1FAB78835ABD834ED1B43B39B50BFBB99B6C76B6F121924918F65545249F41
            5F9DBFF06F7F84F52FDA11BE387ED7FE28B39ADF5AFDA3FC5729D023B85065B1
            F0EE9ECD6D67103D464AB21ECC2DE26A00FD24A28A2800A28A2800A28A2800A2
            8A2800A28A2803CEBF6BDF8D56FF00B387ECA7F127C7F732C7145E0CF0C6A3AC
            82EC1433416D248AB93DD994281DC902BE6AFF0083723E1BDFFC2AFF008229FC
            05D2F5281EDEEAEB49BCD5C2B2ED262BDD46EEF216FC629D0FD08AF37FF83883
            C57A97C73D07E08FEC97E18BAB8875EFDA57C6505AEAED6D8692D3C3FA7B25CD
            F4C40E4053E538ECCB04A3D6BF447C1DE11D37E1FF008474AD0745B3834ED1F4
            4B386C2C6D215DB1DB411208E38D4765545000F41401A3451450014514500145
            1450014514500145145001451450014514500145145001451450014514500145
            1450014514500145145001451450014514500145145001451450014514500145
            14500145145001451450014514500145145001589F12FE1DE8FF0017BE1D6BFE
            13F10D9C7A8E83E26D3AE34AD46D5FEEDC5BCF1B45221FAA311F8D6DD1401F9B
            FF00F06F37C4DD67E0EF84FE29FEC83E3ABA793C71FB31F8824B1B0926C23EAB
            E1EBC77B8B0BA404E5970EDD0612396DD4F515FA415F98DFF05A0F0D6A3FF04F
            9FDAE7E15FEDCFE12D3EEAE34CF0BBC7E0BF8B36766997D43C3D752858EE9946
            373C12B00093CB7D9870A86BF49FC19E31D2FE21F8434AD7F43BFB6D5345D6ED
            22BFB0BCB770F0DDC12A078E4461D55958107D0D006951451400514514005145
            14005145140057E727FC1C11F0B35DF845A17C2BFDAF7C056125E78D7F665D75
            750D56DA11B5F57F0E5CB08AFEDD8819DAAA7393C24725C375AFD1BAE2FF0069
            0D7B44F0B7ECF1E3DD53C4B656BA978734DF0E6A175AADA5CA8686EAD12DA469
            A370782AD186041EC4D005DF829F18FC3DFB42FC22F0D78EBC25A847AAF867C5
            DA6C1AAE9B748302682640E848EAAD8382A7952083C835D3D7C23FF06CDE9175
            A2FF00C1107E06477865334D6DAB5C0F309244726B37CF1819E8BE594C7B57DD
            D4005145140051451400514514005145140051451401F9CBFF0007047C66D7FE
            27F847E1E7EC85F0E2F9ADFE237ED35AB26997D3C5967D13C390B892FEEDC290
            4232A9420F0F1ADC0EA057DE5F04BE0F683FB3DFC1EF0BF813C2D662C3C39E0F
            D2EDF47D360CEE31C1046B1A027F89B0A092792493DEBE03B1D26D741FF83ACE
            F65D6DE1B99B5CFD9ED6EBC3C67C31B49135858A58E2CFDD62915C31C73B59BD
            4D7E925001451450014514500145145001451450014515F0BFFC17A7F6C8F10F
            C08FD9834BF855F0CCBDCFC6BFDA2350FF008423C236D0BED9AD967C47777B91
            CA08A29301F8D8F2A37453401E53FF0004C755FF008291FF00C15BFE397ED637
            08B7BE04F8731FFC2A8F8653300F14E90B17BFBE88FF00B6CEDB5C754BC75FE1
            AFD3EAF1FF00D813F63DD0BF606FD8F7C03F093C3C639ACFC1DA5A5B5C5D2C7B
            0EA376C4C97374473832CEF23E3271B80E8057B0500145145001451450014514
            5001451450014514500145145001451450014514500145145001451450014514
            5001451450014514500145145001451450014514500145145001451450014514
            500145145001451450014514500145145001451450073FF163E16E83F1BFE18F
            883C1BE28D3A0D5BC39E29D3E7D2F52B394652E2DE68CC7221F4CAB1C11C8382
            3915F9CFFF000478F8B5E24FF827E7ED33E25FD84FE2B5F4F747C3C93EBDF07F
            5FBA7E3C49E1E6667FB18240CCF6E039DA33809328C24485BF4E2BE45FF82BFF
            00FC1354FF00C140BE08699A97846FD7C2FF001BBE18DDFF006FFC3DF12C6E62
            96C2F90ABFD9DDC74866288AD9C856546C1DA4100FAEA8AF917FE091FF00F053
            44FDBF3E13EADA1F8C74B5F05FC75F86572743F1FF0084A72127B1BC8CEC3751
            21E4DBCAC09523215B726E6C066FAEA800A28A2800A28A2800A28A2800AF8A7F
            E0E1CFDA21BF675FF82467C5B9AD2655D6FC6D609E0BD2A0C664BB9B52716D24
            683BB8B67B871FF5CCD7DAD5F991FF00050DBF1FB7FF00FC16CFF675FD9CF4F7
            FB5F85BE0983F187C781195E259A12A9A6DB48A7F8B7B26E5EF1DF03D8D007DC
            9FB0EFC024FD963F637F85BF0E1638D25F05785B4ED22E0A7DD9278ADD1667FA
            B4A1D8FBB1AF53A28A0028A28A0028A28A0028A28A0028A28A0028A28A00FCD4
            FF0082BD05FD99BFE0AC1FB0DFC7F36617499FC4B7FF000BBC437AD2048D5756
            B73158073C6163792F65C9E3E4E6BF4AEBE4EFF82DEFEC673FEDD1FF0004D0F8
            93E0ED2A1693C59A6598F11F869E352664D4AC4FDA2248B1D1E55592007B09CD
            75DFF04ADFDB42D7FE0A01FB02FC35F8A092C4FAAEB7A5241AEC4981F66D520F
            DCDE26D1F7479C8ECA0F3B1D0F7A00FA0E8A28A0028A28A0028A28A0028A28A0
            0C0F8A7F147C3FF04BE1BEB9E2FF0015EAD69A1786BC35652EA3A95FDD36D8AD
            208D4B3BB1F603A0C92700024E2BF37FFE0917F0C35EFF008290FED73E23FDBB
            BE256977BA7E97A8C32F877E0D6837C30DA268A8F24725F32721669CF9803038
            224988CAB464737FB4E78FB52FF83807F6CF6F803F0FB51B98BF65BF841AAC37
            7F14FC5166C561F186A119DF168F6B20C6F895D4876071905F9090993F553C2B
            E16D37C0DE17D3744D1AC6D74CD2347B58AC6C6CEDA311C3690448123891470A
            AAAA1401C0005005FA28A2800A28A2800A28A2800A28A2800A28A2800A28A280
            0A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A280
            0A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A280
            0A28A2800A28A2800A28A2800A28A2803E03FF0082A6FF00C1357C6BAB7C5CD2
            7F6A2FD996EEDFC37FB46781E011DED81555B1F88BA5A0CBE9976A70A6565015
            24620E02AEE52B14917B07FC1317FE0A87E0CFF8294FC2ABBBBD3ADAE3C27F11
            3C28E2C7C65E09D4CF97AA786AF412ACAE8C033425D58249B46769042B2B28FA
            72BE15FF008290FF00C123351F8C9F166CFE3FFECF7E264F84BFB4B787555935
            68D42E99E2F85142FD8F538802250CAA882460D85015958042801F75515F0AFF
            00C13CFF00E0B43A77C77F88CDF057E3BF871BE06FED21A3B0B5B9F0CEAB308E
            CBC44FD04FA64EC76CEB260B08D599B1F75A5505ABEEAA0028A28A0028A28A00
            E07F6A5FDA47C31FB207ECF1E30F899E32BC5B2F0E78334C9752BB6DC03CDB07
            C90C79EB248E56345EECEA3BD7C69FF06FDFC01F135EFC28F1EFED39F12ED443
            F137F6A4D5D7C51246F966D334550469B68B9E55044E5C0EE8D083CA71E5BFB5
            8EBB73FF0005DAFF0082895AFECF5E189CCFFB36FC04D4A0D67E296B1039F23C
            4DABC6EDE468B1B2FDE452ADB8838C894F0638CB7EABD95943A6D9C36D6D0C56
            F6F6E8238A28D02246A0602A81C0000C003A5004945145001451450014514500
            14514500145145001451450015F973FB1C5E3FFC126BFE0B33F10FE006A4C961
            F083F69BB89BE207C387388EDB4FD6B18D434D4C00AA5F6FCA83A2456C073257
            EA357CA3FF000585FF008277BFFC1433F6556D3FC3B7CFE1FF008ADE02BD4F14
            7C3FD7A190C33697AB5B90F1A89072A92EDD84F453B1F04C62803EAEA2BE4BFF
            00823F7FC148A1FF0082857ECE128F10DBAE81F18BE1DDD3786FE21F87254F2A
            E34AD520263793CB3C88A528CCA470AC1D324C66BEB4A0028A28A0028A2B9BF8
            B9F187C2BF00FE1D6ABE2EF1AF88349F0B78674484CF7DA9EA572B6F6D6C9EEC
            C719270001C924000920500748CC114924003924F0057E5B7EDA7FB6CF8F7FE0
            AC7F1DB58FD95BF652D61F4DF0BE9B20B5F8ADF162D5DBEC9A0DB64EFD3EC244
            3FBDB8902B21643C9CA82143C8B89E3AFDA6FE34FF00C1C07AFDEF80BE017F6F
            FC20FD95CB35AF89BE28DF5AC96BAAF8C2357DB25A69319C158D80219CE0E370
            729FEA64FD16FD903F63CF87DFB0A7C09D23E1CFC33D02DFC3FE1AD21776C53B
            E7BD988024B89E43CC933900B31F6030A000009FB1CFEC7DE04FD847F67AD03E
            197C39D24695E1AF0FC442EF21EE2F666E64B99DC01E64D2372CD81D80014003
            D3E8A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A280
            0A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A280
            0A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A280
            0A28A2800A28A2800A28A2800A28A2803C0FF6FAFF008268FC22FF008292FC37
            4F0FFC4DF0D4379796196D235EB3C5B6B3A1499CEFB6B903720DC0128728C546
            E53818F8BB4FF197EDA5FF000465BA5B1F12697AB7ED93FB3EDA7CB06AFA5C45
            7C79E1B806389A1CB7DAD1173CE5C9C0264887CB5FA9B45007CCBFB0E7FC1607
            F67DFF008284C71DA7C3DF1EE9E3C53B4F9FE15D67FE25BAF5B30197536B210D
            26CFE2684C883FBD5F4D57CBDFB6DFFC11ABF673FF008280DD3EA5F10BE1CE99
            FF00094E4BC7E26D199B4AD691F00076B8836998AE06D138914765AF9FF43FF8
            2257C7CF8197A2DBE0FF00EDEDF19FC31E1C850456FA678BB44B5F197D9631C0
            48DAE258D1001C00B18C5007E90B30452CC4051C927802BF31BF6D9FF829AF8C
            FF006FBF8B37FF00B2FF00EC617CBAA788AE775AF8F3E29C0ACFA1F80ECC9DB2
            2DBDC21C4B78C03A829D0F084BEE6875FC5BFF000431F8BDFB515D2E9FFB457E
            DABF153E27782D9025C7873C37A05A782ED351507EE5C8B69244950F705377A3
            0AFB73F659FD923E1C7EC53F08EC7C0BF0BBC25A5783FC3360770B6B3425EE24
            20032CD2B1324D290066491998E00CE00A00E6FF00E09FFF00B07F817FE09C7F
            B31E83F0C3C056663D3F4B5335F5FCC01BBD66F1F99AEE761F79DDBA0E8AA154
            61540AF6AA28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A002
            8A28A00FCEDFF829A7EC09F123E107ED1B6BFB5EFECB36E927C5CD16D56D3C6B
            E0DDE22B2F891A4A60BC4C063FD2D551769FBCDB136FCE8A1FE80FF8271FFC15
            33E17FFC14B7E1BBEA3E0FD41F49F17690BE5F88FC1FAA1106B5E1EB8076BA4B
            09C33207C812A8DA7A7CAD951F4957C77FB747FC111FE0FF00EDADF11ADBE22D
            ACFE28F84DF18B4E3E659F8F7C0BA81D27581205DA0CC546D9B8C02C40936FCA
            2451401F62551F1378A34CF05787EF356D6751B1D234AD3E233DD5E5ECE96F6F
            6D18EAEF2390AAA3B9240AFCFCB0FF00825AFEDA5A468116916BFF000515F119
            D32D93CA84DCFC24D2AEAF7674F9EE9EE4CB23E3F8D9B39E6B3BC3DFF06DDF84
            3E2AEB169AB7ED27F1B3E357ED27A859C8648EC75ED766D3F444EFF25A42E5E3
            E7B2CC14F4C6280347E3C7FC1C2FE0AD6FC7F73F0E3F65EF05F88FF6A2F8A2B9
            8CC3E198D93C3FA63676896E75165F2FCA0483BE3DD19E8644EA39EF865FF046
            AF89FF00B777C43B0F895FB75F8EA0F191B29C5E68BF097C3523DBF847C3E7A8
            17041DD7720CE0E49FBA4196643B47E80FC0DFD9E7C09FB3278120F0C7C3CF07
            F873C15E1FB6E56C746B08ECE166C01BD8201BDC81CBB658F726BB1A00A5E1BF
            0DE9DE0DF0FD9693A45859E97A5699025B5A59DA42B0C16B1200A91A2280AAAA
            0000000002AED145001451450014514500145145001451450014514500145145
            0014514500145145001451450014514500145145001451450014514500145145
            007FFFD9}
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo1: TfrxMemoView
          Top = 3.779530000000000000
          Width = 30.236240000000000000
          Height = 60.472480000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = -3.779530000000000000
          Top = 56.692950000000000000
          Width = 41.574830000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Pata')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 45.354360000000000000
          Top = 86.929190000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 47.244094490000000000
          Top = 147.401577240000000000
          Width = 90.708720000000000000
          Height = 52.913420000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Virilha'
            'Esquerda')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 551.811380000000000000
          Top = 90.708720000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 551.811380000000000000
          Top = 147.401577240000000000
          Width = 90.708720000000000000
          Height = 52.913420000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Virilha'
            'Direita')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 544.252320000000000000
          Top = 3.779530000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 642.520100000000000000
          Top = 41.574830000000000000
          Width = 90.708720000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Pata')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 139.842610000000000000
          Top = 124.724416770000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 139.842610000000000000
          Top = 75.590600000000000000
          Width = 90.708720000000000000
          Height = 52.913420000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Funda'#231#227'o'
            'Esquerda')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 419.527830000000000000
          Top = 124.724416770000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 419.527830000000000000
          Top = 60.472480000000000000
          Width = 105.826840000000000000
          Height = 52.913420000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Funda'#231#227'o'
            'Direita')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 317.480520000000000000
          Top = 37.795300000000000000
          Width = 26.456710000000000000
          Height = 49.133890000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 317.480520000000000000
          Top = 86.929190000000000000
          Width = 90.708720000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Culatra')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 272.126160000000000000
          Top = 200.315090000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 294.803340000000000000
          Top = 147.401670000000000000
          Width = 105.826840000000000000
          Height = 52.913420000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '    Arria'#231#227'o'
            'Esq          Dir')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 109.606370000000000000
          Top = 253.228510000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 109.606370000000000000
          Top = 200.315090000000000000
          Width = 90.708720000000000000
          Height = 52.913420000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Matambre'
            'Esquerdo')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 423.307360000000000000
          Top = 253.228510000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 423.307360000000000000
          Top = 200.315090000000000000
          Width = 90.708720000000000000
          Height = 52.913420000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Matambre'
            'Direito')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 283.464750000000000000
          Top = 332.598640000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 283.464750000000000000
          Top = 309.921460000000000000
          Width = 90.708720000000000000
          Height = 52.913420000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cupim')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 287.244280000000000000
          Top = 438.425480000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 287.244280000000000000
          Top = 411.968770000000000000
          Width = 90.708720000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Rolete')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 128.504020000000000000
          Top = 427.086890000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 128.504020000000000000
          Top = 374.173470000000000000
          Width = 90.708720000000000000
          Height = 52.913420000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Paleta'
            'Esquerda')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 411.968770000000000000
          Top = 427.086890000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 411.968770000000000000
          Top = 374.173470000000000000
          Width = 90.708720000000000000
          Height = 52.913420000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Paleta'
            'Direita')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 18.897650000000000000
          Top = 430.866420000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 75.590600000000000000
          Top = 495.118430000000000000
          Width = 90.708720000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Pata')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 570.709030000000000000
          Top = 430.866420000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 574.488560000000000000
          Top = 502.677490000000000000
          Width = 90.708720000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Pata')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 204.094620000000000000
          Top = 555.590910000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 188.976500000000000000
          Top = 532.913730000000000000
          Width = 90.708720000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Barbela')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 400.630180000000000000
          Top = 555.590910000000000000
          Width = 30.236220470000000000
          Height = 60.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'F:'
            'R:'
            'RF:')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 396.850650000000000000
          Top = 532.913730000000000000
          Width = 90.708720000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Barbela')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 226.771800000000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Memo51: TfrxMemoView
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Top = 15.118120000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cupim')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Top = 30.236240000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Rolete')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Top = 45.354360000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Barbela')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Top = 60.472480000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Patas')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Top = 75.590600000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Culatra')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Top = 120.944960000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Funda'#231#227'o')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Top = 136.063080000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Matambre')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Top = 151.181200000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Paleta')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Top = 166.299320000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Virilha')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Top = 181.417440000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Arria'#231#227'o')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Top = 196.535560000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Top = 211.653680000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 71.811070000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Couros Furados')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 207.874150000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Couros Raiados')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 343.937230000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Raias Fundas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 480.000310000000000000
          Width = 200.314992360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Outros Defeitos')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 71.811070000000000000
          Top = 15.118120000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 207.874150000000000000
          Top = 15.118120000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 343.937230000000000000
          Top = 15.118120000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 480.000310000000000000
          Top = 15.118120000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Bernes')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 480.000310000000000000
          Top = 75.590600000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Mal Conservados')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 71.811070000000000000
          Top = 30.236240000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 207.874150000000000000
          Top = 30.236240000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 343.937230000000000000
          Top = 30.236240000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 71.811070000000000000
          Top = 45.354360000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 207.874150000000000000
          Top = 45.354360000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 343.937230000000000000
          Top = 45.354360000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 71.811070000000000000
          Top = 60.472480000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Left = 207.874150000000000000
          Top = 60.472480000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Left = 343.937230000000000000
          Top = 60.472480000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 71.811070000000000000
          Top = 75.590600000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Left = 207.874150000000000000
          Top = 75.590600000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Left = 343.937230000000000000
          Top = 75.590600000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Left = 71.811070000000000000
          Top = 120.944960000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Left = 71.811070000000000000
          Top = 136.063080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Left = 71.811070000000000000
          Top = 151.181200000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Left = 71.811070000000000000
          Top = 166.299320000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 71.811070000000000000
          Top = 181.417440000000000000
          Width = 68.031452130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Left = 71.811070000000000000
          Top = 181.417440000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Left = 71.811070000000000000
          Top = 196.535560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Left = 71.811070000000000000
          Top = 211.653680000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Left = 71.811070000000000000
          Top = 90.708720000000000000
          Width = 68.031452130000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Esquerda')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          Left = 139.842610000000000000
          Top = 120.944960000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Left = 139.842610000000000000
          Top = 136.063080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Left = 139.842610000000000000
          Top = 151.181200000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Left = 139.842610000000000000
          Top = 166.299320000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Left = 139.842610000000000000
          Top = 181.417440000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Left = 139.842610000000000000
          Top = 196.535560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          Left = 139.842610000000000000
          Top = 211.653680000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          Left = 139.842610000000000000
          Top = 90.708720000000000000
          Width = 68.031452130000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Direita')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          Left = 207.874150000000000000
          Top = 120.944960000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Left = 207.874150000000000000
          Top = 136.063080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Left = 207.874150000000000000
          Top = 151.181200000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Left = 207.874150000000000000
          Top = 166.299320000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Left = 207.874150000000000000
          Top = 181.417440000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Left = 207.874150000000000000
          Top = 196.535560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Left = 207.874150000000000000
          Top = 211.653680000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Left = 275.905690000000000000
          Top = 120.944960000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 275.905690000000000000
          Top = 136.063080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Left = 275.905690000000000000
          Top = 151.181200000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Left = 275.905690000000000000
          Top = 166.299320000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Left = 275.905690000000000000
          Top = 181.417440000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Left = 275.905690000000000000
          Top = 196.535560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Left = 275.905690000000000000
          Top = 211.653680000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Left = 343.937230000000000000
          Top = 120.944960000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Left = 343.937230000000000000
          Top = 136.063080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          Left = 343.937230000000000000
          Top = 151.181200000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Left = 343.937230000000000000
          Top = 166.299320000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Left = 343.937230000000000000
          Top = 181.417440000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Left = 343.937230000000000000
          Top = 196.535560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Left = 343.937230000000000000
          Top = 211.653680000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Left = 411.968770000000000000
          Top = 120.944960000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          Left = 411.968770000000000000
          Top = 136.063080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Left = 411.968770000000000000
          Top = 151.181200000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Left = 411.968770000000000000
          Top = 166.299320000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Left = 411.968770000000000000
          Top = 181.417440000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Left = 411.968770000000000000
          Top = 196.535560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Left = 411.968770000000000000
          Top = 211.653680000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Left = 604.724800000000000000
          Top = 15.118120000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Left = 480.000310000000000000
          Top = 30.236240000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Mal Recortados')
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Left = 604.724800000000000000
          Top = 30.236240000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Left = 480.000310000000000000
          Top = 45.354360000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Estrias')
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Left = 604.724800000000000000
          Top = 45.354360000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Left = 480.000310000000000000
          Top = 60.472480000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Riscados')
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Left = 604.724800000000000000
          Top = 60.472480000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Left = 480.000310000000000000
          Top = 90.708720000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Estr. Maq. Couro')
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Left = 604.724800000000000000
          Top = 90.708720000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo147: TfrxMemoView
          Left = 480.000310000000000000
          Top = 105.826840000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Estr. Maq. Pata')
          ParentFont = False
        end
        object Memo148: TfrxMemoView
          Left = 604.724800000000000000
          Top = 105.826840000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo149: TfrxMemoView
          Left = 480.000310000000000000
          Top = 120.944960000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Estr. Maq. Cupim')
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Left = 604.724800000000000000
          Top = 120.944960000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Left = 480.000310000000000000
          Top = 136.063080000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Couros c/ 2 Furos')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Left = 604.724800000000000000
          Top = 136.063080000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Left = 480.000310000000000000
          Top = 151.181200000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Couros 3 + Furos')
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Left = 604.724800000000000000
          Top = 151.181200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Left = 480.000310000000000000
          Top = 166.299320000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Couros c/ 2 Raias')
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Left = 604.724800000000000000
          Top = 166.299320000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Left = 480.000310000000000000
          Top = 181.417440000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Couros 3 + Raias')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Left = 604.724800000000000000
          Top = 181.417440000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Left = 480.000310000000000000
          Top = 196.535560000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Couros c/ 2 RFunda')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Left = 604.724800000000000000
          Top = 196.535560000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Left = 480.000310000000000000
          Top = 211.653680000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Couros  3 + RFunda')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Left = 604.724800000000000000
          Top = 211.653680000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Left = 207.874150000000000000
          Top = 90.708720000000000000
          Width = 68.031452130000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Esquerda')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          Left = 275.905690000000000000
          Top = 90.708720000000000000
          Width = 68.031452130000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Direita')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo129: TfrxMemoView
          Left = 343.937230000000000000
          Top = 90.708720000000000000
          Width = 68.031452130000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Esquerda')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo137: TfrxMemoView
          Left = 411.968770000000000000
          Top = 90.708720000000000000
          Width = 68.031452130000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Direita')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo163: TfrxMemoView
          Left = 604.724800000000000000
          Top = 75.590600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Top = 90.708720000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Top = 105.826840000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
      end
    end
  end
  object QrClientesI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECLIENTEI '
      'FROM entidades ci'
      'WHERE ci.Cliente2="V"'
      'ORDER BY NOMECLIENTEI')
    Left = 149
    Top = 281
    object QrClientesINOMECLIENTEI: TWideStringField
      FieldName = 'NOMECLIENTEI'
      Size = 100
    end
    object QrClientesICodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
  end
  object DsClientesI: TDataSource
    DataSet = QrClientesI
    Left = 177
    Top = 281
  end
  object QrProcedencias: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECLIENTEI '
      'FROM entidades ci'
      'WHERE ci.Fornece1="V"'
      'ORDER BY NOMECLIENTEI')
    Left = 205
    Top = 281
    object QrProcedenciasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrProcedenciasNOMECLIENTEI: TWideStringField
      FieldName = 'NOMECLIENTEI'
      Size = 100
    end
  end
  object DsProcedencias: TDataSource
    DataSet = QrProcedencias
    Left = 233
    Top = 281
  end
end
