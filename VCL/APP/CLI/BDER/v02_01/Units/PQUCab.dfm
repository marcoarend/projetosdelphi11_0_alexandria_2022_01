object FmPQUCab: TFmPQUCab
  Left = 339
  Top = 185
  Caption = 'QUI-INSUM-006 :: Pedido de Produ'#231#227'o'
  ClientHeight = 524
  ClientWidth = 617
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 378
    Width = 617
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 487
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 172
    Width = 617
    Height = 206
    Align = alClient
    Caption = ' Dados do item: '
    TabOrder = 1
    ExplicitLeft = 4
    ExplicitTop = 176
    ExplicitHeight = 217
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label7: TLabel
      Left = 12
      Top = 96
      Width = 106
      Height = 13
      Caption = 'Pedido (ou descri'#231#227'o):'
    end
    object Label1: TLabel
      Left = 12
      Top = 136
      Width = 34
      Height = 13
      Caption = 'Ordem:'
    end
    object Label2: TLabel
      Left = 96
      Top = 136
      Width = 61
      Height = 13
      Caption = 'Data pedido:'
    end
    object Label3: TLabel
      Left = 212
      Top = 136
      Width = 103
      Height = 13
      Caption = 'Data in'#237'cio produ'#231#227'o:'
    end
    object Label4: TLabel
      Left = 372
      Top = 136
      Width = 65
      Height = 13
      Caption = 'Data entrega:'
    end
    object Label5: TLabel
      Left = 96
      Top = 16
      Width = 89
      Height = 13
      Caption = 'Grupo do Emiss'#227'o:'
    end
    object LaCI: TLabel
      Left = 12
      Top = 56
      Width = 70
      Height = 13
      Caption = 'Cliente interno:'
    end
    object SbDtEntrega: TSpeedButton
      Left = 524
      Top = 151
      Width = 23
      Height = 22
      Caption = '?'
      OnClick = SbDtEntregaClick
    end
    object EdCodigo: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 12
      Top = 112
      Width = 593
      Height = 21
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdOrdem: TdmkEdit
      Left = 12
      Top = 152
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object TPDtPedido: TdmkEditDateTimePicker
      Left = 96
      Top = 152
      Width = 112
      Height = 21
      Date = 44054.549949409720000000
      Time = 44054.549949409720000000
      TabOrder = 7
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object TPDtIniProd: TdmkEditDateTimePicker
      Left = 212
      Top = 152
      Width = 112
      Height = 21
      Date = 44054.549949409720000000
      Time = 44054.549949409720000000
      TabOrder = 8
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object TPDtEntrega: TdmkEditDateTimePicker
      Left = 372
      Top = 152
      Width = 112
      Height = 21
      Date = 44054.549949409720000000
      Time = 44054.549949409720000000
      TabOrder = 9
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdEmitGru: TdmkEditCB
      Left = 96
      Top = 32
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdEmitGruRedefinido
      DBLookupComboBox = CBEmitGru
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBEmitGru: TdmkDBLookupComboBox
      Left = 163
      Top = 32
      Width = 440
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsEmitGru
      TabOrder = 2
      dmkEditCB = EdEmitGru
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCliInt: TdmkEditCB
      Left = 12
      Top = 72
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCliInt
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCliInt: TdmkDBLookupComboBox
      Left = 80
      Top = 72
      Width = 521
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMECI'
      ListSource = DsCliInt
      TabOrder = 4
      dmkEditCB = EdCliInt
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdDtIniProd: TdmkEdit
      Left = 324
      Top = 152
      Width = 41
      Height = 21
      TabOrder = 10
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '00:00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdDtEntrega: TdmkEdit
      Left = 480
      Top = 152
      Width = 41
      Height = 21
      TabOrder = 11
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '00:00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object CkEncerrado: TdmkCheckBox
      Left = 12
      Top = 180
      Width = 257
      Height = 17
      Caption = 'Pedido encerrado. (n'#227'o far'#225' parte do c'#225'lculo)'
      TabOrder = 12
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 617
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 569
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 521
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 244
        Height = 32
        Caption = 'Pedido de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 244
        Height = 32
        Caption = 'Pedido de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 244
        Height = 32
        Caption = 'Pedido de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 410
    Width = 617
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitTop = 519
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 613
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 454
    Width = 617
    Height = 70
    Align = alBottom
    TabOrder = 5
    ExplicitTop = 563
    object PnSaiDesis: TPanel
      Left = 471
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 469
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 48
    Width = 617
    Height = 124
    Align = alTop
    TabOrder = 0
    TabStop = True
    object Label23: TLabel
      Left = 8
      Top = 64
      Width = 75
      Height = 13
      Caption = 'Qtde a produzir:'
    end
    object LaQtPedido: TLabel
      Left = 8
      Top = 80
      Width = 33
      Height = 13
      Caption = '?????:'
    end
    object Label10: TLabel
      Left = 112
      Top = 64
      Width = 66
      Height = 13
      Caption = 'Produ'#231#227'o dia:'
    end
    object LaQtProdDia: TLabel
      Left = 112
      Top = 80
      Width = 33
      Height = 13
      Caption = '?????:'
    end
    object RGPedGrandeza: TRadioGroup
      Left = 6
      Top = 5
      Width = 439
      Height = 60
      Caption = ' Grandeza: '
      Columns = 5
      ItemIndex = 0
      Items.Strings = (
        'Indefinido'
        #193'rea m'#178
        #193'rea ft'#178
        'Peso kg'
        'Couros')
      TabOrder = 0
      TabStop = True
      OnClick = RGPedGrandezaClick
    end
    object EdPedQtPdGdz: TdmkEdit
      Left = 8
      Top = 96
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdQtProdDia: TdmkEdit
      Left = 112
      Top = 96
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object QrEmitGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM emitgru '
      'ORDER BY Nome ')
    Left = 482
    Top = 24
    object QrEmitGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEmitGru: TDataSource
    DataSet = QrEmitGru
    Left = 484
    Top = 69
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 361
    Top = 40
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 361
    Top = 88
  end
end
