unit FormulasImpEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, StdCtrls, Buttons, UnInternalConsts,
  dmkgeral, Db, mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmFormulasImpEdit = class(TForm)
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    TbPQ: TmySQLTable;
    DsPQ: TDataSource;
    TbPQCodigo: TIntegerField;
    TbPQNome: TWideStringField;
    TbPQPrecoComparativo: TFloatField;
    TbPQPreco: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtCancela: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmFormulasImpEdit: TFmFormulasImpEdit;

implementation

{$R *.DFM}

uses UnMyObjects, Module;

procedure TFmFormulasImpEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormulasImpEdit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmFormulasImpEdit.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulasImpEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
