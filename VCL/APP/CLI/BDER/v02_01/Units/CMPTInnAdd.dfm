object FmCMPTInnAdd: TFmCMPTInnAdd
  Left = 339
  Top = 185
  Caption = 'PST-_CMPT-004 :: Edi'#231#227'o de Item de Entrada de CMPT'
  ClientHeight = 670
  ClientWidth = 965
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 965
    Height = 478
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 965
      Height = 164
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 15
        Top = 10
        Width = 81
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data entrada:'
      end
      object Label9: TLabel
        Left = 143
        Top = 10
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
      end
      object Label5: TLabel
        Left = 15
        Top = 59
        Width = 44
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente:'
      end
      object Label17: TLabel
        Left = 15
        Top = 108
        Width = 80
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Proced'#234'ncia:'
      end
      object TPDataE: TdmkEditDateTimePicker
        Left = 15
        Top = 30
        Width = 124
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 39749.934630706020000000
        Time = 39749.934630706020000000
        TabOrder = 0
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataE'
        UpdCampo = 'DataE'
        UpdType = utYes
      end
      object EdFilial: TdmkEditCB
        Left = 143
        Top = 30
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Filial'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFilial
        IgnoraDBLookupComboBox = False
      end
      object CBFilial: TdmkDBLookupComboBox
        Left = 217
        Top = 30
        Width = 734
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Filial'
        ListField = 'NomeEmp'
        ListSource = DModG.DsFiliLog
        TabOrder = 2
        dmkEditCB = EdFilial
        QryCampo = 'Filial'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdClienteEdit: TdmkEditCB
        Left = 15
        Top = 79
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClienteEdit
        IgnoraDBLookupComboBox = False
      end
      object CBClienteEdit: TdmkDBLookupComboBox
        Left = 90
        Top = 79
        Width = 679
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMECLI'
        ListSource = DsClientesEdit
        TabOrder = 4
        dmkEditCB = EdClienteEdit
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGTipoNF: TdmkRadioGroup
        Left = 778
        Top = 59
        Width = 173
        Height = 46
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tipo de Nota Fiscal:'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'NF-e'
          'NF')
        TabOrder = 5
        OnClick = RGTipoNFClick
        QryCampo = 'TipoNF'
        UpdType = utYes
        OldValor = 0
      end
      object EdProcedencia: TdmkEditCB
        Left = 15
        Top = 128
        Width = 59
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Procedencia'
        UpdCampo = 'Procedencia'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBProcedencia
        IgnoraDBLookupComboBox = False
      end
      object CBProcedencia: TdmkDBLookupComboBox
        Left = 75
        Top = 128
        Width = 490
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsProcedencias
        TabOrder = 7
        dmkEditCB = EdProcedencia
        QryCampo = 'Procedencia'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object BitBtn1: TBitBtn
        Left = 566
        Top = 128
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        TabOrder = 8
        TabStop = False
        OnClick = BitBtn1Click
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 164
      Width = 965
      Height = 93
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet2
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nota Fiscal Eletr'#244'nica (NF-e) '
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 957
          Height = 62
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label15: TLabel
            Left = 10
            Top = 5
            Width = 75
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Chave NF-e:'
          end
          object EdrefNFe: TdmkEdit
            Left = 10
            Top = 25
            Width = 932
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtChaveNFe
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'refNFe'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdrefNFeExit
          end
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nota Fiscal (NF)'
        ImageIndex = 1
        object DBGrid1: TDBGrid
          Left = 443
          Top = 0
          Width = 514
          Height = 62
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          DataSource = DsGraGruX
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
          Columns = <
            item
              Expanded = False
              FieldName = 'GraGruX'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_GG1'
              Title.Caption = 'Grupo (produto)'
              Width = 189
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_COR'
              Title.Caption = 'Cor'
              Width = 86
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TAM'
              Title.Caption = 'Tamanho'
              Width = 54
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 443
          Height = 62
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label10: TLabel
            Left = 295
            Top = 5
            Width = 43
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'NF RP:'
          end
          object Label1: TLabel
            Left = 153
            Top = 5
            Width = 138
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'S'#233'rie (zero para '#250'nica):'
          end
          object Label16: TLabel
            Left = 10
            Top = 5
            Width = 131
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Modelo do doc. fiscal:'
          end
          object EdNFInn: TdmkEdit
            Left = 295
            Top = 25
            Width = 135
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '999999999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'NFInn'
            UpdCampo = 'NFInn'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdSerie: TdmkEdit
            Left = 153
            Top = 25
            Width = 134
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Serie'
            UpdCampo = 'Serie'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdmodNF: TdmkEdit
            Left = 10
            Top = 25
            Width = 134
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '99'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'modNF'
            UpdCampo = 'modNF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 257
      Width = 965
      Height = 54
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object PainelGGX: TPanel
        Left = 139
        Top = 0
        Width = 826
        Height = 54
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        object Label12: TLabel
          Left = 0
          Top = 5
          Width = 97
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Nome do grupo:'
        end
        object Label13: TLabel
          Left = 409
          Top = 5
          Width = 24
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cor:'
          FocusControl = DBEdit2
        end
        object Label14: TLabel
          Left = 729
          Top = 5
          Width = 61
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Tamanho:'
          FocusControl = DBEdit3
        end
        object DBEdit1: TDBEdit
          Left = 0
          Top = 25
          Width = 405
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'NO_GG1'
          DataSource = DsGraGruX
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 409
          Top = 25
          Width = 316
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'NO_COR'
          DataSource = DsGraGruX
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 729
          Top = 25
          Width = 85
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'NO_TAM'
          DataSource = DsGraGruX
          TabOrder = 2
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 139
        Height = 54
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Label11: TLabel
          Left = 10
          Top = 5
          Width = 61
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Reduzido:'
        end
        object SpeedButton1: TSpeedButton
          Left = 108
          Top = 25
          Width = 26
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '?'
          OnClick = SpeedButton1Click
        end
        object EdGraGruX: TdmkEdit
          Left = 10
          Top = 25
          Width = 98
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'GraGruX'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdGraGruXChange
          OnExit = EdGraGruXExit
        end
      end
    end
    object PnEdita: TPanel
      Left = 0
      Top = 311
      Width = 965
      Height = 167
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 3
      object Label4: TLabel
        Left = 10
        Top = 5
        Width = 56
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'PNF (kg):'
      end
      object Label6: TLabel
        Left = 103
        Top = 5
        Width = 42
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pe'#231'as:'
      end
      object Label7: TLabel
        Left = 276
        Top = 5
        Width = 63
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Valor total:'
      end
      object Label8: TLabel
        Left = 379
        Top = 5
        Width = 42
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'NF VP:'
      end
      object Label3: TLabel
        Left = 182
        Top = 5
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = #193'rea (m'#178'):'
      end
      object Label20: TLabel
        Left = 463
        Top = 5
        Width = 42
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'NF CC:'
      end
      object Label21: TLabel
        Left = 852
        Top = 5
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Total MO:'
        Enabled = False
      end
      object Label18: TLabel
        Left = 670
        Top = 5
        Width = 54
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'PLE (kg):'
      end
      object Label19: TLabel
        Left = 763
        Top = 5
        Width = 63
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pre'#231'o MO:'
      end
      object EdInnQtdkg: TdmkEdit
        Left = 10
        Top = 25
        Width = 88
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'InnQtdkg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdInnQtdkgRedefinido
      end
      object EdInnQtdPc: TdmkEdit
        Left = 103
        Top = 25
        Width = 74
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 1
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0'
        QryCampo = 'InnQtdPc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdInnQtdVal: TdmkEdit
        Left = 276
        Top = 25
        Width = 98
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'InnQtdVal'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdNFRef: TdmkEdit
        Left = 379
        Top = 25
        Width = 79
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        QryCampo = 'NFRef'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CkContinuar: TCheckBox
        Left = 10
        Top = 54
        Width = 154
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Continuar inserindo.'
        TabOrder = 10
        Visible = False
      end
      object EdInnQtdM2: TdmkEdit
        Left = 182
        Top = 25
        Width = 89
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'InnQtdM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdNF_CC: TdmkEdit
        Left = 463
        Top = 25
        Width = 79
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        QryCampo = 'NF_CC'
        UpdCampo = 'NF_CC'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdValMaoObra: TdmkEdit
        Left = 852
        Top = 25
        Width = 98
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ValMaoObra'
        UpdCampo = 'ValMaoObra'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPLEKg: TdmkEdit
        Left = 670
        Top = 25
        Width = 88
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PLEKg'
        UpdCampo = 'PLEKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdPLEKgRedefinido
      end
      object RGHowCobrMO: TdmkRadioGroup
        Left = 546
        Top = 0
        Width = 120
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'M'#227'o-de-obra:'
        Columns = 2
        ItemIndex = 1
        Items.Strings = (
          'PNF'
          'PLE')
        TabOrder = 6
        OnClick = RGHowCobrMOClick
        QryCampo = 'HowCobrMO'
        UpdCampo = 'HowCobrMO'
        UpdType = utYes
        OldValor = 0
      end
      object EdPrcUnitMO: TdmkEdit
        Left = 763
        Top = 25
        Width = 84
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'PrcUnitMO'
        UpdCampo = 'PrcUnitMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdPrcUnitMORedefinido
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 906
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 847
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 525
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Edi'#231#227'o de Item de Entrada de CMPT'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 525
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Edi'#231#227'o de Item de Entrada de CMPT'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 525
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Edi'#231#227'o de Item de Entrada de CMPT'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 537
    Width = 965
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel7: TPanel
      Left = 2
      Top = 18
      Width = 961
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 645
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'VP: Venda Proced'#234'ncia.          RP: Remessa Proced'#234'ncia.        ' +
          '  CC: Cobertura Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 645
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'VP: Venda Proced'#234'ncia.          RP: Remessa Proced'#234'ncia.        ' +
          '  CC: Cobertura Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 591
    Width = 965
    Height = 79
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel8: TPanel
      Left = 2
      Top = 18
      Width = 961
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 784
        Top = 0
        Width = 177
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 15
          Top = 4
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle GraGruX, gg1.Nome NO_GG1, '
      'gcc.Nome NO_COR,  gti.Nome NO_TAM'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pgt.TipPrd=2'
      'ORDER BY NO_GG1')
    Left = 408
    Top = 52
    object QrGraGruXNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 30
    end
    object QrGraGruXNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGraGruXNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 404
    Top = 100
  end
  object QrClientesEdit: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMECLI'
      'FROM entidades en '
      'WHERE en.Cliente2="V"'
      'ORDER BY NOMECLI')
    Left = 324
    Top = 52
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsClientesEdit: TDataSource
    DataSet = QrClientesEdit
    Left = 324
    Top = 100
  end
  object QrProcedencias: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emp.Corretor, emp.Comissao, emp.DescAdiant,'
      'emp.CondPagto, emp.CondComis, emp.LocalEntrg,'
      'emp.PreDescarn, emp.MaskLetras, emp.MaskFormat, '
      'emp.Abate, emp.Transporte, '
      'emp.LoteLetras, emp.LoteFormat,  ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'LEFT JOIN entimp emp ON emp.Codigo=ent.Codigo'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 240
    Top = 52
    object QrProcedenciasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProcedenciasNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrProcedenciasPreDescarn: TSmallintField
      FieldName = 'PreDescarn'
    end
    object QrProcedenciasMaskLetras: TWideStringField
      FieldName = 'MaskLetras'
      Size = 10
    end
    object QrProcedenciasMaskFormat: TWideStringField
      FieldName = 'MaskFormat'
      Size = 10
    end
    object QrProcedenciasCorretor: TIntegerField
      FieldName = 'Corretor'
    end
    object QrProcedenciasComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrProcedenciasDescAdiant: TFloatField
      FieldName = 'DescAdiant'
    end
    object QrProcedenciasCondPagto: TWideStringField
      FieldName = 'CondPagto'
      Size = 30
    end
    object QrProcedenciasCondComis: TWideStringField
      FieldName = 'CondComis'
      Size = 30
    end
    object QrProcedenciasLocalEntrg: TWideStringField
      FieldName = 'LocalEntrg'
      Size = 50
    end
    object QrProcedenciasAbate: TIntegerField
      FieldName = 'Abate'
    end
    object QrProcedenciasTransporte: TIntegerField
      FieldName = 'Transporte'
    end
    object QrProcedenciasLoteLetras: TWideStringField
      FieldName = 'LoteLetras'
      Size = 10
    end
    object QrProcedenciasLoteFormat: TWideStringField
      FieldName = 'LoteFormat'
      Size = 10
    end
  end
  object DsProcedencias: TDataSource
    DataSet = QrProcedencias
    Left = 240
    Top = 100
  end
end
