unit PQ;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, Grids, DmkDAC_PF,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, DBGrids,
  ComCtrls, Menus, Variants, dmkCheckGroup, dmkGeral, dmkEdit, AppListas,
  dmkDBLookupComboBox, dmkEditCB, UnDmkProcFunc, dmkLabel, dmkImage, UnDmkEnums,
  dmkCheckBox, dmkEditDateTimePicker, dmkDBGrid, UnEmpresas, UnAppPF, dmkValUsu,
  UnUMedi_PF;

type
  TFmPQ = class(TForm)
    PainelDados: TPanel;
    DsPQ: TDataSource;
    QrPQ: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    DBEdit01: TDBEdit;
    QrEntiFor2: TmySQLQuery;
    QrEntiFor2Codigo: TIntegerField;
    QrEntiFor2Nome: TWideStringField;
    DsEntiFor2: TDataSource;
    Label4: TLabel;
    EdIQ: TdmkEditCB;
    CBIQ: TdmkDBLookupComboBox;
    DBEdit03: TDBEdit;
    DBEdit02: TDBEdit;
    Label8: TLabel;
    QrPQNOMEIQ: TWideStringField;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    QrPQIQ: TIntegerField;
    QrPQAtivo: TSmallintField;
    QrPQLk: TIntegerField;
    QrPQDataCad: TDateField;
    QrPQDataAlt: TDateField;
    QrPQUserCad: TIntegerField;
    QrPQUserAlt: TIntegerField;
    RGGGXNiv2: TRadioGroup;
    QrPQFor: TmySQLQuery;
    DsPQFor: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PnEditFor: TPanel;
    Label24: TLabel;
    EdPrazo: TdmkEdit;
    QrSetores: TmySQLQuery;
    QrSetoresCodigo: TIntegerField;
    QrSetoresNome: TWideStringField;
    DsSetores: TDataSource;
    DsEntiCli2: TDataSource;
    QrEntiCli2: TmySQLQuery;
    QrEntiCli2Nome: TWideStringField;
    QrEntiCli2Codigo: TIntegerField;
    EdFornecedor: TdmkEditCB;
    Label27: TLabel;
    CBFornecedor: TdmkDBLookupComboBox;
    PMInclui: TPopupMenu;
    IncluinovoProdutoqumico1: TMenuItem;
    PMAltera: TPopupMenu;
    AlteracadastrodoProdutoqumico1: TMenuItem;
    PnEditCli: TPanel;
    Label28: TLabel;
    Label29: TLabel;
    Label36: TLabel;
    EdEstSegur: TdmkEdit;
    EdMinEstq: TdmkEdit;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    DsPQCli: TDataSource;
    QrPQCli: TmySQLQuery;
    QrPQCliNOMECLII: TWideStringField;
    QrPQCliControle: TIntegerField;
    QrPQCliPQ: TIntegerField;
    QrPQCliCI: TIntegerField;
    QrPQCliMinEstq: TFloatField;
    QrPQCliEstSegur: TIntegerField;
    QrPQCliLk: TIntegerField;
    QrPQCliDataCad: TDateField;
    QrPQCliDataAlt: TDateField;
    QrPQCliUserCad: TIntegerField;
    QrPQCliUserAlt: TIntegerField;
    IncluiClienteinterno1: TMenuItem;
    IncluiFornecedoraoclienteinterno1: TMenuItem;
    QrDuplFor: TmySQLQuery;
    AlteradadosdoClienteinterno1: TMenuItem;
    AlteradadosdoFornecedordoclienteinterno1: TMenuItem;
    PnEditCot: TPanel;
    Label3: TLabel;
    EdEmb: TdmkEditCB;
    CBEmb: TdmkDBLookupComboBox;
    QrEmbalagens: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsEmbalagens: TDataSource;
    TPDataCot: TDateTimePicker;
    Label5: TLabel;
    RGMoeda: TRadioGroup;
    Label18: TLabel;
    EdPreco: TdmkEdit;
    Label23: TLabel;
    EdICMS: TdmkEdit;
    Label19: TLabel;
    EdIPI: TdmkEdit;
    Label20: TLabel;
    EdPIS: TdmkEdit;
    Label21: TLabel;
    EdCOFINS: TdmkEdit;
    IncluicoTaodepreo1: TMenuItem;
    Label6: TLabel;
    EdFrete: TdmkEdit;
    Label7: TLabel;
    EdRICMS: TdmkEdit;
    Label11: TLabel;
    EdRIPI: TdmkEdit;
    EdRPIS: TdmkEdit;
    Label12: TLabel;
    Label13: TLabel;
    EdRCOFINS: TdmkEdit;
    Label17: TLabel;
    EdReais: TdmkEdit;
    QrPQForNOMEIQ: TWideStringField;
    QrPQForControle: TIntegerField;
    QrPQForIQ: TIntegerField;
    QrPQForPQ: TIntegerField;
    QrPQForCI: TIntegerField;
    QrPQForPrazo: TWideStringField;
    QrPQForLk: TIntegerField;
    QrPQForDataCad: TDateField;
    QrPQForDataAlt: TDateField;
    QrPQForUserCad: TIntegerField;
    QrPQForUserAlt: TIntegerField;
    QrPQCot: TmySQLQuery;
    DsPQCot: TDataSource;
    DBEdit04: TDBEdit;
    Label22: TLabel;
    QrPQCotNOMEEMBLAGEM: TWideStringField;
    QrPQCotControle: TIntegerField;
    QrPQCotIQ: TIntegerField;
    QrPQCotPQ: TIntegerField;
    QrPQCotCI: TIntegerField;
    QrPQCotDataCot: TDateField;
    QrPQCotPreco: TFloatField;
    QrPQCotMoeda: TSmallintField;
    QrPQCotFrete: TFloatField;
    QrPQCotICMS: TFloatField;
    QrPQCotIPI: TFloatField;
    QrPQCotPIS: TFloatField;
    QrPQCotCOFINS: TFloatField;
    QrPQCotRICMS: TFloatField;
    QrPQCotRIPI: TFloatField;
    QrPQCotRPIS: TFloatField;
    QrPQCotRCOFINS: TFloatField;
    QrPQCotLk: TIntegerField;
    QrPQCotDataCad: TDateField;
    QrPQCotDataAlt: TDateField;
    QrPQCotUserCad: TIntegerField;
    QrPQCotUserAlt: TIntegerField;
    QrPQCotEmbalagem: TIntegerField;
    QrPQCotCusto: TFloatField;
    QrPQCotNOMEMOEDA: TWideStringField;
    AlteracoTaodepreo1: TMenuItem;
    Label25: TLabel;
    EdCusto: TdmkEdit;
    QrPQCotREAIS: TFloatField;
    EdSetor: TdmkEditCB;
    Label26: TLabel;
    CBSetor: TdmkDBLookupComboBox;
    EdGrupo: TdmkEditCB;
    Label30: TLabel;
    CBGrupo: TdmkDBLookupComboBox;
    QrGruposQuimicos: TmySQLQuery;
    DsGruposQuimicos: TDataSource;
    QrGruposQuimicosCodigo: TIntegerField;
    QrGruposQuimicosNome: TWideStringField;
    QrPQGrupoQuimico: TIntegerField;
    QrPQSetor: TIntegerField;
    QrPQNOMESETOR: TWideStringField;
    Label31: TLabel;
    DBEdit1: TDBEdit;
    QrPQNOMEGRUPOQUIMICO: TWideStringField;
    Label32: TLabel;
    DBEdit2: TDBEdit;
    QrPQCliCustoPadrao: TFloatField;
    RGMoedaPadrao: TRadioGroup;
    QrPQCliMoedaPadrao: TIntegerField;
    QrPQCliNOMEMOEDAPADRAO: TWideStringField;
    QrPQCliPeso: TFloatField;
    QrPQCliValor: TFloatField;
    QrPQCliCusto: TFloatField;
    PMExclui: TPopupMenu;
    Fornecedordoclienteinterno1: TMenuItem;
    Clienteinterno1: TMenuItem;
    CGReceitas: TdmkCheckGroup;
    QrPQReceitas: TIntegerField;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    QrPQPercQuebra: TFloatField;
    EdPercQuebra: TdmkEdit;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit3: TDBEdit;
    SpeedButton9: TSpeedButton;
    SpeedButton10: TSpeedButton;
    IncluiEmbalagemaoFornecedor1: TMenuItem;
    QrPQForEmb: TmySQLQuery;
    StringField2: TWideStringField;
    DsPQForEmb: TDataSource;
    QrPQForEmbControle: TIntegerField;
    QrPQForEmbEmbalagem: TIntegerField;
    QrPQForEmbObservacao: TWideStringField;
    Alteraembalagemdefornecedorselecionada1: TMenuItem;
    QrPQForEmbcProd: TWideStringField;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label16: TLabel;
    EdICMS_pRedBC_: TdmkEdit;
    GroupBox3: TGroupBox;
    Label35: TLabel;
    EdIPI_pRedBC_: TdmkEdit;
    GroupBox4: TGroupBox;
    Label37: TLabel;
    EdPIS_pRedBC_: TdmkEdit;
    GroupBox5: TGroupBox;
    Label38: TLabel;
    EdCOFINS_pRedBC_: TdmkEdit;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    Label39: TLabel;
    GroupBox8: TGroupBox;
    Label40: TLabel;
    GroupBox9: TGroupBox;
    Label41: TLabel;
    GroupBox10: TGroupBox;
    Label42: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    TSGrade: TTabSheet;
    CotaodePreo1: TMenuItem;
    TSTabEstoque: TTabSheet;
    DBRGGGXNiv2: TDBRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfCliFor: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma2: TBitBtn;
    BtPadrao: TBitBtn;
    BtDesiste2: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    Panel9: TPanel;
    BtConfirma: TBitBtn;
    Label34: TLabel;
    BtDesiste: TBitBtn;
    QrPQCliCodProprio: TWideStringField;
    BtGraGruN: TBitBtn;
    DBCheckBox2: TDBCheckBox;
    GridPanel1: TGridPanel;
    Panel2: TPanel;
    Panel4: TPanel;
    DBGrid2: TDBGrid;
    DBGrid1: TDBGrid;
    DBGrid4: TDBGrid;
    DBGrid3: TDBGrid;
    GridPanel2: TGridPanel;
    GroupBox11: TGroupBox;
    GroupBox12: TGroupBox;
    DBGrid5: TdmkDBGrid;
    DBGrid6: TdmkDBGrid;
    QrFormulasIts: TmySQLQuery;
    QrFormulasItsNumero: TIntegerField;
    QrFormulasItsOrdem: TIntegerField;
    QrFormulasItsControle: TIntegerField;
    QrFormulasItsPorcent: TFloatField;
    QrFormulasItsNO_FRM: TWideStringField;
    DsFormulasIts: TDataSource;
    QrTintasIts: TmySQLQuery;
    QrTintasItsNumero: TIntegerField;
    QrTintasItsOrdem: TIntegerField;
    QrTintasItsControle: TIntegerField;
    QrTintasItsGramasTi: TFloatField;
    QrTintasItsGramasKg: TFloatField;
    QrTintasItsNO_FRM: TWideStringField;
    QrTintasItsNO_TIN: TWideStringField;
    QrTintasItsCodigo: TIntegerField;
    DsTintasIts: TDataSource;
    TabSheet4: TTabSheet;
    DBGrid7: TDBGrid;
    QrPQE: TmySQLQuery;
    QrPQECodigo: TIntegerField;
    QrPQEControle: TIntegerField;
    QrPQEConta: TIntegerField;
    QrPQEInsumo: TIntegerField;
    QrPQEVolumes: TIntegerField;
    QrPQEPesoVB: TFloatField;
    QrPQEPesoVL: TFloatField;
    QrPQEValorItem: TFloatField;
    QrPQEIPI: TFloatField;
    QrPQERIPI: TFloatField;
    QrPQETotalCusto: TFloatField;
    QrPQETotalPeso: TFloatField;
    QrPQECFin: TFloatField;
    QrPQEICMS: TFloatField;
    QrPQERICMS: TFloatField;
    QrPQENOMEFORNECEDOR: TWideStringField;
    QrPQENOMEINSUMO: TWideStringField;
    QrPQENF: TIntegerField;
    QrPQEData: TDateField;
    QrPQEConhecimento: TIntegerField;
    QrPQENF_RP: TIntegerField;
    QrPQENF_CC: TIntegerField;
    QrPQEIQ: TIntegerField;
    QrPQECI: TIntegerField;
    QrPQENOMECLIINT: TWideStringField;
    DsPQE: TDataSource;
    Panel10: TPanel;
    Label44: TLabel;
    Label45: TLabel;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    QrPQGGXNiv2: TIntegerField;
    QrPQNOMEGGXNiv2: TWideStringField;
    LaAviso: TLabel;
    QrFormulasItsAtivo: TSmallintField;
    QrTintasItsAtivo: TSmallintField;
    PMImprime: TPopupMenu;
    Diversos1: TMenuItem;
    PMNovo: TPopupMenu;
    Atualizaestoque1: TMenuItem;
    Atualizacustopadro1: TMenuItem;
    PB1: TProgressBar;
    QrPQCliCustoPadraoAtz: TSmallintField;
    RGCustoPadraoAtz: TRadioGroup;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    SBUnidMed: TSpeedButton;
    Label46: TLabel;
    EdUnidMed: TdmkEditCB;
    VUUnidMed: TdmkValUsu;
    Label33: TLabel;
    Label43: TLabel;
    EdCodProprio: TdmkEdit;
    EdCustoPadrao: TdmkEdit;
    Label47: TLabel;
    EdNCM: TdmkEdit;
    EdKgLiqEmb: TdmkEdit;
    Label48: TLabel;
    QrPQCliKgLiqEmb: TFloatField;
    EdKgUsoDd: TdmkEdit;
    Label49: TLabel;
    QrPQKgUsoDd: TFloatField;
    Atualizaconsumodirio1: TMenuItem;
    QrPQKgEstqSegur: TFloatField;
    Label50: TLabel;
    EdKgEstqSegur: TdmkEdit;
    Label51: TLabel;
    DBEdit8: TDBEdit;
    Label52: TLabel;
    DBEdit9: TDBEdit;
    CkUsarDensid: TdmkCheckBox;
    Label53: TLabel;
    EdDensidade: TdmkEdit;
    QrPQUsarDensid: TSmallintField;
    QrPQDensidade: TFloatField;
    DBCheckBox1: TDBCheckBox;
    Label54: TLabel;
    DBEdit10: TDBEdit;
    RGMoedaMan: TRadioGroup;
    EdCustoMan: TdmkEdit;
    Label55: TLabel;
    QrPQCliCustoMan: TFloatField;
    QrPQCliMoedaMan: TSmallintField;
    Label56: TLabel;
    EdRelevancia: TdmkEdit;
    QrPQForRelevancia: TSmallintField;
    QrEntiFor2NOME_E_DOC_ENTIDADE: TWideStringField;
    EdCusIntrn: TdmkEdit;
    LaCusIntrn: TLabel;
    Label58: TLabel;
    QrPQCusIntrn: TFloatField;
    Label59: TLabel;
    DBEdit11: TDBEdit;
    CkAtivo: TdmkCheckBox;
    CorrigeGrupoemmassa1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPQAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrPQAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPQBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure IncluinovoProdutoqumico1Click(Sender: TObject);
    procedure AlteracadastrodoProdutoqumico1Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtDesiste2Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure IncluiClienteinterno1Click(Sender: TObject);
    procedure IncluiFornecedoraoclienteinterno1Click(Sender: TObject);
    procedure QrPQCliAfterScroll(DataSet: TDataSet);
    procedure EdCOFINSExit(Sender: TObject);
    procedure PMIncluiPopup(Sender: TObject);
    procedure AlteradadosdoClienteinterno1Click(Sender: TObject);
    procedure AlteradadosdoFornecedordoclienteinterno1Click(
      Sender: TObject);
    procedure PMAlteraPopup(Sender: TObject);
    procedure IncluicoTaodepreo1Click(Sender: TObject);
    procedure EdPrecoExit(Sender: TObject);
    procedure EdFreteExit(Sender: TObject);
    procedure EdICMSExit(Sender: TObject);
    procedure EdIPIExit(Sender: TObject);
    procedure EdPISExit(Sender: TObject);
    procedure EdRICMSExit(Sender: TObject);
    procedure EdRIPIExit(Sender: TObject);
    procedure EdRPISExit(Sender: TObject);
    procedure EdRCOFINSExit(Sender: TObject);
    procedure QrPQCalcFields(DataSet: TDataSet);
    procedure QrPQCotCalcFields(DataSet: TDataSet);
    procedure RGMoedaClick(Sender: TObject);
    procedure AlteracoTaodepreo1Click(Sender: TObject);
    procedure QrPQForAfterScroll(DataSet: TDataSet);
    procedure QrPQCliAfterOpen(DataSet: TDataSet);
    procedure QrPQForAfterOpen(DataSet: TDataSet);
    procedure EdCustoPadraoExit(Sender: TObject);
    procedure QrPQCliCalcFields(DataSet: TDataSet);
    procedure BtPadraoClick(Sender: TObject);
    procedure EdPrazoExit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure Fornecedordoclienteinterno1Click(Sender: TObject);
    procedure Clienteinterno1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure QrPQBeforeClose(DataSet: TDataSet);
    procedure SpeedButton8Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure QrPQForBeforeClose(DataSet: TDataSet);
    procedure IncluiEmbalagemaoFornecedor1Click(Sender: TObject);
    procedure Alteraembalagemdefornecedorselecionada1Click(Sender: TObject);
    procedure RGGGXNiv2Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure CotaodePreo1Click(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtGraGruNClick(Sender: TObject);
    procedure DBGrid5DblClick(Sender: TObject);
    procedure DBGrid6DblClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure TPIniExit(Sender: TObject);
    procedure TPFimExit(Sender: TObject);
    procedure DBGrid7DblClick(Sender: TObject);
    procedure Diversos1Click(Sender: TObject);
    procedure Atualizaestoque1Click(Sender: TObject);
    procedure Atualizacustopadro1Click(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBUnidMedClick(Sender: TObject);
    procedure EdNCMRedefinido(Sender: TObject);
    procedure Atualizaconsumodirio1Click(Sender: TObject);
    procedure CorrigeGrupoemmassa1Click(Sender: TObject);
  private
    FCli, FFor, FCot: Integer;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenPQCli();
    procedure ReopenPQFor;
    procedure ReopenPQCot;
    procedure CalculaCustoPQ;
    procedure ReopenFormulas(FormulasIts, TintasIts: Integer);
    procedure ConfiguraPQE();
    procedure ReopenPQE(Codigo: Integer);
    procedure PQCotInsUpd((*DataCot: String; Controle, IQ, PQ, CI, Embalagem,
              Moeda, Empresa: Integer; Preco, Frete, ICMS, IPI, PIS, COFINS,
              RICMS, RIPI, RPIS, RCOFINS, Custo: Double; SQLType: TSQLType*));
    procedure MostraSubForm(SubForm: TSubFormPQ);
  public
    { Public declarations }
    FMostraSubForm: TSubFormPQ;
    FCodLoc, FEmpresa: Integer;
    FNewNom: String;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPQForEmb(cProd: String);
  end;

var
  FmPQ: TFmPQ;
const
  FFormatFloat = '000000';

implementation

uses UnMyObjects, Module, PQx, Principal, GruposQuimicos, MyDBCheck, UnMoedas,
  ListaSetores, ModProd, Embalagens, PQForEmb, ModuleGeral, ModEmit, UnGrade_Jan,
  UnidMed, UnPQ_PF, PQUsoDiaAtz, UnApp_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPQ.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPQ.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPQCodigo.Value, LaRegistro.Caption[2]);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPQ.DefParams;
begin
  VAR_GOTOTABELA := 'PQ';
  VAR_GOTOMYSQLTABLE := QrPQ;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial');
  VAR_SQLx.Add('ELSE fo.Nome END NOMEIQ, ls.Nome NOMESETOR,');
  VAR_SQLx.Add('gq.Nome NOMEGRUPOQUIMICO, pq.*');
  VAR_SQLx.Add('FROM pq pq');
  VAR_SQLx.Add('LEFT JOIN entidades      fo ON fo.Codigo=pq.IQ');
  VAR_SQLx.Add('LEFT JOIN listasetores   ls ON ls.Codigo=pq.Setor');
  VAR_SQLx.Add('LEFT JOIN gruposquimicos gq ON gq.Codigo=pq.GrupoQuimico');
  VAR_SQLx.Add('WHERE pq.Codigo <> -1000');
  //
  VAR_SQL1.Add('AND pq.Codigo=:P0');
  //
  VAR_SQLa.Add('AND pq.Nome Like :P0');
  //
end;

procedure TFmPQ.Diversos1Click(Sender: TObject);
begin
  FmPrincipal.CadastroPQImp('FmPQ');
end;

procedure TFmPQ.ReopenFormulas(FormulasIts, TintasIts: Integer);
var
  Produto: Integer;
begin
  Produto := QrPQCodigo.Value;
  //
  if Produto <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrFormulasIts, Dmod.MyDb, [
      'SELECT foi.Numero, foi.Ordem, foi.Controle, foi.Porcent, ',
      'frm.Nome NO_FRM, foi.Ativo ',
      'FROM formulasits foi ',
      'LEFT JOIN formulas frm ON frm.Numero=foi.Numero ',
      'WHERE foi.Produto=' + Geral.FF0(Produto),
      'ORDER BY foi.Numero, foi.Ordem ',
      '']);
    if FormulasIts <> 0 then
      QrFormulasIts.Locate('Numero', FormulasIts, []);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrTintasIts, Dmod.MyDb, [
      'SELECT foi.Numero, foi.Ordem, foi.Ativo, ',
      'foi.Codigo, foi.Controle, foi.GramasTi, ',
      'foi.GramasKg, frm.Nome NO_FRM, tin.Nome NO_TIN ',
      'FROM tintasits foi ',
      'LEFT JOIN tintascab frm ON frm.Numero=foi.Numero ',
      'LEFT JOIN tintastin tin ON tin.Codigo=foi.Codigo ',
      'WHERE foi.Produto=' + Geral.FF0(Produto),
      'ORDER BY foi.Numero, tin.Ordem, foi.Ordem ',
      '']);
    if TintasIts <> 0 then
      QrTintasIts.Locate('Controle', TintasIts, []);
  end else
  begin
    QrFormulasIts.Close;
    QrTintasIts.Close;
  end;
end;

procedure TFmPQ.ReopenPQE(Codigo: Integer);
var
  Insumo: Integer;
  Ini, Fim: String;
begin
  Screen.Cursor := crHourGlass;
  try
    if Insumo <> 0 then
    begin
      Ini    := Geral.FDT(TPIni.Date, 1);
      Fim    := Geral.FDT(TPFim.Date, 1);
      Insumo := QrPQCodigo.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQE, Dmod.MyDB, [
        'SELECT ei.*,  ',
        'CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial ',
        'ELSE ci.Nome END NOMECLIINT, ',
        'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
        'ELSE fo.Nome END NOMEFORNECEDOR, ',
        'pq.Nome NOMEINSUMO, pe.NF, pe.Data, ',
        'pe.Conhecimento, pe.NF_RP, pe.NF_CC, pe.IQ, pe.CI ',
        'FROM pqe pe ',
        'LEFT JOIN pqeIts ei ON ei.Codigo=pe.Codigo ',
        'LEFT JOIN Entidades ci ON ci.Codigo=pe.CI ',
        'LEFT JOIN Entidades fo ON fo.Codigo=pe.IQ ',
        'LEFT JOIN pq pq ON pq.Codigo=ei.Insumo ',
        'WHERE pe.Data BETWEEN "' + Ini + '" AND "' + Fim + '" ',
        'AND ei.Insumo=' + Geral.FF0(Insumo),
        'ORDER BY pe.Data DESC ',
        '']);
      if Codigo <> 0 then
        QrPQE.Locate('Codigo', Codigo, []);
    end else
      QrPQE.Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPQ.ConfiguraPQE();
begin
  TPIni.Date := Geral.PrimeiroDiaDoMes(DmodG.ObtemAgora());
  TPFim.Date := Geral.UltimoDiaDoMes(DmodG.ObtemAgora());
  //
  ReopenPQE(0);
end;

procedure TFmPQ.CorrigeGrupoemmassa1Click(Sender: TObject);
begin
  App_Jan.MostraFormPQCorrigeGGXNiv2();
end;

procedure TFmPQ.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
var
  Sigla, NCM: String;
  Tipo_Item, GraGruX, UnidMed: Integer;
  Habilita: Boolean;
begin
  case Mostra of
    0:
    begin
      GBCntrl.Visible :=True;
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      GBConfCliFor.Visible   := False;
      PnEditCli.Visible      := False;
      PnEditFor.Visible      := False;
      PnEditCot.Visible      := False;
      BtPadrao.Visible       := False;
    end;
    1:
    begin
      {
      if Dmod.QrControlePQ_PrdGrup Tip.Value = 0 then
      begin
        Geral.MB_Aviso('Tipo de Grupo de Produtos n�o definido ' +
        'nas op��es espec�ficas do aplicativo!'));
        Exit;
      end;
      }
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      if SQLType = stIns then
      begin
        if FNewNom = '' then
          FNewNom := 'Uso e Consumo';
        EdCodigo.ValueVariant       := 0;
        EdNome.Text                 := FNewNom;
        EdIQ.Text                   := '';
        CBIQ.KeyValue               := NULL;
        EdSetor.Text                := '';
        CBSetor.KeyValue            := NULL;
        EdGrupo.Text                := '';
        CBGrupo.KeyValue            := NULL;
        RGGGXNiv2.ItemIndex         := 2;
        CGReceitas.Value            := 31;
        EdPercQuebra.ValueVariant   := 2;
        EdKgUsoDd.ValueVariant      := 0.000;
        EdKgEstqSegur.ValueVariant  := 0.000;
        CkUsarDensid.Checked        := False;
        EdDensidade.ValueVariant    := 0.000;
        EdCusIntrn.ValueVariant     := 0.0000;
        CkAtivo.Checked             := True;
        EdNCM.Text                  := '';
        EdUnidMed.ValueVariant      := 0;
        EdSigla.Text                := '';
        CBUnidMed.KeyValue          := Null;
        //
        EdCusIntrn.Enabled := False;
        LaCusIntrn.Enabled := False;
        //
        DmProd.FPrdGrupTip_Nome     := '';
      end else begin
        EdCodigo.ValueVariant        := QrPQCodigo.Value;
        EdNome.Text                  := QrPQNome.Value;
        EdIQ.Text                    := Geral.FF0(QrPQIQ.Value);
        CBIQ.KeyValue                := QrPQIQ.Value;
        EdSetor.Text                 := Geral.FF0(QrPQSetor.Value);
        CBSetor.KeyValue             := QrPQSetor.Value;
        EdGrupo.Text                 := Geral.FF0(QrPQGrupoQuimico.Value);
        CBGrupo.KeyValue             := QrPQGrupoQuimico.Value;
        RGGGXNiv2.ItemIndex          := QrPQGGXNiv2.Value;
        CGReceitas.Value             := QrPQReceitas.Value;
        EdPercQuebra.ValueVariant    := QrPQPercQuebra.Value;
        EdKgUsoDd.ValueVariant       := QrPQKgUsoDd.Value;
        EdKgEstqSegur.ValueVariant   := QrPQKgEstqSegur.Value;
        CkUsarDensid.Checked         := QrPQUsarDensid.Value = 1;
        EdDensidade.ValueVariant     := QrPQDensidade.Value;
        EdCusIntrn.ValueVariant      := QrPQCusIntrn.Value;
        CkAtivo.Checked              := Geral.IntToBool(QrPQAtivo.Value);
        //
        Habilita := QrPQCodigo.Value < 0;
        EdCusIntrn.Enabled := Habilita;
        LaCusIntrn.Enabled := Habilita;
        //
        PQ_PF.ObtemDadosGradeDePQ(QrPQCodigo.Value, Tipo_Item, GraGruX,
          UnidMed, Sigla, NCM, True);
        EdNCM.Text                := NCM;
        EdSigla.Text              := Sigla;
        EdUnidMed.ValueVariant    := UnidMed;
        CBUnidMed.KeyValue        := UnidMed;

      end;
      EdNome.SetFocus;
    end;
    2:
    begin
      GBConfCliFor.Visible   := True;
      PnEditCli.Visible     := True;
      GBCntrl.Visible := False;
      if SQLType = stIns then
      begin
        EdCI.Text                  := '';
        CBCI.KeyValue              := NULL;
        EdEstSegur.Text            := '10';
        EdMinEstq.Text             := '';
        EdCustoPadrao.Text         := '';
        RGCustoPadraoAtz.ItemIndex := 0;
        EdCustoMan.ValueVariant    := 0.0000;
        RGMoedaMan.ItemIndex       := 0;
        RGMoedaPadrao.Itemindex    := 3;
        EdCodProprio.Text          := '';
        EdKgLiqEmb.ValueVariant    := 0.000;
      end else
      begin
        EdCI.Text                  := Geral.FF0(QrPQCliCI.Value);
        CBCI.KeyValue              := QrPQCliCI.Value;
        EdEstSegur.Text            := Geral.FF0(QrPQCliEstSegur.Value);
        EdMinEstq.Text             := Geral.FFT(QrPQCliMinEstq.Value, 3, siPositivo);
        EdCustoPadrao.Text         := Geral.FFT(QrPQCliCustoPadrao.Value, 4, siPositivo);
        RGCustoPadraoAtz.ItemIndex := QrPQCliCustoPadraoAtz.Value;
        EdCustoMan.ValueVariant    := QrPQCliCustoMan.Value;
        RGMoedaMan.ItemIndex       := QrPQCliMoedaMan.Value;
        RGMoedaPadrao.Itemindex    := QrPQCliMoedaPadrao.Value;
        EdCodProprio.Text          := QrPQCliCodProprio.Value;
        EdKgLiqEmb.ValueVariant    := QrPQCliKgLiqEmb.Value;
      end;
      EdCI.SetFocus;
    end;
    3:
    begin
      GBConfCliFor.Visible := True;
      PnEditFor.Visible    := True;
      GBCntrl.Visible      := False;
      //
      if SQLType = stIns then
      begin
        EdFornecedor.Text     := '';
        CBFornecedor.KeyValue := NULL;
        EdPrazo.Text          := '';
        EdRelevancia.ValueVariant := 50;
      end else begin
        EdFornecedor.Text     := Geral.FF0(QrPQForIQ.Value);
        CBFornecedor.KeyValue := QrPQForIQ.Value;
        EdPrazo.Text          := QrPQForPrazo.Value;
        EdRelevancia.ValueVariant := QrPQForRelevancia.Value;
      end;
      EdFornecedor.SetFocus;
    end;
    4:
    begin
      GBConfCliFor.Visible := True;
      PnEditCot.Visible    := True;
      BtPadrao.Visible     := True;
      GBCntrl.Visible      := False;
      //
      if SQLType = stIns then
      begin
        EdEmb.Text        := '';
        CBEmb.KeyValue    := NULL;
        TPDataCot.Date    := Date;
        RGMoeda.ItemIndex := 0;
        EdPreco.Text      := '';
        EdFrete.Text      := '';
        EdICMS.Text       := '';
        EdIPI.Text        := '';
        EdPIS.Text        := '';
        EdCOFINS.Text     := '';
        EdRICMS.Text      := '';
        EdRIPI.Text       := '';
        EdRPIS.Text       := '';
        EdRCOFINS.Text    := '';
      end else begin
        EdEmb.Text        := Geral.FF0(QrPQCotEmbalagem.Value);
        CBEmb.KeyValue    := QrPQCotEmbalagem.Value;
        TPDataCot.Date    := QrPQCotDataCot.Value;
        RGMoeda.ItemIndex := QrPQCotMoeda.Value;
        EdPreco.Text      := Geral.FFT(QrPQCotPreco.Value, 4, siPositivo);
        EdFrete.Text      := Geral.FFT(QrPQCotFrete.Value, 4, siPositivo);
        EdICMS.Text       := Geral.FFT(QrPQCotICMS.Value, 2, siPositivo);
        EdIPI.Text        := Geral.FFT(QrPQCotIPI.Value, 2, siPositivo);
        EdPIS.Text        := Geral.FFT(QrPQCotPIS.Value, 2, siPositivo);
        EdCOFINS.Text     := Geral.FFT(QrPQCotCOFINS.Value, 2, siPositivo);
        EdRICMS.Text      := Geral.FFT(QrPQCotRICMS.Value, 2, siPositivo);
        EdRIPI.Text       := Geral.FFT(QrPQCotRIPI.Value, 2, siPositivo);
        EdRPIS.Text       := Geral.FFT(QrPQCotRPIS.Value, 2, siPositivo);
        EdRCOFINS.Text    := Geral.FFT(QrPQCotRCOFINS.Value, 2, siPositivo);
        CalculaCustoPQ;
      end;
      TPDataCot.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmPQ.MostraSubForm(SubForm: TSubFormPQ);
begin
  case SubForm of
    tsfpqNenhum:
      ;
    tsfpqCI:
      MostraEdicao(2, stIns, 0);
  end;
end;

procedure TFmPQ.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPQ.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPQ.DBGrid5DblClick(Sender: TObject);
var
  Numero, Controle: Integer;
begin
  if (QrFormulasIts.State <> dsInactive) and (QrFormulasIts.RecordCount > 0) then
  begin
    Numero   := QrFormulasItsNumero.Value;
    Controle := QrFormulasItsControle.Value;
    //
    FmPrincipal.CadastroFormulas(Numero, Controle);
    //
    ReopenFormulas(Numero, 0);
  end;
end;

procedure TFmPQ.DBGrid6DblClick(Sender: TObject);
var
  Numero, Codigo, Controle, FormulasIts: Integer;
begin
  if (QrTintasIts.State <> dsInactive) and (QrTintasIts.RecordCount > 0) then
  begin
    Numero   := QrTintasItsNumero.Value;
    Codigo   := QrTintasItsCodigo.Value;
    Controle := QrTintasItsControle.Value;
    //
    FmPrincipal.CadastroTintas(Numero, Codigo, Controle);
    //
    ReopenFormulas(0, Controle);
  end;
end;

procedure TFmPQ.DBGrid7DblClick(Sender: TObject);
begin
  if (QrPQE.State <> dsInactive) and (QrPQE.RecordCount > 0) then
  begin
    PQ_PF.MostraFormPQE(QrPQECodigo.Value);
    //
    ReopenPQE(QrPQECodigo.Value);
  end;
end;

procedure TFmPQ.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPQ.SpeedButton10Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEmbalagens, FmEmbalagens, afmoNegarComAviso) then
  begin
    FmEmbalagens.ShowModal;
    FmEmbalagens.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      QrEmbalagens.Close;
      UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
      EdEmb.Text     := Geral.FF0(VAR_CADASTRO);
      CBEmb.KeyValue := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmPQ.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPQ.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPQ.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPQ.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPQ.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  if VAR_CADASTRO <> 0 then
  begin
    QrEntiFor2.Close;
    UnDmkDAC_PF.AbreQuery(QrEntiFor2, Dmod.MyDB);
    EdIQ.Text         := Geral.FF0(VAR_CADASTRO);
    CBIQ.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmPQ.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmGruposQuimicos, FmGruposQuimicos, afmoNegarComAviso) then
  begin
    FmGruposQuimicos.ShowModal;
    FmGruposQuimicos.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    QrGruposQuimicos.Close;
    UnDmkDAC_PF.AbreQuery(QrGruposQuimicos, Dmod.MyDB);
    EdGrupo.Text         := Geral.FF0(VAR_CADASTRO);
    CBGrupo.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmPQ.SpeedButton7Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmListaSetores, FmListaSetores, afmoNegarComAviso) then
  begin
    FmListaSetores.ShowModal;
    FmListaSetores.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    QrSetores.Close;
    UnDmkDAC_PF.AbreQuery(QrSetores, Dmod.MyDB);
    EdSetor.Text         := Geral.FF0(VAR_CADASTRO);
    CBSetor.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmPQ.SpeedButton8Click(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := EdCI.ValueVariant;
  //
  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrEntiCli2.Close;
    UnDmkDAC_PF.AbreQuery(QrEntiCli2, Dmod.MyDB);
    EdCI.Text         := Geral.FF0(VAR_CADASTRO);
    CBCI.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmPQ.SpeedButton9Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  if VAR_CADASTRO <> 0 then
  begin
    QrEntiFor2.Close;
    UnDmkDAC_PF.AbreQuery(QrEntiFor2, Dmod.MyDB);
    EdFornecedor.Text     := Geral.FF0(VAR_CADASTRO);
    CBFornecedor.KeyValue := VAR_CADASTRO;
  end;
end;

procedure TFmPQ.TPFimExit(Sender: TObject);
begin
  ReopenPQE(0);
end;

procedure TFmPQ.TPIniExit(Sender: TObject);
begin
  ReopenPQE(0);
end;

procedure TFmPQ.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPQCodigo.Value;
  Close;
end;

procedure TFmPQ.BtConfirmaClick(Sender: TObject);
var
  Codigo, IQ, GGXNiv2, Ativo, UnidMed: Integer;
  Nome, NCM: String;
  //
  Nivel1, Nivel2, UsarDensid: Integer;
  KgUsoDd, KgEstqSegur, Densidade, CusIntrn: Double;
const
  ForcaCadastrGGX_SemCorTam = True;
begin
  Nome    := EdNome.Text;
  Nivel2  := - RGGGXNiv2.ItemIndex;
  GGXNiv2 := RGGGXNiv2.ItemIndex;
  Ativo   := Geral.BoolToInt(CkAtivo.Checked);
  KgUsoDd := EdKgUsoDd.ValueVariant;
  KgEstqSegur := EdKgEstqSegur.ValueVariant;
  UsarDensid  := Geral.BoolToInt(CkUsarDensid.Checked);
  Densidade   := EdDensidade.ValueVariant;
  CusIntrn    := EdCusIntrn.ValueVariant;
  //
  if Nivel2 = 0 then
    Nivel2 := -9;
  //
  if MyObjects.FIC(GGXNiv2 = 0, RGGGXNiv2, 'Informe o grupo!') then Exit;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Erro('Defina uma descri��o.');
    Exit;
  end;
  if ImgTipo.SQLType = stIns then
    if UnDmkDAC_PF.TextoJaCadastrado(Nome, 'Nome', 'PQ',  Dmod.MyDB) then
      Exit;
  IQ := Geral.IMV(EdIQ.Text);
  Dmod.QrUpdU.SQL.Clear;
  //
  // GraGru1
  UnidMed := VUUnidMed.ValueVariant;
  if MyObjects.FIC(UnidMed = 0, EdUnidMed, 'Informe a unidade de medida! (1)') then  ;
    //Exit;
  NCM := EdNCM.Text;
  //if MyObjects.FIC(NCM = '', EdNCM, 'Informe o NCM!') then ; //Exit;
  if MyObjects.FIC(NCM = '', nil, 'Informe o NCM!') then ; //Exit;
  // Fim GraGru1
  //
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO pq SET ');
    //
    Nivel1 := DmProd.GeraNovoCodigoNivel(1, -2, 1, False);
    //
    if Nivel1 = 0 then
    begin
      Geral.MB_Aviso('N�o foi poss�vel definir o c�digo do Insumo!');
      Exit;
    end;
    if not DmProd.InsereItemPrdGruNew(cggnFmPQ, -2, UnidMed, Nome, NCM,
    ForcaCadastrGGX_SemCorTam, 0, 0, 0, Nivel2, Nivel1) then
      Exit
    else
      Codigo := DmProd.FNewGraGru1;
      // Evitar tentativa de duplica��o!
      DmProd.FNewGraGru1 := 0;
      if Codigo = 0 then
      begin
        Geral.MB_Aviso('N�o foi poss�vel definir o c�digo do Insumo!');
        Exit;
      end;
    //Codigo := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'PQ', 'PQ', 'Codigo');
  end else
  begin
    Codigo := QrPQCodigo.Value;
    //
    if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False,
      ['Nome', 'Nivel2', 'UnidMed', 'NCM'], [
      'Nivel1'], [
      Nome, Nivel2, UnidMed, NCM], [
      Codigo], True) then
    begin
      Geral.MB_Erro('Falha ao atualizar produto!');
      Exit;
    end;
    //
    Dmod.QrUpdU.SQL.Add('UPDATE pq SET ');
  end;
  Dmod.QrUpdU.SQL.Add('Nome=:P0, IQ=:P1, GGXNiv2=:P2, Setor=:P3, ');
  Dmod.QrUpdU.SQL.Add('GrupoQuimico=:P4, Receitas=:P5, PercQuebra=:P6, ');
  Dmod.QrUpdU.SQL.Add('KgUsoDd=:P7, KgEstqSegur=:P8, ');
  Dmod.QrUpdU.SQL.Add('UsarDensid=:P9, Densidade=:P10, CusIntrn=:P11, ');
  Dmod.QrUpdU.SQL.Add('Ativo=:P12, ');
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := IQ;
  Dmod.QrUpdU.Params[02].AsInteger := RGGGXNiv2.ItemIndex;
  Dmod.QrUpdU.Params[03].AsInteger := Geral.IMV(EdSetor.Text);
  Dmod.QrUpdU.Params[04].AsInteger := Geral.IMV(EdGrupo.Text);
  Dmod.QrUpdU.Params[05].AsInteger := CGReceitas.Value;
  Dmod.QrUpdU.Params[06].AsFloat   := EdPercQuebra.ValueVariant;
  Dmod.QrUpdU.Params[07].AsFloat   := KgUsoDd;
  Dmod.QrUpdU.Params[08].AsFloat   := KgEstqSegur;
  Dmod.QrUpdU.Params[09].AsInteger := UsarDensid;
  Dmod.QrUpdU.Params[10].AsFloat   := Densidade;
  Dmod.QrUpdU.Params[11].AsFloat   := CusIntrn;
  Dmod.QrUpdU.Params[12].AsInteger := Ativo;
  Dmod.QrUpdU.Params[13].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[14].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[15].AsInteger := Codigo;
  Dmod.QrUpdU.Database:= Dmod.MyDB;
  UMyMod.ExecutaQuery(Dmod.QrUpdU);//.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PQ', 'Codigo');
  MostraEdicao(0, stLok, 0);
///////////////////////// Inicio 2019-01-19 ////////////////////////////////////
  AppPF.CorrigeReduzidosDuplicadosDeInsumo(False);
////////////////////////// Fim 2019-01-19 //////////////////////////////////////
  LocCod(Codigo,Codigo);
  //
end;

procedure TFmPQ.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PQ', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmPQ.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  //
  FNewNom        := '';
  FMostraSubForm := tsfpqNenhum;
  //
  MyObjects.PreencheComponente(RGGGXNiv2, sListaTipoCadPQ, 4);
  MyObjects.PreencheComponente(DBRGGGXNiv2, sListaTipoCadPQ, 4);
  UnDmkDAC_PF.AbreQuery(QrEntiFor2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntiCli2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSetores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGruposQuimicos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  //
  Panel4.Height           := 95;
  DmProd.FPrdGrupTip_Nome := '';
  TPDataCot.Date          := Date;
  //
  RGMoeda.Items.Clear;
  RGMoeda.Items.AddStrings(UMoedas.ObtemListaMoedas());
  //
  RGMoedaPadrao.Items.Clear;
  RGMoedaPadrao.Items.AddStrings(UMoedas.ObtemListaMoedas());
  //
  TSGrade.TabVisible      := False;
  TSTabEstoque.TabVisible := False;
  //
  DBGrid7.DataSource := DsPQE;
  DBGrid5.DataSource := DsFormulasIts;
  DBGrid6.DataSource := DsTintasIts;
  //
  PainelData.Align   := alTop;
  PainelEdita.Align  := alClient;
  PainelDados.Align  := alClient;
  PainelEdit.Align   := alClient;
  GBConfCliFor.Align := alBottom;//alClient;
  PageControl1.Align := alClient;
  LaRegistro.Align   := alClient;
  //
  PageControl1.ActivePageIndex := 0;
  //
  CriaOForm;
end;

procedure TFmPQ.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPQCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPQ.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmPQ.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPQ.SbNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNovo, SbNovo);
end;

procedure TFmPQ.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_CADASTRO := QrPQCodigo.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQ.QrPQAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  if QrPQ.RecordCount = 0 then ReopenPQCli;
end;

procedure TFmPQ.FormActivate(Sender: TObject);
var
  PQ: Integer;
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'PQ', 'Livres', 99) then
  //BtInclui.Enabled := False;
  //if VAR_LIB_EMPRESA_SEL = 0 then
  if FEmpresa = 0 then
  begin
    Geral.MB_Erro('Nenhuma empresa est� logada!');
    Close;
    Exit;
  end;
  if FCodLoc <> 0 then
  begin
    PQ := FCodLoc;
    FCodLoc := 0;
    LocCod(PQ, PQ);
  end;
  MostraSubForm(FMostraSubForm);
end;

procedure TFmPQ.QrPQAfterScroll(DataSet: TDataSet);
begin
  ReopenPQCli;
  ReopenFormulas(0, 0);
  //
  if PageControl1.ActivePageIndex = 1 then
    ReopenPQE(0);
  //
  BtAltera.Enabled := QrPQ.RecordCount > 0;
end;

procedure TFmPQ.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPQCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PQ', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPQ.SBUnidMedClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VUUnidMed.ValueVariant;
  UMedi_PF.CriaEEscolheUnidMed(Codigo, EdUnidMed, CBUnidMed, QrUnidMed);
end;

procedure TFmPQ.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQ.QrPQBeforeClose(DataSet: TDataSet);
begin
  QrPQCli.Close;
  QrFormulasIts.Close;
  QrTintasIts.Close;
  QrPQE.Close;
  //
  BtAltera.Enabled := False;
end;

procedure TFmPQ.QrPQBeforeOpen(DataSet: TDataSet);
begin
  QrPQCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPQ.ReopenPQCli;
begin
(*
  QrPQCli.Close;
  QrPQCli.Params[0].AsInteger := QrPQCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPQCli, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQCli, Dmod.MyDB, [
    'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLII, pci.*  ',
    'FROM pqcli pci ',
    'LEFT JOIN entidades cli ON cli.Codigo=pci.CI ',
    'WHERE pci.PQ=' + Geral.FF0(QrPQCodigo.Value),
    'AND pci.Empresa=' + Geral.FF0(FEmpresa),
    'ORDER BY NOMECLII ',
    '']);
  //
  if FCli > 0 then
    QrPQCli.Locate('Controle', FCli, []);
end;

procedure TFmPQ.ReopenPQFor;
begin
(*
  QrPQFor.Close;
  QrPQFor.Params[0].AsInteger := QrPQCodigo.Value;
  QrPQFor.Params[1].AsInteger := QrPQCliCI.Value;
  UnDmkDAC_PF.AbreQuery(QrPQFor, Dmod.MyDB);
  //
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQFor, Dmod.MyDB, [
  'SELECT IF(fiq.Tipo=0, fiq.RazaoSocial, fiq.Nome) NOMEIQ, piq.*  ',
  'FROM pqfor piq ',
  'LEFT JOIN entidades fiq ON fiq.Codigo=piq.IQ ',
  'WHERE piq.PQ=' + Geral.FF0(QrPQCodigo.Value),
  'AND piq.CI=' + Geral.FF0(QrPQCliCI.Value),
  'AND piq.Empresa=' + Geral.FF0(FEmpresa),
  'ORDER BY NOMEIQ ',
  '']);
  //
  if FFor > 0 then QrPQFor.Locate('Controle', FFor, []);
end;

procedure TFmPQ.ReopenPQForEmb(cProd: String);
begin
  {  Tabela n�o existe mais! buscar do FmGraGruN
  QrPQForEmb.Close;
  QrPQForEmb.Params[0].AsInteger := QrPQForControle.Value;
  UnDmkDAC_PF.AbreQuery(QrPQForEmb, Dmod.MyDB);
  //
  QrPQForEmb.Locate('cProd', cProd, []);
  }
end;

procedure TFmPQ.ReopenPQCot;
begin
(*
  QrPQCot.Close;
  QrPQCot.Params[0].AsInteger := QrPQCodigo.Value;
  QrPQCot.Params[1].AsInteger := QrPQCliCI.Value;
  QrPQCot.Params[2].AsInteger := QrPQForIQ.Value;
  UnDmkDAC_PF.AbreQuery(QrPQCot, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQCot, Dmod.MyDB, [
  'SELECT emb.Nome NOMEEMBLAGEM, cot.*',
  'FROM pqcot cot',
  'LEFT JOIN embalagens emb ON emb.Codigo=cot.Embalagem',
  'WHERE cot.PQ=' + Geral.FF0(QrPQCodigo.Value),
  'AND cot.CI=' + Geral.FF0(QrPQCliCI.Value),
  'AND cot.IQ=' + Geral.FF0(QrPQForIQ.Value),
  'AND cot.Empresa=' + Geral.FF0(FEmpresa),
  'ORDER BY cot.DataCot DESC',
  '']);
  //
  if FCot > 0 then QrPQCot.Locate('Controle', FCot, []);
end;

procedure TFmPQ.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmPQ.IncluinovoProdutoqumico1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmPQ.AlteracadastrodoProdutoqumico1Click(Sender: TObject);
var
  PQ : Integer;
begin
  PQ := QrPQCodigo.Value;
  if QrPQCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(PQ, Dmod.MyDB, 'PQ', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PQ, Dmod.MyDB, 'PQ', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPQ.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
  //MostraEdicao(1, stIns, 0);
end;

procedure TFmPQ.BtDesiste2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmPQ.BtConfirma2Click(Sender: TObject);
var
  EstSegur, PQ, CI, IQ, Controle, GraGru1, GraGruC, GraTamI, MoedaPadrao,
  CustoPadraoAtz, MoedaMan, Relevancia: Integer;
  MinEstq, CustoPadrao, CustoMan: Double;
  //
  SQLType: TSQLType;
  Prazo, CodProprio: String;
  KgLiqEmb: Double;
begin
  if PnEditCli.Visible then
  begin
    EstSegur       := Geral.IMV(EdEstSegur.Text);
    PQ             := QrPQCodigo.Value;
    CI             := Geral.IMV(EdCI.Text);
    MinEstq        := Geral.DMV(EdMinEstq.Text);
    CustoPadrao    := Geral.DMV(EdCustoPadrao.Text);
    CustoPadraoAtz := RGCustoPadraoAtz.ItemIndex;
    MoedaPadrao    := RGMoedaPadrao.ItemIndex;
    CustoMan       := Geral.DMV(EdCustoMan.Text);
    MoedaMan       := RGMoedaMan.ItemIndex;
    CodProprio     := EdCodProprio.Text;
    KgLiqEmb       := EdKgLiqEmb.ValueVariant;
    //
    if UnPQx.PQ_IncluiClienteInterno(ImgTipo.SQLType, PQ, FEmpresa, CI,
    MoedaPadrao, CustoPadraoAtz, MoedaMan, EstSegur, QrPQCliControle.Value,
    MinEstq, CustoPadrao, CustoMan, KgLiqEmb, CBCI.Text, CodProprio, FCli) then
      ReopenPQCli();
    {
    if ImgTipo.SQLType = stIns then
    begin
      (*
      QrDuplCli.Close;
      QrDuplCli.Params[0].AsInteger := PQ;
      QrDuplCli.Params[1].AsInteger := CI;
      UnDmkDAC_PF.AbreQuery(QrDuplCli, Dmod.MyDB);
      *)
      UnDmkDAC_PF.AbreMySQLQuery0(QrDuplCli, Dmod.MyDB, [
      'SELECT * ',
      'FROM pqcli ',
      'WHERE PQ=' + Geral.FF0(PQ),
      'AND CI=' + Geral.FF0(CI),
      'AND Empresa=' + Geral.FF0(FEmpresa),
      '']);
      if MyObjects.FIC(QrDuplCli.RecordCount > 0, nil,
      'Cliente interno j� cadastrado para o insumo na empresa logada!') then
        Exit;
      //
      GraGru1 := QrPqCodigo.Value;
      // 2011-05-07
      Controle := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', stIns, 0);
      QrLocGGX.Close;
      QrLocGGX.Params[0].AsInteger := GraGru1;
      UnDmkDAC_PF.AbreQuery(QrLocGGX, Dmod.MyDB);
      if QrLocGGX.RecordCount = 0 then
      begin
        GraGruC := DmProd.ObtemGraGruCDeIdx(GraGru1, 'PQCli', CBCI.Text);
        //
         //GraTamI := 1; // �nico
        // 2011-04-28
        GraTamI := 0; // �nico
        //Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'PQCli',
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False,
          ['GraGruC', 'GraGru1', 'GraTamI'], ['Controle'],
          [GraGruC, GraGru1, GraTamI], [Controle], True)
        then
          Controle := 0;
        if (GraGru1 = 0) or (GraGruC = 0) or (Controle = 0) then
        begin
          Geral.MB_Erro('N�o foi poss�vel gerar o item de grade!');
          Screen.Cursor := crDefault;
          Exit;
        end;
      end;
      // FIM 2011-05-07
      // 2014-07-24
      //Dmod.QrUpd.SQL.Clear;
      //Dmod.QrUpd.SQL.Add('INSERT INTO pqcli SET ');
    end else begin
      //Dmod.QrUpd.SQL.Clear;
      //Dmod.QrUpd.SQL.Add('UPDATE pqcli SET ');
      Controle := QrPQCliControle.Value;
    end;
    (*
    Dmod.QrUpd.SQL.Add('PQ=:P0, CI=:P1, MinEstq=:P2, EstSegur=:P3, ');
    Dmod.QrUpd.SQL.Add('CustoPadrao=:P4, MoedaPadrao=:P5, ');
    if ImgTipo.SQLType = stIns then
      Dmod.QrUpd.SQL.Add('DataCad=:Px, UserCad=:Py, Controle=:Pz')
    else Dmod.QrUpd.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Controle=:Pz');
    Dmod.QrUpd.Params[00].AsInteger := PQ;
    Dmod.QrUpd.Params[01].AsInteger := CI;
    Dmod.QrUpd.Params[02].AsFloat   := MinEstq;
    Dmod.QrUpd.Params[03].AsInteger := EstSegur;
    Dmod.QrUpd.Params[04].AsFloat   := CustoPadrao;
    Dmod.QrUpd.Params[05].AsInteger := MoedaPadrao;
    //
    Dmod.QrUpd.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpd.Params[07].AsInteger := VAR_USUARIO;
    Dmod.QrUpd.Params[08].AsInteger := Controle;
    UMyMod.ExecutaQuery(Dmod.QrUpd);
    *)
    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pqcli', False, [
      'PQ', 'CI', 'MinEstq',
      'EstSegur', 'CustoPadrao', 'CustoPadraoAtz', 'MoedaPadrao',
      //'Peso', 'Valor', 'Custo'
      'CodProprio', 'Empresa', 'KgLiqEmb',
      'CustoMan', 'MoedaMan'], [
      'Controle'], [
      PQ, CI, MinEstq,
      EstSegur, CustoPadrao, CustoPadraoAtz, MoedaPadrao,
      //Peso, Valor, Custo
      CodProprio, FEmpresa, KgLiqEmb,
      CustoMan, MoedaMan], [
      Controle], True);
    // FIM 2014-07-24
    FCli := Controle;
    }
  end else if PnEditFor.Visible then
  begin
    Dmod.QrUpd.SQL.Clear;
    PQ := QrPQCodigo.Value;
    CI := QrPQCliCI.Value;
    IQ := Geral.IMV(EdFornecedor.Text);
    Relevancia := EdRelevancia.ValueVariant;
    if ImgTipo.SQLType = stIns then
    begin
      (*QrDuplFor.Close;
      QrDuplFor.Params[0].AsInteger := PQ;
      QrDuplFor.Params[1].AsInteger := CI;
      QrDuplFor.Params[2].AsInteger := IQ;
      UnDmkDAC_PF.AbreQuery(QrDuplFor, Dmod.MyDB);*)
      UnDmkDAC_PF.AbreMySQLQuery0(QrDuplFor, Dmod.MyDB, [
      'SELECT * FROM pqfor',
      'WHERE PQ=' + Geral.FF0(PQ),
      'AND CI=' + Geral.FF0(CI),
      'AND IQ=' + Geral.FF0(IQ),
      'AND Empresa=' + Geral.FF0(FEmpresa),
      '']);
      if QrDuplFor.RecordCount > 0 then
      begin
        Geral.MB_Erro('Fornecedor j� cadastrado para o insumo / cliente interno!');
        Exit;
      end;
(*
      Dmod.QrUpd.SQL.Add('INSERT INTO pqfor SET ');
      Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'PQFor',
      'PQFor', 'Controle');
    end else begin
      Dmod.QrUpd.SQL.Add('UPDATE pqfor SET ');
      Controle := QrPQForControle.Value;
    end;
*)
      SQLType := stIns;
      //Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'PQFor',
      //'PQFor', 'Controle');
    end else begin
      SQLType := stUpd;
      Controle := QrPQForControle.Value;
    end;
(*
    Dmod.QrUpd.SQL.Add('PQ=:P0, CI=:P1, IQ=:P2, Prazo=:P3,');
    if ImgTipo.SQLType = stIns then
      Dmod.QrUpd.SQL.Add('DataCad=:Px, UserCad=:Py, Controle=:Pz')
    else Dmod.QrUpd.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Controle=:Pz');
    Dmod.QrUpd.Params[00].AsInteger := PQ;
    Dmod.QrUpd.Params[01].AsInteger := CI;
    Dmod.QrUpd.Params[02].AsInteger := IQ;
    Dmod.QrUpd.Params[03].AsString  := EdPrazo.Text;
    //
    Dmod.QrUpd.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpd.Params[05].AsInteger := VAR_USUARIO;
    Dmod.QrUpd.Params[06].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
*)
    Prazo          := EdPrazo.Text;
    //
    Controle := UMyMod.BuscaEmLivreY_Def('pqfor', 'Controle', SQLType, Controle);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqfor', False, [
    'IQ', 'PQ', 'CI',
    'Prazo', 'Empresa', 'Relevancia'], [
    'Controle'], [
    IQ, PQ, CI,
    Prazo, FEmpresa, Relevancia], [
    Controle], True) then
    begin
      FFor := Controle;
      FCli := QrPQCliControle.Value;
    end;
  end else if PnEditCot.Visible then
  begin
    PQCotInsUpd();
    (*
    Dmod.QrUpd.SQL.Clear;
    PQ := QrPQCodigo.Value;
    CI := QrPQCliCI.Value;
    IQ := QrPQForIQ.Value;
    if ImgTipo.SQLType = stIns then
    begin
      Dmod.QrUpd.SQL.Add('INSERT INTO pqcot SET ');
      Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'PQCot',
      'PQCot', 'Controle');
    end else begin
      Dmod.QrUpd.SQL.Add('UPDATE pqcot SET ');
      Controle := QrPQCotControle.Value;
    end;
    Dmod.QrUpd.SQL.Add('PQ=:P0, CI=:P1, IQ=:P2, DataCot=:P3, Moeda=:P4, ');
    Dmod.QrUpd.SQL.Add('Preco=:P5, ICMS=:P6, IPI=:P7, PIS=:P8, COFINS=:P9, ');
    Dmod.QrUpd.SQL.Add('Frete=:P10, RICMS=:P11, RIPI=:P12, RPIS=:P13, ');
    Dmod.QrUpd.SQL.Add('RCOFINS=:P14, Custo=:P15, Embalagem=:P16, ');
    if ImgTipo.SQLType = stIns then
      Dmod.QrUpd.SQL.Add('DataCad=:Px, UserCad=:Py, Controle=:Pz')
    else Dmod.QrUpd.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Controle=:Pz');
    Dmod.QrUpd.Params[00].AsInteger := PQ;
    Dmod.QrUpd.Params[01].AsInteger := CI;
    Dmod.QrUpd.Params[02].AsInteger := IQ;
    Dmod.QrUpd.Params[03].AsString  := Geral.FDT(TPDataCot.Date, 1);
    Dmod.QrUpd.Params[04].AsInteger := RGMoeda.ItemIndex;
    Dmod.QrUpd.Params[05].AsFloat  := Geral.DMV(EdPreco.Text);
    Dmod.QrUpd.Params[06].AsFloat  := Geral.DMV(EdICMS.Text);
    Dmod.QrUpd.Params[07].AsFloat  := Geral.DMV(EdIPI.Text);
    Dmod.QrUpd.Params[08].AsFloat  := Geral.DMV(EdPIS.Text);
    Dmod.QrUpd.Params[09].AsFloat  := Geral.DMV(EdCOFINS.Text);
    Dmod.QrUpd.Params[10].AsFloat  := Geral.DMV(EdFrete.Text);
    Dmod.QrUpd.Params[11].AsFloat  := Geral.DMV(EdRICMS.Text);
    Dmod.QrUpd.Params[12].AsFloat  := Geral.DMV(EdRIPI.Text);
    Dmod.QrUpd.Params[13].AsFloat  := Geral.DMV(EdRPIS.Text);
    Dmod.QrUpd.Params[14].AsFloat  := Geral.DMV(EdRCOFINS.Text);
    Dmod.QrUpd.Params[15].AsFloat  := Geral.DMV(EdCUSTO.Text);
    Dmod.QrUpd.Params[16].AsInteger := Geral.IMV(EdEmb.Text);
    //
    Dmod.QrUpd.Params[17].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpd.Params[18].AsInteger := VAR_USUARIO;
    Dmod.QrUpd.Params[19].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    FCot := Controle;
    FFor := QrPQForControle.Value;
    FCli := QrPQCliControle.Value;
    if SQLType = stIns then
      FNewNom := '';
    *)
  end else
  begin
    Geral.MB_Erro('A��o de confirma��o n�o definida!');
    Exit;
  end;
  MostraEdicao(0, stLok, QrPQCodigo.Value);
end;

procedure TFmPQ.QrPQCliAfterScroll(DataSet: TDataSet);
begin
  ReopenPQFor;
end;

procedure TFmPQ.IncluiClienteinterno1Click(Sender: TObject);
begin
  {
  if Dmod.QrControlePQ_PrdGrup Tip.Value = 0 then
  begin
    Geral.MB_Aviso(
      'Tipo de Grupo de Produto n�o definido nas op��es do aplicativo');
    Exit;
  end;
  }
  MostraSubForm(tsfpqCI);
end;

procedure TFmPQ.IncluiFornecedoraoclienteinterno1Click(Sender: TObject);
begin
  MostraEdicao(3, stIns, 0);
end;

procedure TFmPQ.AlteradadosdoClienteinterno1Click(Sender: TObject);
begin
  MostraEdicao(2, stUpd, 0);
end;

procedure TFmPQ.AlteradadosdoFornecedordoclienteinterno1Click(
  Sender: TObject);
begin
  MostraEdicao(3, stUpd, 0);
end;

procedure TFmPQ.Alteraembalagemdefornecedorselecionada1Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmPQForEmb, FmPQForEmb, afmoNegarComAviso) then
  begin
    FmPQForEmb.ImgTipo.SQLType           := stUpd;
    FmPQForEmb.EdcProd.Text             := QrPQForEmbcProd.Value;
    FmPQForEmb.EdObservacao.Text        := QrPQForEmbObservacao.Value;
    FmPQForEmb.EdEmbalagem.ValueVariant := QrPQForEmbEmbalagem.Value;
    FmPQForEmb.CBEmbalagem.KeyValue     := QrPQForEmbEmbalagem.Value;
    FmPQForEmb.ShowModal;
    FmPQForEmb.Destroy;
  end;
}
end;

procedure TFmPQ.Atualizaconsumodirio1Click(Sender: TObject);
begin
  App_Jan.MostraFormPQUsoDiaAtz();
end;

procedure TFmPQ.Atualizacustopadro1Click(Sender: TObject);
var
  Qry1, Qry2: TmySQLQuery;
  CliInt, Insumo: Integer;
  CustoPadrao: Double;
begin
  if Geral.MB_Pergunta(
  'Este procedimento ir� atualizar o custo da �ltima compra de TODOS produtos!'
  + sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Qry1          := TmySQLQuery.Create(Dmod);
    Qry2          := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
        'SELECT DISTINCT Insumo ',
        'FROM pqeits ',
        'ORDER BY Insumo ',
        '']);
      //
      PB1.Position := 0;
      PB1.Max      := Qry1.RecordCount;
      //
      if Qry1.RecordCount > 0 then
      begin
        while not Qry1.EOF do
        begin
          MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
          //
          UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
            'SELECT pqx.DataX, pqx.CliDest, pqx.Insumo, ',
            'pqx.Valor / pqx.Peso CustoPadrao ',
            'FROM pqx ',
            'LEFT JOIN pq pqc ON pqc.Codigo = pqx.Insumo ',
            'LEFT JOIN pqcli pcl ON (pcl.PQ = pqc.Codigo AND pcl.CI = -11) ',
            'WHERE pqx.Insumo=' + Geral.FF0(Qry1.FieldByName('Insumo').AsInteger),
            'AND pqx.Tipo = 10 ',
            'AND pqx.CliDest = -11',
            'AND pcl.MoedaPadrao = 0', //Atualiza apenas R$
            'ORDER BY pqx.DataX DESC ',
            'LIMIT 1,1 ',
            '']);
          if Qry2.RecordCount > 0 then
          begin
            CliInt      := Qry2.FieldByName('CliDest').AsInteger;
            Insumo      := Qry2.FieldByName('Insumo').AsInteger;
            CustoPadrao := Qry2.FieldByName('CustoPadrao').AsFloat;
            //
            UnPQx.AtualizaDadosPQ(CliInt, Insumo, 0, CustoPadrao, 0);
          end;
          Qry1.Next;
        end;
      end;
    finally
      Qry1.Free;
      Qry2.Free;
      //
      PB1.Position := 0;
      //
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPQ.Atualizaestoque1Click(Sender: TObject);
begin
  FCli := QrPQCliControle.Value;
  UnPQx.AtualizaEstoquePQ(QrPQCliCI.Value, QrPQCodigo.Value, FEmpresa, aeMsg, CO_VAZIO);
  ReopenPQCli();
end;

procedure TFmPQ.PMIncluiPopup(Sender: TObject);
begin
  IncluiClienteinterno1.Enabled :=
    dmkPF.IntToBool_SoPositivo(QrPQCodigo.Value);
  IncluiFornecedoraoclienteinterno1.Enabled :=
    dmkPF.IntToBool_SoPositivo(QrPQCli.RecordCount);
  IncluicoTaodepreo1.Enabled :=
    dmkPF.IntToBool_SoPositivo(QrPQFor.RecordCount);
end;

procedure TFmPQ.PQCotInsUpd((*DataCot: String; Controle, IQ, PQ, CI, Embalagem,
  Moeda, Empresa: Integer; Preco, Frete, ICMS, IPI, PIS, COFINS, RICMS, RIPI,
  RPIS, RCOFINS, Custo: Double; SQLType: TSQLType*));
var
  DataCot: String;
  Controle, IQ, PQ, CI, Embalagem, Moeda, Empresa: Integer;
  Preco, Frete, ICMS, IPI, PIS, COFINS, RICMS, RIPI, RPIS, RCOFINS, Custo: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Controle       := QrPQCotControle.Value;
  IQ             := QrPQForIQ.Value;
  PQ             := QrPQCodigo.Value;
  CI             := QrPQCliCI.Value;
  Embalagem      := EdEmb.ValueVariant;
  DataCot        := Geral.FDT(TPDataCot.Date, 1);
  Preco          := EdPreco.ValueVariant;
  Moeda          := RGMoeda.ItemIndex;
  Frete          := EdFrete.ValueVariant;
  ICMS           := EdICMS.ValueVariant;
  IPI            := EdIPI.ValueVariant;
  PIS            := EdPIS.ValueVariant;
  COFINS         := EdCOFINS.ValueVariant;
  RICMS          := EdRICMS.ValueVariant;
  RIPI           := EdRIPI.ValueVariant;
  RPIS           := EdRPIS.ValueVariant;
  RCOFINS        := EdRCOFINS.ValueVariant;
  Custo          := EdCUSTO.ValueVariant;
  Empresa        := FEmpresa;

  Controle := UMyMod.BuscaEmLivreY_Def('pqcot', 'Controle', SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqcot', False, [
  'IQ', 'PQ', 'CI',
  'Embalagem', 'DataCot', 'Preco',
  'Moeda', 'Frete', 'ICMS',
  'IPI', 'PIS', 'COFINS',
  'RICMS', 'RIPI', 'RPIS',
  'RCOFINS', 'Custo', 'Empresa'], [
  'Controle'], [
  IQ, PQ, CI,
  Embalagem, DataCot, Preco,
  Moeda, Frete, ICMS,
  IPI, PIS, COFINS,
  RICMS, RIPI, RPIS,
  RCOFINS, Custo, Empresa], [
  Controle], True) then
  begin
    FCot := Controle;
    FFor := QrPQForControle.Value;
    FCli := QrPQCliControle.Value;
    if SQLType = stIns then
      FNewNom := '';
  end;
end;

procedure TFmPQ.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 1 then
    ConfiguraPQE
  else
    QrPQE.Close;
end;

procedure TFmPQ.PMAlteraPopup(Sender: TObject);
begin
  AlteracadastrodoProdutoqumico1.Enabled :=
    dmkPF.IntToBool_SoPositivo(QrPQ.RecordCount);
  AlteradadosdoClienteinterno1.Enabled :=
    dmkPF.IntToBool_SoPositivo(QrPQCli.RecordCount);
  AlteradadosdoFornecedordoclienteinterno1.Enabled :=
    dmkPF.IntToBool_SoPositivo(QrPQFor.RecordCount);
  AlteracoTaodepreo1.Enabled :=
    dmkPF.IntToBool_SoPositivo(QrPQCot.RecordCount);
end;

procedure TFmPQ.IncluicoTaodepreo1Click(Sender: TObject);
begin
  MostraEdicao(4, stIns, 0);
end;

procedure TFmPQ.IncluiEmbalagemaoFornecedor1Click(Sender: TObject);
begin
  {
  if DBCheck.CriaFm(TFmPQForEmb, FmPQForEmb, afmoNegarComAviso) then
  begin
    FmPQForEmb.ImgTipo.SQLType := stIns;
    FmPQForEmb.ShowModal;
    FmPQForEmb.Destroy;
  end;
  }
end;

procedure TFmPQ.EdPrecoExit(Sender: TObject);
begin
  CalculaCustoPQ;
end;

procedure TFmPQ.EdFreteExit(Sender: TObject);
begin
  CalculaCustoPQ;
end;

procedure TFmPQ.EdICMSExit(Sender: TObject);
begin
  CalculaCustoPQ;
end;

procedure TFmPQ.EdIPIExit(Sender: TObject);
begin
  CalculaCustoPQ;
end;

procedure TFmPQ.EdNCMRedefinido(Sender: TObject);
begin
  EdNCM.Text := Geral.FormataNCM(EdNCM.Text);
end;

procedure TFmPQ.EdPISExit(Sender: TObject);
begin
  CalculaCustoPQ;
end;

procedure TFmPQ.EdCOFINSExit(Sender: TObject);
begin
  CalculaCustoPQ;
end;

procedure TFmPQ.EdRICMSExit(Sender: TObject);
begin
  CalculaCustoPQ;
end;

procedure TFmPQ.EdRIPIExit(Sender: TObject);
begin
  CalculaCustoPQ;
end;

procedure TFmPQ.EdRPISExit(Sender: TObject);
begin
  CalculaCustoPQ;
end;

procedure TFmPQ.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmPQ.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmPQ.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger)
end;

procedure TFmPQ.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmPQ.EdUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmPQ.EdRCOFINSExit(Sender: TObject);
begin
  if BtConfirma2.Visible then BtConfirma2.SetFocus;
  CalculaCustoPQ;
end;

procedure TFmPQ.QrPQCalcFields(DataSet: TDataSet);
begin
  //QrPQNOMEGGXNiv2.Value := RGGGXNiv2.Items[QrPQAtivo.Value];
  try
    QrPQNOMEGGXNiv2.Value := RGGGXNiv2.Items[QrPQGGXNiv2.Value];
  except
    on E: Exception do
      Geral.MB_Erro('N�o foi poss�vel obter o nome do n�vel 2 do reduzido!' +
      sLineBreak + sLineBreak + E.Message);
  end;
end;

procedure TFmPQ.QrPQCotCalcFields(DataSet: TDataSet);
begin
  QrPQCotNOMEMOEDA.Value := RGMoeda.Items[QrPQCotMoeda.Value];
  QrPQCotREAIS.Value := DmModEmit.CalculaValorPQ2(
    QrPQCotPreco.Value,
    QrPQCotICMS.Value,
    QrPQCotRICMS.Value,
    QrPQCotIPI.Value,
    QrPQCotRIPI.Value,
    QrPQCotPIS.Value,
    QrPQCotRPIS.Value,
    QrPQCotCOFINS.Value,
    QrPQCotRCOFINS.Value,
    QrPQCotFrete.Value,
    (*PrecoUsuario*)0,
    QrPQCotMoeda.Value);
end;

procedure TFmPQ.RGGGXNiv2Click(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    if RGGGXNiv2.ItemIndex = 2 then
      CGReceitas.Value := 31
    else
      CGReceitas.Value := 0;
  end;
end;

procedure TFmPQ.RGMoedaClick(Sender: TObject);
begin
  CalculaCustoPQ;
end;

procedure TFmPQ.CalculaCustoPQ;
var
  Valor: Double;
begin
  Valor := DmModEmit.CalculaValorPQ2(
    Geral.DMV(EdPreco.Text),
    Geral.DMV(EdICMS.Text),
    Geral.DMV(EdRICMS.Text),
    Geral.DMV(EdIPI.Text),
    Geral.DMV(EdRIPI.Text),
    Geral.DMV(EdPIS.Text),
    Geral.DMV(EdRPIS.Text),
    Geral.DMV(EdCOFINS.Text),
    Geral.DMV(EdRCOFINS.Text),
    Geral.DMV(EdFrete.Text),
    (*PrecoUsuario*)0,
    RGMoeda.ItemIndex);
  EdCusto.Text := Geral.FFT(Valor, 4, siPositivo);
  Valor := DmModEmit.CalculaValorPQ2(
    Geral.DMV(EdPreco.Text),
    Geral.DMV(EdICMS.Text),
    Geral.DMV(EdRICMS.Text),
    Geral.DMV(EdIPI.Text),
    Geral.DMV(EdRIPI.Text),
    Geral.DMV(EdPIS.Text),
    Geral.DMV(EdRPIS.Text),
    Geral.DMV(EdCOFINS.Text),
    Geral.DMV(EdRCOFINS.Text),
    Geral.DMV(EdFrete.Text),
    (*PrecoUsuario*)0,
    RGMoeda.ItemIndex);
  EdReais.Text := Geral.FFT(Valor, 4, siPositivo);
end;

procedure TFmPQ.AlteracoTaodepreo1Click(Sender: TObject);
begin
  MostraEdicao(4, stUpd, 0);
end;

procedure TFmPQ.QrPQForAfterScroll(DataSet: TDataSet);
begin
  ReopenPQCot;
  ReopenPQForEmb('');
end;

procedure TFmPQ.QrPQForBeforeClose(DataSet: TDataSet);
begin
  QrPQForEmb.Close;
end;

procedure TFmPQ.QrPQCliAfterOpen(DataSet: TDataSet);
begin
  if QrPQCli.RecordCount = 0 then ReopenPQFor;
end;

procedure TFmPQ.QrPQForAfterOpen(DataSet: TDataSet);
begin
  if QrPQFor.RecordCount = 0 then ReopenPQCot;
end;

procedure TFmPQ.EdCustoPadraoExit(Sender: TObject);
begin
  if BtConfirma2.Visible then BtConfirma2.SetFocus;
end;

procedure TFmPQ.QrPQCliCalcFields(DataSet: TDataSet);
begin
  QrPQCliNOMEMOEDAPADRAO.Value := UMoedas.ObtemNomeMoeda(QrPQCliMoedaPadrao.Value);
end;

procedure TFmPQ.BtPadraoClick(Sender: TObject);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE pqcli Set MoedaPadrao=:P0, CustoPadrao=:P1 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.Params[0].AsInteger := RGMoeda.ItemIndex;
  Dmod.QrUpd.Params[1].AsFloat   := Geral.DMV(EdCusto.Text);
  Dmod.QrUpd.Params[2].AsInteger := QrPQCliControle.Value;
  Dmod.QrUpd.ExecSQL;
  FCli := QrPQCliControle.Value;
  FFor := QrPQForControle.Value;
  FCot := QrPQCotControle.Value;
  LocCod(QrPQCodigo.Value, QrPQCodigo.Value);
end;

procedure TFmPQ.EdPrazoExit(Sender: TObject);
begin
  BtConfirma2.SetFocus;
end;

procedure TFmPQ.BitBtn1Click(Sender: TObject);
begin
  UnPQx.AtualizaEstoquePQ(QrPQCliCI.Value, QrPQCodigo.Value, FEmpresa, aeMsg, CO_VAZIO);
end;

procedure TFmPQ.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmPQ.BtGraGruNClick(Sender: TObject);
var
  Nivel1: Integer;
begin
  if (QrPQ.State <> dsInactive) and (QrPQ.RecordCount > 0) then
    Nivel1 := QrPQCodigo.Value
  else
    Nivel1 := 0;
  //
  Grade_Jan.MostraFormGraGruN(Nivel1);
  //
  LocCod(Nivel1, Nivel1);
end;

procedure TFmPQ.PMExcluiPopup(Sender: TObject);
begin
  Clienteinterno1.Enabled := not Geral.IntToBool_0(QrPQFor.RecordCount);
  Fornecedordoclienteinterno1.Enabled := not Geral.IntToBool_0(QrPQCot.RecordCount);
end;

procedure TFmPQ.Fornecedordoclienteinterno1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do fornecedor "' +
    QrPQForNOMEIQ.Value + '" do cliente interno "' + QrPQCliNOMECLII.Value +
    '"?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM pqfor WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrPQForControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenPQFor;
  end;
end;

procedure TFmPQ.Clienteinterno1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do cliente interno "' +
    QrPQCliNOMECLII.Value + '"?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM pqcli WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrPQCliControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenPQCli;
  end;
end;

procedure TFmPQ.CotaodePreo1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da cota��o selecionada?')= ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM pqcot WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrPQCotControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenPQCot;
  end;
end;

end.

