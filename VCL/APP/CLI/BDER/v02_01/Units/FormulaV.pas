unit FormulaV;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  Vcl.Grids, Vcl.DBGrids, frxClass, frxDBSet, Vcl.ComCtrls, Vcl.Menus;

type
  TFmFormulaV = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel4: TPanel;
    Label16: TLabel;
    DBEdit12: TDBEdit;
    DBEdit4: TDBEdit;
    Label18: TLabel;
    Label17: TLabel;
    DBEdit13: TDBEdit;
    DBEdit26: TDBEdit;
    Label30: TLabel;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label24: TLabel;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    Label25: TLabel;
    Label26: TLabel;
    DBEdit22: TDBEdit;
    EdDBClienteI: TDBEdit;
    Label4: TLabel;
    DBEdit19: TDBEdit;
    Label23: TLabel;
    EdDBGraCorCad: TDBEdit;
    Label6: TLabel;
    Label8: TLabel;
    Label22: TLabel;
    Label10: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrFormulaVIts: TMySQLQuery;
    DsFormulaVIts: TDataSource;
    QrFormulaVItsNOMEPQ: TWideStringField;
    QrFormulaVItsNumero: TIntegerField;
    QrFormulaVItsOrdem: TIntegerField;
    QrFormulaVItsControle: TIntegerField;
    QrFormulaVItsReordem: TIntegerField;
    QrFormulaVItsProcesso: TWideStringField;
    QrFormulaVItsPorcent: TFloatField;
    QrFormulaVItsProduto: TIntegerField;
    QrFormulaVItsTempoR: TIntegerField;
    QrFormulaVItsTempoP: TIntegerField;
    QrFormulaVItspH: TFloatField;
    QrFormulaVItspH_Min: TFloatField;
    QrFormulaVItspH_Max: TFloatField;
    QrFormulaVItsBe: TFloatField;
    QrFormulaVItsBe_Min: TFloatField;
    QrFormulaVItsBe_Max: TFloatField;
    QrFormulaVItsGC_Min: TFloatField;
    QrFormulaVItsGC_Max: TFloatField;
    QrFormulaVItsObs: TWideStringField;
    QrFormulaVItsObsProces: TWideStringField;
    QrFormulaVItsSC: TIntegerField;
    QrFormulaVItsReciclo: TIntegerField;
    QrFormulaVItsDiluicao: TFloatField;
    QrFormulaVItsCProd: TFloatField;
    QrFormulaVItsCSolu: TFloatField;
    QrFormulaVItsCusto: TFloatField;
    QrFormulaVItsMinimo: TFloatField;
    QrFormulaVItsMaximo: TFloatField;
    QrFormulaVItsGraus: TFloatField;
    QrFormulaVItsBoca: TWideStringField;
    QrFormulaVItsEhGGX: TSmallintField;
    QrFormulaVItsGraGruX: TIntegerField;
    QrFormulaVItsDensidade: TFloatField;
    QrFormulaVItsUsarDensid: TSmallintField;
    QrFormulaVItsAlterWeb: TSmallintField;
    QrFormulaVItsAWServerID: TIntegerField;
    QrFormulaVItsAWStatSinc: TSmallintField;
    QrFormulaVItsAtivo: TSmallintField;
    QrFormulaVItsUserDel: TIntegerField;
    QrFormulaVItsDataDel: TDateTimeField;
    QrFormulaVItsMotvDel: TIntegerField;
    QrFormulaVItsVersao: TIntegerField;
    QrFormulaVItsVerOri: TIntegerField;
    DBGrid2: TDBGrid;
    QrFormulaV: TMySQLQuery;
    QrFormulaVNumero: TIntegerField;
    QrFormulaVNome: TWideStringField;
    QrFormulaVClienteI: TIntegerField;
    QrFormulaVTipificacao: TIntegerField;
    QrFormulaVDataI: TDateField;
    QrFormulaVDataA: TDateField;
    QrFormulaVTecnico: TWideStringField;
    QrFormulaVTempoR: TIntegerField;
    QrFormulaVTempoP: TIntegerField;
    QrFormulaVTempoT: TIntegerField;
    QrFormulaVHorasR: TIntegerField;
    QrFormulaVHorasP: TIntegerField;
    QrFormulaVHorasT: TIntegerField;
    QrFormulaVHidrica: TIntegerField;
    QrFormulaVLinhaTE: TIntegerField;
    QrFormulaVCaldeira: TIntegerField;
    QrFormulaVSetor: TIntegerField;
    QrFormulaVEspessura: TIntegerField;
    QrFormulaVPeso: TFloatField;
    QrFormulaVQtde: TFloatField;
    QrFormulaVGrupo: TIntegerField;
    QrFormulaVVersao: TIntegerField;
    QrFormulaVEhGGX: TSmallintField;
    QrFormulaVLk: TIntegerField;
    QrFormulaVDataCad: TDateField;
    QrFormulaVDataAlt: TDateField;
    QrFormulaVUserCad: TIntegerField;
    QrFormulaVUserAlt: TIntegerField;
    QrFormulaVAlterWeb: TSmallintField;
    QrFormulaVAtivo: TSmallintField;
    QrFormulaVRetrabalho: TSmallintField;
    QrFormulaVBxaEstqVS: TSmallintField;
    QrFormulaVGraGruX: TIntegerField;
    QrFormulaVGraCorCad: TIntegerField;
    QrFormulaVEspRebaixe: TIntegerField;
    QrFormulaVAWServerID: TIntegerField;
    QrFormulaVAWStatSinc: TSmallintField;
    QrFormulaVFormulasGru: TIntegerField;
    QrFormulaVHistAlter: TWideMemoField;
    QrFormulaVUserDel: TIntegerField;
    QrFormulaVDataDel: TDateTimeField;
    QrFormulaVMotvDel: TIntegerField;
    QrFormulaVNO_SETOR: TWideStringField;
    QrFormulaVNO_GraCorCad: TWideStringField;
    QrFormulaVLinhasReb: TWideStringField;
    QrFormulaVLinhasSemi: TWideStringField;
    QrFormulaVNO_ClienteI: TWideStringField;
    DsFormulaV: TDataSource;
    QrFormulaVHHMM_P: TWideStringField;
    QrFormulaVHHMM_R: TWideStringField;
    QrFormulaVHHMM_T: TWideStringField;
    QrFormulaVMEDIAPESO: TFloatField;
    QrFormulaVNumCODIF: TWideStringField;
    frxDsFormulaV: TfrxDBDataset;
    frxDsFormulaVIts: TfrxDBDataset;
    PCVerSel: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    DBMemo1: TDBMemo;
    frxQUI_RECEI_201_A: TfrxReport;
    QrFormulaVItsSeq: TIntegerField;
    PMImprime: TPopupMenu;
    Imprimeaversoselecionada1: TMenuItem;
    Listadasdescries1: TMenuItem;
    frxQUI_RECEI_201_B: TfrxReport;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFormulaVAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFormulaVBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrFormulaVAfterScroll(DataSet: TDataSet);
    procedure QrFormulaVBeforeClose(DataSet: TDataSet);
    procedure QrFormulaVCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrFormulaVItsCalcFields(DataSet: TDataSet);
    procedure Imprimeaversoselecionada1Click(Sender: TObject);
    procedure Listadasdescries1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    //procedure ReopenVersoes();
    procedure ReopenFormulaVIts();
  public
    { Public declarations }
    procedure LocCod(Atual, Numero: Integer);
  end;

var
  FmFormulaV: TFmFormulaV;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFormulaV.Listadasdescries1Click(Sender: TObject);
begin
  QrFormulaV.First;
  MyObjects.frxDefineDatasets(frxQUI_RECEI_201_B, [
    Dmod.frxDsMaster,
    frxDsFormulaV]);
  MyObjects.frxMostra(frxQUI_RECEI_201_B, 'Lista das descrições das alterações');
end;

procedure TFmFormulaV.LocCod(Atual, Numero: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Numero);
end;

procedure TFmFormulaV.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFormulaVNumero.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFormulaV.DefParams;
begin
  VAR_GOTOTABELA := 'formulav';
  VAR_GOTOMYSQLTABLE := QrFormulaV;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_NUMERO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT frv.*,');
  VAR_SQLx.Add('lse.Nome NO_SETOR, gcc.Nome NO_GraCorCad,');
  VAR_SQLx.Add('esr.Linhas LinhasReb, ess.Linhas LinhasSemi,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_ClienteI ');
  VAR_SQLx.Add('FROM formulav frv');
  VAR_SQLx.Add('LEFT JOIN listasetores lse ON lse.Codigo=frv.Setor');
  VAR_SQLx.Add('LEFT JOIN gracorcad gcc ON gcc.Codigo=frv.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN espessuras esr ON esr.Codigo=frv.Espessura');
  VAR_SQLx.Add('LEFT JOIN espessuras ess ON ess.Codigo=frv.EspRebaixe');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=frv.ClienteI');  //
  VAR_SQL1.Add('WHERE frv.Numero=:P0');
  //
  //VAR_SQL2.Add('AND frv.CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE frv.Nome Like :P0');
  //
end;

procedure TFmFormulaV.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFormulaV.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFormulaV.ReopenFormulaVIts;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulaVIts, Dmod.MyDB, [
  'SELECT pq_.Nome NOMEPQ, its.* ',
  'FROM formulavits its ',
  'LEFT JOIN pq pq_ ON pq_.Codigo=its.Produto ',
  'WHERE Numero=' + Geral.FF0(QrFormulaVNumero.Value),
  'AND Versao=' + Geral.FF0(QrFormulaVVersao.Value),
  'ORDER BY Ordem, Reordem, Controle ',
  '']);
end;

(*
procedure TFmFormulaV.ReopenVersoes();
begin

end;
*)
procedure TFmFormulaV.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmFormulaV.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFormulaV.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFormulaV.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFormulaV.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFormulaV.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulaV.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFormulaVNumero.Value;
  Close;
end;

procedure TFmFormulaV.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  //GBDados.Align := alClient;
  CriaOForm;
  PCVerSel.ActivePageIndex := 0;
end;

procedure TFmFormulaV.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFormulaVNumero.Value, LaRegistro.Caption);
end;

procedure TFmFormulaV.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmFormulaV.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFormulaV.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFormulaVNumero.Value, LaRegistro.Caption);
end;

procedure TFmFormulaV.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFormulaV.QrFormulaVAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFormulaV.QrFormulaVAfterScroll(DataSet: TDataSet);
begin
  ReopenFormulaVIts();
end;

procedure TFmFormulaV.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormulaV.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFormulaVNumero.Value,
  CuringaLoc.CriaForm(CO_NUMERO, CO_NOME, 'formulav', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFormulaV.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulaV.Imprimeaversoselecionada1Click(Sender: TObject);
begin
  MyObjects.frxDefineDatasets(frxQUI_RECEI_201_A, [
    Dmod.frxDsMaster,
    frxDsFormulaV,
    frxDsFormulaVIts]);
  MyObjects.frxMostra(frxQUI_RECEI_201_A, 'Receita Obsoleta');
end;

procedure TFmFormulaV.QrFormulaVBeforeClose(DataSet: TDataSet);
begin
  QrFormulaVIts.Close;
end;

procedure TFmFormulaV.QrFormulaVBeforeOpen(DataSet: TDataSet);
begin
  QrFormulaVNumero.DisplayFormat := FFormatFloat;
end;

procedure TFmFormulaV.QrFormulaVCalcFields(DataSet: TDataSet);
begin
  if QrFormulaVQtde.Value > 0 then
    QrFormulaVMEDIAPESO.Value :=
    QrFormulaVPeso.Value /
    QrFormulaVQtde.Value
  else
    QrFormulaVMEDIAPESO.Value := 0;
  QrFormulaVHHMM_P.Value := dmkPF.HorasMH(QrFormulaVTempoP.Value, True);
  QrFormulaVHHMM_R.Value := dmkPF.HorasMH(QrFormulaVTempoR.Value, True);
  QrFormulaVHHMM_T.Value := dmkPF.HorasMH(QrFormulaVTempoT.Value, True);
  //
  QrFormulaVNumCODIF.Value := dmkPF.IntToCODIF_Receita(QrFormulaVNumero.Value);
end;

procedure TFmFormulaV.QrFormulaVItsCalcFields(DataSet: TDataSet);
begin
  QrFormulaVItsSeq.Value := QrFormulaVIts.RecNo;
end;

end.

