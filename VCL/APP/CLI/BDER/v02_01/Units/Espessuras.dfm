object FmEspessuras: TFmEspessuras
  Left = 510
  Top = 211
  Caption = 'CAD-BDERM-003 :: Espessuras'
  ClientHeight = 446
  ClientWidth = 343
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 343
    Height = 290
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object dmkDBGridDAC1: TdmkDBGridDAC
      Left = 0
      Top = 0
      Width = 343
      Height = 290
      SQLFieldsToChange.Strings = (
        'Linhas'
        'EMCM')
      SQLIndexesOnUpdate.Strings = (
        'Codigo')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Linhas'
          Title.Caption = 'Descri'#231#227'o'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EMCM'
          Title.Caption = 'EMCM [F4]'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 17
          Visible = True
        end>
      Color = clWindow
      DataSource = DsEspessuras
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dmkDBGridDAC1CellClick
      OnKeyDown = dmkDBGridDAC1KeyDown
      SQLTable = 'Espessuras'
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Linhas'
          Title.Caption = 'Descri'#231#227'o'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EMCM'
          Title.Caption = 'EMCM [F4]'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 17
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 343
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 295
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 247
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 143
        Height = 32
        Caption = 'Espessuras'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 143
        Height = 32
        Caption = 'Espessuras'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 143
        Height = 32
        Caption = 'Espessuras'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 338
    Width = 343
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 339
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 286
        Height = 16
        Caption = 'EMCM: Espessura M'#233'dia para C'#225'lculo de Massa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 286
        Height = 16
        Caption = 'EMCM: Espessura M'#233'dia para C'#225'lculo de Massa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 382
    Width = 343
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 339
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 195
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtAcao: TBitBtn
        Tag = 294
        Left = 2
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&A'#231#227'o'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAcaoClick
      end
    end
  end
  object DsEspessuras: TDataSource
    DataSet = QrEspessuras
    Left = 168
    Top = 176
  end
  object QrEspessuras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM espessuras')
    Left = 168
    Top = 132
    object QrEspessurasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEspessurasLinhas: TWideStringField
      FieldName = 'Linhas'
      Size = 7
    end
    object QrEspessurasEMCM: TFloatField
      FieldName = 'EMCM'
    end
    object QrEspessurasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEspessurasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEspessurasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEspessurasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEspessurasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEspessurasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEspessurasAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object PMAcao: TPopupMenu
    Left = 104
    Top = 249
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
  end
end
