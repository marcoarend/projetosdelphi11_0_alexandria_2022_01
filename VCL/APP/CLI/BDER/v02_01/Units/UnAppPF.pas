unit UnAppPF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, SHDocVw,
  Buttons, Variants, TypInfo,
  UnInternalConsts2, ComCtrls, dmkGeral, AppListas, UnProjGroup_Consts,
  mySQLDBTables, UnDmkEnums, dmkEdit, UnAppEnums, UnGrade_Jan, dmkCheckGroup;

type
  TSubFormPQ = (tsfpqNenhum=0, tsfpqCI=1);
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    // C O R R E � � E S
    function  CorrigeCamposNFemPQe(): Boolean;
    // F I M    C O R R E � � E S
    function  AcaoEspecificaDeApp(Tabela: String): Boolean;
    procedure CadastroInsumo(GraGruX: Integer; NewNome: String;
              GraGru1: Integer = 0; MostraSubForm: TSubFormPQ = tsfpqNenhum);
    procedure CadastroMateriaPrima(GraGruX: Integer; NewNome: String);
    procedure CadastroProdutoAcabado(GraGruX: Integer; NewNome: String);
    procedure CadastroServicoNFe(GraGruX: Integer; NewNome: String);
    procedure CadastroSubProduto(GraGruX: Integer; NewNome: String);
    procedure CadastroUsoPermanente(GraGruX: Integer; NewNome: String);
    function  GGXCadGetForcaCor(GraGruY: Integer): Boolean;
    function  GGXCadGetForcaTam(GraGruY: Integer): Boolean;
    procedure Html_ConfigarImagem(WebBrowser: TWebBrowser);
    procedure Html_ConfigarUrl(WebBrowser: TWebBrowser);
    procedure Html_ConfigarVideo(WebBrowser: TWebBrowser);
    //procedure MostraFormSPED_EFD_X999_Balancos(ImporExpor, AnoMes, Empresa,
    //          LinArq: Integer; Registro: String; Data: TDateTime);
    procedure MostraFormOP(OP, ID_Item: String);
    procedure MostraFormCfgMovEFD(ImporExpor, AnoMes, Empresa, PeriApu: Integer);
    procedure MostraFormCfgEstqInNatEFD(ImporExpor, AnoMes, Empresa, PeriApu: Integer);
    procedure MostraFormCfgEstqOthersEFD(ImporExpor, AnoMes, Empresa, PeriApu: Integer);
    procedure MostraFormCfgEstqGeradoEFD(ImporExpor, AnoMes, Empresa, PeriApu: Integer);
    procedure MostraFormCfgEstqVSxPQ_EFD(ImporExpor, AnoMes, Empresa, PeriApu: Integer);
    function  ObtemNomeDeIDMovIts(MovID: Integer): String;
    function  ReabreSPEDEFD0200Exportacao(Qry: TmySQLQuery; TabName: String): Boolean;
    procedure VerificaGraGruYsNaoConfig();
    procedure MostraJanelaOrigemInfoIncCab(InfoIncCab: Integer);
    procedure VerificaSeExisteIMEC(EdTxt: TdmkEdit; IMEC: Integer);
    function  ObtemNomeTabelaGraGruY(GraGruY: Integer): String;
    procedure MostraFormXxXxxXxxGraGruY(GraGruY, GraGruX: Integer; Edita:
              Boolean; Qry: TmySQLQuery(*; MultiplosGGX: array of Integer*));
    procedure VerificaCadastroXxArtigoIncompleta();
    procedure ConfiguraGGXsDeGGY(GraGruY: Integer; iNiveis1: array of Integer);
    procedure CorrigeReduzidosDuplicadosDeInsumo(CorrigeGraTamIts: Boolean);
    function  CorrigeGraGruYdeGraGruX(GraGruY, GraGruX: Integer): Boolean;
    procedure MostraPopupDeCadsatroEspecificoDeGraGruXY(Form: TForm; Sb:
              TSpeedButton; PM: TPopupMenu);
    function  TextoDeCodHist(CodHist: Integer): String;
    function  AtualizaEstoqueGraGXPatr(GraGruX, Empresa: Integer; AvisaNegativo: Boolean): Boolean;
    procedure MostraOrigemFatGenerico(FatID: Integer; FatNum: Double;
              FatParcela, FatParcRef: Integer);
    // Compatibilidade do Toolrent
    procedure MostraOrigemFat(FatParcRef: Integer);
    function  TextoDeCodHist_Sigla(CodHist: Integer): String;           //*Toolrent
    procedure LocalizaOrigemDoFatPedCab(Codigo: Integer);               // *Toolrent
    procedure ConfiguraRGMapaOperPosProc(RGOperPosProc: TRadioGroup; Habilita:
              Boolean; Default: Integer);
    procedure ConfiguraCGMapaOperPosProc(CGOperPosProc: TdmkCheckGroup; Habilita:
              Boolean; Default: Integer);
    function  ObtemNumeroDeCODIF(Atual: Integer): Integer;
    function  SQLPesqVeiculo(): String;
    function  ImpedePorMovimentoAberto(): Boolean;
    function  NFeJaLancada(ChaveNFe: String): Boolean;
    function  ObtemTabelaDeFatID(FatID: Integer): String;

 end;

var
  AppPF: TUnAppPF;

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, UMySQLModule,
  UnDmkProcFunc, VSCfgEstqInNatEFD, VSCfgEstqOthersEFD, VSCfgEstqGeradoEFD,
  VSCfgEstqVSxPQ_EFD, UnVS_PF, UnPQ_PF, PQ, ModProd, NFe_PF, UnGrade_PF,
  GraGruYIncompleto, UnVS_CRC_PF;


{ TUnAppPF }

function TUnAppPF.AcaoEspecificaDeApp(Tabela: String): Boolean;
begin
  //Compatibilidade
  Result := True;
end;

procedure TUnAppPF.ConfiguraCGMapaOperPosProc(CGOperPosProc: TdmkCheckGroup;
  Habilita: Boolean; Default: Integer);
const
  Colunas = 3;
begin
  MyObjects.ConfiguraCheckGroup(CGOperPosProc, sMapaOperPosProcStat, Colunas, Default, (*FirstAll*)False);
  CGOperPosProc.Enabled := Habilita;
end;

procedure TUnAppPF.ConfiguraGGXsDeGGY(GraGruY: Integer;
  iNiveis1: array of Integer);
const
  sProcName = 'TUnAppPF.ConfiguraGGXsDeGGY()';
var
  GraGruX, I: Integer;
  Tabela, sNiveis1: String;
  Qry: TmySQLQuery;
  Continua: Boolean;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    sNiveis1 := MyObjects.CordaDeArrayInt(iNiveis1);
    //if Nivel1 <> 0 then
    if sNiveis1 <> '' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM gragrux ',
      //'WHERE GraGru1=' + Geral.FF0(sNiveis1),
      'WHERE GraGru1 IN (' + sNiveis1 + ')',
      '']);
      Qry.First;
      GraGruX := Qry.FieldByName('Controle').AsInteger;
      Tabela := AppPF.ObtemNomeTabelaGraGruY(GraGruY);
      if Tabela <> '' then
      begin
        //UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False, [
        Qry.First;
        while not Qry.Eof do
        begin
          UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, Tabela, False, [
          ], [
          'GraGruX'], ['Ativo'], [
          ], [
          Qry.FieldByName('Controle').AsInteger], [1], True);
          //
          Qry.Next;
        end;
        case GraGruY of
          (*
          CO_GraGruY_0512_VSSubPrd,
          CO_GraGruY_0683_VSPSPPro,
          *)
          CO_GraGruY_0853_VSPSPEnd,
          CO_GraGruY_1024_VSNatCad,
          (*CO_GraGruY_1072_VSNatInC,*)
          CO_GraGruY_1088_VSNatCon,
          (*
          CO_GraGruY_1195_VSNatPDA,
          CO_GraGruY_1365_VSProCal,
          CO_GraGruY_1536_VSCouCal,
          CO_GraGruY_1621_VSCouDTA,
          CO_GraGruY_1707_VSProCur,
          CO_GraGruY_1877_VSCouCur,
          CO_GraGruY_2048_VSRibCad,
          *)
          CO_GraGruY_3072_VSRibCla:
          //CO_GraGruY_4096_VSRibOpe:
          begin
            Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
            'GraGruY'], [
            'Controle'], [
            GraGruY], [
            GraGruX], True);
          end;
          //CO_GraGruY_5120_VSWetEnd,
          CO_GraGruY_6144_VSFinCla:
          begin
            Grade_PF.CorrigeGraGruYdeGraGruX();
            Continua := True;
          end;
          //CO_GraGruY_7168_VSRepMer
          else
          Geral.MB_Erro('O grupo de estoque n�mero ' + Geral.FF0(GraGruY) +
          ' n�o est� implementado em ' + sProcName);
        end;
        if Continua (*and (not JaTemN1)*) then
        begin
          AppPF.MostraFormXxXxxXxxGraGruY(GraGruY, GraGruX, True, Qry(*, MultiplosGGX*));
        end;
      end;
    end else
      Geral.MB_Erro('N�o foi poss�vel localizar o(s) produto(s) ' + sNiveis1 +
      ' para complementar os dados do seu cadastro!');
  finally
    Qry.Free;
  end;
end;

procedure TUnAppPF.ConfiguraRGMapaOperPosProc(RGOperPosProc: TRadioGroup;
  Habilita: Boolean; Default: Integer);
const
  Colunas = 3;
begin
  MyObjects.ConfiguraRadioGroup(RGOperPosProc, sMapaOperPosProcStat, Colunas, Default);
  RGOperPosProc.Enabled := Habilita;
end;

function TUnAppPF.CorrigeCamposNFemPQe(): Boolean;

  function AtualizaPQe(Qry: TmySQLQuery): Boolean;
  const
    modNF = 55;
  var
    Codigo, NF, Serie: Integer;
    refNFe: String;
  begin
    Result := False;
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM pqe ',
        'WHERE refNFe <> "" ',
        'AND NF = 0 ',
        '']);
      if Qry.RecordCount > 0 then
      begin
        while not Qry.EOF do
        begin
          Codigo := Qry.FieldByName('Codigo').AsInteger;
          refNFe := Qry.FieldByName('refNFe').AsString;
          //
          UnNFe_PF.ObtemSerieNumeroByID(refNFe, Serie, NF);
          //
          if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pqe', False,
            ['modNF', 'Serie', 'NF'], ['Codigo'], [modNF, Serie, NF], [Codigo], True)
          then
            Exit;
          //
          Qry.Next;
        end;
      end;
      Result := True;
    except
      Result := False;
    end;
  end;

  function AtualizaLctEmiss(Qry: TmySQLQuery; FatId: Integer): Boolean;
  var
    Controle, Serie, NF: Integer;
    TabLctA: String;
  begin
    Result := False;
    try
      TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, DModG.QrFiliLogFilial.Value);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT pq.Serie, pq.NF, la.Controle ',
        'FROM ' + TabLctA + ' la ',
        'LEFT JOIN pqe pq ON pq.Codigo = la.FatNum ',
        'WHERE la.FatID=' + Geral.FF0(FatId),
        'AND NotaFiscal = 0 ',
        '']);
      if Qry.RecordCount > 0 then
      begin
        while not Qry.EOF do
        begin
          Controle := Qry.FieldByName('Controle').AsInteger;
          Serie    := Qry.FieldByName('Serie').AsInteger;
          NF       := Qry.FieldByName('NF').AsInteger;
          //
          if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabLctA, False,
            ['SerieNF', 'NotaFiscal'], ['Controle'], [Serie, NF], [Controle], True)
          then
            Exit;
          //
          Qry.Next;
        end;
      end;
      Result := True;
    except
      Result := False;
    end;
  end;

var
  Continua: Boolean;
  Qry: TmySQLQuery;
begin
  Result   := False;
  Continua := False;
  Qry      := TmySQLQuery.Create(Dmod);
  try
    Continua := AtualizaPQe(Qry);
    //
    if Continua = True then
    begin
      Continua := AtualizaLctEmiss(Qry, VAR_FATID_1001);
      //
      if Continua = True then
        Continua := AtualizaLctEmiss(Qry, VAR_FATID_1002);
    end;
    Result := Continua;
  finally
    Qry.Free;
  end;
end;

function TUnAppPF.CorrigeGraGruYdeGraGruX(GraGruY, GraGruX: Integer): Boolean;
begin
  Result := False;
  case GraGruY of
    CO_GraGruY_0512_VSSubPrd,
    CO_GraGruY_0683_VSPSPPro,
    CO_GraGruY_0853_VSPSPEnd,
    CO_GraGruY_1024_VSNatCad,
    CO_GraGruY_1072_VSNatInC,
    CO_GraGruY_1088_VSNatCon,
    CO_GraGruY_1195_VSNatPDA,
    CO_GraGruY_1365_VSProCal,
    CO_GraGruY_1536_VSCouCal,
    CO_GraGruY_1621_VSCouDTA,
    CO_GraGruY_1707_VSProCur,
    CO_GraGruY_1877_VSCouCur,
    CO_GraGruY_2048_VSRibCad,
    CO_GraGruY_3072_VSRibCla,
    CO_GraGruY_4096_VSRibOpe,
    CO_GraGruY_7168_VSRepMer:
    begin
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
      'GraGruY'], [
      'Controle'], [
      GraGruY], [
      GraGruX], True);
    end;
    CO_GraGruY_5120_VSWetEnd,
    CO_GraGruY_6144_VSFinCla:
    begin
      Grade_PF.CorrigeGraGruYdeGraGruX();
      Result := True;
    end;
    else
      Geral.MB_Erro('Grupo de estoque n�o implementado! [2]');
  end;
end;

procedure TUnAppPF.CorrigeReduzidosDuplicadosDeInsumo(
  CorrigeGraTamIts: Boolean);
var
  Qry1, Qry2: TmySQLQuery;
  ItensRemovidos, Controle: Integer;
  OK: Boolean;
  //
  function CorrigeTabelaGraTamIts(): Boolean;
  begin
    UMyMod.ExcluiRegistroInt2('', 'gratamits', 'Codigo', 'Controle', 0, 1, Dmod.MyDB);
    UMyMod.ExcluiRegistroInt2('', 'gratamits', 'Codigo', 'Controle', 1, 0, Dmod.MyDB);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
    'SELECT * ',
    'FROM gratamits',
    'WHERE Codigo=0',
    'AND Controle=0',
    '']);
    if Qry2.RecordCount = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gratamits', False, [
      'Codigo', 'Nome', 'PrintTam'], [
      'Controle'], [
      (*Codigo*)0, (*Nome*)'Unico', (*PrintTam*)0], [
      (*Controle*)0], True);
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
    'SELECT * ',
    'FROM gratamits',
    'WHERE Codigo=1',
    'AND Controle=1',
    '']);
    if Qry2.RecordCount = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gratamits', False, [
      'Codigo', 'Nome', 'PrintTam'], [
      'Controle'], [
      (*Codigo*)1, (*Nome*)'Unico', (*PrintTam*)0], [
      (*Controle*)1], True);
    end;
    //
  end;
  procedure CorrigeCod0doNivel2();
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru2', False, [
    'Tipo_Item'], ['Nivel2'], [(*Tipo_Item*)-1], [(*Nivel2*)0], True);
  end;
  procedure ExcluiGraGru2SemGraGru1();
  var
    Nivel2: Integer;
  begin
    if Geral.MB_Pergunta(
    'Deseja excluir os N�veis 2 n�o usados nos N�veis 1?') <> ID_YES then
      Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
    'SELECT gg1.Nivel2 NullNiv2, gg2.Nivel2',
    'FROM gragru2 gg2',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel2=gg2.Nivel2',
    'WHERE gg2.Nivel2 > 0',
    'AND gg1.Nivel2 IS NULL',
    '']);
    while not Qry2.Eof do
    begin
      Nivel2 := Qry2.FieldByName('Nivel2').AsInteger;
      //
(*

      if UMyMod.ExcluiRegistro_EnviaArquivoMorto('gragru2z', 'Nivel2',
      dmkPF.MotivDel_ValidaCodigo(?), ['Nivel2'], [Nivel2],
      Dmod.MyDB) then
      begin
*)
        UMyMod.ExcluiRegistroInt1('', 'gragru2', 'Nivel2', Nivel2, Dmod.MyDB);
(*
        ItensRemovidos := ItensRemovidos + 1;
      end;
*)
      //
      Qry2.Next;
    end;
  end;
  procedure RecriaCouNiv1eCouNiv2();
  begin
    if Geral.MB_Pergunta(
    'Deseja recriar CouNiv1 e CouNiv2?') <> ID_YES then
      Exit;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM couniv1; ',
    'DELETE FROM couniv2; ',
    '']);
    DBCheck.VerificaRegistrosObrigatorios_Inclui(Dmod.MyDB, 'CouNiv1', 'CouNiv1',
    nil, False, True, nil, nil, nil, nil);
    //
    DBCheck.VerificaRegistrosObrigatorios_Inclui(Dmod.MyDB, 'CouNiv2', 'CouNiv2',
    nil, False, True, nil, nil, nil, nil);
    //
  end;
begin
  ItensRemovidos := 0;
  Screen.Cursor := crHourGlass;
  try
    Qry1 := TmySQLQuery.Create(Dmod);
    try
      Qry2 := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
        'DROP TABLE IF EXISTS _ARRUMA_GGX_PQ_IUGUTYFDRDR_; ',
        ' ',
        'CREATE TABLE _ARRUMA_GGX_PQ_IUGUTYFDRDR_ ',
        'SELECT gg1.Nivel1, COUNT(gg1.Nivel1) ITENS ',
        'FROM gragru1 gg1 ',
        'LEFT JOIN gragrux ggx ON ggx.GraGru1=gg1.Nivel1 ',
        'WHERE gg1.PrdGrupTip=-2 ',
        'GROUP BY gg1.Nivel1 ',
        'ORDER BY ITENS DESC ',
        '; ',
        ' ',
        'DELETE FROM _ARRUMA_GGX_PQ_IUGUTYFDRDR_ ',
        'WHERE ITENS < 2; ',
        ' ',
        'SELECT * FROM ',
        '_ARRUMA_GGX_PQ_IUGUTYFDRDR_ ',
        'ORDER BY Nivel1 ',
        '']);
        //
        Qry1.First;
        while not Qry1.Eof do
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
          'SELECT ggx.* ',
          'FROM gragrux ggx',
          'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
          'WHERE gg1.Nivel1=' + Geral.FF0(Qry1.FieldByName('Nivel1').AsInteger),
          'ORDER BY Controle',
          '']);
          if Qry2.RecordCount > 1 then
          begin
            // N�o deletar o primeiro!!!
            Qry2.Next;
            //
            while not Qry2.Eof do
            begin
              Controle := Qry2.FieldByName('Controle').AsInteger;
              if UMyMod.ExcluiRegistro_EnviaArquivoMorto('gragruz', 'gragrux',
              dmkPF.MotivDel_ValidaCodigo(996), ['Controle'], [Controle],
              Dmod.MyDB) then
              begin
                UMyMod.ExcluiRegistroInt1('', 'gragrux', 'Controle', Controle, Dmod.MyDB);
                ItensRemovidos := ItensRemovidos + 1;
              end;
              //
              Qry2.Next;
            end;
            // Corrigir tamanho!!!
            //Qry2.First;
            //Controle := Qry2.FieldByName('Controle').AsInteger;
            //

          end else
            Geral.MB_Erro('Insumo sem duplicidade! Avise a Dermatek');
          //
          Qry1.Next;
        end;
        if CorrigeGraTamIts then
        begin
          CorrigeTabelaGraTamIts();
          CorrigeCod0doNivel2();
          ExcluiGraGru2SemGraGru1();
          RecriaCouNiv1eCouNiv2();
        end;
        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(Qry2, Dmod.MyDB, [
        'UPDATE gragrux ',
        'SET GraTamI=1 ',
        'WHERE GraTamI=0 ',
        '']);
        UnDmkDAC_PF.ExecutaMySQLQuery0(Qry2, Dmod.MyDB, [
        'UPDATE gragru1 ',
        'SET GraTamCad=1 ',
        'WHERE GraTamCad=0 ',
        '']);
      finally
         UnDmkDAC_PF.ExecutaMySQLQuery0(Qry1, Dmod.MyDB, [
         'DROP TABLE IF EXISTS _ARRUMA_GGX_PQ_IUGUTYFDRDR_; ',
         '']);
        Qry2.Free;
      end;

    finally
      Qry1.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  if ItensRemovidos > 0 then
    Geral.MB_Aviso(Geral.FF0(ItensRemovidos) +
    ' reduzidos duplicados de Insumos Qu�micos foram movidos para o arquivo morto!'
    + sLineBreak + 'Avise a Dermatek!');
end;

function TUnAppPF.GGXCadGetForcaCor(GraGruY: Integer): Boolean;
begin
  Result :=
    (GraGruY = CO_GraGruY_6144_VSFinCla)
    or
    (GraGruY = CO_GraGruY_5120_VSWetEnd);
end;

function TUnAppPF.GGXCadGetForcaTam(GraGruY: Integer): Boolean;
begin
  Result := GGXCadGetForcaCor(GraGruY);
end;

function TUnAppPF.AtualizaEstoqueGraGXPatr(GraGruX, Empresa: Integer;
  AvisaNegativo: Boolean): Boolean;
begin
  // Compatibilidade
  Result := True;
end;

procedure TUnAppPF.CadastroInsumo(GraGruX: Integer; NewNome: String;
  GraGru1: Integer = 0; MostraSubForm: TSubFormPQ = tsfpqNenhum);
var
  Codigo: Integer;
begin
  PQ_PF.CorrigePQGGXNiv2();
  //
  if GraGruX <> 0 then
    Codigo := DmProd.ObtemGraGru1DeGraGruX(GraGruX)
  else if GraGru1 <> 0 then
    Codigo := GraGru1;
  //
  if DBCheck.CriaFm(TFmPQ, FmPQ, afmoNegarComAviso) then
  begin
    FmPQ.FCodLoc        := Codigo;
    FmPQ.FNewNom        := NewNome;
    FmPQ.FMostraSubForm := MostraSubForm;
    FmPQ.ShowModal;
    FmPQ.Destroy;
  end;
end;

procedure TUnAppPF.CadastroMateriaPrima(GraGruX: Integer; NewNome: String);
begin
  Grade_Jan.MostraFormGraGruY(1024, GraGruX, NewNome);
end;

procedure TUnAppPF.CadastroProdutoAcabado(GraGruX: Integer; NewNome: String);
const
  Aviso  = '...';
  Titulo = 'Sele��o Grupo de Produtp Acabado';
  Prompt = 'Selecione o grupo';
  Campo  = 'Descricao';
var
  Codigo: Integer;
  Resp: Variant;
  Qry: TmySQLQuery;
  GraGruY: Integer;
  //
begin
  GraGruY := 6144;
  Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Codigo, Nome Descricao  ',
  'FROM gragruy ',
  'WHERE Codigo IN (2048,3072,6144) ',
  ''], Dmod.MyDB, True);
  if Resp <> Null then
    GraGruY := Integer(Resp);
  Grade_Jan.MostraFormGraGruY(GraGruY, GraGruX, NewNome);
end;

procedure TUnAppPF.CadastroServicoNFe(GraGruX: Integer; NewNome: String);
const
  GraGruY = -1;
  PrdGrupTip = 0;
  GraGru1 = 0;
  GraGruC = 0;
  JaTemN1 = False;
  SQL_WHERE_PGT = 'WHERE Tipo_Item=9 ';
  ForcaCor = False;
  ForcaTam = False;
begin
  Grade_PF.CadastraProdutoGrade(GraGruY, PrdGrupTip, GraGru1, GraGruC, JaTemN1,
  NewNome, SQL_WHERE_PGT, ForcaCor, ForcaTam);
end;

procedure TUnAppPF.CadastroSubProduto(GraGruX: Integer; NewNome: String);
begin
  Grade_Jan.MostraFormGraGruY(512, GraGruX, NewNome);
end;

procedure TUnAppPF.CadastroUsoPermanente(GraGruX: Integer; NewNome: String);
const
  GraGruY = -1;
  PrdGrupTip = 0;
  GraGru1 = 0;
  GraGruC = 0;
  JaTemN1 = False;
  SQL_WHERE_PGT = 'WHERE Tipo_Item IN (7,8) ';
  ForcaCor = False;
  ForcaTam = False;
begin
  Grade_PF.CadastraProdutoGrade(GraGruY, PrdGrupTip, GraGru1, GraGruC, JaTemN1,
  NewNome, SQL_WHERE_PGT, ForcaCor, ForcaTam);
end;

procedure TUnAppPF.Html_ConfigarImagem(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarUrl(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarVideo(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

function TUnAppPF.ImpedePorMovimentoAberto(): Boolean;
begin
  // Compatibilidade: Estoque stqmovitsa com abertura e fechamento
  Result := False;
end;

procedure TUnAppPF.LocalizaOrigemDoFatPedCab(Codigo: Integer);
begin
 // Compatibilidade *Toolrent
end;

procedure TUnAppPF.MostraFormCfgEstqGeradoEFD(ImporExpor, AnoMes, Empresa,
  PeriApu: Integer);
begin
  if DBCheck.CriaFm(TFmVSCfgEstqGeradoEFD, FmVSCfgEstqGeradoEFD, afmoNegarComAviso) then
  begin
    FmVSCfgEstqGeradoEFD.FImporExpor := ImporExpor;
    FmVSCfgEstqGeradoEFD.FAnoMes     := AnoMes;
    FmVSCfgEstqGeradoEFD.FEmpresa    := Empresa;
    FmVSCfgEstqGeradoEFD.FPeriApu    := PeriApu;
    //
    FmVSCfgEstqGeradoEFD.PreencheAnoMesPeriodo(AnoMes);
    FmVSCfgEstqGeradoEFD.ShowModal;
    FmVSCfgEstqGeradoEFD.Destroy;
  end;
end;

procedure TUnAppPF.MostraFormCfgEstqInNatEFD(ImporExpor, AnoMes, Empresa,
  PeriApu: Integer);
begin
  if DBCheck.CriaFm(TFmVSCfgEstqInNatEFD, FmVSCfgEstqInNatEFD, afmoNegarComAviso) then
  begin
    FmVSCfgEstqInNatEFD.FImporExpor := ImporExpor;
    FmVSCfgEstqInNatEFD.FAnoMes     := AnoMes;
    FmVSCfgEstqInNatEFD.FEmpresa    := Empresa;
    FmVSCfgEstqInNatEFD.FPeriApu    := PeriApu;
    //
    FmVSCfgEstqInNatEFD.PreencheAnoMesPeriodo(AnoMes);
    FmVSCfgEstqInNatEFD.ShowModal;
    FmVSCfgEstqInNatEFD.Destroy;
  end;
end;

procedure TUnAppPF.MostraFormCfgEstqOthersEFD(ImporExpor, AnoMes, Empresa,
  PeriApu: Integer);
begin
  if DBCheck.CriaFm(TFmVSCfgEstqOthersEFD, FmVSCfgEstqOthersEFD, afmoNegarComAviso) then
  begin
    FmVSCfgEstqOthersEFD.FImporExpor := ImporExpor;
    FmVSCfgEstqOthersEFD.FAnoMes     := AnoMes;
    FmVSCfgEstqOthersEFD.FEmpresa    := Empresa;
    FmVSCfgEstqOthersEFD.FPeriApu    := PeriApu;
    //
    FmVSCfgEstqOthersEFD.PreencheAnoMesPeriodo(AnoMes);
    FmVSCfgEstqOthersEFD.ShowModal;
    FmVSCfgEstqOthersEFD.Destroy;
  end;
end;

procedure TUnAppPF.MostraFormCfgEstqVSxPQ_EFD(ImporExpor, AnoMes, Empresa,
  PeriApu: Integer);
begin
  if DBCheck.CriaFm(TFmVSCfgEstqVSxPQ_EFD, FmVSCfgEstqVSxPQ_EFD, afmoNegarComAviso) then
  begin
    FmVSCfgEstqVSxPQ_EFD.FImporExpor := ImporExpor;
    FmVSCfgEstqVSxPQ_EFD.FAnoMes     := AnoMes;
    FmVSCfgEstqVSxPQ_EFD.FEmpresa    := Empresa;
    FmVSCfgEstqVSxPQ_EFD.FPeriApu    := PeriApu;
    //
    FmVSCfgEstqVSxPQ_EFD.PreencheAnoMesPeriodo(AnoMes);
    FmVSCfgEstqVSxPQ_EFD.ShowModal;
    FmVSCfgEstqVSxPQ_EFD.Destroy;
  end;
end;

procedure TUnAppPF.MostraFormCfgMovEFD(ImporExpor, AnoMes, Empresa, PeriApu: Integer);
begin
  VS_PF.MostraFormVSCfgMovEFD(ImporExpor, AnoMes, Empresa, PeriApu);
end;

procedure TUnAppPF.MostraFormOP(OP, ID_Item: String);
var
  MovimID, Codigo, MovimCod, Controle: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    MovimCod := Geral.IMV(OP);
    if MovimCod = 0 then
    begin
      Geral.MB_Aviso('A OP ' + OP + ' n�o � um numero v�lido!');
      Exit;
    end;
    UnDmkDAC_PF.AbremySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM vsmovcab ',
    'WHERE Codigo=' + Geral.FF0(MovimCod),
    '']);
    //
    MovimID  := Qry.FieldByName('MovimID').AsInteger;
    Codigo   := Qry.FieldByName('CodigoID').AsInteger;
    Controle := Geral.IMV(ID_Item); // nao sei
    //
    VS_PF.MostraFormVS_XXX(MovimID, Codigo, Controle);
  finally
    Qry.Free;
  end;
end;

procedure TUnAppPF.MostraFormXxXxxXxxGraGruY(GraGruY, GraGruX: Integer;
  Edita: Boolean; Qry: TmySQLQuery);
const
  sProcName = 'TUnAppPF.MostraFormXxXxxXxxGraGruY()';
begin
  case GraGruY of
    CO_GraGruY_0512_VSSubPrd: VS_PF.MostraFormVSSubPrd(GraGruX, True);
    CO_GraGruY_0683_VSPSPPro: VS_PF.MostraFormVSPSPPro(GraGruX, True);
    CO_GraGruY_0853_VSPSPEnd: VS_PF.MostraFormVSPSPEnd(GraGruX, True, Qry);
    CO_GraGruY_1024_VSNatCad: VS_PF.MostraFormVSNatCad(GraGruX, True, Qry);
    CO_GraGruY_1072_VSNatInC: VS_PF.MostraFormVSNatInC(GraGruX, True);
    CO_GraGruY_1088_VSNatCon: VS_PF.MostraFormVSNatCon(GraGruX, True, Qry);
    CO_GraGruY_1195_VSNatPDA: VS_PF.MostraFormVSNatPDA(GraGruX, True);
    CO_GraGruY_1365_VSProCal: VS_PF.MostraFormVSProCal(GraGruX, True);
    CO_GraGruY_1536_VSCouCal: VS_PF.MostraFormVSCouCal(GraGruX, True);
    CO_GraGruY_1621_VSCouDTA: VS_PF.MostraFormVSCouDTA(GraGruX, True);
    CO_GraGruY_1707_VSProCur: VS_PF.MostraFormVSProCur(GraGruX, True);
    CO_GraGruY_1877_VSCouCur: VS_PF.MostraFormVSCouCur(GraGruX, True);
    CO_GraGruY_2048_VSRibCad: VS_PF.MostraFormVSRibCad(GraGruX, True);
    CO_GraGruY_3072_VSRibCla: VS_PF.MostraFormVSRibCla(GraGruX, True, Qry);
    CO_GraGruY_4096_VSRibOpe: VS_PF.MostraFormVSRibOpe(GraGruX, True);
    CO_GraGruY_5120_VSWetEnd: VS_PF.MostraFormVSWetEnd(GraGruX, True, Qry(*, MultiplosGGX*));
    CO_GraGruY_6144_VSFinCla: VS_PF.MostraFormVSFinCla(GraGruX, True, Qry(*, MultiplosGGX*));
    CO_GraGruY_7168_VSRepMer: VS_PF.MostraFormVSRepMer(GraGruX, True, Qry(*, MultiplosGGX*));
   else Geral.MB_Erro('O grupo de estoque n�mero ' + Geral.FF0(GraGruY) +
    ' n�o est� implementado em ' + sProcName);
   end;
end;

procedure TUnAppPF.MostraJanelaOrigemInfoIncCab(InfoIncCab: Integer);
const
  sProcName = 'UnAppPF.MostraJanelaOrigemInfoIncCab()';
var
  ModuloNiv1, Codigo, MovimCod, Controle, NivNiv, NivSel: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM infoinccab ',
    'WHERE Codigo=' + Geral.FF0(InfoIncCab),
    '']);
    //
    ModuloNiv1 := Qry.FieldByName('ModuloNiv1').AsInteger;
    case TDmkModuloApp(ModuloNiv1) of
      mdlappVS:
      begin
        Codigo   := Qry.FieldByName('Nivel1').AsInteger;
        MovimCod := Qry.FieldByName('Nivel0').AsInteger;
        Controle := Qry.FieldByName('Nivel2').AsInteger;
        //
        NivNiv   := Qry.FieldByName('NivNivel').AsInteger;
        NivSel   := Qry.FieldByName('NivValor').AsInteger;
        //
        case NivNiv of
          0:   VS_PF.MostroFormVSMovimCod(MovimCod);
          1:   VS_PF.MostroFormVSMovimCod(MovimCod);
          2:   VS_PF.MostraFormVSMovIts(Controle);
          else Geral.MB_Erro('"NivNivel" n�o implementado em ' + sProcName);
        end;
      end;
      else Geral.MB_Erro('M�dulo "' + GetEnumName(TypeInfo(TDmkModuloApp),
      ModuloNiv1) + '" n�o implementado em ' + sProcName);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnAppPF.MostraOrigemFat(FatParcRef: Integer);
begin
  // Compatibilidade
end;

procedure TUnAppPF.MostraOrigemFatGenerico(FatID: Integer; FatNum: Double;
  FatParcela, FatParcRef: Integer);
  //
  function ObtemCodigoID(const FatNum: Double; var MovimID, CodigoID: Integer): Boolean;
  const
    sProcName = 'UnAppPF.MostraOrigemFatGenerico.ObtemCodigoID()';
  var
    Query: TmySQLQuery;
  begin
    Result   := False;
    MovimID  := 0;
    CodigoID := 0;
    Query := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
      'SELECT MovimID, CodigoID ',
      'FROM vsmovcab ',
      'WHERE Codigo=' + Geral.FF0(Trunc(FatNum)),
      '']);
      //
      MovimID  := Query.FieldByName('MovimID').AsInteger;
      CodigoID := Query.FieldByName('CodigoID').AsInteger;
      //
      Result := (CodigoID <> 0) and (MovimID <> 0);
      //
      if Result = False then
        Geral.MB_Aviso(
          'N�o foi poss�vel obter o "CodigoID" para o "VSMovCab.Codigo" ' +
          Geral.FF0(Trunc(FatID)) + sLineBreak + sProcName);
    finally
      FreeAndNil(Query);
    end;
  end;
var
  //xEnumName: String;
  MovimID, CodigoID: Integer;
begin
  case FatID of
    0: Geral.MB_Info('Este lan�amento n�o foi gerado em nenhuma janela espec�fica!');
    1001, 1002: // Compra e frete de PQ (Uso e consumo)
      PQ_PF.MostraFormPQE(Trunc(FatNum));
    1003, 1004, 1010: // MP: Compra, frete, comiss�o
    begin
      //IC3_ED_FatNum - MovimCod
      //VS_PF.MostraFormVSInnCab(Trunc(FatNum), 0, 0);
      //procedure TUnVS_CRC_PF.MostraFormVS_XXX(MovimID, Codigo, Controle: Integer);
      if ObtemCodigoID(FatNum, MovimID, CodigoID) then
        VS_CRC_PF.MostraFormVS_XXX(MovimID, CodigoID, 0);
    end else
    begin
      //xEnumName := GetEnumName(TypeInfo(TItemTuplePurpose), Integer(FRCampos.Purpose))
       Geral.MB_Info('"FatID" ' + Geral.FF0(FatID) + ' n�o implementado! Solicite � Dermatek!');
    end;
  end;
end;

procedure TUnAppPF.MostraPopupDeCadsatroEspecificoDeGraGruXY(Form: TForm;
  Sb: TSpeedButton; PM: TPopupMenu);
begin
  MyObjects.MostraPopOnControlXY(PM, Sb, 0, 0);
end;

function TUnAppPF.NFeJaLancada(ChaveNFe: String): Boolean;
var
  Qry: TmySQLQuery;
  Entrada: Integer;
begin
  Result := False;
  if ChaveNFe = EmptyStr then Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM pqe',
    'WHERE refNFe="' + Geral.SoNumero_TT(ChaveNFe) + '"',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Entrada := Qry.Fields[0].AsInteger;
      Result := Entrada <> 0;
      if Result then
        Geral.MB_Aviso('Esta NFe j� foi lan�ada na entrada de uso e consumo n� '
        + Geral.FF0(Entrada));
    end;
  finally
    Qry.Free;
  end;
end;

(*
procedure TUnAppPF.MostraFormSPED_EFD_X999_Balancos(ImporExpor, AnoMes, Empresa,
  LinArq: Integer; Registro: String; Data: TDateTime);
var
  Ano, Mes: Word;
begin
  if DBCheck.CriaFm(TFmSPED_EFD_X999_Balancos, FmSPED_EFD_X999_Balancos, afmoNegarComAviso) then
  begin
    FmSPED_EFD_X999_Balancos.FImporExpor := ImporExpor;
    FmSPED_EFD_X999_Balancos.FAnoMes     := AnoMes;
    FmSPED_EFD_X999_Balancos.FEmpresa    := Empresa;
    FmSPED_EFD_X999_Balancos.FRegPai     := LinArq;
    FmSPED_EFD_X999_Balancos.FData       := Data;
    //
    dmkPF.AnoMesDecode(AnoMes, Ano, Mes);
    FmSPED_EFD_X999_Balancos.CBMes.ItemIndex := Mes - 1;
    FmSPED_EFD_X999_Balancos.CBAno.Text      := Geral.FF0(Ano);
    FmSPED_EFD_X999_Balancos.TPData.Date     := Data;
    //
    if Registro = 'H010' then
    begin
      FmSPED_EFD_X999_Balancos.PCRegistro.ActivePageIndex := 1;
      FmSPED_EFD_X999_Balancos.LaData.Visible             := False;
      FmSPED_EFD_X999_Balancos.TPData.Visible             := False;
      FmSPED_EFD_X999_Balancos.PnPeriodo.Enabled          := True;
    end else
    if Registro = 'K200' then
    begin
      FmSPED_EFD_X999_Balancos.PCRegistro.ActivePageIndex := 2;
      FmSPED_EFD_X999_Balancos.LaData.Visible             := True;
      FmSPED_EFD_X999_Balancos.TPData.Visible             := True;
      FmSPED_EFD_X999_Balancos.PnPeriodo.Enabled          := False;
    end else
      FmSPED_EFD_X999_Balancos.PCRegistro.ActivePageIndex := 0;
    //
    if FmSPED_EFD_X999_Balancos.PCRegistro.ActivePageIndex > 0 then
      FmSPED_EFD_X999_Balancos.ShowModal
    else
      Geral.MB_Erro('Registro n�o implementdo em balan�o de SPED EFD: ' +
      Registro);
    FmSPED_EFD_X999_Balancos.Destroy;
  end;
end;
*)

function TUnAppPF.ObtemNomeDeIDMovIts(MovID: Integer): String;
begin
  Result := sEstqMovimID_FRENDLY[MovID];
end;

function TUnAppPF.ObtemNomeTabelaGraGruY(GraGruY: Integer): String;
const
  sProcName = 'TUnAppPF.ObtemNomeTabelaGraGruY()';
begin
  case GraGruY of
    -1: Result := '';
    CO_GraGruY_0512_VSSubPrd: Result := LowerCase('VSSubPrd');
    CO_GraGruY_0683_VSPSPPro: Result := LowerCase('VSPSPPro');
    CO_GraGruY_0853_VSPSPEnd: Result := LowerCase('VSPSPEnd');
    CO_GraGruY_1024_VSNatCad: Result := LowerCase('VSNatCad');
    CO_GraGruY_1072_VSNatInC: Result := LowerCase('VSNatInC');
    CO_GraGruY_1088_VSNatCon: Result := LowerCase('VSNatCon');
    CO_GraGruY_1195_VSNatPDA: Result := LowerCase('VSNatpda');
    CO_GraGruY_1365_VSProCal: Result := LowerCase('VSProCal');
    CO_GraGruY_1536_VSCouCal: Result := LowerCase('VSCouCal');
    CO_GraGruY_1621_VSCouDTA: Result := LowerCase('VSCouDTA');
    CO_GraGruY_1707_VSProCur: Result := LowerCase('VSProCur');
    CO_GraGruY_1877_VSCouCur: Result := LowerCase('VSCouCur');
    CO_GraGruY_2048_VSRibCad: Result := LowerCase('VSRibCad');
    CO_GraGruY_3072_VSRibCla: Result := LowerCase('VSRibCla');
    CO_GraGruY_4096_VSRibOpe: Result := LowerCase('VSRibOpe');
    CO_GraGruY_5120_VSWetEnd: Result := LowerCase('VSWetEnd');
    CO_GraGruY_6144_VSFinCla: Result := LowerCase('VSFinCla');
    CO_GraGruY_7168_VSRepMer: Result := LowerCase('VSRepMer');
    else
    begin
      Result := '';
      Geral.MB_Erro('O grupo de estoque n�mero ' + Geral.FF0(GraGruY) +
      ' n�o est� implementado em ' + sProcName);
    end;
  end;
end;

function TUnAppPF.ObtemNumeroDeCODIF(Atual: Integer): Integer;
var
  Col: String;
begin
  Result := Atual;
  Col := '';
  if InputQuery('Receita da amostra', 'Defina o C�digo Alfanum�rico', Col) then
  begin
    Col := Uppercase(Col);
    Result := -dmkPF.CODIFToInt_Receita(Col);
  end;
end;

function TUnAppPF.ObtemTabelaDeFatID(FatID: Integer): String;
const
  sProcName = 'TUnAppPF.ObtemTabelaDeFatID()';
begin
  case FatID of
    VAR_FATID_0000,
    VAR_FATID_0020,
    VAR_FATID_0120: Result := 'balancos';
    VAR_FATID_0110: Result := 'emit';
    VAR_FATID_0150: Result := 'pqt';
    VAR_FATID_0170: Result := 'pqd';
    VAR_FATID_0180: Result := 'pqi';
    VAR_FATID_0185: Result := 'pqn';
    VAR_FATID_0190: Result := 'pqo';
    else
    begin
      Result := '???';
      Geral.MB_Erro('FatID n�o implementado em ' + sProcName);
    end;
  end;
end;

function TUnAppPF.ReabreSPEDEFD0200Exportacao(Qry: TmySQLQuery; TabName: String): Boolean;
begin
  Result := False;
(*
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, DmodG.MyPID_DB, [
  'SELECT ggx.Controle, ggx.GraGru1, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, gg1.cGTIN_EAN , gg1.NCM, ',
  'gg1.EX_TIPI, gg1.COD_LST, gg1.SPEDEFD_ALIQ_ICMS, ',
  //'unm.SIGLA, pgt.Tipo_Item ',
  'unm.SIGLA, gg1.prod_CEST, ',
  'IF(cou.CouNiv2 IN (2,3), 5/*subproduto*/, ',
  'IF(gg1.Nivel2 <> 0, gg2.Tipo_Item, pgt.Tipo_Item)) Tipo_Item ',
  //
  'FROM ' + TMeuDB + '.gragrux ggx ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2 ',
  'LEFT JOIN ' + TMeuDB + '.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB + '.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou ON cou.GraGruX=ggx.Controle ',
  'LEFT JOIN ' + TMeuDB + '.couniv2 nv2 ON nv2.Codigo=cou.CouNiv2 ',
  'WHERE ggx.Controle IN ( ',
  '  SELECT DISTINCT GraGruX  ',
  '  FROM ' + TabName,
  ') ',
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  '']);
*)
end;

function TUnAppPF.SQLPesqVeiculo(): String;
begin
  Result := Geral.ATS([
  'SELECT vei.Codigo, vei.Codigo CodUsu, ',
  'CONCAT(vei.Placa, ": ",  ',
  'vma.Nome, " ", vmo.Nome) Nome   ',
  'FROM veiculos vei  ',
  'LEFT JOIN veicmarcas vma ON vma.Codigo=vei.Marca  ',
  'LEFT JOIN veicmodels vmo ON vmo.Codigo=vei.Modelo  ',
  'WHERE vei.Codigo > 0  ',
  'AND vei.Placa Like "' + CO_JOKE_SQL + '"  ',
  'OR vma.Nome Like "' + CO_JOKE_SQL + '"  ',
  'OR vmo.Nome Like "' + CO_JOKE_SQL + '"  ',
  'ORDER BY placa  ',
  '']);
end;

function TUnAppPF.TextoDeCodHist(CodHist: Integer): String;
begin
  // Cpmpatibilidade???
  // Ver   t o o l r e n t
  Result := '?';
end;

function TUnAppPF.TextoDeCodHist_Sigla(CodHist: Integer): String;
begin
  // Compatibilidade?????
  Result := '';
end;

procedure TUnAppPF.VerificaCadastroXxArtigoIncompleta;
var
  Qry: TmySQLQuery;
begin
  if Dmod = nil then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ggx.Controle, gg1.Nome, ',
      'ggx.GraGruY, ggc.GraGruX, ggc.CouNiv1, ',
      'co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2, ',
      'ggc.MediaMinKg, ggc.MediaMaxKg  ',
      'FROM gragrux ggx  ',
      'LEFT JOIN gragruxcou ggc ON ggc.GraGruX=ggx.Controle ',
      'LEFT JOIN couniv1 co1 ON co1.Codigo=ggc.CouNiv1 ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'WHERE ggx.GraGruY<>0 ',
      'AND ggc.GraGruX IS NULL ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Existem artigos com cadastro incompleto!');
      //
      if DBCheck.CriaFm(TFmGraGruYIncompleto, FmGraGruYIncompleto, afmoNegarComAviso) then
      begin
        FmGraGruYIncompleto.ReopenIncompleto(Qry);
        FmGraGruYIncompleto.ShowModal;
        FmGraGruYIncompleto.Destroy;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnAppPF.VerificaGraGruYsNaoConfig();
const
  sProcName = 'TUnAppPF.VerificaGraGruYsNaoConfig()';
var
  Nome, TitNiv1, TitNiv2, TitNiv3, TitNiv4, TitNiv5: String;
  Codigo, CodUsu, MadeBy, Fracio, Gradeado, TipPrd, NivCad, FaixaIni, FaixaFim,
  Customizav, ImpedeCad, LstPrcFisc, Tipo_Item, Genero, GraTabApp: Integer;
  PerComissF, PerComissR: Double;
  //
  function GeraPrdGrupTip(): Integer;
  begin
    Result := 0;
    Codigo := UMyMod.BuscaEmLivreY_Def('prdgruptip', 'Codigo', stIns, 0);
    CodUsu := Codigo;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgruptip', False, [
    'CodUsu', 'Nome', 'MadeBy',
    'Fracio', 'Gradeado', 'TipPrd',
    'NivCad', 'FaixaIni', 'FaixaFim',
    'TitNiv1', 'TitNiv2', 'TitNiv3',
    'TitNiv4', 'TitNiv5', 'Customizav',
    'ImpedeCad', 'LstPrcFisc', 'PerComissF',
    'PerComissR', 'Tipo_Item', 'Genero',
    'GraTabApp'], [
    'Codigo'], [
    CodUsu, Nome, MadeBy,
    Fracio, Gradeado, TipPrd,
    NivCad, FaixaIni, FaixaFim,
    TitNiv1, TitNiv2, TitNiv3,
    TitNiv4, TitNiv5, Customizav,
    ImpedeCad, LstPrcFisc, PerComissF,
    PerComissR, Tipo_Item, Genero,
    GraTabApp], [
    Codigo], True) then
      Result := Codigo
  end;
var
  Qry: TmySQLQuery;
  GraGruY, PrdGrupTip: Integer;
  Inclui: Boolean;
begin
////////////////////////////////////////////////////////////////////////////////
   //EXIT;  // Campo eliminado!!!!!!!!
////////////////////////////////////////////////////////////////////////////////
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM gragruy ',
    'WHERE PrdGrupTip=0 ',
    'AND Codigo <> 0 ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(Qry.RecordCount) +
      ' Grupos de Estoque sem Grupo de Produto definido!' + sLineBreak +
      'Deseja que o(s) Grupo(s) de Produto sejam criado(s) e atrelados aos respectivos Grupos de Estoque?')
      = ID_YES then
      begin
        TitNiv2        := '';
        TitNiv3        := '';
        TitNiv4        := '';
        TitNiv5        := '';
        NivCad         := 1;
        FaixaIni       := 0;
        FaixaFim       := 0;
        Customizav     := 0;
        ImpedeCad      := 1;
        LstPrcFisc     := 5;
        PerComissF     := 0;
        PerComissR     := 0;
        Tipo_Item      := 0;
        Genero         := 0;
        GraTabApp      := 0;
        //
        Qry.First;
        while not Qry.Eof do
        begin
          GraGruY := Qry.FieldByName('Codigo').AsInteger;
          Inclui  := True;
          case GraGruY of
            CO_GraGruY_0512_VSSubPrd:
            begin
              Nome           := CO_TXT_GraGruY_0512_VSSubPrd;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 5;  // subproduto
            end;
            CO_GraGruY_0683_VSPSPPro:
            begin
              Nome           := CO_TXT_GraGruY_0683_VSPSPPro;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_0853_VSPSPEnd:
            begin
              Nome           := CO_TXT_GraGruY_0853_VSPSPEnd;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;
            CO_GraGruY_1024_VSNatCad:
            begin
              Nome           := CO_TXT_GraGruY_1024_VSNatCad;
              MadeBy         := 2;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 2;
              Tipo_Item      := 1; // materia-prima
            end;
            CO_GraGruY_1072_VSNatInC:
            begin
              Nome           := CO_TXT_GraGruY_1072_VSNatInC;
              MadeBy         := 2;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_1088_VSNatCon:
            begin
              Nome           := CO_TXT_GraGruY_1088_VSNatCon;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1; // ???
              Tipo_Item      := 4; // Produto Pronto
            end;
            CO_GraGruY_1195_VSNatPDA:
            begin
              Nome           := CO_TXT_GraGruY_1195_VSNatPDA;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              Tipo_Item      := 6; // intermediario
            end;
            CO_GraGruY_1365_VSProCal:
            begin
              Nome           := CO_TXT_GraGruY_1365_VSProCal;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_1536_VSCouCal:
            begin
              Nome           := CO_TXT_GraGruY_1536_VSCouCal;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 6; // intermediario
            end;
            CO_GraGruY_1621_VSCouDTA:
            begin
              Nome           := CO_TXT_GraGruY_1621_VSCouDTA;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 6; // intermediario
            end;
            CO_GraGruY_1707_VSProCur:
            begin
              Nome           := CO_TXT_GraGruY_1707_VSProCur;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_1877_VSCouCur:
            begin
              Nome           := CO_TXT_GraGruY_1877_VSCouCur;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;
            CO_GraGruY_2048_VSRibCad:
            begin
              Nome           := CO_TXT_GraGruY_2048_VSRibCad;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;
            CO_GraGruY_3072_VSRibCla:
            begin
              Nome           := CO_TXT_GraGruY_3072_VSRibCla;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;
            CO_GraGruY_4096_VSRibOpe:
            begin
              Nome           := CO_TXT_GraGruY_4096_VSRibOpe;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_5120_VSWetEnd:
            begin
              Nome           := CO_TXT_GraGruY_5120_VSWetEnd;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 1;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_6144_VSFinCla:
            begin
              Nome           := CO_TXT_GraGruY_6144_VSFinCla;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 1;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;
            CO_GraGruY_7168_VSRepMer:
            begin
              Nome           := CO_TXT_GraGruY_7168_VSRepMer;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 1;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            else
            begin
              Geral.MB_Erro(
              '"GraGruY" n�o definido em "' + sProcName + '"');
              Inclui := False;
            end;
          end;
          if Inclui then
          begin
            TitNiv1 := Nome;
            PrdGrupTip := GeraPrdGrupTip();
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragruy', False, [
            'PrdGrupTip'], [
            'Codigo'], [
            PrdGrupTip], [
            GraGruY], True);
          end;
          Qry.Next;
        end;
      end;
      Qry.First;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnAppPF.VerificaSeExisteIMEC(EdTxt: TdmkEdit; IMEC: Integer);
var
  Qry: TmySQLQuery;
  Tabela, CampoDt, CampoIdx: String;
  MovimID: TEstqMovimID;
  DataAbertura: TDateTime;
  Codigo, IMEI: Integer;
begin
  if IMEC = 0 then
  begin
    EdTxt.Text := '';
    Exit;
  end;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM vsmovcab ',
    'WHERE Codigo=' + Geral.FF0(IMEC),
    '']);
    if Qry.RecordCount = 0 then
    begin
      EdTxt.Font.Color := clRed;
      EdTxt.Text := 'IME-C n�o localizado';
      Exit;
    end;
    //
    Codigo  := Qry.FieldByName('CodigoID').AsInteger;
    MovimID := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
    //
    EdTxt.Text := sEstqMovimID[Integer(MovimID)];
    if MovimID = TEstqMovimID.emidPreReclasse then
    begin
      EdTxt.Font.Color := clPurple;
      //EdTxt.Text := 'Pr� classe/reclasse';
      Exit;
    end
    else
    if MovimID in ([TEstqMovimID.emidClassArtXXUni,TEstqMovimID.emidReclasXXUni]) then
    begin
      case MovimID of
        TEstqMovimID.emidClassArtXXUni: Tabela := 'vspaclacaba';
        TEstqMovimID.emidReclasXXUni  : Tabela := 'vsparclcaba';
        else Tabela := '???';
      end;
      CampoDt := 'DataHora';
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ' + CO_SEL_TAB_VMI + ' ',
      'FROM ' + Tabela,
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
      IMEI := Qry.FieldByName(CO_SEL_TAB_VMI).AsInteger;
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ' + CampoDt,
      'FROM ' + CO_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(IMEI),
      '']);
      DataAbertura := Qry.FieldByName(CampoDt).AsDateTime;
    end
    else
    begin
      Tabela  := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
      CampoDt := VS_PF.ObtemNomeDtAberturaVSXxxCab(MovimID);
      if MovimID = TEstqMovimID.emidMixInsum then
        CampoIdx := 'VSMovCod'
      else
        CampoIdx := 'MovimCod';
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ' + CampoDt,
      'FROM ' + Tabela,
      'WHERE ' + CampoIdx + '=' + Geral.FF0(IMEC),
      '']);
      DataAbertura := Qry.FieldByName(CampoDt).AsDateTime;
    end;
    EdTxt.Font.Color := clBlue;
    EdTxt.Text := 'OK! ID: ' + Geral.FF0(Codigo) + ' Data abertura: ' +
    Geral.FDT(DataAbertura, 2) + ' ' + EdTxt.Text;
  finally
    Qry.Free;
  end;
end;

end.
