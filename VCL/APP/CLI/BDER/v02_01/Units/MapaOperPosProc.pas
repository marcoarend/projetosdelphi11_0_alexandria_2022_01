unit MapaOperPosProc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, dmkEditCB, DBCtrls,
  dmkDBLookupComboBox, DB, mySQLDbTables, Grids, DBGrids, dmkDBGridDAC,
  ComCtrls, dmkEditDateTimePicker, dmkCheckBox, dmkCheckGroup, UnDmkProcFunc,
  dmkGeral, dmkImage, UnDmkEnums, DmkDAC_PF, frxClass, frxDBSet,
  UnProjGroup_Consts, UnAppPF, Vcl.Menus,
  UnGOTOy, AppListas;

type
  TFmMapaOperPosProc = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    DBGPesq: TDBGrid;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqDataEmis: TDateTimeField;
    QrPesqStatus: TSmallintField;
    QrPesqNumero: TIntegerField;
    QrPesqNOMECI: TWideStringField;
    QrPesqNOMESETOR: TWideStringField;
    QrPesqTecnico: TWideStringField;
    QrPesqNOME: TWideStringField;
    QrPesqClienteI: TIntegerField;
    QrPesqTipificacao: TSmallintField;
    QrPesqTempoR: TIntegerField;
    QrPesqTempoP: TIntegerField;
    QrPesqSetor: TSmallintField;
    QrPesqTipific: TSmallintField;
    QrPesqEspessura: TWideStringField;
    QrPesqDefPeca: TWideStringField;
    QrPesqPeso: TFloatField;
    QrPesqCusto: TFloatField;
    QrPesqQtde: TFloatField;
    QrPesqAreaM2: TFloatField;
    QrPesqFulao: TWideStringField;
    QrPesqObs: TWideStringField;
    QrPesqLk: TIntegerField;
    QrPesqDataCad: TDateField;
    QrPesqDataAlt: TDateField;
    QrPesqUserCad: TIntegerField;
    QrPesqUserAlt: TIntegerField;
    QrPesqAlterWeb: TSmallintField;
    QrPesqAtivo: TSmallintField;
    EdSetor: TdmkEditCB;
    Label5: TLabel;
    CBSetor: TdmkDBLookupComboBox;
    QrSetores: TmySQLQuery;
    DsSetores: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtPesquisa: TBitBtn;
    BtLocaliza: TBitBtn;
    Label4: TLabel;
    LaQtde: TLabel;
    BtSaida: TBitBtn;
    frxQUI_RECEI_025_A: TfrxReport;
    frxDsPesq: TfrxDBDataset;
    BtImprime: TBitBtn;
    BtEmitMopp: TBitBtn;
    QrPesqSetrEmi: TSmallintField;
    QrPesqSourcMP: TSmallintField;
    QrPesqCod_Espess: TIntegerField;
    QrPesqCodDefPeca: TIntegerField;
    QrPesqCustoTo: TFloatField;
    QrPesqCustoKg: TFloatField;
    QrPesqCustoM2: TFloatField;
    QrPesqCusUSM2: TFloatField;
    QrPesqEmitGru: TIntegerField;
    QrPesqRetrabalho: TSmallintField;
    QrPesqSemiCodPeca: TIntegerField;
    QrPesqSemiTxtPeca: TWideStringField;
    QrPesqSemiPeso: TFloatField;
    QrPesqSemiQtde: TFloatField;
    QrPesqSemiAreaM2: TFloatField;
    QrPesqSemiRendim: TFloatField;
    QrPesqSemiCodEspe: TIntegerField;
    QrPesqSemiTxtEspe: TWideStringField;
    QrPesqBRL_USD: TFloatField;
    QrPesqBRL_EUR: TFloatField;
    QrPesqDtaCambio: TDateField;
    QrPesqVSMovCod: TIntegerField;
    QrPesqHoraIni: TTimeField;
    QrPesqGraCorCad: TIntegerField;
    QrPesqNoGraCorCad: TWideStringField;
    QrPesqSemiCodEspReb: TIntegerField;
    QrPesqSemiTxtEspReb: TWideStringField;
    QrPesqDtaBaixa: TDateField;
    QrPesqDtCorrApo: TDateTimeField;
    QrPesqEmpresa: TIntegerField;
    QrPesqAWServerID: TIntegerField;
    QrPesqAWStatSinc: TSmallintField;
    QrPesqSbtCouIntros: TFloatField;
    QrPesqSbtAreaM2: TFloatField;
    QrPesqSbtAreaP2: TFloatField;
    QrPesqSbtRendEsper: TFloatField;
    QrPesqOperPosStatus: TSmallintField;
    QrPesqOperPosGrandz: TIntegerField;
    QrPesqOperPosQtdDon: TFloatField;
    QrPesqOperPosDthIni: TDateTimeField;
    QrPesqOperPosDthFim: TDateTimeField;
    PMEmitMopp: TPopupMenu;
    Incluinovaoperaopsprocesso1: TMenuItem;
    Alteraoperaopsprocessoselecionada1: TMenuItem;
    Excluioperaopsprocessoselecionada1: TMenuItem;
    QrEmitMopp: TMySQLQuery;
    DsEmitMopp: TDataSource;
    DBGrid1: TDBGrid;
    QrSumMopp: TMySQLQuery;
    QrEmitMoppCodigo: TIntegerField;
    QrEmitMoppControle: TIntegerField;
    QrEmitMoppStatus: TSmallintField;
    QrEmitMoppGrandz: TIntegerField;
    QrEmitMoppQtdDon: TFloatField;
    QrEmitMoppDthIni: TDateTimeField;
    QrEmitMoppDthFim: TDateTimeField;
    QrEmitMoppLk: TIntegerField;
    QrEmitMoppDataCad: TDateField;
    QrEmitMoppDataAlt: TDateField;
    QrEmitMoppUserCad: TIntegerField;
    QrEmitMoppUserAlt: TIntegerField;
    QrEmitMoppAlterWeb: TSmallintField;
    QrEmitMoppAWServerID: TIntegerField;
    QrEmitMoppAWStatSinc: TSmallintField;
    QrEmitMoppAtivo: TSmallintField;
    QrSumMoppStatus: TSmallintField;
    QrSumMoppQtdDon: TFloatField;
    QrSumMoppDthIni: TDateTimeField;
    QrSumMoppDthFim: TDateTimeField;
    QrPesqNO_OperPosStatus: TWideStringField;
    N1: TMenuItem;
    Indefinirvrios1: TMenuItem;
    desabilitarmododeindefinirvrios1: TMenuItem;
    QrPesqDtHrFinal: TDateTimeField;
    Memo1: TMemo;
    Redefinirselecionados1: TMenuItem;
    CGOperPosStatus: TdmkCheckGroup;
    BtTodosTipCad: TBitBtn;
    BtNenhumTipCad: TBitBtn;
    BitBtn1: TBitBtn;
    QrSetoresCodigo: TIntegerField;
    QrSetoresNome: TWideStringField;
    QrSetoresOperPosProcDescr: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure BtLocalizaClick(Sender: TObject);
    procedure DBGPesqDblClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxQUI_RECEI_025_AGetValue(const VarName: string;
      var Value: Variant);
    procedure RGOperPosStatusClick(Sender: TObject);
    procedure EdSetorRedefinido(Sender: TObject);
    procedure BtEmitMoppClick(Sender: TObject);
    procedure Incluinovaoperaopsprocesso1Click(Sender: TObject);
    procedure Alteraoperaopsprocessoselecionada1Click(Sender: TObject);
    procedure Excluioperaopsprocessoselecionada1Click(Sender: TObject);
    procedure PMEmitMoppPopup(Sender: TObject);
    procedure QrPesqAfterScroll(DataSet: TDataSet);
    procedure Indefinirvrios1Click(Sender: TObject);
    procedure desabilitarmododeindefinirvrios1Click(Sender: TObject);
    procedure Redefinirselecionados1Click(Sender: TObject);
    procedure BtTodosTipCadClick(Sender: TObject);
    procedure BtNenhumTipCadClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CGOperPosStatusClick(Sender: TObject);
  private
    { Private declarations }
    //
    procedure MostraFormEmitMopp(SQLType: TSQLType);
    procedure AtualizaTotaisMopp(Codigo, Controle: Integer);
    procedure MultiRedefinicaoDeStatus(NovoStatus: Integer);
    function  ImpedeInsAltDel(): Boolean;
  //
  public
    { Public declarations }
    FAptosAOperacao: Integer;
    FNaoReabrir: Boolean;
    //
    procedure ReabrePesquisa();
    procedure ReopenEmitMopp(Controle: Integer);
  end;

  var
  FmMapaOperPosProc: TFmMapaOperPosProc;

implementation

uses UnMyObjects, UMySQLModule, Module, PQxExcl, MyDBCheck, EmitMopp;

{$R *.DFM}

procedure TFmMapaOperPosProc.FormCreate(Sender: TObject);
begin
  FNaoReabrir := True;
  ImgTipo.SQLType := stLok;
  FAptosAOperacao := 28;
  //
  AppPF.ConfiguraCGMapaOperPosProc(CGOperPosStatus, (*Habilita*) True, (*Default*) 1);
  UnDmkDAC_PF.AbreQuery(QrSetores, Dmod.MyDB);
end;

procedure TFmMapaOperPosProc.Alteraoperaopsprocessoselecionada1Click(
  Sender: TObject);
begin
  if ImpedeInsAltDel() then Exit;
  //
  MostraFormEmitMopp(stUpd);
end;

procedure TFmMapaOperPosProc.AtualizaTotaisMopp(Codigo, Controle: Integer);
var
  OperPosDthIni, OperPosDthFim: String;
  OperPosStatus, OperPosGrandz: Integer;
  OperPosQtdDon: Double;
  SQLType: TSQLType;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumMopp, Dmod.MyDB, [
  'SELECT SUM(QtdDon) QtdDon, MAX(Status) Status, ',
  'MIN(DthIni) DthIni, MAX(DthFim) DthFim ',
  'FROM emitmopp ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  ' ']);
  SQLType        := stUpd;
  OperPosStatus  := QrSumMoppStatus.Value;
  OperPosGrandz  := 0;
  OperPosQtdDon  := QrSumMoppQtdDon.Value;
  OperPosDthIni  := Geral.FDT(QrSumMoppDthIni.Value, 109);
  OperPosDthFim  := Geral.FDT(QrSumMoppDthFim.Value, 109);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'emit', False, [
  'OperPosStatus', 'OperPosGrandz',
  'OperPosQtdDon', 'OperPosDthIni', 'OperPosDthFim'], [
  'Codigo'], [
  OperPosStatus, OperPosGrandz,
  OperPosQtdDon, OperPosDthIni, OperPosDthFim], [
  Codigo], True) then
  begin
    ReabrePesquisa();
    //
    ReopenEmitMopp(Controle);
  end;
end;

procedure TFmMapaOperPosProc.BitBtn1Click(Sender: TObject);
begin
  FNaoReabrir := True;
  CGOperPosStatus.Value := FAptosAOperacao;
  FNaoReabrir := False;
  ReabrePesquisa();
end;

procedure TFmMapaOperPosProc.BtEmitMoppClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMEmitMopp, BtEmitMopp);
end;

procedure TFmMapaOperPosProc.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxQUI_RECEI_025_A, 'Pesquisa de pesagens');
end;

procedure TFmMapaOperPosProc.BtLocalizaClick(Sender: TObject);
begin
{
  FmPQxExcl.EdPesagem.Text := IntToStr(QrPesqCodigo.Value);
  FmPQxExcl.ReopenEmit(True);
  Close;
}
end;

procedure TFmMapaOperPosProc.BtNenhumTipCadClick(Sender: TObject);
begin
  FNaoReabrir := True;
  CGOperPosStatus.Value := 0;
  FNaoReabrir := False;
  ReabrePesquisa();
end;

procedure TFmMapaOperPosProc.BtPesquisaClick(Sender: TObject);
{
  function ItensStatusPesagem: String;
  begin
    Result := 'AND em.Status in (';
    Result := Result + FormatFloat('0',
      dmkPF.IntInConjunto2Def(1, CGStatus.Value, 0, -1000)) + ',';
    Result := Result + FormatFloat('0',
      dmkPF.IntInConjunto2Def(2, CGStatus.Value, 1, -1000)) + ',';
    Result := Result + FormatFloat('0',
      dmkPF.IntInConjunto2Def(4, CGStatus.Value, 2, -1000));
    Result := Result + ')';
  end;
}
var
  Qry: TmySQLQuery;
  sVMIs: String;
begin
{
  QrPesq.Close;
  QrPesq.SQL.Clear;

  QrPesq.SQL.Add('SELECT DISTINCT em.*');
  QrPesq.SQL.Add('FROM emit em');
  if EdPQ.ValueVariant > 0 then
  begin
    case RGTipoProd.ItemIndex of
      0: QrPesq.SQL.Add('LEFT JOIN emitits ei ON ei.Codigo=em.Codigo');
      1: QrPesq.SQL.Add('LEFT JOIN pqx px ON px.OrigemCodi=em.Codigo AND px.Tipo=110');
    end;
  end;
  QrPesq.SQL.Add(dmkPF.SQL_Periodo('WHERE em.DataEmis ',
    TPDataIni.Date, TPDataFim.Date, True, True));
  if EdReceita.ValueVariant > 0 then
    QrPesq.SQL.Add('AND em.Numero = ' + FormatFloat('0', EdReceita.ValueVariant));
  if EdSetor.ValueVariant > 0 then
    QrPesq.SQL.Add('AND em.Setor = ' + FormatFloat('0', EdSetor.ValueVariant));
  if EdPQ.ValueVariant > 0 then
  begin
    case RGTipoProd.ItemIndex of
      0: QrPesq.SQL.Add('AND ei.Produto = ' + FormatFloat('0', EdPQ.ValueVariant));
      1:
      begin
        QrPesq.SQL.Add('AND px.Insumo = ' + FormatFloat('0', EdPQ.ValueVariant));
        if CkPesoZero.Checked then
          QrPesq.SQL.Add('AND px.Peso <> 0');
      end;
    end;
  end;
  if CkStatus.Checked then
    QrPesq.SQL.Add(ItensStatusPesagem);
  if CkFulao.Checked then
    QrPesq.SQL.Add('AND em.Fulao = "' + EdFulao.Text + '"');
  if CkCarga.Checked then
    QrPesq.SQL.Add('AND em.Peso BETWEEN "' +
      dmkPF.FFP(EdPesoMin.ValueVariant, 3) + '" AND "' +
      dmkPF.FFP(EdPesoMax.ValueVariant, 3) + '"');
  if CkQtde.Checked then
    QrPesq.SQL.Add('AND em.Qtde BETWEEN "' +
      dmkPF.FFP(EdQtdeMin.ValueVariant, 3) + '" AND "' +
      dmkPF.FFP(EdQtdeMax.ValueVariant, 3) + '"');
  // inicio 2018-08-22
  if Trim(EdMarca.Text) <> '' then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
(*
      'SELECT Controle ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE Marca = "' + EdMarca.Text + '" ',
*)
      'SELECT DISTINCT emc.Codigo',
      'FROM emitcus emc',
      'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=emc.VSMovIts',
      'WHERE vmi.Marca="' + EdMarca.Text + '" ',
      '']);
      sVMIs := '';
      Qry.First;
      while not Qry.Eof do
      begin
        if Qry.RecNo = 1 then
          sVMIs := Geral.FF0(Qry.FieldByName('Codigo').Value)
        else
          sVMIs := sVMIs + ', ' + Geral.FF0(Qry.FieldByName('Codigo').Value);
        //
        Qry.Next;
      end;
      if sVMIs <> '' then
        QrPesq.SQL.Add('AND em.Codigo IN (' + sVMIs + ')');
    finally
      Qry.Free;
    end;
  end;
  // fim 2018-08-22
  //
  QrPesq.SQL.Add('ORDER BY em.DataEmis DESC, em.Codigo DESC');
  UMyMod.AbreQuery(QrPesq, Dmod.MyDB, 'TFmPQExclPesq.BtPesquisaClick()');
  //Geral.MB_SQL(Self, QrPesq);
}
end;

procedure TFmMapaOperPosProc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMapaOperPosProc.BtTodosTipCadClick(Sender: TObject);
begin
  FNaoReabrir := True;
  CGOperPosStatus.Value := CGOperPosStatus.MaxValue;
  FNaoReabrir := False;
  ReabrePesquisa();
end;

procedure TFmMapaOperPosProc.CGOperPosStatusClick(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmMapaOperPosProc.DBGPesqDblClick(Sender: TObject);
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
    BtLocalizaClick(Self);
end;

procedure TFmMapaOperPosProc.desabilitarmododeindefinirvrios1Click(
  Sender: TObject);
begin
//  DBGPesq.Options := DBGPesq.Options - [dgRowSelect, dgMultiSelect];
end;

procedure TFmMapaOperPosProc.EdSetorRedefinido(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmMapaOperPosProc.Excluioperaopsprocessoselecionada1Click(
  Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  if ImpedeInsAltDel() then Exit;
  //
  Codigo := QrPesqCodigo.Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'emitmopp', 'Controle', QrEmitMoppControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrEmitMopp,
      QrEmitMoppControle, QrEmitMoppControle.Value);
    AtualizaTotaisMopp(Codigo, Controle);
  end;
end;

procedure TFmMapaOperPosProc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FNaoReabrir := False;
end;

procedure TFmMapaOperPosProc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMapaOperPosProc.frxQUI_RECEI_025_AGetValue(const VarName: string;
  var Value: Variant);
var
  N: Integer;
  Texto: String;
  I: Integer;
begin
  if AnsiCompareText(VarName, 'VAR_NO_STATUS') = 0 then
  begin
    if CGOperPosStatus.Value = FAptosAOperacao then
      Value := 'Aptos a opera��o de ' + Lowercase(QrSetoresOperPosProcDescr.Value)
    else
      Value := MyObjects.TextosDeItensSelecionadosDeCheckGroup(CGOperPosStatus);
  end
  else if AnsiCompareText(VarName, 'VAR_SET_NOME') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBSetor.Text, EdSetor.ValueVariant, 'TODOS')
  else if AnsiCompareText(VarName, 'VAR_SET_NOME2') = 0 then
    Value := Uppercase(CBSetor.Text)
  //else if AnsiCompareText(VarName, 'VAR_PERIODO') = 0 then
    //Value := dmkPF.PeriodoImp(TPDataIni.Date, TPDataFim.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'VARF_LINHAS_OBS' then
  begin
    //Value := Geral.CountOccurences(#13, QrPesqObs.Value);
    //LinhasNaoVazias(N, Texto);
    N := 0;
    for I := 0 to Memo1.Lines.Count - 1 do
    begin
      if Trim(Memo1.Lines[I]) <> EmptyStr then
        N := N + 1
    end;
    if N = 0 then
      N := 1;
    Value := N;
  end
  else if VarName = 'VARF_TEXTO_OBS' then
  begin
    Texto := EmptyStr;
    for I := 0 to Memo1.Lines.Count - 1 do
    begin
      if Trim(Memo1.Lines[I]) <> EmptyStr then
        Texto := Texto + Memo1.Lines[I] + sLineBreak
    end;
    Value := Texto;
    //Geral.MB_Info(Texto);
  end;
end;

procedure TFmMapaOperPosProc.Incluinovaoperaopsprocesso1Click(Sender: TObject);
begin
  if ImpedeInsAltDel() then Exit;
  //
  MostraFormEmitMopp(stIns);
end;

procedure TFmMapaOperPosProc.Indefinirvrios1Click(Sender: TObject);
begin
//  DBGPesq.Options := DBGPesq.Options + [dgRowSelect, dgMultiSelect];
end;

procedure TFmMapaOperPosProc.MostraFormEmitMopp(SQLType: TSQLType);
var
  Codigo, Controle: Integer;
begin
  Codigo := QrPesqCodigo.Value;
  //
  if DBCheck.CriaFm(TFmEmitMopp, FmEmitMopp, afmoSoBoss) then
  begin
    FmEmitMopp.ImgTipo.SQLType := SQLType;
    //
    FmEmitMopp.EdCodigo.ValueVariant := QrPesqCodigo.Value;
    FmEmitMopp.EdNome.ValueVariant := QrPesqNOME.Value;
    //
    if SQLType = stUpd then
    begin
      FmEmitMopp.EdControle.ValueVariant   := QrPesqCodigo.Value;
      FmEmitMopp.RGOperPosStatus.ItemIndex := QrEmitMoppStatus.Value;
      FmEmitMopp.TPIni.Date                := QrEmitMoppDthIni.Value;
      FmEmitMopp.EdIni.ValueVariant        := Geral.FDT(QrEmitMoppDthIni.Value, 100);
      FmEmitMopp.TPFim.Date                := QrEmitMoppDthFim.Value;
      FmEmitMopp.EdFim.ValueVariant        := Geral.FDT(QrEmitMoppDthFim.Value, 100);
    end;
    //
    FmEmitMopp.ShowModal;
    Controle := FmEmitMopp.EdControle.ValueVariant;
    FmEmitMopp.Destroy;
    AtualizaTotaisMopp(Codigo, Controle);
  end;
end;

procedure TFmMapaOperPosProc.MultiRedefinicaoDeStatus(NovoStatus: Integer);
  procedure IndefineAtual();
  var
    Codigo: Integer;
    SQLType: TSQLType;
  begin
    SQLType        := stUpd;
    Codigo         := QrPesqCodigo.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'emit', False, [
    'OperPosStatus'], [
    'Codigo'], [
    NovoStatus], [
    Codigo], True) then ;
  end;
  //
var
  I, N, Empresa: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    if dgMultiSelect in DBGPesq.Options then
    begin
      //DBGPesq.Options := DBGPesq.Options + [dgRowSelect, dgMultiSelect];
      if DBGPesq.SelectedRows.Count > 0 then
      begin
        N := 0;
        with DBGPesq.DataSource.DataSet do
        for I := 0 to DBGPesq.SelectedRows.Count-1 do
        begin
          GotoBookmark(DBGPesq.SelectedRows.Items[I]);
          N := N + 1;
          IndefineAtual();
        end;
        ReabrePesquisa();
        //
        Geral.MB_Info(Geral.FF0(N) + ' itens foram alterados!');
      end else Geral.MB_Erro('Nenhum item de pesagem foi selecionado!');
    end else Geral.MB_Erro('Modo de multi sele��o n�o ativo!');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMapaOperPosProc.PMEmitMoppPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluinovaoperaopsprocesso1, QrPesq);
  MyObjects.HabilitaMenuItemItsUpd(Alteraoperaopsprocessoselecionada1, QrEmitMopp);
  MyObjects.HabilitaMenuItemItsDel(Excluioperaopsprocessoselecionada1, QrEmitMopp);
  //
  Redefinirselecionados1.Enabled := dgMultiSelect in DBGPesq.Options;
end;

procedure TFmMapaOperPosProc.QrPesqAfterOpen(DataSet: TDataSet);
begin
  LaQtde.Caption := FormatFloat('0', QrPesq.RecordCount);
  BtLocaliza.Enabled := QrPesq.RecordCount > 0;
  BtImprime.Enabled := QrPesq.RecordCount > 0;
end;

procedure TFmMapaOperPosProc.QrPesqAfterScroll(DataSet: TDataSet);
begin
  ReopenEmitMopp(0);
  Memo1.Text := QrPesqObs.Value;
end;

procedure TFmMapaOperPosProc.QrPesqBeforeClose(DataSet: TDataSet);
begin
  LaQtde.Caption := '0';
  BtLocaliza.Enabled := False;
  QrEmitMopp.Close;
end;

procedure TFmMapaOperPosProc.ReabrePesquisa();
var
  Setor: Integer;
  SQL_Setor: String;
  ATT_MapaOperPosProc: String;
begin
  if FNaoReabrir then
    Exit;
  if CGOperPosStatus.Value > 0 then
  begin
    ATT_MapaOperPosProc := dmkPF.ArrayToTexto('emi.OperPosStatus', 'NO_OperPosStatus', pvPos,
      True, sMapaOperPosProcStat);
    //
    Setor := EdSetor.ValueVariant;
    SQL_Setor := '';
    //
    if Setor <> 0 then
      SQL_Setor := 'AND emi.Setor = ' + Geral.FF0(Setor);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
    'SELECT ',
    ATT_MapaOperPosProc,
    'DATE_ADD(DATE_ADD(emi.DtaBaixa, INTERVAL emi.HoraIni HOUR_SECOND), INTERVAL (TempoR + TempoP) MINUTE) DtHrFinal, ',
    'emi.* ',
    'FROM emit emi ',
    //'WHERE emi.OperPosStatus=' + Geral.FF0(RGOperPosStatus.ItemIndex),
    'WHERE emi.OperPosStatus IN (' +
      CGOperPosStatus.GetIndexesChecked((*VerificaZero*)True, (*Incremento*)0) +
      ')',
    //
    SQL_Setor,
    //
    'ORDER BY emi.DataEmis DESC, emi.Codigo DESC ',
    '']);
    //Geral.MB_Teste(QrPesq.SQL.Text);
  end else
    QrPesq.Close;
end;

procedure TFmMapaOperPosProc.Redefinirselecionados1Click(Sender: TObject);
var
  Status: Integer;
begin
  Status := MyObjects.SelRadioGroup('Multi Altera��o de Status',
  'Selecione o novo status', sMapaOperPosProcStat, 1);
  if Status = -1 then
    Exit;
  //
  MultiRedefinicaoDeStatus(Status);
end;

procedure TFmMapaOperPosProc.ReopenEmitMopp(Controle: Integer);
var
  ATT_MapaOperPosProc: String;
begin
  ATT_MapaOperPosProc := dmkPF.ArrayToTexto('mop.Status', 'NO_Status', pvPos,
    True, sMapaOperPosProcStat);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmitMopp, Dmod.MyDB, [
  'SELECT ',
  ATT_MapaOperPosProc,
  'mop.* ',
  'FROM emitmopp mop ',
  'WHERE mop.Codigo=' + Geral.FF0(QrPesqCodigo.Value),
  'ORDER BY Controle ',
  ' ']);
  if Controle <> 0 then
    QrEmitMopp.Locate('Controle', Controle, []);
end;

procedure TFmMapaOperPosProc.RGOperPosStatusClick(Sender: TObject);
begin
  ReabrePesquisa();
end;

function TFmMapaOperPosProc.ImpedeInsAltDel(): Boolean;
begin
  Result := True;
  //
  if (QrPesq.State = dsInactive) or (QrPesq.RecordCount = 0) then
    Geral.MB_Aviso('Nenhum item foi selecionado!')
  else
  if DBGPesq.SelectedRows.Count > 1 then
    Geral.MB_Aviso(
    'Esta a��o s� pode ser executada quando apenas um item est� selecionado!' +
    'Selecione apenas um item e tente novamente!')
  else
    Result := False;
end;

end.

