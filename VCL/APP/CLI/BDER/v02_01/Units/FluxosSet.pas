unit FluxosSet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmFluxosSet = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label2: TLabel;
    EdSetor: TdmkEditCB;
    CBSetor: TdmkDBLookupComboBox;
    Label8: TLabel;
    EdOrdem: TdmkEdit;
    SpeedButton1: TSpeedButton;
    QrSetores: TmySQLQuery;
    DsSetores: TDataSource;
    EdControle: TdmkEdit;
    Label1: TLabel;
    QrSetoresCodigo: TIntegerField;
    QrSetoresNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFluxosSet: TFmFluxosSet;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
FluxosCab, Principal;

{$R *.DFM}

procedure TFmFluxosSet.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Ordem, Setor: Integer;
begin
  Codigo         := FQrCab.FieldByName('Codigo').AsInteger;
  Controle       := EdControle.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  Setor          := EdSetor.ValueVariant;
  //
  Controle := UMyMod.BPGS1I32('fluxosset', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'fluxosset', False, [
  'Codigo', 'Ordem', 'Setor'], [
  'Controle'], [
  Codigo, Ordem, Setor], [
  Controle], True) then
  begin
    FmFluxosCab.Reordena(False, 0);
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdSetor.ValueVariant     := 0;
      CBSetor.KeyValue         := Null;
      //
      EdSetor.SetFocus;
      //
    end else Close;
  end;
end;

procedure TFmFluxosSet.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFluxosSet.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFluxosSet.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrSetores, Dmod.MyDB);
end;

procedure TFmFluxosSet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFluxosSet.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmFluxosSet.SpeedButton1Click(Sender: TObject);
var
  Setor: Integer;
begin
  VAR_CADASTRO := 0;
  Setor        := EdSetor.ValueVariant;
  //
  FmPrincipal.CadastroDeSetores(Setor);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdSetor, CBSetor, QrSetores, VAR_CADASTRO);
    EdSetor.SetFocus;
  end;
end;

end.
