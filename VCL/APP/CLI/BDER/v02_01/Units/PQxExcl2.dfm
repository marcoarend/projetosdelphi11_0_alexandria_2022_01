object FmPQxExcl2: TFmPQxExcl2
  Left = 234
  Top = 172
  Caption = 'QUI-RECEI-008 :: Gerenciamento de Pesagem'
  ClientHeight = 780
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 101
    Width = 1016
    Height = 45
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label3: TLabel
      Left = 4
      Top = 4
      Width = 54
      Height = 13
      Caption = 'Pesagem *:'
    end
    object Label5: TLabel
      Left = 72
      Top = 4
      Width = 42
      Height = 13
      Caption = 'Emiss'#227'o:'
      FocusControl = DBEdit1
    end
    object Label6: TLabel
      Left = 388
      Top = 4
      Width = 40
      Height = 13
      Caption = 'Receita:'
      FocusControl = DBEdit2
    end
    object Label7: TLabel
      Left = 720
      Top = 4
      Width = 35
      Height = 13
      Caption = 'Cliente:'
      FocusControl = DBEdit4
    end
    object Label8: TLabel
      Left = 208
      Top = 4
      Width = 27
      Height = 13
      Caption = 'Peso:'
      FocusControl = DBEdit5
    end
    object Label9: TLabel
      Left = 276
      Top = 4
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
      FocusControl = DBEdit6
    end
    object Label15: TLabel
      Left = 344
      Top = 4
      Width = 29
      Height = 13
      Caption = 'Ful'#227'o:'
      FocusControl = DBEdit10
    end
    object EdPesagem: TdmkEdit
      Left = 4
      Top = 20
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = EdPesagemExit
      OnKeyDown = EdPesagemKeyDown
    end
    object DBEdit1: TDBEdit
      Left = 72
      Top = 20
      Width = 133
      Height = 21
      DataField = 'DataEmis'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 1
    end
    object DBEdit2: TDBEdit
      Left = 388
      Top = 20
      Width = 40
      Height = 21
      DataField = 'NumCODIF'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 2
    end
    object DBEdit3: TDBEdit
      Left = 428
      Top = 20
      Width = 245
      Height = 21
      DataField = 'NOME'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 3
    end
    object DBEdit4: TDBEdit
      Left = 720
      Top = 20
      Width = 289
      Height = 21
      DataField = 'NOMECI'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 4
    end
    object DBEdit5: TDBEdit
      Left = 208
      Top = 20
      Width = 64
      Height = 21
      DataField = 'Peso'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 5
    end
    object DBEdit6: TDBEdit
      Left = 276
      Top = 20
      Width = 64
      Height = 21
      DataField = 'Qtde'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 6
    end
    object DBEdit10: TDBEdit
      Left = 344
      Top = 20
      Width = 41
      Height = 21
      DataField = 'Fulao'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 7
    end
    object DBEdit8: TDBEdit
      Left = 676
      Top = 20
      Width = 40
      Height = 21
      DataField = 'Numero'
      DataSource = DsEmit
      Enabled = False
      TabOrder = 8
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 146
    Width = 1016
    Height = 506
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 0
      Top = 0
      Width = 740
      Height = 258
      Align = alLeft
      DataSource = DsSMIA
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPQ'
          Title.Caption = 'Descri'#231#227'o'
          Width = 168
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cedente do insumo'
          Width = 247
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Title.Caption = 'Quantidade'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoAll'
          Title.Caption = 'Custo total'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CUSTOMEDIO'
          Title.Caption = 'Custo unit.'
          Visible = True
        end>
    end
    object DBGrid2: TDBGrid
      Left = 900
      Top = 0
      Width = 116
      Height = 258
      Align = alRight
      DataSource = DsEmitCus
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'MPIn'
          Title.Caption = 'ID Lote'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Width = 79
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Custo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataMPIn'
          Title.Caption = 'Data MP'
          Visible = True
        end>
    end
    object PnInsumos: TPanel
      Left = 0
      Top = 258
      Width = 1016
      Height = 152
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      Visible = False
      object Panel6: TPanel
        Left = 0
        Top = 63
        Width = 1016
        Height = 89
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 4
          Top = 4
          Width = 40
          Height = 13
          Caption = 'Produto:'
        end
        object Label2: TLabel
          Left = 396
          Top = 4
          Width = 70
          Height = 13
          Caption = 'Cliente interno:'
        end
        object Label19: TLabel
          Left = 4
          Top = 44
          Width = 56
          Height = 13
          Caption = 'Custo $/kg:'
        end
        object Label13: TLabel
          Left = 108
          Top = 44
          Width = 42
          Height = 13
          Caption = 'Peso kg:'
        end
        object Label17: TLabel
          Left = 212
          Top = 44
          Width = 57
          Height = 13
          Caption = 'Estoque kg:'
          FocusControl = DBEdit7
        end
        object Label18: TLabel
          Left = 316
          Top = 44
          Width = 51
          Height = 13
          Caption = 'Estoque $:'
        end
        object Label20: TLabel
          Left = 420
          Top = 44
          Width = 75
          Height = 13
          Caption = 'Saldo futuro kg:'
        end
        object EdGraGruX: TdmkEditCB
          Left = 4
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdGraGruXChange
          DBLookupComboBox = CBGraGruX
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGraGruX: TdmkDBLookupComboBox
          Left = 60
          Top = 20
          Width = 332
          Height = 21
          KeyField = 'Controle'
          ListField = 'Nome'
          ListSource = DsGraGruX
          TabOrder = 1
          dmkEditCB = EdGraGruX
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdEntidade: TdmkEditCB
          Left = 396
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEntidadeChange
          DBLookupComboBox = CBEntidade
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEntidade: TdmkDBLookupComboBox
          Left = 452
          Top = 20
          Width = 233
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMECI'
          ListSource = DsCI
          TabOrder = 3
          dmkEditCB = EdGraGruX
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdPrecokg: TdmkEdit
          Left = 4
          Top = 60
          Width = 100
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdPesoAddExit
        end
        object EdPesoAdd: TdmkEdit
          Left = 108
          Top = 60
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdPesoAddExit
        end
        object DBEdit7: TDBEdit
          Left = 212
          Top = 60
          Width = 100
          Height = 21
          TabStop = False
          DataField = 'QTDE'
          DataSource = DsSaldo
          TabOrder = 6
        end
        object EdValorEstq: TdmkEdit
          Left = 316
          Top = 60
          Width = 100
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          ReadOnly = True
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdSaldoFut: TdmkEdit
          Left = 420
          Top = 60
          Width = 100
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          ReadOnly = True
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object CkDtCorrApo: TCheckBox
          Left = 688
          Top = 24
          Width = 185
          Height = 17
          Caption = #201' corre'#231#227'o de apontamento. Data:'
          TabOrder = 9
          OnClick = CkDtCorrApoClick
        end
        object TPDtCorrApo: TdmkEditDateTimePicker
          Left = 876
          Top = 20
          Width = 129
          Height = 21
          Date = 44769.000000000000000000
          Time = 0.833253726850671200
          Enabled = False
          TabOrder = 10
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpSPED_EFD_MIN_MAX
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 1016
        Height = 63
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 1016
          Height = 58
          Align = alTop
          Caption = ' Pre'#231'os: '
          TabOrder = 0
          object RGTipoPreco: TRadioGroup
            Left = 2
            Top = 15
            Width = 363
            Height = 41
            Align = alLeft
            Caption = ' Origem pre'#231'os: '
            Columns = 3
            Enabled = False
            ItemIndex = 0
            Items.Strings = (
              'Estoque'
              'Cadastro'
              'A definir')
            TabOrder = 0
          end
          object GBGraCusPrc: TGroupBox
            Left = 365
            Top = 15
            Width = 649
            Height = 41
            Align = alClient
            Caption = ' Pre'#231'os de origem de cadastro: '
            Enabled = False
            TabOrder = 1
            object Label16: TLabel
              Left = 8
              Top = 19
              Width = 76
              Height = 13
              Caption = 'Lista de Pre'#231'os:'
            end
            object EdGraCusPrc: TdmkEditCB
              Left = 88
              Top = 15
              Width = 53
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGraCusPrc
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBGraCusPrc: TdmkDBLookupComboBox
              Left = 144
              Top = 15
              Width = 261
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DsGraCusPrc
              TabOrder = 1
              TabStop = False
              dmkEditCB = EdGraCusPrc
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
        end
      end
    end
    object PnImprime: TPanel
      Left = 0
      Top = 410
      Width = 1016
      Height = 96
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      Visible = False
      object RGImpRecRib: TRadioGroup
        Left = 6
        Top = 4
        Width = 700
        Height = 65
        Caption = ' Apresenta'#231#227'o: '
        Columns = 2
        ItemIndex = 1
        Items.Strings = (
          'Procure em:'
          ''
          'FmPrincipal.VAR_RecImpApresentaCol'
          'FmPrincipal.VAR_RecImpApresentaRol')
        TabOrder = 0
      end
      object RGImprime: TRadioGroup
        Left = 712
        Top = 4
        Width = 160
        Height = 65
        Caption = ' Op'#231#245'es: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Visualizar'
          'Imprimir'
          'Matricial'
          'Arquivo')
        TabOrder = 1
      end
      object GBkgTon: TGroupBox
        Left = 870
        Top = 4
        Width = 95
        Height = 65
        Caption = ' Grandeza: '
        TabOrder = 2
        object RBTon: TRadioButton
          Left = 8
          Top = 16
          Width = 50
          Height = 17
          Caption = 'Ton'
          Checked = True
          TabOrder = 0
          TabStop = True
        end
        object RBkg: TRadioButton
          Left = 8
          Top = 36
          Width = 50
          Height = 17
          Caption = 'kg'
          TabOrder = 1
        end
      end
      object CkMatricial: TCheckBox
        Left = 8
        Top = 72
        Width = 117
        Height = 17
        Caption = 'Novo (Matricial)'
        Checked = True
        State = cbChecked
        TabOrder = 3
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 968
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 920
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 344
        Height = 32
        Caption = 'Gerenciamento de Pesagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 344
        Height = 32
        Caption = 'Gerenciamento de Pesagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 344
        Height = 32
        Caption = 'Gerenciamento de Pesagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1016
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1012
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 19
        Width = 1012
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBControle: TGroupBox
    Left = 0
    Top = 652
    Width = 1016
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 1012
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label14: TLabel
        Left = 144
        Top = 16
        Width = 91
        Height = 13
        Caption = '* [F4] Busca '#250'ltima.'
      end
      object PnSaiDesis: TPanel
        Left = 868
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtCancela: TBitBtn
          Tag = 13
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Sair'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtCancelaClick
        end
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtPesquisa: TBitBtn
        Tag = 40
        Left = 252
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtPesquisaClick
      end
      object BitBtn1: TBitBtn
        Left = 372
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Recalcula'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BitBtn1Click
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 492
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Adiciona'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtIncluiClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 612
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtExcluiClick
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 716
    Width = 1016
    Height = 64
    Align = alBottom
    TabOrder = 5
    Visible = False
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 1012
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel9: TPanel
        Left = 868
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste2: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesiste2Click
        end
      end
      object BtConfirma2: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirma2Click
      end
    end
  end
  object QrEmit: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEmitAfterOpen
    BeforeScroll = QrEmitBeforeScroll
    OnCalcFields = QrEmitCalcFields
    SQL.Strings = (
      'SELECT lis.TpReceita, emi.*, gru.Nome NO_EmitGru  '
      'FROM emit emi '
      'LEFT JOIN emitgru gru ON gru.Codigo=emi.EmitGru '
      'LEFT JOIN listasetores lis ON lis.Codigo=emi.Setor')
    Left = 252
    Top = 264
    object QrEmitNumCODIF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NumCODIF'
      Size = 11
      Calculated = True
    end
    object QrEmitCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitDataEmis: TDateTimeField
      FieldName = 'DataEmis'
    end
    object QrEmitStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrEmitNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrEmitNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrEmitNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrEmitTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrEmitNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrEmitClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrEmitTipificacao: TSmallintField
      FieldName = 'Tipificacao'
    end
    object QrEmitTempoR: TSmallintField
      FieldName = 'TempoR'
    end
    object QrEmitTempoP: TSmallintField
      FieldName = 'TempoP'
    end
    object QrEmitSetor: TSmallintField
      FieldName = 'Setor'
    end
    object QrEmitEspessura: TWideStringField
      FieldName = 'Espessura'
      Size = 15
    end
    object QrEmitPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEmitQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmitLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmitDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmitDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmitUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmitAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEmitFulao: TWideStringField
      FieldName = 'Fulao'
      Size = 5
    end
    object QrEmitDefPeca: TWideStringField
      FieldName = 'DefPeca'
      Size = 15
    end
    object QrEmitCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrEmitObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
    object QrEmitAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEmitTipific: TSmallintField
      FieldName = 'Tipific'
    end
    object QrEmitAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitSetrEmi: TSmallintField
      FieldName = 'SetrEmi'
    end
    object QrEmitCod_Espess: TIntegerField
      FieldName = 'Cod_Espess'
    end
    object QrEmitCodDefPeca: TIntegerField
      FieldName = 'CodDefPeca'
    end
    object QrEmitSourcMP: TSmallintField
      FieldName = 'SourcMP'
    end
    object QrEmitCustoTo: TFloatField
      FieldName = 'CustoTo'
    end
    object QrEmitCustoKg: TFloatField
      FieldName = 'CustoKg'
    end
    object QrEmitCustoM2: TFloatField
      FieldName = 'CustoM2'
    end
    object QrEmitCusUSM2: TFloatField
      FieldName = 'CusUSM2'
    end
    object QrEmitEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrEmitRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
    end
    object QrEmitNO_EmitGru: TWideStringField
      FieldName = 'NO_EmitGru'
      Size = 60
    end
    object QrEmitSemiAreaM2: TFloatField
      FieldName = 'SemiAreaM2'
    end
    object QrEmitSemiRendim: TFloatField
      FieldName = 'SemiRendim'
    end
    object QrEmitBRL_USD: TFloatField
      FieldName = 'BRL_USD'
    end
    object QrEmitBRL_EUR: TFloatField
      FieldName = 'BRL_EUR'
    end
    object QrEmitDtaCambio: TDateField
      FieldName = 'DtaCambio'
    end
    object QrEmitSemiTxtEspReb: TWideStringField
      FieldName = 'SemiTxtEspReb'
      Size = 15
    end
    object QrEmitTpReceita: TSmallintField
      FieldName = 'TpReceita'
    end
    object QrEmitSemiCodPeca: TIntegerField
      FieldName = 'SemiCodPeca'
      Required = True
    end
    object QrEmitSemiTxtPeca: TWideStringField
      FieldName = 'SemiTxtPeca'
      Size = 15
    end
    object QrEmitSemiPeso: TFloatField
      FieldName = 'SemiPeso'
      Required = True
    end
    object QrEmitSemiQtde: TFloatField
      FieldName = 'SemiQtde'
      Required = True
    end
    object QrEmitSemiCodEspe: TIntegerField
      FieldName = 'SemiCodEspe'
      Required = True
    end
    object QrEmitSemiTxtEspe: TWideStringField
      FieldName = 'SemiTxtEspe'
      Size = 15
    end
    object QrEmitVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
      Required = True
    end
    object QrEmitHoraIni: TTimeField
      FieldName = 'HoraIni'
      Required = True
    end
    object QrEmitGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Required = True
    end
    object QrEmitNoGraCorCad: TWideStringField
      FieldName = 'NoGraCorCad'
      Size = 30
    end
    object QrEmitSemiCodEspReb: TIntegerField
      FieldName = 'SemiCodEspReb'
    end
    object QrEmitDtaBaixa: TDateField
      FieldName = 'DtaBaixa'
      Required = True
    end
    object QrEmitDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
      Required = True
    end
    object QrEmitEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEmitAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrEmitAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrEmitSbtCouIntros: TFloatField
      FieldName = 'SbtCouIntros'
      Required = True
    end
    object QrEmitSbtAreaM2: TFloatField
      FieldName = 'SbtAreaM2'
      Required = True
    end
    object QrEmitSbtAreaP2: TFloatField
      FieldName = 'SbtAreaP2'
      Required = True
    end
    object QrEmitSbtRendEsper: TFloatField
      FieldName = 'SbtRendEsper'
      Required = True
    end
    object QrEmitOperPosStatus: TSmallintField
      FieldName = 'OperPosStatus'
      Required = True
    end
    object QrEmitOperPosGrandz: TIntegerField
      FieldName = 'OperPosGrandz'
      Required = True
    end
    object QrEmitOperPosQtdDon: TFloatField
      FieldName = 'OperPosQtdDon'
      Required = True
    end
    object QrEmitOperPosDthIni: TDateTimeField
      FieldName = 'OperPosDthIni'
      Required = True
    end
    object QrEmitOperPosDthFim: TDateTimeField
      FieldName = 'OperPosDthFim'
      Required = True
    end
    object QrEmitVersao: TIntegerField
      FieldName = 'Versao'
      Required = True
    end
  end
  object DsEmit: TDataSource
    DataSet = QrEmit
    Left = 252
    Top = 312
  end
  object QrMPIn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MPIn '
      'FROM emitcus'
      'WHERE Codigo=:P0')
    Left = 149
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPInMPIn: TIntegerField
      FieldName = 'MPIn'
    end
  end
  object QrSMIA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial '
      'ELSE cli.Nome END NOMECLI, pq_.Nome NOMEPQ, pqx.*,'
      '(pqx.Valor/pqx.Peso) CUSTOMEDIO'
      'FROM pqx pqx'
      'LEFT JOIN pq        pq_ ON pq_.Codigo=pqx.Insumo'
      'LEFT JOIN entidades cli ON cli.Codigo=pqx.CliOrig'
      'WHERE pqx.OrigemCodi=:P0'
      'AND pqx.Tipo=110'
      '*/'
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI, '
      'gg1.Nome NOMEPQ, smia.*,'
      '(smia.CustoAll/smia.Qtde) CUSTOMEDIO'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN entidades cli ON cli.Codigo=smia.Empresa'
      'WHERE smia.Tipo IN (:P0,:P1)'
      'AND smia.OriCodi=:P2')
    Left = 177
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSMIACUSTOMEDIO: TFloatField
      FieldName = 'CUSTOMEDIO'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrSMIANOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrSMIANOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 120
    end
    object QrSMIADataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrSMIAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrSMIATipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSMIAOriCodi: TIntegerField
      FieldName = 'OriCodi'
    end
    object QrSMIAOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrSMIAOriCnta: TIntegerField
      FieldName = 'OriCnta'
    end
    object QrSMIAOriPart: TIntegerField
      FieldName = 'OriPart'
    end
    object QrSMIAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSMIAStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrSMIAGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSMIAQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrSMIAPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSMIAPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSMIAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSMIAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSMIAAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSMIAAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSMIAFatorClas: TFloatField
      FieldName = 'FatorClas'
    end
    object QrSMIAQuemUsou: TIntegerField
      FieldName = 'QuemUsou'
    end
    object QrSMIARetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrSMIAParTipo: TIntegerField
      FieldName = 'ParTipo'
    end
    object QrSMIAParCodi: TIntegerField
      FieldName = 'ParCodi'
    end
    object QrSMIADebCtrl: TIntegerField
      FieldName = 'DebCtrl'
    end
    object QrSMIASMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
    end
    object QrSMIACustoAll: TFloatField
      FieldName = 'CustoAll'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSMIAValorAll: TFloatField
      FieldName = 'ValorAll'
    end
    object QrSMIAGrupoBal: TIntegerField
      FieldName = 'GrupoBal'
    end
    object QrSMIABaixa: TSmallintField
      FieldName = 'Baixa'
    end
    object QrSMIAAntQtde: TFloatField
      FieldName = 'AntQtde'
    end
  end
  object DsSMIA: TDataSource
    DataSet = QrSMIA
    Left = 204
    Top = 164
  end
  object PMExclui: TPopupMenu
    Left = 556
    Top = 472
    object Itemdepesagem1: TMenuItem
      Caption = '&Item de pesagem'
      OnClick = Itemdepesagem1Click
    end
    object Todapesagem1: TMenuItem
      Caption = '&Toda pesagem'
      OnClick = Todapesagem1Click
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 676
    Top = 4
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 648
    Top = 4
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object QrSumEmit: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEmitAfterOpen
    SQL.Strings = (
      'SELECT SUM(Custo) CUSTO '
      'FROM emit'
      'WHERE Codigo=:P0'
      '')
    Left = 253
    Top = 163
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumEmitCUSTO: TFloatField
      FieldName = 'CUSTO'
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Codigo) UltimaPesagem'
      'FROM emit')
    Left = 352
    Top = 164
    object QrPesqUltimaPesagem: TIntegerField
      FieldName = 'UltimaPesagem'
      Required = True
    end
  end
  object QrEmitCus: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mpi.Marca, mpi.Data DataMPIn, '
      'emc.Pecas, emc.Peso, emc.Custo, emc.MPIn'
      'FROM emitcus emc'
      'LEFT JOIN mpin mpi ON mpi.Controle=emc.MPIn'
      'WHERE emc.Codigo=:P0')
    Left = 316
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmitCusMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrEmitCusDataMPIn: TDateField
      FieldName = 'DataMPIn'
    end
    object QrEmitCusPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrEmitCusPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrEmitCusCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrEmitCusMPIn: TIntegerField
      FieldName = 'MPIn'
      Required = True
    end
  end
  object DsEmitCus: TDataSource
    DataSet = QrEmitCus
    Left = 316
    Top = 312
  end
  object QrGraGruVal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggv.CustoPreco'
      'FROM gragruval ggv'
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggv.GraGruX'
      'WHERE ggv.GraGruX=:P0'
      'AND ggv.GraCusPrc=:P1'
      'AND ggv.Entidade=:P2'
      '')
    Left = 704
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGraGruValCustoPreco: TFloatField
      FieldName = 'CustoPreco'
    end
  end
  object DsGraGruVal: TDataSource
    DataSet = QrGraGruVal
    Left = 732
    Top = 192
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 732
    Top = 220
  end
  object QrGraCusPrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM gracusprc'
      'ORDER BY Nome')
    Left = 704
    Top = 220
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object QrSaldo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smia.Qtde*smia.Baixa) QTDE'
      'FROM stqmovitsa smia'
      'WHERE smia.Baixa=1'
      'AND smia.GraGrux=:P0'
      'AND smia.Empresa=:P1')
    Left = 704
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSaldoQTDE: TFloatField
      FieldName = 'QTDE'
    end
  end
  object DsSaldo: TDataSource
    DataSet = QrSaldo
    Left = 732
    Top = 164
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, gg1.Nome'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE gg1.PrdGrupTip=-2'
      'AND gg1.Nivel2 NOT IN (-3,-6)'
      'ORDER BY gg1.Nome'
      '')
    Left = 648
    Top = 32
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 676
    Top = 32
  end
end
