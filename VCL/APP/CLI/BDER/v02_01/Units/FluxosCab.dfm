object FmFluxosCab: TFmFluxosCab
  Left = 368
  Top = 194
  ActiveControl = BtSaida
  Caption = 'FLU-PRODU-001 :: Cadastro de Fluxos de Produ'#231#227'o'
  ClientHeight = 571
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 475
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitHeight = 395
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 412
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 332
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 898
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 475
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitHeight = 395
    object Splitter1: TSplitter
      Left = 0
      Top = 255
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 208
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsFluxos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 689
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsFluxos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 411
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 331
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 194
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
        ExplicitWidth = 311
      end
      object Panel3: TPanel
        Left = 368
        Top = 15
        Width = 638
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 505
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitLeft = 388
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 10121
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fluxo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 10122
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Opera'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtSetor: TBitBtn
          Tag = 10123
          Left = 376
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Setor'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtSetorClick
        end
        object BtCtr: TBitBtn
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Controle'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtCtrClick
        end
      end
    end
    object DBGFluxosIct: TDBGrid
      Left = 0
      Top = 260
      Width = 1008
      Height = 151
      Align = alBottom
      DataSource = DsFluxosICt
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'SEQ'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Acao1'
          Title.Caption = 'Controle'
          Width = 540
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ordem'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta'
          Visible = True
        end>
    end
    object DBGrid2: TDBGrid
      Left = 812
      Top = 137
      Width = 196
      Height = 118
      Align = alRight
      DataSource = DsFluxosSet
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Setor'
          Width = 33
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SETOR'
          Title.Caption = 'Descri'#231#227'o'
          Visible = True
        end>
    end
    object DBGFluxosIts: TDBGrid
      Left = 0
      Top = 65
      Width = 1008
      Height = 72
      Align = alTop
      DataSource = DsFluxosIts
      TabOrder = 4
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'SEQ'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEOPERACAO'
          Title.Caption = 'Opera'#231#227'o'
          Width = 161
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Acao1'
          Title.Caption = 'Controle 1'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Acao2'
          Title.Caption = 'Controle 2'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Acao3'
          Title.Caption = 'Controle 3'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Acao4'
          Title.Caption = 'Controle 4'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ordem'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 397
        Height = 32
        Caption = 'Cadastro de Fluxos de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 397
        Height = 32
        Caption = 'Cadastro de Fluxos de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 397
        Height = 32
        Caption = 'Cadastro de Fluxos de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 532
    Top = 400
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Gerenciaritens1: TMenuItem
      Caption = '&Gerenciar itens'
      OnClick = Gerenciaritens1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Duplicaoperaoatual1: TMenuItem
      Caption = '&Duplica opera'#231#227'o atual'
      OnClick = Duplicaoperaoatual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object ReordenaOperacoes1: TMenuItem
      Caption = '&Reordena opera'#231#245'es'
      OnClick = ReordenaOperacoes1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 364
    Top = 400
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Duplicafluxoatual1: TMenuItem
      Caption = '&Duplica fluxo atual'
      OnClick = Duplicafluxoatual1Click
    end
  end
  object QrFluxos: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFluxosBeforeOpen
    AfterOpen = QrFluxosAfterOpen
    BeforeClose = QrFluxosBeforeClose
    AfterScroll = QrFluxosAfterScroll
    SQL.Strings = (
      'SELECT * FROM fluxos'
      'WHERE Codigo > 0')
    Left = 148
    Top = 64
    object QrFluxosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFluxosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFluxosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFluxosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFluxosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFluxosCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object QrFluxosNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsFluxos: TDataSource
    DataSet = QrFluxos
    Left = 176
    Top = 64
  end
  object QrFluxosIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFluxosItsBeforeClose
    AfterScroll = QrFluxosItsAfterScroll
    OnCalcFields = QrFluxosItsCalcFields
    SQL.Strings = (
      'SELECT ope.Nome NOMEOPERACAO, fli.*'
      'FROM fluxosits fli'
      'LEFT JOIN operacoes ope ON ope.Codigo=fli.Operacao'
      'WHERE fli.Codigo=:P0'
      'ORDER BY fli.Ordem, fli.Controle DESC')
    Left = 204
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFluxosItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFluxosItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFluxosItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFluxosItsOperacao: TIntegerField
      FieldName = 'Operacao'
    end
    object QrFluxosItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFluxosItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFluxosItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFluxosItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFluxosItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFluxosItsNOMEOPERACAO: TWideStringField
      FieldName = 'NOMEOPERACAO'
      Size = 30
    end
    object QrFluxosItsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrFluxosItsAcao1: TWideStringField
      FieldName = 'Acao1'
      Size = 100
    end
    object QrFluxosItsAcao2: TWideStringField
      FieldName = 'Acao2'
      Size = 100
    end
    object QrFluxosItsAcao3: TWideStringField
      FieldName = 'Acao3'
      Size = 100
    end
    object QrFluxosItsAcao4: TWideStringField
      FieldName = 'Acao4'
      Size = 100
    end
  end
  object DsFluxosIts: TDataSource
    DataSet = QrFluxosIts
    Left = 232
    Top = 64
  end
  object QrOrdena: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ope.Nome NOMEOPERACAO, fli.*'
      'FROM fluxosits fli'
      'LEFT JOIN operacoes ope ON ope.Codigo=fli.Operacao'
      'WHERE fli.Codigo=:P0'
      'ORDER BY fli.Ordem, fli.Controle DESC')
    Left = 132
    Top = 253
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOrdenaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOrdenaControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOrdenaOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrOrdenaOperacao: TIntegerField
      FieldName = 'Operacao'
      Required = True
    end
    object QrOrdenaAcao1: TWideStringField
      FieldName = 'Acao1'
      Required = True
      Size = 30
    end
    object QrOrdenaAcao2: TWideStringField
      FieldName = 'Acao2'
      Required = True
      Size = 30
    end
    object QrOrdenaAcao3: TWideStringField
      FieldName = 'Acao3'
      Required = True
      Size = 30
    end
    object QrOrdenaAcao4: TWideStringField
      FieldName = 'Acao4'
      Required = True
      Size = 30
    end
    object QrOrdenaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOrdenaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOrdenaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOrdenaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOrdenaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOrdenaNOMEOPERACAO: TWideStringField
      FieldName = 'NOMEOPERACAO'
      Size = 30
    end
    object QrOrdenaSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object QrFluxosSet: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFluxosItsCalcFields
    SQL.Strings = (
      'SELECT lse.Nome NO_SETOR, fls.* '
      'FROM fluxosset fls '
      'LEFT JOIN listasetores lse ON lse.Codigo=fls.Setor '
      'WHERE fls.Codigo=:P0 '
      'ORDER BY fls.Ordem, fls.Controle DESC ')
    Left = 260
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFluxosSetNO_SETOR: TWideStringField
      FieldName = 'NO_SETOR'
      Origin = 'listasetores.Nome'
    end
    object QrFluxosSetCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fluxosset.Codigo'
    end
    object QrFluxosSetControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'fluxosset.Controle'
    end
    object QrFluxosSetOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'fluxosset.Ordem'
    end
    object QrFluxosSetSetor: TIntegerField
      FieldName = 'Setor'
      Origin = 'fluxosset.Setor'
    end
  end
  object DsFluxosSet: TDataSource
    DataSet = QrFluxosSet
    Left = 288
    Top = 64
  end
  object PMSetor: TPopupMenu
    OnPopup = PMItsPopup
    Left = 748
    Top = 412
    object Adicionasetor1: TMenuItem
      Caption = '&Adiciona setor'
      OnClick = Adicionasetor1Click
    end
    object Removeosetorselecionado1: TMenuItem
      Caption = '&Remove o setor selecionado'
      OnClick = Removeosetorselecionado1Click
    end
  end
  object PMCtr: TPopupMenu
    Left = 664
    Top = 404
    object Incluinovocontrole1: TMenuItem
      Caption = '&Inclui novo controle'
      OnClick = Incluinovocontrole1Click
    end
    object Alteracontroleatual1: TMenuItem
      Caption = '&Altera controle atual'
      OnClick = Alteracontroleatual1Click
    end
    object Excluicontroleatual1: TMenuItem
      Caption = '&Exclui'
      OnClick = Excluicontroleatual1Click
    end
  end
  object QrFluxosICt: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFluxosICtCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM fluxosict flc'
      'WHERE flc.Controle=:P0'
      'ORDER BY flc.Ordem, flc.Conta DESC')
    Left = 304
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFluxosICtCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFluxosICtControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFluxosICtConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrFluxosICtAcao1: TWideStringField
      FieldName = 'Acao1'
      Size = 60
    end
    object QrFluxosICtOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrFluxosICtSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object DsFluxosICt: TDataSource
    DataSet = QrFluxosICt
    Left = 304
    Top = 236
  end
end
