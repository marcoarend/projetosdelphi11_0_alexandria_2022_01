object FmPQCorrigeFatID: TFmPQCorrigeFatID
  Left = 339
  Top = 185
  Caption = 'Corre'#231#227'o de lan'#231'amentos de PQ'
  ClientHeight = 665
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 509
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 0
      Width = 412
      Height = 316
      Align = alLeft
      DataSource = DsPQE
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NF'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorNF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Fornecedor'
          Width = 150
          Visible = True
        end>
    end
    object DBGrid2: TDBGrid
      Left = 412
      Top = 0
      Width = 372
      Height = 316
      Align = alClient
      DataSource = DsLct
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 316
      Width = 784
      Height = 193
      ActivePage = TabSheet1
      Align = alBottom
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'TabSheet1'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
      end
      object TabSheet2: TTabSheet
        Caption = 'TabSheet2'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 404
        Height = 32
        Caption = 'Corre'#231#227'o de Lan'#231'amentos de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 404
        Height = 32
        Caption = 'Corre'#231#227'o de Lan'#231'amentos de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 404
        Height = 32
        Caption = 'Corre'#231#227'o de Lan'#231'amentos de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 557
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 601
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtAbre: TBitBtn
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Abre'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAbreClick
      end
      object BtCorrige: TBitBtn
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Corrige'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtCorrigeClick
      end
    end
  end
  object QrPQE: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPQEAfterOpen
    BeforeClose = QrPQEBeforeClose
    AfterScroll = QrPQEAfterScroll
    SQL.Strings = (
      'SELECT IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'pqe.Codigo, pqe.IQ, pqe.CI, pqe.Data, pqe.NF, pqe.ValorNF,'
      'pqe.Transportadora, pqe.Conhecimento'
      'FROM pqe pqe'
      'LEFT JOIN entidades frn ON frn.Codigo=pqe.IQ'
      'ORDER BY Codigo')
    Left = 36
    Top = 100
    object QrPQENO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrPQECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQEIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQECI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQEData: TDateField
      FieldName = 'Data'
    end
    object QrPQENF: TIntegerField
      FieldName = 'NF'
    end
    object QrPQEValorNF: TFloatField
      FieldName = 'ValorNF'
    end
    object QrPQETransportadora: TIntegerField
      FieldName = 'Transportadora'
    end
    object QrPQEConhecimento: TIntegerField
      FieldName = 'Conhecimento'
    end
    object QrPQECodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
  end
  object DsPQE: TDataSource
    DataSet = QrPQE
    Left = 64
    Top = 100
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    Left = 432
    Top = 72
    object QrLctData: TDateField
      FieldName = 'Data'
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctSub: TIntegerField
      FieldName = 'Sub'
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 460
    Top = 72
  end
end
