unit MPReclasIns;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DBCtrls, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkEditCalc,
  ComCtrls, dmkEditDateTimePicker, dmkLabel, UnInternalConsts, dmkImage,
  UnDmkEnums, DmkDAC_PF;

type
  TFmMPReclasIns = class(TForm)
    Panel1: TPanel;
    QrGraGruXA: TmySQLQuery;
    QrGraGruXANivel3: TIntegerField;
    QrGraGruXANivel2: TIntegerField;
    QrGraGruXANivel1: TIntegerField;
    QrGraGruXANome: TWideStringField;
    QrGraGruXAPrdGrupTip: TIntegerField;
    QrGraGruXAGraTamCad: TIntegerField;
    QrGraGruXANOMEGRATAMCAD: TWideStringField;
    QrGraGruXACODUSUGRATAMCAD: TIntegerField;
    QrGraGruXACST_A: TSmallintField;
    QrGraGruXACST_B: TSmallintField;
    QrGraGruXAUnidMed: TIntegerField;
    QrGraGruXANCM: TWideStringField;
    QrGraGruXAPeso: TFloatField;
    QrGraGruXASIGLAUNIDMED: TWideStringField;
    QrGraGruXACODUSUUNIDMED: TIntegerField;
    QrGraGruXANOMEUNIDMED: TWideStringField;
    QrGraGruXAHowBxaEstq: TSmallintField;
    QrGraGruXAGerBxaEstq: TSmallintField;
    QrGraGruXAFatorClas: TIntegerField;
    DsGraGruXA: TDataSource;
    QrStqCenCadA: TmySQLQuery;
    QrStqCenCadACodigo: TIntegerField;
    QrStqCenCadACodUsu: TIntegerField;
    QrStqCenCadANome: TWideStringField;
    DsStqCenCadA: TDataSource;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    EdStqCenCadA: TdmkEditCB;
    CBStqCenCadA: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdGraGruXA: TdmkEditCB;
    CBGraGruXA: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    EdStqCenCadB: TdmkEditCB;
    CBStqCenCadB: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdGraGruXB: TdmkEditCB;
    CBGraGruXB: TdmkDBLookupComboBox;
    QrStqCenCadB: TmySQLQuery;
    DsStqCenCadB: TDataSource;
    DsGraGruXB: TDataSource;
    QrGraGruXB: TmySQLQuery;
    QrGraGruXBNivel3: TIntegerField;
    QrGraGruXBNivel2: TIntegerField;
    QrGraGruXBNivel1: TIntegerField;
    QrGraGruXBNome: TWideStringField;
    QrGraGruXBPrdGrupTip: TIntegerField;
    QrGraGruXBGraTamCad: TIntegerField;
    QrGraGruXBHowBxaEstq: TSmallintField;
    QrGraGruXBGerBxaEstq: TSmallintField;
    QrGraGruXBNOMEGRATAMCAD: TWideStringField;
    QrGraGruXBCODUSUGRATAMCAD: TIntegerField;
    QrGraGruXBCST_A: TSmallintField;
    QrGraGruXBCST_B: TSmallintField;
    QrGraGruXBUnidMed: TIntegerField;
    QrGraGruXBNCM: TWideStringField;
    QrGraGruXBPeso: TFloatField;
    QrGraGruXBFatorClas: TIntegerField;
    QrGraGruXBSIGLAUNIDMED: TWideStringField;
    QrGraGruXBCODUSUUNIDMED: TIntegerField;
    QrGraGruXBNOMEUNIDMED: TWideStringField;
    QrStqCenCadBCodigo: TIntegerField;
    QrStqCenCadBCodUsu: TIntegerField;
    QrStqCenCadBNome: TWideStringField;
    Panel3: TPanel;
    CBFilial: TdmkDBLookupComboBox;
    Label7: TLabel;
    EdFilial: TdmkEditCB;
    Panel4: TPanel;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    LaPeso: TLabel;
    EdPecasA: TdmkEdit;
    EdAreaM2A: TdmkEditCalc;
    EdAreaP2A: TdmkEditCalc;
    EdPesoA: TdmkEdit;
    Panel5: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdPecasB: TdmkEdit;
    EdAreaM2B: TdmkEditCalc;
    EdAreaP2B: TdmkEditCalc;
    EdPesoB: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    Label12: TLabel;
    EdHora: TdmkEdit;
    QrGraGruXAGraGruX: TIntegerField;
    QrGraGruXBGraGruX: TIntegerField;
    BitBtn1: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    CkContinuar: TCheckBox;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGruXAChange(Sender: TObject);
    procedure EdGraGruXAEnter(Sender: TObject);
    procedure EdGraGruXAExit(Sender: TObject);
    procedure EdGraGruXBChange(Sender: TObject);
    procedure EdGraGruXBEnter(Sender: TObject);
    procedure EdGraGruXBExit(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPecasAChange(Sender: TObject);
    procedure EdAreaM2AChange(Sender: TObject);
    procedure EdAreaP2AChange(Sender: TObject);
    procedure EdPesoAChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    //FGraGru1A, FGraGru1B: Integer;
    FDescosideraFocus: Boolean;
    procedure HabilitaOK();
  public
    { Public declarations }
    FSMIMultIns: Integer;
    FForcaReopen: Boolean;
    FIDCtrlA, FIDCtrlB: Integer;
  end;

  var
  FmMPReclasIns: TFmMPReclasIns;

implementation

uses UnMyObjects, ModuleGeral, UMySQLModule, Module, UnGrade_Tabs, MPReclasGer, MPReclasImp;

{$R *.DFM}

procedure TFmMPReclasIns.BitBtn1Click(Sender: TObject);
begin
  Application.CreateForm(TFmMPReclasImp, FmMPReclasImp);
  FmMPReclasImp.EdPrdGrupTip.ValueVariant := 1;
  FmMPReclasImp.CBPrdGrupTip.KeyValue     := 1;
  FmMPReclasImp.ShowModal;
  FmMPReclasImp.Destroy;
end;

procedure TFmMPReclasIns.BtOKClick(Sender: TObject);
const
  FatorClas = 1;
var
  IDCtrl, SMIMultIns, OriCodi, OriCtrl, Empresa, StqCenCad, GraGruX: Integer;
  DataHora: String;
  Qtde, Pecas, Peso, AreaM2, AreaP2: Double;
begin
  if MyObjects.FIC(EdFilial.ValueVariant = 0, EdFilial,
    'Informe a empresa!') then Exit;
  //
  if MyObjects.FIC(EdStqCenCadA.ValueVariant = 0, EdStqCenCadA,
    'Informe o centro de estoque de origem!') then Exit;
  if MyObjects.FIC(EdGraGruXA.ValueVariant = 0, EdGraGruXA,
    'Informe o produto de origem!') then Exit;
  //
  if MyObjects.FIC(EdStqCenCadB.ValueVariant = 0, EdStqCenCadB,
    'Informe o centro de estoque de destino!') then Exit;
  if MyObjects.FIC(EdGraGruXB.ValueVariant = 0, EdGraGruXB,
    'Informe o produto de destino!') then Exit;
  //
  DataHora := Geral.FDT(TPData.Date, 1) + ' ' + EdHora.Text;
  //FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
  if ImgTipo.SQLType = stIns then
    SMIMultIns := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'StqMovItsA', 'SMIMultIns', 'SMIMultIns')
  else
    SMIMultIns := FSMIMultIns;
  OriCodi := SMIMultIns;
  Empresa := DModG.QrFiliLogCodigo.Value;
  //
  //  Dados nova classifica��o!
  StqCenCad := QrStqCenCadBCodigo.Value;
  GraGruX   := EdGraGruXB.ValueVariant;
  Pecas     := EdPecasB.ValueVariant;
  Peso      := EdPesoB.ValueVariant;
  AreaM2    := EdAreaM2B.ValueVariant;
  AreaP2    := EdAreaP2B.ValueVariant;
  case QrGraGruXBGerBxaEstq.Value of
    1: Qtde := Pecas;
    2: Qtde := AreaM2; // Controlar sempre por M2 o estoque da �rea?
    3: Qtde := Peso;
    else Qtde := 0;
  end;
  //
  IDCtrl := UMyMod.BuscaEmLivreY_Def('stqmovitsa', 'IDCtrl', ImgTipo.SQLType, FIDCtrlA);
  OriCtrl := IDCtrl;

  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'stqmovitsa', False, [
  'DataHora', 'Tipo', 'OriCodi',
  'OriCtrl', 'Empresa', 'StqCenCad',
  'GraGruX', 'Qtde', 'Pecas',
  'Peso', 'AreaM2', 'AreaP2',
  'FatorClas', 'SMIMultIns'], [
  'IDCtrl'], [
  DataHora, VAR_FATID_0107, OriCodi,
  OriCtrl, Empresa, StqCenCad,
  GraGruX, Qtde, Pecas,
  Peso, AreaM2, AreaP2,
  FatorClas, SMIMultIns], [
  IDCtrl], False) then
  begin
    //  Dados classifica��o antiga!
    StqCenCad := QrStqCenCadACodigo.Value;
    GraGruX   := EdGraGruXA.ValueVariant;
    Pecas     := - EdPecasA.ValueVariant;
    Peso      := - EdPesoA.ValueVariant;
    AreaM2    := - EdAreaM2A.ValueVariant;
    AreaP2    := - EdAreaP2A.ValueVariant;
    case QrGraGruXAGerBxaEstq.Value of
      1: Qtde := Pecas;
      2: Qtde := AreaM2; // Controlar sempre por M2 o estoque da �rea?
      3: Qtde := Peso;
      else Qtde := 0;
    end;
    //
    IDCtrl := UMyMod.BuscaEmLivreY_Def('stqmovitsa', 'IDCtrl', ImgTipo.SQLType, FIDCtrlB);
    OriCtrl := IDCtrl;

    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'stqmovitsa', False, [
    'DataHora', 'Tipo', 'OriCodi',
    'OriCtrl', 'Empresa', 'StqCenCad',
    'GraGruX', 'Qtde', 'Pecas',
    'Peso', 'AreaM2', 'AreaP2',
    'FatorClas', 'SMIMultIns'], [
    'IDCtrl'], [
    DataHora, VAR_FATID_0108, OriCodi,
    OriCtrl, Empresa, StqCenCad,
    GraGruX, Qtde, Pecas,
    Peso, AreaM2, AreaP2,
    FatorClas, SMIMultIns], [
    IDCtrl], False) then
    begin
     if CkContinuar.Checked then
     begin
       ImgTipo.SQLType          := stIns;
       FDescosideraFocus       := True;
       EdGraGruXA.ValueVariant := 0;
       CBGraGruXA.KeyValue     := 0;
       EdPecasA.ValueVariant   := 0;
       EdAreaP2A.ValueVariant  := 0;
       EdPesoA.ValueVariant    := 0;
       //
       EdGraGruXB.ValueVariant := 0;
       CBGraGruXB.KeyValue     := 0;
       EdPecasB.ValueVariant   := 0;
       EdAreaP2B.ValueVariant  := 0;
       EdPesoB.ValueVariant    := 0;
       //
       EdHora.ValueVariant := Now();
       FDescosideraFocus := False;
       //
       if FForcaReopen then
         FmMPReclasGer.ReopenSMI(SMIMultIns);
       EdGraGruXA.SetFocus;
     end else
       Close;
    end;
  end;
end;

procedure TFmMPReclasIns.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPReclasIns.EdAreaM2AChange(Sender: TObject);
begin
  EdAreaM2B.ValueVariant := EdAreaM2A.ValueVariant;
end;

procedure TFmMPReclasIns.EdAreaP2AChange(Sender: TObject);
begin
  EdAreaP2B.ValueVariant := EdAreaP2A.ValueVariant;
end;

procedure TFmMPReclasIns.EdGraGruXAChange(Sender: TObject);
begin
{
  if EdGraGru1A.ValueVariant = 0 then EdReduzidoA.ValueVariant := 0;
  //
  if not EdGraGru1A.Focused and not FDescosideraFocus then
    EdReduzidoA.ValueVariant := DModG.ObtemReduzidoDeNivel1(QrGraGruXANivel1.Value);
}
  HabilitaOK();
end;

procedure TFmMPReclasIns.EdGraGruXAEnter(Sender: TObject);
begin
  //FGraGru1A := QrGraGruXANivel1.Value;
end;

procedure TFmMPReclasIns.EdGraGruXAExit(Sender: TObject);
begin
{
  if FGraGru1A <> QrGraGruXANivel1.Value then
  begin
    FGraGru1A := QrGraGruXANivel1.Value;
    EdReduzidoA.ValueVariant := DModG.ObtemReduzidoDeNivel1(FGraGru1A);
  end;
}
end;

procedure TFmMPReclasIns.EdGraGruXBChange(Sender: TObject);
begin
{
  if EdGraGru1B.ValueVariant = 0 then EdReduzidoB.ValueVariant := 0;
  //
  if not EdGraGru1B.Focused and not FDescosideraFocus then
    EdReduzidoB.ValueVariant := DModG.ObtemReduzidoDeNivel1(QrGraGruXBNivel1.Value);
  HabilitaOK();
}
end;

procedure TFmMPReclasIns.EdGraGruXBEnter(Sender: TObject);
begin
  //FGraGru1B := QrGraGruXBNivel1.Value;
end;

procedure TFmMPReclasIns.EdGraGruXBExit(Sender: TObject);
begin
{
  if FGraGru1B <> QrGraGruXBNivel1.Value then
  begin
    FGraGru1B := QrGraGruXBNivel1.Value;
    EdReduzidoB.ValueVariant := DModG.ObtemReduzidoDeNivel1(FGraGru1B);
  end;
}
end;

procedure TFmMPReclasIns.EdPecasAChange(Sender: TObject);
begin
  EdPecasB.ValueVariant := EdPecasA.ValueVariant;
end;

procedure TFmMPReclasIns.EdPesoAChange(Sender: TObject);
begin
  EdPesoB.ValueVariant := EdPesoA.ValueVariant;
end;

procedure TFmMPReclasIns.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPReclasIns.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FDescosideraFocus := False;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruXA, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCadA, Dmod.MyDB);
  if QrStqCenCadA.RecordCount = 1 then
  begin
    EdStqCenCadA.ValueVariant := QrStqCenCadACodUsu.Value;
    CBStqCenCadA.KeyValue     := QrStqCenCadACodUsu.Value;
  end;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruXB, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCadB, Dmod.MyDB);
  if QrStqCenCadB.RecordCount = 1 then
  begin
    EdStqCenCadB.ValueVariant := QrStqCenCadBCodUsu.Value;
    CBStqCenCadB.KeyValue     := QrStqCenCadBCodUsu.Value;
  end;
  //
  TPData.Date := Date;
  EdHora.ValueVariant := Now();
end;

procedure TFmMPReclasIns.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPReclasIns.HabilitaOK;
begin
  //
end;

end.
