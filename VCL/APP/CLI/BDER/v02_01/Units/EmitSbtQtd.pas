unit EmitSbtQtd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, dmkRadioGroup, dmkEditCalc, Variants;

type
  TFmEmitSbtQtd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrEmitSbtCad: TMySQLQuery;
    DsEmitSbtCad: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdCodigo: TdmkEdit;
    EdControle: TdmkEdit;
    Panel6: TPanel;
    QrEmitSbtCadCodigo: TIntegerField;
    QrEmitSbtCadNome: TWideStringField;
    Label3: TLabel;
    EdEmitSbtCad: TdmkEditCB;
    CBEmitSbtCad: TdmkDBLookupComboBox;
    EdCouIntros: TdmkEdit;
    Label4: TLabel;
    SbEmitSbtCad: TSpeedButton;
    RGVSBastidao: TdmkRadioGroup;
    RGVSUmidade: TdmkRadioGroup;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    EdPesoKg: TdmkEdit;
    LaPeso: TLabel;
    Label5: TLabel;
    EdEspMedLin: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    EdMediaKg: TdmkEdit;
    Label8: TLabel;
    EdMediaM2: TdmkEdit;
    EdMediaP2: TdmkEdit;
    Label9: TLabel;
    EdFatorUmidade: TdmkEdit;
    EdLinRebCul: TdmkEdit;
    Label10: TLabel;
    EdLinRebCab: TdmkEdit;
    Label11: TLabel;
    EdPercEsperRend: TdmkEdit;
    EdLinRebMed: TdmkEdit;
    Label12: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbEmitSbtCadClick(Sender: TObject);
    procedure CalculaEspessuraMediaLinhas(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEmitSbtCad();
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmEmitSbtQtd: TFmEmitSbtQtd;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule, UnVS_PF, CfgCadLista, UnPQ_PF;

{$R *.DFM}

procedure TFmEmitSbtQtd.BtOKClick(Sender: TObject);
var
  Codigo, Controle, EmitSbtCad, VSUmidade, VSBastidao: Integer;
  CouIntros, AreaM2, AreaP2, PesoKg, MediaKg, MediaM2, MediaP2, EspMedLin,
  FatorUmidade, LinRebCul, LinRebCab, LinRebMed, PercEsperRend: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  EmitSbtCad     := EdEmitSbtCad.ValueVariant;
  CouIntros      := EdCouIntros.ValueVariant;
  VSUmidade      := RGVSUmidade.ItemIndex;
  VSBastidao     := RGVSBastidao.ItemIndex;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  MediaKg        := EdMediaKg.ValueVariant;
  MediaM2        := EdMediaM2.ValueVariant;
  MediaP2        := EdMediaP2.ValueVariant;
  EspMedLin      := EdEspMedLin.ValueVariant;
  FatorUmidade   := EdFatorUmidade.ValueVariant;
  LinRebCul      := EdLinRebCul.ValueVariant;
  LinRebCab      := EdLinRebCab.ValueVariant;
  LinRebMed      := EdLinRebMed.ValueVariant;
  PercEsperRend  := EdPercEsperRend.ValueVariant;
  //
  if MyObjects.FIC(EmitSbtCad = 0, EdEmitSbtCad, 'Defina o Substrato de Rendimento.') then
    Exit;
  //
  if MyObjects.FIC(CouIntros = 0, EdCouIntros, 'Defina a quantidade de couros inteiros.') then
    Exit;
  if MyObjects.FIC(AreaM2 = 0, EdAreaM2, 'Defina a �rea.') then
    Exit;
  if MyObjects.FIC(PesoKg = 0, EdPesoKg, 'Defina o Peso kg.') then
    Exit;
  if MyObjects.FIC(LinRebCul = 0, EdLinRebCul, 'Defina s espessura de rebaixe.') then
    Exit;
  if MyObjects.FIC(LinRebCab = 0, EdLinRebCab, 'Defina s espessura de rebaixe.') then
    Exit;
  if MyObjects.FIC(VSBastidao = 0, RGVSBastidao, 'Defina a Bastid�o.') then
    Exit;
  if MyObjects.FIC(VSUmidade = 0, RGVSUmidade, 'Defina a Umidade.') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('emitsbtqtd', 'Controle', '', '', tsPos, SQLType, Controle);
  EdControle.ValueVariant := Controle;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'emitsbtqtd', False, [
  'Codigo', 'EmitSbtCad', 'CouIntros',
  'VSUmidade', 'VSBastidao', 'AreaM2',
  'AreaP2', 'PesoKg', 'MediaKg',
  'MediaM2', 'MediaP2', 'EspMedLin',
  'FatorUmidade', 'LinRebCul', 'LinRebCab',
  'LinRebMed', 'PercEsperRend'], [
  'Controle'], [
  Codigo, EmitSbtCad, CouIntros,
  VSUmidade, VSBastidao, AreaM2,
  AreaP2, PesoKg, MediaKg,
  MediaM2, MediaP2, EspMedLin,
  FatorUmidade, LinRebCul, LinRebCab,
  LinRebMed, PercEsperRend], [
  Controle], True) then
  begin
    PQ_PF.AtualizaSaldo_EmitSbtQtd(Controle);
    PQ_PF.TotalizaEmitSbt(Codigo);
    Close;
  end;
end;

procedure TFmEmitSbtQtd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmitSbtQtd.CalculaEspessuraMediaLinhas(Sender: TObject);
const
  mmToLin = 10;
  FatPerdaEspesCurt = 0.75;
  BaseRend = 7.00; // %
  BaseEspTDA = 32; // linhas
  FatorIncre = 0.181818; // %
  FatorEspes = 16.3; // Linhas de rebaixe moveleiro espessurado final 13/15
  FatorCul = 0.70;
  FatorCab = 0.30;
var
  VSUmidade: Integer;
  CouIntros, AreaM2, AreaP2, PesoKg, MediaKg, MediaM2, MediaP2, EspMedLin,
  LinRebCul, LinRebCab, LinRebMed, FatorUmidade, PercEsperRend,
  PercPerdaEspesDivTripa: Double;
begin
  CouIntros      := EdCouIntros.ValueVariant;
  VSUmidade      := RGVSUmidade.ItemIndex;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  LinRebCul      := EdLinRebCul.ValueVariant;
  LinRebCab      := EdLinRebCab.ValueVariant;
  //
  case TVSUmidade(VSUmidade) of
    //TVSUmidade.vsumidND=0,
    (*1*)TVSUmidade.vsumidOtima:      FatorUmidade := 1.15;
    (*2*)TVSUmidade.vsumidMuitoBoa:   FatorUmidade := 1.12;
    (*3*)TVSUmidade.vsumidBoa:        FatorUmidade := 1.09;
    (*4*)TVSUmidade.vsumidBaixa:      FatorUmidade := 1.06;
    (*5*)TVSUmidade.vsumidMuitoBaixa: FatorUmidade := 1.03;
    (*6*)TVSUmidade.vsumidPessima:    FatorUmidade := 1.00;
    else FatorUmidade := 1.15;
  end;
  //
  if CouIntros > 0 then
  begin
    MediaKg := PesoKg / CouIntros;
    MediaM2 := AreaM2 / CouIntros;
    MediaP2 := AreaP2 / CouIntros;
  end else
  begin
    MediaKg := 0.000;
    MediaM2 := 0.00;
    MediaP2 := 0.00;
  end;
  //
  if (LinRebCul > 0) and (LinRebCab > 0) then
    LinRebMed := (LinRebCul * FatorCul) + (LinRebCab * FatorCab)
  else
    LinRebMed := 0.00;
  //
  if MediaM2 > 0 then
  begin
    EspMedLin := MediaKg / MediaM2 / FatorUmidade * MmToLin / FatPerdaEspesCurt;
    PercPerdaEspesDivTripa := 1 + ((EspMedLin - BaseEspTDA) / BaseEspTDA / 10);
    if LinRebMed > 0 then
      PercEsperRend := ((BaseRend)+(((EspMedLin * PercPerdaEspesDivTripa) - BaseEspTDA) * FatorIncre)) / LinRebMed * FatorEspes
    else
      PercEsperRend := 0;
  end else
  begin
    EspMedLin     := 0.00;
    PercEsperRend := 0.00;
  end;
  //
  EdFatorUmidade.ValueVariant  := FatorUmidade;
  EdMediaKg.ValueVariant       := MediaKg;
  EdMediaM2.ValueVariant       := MediaM2;
  EdMediaP2.ValueVariant       := MediaP2;
  EdEspMedLin.ValueVariant     := EspMedLin;
  EdLinRebMed.ValueVariant     := LinRebMed;

  EdPercEsperRend.ValueVariant := PercEsperRend;
  //
end;

procedure TFmEmitSbtQtd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmitSbtQtd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  VS_PF.ConfiguraRGVSBastidao(RGVSBastidao, True(*Habilita*), (*Default*)-1);
  VS_PF.ConfiguraRGVSUmidade(RGVSUmidade, True(*Habilita*), (*Default*)-1);
  //
  ReopenEmitSbtCad();
end;

procedure TFmEmitSbtQtd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmitSbtQtd.ReopenEmitSbtCad();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmitSbtCad, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM emitsbtcad ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmEmitSbtQtd.SbEmitSbtCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'EmitSbtCad', 60, ncGerlSeq1,
  'Cadastro de Substratos de Rendimento',
  [], False, Null, [], [], False);
  UMyMod.SetaCodigoPesquisado(EdEmitSbtCad, CBEmitSbtCad, QrEmitSbtCad, VAR_CADASTRO);
end;




(*


  'SELECT CouIntros, AreaM2, LinRebMed, ',
  'AreaP2, PesoKg, FatorUmidade, ',
  'PesoKg / FatorUmidade fUmiKg, ',
  'PesoKg / FatorUmidade / 0.75 KgTripa, ',
  'PesoKg / FatorUmidade / AreaM2 * 10 MedLinWB, ',
  'PesoKg / FatorUmidade / 0.75  / AreaM2 * 10 MedLinTripa, ',
  ' ',
  '/**/ ',
  ' ',
  '(PesoKg / FatorUmidade / 0.75  / AreaM2 * 10) ',
  '* ',
  '(1 + (( ',
  '   (PesoKg / FatorUmidade / 0.75  / AreaM2 * 10) ',
  '    - 32) / 32 / 10 ',
  '   ) ',
  ') MedLinTrpFator, ',
  ' ',
  '/**/ ',
  ' ',
  '(7.00 + ',
  '  (( ',
  '    ((PesoKg / FatorUmidade / 0.75  / AreaM2 * 10) ',
  '    * ',
  '    (1 + (( ',
  '    (PesoKg / FatorUmidade / 0.75  / AreaM2 * 10) ',
  '    - 32) / 32 / 10 ',
  '    )) ',
  '  -32)*0.181818) / LinRebMed * 16.3 ',
  ' ',
  ') ',
  ') MinPrcRend, ',
  '/**/ ',
  ' ',
  '1 + ',
  ' ( ',
  '   ( ',
  '     (PesoKg / FatorUmidade / 0.75  / AreaM2 * 10) ',
  '      - ',
  '      32 ',
  '    ) ',
  '  / 32 / 10 ',
  ') fator ',
  ' ',
  'FROM emitsbtqtd ',
  'WHERE Codigo=1149 ',

*)











end.
