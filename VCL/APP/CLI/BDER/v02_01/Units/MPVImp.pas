unit MPVImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DBCtrls, Db,
  mySQLDbTables, ComCtrls, Menus, frxClass, frxDBSet, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkGeral, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmMPVImp = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    Label10: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrMPVIts: TmySQLQuery;
    DsMPVIts: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DBGrid1: TDBGrid;
    CBMP: TdmkDBLookupComboBox;
    EdMP: TdmkEditCB;
    Label1: TLabel;
    EdVendedor: TdmkEditCB;
    Label2: TLabel;
    CBVendedor: TdmkDBLookupComboBox;
    EdTransp: TdmkEditCB;
    Label3: TLabel;
    CBTransp: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdTexto: TdmkEdit;
    RgFiltro: TRadioGroup;
    QrMP: TmySQLQuery;
    DsMP: TDataSource;
    QrMPCodigo: TIntegerField;
    QrMPNome: TWideStringField;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVendedores: TDataSource;
    QrTransp: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsTransp: TDataSource;
    QrMPVItsVendedor: TIntegerField;
    QrMPVItsCliente: TIntegerField;
    QrMPVItsNOMEMP: TWideStringField;
    QrMPVItsCodigo: TIntegerField;
    QrMPVItsControle: TIntegerField;
    QrMPVItsMP: TIntegerField;
    QrMPVItsQtde: TFloatField;
    QrMPVItsPreco: TFloatField;
    QrMPVItsDesco: TFloatField;
    QrMPVItsValor: TFloatField;
    QrMPVItsTexto: TWideStringField;
    QrMPVItsLk: TIntegerField;
    QrMPVItsDataCad: TDateField;
    QrMPVItsDataAlt: TDateField;
    QrMPVItsUserCad: TIntegerField;
    QrMPVItsUserAlt: TIntegerField;
    QrMPVItsNOMEVENDEDOR: TWideStringField;
    QrMPVItsNOMECLIENTE: TWideStringField;
    QrMPVItsNOMETRANSPOR: TWideStringField;
    QrMPVItsSubTotal: TFloatField;
    PnImprime: TPanel;
    RGAgrupa: TRadioGroup;
    RGOrdem4: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem1: TRadioGroup;
    RGOrdem5: TRadioGroup;
    QrMPVItsDataF: TDateField;
    CkComissoes: TCheckBox;
    QrMPVItsComissV_Per: TFloatField;
    QrMPVItsComissV_Val: TFloatField;
    QrMPVItsCOMISS_ITEM: TFloatField;
    QrMPVItsDESCOEXT_V: TFloatField;
    QrMPVItsTOTAL: TFloatField;
    QrMPVItsDescoExtra: TFloatField;
    QrMPVItsVALORMPV: TFloatField;
    QrMPVItsDESCOEXT_P: TFloatField;
    PMEntrega: TPopupMenu;
    Alteraentrega1: TMenuItem;
    Desfazentrega1: TMenuItem;
    QrMPVItsEntrega: TDateField;
    QrMPVItsPronto: TDateField;
    QrMPVItsStatus: TIntegerField;
    QrMPVItsFluxo: TIntegerField;
    QrMPVItsClasse: TWideStringField;
    frxDsMPVIts: TfrxDBDataset;
    frxMPVIts: TfrxReport;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtImprime: TBitBtn;
    BtEntrega: TBitBtn;
    BtSaida: TBitBtn;
    GroupBox1: TGroupBox;
    CkFimDataF: TCheckBox;
    CkIniDataF: TCheckBox;
    TPIniDataF: TDateTimePicker;
    TPFimDataF: TDateTimePicker;
    GroupBox2: TGroupBox;
    CkFimEntrega: TCheckBox;
    CkIniEntrega: TCheckBox;
    TPIniEntrega: TDateTimePicker;
    TPFimEntrega: TDateTimePicker;
    QrMPVImpOpc: TmySQLQuery;
    QrMPVImpOpcNumero: TIntegerField;
    QrMPVImpOpcLogin: TWideStringField;
    QrMPVImpOpcNOMEFUNCIONARIO: TWideStringField;
    QrMPVImpOpcCodigo: TIntegerField;
    BtOpcoes: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdMPChange(Sender: TObject);
    procedure EdVendedorChange(Sender: TObject);
    procedure EdTranspChange(Sender: TObject);
    procedure EdTextoChange(Sender: TObject);
    procedure RgFiltroClick(Sender: TObject);
    procedure QrMPVItsBeforeClose(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure QrMPVItsCalcFields(DataSet: TDataSet);
    procedure QrMPVItsAfterOpen(DataSet: TDataSet);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure RGOrdem3Click(Sender: TObject);
    procedure RGOrdem4Click(Sender: TObject);
    procedure RGOrdem5Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure TPIniDataFChange(Sender: TObject);
    procedure TPFimDataFChange(Sender: TObject);
    procedure BtEntregaClick(Sender: TObject);
    procedure Alteraentrega1Click(Sender: TObject);
    procedure Desfazentrega1Click(Sender: TObject);
    procedure frxMPVItsGetValue(const VarName: String;
      var Value: Variant);
    procedure BtOpcoesClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenMPVImpOpc(Usuario: Integer);
    procedure FechaMPVIts;
    procedure AbreMPVIts;
    function  OrdemBy1(Item: Integer): String;
    function  OrdemBy2(Item: Integer): String;
  public
    { Public declarations }
  end;

  var
  FmMPVImp: TFmMPVImp;

implementation

uses UnMyObjects, UnInternalConsts, Principal, UMySQLModule, Module, DmkDAC_PF,
  ModuleGeral, MyDBCheck, MPVImpOpc;

{$R *.DFM}

procedure TFmMPVImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPVImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPVImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPVImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType  := stLok;
  BtOpcoes.Visible := (Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN));
  //
  UnDmkDAC_PF.AbreQuery(QrVendedores, Dmod.MyDB);
  //
  if FM_MASTER = 'F' then
  begin
    ReopenMPVImpOpc(VAR_USUARIO);
  end;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransp, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMP, Dmod.MyDB);
  //
  TPIniDataF.Date := Date-30;
  TPFimDataF.Date := Date;
  //
  TPIniEntrega.Date := Date;
  TPFimEntrega.Date := Date + 180;
end;

procedure TFmMPVImp.EdClienteChange(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPVImp.EdMPChange(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPVImp.EdVendedorChange(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPVImp.EdTranspChange(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPVImp.FechaMPVIts;
begin
  QrMPVIts.Close;
end;

procedure TFmMPVImp.BtOKClick(Sender: TObject);
begin
  AbreMPVIts;
end;

procedure TFmMPVImp.BtOpcoesClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPVImpOpc, FmMPVImpOpc, afmoNegarComAviso) then
  begin
    FmMPVImpOpc.ShowModal;
    FmMPVImpOpc.Destroy;
    //
    ReopenMPVImpOpc(VAR_USUARIO);
  end;
end;

procedure TFmMPVImp.AbreMPVIts;
var
  Pre, Pos: String;
  Cliente, MP, Vendedor, Transp: Integer;
begin
  QrMPVIts.Close;
  QrMPVIts.SQL.Clear;
  QrMPVIts.SQL.Add('SELECT mpv.Vendedor, mpv.Cliente, art.Nome NOMEMP, mpv.DataF,');
  QrMPVIts.SQL.Add('mpv.ComissV_Per, mpv.ComissV_Val,');
  QrMPVIts.SQL.Add('mpv.DescoExtra, mpv.Valor VALORMPV, mvi.*,');
  QrMPVIts.SQL.Add('CASE WHEN ven.Tipo=0 THEN ven.RazaoSocial ELSE ven.Nome END NOMEVENDEDOR,');
  QrMPVIts.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLIENTE,');
  QrMPVIts.SQL.Add('CASE WHEN tra.Tipo=0 THEN tra.RazaoSocial ELSE tra.Nome END NOMETRANSPOR');
  QrMPVIts.SQL.Add('FROM mpvits mvi');
  QrMPVIts.SQL.Add('LEFT JOIN mpv       mpv ON mpv.Codigo=mvi.Codigo');
  QrMPVIts.SQL.Add('LEFT JOIN entidades ven ON ven.Codigo=mpv.Vendedor');
  QrMPVIts.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=mpv.Cliente');
  QrMPVIts.SQL.Add('LEFT JOIN entidades tra ON tra.Codigo=mpv.Transp');
  QrMPVIts.SQL.Add('LEFT JOIN artigosgrupos art ON art.Codigo=mvi.MP');
  QrMPVIts.SQL.Add(dmkPF.SQL_Periodo('WHERE mpv.DataF ', TPIniDataF.Date,
      TPFimDataF.Date, CkIniDataF.Checked, CkFimDataF.Checked));
  QrMPVIts.SQL.Add(dmkPF.SQL_Periodo('AND mvi.Entrega ', TPIniEntrega.Date,
      TPFimEntrega.Date, CkIniEntrega.Checked, CkFimEntrega.Checked));
  //
  QrMPVIts.SQL.Add('');
  // Parei Aqui
  Cliente := EdCliente.ValueVariant;
  if Cliente <> 0 then
    QrMPVIts.SQL.Add('AND mpv.Cliente = ' + Geral.FF0(Cliente));
  MP := EdMP.ValueVariant;
  if MP <> 0 then
    QrMPVIts.SQL.Add('AND mvi.MP = ' + Geral.FF0(MP));
  Vendedor := EdVendedor.ValueVariant;
  if Vendedor <> 0 then
    QrMPVIts.SQL.Add('AND mpv.Vendedor = ' + Geral.FF0(Vendedor));
  Transp := EdTransp.ValueVariant;
  if Transp <> 0 then
    QrMPVIts.SQL.Add('AND mpv.Transp = ' + Geral.FF0(Transp));
  if EdTexto.Text <> '' then
  begin
    if RgFiltro.ItemIndex in ([1,2]) then Pre := '%' else Pre := '';
    if RgFiltro.ItemIndex in ([1,3]) then Pos := '%' else Pos := '';
    QrMPVIts.SQL.Add('AND mvi.Texto LIKE "'+Pre+EdTexto.Text+Pos+'"');
  end;
  {
  CkProntoI.Checked then sP := 1 else sP := 0;
  CkProntoF.Checked then sP := sP + 1;
  case RGPronto.ItemIndex of
    0: QrMPVIts.SQL.Add('AND mvi.Pronto = 0');
    1:
    begin
      if sP = 0 then QrMPVIts.SQL.Add('AND mvi.Pronto > 0')
      else QrMPVIts.SQL.Add(dmkPF.SQL_Periodo('AND mvi.Pronto ', TPProntoI.Date,
      TPProntoF.Date, CkProntoI.Checked, CkProntoF.Checked));
    end;
    2:
    begin
      if sP = 0 then QrMPVIts.SQL.Add('') // nada (qualquer)
      else QrMPVIts.SQL.Add(dmkPF.SQL_Periodo(
      'AND (mvi.Pronto = 0 OR mvi.Pronto ', TPProntoI.Date,
      TPProntoF.Date, CkProntoI.Checked, CkProntoF.Checked)+')');
    end;
  end;
  }
  //////////////////////////////////////////////////////////////////////////////
  QrMPVIts.SQL.Add('ORDER BY '+
  OrdemBy1(RGOrdem1.ItemIndex)+', '+
  OrdemBy1(RGOrdem2.ItemIndex)+', '+
  OrdemBy1(RGOrdem3.ItemIndex)+', '+
  OrdemBy1(RGOrdem4.ItemIndex)+', '+
  OrdemBy1(RGOrdem5.ItemIndex));
  //
  UnDmkDAC_PF.AbreQuery(QrMPVIts, Dmod.MyDB);
end;

function TFmMPVImp.OrdemBy1(Item: Integer): String;
begin
  case Item of
    0: Result := 'NOMECLIENTE';
    1: Result := 'mvi.MP';
    2: Result := 'NOMEVENDEDOR';
    3: Result := 'NOMETRANSPOR';
    4: Result := 'mvi.Texto';
    5: Result := 'mpv.DataF';
    6: Result := 'mvi.Entrega';
    else Result := '**EROR OrdemBy **';
  end;
end;

function TFmMPVImp.OrdemBy2(Item: Integer): String;
begin
  case Item of
    0: Result := 'NOMECLIENTE';
    1: Result := 'NOMEMP';
    2: Result := 'NOMEVENDEDOR';
    3: Result := 'NOMETRANSPOR';
    4: Result := 'Texto';
    5: Result := 'DataF';
    6: Result := 'Entrega';
    else Result := '**EROR OrdemBy **';
  end;
end;

procedure TFmMPVImp.EdTextoChange(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPVImp.ReopenMPVImpOpc(Usuario: Integer);
var
  Enab: Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMPVImpOpc, Dmod.MyDB, [
    'SELECT op.Codigo, si.Numero, si.Login, en.Nome NOMEFUNCIONARIO ',
    'FROM mpvimpopc op ',
    'LEFT JOIN senhas si ON si.Numero = op.Usuario ',
    'LEFT JOIN entidades en ON en.Codigo=si.Funcionario ',
    'WHERE op.Usuario=' + Geral.FF0(Usuario),
    '']);
  if QrMPVImpOpc.RecordCount = 0 then
    Enab := False
  else
    Enab := True;
  //
  EdVendedor.Text     := IntToStr(VAR_FUNCILOGIN);
  CBVendedor.KeyValue := VAR_FUNCILOGIN;
  EdVendedor.Enabled  := Enab;
  CBVendedor.Enabled  := Enab;
end;

procedure TFmMPVImp.RgFiltroClick(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPVImp.QrMPVItsBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmMPVImp.QrMPVItsCalcFields(DataSet: TDataSet);
var
  DescoExP: Double;
begin
//erro % 66670,00 em vez de 66666,67
  if QrMPVItsVALORMPV.Value = 0 then DescoExP := 0 else
    DescoExP := QrMPVItsDescoExtra.Value / QrMPVItsVALORMPV.Value;
  QrMPVItsSubTotal.Value := QrMPVItsValor.Value + QrMPVItsDesco.Value;
  QrMPVItsDESCOEXT_V.Value :=
    QrMPVItsValor.Value * DescoExP;
  QrMPVItsTOTAL.Value :=
    QrMPVItsValor.Value - QrMPVItsDESCOEXT_V.Value;
  //
  QrMPVItsCOMISS_ITEM.Value :=
    QrMPVItsTOTAL.Value * QrMPVItsComissV_Per.Value / 100;
  //
  QrMPVItsDESCOEXT_P.Value := DescoExP * 100;
end;

procedure TFmMPVImp.QrMPVItsAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := True;
end;

procedure TFmMPVImp.RGOrdem1Click(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPVImp.RGOrdem2Click(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPVImp.RGOrdem3Click(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPVImp.RGOrdem4Click(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPVImp.RGOrdem5Click(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPVImp.BtImprimeClick(Sender: TObject);
  function Condicao(Agrupa: Boolean; Item: Integer): String;
  begin
    if Agrupa then
      Result := '''' + 'frxDsMPVIts."' + OrdemBy2(Item) + '"'''
    else Result := '';
  end;

begin
  frxMPVIts.Variables['VARF_COMI' ]   := CkComissoes.Checked;

  frxMPVIts.Variables['VARF_VISI1']   := RGAgrupa.ItemIndex > 0;
  frxMPVIts.Variables['VARF_VISI2']   := RGAgrupa.ItemIndex > 1;
  frxMPVIts.Variables['VARF_VISI3']   := RGAgrupa.ItemIndex > 2;

  frxMPVIts.Variables['VARF_COND1']   := Condicao(RGAgrupa.ItemIndex > 0, RGOrdem1.ItemIndex);
  frxMPVIts.Variables['VARF_COND2']   := Condicao(RGAgrupa.ItemIndex > 1, RGOrdem2.ItemIndex);
  frxMPVIts.Variables['VARF_COND3']   := Condicao(RGAgrupa.ItemIndex > 2, RGOrdem3.ItemIndex);

  frxMPVIts.Variables['VARF_MEMO1']   := '''' + RGOrdem1.Items[RGOrdem1.ItemIndex] +
    ': ' + '[frxDsMPVIts."' + OrdemBy2(RGOrdem1.ItemIndex) + '"]''';
  frxMPVIts.Variables['VARF_MEMO2']   := '''' + RGOrdem2.Items[RGOrdem2.ItemIndex] +
    ': ' + '[frxDsMPVIts."' + OrdemBy2(RGOrdem2.ItemIndex) + '"]''';
  frxMPVIts.Variables['VARF_MEMO3']   := '''' + RGOrdem3.Items[RGOrdem3.ItemIndex] +
    ': ' + '[frxDsMPVIts."' + OrdemBy2(RGOrdem3.ItemIndex) + '"]''';

  //

  MyObjects.frxMostra(frxMPVIts, 'Relat�rio de vendas');
end;

procedure TFmMPVImp.TPIniDataFChange(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPVImp.TPFimDataFChange(Sender: TObject);
begin
  FechaMPVIts;
end;

procedure TFmMPVImp.BtEntregaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntrega, BtEntrega);
end;

procedure TFmMPVImp.Alteraentrega1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrMPVItsControle.Value;
  Fmprincipal.StatusPedido(Controle, QrMPVItsEntrega.Value);
  //Controle := UMyMod.ProximoRegistro(QrMPVIts, 'Controle', Controle);
  AbreMPVIts;
end;

procedure TFmMPVImp.Desfazentrega1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrMPVItsControle.Value;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE mpvits SET Pronto=0');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
  Dmod.QrUpd.Params[00].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  //Controle := UMyMod.ProximoRegistro(QrMPVIts, 'Controle', Controle);
  AbreMPVIts;
end;

procedure TFmMPVImp.frxMPVItsGetValue(const VarName: String;
  var Value: Variant);
var
  Pre, Pos: String;
begin
  if VarName = 'VARF_PERIODO' then Value :=
    dmkPF.PeriodoImp(TPIniDataF.Date, TPFimDataF.Date, 0, 0, CkIniDataF.Checked,
      CkFimDataF.Checked, False, False, 'Emiss�o:', '') + ' - ' +
    dmkPF.PeriodoImp(TPIniEntrega.Date, TPFimEntrega.Date, 0, 0, CkIniEntrega.Checked,
      CkFimEntrega.Checked, False, False, 'Emiss�o:', '')
  else if VarName = 'VARF_CLIENTE' then
    Value := dmkPF.CampoReportTxt(CBCliente.Text, 'TODOS')
  else if VarName = 'VARF_MP' then
    Value := dmkPF.CampoReportTxt(CBMP.Text, 'TODOS')
  else if VarName = 'VARF_VENDEDOR' then
    Value := dmkPF.CampoReportTxt(CBVendedor.Text, 'TODOS')
  else if VarName = 'VARF_DESCRICAO' then
  begin
    if EdTexto.Text <> '' then
    begin
      if RgFiltro.ItemIndex in ([1,2]) then Pre := '%' else Pre := '';
      if RgFiltro.ItemIndex in ([1,3]) then Pos := '%' else Pos := '';
      Value := Pre+EdTexto.Text+Pos;
    end else Value := ' ';
  end

end;

end.

