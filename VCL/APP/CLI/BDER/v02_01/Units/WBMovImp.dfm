object FmWBMovImp: TFmWBMovImp
  Left = 339
  Top = 185
  Caption = 'WET-RECUR-004 :: Relat'#243'rios de Wet Blue'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 282
        Height = 32
        Caption = 'Relat'#243'rios de Wet Blue'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 282
        Height = 32
        Caption = 'Relat'#243'rios de Wet Blue'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 282
        Height = 32
        Caption = 'Relat'#243'rios de Wet Blue'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 456
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 456
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 456
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 42
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label7: TLabel
            Left = 8
            Top = 0
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object EdEmpresa: TdmkEditCB
            Left = 8
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 64
            Top = 16
            Width = 725
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object PCRelatorio: TPageControl
          Left = 2
          Top = 57
          Width = 808
          Height = 397
          ActivePage = TabSheet1
          Align = alClient
          TabOrder = 1
          object TabSheet1: TTabSheet
            Caption = ' Estoque'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 800
              Height = 369
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object RG00_Ordem1: TRadioGroup
                Left = 8
                Top = 84
                Width = 105
                Height = 105
                Caption = ' Ordem 1:'
                ItemIndex = 0
                Items.Strings = (
                  'Mat'#233'ria-prima'
                  'Fornecedor'
                  'Pallet')
                TabOrder = 0
              end
              object RG00_Ordem2: TRadioGroup
                Left = 120
                Top = 84
                Width = 105
                Height = 105
                Caption = ' Ordem 2:'
                ItemIndex = 1
                Items.Strings = (
                  'Mat'#233'ria-prima'
                  'Fornecedor'
                  'Pallet')
                TabOrder = 1
              end
              object RG00_Ordem3: TRadioGroup
                Left = 232
                Top = 84
                Width = 105
                Height = 105
                Caption = ' Ordem 3:'
                ItemIndex = 2
                Items.Strings = (
                  'Mat'#233'ria-prima'
                  'Fornecedor'
                  'Pallet')
                TabOrder = 2
              end
              object RG00_AgrupaWB: TRadioGroup
                Left = 344
                Top = 84
                Width = 105
                Height = 105
                Caption = ' Agrupamentos:'
                ItemIndex = 0
                Items.Strings = (
                  '0'
                  '1'
                  '2')
                TabOrder = 3
              end
              object Ck00DataCompra: TCheckBox
                Left = 8
                Top = 4
                Width = 381
                Height = 17
                Caption = 
                  'N'#227'o considerar na soma as compras que n'#227'o chegaram na empresa ai' +
                  'nda.'
                TabOrder = 4
              end
              object RG00ZeroNegat: TRadioGroup
                Left = 8
                Top = 28
                Width = 441
                Height = 49
                Caption = ' Estoques a serem considerados: '
                Columns = 3
                ItemIndex = 1
                Items.Strings = (
                  'Somente positivos'
                  'Positivos e negativos'
                  'Todos inclusive zerados')
                TabOrder = 5
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Hist'#243'rico'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 800
              Height = 369
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label1: TLabel
                Left = 8
                Top = 0
                Width = 66
                Height = 13
                Caption = 'Mat'#233'ria-prima:'
              end
              object Label4: TLabel
                Left = 8
                Top = 40
                Width = 29
                Height = 13
                Caption = 'Pallet:'
              end
              object Label10: TLabel
                Left = 8
                Top = 88
                Width = 144
                Height = 13
                Caption = 'Terceiro (fornecedor / cliente):'
              end
              object Ed01GraGruX: TdmkEditCB
                Left = 8
                Top = 16
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Cargo'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB01GraGruX
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB01GraGruX: TdmkDBLookupComboBox
                Left = 64
                Top = 16
                Width = 469
                Height = 21
                KeyField = 'Controle'
                ListField = 'NO_PRD_TAM_COR'
                ListSource = Ds01GraGruX
                TabOrder = 1
                dmkEditCB = Ed01GraGruX
                QryCampo = 'Cargo'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object Ed01Pallet: TdmkEditCB
                Left = 8
                Top = 56
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Pallet'
                UpdCampo = 'Pallet'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB01Pallet
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB01Pallet: TdmkDBLookupComboBox
                Left = 64
                Top = 56
                Width = 469
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = Ds01WBPallet
                TabOrder = 3
                dmkEditCB = Ed01Pallet
                QryCampo = 'Pallet'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object GroupBox2: TGroupBox
                Left = 540
                Top = 4
                Width = 245
                Height = 73
                Caption = ' Per'#237'odo: '
                TabOrder = 4
                object TP01DataIni: TdmkEditDateTimePicker
                  Left = 8
                  Top = 40
                  Width = 112
                  Height = 21
                  CalColors.TextColor = clMenuText
                  Date = 37636.777157974500000000
                  Time = 37636.777157974500000000
                  TabOrder = 1
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object Ck01DataIni: TCheckBox
                  Left = 8
                  Top = 20
                  Width = 112
                  Height = 17
                  Caption = 'Data inicial'
                  Checked = True
                  State = cbChecked
                  TabOrder = 0
                end
                object Ck01DataFim: TCheckBox
                  Left = 124
                  Top = 20
                  Width = 112
                  Height = 17
                  Caption = 'Data final'
                  TabOrder = 2
                end
                object TP01DataFim: TdmkEditDateTimePicker
                  Left = 124
                  Top = 40
                  Width = 112
                  Height = 21
                  Date = 37636.777203761600000000
                  Time = 37636.777203761600000000
                  TabOrder = 3
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
              end
              object Ed01Terceiro: TdmkEditCB
                Left = 160
                Top = 84
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Fornecedor'
                UpdCampo = 'Fornecedor'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB01Terceiro
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB01Terceiro: TdmkDBLookupComboBox
                Left = 216
                Top = 84
                Width = 569
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NOMEENTIDADE'
                ListSource = Ds01Fornecedor
                TabOrder = 6
                dmkEditCB = Ed01Terceiro
                QryCampo = 'Fornecedor'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'Lan'#231'amentos'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 800
              Height = 369
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Panel9: TPanel
                Left = 0
                Top = 0
                Width = 800
                Height = 109
                Align = alTop
                TabOrder = 0
                object Label2: TLabel
                  Left = 8
                  Top = 0
                  Width = 66
                  Height = 13
                  Caption = 'Mat'#233'ria-prima:'
                end
                object Label3: TLabel
                  Left = 8
                  Top = 40
                  Width = 29
                  Height = 13
                  Caption = 'Pallet:'
                end
                object Label5: TLabel
                  Left = 8
                  Top = 88
                  Width = 144
                  Height = 13
                  Caption = 'Terceiro (fornecedor / cliente):'
                end
                object Label6: TLabel
                  Left = 540
                  Top = 88
                  Width = 85
                  Height = 13
                  Caption = 'ID Item (Controle):'
                end
                object Ed02GraGruX: TdmkEditCB
                  Left = 8
                  Top = 16
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'Cargo'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CB02GraGruX
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CB02GraGruX: TdmkDBLookupComboBox
                  Left = 64
                  Top = 16
                  Width = 469
                  Height = 21
                  KeyField = 'Controle'
                  ListField = 'NO_PRD_TAM_COR'
                  ListSource = Ds02GraGruX
                  TabOrder = 1
                  dmkEditCB = Ed02GraGruX
                  QryCampo = 'Cargo'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object Ed02Pallet: TdmkEditCB
                  Left = 8
                  Top = 56
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'Pallet'
                  UpdCampo = 'Pallet'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CB02Pallet
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CB02Pallet: TdmkDBLookupComboBox
                  Left = 64
                  Top = 56
                  Width = 469
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = Ds02WBPallet
                  TabOrder = 3
                  dmkEditCB = Ed02Pallet
                  QryCampo = 'Pallet'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object Ed02Terceiro: TdmkEditCB
                  Left = 160
                  Top = 84
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'Fornecedor'
                  UpdCampo = 'Fornecedor'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CB02Terceiro
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CB02Terceiro: TdmkDBLookupComboBox
                  Left = 216
                  Top = 84
                  Width = 317
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'NOMEENTIDADE'
                  ListSource = Ds02Fornecedor
                  TabOrder = 5
                  dmkEditCB = Ed02Terceiro
                  QryCampo = 'Fornecedor'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object GroupBox3: TGroupBox
                  Left = 540
                  Top = 4
                  Width = 245
                  Height = 73
                  Caption = ' Per'#237'odo: '
                  TabOrder = 6
                  object TP02DataIni: TdmkEditDateTimePicker
                    Left = 8
                    Top = 40
                    Width = 112
                    Height = 21
                    CalColors.TextColor = clMenuText
                    Date = 37636.777157974500000000
                    Time = 37636.777157974500000000
                    TabOrder = 1
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                    DatePurpose = dmkdpNone
                  end
                  object Ck02DataIni: TCheckBox
                    Left = 8
                    Top = 20
                    Width = 112
                    Height = 17
                    Caption = 'Data inicial'
                    Checked = True
                    State = cbChecked
                    TabOrder = 0
                  end
                  object Ck02DataFim: TCheckBox
                    Left = 124
                    Top = 20
                    Width = 112
                    Height = 17
                    Caption = 'Data final'
                    TabOrder = 2
                  end
                  object TP02DataFim: TdmkEditDateTimePicker
                    Left = 124
                    Top = 40
                    Width = 112
                    Height = 21
                    Date = 37636.777203761600000000
                    Time = 37636.777203761600000000
                    TabOrder = 3
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                    DatePurpose = dmkdpNone
                  end
                end
                object Ed02Controle: TdmkEdit
                  Left = 628
                  Top = 84
                  Width = 80
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 7
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
              end
              object Panel10: TPanel
                Left = 0
                Top = 324
                Width = 800
                Height = 45
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 1
                Visible = False
                object BitBtn1: TBitBtn
                  Left = 12
                  Top = 4
                  Width = 120
                  Height = 40
                  Caption = '?'
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtOKClick
                end
              end
              object DG02WBMovIts: TDBGrid
                Left = 0
                Top = 109
                Width = 800
                Height = 215
                Align = alClient
                DataSource = DsWBMovIts
                TabOrder = 2
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnDblClick = DG02WBMovItsDblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_MovimID'
                    Title.Caption = 'Movimento'
                    Width = 58
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'ID item'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataHora'
                    Title.Caption = 'Data / Hora'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pallet'
                    Title.Caption = 'ID Pallet'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_Pallet'
                    Title.Caption = 'Pallet'
                    Width = 76
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD_TAM_COR'
                    Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                    Width = 220
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaP2'
                    Title.Caption = #193'rea ft'#178
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorT'
                    Title.Caption = 'Valor total'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SdoVrtPeca'
                    Title.Caption = 'Sdo virtual p'#231
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SdoVrtArM2'
                    Title.Caption = 'Sdo virtual m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Observ'
                    Title.Caption = 'Observa'#231#245'es'
                    Width = 300
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Terceiro'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TERCEIRO'
                    Title.Caption = 'Nome do terceiro'
                    Width = 220
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'C'#243'digo'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MovimCod'
                    Title.Caption = 'Movim.C'#243'd.'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SrcMovID'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SrcNivel1'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SrcNivel2'
                    Width = 60
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet4: TTabSheet
            Caption = 'Fichas dos Pallets'
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel11: TPanel
              Left = 0
              Top = 0
              Width = 800
              Height = 369
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object DBG03Estq: TdmkDBGridDAC
                Left = 0
                Top = 0
                Width = 800
                Height = 324
                SQLFieldsToChange.Strings = (
                  'Ativo')
                SQLIndexesOnUpdate.Strings = (
                  'GraGruX'
                  'Pallet'
                  'Terceiro')
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Ativo'
                    Title.Caption = 'ok'
                    Width = 17
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pallet'
                    Title.Caption = 'ID Pallet'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PALLET'
                    Title.Caption = 'Pallet'
                    Width = 76
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD_TAM_COR'
                    Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                    Width = 220
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'OrdGGX'
                    Title.Caption = 'Ordem'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaP2'
                    Title.Caption = #193'rea ft'#178
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorT'
                    Title.Caption = 'Valor total'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Terceiro'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TERCEIRO'
                    Title.Caption = 'Nome do terceiro'
                    Width = 220
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsEstqR3
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                SQLTable = '_wbmovimp3_'
                EditForceNextYear = False
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Ativo'
                    Title.Caption = 'ok'
                    Width = 17
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pallet'
                    Title.Caption = 'ID Pallet'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PALLET'
                    Title.Caption = 'Pallet'
                    Width = 76
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD_TAM_COR'
                    Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                    Width = 220
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'OrdGGX'
                    Title.Caption = 'Ordem'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaP2'
                    Title.Caption = #193'rea ft'#178
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorT'
                    Title.Caption = 'Valor total'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Terceiro'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TERCEIRO'
                    Title.Caption = 'Nome do terceiro'
                    Width = 220
                    Visible = True
                  end>
              end
              object Panel12: TPanel
                Left = 0
                Top = 324
                Width = 800
                Height = 45
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 1
                object Bt03Imprime: TBitBtn
                  Tag = 5
                  Left = 20
                  Top = 4
                  Width = 120
                  Height = 40
                  Caption = '&Imprime'
                  Enabled = False
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = Bt03ImprimeClick
                end
                object Bt03Tudo: TBitBtn
                  Tag = 127
                  Left = 140
                  Top = 4
                  Width = 120
                  Height = 40
                  Caption = '&Todos'
                  Enabled = False
                  NumGlyphs = 2
                  TabOrder = 1
                  OnClick = Bt03TudoClick
                end
                object Bt03Nenhum: TBitBtn
                  Tag = 128
                  Left = 260
                  Top = 4
                  Width = 120
                  Height = 40
                  Caption = '&Nenhum'
                  Enabled = False
                  NumGlyphs = 2
                  TabOrder = 2
                  OnClick = Bt03NenhumClick
                end
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 504
    Width = 812
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 808
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEstqR1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.GraGruX, SUM(wmi.Pecas) Pecas,'
      'SUM(wmi.PesoKg) PesoKg, SUM(wmi.AreaM2) AreaM2,'
      'SUM(AreaP2) AreaP2,'
      'ggx.GraGru1, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR'
      'FROM wbmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'GROUP BY wmi.GraGruX'
      'ORDER BY NO_PRD_TAM_COR')
    Left = 60
    Top = 380
    object QrEstqR1GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstqR1Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEstqR1PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrEstqR1AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEstqR1AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrEstqR1GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstqR1NO_PRD_TAM_COR: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrEstqR1Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstqR1NO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrEstqR1Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrEstqR1NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrEstqR1ValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object frxWET_RECUR_004_00_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  FWBNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '    '
      'begin'
      '  FWBNivGer := <VARF_WBNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FWBNivGer > 0;'
      '  MeTitAreaM2.Visible := FWBNivGer > 0;'
      '  MeTitAreaP2.Visible := FWBNivGer > 0;'
      '  MeTitValorT.Visible := FWBNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FWBNivGer > 0;'
      '  MeValAreaM2.Visible := FWBNivGer > 0;'
      '  MeValAreaP2.Visible := FWBNivGer > 0;'
      '  MeValValorT.Visible := FWBNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FWBNivGer > 0;'
      '  MeSu1AreaM2.Visible := FWBNivGer > 0;'
      '  MeSu1AreaP2.Visible := FWBNivGer > 0;'
      '  MeSu1ValorT.Visible := FWBNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FWBNivGer > 0;'
      '  MeSu2AreaM2.Visible := FWBNivGer > 0;'
      '  MeSu2AreaP2.Visible := FWBNivGer > 0;'
      '  MeSu2ValorT.Visible := FWBNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FWBNivGer > 0;'
      '  MeSuTAreaM2.Visible := FWBNivGer > 0;'
      '  MeSuTAreaP2.Visible := FWBNivGer > 0;'
      '  MeSuTValorT.Visible := FWBNivGer > 1;'
      '  //        '
      '  case FWBNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      'end.')
    OnGetValue = frxWET_RECUR_004_00_AGetValue
    Left = 60
    Top = 336
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR1
        DataSetName = 'frxDsEstqR1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque de Mat'#233'ria-Prima para o Recurtimento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 68.031540000000010000
          Top = 64.252010000000000000
          Width = 234.330664720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          Left = 604.724800000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaP2: TfrxMemoView
          Left = 529.134199999999900000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 453.543600000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 377.953000000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitValorT: TfrxMemoView
          Left = 302.362400000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEstqR1
        DataSetName = 'frxDsEstqR1'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 68.031540000000010000
          Width = 234.330664720000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR1."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreap2: TfrxMemoView
          Left = 529.134199999999900000
          Width = 75.590551180000000000
          Height = 15.118110236220470000
          DataField = 'AreaP2'
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110236220470000
          DataField = 'AreaM2'
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110236220470000
          DataField = 'PesoKg'
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValValorT: TfrxMemoView
          Left = 302.362400000000000000
          Width = 75.590551180000000000
          Height = 15.118110236220470000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 445.984540000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 400.630180000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 3.779530000000022000
          Width = 302.362204720000000000
          Height = 15.118110236220470000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 604.724800000000000000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaP2: TfrxMemoView
          Left = 529.134199999999900000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 15.118110236220470000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."AreaP2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaM2: TfrxMemoView
          Left = 453.543600000000000000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 15.118110236220470000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTPesoKg: TfrxMemoView
          Left = 377.953000000000000000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 15.118110236220470000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTValorT: TfrxMemoView
          Left = 302.362400000000000000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 15.118110236220470000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."ValorT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsEstqR1."GraGruX"'
        object Me_GH1: TfrxMemoView
          Top = 3.779529999999994000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
        object Memo3: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT1: TfrxMemoView
          Width = 302.362180310000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"] ')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu1AreaP2: TfrxMemoView
          Left = 529.134199999999900000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."AreaP2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSu1AreaM2: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSu1PesoKg: TfrxMemoView
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSu1ValorT: TfrxMemoView
          Left = 302.362400000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."ValorT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_02: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 204.094620000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsEstqR1."GraGruX"'
        object Me_GH2: TfrxMemoView
          Top = 3.779529999999994000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_02: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        object Me_FT2: TfrxMemoView
          Width = 302.362180310000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"] ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSu2AreaP2: TfrxMemoView
          Left = 529.134199999999900000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."AreaP2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSu2AreaM2: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSu2PesoKg: TfrxMemoView
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSu2ValorT: TfrxMemoView
          Left = 302.362400000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."ValorT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsEstqR1: TfrxDBDataset
    UserName = 'frxDsEstqR1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Terceiro=Terceiro'
      'NO_TERCEIRO=NO_TERCEIRO'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'ValorT=ValorT')
    DataSet = QrEstqR1
    BCDToCurrency = False
    Left = 60
    Top = 424
  end
  object Qr01Fornecedor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 344
    object Qr01FornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr01FornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object Ds01Fornecedor: TDataSource
    DataSet = Qr01Fornecedor
    Left = 344
    Top = 44
  end
  object Qr01WBPallet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wbpallet'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 412
    object Qr01WBPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr01WBPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds01WBPallet: TDataSource
    DataSet = Qr01WBPallet
    Left = 412
    Top = 44
  end
  object Qr01GraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 484
    object Qr01GraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr01GraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr01GraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr01GraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object Qr01GraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object Qr01GraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object Ds01GraGruX: TDataSource
    DataSet = Qr01GraGruX
    Left = 484
    Top = 44
  end
  object QrEstqR2: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEstqR2CalcFields
    SQL.Strings = (
      'DELETE FROM _wbmovimp2_ '
      '; '
      'INSERT INTO _wbmovimp2_ '
      'SELECT 1 OrdGrupSeq, '
      'wmi.Codigo, wmi.Controle, wmi.MovimCod, '
      'wmi.MovimNiv, wmi.Empresa, wmi.Terceiro, '
      'wmi.MovimID, wmi.DataHora, wmi.Pallet, '
      'wmi.GraGruX, wmi.Pecas, wmi.PesoKg, '
      'wmi.AreaM2, wmi.AreaP2, wmi.SrcMovID, '
      'wmi.SrcNivel1, wmi.SrcNivel2, '
      'wmi.SdoVrtPeca, wmi.SdoVrtArM2, '
      'wmi.Observ, 0 ValorT, ggx.GraGru1, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_PALLET, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, '
      '0 AcumPecas, 0 AcumPesoKg, 0 AcumAreaM2, '
      '0 AcumAreaP2, 0 AcumValorT, 1 Ativo '
      'FROM bluederm.wbmovits wmi '
      'LEFT JOIN bluederm.gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN bluederm.gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN bluederm.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN bluederm.gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN bluederm.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN bluederm.wbpallet   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN bluederm.entidades  ent ON ent.Codigo=wmi.Terceiro '
      'WHERE wmi.Empresa=-11'
      'AND wmi.GraGruX=3328'
      'AND wmi.DataHora  BETWEEN "2013-09-07" AND "2013-12-06 23:59:59"'
      '; '
      'SELECT * FROM _wbmovimp2_'
      'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; '
      ' ')
    Left = 200
    Top = 380
    object QrEstqR2OrdGrupSeq: TIntegerField
      FieldName = 'OrdGrupSeq'
    end
    object QrEstqR2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstqR2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEstqR2MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEstqR2MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrEstqR2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstqR2Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstqR2MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEstqR2DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrEstqR2Pallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrEstqR2GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstqR2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEstqR2PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrEstqR2AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEstqR2AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrEstqR2SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrEstqR2SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrEstqR2SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrEstqR2SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrEstqR2SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrEstqR2Observ: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrEstqR2ValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrEstqR2GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstqR2NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 160
    end
    object QrEstqR2NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrEstqR2NO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrEstqR2AcumPecas: TFloatField
      FieldName = 'AcumPecas'
    end
    object QrEstqR2AcumPesoKg: TFloatField
      FieldName = 'AcumPesoKg'
    end
    object QrEstqR2AcumAreaM2: TFloatField
      FieldName = 'AcumAreaM2'
    end
    object QrEstqR2AcumAreaP2: TFloatField
      FieldName = 'AcumAreaP2'
    end
    object QrEstqR2AcumValorT: TFloatField
      FieldName = 'AcumValorT'
    end
    object QrEstqR2Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEstqR2NO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
    end
    object QrEstqR2CustoM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CustoM'
      Calculated = True
    end
    object QrEstqR2AcumCustoM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AcumCustoM'
      Calculated = True
    end
  end
  object frxWET_RECUR_004_01_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412040000000
    ReportOptions.LastChange = 41608.425381412040000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      
        'procedure DBCross1ColumnTotal0OnBeforePrint(Sender: TfrxComponen' +
        't);'
      'begin'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_RECUR_004_00_AGetValue
    Left = 200
    Top = 336
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR2
        DataSetName = 'frxDsEstqR2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 120.944950240000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Shape3: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 975.118740000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 668.976810000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Movimento de Mat'#233'ria-Prima para Semi / Acabado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 820.158010000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 68.031540000000010000
          Top = 105.826840000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 366.614410000000000000
          Top = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Top = 64.252010000000000000
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo2: TfrxMemoView
          Left = 699.213050000000000000
          Top = 64.252010000000000000
          Width = 34.015770000000010000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pallet:')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 733.228820000000000000
          Top = 64.252010000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PALLET_1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Top = 83.149660000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Terceiro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 71.811070000000000000
          Top = 83.149660000000000000
          Width = 891.969080000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_TERCEIRO_1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Top = 64.252010000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 71.811070000000000000
          Top = 64.252010000000000000
          Width = 627.401980000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_GRAGRUX_1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 317.480520000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 423.307360000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 287.244280000000000000
          Top = 105.826840000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 105.826840000000000000
          Top = 105.826840000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 585.827150000000000000
          Top = 105.826840000000000000
          Width = 79.370029919999990000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 752.126470000000000000
          Top = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acum. ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 702.992580000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acum. m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 808.819420000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acum. Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 665.197280000000000000
          Top = 105.826840000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acum. p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 857.953310000000000000
          Top = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo item $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 472.441250000000000000
          Top = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo item $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 105.826840000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 181.417440000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID item')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 230.551330000000000000
          Top = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Movimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 529.134199999999900000
          Top = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 914.646260000000000000
          Top = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 200.315090000000000000
        Width = 971.339210000000000000
        DataSet = frxDsEstqR2
        DataSetName = 'frxDsEstqR2'
        RowCount = 0
        object Memo57: TfrxMemoView
          Left = 68.031540000000010000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'Pallet'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR2."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 366.614410000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 317.480520000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 423.307360000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 287.244280000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 105.826840000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR2."NO_PALLET"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 585.827150000000000000
          Width = 79.370029919999990000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR2."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 752.126470000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'AcumAreaP2'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumAreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 702.992580000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataField = 'AcumAreaM2'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumAreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 808.819420000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataField = 'AcumPesoKg'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumPesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 665.197280000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'AcumPecas'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumPecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 857.953310000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'AcumValorT'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 529.134199999999900000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."CustoM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Width = 68.031496062992130000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR2."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 181.417440000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR2."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 230.551330000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'NO_MovimID'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR2."NO_MovimID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 472.441250000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 914.646260000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumCustoM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 275.905690000000000000
        Width = 971.339210000000000000
        object Memo93: TfrxMemoView
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 600.945270000000000000
          Width = 370.393940000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsEstqR2: TfrxDBDataset
    UserName = 'frxDsEstqR2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'OrdGrupSeq=OrdGrupSeq'
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'ValorT=ValorT'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_PALLET=NO_PALLET'
      'NO_TERCEIRO=NO_TERCEIRO'
      'AcumPecas=AcumPecas'
      'AcumPesoKg=AcumPesoKg'
      'AcumAreaM2=AcumAreaM2'
      'AcumAreaP2=AcumAreaP2'
      'AcumValorT=AcumValorT'
      'Ativo=Ativo'
      'NO_MovimID=NO_MovimID'
      'CustoM=CustoM'
      'AcumCustoM=AcumCustoM')
    DataSet = QrEstqR2
    BCDToCurrency = False
    Left = 200
    Top = 424
  end
  object QrSumIR2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DELETE FROM _wbmovimp2_ '
      '; '
      'INSERT INTO _wbmovimp2_ '
      'SELECT 1 OrdGrupSeq, '
      'wmi.Codigo, wmi.Controle, wmi.MovimCod, '
      'wmi.MovimNiv, wmi.Empresa, wmi.Terceiro, '
      'wmi.MovimID, wmi.DataHora, wmi.Pallet, '
      'wmi.GraGruX, wmi.Pecas, wmi.PesoKg, '
      'wmi.AreaM2, wmi.AreaP2, wmi.SrcMovID, '
      'wmi.SrcNivel1, wmi.SrcNivel2, '
      'wmi.SdoVrtPeca, wmi.SdoVrtArM2, '
      'wmi.Observ, 0 ValorT, ggx.GraGru1, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_PALLET, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, '
      '0 AcumPecas, 0 AcumPesoKg, 0 AcumAreaM2, '
      '0 AcumAreaP2, 0 AcumValorT, 1 Ativo '
      'FROM bluederm.wbmovits wmi '
      'LEFT JOIN bluederm.gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN bluederm.gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN bluederm.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN bluederm.gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN bluederm.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN bluederm.wbpallet   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN bluederm.entidades  ent ON ent.Codigo=wmi.Terceiro '
      'WHERE wmi.Empresa=-11'
      'AND wmi.GraGruX=3328'
      'AND wmi.DataHora  BETWEEN "2013-09-07" AND "2013-12-06 23:59:59"'
      '; '
      'SELECT * FROM _wbmovimp2_'
      'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; '
      ' ')
    Left = 136
    Top = 320
    object QrSumIR2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumIR2PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrSumIR2AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumIR2AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSumIR2ValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object Qr02Fornecedor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 348
    Top = 92
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object Ds02Fornecedor: TDataSource
    DataSet = Qr02Fornecedor
    Left = 348
    Top = 136
  end
  object Qr02WBPallet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wbpallet'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 416
    Top = 92
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds02WBPallet: TDataSource
    DataSet = Qr02WBPallet
    Left = 416
    Top = 136
  end
  object Qr02GraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 488
    Top = 92
    object IntegerField3: TIntegerField
      FieldName = 'GraGru1'
    end
    object IntegerField4: TIntegerField
      FieldName = 'Controle'
    end
    object StringField3: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object StringField4: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object IntegerField5: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object StringField5: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object Ds02GraGruX: TDataSource
    DataSet = Qr02GraGruX
    Left = 488
    Top = 136
  end
  object QrWBMovIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      
        ' ELT(wmi.MovimID + 1,"Ajuste","Compra","Venda","Reclasse","Baixa' +
        '") NO_MovimID,'
      'wbp.Nome NO_Pallet,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO'
      'FROM wbmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet'
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'WHERE wmi.Empresa=-11'
      'AND wmi.DataHora  BETWEEN "2013-09-09" AND "2013-12-08 23:59:59"'
      'ORDER BY wmi.DataHora, wmi.Pecas DESC; ')
    Left = 136
    Top = 380
    object QrWBMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wbmovits.Codigo'
    end
    object QrWBMovItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'wbmovits.Controle'
    end
    object QrWBMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Origin = 'wbmovits.MovimCod'
    end
    object QrWBMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Origin = 'wbmovits.MovimNiv'
    end
    object QrWBMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'wbmovits.Empresa'
    end
    object QrWBMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Origin = 'wbmovits.Terceiro'
    end
    object QrWBMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
      Origin = 'wbmovits.MovimID'
    end
    object QrWBMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'wbmovits.DataHora'
    end
    object QrWBMovItsPallet: TIntegerField
      FieldName = 'Pallet'
      Origin = 'wbmovits.Pallet'
      Required = True
    end
    object QrWBMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'wbmovits.GraGruX'
    end
    object QrWBMovItsPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'wbmovits.Pecas'
      DisplayFormat = '#,###,###.###;-#,###,###.###; '
    end
    object QrWBMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      Origin = 'wbmovits.PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrWBMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      Origin = 'wbmovits.AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrWBMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      Origin = 'wbmovits.AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrWBMovItsValorT: TFloatField
      FieldName = 'ValorT'
      Origin = 'wbmovits.ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrWBMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
      Origin = 'wbmovits.SrcMovID'
      DisplayFormat = '0;-0; '
    end
    object QrWBMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
      Origin = 'wbmovits.SrcNivel1'
      DisplayFormat = '0;-0; '
    end
    object QrWBMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Origin = 'wbmovits.SrcNivel2'
      DisplayFormat = '0;-0; '
    end
    object QrWBMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      Origin = 'wbmovits.SdoVrtPeca'
      DisplayFormat = '#,###,###.###;-#,###,###.###; '
    end
    object QrWBMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      Origin = 'wbmovits.SdoVrtArM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrWBMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Origin = 'wbmovits.Observ'
      Required = True
      Size = 255
    end
    object QrWBMovItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'wbmovits.Lk'
    end
    object QrWBMovItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'wbmovits.DataCad'
    end
    object QrWBMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'wbmovits.DataAlt'
    end
    object QrWBMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'wbmovits.UserCad'
    end
    object QrWBMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'wbmovits.UserAlt'
    end
    object QrWBMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'wbmovits.AlterWeb'
    end
    object QrWBMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'wbmovits.Ativo'
    end
    object QrWBMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Origin = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrWBMovItsNO_MovimID: TWideStringField
      DisplayWidth = 25
      FieldName = 'NO_MovimID'
      Origin = 'NO_MovimID'
      Size = 8
    end
    object QrWBMovItsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Origin = 'wbpallet.Nome'
      Size = 60
    end
    object QrWBMovItsNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Origin = 'NO_TERCEIRO'
      Size = 100
    end
  end
  object DsWBMovIts: TDataSource
    DataSet = QrWBMovIts
    Left = 136
    Top = 424
  end
  object QrEstqR3: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEstqR3AfterOpen
    BeforeClose = QrEstqR3BeforeClose
    OnCalcFields = QrEstqR2CalcFields
    SQL.Strings = (
      'DELETE FROM _wbmovimp2_ '
      '; '
      'INSERT INTO _wbmovimp2_ '
      'SELECT 1 OrdGrupSeq, '
      'wmi.Codigo, wmi.Controle, wmi.MovimCod, '
      'wmi.MovimNiv, wmi.Empresa, wmi.Terceiro, '
      'wmi.MovimID, wmi.DataHora, wmi.Pallet, '
      'wmi.GraGruX, wmi.Pecas, wmi.PesoKg, '
      'wmi.AreaM2, wmi.AreaP2, wmi.SrcMovID, '
      'wmi.SrcNivel1, wmi.SrcNivel2, '
      'wmi.SdoVrtPeca, wmi.SdoVrtArM2, '
      'wmi.Observ, 0 ValorT, ggx.GraGru1, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_PALLET, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, '
      '0 AcumPecas, 0 AcumPesoKg, 0 AcumAreaM2, '
      '0 AcumAreaP2, 0 AcumValorT, 1 Ativo '
      'FROM bluederm.wbmovits wmi '
      'LEFT JOIN bluederm.gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN bluederm.gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN bluederm.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN bluederm.gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN bluederm.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN bluederm.wbpallet   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN bluederm.entidades  ent ON ent.Codigo=wmi.Terceiro '
      'WHERE wmi.Empresa=-11'
      'AND wmi.GraGruX=3328'
      'AND wmi.DataHora  BETWEEN "2013-09-07" AND "2013-12-06 23:59:59"'
      '; '
      'SELECT * FROM _wbmovimp2_'
      'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; '
      ' ')
    Left = 268
    Top = 380
    object QrEstqR3Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstqR3GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstqR3Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,###,###;-#,###,###,###,###; '
    end
    object QrEstqR3PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR3AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR3AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR3GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstqR3NO_PRD_TAM_COR: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrEstqR3Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstqR3NO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrEstqR3Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrEstqR3NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrEstqR3ValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR3Ativo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrEstqR3OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
  end
  object frxWET_RECUR_004_03_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41608.425381412000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Imprime: Boolean;'
      '    '
      'procedure Line3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Line3.Visible := Imprime;                         '
      
        '  Imprime := not Imprime;                                       ' +
        '                                                                ' +
        '                  '
      'end;'
      ''
      'begin'
      '  Imprime := True;                                  '
      'end.')
    OnGetValue = frxWET_RECUR_004_00_AGetValue
    Left = 336
    Top = 336
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsGGXPalTer
        DataSetName = 'frxDsGGXPalTer'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 559.370078740000100000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        DataSet = frxDsGGXPalTer
        DataSetName = 'frxDsGGXPalTer'
        RowCount = 0
        object Shape1: TfrxShapeView
          Top = 37.897650000000000000
          Width = 680.315400000000000000
          Height = 483.779840000000000000
          Shape = skRoundRectangle
        end
        object Memo47: TfrxMemoView
          Left = 200.315090000000000000
          Top = 90.811070000000000000
          Width = 283.464750000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-Prima para Semi / Acabado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 18.897650000000000000
          Top = 90.811070000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 483.779840000000000000
          Top = 90.811070000000000000
          Width = 177.637846540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Item [Line#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 20.015770000000000000
          Top = 128.795300000000000000
          Width = 411.968330630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'PALLET  [frxDsGGXPalTer."Pallet"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 1.118120000000000000
          Top = 176.574830000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          Left = 1.118120000000000000
          Top = 173.015770000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          Top = 268.448828660000000000
          Width = 188.976377950000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 188.976377950000000000
          Top = 268.448828660000000000
          Width = 491.338582680000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Proced'#234'ncia')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 226.574830000000000000
          Width = 120.944881890000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-prima:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 340.157480310000000000
          Top = 362.937034720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Top = 362.937034720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 510.236220470000000000
          Top = 362.937034720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 170.078740160000000000
          Top = 362.937034720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 306.244106690000000000
          Width = 188.976377950000000000
          Height = 52.913395590000000000
          DataField = 'DataHora'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 188.976377950000000000
          Top = 306.244106690000000000
          Width = 491.338582680000000000
          Height = 52.913395590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Terceiro"] - [frxDsGGXPalTer."NO_TERCEIRO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 120.944881890000000000
          Top = 226.574830000000000000
          Width = 559.370078740000100000
          Height = 32.000000000000000000
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."GraGruX"] - [frxDsGGXPalTer."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 340.157480310000000000
          Top = 400.732312760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 400.732312760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsGGXPalTer."' +
              'Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 510.236220470000000000
          Top = 400.732312760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsGGXPalTer."' +
              'PesoKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 170.078740160000000000
          Top = 400.732312760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 120.944881890000000000
          Top = 180.795300000000000000
          Width = 559.370078740000100000
          Height = 40.000000000000000000
          DataField = 'NO_EMPRESA'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_EMPRESA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 180.795300000000000000
          Width = 120.944881890000000000
          Height = 40.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente Interno: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Top = 56.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 226.771800000000000000
          Top = 128.606370000000000000
          Width = 434.645510630000000000
          Height = 38.000000000000000000
          DataField = 'NO_PALLET'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PALLET"]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 559.370103150000000000
          Width = 680.315400000000000000
          OnBeforePrint = 'Line3OnBeforePrint'
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
      end
    end
  end
  object DsEstqR3: TDataSource
    DataSet = QrEstqR3
    Left = 268
    Top = 424
  end
  object QrGGXPalTer: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEstqR2CalcFields
    SQL.Strings = (
      'DELETE FROM _wbmovimp2_ '
      '; '
      'INSERT INTO _wbmovimp2_ '
      'SELECT 1 OrdGrupSeq, '
      'wmi.Codigo, wmi.Controle, wmi.MovimCod, '
      'wmi.MovimNiv, wmi.Empresa, wmi.Terceiro, '
      'wmi.MovimID, wmi.DataHora, wmi.Pallet, '
      'wmi.GraGruX, wmi.Pecas, wmi.PesoKg, '
      'wmi.AreaM2, wmi.AreaP2, wmi.SrcMovID, '
      'wmi.SrcNivel1, wmi.SrcNivel2, '
      'wmi.SdoVrtPeca, wmi.SdoVrtArM2, '
      'wmi.Observ, 0 ValorT, ggx.GraGru1, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_PALLET, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, '
      '0 AcumPecas, 0 AcumPesoKg, 0 AcumAreaM2, '
      '0 AcumAreaP2, 0 AcumValorT, 1 Ativo '
      'FROM bluederm.wbmovits wmi '
      'LEFT JOIN bluederm.gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN bluederm.gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN bluederm.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN bluederm.gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN bluederm.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN bluederm.wbpallet   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN bluederm.entidades  ent ON ent.Codigo=wmi.Terceiro '
      'WHERE wmi.Empresa=-11'
      'AND wmi.GraGruX=3328'
      'AND wmi.DataHora  BETWEEN "2013-09-07" AND "2013-12-06 23:59:59"'
      '; '
      'SELECT * FROM _wbmovimp2_'
      'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; '
      ' ')
    Left = 336
    Top = 380
    object QrGGXPalTerEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrGGXPalTerGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGGXPalTerPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrGGXPalTerPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrGGXPalTerAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGGXPalTerAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGGXPalTerGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXPalTerNO_PRD_TAM_COR: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrGGXPalTerTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrGGXPalTerNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrGGXPalTerPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrGGXPalTerNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrGGXPalTerValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGGXPalTerNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrGGXPalTerDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
  end
  object frxDsGGXPalTer: TfrxDBDataset
    UserName = 'frxDsGGXPalTer'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Empresa=Empresa'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Terceiro=Terceiro'
      'NO_TERCEIRO=NO_TERCEIRO'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'ValorT=ValorT'
      'NO_EMPRESA=NO_EMPRESA'
      'DataHora=DataHora')
    DataSet = QrGGXPalTer
    BCDToCurrency = False
    Left = 336
    Top = 424
  end
  object frxWET_RECUR_004_03_B: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxWET_RECUR_004_00_AGetValue
    Left = 336
    Top = 292
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsGGXPalTer
        DataSetName = 'frxDsGGXPalTer'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Shape3: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 971.339210000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 759.685530000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque de Mat'#233'ria-Prima para o Recurtimento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 865.512370000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 423.307360000000000000
          Top = 64.252010000000000000
          Width = 170.078740160000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 374.173470000000000000
          Top = 64.252010000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          Left = 895.748610000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaP2: TfrxMemoView
          Left = 820.158010000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 744.567410000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 668.976810000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitValorT: TfrxMemoView
          Left = 593.386210000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 49.133890000000000000
          Top = 64.252010000000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Top = 64.252010000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 230.551330000000000000
          Top = 64.252010000000000000
          Width = 143.622054570000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Fornecedor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 181.417440000000000000
          Top = 64.252010000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 158.740260000000000000
        Width = 971.339210000000000000
        DataSet = frxDsGGXPalTer
        DataSetName = 'frxDsGGXPalTer'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 423.307360000000000000
          Width = 170.078740160000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 374.173470000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 895.748610000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreap2: TfrxMemoView
          Left = 820.158010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 744.567410000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 668.976810000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValValorT: TfrxMemoView
          Left = 593.386210000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 49.133890000000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataField = 'NO_PALLET'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PALLET"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'Pallet'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 230.551330000000000000
          Width = 143.622054570000000000
          Height = 15.118110240000000000
          DataField = 'NO_TERCEIRO'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_TERCEIRO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 181.417440000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'Terceiro'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Terceiro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 279.685220000000000000
        Width = 971.339210000000000000
        object Memo93: TfrxMemoView
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 650.079160000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 234.330860000000000000
        Width = 971.339210000000000000
        object Me_FTT: TfrxMemoView
          Top = 3.779529999999994000
          Width = 593.386014720000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Soma dos itens selecionados: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 895.748610000000000000
          Top = 3.779529999999994000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaP2: TfrxMemoView
          Left = 820.158010000000000000
          Top = 3.779529999999994000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."AreaP2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaM2: TfrxMemoView
          Left = 744.567410000000000000
          Top = 3.779529999999994000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTPesoKg: TfrxMemoView
          Left = 668.976810000000000000
          Top = 3.779529999999994000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTValorT: TfrxMemoView
          Left = 593.386210000000000000
          Top = 3.779529999999994000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."ValorT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object PM03Imprime: TPopupMenu
    Left = 524
    Top = 212
    object Fichas1: TMenuItem
      Caption = '&Fichas'
      OnClick = Fichas1Click
    end
    object Lista1: TMenuItem
      Caption = '&Lista'
      OnClick = Lista1Click
    end
  end
end
