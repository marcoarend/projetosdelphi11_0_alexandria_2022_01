unit FluxosItsMul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables;

type
  THackDBGrid = class(TDBGrid);
  TFmFluxosItsMul = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    DBGFluxosIts: TDBGrid;
    TbFluxosIts: TmySQLTable;
    TbFluxosItsCodigo: TIntegerField;
    TbFluxosItsControle: TIntegerField;
    TbFluxosItsOrdem: TIntegerField;
    TbFluxosItsOperacao: TIntegerField;
    TbFluxosItsLk: TIntegerField;
    TbFluxosItsDataCad: TDateField;
    TbFluxosItsDataAlt: TDateField;
    TbFluxosItsUserCad: TIntegerField;
    TbFluxosItsUserAlt: TIntegerField;
    TbFluxosItsSEQ: TIntegerField;
    TbFluxosItsAcao1: TWideStringField;
    TbFluxosItsAcao2: TWideStringField;
    TbFluxosItsAcao3: TWideStringField;
    TbFluxosItsAcao4: TWideStringField;
    DsFluxosIts: TDataSource;
    QrOperacoes: TmySQLQuery;
    QrOperacoesCodigo: TIntegerField;
    QrOperacoesNome: TWideStringField;
    TbFluxosItsNOMEOPERACAO: TWideStringField;
    LaAviso3: TLabel;
    LaAviso4: TLabel;
    BtReordena: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TbFluxosItsCalcFields(DataSet: TDataSet);
    procedure DBGFluxosItsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TbFluxosItsBeforePost(DataSet: TDataSet);
    procedure BtReordenaClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenFluxosIts();
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmFluxosItsMul: TFmFluxosItsMul;

implementation

uses UnMyObjects, Module, DmkDAC_PF, MeuDBUses, Principal, UMySQLModule,
  UnReordena, UnVS_PF;

{$R *.DFM}

procedure TFmFluxosItsMul.BtReordenaClick(Sender: TObject);
begin
  if (TbFluxosIts.State <> dsInactive) and (TbFluxosIts.RecordCount > 0) then
  begin
    UReordena.ReordenaItens(TMySQLQuery(TbFluxosIts), Dmod.QrUpd, 'fluxosits',
      'Ordem', 'Controle', 'NOMEOPERACAO', '', '', '', nil);
    //
    UnDmkDAC_PF.AbreTable(TbFluxosIts, Dmod.MyDB);
  end;
end;

procedure TFmFluxosItsMul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFluxosItsMul.DBGFluxosItsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Campo: String;
begin
  if (TbFluxosIts.State <> dsInactive) then
  begin
    Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    //
    if UpperCase(Campo) = UpperCase('NOMEOPERACAO') then
    begin
      if key = VK_F4 then
      begin
        VAR_CADASTRO := 0;
        VS_PF.MostraFormOperacoes(TbFluxosItsOperacao.Value);
        //
        if VAR_CADASTRO <> 0 then
        begin
          Screen.Cursor := crHourGlass;
          try
            QrOperacoes.Close;
            UnDmkDAC_PF.AbreQuery(QrOperacoes, Dmod.MyDB);
            //
            TbFluxosIts.Edit;
            TbFluxosItsOperacao.Value := VAR_CADASTRO;
            TbFluxosIts.Post;
          finally
            Screen.Cursor := crDefault;
          end;
        end;
      end else
      if key = VK_F7 then
      begin
        FmMeuDBUses.PesquisaNome('operacoes', 'Nome', 'Codigo', '');
        //
        if VAR_CADASTRO <> 0 then
        begin
          Screen.Cursor := crHourGlass;
          try
            TbFluxosIts.Edit;
            TbFluxosItsOperacao.Value := VAR_CADASTRO;
            TbFluxosIts.Post;
          finally
            Screen.Cursor := crDefault;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFmFluxosItsMul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFluxosItsMul.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmFluxosItsMul.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFluxosItsMul.FormShow(Sender: TObject);
begin
  ReopenFluxosIts();
end;

procedure TFmFluxosItsMul.ReopenFluxosIts;
begin
  TbFluxosIts.Close;
  //
  if FCodigo <> 0 then
  begin
    DBGFluxosIts.Visible := True;
    //
    UnDmkDAC_PF.AbreQuery(QrOperacoes, Dmod.MyDB);
    //
    TbFluxosIts.Filtered := True;
    TbFluxosIts.Filter   := 'Codigo=' + Geral.FF0(FCodigo);
    UnDmkDAC_PF.AbreTable(TbFluxosIts, Dmod.MyDB);
  end else
  begin
    Geral.MB_Erro('C�digo n�o informado!');
    DBGFluxosIts.Visible := False;
  end;
end;

procedure TFmFluxosItsMul.TbFluxosItsBeforePost(DataSet: TDataSet);
begin
  TbFluxosItsCodigo.Value   := FCodigo;
  TbFluxosItsControle.Value := UMyMod.BuscaEmLivreY_Def('fluxosits', 'Controle', stIns, 0);
  TbFluxosItsOrdem.Value    := UReordena.VerificaOrdem(Dmod.QrAux, Dmod.MyDB,
                                 'Codigo', 'Ordem', 'fluxosits', FCodigo, 0, True);
end;

procedure TFmFluxosItsMul.TbFluxosItsCalcFields(DataSet: TDataSet);
begin
  TbFluxosItsSEQ.Value := TbFluxosIts.RecNo;
end;

end.
