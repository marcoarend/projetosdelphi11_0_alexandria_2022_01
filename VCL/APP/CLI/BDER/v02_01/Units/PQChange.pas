unit PQChange;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, Grids, DBGrids,
  DBCtrls, Menus, Variants, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral,
  dmkLabel, dmkImage, UnDmkEnums;

type
  TFmPQChange = class(TForm)
    PainelDados: TPanel;
    DBGrid1: TDBGrid;
    QrBaixa: TmySQLQuery;
    QrBaixaCodigo: TIntegerField;
    QrBaixaControle: TIntegerField;
    QrBaixaNomePQ: TWideStringField;
    QrBaixaAtivo: TSmallintField;
    QrBaixaPQCI: TIntegerField;
    QrBaixaCustoPadrao: TFloatField;
    QrBaixaMoedaPadrao: TIntegerField;
    QrBaixaNumero: TIntegerField;
    QrBaixaPorcent: TFloatField;
    QrBaixaProduto: TIntegerField;
    QrBaixaTempoR: TIntegerField;
    QrBaixaTempoP: TIntegerField;
    QrBaixapH: TFloatField;
    QrBaixaBe: TFloatField;
    QrBaixaObs: TWideStringField;
    QrBaixaCusto: TFloatField;
    QrBaixaGraus: TFloatField;
    QrBaixaBoca: TWideStringField;
    QrBaixaProcesso: TWideStringField;
    QrBaixaObsProces: TWideStringField;
    QrBaixaLk: TIntegerField;
    QrBaixaDataCad: TDateField;
    QrBaixaDataAlt: TDateField;
    QrBaixaUserCad: TIntegerField;
    QrBaixaUserAlt: TIntegerField;
    QrBaixaOrdem: TIntegerField;
    QrBaixaPeso_PQ: TFloatField;
    QrBaixaProdutoCI: TIntegerField;
    DsBaixa: TDataSource;
    QrBaixaNOMECLI_ORIG: TWideStringField;
    QrBaixaCli_Orig: TIntegerField;
    QrBaixaCli_Dest: TIntegerField;
    QrPreuso: TmySQLQuery;
    QrPreusoPeso_PQ: TFloatField;
    QrPreusoProduto: TIntegerField;
    QrPreusoPQCI: TIntegerField;
    QrPreusoProdutoCI: TIntegerField;
    QrPreusoNomePQ: TWideStringField;
    QrPreusoPeso: TFloatField;
    QrPreusoPESOFUTURO: TFloatField;
    QrPreusoCli_Orig: TIntegerField;
    QrPreusoNOMECLI_ORIG: TWideStringField;
    QrBaixaPESOFUTURO: TFloatField;
    QrEntiCli2: TmySQLQuery;
    DsEntiCli2: TDataSource;
    QrEntiCli2Codigo: TIntegerField;
    QrEntiCli2NOMECLI: TWideStringField;
    QrEntiCli2Peso: TFloatField;
    QrEntiCli2Valor: TFloatField;
    QrEntiCli2PRODUTOCI: TIntegerField;
    PMEmpresta: TPopupMenu;
    Insumoatual1: TMenuItem;
    Todospendentes1: TMenuItem;
    QrPesq: TmySQLQuery;
    QrPesqPeso: TFloatField;
    QrPesqValor: TFloatField;
    QrPesqPRODUTOCI: TIntegerField;
    QrPesqCI: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBEmpresta: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    Label36: TLabel;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    BtConfirmaE: TBitBtn;
    GBConfirma: TGroupBox;
    Panel3: TPanel;
    Panel5: TPanel;
    BtBaixa: TBitBtn;
    BitBtn1: TBitBtn;
    BtEmpresta: TBitBtn;
    BitBtn3: TBitBtn;
    BtSaida: TBitBtn;
    BitBtn4: TBitBtn;
    QrBaixaDiluicao: TFloatField;
    QrBaixaMinimo: TFloatField;
    QrBaixaMaximo: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure BtEmprestaClick(Sender: TObject);
    procedure BtConfirmaEClick(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEntiCli2AfterOpen(DataSet: TDataSet);
    procedure Insumoatual1Click(Sender: TObject);
    procedure Todospendentes1Click(Sender: TObject);
    procedure BtBaixaClick(Sender: TObject);
    procedure QrBaixaAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FUnico: Boolean;
    FCliOrigem, FCliDest, FBaixaControle: Integer;
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
  public
    { Public declarations }
    FCancela: Boolean;
    FAllDest, FEmit, FJanela: Integer;
    procedure ReopenBaixa();
    procedure ReopenEntiCli2();
    procedure ReopenPreUso();
  end;

  var
  FmPQChange: TFmPQChange;

implementation

uses UnMyObjects, Module, FormulasImpShow, FormulasImpShowNew, MyVCLSkin, UnInternalConsts, UnGOTOy,
DmkDAC_PF;

{$R *.DFM}

procedure TFmPQChange.ReopenBaixa();
begin
  ReopenPreUso;
  UnDmkDAC_PF.AbreMySQLQuery0(QrBaixa, Dmod.MyDB, [
  'SELECT CASE WHEN ori.Tipo=0 THEN ori.RazaoSocial ',
  'ELSE ori.Nome END NOMECLI_ORIG, ems.* ',
  'FROM emitits ems ',
  'LEFT JOIN entidades ori ON ori.Codigo=ems.Cli_Orig ',
  'WHERE ems.Ativo=2 ',
  'AND ems.Codigo=' + Geral.FF0(FEmit),
  '']);
  //
  if FBaixaControle <> 0 then QrBaixa.Locate('Controle', FBaixaControle, []);
end;

procedure TFmPQChange.ReopenEntiCli2;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiCli2, Dmod.MyDB, [
  'SELECT pqc.Peso, pqc.Valor, ent.Codigo, ',
  'pqc.Controle PRODUTOCI, ',
  'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
  'ELSE ent.Nome END NOMECLI ',
  'FROM entidades ent ',
  'LEFT JOIN pqcli pqc ON pqc.CI=ent.Codigo ',
  'WHERE pqc.PQ=' + Geral.FF0(QrBaixaProduto.Value),
  'AND pqc.Peso>=' + Geral.FFN_Dot(QrBaixaPeso_PQ.Value),
  'ORDER BY NOMECLI ',
  '']);
end;

procedure TFmPQChange.ReopenPreUso();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPreUso, Dmod.MyDB, [
  'SELECT SUM(ems.Peso_PQ) Peso_PQ, ems.Produto, ems.PQCI, ',
  'ems.ProdutoCI, ems.NomePQ, pqc.Peso, ems.Cli_Orig, ',
  'IF(pqc.Peso IS NULL, 0, pqc.Peso) - SUM(ems.Peso_PQ) PESOFUTURO, ',
  'CASE WHEN cli.Tipo=0 ',
  'THEN cli.RazaoSocial ELSE cli.Nome END NOMECLI_ORIG ',
  'FROM emitits ems ',
  'LEFT JOIN pqcli     pqc ON pqc.Controle=ems.ProdutoCI ',
  'LEFT JOIN entidades cli ON cli.Codigo=ems.Cli_Orig ',
  'WHERE ems.Ativo=2 ',
  'AND ems.Codigo=' + Geral.FF0(FEmit),
  'GROUP BY ems.Produto, Cli_Orig' +
  ' ']);
end;

procedure TFmPQChange.BtSaidaClick(Sender: TObject);
begin
  frFormulasImpShow.FCancela := True;
  FCancela := True;
  Close;
end;

procedure TFmPQChange.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQChange.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQChange.FormShow(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmPQChange.BitBtn1Click(Sender: TObject);
begin
  case FJanela of
    0:
    begin
      frFormulasImpShow.ReopenPreUso(FEmit);
      MyObjects.frxMostra(frFormulasImpShow.frxPreUso, 'Pr� uso');
    end;
    1:
    begin
      frFormulasImpShowNew.ReopenPreUso(FEmit);
      MyObjects.frxMostra(frFormulasImpShowNew.frxPreUso, 'Pr� uso');
    end;
  end;
end;

procedure TFmPQChange.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CorTexto, CorFundo: Integer;
begin
  if QrBaixaPESOFUTURO.Value < 0 then
  begin
    CorTexto := clRed;
    CorFundo := $00E4E4E4;
  end else begin
    CorTexto := clNavy;
    CorFundo := clWhite;
  end;
  MyObjects.DesenhaTextoEmDBGrid(DBGrid1, Rect, CorTexto, CorFundo,
    Column.Alignment, Column.Field.DisplayText);
end;

procedure TFmPQChange.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FJanela := 0;
  //
  frFormulasImpShow.FCancela := True;
  FCancela := True;
  ReopenEntiCli2();
end;

procedure TFmPQChange.BtEmprestaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEmpresta, BtEmpresta);
end;

(* ini 2023-10-06
procedure TFmPQChange.BtConfirmaEClick(Sender: TObject);
begin
  FCliOrigem := Geral.IMV(EdCI.Text);
  if FCliOrigem = 0 then
  begin
    Application.MessageBox('Informe o cliente interno cedente do insumo!',
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if FUnico then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE EmitIts SET ProdutoCI=:P0, Cli_Orig=:P1');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
    Dmod.QrUpd.Params[0].AsInteger := QrEntiCli2PRODUTOCI.Value;
    Dmod.QrUpd.Params[1].AsInteger := FCliOrigem;
    Dmod.QrUpd.Params[2].AsInteger := QrBaixaControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenBaixa;
  end else begin
    while not QrBaixa.Eof do
    begin
      if QrBaixaPESOFUTURO.Value < 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
        'SELECT pqc.Peso, pqc.Valor, pqc.Controle PRODUTOCI, pqc.CI ',
        'FROM pqcli pqc ',
        'WHERE pqc.PQ=' + Geral.FF0(QrBaixaProduto.Value),
        'AND pqc.Peso>=' + Geral.FFN_Dot(QrBaixaPeso_PQ.Value),
        'AND pqc.CI=' + Geral.FF0(FCliOrigem),
        '']);
        if QrPesq.RecordCount > 0 then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE EmitIts SET ProdutoCI=:P0, Cli_Orig=:P1');
          Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
          Dmod.QrUpd.Params[0].AsInteger := QrPesqPRODUTOCI.Value;
          Dmod.QrUpd.Params[1].AsInteger := FCliOrigem;
          Dmod.QrUpd.Params[2].AsInteger := QrBaixaControle.Value;
          Dmod.QrUpd.ExecSQL;
          FBaixaControle := QrBaixaControle.Value;
          ReopenBaixa;
        end;
      end;
      QrBaixa.Next;
    end;
  end;  
  MostraEdicao(0, stLok, QrBaixaControle.Value);
end;
*)
procedure TFmPQChange.BtConfirmaEClick(Sender: TObject);
  procedure MudaAtual();
  begin
    if QrBaixaPESOFUTURO.Value < 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
      'SELECT pqc.Peso, pqc.Valor, pqc.Controle PRODUTOCI, pqc.CI ',
      'FROM pqcli pqc ',
      'WHERE pqc.PQ=' + Geral.FF0(QrBaixaProduto.Value),
      'AND pqc.Peso>=' + Geral.FFN_Dot(QrBaixaPeso_PQ.Value),
      'AND pqc.CI=' + Geral.FF0(FCliOrigem),
      '']);
      if QrPesq.RecordCount > 0 then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE EmitIts SET ProdutoCI=:P0, Cli_Orig=:P1');
        Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
        Dmod.QrUpd.Params[0].AsInteger := QrPesqPRODUTOCI.Value;
        Dmod.QrUpd.Params[1].AsInteger := FCliOrigem;
        Dmod.QrUpd.Params[2].AsInteger := QrBaixaControle.Value;
        Dmod.QrUpd.ExecSQL;
        FBaixaControle := QrBaixaControle.Value;
      end;
    end;
  end;
begin
  FCliOrigem := Geral.IMV(EdCI.Text);
  if FCliOrigem = 0 then
  begin
    Application.MessageBox('Informe o cliente interno cedente do insumo!',
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  try
    if FUnico then
    begin
      MudaAtual();
    end else begin
      while not QrBaixa.Eof do
      begin
        if QrBaixaPESOFUTURO.Value < 0 then
          MudaAtual();
        QrBaixa.Next;
      end;
    end;
  finally
    ReopenBaixa;
    MostraEdicao(0, stLok, QrBaixaControle.Value);
  end;
end;
// fim 2023-10-09

procedure TFmPQChange.BitBtn4Click(Sender: TObject);
begin
  FCliOrigem := 0;
  MostraEdicao(0, stLok, 0);
end;

procedure TFmPQChange.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQChange.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      GBConfirma.Visible := True;
      GBEmpresta.Visible := False;
      //
      DBGrid1.Enabled    := True;
    end;
    1:
    begin
      GBEmpresta.Visible := True;
      GBConfirma.Visible := False;
      DBGrid1.Enabled := False;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then
  begin
    FBaixaControle := Codigo;
    ReopenBaixa;
  end;
end;

procedure TFmPQChange.QrEntiCli2AfterOpen(DataSet: TDataSet);
begin
  if not QrEntiCli2.Locate('Codigo', Geral.IMV(EdCI.Text), []) then
  begin
    EdCI.Text     := '';
    CBCI.KeyValue := NULL;
  end;
end;

procedure TFmPQChange.Insumoatual1Click(Sender: TObject);
begin
  FUnico := True;
  ReopenEntiCli2();
  MostraEdicao(1, stUpd, 0);
end;

procedure TFmPQChange.Todospendentes1Click(Sender: TObject);
begin
  FUnico := False;
  QrBaixa.First;
  while not QrBaixa.Eof do
  begin
    if QrBaixaPESOFUTURO.Value < 0 then
    begin
      ReopenEntiCli2();
      MostraEdicao(1, stUpd, 0);
      Break;
    end;
    QrBaixa.Next;
  end;
end;

procedure TFmPQChange.BtBaixaClick(Sender: TObject);
begin
  frFormulasImpShow.FCancela := False;
  FCancela := False;
  Close;
end;

procedure TFmPQChange.QrBaixaAfterOpen(DataSet: TDataSet);
var
  NotLibera: Integer;
begin
  NotLibera := 0;
  FAllDest  := 0;
  QrBaixa.DisableControls;
  QrBaixa.First;
  while not QrBaixa.Eof do
  begin
    if QrBaixaPESOFUTURO.Value < 0 then
      NotLibera := NotLibera + 1;
    //
    if QrBaixaCli_Dest.Value > 0 then
      FAllDest := FAllDest + 1;
    //
    QrBaixa.Next;
  end;
  QrBaixa.EnableControls;
  //
  if FAllDest <> QrBaixa.RecordCount then
    FAllDest := 0;
  BtBaixa.Enabled := (FAllDest > 0) or (NotLibera = 0);
end;

end.
