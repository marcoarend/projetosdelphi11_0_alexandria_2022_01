object FmPQPedEdit: TFmPQPedEdit
  Left = 290
  Top = 206
  Caption = 'QUI-PEDID-003 :: Item de Pedido de PQ'
  ClientHeight = 422
  ClientWidth = 652
  Color = clBtnFace
  Constraints.MaxHeight = 535
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 652
    Height = 266
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PainelTitulo2: TPanel
      Left = 0
      Top = 0
      Width = 652
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 8
        Top = 16
        Width = 29
        Height = 13
        Caption = 'Linha:'
      end
      object ImgTipo: TdmkLabel
        Left = 574
        Top = 0
        Width = 78
        Height = 41
        Align = alRight
        Alignment = taCenter
        AutoSize = False
        Caption = 'Travado'
        Font.Charset = ANSI_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        UpdType = utYes
        SQLType = stNil
        ExplicitLeft = 581
        ExplicitTop = 1
        ExplicitHeight = 39
      end
      object EdCodigo: TdmkEdit
        Left = 60
        Top = 12
        Width = 57
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '1'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
      end
    end
    object PainelDados: TPanel
      Left = 0
      Top = 41
      Width = 652
      Height = 225
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 113
        Height = 13
        Caption = 'Produto: C'#243'digo - Nome'
      end
      object Label14: TLabel
        Left = 516
        Top = 8
        Width = 43
        Height = 13
        Caption = 'Volumes:'
      end
      object Label4: TLabel
        Left = 576
        Top = 8
        Width = 60
        Height = 13
        Caption = 'kg Bruto vol:'
      end
      object Label15: TLabel
        Left = 8
        Top = 92
        Width = 52
        Height = 13
        Caption = 'kg Liq. vol:'
      end
      object Label3: TLabel
        Left = 156
        Top = 92
        Width = 29
        Height = 13
        Caption = 'ICMS:'
      end
      object Label16: TLabel
        Left = 206
        Top = 92
        Width = 16
        Height = 13
        Caption = 'IPI:'
      end
      object Label7: TLabel
        Left = 250
        Top = 92
        Width = 45
        Height = 13
        Caption = 'C.Financ.'
      end
      object Label5: TLabel
        Left = 312
        Top = 92
        Width = 42
        Height = 13
        Caption = 'Valor kg:'
      end
      object Label18: TLabel
        Left = 394
        Top = 92
        Width = 69
        Height = 13
        Caption = 'Total kg bruto:'
      end
      object Label19: TLabel
        Left = 479
        Top = 92
        Width = 60
        Height = 13
        Caption = 'Total kg l'#237'q.:'
      end
      object Label10: TLabel
        Left = 556
        Top = 92
        Width = 59
        Height = 13
        Caption = 'Vlr tot. $/kg:'
      end
      object Label6: TLabel
        Left = 76
        Top = 136
        Width = 30
        Height = 13
        Caption = 'Prazo:'
      end
      object Label8: TLabel
        Left = 8
        Top = 136
        Width = 62
        Height = 13
        Caption = 'dd Consumo:'
      end
      object Label13: TLabel
        Left = 460
        Top = 50
        Width = 36
        Height = 13
        Caption = '?? [F4]:'
      end
      object Label17: TLabel
        Left = 12
        Top = 184
        Width = 233
        Height = 13
        Caption = 'Obs.: Dados tirados da '#250'ltima cota'#231#227'o de pre'#231'os.'
      end
      object Label12: TLabel
        Left = 8
        Top = 50
        Width = 48
        Height = 13
        Caption = 'Pre'#231'o/kg:'
      end
      object Label9: TLabel
        Left = 268
        Top = 50
        Width = 49
        Height = 13
        Caption = 'D'#243'lar [F4]:'
      end
      object Label11: TLabel
        Left = 364
        Top = 50
        Width = 46
        Height = 13
        Caption = 'Euro [F4]:'
      end
      object Label20: TLabel
        Left = 556
        Top = 50
        Width = 26
        Height = 13
        Caption = '$/kg:'
      end
      object EdProduto: TdmkEditCB
        Left = 8
        Top = 24
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBProduto
        IgnoraDBLookupComboBox = False
      end
      object CBProduto: TdmkDBLookupComboBox
        Left = 76
        Top = 24
        Width = 437
        Height = 21
        KeyField = 'PQ'
        ListField = 'NOMEPQ'
        ListSource = DsPQ1
        TabOrder = 1
        dmkEditCB = EdProduto
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdVolumes: TdmkEdit
        Left = 516
        Top = 24
        Width = 57
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdVolumesChange
        OnExit = EdVolumesExit
      end
      object EdKgBruto: TdmkEdit
        Left = 576
        Top = 24
        Width = 70
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdKgBrutoChange
        OnExit = EdKgBrutoExit
      end
      object EdkgLiq: TdmkEdit
        Left = 8
        Top = 108
        Width = 70
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdkgLiqChange
        OnExit = EdkgLiqExit
      end
      object BtCalcula: TBitBtn
        Left = 80
        Top = 95
        Width = 73
        Height = 40
        Caption = '&Calc'
        NumGlyphs = 2
        TabOrder = 11
        TabStop = False
        Visible = False
        OnClick = BtCalculaClick
      end
      object EdICMS: TdmkEdit
        Left = 156
        Top = 108
        Width = 42
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdIPIChange
        OnExit = EdICMSExit
      end
      object EdIPI: TdmkEdit
        Left = 204
        Top = 108
        Width = 42
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdIPIChange
        OnExit = EdIPIExit
      end
      object EdCFin: TdmkEdit
        Left = 250
        Top = 108
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdCFinChange
        OnExit = EdCFinExit
      end
      object EdValorkg: TdmkEdit
        Left = 312
        Top = 108
        Width = 77
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 15
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdTotalkgBruto: TdmkEdit
        Left = 394
        Top = 108
        Width = 83
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 16
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdTotalkgLiq: TdmkEdit
        Left = 479
        Top = 108
        Width = 74
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 17
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdTotalItem: TdmkEdit
        Left = 556
        Top = 108
        Width = 90
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 18
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPrazo: TEdit
        Left = 76
        Top = 152
        Width = 569
        Height = 21
        Color = clWhite
        TabOrder = 20
      end
      object EdConsumodd: TdmkEdit
        Left = 8
        Top = 152
        Width = 65
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 19
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '15'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 15
        ValWarn = False
        OnChange = EdVolumesChange
        OnExit = EdVolumesExit
      end
      object RGMoeda: TRadioGroup
        Left = 104
        Top = 49
        Width = 157
        Height = 39
        Caption = ' Moeda: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'R$ '
          'U$'
          #8364
          '??')
        TabOrder = 5
        OnClick = RGMoedaClick
      end
      object EdIdx: TdmkEdit
        Left = 460
        Top = 66
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdPrecoChange
        OnExit = EdIdxExit
        OnKeyDown = EdIdxKeyDown
      end
      object EdPreco: TdmkEdit
        Left = 8
        Top = 66
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdPrecoChange
      end
      object EdDolar: TdmkEdit
        Left = 268
        Top = 66
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdPrecoChange
        OnKeyDown = EdDolarKeyDown
      end
      object EdEuro: TdmkEdit
        Left = 364
        Top = 66
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdPrecoChange
        OnKeyDown = EdEuroKeyDown
      end
      object EdReais: TdmkEdit
        Left = 556
        Top = 66
        Width = 90
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 652
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 604
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object dmkImage1: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 556
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 266
        Height = 32
        Caption = 'Item de Pedido de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 266
        Height = 32
        Caption = 'Item de Pedido de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 266
        Height = 32
        Caption = 'Item de Pedido de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 314
    Width = 652
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 648
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 358
    Width = 652
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 648
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 504
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
      object BtSoma: TBitBtn
        Left = 132
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Arredonda'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
        OnClick = BtSomaClick
      end
      object BtRefresh: TBitBtn
        Tag = 18
        Left = 252
        Top = 4
        Width = 153
        Height = 40
        Caption = 'Atualizar &defini'#231#245'es'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        Visible = False
        OnClick = BtRefreshClick
      end
    end
  end
  object QrPQ1: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPQ1AfterScroll
    OnCalcFields = QrPQ1CalcFields
    SQL.Strings = (
      'SELECT pcl.Peso, pq.Nome NOMEPQ, pfo.* '
      'FROM pqfor pfo'
      'LEFT JOIN pq pq ON pfo.PQ=pq.Codigo'
      'LEFT JOIN pqcli pcl ON pcl.PQ=pq.Codigo AND pcl.CI=pfo.CI'
      'WHERE pfo.CI=:P0'
      'AND pfo.IQ=:P1'
      'ORDER BY pq.Nome')
    Left = 156
    Top = 46
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPQ1NOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQ1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQ1IQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQ1PQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrPQ1CI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQ1Prazo: TWideStringField
      FieldName = 'Prazo'
    end
    object QrPQ1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQ1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQ1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQ1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQ1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQ1DDESTQ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DDESTQ'
      Calculated = True
    end
    object QrPQ1Peso: TFloatField
      FieldName = 'Peso'
    end
  end
  object DsPQ1: TDataSource
    DataSet = QrPQ1
    Left = 184
    Top = 46
  end
  object QrPQ2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emb.Nome NOMEEMBLAGEM, emb.Bruto, emb.Liqui, cot.*'
      'FROM pqcot cot'
      'LEFT JOIN embalagens emb ON emb.Codigo=cot.Embalagem'
      'WHERE cot.PQ=:P0'
      'AND cot.CI=:P1'
      'AND cot.IQ=:P2'
      'ORDER BY cot.DataCot DESC')
    Left = 212
    Top = 48
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPQ2NOMEEMBLAGEM: TWideStringField
      FieldName = 'NOMEEMBLAGEM'
    end
    object QrPQ2Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQ2IQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQ2PQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrPQ2CI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQ2Embalagem: TIntegerField
      FieldName = 'Embalagem'
    end
    object QrPQ2DataCot: TDateField
      FieldName = 'DataCot'
    end
    object QrPQ2Preco: TFloatField
      FieldName = 'Preco'
    end
    object QrPQ2Moeda: TSmallintField
      FieldName = 'Moeda'
      Required = True
    end
    object QrPQ2Frete: TFloatField
      FieldName = 'Frete'
    end
    object QrPQ2ICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrPQ2IPI: TFloatField
      FieldName = 'IPI'
    end
    object QrPQ2PIS: TFloatField
      FieldName = 'PIS'
    end
    object QrPQ2COFINS: TFloatField
      FieldName = 'COFINS'
    end
    object QrPQ2RICMS: TFloatField
      FieldName = 'RICMS'
    end
    object QrPQ2RIPI: TFloatField
      FieldName = 'RIPI'
    end
    object QrPQ2RPIS: TFloatField
      FieldName = 'RPIS'
    end
    object QrPQ2RCOFINS: TFloatField
      FieldName = 'RCOFINS'
    end
    object QrPQ2Custo: TFloatField
      FieldName = 'Custo'
    end
    object QrPQ2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQ2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQ2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQ2UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQ2UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQ2Bruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrPQ2Liqui: TFloatField
      FieldName = 'Liqui'
    end
  end
  object QrUpdPQ: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'UPDATE pq SET'
      'Preco=:P0,'
      'PBEmb=:P1,'
      'PLEmb=:P2,'
      'ICMS=:P3,'
      'IPI=:P4,'
      'Valor=:P5,'
      'Prazo=:P6,'
      'CFin=:P7'
      'WHERE Codigo=:P8')
    Left = 196
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P8'
        ParamType = ptUnknown
      end>
  end
  object QrPQ3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pq WHERE Codigo=:P0')
    Left = 256
    Top = 196
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQ3Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrPQ3Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.pq.Nome'
      Size = 128
    end
    object QrPQ3IQ: TIntegerField
      FieldName = 'IQ'
      Origin = 'DBMBWET.pq.IQ'
    end
    object QrPQ3Preco: TFloatField
      FieldName = 'Preco'
      Origin = 'DBMBWET.pq.Preco'
    end
    object QrPQ3ICMS: TFloatField
      FieldName = 'ICMS'
      Origin = 'DBMBWET.pq.ICMS'
    end
    object QrPQ3Frete: TFloatField
      FieldName = 'Frete'
      Origin = 'DBMBWET.pq.Frete'
    end
    object QrPQ3IPI: TFloatField
      FieldName = 'IPI'
      Origin = 'DBMBWET.pq.IPI'
    end
    object QrPQ3RIPI: TFloatField
      FieldName = 'RIPI'
      Origin = 'DBMBWET.pq.RIPI'
    end
    object QrPQ3Prazo: TWideStringField
      FieldName = 'Prazo'
      Origin = 'DBMBWET.pq.Prazo'
      Size = 128
    end
    object QrPQ3Corante: TWideStringField
      FieldName = 'Corante'
      Origin = 'DBMBWET.pq.Corante'
      FixedChar = True
      Size = 128
    end
    object QrPQ3Pigmento: TWideStringField
      FieldName = 'Pigmento'
      Origin = 'DBMBWET.pq.Pigmento'
      FixedChar = True
      Size = 128
    end
    object QrPQ3Desperdicio: TFloatField
      FieldName = 'Desperdicio'
      Origin = 'DBMBWET.pq.Desperdicio'
    end
    object QrPQ3ClienteI: TIntegerField
      FieldName = 'ClienteI'
      Origin = 'DBMBWET.pq.ClienteI'
    end
    object QrPQ3EstqPeso: TFloatField
      FieldName = 'EstqPeso'
      Origin = 'DBMBWET.pq.EstqPeso'
    end
    object QrPQ3Valor: TFloatField
      FieldName = 'Valor'
      Origin = 'DBMBWET.pq.Valor'
    end
    object QrPQ3EstqValor: TFloatField
      FieldName = 'EstqValor'
      Origin = 'DBMBWET.pq.EstqValor'
    end
    object QrPQ3EstqFin: TFloatField
      FieldName = 'EstqFin'
      Origin = 'DBMBWET.pq.EstqFin'
    end
    object QrPQ3RICMS: TFloatField
      FieldName = 'RICMS'
      Origin = 'DBMBWET.pq.RICMS'
    end
    object QrPQ3RICMSF: TFloatField
      FieldName = 'RICMSF'
      Origin = 'DBMBWET.pq.RICMSF'
    end
    object QrPQ3Custo: TFloatField
      FieldName = 'Custo'
      Origin = 'DBMBWET.pq.Custo'
    end
    object QrPQ3MinEstq: TFloatField
      FieldName = 'MinEstq'
      Origin = 'DBMBWET.pq.MinEstq'
    end
    object QrPQ3Ativo: TWideStringField
      FieldName = 'Ativo'
      Origin = 'DBMBWET.pq.Ativo'
      FixedChar = True
      Size = 128
    end
    object QrPQ3Lk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMBWET.pq.Lk'
    end
    object QrPQ3PBEmb: TFloatField
      FieldName = 'PBEmb'
      Origin = 'DBMBWET.pq.PBEmb'
    end
    object QrPQ3PLEmb: TFloatField
      FieldName = 'PLEmb'
      Origin = 'DBMBWET.pq.PLEmb'
    end
    object QrPQ3EstSegur: TIntegerField
      FieldName = 'EstSegur'
      Origin = 'DBMBWET.pq.EstSegur'
    end
    object QrPQ3SAkg: TFloatField
      FieldName = 'SAkg'
      Origin = 'DBMBWET.pq.SAkg'
    end
    object QrPQ3SAVlr: TFloatField
      FieldName = 'SAVlr'
      Origin = 'DBMBWET.pq.SAVlr'
    end
    object QrPQ3SetorMaster: TIntegerField
      FieldName = 'SetorMaster'
      Origin = 'DBMBWET.pq.SetorMaster'
    end
    object QrPQ3CFin: TFloatField
      FieldName = 'CFin'
      Origin = 'DBMBWET.pq.CFin'
    end
    object QrPQ3Dolar: TFloatField
      FieldName = 'Dolar'
    end
    object QrPQ3Euro: TFloatField
      FieldName = 'Euro'
    end
    object QrPQ3Moeda: TSmallintField
      FieldName = 'Moeda'
    end
    object QrPQ3Reais: TFloatField
      FieldName = 'Reais'
    end
  end
  object QrMaxCod: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Codigo) MaxCodigo,  Codigo, Conta, Controle'
      'FROM pqeits'
      'WHERE Insumo=:P0'
      'GROUP BY Codigo, Conta, Controle')
    Left = 124
    Top = 47
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMaxCodMaxCodigo: TIntegerField
      FieldName = 'MaxCodigo'
      Required = True
    end
    object QrMaxCodCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMaxCodConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMaxCodControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrConsumo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT -SUM(Peso) Peso '
      'FROM pqx'
      'WHERE Peso < 0'
      'AND DataX BETWEEN :P0 AND :P1'
      'AND Insumo=:P2'
      'AND CliDest=:P3')
    Left = 244
    Top = 47
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrConsumoPeso: TFloatField
      FieldName = 'Peso'
    end
  end
  object Query2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Codigo) Codigo, Max(Conta) Conta,'
      'Max(Controle) Controle'
      'FROM pqeits'
      'WHERE Insumo=:P0')
    Left = 320
    Top = 47
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pqeits.Codigo'
    end
    object IntegerField5: TIntegerField
      FieldName = 'Conta'
      Origin = 'DBMBWET.pqeits.Conta'
    end
    object IntegerField6: TIntegerField
      FieldName = 'Controle'
      Origin = 'DBMBWET.pqeits.Controle'
    end
  end
  object QrIQ1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT piq.Prazo '
      'FROM pqfor piq'
      'WHERE piq.PQ=:P0'
      'AND piq.CI=:P1'
      'AND piq.IQ=:P2')
    Left = 280
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrIQ1Prazo: TWideStringField
      FieldName = 'Prazo'
    end
  end
end
