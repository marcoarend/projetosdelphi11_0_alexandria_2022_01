object FmCMPTOut: TFmCMPTOut
  Left = 339
  Top = 185
  Caption = 
    'PST-_CMPT-002 :: Controle de Mat'#233'ria-primas de Terceiros - Devol' +
    'u'#231#227'o'
  ClientHeight = 705
  ClientWidth = 1015
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 92
    Width = 1015
    Height = 613
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    Visible = False
    object PnDadosEdita: TPanel
      Left = 0
      Top = 0
      Width = 1015
      Height = 93
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 816
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object Label3: TLabel
        Left = 932
        Top = 4
        Width = 17
        Height = 13
        Caption = 'NF:'
      end
      object Label5: TLabel
        Left = 8
        Top = 44
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label4: TLabel
        Left = 8
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object SpeedButton1: TSpeedButton
        Left = 976
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object TPDataS: TdmkEditDateTimePicker
        Left = 816
        Top = 20
        Width = 112
        Height = 21
        Date = 39749.934630706020000000
        Time = 39749.934630706020000000
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdNFOut: TdmkEdit
        Left = 932
        Top = 20
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdClienteEdit: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClienteEdit
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClienteEdit: TdmkDBLookupComboBox
        Left = 68
        Top = 60
        Width = 905
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECLI'
        ListSource = DsClientesEdit
        TabOrder = 5
        dmkEditCB = EdClienteEdit
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFilialEdit: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFilialEdit
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFilialEdit: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 745
        Height = 21
        KeyField = 'Filial'
        ListField = 'NomeEmp'
        ListSource = DModG.DsFiliLog
        TabOrder = 1
        dmkEditCB = EdFilialEdit
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 549
      Width = 1015
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel10: TPanel
        Left = 2
        Top = 15
        Width = 1011
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel11: TPanel
          Left = 867
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn1: TBitBtn
            Tag = 15
            Left = 2
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Desiste'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn1Click
          end
        end
        object BitBtn2: TBitBtn
          Tag = 14
          Left = 20
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Confirma'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
    end
  end
  object PnControla: TPanel
    Left = 0
    Top = 92
    Width = 1015
    Height = 613
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 232
    ExplicitTop = 112
    object PnPesquisa: TPanel
      Left = 0
      Top = 0
      Width = 1015
      Height = 108
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1015
        Height = 108
        Align = alClient
        Caption = ' Filtros de pesquisa: '
        TabOrder = 0
        object Panel3: TPanel
          Left = 2
          Top = 15
          Width = 887
          Height = 91
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 44
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object Label10: TLabel
            Left = 8
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object CBClientePesq: TdmkDBLookupComboBox
            Left = 68
            Top = 58
            Width = 580
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECLI'
            ListSource = DsClientesPesq
            TabOrder = 3
            dmkEditCB = EdClientePesq
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdClientePesq: TdmkEditCB
            Left = 8
            Top = 58
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClientePesqChange
            DBLookupComboBox = CBClientePesq
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdFilialPesq: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFilialPesqChange
            DBLookupComboBox = CBFilialPesq
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFilialPesq: TdmkDBLookupComboBox
            Left = 68
            Top = 20
            Width = 580
            Height = 21
            KeyField = 'Filial'
            ListField = 'NomeEmp'
            ListSource = DModG.DsFiliLog
            TabOrder = 1
            dmkEditCB = EdFilialPesq
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object Panel5: TPanel
          Left = 889
          Top = 15
          Width = 124
          Height = 91
          Align = alRight
          BevelOuter = bvLowered
          TabOrder = 1
          object TPFim: TdmkEditDateTimePicker
            Left = 7
            Top = 66
            Width = 110
            Height = 21
            Date = 39750.896066076390000000
            Time = 39750.896066076390000000
            TabOrder = 3
            OnClick = TPFimClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkFim: TCheckBox
            Left = 7
            Top = 48
            Width = 110
            Height = 17
            Caption = 'Data final:'
            TabOrder = 2
            OnClick = CkFimClick
          end
          object TPIni: TdmkEditDateTimePicker
            Left = 7
            Top = 22
            Width = 110
            Height = 21
            Date = 39750.896049618060000000
            Time = 39750.896049618060000000
            TabOrder = 1
            OnClick = TPIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkIni: TCheckBox
            Left = 7
            Top = 4
            Width = 110
            Height = 17
            Caption = 'Data inicial:'
            Checked = True
            State = cbChecked
            TabOrder = 0
            OnClick = CkIniClick
          end
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 108
      Width = 1015
      Height = 361
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object PnGrades: TPanel
        Left = 0
        Top = 0
        Width = 1015
        Height = 361
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 0
          Top = 203
          Width = 1015
          Height = 5
          Cursor = crVSplit
          Align = alBottom
          ExplicitLeft = 1
          ExplicitTop = 202
          ExplicitWidth = 900
        end
        object GradeOut: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 1015
          Height = 203
          Align = alClient
          DataSource = DsCMPTOut
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMEEMP'
              Title.Caption = 'Empresa'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Title.Caption = 'Cliente'
              Width = 192
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataS'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFOut'
              Title.Caption = 'NF '
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotQtdkg'
              Title.Caption = 'Peso (kg )'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotQtdPc'
              Title.Caption = 'Pe'#231'as'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotQtdVal'
              Title.Caption = '$ Tot. devol.'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Abertura'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MOFatuVal'
              Title.Caption = 'MO $ total'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MOFatuKg'
              Title.Caption = 'MO Kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ENCERROU_TXT'
              Title.Caption = 'Encerramento'
              Visible = True
            end>
        end
        object GradeIts: TDBGrid
          Left = 0
          Top = 208
          Width = 1015
          Height = 58
          Align = alBottom
          DataSource = DsCMPTOutIts
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'DataE'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFInn'
              Title.Caption = 'NF RP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFRef'
              Title.Caption = 'NF VP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CMPTInn'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OutQtdkg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OutQtdPc'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OutQtdVal'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MOPesoKg'
              Title.Caption = 'MO Kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MOPrcKg'
              Title.Caption = 'MO $ unit.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MOValor'
              Title.Caption = 'MO $ Tot'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFCMO'
              Title.Caption = 'NF CMO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFDMP'
              Title.Caption = 'NF DMP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFDIQ'
              Title.Caption = 'NF DIQ'
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 0
          Top = 325
          Width = 1015
          Height = 36
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object Label6: TLabel
            Left = 0
            Top = 0
            Width = 64
            Height = 36
            Align = alLeft
            AutoSize = False
            Caption = 'Status NF-e:'
            ExplicitLeft = 32
            ExplicitTop = 4
          end
          object DBMemo1: TDBMemo
            Left = 64
            Top = 0
            Width = 951
            Height = 36
            Align = alClient
            DataField = 'cStat_xMotivo'
            DataSource = DsNFeCabA
            TabOrder = 0
          end
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 266
          Width = 1015
          Height = 59
          Align = alBottom
          DataSource = DsFatPedNFs
          TabOrder = 3
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Filial'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SerieNFTxt'
              Title.Caption = 'S'#233'rie NF'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NumeroNF'
              Title.Caption = 'N'#250'mero'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DTEMISSNF_TXT'
              Title.Caption = 'Emiss'#227'o NF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DTENTRASAI_TXT'
              Title.Caption = 'Data sa'#237'da'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataAlt_TXT'
              Title.Caption = 'Altera'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FreteVal'
              Title.Caption = '$ Frete'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Seguro'
              Title.Caption = '$ Seguro'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Outros'
              Title.Caption = '$ Desp. Acess.'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CFOP1'
              Title.Caption = 'CFOP 1'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IDCtrl'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Status'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'infProt_cStat'
              Title.Caption = 'Cria'#231#227'o NF-e'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'infCanc_cStat'
              Title.Caption = 'Cancel. NF-e'
              Width = 68
              Visible = True
            end>
        end
      end
    end
    object GbControla: TGroupBox
      Left = 0
      Top = 549
      Width = 1015
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 1011
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 867
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 4
          object BtSaida: TBitBtn
            Tag = 13
            Left = 2
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtNF: TBitBtn
          Tag = 396
          Left = 20
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Fatura'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtNFClick
        end
        object BtItens: TBitBtn
          Tag = 425
          Left = 140
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Itens'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtItensClick
        end
        object BtEncerra: TBitBtn
          Left = 384
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Encerrar'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtEncerraClick
        end
        object BtNFe: TBitBtn
          Tag = 5
          Left = 504
          Top = 4
          Width = 120
          Height = 40
          Caption = '&NF-e'
          NumGlyphs = 2
          TabOrder = 3
          Visible = False
          OnClick = BtNFeClick
        end
        object BtImpressao: TBitBtn
          Tag = 5
          Left = 260
          Top = 4
          Width = 120
          Height = 40
          Caption = 'I&mprime'
          NumGlyphs = 2
          TabOrder = 5
          OnClick = BtImpressaoClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1015
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 967
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 919
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 639
        Height = 32
        Caption = 'Controle de Mat'#233'ria-primas de Terceiros - Devolu'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 639
        Height = 32
        Caption = 'Controle de Mat'#233'ria-primas de Terceiros - Devolu'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 639
        Height = 32
        Caption = 'Controle de Mat'#233'ria-primas de Terceiros - Devolu'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1015
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1011
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PMNF: TPopupMenu
    Left = 60
    Top = 524
    object Inclui1: TMenuItem
      Caption = '&Inclui nova NF'
      Enabled = False
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera NF atual'
      Enabled = False
      OnClick = Altera1Click
    end
    object Incluinovafatura1: TMenuItem
      Caption = '&Inclui nova devolu'#231#227'o / fatura'
      OnClick = Incluinovafatura1Click
    end
    object Alteradevoluofaturaatual1: TMenuItem
      Caption = '&Altera devolu'#231#227'o / fatura atual'
      OnClick = Alteradevoluofaturaatual1Click
    end
  end
  object PMItens: TPopupMenu
    Left = 164
    Top = 516
    object Incluiitemdedevoluo1: TMenuItem
      Caption = '&Inclui item de devolu'#231#227'o'
      OnClick = Incluiitemdedevoluo1Click
    end
    object Retiraitemdedevoluo1: TMenuItem
      Caption = '&Retira item de devolu'#231#227'o'
      OnClick = Retiraitemdedevoluo1Click
    end
  end
  object PMNFe: TPopupMenu
    OnPopup = PMNFePopup
    Left = 584
    Top = 640
    object RecriatodaNFe1: TMenuItem
      Caption = '&Recria toda NFe'
      OnClick = RecriatodaNFe1Click
    end
  end
  object QrCMPTOut: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCMPTOutBeforeClose
    AfterScroll = QrCMPTOutAfterScroll
    OnCalcFields = QrCMPTOutCalcFields
    SQL.Strings = (
      'SELECT   '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP, '
      'cmp.* '
      'FROM cmptout cmp '
      'LEFT JOIN entidades cli ON cmp.Cliente=cli.Codigo '
      'LEFT JOIN entidades emp ON cmp.Empresa=emp.Codigo '
      'WHERE cmp.DataS  > -999999999 '
      'ORDER BY DataS DESC ')
    Left = 44
    Top = 224
    object QrCMPTOutNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCMPTOutCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cmptout.Codigo'
      Required = True
    end
    object QrCMPTOutCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'cmptout.Cliente'
      Required = True
    end
    object QrCMPTOutDataS: TDateField
      FieldName = 'DataS'
      Origin = 'cmptout.DataS'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCMPTOutNFOut: TIntegerField
      FieldName = 'NFOut'
      Origin = 'cmptout.NFOut'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTOutTotQtdkg: TFloatField
      FieldName = 'TotQtdkg'
      Origin = 'cmptout.TotQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTOutTotQtdPc: TFloatField
      FieldName = 'TotQtdPc'
      Origin = 'cmptout.TotQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCMPTOutTotQtdVal: TFloatField
      FieldName = 'TotQtdVal'
      Origin = 'cmptout.TotQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTOutEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'cmptout.Empresa'
    end
    object QrCMPTOutTotQtdM2: TFloatField
      FieldName = 'TotQtdM2'
      Origin = 'cmptout.TotQtdM2'
    end
    object QrCMPTOutAbertura: TDateTimeField
      FieldName = 'Abertura'
      Origin = 'cmptout.Abertura'
    end
    object QrCMPTOutEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Origin = 'cmptout.Encerrou'
    end
    object QrCMPTOutMOFatuKg: TFloatField
      FieldName = 'MOFatuKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTOutMOFatuVal: TFloatField
      FieldName = 'MOFatuVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTOutNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrCMPTOutSrvValTot: TFloatField
      FieldName = 'SrvValTot'
    end
    object QrCMPTOutCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrCMPTOutCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrCMPTOutFretePor: TIntegerField
      FieldName = 'FretePor'
    end
    object QrCMPTOutTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrCMPTOutFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
    end
    object QrCMPTOutDataFat: TDateField
      FieldName = 'DataFat'
    end
    object QrCMPTOutStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrCMPTOutReabriu: TDateTimeField
      FieldName = 'Reabriu'
    end
    object QrCMPTOutSerieDesfe: TIntegerField
      FieldName = 'SerieDesfe'
    end
    object QrCMPTOutNFDesfeita: TIntegerField
      FieldName = 'NFDesfeita'
    end
    object QrCMPTOutLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCMPTOutDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCMPTOutDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCMPTOutUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCMPTOutUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCMPTOutAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCMPTOutAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCMPTOutJaAtz: TSmallintField
      FieldName = 'JaAtz'
    end
    object QrCMPTOutFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrCMPTOutENCERROU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENCERROU_TXT'
      Calculated = True
    end
  end
  object DsCMPTOut: TDataSource
    DataSet = QrCMPTOut
    Left = 44
    Top = 272
  end
  object QrCMPTOutIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT inn.DataE, inn.NFInn, inn.NFRef, inn.GraGruX, '
      'inn.TipoNF, inn.refNFe, inn.modNF, inn.Serie, its.*'
      'FROM CMPToutits its'
      'LEFT JOIN CMPTinn inn ON inn.Codigo=its.CMPTInn'
      'WHERE its.Codigo=:P0')
    Left = 120
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCMPTOutItsDataE: TDateField
      FieldName = 'DataE'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCMPTOutItsNFInn: TIntegerField
      FieldName = 'NFInn'
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTOutItsNFRef: TIntegerField
      FieldName = 'NFRef'
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTOutItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCMPTOutItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCMPTOutItsCMPTInn: TIntegerField
      FieldName = 'CMPTInn'
      Required = True
    end
    object QrCMPTOutItsOutQtdkg: TFloatField
      FieldName = 'OutQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;#,###,##0.000; '
    end
    object QrCMPTOutItsOutQtdPc: TFloatField
      FieldName = 'OutQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;#,###,##0.0; '
    end
    object QrCMPTOutItsOutQtdVal: TFloatField
      FieldName = 'OutQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;#,###,##0.00; '
    end
    object QrCMPTOutItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrCMPTOutItsBenefikg: TFloatField
      FieldName = 'Benefikg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTOutItsBenefiPc: TFloatField
      FieldName = 'BenefiPc'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCMPTOutItsBenefiM2: TFloatField
      FieldName = 'BenefiM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTOutItsOutQtdM2: TFloatField
      FieldName = 'OutQtdM2'
    end
    object QrCMPTOutItsTipoNF: TSmallintField
      FieldName = 'TipoNF'
    end
    object QrCMPTOutItsrefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrCMPTOutItsmodNF: TSmallintField
      FieldName = 'modNF'
    end
    object QrCMPTOutItsSerie: TIntegerField
      FieldName = 'Serie'
    end
    object QrCMPTOutItsSitDevolu: TSmallintField
      FieldName = 'SitDevolu'
    end
    object QrCMPTOutItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCMPTOutItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCMPTOutItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCMPTOutItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCMPTOutItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCMPTOutItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCMPTOutItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCMPTOutItsMOPesoKg: TFloatField
      FieldName = 'MOPesoKg'
    end
    object QrCMPTOutItsMOPrcKg: TFloatField
      FieldName = 'MOPrcKg'
    end
    object QrCMPTOutItsMOValor: TFloatField
      FieldName = 'MOValor'
    end
    object QrCMPTOutItsHowCobrMO: TSmallintField
      FieldName = 'HowCobrMO'
    end
    object QrCMPTOutItsNFCMO: TIntegerField
      FieldName = 'NFCMO'
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTOutItsNFDMP: TIntegerField
      FieldName = 'NFDMP'
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTOutItsNFDIQ: TIntegerField
      FieldName = 'NFDIQ'
      DisplayFormat = '000000;-000000; '
    end
  end
  object DsCMPTOutIts: TDataSource
    DataSet = QrCMPTOutIts
    Left = 120
    Top = 272
  end
  object QrClientesPesq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMECLI'
      'FROM entidades en '
      'WHERE en.Cliente2="V"'
      'ORDER BY NOMECLI')
    Left = 604
    Top = 64
    object QrClientesPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClientesPesqNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsClientesPesq: TDataSource
    DataSet = QrClientesPesq
    Left = 604
    Top = 108
  end
  object QrClientesEdit: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMECLI'
      'FROM entidades en '
      'WHERE en.Cliente2="V"'
      'ORDER BY NOMECLI')
    Left = 768
    Top = 64
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsClientesEdit: TDataSource
    DataSet = QrClientesEdit
    Left = 768
    Top = 112
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 860
    Top = 112
  end
  object QrNFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeCabACalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND ide_nNF=:P3')
    Left = 860
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeCabAStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrNFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrNFeCabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrNFeCabAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrNFeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrNFeCabAcStat: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'cStat'
      Calculated = True
    end
    object QrNFeCabAxMotivo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xMotivo'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAcStat_xMotivo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'cStat_xMotivo'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
  end
  object DsFatPedNFs: TDataSource
    DataSet = QrFatPedNFs
    Left = 676
    Top = 108
  end
  object QrFatPedNFs: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFatPedNFsBeforeClose
    AfterScroll = QrFatPedNFsAfterScroll
    SQL.Strings = (
      'SELECT nfe.Status, nfe.infProt_cStat, nfe.infCanc_cStat, '
      'ent.Filial, ent.Codigo CO_ENT_EMP, smna.*'
      'FROM stqmovnfsa smna '
      'LEFT JOIN entidades ent ON ent.Codigo=smna.Empresa'
      'LEFT JOIN nfecaba nfe ON nfe.IDCtrl=smna.IDCtrl'
      'WHERE smna.Tipo=:P0'
      'AND smna.OriCodi=:P1'
      'AND smna.Empresa=:P2'
      'ORDER BY ent.Filial')
    Left = 680
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrFatPedNFsFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
      DisplayFormat = '000'
    end
    object QrFatPedNFsIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'stqmovnfsa.IDCtrl'
      Required = True
    end
    object QrFatPedNFsTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'stqmovnfsa.Tipo'
    end
    object QrFatPedNFsOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'stqmovnfsa.OriCodi'
    end
    object QrFatPedNFsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'stqmovnfsa.Empresa'
    end
    object QrFatPedNFsNumeroNF: TIntegerField
      FieldName = 'NumeroNF'
      Origin = 'stqmovnfsa.NumeroNF'
      DisplayFormat = '000000;-000000; '
    end
    object QrFatPedNFsIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
      Origin = 'stqmovnfsa.IncSeqAuto'
    end
    object QrFatPedNFsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'stqmovnfsa.AlterWeb'
    end
    object QrFatPedNFsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'stqmovnfsa.Ativo'
    end
    object QrFatPedNFsSerieNFCod: TIntegerField
      FieldName = 'SerieNFCod'
      Origin = 'stqmovnfsa.SerieNFCod'
    end
    object QrFatPedNFsSerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Origin = 'stqmovnfsa.SerieNFTxt'
      Size = 5
    end
    object QrFatPedNFsCO_ENT_EMP: TIntegerField
      FieldName = 'CO_ENT_EMP'
      Origin = 'entidades.Codigo'
    end
    object QrFatPedNFsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'stqmovnfsa.DataCad'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'stqmovnfsa.DataAlt'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsDataAlt_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataAlt_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFsFreteVal: TFloatField
      FieldName = 'FreteVal'
      Origin = 'stqmovnfsa.FreteVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFsSeguro: TFloatField
      FieldName = 'Seguro'
      Origin = 'stqmovnfsa.Seguro'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFsOutros: TFloatField
      FieldName = 'Outros'
      Origin = 'stqmovnfsa.Outros'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFsPlacaUF: TWideStringField
      FieldName = 'PlacaUF'
      Origin = 'stqmovnfsa.PlacaUF'
      Size = 2
    end
    object QrFatPedNFsPlacaNr: TWideStringField
      FieldName = 'PlacaNr'
      Origin = 'stqmovnfsa.PlacaNr'
      Size = 8
    end
    object QrFatPedNFsEspecie: TWideStringField
      FieldName = 'Especie'
      Origin = 'stqmovnfsa.Especie'
      Size = 30
    end
    object QrFatPedNFsMarca: TWideStringField
      FieldName = 'Marca'
      Origin = 'stqmovnfsa.Marca'
      Size = 30
    end
    object QrFatPedNFsNumero: TWideStringField
      FieldName = 'Numero'
      Origin = 'stqmovnfsa.Numero'
      Size = 30
    end
    object QrFatPedNFskgBruto: TFloatField
      FieldName = 'kgBruto'
      Origin = 'stqmovnfsa.kgBruto'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrFatPedNFskgLiqui: TFloatField
      FieldName = 'kgLiqui'
      Origin = 'stqmovnfsa.kgLiqui'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrFatPedNFsQuantidade: TWideStringField
      FieldName = 'Quantidade'
      Origin = 'stqmovnfsa.Quantidade'
      Size = 30
    end
    object QrFatPedNFsObservacao: TWideStringField
      FieldName = 'Observacao'
      Origin = 'stqmovnfsa.Observacao'
      Size = 255
    end
    object QrFatPedNFsCFOP1: TWideStringField
      FieldName = 'CFOP1'
      Origin = 'stqmovnfsa.CFOP1'
      Size = 6
    end
    object QrFatPedNFsDtEmissNF: TDateField
      FieldName = 'DtEmissNF'
      Origin = 'stqmovnfsa.DtEmissNF'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsDtEntraSai: TDateField
      FieldName = 'DtEntraSai'
      Origin = 'stqmovnfsa.DtEntraSai'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'nfecaba.Status'
    end
    object QrFatPedNFsinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
      Origin = 'nfecaba.infProt_cStat'
    end
    object QrFatPedNFsinfAdic_infCpl: TWideMemoField
      FieldName = 'infAdic_infCpl'
      Origin = 'stqmovnfsa.infAdic_infCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFatPedNFsinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
      Origin = 'nfecaba.infCanc_cStat'
    end
    object QrFatPedNFsDTEMISSNF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTEMISSNF_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFsDTENTRASAI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTENTRASAI_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFsRNTC: TWideStringField
      FieldName = 'RNTC'
      Origin = 'stqmovnfsa.RNTC'
    end
    object QrFatPedNFsUFembarq: TWideStringField
      FieldName = 'UFembarq'
      Origin = 'stqmovnfsa.UFembarq'
      Size = 2
    end
    object QrFatPedNFsxLocEmbarq: TWideStringField
      FieldName = 'xLocEmbarq'
      Origin = 'stqmovnfsa.xLocEmbarq'
      Size = 60
    end
    object QrFatPedNFsHrEntraSai: TTimeField
      FieldName = 'HrEntraSai'
      Origin = 'stqmovnfsa.HrEntraSai'
    end
    object QrFatPedNFside_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
      Origin = 'stqmovnfsa.ide_dhCont'
    end
    object QrFatPedNFside_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Origin = 'stqmovnfsa.ide_xJust'
      Size = 255
    end
    object QrFatPedNFsemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
      Origin = 'stqmovnfsa.emit_CRT'
    end
    object QrFatPedNFsdest_email: TWideStringField
      FieldName = 'dest_email'
      Origin = 'stqmovnfsa.dest_email'
      Size = 60
    end
    object QrFatPedNFsvagao: TWideStringField
      FieldName = 'vagao'
      Origin = 'stqmovnfsa.vagao'
    end
    object QrFatPedNFsbalsa: TWideStringField
      FieldName = 'balsa'
      Origin = 'stqmovnfsa.balsa'
    end
    object QrFatPedNFsCompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrFatPedNFsCompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrFatPedNFsCompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
  end
  object frxPST__CMPT_002_01: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38928.750734444400000000
    ReportOptions.Name = 'Or'#231'amento'
    ReportOptions.LastChange = 38928.750734444400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '(*'
      '  if <VFR_ORDENADO> then'
      '  begin'
      
        '    GroupHeader1.Visible := True;                               ' +
        '                                   '
      '    GroupFooter1.Visible := True;'
      '    //          '
      '    GroupHeader1.Condition := <VARF_GRUPO1>;            '
      '    MeGrupo1Head.Memo.Text := <VARF_HEADR1>;            '
      '    MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;            '
      '  end else begin'
      
        '    GroupHeader1.Visible := False;                              ' +
        '                                    '
      
        '    GroupFooter1.Visible := False;                              ' +
        '                                    '
      '  end;            '
      
        '  //if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLog' +
        'oCaminho>);'
      '*)'
      'end.')
    OnGetValue = frxPST__CMPT_002_01GetValue
    Left = 416
    Top = 236
    Datasets = <
      item
        DataSet = frxDsCMPTOut
        DataSetName = 'frxDsCMPTOut'
      end
      item
        DataSet = frxDsCMPTOutIts
        DataSetName = 'frxDsCMPTOutIts'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <
      item
        Name = ' MyVars'
        Value = Null
      end
      item
        Name = 'MeuLogo'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCMPTOut
        DataSetName = 'frxDsCMPTOut'
        KeepChild = True
        KeepFooter = True
        KeepHeader = True
        KeepTogether = True
        RowCount = 0
        object Memo10: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 3.779530000000000000
          Width = 668.976565910000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 321.260050000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCMPTOut
          DataSetName = 'frxDsCMPTOut'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOut."TotQtdVal"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 264.567100000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsCMPTOut
          DataSetName = 'frxDsCMPTOut'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOut."TotQtdPc"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 196.535560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCMPTOut
          DataSetName = 'frxDsCMPTOut'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOut."TotQtdkg"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 56.692950000000010000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'DataS'
          DataSet = frxDsCMPTOut
          DataSetName = 'frxDsCMPTOut'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCMPTOut."DataS"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 483.779840000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCMPTOut
          DataSetName = 'frxDsCMPTOut'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOut."MOFatuVal"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 551.811380000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'MOFatuKg'
          DataSet = frxDsCMPTOut
          DataSetName = 'frxDsCMPTOut'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOut."MOFatuKg"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 102.047310000000000000
          Width = 94.488206060000000000
          Height = 15.118110240000000000
          DataField = 'Abertura'
          DataSet = frxDsCMPTOut
          DataSetName = 'frxDsCMPTOut'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCMPTOut."Abertura"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 389.291590000000000000
          Width = 94.488206060000000000
          Height = 15.118110240000000000
          DataField = 'ENCERROU_TXT'
          DataSet = frxDsCMPTOut
          DataSetName = 'frxDsCMPTOut'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCMPTOut."ENCERROU_TXT"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 11.338590000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Codigo'
          DataSet = frxDsCMPTOut
          DataSetName = 'frxDsCMPTOut'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOut."Codigo"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 529.134199999999900000
        Width = 680.315400000000000000
        object Memo33: TfrxMemoView
          Left = 321.260050000000000000
          Top = 7.559060000000159000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTOut."TotQtdVal">)]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 264.567100000000000000
          Top = 7.559060000000159000
          Width = 56.692906060000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTOut."TotQtdPc">)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 196.535560000000000000
          Top = 7.559060000000159000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTOut."TotQtdkg">)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Top = 7.559060000000159000
          Width = 177.637910000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 483.779840000000000000
          Top = 7.559060000000159000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTOut."MOFatuVal">)]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 551.811380000000000000
          Top = 7.559060000000159000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTOut."MOFatuKg">)]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 389.291590000000000000
          Top = 7.559059999999931000
          Width = 94.488206060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 143.622140000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCMPTOut."Cliente"'
        object MeGrupo1Head: TfrxMemoView
          Top = 7.559059999999988000
          Width = 680.315058270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente: [frxDsCMPTOut."NOMECLI"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 427.086890000000000000
        Width = 680.315400000000000000
        object Memo52: TfrxMemoView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 321.260050000000000000
          Top = 15.118119999999860000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTOut."TotQtdVal">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 264.567100000000000000
          Top = 15.118119999999860000
          Width = 56.692906060000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTOut."TotQtdPc">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 196.535560000000000000
          Top = 15.118119999999860000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTOut."TotQtdkg">)]')
          ParentFont = False
        end
        object MeGrupo1Foot: TfrxMemoView
          Width = 196.535560000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total: [frxDsCMPTOut."NOMECLI"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 483.779840000000000000
          Top = 15.118119999999860000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTOut."MOFatuVal">)]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 551.811380000000000000
          Top = 15.118119999999860000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCMPTOut."MOFatuKg">)]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 389.291590000000000000
          Top = 15.118119999999860000
          Width = 94.488206060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 321.260050000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 264.567100000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 196.535560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg ')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 483.779840000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'MO valor')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 551.811380000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'MO Kg')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 389.291590000000000000
          Width = 94.488206060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCMPTOut."Codigo"'
        object Memo3: TfrxMemoView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 3.779530000000000000
          Top = 7.559059999999988000
          Width = 668.976810000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'NF Devolu'#231#227'o: [frxDsCMPTOut."NFOut"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 56.692950000000010000
          Top = 22.677179999999960000
          Width = 45.354320940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 321.260050000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 264.567100000000000000
          Top = 22.677179999999990000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 196.535560000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg ')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 483.779840000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'MO valor')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 551.811380000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'MO Kg')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 102.047310000000000000
          Top = 22.677179999999960000
          Width = 94.488206060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Abertura')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 389.291590000000000000
          Top = 22.677179999999990000
          Width = 94.488206060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Encerramento')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 11.338590000000000000
          Top = 22.677179999999960000
          Width = 45.354320940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 9.448826220000001000
        Top = 393.071120000000000000
        Width = 680.315400000000000000
        object Memo54: TfrxMemoView
          Width = 680.315400000000000000
          Height = 9.448826220000001000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 3.779530000000000000
          Width = 668.976565910000000000
          Height = 3.779527559055118000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 574.488560000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 362.834880000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 351.496290000000000000
          Width = 328.819110000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCMPTOutIts
        DataSetName = 'frxDsCMPTOutIts'
        KeepFooter = True
        KeepHeader = True
        KeepTogether = True
        RowCount = 0
        object Memo8: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 3.779530000000000000
          Width = 668.976565910000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 11.338590000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'DataE'
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."DataE"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 56.692950000000010000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."NFInn"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 102.047310000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'NFRef'
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."NFRef"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 147.401670000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."Controle"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 192.756030000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'CMPTInn'
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."CMPTInn"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 328.819110000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'OutQtdVal'
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."OutQtdVal"]')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 294.803340000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'OutQtdPc'
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."OutQtdPc"]')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 238.110390000000000000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          DataField = 'OutQtdkg'
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."OutQtdkg"]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 476.220780000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'MOValor'
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."MOValor"]')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 434.645950000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'MOPrcKg'
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."MOPrcKg"]')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 377.953000000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'MOPesoKg'
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."MOPesoKg"]')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Left = 532.913730000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'NFCMO'
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."NFCMO"]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 578.268090000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'NFDMP'
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."NFDMP"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 623.622450000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'NFDIQ'
          DataSet = frxDsCMPTOutIts
          DataSetName = 'frxDsCMPTOutIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCMPTOutIts."NFDIQ"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        object Memo13: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 3.779530000000000000
          Width = 668.976565910000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 11.338590000000000000
          Width = 45.354320940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 56.692950000000010000
          Width = 45.354320940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'NF RP')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 102.047310000000000000
          Width = 45.354320940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'NF VP')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 147.401670000000000000
          Width = 45.354320940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 192.756030000000000000
          Width = 45.354320940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID CMPT')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 328.819110000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Devol. Valor')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 294.803340000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Devl.Pe'#231'as')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 238.110390000000000000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Devol. Kg ')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 476.220780000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'MO Valor')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 434.645950000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'MO Pre'#231'o')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 377.953000000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'MO Kg ')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Left = 532.913730000000000000
          Width = 45.354320940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'NF CMO')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Left = 578.268090000000000000
          Width = 45.354320940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'NF DMP')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Left = 623.622450000000000000
          Width = 45.354320940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'NF DIQ')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 1.889766220000000000
        Top = 366.614410000000000000
        Width = 680.315400000000000000
        object Memo24: TfrxMemoView
          Width = 680.315400000000000000
          Height = 1.889763780000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 3.779530000000000000
          Width = 668.976565910000000000
          Height = 1.889763780000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
      end
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle de Estoque de Couros de Terceiros')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 332.598640000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 340.157700000000000000
          Top = 41.574830000000000000
          Width = 336.378170000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente: [VARF_CLIENTE]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsCMPTOut: TfrxDBDataset
    UserName = 'frxDsCMPTOut'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMECLI=NOMECLI'
      'Codigo=Codigo'
      'Cliente=Cliente'
      'DataS=DataS'
      'NFOut=NFOut'
      'TotQtdkg=TotQtdkg'
      'TotQtdPc=TotQtdPc'
      'TotQtdVal=TotQtdVal'
      'Empresa=Empresa'
      'TotQtdM2=TotQtdM2'
      'Abertura=Abertura'
      'Encerrou=Encerrou'
      'MOFatuKg=MOFatuKg'
      'MOFatuVal=MOFatuVal'
      'NOMEEMP=NOMEEMP'
      'SrvValTot=SrvValTot'
      'CondicaoPg=CondicaoPg'
      'Carteira=Carteira'
      'FretePor=FretePor'
      'Transporta=Transporta'
      'FisRegCad=FisRegCad'
      'DataFat=DataFat'
      'Status=Status'
      'Reabriu=Reabriu'
      'SerieDesfe=SerieDesfe'
      'NFDesfeita=NFDesfeita'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'JaAtz=JaAtz'
      'Filial=Filial'
      'ENCERROU_TXT=ENCERROU_TXT')
    DataSet = QrCMPTOut
    BCDToCurrency = False
    Left = 40
    Top = 320
  end
  object frxDsCMPTOutIts: TfrxDBDataset
    UserName = 'frxDsCMPTOutIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataE=DataE'
      'NFInn=NFInn'
      'NFRef=NFRef'
      'Codigo=Codigo'
      'Controle=Controle'
      'CMPTInn=CMPTInn'
      'OutQtdkg=OutQtdkg'
      'OutQtdPc=OutQtdPc'
      'OutQtdVal=OutQtdVal'
      'GraGruX=GraGruX'
      'Benefikg=Benefikg'
      'BenefiPc=BenefiPc'
      'BenefiM2=BenefiM2'
      'OutQtdM2=OutQtdM2'
      'TipoNF=TipoNF'
      'refNFe=refNFe'
      'modNF=modNF'
      'Serie=Serie'
      'SitDevolu=SitDevolu'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'MOPesoKg=MOPesoKg'
      'MOPrcKg=MOPrcKg'
      'MOValor=MOValor'
      'HowCobrMO=HowCobrMO'
      'NFCMO=NFCMO'
      'NFDMP=NFDMP'
      'NFDIQ=NFDIQ')
    DataSet = QrCMPTOutIts
    BCDToCurrency = False
    Left = 116
    Top = 320
  end
end
