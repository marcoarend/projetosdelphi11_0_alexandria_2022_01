unit ModAppGraG1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, mySQLDbTables,
  dmkGeral, MyDBCheck, UnDmkEnums, UnMyObjects, dmkEdit, UnInternalConsts,
  BluedermConsts, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls, frxClass,
  frxDBSet;

type
  TDfModAppGraG1 = class(TForm)
    QrPQEIts: TmySQLQuery;
    QrPQEItsCUSTOITEM: TFloatField;
    QrPQEItsVALORKG: TFloatField;
    QrPQEItsTOTALKGBRUTO: TFloatField;
    QrPQEItsCUSTOKG: TFloatField;
    QrPQEItsPRECOKG: TFloatField;
    QrPQEItsCodigo: TIntegerField;
    QrPQEItsControle: TIntegerField;
    QrPQEItsConta: TIntegerField;
    QrPQEItsVolumes: TIntegerField;
    QrPQEItsPesoVB: TFloatField;
    QrPQEItsPesoVL: TFloatField;
    QrPQEItsValorItem: TFloatField;
    QrPQEItsIPI: TFloatField;
    QrPQEItsRIPI: TFloatField;
    QrPQEItsCFin: TFloatField;
    QrPQEItsICMS: TFloatField;
    QrPQEItsRICMS: TFloatField;
    QrPQEItsTotalCusto: TFloatField;
    QrPQEItsInsumo: TIntegerField;
    QrPQEItsTotalPeso: TFloatField;
    QrPQEItsNOMEPQ: TWideStringField;
    QrPQEItsprod_cProd: TWideStringField;
    QrPQEItsprod_cEAN: TWideStringField;
    QrPQEItsprod_xProd: TWideStringField;
    QrPQEItsprod_NCM: TWideStringField;
    QrPQEItsprod_EX_TIPI: TWideStringField;
    QrPQEItsprod_genero: TSmallintField;
    QrPQEItsprod_CFOP: TWideStringField;
    QrPQEItsprod_uCom: TWideStringField;
    QrPQEItsprod_qCom: TFloatField;
    QrPQEItsprod_vUnCom: TFloatField;
    QrPQEItsprod_vProd: TFloatField;
    QrPQEItsprod_cEANTrib: TWideStringField;
    QrPQEItsprod_uTrib: TWideStringField;
    QrPQEItsprod_qTrib: TFloatField;
    QrPQEItsprod_vUnTrib: TFloatField;
    QrPQEItsprod_vFrete: TFloatField;
    QrPQEItsprod_vSeg: TFloatField;
    QrPQEItsprod_vDesc: TFloatField;
    QrPQEItsICMS_Orig: TSmallintField;
    QrPQEItsICMS_CST: TSmallintField;
    QrPQEItsICMS_modBC: TSmallintField;
    QrPQEItsICMS_pRedBC: TFloatField;
    QrPQEItsICMS_vBC: TFloatField;
    QrPQEItsICMS_pICMS: TFloatField;
    QrPQEItsICMS_vICMS: TFloatField;
    QrPQEItsICMS_modBCST: TSmallintField;
    QrPQEItsICMS_pMVAST: TFloatField;
    QrPQEItsICMS_pRedBCST: TFloatField;
    QrPQEItsICMS_vBCST: TFloatField;
    QrPQEItsICMS_pICMSST: TFloatField;
    QrPQEItsICMS_vICMSST: TFloatField;
    QrPQEItsIPI_cEnq: TWideStringField;
    QrPQEItsIPI_CST: TSmallintField;
    QrPQEItsIPI_vBC: TFloatField;
    QrPQEItsIPI_qUnid: TFloatField;
    QrPQEItsIPI_vUnid: TFloatField;
    QrPQEItsIPI_pIPI: TFloatField;
    QrPQEItsIPI_vIPI: TFloatField;
    QrPQEItsPIS_CST: TSmallintField;
    QrPQEItsPIS_vBC: TFloatField;
    QrPQEItsPIS_pPIS: TFloatField;
    QrPQEItsPIS_vPIS: TFloatField;
    QrPQEItsPIS_qBCProd: TFloatField;
    QrPQEItsPIS_vAliqProd: TFloatField;
    QrPQEItsPISST_vBC: TFloatField;
    QrPQEItsPISST_pPIS: TFloatField;
    QrPQEItsPISST_qBCProd: TFloatField;
    QrPQEItsPISST_vAliqProd: TFloatField;
    QrPQEItsPISST_vPIS: TFloatField;
    QrPQEItsCOFINS_CST: TSmallintField;
    QrPQEItsCOFINS_vBC: TFloatField;
    QrPQEItsCOFINS_pCOFINS: TFloatField;
    QrPQEItsCOFINS_qBCProd: TFloatField;
    QrPQEItsCOFINS_vAliqProd: TFloatField;
    QrPQEItsCOFINS_vCOFINS: TFloatField;
    QrPQEItsCOFINSST_vBC: TFloatField;
    QrPQEItsCOFINSST_pCOFINS: TFloatField;
    QrPQEItsCOFINSST_qBCProd: TFloatField;
    QrPQEItsCOFINSST_vAliqProd: TFloatField;
    QrPQEItsCOFINSST_vCOFINS: TFloatField;
    PB1: TProgressBar;
    frxErrGrandz: TfrxReport;
    QrErrGrandz: TmySQLQuery;
    QrErrGrandzNivel1: TIntegerField;
    QrErrGrandzReduzido: TIntegerField;
    QrErrGrandzNO_GG1: TWideStringField;
    QrErrGrandzSigla: TWideStringField;
    frxDsErrGrandz: TfrxDBDataset;
    QrErrGrandzdif_Grandeza: TFloatField;
    QrPQEItsDtCorrApo: TDateTimeField;
    QrErrGrandzxco_Grandeza: TFloatField;
    QrErrGrandzGrandeza: TFloatField;
    QrErrNCMs: TmySQLQuery;
    QrErrNCMsNivel1: TIntegerField;
    QrErrNCMsNome: TWideStringField;
    QrErrNCMsNCM: TWideStringField;
    frxDsErrNCMs: TfrxDBDataset;
    frxErrNCMs: TfrxReport;
    QrPQEItsxLote: TWideStringField;
    QrPQEItsdFab: TDateField;
    QrPQEItsdVal: TDateField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AlteraProdutoTabelaPersonalizada(GraTabApp, Nivel1: Integer;
              Nome: String; QueryNivel: TMySQLQuery; PreCadastra: Boolean = False);
    function  GrandesasInconsistentes(SQL: array of String; DB: TmySQLDataBase):
              Boolean;
    function  InsAltUsoConsumoCalculaDiferencas(tPag: TAquemPag; Codigo:
              Integer; TabLctA: String): Double;
    function  InsAltUsoConsumoCab(Data, DataE, refNFe: String; UpdCodigo, IQ, CI,
              Transportadora, NF, Conhecimento, Pedido, TipoNF, modNF, Serie,
              NF_RP, NF_CC, HowLoad: Integer; Frete, PesoB, PesoL, ValorNF,
              ICMS: Double; SQLType: TSQLType; FatID, FatNum,
              Empresa: Integer): Boolean;
    procedure InsAltUsoConsumoDistribuiCustoDoFrete(Codigo: Integer;
              PQEValorNF, PQEJuros, PQEFrete, PQEPesoB, PQEPesoL: Double);
    function  InsAltUsoConsumoExcluiItem(FatID, FatNum, Empresa, nItem:
              Integer): Boolean;
    function  InsAltUsoConsumoFinalizaEntrada(Pergunta: Boolean; SQLType:
              TSQLType; Codigo: Integer): Boolean;
    function  InsAltUsoConsumoIts(FatID, FatNum, Empresa, nItem, OriCtrl:
              Integer; GraGruX, Volumes: Integer; PesoVB, PesoVL, ValorItem,
              IPI_pIPI, IPI_vIPI, TotalCusto, TotalPeso: Double; prod_CFOP:
              Integer; SQLType: TSQLType): Boolean;
    function  NCMsInconsistentes(SQL: array of String; DB: TmySQLDataBase):
              Boolean;
    function  PreparaUpdUsoConsumoCab(FatID, FatNUm, Empresa: Integer;
              EdNF_RP, EdNF_CC, EdConhecimento, EdFrete, EdPesoL, EdPesoB:
              TdmkEdit(*; EdCodInfoCliI: TdmkEditCB; CBCodInfoCliI:
              TdmkDBLookupComboBox*)): Boolean;
  end;

var
  DfModAppGraG1: TDfModAppGraG1;

implementation

uses
  DmkDAC_PF, Module, UMySQLModule, ModProd, UnFinanceiro, ModuleGeral,
  ModuleNFe_0000, GraGru1, GraGruX, PQx, UnPQ_PF, UnUMedi_PF, UnGrade_Jan;
{$R *.dfm}

{ TFmModAppGraG1 }
procedure TDfModAppGraG1.AlteraProdutoTabelaPersonalizada(GraTabApp,
  Nivel1: Integer; Nome: String; QueryNivel: TMySQLQuery;
  PreCadastra: Boolean = False);
begin
  //Compatibilidade
  //imprimir diferencas de baixa extra
end;

function TDfModAppGraG1.GrandesasInconsistentes(SQL: array of String; DB:
  TmySQLDataBase): Boolean;
var
  SQL_PeriodoVMI: String;
  Forma: Integer;
begin
  Result := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrGrandz, DB, SQL);
  //Geral.MB_SQL(nil, QrErrGrandz);
  if QrErrGrandz.RecordCount > 0 then
  begin
    Geral.MB_Info('QrErrGrandz.SQL.Text:' + sLineBreak + QrErrGrandz.SQL.Text);
    Result := True;
    Forma := MyObjects.SelRadioGroup('Grandezas inesperadas', '', ['Relat�rio',
    'Grade edit�vel'], 2);
    case Forma of
      0:
      begin
        MyObjects.frxDefineDataSets(frxErrGrandz, [
          DModG.frxDsDono,
          frxDsErrGrandz
        ]);
        MyObjects.frxMostra(DfModAppGraG1.frxErrGrandz, 'Grandezas inesperadas');
      end;
      1: UMedi_PF.MostraFormUnidMedMul(SQL, DB);
    end;
  end;
end;

function TDfModAppGraG1.InsAltUsoConsumoCab(Data, DataE, refNFe: String; UpdCodigo, IQ,
  CI, Transportadora, NF, Conhecimento, Pedido, TipoNF, modNF, Serie, NF_RP,
  NF_CC, HowLoad: Integer; Frete, PesoB, PesoL, ValorNF, ICMS: Double;
  SQLType: TSQLType; FatID, FatNum, Empresa: Integer): Boolean;
var
  Qry: TmySQLQuery;
  MySQLType: TSQLType;
  Filial, Codigo: Integer;
  TabLctA: String;
begin
  Result := False;
  if (FatID=0) or (FatNum=0) or (Empresa=0) then
  begin
    Geral.MB_Erro('Tentativa de ins/upd em PQE inv�lida!');
    Exit;
  end;
  Filial := DModG.ObtemFilialDeEntidade(Empresa);
  TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  if SQLType = stUpd then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM pqe ',
      'WHERE FatID=' + Geral.FF0(FatID),
      'AND FatNum=' + Geral.FF0(FatNum),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
      Codigo := Qry.FieldByName('Codigo').AsInteger;
      if Codigo <> 0 then
        MySQLType := stUpd
      else
        MySQLType := stIns;
    finally
      Qry.Free;
    end;
  end else
    MySQLType := stIns;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('pqe', 'Codigo', MySQLType, Codigo);
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, MySQLType, 'pqe', False, [
  'Data', 'DataE', 'refNFe',
  'Codigo', 'IQ', 'CI',
  'Transportadora', 'NF', 'Conhecimento',
  'Pedido', 'TipoNF', 'modNF',
  'Serie', 'NF_RP', 'NF_CC',
  'HowLoad', 'Frete', 'PesoB',
  'PesoL', 'ValorNF', 'ICMS'], [
  'FatID', 'FatNum', 'Empresa'], [
  Data, DataE, refNFe,
  Codigo, IQ, CI,
  Transportadora, NF, Conhecimento,
  Pedido, TipoNF, modNF,
  Serie, NF_RP, NF_CC,
  HowLoad, Frete, PesoB,
  PesoL, ValorNF, ICMS], [
  FatID, FatNum, Empresa], True);
  //
  if Result then
    InsAltUsoConsumoCalculaDiferencas(apNinguem, Codigo, TabLctA);
end;

function TDfModAppGraG1.InsAltUsoConsumoCalculaDiferencas(tPag: TAquemPag;
  Codigo: Integer; TabLctA: String): Double;
var
  Qry: TmySQLQuery;
  //
  ConfTPesoL, ConfTPesoB, ConfTTotalCusto, SumFDebito, SumTDebito, PQEValorNF,
  PQEFrete, PQEJuros, PQEPesoB, PQEPesoL, ErrNota, ErrBruto, ErrLiq, ErrPagT,
  ErrPagF: Double;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ValorNF, Frete, Juros, PesoB, PesoL ',
    'FROM pqe ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    PQEValorNF  := Qry.FieldByName('ValorNF').AsFloat;
    PQEFrete    := Qry.FieldByName('Frete').AsFloat;
    PQEJuros    := Qry.FieldByName('Juros').AsFloat;
    PQEPesoB    := Qry.FieldByName('PesoB').AsFloat;
    PQEPesoL    := Qry.FieldByName('PesoL').AsFloat;
    InsAltUsoConsumoDistribuiCustoDoFrete(Codigo, PQEValorNF, PQEJuros, PQEFrete,
      PQEPesoB, PQEPesoL);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(TotalPeso) PesoL, ',
    'SUM(Volumes*PesoVB) PesoB, ',
    'SUM(TotalCusto) TotalCusto ',
    'FROM pqeits ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    ConfTPesoL      := Qry.FieldByName('PesoL').AsFloat;
    ConfTPesoB      := Qry.FieldByName('PesoB').AsFloat;
    ConfTTotalCusto := Qry.FieldByName('TotalCusto').AsFloat;
    //
    ErrPagT := 0;
    ErrPagF := 0;
    if UFinanceiro.TabLctNaoDef(TabLctA, False) then
      Exit;
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(Debito) Debito ',
      'FROM ' + TabLctA,
      'WHERE FatID=' + Geral.FF0(VAR_FATID_1001),
      'AND FatNum=' + Geral.FF0(Codigo),
      '']);
      SumFDebito := Qry.FieldByName('Debito').AsFloat;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(Debito) Debito ',
      'FROM ' + TabLctA,
      'WHERE FatID=' + Geral.FF0(VAR_FATID_1002),
      'AND FatNum=' + Geral.FF0(Codigo),
      '']);
      SumTDebito := Qry.FieldByName('Debito').AsFloat;
      //
      if SumTDebito <> 0 then
        ErrPagT := SumTDebito - PQEFrete;
      if SumFDebito <> 0 then
        ErrPagF := SumFDebito - PQEValorNF;
    end;
    ErrNota := ConfTTotalCusto - PQEValorNF + PQEJuros - PQEFrete;
    // NFe n�o tem peso bruto por item
    if ConfTPesoB = 0 then
      ErrBruto := 0
    else
      ErrBruto := ConfTPesoB - PQEPesoB;
    ErrLiq := ConfTPesoL - PQEPesoL;
(*
    ErrNota   := N;
    ErrBruto  := B;
    ErrLiq    := L;
    ErrPagT   := T;
    ErrPagF   := X;
*)
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pqe', False, [
    'ErrNota', 'ErrBruto', 'ErrLiq',
    'ErrPagT', 'ErrPagF'], [
    'Codigo'], [
    ErrNota, ErrBruto, ErrLiq,
    ErrPagT, ErrPagF], [
    Codigo], True);
    //
    if tPag = apCliente then result := ErrPagF else
    if tPag = apFornece then result := ErrPagF else
    if tPag = apTranspo then result := ErrPagT else
    if tPag = apNinguem then result := 0 else
    Result := 0;
  finally
    Qry.Free;
  end;
end;

procedure TDfModAppGraG1.InsAltUsoConsumoDistribuiCustoDoFrete(Codigo: Integer;
  PQEValorNF, PQEJuros, PQEFrete, PQEPesoB, PQEPesoL: Double);
var
  Frete, CFin: Double;
  C_TXT, F_TXT: String;
begin
  if (PQEValorNF - PQEJuros) > 0 then
    CFin := (PQEJuros / (PQEValorNF - PQEJuros)) * 100
  else CFin := 0;
  ///
  Frete := 0;
  if PQEPesoL > 0 then
  Frete := (PQEFrete / PQEPesoL);
  //
  C_TXT := Geral.FFT_Dot(CFin, 2, siNegativo);
  F_TXT := Geral.FFT_Dot(Frete, 2, siNegativo);
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdW, Dmod.MyDB, [
  'UPDATE pqeits SET ',
  'TotalCusto=((ValorItem + IPI_vIPI) * (1+(' + C_TXT + ' /100))) ',
  ' + (PesoVL * Volumes * ' + F_TXT + '), ',
  'CFin=' +  C_TXT,
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

function TDfModAppGraG1.InsAltUsoConsumoExcluiItem(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  if (FatID = 0) or (FatNum = 0) or (Empresa = 0) or (nItem = 0) then
  begin
    Geral.MB_ERRO('N�o foi poss�vel excluir o item de "PQEIts"!');
    Exit;
  end;
  Result := UMyMod.ExcluiRegistroIntArr('', 'pqeits', [
  'FatID', 'FatNum', 'Empresa', 'nItem'
  ], [
  FatID, FatNum, Empresa, nItem
  ]) = ID_YES;
end;

function TDfModAppGraG1.InsAltUsoConsumoFinalizaEntrada(Pergunta: Boolean;
  SQLType: TSQLType; Codigo: Integer): Boolean;
var
  Atualiza: Boolean;
  DataX, DtCorrApo, xLote, dFab, dVal: String;
  Qry1, Qry2: TmySQLQuery;
  Insumo, Empresa, CliOrig, CliDest, HowLoad, Controle, OrigemCodi,
  OrigemCtrl, Tipo, ide_serie, ide_nNF: Integer;
  Data: TDateTime;
  Peso, Valor, PQEValorNF, PQEFrete, PQEJuros, PQEPesoB, PQEPesoL: Double;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  try
  Qry2 := TmySQLQuery.Create(Dmod);
  try
    if (SQLType <> stIns) and Pergunta then
    begin
      Atualiza := Geral.MB_Pergunta(
      'Deseja atualizar o(s) pre�o(s) do(s) insumo(s)?') = ID_YES;
    end else
      Atualiza := False;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT Empresa, CI, Data, HowLoad, ',
    'ValorNF, Frete, Juros, PesoB, PesoL ',
    // ini 2023-10-20
    ', Serie, NF ',
    // fim 2023-10-20
    'FROM pqe ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    Empresa     := Qry1.FieldByName('Empresa').AsInteger;
    CliOrig     := Qry1.FieldByName('CI').AsInteger;
    CliDest     := Qry1.FieldByName('CI').AsInteger;
    HowLoad     := Qry1.FieldByName('HowLoad').AsInteger;
    Data        := Qry1.FieldByName('Data').AsDateTime;
    PQEValorNF  := Qry1.FieldByName('ValorNF').AsFloat;
    PQEFrete    := Qry1.FieldByName('Frete').AsFloat;
    PQEJuros    := Qry1.FieldByName('Juros').AsFloat;
    PQEPesoB    := Qry1.FieldByName('PesoB').AsFloat;
    PQEPesoL    := Qry1.FieldByName('PesoL').AsFloat;
    ide_serie   := Qry1.FieldByName('Serie').AsInteger;
    ide_nNF     := Qry1.FieldByName('NF').AsInteger;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT DISTINCT Insumo ',
    'FROM pqx ',
    'WHERE Tipo=' + Geral.FF0(VAR_FATID_0010) + ' AND OrigemCodi='+ Geral.FF0(Codigo),
    '']);
(*
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdM, Dmod.MyDB, [
    'DELETE FROM pqx ',
    'WHERE Tipo=' + Geral.FF0(VAR_FATID_0010),
    'AND OrigemCodi=' + Geral.FF0(Codigo),
    '']);
*)
    PQ_PF.ExcluiPQx_Mul(nil, Codigo, VAR_FATID_0010, PB1, False);
    //
    while not Qry1.Eof do
    begin
      Insumo := Qry1.FieldByName('Insumo').AsInteger;
      // MsgNenhum pois ir� incluir depois !
      UnPQx.AtualizaEstoquePQ(Empresa, Insumo, Empresa, aeNenhum, CO_VAZIO);
      Qry1.Next;
    end;
    //
    InsAltUsoConsumoDistribuiCustoDoFrete(Codigo, PQEValorNF, PQEJuros, PQEFrete,
      PQEPesoB, PQEPesoL);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPQEIts, Dmod.MyDB, [
    'SELECT pq.Nome NOMEPQ, ei.* ',
    'FROM pqeits ei ',
    'LEFT JOIN pq ON pq.Codigo=ei.Insumo ',
    'WHERE ei.Codigo=' + Geral.FF0(Codigo),
    'ORDER BY Conta ',
    '']);
    //
    QrPQEIts.DisableControls;
    QrPQEIts.First;
    DataX := Geral.FDT(Data, 1);
    while not QrPQEIts.Eof do
    begin
      Insumo     := QrPQEItsInsumo.Value;
      Peso       := QrPQEItsTotalPeso.Value;
      Valor      := QrPQEItsTotalCusto.Value;
      Controle   := QrPQEItsControle.Value;
      OrigemCodi := Codigo;
      OrigemCtrl := Controle;
      Tipo       := VAR_FATID_0010;
      DtCorrApo  := Geral.FDT(QrPQEItsDtCorrApo.Value, 1);
      //
      dFab       := Geral.FDT(QrPQEItsdFab.Value, 1);
      dVal       := Geral.FDT(QrPQEItsdFab.Value, 1);
      xLote      := QrPQEItsxLote.Value;
{
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'p q x', False, [
      'DataX', 'CliOrig', 'CliDest',
      'Insumo', 'Peso', 'Valor',
      (*'Retorno', 'StqMovIts', 'RetQtd',*)
      'HowLoad'(*, 'StqCenLoc', 'SdoPeso',
      'SdoValr', 'AcePeso', 'AceValr'*)], [
      'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
      DataX, CliOrig, CliDest,
      Insumo, Peso, Valor,
      (*Retorno, StqMovIts, RetQtd,*)
      HowLoad(*, StqCenLoc, SdoPeso,
      SdoValr, AcePeso, AceValr*)], [
      OrigemCodi, OrigemCtrl, Tipo], False) then
      begin
        PQ_PF.AtualizaSdoPQx(Qry1, OrigemCodi, OrigemCtrl, Tipo);
      end;
}
      PQ_PF.InserePQx_Inn(Qry1, DataX, CliOrig, CliDest, Insumo, Peso, Valor,
      HowLoad, OrigemCodi, OrigemCtrl, Tipo, DtCorrApo, Empresa,
      ide_serie, ide_nNF, xLote, dFab, dVal);
      //
      QrPQEIts.Next;
    end;
    QrPQEIts.First;
    if Atualiza then Atualiza := BDC_ATUALIZA_PRECO_PQ;
    while not QrPQEIts.Eof do
    begin
      // Desabilitado 2016-02-13 ninguem usa!
      //AtualizaEntradaPedidoIts(QrPQEItsInsumo.Value);
      //
      /// Pre�o m�dio e �ltimo pre�o GraGruX
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM gragrux ',
      'WHERE GraGru1=' + Geral.FF0(QrPQEItsInsumo.Value),
      '']);
      while not Qry1.Eof do
      begin
        DmodG.AtualizaPrecosGraGruVal2(Qry1.FieldByName('Controle').AsInteger, Empresa);
        Qry1.Next;
      end;
      /// FIM Pre�o m�dio e �ltimo pre�o GraGruX
      //
      QrPQEIts.Next;
    end;
    QrPQEIts.EnableControls;
    // Desabilitado 2016-02-13 ninguem usa!
    //AtualizaEntradaPedido;
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE pqe ',
    'SET Cancelado = "F" ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    InsAltUsoConsumoDistribuiCustoDoFrete(Codigo, PQEValorNF, PQEJuros, PQEFrete,
      PQEPesoB, PQEPesoL);
    QrPQEIts.First;
    while not QrPQEIts.Eof do
    begin
      UnPQx.AtualizaEstoquePQ(Empresa, QrPQEItsInsumo.Value, Empresa, aeMsg, CO_VAZIO);
      //
      QrPQEIts.Next;
    end;
    //
    Result := True;
  finally
    Qry2.Free;
  end;
  finally
    Qry1.Free;
  end;
end;

function TDfModAppGraG1.InsAltUsoConsumoIts(FatID, FatNum, Empresa, nItem,
  OriCtrl: Integer; GraGruX, Volumes: Integer; PesoVB, PesoVL, ValorItem,
  IPI_pIPI, IPI_vIPI, TotalCusto, TotalPeso: Double; prod_CFOP: Integer;
  SQLType: TSQLType): Boolean;
var
  Qry: TmySQLQuery;
  MySQLType: TSQLType;
  Codigo, Controle, Conta, Insumo: Integer;
begin
  Result := False;
  if (FatID=0) or (FatNum=0) or (Empresa=0) then
  begin
    Geral.MB_Erro('Tentativa de ins/upd em PQE inv�lida!');
    Exit;
  end;
  MySQLType := stIns;
  Qry := TmySQLQuery.Create(Dmod);
  try
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo, Controle ',
      'FROM pqeits ',
      'WHERE FatID=' + Geral.FF0(FatID),
      'AND FatNum=' + Geral.FF0(FatNum),
      'AND Empresa=' + Geral.FF0(Empresa),
      'AND nItem=' + Geral.FF0(nItem),
      '']);
    end;
    Codigo   := Qry.FieldByName('Codigo').AsInteger;
    Controle := Qry.FieldByName('Controle').AsInteger;
    if (SQLType = stUpd) and (Controle <> 0) then
      MySQLType := stUpd;
    if (SQLType = stIns) and (Codigo = 0) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM pqe ',
      'WHERE FatID=' + Geral.FF0(FatID),
      'AND FatNum=' + Geral.FF0(FatNum),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
      //Geral.MB_SQL(nil, Qry);
      Codigo   := Qry.FieldByName('Codigo').AsInteger;
    end;
    //
  finally
    Qry.Free;
  end;
  //
  Insumo := DmProd.ObtemGraGru1DeGraGruX(GraGruX);
  Controle := UMyMod.BuscaEmLivreY_Def('pqeits', 'Controle', MySQLType, Controle);
  Conta := nItem;
  if (Codigo = 0) or (Controle = 0) or (nItem = 0) or (Insumo = 0) then
  begin
    Result := False;
    if DmNFe_0000.ExcluiEntradaItsDeNFe(FatID, FatNum, Empresa, nItem, OriCtrl) then
    begin
      Result := False;
      Geral.MB_ERRO(
      'N�o foi posss�vel inserir o registro de estoque na tabela de insumos qu�micos!');
      Exit;
    end;
  end else
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, MySQLType, 'pqeits', False, [
    'Insumo', 'Volumes', 'PesoVB',
    'PesoVL', 'ValorItem', 'IPI_pIPI',
    'IPI_vIPI', 'TotalCusto', 'TotalPeso',
    'prod_CFOP', 'Codigo', 'Conta',
    'FatID', 'FatNum', 'Empresa',
    'nItem'
    ], ['Controle'], [
    Insumo, Volumes, PesoVB,
    PesoVL, ValorItem, IPI_pIPI,
    IPI_vIPI, TotalCusto, TotalPeso,
    prod_CFOP, Codigo, Conta,
    FatID, FatNum, Empresa,
    nItem
    ], [Controle], True);
  end;
end;

function TDfModAppGraG1.NCMsInconsistentes(SQL: array of String; DB:
  TmySQLDataBase): Boolean;
var
  SQL_PeriodoVMI: String;
  Forma: Integer;
begin
  Result := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrNCMs, DB, SQL);
  //Geral.MB_SQL(nil, QrErrNCMs);
  if QrErrNCMs.RecordCount > 0 then
  begin
    Geral.MB_Info('QrErrNCMs.SQL.Text:' + sLineBreak + QrErrNCMs.SQL.Text);
    Result := True;
    Forma := MyObjects.SelRadioGroup('NCMs inesperados', '', ['Relat�rio',
    'Grade edit�vel'], 2);
    case Forma of
      0:
      begin
        MyObjects.frxDefineDataSets(frxErrNCMs, [
          DModG.frxDsDono,
          frxDsErrNCMs
        ]);
        MyObjects.frxMostra(DfModAppGraG1.frxErrNCMs, 'NCMs inesperados');
      end;
      1: Grade_Jan.MostraFormNCM_Mul(SQL, DB);
    end;
  end;
end;

function TDfModAppGraG1.PreparaUpdUsoConsumoCab(FatID, FatNUm, Empresa: Integer;
  EdNF_RP, EdNF_CC, EdConhecimento, EdFrete, EdPesoL, EdPesoB: TdmkEdit(*;
  EdCodInfoCliI: TdmkEditCB; CBCodInfoCliI: TdmkDBLookupComboBox*)): Boolean;
var
  Qry: TmySQLQuery;
  MySQLType: TSQLType;
  Codigo: Integer;
begin
  Result := False;
  if (FatID=0) or (FatNum=0) or (Empresa=0) then
  begin
    Geral.MB_Erro('Tentativa de prepara��o de upd em PQE inv�lida!');
    Exit;
  end;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT NF_RP, NF_CC, Conhecimento, Frete, PesoL, PesoB ',
    'FROM pqe ',
    'WHERE FatID=' + Geral.FF0(FatID),
    'AND FatNum=' + Geral.FF0(FatNum),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
    EdNF_RP.ValueVariant        := Qry.FieldByName('NF_RP').AsInteger;
    EdNF_CC.ValueVariant        := Qry.FieldByName('NF_CC').AsInteger;
    EdConhecimento.ValueVariant := Qry.FieldByName('Conhecimento').AsInteger;
    EdFrete.ValueVariant        := Qry.FieldByName('Frete').AsFloat;
    EdPesoL.ValueVariant        := Qry.FieldByName('PesoL').AsFloat;
    EdPesoB.ValueVariant        := Qry.FieldByName('PesoB').AsFloat;
    //EdCodInfoCliI.ValueVariant  := Qry.FieldByName('CodInfoCliI').AsInteger;
    //CBCodInfoCliI.KeyValue      := Qry.FieldByName('CodInfoCliI').AsInteger;
  finally
    Qry.Free;
  end;
end;

end.
