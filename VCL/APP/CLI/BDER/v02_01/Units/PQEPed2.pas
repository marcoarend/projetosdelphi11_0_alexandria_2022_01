unit PQEPed2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DBCtrls, Db, (*DBTables,*) Grids, DBGrids, ExtCtrls,
  dmkGeral, mySQLDbTables, Variants, dmkImage, UnDmkEnums, DmkDAC_PF, dmkEdit,
  Vcl.ComCtrls, dmkDBGrid;

type
  TFmPQEPed2 = class(TForm)
    QrFornece: TmySQLQuery;
    QrPedidos: TmySQLQuery;
    DsFornece: TDataSource;
    DsPedidos: TDataSource;
    QrPedidosCodigo: TIntegerField;
    Panel2: TPanel;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTIDADE: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    TabSheet2: TTabSheet;
    PainelDados: TPanel;
    Label1: TLabel;
    DBGrid1: TDBGrid;
    Panel5: TPanel;
    EdCodUsu: TdmkEdit;
    Label2: TLabel;
    EdReferencia: TdmkEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    QrPediVda: TMySQLQuery;
    QrPediVdaCodigo: TIntegerField;
    QrPediVdaCodUsu: TIntegerField;
    QrPediVdaCliente: TIntegerField;
    QrPediVdaDtaEntra: TDateField;
    QrPediVdaDtaPrevi: TDateField;
    QrPediVdaObserva: TWideMemoField;
    QrPediVdaQuantP: TFloatField;
    QrPediVdaValLiq: TFloatField;
    QrPediVdaReferenPedi: TWideStringField;
    QrPediVdaNO_ENT: TWideStringField;
    DsPediVda: TDataSource;
    DBMemo1: TDBMemo;
    QrPediVdaGru: TMySQLQuery;
    QrPediVdaGruCodUsu: TIntegerField;
    QrPediVdaGruNome: TWideStringField;
    QrPediVdaGruNivel1: TIntegerField;
    QrPediVdaGruGRATAMCAD: TIntegerField;
    QrPediVdaGruQuantP: TFloatField;
    QrPediVdaGruValLiq: TFloatField;
    QrPediVdaGruItensCustomizados: TFloatField;
    QrPediVdaGruFracio: TSmallintField;
    QrPediVdaGruControle: TIntegerField;
    QrPediVdaGruValBru: TFloatField;
    QrPediVdaGruPrecoF: TFloatField;
    DsPediVdaGru: TDataSource;
    DBGGru: TDBGrid;
    QrPediVdaGruSaldoQtd: TFloatField;
    QrPediVdaIts: TMySQLQuery;
    QrPediVdaItsCodigo: TIntegerField;
    QrPediVdaItsControle: TIntegerField;
    QrPediVdaItsGraGruX: TIntegerField;
    QrPediVdaItsPrecoO: TFloatField;
    QrPediVdaItsPrecoR: TFloatField;
    QrPediVdaItsQuantP: TFloatField;
    QrPediVdaItsQuantC: TFloatField;
    QrPediVdaItsQuantV: TFloatField;
    QrPediVdaItsValBru: TFloatField;
    QrPediVdaItsDescoP: TFloatField;
    QrPediVdaItsDescoV: TFloatField;
    QrPediVdaItsValLiq: TFloatField;
    QrPediVdaItsPrecoF: TFloatField;
    QrPediVdaItsMedidaC: TFloatField;
    QrPediVdaItsMedidaL: TFloatField;
    QrPediVdaItsMedidaA: TFloatField;
    QrPediVdaItsMedidaE: TFloatField;
    QrPediVdaItsPercCustom: TFloatField;
    QrPediVdaItsCustomizad: TSmallintField;
    QrPediVdaItsInfAdCuztm: TIntegerField;
    QrPediVdaItsOrdem: TIntegerField;
    QrPediVdaItsReferencia: TWideStringField;
    QrPediVdaItsMulti: TIntegerField;
    QrPediVdaItsvProd: TFloatField;
    QrPediVdaItsvFrete: TFloatField;
    QrPediVdaItsvSeg: TFloatField;
    QrPediVdaItsvOutro: TFloatField;
    QrPediVdaItsvDesc: TFloatField;
    QrPediVdaItsvBC: TFloatField;
    BtAtzSdos: TBitBtn;
    PB1: TProgressBar;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBEncerradosClick(Sender: TObject);
    procedure CBFornecedorClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure QrPedidosAfterScroll(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure QrPediVdaGruAfterOpen(DataSet: TDataSet);
    procedure QrPediVdaGruBeforeClose(DataSet: TDataSet);
    procedure BtAtzSdosClick(Sender: TObject);
  private
    { Private declarations }
    FConfirma: Boolean;
    procedure ReopenPedidos(CodUsu: Integer; Referencia: String);
    procedure ReopenPQEItsDePediVda(Codigo: Integer);
  public
    { Public declarations }
  end;

var
  FmPQEPed2: TFmPQEPed2;

implementation

uses UnMyObjects, PQE, Module, ModPediVda;

{$R *.DFM}

procedure TFmPQEPed2.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQEPed2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FConfirma := False;
  //
  QrFornece.Params[0].AsInteger := 2;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPQEPed2.CBEncerradosClick(Sender: TObject);
begin
{
  QrFornece.Close;
  QrPedidos.Close;
  if CBEncerrados.Checked then QrFornece.Params[0].AsInteger := 3
  else QrFornece.Params[0].AsInteger := 2;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
}
end;

procedure TFmPQEPed2.CBFornecedorClick(Sender: TObject);
begin
{
  QrPedidos.Close;
  if CBFornecedor.KeyValue = NULL then CBFornecedor.KeyValue := 0;
  QrPedidos.Params[0].AsInteger := CBFornecedor.KeyValue;
  if CBEncerrados.Checked then QrPedidos.Params[1].AsInteger := 3
  else QrPedidos.Params[1].AsInteger := 2;
  UnDmkDAC_PF.AbreQuery(QrPedidos, Dmod.MyDB);
}
end;

procedure TFmPQEPed2.DBGrid1DblClick(Sender: TObject);
begin
  if (QrPediVda.State <> dsInactive) and (QrPediVda.RecordCount > 0) then
  begin
    FConfirma := True;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPediVdaGru, Dmod.MyDB, [
    'SELECT pvi.Controle, SUM(QuantP) QuantP, ',
    'SUM(ValLiq) ValLiq, SUM(ValBru) ValBru, ',
    'SUM(Customizad) ItensCustomizados, ',
    'gti.Codigo GRATAMCAD, ',
    'gg1.CodUsu, gg1.Nome, gg1.Nivel1, pgt.Fracio, ',
    'IF(QuantP <= 0, 0.00, vProd / QuantP) PrecoF, ',
    'SUM(QuantP - QuantV) SaldoQtd ',
    'FROM pedivdaits pvi ',
    'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX ',
    'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN prdgruptip   pgt ON pgt.Codigo=gg1.PrdGrupTip ',
    'WHERE pvi.Codigo=' + Geral.FF0(QrPediVdaCodigo.Value),
    'GROUP BY gg1.Nivel1 ',
    'ORDER BY pvi.Controle ',
    '']);
    //
    PageControl1.ActivePageIndex := 1;
  end;
end;

procedure TFmPQEPed2.BtAtzSdosClick(Sender: TObject);
var
  Pedido: Integer;
begin
  Pedido := QrPediVdaCodigo.Value;
  DmPediVda.AtualizaTodosItensPediVda_(Pedido, PB1);
  ReopenPQEItsDePediVda(Pedido);
end;

procedure TFmPQEPed2.BtConfirmaClick(Sender: TObject);
var
  Pedido : Integer;
begin
  if (QrPediVda.State <> dsInactive) and (QrPediVda.RecordCount > 0) and
  (QrPediVdaGru.State <> dsInactive) and (QrPediVdaGru.RecordCount > 0) then
    Pedido := QrPediVdaCodigo.Value
  else
    Pedido := 0;
  //
  if Pedido = 0 then
  begin
    Geral.MB_Aviso('Defina o Pedido!');
    PageControl1.ActivePageIndex := 0;
    Exit;
  end else
  begin
    ReopenPQEItsDePediVda(Pedido);
    if QrPediVdaIts.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Este pedido n�o tem mais nenhum item com saldo!');
      Exit;
    end;
    Hide;
    FmPQE.InsereRegCondicionado2(Pedido);
    Close;
  end;
end;

procedure TFmPQEPed2.QrPedidosAfterScroll(DataSet: TDataSet);
begin
{
  QrPedidosIts.Close;
  if CBPedido.KeyValue <> NULL then
  begin
    QrPedidosIts.Params[0].AsInteger := QrPedidosCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrPedidosIts, Dmod.MyDB);
  end;
}
end;

procedure TFmPQEPed2.QrPediVdaGruAfterOpen(DataSet: TDataSet);
begin
  BtConfirma.Enabled := QrPediVdaGru.RecordCount > 0;
  BtAtzSdos.Enabled  := QrPediVdaGru.RecordCount > 0;
end;

procedure TFmPQEPed2.QrPediVdaGruBeforeClose(DataSet: TDataSet);
begin
  BtConfirma.Enabled := False;
  BtAtzSdos.Enabled  := False;
end;

procedure TFmPQEPed2.ReopenPedidos(CodUsu: Integer; Referencia: String);
var
  SQL_Pesq: String;
begin
  if CodUsu <> 0 then
    SQL_Pesq := 'AND pvd.CodUsu=' + Geral.FF0(CodUsu)
  else
    SQL_Pesq := 'AND ReferenPedi LIKE "' + Referencia + '" ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediVda, Dmod.MyDB, [
  'SELECT pvd.Codigo, pvd.CodUsu, pvd.Cliente,  ',
  'pvd.DtaEntra, pvd.DtaPrevi, pvd.Observa,  ',
  'pvd.QuantP, pvd.ValLiq, pvd.ReferenPedi, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT  ',
  'FROM pedivda pvd ',
  'LEFT JOIN entidades ent ON ent.Codigo=pvd.Cliente ',
  'WHERE pvd.EntSai=0 ',
  SQL_Pesq,
  '']);
end;

procedure TFmPQEPed2.ReopenPQEItsDePediVda(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediVdaIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM pedivdaits',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND QuantP > QuantV ',
  'ORDER BY Controle ',
  '']);
end;

procedure TFmPQEPed2.SpeedButton1Click(Sender: TObject);
begin
  ReopenPedidos(EdCodUsu.ValueVariant, '');
end;

procedure TFmPQEPed2.SpeedButton2Click(Sender: TObject);
begin
  ReopenPedidos(0, EdReferencia.ValueVariant);
end;

procedure TFmPQEPed2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQEPed2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
