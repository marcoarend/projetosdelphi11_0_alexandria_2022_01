object FmCECTInn: TFmCECTInn
  Left = 339
  Top = 185
  Caption = 
    'PST-_CECT-001 :: Controle de Estoque de Couros de Terceiros - En' +
    'trada'
  ClientHeight = 659
  ClientWidth = 904
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 904
    Height = 439
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnPesquisa: TPanel
      Left = 0
      Top = 0
      Width = 904
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 904
        Height = 60
        Align = alClient
        Caption = ' Filtros de pesquisa: '
        TabOrder = 0
        object RGAbertos: TRadioGroup
          Left = 421
          Top = 15
          Width = 244
          Height = 43
          Align = alLeft
          Caption = ' Status: '
          Columns = 3
          ItemIndex = 2
          Items.Strings = (
            'Abertos'
            'Encerrados'
            'Ambos')
          TabOrder = 0
          OnClick = RGAbertosClick
        end
        object Panel3: TPanel
          Left = 2
          Top = 15
          Width = 419
          Height = 43
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object CBClientePesq: TdmkDBLookupComboBox
            Left = 60
            Top = 18
            Width = 353
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECLI'
            ListSource = DsClientesPesq
            TabOrder = 0
            dmkEditCB = EdClientePesq
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdClientePesq: TdmkEditCB
            Left = 4
            Top = 18
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClientePesqChange
            DBLookupComboBox = CBClientePesq
            IgnoraDBLookupComboBox = False
          end
        end
        object TPIni: TdmkEditDateTimePicker
          Left = 672
          Top = 32
          Width = 110
          Height = 21
          Date = 39750.896049618060000000
          Time = 39750.896049618060000000
          TabOrder = 2
          OnClick = TPIniClick
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPFim: TdmkEditDateTimePicker
          Left = 784
          Top = 32
          Width = 110
          Height = 21
          Date = 39750.896066076390000000
          Time = 39750.896066076390000000
          TabOrder = 3
          OnClick = TPFimClick
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object CkIni: TCheckBox
          Left = 672
          Top = 12
          Width = 110
          Height = 17
          Caption = 'Data inicial:'
          TabOrder = 4
          OnClick = CkIniClick
        end
        object CkFim: TCheckBox
          Left = 784
          Top = 12
          Width = 110
          Height = 17
          Caption = 'Data final:'
          TabOrder = 5
          OnClick = CkFimClick
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 60
      Width = 904
      Height = 379
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object PnEdita: TPanel
        Left = 0
        Top = 309
        Width = 904
        Height = 70
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        object Label2: TLabel
          Left = 12
          Top = 8
          Width = 65
          Height = 13
          Caption = 'Data entrada:'
        end
        object Label3: TLabel
          Left = 540
          Top = 8
          Width = 56
          Height = 13
          Caption = 'NF entrada:'
        end
        object Label4: TLabel
          Left = 608
          Top = 8
          Width = 48
          Height = 13
          Caption = 'Peso (kg):'
        end
        object Label5: TLabel
          Left = 128
          Top = 8
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object Label6: TLabel
          Left = 684
          Top = 8
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object Label7: TLabel
          Left = 748
          Top = 8
          Width = 50
          Height = 13
          Caption = 'Valor total:'
        end
        object Label8: TLabel
          Left = 832
          Top = 8
          Width = 67
          Height = 13
          Caption = 'Referente NF:'
        end
        object TPDataE: TdmkEditDateTimePicker
          Left = 12
          Top = 24
          Width = 112
          Height = 21
          Date = 39749.934630706020000000
          Time = 39749.934630706020000000
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdNFInn: TdmkEdit
          Left = 540
          Top = 24
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdInnQtdkg: TdmkEdit
          Left = 608
          Top = 24
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdClienteEdit: TdmkEditCB
          Left = 128
          Top = 24
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBClienteEdit
          IgnoraDBLookupComboBox = False
        end
        object CBClienteEdit: TdmkDBLookupComboBox
          Left = 184
          Top = 24
          Width = 352
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMECLI'
          ListSource = DsClientesEdit
          TabOrder = 2
          dmkEditCB = EdClienteEdit
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdInnQtdPc: TdmkEdit
          Left = 684
          Top = 24
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 1
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdInnQtdVal: TdmkEdit
          Left = 748
          Top = 24
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdNFRef: TdmkEdit
          Left = 832
          Top = 24
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CkContinuar: TCheckBox
          Left = 12
          Top = 48
          Width = 125
          Height = 17
          Caption = 'Continuar inserindo.'
          TabOrder = 8
          Visible = False
        end
      end
      object PnGrades: TPanel
        Left = 0
        Top = 0
        Width = 904
        Height = 309
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Splitter1: TSplitter
          Left = 0
          Top = 226
          Width = 904
          Height = 5
          Cursor = crVSplit
          Align = alBottom
          ExplicitLeft = 1
          ExplicitTop = 202
          ExplicitWidth = 900
        end
        object GradeInn: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 904
          Height = 226
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataE'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFInn'
              Title.Caption = 'NF Entrada'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InnQtdkg'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InnQtdPc'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InnQtdVal'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFRef'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoQtdkg'
              Title.Caption = 'Sdo kg'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoQtdPc'
              Title.Caption = 'Sdo pe'#231'as'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoQtdVal'
              Title.Caption = 'Sdo valor'
              Width = 72
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCECTInn
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataE'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFInn'
              Title.Caption = 'NF Entrada'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InnQtdkg'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InnQtdPc'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InnQtdVal'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFRef'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoQtdkg'
              Title.Caption = 'Sdo kg'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoQtdPc'
              Title.Caption = 'Sdo pe'#231'as'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoQtdVal'
              Title.Caption = 'Sdo valor'
              Width = 72
              Visible = True
            end>
        end
        object GradeIts: TDBGrid
          Left = 0
          Top = 231
          Width = 904
          Height = 78
          Align = alBottom
          DataSource = DsCECTInnIts
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Devolu'#231#227'o'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID Item'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataS'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFOut'
              Title.Caption = 'NF'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OutQtdkg'
              Title.Caption = 'Peso kg'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OutQtdPc'
              Title.Caption = 'Pe'#231'as'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OutQtdVal'
              Title.Caption = 'Valor'
              Width = 72
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 904
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 856
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 808
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 657
        Height = 32
        Caption = 'Controle de Estoque de Couros de Terceiros - Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 657
        Height = 32
        Caption = 'Controle de Estoque de Couros de Terceiros - Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 657
        Height = 32
        Caption = 'Controle de Estoque de Couros de Terceiros - Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 904
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 900
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBControla: TGroupBox
    Left = 0
    Top = 595
    Width = 904
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 900
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 756
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Inclui'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 132
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtDevolve: TBitBtn
        Left = 252
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Devolu'#231#245'es'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtDevolveClick
      end
      object BtImpressao: TBitBtn
        Tag = 5
        Left = 372
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Impress'#227'o'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtImpressaoClick
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 531
    Width = 904
    Height = 64
    Align = alBottom
    TabOrder = 4
    Visible = False
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 900
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel7: TPanel
        Left = 756
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn1: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn1Click
        end
      end
      object BitBtn2: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn2Click
      end
    end
  end
  object QrCECTInn: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCECTInnBeforeClose
    AfterScroll = QrCECTInnAfterScroll
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, RazaoSocial, cli.Nome) NOMECLI, inn.*'
      'FROM cectinn inn'
      'LEFT JOIN entidades cli ON inn.Cliente=cli.Codigo')
    Left = 16
    Top = 208
    object QrCECTInnNOMECLI: TWideStringField
      DisplayLabel = 'Cliente'
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCECTInnCodigo: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'Codigo'
      Required = True
    end
    object QrCECTInnCliente: TIntegerField
      DisplayLabel = 'C'#243'd.Cli.'
      FieldName = 'Cliente'
      Required = True
    end
    object QrCECTInnDataE: TDateField
      DisplayLabel = 'Data entrada'
      FieldName = 'DataE'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCECTInnNFInn: TIntegerField
      DisplayLabel = 'NF entrada'
      FieldName = 'NFInn'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCECTInnNFRef: TIntegerField
      DisplayLabel = 'NF Ref.'
      FieldName = 'NFRef'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCECTInnInnQtdkg: TFloatField
      DisplayLabel = 'kg entrada'
      FieldName = 'InnQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCECTInnInnQtdPc: TFloatField
      DisplayLabel = 'Pe'#231'as entrada'
      FieldName = 'InnQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCECTInnInnQtdVal: TFloatField
      DisplayLabel = 'Valor entrada'
      FieldName = 'InnQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCECTInnSdoQtdkg: TFloatField
      DisplayLabel = 'Saldo kg'
      FieldName = 'SdoQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCECTInnSdoQtdPc: TFloatField
      DisplayLabel = 'Saldo pe'#231'as'
      FieldName = 'SdoQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCECTInnSdoQtdVal: TFloatField
      DisplayLabel = 'Saldo Valor'
      FieldName = 'SdoQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsCECTInn: TDataSource
    DataSet = QrCECTInn
    Left = 44
    Top = 208
  end
  object QrClientesPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMECLI'
      'FROM entidades en '
      'WHERE en.Cliente2="V"'
      'ORDER BY NOMECLI')
    Left = 16
    Top = 264
    object QrClientesPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClientesPesqNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsClientesPesq: TDataSource
    DataSet = QrClientesPesq
    Left = 44
    Top = 264
  end
  object QrCECTInnIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT out.DataS, out.NFOut, its.*'
      'FROM cectoutits its'
      'LEFT JOIN cectout out ON out.Codigo=its.Codigo'
      'WHERE its.cectinn=:P0')
    Left = 16
    Top = 236
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCECTInnItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCECTInnItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCECTInnItsCECTInn: TIntegerField
      FieldName = 'CECTInn'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCECTInnItsOutQtdkg: TFloatField
      FieldName = 'OutQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCECTInnItsOutQtdPc: TFloatField
      FieldName = 'OutQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCECTInnItsOutQtdVal: TFloatField
      FieldName = 'OutQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00;  '
    end
    object QrCECTInnItsDataS: TDateField
      FieldName = 'DataS'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCECTInnItsNFOut: TIntegerField
      FieldName = 'NFOut'
      DisplayFormat = '000000;-000000; '
    end
  end
  object DsCECTInnIts: TDataSource
    DataSet = QrCECTInnIts
    Left = 44
    Top = 236
  end
  object QrClientesEdit: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMECLI'
      'FROM entidades en '
      'WHERE en.Cliente2="V"'
      'ORDER BY NOMECLI')
    Left = 72
    Top = 264
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsClientesEdit: TDataSource
    DataSet = QrClientesEdit
    Left = 100
    Top = 264
  end
  object frxCECTInn: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38928.750734444400000000
    ReportOptions.Name = 'Or'#231'amento'
    ReportOptions.LastChange = 38928.750734444400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      '  if <VFR_ORDENADO> then'
      '  begin'
      
        '    GroupHeader1.Visible := True;                               ' +
        '                                   '
      '    GroupFooter1.Visible := True;'
      '    //          '
      '    GroupHeader1.Condition := <VARF_GRUPO1>;            '
      '    MeGrupo1Head.Memo.Text := <VARF_HEADR1>;            '
      '    MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;            '
      '  end else begin'
      
        '    GroupHeader1.Visible := False;                              ' +
        '                                    '
      
        '    GroupFooter1.Visible := False;                              ' +
        '                                    '
      '  end;            '
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);'
      'end.')
    OnGetValue = frxCECTInnGetValue
    Left = 101
    Top = 208
    Datasets = <
      item
        DataSet = frxDsCECTInn
        DataSetName = 'frxDsCECTInn'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <
      item
        Name = ' MyVars'
        Value = Null
      end
      item
        Name = 'MeuLogo'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 64.251968500000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Picture1: TfrxPictureView
          Width = 166.299212600000000000
          Height = 64.251968500000000000
          ShowHint = False
          DataField = 'Logo'
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo2: TfrxMemoView
          Left = 181.417440000000000000
          Top = 3.779530000000001000
          Width = 377.952780310000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 181.417440000000000000
          Top = 26.456710000000000000
          Width = 117.165430000000000000
          Height = 22.677165350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Data / hora da impress'#227'o:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 298.582870000000000000
          Top = 26.456710000000000000
          Width = 377.952780310000000000
          Height = 22.677165350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[Date] [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Left = 559.370440000000000000
          Top = 3.779530000000001000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 30.236240000000000000
        Top = 438.425480000000000000
        Width = 718.110700000000000000
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 253.228510000000000000
        Width = 718.110700000000000000
        DataSet = frxDsCECTInn
        DataSetName = 'frxDsCECTInn'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 147.401626060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMECLI'
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCECTInn."NOMECLI"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 650.079160000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'SdoQtdVal'
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCECTInn."SdoQtdVal"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'SdoQtdPc'
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCECTInn."SdoQtdPc"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'SdoQtdkg'
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCECTInn."SdoQtdkg"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 464.882190000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NFRef'
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCECTInn."NFRef"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 393.071120000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'InnQtdVal'
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCECTInn."InnQtdVal"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 325.039580000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'InnQtdPc'
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCECTInn."InnQtdPc"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 257.008040000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'InnQtdkg'
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCECTInn."InnQtdkg"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 207.874150000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NFInn'
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCECTInn."NFInn"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 147.401670000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DataE'
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCECTInn."DataE"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 41.574830000000000000
        Top = 105.826840000000000000
        Width = 718.110700000000000000
        object Memo87: TfrxMemoView
          Width = 737.007910630000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle de Estoque de Couros de Terceiros')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 22.677179999999990000
          Width = 147.401626060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 650.079160000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Saldo valor')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 582.047620000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Saldo pe'#231'as')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 514.016080000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Saldo kg')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 464.882190000000000000
          Top = 22.677179999999990000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'NF Ref.')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 393.071120000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Valor entrada')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 325.039580000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pe'#231'as entrada')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 257.008040000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'kg entrada')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 207.874150000000000000
          Top = 22.677179999999990000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'NF Entr.')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 147.401670000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 34.015770000000000000
        Top = 381.732530000000000000
        Width = 718.110700000000000000
        object Memo30: TfrxMemoView
          Left = 653.858690000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCECTInn."SdoQtdVal">)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 585.827150000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCECTInn."SdoQtdPc">)]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 514.016080000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCECTInn."SdoQtdkg">)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 393.071120000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCECTInn."InnQtdVal">)]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 325.039580000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCECTInn."InnQtdPc">)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 257.008040000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCECTInn."InnQtdkg">)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Top = 7.559059999999988000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 22.677180000000000000
        Top = 207.874150000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsCECTInn."NOMECLI"'
        object MeGrupo1Head: TfrxMemoView
          Top = 3.779529999999994000
          Width = 714.331170000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '-   -   -   -')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 26.456710000000000000
        Top = 294.803340000000000000
        Width = 718.110700000000000000
        object Memo24: TfrxMemoView
          Left = 650.079160000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCECTInn."SdoQtdVal">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 582.047620000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCECTInn."SdoQtdPc">)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 514.016080000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCECTInn."SdoQtdkg">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 393.071120000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCECTInn."InnQtdVal">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 325.039580000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCECTInn."InnQtdPc">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 257.008040000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCECTInn."InnQtdkg">)]')
          ParentFont = False
        end
        object MeGrupo1Foot: TfrxMemoView
          Top = 3.779530000000022000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCECTInn
          DataSetName = 'frxDsCECTInn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total: -   -   -   -')
          ParentFont = False
        end
      end
    end
  end
  object frxDsCECTInn: TfrxDBDataset
    UserName = 'frxDsCECTInn'
    CloseDataSource = False
    DataSet = QrCECTInn
    BCDToCurrency = False
    Left = 72
    Top = 208
  end
  object PMImpressao: TPopupMenu
    Left = 436
    Top = 456
    object Imprimeestoquepesquisado1: TMenuItem
      Caption = '&Imprime estoque pesquisado'
      OnClick = Imprimeestoquepesquisado1Click
    end
    object Removeordenao1: TMenuItem
      Caption = '&Remove ordena'#231#227'o'
      OnClick = Removeordenao1Click
    end
  end
end
