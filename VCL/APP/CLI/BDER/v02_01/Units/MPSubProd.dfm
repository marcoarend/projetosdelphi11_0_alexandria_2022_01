object FmMPSubProd: TFmMPSubProd
  Left = 368
  Top = 194
  Caption = 'SUB-PRODU-001 :: Cadastro de Gera'#231#227'o de Sub-produtos'
  ClientHeight = 627
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 531
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 208
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label8: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label9: TLabel
        Left = 68
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label1: TLabel
        Left = 8
        Top = 44
        Width = 151
        Height = 13
        Caption = 'Reduzido do produto de origem:'
      end
      object Label4: TLabel
        Left = 12
        Top = 84
        Width = 182
        Height = 13
        Caption = 'Reduzido do produto que ser'#225' gerado:'
      end
      object Label5: TLabel
        Left = 12
        Top = 176
        Width = 84
        Height = 13
        Caption = 'Fator de gera'#231#227'o:'
      end
      object LaOriGer_Edit: TLabel
        Left = 208
        Top = 176
        Width = 83
        Height = 13
        Caption = '?????? / ??????'
      end
      object Label11: TLabel
        Left = 396
        Top = 176
        Width = 81
        Height = 13
        Caption = 'Arredondamento:'
      end
      object LaArred_Edit: TLabel
        Left = 592
        Top = 176
        Width = 36
        Height = 13
        Caption = '??????'
      end
      object EdNome: TdmkEdit
        Left = 68
        Top = 20
        Width = 713
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utIdx
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdGGX_Ori: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GGX_Ori'
        UpdCampo = 'GGX_Ori'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGGX_OriChange
        DBLookupComboBox = CBGGX_Ori
        IgnoraDBLookupComboBox = False
      end
      object CBGGX_Ori: TdmkDBLookupComboBox
        Left = 64
        Top = 60
        Width = 717
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGGX_Ori
        TabOrder = 3
        dmkEditCB = EdGGX_Ori
        QryCampo = 'GGX_Ori'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGGX_Ger: TdmkEditCB
        Left = 8
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GGX_Ger'
        UpdCampo = 'GGX_Ger'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGGX_GerChange
        DBLookupComboBox = CBGGX_Ger
        IgnoraDBLookupComboBox = False
      end
      object CBGGX_Ger: TdmkDBLookupComboBox
        Left = 64
        Top = 100
        Width = 717
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGGX_Ger
        TabOrder = 5
        dmkEditCB = EdGGX_Ger
        QryCampo = 'GGX_Ger'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGGrndzaBase: TdmkRadioGroup
        Left = 9
        Top = 125
        Width = 772
        Height = 40
        Caption = 
          ' Unidade de medida do produto de origem sobre o qual ser'#225' gerado' +
          ' o sub-produto: '
        Columns = 6
        Items.Strings = (
          '? ? ?'
          'Pe'#231'a'
          'm'#178
          'kg'
          't (ton)'
          'ft'#178)
        TabOrder = 6
        OnClick = RGGrndzaBaseClick
        QryCampo = 'GrndzaBase'
        UpdCampo = 'GrndzaBase'
        UpdType = utYes
        OldValor = 0
      end
      object EdFator: TdmkEdit
        Left = 100
        Top = 172
        Width = 101
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'Fator'
        UpdCampo = 'Fator'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdArredonda: TdmkEdit
        Left = 484
        Top = 172
        Width = 101
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0,001'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,001'
        QryCampo = 'Arredonda'
        UpdCampo = 'Arredonda'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.001000000000000000
        ValWarn = False
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 467
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 531
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 208
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label2: TLabel
        Left = 92
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label6: TLabel
        Left = 8
        Top = 44
        Width = 151
        Height = 13
        Caption = 'Reduzido do produto de origem:'
      end
      object Label7: TLabel
        Left = 12
        Top = 84
        Width = 182
        Height = 13
        Caption = 'Reduzido do produto que ser'#225' gerado:'
      end
      object Label10: TLabel
        Left = 12
        Top = 176
        Width = 84
        Height = 13
        Caption = 'Fator de gera'#231#227'o:'
      end
      object LaOriGer_Qry: TLabel
        Left = 208
        Top = 176
        Width = 83
        Height = 13
        Caption = '?????? / ??????'
      end
      object Label12: TLabel
        Left = 396
        Top = 176
        Width = 81
        Height = 13
        Caption = 'Arredondamento:'
      end
      object LaArred_Qry: TLabel
        Left = 588
        Top = 176
        Width = 36
        Height = 13
        Caption = '??????'
      end
      object DBEdNome: TdmkDBEdit
        Left = 92
        Top = 20
        Width = 688
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsMPSubProd
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdCodigo: TDBEdit
        Left = 8
        Top = 20
        Width = 80
        Height = 21
        DataField = 'Codigo'
        DataSource = DsMPSubProd
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        DataField = 'GGX_Ori'
        DataSource = DsMPSubProd
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 64
        Top = 60
        Width = 717
        Height = 21
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsShowOri
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 8
        Top = 100
        Width = 56
        Height = 21
        DataField = 'GGX_Ger'
        DataSource = DsMPSubProd
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 64
        Top = 100
        Width = 717
        Height = 21
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsShowGer
        TabOrder = 5
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 8
        Top = 124
        Width = 772
        Height = 40
        Caption = 
          ' Unidade de medida do produto de origem sobre o qual ser'#225' gerado' +
          ' o sub-produto: '
        Columns = 6
        DataField = 'GrndzaBase'
        DataSource = DsMPSubProd
        Items.Strings = (
          '? ? ?'
          'Pe'#231'a'
          'm'#178
          'kg'
          't (ton)'
          'ft'#178)
        ParentBackground = True
        TabOrder = 6
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object DBEdit5: TDBEdit
        Left = 100
        Top = 172
        Width = 101
        Height = 21
        DataField = 'Fator'
        DataSource = DsMPSubProd
        TabOrder = 7
      end
      object DBEdit6: TDBEdit
        Left = 480
        Top = 172
        Width = 102
        Height = 21
        DataField = 'Arredonda'
        DataSource = DsMPSubProd
        TabOrder = 8
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 467
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 269
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 469
        Height = 32
        Caption = 'Cadastro de Gera'#231#227'o de Sub-produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 469
        Height = 32
        Caption = 'Cadastro de Gera'#231#227'o de Sub-produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 469
        Height = 32
        Caption = 'Cadastro de Gera'#231#227'o de Sub-produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsMPSubProd: TDataSource
    DataSet = QrMPSubProd
    Left = 40
    Top = 12
  end
  object QrMPSubProd: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrMPSubProdBeforeOpen
    AfterOpen = QrMPSubProdAfterOpen
    BeforeClose = QrMPSubProdBeforeClose
    AfterScroll = QrMPSubProdAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM mpsubprod')
    Left = 12
    Top = 12
    object QrMPSubProdCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mpsubprod.Codigo'
    end
    object QrMPSubProdNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'mpsubprod.Nome'
      Size = 50
    end
    object QrMPSubProdGGX_Ori: TIntegerField
      FieldName = 'GGX_Ori'
      Origin = 'mpsubprod.GGX_Ori'
    end
    object QrMPSubProdGGX_Ger: TIntegerField
      FieldName = 'GGX_Ger'
      Origin = 'mpsubprod.GGX_Ger'
    end
    object QrMPSubProdGrndzaBase: TSmallintField
      FieldName = 'GrndzaBase'
      Origin = 'mpsubprod.GrndzaBase'
    end
    object QrMPSubProdFator: TFloatField
      FieldName = 'Fator'
      Origin = 'mpsubprod.Fator'
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrMPSubProdArredonda: TFloatField
      FieldName = 'Arredonda'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 68
    Top = 12
  end
  object QrGGX_Ori: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle')
    Left = 320
    Top = 108
    object QrGGX_OriControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGX_OriNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsGGX_Ori: TDataSource
    DataSet = QrGGX_Ori
    Left = 348
    Top = 108
  end
  object QrGGX_Ger: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, gg1.GerBxaEstq, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle')
    Left = 320
    Top = 136
    object QrGGX_GerControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGX_GerNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGX_GerGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
  end
  object DsGGX_Ger: TDataSource
    DataSet = QrGGX_Ger
    Left = 348
    Top = 136
  end
  object QrShowOri: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggx.Controle =:P0')
    Left = 508
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrShowOriNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsShowOri: TDataSource
    DataSet = QrShowOri
    Left = 536
    Top = 196
  end
  object QrShowGer: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.GerBxaEstq, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggx.Controle =:P0')
    Left = 508
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrShowGerNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrShowGerGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
  end
  object DsShowGer: TDataSource
    DataSet = QrShowGer
    Left = 536
    Top = 224
  end
end
