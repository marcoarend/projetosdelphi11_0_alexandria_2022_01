unit MPV2Bxa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, dmkEdit, Grids, DmkDAC_PF,
  DBGrids, Db, mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmMPV2Bxa = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    Panel4: TPanel;
    Label2: TLabel;
    dmkEdPecas: TdmkEdit;
    Label3: TLabel;
    dmkEdPeso: TdmkEdit;
    Label4: TLabel;
    dmkEdM2: TdmkEdit;
    Label5: TLabel;
    dmkEdP2: TdmkEdit;
    QrMarcas: TmySQLQuery;
    DsMarcas: TDataSource;
    QrMarcasControle: TIntegerField;
    QrMarcasMarca: TWideStringField;
    QrMarcasPcsSdo: TFloatField;
    QrMarcasPNFSdo: TFloatField;
    QrMarcasPLESdo: TFloatField;
    QrMarcasM2Sdo: TFloatField;
    QrMarcasP2Sdo: TFloatField;
    RGPeso: TRadioGroup;
    Panel5: TPanel;
    CkMostrarTodas: TCheckBox;
    EdMarca: TEdit;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure EdMarcaChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenQrMarcas(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmMPV2Bxa: TFmMPV2Bxa;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UMySQLModule, MPV2, UnInternalConsts;

procedure TFmMPV2Bxa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPV2Bxa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPV2Bxa.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmMPV2Bxa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPV2Bxa.DBGrid1DblClick(Sender: TObject);
begin
  dmkEdPecas.ValueVariant := QrMarcasPcsSdo.Value;
  case RGPeso.ItemIndex of
    0: dmkEdPeso.ValueVariant  := QrMarcasPNFSdo.Value;
    1: dmkEdPeso.ValueVariant  := QrMarcasPLESdo.Value;
    else dmkEdPeso.ValueVariant  := 0;
  end;
  dmkEdM2.ValueVariant    := QrMarcasM2Sdo.Value;
  dmkEdP2.ValueVariant    := QrMarcasP2Sdo.Value;
end;

procedure TFmMPV2Bxa.EdMarcaChange(Sender: TObject);
var
  Controle: Integer;
begin
  if QrMarcas.State = dsBrowse then
    Controle := QrMarcasControle.Value
  else
    Controle := 0;
  ReopenQrMarcas(Controle);
end;

procedure TFmMPV2Bxa.ReopenQrMarcas(Controle: Integer);
begin
  Screen.Cursor := crHourGlass;
  QrMarcas.Close;
  QrMarcas.SQL.Clear;
  QrMarcas.SQL.Add('SELECT Controle, Marca, (Pecas-PcBxa) PcsSdo,');
  QrMarcas.SQL.Add('(PNF-kgBxa) PNFSdo, (PLE-kgBxa) PLESdo, (M2-M2Bxa)');
  QrMarcas.SQL.Add('M2Sdo, (P2-P2Bxa) P2Sdo');
  QrMarcas.SQL.Add('FROM mpin');
  QrMarcas.SQL.Add('WHERE Marca LIKE "%' + EdMarca.Text + '%"');
  if not CkMostrarTodas.Checked then
    QrMarcas.SQL.Add('AND Pecas > PcBxa');
  QrMarcas.SQL.Add('ORDER BY Data DESC');
  UnDmkDAC_PF.AbreQuery(QrMarcas, Dmod.MyDB);
  Screen.Cursor := crDefault;
end;

procedure TFmMPV2Bxa.BtOKClick(Sender: TObject);
var
  Conta: Integer;
begin
  Conta := UMyMod.BuscaEmLivreY_Def('mpv2bxa', 'Conta', stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mpv2bxa', False,
  [
    'Codigo', 'Controle',
    'Estagio', 'Marca_ID',
    'Peso', 'Pecas',
    'M2', 'P2'
  ], ['conta'], [
    FmMPV2.QrMPV2Codigo.Value, FmMPV2.QrMPV2ItsControle.Value,
    FmMPV2.QrMPV2ItsEstagio.Value, QrMarcasControle.Value,
    dmkEdPeso.ValueVariant, dmkEdPecas.ValueVariant,
    dmkEdM2.ValueVariant, dmkEdP2.ValueVariant
  ], [Conta], True) then
  begin
    Dmod.AtualizaEstoqueMarca(QrMarcasControle.Value);
    ReopenQrMarcas(QrMarcasControle.Value);
    Application.MessageBox('Baixa realizada com sucesso!', 'Informação',
      MB_OK+MB_ICONINFORMATION);
  end;
end;

end.

