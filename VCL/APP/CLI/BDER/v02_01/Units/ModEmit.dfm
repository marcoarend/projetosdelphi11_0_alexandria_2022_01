object DmModEmit: TDmModEmit
  OldCreateOrder = False
  Height = 741
  Width = 930
  object QrLotes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ec.Controle, ec.MPIn, ec.Peso, ec.Pecas, '
      'mi.Ficha, mi.Marca, mi.Lote'
      'FROM emitcus ec'
      'LEFT JOIN mpin mi ON mi.Controle=ec.MPIn'
      'WHERE ec.Codigo=:P0')
    Left = 44
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotesControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLotesMPIn: TIntegerField
      FieldName = 'MPIn'
      Required = True
    end
    object QrLotesPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrLotesPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrLotesSourcMP: TIntegerField
      FieldName = 'SourcMP'
    end
    object QrLotesAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
  end
  object QrEmit: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM emit'
      'WHERE Codigo=0')
    Left = 460
    Top = 260
    object QrEmitCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmitDataEmis: TDateTimeField
      FieldName = 'DataEmis'
      Required = True
    end
    object QrEmitStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrEmitNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrEmitNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Required = True
      Size = 100
    end
    object QrEmitNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
      Required = True
    end
    object QrEmitTecnico: TWideStringField
      FieldName = 'Tecnico'
      Required = True
    end
    object QrEmitNOME: TWideStringField
      FieldName = 'NOME'
      Required = True
      Size = 100
    end
    object QrEmitClienteI: TIntegerField
      FieldName = 'ClienteI'
      Required = True
    end
    object QrEmitTipificacao: TSmallintField
      FieldName = 'Tipificacao'
      Required = True
    end
    object QrEmitTempoR: TIntegerField
      FieldName = 'TempoR'
      Required = True
    end
    object QrEmitTempoP: TIntegerField
      FieldName = 'TempoP'
      Required = True
    end
    object QrEmitSetor: TSmallintField
      FieldName = 'Setor'
      Required = True
    end
    object QrEmitEspessura: TWideStringField
      FieldName = 'Espessura'
      Required = True
      Size = 15
    end
    object QrEmitDefPeca: TWideStringField
      FieldName = 'DefPeca'
      Required = True
      Size = 15
    end
    object QrEmitPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrEmitCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrEmitQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrEmitAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrEmitFulao: TWideStringField
      FieldName = 'Fulao'
      Required = True
      Size = 5
    end
    object QrEmitObs: TWideStringField
      FieldName = 'Obs'
      Required = True
      Size = 255
    end
    object QrEmitTipific: TSmallintField
      FieldName = 'Tipific'
      Required = True
    end
    object QrEmitLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmitDataCad: TDateField
      FieldName = 'DataCad'
      Required = True
    end
    object QrEmitDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrEmitUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmitAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEmitAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitSetrEmi: TSmallintField
      FieldName = 'SetrEmi'
    end
    object QrEmitCod_Espess: TIntegerField
      FieldName = 'Cod_Espess'
    end
    object QrEmitCodDefPeca: TIntegerField
      FieldName = 'CodDefPeca'
    end
    object QrEmitCustoTo: TFloatField
      FieldName = 'CustoTo'
    end
    object QrEmitCustoKg: TFloatField
      FieldName = 'CustoKg'
    end
    object QrEmitCustoM2: TFloatField
      FieldName = 'CustoM2'
    end
    object QrEmitCusUSM2: TFloatField
      FieldName = 'CusUSM2'
    end
  end
  object QrEmitFlu: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEmitFluBeforeClose
    AfterScroll = QrEmitFluAfterScroll
    RequestLive = True
    SQL.Strings = (
      'SELECT * '
      'FROM _emit_flu_'
      'WHERE Codigo=0')
    Left = 460
    Top = 308
    object QrEmitFluCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmitFluControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmitFluOrdemTin: TIntegerField
      FieldName = 'OrdemTin'
      Required = True
    end
    object QrEmitFluOrdemFlu: TIntegerField
      FieldName = 'OrdemFlu'
      Required = True
    end
    object QrEmitFluTintasFlu: TIntegerField
      FieldName = 'TintasFlu'
      Required = True
    end
    object QrEmitFluNomeFlu: TWideStringField
      FieldName = 'NomeFlu'
      Required = True
      Size = 100
    end
    object QrEmitFluNomeTin: TWideStringField
      FieldName = 'NomeTin'
      Required = True
      Size = 30
    end
    object QrEmitFluTintasTin: TIntegerField
      FieldName = 'TintasTin'
      Required = True
    end
    object QrEmitFluGramasM2: TFloatField
      FieldName = 'GramasM2'
      Required = True
    end
    object QrEmitFluGramasTo: TFloatField
      FieldName = 'GramasTo'
      Required = True
    end
    object QrEmitFluCustoTo: TFloatField
      FieldName = 'CustoTo'
      Required = True
    end
    object QrEmitFluCustoKg: TFloatField
      FieldName = 'CustoKg'
      Required = True
    end
    object QrEmitFluAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrEmitFluCustoM2: TFloatField
      FieldName = 'CustoM2'
      Required = True
    end
    object QrEmitFluCusUSM2: TFloatField
      FieldName = 'CusUSM2'
      Required = True
    end
    object QrEmitFluLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmitFluDataCad: TDateField
      FieldName = 'DataCad'
      Required = True
    end
    object QrEmitFluDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitFluUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrEmitFluUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmitFluAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEmitFluAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitFluInfoCargM2: TFloatField
      FieldName = 'InfoCargM2'
    end
    object QrEmitFluDescriFlu: TWideStringField
      FieldName = 'DescriFlu'
      Size = 255
    end
    object QrEmitFluOpProc: TSmallintField
      FieldName = 'OpProc'
    end
  end
  object QrEmitIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _emit_its_'
      'WHERE Codigo=0')
    Left = 460
    Top = 356
    object QrEmitItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmitItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmitItsNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Required = True
      Size = 50
    end
    object QrEmitItsPQCI: TIntegerField
      FieldName = 'PQCI'
      Required = True
    end
    object QrEmitItsCli_Orig: TIntegerField
      FieldName = 'Cli_Orig'
      Required = True
    end
    object QrEmitItsCli_Dest: TIntegerField
      FieldName = 'Cli_Dest'
      Required = True
    end
    object QrEmitItsCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
      Required = True
    end
    object QrEmitItsMoedaPadrao: TIntegerField
      FieldName = 'MoedaPadrao'
      Required = True
    end
    object QrEmitItsNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrEmitItsOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrEmitItsPorcent: TFloatField
      FieldName = 'Porcent'
      Required = True
    end
    object QrEmitItsProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrEmitItsTempoR: TIntegerField
      FieldName = 'TempoR'
      Required = True
    end
    object QrEmitItsTempoP: TIntegerField
      FieldName = 'TempoP'
      Required = True
    end
    object QrEmitItspH: TFloatField
      FieldName = 'pH'
      Required = True
    end
    object QrEmitItsBe: TFloatField
      FieldName = 'Be'
      Required = True
    end
    object QrEmitItsObs: TWideStringField
      FieldName = 'Obs'
      Required = True
    end
    object QrEmitItsCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrEmitItsGraus: TFloatField
      FieldName = 'Graus'
      Required = True
    end
    object QrEmitItsBoca: TWideStringField
      FieldName = 'Boca'
      Required = True
      Size = 1
    end
    object QrEmitItsProcesso: TWideStringField
      FieldName = 'Processo'
      Required = True
      Size = 30
    end
    object QrEmitItsObsProces: TWideStringField
      FieldName = 'ObsProces'
      Required = True
      Size = 120
    end
    object QrEmitItsPeso_PQ: TFloatField
      FieldName = 'Peso_PQ'
      Required = True
    end
    object QrEmitItsProdutoCI: TIntegerField
      FieldName = 'ProdutoCI'
      Required = True
    end
    object QrEmitItspH_Min: TFloatField
      FieldName = 'pH_Min'
      Required = True
    end
    object QrEmitItspH_Max: TFloatField
      FieldName = 'pH_Max'
      Required = True
    end
    object QrEmitItsBe_Min: TFloatField
      FieldName = 'Be_Min'
      Required = True
    end
    object QrEmitItsBe_Max: TFloatField
      FieldName = 'Be_Max'
      Required = True
    end
    object QrEmitItsGC_Min: TFloatField
      FieldName = 'GC_Min'
      Required = True
    end
    object QrEmitItsGC_Max: TFloatField
      FieldName = 'GC_Max'
      Required = True
    end
    object QrEmitItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrEmitItsOrdemFlu: TIntegerField
      FieldName = 'OrdemFlu'
    end
    object QrEmitItsOrdemIts: TIntegerField
      FieldName = 'OrdemIts'
    end
    object QrEmitItsTintas: TIntegerField
      FieldName = 'Tintas'
    end
    object QrEmitItsGramasTi: TFloatField
      FieldName = 'GramasTi'
    end
    object QrEmitItsGramasKg: TFloatField
      FieldName = 'GramasKg'
    end
    object QrEmitItsGramasTo: TFloatField
      FieldName = 'GramasTo'
    end
    object QrEmitItsSiglaMoeda: TWideStringField
      FieldName = 'SiglaMoeda'
      Size = 10
    end
    object QrEmitItsPrecoMo_Kg: TFloatField
      FieldName = 'PrecoMo_Kg'
    end
    object QrEmitItsCotacao_Mo: TFloatField
      FieldName = 'Cotacao_Mo'
    end
    object QrEmitItsCustoRS_Kg: TFloatField
      FieldName = 'CustoRS_Kg'
    end
    object QrEmitItsCustoRS_To: TFloatField
      FieldName = 'CustoRS_To'
    end
    object QrEmitItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmitItsDataCad: TDateField
      FieldName = 'DataCad'
      Required = True
    end
    object QrEmitItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrEmitItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmitItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEmitItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitItsDiluicao: TFloatField
      FieldName = 'Diluicao'
    end
    object QrEmitItsMinimo: TFloatField
      FieldName = 'Minimo'
    end
    object QrEmitItsMaximo: TFloatField
      FieldName = 'Maximo'
    end
  end
  object frxDsEmit: TfrxDBDataset
    UserName = 'frxDsEmit'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'DataEmis=DataEmis'
      'Status=Status'
      'Numero=Numero'
      'NOMECI=NOMECI'
      'NOMESETOR=NOMESETOR'
      'Tecnico=Tecnico'
      'NOME=NOME'
      'ClienteI=ClienteI'
      'Tipificacao=Tipificacao'
      'TempoR=TempoR'
      'TempoP=TempoP'
      'Setor=Setor'
      'Espessura=Espessura'
      'DefPeca=DefPeca'
      'Peso=Peso'
      'Custo=Custo'
      'Qtde=Qtde'
      'AreaM2=AreaM2'
      'Fulao=Fulao'
      'Obs=Obs'
      'Tipific=Tipific'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SetrEmi=SetrEmi'
      'Cod_Espess=Cod_Espess'
      'CodDefPeca=CodDefPeca'
      'CustoTo=CustoTo'
      'CustoKg=CustoKg'
      'CustoM2=CustoM2'
      'CusUSM2=CusUSM2')
    DataSet = QrEmit
    BCDToCurrency = False
    Left = 536
    Top = 260
  end
  object frxDsEmitFlu: TfrxDBDataset
    UserName = 'frxDsEmitFlu'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'OrdemTin=OrdemTin'
      'OrdemFlu=OrdemFlu'
      'TintasFlu=TintasFlu'
      'NomeFlu=NomeFlu'
      'NomeTin=NomeTin'
      'TintasTin=TintasTin'
      'GramasM2=GramasM2'
      'GramasTo=GramasTo'
      'CustoTo=CustoTo'
      'CustoKg=CustoKg'
      'AreaM2=AreaM2'
      'CustoM2=CustoM2'
      'CusUSM2=CusUSM2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'InfoCargM2=InfoCargM2'
      'DescriFlu=DescriFlu'
      'OpProc=OpProc')
    DataSet = QrEmitFlu
    BCDToCurrency = False
    Left = 536
    Top = 308
  end
  object frxDsEmitIts: TfrxDBDataset
    UserName = 'frxDsEmitIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'NomePQ=NomePQ'
      'PQCI=PQCI'
      'Cli_Orig=Cli_Orig'
      'Cli_Dest=Cli_Dest'
      'CustoPadrao=CustoPadrao'
      'MoedaPadrao=MoedaPadrao'
      'Numero=Numero'
      'Ordem=Ordem'
      'Porcent=Porcent'
      'Produto=Produto'
      'TempoR=TempoR'
      'TempoP=TempoP'
      'pH=pH'
      'Be=Be'
      'Obs=Obs'
      'Custo=Custo'
      'Graus=Graus'
      'Boca=Boca'
      'Processo=Processo'
      'ObsProces=ObsProces'
      'Peso_PQ=Peso_PQ'
      'ProdutoCI=ProdutoCI'
      'pH_Min=pH_Min'
      'pH_Max=pH_Max'
      'Be_Min=Be_Min'
      'Be_Max=Be_Max'
      'GC_Min=GC_Min'
      'GC_Max=GC_Max'
      'GraGruX=GraGruX'
      'OrdemFlu=OrdemFlu'
      'OrdemIts=OrdemIts'
      'Tintas=Tintas'
      'GramasTi=GramasTi'
      'GramasKg=GramasKg'
      'GramasTo=GramasTo'
      'SiglaMoeda=SiglaMoeda'
      'PrecoMo_Kg=PrecoMo_Kg'
      'Cotacao_Mo=Cotacao_Mo'
      'CustoRS_Kg=CustoRS_Kg'
      'CustoRS_To=CustoRS_To'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Diluicao=Diluicao'
      'Minimo=Minimo'
      'Maximo=Maximo')
    DataSet = QrEmitIts
    BCDToCurrency = False
    Left = 536
    Top = 356
  end
  object frxQUI_RECEI_105_001: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38077.792695474500000000
    ReportOptions.Name = 'Receita Skintan'
    ReportOptions.LastChange = 42605.413587453700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MeLinhasOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  {'
      '  with MeLinhas, Engine do'
      '  begin'
      '  if <VAR_TXT> = '#39#39' then'
      '    MeLinhas.Visible := False'
      '  else'
      '    MeLinhas.Visible := True;'
      '  end'
      '  }      '
      'end;'
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  Visi: Boolean;                                        '
      'begin'
      '  if <frxDsEmitFlu."OpProc"> = 1 then //Opera'#231#227'o'
      '    Visi := False'
      '  else'
      '    Visi := True;'
      '  //'
      '  Memo26.Visible := Visi;'
      
        '  Memo28.Visible := Visi;                                       ' +
        '       '
      '  Memo27.Visible := Visi;    '
      '  Memo29.Visible := Visi;    '
      '  Memo6.Visible  := Visi;    '
      '  Memo13.Visible := Visi;'
      '  //'
      '  if <frxDsEmitFlu."OpProc"> = 1 then //Opera'#231#227'o'
      '  begin'
      '    Memo25.Width := Shape1.Width - Memo23.Width;'
      
        '    Memo24.Width := Shape1.Width - (Memo14.Width + Memo18.Width)' +
        ';                                                               ' +
        '                                       '
      '  end else'
      '  begin'
      
        '    Memo25.Width := Shape1.Width - (Memo23.Width + Memo26.Width ' +
        '+ Memo28.Width + Memo27.Width + Memo29.Width);'
      
        '    Memo24.Width := Shape1.Width - (Memo14.Width + Memo6.Width +' +
        ' Memo13.Width + Memo18.Width);                                  ' +
        '                                                                ' +
        '    '
      
        '  end;                                                          ' +
        '                             '
      'end;'
      ''
      'begin'
      'end.')
    Left = 84
    Top = 404
    Datasets = <
      item
      end
      item
        DataSet = frxDsEmitFlu
        DataSetName = 'frxDsEmitFlu'
      end
      item
        DataSet = frxDsEmitIts
        DataSetName = 'frxDsEmitIts'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsEmit
        DataSetName = 'frxDsEmit'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 83.149606300000000000
        Top = 181.417440000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEmitFlu
        DataSetName = 'frxDsEmitFlu'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object Shape1: TfrxShapeView
          Top = 7.559059999999988000
          Width = 680.315400000000000000
          Height = 75.590551180000000000
          Curve = 1
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo23: TfrxMemoView
          Top = 7.559059999999988000
          Width = 68.031491180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HideZeros = True
          Memo.UTF8W = (
            'Processo: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 68.031540000000010000
          Top = 26.456709999999990000
          Width = 385.511671890000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HideZeros = True
          Memo.UTF8W = (
            '[frxDsEmitFlu."NomeFlu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 68.031540000000010000
          Top = 7.559059999999988000
          Width = 317.480314960000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HideZeros = True
          Memo.UTF8W = (
            '[frxDsEmitFlu."NomeTin"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 385.512060000000000000
          Top = 7.559059999999988000
          Width = 49.133860710000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            'kg tinta: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 510.236550000000000000
          Top = 7.559059999999988000
          Width = 94.488220709999990000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            'g/m'#178' consumo: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 434.645950000000000000
          Top = 7.559059999999988000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000'#39',<frxDsEmitFlu."GramasTo">/1000)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 604.724800000000000000
          Top = 7.559059999999988000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000'#39',<frxDsEmitFlu."GramasM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 453.543600000000000000
          Top = 26.456709999999990000
          Width = 94.488220709999990000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            'g/m'#178' carga:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 548.031849999999900000
          Top = 26.456709999999990000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000'#39',<frxDsEmitFlu."InfoCargM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Top = 26.456709999999990000
          Width = 68.031491180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HideZeros = True
          Memo.UTF8W = (
            'Opera'#231#227'o: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeEtapa: TfrxMemoView
          Top = 45.354359999999990000
          Width = 68.031491180000000000
          Height = 37.795287800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HideZeros = True
          Memo.UTF8W = (
            'Fluxo: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 68.031540000000010000
          Top = 45.354359999999990000
          Width = 555.590521890000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HideZeros = True
          Memo.UTF8W = (
            '[frxDsEmitFlu."DescriFlu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 623.622450000000000000
          Top = 26.456709999999990000
          Width = 56.692901180000000000
          Height = 56.692940240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEmitIts
        DataSetName = 'frxDsEmitIts'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 340.157573070000000000
          Width = 94.488201180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000'#39',<frxDsEmitIts."GramasTi">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 56.692950000000000000
          Width = 283.464566929134000000
          Height = 15.118110240000000000
          DataField = 'NomePQ'
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEmitIts."NomePQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 434.645950000000000000
          Width = 94.488201180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000000'#39',<frxDsEmitIts."Gramaskg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 529.134200000000000000
          Width = 94.488201180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000'#39',<frxDsEmitIts."GramasTo">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Width = 56.692901180000000000
          Height = 15.118110236220500000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000; ; '#39',<frxDsEmitIts."Produto">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 623.622450000000000000
          Width = 56.692901180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEmitIts."OrdemFlu"'
        object Memo5: TfrxMemoView
          Left = 340.157573070000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Composi'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 56.692950000000000000
          Width = 283.464566930000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do produto qu'#237'mico')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 434.645950000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'g/kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 529.134200000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'g pesagem')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Width = 56.692901180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'digo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 623.622450000000000000
          Width = 56.692888980000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ok')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Top = 362.834880000000000000
        Width = 680.315400000000000000
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 423.307360000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Cabecalho: TfrxPageHeader
        FillType = ftBrush
        Height = 102.047310000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo62: TfrxMemoView
          Left = 442.433231970000000000
          Top = 41.574732360000000000
          Width = 34.015748030000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            #193'rea:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 559.598586300000000000
          Top = 41.574732360000000000
          Width = 52.913385830000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Quantia:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Top = 41.574732360000000000
          Width = 64.251968500000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'T'#233'cnico:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 64.480478500000000000
          Top = 41.574732360000000000
          Width = 113.385826770000000000
          Height = 20.787401570000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEmit."Tecnico"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 268.574964250000000000
          Top = 41.574732360000000000
          Width = 64.251968500000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Espessura:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 332.826932760000000000
          Top = 41.574732360000000000
          Width = 108.661410000000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEmit."Espessura"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 476.448980000000000000
          Top = 41.574732360000000000
          Width = 83.149606300000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_Area]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 612.511972130000000000
          Top = 41.574732360000000000
          Width = 64.440940000000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_Qtde]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape3: TfrxShapeView
          Left = 0.228510000000000000
          Width = 680.315400000000000000
          Height = 41.574830000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo7: TfrxMemoView
          Left = 0.228510000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmit."NOMECI"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 0.228510000000000000
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          Left = 151.409710000000000000
          Top = 18.897650000000000000
          Width = 370.393940000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PESAGEM]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 7.787570000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 521.803650000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 0.228510000000000000
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 34.015770000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Receita de [frxDsEmit."NOMESETOR"]: [frxDsEmit."Numero"] - [frxD' +
              'sEmit."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxQUI_RECEI_105_003: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41537.466215289350000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '    '
      'end.')
    Left = 252
    Top = 520
    Datasets = <
      item
      end
      item
        DataSet = frxDsFalta
        DataSetName = 'frxDsFalta'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 101.149494020000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Top = 83.149494020000000000
          Width = 408.188976380000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 408.188971500000000000
          Top = 83.149494020000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg Estoque')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 498.897691500000000000
          Top = 83.149494020000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg Necess'#225'rio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 589.606411500000000000
          Top = 83.149494020000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg SALDO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE INSUFICIENTE PARA BAIXA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Top = 60.472479999999990000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 181.417440000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsFalta
        DataSetName = 'frxDsFalta'
        RowCount = 0
        object Memo6: TfrxMemoView
          Left = 52.913385830000000000
          Width = 355.275590550000000000
          Height = 15.118110240000000000
          DataField = 'Nome'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFalta."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 408.188976380000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'QtdEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFalta."QtdEstq"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 498.897618270000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'QtdGastar'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFalta."QtdGastar"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 589.606279690000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'QTD_SALDO'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFalta."QTD_SALDO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Produto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFalta."Produto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 257.008040000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrFalta: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 256
    Top = 424
    object QrFaltaProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrFaltaNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
    object QrFaltaQtdEstq: TFloatField
      FieldName = 'QtdEstq'
    end
    object QrFaltaQtdGastar: TFloatField
      FieldName = 'QtdGastar'
    end
    object QrFaltaQTD_SALDO: TFloatField
      FieldName = 'QTD_SALDO'
    end
  end
  object frxDsFalta: TfrxDBDataset
    UserName = 'frxDsFalta'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Produto=Produto'
      'Nome=Nome'
      'QtdEstq=QtdEstq'
      'QtdGastar=QtdGastar'
      'QTD_SALDO=QTD_SALDO')
    DataSet = QrFalta
    BCDToCurrency = False
    Left = 256
    Top = 472
  end
  object frxQUI_RECEI_105_002: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38077.792695474500000000
    ReportOptions.Name = 'Receita Skintan'
    ReportOptions.LastChange = 39541.568236400500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MeLinhasOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  {'
      '  with MeLinhas, Engine do'
      '  begin'
      '  if <VAR_TXT> = '#39#39' then'
      '    MeLinhas.Visible := False'
      '  else'
      '    MeLinhas.Visible := True;'
      '  end'
      '  }      '
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 84
    Top = 452
    Datasets = <
      item
      end
      item
        DataSet = frxDsEmitFlu
        DataSetName = 'frxDsEmitFlu'
      end
      item
        DataSet = frxDsEmitIts
        DataSetName = 'frxDsEmitIts'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsEmit
        DataSetName = 'frxDsEmit'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 56.692950000000010000
        Top = 181.417440000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEmitFlu
        DataSetName = 'frxDsEmitFlu'
        RowCount = 0
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 56.692950000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object MeEtapa: TfrxMemoView
          Width = 49.133841180000000000
          Height = 37.795285350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HideZeros = True
          Memo.UTF8W = (
            'Fluxo: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Top = 37.795299999999850000
          Width = 49.133841180000000000
          Height = 18.897637795275600000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HideZeros = True
          Memo.UTF8W = (
            'Processo: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 49.133890000000000000
          Width = 226.771653543307000000
          Height = 37.795285350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HideZeros = True
          Memo.UTF8W = (
            '[frxDsEmitFlu."NomeFlu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 49.133890000000000000
          Top = 37.795299999999850000
          Width = 623.622047240000000000
          Height = 18.897637795275600000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HideZeros = True
          Memo.UTF8W = (
            '[frxDsEmitFlu."NomeTin"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 275.905690000000000000
          Top = 18.897649999999940000
          Width = 60.472431180000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HideZeros = True
          Memo.UTF8W = (
            'Custo tinta $:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 336.378170000000000000
          Top = 18.897649999999940000
          Width = 79.370059210000000000
          Height = 18.897635350000000000
          DataSet = frxDsEmitFlu
          DataSetName = 'frxDsEmitFlu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39',<frxDsEmitFlu."CustoTo">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 408.189240000000000000
          Width = 52.913371180000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HideZeros = True
          Memo.UTF8W = (
            'R$/kg tinta:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 544.252320000000000000
          Top = 18.897649999999940000
          Width = 52.913371180000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HideZeros = True
          Memo.UTF8W = (
            'R$/m'#178' tinta: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 461.102660000000000000
          Width = 79.370059210000000000
          Height = 18.897635350000000000
          DataSet = frxDsEmitFlu
          DataSetName = 'frxDsEmitFlu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.4n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39',<frxDsEmitFlu."CustoKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 597.165740000000000000
          Top = 18.897649999999940000
          Width = 79.370059210000000000
          Height = 18.897635350000000000
          DataSet = frxDsEmitFlu
          DataSetName = 'frxDsEmitFlu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.4n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.0000'#39',<frxDsEmitFlu."CustoM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 275.905690000000000000
          Width = 41.574781180000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HideZeros = True
          Memo.UTF8W = (
            'kg tinta:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 419.527830000000000000
          Top = 18.897649999999940000
          Width = 41.574781180000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HideZeros = True
          Memo.UTF8W = (
            'g/m'#178': ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Left = 317.480520000000000000
          Width = 79.370059210000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000'#39',<frxDsEmitFlu."GramasTo">/1000)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 461.102660000000000000
          Top = 18.897649999999940000
          Width = 79.370059210000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000'#39',<frxDsEmitFlu."GramasM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 544.252320000000000000
          Width = 52.913371180000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HideZeros = True
          Memo.UTF8W = (
            'U$/m'#178' tinta: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 597.165740000000000000
          Width = 79.370059210000000000
          Height = 18.897635350000000000
          DataSet = frxDsEmitFlu
          DataSetName = 'frxDsEmitFlu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.4n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.0000'#39',<frxDsEmitFlu."CusUSM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 17.000000000000000000
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEmitIts
        DataSetName = 'frxDsEmitIts'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 249.448853070000000000
          Width = 52.913376060000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000'#39',<frxDsEmitIts."GramasTi">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 30.236240000000000000
          Width = 219.212598425196800000
          Height = 17.000000000000000000
          DataField = 'NomePQ'
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsEmitIts."NomePQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 302.362400000000000000
          Width = 52.913376060000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000'#39',<frxDsEmitIts."Gramaskg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 355.275820000000000000
          Width = 68.031496060000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000'#39',<frxDsEmitIts."GramasTo">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Width = 30.236191180000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000; ; '#39',<frxDsEmitIts."Produto">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 442.205010000000000000
          Width = 49.133846060000010000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39',<frxDsEmitIts."PrecoMo_Kg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 491.338900000000000000
          Width = 52.913376060000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.0000'#39',<frxDsEmitIts."Cotacao_Mo">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 544.252320000000100000
          Width = 60.472436060000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.0000'#39',<frxDsEmitIts."CustoRS_Kg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 423.307360000000000000
          Width = 18.897606060000000000
          Height = 17.000000000000000000
          DataField = 'SiglaMoeda'
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEmitIts."SiglaMoeda"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 604.724800000000000000
          Width = 49.133858267716530000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.0000'#39',<frxDsEmitIts."CustoRS_To">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 653.858690000000000000
          Width = 26.456678270000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 20.779530000000000000
        Top = 260.787570000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEmitIts."OrdemFlu"'
        object Memo5: TfrxMemoView
          Left = 249.448853070000000000
          Top = 3.779530000000022000
          Width = 52.913376060000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Composi'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 30.236240000000000000
          Top = 3.779530000000022000
          Width = 219.212598425196800000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do produto qu'#237'mico')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 302.362400000000000000
          Top = 3.779530000000022000
          Width = 52.913376060000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'g/kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 355.275820000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'g pesagem')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 3.779530000000193000
          Width = 30.236191180000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 423.307360000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o original')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 491.338900000000000000
          Top = 3.779530000000022000
          Width = 52.913376060000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cota'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 544.252320000000100000
          Top = 3.779530000000022000
          Width = 60.472436060000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o R$/kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 604.724800000000000000
          Top = 3.779530000000022000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Item R$')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 653.858690000000000000
          Top = 3.779530000000022000
          Width = 26.456678270000000000
          Height = 17.000000000000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ok')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 347.716760000000000000
        Width = 680.315400000000000000
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 49.133890000000000000
        Top = 411.968770000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 37.795300000000000000
          Shape = skEllipse
        end
        object Shape4: TfrxShapeView
          Left = 642.519685039370000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 37.795300000000000000
          Shape = skEllipse
        end
        object Memo50: TfrxMemoView
          Left = 18.897650000000000000
          Top = 3.779530000000022000
          Width = 642.519685040000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Fill.BackColor = clWhite
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 18.897650000000000000
          Top = 15.118119999999860000
          Width = 113.385851180000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HideZeros = True
          Memo.UTF8W = (
            'R$ total soma tintas:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 260.787570000000000000
          Top = 15.118119999999860000
          Width = 98.267731180000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HideZeros = True
          Memo.UTF8W = (
            'U$/m'#178' total tintas: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 132.283550000000000000
          Top = 15.118119999999860000
          Width = 128.503949210000000000
          Height = 18.897635350000000000
          DataSet = frxDsEmitFlu
          DataSetName = 'frxDsEmitFlu'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HideZeros = True
          Memo.UTF8W = (
            '[frxDsEmit."CustoTo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 359.055350000000000000
          Top = 15.118119999999860000
          Width = 102.047244094488000000
          Height = 18.897635350000000000
          DataSet = frxDsEmitFlu
          DataSetName = 'frxDsEmitFlu'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HideZeros = True
          Memo.UTF8W = (
            '[frxDsEmit."CusUSM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 461.102660000000000000
          Top = 15.118119999999860000
          Width = 98.267731180000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HideZeros = True
          Memo.UTF8W = (
            'R$/m'#178' total tintas: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 559.370440000000000000
          Top = 15.118119999999860000
          Width = 102.047244090000000000
          Height = 18.897635350000000000
          DataSet = frxDsEmitFlu
          DataSetName = 'frxDsEmitFlu'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HideZeros = True
          Memo.UTF8W = (
            '[frxDsEmit."CustoM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 483.779840000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Date] - [Time] - [VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Cabecalho: TfrxPageHeader
        FillType = ftBrush
        Height = 102.047310000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo62: TfrxMemoView
          Left = 442.433231970000000000
          Top = 41.574732360000000000
          Width = 34.015748030000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            #193'rea:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 559.598586300000000000
          Top = 41.574732360000000000
          Width = 52.913385830000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Quantia:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Top = 41.574732360000000000
          Width = 64.251968500000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'T'#233'cnico:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 64.480478500000000000
          Top = 41.574732360000000000
          Width = 113.385826770000000000
          Height = 20.787401570000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEmit."Tecnico"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 268.574964250000000000
          Top = 41.574732360000000000
          Width = 64.251968500000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Espessura:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 332.826932760000000000
          Top = 41.574732360000000000
          Width = 108.661410000000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEmit."Espessura"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 476.448980000000000000
          Top = 41.574732360000000000
          Width = 83.149606300000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_Area]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 612.511972130000000000
          Top = 41.574732360000000000
          Width = 64.440940000000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_Qtde]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape3: TfrxShapeView
          Left = 0.228510000000000000
          Width = 680.315400000000000000
          Height = 41.574830000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo7: TfrxMemoView
          Left = 0.228510000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmit."NOMECI"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 0.228510000000000000
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          Left = 151.409710000000000000
          Top = 18.897650000000000000
          Width = 370.393940000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PESAGEM]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 7.787570000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 521.803650000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 0.228510000000000000
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 34.015748031496060000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Receita de [frxDsEmit."NOMESETOR"]: [frxDsEmit."Numero"] - [frxD' +
              'sEmit."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrSumEmitCus: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Peso) Peso, '
      'SUM(Pecas) Pecas'
      'FROM emitcus'
      'WHERE Codigo=:P0')
    Left = 44
    Top = 49
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumEmitCusPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSumEmitCusPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumEmitCusAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
  end
  object QrSumMPVIts: TMySQLQuery
    Database = Dmod.MyDB
    Left = 44
    Top = 96
    object QrSumMPVItsCusto: TFloatField
      FieldName = 'Custo'
    end
  end
  object QrSumWBMovIts: TMySQLQuery
    Database = Dmod.MyDB
    Left = 44
    Top = 144
    object QrSumWBMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object frxQUI_RECEI_105_004: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38077.792695474500000000
    ReportOptions.Name = 'Receita Skintan'
    ReportOptions.LastChange = 39541.568236400500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MeLinhasOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  {'
      '  with MeLinhas, Engine do'
      '  begin'
      '  if <VAR_TXT> = '#39#39' then'
      '    MeLinhas.Visible := False'
      '  else'
      '    MeLinhas.Visible := True;'
      '  end'
      '  }      '
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 84
    Top = 504
    Datasets = <
      item
        DataSet = frxDsEmit
        DataSetName = 'frxDsEmit'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPesaCab
        DataSetName = 'frxDsPesaCab'
      end
      item
        DataSet = frxDsPesaIts
        DataSetName = 'frxDsPesaIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897637800000000000
        Top = 181.417440000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPesaCab
        DataSetName = 'frxDsPesaCab'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 18.897601180000000000
          Curve = 1
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo23: TfrxMemoView
          Width = 68.031491180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HideZeros = True
          Memo.UTF8W = (
            'Processo: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 68.031540000000000000
          Width = 476.220550550000000000
          Height = 18.897637800000000000
          DataField = 'NomeTin'
          DataSet = frxDsPesaCab
          DataSetName = 'frxDsPesaCab'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HideZeros = True
          Memo.UTF8W = (
            '[frxDsPesaCab."NomeTin"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 544.252320000000000000
          Width = 49.133860710000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            'kg tinta: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 593.386210000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000'#39',<frxDsPesacab."GramasTo">/1000)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 260.787570000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPesaIts
        DataSetName = 'frxDsPesaIts'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 328.818983070000000000
          Width = 98.267731180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000'#39',<frxDsPesaIts."GramasTi">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 56.692950000000010000
          Width = 272.125976930000000000
          Height = 15.118110240000000000
          DataField = 'NomePQ'
          DataSet = frxDsPesaIts
          DataSetName = 'frxDsPesaIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesaIts."NomePQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 427.086890000000000000
          Width = 98.267731180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000000'#39',<frxDsPesaIts."Gramaskg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 525.354670000000100000
          Width = 98.267731180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000'#39',<frxDsPesaIts."GramasTo">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Width = 56.692901180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000; ; '#39',<frxDsPesaIts."Produto">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 623.622450000000000000
          Width = 56.692901180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Cabecalho: TfrxPageHeader
        FillType = ftBrush
        Height = 102.047310000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo62: TfrxMemoView
          Left = 442.433231970000000000
          Top = 41.574732360000000000
          Width = 34.015748030000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            #193'rea:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 559.598586300000000000
          Top = 41.574732360000000000
          Width = 52.913385830000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Quantia:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Top = 41.574732360000000000
          Width = 64.251968500000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'T'#233'cnico:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 64.480478500000000000
          Top = 41.574732360000000000
          Width = 113.385826770000000000
          Height = 20.787401570000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEmit."Tecnico"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 268.574964250000000000
          Top = 41.574732360000000000
          Width = 64.251968500000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Espessura:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 332.826932760000000000
          Top = 41.574732360000000000
          Width = 108.661410000000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEmit."Espessura"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 476.448980000000000000
          Top = 41.574732360000000000
          Width = 83.149606300000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_Area]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 612.511972130000000000
          Top = 41.574732360000000000
          Width = 64.440940000000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_Qtde]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape3: TfrxShapeView
          Left = 0.228510000000000000
          Width = 680.315400000000000000
          Height = 41.574830000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo7: TfrxMemoView
          Left = 0.228510000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmit."NOMECI"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 0.228510000000000000
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          Left = 151.409710000000000000
          Top = 18.897650000000000000
          Width = 370.393940000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PESAGEM]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 7.787570000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 521.803650000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 0.228510000000000000
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 34.015770000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Pesagem de [frxDsEmit."NOMESETOR"]: [frxDsEmit."Numero"] - [frxD' +
              'sEmit."Nome"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        object Memo5: TfrxMemoView
          Left = 328.818983070000000000
          Width = 98.267716540000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Composi'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 56.692950000000010000
          Width = 272.125976930000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do produto qu'#237'mico')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 427.086890000000000000
          Width = 98.267716540000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'g/kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 525.354670000000100000
          Width = 98.267716540000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'g pesagem')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Width = 56.692901180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'digo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 623.622450000000000000
          Width = 56.692888980000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmitIts
          DataSetName = 'frxDsEmitIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ok')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 18.897637795275590000
        Top = 298.582870000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object QrPesaCab: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPesaCabAfterScroll
    Left = 460
    Top = 456
    object QrPesaCabTintasTin: TIntegerField
      FieldName = 'TintasTin'
    end
    object QrPesaCabNomeTin: TWideStringField
      FieldName = 'NomeTin'
      Size = 30
    end
    object QrPesaCabOrdemFlu: TIntegerField
      FieldName = 'OrdemFlu'
    end
    object QrPesaCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesaCabGramasTo: TFloatField
      FieldName = 'GramasTo'
    end
  end
  object QrPesaIts: TMySQLQuery
    Database = Dmod.MyDB
    Left = 460
    Top = 504
    object QrPesaItsTintas: TIntegerField
      FieldName = 'Tintas'
    end
    object QrPesaItsProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrPesaItsNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Size = 50
    end
    object QrPesaItsGramasTi: TFloatField
      FieldName = 'GramasTi'
    end
    object QrPesaItsGramasKg: TFloatField
      FieldName = 'GramasKg'
    end
    object QrPesaItsGramasTo: TFloatField
      FieldName = 'GramasTo'
    end
    object QrPesaItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object frxDsPesaCab: TfrxDBDataset
    UserName = 'frxDsPesaCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TintasTin=TintasTin'
      'NomeTin=NomeTin'
      'OrdemFlu=OrdemFlu'
      'Codigo=Codigo'
      'GramasTo=GramasTo')
    DataSet = QrPesaCab
    BCDToCurrency = False
    Left = 536
    Top = 456
  end
  object frxDsPesaIts: TfrxDBDataset
    UserName = 'frxDsPesaIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Tintas=Tintas'
      'Produto=Produto'
      'NomePQ=NomePQ'
      'GramasTi=GramasTi'
      'GramasKg=GramasKg'
      'GramasTo=GramasTo'
      'Ordem=Ordem')
    DataSet = QrPesaIts
    BCDToCurrency = False
    Left = 536
    Top = 504
  end
end
