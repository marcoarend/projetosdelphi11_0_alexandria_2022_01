unit Equipes;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkGeral, dmkPermissoes, dmkEdit, dmkImage, UnDmkEnums;

type
  TFmEquipes = class(TForm)
    PainelDados: TPanel;
    DsEquipes: TDataSource;
    QrEquipes: TmySQLQuery;
    QrEquipesLk: TIntegerField;
    QrEquipesDataCad: TDateField;
    QrEquipesDataAlt: TDateField;
    QrEquipesUserCad: TIntegerField;
    QrEquipesUserAlt: TIntegerField;
    QrEquipesCodigo: TSmallintField;
    QrEquipesNome: TWideStringField;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrEquipesEntraA1: TIntegerField;
    QrEquipesSaidaA1: TIntegerField;
    QrEquipesEntraA2: TIntegerField;
    QrEquipesSaidaA2: TIntegerField;
    QrEquipesEntraB1: TIntegerField;
    QrEquipesSaidaB1: TIntegerField;
    QrEquipesEntraB2: TIntegerField;
    QrEquipesSaidaB2: TIntegerField;
    QrEquipesEntraC1: TIntegerField;
    QrEquipesSaidaC1: TIntegerField;
    QrEquipesEntraC2: TIntegerField;
    QrEquipesSaidaC2: TIntegerField;
    GroupBox1: TGroupBox;
    Label32: TLabel;
    EdEntraA1: TdmkEdit;
    EdSaidaA1: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    EdEntraA2: TdmkEdit;
    EdSaidaA2: TdmkEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    EdEntraB1: TdmkEdit;
    EdSaidaB1: TdmkEdit;
    EdEntraB2: TdmkEdit;
    EdSaidaB2: TdmkEdit;
    GroupBox3: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    EdEntraC1: TdmkEdit;
    EdSaidaC1: TdmkEdit;
    EdEntraC2: TdmkEdit;
    EdSaidaC2: TdmkEdit;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    Label20: TLabel;
    DBEdit2: TDBEdit;
    Label21: TLabel;
    DBEdit3: TDBEdit;
    Label22: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label23: TLabel;
    Label24: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label25: TLabel;
    Label26: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    Label27: TLabel;
    Label28: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label29: TLabel;
    Label30: TLabel;
    DBEdit12: TDBEdit;
    QrEquipesENTRAA1_TXT: TWideStringField;
    QrEquipesSAIDAA1_TXT: TWideStringField;
    QrEquipesSAIDAA2_TXT: TWideStringField;
    QrEquipesENTRAA2_TXT: TWideStringField;
    QrEquipesENTRAB1_TXT: TWideStringField;
    QrEquipesSAIDAB1_TXT: TWideStringField;
    QrEquipesENTRAB2_TXT: TWideStringField;
    QrEquipesSAIDAB2_TXT: TWideStringField;
    QrEquipesENTRAC1_TXT: TWideStringField;
    QrEquipesSAIDAC1_TXT: TWideStringField;
    QrEquipesENTRAC2_TXT: TWideStringField;
    QrEquipesSAIDAC2_TXT: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEquipesAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrEquipesAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEquipesBeforeOpen(DataSet: TDataSet);
    procedure EdEntraA1Exit(Sender: TObject);
    procedure EdEntraB1Exit(Sender: TObject);
    procedure EdEntraC1Exit(Sender: TObject);
    procedure EdSaidaA1Exit(Sender: TObject);
    procedure EdSaidaB1Exit(Sender: TObject);
    procedure EdSaidaC1Exit(Sender: TObject);
    procedure EdEntraA2Exit(Sender: TObject);
    procedure EdEntraB2Exit(Sender: TObject);
    procedure EdEntraC2Exit(Sender: TObject);
    procedure EdSaidaA2Exit(Sender: TObject);
    procedure EdSaidaB2Exit(Sender: TObject);
    procedure EdSaidaC2Exit(Sender: TObject);
    procedure QrEquipesCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    FSeq: Integer;
  end;

var
  FmEquipes: TFmEquipes;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEquipes.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEquipes.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEquipesCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEquipes.DefParams;
begin
  VAR_GOTOTABELA := 'Equipes';
  VAR_GOTOMYSQLTABLE := QrEquipes;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM equipes');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmEquipes.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      if SQLType = stIns then
      begin
        EdCodigo.Text  := '';
        EdNome.Text    := '';
        EdEntraA1.Text := '';
        EdSaidaA1.Text := '';
        EdEntraA2.Text := '';
        EdSaidaA2.Text := '';
        EdEntraB1.Text := '';
        EdSaidaB1.Text := '';
        EdEntraB2.Text := '';
        EdSaidaB2.Text := '';
        EdEntraC1.Text := '';
        EdSaidaC1.Text := '';
        EdEntraC2.Text := '';
        EdSaidaC2.Text := '';
      end else begin
        EdCodigo.Text  := IntToStr(QrEquipesCodigo.Value);
        EdNome.Text    := QrEquipesNome.Value;
        EdEntraA1.Text := dmkPF.MTHT(QrEquipesEntraA1.Value, 'hh:nn');
        EdSaidaA1.Text := dmkPF.MTHT(QrEquipesSaidaA1.Value, 'hh:nn');
        EdEntraA2.Text := dmkPF.MTHT(QrEquipesEntraA2.Value, 'hh:nn');
        EdSaidaA2.Text := dmkPF.MTHT(QrEquipesSaidaA2.Value, 'hh:nn');
        EdEntraB1.Text := dmkPF.MTHT(QrEquipesEntraB1.Value, 'hh:nn');
        EdSaidaB1.Text := dmkPF.MTHT(QrEquipesSaidaB1.Value, 'hh:nn');
        EdEntraB2.Text := dmkPF.MTHT(QrEquipesEntraB2.Value, 'hh:nn');
        EdSaidaB2.Text := dmkPF.MTHT(QrEquipesSaidaB2.Value, 'hh:nn');
        EdEntraC1.Text := dmkPF.MTHT(QrEquipesEntraC1.Value, 'hh:nn');
        EdSaidaC1.Text := dmkPF.MTHT(QrEquipesSaidaC1.Value, 'hh:nn');
        EdEntraC2.Text := dmkPF.MTHT(QrEquipesEntraC2.Value, 'hh:nn');
        EdSaidaC2.Text := dmkPF.MTHT(QrEquipesSaidaC2.Value, 'hh:nn');
      end;
      EdNome.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmEquipes.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEquipes.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEquipes.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmEquipes.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEquipes.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEquipes.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEquipes.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEquipes.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmEquipes.BtAlteraClick(Sender: TObject);
var
  Equipes : Integer;
begin
  Equipes := QrEquipesCodigo.Value;
  if not UMyMod.SelLockY(Equipes, Dmod.MyDB, 'Equipes', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Equipes, Dmod.MyDB, 'Equipes', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmEquipes.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEquipesCodigo.Value;
  Close;
end;

procedure TFmEquipes.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descrição.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO equipes SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Equipes', 'Equipes', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE equipes SET ');
    Codigo := QrEquipesCodigo.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Nome=:P0, EntraA1=:P1, SaidaA1=:P2, EntraA2=:P3, ');
  Dmod.QrUpdU.SQL.Add('SaidaA2=:P4, EntraB1=:P5, SaidaB1=:P6, EntraB2=:P7, ');
  Dmod.QrUpdU.SQL.Add('SaidaB2=:P8, EntraC1=:P9, SaidaC1=:P10, EntraC2=:P11, ');
  Dmod.QrUpdU.SQL.Add('SaidaC2=:P12, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := dmkPF.TTM(EdEntraA1.Text);
  Dmod.QrUpdU.Params[02].AsInteger := dmkPF.TTM(EdSaidaA1.Text);
  Dmod.QrUpdU.Params[03].AsInteger := dmkPF.TTM(EdEntraA2.Text);
  Dmod.QrUpdU.Params[04].AsInteger := dmkPF.TTM(EdSaidaA2.Text);
  Dmod.QrUpdU.Params[05].AsInteger := dmkPF.TTM(EdEntraB1.Text);
  Dmod.QrUpdU.Params[06].AsInteger := dmkPF.TTM(EdSaidaB1.Text);
  Dmod.QrUpdU.Params[07].AsInteger := dmkPF.TTM(EdEntraB2.Text);
  Dmod.QrUpdU.Params[08].AsInteger := dmkPF.TTM(EdSaidaB2.Text);
  Dmod.QrUpdU.Params[09].AsInteger := dmkPF.TTM(EdEntraC1.Text);
  Dmod.QrUpdU.Params[10].AsInteger := dmkPF.TTM(EdSaidaC1.Text);
  Dmod.QrUpdU.Params[11].AsInteger := dmkPF.TTM(EdEntraC2.Text);
  Dmod.QrUpdU.Params[12].AsInteger := dmkPF.TTM(EdSaidaC2.Text);
  //
  Dmod.QrUpdU.Params[13].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[14].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[15].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Equipes', 'Codigo');
  MostraEdicao(0, stLok, 0);
  LocCod(Codigo,Codigo);
  if FSeq = 1 then Close;
end;

procedure TFmEquipes.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Equipes', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Equipes', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Equipes', 'Codigo');
end;

procedure TFmEquipes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FSeq := 0;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmEquipes.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEquipesCodigo.Value,LaRegistro.Caption);
end;

procedure TFmEquipes.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmEquipes.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEquipes.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmEquipes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrEquipesCodigo.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEquipes.QrEquipesAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEquipes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Equipes', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, stIns, 0);
end;

procedure TFmEquipes.QrEquipesAfterScroll(DataSet: TDataSet);
begin
  //  A equipe 0 ("Zero") PODE ser alterada !!!
  //BtAltera.Enabled := GOTOy.BtEnabled(QrEquipesCodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrEquipesCodigo.Value, False);
end;

procedure TFmEquipes.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEquipesCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Equipes', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEquipes.FormResize(Sender: TObject);
begin
   MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEquipes.QrEquipesBeforeOpen(DataSet: TDataSet);
begin
  QrEquipesCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEquipes.EdEntraA1Exit(Sender: TObject);
begin
  EdEntraA1.Text := dmkPF.HTH(EdEntraA1.Text, 'hh:nn');
end;

procedure TFmEquipes.EdEntraB1Exit(Sender: TObject);
begin
  EdEntraB1.Text := dmkPF.HTH(EdEntraB1.Text, 'hh:nn');
end;

procedure TFmEquipes.EdEntraC1Exit(Sender: TObject);
begin
  EdEntraC1.Text := dmkPF.HTH(EdEntraC1.Text, 'hh:nn');
end;

procedure TFmEquipes.EdSaidaA1Exit(Sender: TObject);
begin
  EdSaidaA1.Text := dmkPF.HTH(EdSaidaA1.Text, 'hh:nn');
end;

procedure TFmEquipes.EdSaidaB1Exit(Sender: TObject);
begin
  EdSaidaB1.Text := dmkPF.HTH(EdSaidaB1.Text, 'hh:nn');
end;

procedure TFmEquipes.EdSaidaC1Exit(Sender: TObject);
begin
  EdSaidaC1.Text := dmkPF.HTH(EdSaidaC1.Text, 'hh:nn');
end;

procedure TFmEquipes.EdEntraA2Exit(Sender: TObject);
begin
  EdEntraA2.Text := dmkPF.HTH(EdEntraA2.Text, 'hh:nn');
end;

procedure TFmEquipes.EdEntraB2Exit(Sender: TObject);
begin
  EdEntraB2.Text := dmkPF.HTH(EdEntraB2.Text, 'hh:nn');
end;

procedure TFmEquipes.EdEntraC2Exit(Sender: TObject);
begin
  EdEntraC2.Text := dmkPF.HTH(EdEntraC2.Text, 'hh:nn');
end;

procedure TFmEquipes.EdSaidaA2Exit(Sender: TObject);
begin
  EdSaidaA2.Text := dmkPF.HTH(EdSaidaA2.Text, 'hh:nn');
end;

procedure TFmEquipes.EdSaidaB2Exit(Sender: TObject);
begin
  EdSaidaB2.Text := dmkPF.HTH(EdSaidaB2.Text, 'hh:nn');
end;

procedure TFmEquipes.EdSaidaC2Exit(Sender: TObject);
begin
  EdSaidaC2.Text := dmkPF.HTH(EdSaidaC2.Text, 'hh:nn');
end;

procedure TFmEquipes.QrEquipesCalcFields(DataSet: TDataSet);
begin
  QrEquipesENTRAA1_TXT.Value := dmkPF.MTHT(QrEquipesEntraA1.Value, 'hh:nn');
  QrEquipesSAIDAA1_TXT.Value := dmkPF.MTHT(QrEquipesSaidaA1.Value, 'hh:nn');
  QrEquipesENTRAA2_TXT.Value := dmkPF.MTHT(QrEquipesEntraA2.Value, 'hh:nn');
  QrEquipesSAIDAA2_TXT.Value := dmkPF.MTHT(QrEquipesSaidaA2.Value, 'hh:nn');

  QrEquipesENTRAB1_TXT.Value := dmkPF.MTHT(QrEquipesEntraB1.Value, 'hh:nn');
  QrEquipesSAIDAB1_TXT.Value := dmkPF.MTHT(QrEquipesSaidaB1.Value, 'hh:nn');
  QrEquipesENTRAB2_TXT.Value := dmkPF.MTHT(QrEquipesEntraB2.Value, 'hh:nn');
  QrEquipesSAIDAB2_TXT.Value := dmkPF.MTHT(QrEquipesSaidaB2.Value, 'hh:nn');

  QrEquipesENTRAC1_TXT.Value := dmkPF.MTHT(QrEquipesEntraC1.Value, 'hh:nn');
  QrEquipesSAIDAC1_TXT.Value := dmkPF.MTHT(QrEquipesSaidaC1.Value, 'hh:nn');
  QrEquipesENTRAC2_TXT.Value := dmkPF.MTHT(QrEquipesEntraC2.Value, 'hh:nn');
  QrEquipesSAIDAC2_TXT.Value := dmkPF.MTHT(QrEquipesSaidaC2.Value, 'hh:nn');

end;

end.

