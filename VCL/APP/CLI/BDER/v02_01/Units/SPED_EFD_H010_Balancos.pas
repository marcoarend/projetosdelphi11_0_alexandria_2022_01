unit SPED_EFD_H010_Balancos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkCheckGroup, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Variants;

type
  TFmSPED_EFD_H010_Balancos = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    LaMes: TLabel;
    CBMes: TComboBox;
    LaAno: TLabel;
    CBAno: TComboBox;
    QrPQx: TmySQLQuery;
    QrPQxInsumo: TIntegerField;
    QrPQxPeso: TFloatField;
    QrPQxValor: TFloatField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrH010: TmySQLQuery;
    QrH010ImporExpor: TSmallintField;
    QrH010AnoMes: TIntegerField;
    QrH010Empresa: TIntegerField;
    QrH010H005: TIntegerField;
    QrH010LinArq: TIntegerField;
    QrH010REG: TWideStringField;
    QrH010COD_ITEM: TWideStringField;
    QrH010UNID: TWideStringField;
    QrH010QTD: TFloatField;
    QrH010VL_UNIT: TFloatField;
    QrH010VL_ITEM: TFloatField;
    QrH010IND_PROP: TWideStringField;
    QrH010COD_PART: TWideStringField;
    QrH010TXT_COMPL: TWideStringField;
    QrH010COD_CTA: TWideStringField;
    QrH010VL_ITEM_IR: TFloatField;
    QrH010BalID: TIntegerField;
    QrH010BalNum: TIntegerField;
    QrH010BalItm: TIntegerField;
    QrGraGruXSigla: TWideStringField;
    QrPQxCliOrig: TIntegerField;
    CGImportar: TdmkCheckGroup;
    QrNivelSel1: TmySQLQuery;
    QrNivelSel1Codigo: TIntegerField;
    QrNivelSel1Nome: TWideStringField;
    DsNivelSel1: TDataSource;
    GBImportar1: TGroupBox;
    RGNivPlaCta1: TRadioGroup;
    Panel5: TPanel;
    EdGenPlaCta1: TdmkEditCB;
    CBGenPlaCta1: TdmkDBLookupComboBox;
    Label1: TLabel;
    GBImportar2: TGroupBox;
    RGNivPlaCta2: TRadioGroup;
    Panel6: TPanel;
    Label2: TLabel;
    EdGenPlaCta2: TdmkEditCB;
    CBGenPlaCta2: TdmkDBLookupComboBox;
    QrNivelSel2: TmySQLQuery;
    QrNivelSel2Codigo: TIntegerField;
    QrNivelSel2Nome: TWideStringField;
    DsNivelSel2: TDataSource;
    QrVMI: TmySQLQuery;
    QrVMISigla: TWideStringField;
    QrVMICouNiv2: TIntegerField;
    QrVMIGraGruY: TIntegerField;
    QrVMIGragRuX: TIntegerField;
    QrVMIGraGru1: TIntegerField;
    QrVMIUnMedTxt: TWideStringField;
    QrVMITipoEstq: TLargeintField;
    QrVMISdoVrtPeca: TFloatField;
    QrVMISdoVrtArM2: TFloatField;
    QrVMISdoVrtPeso: TFloatField;
    QrVMINO_PRD_TAM_COR: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrVMIEmpresa: TIntegerField;
    QrVMIGrandeza: TSmallintField;
    QrVMIUnidMed: TIntegerField;
    QrVMIPecas: TFloatField;
    QrVMIAreaM2: TFloatField;
    QrVMIPesoKg: TFloatField;
    QrVMIValorT: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGNivPlaCta1Click(Sender: TObject);
    procedure CGImportarClick(Sender: TObject);
    procedure RGNivPlaCta2Click(Sender: TObject);
  private
    { Private declarations }
    function  InsereItemAtual(BalID, BalNum, BalItm: Integer;  COD_ITEM, UNID,
              IND_PROP, COD_PART, TXT_COMPL, COD_CTA: String; QTD, VL_UNIT,
              VL_ITEM, VL_ITEM_IR: Double): Boolean;
    function  ReopenGraGruX(Nivel1: Integer): Boolean;
  public
    { Public declarations }
    FImporExpor, FAnoMes, FEmpresa, FH005: Integer;
  end;

  var
  FmSPED_EFD_H010_Balancos: TFmSPED_EFD_H010_Balancos;

implementation

uses UnMyObjects, Module, UnDmkProcFunc, DmkDAC_PF, UMySQLModule, UnFinanceiro,
  ModuleFin, ModuleGeral, UnVS_PF;

{$R *.DFM}

procedure TFmSPED_EFD_H010_Balancos.BtOKClick(Sender: TObject);
  function AbreSQLGraGruX(Query: TmySQLQuery; SQL_PSQ: String): Boolean;
  begin
    Result := UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
   'SELECT med.Sigla, med.Grandeza, vmi.CouNiv2, vmi.GraGruY, ',
   'vmi.GragRuX, vmi.GraGru1, vmi.Empresa, vmi.Pecas, vmi.AreaM2, ',
   'vmi.PesoKg, vmi.ValorT, gg1.UnidMed, CONCAT(gg1.Nome,',
   'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),',
   'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
   'NO_PRD_TAM_COR, IF((vmi.GraGruY<2048 OR  ',
   'vmi.CouNiv2<>1) AND vmi.SdoVrtPeso > 0, "Kg",   ',
   'IF(vmi.CouNiv2=1 AND vmi.SdoVrtArM2 > 0, "m�", "Erro!")) UnMedTxt,   ',
   'IF((vmi.GraGruY<2048 OR  ',
   'vmi.CouNiv2<>1) AND vmi.SdoVrtPeso > 0, 1,   ',
   'IF(vmi.CouNiv2=1 AND vmi.SdoVrtArM2 > 0, 2, -1)) TipoEstq,   ',
   'vmi.SdoVrtPeca,   ',
   'vmi.SdoVrtArM2,   ',
   'vmi.SdoVrtPeso   ',
   'FROM _vsmovimp1_ vmi  ',
   'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=vmi.GraGru1  ',
   'LEFT JOIN ' + TMeuDB + '.unidmed med ON med.Codigo=gg1.UnidMed  ',
   'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX',
   'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC',
   'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
   'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI',
   'WHERE vmi.SdoVrtPeca > 0  ',
   'AND (  ',
   '  ((vmi.GraGruY<2048 OR vmi.CouNiv2<>1) AND vmi.SdoVrtPeso > 0)  ',
   '   OR  ',
   '  (vmi.CouNiv2=1 AND vmi.SdoVrtArM2 > 0)  ',
   ')',
    SQL_PSQ,
    '']);
    //Geral.MB_SQL(Self, Query);
  end;
  function ObtemCodigoAreaM2(var Codigo: Integer): Boolean;
  var
    Qry: TmySQLQuery;
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo, Sigla,  ',
      'IF(UPPER(Sigla) = "M�", 1,  ',
      '  IF(UPPER(Sigla) = "M2", 2, 3)) Prioridade  ',
      'FROM unidmed ',
      'WHERE Grandeza=' + Geral.FF0(Integer(TGrandezaUnidMed.gumAreaM2)), // 1
      'AND ( ',
      '  UPPER(Sigla) = "M�" ',
      '  OR UPPER(Sigla) = "M2" ',
      ') ',
      'ORDER BY Prioridade, Codigo ',
      ' ']);
      Codigo := Qry.FieldByName('Codigo').AsInteger;
      Result := Codigo > 0;
      if not Result then
      begin
        Geral.MB_ERRO(
        'N�o foi localizada nenhuma unidade de medida para �rea em m� na tabela de unidades de medida!');
        Close;
      end;
    finally
      Qry.Free;
    end;
  end;
  function ObtemCodigoPesoKg(var Codigo: Integer): Boolean;
  var
    Qry: TmySQLQuery;
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo, Sigla,  ',
      'IF(UPPER(Sigla) = "KG", 1,  ',
      '  IF(UPPER(Sigla) LIKE "KILO%", 2, ',
      '  IF(UPPER(Sigla) LIKE "QUILO%", 3, 4))) Prioridade  ',
      'FROM unidmed ',
      'WHERE Grandeza=' + Geral.FF0(Integer(TGrandezaUnidMed.gumPesoKg)), //2
      'AND (',
      '  UPPER(Sigla) = "KG" ',
      '  OR UPPER(Sigla) LIKE "KILO%" ',
      '  OR UPPER(Sigla) LIKE "QUILO%" ',
      ')',
      'ORDER BY Prioridade, Codigo ',
      ' ']);
      Codigo := Qry.FieldByName('Codigo').AsInteger;
      Result := Codigo > 0;
      if not Result then
      begin
       Geral.MB_ERRO(
       'N�o foi localizada nenhuma unidade de medida para �rea em m� na tabela de unidades de medida!');
       Close;
      end;
    finally
      Qry.Free;
    end;
  end;
  function ObtemCodigoPecaPc(var Codigo: Integer): Boolean;
  var
    Qry: TmySQLQuery;
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      ' SELECT Codigo, Sigla,  ',
      '  IF(UPPER(Sigla) = "PE�A", 1,  ',
      '  IF(UPPER(Sigla) = "PE�AS", 2,  ',
      '  IF(UPPER(Sigla) = "PECA", 3, ',
      '  IF(UPPER(Sigla) = "PECAS", 4, ',
      '  IF(UPPER(Sigla) = "PC", 5, ',
      '  IF(UPPER(Sigla) = "PCS", 6, ',
      '  IF(UPPER(Sigla) = "P�", 7, ',
      '  IF(UPPER(Sigla) = "P�S", 8, ',
      '  IF(UPPER(Sigla) = "PCA", 9, ',
      '  IF(UPPER(Sigla) = "PCAS", 10, ',
      '  IF(UPPER(Sigla) = "P�A", 11, ',
      '  IF(UPPER(Sigla) = "P�AS", 12, ',
      '  IF(UPPER(Sigla) = "PEC", 13, ',
      '  IF(UPPER(Sigla) = "PE�", 14, 15)))))))))))))) Prioridade  ',
      'FROM unidmed ',
      'WHERE Grandeza=' + Geral.FF0(Integer(TGrandezaUnidMed.gumPeca)), //0
      'AND (',
      '  UPPER(Sigla) = "PE�A"',
      '  OR UPPER(Sigla) = "PE�AS"',
      '  OR UPPER(Sigla) = "PECA"',
      '  OR UPPER(Sigla) = "PECAS"',
      '  OR UPPER(Sigla) = "PC"',
      '  OR UPPER(Sigla) = "PCS"',
      '  OR UPPER(Sigla) = "P�"',
      '  OR UPPER(Sigla) = "P�S"',
      '  OR UPPER(Sigla) = "PCA"',
      '  OR UPPER(Sigla) = "PCAS"',
      '  OR UPPER(Sigla) = "P�A"',
      '  OR UPPER(Sigla) = "P�AS"',
      '  OR UPPER(Sigla) = "PEC"',
      '  OR UPPER(Sigla) = "PE�"',
      ')',
      'ORDER BY Prioridade, Codigo ',
      ' ']);
      Codigo := Qry.FieldByName('Codigo').AsInteger;
      Result := Codigo > 0;
      if not Result then
      begin
       Geral.MB_ERRO(
       'N�o foi localizada nenhuma unidade de medida para Pe�a na tabela de unidades de medida!');
       Close;
      end;
    finally
      Qry.Free;
    end;
  end;
const
  DBG00GraGruY = nil;
  DBG00GraGruX = nil;
  DBG00CouNiv2 = nil;
  Qr00GraGruY  = nil;
  Qr00GraGruX  = nil;
  Qr00CouNiv2  = nil;
var
  DataBal: TDateTime;
  Bal_TXT: String;
  NoUnid: Integer;
  //
  BalID, BalNum, BalItm: Integer;
  COD_ITEM, UNID, IND_PROP, COD_PART, TXT_COMPL, COD_CTA: String;
  QTD, VL_UNIT, VL_ITEM, VL_ITEM_IR, Preco: Double;
var
  Entidade, Filial, Terceiro, Ordem1, Ordem2, Ordem3, Agrupa, StqCenCad,
  ZeroNegat: Integer;
  TableSrc, NO_Empresa, NO_StqCenCad, NO_Terceiro, SQL_PSQ: String;
  EstoqueEm, DataRelativa: TDateTime;
  DataCompra, MostraFrx: Boolean;
  //
  UnidMed, Nivel1, UnidMedPc, UnidMedKg, UnidMedM2: Integer;
  Corda: String;
  Erro: Boolean;
  Fator, MyQtd, MyVlU, MyBas: Double;
  Tipo: TGrandezaUnidMed;
begin
  Erro := False;
  ObtemCodigoPecaPc(UnidMedPc);
  ObtemCodigoPesoKg(UnidMedKg);
  ObtemCodigoAreaM2(UnidMedM2);
  if (UnidMedKg = 0) or (UnidMedM2 = 0) then Exit;
  // Uso e consumo
  if DmkPF.IntInConjunto2(1, CGImportar.Value) then
  begin
    if MyObjects.FIC(EdGenPlaCta1.ValueVariant = 0, EdGenPlaCta1,
    'Informe o n�vel e o c�digo do plano de contas!') then Exit;
    DataBal := EncodeDate(Geral.IMV(CBAno.Text), CBMes.ItemIndex + 1, 1);
    Bal_TXT := Geral.FDT(DataBal, 1);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPQx, Dmod.MyDB, [
    'SELECT pqx.Insumo, pqx.CliOrig,',
    'SUM(pqx.Peso) Peso, SUM(pqx.Valor) Valor ',
    'FROM pqx pqx',
    //'WHERE /*pqx.CliOrig=-11 ',
    //'AND*/ pqx.Tipo=0 ',
    'WHERE pqx.Tipo=0 ',
    'AND pqx.DataX="' + Bal_TXT + '" ',
    'GROUP BY pqx.Insumo, pqx.CliOrig ',
    '']);
    QrPQx.First;
    while not QrPQx.Eof do
    begin
      //Reabre QrGraGruX
      if QrPQxPeso.Value > 0 then
      begin
        ReopenGraGruX(QrPQxInsumo.Value);
        UnidMed := QrGraGruXUnidMed.Value;
        if UnidMed = 0 then
        begin
          Nivel1  := QrPQxInsumo.Value;
          UnidMed := UnidMedKG;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
          'UnidMed'], ['Nivel1'], [
          UnidMed], [Nivel1], True);
          //
          ReopenGraGruX(QrPQxInsumo.Value);
        end;
        //
        BalID          := Integer(TSPED_EFD_Bal.sebalPQx);
        BalNum         := FAnoMes;
        BalItm         := QrPQxInsumo.Value;
        COD_ITEM       := Geral.FF0(QrGraGruXControle.Value);
        UNID           := QrGraGruXSigla.Value;
        IND_PROP       := dmkPF.EscolhaDe2Str(FEmpresa = QrPQxCliOrig.Value, '0', '2');
        COD_PART       := dmkPF.EscolhaDe2Str(FEmpresa = QrPQxCliOrig.Value, '', Geral.FF0(QrPQxCliOrig.Value));
        TXT_COMPL      := '';
        COD_CTA        := UFinanceiro.MontaSPED_COD_CTA(RGNivPlaCta1.ItemIndex, EdGenPlaCta1.ValueVariant);
        QTD            := QrPQxPeso.Value;
        VL_UNIT        := QrPQxValor.Value / QrPQxPeso.Value;
        VL_ITEM        := QrPQxValor.Value;
        VL_ITEM_IR     := QrPQxValor.Value;
        //
        InsereItemAtual(BalID, BalNum, BalItm, COD_ITEM, UNID, IND_PROP, COD_PART,
        TXT_COMPL, COD_CTA, QTD, VL_UNIT, VL_ITEM, VL_ITEM_IR);
      end;
      //
      QrPQx.Next;
    end;
    if NoUnid > 0 then
      Geral.MB_Aviso(Geral.FF0(NoUnid) +
      ' insumos n�o tem definido a sigla da unidade em seu cadastro de grade!' +
      slineBreak + 'Para estes insumos foi considerado a sigla "KG"!');
  end;
  // Couros VS
  if DmkPF.IntInConjunto2(2, CGImportar.Value) then
  begin
    Entidade     := FEmpresa;
    Filial       := DModG.ObtemFilialDeEntidade(FEmpresa);
    Terceiro     := 0;
    Ordem1       := 0;
    Ordem2       := 0;
    Ordem3       := 0;
    Agrupa       := 0;
    StqCenCad    := 0;
    ZeroNegat    := 0; // Somente positivos!
    NO_Empresa   := '';
    NO_StqCenCad := '';
    NO_Terceiro  := '';
    EstoqueEm    := IncMonth(EncodeDate(Geral.IMV(CBAno.Text), CBMes.ItemIndex + 1, 1), 1) -1;
    DataRelativa := EstoqueEm;
    MostraFrx    := False;
    //
    if VS_PF.ImprimeEstoqueEm(Entidade, Filial, Terceiro, Ordem1, Ordem2, Ordem3,
    Agrupa, StqCenCad, ZeroNegat, NO_Empresa, NO_StqCenCad, NO_Terceiro,
    DataRelativa, DataCompra, DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2,
    Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2, EstoqueEm, LaAviso1, LaAviso2,
    False) then
    begin
      // GraGru1 Sem UnidMed
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando unidades de medida! (A)');
      SQL_PSQ := 'AND gg1.UnidMed < 1 ';
      AbreSQLGraGruX(QrVMI, SQL_PSQ);
      QrVMI.First;
      while not QrVMI.Eof do
      begin
        Nivel1 := QrVMIGraGru1.Value;
        UnidMed := 0;
        case QrVMITipoEstq.Value of
          1(*kg*): UnidMed := UnidMedKg;
          2(*m2*): UnidMed := UnidMedM2;
          else
          begin
            Geral.MB_Erro(
            'ERRO! Tipo de movimenta��o de estoque diferente de kg e m2!' +
            'Tipo: ' + Geral.FF0(QrVMIGraGru1.Value));
            Erro := True;
          end;
        end;
        //
        if UnidMed > 0 then
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
          'UnidMed'], ['Nivel1'], [
          UnidMed], [Nivel1], True);
        end;
        QrVMI.Next;
      end;
      if Erro then Exit;
      //
      // GraGru1 com UnidMed sem rela��o com pe�a, kg ou m2
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando unidades de medida! (B)');
      SQL_PSQ := 'AND NOT (med.Grandeza IN (0,1,2,6,7)) ';
      AbreSQLGraGruX(QrVMI, SQL_PSQ);
      if QrVMI.RecordCount > 0 then
      begin
        Erro := True;
        Corda := Geral.FF0(QrVMIGraGru1.Value);
        QrVMI.Next;
        while not QrVMI.Eof do
        begin
          Corda := Corda + ',' + Geral.FF0(QrVMIGraGru1.Value);
          QrVMI.Next;
        end;
        Geral.MB_ERRO(
        'A unidade de medida dos produtos abaixo (Nivel 1) tem cadastrado unidades de medida inv�lidas!');
        Close;
      end;
      //
      if Erro then Exit;
      // Importa��o
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Importando');
      SQL_PSQ := '';
      AbreSQLGraGruX(QrVMI, SQL_PSQ);
      while not QrVMI.Eof do
      begin
{
        case TGrandezaUnidMed(QrVMITipoEstq.Value) of
          (*0*)gumPeca:
          begin
            UnidMed := UnidMedPc;
            Fator   := 1.0;
          end;
          (*1*)gumAreaM2:
          begin
            UnidMed := UnidMedM2;
            Fator   := 1.0;
          end;
          (*2*)gumPesoKG:
          begin
            UnidMed := UnidMedKg;
            Fator   := 1.0;
          end;
          //(*3*)gumVolumeM3=3,
          //(*4*)gumLinearM=4,
          //(*5*)gumOutros=5,
          (*6*)gumAreaP2:
          begin
            UnidMed := UnidMedM2;
            Fator   := 1.0;
          end;
          (*7*)gumPesoTon=7
        end;
}
        case TGrandezaUnidMed(QrVMIGrandeza.Value) of
          (*0*)gumPeca:
          begin
            Tipo  := TGrandezaUnidMed.gumPeca;
            Fator := 1.0;
            MyQtd := QrVMISdoVrtPeca.Value;
          end;
          (*1*)gumAreaM2:
          begin
            Tipo  := TGrandezaUnidMed.gumAreaM2;
            Fator := 1.0;
            MyQtd := QrVMISdoVrtArM2.Value;
          end;
          (*2*)gumPesoKG:
          begin
            Tipo  := TGrandezaUnidMed.gumPesoKG;
            Fator := 1.0;
            MyQtd := QrVMISdoVrtPeso.Value;
          end;
          //(*3*)gumVolumeM3=3,
          //(*4*)gumLinearM=4,
          //(*5*)gumOutros=5,
          (*6*)gumAreaP2:
          begin
            Tipo := TGrandezaUnidMed.gumAreaM2;
            Fator := 10.76391;
            MyQtd := QrVMISdoVrtArM2.Value;
          end;
          (*7*)gumPesoTon:
          begin
            Tipo := TGrandezaUnidMed.gumPesoKG;
            Fator := 0.001;
            MyQtd := QrVMISdoVrtPeso.Value;
          end;
          else
          begin
            Tipo := TGrandezaUnidMed.gumOutros;
            Fator := 0;
            MyQtd := 0;
          end;
        end;
        if Tipo = TGrandezaUnidMed.gumOutros then
        begin
          Geral.MB_Erro('Grandeza indefinida!');
          Exit;
        end;
        case Tipo of
          gumPeca    : MyBas := QrVMIPecas.Value;
          gumAreaM2  : MyBas := QrVMIAreaM2.Value;
          gumPesoKg  : MyBas := QrVMIPesoKg.Value;
          else         MyBas := 0;
        end;
        if MyBas > 0 then
          MyVlU := QrVMIValorT.Value / MyQtd
        else
          MyVlU := 0;
        //
        if MyVlU <= 0 then
        begin
          Geral.MB_Erro('Valor unit�rio indefinido!');
          Exit;
        end;
        case Tipo of
          gumPeca    : VL_ITEM := MyVlU * QrVMISdoVrtPeca.Value;
          gumAreaM2  : VL_ITEM := MyVlU * QrVMISdoVrtArM2.Value;
          gumPesoKg  : VL_ITEM := MyVlU * QrVMISdoVrtPeso.Value;
          else         VL_ITEM := 0;
        end;
        if VL_ITEM <= 0 then
        begin
          Geral.MB_Erro('Valor total do item indefinido!');
          Exit;
        end;
        BalID          := Integer(TSPED_EFD_Bal.sebalVSMovItX);
        BalNum         := FAnoMes;
        BalItm         := QrVMIGraGruX.Value;
        COD_ITEM       := Geral.FF0(QrVMIGraGruX.Value);
        UNID           := QrVMISigla.Value;
        IND_PROP       := dmkPF.EscolhaDe2Str(FEmpresa = QrVMIEmpresa.Value, '0', '2');
        COD_PART       := dmkPF.EscolhaDe2Str(FEmpresa = QrVMIEmpresa.Value, '', Geral.FF0(QrVMIEmpresa.Value));
        TXT_COMPL      := '';
        COD_CTA        := UFinanceiro.MontaSPED_COD_CTA(RGNivPlaCta1.ItemIndex, EdGenPlaCta1.ValueVariant);
        QTD            := MyQtd * Fator;
        VL_UNIT        := MyVlU;
        //VL_ITEM        := Acima
        VL_ITEM_IR     := VL_ITEM;
        //
        InsereItemAtual(BalID, BalNum, BalItm, COD_ITEM, UNID, IND_PROP, COD_PART,
        TXT_COMPL, COD_CTA, QTD, VL_UNIT, VL_ITEM, VL_ITEM_IR);
        //
        QrVMI.Next;
      end;
      //
    end;
  end;
end;

procedure TFmSPED_EFD_H010_Balancos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSPED_EFD_H010_Balancos.CGImportarClick(Sender: TObject);
begin
  GBImportar1.Visible := DmkPF.IntInConjunto2(1, CGImportar.Value);
  GBImportar2.Visible := DmkPF.IntInConjunto2(2, CGImportar.Value);
end;

procedure TFmSPED_EFD_H010_Balancos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSPED_EFD_H010_Balancos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
end;

procedure TFmSPED_EFD_H010_Balancos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmSPED_EFD_H010_Balancos.InsereItemAtual(BalID, BalNum, BalItm:
  Integer;   COD_ITEM, UNID, IND_PROP, COD_PART, TXT_COMPL, COD_CTA: String;
  QTD, VL_UNIT, VL_ITEM, VL_ITEM_IR: Double): Boolean;
const
  REG = 'H010';
var
  ImporExpor, AnoMes, Empresa, H005, LinArq: Integer;
  SQLType: TSQLType;
begin
  Result := False;
  //SQLType        := ImgTipo.SQLType?;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  H005           := FH005;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrH010, Dmod.MyDB, [
  'SELECT * ',
  'FROM efd_h010 ',
  'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
  'AND AnoMes=' + Geral.FF0(FAnoMes),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND H005=' + Geral.FF0(FH005),
  'AND BalID=' + Geral.FF0(BalID),
  'AND BalNum=' + Geral.FF0(BalNum),
  'AND BalItm=' + Geral.FF0(BalItm),
  '']);
  if QrH010.RecordCount > 0 then
  begin
    LinArq  := QrH010LinArq.Value;
    SQLType := stUpd;
  end else
  begin
    LinArq := 0;
    LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_h010', 'LinArq', [
    (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
    stIns, LinArq, siPositivo, nil);
    //
    SQLType := stIns;
  end;
(*  COD_ITEM       := ;
  UNID           := ;
  QTD            := ;
  VL_UNIT        := ;
  VL_ITEM        := ;
  IND_PROP       := ;
  COD_PART       := ;
  TXT_COMPL      := ;
  COD_CTA        := ;
  VL_ITEM_IR     := ;
  BalID          := ;
  BalNum         := ;
  BalItm         := ;*)
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efd_h010', False, [
  'H005', 'REG', 'COD_ITEM',
  'UNID', 'QTD', 'VL_UNIT',
  'VL_ITEM', 'IND_PROP', 'COD_PART',
  'TXT_COMPL', 'COD_CTA', 'VL_ITEM_IR',
  'BalID', 'BalNum', 'BalItm'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  H005, REG, COD_ITEM,
  UNID, QTD, VL_UNIT,
  VL_ITEM, IND_PROP, COD_PART,
  TXT_COMPL, COD_CTA, VL_ITEM_IR,
  BalID, BalNum, BalItm], [
  ImporExpor, AnoMes, Empresa, LinArq], True);
end;

function TFmSPED_EFD_H010_Balancos.ReopenGraGruX(Nivel1: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.Controle, gg1.UnidMed, med.Sigla',
  'FROM gragrux ggx',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed',
  'WHERE ggx.GraGru1=' + Geral.FF0(Nivel1),
  'ORDER BY ggx.Controle ',
  '']);
end;

procedure TFmSPED_EFD_H010_Balancos.RGNivPlaCta1Click(Sender: TObject);
begin
  DModFin.ReopenNivelSel(QrNivelSel1, EdGenPlaCta1, CBGenPlaCta1, RGNivPlaCta1);
end;

procedure TFmSPED_EFD_H010_Balancos.RGNivPlaCta2Click(Sender: TObject);
begin
  DModFin.ReopenNivelSel(QrNivelSel2, EdGenPlaCta2, CBGenPlaCta2, RGNivPlaCta2);
end;

end.
