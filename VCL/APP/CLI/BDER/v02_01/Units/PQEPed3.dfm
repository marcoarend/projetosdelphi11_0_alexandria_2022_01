object FmPQEPed3: TFmPQEPed3
  Left = 366
  Top = 203
  Caption = 'QUI-ENTRA-009 :: Entrada de PQ por Pedido X NFe'
  ClientHeight = 604
  ClientWidth = 1183
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1183
    Height = 437
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 1183
      Height = 437
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Pesquisa '
        object PnCfgPesq: TPanel
          Left = 0
          Top = 0
          Width = 1175
          Height = 409
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object PainelDados: TPanel
            Left = 0
            Top = 181
            Width = 1175
            Height = 40
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Label1: TLabel
              Left = 8
              Top = 0
              Width = 40
              Height = 13
              Caption = 'Numero:'
            end
            object Label2: TLabel
              Left = 96
              Top = 0
              Width = 55
              Height = 13
              Caption = 'Refer'#234'ncia:'
            end
            object SbPesqCod: TSpeedButton
              Left = 66
              Top = 14
              Width = 25
              Height = 25
              Caption = '>'
              OnClick = SbPesqCodClick
            end
            object SbPesqRef: TSpeedButton
              Left = 420
              Top = 14
              Width = 25
              Height = 25
              Caption = '>'
              OnClick = SbPesqRefClick
            end
            object EdCodUsu: TdmkEdit
              Left = 8
              Top = 16
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdReferencia: TdmkEdit
              Left = 96
              Top = 16
              Width = 321
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '%%'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = '%%'
              ValWarn = False
            end
          end
          object DBGrid1: TDBGrid
            Left = 0
            Top = 221
            Width = 733
            Height = 188
            Align = alLeft
            DataSource = DsPediVda
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGrid1DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ReferenPedi'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaEntra'
                Title.Caption = 'Dta cadastro'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaPrevi'
                Title.Caption = 'Dta previs'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Cliente'
                Title.Caption = 'Fornecedor'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_ENT'
                Title.Caption = 'Nome do Fornecedor'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantP'
                Title.Caption = 'Itens'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValLiq'
                Title.Caption = 'Val. Liq.'
                Visible = True
              end>
          end
          object DBMemo1: TDBMemo
            Left = 733
            Top = 221
            Width = 442
            Height = 188
            Align = alClient
            DataField = 'Observa'
            DataSource = DsPediVda
            TabOrder = 3
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 1175
            Height = 181
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 1175
              Height = 101
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label6: TLabel
                Left = 352
                Top = 44
                Width = 57
                Height = 13
                Caption = 'Fornecedor:'
              end
              object LaCI: TLabel
                Left = 8
                Top = 43
                Width = 70
                Height = 13
                Caption = 'Cliente interno:'
              end
              object LaEmpresa: TLabel
                Left = 8
                Top = 4
                Width = 44
                Height = 13
                Caption = 'Empresa:'
              end
              object CkTipoNF: TdmkCheckBox
                Left = 8
                Top = 84
                Width = 162
                Height = 17
                Caption = 'Nota Fiscal Eletr'#244'nica (NF-e)'
                Checked = True
                State = cbChecked
                TabOrder = 6
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
              object EdFornecedor: TdmkEditCB
                Left = 352
                Top = 60
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBFornecedor
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBFornecedor: TdmkDBLookupComboBox
                Left = 412
                Top = 60
                Width = 433
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NOME'
                ListSource = DsFornecedores
                TabOrder = 5
                dmkEditCB = EdFornecedor
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdCI: TdmkEditCB
                Left = 8
                Top = 59
                Width = 55
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBCI
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBCI: TdmkDBLookupComboBox
                Left = 64
                Top = 59
                Width = 284
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NOME'
                ListSource = DsCI
                TabOrder = 3
                dmkEditCB = EdCI
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdEmpresa: TdmkEditCB
                Left = 8
                Top = 20
                Width = 55
                Height = 20
                TabStop = False
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Empresa'
                UpdCampo = 'Empresa'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBEmpresa
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBEmpresa: TdmkDBLookupComboBox
                Left = 68
                Top = 20
                Width = 777
                Height = 21
                KeyField = 'Filial'
                ListField = 'NOMEFILIAL'
                ListSource = DModG.DsEmpresas
                TabOrder = 1
                TabStop = False
                dmkEditCB = EdEmpresa
                QryCampo = 'Empresa'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
            end
            object PnNFs: TPanel
              Left = 0
              Top = 101
              Width = 1175
              Height = 80
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object LarefNFe: TLabel
                Left = 8
                Top = 2
                Width = 150
                Height = 13
                Caption = 'Chave de acesso da NF-e (VP):'
              end
              object LaModNF: TLabel
                Left = 292
                Top = 1
                Width = 38
                Height = 13
                Caption = 'Modelo:'
              end
              object LaSerie: TLabel
                Left = 335
                Top = 1
                Width = 27
                Height = 13
                Caption = 'S'#233'rie:'
              end
              object LaNF: TLabel
                Left = 368
                Top = 2
                Width = 34
                Height = 13
                Caption = 'NF VP:'
              end
              object LaNFe_CC: TLabel
                Left = 8
                Top = 42
                Width = 150
                Height = 13
                Caption = 'Chave de acesso da NF-e (CC):'
              end
              object LamodCC: TLabel
                Left = 292
                Top = 41
                Width = 38
                Height = 13
                Caption = 'Modelo:'
              end
              object LaSerCC: TLabel
                Left = 335
                Top = 41
                Width = 27
                Height = 13
                Caption = 'S'#233'rie:'
              end
              object LaNF_CC: TLabel
                Left = 367
                Top = 40
                Width = 34
                Height = 13
                Caption = 'NF CC:'
              end
              object SbNF_VP: TSpeedButton
                Left = 436
                Top = 14
                Width = 25
                Height = 25
                Caption = '<>'
                OnClick = SbNF_VPClick
              end
              object Label4: TLabel
                Left = 468
                Top = 1
                Width = 125
                Height = 13
                Caption = 'Atrelamento com XML BD:'
              end
              object Label9: TLabel
                Left = 468
                Top = 41
                Width = 125
                Height = 13
                Caption = 'Atrelamento com XML BD:'
              end
              object SbNF_CC: TSpeedButton
                Left = 436
                Top = 54
                Width = 25
                Height = 25
                Caption = '<>'
                OnClick = SbNF_CCClick
              end
              object EdrefNFe: TdmkEdit
                Left = 8
                Top = 16
                Width = 280
                Height = 21
                MaxLength = 44
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnExit = EdrefNFeExit
                OnRedefinido = EdrefNFeRedefinido
              end
              object EdModNF: TdmkEdit
                Left = 292
                Top = 16
                Width = 39
                Height = 21
                Alignment = taRightJustify
                MaxLength = 2
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdSerie: TdmkEdit
                Left = 335
                Top = 16
                Width = 31
                Height = 21
                Alignment = taRightJustify
                MaxLength = 3
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdNF: TdmkEdit
                Left = 368
                Top = 16
                Width = 66
                Height = 21
                Alignment = taRightJustify
                MaxLength = 9
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdNFChange
              end
              object EdNFe_CC: TdmkEdit
                Left = 8
                Top = 56
                Width = 280
                Height = 21
                MaxLength = 44
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnExit = EdNFe_CCExit
                OnRedefinido = EdNFe_CCRedefinido
              end
              object EdmodCC: TdmkEdit
                Left = 292
                Top = 56
                Width = 39
                Height = 21
                Alignment = taRightJustify
                MaxLength = 2
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdSerCC: TdmkEdit
                Left = 335
                Top = 56
                Width = 31
                Height = 21
                Alignment = taRightJustify
                MaxLength = 3
                TabOrder = 9
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdNF_CC: TdmkEdit
                Left = 367
                Top = 56
                Width = 67
                Height = 21
                Alignment = taRightJustify
                TabOrder = 10
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 6
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000000'
                QryCampo = 'NF_CC'
                UpdCampo = 'NF_CC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdNF_CCChange
              end
              object EdNFVP_StaLnk: TdmkEdit
                Left = 468
                Top = 16
                Width = 28
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                MaxLength = 9
                ReadOnly = True
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdNFVP_FatID: TdmkEdit
                Left = 500
                Top = 16
                Width = 39
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                MaxLength = 2
                ReadOnly = True
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdNFVP_FatNum: TdmkEdit
                Left = 543
                Top = 16
                Width = 56
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                MaxLength = 3
                ReadOnly = True
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdNFCC_StaLnk: TdmkEdit
                Left = 468
                Top = 56
                Width = 28
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                MaxLength = 9
                ReadOnly = True
                TabOrder = 11
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdNFCC_FatID: TdmkEdit
                Left = 500
                Top = 56
                Width = 39
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                MaxLength = 2
                ReadOnly = True
                TabOrder = 12
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdNFCC_FatNum: TdmkEdit
                Left = 543
                Top = 56
                Width = 56
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                MaxLength = 3
                ReadOnly = True
                TabOrder = 13
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Verifica pedido'
        ImageIndex = 1
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 1175
          Height = 409
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGGru: TDBGrid
            Left = 0
            Top = 0
            Width = 1175
            Height = 409
            Align = alClient
            DataSource = DsPediVdaGru
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 403
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SaldoQtd'
                Title.Caption = 'Saldo pedido'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantP'
                Title.Caption = 'Qtd pedido'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrecoF'
                Title.Caption = 'Pre'#231'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValBru'
                Title.Caption = 'Val. Bru.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValLiq'
                Title.Caption = 'Val. liq.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end>
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Verifica pedido X NFe'
        ImageIndex = 2
        object DBGPedXNFe: TDBGrid
          Left = 0
          Top = 0
          Width = 1175
          Height = 409
          Align = alClient
          DataSource = DsPedXNFe
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'DiferencaItm'
              Title.Caption = 'Diferen'#231'a'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGru1'
              Title.Caption = 'Produto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Nome do produto no ERP'
              Width = 280
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PrecoP'
              Title.Caption = 'Pre'#231'o pedido'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'vTotItm'
              Title.Caption = '$ Pedido'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_cProd'
              Title.Caption = 'C'#243'd.Prod.NFe'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_xProd'
              Title.Caption = 'Nome do produtio na NFe'
              Width = 280
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_vTotItm'
              Title.Caption = '$ NFe'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PrecoF'
              Title.Caption = 'Pre'#231'o faturado'
              Width = 90
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1183
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1135
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1087
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 401
        Height = 32
        Caption = 'Entrada de PQ por Pedido X NFe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 401
        Height = 32
        Caption = 'Entrada de PQ por Pedido X NFe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 401
        Height = 32
        Caption = 'Entrada de PQ por Pedido X NFe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 485
    Width = 1183
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1179
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 632
        Height = 16
        Caption = 
          'VP: NFe de venda do fornecedor para a empresa ou cliente.       ' +
          '  CC: NFe do cliente interno para a empresa.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 632
        Height = 16
        Caption = 
          'VP: NFe de venda do fornecedor para a empresa ou cliente.       ' +
          '  CC: NFe do cliente interno para a empresa.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 1179
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 540
    Width = 1183
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1179
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 1035
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
      object BtAtzSdos: TBitBtn
        Tag = 18
        Left = 136
        Top = 5
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Atz Saldos'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAtzSdosClick
      end
    end
  end
  object QrFornece: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT fo.Codigo,'
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOMEENTIDADE'
      'FROM entidades fo, PQPed pp'
      'WHERE pp.Fornece=fo.Codigo'
      'AND pp.Sit <:P0'
      'AND fo.Fornece2='#39'V'#39
      'ORDER BY NOMEENTIDADE')
    Left = 136
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      FixedChar = True
      Size = 128
    end
  end
  object QrPedidos: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPedidosAfterScroll
    SQL.Strings = (
      'SELECT Codigo'
      'FROM pqped'
      'WHERE Fornece=:P0'
      'AND Sit<:P1'
      '')
    Left = 236
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPedidosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pqped.Codigo'
      DisplayFormat = '000'
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 136
    Top = 52
  end
  object DsPedidos: TDataSource
    DataSet = QrPedidos
    Left = 236
    Top = 52
  end
  object QrPediVda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvd.Codigo, pvd.CodUsu, pvd.Cliente, '
      'pvd.DtaEntra, pvd.DtaPrevi, pvd.Observa, '
      'pvd.QuantP, pvd.ValLiq, pvd.ReferenPedi,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT '
      'FROM pedivda pvd'
      'LEFT JOIN entidades ent ON ent.Codigo=pvd.Cliente'
      'WHERE pvd.EntSai=0'
      'AND pvd.CodUsu=2'
      'OR ReferenPedi LIKE "%74%"')
    Left = 292
    Top = 208
    object QrPediVdaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediVdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrPediVdaCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPediVdaDtaEntra: TDateField
      FieldName = 'DtaEntra'
      Required = True
    end
    object QrPediVdaDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
      Required = True
    end
    object QrPediVdaObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPediVdaQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrPediVdaValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrPediVdaReferenPedi: TWideStringField
      FieldName = 'ReferenPedi'
      Size = 60
    end
    object QrPediVdaNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrPediVdaRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
    end
    object QrPediVdaCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object QrPediVdaMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrPediVdaCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
    end
  end
  object DsPediVda: TDataSource
    DataSet = QrPediVda
    Left = 292
    Top = 260
  end
  object QrPediVdaGru: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPediVdaGruAfterOpen
    BeforeClose = QrPediVdaGruBeforeClose
    SQL.Strings = (
      'SELECT pvi.Controle, SUM(QuantP) QuantP, SUM(ValLiq) ValLiq, '
      'SUM(Customizad) ItensCustomizados,'
      'gti.Codigo GRATAMCAD,'
      'gg1.CodUsu, gg1.Nome, gg1.Nivel1, pgt.Fracio'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip   pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pvi.Codigo=:P0'
      'GROUP BY gg1.Nivel1'
      'ORDER BY pci.Controle')
    Left = 392
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaGruCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediVdaGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPediVdaGruNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPediVdaGruGRATAMCAD: TIntegerField
      FieldName = 'GRATAMCAD'
    end
    object QrPediVdaGruQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrPediVdaGruValLiq: TFloatField
      FieldName = 'ValLiq'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPediVdaGruItensCustomizados: TFloatField
      FieldName = 'ItensCustomizados'
    end
    object QrPediVdaGruFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrPediVdaGruControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPediVdaGruValBru: TFloatField
      FieldName = 'ValBru'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPediVdaGruPrecoF: TFloatField
      FieldName = 'PrecoF'
      DisplayFormat = '#,###,###,##0.000000'
    end
    object QrPediVdaGruSaldoQtd: TFloatField
      FieldName = 'SaldoQtd'
    end
  end
  object DsPediVdaGru: TDataSource
    DataSet = QrPediVdaGru
    Left = 392
    Top = 260
  end
  object QrPediVdaIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pedivdaits')
    Left = 492
    Top = 205
    object QrPediVdaItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediVdaItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediVdaItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrPediVdaItsPrecoO: TFloatField
      FieldName = 'PrecoO'
      Required = True
    end
    object QrPediVdaItsPrecoR: TFloatField
      FieldName = 'PrecoR'
      Required = True
    end
    object QrPediVdaItsQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrPediVdaItsQuantC: TFloatField
      FieldName = 'QuantC'
      Required = True
    end
    object QrPediVdaItsQuantV: TFloatField
      FieldName = 'QuantV'
      Required = True
    end
    object QrPediVdaItsValBru: TFloatField
      FieldName = 'ValBru'
      Required = True
    end
    object QrPediVdaItsDescoP: TFloatField
      FieldName = 'DescoP'
      Required = True
    end
    object QrPediVdaItsDescoV: TFloatField
      FieldName = 'DescoV'
      Required = True
    end
    object QrPediVdaItsValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrPediVdaItsPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrPediVdaItsMedidaC: TFloatField
      FieldName = 'MedidaC'
      Required = True
    end
    object QrPediVdaItsMedidaL: TFloatField
      FieldName = 'MedidaL'
      Required = True
    end
    object QrPediVdaItsMedidaA: TFloatField
      FieldName = 'MedidaA'
      Required = True
    end
    object QrPediVdaItsMedidaE: TFloatField
      FieldName = 'MedidaE'
      Required = True
    end
    object QrPediVdaItsPercCustom: TFloatField
      FieldName = 'PercCustom'
      Required = True
    end
    object QrPediVdaItsCustomizad: TSmallintField
      FieldName = 'Customizad'
      Required = True
    end
    object QrPediVdaItsInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Required = True
    end
    object QrPediVdaItsOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrPediVdaItsReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 11
    end
    object QrPediVdaItsMulti: TIntegerField
      FieldName = 'Multi'
      Required = True
    end
    object QrPediVdaItsvProd: TFloatField
      FieldName = 'vProd'
      Required = True
    end
    object QrPediVdaItsvFrete: TFloatField
      FieldName = 'vFrete'
      Required = True
    end
    object QrPediVdaItsvSeg: TFloatField
      FieldName = 'vSeg'
      Required = True
    end
    object QrPediVdaItsvOutro: TFloatField
      FieldName = 'vOutro'
      Required = True
    end
    object QrPediVdaItsvDesc: TFloatField
      FieldName = 'vDesc'
      Required = True
    end
    object QrPediVdaItsvBC: TFloatField
      FieldName = 'vBC'
      Required = True
    end
  end
  object QrFornecedores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NOME'
      'FROM entidades'
      'WHERE Fornece2='#39'V'#39
      'ORDER BY Nome')
    Left = 784
    Top = 100
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedoresNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrFornecedoresTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrFornecedoresCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrFornecedoresCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 788
    Top = 148
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NOME'
      'FROM entidades'
      'WHERE Cliente2='#39'V'#39
      'ORDER BY Nome')
    Left = 692
    Top = 104
    object QrCITipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCICNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrCICPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 692
    Top = 152
  end
  object Qr00_NFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, IDCtrl,'
      'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk,'
      'ide_mod, ide_serie, ide_nNF  '
      'FROM nfecaba '
      'WHERE FatID=53 '
      'AND Empresa=-11 '
      'AND ide_mod>0 '
      'AND ide_serie>0 '
      'AND ide_nNF>0 ')
    Left = 614
    Top = 517
    object Qr00_NFeCabAFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object Qr00_NFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object Qr00_NFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object Qr00_NFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object Qr00_NFeCabAAtrelaFatID: TIntegerField
      FieldName = 'AtrelaFatID'
      Required = True
    end
    object Qr00_NFeCabAAtrelaFatNum: TIntegerField
      FieldName = 'AtrelaFatNum'
      Required = True
    end
    object Qr00_NFeCabAAtrelaStaLnk: TSmallintField
      FieldName = 'AtrelaStaLnk'
      Required = True
    end
    object Qr00_NFeCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
      Required = True
    end
    object Qr00_NFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      Required = True
    end
    object Qr00_NFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Required = True
    end
    object Qr00_NFeCabAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object Qr00_NFeCabAemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
  end
  object QrItsI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=53'
      'AND FatNum=27'
      'AND Empresa =-11'
      'ORDER BY nItem')
    Left = 304
    Top = 356
    object QrItsIFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrItsIFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrItsIEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrItsInItem: TIntegerField
      FieldName = 'nItem'
      Required = True
    end
    object QrItsIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrItsIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrItsIprod_cBarra: TWideStringField
      FieldName = 'prod_cBarra'
      Size = 30
    end
    object QrItsIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrItsIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrItsIprod_CEST: TIntegerField
      FieldName = 'prod_CEST'
      Required = True
    end
    object QrItsIprod_indEscala: TWideStringField
      FieldName = 'prod_indEscala'
      Size = 1
    end
    object QrItsIprod_CNPJFab: TWideStringField
      FieldName = 'prod_CNPJFab'
      Size = 14
    end
    object QrItsIprod_cBenef: TWideStringField
      FieldName = 'prod_cBenef'
      Size = 10
    end
    object QrItsIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrItsIprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrItsIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
      Required = True
    end
    object QrItsIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrItsIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
      Required = True
    end
    object QrItsIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
      Required = True
    end
    object QrItsIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      Required = True
    end
    object QrItsIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrItsIprod_cBarraTrib: TWideStringField
      FieldName = 'prod_cBarraTrib'
      Size = 30
    end
    object QrItsIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrItsIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
      Required = True
    end
    object QrItsIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
      Required = True
    end
    object QrItsIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Required = True
    end
    object QrItsIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Required = True
    end
    object QrItsIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
      Required = True
    end
    object QrItsIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrItsIprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
      Required = True
    end
    object QrItsIprod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrItsIprod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrItsITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
      Required = True
    end
    object QrItsI_Ativo_: TSmallintField
      FieldName = '_Ativo_'
      Required = True
    end
    object QrItsIInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Required = True
    end
    object QrItsIEhServico: TIntegerField
      FieldName = 'EhServico'
      Required = True
    end
    object QrItsIUsaSubsTrib: TSmallintField
      FieldName = 'UsaSubsTrib'
      Required = True
    end
    object QrItsIICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      Required = True
    end
    object QrItsIICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
      Required = True
    end
    object QrItsIICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      Required = True
    end
    object QrItsIICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
      Required = True
    end
    object QrItsIIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      Required = True
    end
    object QrItsIIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
      Required = True
    end
    object QrItsIIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      Required = True
    end
    object QrItsIIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
      Required = True
    end
    object QrItsIPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      Required = True
    end
    object QrItsIPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
      Required = True
    end
    object QrItsIPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      Required = True
    end
    object QrItsIPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
      Required = True
    end
    object QrItsICOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      Required = True
    end
    object QrItsICOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
      Required = True
    end
    object QrItsICOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      Required = True
    end
    object QrItsICOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
      Required = True
    end
    object QrItsIMeuID: TIntegerField
      FieldName = 'MeuID'
      Required = True
    end
    object QrItsINivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrItsIGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrItsIUnidMedCom: TIntegerField
      FieldName = 'UnidMedCom'
      Required = True
    end
    object QrItsIUnidMedTrib: TIntegerField
      FieldName = 'UnidMedTrib'
      Required = True
    end
    object QrItsIICMSRec_vBCST: TFloatField
      FieldName = 'ICMSRec_vBCST'
      Required = True
    end
    object QrItsIICMSRec_vICMSST: TFloatField
      FieldName = 'ICMSRec_vICMSST'
      Required = True
    end
    object QrItsIICMSRec_pAliqST: TFloatField
      FieldName = 'ICMSRec_pAliqST'
      Required = True
    end
    object QrItsITem_II: TSmallintField
      FieldName = 'Tem_II'
      Required = True
    end
    object QrItsIprod_nFCI: TWideStringField
      FieldName = 'prod_nFCI'
      Size = 36
    end
    object QrItsIStqMovValA: TIntegerField
      FieldName = 'StqMovValA'
      Required = True
    end
    object QrItsIAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrItsIAtrelaID: TIntegerField
      FieldName = 'AtrelaID'
      Required = True
    end
  end
  object QrItsS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeitss'
      'WHERE FatID=53 '
      'AND FatNum=27 '
      'AND Empresa =-11 '
      'AND nItem=1'
      ' ')
    Left = 648
    Top = 352
    object QrItsSCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
      Required = True
    end
  end
  object QrItsV: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeitsv'
      'WHERE FatID=53 '
      'AND FatNum=27 '
      'AND Empresa =-11 '
      'AND nItem=1'
      ' ')
    Left = 372
    Top = 356
    object QrItsVnItem: TIntegerField
      FieldName = 'nItem'
      Required = True
    end
    object QrItsVInfAdProd: TWideMemoField
      FieldName = 'InfAdProd'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrItsN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeitsn'
      'WHERE FatID=53 '
      'AND FatNum=27 '
      'AND Empresa =-11 '
      'AND nItem=1'
      ' ')
    Left = 460
    Top = 356
    object QrItsNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
      Required = True
    end
    object QrItsNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
      Required = True
    end
    object QrItsNICMS_CSOSN: TIntegerField
      FieldName = 'ICMS_CSOSN'
      Required = True
    end
  end
  object QrItsO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeitso'
      'WHERE FatID=53 '
      'AND FatNum=27 '
      'AND Empresa =-11 '
      'AND nItem=1'
      ' ')
    Left = 536
    Top = 360
    object QrItsOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrItsOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
  end
  object QrItsQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeitsq'
      'WHERE FatID=53 '
      'AND FatNum=27 '
      'AND Empresa =-11 '
      'AND nItem=1'
      ' ')
    Left = 592
    Top = 420
    object QrItsQPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
      Required = True
    end
  end
  object QrPedXNFe: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _INN_PED_X_NFe_PED_;'
      'CREATE TABLE _INN_PED_X_NFe_PED_'
      'SELECT SUM(pvi.QuantP) QuantP, '
      'SUM(vProd) vProd, SUM(vFrete) vFrete, SUM(vSeg) vSeg, '
      'SUM(vDesc) vDesc, SUM(vOutro) vOutro, '
      'SUM(vProd + vFrete + vSeg - vDesc + vOutro) vTotItm,'
      'SUM(pvi.ValLiq) ValLiq, SUM(pvi.ValBru) ValBru, '
      'pvi.GraGruX, ggx.GraGru1, gg1.Nome, '
      'IF(pvi.QuantP <= 0, 0.00, pvi.vProd / pvi.QuantP) PrecoF, '
      'SUM(pvi.QuantP - pvi.QuantV) SaldoQtd '
      'FROM bluederm_colosso.pedivdaits pvi '
      
        'LEFT JOIN bluederm_colosso.gragrux ggx ON ggx.Controle=pvi.GraGr' +
        'uX '
      'LEFT JOIN bluederm_colosso.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE pvi.Codigo=24'
      'GROUP BY pvi.GraGruX;'
      ''
      'DROP TABLE IF EXISTS _INN_PED_X_NFe_NFe_;'
      'CREATE TABLE _INN_PED_X_NFe_NFe_'
      'SELECT nfi.GraGruX, nfi.prod_cProd, nfi.prod_xProd,'
      'SUM(prod_qCom) prod_qCom, SUM(prod_vProd) prod_vProd,'
      'SUM(prod_vFrete) prod_vFrete, SUM(prod_vSeg) prod_vSeg,'
      'SUM(prod_vDesc) prod_vDesc, SUM(prod_vOutro) prod_vOutro,'
      'SUM(prod_vProd + prod_vFrete + prod_vSeg - prod_vDesc + '
      'prod_vOutro) prod_vTotItm'
      ''
      ' '
      'FROM bluederm_colosso.nfeitsi nfi'
      'WHERE FatID=53'
      'AND FatNum=236'
      'AND Empresa=-11'
      'GROUP BY nfi.GraGruX'
      ';'
      ''
      
        'SELECT nfi.prod_vTotItm - ped.vTotItm DiferencaItm, ped.*, nfi.*' +
        ' '
      'FROM _INN_PED_X_NFe_PED_ ped'
      'LEFT JOIN _INN_PED_X_NFe_NFe_ nfi ON ped.GraGrux=nfi.GraGruX'
      ';')
    Left = 108
    Top = 188
    object QrPedXNFeDiferencaItm: TFloatField
      FieldName = 'DiferencaItm'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFeQuantP: TFloatField
      FieldName = 'QuantP'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPedXNFePrecoP: TFloatField
      FieldName = 'PrecoP'
    end
    object QrPedXNFevProd: TFloatField
      FieldName = 'vProd'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFevFrete: TFloatField
      FieldName = 'vFrete'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFevSeg: TFloatField
      FieldName = 'vSeg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFevDesc: TFloatField
      FieldName = 'vDesc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFevOutro: TFloatField
      FieldName = 'vOutro'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFevTotItm: TFloatField
      FieldName = 'vTotItm'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFeValLiq: TFloatField
      FieldName = 'ValLiq'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFeValBru: TFloatField
      FieldName = 'ValBru'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFeGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrPedXNFeGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrPedXNFeNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrPedXNFeSaldoQtd: TFloatField
      FieldName = 'SaldoQtd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPedXNFeprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrPedXNFePrecoF: TFloatField
      FieldName = 'PrecoF'
      DisplayFormat = '#,###,###,##0.0000000000;-#,###,###,##0.0000000000; '
    end
    object QrPedXNFeprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrPedXNFeprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFeprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFeprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFeprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFeprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFeprod_vTotItm: TFloatField
      FieldName = 'prod_vTotItm'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedXNFeprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
  end
  object DsPedXNFe: TDataSource
    DataSet = QrPedXNFe
    Left = 108
    Top = 240
  end
  object QrCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=53'
      'AND FatNum=27'
      'AND Empresa =-11'
      '')
    Left = 244
    Top = 356
    object QrCabAemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
      Required = True
    end
    object QrCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
  end
end
