unit Principal2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UMySQLModule, ExtCtrls, Menus, Grids, DBGrids, Db, (*DBTables,*)
  TypInfo, StdCtrls, ZCF2, ToolWin, ComCtrls, UnInternalConsts, ExtDlgs, jpeg,
  mySQLDbTables, Buttons, WinSkinStore, WinSkinData, frxClass, frxDBSet,
  dmkEditDateTimePicker, Variants, dmkEdit, dmkGeral, AdvShapeButton, AdvToolBar,
  AdvGlowButton, UrlMon, AdvMenus, AdvToolBarStylers, AdvPreviewMenu,
  AdvPreviewMenuStylers, UnFinanceiro, UnDmkProcFunc, xmldom, XMLIntf,
  msxmldom, XMLDoc, dmkImage, Vcl.Imaging.pngimage, UnDmkEnums, IdBaseComponent,
  IdComponent, IdIPWatch, ShellApi, dmkPageControl,
  DmkBase64, DmkCoding, ZLibExGZ, UnMyPrinters,
  IdZLibCompressorBase, IdCompressorZLib, ZipForge, frxGZip,
  System.ZLib(*IdZLib*), UnMyVCLRef,
  UnProjGroup_Vars, UnProjGroup_Consts, UMySQLDB, UnComps_Vars, UnVS_CRC_PF,
  UnAppEnums, UnGrl_Vars, UnMyLinguas, acPNG, sPanel, Vcl.ImgList, sBitBtn,
  sButton;

type
(*
  TTipoMaterialNF = (tnfMateriaPrima, tnfUsoEConsumo);
  TcpCalc = (cpJurosMes, cpMulta);
*)
  TFmPrincipal2 = class(TForm)
    OpenPictureDialog1: TOpenPictureDialog;
    Timer1: TTimer;
    PMProdImp: TPopupMenu;
    Estoque1: TMenuItem;
    Histrico1: TMenuItem;
    Vendas1: TMenuItem;
    QrView1: TmySQLQuery;
    QrView1Codigo: TIntegerField;
    QrView1Campo: TIntegerField;
    QrView1Topo: TIntegerField;
    QrView1MEsq: TIntegerField;
    QrView1Larg: TIntegerField;
    QrView1FTam: TIntegerField;
    QrView1Negr: TIntegerField;
    QrView1Ital: TIntegerField;
    QrView1Lk: TIntegerField;
    QrView1DataCad: TDateField;
    QrView1DataAlt: TDateField;
    QrView1UserCad: TIntegerField;
    QrView1UserAlt: TIntegerField;
    QrView1Altu: TIntegerField;
    QrView1Pref: TWideStringField;
    QrView1Sufi: TWideStringField;
    QrView1Padr: TWideStringField;
    QrTopos1: TmySQLQuery;
    QrTopos1TopR: TIntegerField;
    QrView1AliV: TIntegerField;
    QrView1AliH: TIntegerField;
    QrView1TopR: TIntegerField;
    QrView1PrEs: TIntegerField;
    StatusBar: TStatusBar;
    PMGeral: TPopupMenu;
    Button1: TButton;
    Memo3: TMemo;
    OpenDialog1: TOpenDialog;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvToolBarPager11: TAdvPage;
    AdvToolBarPager12: TAdvPage;
    AdvToolBarPager13: TAdvPage;
    AdvShapeButton1: TAdvShapeButton;
    AdvPage1: TAdvPage;
    AdvPage2: TAdvPage;
    AdvPage4: TAdvPage;
    AdvToolBar1: TAdvToolBar;
    AdvGlowButton1: TAdvGlowButton;
    AdvGlowButton3: TAdvGlowButton;
    AdvGlowButton5: TAdvGlowButton;
    AdvGlowButton7: TAdvGlowButton;
    AdvToolBar5: TAdvToolBar;
    AdvGlowButton17: TAdvGlowButton;
    AdvGlowButton18: TAdvGlowButton;
    AGBEntraUC: TAdvGlowButton;
    AdvGlowButton21: TAdvGlowButton;
    AdvToolBar6: TAdvToolBar;
    AdvGlowButton32: TAdvGlowButton;
    AdvGlowButton4: TAdvGlowButton;
    GBBaixaETE: TAdvGlowButton;
    GBBaixaOutros: TAdvGlowButton;
    AGBRelatUC: TAdvGlowButton;
    AdvGlowButton37: TAdvGlowButton;
    AdvGlowButton38: TAdvGlowButton;
    AdvGlowButton28: TAdvGlowButton;
    AdvGlowButton29: TAdvGlowButton;
    AdvToolBar9: TAdvToolBar;
    AdvToolBar10: TAdvToolBar;
    AdvGlowButton46: TAdvGlowButton;
    AdvGlowButton47: TAdvGlowButton;
    AdvToolBar11: TAdvToolBar;
    AdvGlowButton30: TAdvGlowButton;
    AdvPage5: TAdvPage;
    AdvPage6: TAdvPage;
    AdvPage7: TAdvPage;
    AdvPage8: TAdvPage;
    AdvPage9: TAdvPage;
    AdvToolBar2: TAdvToolBar;
    AdvGlowButton9: TAdvGlowButton;
    AdvGlowButton11: TAdvGlowButton;
    AdvGlowButton12: TAdvGlowButton;
    AdvGlowButton13: TAdvGlowButton;
    AdvGlowButton6: TAdvGlowButton;
    AdvGlowButton8: TAdvGlowButton;
    AdvToolBar4: TAdvToolBar;
    AdvToolBar15: TAdvToolBar;
    AdvGlowButton58: TAdvGlowButton;
    AdvGlowButton59: TAdvGlowButton;
    AdvGlowButton16: TAdvGlowButton;
    AdvGlowButton60: TAdvGlowButton;
    AdvToolBar3: TAdvToolBar;
    AdvGlowButton14: TAdvGlowButton;
    AdvGlowButton62: TAdvGlowButton;
    AdvGlowButton63: TAdvGlowButton;
    AdvToolBar16: TAdvToolBar;
    AdvGlowButton64: TAdvGlowButton;
    AdvGlowButton65: TAdvGlowButton;
    AdvGlowButton66: TAdvGlowButton;
    AdvGlowButton68: TAdvGlowButton;
    AdvGlowButton69: TAdvGlowButton;
    AdvToolBar17: TAdvToolBar;
    AdvGlowButton70: TAdvGlowButton;
    AdvGlowButton71: TAdvGlowButton;
    AdvToolBar18: TAdvToolBar;
    AdvGlowButton72: TAdvGlowButton;
    AdvGlowButton74: TAdvGlowButton;
    AdvToolBar13: TAdvToolBar;
    AdvGlowButton56: TAdvGlowButton;
    AdvGlowButton75: TAdvGlowButton;
    AdvGlowButton76: TAdvGlowButton;
    AdvGlowButton77: TAdvGlowButton;
    AdvToolBar20: TAdvToolBar;
    AdvToolBar19: TAdvToolBar;
    AdvGlowButton78: TAdvGlowButton;
    AdvGlowButton79: TAdvGlowButton;
    AdvGlowButton86: TAdvGlowButton;
    AdvGlowButton87: TAdvGlowButton;
    AdvToolBar21: TAdvToolBar;
    AdvGlowButton51: TAdvGlowButton;
    AdvGlowButton25: TAdvGlowButton;
    AdvPage3: TAdvPage;
    AdvToolBar7: TAdvToolBar;
    AdvGlowButton48: TAdvGlowButton;
    AdvGlowButton89: TAdvGlowButton;
    AdvPMImp: TAdvPopupMenu;
    Escolher2: TMenuItem;
    AGMBSkin: TAdvGlowMenuButton;
    AdvPMGeral: TAdvPopupMenu;
    Setores2: TMenuItem;
    Unidadesdemedida2: TMenuItem;
    Embalagens1: TMenuItem;
    CFOP2: TMenuItem;
    Perdasdecouros1: TMenuItem;
    Gruposqumicos1: TMenuItem;
    AdvPMFluxos: TAdvPopupMenu;
    Fluxos3: TMenuItem;
    Operaes1: TMenuItem;
    AdvPMDefeitos: TAdvPopupMenu;
    ipos1: TMenuItem;
    Locais2: TMenuItem;
    AdvPMVendas: TAdvPopupMenu;
    Gerencia2: TMenuItem;
    Relatrios1: TMenuItem;
    AdvPMPedidos: TAdvPopupMenu;
    Gerenciar2: TMenuItem;
    Relatrios3: TMenuItem;
    OSsAbertas1: TMenuItem;
    AdvPMMenuCor: TAdvPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvPMImagem: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    Padro1: TMenuItem;
    N3: TMenuItem;
    Estilo2: TMenuItem;
    Automtico1: TMenuItem;
    Centralizado1: TMenuItem;
    ManterProporo1: TMenuItem;
    ManterAltura1: TMenuItem;
    ManterLargura1: TMenuItem;
    Nenhum1: TMenuItem;
    Espichar1: TMenuItem;
    Repetido1: TMenuItem;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    AdvPreviewMenuOfficeStyler1: TAdvPreviewMenuOfficeStyler;
    AdvQuickAccessToolBar1: TAdvQuickAccessToolBar;
    AdvToolBarButton1: TAdvToolBarButton;
    AdvToolBarButton2: TAdvToolBarButton;
    AdvToolBarButton3: TAdvToolBarButton;
    AdvPreviewMenu1: TAdvPreviewMenu;
    ATBRapido: TAdvToolBar;
    AGBEntidades: TAdvGlowButton;
    AdvGlowButton91: TAdvGlowButton;
    AdvGlowButton93: TAdvGlowButton;
    AdvGlowButton95: TAdvGlowButton;
    AdvGlowButton100: TAdvGlowButton;
    AdvGlowButton101: TAdvGlowButton;
    ATBFormulasImpFI: TAdvGlowButton;
    AdvGlowButton108: TAdvGlowButton;
    AdvGlowButton107: TAdvGlowButton;
    ATBFormulasImpWE: TAdvGlowButton;
    AdvPMOsAbertos: TAdvPopupMenu;
    Setorial2: TMenuItem;
    Geral1: TMenuItem;
    Geral2: TMenuItem;
    Setorial1: TMenuItem;
    AdvToolBar23: TAdvToolBar;
    AdvGlowButton104: TAdvGlowButton;
    AdvGlowButton98: TAdvGlowButton;
    AdvGlowButton99: TAdvGlowButton;
    AdvGlowButton103: TAdvGlowButton;
    AdvGlowButton92: TAdvGlowButton;
    AdvGlowButton94: TAdvGlowButton;
    AdvGlowButton97: TAdvGlowButton;
    AdvToolBar24: TAdvToolBar;
    AdvGlowButton111: TAdvGlowButton;
    AdvGlowButton109: TAdvGlowButton;
    AdvGlowButton112: TAdvGlowButton;
    AdvPMCMPT: TAdvPopupMenu;
    Entradas1: TMenuItem;
    Devolues1: TMenuItem;
    MainMenu1: TMainMenu;
    Cadastros1: TMenuItem;
    Entidades1: TMenuItem;
    Funcionrios1: TMenuItem;
    Impresso1: TMenuItem;
    Formatos1: TMenuItem;
    Outros2: TMenuItem;
    Textosdeobservaes1: TMenuItem;
    MenuItem3: TMenuItem;
    Setores1: TMenuItem;
    UnidadesdeMedida1: TMenuItem;
    MenuItem4: TMenuItem;
    CFOP1: TMenuItem;
    MenuItem5: TMenuItem;
    GrUposqumicos2: TMenuItem;
    Equipes: TMenuItem;
    Fluxos2: TMenuItem;
    Fluxos1: TMenuItem;
    Operacoes1: TMenuItem;
    FornecedoresMP1: TMenuItem;
    Balanascomunicveis1: TMenuItem;
    Financeiros1: TMenuItem;
    Planodecontas1: TMenuItem;
    Todosnveis1: TMenuItem;
    Planos1: TMenuItem;
    Conjuntos1: TMenuItem;
    Grupos1: TMenuItem;
    Subgrupos1: TMenuItem;
    Contas1: TMenuItem;
    Carteiras1: TMenuItem;
    Bancos1: TMenuItem;
    AdvGlowButton121: TAdvGlowButton;
    AdvToolBar29: TAdvToolBar;
    AdvGlowButton27: TAdvGlowButton;
    AdvGlowButton23: TAdvGlowButton;
    AdvGlowButton26: TAdvGlowButton;
    AdvGlowButton50: TAdvGlowButton;
    AdvGlowButton125: TAdvGlowButton;
    AdvToolBar28: TAdvToolBar;
    AdvGlowButton119: TAdvGlowButton;
    AdvGlowButton120: TAdvGlowButton;
    QrPQ_GGX: TmySQLQuery;
    QrPQ_GGXControle: TIntegerField;
    QrPQ_GGXPQ: TIntegerField;
    QrPQ_GGXCI: TIntegerField;
    QrPQ_GGXNOMECLI: TWideStringField;
    QrPQ_GGXNOMEPQ: TWideStringField;
    AdvGlowButton110: TAdvGlowButton;
    AdvGlowButton22: TAdvGlowButton;
    AdvGlowButton128: TAdvGlowButton;
    AdvGlowButton73: TAdvGlowButton;
    AdvToolBar25: TAdvToolBar;
    AdvGlowButton10: TAdvGlowButton;
    ATBFormulasImpXX: TAdvGlowButton;
    GBGerenciaPesagem: TAdvGlowButton;
    Button2: TButton;
    BtEntiProSoft: TAdvGlowButton;
    AGBSintegra: TAdvGlowButton;
    AdvGlowButton132: TAdvGlowButton;
    AdvToolBar31: TAdvToolBar;
    AdvGlowButton133: TAdvGlowButton;
    AdvGlowButton134: TAdvGlowButton;
    N1: TMenuItem;
    ListaEstoqueHistrico1: TMenuItem;
    AdvGlowButton135: TAdvGlowButton;
    AdvGlowButton136: TAdvGlowButton;
    AdvGlowButton40: TAdvGlowButton;
    AdvGlowButton137: TAdvGlowButton;
    AdvPMCMPP: TAdvPopupMenu;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    AdvToolBar32: TAdvToolBar;
    AdvGlowMenuButton1: TAdvGlowMenuButton;
    GBMPReclas: TAdvGlowButton;
    AdvGlowButton139: TAdvGlowButton;
    AdvGlowMenuButton2: TAdvGlowMenuButton;
    AdvGlowButton140: TAdvGlowButton;
    AdvGlowButton141: TAdvGlowButton;
    AdvGlowButton143: TAdvGlowButton;
    AdvGlowButton144: TAdvGlowButton;
    AdvGlowButton146: TAdvGlowButton;
    QrBalancos: TmySQLQuery;
    QrBalancosPeriodo: TIntegerField;
    QrBalancosGrupoBal: TIntegerField;
    Panel1: TPanel;
    DBGPQ_GGX: TDBGrid;
    DsPQ_GGX: TDataSource;
    FormatoA1: TMenuItem;
    FormatoB1: TMenuItem;
    AdvGlowButton145: TAdvGlowButton;
    AdvPMCECT: TAdvPopupMenu;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    AdvGlowButton147: TAdvGlowButton;
    AdvPMResultados: TAdvPopupMenu;
    Emitidos1: TMenuItem;
    Quitados1: TMenuItem;
    AdvGlowButton149: TAdvGlowButton;
    AdvGlowButton150: TAdvGlowButton;
    AdvGlowButton151: TAdvGlowButton;
    Button3: TButton;
    Edversao: TdmkEdit;
    Label191: TLabel;
    Label1: TLabel;
    Button5: TButton;
    AdvToolBar33: TAdvToolBar;
    AdvGlowButton154: TAdvGlowButton;
    BitBtn1: TBitBtn;
    QrCabA: TmySQLQuery;
    QrCabAIDCtrl: TIntegerField;
    QrCabAinfProt_ID: TWideStringField;
    AdvToolBar34: TAdvToolBar;
    Button6: TButton;
    Edit1: TEdit;
    AdvGlowButton157: TAdvGlowButton;
    AdvGlowButton159: TAdvGlowButton;
    DataSource1: TDataSource;
    AdvToolBar35: TAdvToolBar;
    AdvGlowButton88: TAdvGlowButton;
    GBFabricas: TAdvGlowButton;
    AdvGlowButton158: TAdvGlowButton;
    AdvGlowButton156: TAdvGlowButton;
    Label2: TLabel;
    mySQLQuery1: TmySQLQuery;
    Button7: TButton;
    EdICMS_BC: TdmkEdit;
    EdICMS_Alq: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdICMS_Val: TdmkEdit;
    AdvToolBar36: TAdvToolBar;
    GBUsoConsCab: TAdvGlowButton;
    frxReport1: TfrxReport;
    GBImportaXML: TAdvGlowButton;
    GBImportaNFes: TAdvGlowButton;
    GBAbreXML: TAdvGlowButton;
    AdvGlowButton160: TAdvGlowButton;
    GBTipoMov: TAdvGlowButton;
    PMMPReclas: TPopupMenu;
    Gerencia1: TMenuItem;
    Inclui1: TMenuItem;
    GBConflitos: TAdvGlowButton;
    BitBtn2: TBitBtn;
    GBMPSubProd: TAdvGlowButton;
    AdvGlowButton33: TAdvGlowButton;
    AdvGlowButton142: TAdvGlowButton;
    AGB_SPED_EFD_Imp: TAdvGlowButton;
    AdvSPED_EFD: TAdvPopupMenu;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    AGB_SPED_EFD_Exp: TAdvGlowButton;
    PMEntraUC: TPopupMenu;
    Antigo1: TMenuItem;
    Novo1: TMenuItem;
    AGBEntraMP: TAdvGlowButton;
    AGBCorrigeFatID: TAdvGlowButton;
    QrErr103: TmySQLQuery;
    QrErr103FatID: TIntegerField;
    QrErr103FatNum: TIntegerField;
    QrErr103Empresa: TIntegerField;
    AGBConfEstq: TAdvGlowButton;
    AGBLaySPEDEFD: TAdvGlowButton;
    Button8: TButton;
    QrFlds: TmySQLQuery;
    Button9: TButton;
    QrFldsBloco: TWideStringField;
    QrFldsRegistro: TWideStringField;
    QrFldsNumero: TIntegerField;
    QrFldsVersaoIni: TIntegerField;
    QrFldsVersaoFim: TIntegerField;
    QrFldsCampo: TWideStringField;
    QrFldsTipo: TWideStringField;
    QrFldsTam: TSmallintField;
    QrFldsTObrig: TSmallintField;
    QrFldsDecimais: TSmallintField;
    QrFldsCObrig: TWideStringField;
    QrFldsEObrig: TWideStringField;
    QrFldsSObrig: TWideStringField;
    QrFldsDescrLin1: TWideStringField;
    QrFldsDescrLin2: TWideStringField;
    QrFldsDescrLin3: TWideStringField;
    QrFldsDescrLin4: TWideStringField;
    QrFldsDescrLin5: TWideStringField;
    QrFldsDescrLin6: TWideStringField;
    QrFldsDescrLin7: TWideStringField;
    QrFldsDescrLin8: TWideStringField;
    QrFldsLk: TIntegerField;
    QrFldsDataCad: TDateField;
    QrFldsDataAlt: TDateField;
    QrFldsUserCad: TIntegerField;
    QrFldsUserAlt: TIntegerField;
    QrFldsAlterWeb: TSmallintField;
    QrFldsAtivo: TSmallintField;
    AGBTabsSPEDEFD: TAdvGlowButton;
    AGB_SPED_Compara: TAdvGlowButton;
    EdGGXExluido: TdmkEdit;
    Button10: TButton;
    EdGGXSubstituto: TdmkEdit;
    EdNivelExclusao: TdmkEdit;
    PMRelatUC: TPopupMenu;
    NovoGrade1: TMenuItem;
    AntigoPQ1: TMenuItem;
    PMSintegra: TPopupMenu;
    Geral3: TMenuItem;
    SPED20101: TMenuItem;
    SkinStore1: TSkinStore;
    sd1: TSkinData;
    AGBApurICMSIPI: TAdvGlowButton;
    PMEntrada: TPopupMenu;
    Usoeconsumo1: TMenuItem;
    Energiaeltricaguags1: TMenuItem;
    ComunicaoTelecomunicao1: TMenuItem;
    UsoeconsumoSPED1: TMenuItem;
    N2: TMenuItem;
    D100Frete07088B0910112627571: TMenuItem;
    AdvToolBar37: TAdvToolBar;
    AGBCartNiv2: TAdvGlowButton;
    AGBCarteiras: TAdvGlowButton;
    AGBReceDesp: TAdvGlowButton;
    AdvPage11: TAdvPage;
    AdvToolBar39: TAdvToolBar;
    AdvToolBar38: TAdvToolBar;
    AGBFatPedNFs: TAdvGlowButton;
    AdvGlowButton85: TAdvGlowButton;
    AdvGlowButton80: TAdvGlowButton;
    AdvGlowButton81: TAdvGlowButton;
    AdvGlowButton126: TAdvGlowButton;
    AdvGlowButton124: TAdvGlowButton;
    AdvToolBar40: TAdvToolBar;
    AdvGlowButton35: TAdvGlowButton;
    AdvGlowButton36: TAdvGlowButton;
    AdvGlowButton61: TAdvGlowButton;
    AdvGlowButton131: TAdvGlowButton;
    AdvGlowButton148: TAdvGlowButton;
    AdvGlowButton83: TAdvGlowButton;
    AdvGlowButton152: TAdvGlowButton;
    GBValidaXML: TAdvGlowButton;
    GBClasFisc: TAdvGlowButton;
    AdvGlowButton84: TAdvGlowButton;
    AdvGlowButton155: TAdvGlowButton;
    GBNatOper: TAdvGlowButton;
    AdvToolBar30: TAdvToolBar;
    AdvGlowButton122: TAdvGlowButton;
    AdvGlowButton123: TAdvGlowButton;
    AGBContingencia: TAdvGlowButton;
    AGBImportaNFeWeb: TAdvGlowButton;
    Button11: TButton;
    AGBNFeEventos: TAdvGlowButton;
    AGBConsultaNFe: TAdvGlowButton;
    AGBNFeExportaXML_B: TAdvGlowButton;
    AdvPMVerificaBD: TAdvPopupMenu;
    VerificaBDServidor1: TMenuItem;
    VerificaTabelasterceiros1: TMenuItem;
    MenuItem2: TMenuItem;
    GerenciaCB41: TMenuItem;
    AdvGlowMenuButton3: TAdvGlowMenuButton;
    AdvPage10: TAdvPage;
    AdvToolBar27: TAdvToolBar;
    AdvGlowButton117: TAdvGlowButton;
    AdvGlowButton118: TAdvGlowButton;
    AdvGlowButton105: TAdvGlowButton;
    AdvGlowButton113: TAdvGlowButton;
    GBReduzido: TAdvGlowButton;
    AdvGlowButton34: TAdvGlowButton;
    AdvToolBar26: TAdvToolBar;
    AdvGlowButton114: TAdvGlowButton;
    AdvGlowButton115: TAdvGlowButton;
    AdvGlowButton116: TAdvGlowButton;
    AdvToolBar8: TAdvToolBar;
    AdvGlowButton31: TAdvGlowButton;
    AdvGlowButton41: TAdvGlowButton;
    AdvGlowButton42: TAdvGlowButton;
    AdvGlowButton43: TAdvGlowButton;
    AdvGlowButton44: TAdvGlowButton;
    AdvGlowButton49: TAdvGlowButton;
    AGBOpcoesGrad: TAdvGlowButton;
    AdvToolBar12: TAdvToolBar;
    AdvGlowButton15: TAdvGlowButton;
    AdvGlowButton53: TAdvGlowButton;
    AdvGlowButton54: TAdvGlowButton;
    AdvToolBarButton7: TAdvToolBarButton;
    TySuporte: TTrayIcon;
    TmSuporte: TTimer;
    AGBNFeConsCad: TAdvGlowButton;
    AdvGlowButton55: TAdvGlowButton;
    AGBMPRecebImp: TAdvGlowButton;
    Label6: TLabel;
    AdvGlowButton24: TAdvGlowButton;
    AGBNFeDest: TAdvGlowButton;
    AGBNFeLoad_Inn: TAdvGlowButton;
    AGBNFeDesDowC: TAdvGlowButton;
    AdvToolBar41: TAdvToolBar;
    AdvGlowButton96: TAdvGlowButton;
    ATBFormulasImpBH: TAdvGlowButton;
    APMPesagem: TAdvPopupMenu;
    Caleirocurtimento1: TMenuItem;
    Recurtimento1: TMenuItem;
    Acabamento1: TMenuItem;
    AGBPQSubstitui: TAdvGlowButton;
    AdvPage12: TAdvPage;
    AdvToolBar22: TAdvToolBar;
    AGBWBInnCab: TAdvGlowButton;
    AGBWBOutCab: TAdvGlowButton;
    AdvGlowButton20: TAdvGlowButton;
    AGBWBAjsCab: TAdvGlowButton;
    AGBWBRclCab: TAdvGlowButton;
    IdIPWatch1: TIdIPWatch;
    AdvToolBar42: TAdvToolBar;
    AGBWBPallets: TAdvGlowButton;
    AGBWBArtCab: TAdvGlowButton;
    AGBWBMPrCab: TAdvGlowButton;
    AdvToolBar43: TAdvToolBar;
    AGBWBMovImp: TAdvGlowButton;
    AGBWBIndsWB: TAdvGlowButton;
    AdvGlowButton2: TAdvGlowButton;
    AGBNewFinMigra: TAdvGlowButton;
    PageControl1: TdmkPageControl;
    TabSheet1: TTabSheet;
    ImgPrincipal: TImage;
    ATBBATBAutoExp: TAdvToolBarButton;
    BalloonHint1: TBalloonHint;
    AdvGlowButton19: TAdvGlowButton;
    APRibeira: TAdvPage;
    AdvToolBar44: TAdvToolBar;
    AGBVSNatCad: TAdvGlowButton;
    AGBVSRibCad: TAdvGlowButton;
    AGBVSRibCla: TAdvGlowButton;
    AGBVSPallet: TAdvGlowButton;
    AGBVSPalSta: TAdvGlowButton;
    APMVSClaArt: TAdvPopupMenu;
    ComearnovaOC1: TMenuItem;
    RecomearclkassificaodeOCjconfigurada1: TMenuItem;
    AGBVSSerFch: TAdvGlowButton;
    ATBBMenuGeral: TAdvToolBarButton;
    ATBBLastWork1: TAdvToolBarButton;
    ReabreTabelas1: TMenuItem;
    APMVSRclArt: TAdvPopupMenu;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    AGBNfeNewVer: TAdvGlowButton;
    AGBFatParcelaNfeCabY: TAdvGlowButton;
    AdvGlowButton52: TAdvGlowButton;
    AdvPopupMenu1: TAdvPopupMenu;
    APMMigrarVersaoNFe: TAdvPopupMenu;
    CamposdaNFe1: TMenuItem;
    ListadeWebServices1: TMenuItem;
    IdCompressorZLib1: TIdCompressorZLib;
    ZipForge1: TZipForge;
    AGBVSMrtCab: TAdvGlowButton;
    AGBVSRibOpe: TAdvGlowButton;
    RecomearclkassificaodeOCjconfigurada2: TMenuItem;
    N4: TMenuItem;
    Gerenciamento1: TMenuItem;
    N5: TMenuItem;
    Gerenciamento2: TMenuItem;
    ATBBFavoritos: TAdvToolBarButton;
    AdvGlowButton82: TAdvGlowButton;
    ATBVSGerencia: TAdvToolBar;
    AGBVSMovIts: TAdvGlowButton;
    AGBVSPallet2: TAdvGlowButton;
    AGBVSBoxes: TAdvGlowButton;
    AdvGlowButton67: TAdvGlowButton;
    AGBVSDsnCab: TAdvGlowButton;
    AGBVSFichas: TAdvGlowButton;
    N6: TMenuItem;
    N7: TMenuItem;
    ClasificaoMassiva1: TMenuItem;
    Prepara1: TMenuItem;
    Novo2: TMenuItem;
    Antigo2: TMenuItem;
    NovaConfiguraodeOC1: TMenuItem;
    AGBOCs: TAdvGlowButton;
    AGBVSEqzCab: TAdvGlowButton;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    AGBVSCfgEqz: TAdvGlowButton;
    AGBVSMotivBxa: TAdvGlowButton;
    AGBGraGruY: TAdvGlowButton;
    AGBVSEntiMP: TAdvGlowButton;
    AdvToolBar47: TAdvToolBar;
    AdvGlowButton102: TAdvGlowButton;
    AdvGlowButton106: TAdvGlowButton;
    AdvGlowButton129: TAdvGlowButton;
    AdvGlowButton130: TAdvGlowButton;
    AGBVSCGICab: TAdvGlowButton;
    AGBVSWetEnd: TAdvGlowButton;
    AGBVSFinCla: TAdvGlowButton;
    AdvToolBarButton4: TAdvToolBarButton;
    ATBBLastWork2: TAdvToolBarButton;
    APMVSDvlRtb: TAdvPopupMenu;
    DevoluoDefinitivaAoEstoque1: TMenuItem;
    RetornoParaRetrabalhoEPosteriorReenvio1: TMenuItem;
    AGBVSSubPrd: TAdvGlowButton;
    N8: TMenuItem;
    N9: TMenuItem;
    ReclassificaoMassiva1: TMenuItem;
    AGBTribDefCad: TAdvGlowButton;
    AGBTribDefCab: TAdvGlowButton;
    GerenciamentodePrReclasse1: TMenuItem;
    APMCorrigeIMEIs: TAdvPopupMenu;
    IMEIssemFornecedor1: TMenuItem;
    IMEIsGmeosorfos1: TMenuItem;
    APMVSCadDiversos: TAdvPopupMenu;
    iposdeArtigo1: TMenuItem;
    APMResultadoFichas: TAdvPopupMenu;
    nica1: TMenuItem;
    Vrias1: TMenuItem;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    AdvToolBar49: TAdvToolBar;
    AGBVSCalCab: TAdvGlowButton;
    AGBVSCurCab: TAdvGlowButton;
    TmVersao: TTimer;
    BtAjustePQw: TAdvGlowButton;
    SG1: TStringGrid;
    AGBParar: TAdvGlowButton;
    AdvToolBar50: TAdvToolBar;
    AGBPQRAjuInn: TAdvGlowButton;
    AGBPQRAjuOut: TAdvGlowButton;
    AdvGlowButton127: TAdvGlowButton;
    AdvToolBar51: TAdvToolBar;
    AGBConsLeit: TAdvGlowButton;
    AdvGlowButton161: TAdvGlowButton;
    AGBImprimeVSEmProcBH: TAdvGlowButton;
    AdvGlowButton162: TAdvGlowButton;
    AdvToolBar52: TAdvToolBar;
    AGBCorrigeNFesIMIs: TAdvGlowButton;
    AdvGlowButton163: TAdvGlowButton;
    AGBVSCOPCab: TAdvGlowButton;
    AdvToolBar53: TAdvToolBar;
    AdvGlowMenuButton5: TAdvGlowMenuButton;
    AdvPage13: TAdvPage;
    AdvToolBar45: TAdvToolBar;
    AGBVSInnCab: TAdvGlowButton;
    AGBVSOutCab: TAdvGlowButton;
    AGBVSOutIts: TAdvGlowButton;
    AGBVSAjsCab: TAdvGlowButton;
    AGBVSGeraArtCab: TAdvGlowButton;
    AGBVSClaArt: TAdvGlowMenuButton;
    AGBVSRclArt: TAdvGlowMenuButton;
    AGBVSOpeCab: TAdvGlowButton;
    AGBVSPlCCab: TAdvGlowButton;
    AGBVSExBCab: TAdvGlowButton;
    AGBVSPWECab: TAdvGlowButton;
    AGBVSDvlRtb: TAdvGlowMenuButton;
    AGBVSPedCab: TAdvGlowButton;
    AGBVSMovItb: TAdvGlowButton;
    AGBVSTrfLocCab: TAdvGlowButton;
    AdvToolBar46: TAdvToolBar;
    AGBVSMovImp: TAdvGlowMenuButton;
    AGBVSFchRslCab: TAdvGlowMenuButton;
    AGBSPEDEFDEnce: TAdvGlowButton;
    AGBVSCfgEFD: TAdvGlowMenuButton;
    Localizarcadastrode1: TMenuItem;
    AGBCFOPCFOP: TAdvGlowButton;
    AGBGraGruEGer: TAdvGlowButton;
    AGBCadDiversos: TAdvGlowMenuButton;
    AGBVSPSPCab: TAdvGlowButton;
    AdvGlowMenuButton4: TAdvGlowMenuButton;
    AGBVSPwdDd: TAdvGlowButton;
    AGBVSRRMCab: TAdvGlowButton;
    AGBAtzArtigosPallets: TAdvGlowButton;
    AdvPage14: TAdvPage;
    AdvToolBar54: TAdvToolBar;
    AdvGlowButton164: TAdvGlowButton;
    AdvGlowButton165: TAdvGlowButton;
    AdvToolBar55: TAdvToolBar;
    AdvGlowButton166: TAdvGlowButton;
    AdvGlowButton167: TAdvGlowButton;
    AGBVSArtCab: TAdvGlowButton;
    QrPQE: TmySQLQuery;
    LaTopWarn2: TLabel;
    LaTopWarn1: TLabel;
    AGBVSCPMRSBCb: TAdvGlowButton;
    AGBVSReqMov: TAdvGlowButton;
    AGBVSMovCab: TAdvGlowButton;
    AdvGlowButton45: TAdvGlowButton;
    AdvGlowMenuButton6: TAdvGlowMenuButton;
    APMVSInfXxx: TAdvPopupMenu;
    IECInformaodeEntradadeCouros1: TMenuItem;
    Inconsistnciadeinformao1: TMenuItem;
    AGBInfoIncCab: TAdvGlowButton;
    EdSelCod: TdmkEdit;
    PMLocaliza: TPopupMenu;
    IMEI1: TMenuItem;
    IMEC1: TMenuItem;
    Reduzido1: TMenuItem;
    Entidade1: TMenuItem;
    reaVSm1: TMenuItem;
    PesoVSkg1: TMenuItem;
    NFeemitida1: TMenuItem;
    Nmeroainformar1: TMenuItem;
    Componenteativo1: TMenuItem;
    NFeRecebida1: TMenuItem;
    APMVSImprime: TAdvPopupMenu;
    N26EstoqueCustoIntegrado1: TMenuItem;
    AGBVSMovimID: TAdvGlowButton;
    AGBClaReCo: TAdvGlowButton;
    AdvGlowButton57: TAdvGlowButton;
    AdvPMBaixaPQ: TAdvPopupMenu;
    Consumo1: TMenuItem;
    Devoluonovo1: TMenuItem;
    AGBGraGruGruXCou: TAdvGlowButton;
    AdvToolBar14: TAdvToolBar;
    AdvGlowButton90: TAdvGlowButton;
    AdvGlowButton138: TAdvGlowButton;
    AdvGlowButton168: TAdvGlowButton;
    AdvGlowButton169: TAdvGlowButton;
    QrTeste: TmySQLQuery;
    APMVSCfgEFD: TAdvPopupMenu;
    Movimentaes1: TMenuItem;
    BaixaxestoquedeInNatura1: TMenuItem;
    Outrasbaixasxestoque1: TMenuItem;
    Geraoxestoque1: TMenuItem;
    VSxPesagemPQ1: TMenuItem;
    AGBVSMOEnvXXX: TAdvGlowMenuButton;
    APMVSMOEnvXXX: TAdvPopupMenu;
    Fretesemretorno1: TMenuItem;
    Envioparaposteriorretorno1: TMenuItem;
    Retornodeenvio1: TMenuItem;
    N10: TMenuItem;
    CTes1: TMenuItem;
    N21RendimentoSemi1: TMenuItem;
    N24RendimentoeMO1: TMenuItem;
    AdvGlowButton39: TAdvGlowButton;
    AdvGlowButton170: TAdvGlowButton;
    AdvGlowButton171: TAdvGlowButton;
    AdvToolBar56: TAdvToolBar;
    MainMenu2: TMainMenu;
    sPanel5: TsPanel;
    SpeedButton6: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton1: TSpeedButton;
    dmkImage1: TdmkImage;
    sButton1: TsButton;
    sBitBtn1: TsBitBtn;
    ImageList1: TImageList;
    sBitBtn2: TsBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SBCadEntidadesClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BtImagemClick(Sender: TObject);
    procedure Entidades2Click(Sender: TObject);
    procedure PQCadastro1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AdvGlowButton5Click(Sender: TObject);
    procedure AdvGlowButton7Click(Sender: TObject);
    procedure AdvGlowButton3Click(Sender: TObject);
    procedure AdvGlowButton17Click(Sender: TObject);
    procedure AGBEntraUCClick(Sender: TObject);
    procedure AdvGlowButton18Click(Sender: TObject);
    procedure AdvGlowButton21Click(Sender: TObject);
    procedure AdvGlowButton10Click(Sender: TObject);
    procedure GBGerenciaPesagemClick(Sender: TObject);
    procedure GBBaixaETEClick(Sender: TObject);
    procedure AGBRelatUCClick(Sender: TObject);
    procedure AdvGlowButton37Click(Sender: TObject);
    procedure AdvGlowButton38Click(Sender: TObject);
    procedure AdvGlowButton24Click(Sender: TObject);
    procedure AdvGlowButton23Click(Sender: TObject);
    procedure AdvGlowButton26Click(Sender: TObject);
    procedure AdvGlowButton22Click(Sender: TObject);
    procedure AdvGlowButton27Click(Sender: TObject);
    procedure AdvGlowButton40Click(Sender: TObject);
    procedure AdvGlowButton43Click(Sender: TObject);
    procedure AdvGlowButton44Click(Sender: TObject);
    procedure AdvGlowButton46Click(Sender: TObject);
    procedure AdvGlowButton49Click(Sender: TObject);
    procedure AdvGlowButton31Click(Sender: TObject);
    procedure AdvGlowButton41Click(Sender: TObject);
    procedure AdvGlowButton8Click(Sender: TObject);
    procedure AdvGlowButton6Click(Sender: TObject);
    procedure AdvGlowButton9Click(Sender: TObject);
    procedure AdvGlowButton11Click(Sender: TObject);
    procedure AdvGlowButton12Click(Sender: TObject);
    procedure AdvGlowButton13Click(Sender: TObject);
    procedure AdvGlowButton25Click(Sender: TObject);
    procedure AdvGlowButton16Click(Sender: TObject);
    procedure AdvGlowButton78Click(Sender: TObject);
    procedure AdvGlowButton79Click(Sender: TObject);
    procedure AdvGlowButton86Click(Sender: TObject);
    procedure AdvGlowButton87Click(Sender: TObject);
    procedure AdvGlowButton85Click(Sender: TObject);
    procedure AdvGlowButton80Click(Sender: TObject);
    procedure AdvGlowButton81Click(Sender: TObject);
    procedure AdvGlowButton83Click(Sender: TObject);
    procedure AdvGlowButton74Click(Sender: TObject);
    procedure AdvGlowButton73Click(Sender: TObject);
    procedure AdvGlowButton72Click(Sender: TObject);
    procedure AdvGlowButton56Click(Sender: TObject);
    procedure AdvGlowButton75Click(Sender: TObject);
    procedure AdvGlowButton76Click(Sender: TObject);
    procedure AdvGlowButton77Click(Sender: TObject);
    procedure AdvGlowButton69Click(Sender: TObject);
    procedure AdvGlowButton66Click(Sender: TObject);
    procedure Gfg(Sender: TObject);
    procedure AdvGlowButton70Click(Sender: TObject);
    procedure AdvGlowButton63Click(Sender: TObject);
    procedure AdvGlowButton62Click(Sender: TObject);
    procedure AdvGlowButton14Click(Sender: TObject);
    procedure AdvGlowButton52Click(Sender: TObject);
    procedure AdvGlowButton51Click(Sender: TObject);
    procedure AdvGlowButton58Click(Sender: TObject);
    procedure AdvGlowButton59Click(Sender: TObject);
    procedure AdvGlowButton60Click(Sender: TObject);
    procedure AdvGlowButton89Click(Sender: TObject);
    procedure GBValidaXMLClick(Sender: TObject);
    procedure Escolher2Click(Sender: TObject);
    procedure Setores2Click(Sender: TObject);
    procedure Unidadesdemedida2Click(Sender: TObject);
    procedure Embalagens1Click(Sender: TObject);
    procedure CFOP2Click(Sender: TObject);
    procedure Perdasdecouros1Click(Sender: TObject);
    procedure Gruposqumicos1Click(Sender: TObject);
    procedure Fluxos3Click(Sender: TObject);
    procedure Operaes1Click(Sender: TObject);
    procedure ipos1Click(Sender: TObject);
    procedure Locais2Click(Sender: TObject);
    procedure Gerencia2Click(Sender: TObject);
    procedure Relatrios1Click(Sender: TObject);
    procedure Gerenciar2Click(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Padro1Click(Sender: TObject);
    procedure Automtico1Click(Sender: TObject);
    procedure AdvToolBarButton1Click(Sender: TObject);
    procedure AdvToolBarButton2Click(Sender: TObject);
    procedure AdvToolBarButton3Click(Sender: TObject);
    procedure AdvPreviewMenu1Buttons0Click(Sender: TObject;
      Button: TButtonCollectionItem);
    procedure AdvPreviewMenu1Buttons1Click(Sender: TObject;
      Button: TButtonCollectionItem);
    procedure AdvPreviewMenu1Buttons2Click(Sender: TObject;
      Button: TButtonCollectionItem);
    procedure AdvPreviewMenu1Buttons3Click(Sender: TObject;
      Button: TButtonCollectionItem);
    procedure AdvPreviewMenu1MenuItemClick(Sender: TObject; ItemIndex: Integer);
    procedure AGBEntidadesClick(Sender: TObject);
    procedure AdvGlowButton91Click(Sender: TObject);
    procedure AdvGlowButton110Click(Sender: TObject);
    procedure AdvGlowButton95Click(Sender: TObject);
    procedure AdvGlowButton93Click(Sender: TObject);
    procedure AdvGlowButton96Click(Sender: TObject);
    procedure AdvGlowButton109Click(Sender: TObject);
    procedure AdvGlowButton111Click(Sender: TObject);
    procedure AdvGlowButton100Click(Sender: TObject);
    procedure AdvGlowButton101Click(Sender: TObject);
    procedure ATBFormulasImpFIClick(Sender: TObject);
    procedure AdvGlowButton107Click(Sender: TObject);
    procedure AdvGlowButton92Click(Sender: TObject);
    procedure AdvGlowButton94Click(Sender: TObject);
    procedure AdvGlowButton97Click(Sender: TObject);
    procedure AdvGlowButton99Click(Sender: TObject);
    procedure AdvGlowButton98Click(Sender: TObject);
    procedure AdvGlowButton104Click(Sender: TObject);
    procedure Geral1Click(Sender: TObject);
    procedure Geral2Click(Sender: TObject);
    procedure Entradas1Click(Sender: TObject);
    procedure Devolues1Click(Sender: TObject);
    procedure Outros2Click(Sender: TObject);
    procedure Textosdeobservaes1Click(Sender: TObject);
    procedure Setores1Click(Sender: TObject);
    procedure UnidadesdeMedida1Click(Sender: TObject);
    procedure CFOP1Click(Sender: TObject);
    procedure EquipesClick(Sender: TObject);
    procedure Fluxos1Click(Sender: TObject);
    procedure Operacoes1Click(Sender: TObject);
    procedure FornecedoresMP1Click(Sender: TObject);
    procedure Balanascomunicveis1Click(Sender: TObject);
    procedure Todosnveis1Click(Sender: TObject);
    procedure Planos1Click(Sender: TObject);
    procedure Conjuntos1Click(Sender: TObject);
    procedure Grupos1Click(Sender: TObject);
    procedure Subgrupos1Click(Sender: TObject);
    procedure Contas1Click(Sender: TObject);
    procedure Carteiras1Click(Sender: TObject);
    procedure Bancos1Click(Sender: TObject);
    procedure AdvGlowButton105Click(Sender: TObject);
    procedure AdvGlowButton113Click(Sender: TObject);
    procedure AdvGlowButton115Click(Sender: TObject);
    procedure AdvGlowButton116Click(Sender: TObject);
    procedure AdvGlowButton114Click(Sender: TObject);
    procedure AdvGlowButton117Click(Sender: TObject);
    procedure AdvGlowButton118Click(Sender: TObject);
    procedure AdvGlowButton119Click(Sender: TObject);
    procedure AdvGlowButton120Click(Sender: TObject);
    procedure AdvGlowButton121Click(Sender: TObject);
    procedure AdvGlowButton123Click(Sender: TObject);
    procedure AdvGlowButton122Click(Sender: TObject);
    procedure AdvGlowButton124Click(Sender: TObject);
    procedure AdvGlowButton125Click(Sender: TObject);
    procedure AdvGlowButton126Click(Sender: TObject);
    procedure AdvGlowButton127Click(Sender: TObject);
    procedure AdvGlowButton128Click(Sender: TObject);
    procedure AdvGlowButton129Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure BtEntiProSoftClick(Sender: TObject);
    procedure AGBSintegraClick(Sender: TObject);
    procedure AdvGlowButton131Click(Sender: TObject);
    procedure AdvGlowButton132Click(Sender: TObject);
    procedure AdvGlowButton133Click(Sender: TObject);
    procedure AdvGlowButton134Click(Sender: TObject);
    procedure ListaEstoqueHistrico1Click(Sender: TObject);
    procedure AdvGlowButton135Click(Sender: TObject);
    procedure AdvGlowButton136Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure MenuItem7Click(Sender: TObject);
    procedure GBMPReclasClick(Sender: TObject);
    procedure AdvGlowButton139Click(Sender: TObject);
    procedure AdvGlowButton140Click(Sender: TObject);
    procedure AdvGlowButton141Click(Sender: TObject);
    procedure AdvGlowButton142Click(Sender: TObject);
    procedure AdvGlowButton143Click(Sender: TObject);
    procedure AdvGlowButton144Click(Sender: TObject);
    procedure AdvGlowButton146Click(Sender: TObject);
    procedure FormatoA1Click(Sender: TObject);
    procedure FormatoB1Click(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
    procedure AdvGlowButton147Click(Sender: TObject);
    procedure AdvGlowButton68Click(Sender: TObject);
    procedure Emitidos1Click(Sender: TObject);
    procedure Quitados1Click(Sender: TObject);
    procedure AdvGlowButton30Click(Sender: TObject);
    procedure AdvGlowButton149Click(Sender: TObject);
    procedure AdvGlowButton148Click(Sender: TObject);
    procedure AdvGlowButton150Click(Sender: TObject);
    procedure AdvGlowButton151Click(Sender: TObject);
    procedure AdvGlowButton152Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure AdvGlowButton154Click(Sender: TObject);
    procedure AdvGlowButton155Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure AdvGlowButton156Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure AdvGlowButton157Click(Sender: TObject);
    procedure AdvGlowButton158Click(Sender: TObject);
    procedure AdvGlowButton159Click(Sender: TObject);
    procedure GBInfCplClick(Sender: TObject);
    procedure AdvGlowButton88Click(Sender: TObject);
    procedure GBFabricasClick(Sender: TObject);
    procedure GBClasFiscClick(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure AdvGlowButton84Click(Sender: TObject);
    procedure GBUsoConsCabClick(Sender: TObject);
    procedure GBNatOperClick(Sender: TObject);
    procedure GBImportaXMLClick(Sender: TObject);
    procedure GBImportaNFesClick(Sender: TObject);
    procedure GBAbreXMLClick(Sender: TObject);
    procedure AdvGlowButton160Click(Sender: TObject);
    procedure GBTipoMovClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Gerencia1Click(Sender: TObject);
    procedure GBConflitosClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure GBMPSubProdClick(Sender: TObject);
    procedure AdvGlowButton42Click(Sender: TObject);
    procedure AdvGlowButton33Click(Sender: TObject);
    procedure GBReduzidoClick(Sender: TObject);
    procedure AGB_SPED_EFD_ImpClick(Sender: TObject);
    procedure AGB_SPED_EFD_ExpClick(Sender: TObject);
    procedure Novo1Click(Sender: TObject);
    procedure Antigo1Click(Sender: TObject);
    procedure AGBEntraMPClick(Sender: TObject);
    procedure AGBCorrigeFatIDClick(Sender: TObject);
    procedure AGBConfEstqClick(Sender: TObject);
    procedure AGBLaySPEDEFDClick(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure AGBTabsSPEDEFDClick(Sender: TObject);
    procedure AGB_SPED_ComparaClick(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure NovoGrade1Click(Sender: TObject);
    procedure AntigoPQ1Click(Sender: TObject);
    procedure SPED20101Click(Sender: TObject);
    procedure Geral3Click(Sender: TObject);
    procedure AdvGlowButton15Click(Sender: TObject);
    procedure AGBApurICMSIPIClick(Sender: TObject);
    procedure Usoeconsumo1Click(Sender: TObject);
    procedure Energiaeltricaguags1Click(Sender: TObject);
    procedure ComunicaoTelecomunicao1Click(Sender: TObject);
    procedure UsoeconsumoSPED1Click(Sender: TObject);
    procedure D100Frete07088B0910112627571Click(Sender: TObject);
    procedure AGBCarteirasClick(Sender: TObject);
    procedure AGBCartNiv2Click(Sender: TObject);
    procedure AGBReceDespClick(Sender: TObject);
    procedure AGBContingenciaClick(Sender: TObject);
    procedure AGBImportaNFeWebClick(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure AGBNFeEventosClick(Sender: TObject);
    procedure AGBConsultaNFeClick(Sender: TObject);
    procedure AGBNFeExportaXML_BClick(Sender: TObject);
    procedure AGBOpcoesGradClick(Sender: TObject);
    procedure VerificaTabelasterceiros1Click(Sender: TObject);
    procedure VerificaBDServidor1Click(Sender: TObject);
    procedure AdvToolBarButton7Click(Sender: TObject);
    procedure TmSuporteTimer(Sender: TObject);
    procedure AGBNFeConsCadClick(Sender: TObject);
    procedure AdvGlowButton55Click(Sender: TObject);
    procedure AGBMPRecebImpClick(Sender: TObject);
    procedure AGBNFeDestClick(Sender: TObject);
    procedure AGBNFeDesDowCClick(Sender: TObject);
    procedure AdvGlowButton108Click(Sender: TObject);
    procedure Caleirocurtimento1Click(Sender: TObject);
    procedure Recurtimento1Click(Sender: TObject);
    procedure Acabamento1Click(Sender: TObject);
    procedure ATBFormulasImpWEClick(Sender: TObject);
    procedure AGBPQSubstituiClick(Sender: TObject);
    procedure AGBWBInnCabClick(Sender: TObject);
    procedure AGBWBMovImpClick(Sender: TObject);
    procedure AGBWBOutCabClick(Sender: TObject);
    procedure AGBWBArtCabClick(Sender: TObject);
    procedure AdvGlowButton20Click(Sender: TObject);
    procedure AdvGlowButton35Click(Sender: TObject);
    procedure AGBWBAjsCabClick(Sender: TObject);
    procedure AGBWBPalletsClick(Sender: TObject);
    procedure AGBWBMPrCabClick(Sender: TObject);
    procedure AGBWBRclCabClick(Sender: TObject);
    procedure IdIPWatch1Status(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure AGBPQRAjuInnClick(Sender: TObject);
    procedure AGBPQRAjuOutClick(Sender: TObject);
    procedure AdvGlowButton2Click(Sender: TObject);
    procedure AGBNewFinMigraClick(Sender: TObject);
    procedure ATBBATBAutoExpClick(Sender: TObject);
    procedure PageControl1Enter(Sender: TObject);
    procedure PageControl1MouseEnter(Sender: TObject);
    procedure PageControl1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AGBVSNatCadClick(Sender: TObject);
    procedure AGBVSRibCadClick(Sender: TObject);
    procedure AGBVSInnCabClick(Sender: TObject);
    procedure AGBVSGeraArtCabClick(Sender: TObject);
    procedure AGBVSPalletClick(Sender: TObject);
    procedure AGBVSPalStaClick(Sender: TObject);
    procedure ComearnovaOC1Click(Sender: TObject);
    procedure AGBVSMovImpClick(Sender: TObject);
    procedure AGBVSOutCabClick(Sender: TObject);
    procedure AGBVSOutItsClick(Sender: TObject);
    procedure AGBVSRibClaClick(Sender: TObject);
    procedure AGBVSSerFchClick(Sender: TObject);
    procedure ATBBLastWork1Click(Sender: TObject);
    procedure ATBBMenuGeralClick(Sender: TObject);
    procedure ReabreTabelas1Click(Sender: TObject);
    procedure MenuItem13Click(Sender: TObject);
    procedure AGBFatParcelaNfeCabYClick(Sender: TObject);
    procedure CamposdaNFe1Click(Sender: TObject);
    procedure ListadeWebServices1Click(Sender: TObject);
    procedure AGBVSMrtCabClick(Sender: TObject);
    procedure AGBVSOpeCabClick(Sender: TObject);
    procedure AGBVSRibOpeClick(Sender: TObject);
    procedure RecomearclkassificaodeOCjconfigurada2Click(Sender: TObject);
    procedure RecomearclkassificaodeOCjconfigurada1Click(Sender: TObject);
    procedure Gerenciamento1Click(Sender: TObject);
    procedure AGBVSMovItsClick(Sender: TObject);
    procedure Gerenciamento2Click(Sender: TObject);
    procedure ATBBFavoritosClick(Sender: TObject);
    procedure AGBVSDsnCabClick(Sender: TObject);
    procedure AdvGlowButton67Click(Sender: TObject);
    procedure AdvGlowButton82Click(Sender: TObject);
    procedure AGBVSBoxesClick(Sender: TObject);
    procedure AGBVSFichasClick(Sender: TObject);
    procedure ClasificaoMassiva1Click(Sender: TObject);
    procedure Prepara1Click(Sender: TObject);
    procedure Antigo2Click(Sender: TObject);
    procedure Novo2Click(Sender: TObject);
    procedure NovaConfiguraodeOC1Click(Sender: TObject);
    procedure AGBOCsClick(Sender: TObject);
    procedure AGBVSEqzCabClick(Sender: TObject);
    procedure AdvGlowButton102Click(Sender: TObject);
    procedure AdvGlowButton106Click(Sender: TObject);
    procedure AGBVSCfgEqzClick(Sender: TObject);
    procedure AGBVSPlCCabClick(Sender: TObject);
    procedure AGBVSExBCabClick(Sender: TObject);
    procedure AGBVSMotivBxaClick(Sender: TObject);
    procedure AGBGraGruYClick(Sender: TObject);
    procedure AGBVSEntiMPClick(Sender: TObject);
    procedure AdvGlowButton130Click(Sender: TObject);
    procedure AGBVSCGICabClick(Sender: TObject);
    procedure AGBVSWetEndClick(Sender: TObject);
    procedure AGBVSPedCabClick(Sender: TObject);
    procedure AGBVSPWECabClick(Sender: TObject);
    procedure AGBVSFinClaClick(Sender: TObject);
    procedure AdvToolBarButton4Click(Sender: TObject);
    procedure ATBBLastWork2Click(Sender: TObject);
    procedure DevoluoDefinitivaAoEstoque1Click(Sender: TObject);
    procedure RetornoParaRetrabalhoEPosteriorReenvio1Click(Sender: TObject);
    procedure AGBVSSubPrdClick(Sender: TObject);
    procedure ReclassificaoMassiva1Click(Sender: TObject);
    procedure AGBVSReqMovClick(Sender: TObject);
    procedure AGBTribDefCadClick(Sender: TObject);
    procedure AGBTribDefCabClick(Sender: TObject);
    procedure AGBVSMovItbClick(Sender: TObject);
    procedure AdvToolBarPager1Change(Sender: TObject);
    procedure GerenciamentodePrReclasse1Click(Sender: TObject);
    procedure IMEIssemFornecedor1Click(Sender: TObject);
    procedure IMEIsGmeosorfos1Click(Sender: TObject);
    procedure AGBVSTrfLocCabClick(Sender: TObject);
    procedure iposdeArtigo1Click(Sender: TObject);
    procedure nica1Click(Sender: TObject);
    procedure Vrias1Click(Sender: TObject);
    procedure AGBVSFchRslCabClick(Sender: TObject);
    procedure AGBVSPwdDdClick(Sender: TObject);
    procedure AdvGlowButton153Click(Sender: TObject);
    procedure AGBVSCalCabClick(Sender: TObject);
    procedure AGBVSCurCabClick(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure BtAjustePQwClick(Sender: TObject);
    procedure AGBPararClick(Sender: TObject);
    procedure AGBConsLeitClick(Sender: TObject);
    procedure AdvGlowButton161Click(Sender: TObject);
    procedure AGBImprimeVSEmProcBHClick(Sender: TObject);
    procedure AdvGlowButton162Click(Sender: TObject);
    procedure AGBCorrigeNFesIMIsClick(Sender: TObject);
    procedure AdvGlowButton163Click(Sender: TObject);
    procedure AGBVSCOPCabClick(Sender: TObject);
    procedure AdvGlowMenuButton5Click(Sender: TObject);
    procedure sd1FormSkin(Sender: TObject; aName: string; var DoSkin: Boolean);
    procedure IMEI1Click(Sender: TObject);
    procedure IMEC1Click(Sender: TObject);
    procedure Reduzido1Click(Sender: TObject);
    procedure Entidade1Click(Sender: TObject);
    procedure AGBGraGruEGerClick(Sender: TObject);
    procedure reaVSm21Click(Sender: TObject);
    procedure AGBVSPSPCabClick(Sender: TObject);
    procedure AGBVSRRMCabClick(Sender: TObject);
    procedure AGBAtzArtigosPalletsClick(Sender: TObject);
    procedure AGBSPEDEFDEnceClick(Sender: TObject);
    procedure AdvGlowButton164Click(Sender: TObject);
    procedure AdvGlowButton166Click(Sender: TObject);
    procedure AdvGlowButton165Click(Sender: TObject);
    procedure AdvGlowButton167Click(Sender: TObject);
    procedure AGBVSArtCabClick(Sender: TObject);
    procedure PesoVSkg1Click(Sender: TObject);
    procedure AGBVSCPMRSBCbClick(Sender: TObject);
    procedure AGBVSMovCabClick(Sender: TObject);
    procedure AdvGlowButton45Click(Sender: TObject);
    procedure IECInformaodeEntradadeCouros1Click(Sender: TObject);
    procedure Inconsistnciadeinformao1Click(Sender: TObject);
    procedure AGBInfoIncCabClick(Sender: TObject);
    procedure NFe1Click(Sender: TObject);
    procedure IDAntes1Click(Sender: TObject);
    procedure reaVSm1Click(Sender: TObject);
    procedure Nmeroainformar1Click(Sender: TObject);
    procedure Componenteativo1Click(Sender: TObject);
    procedure NFeRecebida1Click(Sender: TObject);
    procedure N26EstoqueCustoIntegrado1Click(Sender: TObject);
    procedure AGBVSMovimIDClick(Sender: TObject);
    procedure AGBClaReCoClick(Sender: TObject);
    procedure AdvGlowButton57Click(Sender: TObject);
    procedure Consumo1Click(Sender: TObject);
    procedure Devoluonovo1Click(Sender: TObject);
    procedure AGBGraGruGruXCouClick(Sender: TObject);
    procedure AdvGlowButton90Click(Sender: TObject);
    procedure AdvGlowButton138Click(Sender: TObject);
    procedure AdvGlowButton168Click(Sender: TObject);
    procedure AdvGlowButton169Click(Sender: TObject);
    procedure Movimentaes1Click(Sender: TObject);
    procedure BaixaxestoquedeInNatura1Click(Sender: TObject);
    procedure Outrasbaixasxestoque1Click(Sender: TObject);
    procedure Geraoxestoque1Click(Sender: TObject);
    procedure VSxPesagemPQ1Click(Sender: TObject);
    procedure Fretesemretorno1Click(Sender: TObject);
    procedure Envioparaposteriorretorno1Click(Sender: TObject);
    procedure CTes1Click(Sender: TObject);
    procedure N21RendimentoSemi1Click(Sender: TObject);
    procedure N24RendimentoeMO1Click(Sender: TObject);
    procedure AdvGlowButton170Click(Sender: TObject);
    procedure AdvGlowButton171Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
    FALiberar: Boolean;
    FTamLinha, FFoiExpand: Integer;
    FArqPrn: TextFile;
    FPrintedLine: Boolean;
    procedure VerificaPQ_GGX();
    procedure VerificaMP_GGX();
    procedure SkinMenu(Index: integer);
    function  AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
    function  EspacoEntreTextos(QrView: TmySQLQuery; CPI: Integer): String;
    function  Formata(Texto, Prefixo, Sufixo: String; EspacosPrefixo, Formato:
              Integer; Nulo: Boolean): String;
    function  FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
              EspacosPrefixo, Substitui, Alinhamento, Comprimento, Fonte,
              Formatacao, CPI: Integer; Nulo: Boolean): String;
    procedure MostraFormVSIndsVS(SQLType: TSQLType; Controle, MPVIts, Emit,
              EmitCus, InsEmpr: Integer; QrWBMovIts: TmySQLQuery;
              FormaWBIndsWE: TFormaWBIndsWE);
    procedure AppIdle(Sender: TObject; var Done: Boolean);
  public
    { Public declarations }
    //FImportPath: String;
    FTipoEntradTitu: String;
    FTipoEntradaDig, FTipoEntradaNFe, FTipoEntradaEFD,
    FCliIntUnico, FEntInt, FTipoNovoEnti: Integer;
    FParar: Boolean;
    //
    procedure DefineVarsCliInt(CliInt: Integer);
    procedure ShowHint(Sender: TObject);
    procedure RetornoCNAB;
    function PreparaMinutosSQL: Boolean;
    function AdicionaMinutosSQL(HI, HF: TTime): Boolean;
    procedure AcoesIniciaisDoAplicativo();
    procedure MostraParamsEmp();
    procedure CadastroMPIn();
    procedure CadastroFluxos();
    procedure CadastroFornecedorMP();
    procedure CadastroEquipes();
    procedure CadastroDeEspessuras();
    procedure CadastroDeSetores(Codigo: Integer);
    procedure CadastroDeDefPecas();
    procedure CadastroDeCambio();
    procedure CadastroDeArtigosGrupos();
    procedure CadastroDeMPEstagios();
    procedure CadastroDeImpObs();
    procedure CadastroImp();
    procedure CadastroEntiMP();
    procedure CadastroPortSerBal();
    procedure CadastroBancos();
    // Financeiros
    function CadastroDeContasSdoSimples(Entidade, Conta: Integer): Boolean;
    procedure CadastroDeContasSdoLista();
    //procedure CadastroDeCarteiras(TipoCarteira, ItemCarteira: Integer);
    procedure CadastroDeCarteiras(TipoCarteira, ItemCarteira, x: Integer);
    procedure CadastroDeCarteira;
    procedure CadastroDeContas;
    procedure CadastroDeSubGrupos;
    procedure CadastroDeGrupos;
    procedure CadastroDeConjuntos();
    procedure CadastroDePlano();
    procedure CadastroDeNiveisPlano();
    procedure CadastroDeContasNiv();
    procedure CriaImpressaoDiversos(Indice: Integer);
    procedure SaldoDeContas;
    procedure AtzSdoContas;
    procedure ImpressaoDoPlanoDeContas;
    procedure CadastroCFOP2003();

    procedure ImprimeChequeAtual(ChAtu, ChMax, ChConfCab, CPI, TopoIni, Altura,
              MEsq: Integer; Dia, Mes, Ano, Vcto, Valor, Favorecido, Extenso1,
              Extenso2, Obs, Cidade: String);

    procedure SelecionaImagemdefundo;
    procedure MostraBackup;
    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid; Entidade: Integer; Aba: Boolean);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade1: TStringGrid);
    function RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    procedure AjustaEstiloImagem(Index: integer);
    function CartaoDeFatura: Integer;
    function CompensacaoDeFatura: String;
    procedure MostraCarteiras(Carteira: Integer);
    procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );

    procedure MostraFisRegCad;
    procedure MostraCFOP2003();
    procedure MostraModelosImp;
    procedure MostraPediPrzCab(Codigo: Integer);
    procedure MostraPediAcc;
    procedure MostraGeosite;
    procedure MostraRegioes();
    procedure MostraTabePrcCab;
    procedure MostraCambioMda;
    procedure CadastroFormulas(Numero, Controle: Integer);
    procedure CadastroTintas(Numero, Codigo, Controle: Integer);
    procedure CadastroOpcoes();
    procedure CadastroGruposQuimicos();
    procedure CadastroMatriz();
    procedure CadastroPallets();

    procedure VerificaBD();
    function VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;

    //procedure RefreshInsumos;
    procedure VendasGerencia(Direto: Boolean);
    procedure VendasRelatorios;
    procedure PedidosGerencia(Direto: Boolean);
    procedure PedidosRelatorios;
    procedure CadastroPQ(Codigo: Integer);
    procedure CadastroPQImp(FormOrig: String);
    procedure StatusPedido(Controle: Integer; DataAtual: TDateTime);
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
             Data: TDateTime; Arquivo: String): Boolean;
    function ObtemUnidadeTxt(Unidade: Integer): String;
    function ObtemMoedaTxt(Moeda: Integer): String;
    function ObtemPeriodoTxt(Periodo: Integer): String;
    function AvisaFaltaDeContaDoPlano(Item: Integer): Boolean;
    //procedure MostraCalculadoraDeramtek(Sender: TObject);
    procedure AtualizaBalancoDePQNoSMI(PB: TProgressBar);
(*
    function  CriaFormEntradaCab(MaterialNF: TTipoMaterialNF;
              ShowForm: Boolean; IDCtrl: Integer): Boolean;
*)
    procedure ReCaptionComponentesDeForm(Form: TForm);
    //
    procedure MostraFormOSsAberta();
    procedure MostraDefeitostip(Codigo: Integer);
    procedure MostraDefeitosloc(Codigo: Integer);
    procedure MostraFormWBRclIns(SQLType: TSQLType; Filial, MovimCod, MovimTwn,
              Codigo, Controle: Integer; ValorT, AreaM2: Double;
              QrReopen: TmySQLQuery; SaldoPecas: Double; WBRclass: TWBRclass);
    procedure MostraFormWBAjsCab(Codigo, Controle: Integer);
    procedure MostraFormWBPallet();
    procedure MostraFormWBInnCab(Codigo, WBInnIts, WBReclas: Integer);
    procedure MostraFormWBOutCab(Codigo, Controle: Integer);
    procedure MostraFormWBOutIts(SQLType: TSQLType; Controle: Integer;
              QrCab, QrIts: TmySQLQuery);
    procedure MostraFormWBIndsWE(SQLType: TSQLType; Controle, MPVIts,
              Emit, EmitCus, InsEmpr: Integer; QrWBMovIts: TmySQLQuery;
              FormaWBIndsWE: TFormaWBIndsWE);
    procedure MostraFormWBRclCab(Codigo, WBRclIts, WBReclas: Integer);
    procedure MostraFormBalancoPQ();
    procedure ExtratosFinanceirosEmpresaUnica(TabLctA: String);
    procedure MostraPQe(Codigo: Integer);
    function  FixBugAtualizacoesApp(FixBug: Integer; FixBugStr: String): Boolean;
    procedure MostraFormEmitGru(Codigo: Integer);
    // Provis�rio
    procedure Mover_PrdGrupTip_do_GraGruY_Para_GraGrYPGT();

    // Fim provis�rio

  end;

var
  FmPrincipal2: TFmPrincipal2;

implementation

uses UnMyObjects, Module, Entidades, MyGlyfs, MyDBCheck, BlueDerm_Dmk, UnEiia_PF,
  Opcoes, MyListas, MyVCLSkin, UCreate, PQ, Embalagens, UnGFat_Jan, UnALL_Jan,
  ListaSetores, GruposQuimicos, Formulas, CalcPercent, PQx, PQE, (*PQB,*) PQB2,
  DefPecas, PQImp, MPIn, Defeitosloc, Defeitostip, About, UnApp_Jan,
  MPClas, MPGrup, Pallets, MPVn, MPVImp,
  Equipes, FluxosCab, Senha, ArtigosGrupos, Duplicata1, Imprime,
  PQPed, MPP, MPPImp, Espessuras, EntiMP, UnMsgInt, PortSerBal,
  (*NF1,*) CFOP2003, MPEstagios, MPV2, Contas, SubGrupos, Grupos, PlaCtas, Plano,
  Carteiras, Conjuntos, ImpObs, Promissoria, NovaVersao, Backup3, ContasNiv,
  CMPTInn, CMPTOut, RolPerdas, TintasCab, Bancos, ServicoManager, Entidade2,
  Matriz, ParamsEmp, ModuleGeral, ContasSdoImp, CambioMda, CambioCot, StqManCad,
  ModPediVda, PediVdaImp, PediAcc, PediPrzCab1, (*PediPrzCab2,*) (*PediVda,*)
  Regioes, TabePrcCab, Geosite, (*SugVPedCab,*) NCMs, UnidMed, FisRegCad,
  PrdGrupTip, GraGruN, GraCorPan, GraCorCad, GraCorFam, GraTamCad, GraCusPrc,
  StqCenCad, StqBalCad, UFs, ListServ, GraSrv1, CNAE21, ModProd,
  EntiLoad02, IBGE_DTB, GraImpLista, (*CMPPVai,*) MPClasMulGer,
  StqInnCad, PreEmail, ModuleFin, CECTInn, CECTOut, CashBal,
  Prosoft1, LeDataArq, LoadXML, Email_SSL, Restaura2, NFMoDocFis,
  PQCorrigeFatID, Fabricas, ClasFisc, NFaEditCSOSNAlq, (*EntradaCab,*) NatOper,
  TesteXML, PrdGruNewU, MPReclasGer, MPReclasIns, GraImpConflitos, MPSubProd,
  GraOptEnt, PQxExcl2, Maladireta, GraGruReduzido, GetValor,
  (* PQRet, PQRCab, PQRAjuInn, PQRAjuOut, *)
(*
  SPED_EFD_Importa, EfdIcmsIpiExporta, SPED_EFD_Compara,
  EFD_E001, EFD_C001, EFD_D001, ImportaSintegraGer, Sintegra_Arq,
*)
  SPED_EFD_Tabelas, SPEDEstqGer,
  //
  UnGrade_Tabs, LinkRankSkin, ReceDesp, MPRecebImp,
  UnDmkWeb, OSsAbertas, PQSubstitui, WBInnCab, WBMovImp, WBRclIns,
  DmkDAC_PF, WBOutCab, WBArtCab, CfgCadLista, WBOutIts, WBAjsCab, WBIndsWE,
  WBMPrCab, WBRclCab, WBPallet, EmitGru,
  Resultados1, Resultados2, LctAjustesB, CartNiv2, SelfGer2, ModuleLct2,
  // Provisorio
  TesteChart, Unit1, VSCorrigeMulFrn, UnAppPF, GerlShowGrid, Unit4,
  // Fim provisorio
  ContasHistSdo3, UnVS_PF, ModuleNFe_0000, NFe_PF, UnGrade_Jan, FavoritosG,
  UnTributos_PF, ModVS, VSCorrigeMovimTwn, (*EFD_RegObrig,*) UnEfdIcmsIPI_PF,
  UnEfdIcmsIpi_Jan, UnPQ_PF, UnConsumoGerlJan, UnConsumoJan, UnFixBugs,
  SPED_EFD_Importa, WBInnIts, UnUMedi_PF, VSInfIEC, ModVS_CRC, UnVS_Jan,
  VSClassifOneDefei, Principal;

const
  FAltLin = CO_POLEGADA / 2.16;

{$R *.DFM}

procedure TFmPrincipal2.ListadeWebServices1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeWebservices();
end;

procedure TFmPrincipal2.ListaEstoqueHistrico1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraImpLista, FmGraImpLista, afmoNegarComAviso) then
  begin
    FmGraImpLista.ShowModal;
    FmGraImpLista.Destroy;
  end;
end;

procedure TFmPrincipal2.Locais2Click(Sender: TObject);
begin
  MostraDefeitosloc(0);
end;

function TFmPrincipal2.FixBugAtualizacoesApp(FixBug: Integer;
  FixBugStr: String): Boolean;
begin
  Result := True;
  try
    if FixBug = 0 then
      Result := True
    else if FixBug = 1 then
      Result := AppPF.CorrigeCamposNFemPQe()
    else
      Result := False;
  except
    Result := False;
  end;
end;

procedure TFmPrincipal2.Fluxos1Click(Sender: TObject);
begin
  CadastroFluxos();
end;

procedure TFmPrincipal2.Fluxos3Click(Sender: TObject);
begin
  CadastroFluxos();
end;

procedure TFmPrincipal2.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente();
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  //
  if Geral.VersaoTxt2006(CO_VERSAO) <> Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    Geral.MB_Aviso(ivMsgVersaoDifere);
  if not FALiberar then
    Timer1.Enabled := True;
end;

procedure TFmPrincipal2.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
  Imagem(*, Avisa*): String;
  //i, j: Integer;
begin
  VAR_CAMBIO_DATA       := 0;
  VAR_MULTIPLAS_TAB_LCT := True;
  VAR_TYPE_LOG          := ttlFiliLog;
  VAR_ADMIN             := 'owemqOQWD';
  //
  VAR_AdvToolBarPagerPrincipal := AdvToolBarPager1;
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  //VAR_CALCULADORA_COMPONENTCLASS := TFmCalculadora;
  //VAR_CALCULADORA_REFERENCE      := FmCalculadora;
  //
  AdvToolBar23.Visible := False;
  //
  Geral.DefineFormatacoes;
  Application.OnException := MyObjects.MostraErro;
  VAR_USA_TAG_BITBTN := True;
  VAR_CAMINHOSKINPADRAO := 'C:\Dermatek\Skins\VCLSkin\Office 2007.skn';
  FTipoNovoEnti := 0;
  VAR_STLOGIN       := StatusBar.Panels[01];
  //
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  //
  VAR_STEMPRESA     := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  //VAR_STAVISOS      := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[09];
  VAR_SKINUSANDO    := StatusBar.Panels[11];
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  Application.OnMessage := MyObjects.FormMsg;
  Application.OnIdle    := AppIdle;
  //
  Imagem := Geral.ReadAppKey(
    'ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg', HKEY_LOCAL_MACHINE);
  //
  PageControl1.Align := alClient;
  //
  if FileExists(Imagem) then
  begin
    ImgPrincipal.Picture.LoadFromFile(Imagem);
    AjustaEstiloImagem(ImgPrincipal.Tag);
  end;
  AjustaEstiloImagem(-1);
  UnPQx.SetaVariaveisIniciais;
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  SkinMenu(MenuStyle);
  VAR_CAD_POPUP := PMGeral;
  VAR_LOC_POPUP := PMLocaliza;
  AdvToolBarPager1.ActivePageIndex := 0;
  // M L A G e r a l .CopiaItensDeMenu(PMGeral, Cadastros1, FmPrincipal);
  MyObjects.CopiaItensDeMenu(PMGeral, FmPrincipal);
  //
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfTrue);
end;

procedure TFmPrincipal2.SelecionaImagemdefundo;
begin
  if OpenPictureDialog1.Execute then
  begin
    ImgPrincipal.Picture.LoadFromFile(OpenpictureDialog1.FileName);
    Geral.WriteAppKey('ImagemFundo', Application.Title, OpenPictureDialog1.FileName, ktString, HKEY_LOCAL_MACHINE);
  end;
end;

procedure TFmPrincipal2.Setores1Click(Sender: TObject);
begin
  CadastroDeSetores(0);
end;

procedure TFmPrincipal2.Setores2Click(Sender: TObject);
begin
  CadastroDeSetores(0);
end;

procedure TFmPrincipal2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if DModG <> nil then
    DModG.MostraBackup3(False);
  Application.Terminate;
end;

procedure TFmPrincipal2.Prepara1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSReclassPrePalCac(stIns, 0, 0, 0, 0, 0, 0, 0, 0);
end;

function TFmPrincipal2.PreparaMinutosSQL: Boolean;
//var
//  i: Integer;
begin
  Result := True;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM ocupacao');
    Dmod.QrUpdL.ExecSQL;
    ///
(*    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('INSERT INTO ocupacao SET Min=:P0');
    for i := 1 to 1440 do
    begin
      Dmod.QrUpdL.Params[0].AsInteger := I;
      Dmod.QrUpdL.ExecSQL;
    end;*)
  except
    Result := False
  end;
end;

procedure TFmPrincipal2.Quitados1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmResultados2, FmResultados2, afmoNegarComAviso) then
  begin
    FmResultados2.ShowModal;
    FmResultados2.Destroy;
  end;
end;

function TFmPrincipal2.AdicionaMinutosSQL(HI, HF: TTime): Boolean;
var
  Hour, Min, Sec, MSec: Word;
  i, Ini, Fim: Integer;
begin
  Result := True;
  if (HI=0) and (HF=0) then Exit;
  DecodeTime(HI, Hour, Min, Sec, MSec);
  Ini := (Hour * 60) + Min;
  DecodeTime(HF, Hour, Min, Sec, MSec);
  Fim := (Hour * 60) + Min - 1;
  if Fim < Ini then Fim := Fim + 1440;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO ocupacao SET Qtd=1, Min=:P0');
  for i := Ini to Fim do
  begin
    Dmod.QrUpdL.Params[0].AsInteger := I;
    Dmod.QrUpdL.ExecSQL;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton100Click(Sender: TObject);
begin
  CadastroPQ(0);
end;

procedure TFmPrincipal2.AdvGlowButton101Click(Sender: TObject);
begin
  CadastroPQImp('');
end;

procedure TFmPrincipal2.AdvGlowButton102Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Itens, Controle: Integer;
  SubClass: String;
begin
  PageControl1.ActivePageIndex := 0;
  //
  Qry := TmySQLQuery.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle, SubClass ',
    'FROM vscacitsa cia  ',
    'WHERE SubClass <> "" ',
    '']);
    //
    Qry.First;
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    Itens := 0;
    while not Qry.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      SubClass := dmkPF.SoTextoLayout(Qry.FieldByName('SubClass').AsString);
      if SubClass <> Qry.FieldByName('SubClass').AsString then
      begin
        Controle := Qry.FieldByName('Controle').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
        'SubClass'], ['Controle'], [SubClass], [Controle], False) then
          Itens := Itens + 1;
      end;
      //
      Qry.Next;
    end;
    Geral.MB_Aviso(Geral.FF0(Itens) + ' itens foram corrigidos!');
  finally
    Screen.Cursor := crDefault;
    Qry.Free;
  end;
end;

procedure TFmPrincipal2.ATBBATBAutoExpClick(Sender: TObject);
begin
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfInverse);
end;

procedure TFmPrincipal2.ATBBFavoritosClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFavoritosG, FmFavoritosG, afmoLiberado) then
  begin
    FmFavoritosG.ShowModal;
    FmFavoritosG.Destroy;
    //
    PageControl1.ActivePageIndex := 0;
    //
    DModG.CriaFavoritos(AdvToolBarPager1, LaAviso2, LaAviso1, AGBEntidades, FmPrincipal);
  end;
end;

procedure TFmPrincipal2.ATBBLastWork1Click(Sender: TObject);
(*
const
  Pecas    = 246;
  Peso     = 6460;
  AreaM2   = 998.90;
  FatorMP  = 1;
  FatorAR  = 1.10;
var
  Nota: Double;
*)
begin
   //
   Application.CreateForm(TForm1, Form1);
   Form1.ShowModal;
   Form1.Destroy;
end;

procedure TFmPrincipal2.ATBBLastWork2Click(Sender: TObject);
(*
var
  Txt: String;
  IMEI: Integer;
begin
  Txt := '12153';
    IMEI := Geral.IMV(Txt);
    VS_PF.MostraFormVSArvoreArtigos(IMEI);
*)
//begin
  //EfdIcmsIpi_Jan.MostraFormEfdIcmsIpiExporta();
  //AppPF.MostraFormSpedEfdIcmsIpiH010_Balancos(3, 201601, -11, 1);
var
  Num_TXT: String;
  Num: Integer;
begin
  if InputQuery('IMEI', 'Informe o Numero do IMEI', Num_TXT) then
  begin
    Num := Geral.IMV(Num_TXT);
    DmModVS.AtualizaVSMovIts_CusEmit(Num);
  end;
end;

(*
  Nota := VS_PF.NotaCouroRibeira(Pecas, Peso, AreaM2, FatorMP, FatorAR);
  Geral.MB_Info(Geral.FFT(Nota, 4, siNegativo));
*)
(*
const
  FBase64 = 'C:\Dermatek\Base64\Base64.exe';
  Caminho = 'C:\Dermatek\NFE\Temp\';
var
  Str: String;
  Comando: PChar;
  IniPath, OutPath: String;
begin
  Str :=
  'H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwda' +
  'UcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkrayZ' +
  '4hgKrIHz9+fB8/Ih7XefPiWZ6+W5TL5tG7pvjso3nbrh7dvXt1dTW+ujeu6ou7ezs' +
  '7u3d/7y+ev57O80W2XSybNltO84/sW7Ob3/oovczrJqs++2h3vLOjrwavraq6zcrz' +
  'oplm5bhYno8n9d3lef7R0ePpnFA8ur+zu7+7u3N/Z2/v4NNPdwj67oP79+nHDp4He' +
  '/c/pb/37t1/sL+39/iuvPP45MXL7xyF7zy+yx8+fveiWuRHb05fvTp78+Wrsy/Tp1' +
  '+mJ19+9erL9Pmbp8eP78r3j89Oj/YO7u3tPtjfvff4Lv31eDY/XRRHe4TO9u7u9u7' +
  '+m93dRzsPHu3sbO/s0b+P70qDx+3qxbOj3cd3+efjS/yxN8b3+PXxrLj4yaw8au6+' +
  'On3x9Hr61YPvltM393/q4ef3z37i7PL3uf5FB58RJGlEXb7Kp5O2CnrdQa/391yv2' +
  'ubx8mVdtUe7IBhIQwTa36Fhy8ePp6+LFrQhzMyvj+8KGxz9Pz8CEx0PAgAA';
  if Str <> '' then
  begin
    if FileExists(FBase64) then
    begin
      //if FileExist(TxtPath) then
        //DeleteFile(TxtPath);
      IniPath := Caminho + 'Texto.inn';
      OutPath := dmkPF.MudaExtensaoDeArquivo(IniPath, 'out');
      Geral.SalvaTextoEmArquivo(IniPath, Str, True);
      Comando := PChar(FBase64 + ' -e ' + IniPath + ' ' + OutPath);
      //"C:\base64 -e bg2.jpg bg2.txt"
      dmkPF.WinExecAndWaitOrNot(Comando, SW_SHOW, MeEncode, True);
      //ArqExiste := FileExists(ImgPath);
    end else Geral.MB_Aviso('O aplicativo "' + FBase64 +
    '" n�o existe para descomprimir o texto "' + Str + '".');
  end;
end;
*)
(*
var
  Str: String;
  Buffer: TBytes;
  StreamInn, StreamOut: TMemoryStream;
begin
  Str :=
  'H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwda' +
  'UcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkrayZ' +
  '4hgKrIHz9+fB8/Ih7XefPiWZ6+W5TL5tG7pvjso3nbrh7dvXt1dTW+ujeu6ou7ezs' +
  '7u3d/7y+ev57O80W2XSybNltO84/sW7Ob3/oovczrJqs++2h3vLOjrwavraq6zcrz' +
  'oplm5bhYno8n9d3lef7R0ePpnFA8ur+zu7+7u3N/Z2/v4NNPdwj67oP79+nHDp4He' +
  '/c/pb/37t1/sL+39/iuvPP45MXL7xyF7zy+yx8+fveiWuRHb05fvTp78+Wrsy/Tp1' +
  '+mJ19+9erL9Pmbp8eP78r3j89Oj/YO7u3tPtjfvff4Lv31eDY/XRRHe4TO9u7u9u7' +
  '+m93dRzsPHu3sbO/s0b+P70qDx+3qxbOj3cd3+efjS/yxN8b3+PXxrLj4yaw8au6+' +
  'On3x9Hr61YPvltM393/q4ef3z37i7PL3uf5FB58RJGlEXb7Kp5O2CnrdQa/391yv2' +
  'ubx8mVdtUe7IBhIQwTa36Fhy8ePp6+LFrQhzMyvj+8KGxz9Pz8CEx0PAgAA';
  if Str <> '' then
  begin
    StreamInn := TMemoryStream.Create;
    try
      StreamOut := TMemoryStream.Create;
      try
        StreamInn.WriteBuffer(Pointer(Str)^, Length(Str));
        StreamOut.WriteBuffer(Pointer(Str)^, Length(Str));
        //StreamInn.Position := 0;
        // Erro!
        //IdCompressorZLib1.Decompress(StreamInn, StreamOut);
        DecompressStream(StreamInn, StreamOut);
        //
        Geral.MB_Info(StreamOut.ToString);
      finally
        StreamOut.Free;
      end;
    finally
      StreamInn.Free;
    end;
  end;
end;
*)
(*
const
  cCompressedData = 1;
var
  Str: String;
  Buffer: TBytes;
  StreamInn, StreamOut: TStringStream;
  //
begin
  Str :=
  'H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwda' +
  'UcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkrayZ' +
  '4hgKrIHz9+fB8/Ih7XefPiWZ6+W5TL5tG7pvjso3nbrh7dvXt1dTW+ujeu6ou7ezs' +
  '7u3d/7y+ev57O80W2XSybNltO84/sW7Ob3/oovczrJqs++2h3vLOjrwavraq6zcrz' +
  'oplm5bhYno8n9d3lef7R0ePpnFA8ur+zu7+7u3N/Z2/v4NNPdwj67oP79+nHDp4He' +
  '/c/pb/37t1/sL+39/iuvPP45MXL7xyF7zy+yx8+fveiWuRHb05fvTp78+Wrsy/Tp1' +
  '+mJ19+9erL9Pmbp8eP78r3j89Oj/YO7u3tPtjfvff4Lv31eDY/XRRHe4TO9u7u9u7' +
  '+m93dRzsPHu3sbO/s0b+P70qDx+3qxbOj3cd3+efjS/yxN8b3+PXxrLj4yaw8au6+' +
  'On3x9Hr61YPvltM393/q4ef3z37i7PL3uf5FB58RJGlEXb7Kp5O2CnrdQa/391yv2' +
  'ubx8mVdtUe7IBhIQwTa36Fhy8ePp6+LFrQhzMyvj+8KGxz9Pz8CEx0PAgAA';
  if Str <> '' then
  begin
    Buffer := PlatformBytesOf(Str);
SrcStream. Position: = 0;
GZStream: = TDecompressionStream. Create (SrcStream); [b]//here happens AV to 00000038 [/b address
try
DestStream. Position: = 0;
GZStream. Position: = 0;
try
repeat
bytesread: = GZStream. Read (mainbuffer, SizeOf (MainBuffer));
DestStream. Write (mainbuffer, bytesread);
until bytesread &lt;1024;
except
GZStream. Free;
exit;
    //Str := ProcessOutput(Buffer);
    //Buffer := ProcessOutput(Buffer);
    Str := ZDecompressStr(Buffer);
    //Geral.MB_Info(PlatformStringOf(Buffer));
    Geral.MB_Info(Str);
  end;
end;
*)
{
var
SrcStream, DestStream: TStringStream;
GZStream: TDecompressionStream;
BytesRead: integer;
MainBuffer: array [0..1023] of AnsiChar;
  Str: String;
begin
  Str :=
  'H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwda' +
  'UcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkrayZ' +
  '4hgKrIHz9+fB8/Ih7XefPiWZ6+W5TL5tG7pvjso3nbrh7dvXt1dTW+ujeu6ou7ezs' +
  '7u3d/7y+ev57O80W2XSybNltO84/sW7Ob3/oovczrJqs++2h3vLOjrwavraq6zcrz' +
  'oplm5bhYno8n9d3lef7R0ePpnFA8ur+zu7+7u3N/Z2/v4NNPdwj67oP79+nHDp4He' +
  '/c/pb/37t1/sL+39/iuvPP45MXL7xyF7zy+yx8+fveiWuRHb05fvTp78+Wrsy/Tp1' +
  '+mJ19+9erL9Pmbp8eP78r3j89Oj/YO7u3tPtjfvff4Lv31eDY/XRRHe4TO9u7u9u7' +
  '+m93dRzsPHu3sbO/s0b+P70qDx+3qxbOj3cd3+efjS/yxN8b3+PXxrLj4yaw8au6+' +
  'On3x9Hr61YPvltM393/q4ef3z37i7PL3uf5FB58RJGlEXb7Kp5O2CnrdQa/391yv2' +
  'ubx8mVdtUe7IBhIQwTa36Fhy8ePp6+LFrQhzMyvj+8KGxz9Pz8CEx0PAgAA';
  if Str <> '' then
  begin
    SrcStream := TStringStream.Create;
    try
      DestStream := TStringStream.Create;
      try
        SrcStream.WriteBuffer(Pointer(Str)^, Length(Str));
        SrcStream.Position := 0;
        GZStream := TDecompressionStream.Create(SrcStream); //[b]//here happens AV to 00000038 [/b address
        try
          DestStream.Position := 0;
          GZStream.Position := 0;
        try
          repeat
          bytesread := GZStream.Read(mainbuffer, SizeOf(MainBuffer));
          DestStream.Write(mainbuffer, bytesread);
          until bytesread <> 0//&lt;1024;
        except
          GZStream.Free;
        exit;
        end;
        finally

        end;
      finally
        //
      end;
    finally
      //
    end;
  end;
end;
}
(*
var
  SrcStream, DestStream: TStringStream;
  Str: String;
begin
  Str :=
  'H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwda' +
  'UcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkrayZ' +
  '4hgKrIHz9+fB8/Ih7XefPiWZ6+W5TL5tG7pvjso3nbrh7dvXt1dTW+ujeu6ou7ezs' +
  '7u3d/7y+ev57O80W2XSybNltO84/sW7Ob3/oovczrJqs++2h3vLOjrwavraq6zcrz' +
  'oplm5bhYno8n9d3lef7R0ePpnFA8ur+zu7+7u3N/Z2/v4NNPdwj67oP79+nHDp4He' +
  '/c/pb/37t1/sL+39/iuvPP45MXL7xyF7zy+yx8+fveiWuRHb05fvTp78+Wrsy/Tp1' +
  '+mJ19+9erL9Pmbp8eP78r3j89Oj/YO7u3tPtjfvff4Lv31eDY/XRRHe4TO9u7u9u7' +
  '+m93dRzsPHu3sbO/s0b+P70qDx+3qxbOj3cd3+efjS/yxN8b3+PXxrLj4yaw8au6+' +
  'On3x9Hr61YPvltM393/q4ef3z37i7PL3uf5FB58RJGlEXb7Kp5O2CnrdQa/391yv2' +
  'ubx8mVdtUe7IBhIQwTa36Fhy8ePp6+LFrQhzMyvj+8KGxz9Pz8CEx0PAgAA';
  if Str <> '' then
  begin
    SrcStream := TStringStream.Create;
    try
      DestStream := TStringStream.Create;
      try
        SrcStream.WriteBuffer(Pointer(Str)^, Length(Str));
        SrcStream.Position := 0;
        Str := frxDecompressStream(SrcStream, DestStream);
        Geral.MB_Aviso(Str);
      finally
        DestStream.Free;
      end;
    finally
      SrcStream.Free;
    end;
  end;
end;
*)
(*
//Op��o de Encode
var
	x, InnPath, OutPath: String;
const
  FDirXML = 'C:\Dermatek\Temp\Orig\';
  FArqXML = 'TextoXML';
  FExtB64 = '.cryp';
  FExtZip = '.gzip';
  Txt =
  'H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwd' +
  'aUcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkra' +
  'yZ4hgKrIHz9+fB8/Ih7XefPiWZ6+W5TL5tG7pvjso3nbrh7dvXt1dTW+ujeu6ou7' +
  'ezs7u3d/7y+ev57O80W2XSybNltO84/sW7Ob3/oovczrJqs++2h3vLOjrwavraq6' +
  'zcrzoplm5bhYno8n9d3lef7R0ePpnFA8ur+zu7+7u3N/Z2/v4NNPdwj67oP79+nH' +
  'Dp4He/c/pb/37t1/sL+39/iuvPP45MXL7xyF7zy+yx8+fveiWuRHb05fvTp78+Wr' +
  'sy/Tp1+mJ19+9erL9Pmbp8eP78r3j89Oj/YO7u3tPtjfvff4Lv31eDY/XRRHe4TO' +
  '9u7u9u7+m93dRzsPHu3sbO/s0b+P70qDx+3qxbOj3cd3+efjS/yxN8b3+PXxrLj4' +
  'yaw8au6+On3x9Hr61YPvltM393/q4ef3z37i7PL3uf5FB58RJGlEXb7Kp5O2Cnrd' +
  'Qa/391yv2ubx8mVdtUe7IBhIQwTa36Fhy8ePp6+LFrQhzMyvj+8KGxz9Pz8CEx0PAgAA';
begin
  ForceDirectories(FDirXML);
  InnPath := FDirXML + FArqXML + FExtB64;
  OutPath := FDirXML + FArqXML + FExtZip;
  UnDmkCoding.DecriptaBase64(Txt, InnPath, OutPath, Memo3);
  x := UnDmkCoding.DescompactaArquivoGZipToString(OutPath);
  Geral.MB_Aviso(x);
end;
*)

procedure TFmPrincipal2.ATBBMenuGeralClick(Sender: TObject);
begin
  MyObjects.MostraPopupGeral();
end;

procedure TFmPrincipal2.ATBFormulasImpFIClick(Sender: TObject);
begin
  CadastroFormulas(0, 0);
end;

procedure TFmPrincipal2.ATBFormulasImpWEClick(Sender: TObject);
begin
  PQ_PF.MostraFormFormulasImpWE();
end;

procedure TFmPrincipal2.AdvGlowButton104Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPallets, FmPallets, afmoNegarComAviso) then
  begin
    FmPallets.ShowModal;
    FmPallets.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton105Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPrdGrupTip, FmPrdGrupTip, afmoNegarComAviso) then
  begin
    FmPrdGrupTip.ShowModal;
    FmPrdGrupTip.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton106Click(Sender: TObject);
  procedure InsereAtual();
  var
    DataHora, Observ, Marca: String;
    Codigo, Controle, MovimCod, MovimNiv, MovimTwn, Empresa, Terceiro, CliVenda,
    MovimID, LnkNivXtr1, LnkNivXtr2, Pallet, GraGruX, SrcMovID, SrcNivel1,
    SrcNivel2, SrcGGX, SerieFch, Ficha, Misturou, FornecMO, DstMovID, DstNivel1,
    DstNivel2, DstGGX, AptoUso, TpCalcAuto: Integer;
    Pecas, PesoKg, AreaM2, AreaP2, ValorT, SdoVrtPeca, SdoVrtPeso, SdoVrtArM2,
    CustoMOKg, CustoMOTot, ValorMP, QtdGerPeca, QtdGerPeso, QtdGerArM2,
    QtdGerArP2, QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, NotaMPAG: Double;
  begin
    Codigo         := QrVSMovItsCodigo     .Value;
    //Controle       := QrVSMovItsControle   .Value;
    MovimCod       := QrVSMovItsMovimCod   .Value;
    MovimNiv       := Integer(TEstqMovimNiv.eminSorcCurtiXX);//QrVSMovItsMovimNiv   .Value;
    //MovimTwn       := QrVSMovItsMovimTwn   .Value;
    Empresa        := QrVSMovItsEmpresa    .Value;
    Terceiro       := QrVSMovItsTerceiro   .Value;
    CliVenda       := QrVSMovItsCliVenda   .Value;
    MovimID        := QrVSMovItsMovimID    .Value;
    LnkNivXtr1     := QrVSMovItsLnkNivXtr1 .Value;
    LnkNivXtr2     := QrVSMovItsLnkNivXtr2 .Value;
    DataHora       := Geral.FDT(QrVSMovItsDataHora.Value, 109);
    Pallet         := QrVSMovItsPallet     .Value;
    GraGruX        := QrVSMovItsGraGruX    .Value;
    Pecas          := -QrVSMovItsPecas      .Value;
    PesoKg         := -QrVSMovItsPesoKg     .Value;
    AreaM2         := -QrVSMovItsAreaM2     .Value;
    AreaP2         := -QrVSMovItsAreaP2     .Value;
    ValorT         := -QrVSMovItsValorT     .Value;
    SrcMovID       := 0;//QrVSMovItsSrcMovID   .Value;
    SrcNivel1      := 0;//QrVSMovItsSrcNivel1  .Value;
    SrcNivel2      := 0;//QrVSMovItsSrcNivel2  .Value;
    SrcGGX         := 0;//QrVSMovItsSrcGGX     .Value;
    SdoVrtPeca     := QrVSMovItsSdoVrtPeca .Value;
    SdoVrtPeso     := QrVSMovItsSdoVrtPeso .Value;
    SdoVrtArM2     := QrVSMovItsSdoVrtArM2 .Value;
    Observ         := QrVSMovItsObserv     .Value;
    SerieFch       := QrVSMovItsSerieFch   .Value;
    Ficha          := QrVSMovItsFicha      .Value;
    Misturou       := QrVSMovItsMisturou   .Value;
    FornecMO       := QrVSMovItsFornecMO   .Value;
    CustoMOKg      := QrVSMovItsCustoMOKg  .Value;
    CustoMOTot     := QrVSMovItsCustoMOTot .Value;
    ValorMP        := QrVSMovItsValorMP    .Value;
    DstMovID       := QrVSMovItsDstMovID   .Value;
    DstNivel1      := QrVSMovItsDstNivel1  .Value;
    DstNivel2      := QrVSMovItsDstNivel2  .Value;
    DstGGX         := QrVSMovItsDstGGX     .Value;
    QtdGerPeca     := QrVSMovItsQtdGerPeca .Value;
    QtdGerPeso     := QrVSMovItsQtdGerPeso .Value;
    QtdGerArM2     := QrVSMovItsQtdGerArM2 .Value;
    QtdGerArP2     := QrVSMovItsQtdGerArP2 .Value;
    QtdAntPeca     := QrVSMovItsQtdAntPeca .Value;
    QtdAntPeso     := QrVSMovItsQtdAntPeso .Value;
    QtdAntArM2     := QrVSMovItsQtdAntArM2 .Value;
    QtdAntArP2     := QrVSMovItsQtdAntArP2 .Value;
    AptoUso        := QrVSMovItsAptoUso    .Value;
    NotaMPAG       := QrVSMovItsNotaMPAG   .Value;
    Marca          := QrVSMovItsMarca      .Value;
    TpCalcAuto     := QrVSMovItsTpCalcAuto .Value;

    //
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, stIns, 0);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, stIns, 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_INS_TAB_VMI, False, [
    'Codigo', 'MovimCod', 'MovimNiv',
    'MovimTwn', 'Empresa', 'Terceiro',
    'CliVenda', 'MovimID', 'LnkNivXtr1',
    'LnkNivXtr2', CO_DATA_HORA_VMI, 'Pallet',
    'GraGruX', 'Pecas', 'PesoKg',
    'AreaM2', 'AreaP2', 'ValorT',
    'SrcMovID', 'SrcNivel1', 'SrcNivel2',
    'SrcGGX', 'SdoVrtPeca', 'SdoVrtPeso',
    'SdoVrtArM2', 'Observ', 'SerieFch',
    'Ficha', 'Misturou', 'FornecMO',
    'CustoMOKg', 'CustoMOTot', 'ValorMP',
    'DstMovID', 'DstNivel1', 'DstNivel2',
    'DstGGX', 'QtdGerPeca', 'QtdGerPeso',
    'QtdGerArM2', 'QtdGerArP2', 'QtdAntPeca',
    'QtdAntPeso', 'QtdAntArM2', 'QtdAntArP2',
    'AptoUso', 'NotaMPAG', 'Marca',
    'TpCalcAuto'], [
    'Controle'], [
    Codigo, MovimCod, MovimNiv,
    MovimTwn, Empresa, Terceiro,
    CliVenda, MovimID, LnkNivXtr1,
    LnkNivXtr2, DataHora, Pallet,
    GraGruX, Pecas, PesoKg,
    AreaM2, AreaP2, ValorT,
    SrcMovID, SrcNivel1, SrcNivel2,
    SrcGGX, SdoVrtPeca, SdoVrtPeso,
    SdoVrtArM2, Observ, SerieFch,
    Ficha, Misturou, FornecMO,
    CustoMOKg, CustoMOTot, ValorMP,
    DstMovID, DstNivel1, DstNivel2,
    DstGGX, QtdGerPeca, QtdGerPeso,
    QtdGerArM2, QtdGerArP2, QtdAntPeca,
    QtdAntPeso, QtdAntArM2, QtdAntArP2,
    AptoUso, NotaMPAG, Marca,
    TpCalcAuto], [
    Controle], True) then
    begin
      Controle  := QrVSMovItsControle.Value;
      MovimNiv  := Integer(TEstqMovimNiv.eminBaixCurtiXX);
      DstMovID  := 0;
      DstNivel1 := 0;
      DstNivel2 := 0;
      DstGGX    := 0;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'DstMovID', 'DstNivel1', 'DstNivel2',
      'DstGGX', 'MovimNiv', 'MovimTwn'], ['Controle'], [
      DstMovID, DstNivel1, DstNivel2,
      DstGGX, MovimNiv, MovimTwn], [Controle], False);
    end;
  end;
begin
  PageControl1.ActivePageIndex := 0;
  //
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminSorcIndsWB)), // Antigo
    'AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),
    '']);
    //
    PB1.Position := 0;
    PB1.Max := QrVSMovIts.RecordCount;
    QrVSMovIts.First;
    while not QrVSMovIts.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      InsereAtual();
      //
      QrVSMovIts.Next;
    end;
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ' + CO_UPD_TAB_VMI,
    'SET MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)),
    'WHERE MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestIndsWB)),
    'AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),
    '']);
    //
    Geral.MB_Aviso(Geral.FF0(QrVSMovIts.RecordCount) + ' itens foram corrigidos!');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton107Click(Sender: TObject);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmTintasCab2, FmTintasCab2, afmoNegarComAviso) then
      begin
        FmTintasCab2.ShowModal;
        FmTintasCab2.Destroy;
      end;
    fggxPQ:
}
      if DBCheck.CriaFm(TFmTintasCab, FmTintasCab, afmoNegarComAviso) then
      begin
        FmTintasCab.ShowModal;
        FmTintasCab.Destroy;
      end;
{
  end;
}
end;

procedure TFmPrincipal2.AdvGlowButton108Click(Sender: TObject);
begin
  PQ_PF.MostraFormFormulasImpFI();
end;

procedure TFmPrincipal2.AdvGlowButton109Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal2.AdvGlowButton10Click(Sender: TObject);
begin
  CadastroFormulas(0, 0);
end;

procedure TFmPrincipal2.AdvGlowButton110Click(Sender: TObject);
begin
(*
  UCriar.GerenciaTabelaLocal('NF1Fat', acCreate);
  UCriar.GerenciaTabelaLocal('NF1Pro', acCreate);
  UCriar.GerenciaTabelaLocal('Imprimir1', acCreate);
  if DBCheck.CriaFm(TFmNF1, FmNF1, afmoNegarComAviso) then
  begin
    FmNF1.ShowModal;
    FmNF1.Destroy;
  end;
*)
end;

procedure TFmPrincipal2.AdvGlowButton111Click(Sender: TObject);
begin
  MostraBackup;
end;

procedure TFmPrincipal2.AdvGlowButton113Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton114Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraCorPan, FmGraCorPan, afmoNegarComAviso) then
  begin
    FmGraCorPan.ShowModal;
    FmGraCorPan.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton115Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraCorCad, FmGraCorCad, afmoNegarComAviso) then
  begin
    FmGraCorCad.ShowModal;
    FmGraCorCad.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton116Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraCorFam, FmGraCorFam, afmoNegarComAviso) then
  begin
    FmGraCorFam.ShowModal;
    FmGraCorFam.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton117Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraTamCad, FmGraTamCad, afmoNegarComAviso) then
  begin
    FmGraTamCad.ShowModal;
    FmGraTamCad.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton118Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraCusPrc, FmGraCusPrc, afmoNegarComAviso) then
  begin
    FmGraCusPrc.ShowModal;
    FmGraCusPrc.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton119Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqCenCad, FmStqCenCad, afmoNegarComAviso) then
  begin
    FmStqCenCad.ShowModal;
    FmStqCenCad.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton11Click(Sender: TObject);
begin
  CadastroDeGrupos;
end;

procedure TFmPrincipal2.AdvGlowButton120Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqBalCad, FmStqBalCad, afmoNegarComAviso) then
  begin
    FmStqBalCad.FMultiGrandeza := True;
    FmStqBalCad.ShowModal;
    FmStqBalCad.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton121Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmUFs, FmUFs, afmoNegarComAviso) then
  begin
    FmUFs.ShowModal;
    FmUFs.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton122Click(Sender: TObject);
begin
  {
  if DBCheck.CriaFm(TFmListServ, FmListServ, afmoNegarComAviso) then
  begin
    FmListServ.ShowModal;
    FmListServ.Destroy;
  end;
  }
end;

procedure TFmPrincipal2.AdvGlowButton123Click(Sender: TObject);
begin
  {
  if DBCheck.CriaFm(TFmGraSrv1, FmGraSrv1, afmoNegarComAviso) then
  begin
    FmGraSrv1.ShowModal;
    FmGraSrv1.Destroy;
  end;
  }
end;

procedure TFmPrincipal2.AdvGlowButton124Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabA();
end;

procedure TFmPrincipal2.AdvGlowButton125Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAE21, FmCNAE21, afmoNegarComAviso) then
  begin
    FmCNAE21.ShowModal;
    FmCNAE21.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton126Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormStepsNFe_StatusServico();
end;

procedure TFmPrincipal2.AdvGlowButton127Click(Sender: TObject);
begin
(*  DESABILITADO EM 2016-06-09 - Nova forma de devlver PQ pelo pqw!
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      Geral.MB_Aviso('A��o n�o implementada!' + sLineBreak + 'Solicite � DERMATEK!');
    fggxPQ:
}
      if DBCheck.CriaFm(TFmPQRCab, FmPQRCab, afmoNegarComAviso) then
      begin
        FmPQRCab.ShowModal;
        FmPQRCab.Destroy;
      end;
{
  end;
}
*)
end;

procedure TFmPrincipal2.AdvGlowButton128Click(Sender: TObject);
begin
{ Desativado em 2014-10-17! Algu�m usa?
  if DBCheck.CriaFm(TFmNFeMPInn, FmNFeMPInn, afmoNegarComAviso) then
  begin
    FmNFeMPInn.ShowModal;
    FmNFeMPInn.Destroy;
  end;
}
end;

procedure TFmPrincipal2.AdvGlowButton129Click(Sender: TObject);
{
var
  Controle: Integer;
  procedure InsereAtual();
  var
    DataHora, Observ, Marca: String;
    Codigo, (*Controle,*) MovimCod, MovimNiv, MovimTwn, Empresa, Terceiro, CliVenda,
    MovimID, LnkNivXtr1, LnkNivXtr2, Pallet, GraGruX, SrcMovID, SrcNivel1,
    SrcNivel2, SrcGGX, SerieFch, Ficha, Misturou, FornecMO, DstMovID, DstNivel1,
    DstNivel2, DstGGX, AptoUso, TpCalcAuto: Integer;
    Pecas, PesoKg, AreaM2, AreaP2, ValorT, SdoVrtPeca, SdoVrtPeso, SdoVrtArM2,
    CustoMOKg, CustoMOTot, ValorMP, QtdGerPeca, QtdGerPeso, QtdGerArM2,
    QtdGerArP2, QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, NotaMPAG: Double;
  begin
    Codigo         := QrVSMovItsCodigo     .Value;
    //Controle       := QrVSMovItsControle   .Value;
    MovimCod       := QrVSMovItsMovimCod   .Value;
    MovimNiv       := Integer(TEstqMovimNiv.eminSorcReclass);//QrVSMovItsMovimNiv   .Value;
    //MovimTwn       := QrVSMovItsMovimTwn   .Value;
    Empresa        := QrVSMovItsEmpresa    .Value;
    Terceiro       := QrVSMovItsTerceiro   .Value;
    CliVenda       := QrVSMovItsCliVenda   .Value;
    MovimID        := QrVSMovItsMovimID    .Value;
    LnkNivXtr1     := QrVSMovItsLnkNivXtr1 .Value;
    LnkNivXtr2     := QrVSMovItsLnkNivXtr2 .Value;
    DataHora       := Geral.FDT(QrVSMovItsDataHora.Value, 109);
    Pallet         := QrVSMovItsPallet     .Value;
    GraGruX        := QrVSMovItsGraGruX    .Value;
    Pecas          := -QrVSMovItsPecas      .Value;
    PesoKg         := -QrVSMovItsPesoKg     .Value;
    AreaM2         := -QrVSMovItsAreaM2     .Value;
    AreaP2         := -QrVSMovItsAreaP2     .Value;
    ValorT         := -QrVSMovItsValorT     .Value;
    SrcMovID       := 0;//QrVSMovItsSrcMovID   .Value;
    SrcNivel1      := 0;//QrVSMovItsSrcNivel1  .Value;
    //SrcNivel2      := ;//QrVSMovItsSrcNivel2  .Value;
    case QrVSMovItsControle.Value of
      3264, 3265, 3266, 3267, 3268, 3275, 3276: SrcNivel2 := 3231;
      3253, 3254, 3255, 3256, 3257: SrcNivel2 := 3240;
      3351, 3352, 3355: SrcNivel2 := 3250;
      3357, 3358, 3359: SrcNivel2 := 3256;
    end;
    SrcGGX         := 0;//QrVSMovItsSrcGGX     .Value;
    SdoVrtPeca     := QrVSMovItsSdoVrtPeca .Value;
    SdoVrtPeso     := QrVSMovItsSdoVrtPeso .Value;
    SdoVrtArM2     := QrVSMovItsSdoVrtArM2 .Value;
    Observ         := QrVSMovItsObserv     .Value;
    SerieFch       := QrVSMovItsSerieFch   .Value;
    Ficha          := QrVSMovItsFicha      .Value;
    Misturou       := QrVSMovItsMisturou   .Value;
    FornecMO       := QrVSMovItsFornecMO   .Value;
    CustoMOKg      := QrVSMovItsCustoMOKg  .Value;
    CustoMOTot     := QrVSMovItsCustoMOTot .Value;
    ValorMP        := QrVSMovItsValorMP    .Value;
    DstMovID       := QrVSMovItsDstMovID   .Value;
    DstNivel1      := QrVSMovItsDstNivel1  .Value;
    DstNivel2      := QrVSMovItsDstNivel2  .Value;
    DstGGX         := QrVSMovItsDstGGX     .Value;
    QtdGerPeca     := QrVSMovItsQtdGerPeca .Value;
    QtdGerPeso     := QrVSMovItsQtdGerPeso .Value;
    QtdGerArM2     := QrVSMovItsQtdGerArM2 .Value;
    QtdGerArP2     := QrVSMovItsQtdGerArP2 .Value;
    QtdAntPeca     := QrVSMovItsQtdAntPeca .Value;
    QtdAntPeso     := QrVSMovItsQtdAntPeso .Value;
    QtdAntArM2     := QrVSMovItsQtdAntArM2 .Value;
    QtdAntArP2     := QrVSMovItsQtdAntArP2 .Value;
    AptoUso        := QrVSMovItsAptoUso    .Value;
    NotaMPAG       := QrVSMovItsNotaMPAG   .Value;
    Marca          := QrVSMovItsMarca      .Value;
    TpCalcAuto     := QrVSMovItsTpCalcAuto .Value;

    //
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, stIns, 0);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, stIns, 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_INS_TAB_VMI, False, [
    'Codigo', 'MovimCod', 'MovimNiv',
    'MovimTwn', 'Empresa', 'Terceiro',
    'CliVenda', 'MovimID', 'LnkNivXtr1',
    'LnkNivXtr2', CO_DATA_HORA_VMI, 'Pallet',
    'GraGruX', 'Pecas', 'PesoKg',
    'AreaM2', 'AreaP2', 'ValorT',
    'SrcMovID', 'SrcNivel1', 'SrcNivel2',
    'SrcGGX', 'SdoVrtPeca', 'SdoVrtPeso',
    'SdoVrtArM2', 'Observ', 'SerieFch',
    'Ficha', 'Misturou', 'FornecMO',
    'CustoMOKg', 'CustoMOTot', 'ValorMP',
    'DstMovID', 'DstNivel1', 'DstNivel2',
    'DstGGX', 'QtdGerPeca', 'QtdGerPeso',
    'QtdGerArM2', 'QtdGerArP2', 'QtdAntPeca',
    'QtdAntPeso', 'QtdAntArM2', 'QtdAntArP2',
    'AptoUso', 'NotaMPAG', 'Marca',
    'TpCalcAuto'], [
    'Controle'], [
    Codigo, MovimCod, MovimNiv,
    MovimTwn, Empresa, Terceiro,
    CliVenda, MovimID, LnkNivXtr1,
    LnkNivXtr2, DataHora, Pallet,
    GraGruX, Pecas, PesoKg,
    AreaM2, AreaP2, ValorT,
    SrcMovID, SrcNivel1, SrcNivel2,
    SrcGGX, SdoVrtPeca, SdoVrtPeso,
    SdoVrtArM2, Observ, SerieFch,
    Ficha, Misturou, FornecMO,
    CustoMOKg, CustoMOTot, ValorMP,
    DstMovID, DstNivel1, DstNivel2,
    DstGGX, QtdGerPeca, QtdGerPeso,
    QtdGerArM2, QtdGerArP2, QtdAntPeca,
    QtdAntPeso, QtdAntArM2, QtdAntArP2,
    AptoUso, NotaMPAG, Marca,
    TpCalcAuto], [
    Controle], True) then
    begin
      Controle  := QrVSMovItsControle.Value;
      MovimNiv  := Integer(TEstqMovimNiv.eminBaixCurtiVS);
      DstMovID  := 0;
      DstNivel1 := 0;
      DstNivel2 := 0;
      DstGGX    := 0;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'DstMovID', 'DstNivel1', 'DstNivel2',
      'DstGGX', 'MovimNiv', 'MovimTwn'], ['Controle'], [
      DstMovID, DstNivel1, DstNivel2,
      DstGGX, MovimNiv, MovimTwn], [Controle], False);
    end;
  end;
}
var
  Itens: Integer;
begin
  PageControl1.ActivePageIndex := 0;
  //
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //EXIT;
(*
  VS_PF.AtualizaOrigensReclasseSemPallet(PB1, LaAviso1, LaAviso2);
  VS_PF.AtualizaOrigensReclassePalletErrado(PB1, LaAviso1, LaAviso2);
  VS_PF.AtualizaIMEIsPalletComGraGruXErrado(PB1, LaAviso1, LaAviso2);
*)
  Itens := 0;
  Itens := Itens + VS_PF.AtualizaReclassesSemPallet(PB1, LaAviso1, LaAviso2);
  Itens := Itens + VS_PF.AtualizaOrigensReclasseSemGragruX(PB1, LaAviso1, LaAviso2);
  Itens := Itens + VS_PF.AtualizaReclassesComPalletErrado(PB1, LaAviso1, LaAviso2);
  Itens := Itens + VS_PF.AtualizaArtigosDePallets(PB1, LaAviso1, LaAviso2);
  Itens := Itens + VS_PF.AtualizaMovimCodDeVSMovCab(PB1, LaAviso1, LaAviso2);
  Geral.MB_Info(Geral.FF0(Itens) + ' itens foram corrigidos!');
  (*
  Controle := 3195;
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
    'SELECT * ',
    'FROM v s m o v i t s ',
    'WHERE Controle IN ( ',
    //'3264, 3265, 3266, 3267, 3268, 3275, 3276, ',
    '3267, ',
    //'3253, 3254, 3255, 3256, 3257, ',
    '3256, '
    '3351, 3352, 3355, ',
    '3357, 3358, 3359  ',
    ') ',
    'ORDER BY Controle DESC ',
    '']);
    //
    PB1.Position := 0;
    PB1.Max := QrVSMovIts.RecordCount;
    QrVSMovIts.First;
    while not QrVSMovIts.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      InsereAtual();
      Controle := Controle - 1;
      //
      QrVSMovIts.Next;
    end;
    Geral.MB_Aviso(Geral.FF0(QrVSMovIts.RecordCount) + ' itens foram corrigidos!');
  finally
    Screen.Cursor := crDefault;
  end;
*)
end;

procedure TFmPrincipal2.AdvGlowButton12Click(Sender: TObject);
begin
  CadastroDeSubgrupos;
end;

procedure TFmPrincipal2.AGBSintegraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSintegra, AGBSintegra);
end;

procedure TFmPrincipal2.AGBSPEDEFDEnceClick(Sender: TObject);
begin
  EfdIcmsIpi_Jan.MostraFormSPEDEFDEnce();
end;

procedure TFmPrincipal2.AdvGlowButton130Click(Sender: TObject);
(*

SELECT * FROM v s m o v i t s
WHERE Movimcod=4

SELECT vmi.*
FROM v s m o v i t s vmi
LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX
WHERE Pallet=0
AND ggx.GraGruY=3072

*)
var
  Qry1, Qry2: TmySQLQuery;
  GraGruX, SrcGGX, SrcMovID, SrcMovNiv, SrcNivel1, SrcNivel2, MovimCod,
  Controle: Integer;
  Texto: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  Texto := '';
  Qry1 := TmySQLQuery.Create(Dmod);
  Qry2 := TmySQLQuery.Create(Dmod);
  try
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI,
    'WHERE Movimcod IN (3,4) ',
    'AND MovimNiv=1 ',
    'AND ',
    ' (SrcNivel1>SrcNivel2',
    '  OR',
    '  SrcGGx=6',
    ')',
    '']);
    Qry1.First;
    while not Qry1.Eof do
    begin
      GraGruX     := Qry1.FieldByName('MovimCod').AsInteger;
      SrcGGX      := Qry1.FieldByName('MovimCod').AsInteger;
      SrcMovID    := Qry1.FieldByName('SrcGGX').AsInteger;
      SrcMovNiv   := Qry1.FieldByName('SrcMovID').AsInteger;
      //SrcNivel1   := Qry1.FieldByName('').AsInteger;
      SrcNivel2   := Qry1.FieldByName('SrcNivel1').AsInteger;
      MovimCod    := Qry1.FieldByName('SrcNivel2').AsInteger;
      Controle    := Qry1.FieldByName('Controle').AsInteger;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
      'SELECT Codigo FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(SrcNivel2),
      '']);
      SrcNivel1   := Qry2.FieldByName('Codigo').AsInteger;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'MovimCod', 'GraGruX',
      'SrcMovID', 'SrcNivel1', 'SrcNivel2',
      'SrcGGX'], [
      'Controle'], [
      MovimCod, GraGruX,
      SrcMovID, SrcNivel1, SrcNivel2,
      SrcGGX], [
      Controle], True);
      //
      Texto := Texto + Geral.FF0(Controle) + ', ';
      Qry1.Next;
    end;
    Geral.MB_Info('IMEIS mudados: ' + Texto);
  finally
    Qry2.Free;
  end;
  finally
    Qry1.Free;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton131Click(Sender: TObject);
begin
  DmNFe_0000.AtualizaXML_No_BD_Tudo(True);
end;

procedure TFmPrincipal2.AdvGlowButton132Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraImpLista, FmGraImpLista, afmoNegarComAviso) then
  begin
    FmGraImpLista.ShowModal;
    FmGraImpLista.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton133Click(Sender: TObject);
begin
  Dmod.AtualizaStqMovIts_PQX(PB1, LaAviso1, LaAviso2);
end;

procedure TFmPrincipal2.AdvGlowButton134Click(Sender: TObject);
begin
  Geral.MB_Aviso(dmkPF.PrimeiroDiaAposPeriodo(UnPQx.VerificaBalanco, dtSystem));
end;

procedure TFmPrincipal2.AdvGlowButton135Click(Sender: TObject);
begin
  Dmod.AtualizaStqMovIts_MPI(PB1);
end;

procedure TFmPrincipal2.AdvGlowButton136Click(Sender: TObject);
begin
  Dmod.AtualizaStqMovIts_CWB();
end;

procedure TFmPrincipal2.AdvGlowButton138Click(Sender: TObject);
begin
  AppPF.CorrigeReduzidosDuplicadosDeInsumo(True);
end;

procedure TFmPrincipal2.GBMPReclasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMPReclas, GBMPReclas);
end;

procedure TFmPrincipal2.GBMPSubProdClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPSubProd, FmMPSubProd, afmoNegarComAviso) then
  begin
    FmMPSubProd.ShowModal;
    FmMPSubProd.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton139Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPClasMulGer, FmMPClasMulGer, afmoNegarComAviso) then
  begin
    FmMPClasMulGer.ShowModal;
    FmMPClasMulGer.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton13Click(Sender: TObject);
begin
  CadastroDeContas;
end;

procedure TFmPrincipal2.AdvGlowButton140Click(Sender: TObject);
var
  Nivel1, Nivel2, PrdGrupTip: Integer;
  Nome: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE gragru1');
    Dmod.QrUpd.SQL.Add('SET PrdGrupTip=-2');
    Dmod.QrUpd.SQL.Add('WHERE Nivel1 IN (');
    Dmod.QrUpd.SQL.Add('  SELECT Codigo FROM PQ');
    Dmod.QrUpd.SQL.Add(')');
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE GraGru1');
    Dmod.QrUpd.SQL.Add('SET GraTamCad=0');
    Dmod.QrUpd.SQL.Add('WHERE PrdGrupTip=-2');
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrPQ_GG1.Close;
    UnDmkDAC_PF.AbreQuery(Dmod.QrPQ_GG1, Dmod.MyDB);
    //
    while not Dmod.QrPQ_GG1.Eof do
    begin
      Nivel2     := - Dmod.QrPQ_GG1Ativo.Value;
      Nome       := Dmod.QrPQ_GG1NO_PQ.Value;
      PrdGrupTip := -2; // PQ
      Nivel1     := Dmod.QrPQ_GG1Nivel1.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
      'Nivel2', 'Nome', 'PrdGrupTip'], ['Nivel1'], [
      Nivel2, Nome, PrdGrupTip], [Nivel1], True);
      //
      Dmod.QrPQ_GG1.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton141Click(Sender: TObject);
{
var
  cab_Pedido, cab_TipoNF, cab_qVol, FatID, FatNum, Empresa, CliFor, Transporta,
  cab_Conhecimento, cab_NF, cab_modNF, cab_Serie: Integer;
  cab_Juros, cab_RICMS, cab_RICMSF, ValFrete, cab_PesoB, cab_PesoL, cab_ICMS,
  cab_ValProd, cab_Frete, cab_Seguro, cab_Desconto, cab_IPI, cab_PIS,
  cab_COFINS, cab_Outros, cab_ValorNF: Double;
  cab_Data, cab_DataE, cab_Cancelado, cab_refNFe: String;
  //
  Controle, Periodo, Conta, Insumo, its_Volumes: Integer;
  its_PesoVB, its_PesoVL, its_ValorItem, its_RIPI, its_CFin, its_Seguro,
  its_Frete, its_Desconto, its_TotalCusto, its_TotalPeso, its_IPI, its_ICMS,
  its_RICMS, PC, kg, qVol, qCom: Double;
  DataFiscal: TDateTime;
}
begin
  //FatID := VAR_FATID_0005;
  {
  / Parei Aqui!
  FatNum := UMyMod.BuscaEmLivreY_Def('mpinits', 'Conta', stIns, 0);
  //
  if UnNFeImporta.ImportaDadosNFeDeXML(FmMPIn, FatID, FatNum, Empresa, CliFor,
  Transporta, cab_qVol, cab_PesoB, cab_PesoL, DataFiscal) then
  begin
    cab_Data          := FormatDateTime('yyyy-mm-dd', Date); // Entrada
    cab_TipoNF        := 1; // Nota Fiscal Eletr�nica
    //
    cab_Pedido        := 0;
    cab_Juros         := 0;
    cab_RICMS         := 0;
    cab_RICMSF        := 0;
    cab_Conhecimento  := 0;
    cab_Cancelado     := 'V';
    //
    DmNFe_0000.ReopenQrA(FatID, FatNum, Empresa);
    //
    cab_NF      := DmNFe_0000.QrAide_nNF.Value;
    cab_dataE   := Geral.FDT(DmNFe_0000.QrAide_dEmi.Value, 1);
    cab_refNFe  := DmNFe_0000.QrAId.Value;
    cab_modNF   := DmNFe_0000.QrAide_mod.Value;
    cab_Serie   := DmNFe_0000.QrAide_serie.Value;
    cab_ICMS    := DmNFe_0000.QrAICMSTot_vICMS.Value;
    cab_ValProd := DmNFe_0000.QrAICMSTot_vProd.Value;
    cab_Frete   := DmNFe_0000.QrAICMSTot_vFrete.Value;
    cab_Seguro  := DmNFe_0000.QrAICMSTot_vSeg.Value;
    cab_Desconto:= DmNFe_0000.QrAICMSTot_vDesc.Value;
    cab_IPI     := DmNFe_0000.QrAICMSTot_vIPI.Value;
    cab_PIS     := DmNFe_0000.QrAICMSTot_vPIS.Value;
    cab_COFINS  := DmNFe_0000.QrAICMSTot_vCOFINS.Value;
    cab_Outros  := DmNFe_0000.QrAICMSTot_vOutro.Value;
    cab_ValorNF := DmNFe_0000.QrAICMSTot_vNF.Value;

    Controle := UMyMod.BuscaEmLivreY_Def('mpin', 'Controle', stIns, 0);
    Application.CreateForm(TFmMPInAdd, FmMPInAdd);
    FmMPInAdd.FPeriodo                      := 0;
    FmMPInAdd.FControle                     := Controle;
    FmMPInAdd.ImgTipo.SQLType                := stIns;
    FmMPInAdd.TPData.Date                   := DataFiscal;
    FmMPInAdd.EdClienteI.ValueVariant       := Empresa;
    FmMPInAdd.CBClienteI.KeyValue           := Empresa;
    // Deve ser antes do SeboPreDescarne
    FmMPInAdd.EdProcedencia.ValueVariant    := CliFor;
    FmMPInAdd.CBProcedencia.KeyValue        := CliFor;
    //
    if Transporta <> 0 then
    begin
      FmMPInAdd.EdTransporte.ValueVariant   := Transporta;
      FmMPInAdd.CBTransporte.KeyValue       := Transporta;
    end;
    (*
    Autom�tico?
    FmMPInAdd.EdMarca.Text                  := QrMPInMarca.Value;
    FmMPInAdd.EdLote.Text                   := QrMPInLote.Value;
    *)
    (* Manual?
    FmMPInAdd.EdCorretor.Text               := Geral.FF0(QrMPInCorretor.Value);
    FmMPInAdd.CBCorretor.KeyValue           := QrMPInCorretor.Value;
    FmMPInAdd.EdFicha.Text                  := Geral.FFT(QrMPInFicha.Value, 0, siPositivo);
    FmMPInAdd.RGTipificacao.ItemIndex       := QrMPInTipificacao.Value;
    FmMPInAdd.RGAbateTipo.ItemIndex         := QrMPInAbateTipo.Value;
    FmMPInAdd.RGAparasCabelo.ItemIndex      := QrMPInAparasCabelo.Value;
    FmMPInAdd.RGSeboPreDescarne.ItemIndex   := QrMPInSeboPreDescarne.Value;
    //
    FmMPInAdd.dmkEdComisPer.ValueVariant    := QrMPInComisPer.Value;
    FmMPInAdd.dmkEdDescAdiant.ValueVariant  := QrMPInDescAdiant.Value;
    FmMPInAdd.EdLocalEntrg.Text             := QrMPInLocalEntrg.Value;
    FmMPInAdd.MeObserv.Text                 := QrMPInObserv.Value;
    FmMPInAdd.EdCondPagto.Text              := QrMPInCondPagto.Value;
    FmMPInAdd.EdCondComis.Text              := QrMPInCondComis.Value;
    //
    FmMPInAdd.RGAnimal.ItemIndex            := QrMPInAnimal.Value;
    //
    FmMPInAdd.dmkEdRecorte_PDA.ValueVariant := QrMPInRecorte_PDA.Value;
    FmMPInAdd.dmkEdRecorte_PTA.ValueVariant := QrMPInRecorte_PTA.Value;
    FmMPInAdd.dmkEdRaspa_PTA.ValueVariant   := QrMPInRaspa_PTA.Value;
    FmMPInAdd.dmkEdPTA.ValueVariant         := QrMPInPTA.Value;
    FmMPInAdd.dmkEdFimM2.ValueVariant       := QrMPInFimM2.Value;
    FmMPInAdd.dmkEdFimP2.ValueVariant       := QrMPInFimP2.Value;
    *)
    FmMPInAdd.ShowModal;
    FControle := FmMPInAdd.FControle;
    Periodo   := FmMPInAdd.FPeriodo;
    FmMPInAdd.Destroy;
    ReopenPri(qqMPIn, FControle);
    //
    if FControle = 0 then
    begin
      ExcluirEntrada(FatID, FatNum, Empresa, True);
    end else begin
      // ITENS
      DmNFe_0000.ReopenQrI(FatID, FatNum, Empresa);
      //
      DmNFe_0000.QrI.First;
      while not DmNFe_0000.QrI.Eof do
      begin
        if DmNFe_0000.QrI.RecNo > 1 then
          FatNum := UMyMod.BuscaEmLivreY_Def('mpinits', 'Conta', stIns, 0);
        Conta := FatNum;
        DmNFe_0000.ReopenQrN_QrS(FatID, FatNum, Empresa, DmNFe_0000.QrInItem.Value);
        //
        qCom := DmNFe_0000.QrIprod_qCom.Value;
        DmNFe_0000.QrXVol.Close;
        DmNFe_0000.QrXVol.SQL.Clear;
        DmNFe_0000.QrXVol.SQL.Add('SELECT *');
        DmNFe_0000.QrXVol.SQL.Add('FROM nfecabxvol');
        DmNFe_0000.QrXVol.SQL.Add('WHERE FatID=:P0');
        DmNFe_0000.QrXVol.SQL.Add('AND FatNum=:P1');
        DmNFe_0000.QrXVol.SQL.Add('AND Empresa=:P2');
        DmNFe_0000.QrXVol.SQL.Add('AND pesoL=:P3');
        DmNFe_0000.QrXVol.Params[00].AsInteger := FatID;
        DmNFe_0000.QrXVol.Params[01].AsInteger := FatNum;
        DmNFe_0000.QrXVol.Params[02].AsInteger := Empresa;
        DmNFe_0000.QrXVol.Params[03].AsFloat   := qCom;
        UnDmkDAC_PF.AbreQuery(DmNFe_0000.QrXVol, Dmod.MyDB);
        qVol := DmNFe_0000.QrXVolqVol.Value;
        if qVol > 0 then
        begin
          if qVol > qCom then
          begin
            kg := qVol;
            PC := qCom;
          end else begin
            kg := qCom;
            PC := qVol;
          end;
        end else begin
          kg := qVol;
          PC := qCom;
        end;
        (*
        Geral.MB_Aviso('prod_qCom = ' + FloatToStr(DmNFe_0000.QrIprod_qCom.Value) +
        sLineBreak + 'qVol = ' + FloatToStr(DmNFe_0000.QrXVolqVol.Value));
        *)

LimpaMPInRat();
Application.CreateForm(TFmMPInIts1, FmMPInIts1);
FmMPInIts1.FConta                      := Conta;
FmMPInIts1.FCodigo                     := Periodo;
FmMPInIts1.FControle                   := FControle;
FmMPInIts1.ImgTipo.SQLType              := stIns;
FmMPInIts1.FEntradaPorXML              := True;
//
FmMPInIts1.EdFornece.ValueVariant      := CliFor;
FmMPInIts1.CBFornece.KeyValue          := CliFor;
FmMPInIts1.EdCaminhao.ValueVariant     := 1;

FmMPInIts1.EdPecasNF.ValueVariant      := PC;
FmMPInIts1.EdPNF.ValueVariant          := kg;
//FmMPInIts1.EdPNF_Fat.ValueVariant      := _ITS_PNF_Fat.Value;
FmMPInIts1.EdPecas.ValueVariant        := PC;
FmMPInIts1.EdPLE.ValueVariant          := kg;
FmMPInIts1.EdPDA.ValueVariant          := 0;
FmMPInIts1.EdCMPValor.ValueVariant     := DmNFe_0000.QrIprod_vProd.Value;
FmMPInIts1.EdCMPFrete.ValueVariant     := 0;
//
FmMPInIts1.EdNF.ValueVariant           := DmNFe_0000.QrAide_nNF.Value;
FmMPInIts1.EdConheci.ValueVariant      := 0;
FmMPInIts1.TPEmissao.Date              := DmNFe_0000.QrAide_dEmi.Value;
FmMPInIts1.TPVencto.Date               := DmNFe_0000.QrAide_dEmi.Value;
FmMPInIts1.TPEmisFrete.Date            := DmNFe_0000.QrAide_dEmi.Value;
FmMPInIts1.TPVctoFrete.Date            := DmNFe_0000.QrAide_dEmi.Value;
//
(* ??? Presta��o de servi�o ????
FmMPInIts1.EdPS_QtdTot.ValueVariant    := 0;
FmMPInIts1.EdPS_ValTot.ValueVariant    := 0;
FmMPInIts1.EdPS_ValUni.ValueVariant    := 0;
*)
//
FmMPInIts1.CkNF_Inn_Fut.Checked        := False;
//
FmMPInIts1.ShowModal;
FmMPInIts1.Destroy;
        //
        DmNFe_0000.QrI.Next;
      end;
    end;
  end;
  ReopenPRi(qqMPIn, 0);
}
end;

procedure TFmPrincipal2.AdvGlowButton142Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqInnCad, FmStqInnCad, afmoNegarComAviso) then
  begin
    FmStqInnCad.ShowModal;
    FmStqInnCad.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton143Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton144Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o desabilitada. Avise a DERMATEK');
  //DModG.AtualizaPrecosGraGruVal2_All(PB1, Empresa?);
end;

procedure TFmPrincipal2.AdvGlowButton146Click(Sender: TObject);
begin
  AtualizaBalancoDePQNoSMI(PB1);
end;

procedure TFmPrincipal2.AdvGlowButton147Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE cmptinn SET ');
  DMod.QrUpd.SQL.Add('GraGruX=-201, TipoNF=1');
  DMod.QrUpd.SQL.Add('WHERE GraGruX=0;');
  DMod.QrUpd.ExecSQL;
  //
  DMod.QrAux.Close;
  DMod.QrAux.SQL.Clear;
  DMod.QrAux.SQL.Add('SELECT Codigo');
  DMod.QrAux.SQL.Add('FROM cmptout');
  DMod.QrAux.SQL.Add('WHERE JaAtz=0');
  UnDmkDAC_PF.AbreQuery(DMod.QrAux, Dmod.MyDB);
  PB1.Max := Dmod.QrAux.RecordCount;
  //
  DMod.QrAux.Close;
  DMod.QrAux.SQL.Clear;
  DMod.QrAux.SQL.Add('SELECT Codigo');
  DMod.QrAux.SQL.Add('FROM cmptinn');
  DMod.QrAux.SQL.Add('WHERE JaAtz=0');
  UnDmkDAC_PF.AbreQuery(DMod.QrAux, Dmod.MyDB);
  PB1.Max := PB1.Max + Dmod.QrAux.RecordCount;
  while not DMod.QrAux.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Dmod.AtualizaEstoqueCMPT(Dmod.QrAux.FieldByName('Codigo').AsInteger, 0);
    Dmod.QrAux.Next;
  end;
  //
  DMod.QrAux.Close;
  DMod.QrAux.SQL.Clear;
  DMod.QrAux.SQL.Add('SELECT Codigo');
  DMod.QrAux.SQL.Add('FROM cmptout');
  DMod.QrAux.SQL.Add('WHERE JaAtz=0');
  UnDmkDAC_PF.AbreQuery(DMod.QrAux, Dmod.MyDB);
  PB1.Max := PB1.Max + Dmod.QrAux.RecordCount;
  while not DMod.QrAux.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Dmod.AtualizaEstoqueCMPT(0, Dmod.QrAux.FieldByName('Codigo').AsInteger);
    Dmod.QrAux.Next;
  end;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal2.AdvGlowButton148Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeExportaXML();
end;

procedure TFmPrincipal2.AdvGlowButton149Click(Sender: TObject);
begin
  MostraPediPrzCab(0);
end;

procedure TFmPrincipal2.AtualizaBalancoDePQNoSMI(PB: TProgressBar);
var
  GrupoBal: Integer;
  DataHora_I, DataHora_F: String;
  //
  Codigo, CodUsu, Empresa, PrdGrupTip, StqCenCad, CasasProd: Integer;
  Nome, Abertura, Encerrou, Data: String;
begin
  Screen.Cursor := crHourGlass;
  try
    QrBalancos.Close;
    UnDmkDAC_PF.AbreQuery(QrBalancos, Dmod.MyDB);
    if PB <> nil then
    begin
      PB.Position := 0;
      PB.Max := QrBalancos.RecordCount;
    end;
    DataHora_I := Geral.FDT(0, 1);
    while not QrBalancos.Eof do
    begin
      if PB <> nil then
      begin
        PB.Position := PB.Position + 1;
        PB.Update;
      end;
      Application.ProcessMessages;
      //
      DataHora_F := dmkPF.PrimeiroDiaDoPeriodo(QrBalancosPeriodo.Value, dtSystem);
      if QrBalancosGrupoBal.Value = 0 then
      begin
        if not DModG.BuscaProximoCodigoInt_Novo('Controle', 'GrupoBal', '', 0, '', GrupoBal) then
        begin
          Screen.Cursor := crDefault;
          Exit;
        end;
      end else GrupoBal := QrBalancosGrupoBal.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE balancos SET grupobal=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE Periodo=:P1');
      Dmod.QrUpd.Params[00].AsInteger := GrupoBal;
      Dmod.QrUpd.Params[01].AsInteger := QrBalancosPeriodo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa smia, GraGruX ggx, GraGru1 gg1');
      Dmod.QrUpd.SQL.Add('SET smia.GrupoBal=:P0');
      Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smia.GraGruX');
      Dmod.QrUpd.SQL.Add('AND gg1.Nivel1=ggx.GraGru1');
      Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
      Dmod.QrUpd.SQL.Add('AND (smia.DataHora >= :P1 AND smia.DataHora < :P2)');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := GrupoBal;
      Dmod.QrUpd.Params[01].AsString  := DataHora_I;
      Dmod.QrUpd.Params[02].AsString  := DataHora_F;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsb smia, GraGruX ggx, GraGru1 gg1');
      Dmod.QrUpd.SQL.Add('SET smia.GrupoBal=:P0');
      Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smia.GraGruX');
      Dmod.QrUpd.SQL.Add('AND gg1.Nivel1=ggx.GraGru1');
      Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
      Dmod.QrUpd.SQL.Add('AND (smia.DataHora >= :P1 AND smia.DataHora < :P2)');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := GrupoBal;
      Dmod.QrUpd.Params[01].AsString  := DataHora_I;
      Dmod.QrUpd.Params[02].AsString  := DataHora_F;
      Dmod.QrUpd.ExecSQL;
      //

      //  Cadastro da balanco em StqBalCad (Grade)
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT * FROM stqbalcad');
      Dmod.QrAux.SQL.Add('WHERE Empresa=-11');
      Dmod.QrAux.SQL.Add('AND PrdGrupTip=-2');
      Dmod.QrAux.SQL.Add('AND StqCenCad=1');
      Dmod.QrAux.SQL.Add('AND Abertura=:P0');
      Dmod.QrAux.SQL.Add('');
      Dmod.QrAux.Params[0].AsString := DataHora_F;
      UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
      if Dmod.QrAux.RecordCount = 0 then
      begin
        Nome := 'Balan�o de Uso e Consumo de ' + Geral.Maiusculas(
          dmkPF.PrimeiroDiaDoPeriodo(QrBalancosPeriodo.Value, dtTexto), False);
        Empresa    := -11;
        PrdGrupTip := -2; // Produtos qu�micos
        StqCenCad  := 1;
        CasasProd  := 3;
        Abertura   := dmkPF.PrimeiroDiaDoPeriodo(QrBalancosPeriodo.Value, dtSystem);
        Encerrou   := Abertura;
        Codigo     := UMyMod.BuscaEmLivreY_Def('stqbalcad', 'Codigo', stIns, 0);
        CodUsu     := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux,
          'StqBalCad', 'CodUsu', [], [], stIns, 0, siPositivo, nil);
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqbalcad', False, [
        'CodUsu', 'Nome', 'Empresa',
        'PrdGrupTip', 'StqCenCad', 'CasasProd',
        'Abertura', 'Encerrou', 'GrupoBal'], [
        'Codigo'], [
        CodUsu, Nome, Empresa,
        PrdGrupTip, StqCenCad, CasasProd,
        Abertura, Encerrou, GrupoBal], [
        Codigo], True);
      end;
      //
      DataHora_I := DataHora_F;
      QrBalancos.Next;
    end;
    //
    //  Colocar itens do StqMovItsA no StqMovItsB
    Data := dmkPF.PrimeiroDiaAposPeriodo(UnPQx.VerificaBalanco, dtSystem);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('REPLACE INTO stqmovitsb');
  (*
    Dmod.QrUpd.SQL.Add('SELECT smia.*');
    Dmod.QrUpd.SQL.Add('FROM pqx pqx');
    Dmod.QrUpd.SQL.Add('LEFT JOIN stqmovitsa smia');
    Dmod.QrUpd.SQL.Add('  ON smia.ParCodi=pqx.StqMovIts');
    Dmod.QrUpd.SQL.Add('  AND smia.ParTipo=1');
    Dmod.QrUpd.SQL.Add('WHERE pqx.Insumo > 0');
    Dmod.QrUpd.SQL.Add('AND smia.IDCtrl IS NOT NULL');
    Dmod.QrUpd.SQL.Add('AND pqx.DataX < :P0');
  *)
    Dmod.QrUpd.SQL.Add('SELECT smia.*');
    Dmod.QrUpd.SQL.Add('FROM stqmovitsa smia');
    Dmod.QrUpd.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX');
    Dmod.QrUpd.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
    Dmod.QrUpd.SQL.Add('WHERE gg1.PrdGrupTip=-2');
    Dmod.QrUpd.SQL.Add('AND smia.DataHora < :P0');
    Dmod.QrUpd.Params[0].AsString := Data;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
  (*
    Dmod.QrUpd.SQL.Add('DELETE smia FROM stqmovitsa smia, pqx pqx');
    Dmod.QrUpd.SQL.Add('WHERE smia.ParCodi=pqx.StqMovIts');
    Dmod.QrUpd.SQL.Add('AND smia.ParTipo=1');
    Dmod.QrUpd.SQL.Add('AND smia.IDCtrl IS NOT NULL');
    Dmod.QrUpd.SQL.Add('AND pqx.DataX < :P0');
  *)
    Dmod.QrUpd.SQL.Add('DELETE smia ');
    Dmod.QrUpd.SQL.Add('FROM stqmovitsa smia, gragrux ggx, gragru1 gg1');
    Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smia.GraGruX');
    Dmod.QrUpd.SQL.Add('AND gg1.Nivel1=ggx.GraGru1');
    Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
    Dmod.QrUpd.SQL.Add('AND smia.DataHora < :P0');
    Dmod.QrUpd.Params[0].AsString := Data;
    Dmod.QrUpd.ExecSQL;
    //
  (*
    Dmod.QrMaxPerBal.Close;
    UnDmkDAC_PF.AbreQuery(Dmod.QrMaxPerBal, Dmod.MyDB);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa smib, gragrux ggx, gragru1 gg1');
    Dmod.QrUpd.SQL.Add('SET smib.GrupoBal=:P0');
    Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smib.GraGruX');
    Dmod.QrUpd.SQL.Add('AND smib.GrupoBal=0');
    Dmod.QrUpd.SQL.Add('AND gg1.Nivel1=ggx.GraGru1');
    Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
    Dmod.QrUpd.SQL.Add('AND smib.DataHora < :P1');
    Dmod.QrUpd.Params[00].AsInteger := Dmod.QrMaxPerBalGrupoBal.Value;
    Dmod.QrUpd.Params[01].AsString  := Data;
    Dmod.QrUpd.ExecSQL;
  *)
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton14Click(Sender: TObject);
begin
  ImpressaoDoPlanoDeContas;
end;

procedure TFmPrincipal2.AdvGlowButton150Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmProsoft1, FmProsoft1, afmoNegarComAviso) then
  begin
    FmProsoft1.ShowModal;
    FmProsoft1.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton151Click(Sender: TObject);
begin
  Application.CreateForm(TFmLeDataArq, FmLeDataArq);
  FmLeDataArq.ShowModal;
  FmLeDataArq.Destroy;
end;

procedure TFmPrincipal2.AdvGlowButton152Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmLoadXML, FmLoadXML, afmoLiberado) then
  begin
    FmLoadXML.ShowModal;
    FmLoadXML.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton153Click(Sender: TObject);
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "ERegObrig"');
(*
  Application.CreateForm(TFmEFD_RegObrig, FmEFD_RegObrig);
  FmEFD_RegObrig.ShowModal;
  FmEFD_RegObrig.Destroy;
*)
end;

procedure TFmPrincipal2.AdvGlowButton154Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa smia, nfecaba nfea');
  Dmod.QrUpd.SQL.Add('SET smia.Baixa=0');
  Dmod.QrUpd.SQL.Add('WHERE nfea.FatID=smia.Tipo');
  Dmod.QrUpd.SQL.Add('AND nfea.FatNum=smia.OriCodi');
  Dmod.QrUpd.SQL.Add('AND nfea.Empresa=smia.Empresa');
  Dmod.QrUpd.SQL.Add('AND (nfea.ide_tpAmb=2');
  Dmod.QrUpd.SQL.Add('OR nfea.Status=101)');
{
  Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Baixa=0');
  Dmod.QrUpd.SQL.Add('WHERE IDCtrl IN');
  Dmod.QrUpd.SQL.Add('(');
  Dmod.QrUpd.SQL.Add('SELECT smia.IDCtrl');
  Dmod.QrUpd.SQL.Add('FROM stqmovitsa smia, nfecaba nfea');
  Dmod.QrUpd.SQL.Add('WHERE nfea.FatID=smia.Tipo');
  Dmod.QrUpd.SQL.Add('AND nfea.FatNum=smia.OriCodi');
  Dmod.QrUpd.SQL.Add('AND nfea.Empresa=smia.Empresa');
  Dmod.QrUpd.SQL.Add('AND (nfea.ide_tpAmb=2');
  Dmod.QrUpd.SQL.Add('OR nfea.Status=101)');
  Dmod.QrUpd.SQL.Add(')');
}
  Dmod.QrUpd.ExecSQL;
  //
  Geral.MB_Info('Atualiza��o realizada com sucesso!');
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal2.AdvGlowButton155Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFMoDocFis, FmNFMoDocFis, afmoNegarComAviso) then
  begin
    FmNFMoDocFis.ShowModal;
    FmNFMoDocFis.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton156Click(Sender: TObject);
begin
  MyObjects.CriaForm_AcessoTotal(TFmPQCorrigeFatID, FmPQCorrigeFatID);
  FmPQCorrigeFatID.ShowModal;
  FmPQCorrigeFatID.Destroy;
end;

procedure TFmPrincipal2.AdvGlowButton157Click(Sender: TObject);
begin
  Application.CreateForm(TFmLctAjustesB, FmLctAjustesB);
  FmLctAjustesB.ShowModal;
  FmLctAjustesB.Destroy;
end;

procedure TFmPrincipal2.AdvGlowButton158Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  Screen.Cursor := crHourGlass;
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE lct0001a');
  DMod.QrUpd.SQL.Add('SET FatID=FatID+1000');
  DMod.QrUpd.SQL.Add('WHERE FatID in (1,2,3,4,5,6)');
  DMod.QrUpd.SQL.Add('AND Debito > 0');
  DMod.QrUpd.ExecSQL;
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE lct0001a');
  DMod.QrUpd.SQL.Add('SET FatID=FatID+1000');
  DMod.QrUpd.SQL.Add('WHERE FatID in (7,8,13,14)');
  DMod.QrUpd.SQL.Add('AND Credito > 0');
  DMod.QrUpd.ExecSQL;
  Screen.Cursor := crDefault;
  //
  DmProd.CorrigeFatID_Venda(PB1, LaAviso1, LaAviso2);
  //
  Geral.MB_Info('Atualiza��o finalizada!');
end;

procedure TFmPrincipal2.AdvGlowButton159Click(Sender: TObject);
begin
  // bt lcts. sem cli int aba ferramentas - correcoes
  if DModG.SelecionaEmpresa(sllNenhum, True) then
    DmodFin.ExisteLancamentoSemCliInt(VAR_TAB_LCT_SEL);
end;

procedure TFmPrincipal2.AdvGlowButton15Click(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal2.AdvGlowButton160Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLoad_Inn();
end;

procedure TFmPrincipal2.AdvGlowButton161Click(Sender: TObject);
begin
  ConsumoJan.MostraLeiGer();
end;

procedure TFmPrincipal2.AdvGlowButton162Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Aviso: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT Codigo ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=27 ',
    'AND QtdAntPeca > -Pecas ',
    'AND QtdAntPeca>0 ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Aviso := 'Erros foram encontrados nos seguintes IDs:' + sLineBreak;
      Qry.First;
      while not Qry.Eof do
      begin
        Aviso := Aviso + Geral.FF0(Qry.FieldByName('Codigo').AsInteger) + sLineBreak;
        //
        Qry.Next;
      end;
    end else
      Aviso := 'N�o foram encontrados erros! ';
    //
    Geral.MB_Info(Aviso);
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton163Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPMOCab(0);
end;

procedure TFmPrincipal2.AdvGlowButton164Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal2.AdvGlowButton165Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal2.AdvGlowButton166Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal2.AdvGlowButton167Click(Sender: TObject);
begin
  dmkWeb.AbrirAppAcessoRemoto;
end;

procedure TFmPrincipal2.AdvGlowButton168Click(Sender: TObject);
begin
  Application.CreateForm(TForm4, Form4);
  Form4.ShowModal;
  Form4.Destroy;
end;

procedure TFmPrincipal2.AdvGlowButton169Click(Sender: TObject);
var
  Lista: array of Integer;
  K, I, J, A, N: Integer;
  JaFoi: Boolean;
  T1, T2: Cardinal;
begin
  GBAvisos1.Visible := True;
  //
  Screen.Cursor := crHourGlass;
  try
    I := 0;
    K := 0;
    N := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(QrTeste, DModG.MyPID_DB, [
    'SELECT Controle, SrcNivel2  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE Controle IN ( ',
    ' SELECT DISTINCT JmpNivel2 ',
    '  FROM  _seii_imecs_ _si ',
    ') ',
    '']);
    T1 := GetTickCount;
    PB1.Max := 100;
    PB1.Position := 0;
    for J := 1 to 100 do
    begin
      SetLength(Lista, 0);
      K := 0;
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      QrTeste.First;
      while not QrTeste.Eof do
      begin
        A := QrTeste.FieldByName('SrcNivel2').AsInteger;
        JaFoi := False;
        //
        for I := 0 to K-1 do
        begin
          if Lista[I] = A then
          begin
            N := N + 1;
            JaFoi := True;
            Break;
          end;
        end;
        if JaFoi = False then
        begin
          K := K + 1;
          SetLength(Lista, K);
          Lista[K-1] := A;
        end;
        //
        QrTeste.Next;
      end;
    end;
    T2 := GetTickCount;
    Geral.MB_Info('N = ' + Geral.FF0(N) + sLineBreak +
    'Tempo: ' + FloatToStr((T2-T1) / 1000) + ' s');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal2.AGBAtzArtigosPalletsClick(Sender: TObject);
begin
  if Geral.MB_Pergunta('Deseja realmente atualizar os artigos de pallets?' +
    sLineBreak +
    'ATEN��O: Este procedimento poder� demorar v�rios minutos!') = ID_YES
  then
    VS_PF.AtualizaArtigosDePallets(PB1, LaAviso1, LaAviso2);
end;

procedure TFmPrincipal2.AdvGlowButton16Click(Sender: TObject);
begin
  CadastroDeCambio();
end;

procedure TFmPrincipal2.AdvGlowButton170Click(Sender: TObject);
begin
  //VS_Jan.ImprimeVSImpMapaDefei();
end;

procedure TFmPrincipal2.AdvGlowButton171Click(Sender: TObject);
begin
    Application.CreateForm(TFmVSClassifOneDefei, FmVSClassifOneDefei);
    FmVSClassifOneDefei.ShowModal;
    FmVSClassifOneDefei.Destroy;
end;

procedure TFmPrincipal2.AdvGlowButton17Click(Sender: TObject);
begin
  CadastroPQ(0);
end;

procedure TFmPrincipal2.AdvGlowButton18Click(Sender: TObject);
begin
  CadastroGruposQuimicos();
end;

procedure TFmPrincipal2.AGBCorrigeFatIDClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  DmProd.CorrigeFatID_Venda(PB1, LaAviso1, LaAviso2);
end;

procedure TFmPrincipal2.AGBCorrigeNFesIMIsClick(Sender: TObject);
var
  Qry: TmySQLQuery;
  //
  procedure AtualizaTabela(MovimID: TEstqMovimID; Origens, Destinos: array of TEstqMovimNiv);
  var
    TabCab, FldCabSer, FldCabNum, Erros: String;
    NFeSer1, NFeNum1, NFeSer2, NFeNum2, VSMulNFeCab, Codigo, MovimCod: Integer;

  begin
    TabCab := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
    VS_PF.ObtemNomeCamposNFeVSXxxCab(MovimID, FldCabSer, FldCabNum);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.Codigo, cab.MovimCod, ',
    'cab.' + FldCabSer + ' Ser1, cab.' + FldCabNum + ' Num1,',
    'nfe.ide_serie Ser2, nfe.ide_nNF Num2  ',
    'FROM ' + TabCab + ' cab ',
    'LEFT JOIN vsoutnfecab nfe ON nfe.MovimCod=cab.MovimCod ',
    ' AND nfe.NFeStatus=100  ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Erros := '';
    while not Qry.Eof do
    begin
      NFeSer1   := Qry.FieldByName('Ser1').AsInteger;
      NFeNum1   := Qry.FieldByName('Num1').AsInteger;
      Codigo   := Qry.FieldByName('Codigo').AsInteger;
      MovimCod := Qry.FieldByName('MovimCod').AsInteger;
      //
      if NFeNum1 = 0 then
      begin
        NFeSer2 := Qry.FieldByName('Ser2').AsInteger;
        NFeNum2:= Qry.FieldByName('Num2').AsInteger;
        //
        if NFeNum2 <> 0 then
        begin
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabCab, False, [
          FldCabSer, FldCabNum], [
          'Codigo'], [
          NFeSer2, NFeNum2], [
          Codigo], True) then ;
        end;
      end;
      if (NFeNum1 = 0) and (NFeNum2 = 0) then
        Erros := Erros + Geral.FF0(Qry.FieldByName('Codigo').AsInteger) + ' | '
      else
        DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
          MovimCod, MovimID, Origens, Destinos);
      //
      Qry.Next;
    end;
    if Trim(Erros)<> '' then
      Geral.MB_Aviso(
      'Os c�digos abaixo da tabela ' + TabCab + ' n�o tem NFe definida!' +
      sLineBreak + Erros);
    Erros := '';
  end;
begin
  Qry := tMySQLQuery.Create(Dmod);
  try
    // 1 - Entrada in natura
    AtualizaTabela(TEstqMovimID.emidCompra, [(*s� por info de NFe*)], [eminSemNiv]);
    // 2 - Saida por venda
    AtualizaTabela(TEstqMovimID.emidVenda, [(*s� por info de NFe*)], [eminSemNiv]);
    //
    // 16 - Compra WB
    AtualizaTabela(TEstqMovimID.emidEntradaPlC, [(*s� por info de NFe*)], [eminSemNiv]);
    //
    // 21 - Devolucao definitiva
    AtualizaTabela(TEstqMovimID.emidDevolucao, [(*s� por info de NFe*)], [eminSemNiv]);
    // 22 - Devolucao para retrabalho
    AtualizaTabela(TEstqMovimID.emidRetrabalho, [(*s� por info de NFe*)], [eminSemNiv]);
    // 25 - Transferencia
    AtualizaTabela(TEstqMovimID.emidTransfLoc, [eminSorcLocal], [eminDestLocal]);
    //
////////////////////////////////////////////////////////////////////////////////
    // F A S E   2
////////////////////////////////////////////////////////////////////////////////
    // 26 - Caleiro - Falta Fazer !!!!!!!!!!
    //AtualizaTabela(TEstqMovimID.emidEmProCal, [emin?], [emin?]);
    // 27 - Curtimento - Falta Fazer !!!!!!!!!!
    //AtualizaTabela(TEstqMovimID.emidEmProCur, [emin?], [emin?]);
    // 11 - Operacao - Falta Fazer !!!!!!!!!!
    AtualizaTabela(TEstqMovimID.emidEmOperacao, [eminSorcOper],
    [eminEmOperInn, eminDestOper, eminEmOperBxa]);
     //19 - Recurtimento - Falta Fazer !!!!!!!!!!
    AtualizaTabela(TEstqMovimID.emidEmProcWE, [eminSorcWEnd],
    [eminEmWEndInn, eminDestWEnd, eminEmWEndBxa]);
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal2.AGBApurICMSIPIClick(Sender: TObject);
begin
  //EfdIcmsIpi_Jan.MostraFormEFD_E001();
  EfdIcmsIpi_Jan.MostraFormEfdIcmsIpiE001();
end;

procedure TFmPrincipal2.AGBConfEstqClick(Sender: TObject);
begin
  if DmodG.QrDonoCNPJ_CPF.Value = '02717861000110' then //Apenas o SoftCouro at� finalizar o Sped depois remover
  begin
    if DBCheck.CriaFm(TFmSPEDEstqGer, FmSPEDEstqGer, afmoNegarComAviso) then
    begin
      FmSPEDEstqGer.FImporExpor := 2;
      FmSPEDEstqGer.FExpImpTXT := FormatFloat('0', FmSPEDEstqGer.FImporExpor);
      FmSPEDEstqGer.ShowModal;
      FmSPEDEstqGer.Destroy;
    end;
  end;
end;

procedure TFmPrincipal2.AGBConsLeitClick(Sender: TObject);
begin
  ConsumoGerlJan.MostraCons(0);
end;

procedure TFmPrincipal2.AGBConsultaNFeClick(Sender: TObject);
begin
  ConsumoJan.MostraLeiGer();
end;

procedure TFmPrincipal2.AGBContingenciaClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCntngnc();
end;

procedure TFmPrincipal2.AGBEntraMPClick(Sender: TObject);
begin
  Grade_Jan.CriaFormEntradaCab(tnfMateriaPrima, True, 0);
end;

procedure TFmPrincipal2.MostraPQE(Codigo: Integer);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if DBCheck.CriaFm(TFmPQE, FmPQE, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPQE.LocCod(Codigo, Codigo);
    FmPQE.ShowModal;
    FmPQE.Destroy;
  end;
end;

procedure TFmPrincipal2.AGBEntraUCClick(Sender: TObject);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
    begin
      MyObjects.MostraPopUpDeBotao(PMEntraUC, AGBEntraUC);
    end;
    fggxPQ:
    begin
}
  MostraPQE(0);
{
    end;
  end;
}
end;

procedure TFmPrincipal2.AGBImportaNFeWebClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLoad_Web();
end;

procedure TFmPrincipal2.AGBImprimeVSEmProcBHClick(Sender: TObject);
begin
  VS_PF.ImprimeVSEmProcBH();
end;

procedure TFmPrincipal2.AGBInfoIncCabClick(Sender: TObject);
begin
  Eiia_PF.MostraFormInfoIncCab(0);
end;

procedure TFmPrincipal2.AdvGlowButton1Click(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal2.AdvGlowButton20Click(Sender: TObject);
begin
  MostraFormWBOutIts(stIns, 0, nil, nil);
end;

procedure TFmPrincipal2.AdvGlowButton21Click(Sender: TObject);
begin
  MostraFormBalancoPQ();
end;

procedure TFmPrincipal2.AdvGlowButton22Click(Sender: TObject);
begin
  CadastrodeEspessuras();
end;

procedure TFmPrincipal2.AdvGlowButton23Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPClas, FmMPClas, afmoNegarComAviso) then
  begin
    FmMPClas.ShowModal;
    FmMPClas.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton24Click(Sender: TObject);
begin
  CadastroDeArtigosGrupos();
end;

procedure TFmPrincipal2.AdvGlowButton25Click(Sender: TObject);
begin
  CadastroBancos();
end;

procedure TFmPrincipal2.AdvGlowButton26Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPGrup, FmMPGrup, afmoNegarComAviso) then
  begin
    FmMPGrup.ShowModal;
    FmMPGrup.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton27Click(Sender: TObject);
begin
  CadastroDeMPEstagios();
end;

procedure TFmPrincipal2.MostraFormEmitGru(Codigo: Integer);
begin
(*
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'emitgru', 60, ncGerlSeq1,
  'Cadastro de Grupo de Emiss�o',
  [], False, Null, [], [], False);
*)
  if DBCheck.CriaFm(TFmEmitGru, FmEmitGru, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEmitGru.LocCod(Codigo, Codigo);
    FmEmitGru.ShowModal;
    FmEmitGru.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton2Click(Sender: TObject);
begin
  MostraFormEmitGru(0);
end;

procedure TFmPrincipal2.AdvGlowButton30Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCashBal, FmCashBal, afmoNegarComAviso) then
  begin
    FmCashBal.ShowModal;
    FmCashBal.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton31Click(Sender: TObject);
begin
  CadastroMatriz();
end;

procedure TFmPrincipal2.AdvGlowButton33Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton35Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeJust();
end;

procedure TFmPrincipal2.AGBLaySPEDEFDClick(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Ser�o baixadas e atualizadas as tabelas do layout do SPED EFD.' +
  'Tem certeza que deseja continuar?') =
  ID_YES then
  begin
    Memo3.Visible := True;
    Memo3.Lines.Clear;
    EfdIcmsIpi_PF.BaixaLayoutSPED(Memo3);
    Memo3.Visible := False;
  end;
end;

procedure TFmPrincipal2.AGBMPRecebImpClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPRecebImp, FmMPRecebImp, afmoNegarComAviso) then
  begin
    FmMPRecebImp.ShowModal;
    FmMPRecebImp.Destroy;
  end;
end;

procedure TFmPrincipal2.AGBNewFinMigraClick(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Deseja alterar todos "' + VAR_LCT + '" de "CliInt" > 0 para "CliInt" = -11 ?') =
  ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE ' + VAR_LCT + ' SET CliInt=-11 ',
      'WHERE CliInt > 0 ',
      'AND Controle <> 0 ',
      '']);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  DmLct2.MigraLctsParaTabLct();
end;

procedure TFmPrincipal2.AGBNFeConsCadClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormStepsNFe_ConsultaCadastroEntidade();
end;

procedure TFmPrincipal2.AGBNFeDesDowCClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCnfDowC_0100();
end;

procedure TFmPrincipal2.AGBNFeDestClick(Sender: TObject);
begin
  // Deprecado e nunca usado!
  //UnNFe_PF.MostraFormNFeDesConC_0101();
  // Substituto
  UnNFe_PF.MostraFormNFeDistDFeInt();
end;

procedure TFmPrincipal2.AGBNFeEventosClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeEveRLoE(0);
end;

procedure TFmPrincipal2.AGBNFeExportaXML_BClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeExportaXML_B();
end;

procedure TFmPrincipal2.AGBOCsClick(Sender: TObject);
begin
  VS_PF.MostraFormVSOCGerCab(0);
end;

procedure TFmPrincipal2.AGBOpcoesGradClick(Sender: TObject);
begin
  Grade_Jan.MostraFormOpcoesGrad();
end;

procedure TFmPrincipal2.AGBPararClick(Sender: TObject);
begin
  FParar := True;
end;

procedure TFmPrincipal2.AGBPQRAjuInnClick(Sender: TObject);
begin
(*  DESABILITADO EM 2016-06-09 - Nova forma de devlver PQ pelo pqw!
  if DBCheck.CriaFm(TFmPQRAjuInn, FmPQRAjuInn, afmoNegarComAviso) then
  begin
    FmPQRAjuInn.ShowModal;
    FmPQRAjuInn.Destroy;
  end;
*)
end;

procedure TFmPrincipal2.AGBPQRAjuOutClick(Sender: TObject);
begin
(*  DESABILITADO EM 2016-06-09 - Nova forma de devlver PQ pelo pqw!
  if DBCheck.CriaFm(TFmPQRAjuOut, FmPQRAjuOut, afmoNegarComAviso) then
  begin
    FmPQRAjuOut.ShowModal;
    FmPQRAjuOut.Destroy;
  end;
*)
end;

procedure TFmPrincipal2.AGBPQSubstituiClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPQSubstitui, FmPQSubstitui, afmoNegarComAviso) then
  begin
    FmPQSubstitui.ShowModal;
    FmPQSubstitui.Destroy;
  end;
end;

procedure TFmPrincipal2.AGBTabsSPEDEFDClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPED_EFD_Tabelas, FmSPED_EFD_Tabelas, afmoNegarComAviso) then
  begin
    FmSPED_EFD_Tabelas.ShowModal;
    FmSPED_EFD_Tabelas.Destroy;
  end;
end;

procedure TFmPrincipal2.AGBTribDefCabClick(Sender: TObject);
begin
  Tributos_PF.MostraFormTribDefCab(0);
end;

procedure TFmPrincipal2.AGBTribDefCadClick(Sender: TObject);
begin
  Tributos_PF.MostraFormTribDefCad(0);
end;

procedure TFmPrincipal2.AGBVSArtCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSArtCab(0, 0);
end;

procedure TFmPrincipal2.AGBVSBoxesClick(Sender: TObject);
begin
  VS_PF.MostraFormVSPaCRIts();
end;

procedure TFmPrincipal2.AGBVSCalCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSCalCab(0);
end;

procedure TFmPrincipal2.AGBVSCfgEqzClick(Sender: TObject);
begin
  VS_PF.MostraFormVSCfgEqzCb(0);
end;

procedure TFmPrincipal2.AGBVSCGICabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSCGICab(0);
end;

procedure TFmPrincipal2.AGBVSCOPCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSCOPCab(0);
end;

procedure TFmPrincipal2.AGBVSCPMRSBCbClick(Sender: TObject);
begin
  VS_PF.MostraFormVSCPMRSBCb();
end;

procedure TFmPrincipal2.AGBVSCurCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSCurCab(0);
end;

procedure TFmPrincipal2.AGBVSDsnCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSDsnCab(0);
end;

procedure TFmPrincipal2.AGBVSEntiMPClick(Sender: TObject);
begin
  VS_PF.MostraFormVSEntiMP(0);
end;

procedure TFmPrincipal2.AGBVSEqzCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSEqzCab(0);
end;

procedure TFmPrincipal2.AGBVSExBCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSExBCab(0, 0);
end;

procedure TFmPrincipal2.AGBVSFchRslCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSImpClaMulRMP();
end;

procedure TFmPrincipal2.AGBVSFichasClick(Sender: TObject);
const
  SerieFicha = 0;
  Ficha      = 0;
begin
  VS_PF.MostraFormVSFchGerCab(SerieFicha, Ficha);
end;

procedure TFmPrincipal2.AGBVSFinClaClick(Sender: TObject);
begin
  VS_PF.MostraFormVSFinCla(0, False, nil);
end;

procedure TFmPrincipal2.AGBVSGeraArtCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSGeraArt(0, 0);
end;

procedure TFmPrincipal2.AGBVSInnCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSInnCab(0, 0, 0);
end;

procedure TFmPrincipal2.AGBVSMotivBxaClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMotivBxa();
end;

procedure TFmPrincipal2.AGBVSMovCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMovCab(0);
end;

procedure TFmPrincipal2.AGBVSMovimIDClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMovimID(TEstqMovimID.emidAjuste);
end;

procedure TFmPrincipal2.AGBVSMovImpClick(Sender: TObject);
begin
  if Screen.Height <= 768 then
    VS_PF.MostraFormVSMovImp(False, nil, nil, -1)
  else
  if Sender is TMenuItem then
    VS_PF.MostraFormVSMovImp(False, nil, nil, -1)
  else
    VS_PF.MostraFormVSMovImp(True, PageControl1, AdvToolBarPager1, -1);
end;

procedure TFmPrincipal2.AGBVSMovItbClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMovItbAdd();
end;

procedure TFmPrincipal2.AGBVSMovItsClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMovIts(0);
end;

procedure TFmPrincipal2.AGBVSMrtCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMrtCad();
end;

procedure TFmPrincipal2.AGBVSNatCadClick(Sender: TObject);
begin
  VS_PF.MostraFormVSNatCad(0, False, nil);
end;

procedure TFmPrincipal2.AGBVSRibOpeClick(Sender: TObject);
begin
  VS_PF.MostraFormVSRibOpe(0, False);
end;

procedure TFmPrincipal2.AGBVSRRMCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSRRMCab(0);
end;

procedure TFmPrincipal2.AGBVSOutCabClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSOutCab(0, 0);
end;

procedure TFmPrincipal2.AGBVSOutItsClick(Sender: TObject);
begin
  VS_PF.MostraFormVSBxaCab(0, 0);
end;

procedure TFmPrincipal2.AGBVSPalletClick(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(0);
end;

procedure TFmPrincipal2.AGBVSPalStaClick(Sender: TObject);
begin
  VS_PF.MostraFormVSPalSta();
end;

procedure TFmPrincipal2.AGBVSPedCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSPedCab(0);
end;

procedure TFmPrincipal2.AGBVSPlCCabClick(Sender: TObject);
const
  Codigo   = 0;
  Controle = 0;
begin
  VS_PF.MostraFormVSPlCCab(Codigo, Controle);
end;

procedure TFmPrincipal2.AGBVSPSPCabClick(Sender: TObject);
begin
//testar!
  VS_PF.MostraFormVSPSPCab(0);
end;

procedure TFmPrincipal2.AGBVSPwdDdClick(Sender: TObject);
(*
var
  Qry: TmySQLQuery;
  VSPwdDdClas: String;
  VSPwdDdData: TDateTime;
  VSPwdDdUser: Integer;
begin
  Qry := TmySQLQuery.Create;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT VSPwdDdClas, VSPwdDdData, VSPwdDdUser ',
    'FROM controle ',
    'WHERE Codigo=1 ',
    '']);
    VSPwdDdClas := Qry.FieldByName('VSPwdDdClas').AsString;
    VSPwdDdData := Qry.FieldByName('VSPwdDdData').AsDateTime;
    VSPwdDdUser := Qry.FieldByName('VSPwdDdUser').AsInteger;
    if (VSPwdDdUser = VAR_USUARIO) or (VAR_USUARIO < 0) then
*)
begin
      VS_PF.MostraFormVSPwdDd(False);
(*
    else
      Geral.MB_Aviso('Acesso restrito pelo login!');
  finally
    Qry.Free;
  end;
*)
end;

procedure TFmPrincipal2.AGBVSPWECabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSPWECab(0);
end;

procedure TFmPrincipal2.AGBVSReqMovClick(Sender: TObject);
var
  Janela: Integer;
begin
  Janela := MyObjects.SelRadioGroup('Tipo de requisi��o',
    'Escolha o tipo de requisi��o', [
    (*00*)'RME - Requisi��o de Movimenta��o de Estoque',
    (*01*)'RDC - Requisi��o de Divis�o de Couro',
    (*02*)'Ficha de estoque - Invent�rio',
    (*03*)'IEC - Informa��o de Entrada de Couros',
    (*04*)'ICR - Informa��o de Classe/Reclasse de Couros',
    (*05*)'ISC - Informa��o de Sa�da de Couros',
    (*06*)'LSP - Lista Sequencial de Pallets',
    (*07*)'IPM - Informa��o de Pallets Movimentados'
    ], -1, 1);
  case Janela of
    0: VS_PF.MostraFormVSReqMov(0);
    1: VS_PF.MostraFormVSReqDiv(0);
    2: VS_PF.ImprimeFichaEstoque(-11, Dmod.QrMasterEm.Value, '');
    3: VS_PF.MostraFormVSInfInn(0);
    4: VS_PF.MostraFormVSInfMov(0);
    5: VS_PF.MostraFormVSInfOut(0);
    6: VS_PF.MostraFormVSLstPal(0);
    7: VS_PF.MostraFormVSInfPal(0);
  end;
end;

procedure TFmPrincipal2.AGBVSRibCadClick(Sender: TObject);
begin
  VS_PF.MostraFormVSRibCad(0, False);
end;

procedure TFmPrincipal2.AGBVSRibClaClick(Sender: TObject);
begin
  VS_PF.MostraFormVSRibCla(0, False, nil);
end;

procedure TFmPrincipal2.AGBVSSerFchClick(Sender: TObject);
begin
  VS_PF.MostraFormVSSerFch();
end;

procedure TFmPrincipal2.AGBVSSubPrdClick(Sender: TObject);
begin
  VS_PF.MostraFormVSSubPrd(0, False);
end;

procedure TFmPrincipal2.AGBVSTrfLocCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSTrfLocCab(0);
end;

procedure TFmPrincipal2.AGBVSWetEndClick(Sender: TObject);
begin
  VS_PF.MostraFormVSWetEnd(0, False, nil);
end;

procedure TFmPrincipal2.AGBWBAjsCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSAjsCab(0, 0);
end;

procedure TFmPrincipal2.AGBWBArtCabClick(Sender: TObject);
var
  Qry: TmySQLQuery;
  Ds: TDataSource;
  I, GraGruX, Codigo, Controle: Integer;
  SQLType: TSQLType;
  //
  procedure IncluiCab();
  var
    Fluxo, ReceiRecu, ReceiRefu, ReceiAcab: Integer;
    Nome, TxtMPs, Observa: String;
  begin
    SQLType        := stIns;
    Codigo         := 0;
    Nome           := Qry.FieldByName('NO_PRD_TAM_COR').AsString + ' (?)';
    Fluxo          := Qry.FieldByName('Fluxo').AsInteger;
    ReceiRecu      := Qry.FieldByName('ReceiRecu').AsInteger;
    ReceiRefu      := Qry.FieldByName('ReceiRefu').AsInteger;
    ReceiAcab      := Qry.FieldByName('ReceiAcab').AsInteger;
    TxtMPs         := Qry.FieldByName('TxtMPs').AsString;
    Observa        := Qry.FieldByName('Observa').AsString;
    //
    Codigo := UMyMod.BPGS1I32('vsartcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsartcab', False, [
    'Nome',
    'Fluxo', 'ReceiRecu', 'ReceiRefu',
    'ReceiAcab', 'TxtMPs', 'Observa'], [
    'Codigo'], [
    Nome,
    Fluxo, ReceiRecu, ReceiRefu,
    ReceiAcab, TxtMPs, Observa], [
    Codigo], True) then
  end;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, ',
    'vag.Controle, wac.*  ',
    'FROM wbartcab wac  ',
    'LEFT JOIN vsartggx vag ON wac.GraGruX=vag.GraGruX ',
    '',
    'LEFT JOIN gragrux ggx ON ggx.Controle=wac.GraGruX',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    '',
    'WHERE vag.Controle IS NULL ',
    'ORDER BY vag.DataCad, vag.GraGruX ',
    '']);
    //
    Geral.MB_Aviso('Cadastro deprecado! Use o cadastro da guia "Ribeira 1"!');
    if Qry.RecordCount > 0 then
    begin
      Ds := TDataSource.Create(Dmod);
      Ds.DataSet := Qry;
      try
        if DBCheck.EscolheCodigoUniGrid('Aviso',
        'Importa��o de configura��o de Artigo',
        'Selecione os itens que deseja cadastrar no sistema VS!', Ds, True, False) then
        begin
          if FmGerlShowGrid.DBGSel.SelectedRows.Count > 0 then
          begin
            with FmGerlShowGrid.DBGSel.DataSource.DataSet do
            for I := 0 to FmGerlShowGrid.DBGSel.SelectedRows.Count-1 do
            begin
              GotoBookmark(pointer(FmGerlShowGrid.DBGSel.SelectedRows.Items[I]));
              //
              IncluiCab();
              GraGruX := Qry.FieldByName('GraGruX').AsInteger;
              Controle := UMyMod.BPGS1I32('vsartggx', 'Controle', '', '', tsPos, SQLType, 0);
              //if
              UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsartggx', False, [
              'Codigo', 'GraGruX'], [
              'Controle'], [
              Codigo, GraGruX], [
              Controle], True);
            end;
          end;
          FmGerlShowGrid.Destroy;
        end;
      finally
        Ds.Free;
      end;
    end;
  finally
    Qry.Free;
  end;
  //////////////
(*
  if DBCheck.CriaFm(TFmWBArtCab, FmWBArtCab, afmoNegarComAviso) then
  begin
    FmWBArtCab.ShowModal;
    FmWBArtCab.Destroy;
  end;
*)
end;

procedure TFmPrincipal2.AGBWBInnCabClick(Sender: TObject);
begin
  MostraFormWBInnCab(0, 0, 0);
end;

procedure TFmPrincipal2.AGBWBMovImpClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmWBMovImp, FmWBMovImp, afmoNegarComAviso) then
  begin
    FmWBMovImp.ShowModal;
    FmWBMovImp.Destroy;
  end;
end;

procedure TFmPrincipal2.AGBWBMPrCabClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmWBMPrCab, FmWBMPrCab, afmoNegarComAviso) then
  begin
    FmWBMPrCab.ShowModal;
    FmWBMPrCab.Destroy;
  end;
end;

procedure TFmPrincipal2.AGBWBOutCabClick(Sender: TObject);
begin
  MostraFormWBOutCab(0, 0);
end;

procedure TFmPrincipal2.AGBWBPalletsClick(Sender: TObject);
begin
  MostraFormWBPallet();
end;

procedure TFmPrincipal2.AGBWBRclCabClick(Sender: TObject);
begin
  //MostraFormWBRclCab(0, 0, 0);
  VS_PF.MostraFormVSRclCab(0, 0, 0);
end;

procedure TFmPrincipal2.GBBaixaETEClick(Sender: TObject);
begin
  UnPQx.GerenciaBaixaETE(0);
end;

procedure TFmPrincipal2.GBGerenciaPesagemClick(Sender: TObject);
begin
  UnPQX.GerenciaPesagem(0);
end;

procedure TFmPrincipal2.AGBReceDespClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmReceDesp, FmReceDesp, afmoNegarComAviso) then
  begin
    FmReceDesp.ShowModal;
    FmReceDesp.Destroy;
  end;
end;

procedure TFmPrincipal2.AGBRelatUCClick(Sender: TObject);
begin
  CadastroPQImp('');
end;

procedure TFmPrincipal2.AdvGlowButton37Click(Sender: TObject);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      Geral.MB_Aviso('A��o n�o implementada!' + sLineBreak + 'Solicite � DERMATEK!');
    fggxPQ:
}
      if DBCheck.CriaFm(TFmPQPed, FmPQPed, afmoNegarComAviso) then
      begin
        FmPQPed.ShowModal;
        FmPQPed.Destroy;
      end;
{
  end;
}
end;

procedure TFmPrincipal2.AdvGlowButton38Click(Sender: TObject);
begin
  CadastroMPIn();
end;

procedure TFmPrincipal2.AdvGlowButton3Click(Sender: TObject);
begin
  CadastroPortSerBal();
end;

procedure TFmPrincipal2.AdvGlowButton40Click(Sender: TObject);
begin
  CadastroPallets();
end;

procedure TFmPrincipal2.AdvGlowButton41Click(Sender: TObject);
begin
  MostraParamsEmp();
end;

procedure TFmPrincipal2.AdvGlowButton42Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraOptEnt, FmGraOptEnt, afmoNegarComAviso) then
  begin
    FmGraOptEnt.ShowModal;
    FmGraOptEnt.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton43Click(Sender: TObject);
begin
  App_Jan.CadastroOpcoesBlueDerm();
end;

procedure TFmPrincipal2.AdvGlowButton44Click(Sender: TObject);
begin
  CadastroOpcoes;
end;

procedure TFmPrincipal2.AdvGlowButton45Click(Sender: TObject);
begin
  UnPQx.MostraFormPQMCab(0);
end;

procedure TFmPrincipal2.AdvGlowButton46Click(Sender: TObject);
begin
  MostraBackup;
end;

procedure TFmPrincipal2.AdvGlowButton49Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmServicoManager, FmServicoManager, afmoNegarComAviso) then
  begin
    FmServicoManager.ShowModal;
    FmServicoManager.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton51Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmIBGE_DTB, FmIBGE_DTB, afmoSoAdmin) then
  begin
    FmIBGE_DTB.ShowModal;
    FmIBGE_DTB.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton52Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormLayoutNFe();
end;

procedure TFmPrincipal2.AdvGlowButton55Click(Sender: TObject);
var
  FatNum, Empresa, IDCtrl: Integer;
  SQL: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  //Atualiza XML DB com �spas triplas
  try
    Screen.Cursor := crHourGlass;
    //
    SQL := '';
    //
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT FatNum, Empresa, IDCtrl, XML_NFe');
    DModG.QrAux.SQL.Add('FROM nfearq');
    DModG.QrAux.SQL.Add('WHERE XML_NFe LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_NFe LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_NFe LIKE ''%'+ '""' + '%''');
    UnDmkDAC_PF.AbreQuery(DModG.QrAux, Dmod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      DModG.QrAux.First;
      while not DModG.QrAux.Eof do
      begin
        FatNum  := DModG.QrAux.FieldByName('FatNum').AsInteger;
        Empresa := DModG.QrAux.FieldByName('Empresa').AsInteger;
        IDCtrl  := DModG.QrAux.FieldByName('IDCtrl').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfearq', False, [
          'XML_NFe'
        ], ['FatNum', 'Empresa', 'IDCtrl'],
        [
          Geral.WideStringToSQLString(DModG.QrAux.FieldByName('XML_NFe').AsWideString)
        ], [FatNum, Empresa, IDCtrl], True) then
        begin
        end;
        DModG.QrAux.Next;
      end;
    end;
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT FatNum, Empresa, IDCtrl, XML_Aut');
    DModG.QrAux.SQL.Add('FROM nfearq');
    DModG.QrAux.SQL.Add('WHERE XML_Aut LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_Aut LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_Aut LIKE ''%'+ '""' + '%''');
    UnDmkDAC_PF.AbreQuery(DModG.QrAux, Dmod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      DModG.QrAux.First;
      while not DModG.QrAux.Eof do
      begin      
        FatNum  := DModG.QrAux.FieldByName('FatNum').AsInteger;
        Empresa := DModG.QrAux.FieldByName('Empresa').AsInteger;
        IDCtrl  := DModG.QrAux.FieldByName('IDCtrl').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfearq', False, [
          'XML_Aut'
        ], ['FatNum', 'Empresa', 'IDCtrl'],
        [
          Geral.WideStringToSQLString(DModG.QrAux.FieldByName('XML_Aut').AsWideString)
        ], [FatNum, Empresa, IDCtrl], True) then
        begin
        end;
        DModG.QrAux.Next;
      end;
    end;
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT FatNum, Empresa, IDCtrl, XML_Can');
    DModG.QrAux.SQL.Add('FROM nfearq');
    DModG.QrAux.SQL.Add('WHERE XML_Can LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_Can LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_Can LIKE ''%'+ '""' + '%''');
    UnDmkDAC_PF.AbreQuery(DModG.QrAux, Dmod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      DModG.QrAux.First;
      while not DModG.QrAux.Eof do
      begin
        FatNum  := DModG.QrAux.FieldByName('FatNum').AsInteger;
        Empresa := DModG.QrAux.FieldByName('Empresa').AsInteger;
        IDCtrl  := DModG.QrAux.FieldByName('IDCtrl').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfearq', False, [
          'XML_Can'
        ], ['FatNum', 'Empresa', 'IDCtrl'],
        [
          Geral.WideStringToSQLString(DModG.QrAux.FieldByName('XML_Can').AsWideString)
        ], [FatNum, Empresa, IDCtrl], True) then
        begin
        end;
        DModG.QrAux.Next;
      end;
    end;
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT FatNum, Empresa, IDCtrl, XML_Den');
    DModG.QrAux.SQL.Add('FROM nfearq');
    DModG.QrAux.SQL.Add('WHERE XML_Den LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_Den LIKE ''%'+ '"""' + '%''');
    DModG.QrAux.SQL.Add('OR XML_Den LIKE ''%'+ '""' + '%''');
    UnDmkDAC_PF.AbreQuery(DModG.QrAux, Dmod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      DModG.QrAux.First;
      while not DModG.QrAux.Eof do
      begin
        FatNum  := DModG.QrAux.FieldByName('FatNum').AsInteger;
        Empresa := DModG.QrAux.FieldByName('Empresa').AsInteger;
        IDCtrl  := DModG.QrAux.FieldByName('IDCtrl').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfearq', False, [
          'XML_Den'
        ], ['FatNum', 'Empresa', 'IDCtrl'],
        [
          Geral.WideStringToSQLString(DModG.QrAux.FieldByName('XML_Den').AsWideString)
        ], [FatNum, Empresa, IDCtrl], True) then
        begin
        end;
        DModG.QrAux.Next;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
    Geral.MB_Info('Atualiza��o finalizada!');
  end;
end;

procedure TFmPrincipal2.AdvGlowButton56Click(Sender: TObject);
begin
  MostraPediAcc;
end;

procedure TFmPrincipal2.AdvGlowButton57Click(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
      'UPDATE gragru1 ',
      'SET prod_indEscala = "S" ',
      'WHERE prod_indEscala = "1" ',
      '']);
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
      'UPDATE gragru1 ',
      'SET prod_indEscala = "N" ',
      'WHERE prod_indEscala = "0" ',
      '']);
  finally
    Qry.Free;
  end;
  Geral.MB_Aviso('Atualizado com sucesso!');
end;

procedure TFmPrincipal2.AdvGlowButton58Click(Sender: TObject);
var
  DmLctX: TDataModule;
begin
  if UnDmkDAC_PF.CriaDataModule(FmPrincipal, TDmLct2, DmLctX) then
    TDmLct2(DmLctX).GerenciaEmpresa(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal2.AdvGlowButton59Click(Sender: TObject);
const
  Cliente = 0;
  Fornecedor = 0;
var
  DmLctX: TDataModule;
begin
  if UnDmkDAC_PF.CriaDataModule(FmPrincipal, TDmLct2, DmLctX) then
    TDmLct2(DmLctX).GerenciaEmpresa2(PageControl1, AdvToolBarPager1,
    Cliente, Fornecedor);
end;

procedure TFmPrincipal2.AdvGlowButton5Click(Sender: TObject);
begin
  CadastroEquipes();
end;

procedure TFmPrincipal2.AdvGlowButton60Click(Sender: TObject);
begin
  MostraCambioMda;
end;

procedure TFmPrincipal2.AGBCarteirasClick(Sender: TObject);
begin
  CadastroDeCarteira;
end;

procedure TFmPrincipal2.AGBCartNiv2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCartNiv2, FmCartNiv2, afmoNegarComAviso) then
  begin
    FmCartNiv2.ShowModal;
    FmCartNiv2.Destroy;
  end;
end;

procedure TFmPrincipal2.AGBClaReCoClick(Sender: TObject);
begin
  //VS_Jan.MostraFormVSLoadCRCCab1(0, 0);
  VS_Jan.MostraFormVSLoadCRCCab2(0, 0);
end;

procedure TFmPrincipal2.AGBVSOpeCabClick(Sender: TObject);
begin
  VS_PF.MostraFormVSOpeCab(0);
end;

procedure TFmPrincipal2.AdvGlowButton62Click(Sender: TObject);
begin
  AtzSdoContas;
end;

procedure TFmPrincipal2.AdvGlowButton63Click(Sender: TObject);
begin
  SaldoDeContas;
end;

procedure TFmPrincipal2.AdvGlowButton66Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqManCad, FmStqManCad, afmoNegarComAviso) then
  begin
    FmStqManCad.ShowModal;
    FmStqManCad.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton67Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmTesteChart, FmTesteChart, afmoLiberado) then
  begin
    FmTesteChart.ShowModal;
    FmTesteChart.Destroy;
  end;
end;

procedure TFmPrincipal2.AGBFatParcelaNfeCabYClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  DmNFe_0000.CorrigeFatParcelaNFeCabY(PB1, LaAviso1, LaAviso2);
end;

procedure TFmPrincipal2.Gfg(Sender: TObject);
const
  Cliente       = 0;
  CU_PediVda    = 0;
  ForcaCriarXML = False;
var
  EMP_FILIAL: Integer;
begin
  EMP_FILIAL := DmodG.QrFiliLogFilial.Value;
  //
  UnNFe_PF.MostraFormFatPedNFs(EMP_FILIAL, Cliente, CU_PediVda, ForcaCriarXML);
end;

procedure TFmPrincipal2.AGBGraGruEGerClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruEGerCab();
end;

procedure TFmPrincipal2.AGBGraGruGruXCouClick(Sender: TObject);
var
  Val: Variant;
  GraGruX: Integer;
begin
  Val := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, GraGruX, 0, 0,
  '', '', True, 'Reduzido', 'Informe o Reduzido: ', 0, Val) then
  begin
    GraGruX := Val;
    VS_Jan.MostraFormGraGruXCou(GraGruX);
  end;
end;

procedure TFmPrincipal2.AGBGraGruYClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruY(0, 0, '');
end;

procedure TFmPrincipal2.AdvGlowButton68Click(Sender: TObject);
begin
  GFat_Jan.MostraFormFatDivGer();
end;

procedure TFmPrincipal2.AdvGlowButton69Click(Sender: TObject);
begin
  GFat_Jan.MostraFormFatPedCab();
end;

procedure TFmPrincipal2.AdvGlowButton6Click(Sender: TObject);
begin
  CadastroDePlano();
end;

procedure TFmPrincipal2.AdvGlowButton70Click(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmSugVPedCab, FmSugVPedCab, afmoNegarComAviso) then
  begin
    FmSugVPedCab.ShowModal;
    FmSugVPedCab.Destroy;
  end;
*)
end;

procedure TFmPrincipal2.AdvGlowButton72Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  // N�o precisa todas, Mudar?
  DmPediVda.ReopenTabelasPedido();
  Screen.Cursor := crDefault;
  //
  if DBCheck.CriaFm(TFmPediVdaImp, FmPediVdaImp, afmoNegarComAviso) then
  begin
    FmPediVdaImp.ShowModal;
    FmPediVdaImp.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton73Click(Sender: TObject);
begin
  MostraPediPrzCab(0);
end;

procedure TFmPrincipal2.AdvGlowButton74Click(Sender: TObject);
begin
  GFat_Jan.MostraFormPediVda;
end;

procedure TFmPrincipal2.AdvGlowButton75Click(Sender: TObject);
begin
  MostraGeosite;
end;

procedure TFmPrincipal2.AdvGlowButton76Click(Sender: TObject);
begin
  MostraRegioes();
end;

procedure TFmPrincipal2.AdvGlowButton77Click(Sender: TObject);
begin
  MostraTabePrcCab;
end;

procedure TFmPrincipal2.AdvGlowButton78Click(Sender: TObject);
begin
  MostraFisRegCad;
end;

procedure TFmPrincipal2.AdvGlowButton79Click(Sender: TObject);
begin
  MostraCFOP2003();
end;

procedure TFmPrincipal2.AdvGlowButton7Click(Sender: TObject);
begin
  PQ_PF.MostraFormFormulasImpBH();
end;

procedure TFmPrincipal2.AdvGlowButton80Click(Sender: TObject);
begin
  if Sender is TMenuItem then
    UnNFe_PF.MostraFormNFePesq(False, nil, nil, 0)
  else
    UnNFe_PF.MostraFormNFePesq(True, PageControl1, AdvToolBarPager1, 0);
end;

procedure TFmPrincipal2.AdvGlowButton81Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeInut();
end;

procedure TFmPrincipal2.AdvGlowButton82Click(Sender: TObject);
begin
  Dmod.MyDB.Execute('DELETE FROM couniv1');
  Geral.MB_Aviso('Registros CouNiv1 exclu�dos!' + sLineBreak +
  'Verifique o BD para recriar os registros CouNiv1!');
end;

procedure TFmPrincipal2.AdvGlowButton83Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormStepsNFe_StepGenerico();
end;

procedure TFmPrincipal2.AdvGlowButton84Click(Sender: TObject);
begin
  //DModG.ReopenParamsEmp(Geral.IMV(VAR_LIB_EMPRESAS));
  if DBCheck.CriaFm(TFmNFaEditCSOSNAlq, FmNFaEditCSOSNAlq, afmoNegarComAviso) then
  begin
    FmNFaEditCSOSNAlq.QrParamsEmp.Locate('Codigo', DModG.QrParamsEmpCodigo.Value, []);
    FmNFaEditCSOSNAlq.ShowModal;
    FmNFaEditCSOSNAlq.Destroy;
  end;
end;

procedure TFmPrincipal2.GBValidaXMLClick(Sender: TObject);
begin
  UnNFe_PF.ValidaXML_NFe('');
end;

procedure TFmPrincipal2.AdvGlowButton85Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLEnc(0);
end;

procedure TFmPrincipal2.AdvGlowButton86Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNCMs, FmNCMs, afmoNegarComAviso) then
  begin
    FmNCMs.ShowModal;
    FmNCMs.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton87Click(Sender: TObject);
begin
  MostraModelosImp;
end;

procedure TFmPrincipal2.AdvGlowButton88Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmImportaFoxPro, FmImportaFoxPro, afmoSoMaster) then
  begin
    FmImportaFoxPro.ShowModal;
    FmImportaFoxPro.Destroy;
  end;
}
end;

procedure TFmPrincipal2.GBAbreXMLClick(Sender: TObject);
begin
  if MyObjects.CriaForm_AcessoTotal(TFmTesteXML, FmTesteXML) then
  begin
    FmTesteXML.ShowModal;
    FmTesteXML.Destroy;
  end;
end;

procedure TFmPrincipal2.GBClasFiscClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmClasFisc, FmClasFisc, afmoNegarComAviso) then
  begin
    FmClasFisc.ShowModal;
    FmClasFisc.Destroy;
  end;
end;

procedure TFmPrincipal2.GBConflitosClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraImpConflitos, FmGraImpConflitos, afmoNegarComAviso) then
  begin
    FmGraImpConflitos.ShowModal;
    FmGraImpConflitos.Destroy;
  end;
end;

procedure TFmPrincipal2.GBFabricasClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFabricas, FmFabricas, afmoNegarComAviso) then
  begin
    FmFabricas.ShowModal;
    FmFabricas.Destroy;
  end;
end;

procedure TFmPrincipal2.GBImportaNFesClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLoad_Dir();
end;

procedure TFmPrincipal2.GBImportaXMLClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLoad_Arq();
end;

procedure TFmPrincipal2.GBInfCplClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeInfCpl();
end;

procedure TFmPrincipal2.GBNatOperClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNatOper, FmNatOper, afmoNegarComAviso) then
  begin
    FmNatOper.ShowModal;
    FmNatOper.Destroy;
  end;
end;

procedure TFmPrincipal2.GBReduzidoClick(Sender: TObject);
var
  GraGruX: Variant;
  //Cod: Integer;
begin
  GraGruX := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, GraGruX, 0, 0,
  '', '', True, 'Reduzido', 'Informe o Reduzido: ', 0, GraGruX) then
  begin
    if DBCheck.CriaFm(TFmGraGruReduzido, FmGraGruReduzido, afmoSoAdmin) then
    begin
      FmGraGruReduzido.FGraGruX := GraGruX;
      FmGraGruReduzido.ReopenGraGruX();
      FmGraGruReduzido.ShowModal;
      FmGraGruReduzido.Destroy;
    end;
  end;
end;

procedure TFmPrincipal2.GBTipoMovClick(Sender: TObject);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  if DBCheck.CriaFm(TFmAtzIdeal, FmAtzIdeal, afmoSoMaster) then
  begin
    FmAtzIdeal.ShowModal;
    FmAtzIdeal.Destroy;
  end;
}
end;

procedure TFmPrincipal2.GBUsoConsCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntrada, GBUsoConsCab);
end;

procedure TFmPrincipal2.AdvGlowButton89Click(Sender: TObject);
begin
  UMedi_PF.MostraUnidMed(0);
end;

procedure TFmPrincipal2.AdvGlowButton8Click(Sender: TObject);
begin
  CadastroDeNiveisPlano();
end;

procedure TFmPrincipal2.AGBEntidadesClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal2.AdvGlowButton90Click(Sender: TObject);
const
  Esperado = '696bfa2de10ce17eaee3ea8123639867c82b8a0c';
var
  ChaveNFe, idCSRT, Crypt, Explaned, Crypted: String;
  I, J, N: Integer;
  X, Parte: WideString;
  teste: AnsiChar;
  IsOK: Boolean;
  CSRT: String;
begin
  CSRT := Dmod.MyDB.SelectString(Geral.ATS([
  'SELECT AES_DECRYPT(infRespTec_CSRT, ',
  '"' + CO_CRYPT_CSRT + '") CSRT ',
  'FROM paramsemp ',
  'WHERE Codigo=-11 ',
  '']), IsOK);

{
G8063VRTNDMO886SFNK5LDUDEI24XJ22YIPO41180678393592000146558900000006041028190697
Chave de Acesso: 41180678393592000146558900000006041028190697
CSRT: G8063VRTNDMO886SFNK5LDUDEI24XJ22YIPO
idCSRT: 01
}
  ChaveNFe := '41180678393592000146558900000006041028190697';
  UnNFe_PF.HashCSRT(-11, ChaveNFe);
  //CSRT     := 'G8063VRTNDMO886SFNK5LDUDEI24XJ22YIPO';
  idCSRT   := '01';
  //
  Crypted  := '';
  // 696bfa2de10ce17eaee3ea8123639867c82b8a0c  MySQL
  // 696bfa2de10ce17eaee3ea8123639867c82b8a0c
  if UnNFe_PF.CryptSHA1(CSRT + ChaveNFE, Crypted) then
  begin
    if Esperado = Crypted then
      Explaned := 'Valor coicide!'
    else
      Explaned := 'Valor N�O coicide!';
    //
    Geral.MB_Info(
    'ChaveNFe = ' + ChaveNFe + sLineBreak +
    'CSRT = ' + CSRT + sLineBreak +
    'idCSRT = ' + idCSRT + sLineBreak +
    'Esperado = ' + Esperado + sLineBreak +
    'Resultado = ' + Crypted + sLineBreak +
     Explaned);
     //
    N := Length(Crypted) div 2;
    X := '';
    for I := 0 to N - 1 do
    begin
      J := (I * 2) + 1;
      Parte := Crypted[J] +  Crypted[J + 1];
      //X := X + Char(dmkPF.HexToInt(Parte));
      X := X + Char(StrToInt64('$' + Parte));
    end;
    Geral.MB_Info(X);
(*
    X := Dmod.MyDB.SelectString(
    'SELECT TO_BASE64("' + X + '") Base64', IsOK, 'Base64');
    if IsOK then
      Geral.MB_Info(X)
    else
      Geral.MB_Info('ERRO na SQL!');
*)
    X := dmkPF.EncodeBase64(X);
    Geral.MB_Info('hashCSRT:  ' + X)
  end;
end;

procedure TFmPrincipal2.AdvGlowButton91Click(Sender: TObject);
begin
  CadastroDeCambio();
end;

procedure TFmPrincipal2.AdvGlowButton92Click(Sender: TObject);
begin
  VendasGerencia(True);
end;

procedure TFmPrincipal2.AdvGlowButton93Click(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmPromissoria, FmPromissoria, afmoNegarComAviso) then
  begin
    FmPromissoria.ShowModal;
    FmPromissoria.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton94Click(Sender: TObject);
begin
  VendasRelatorios;
end;

procedure TFmPrincipal2.AdvGlowButton95Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmDuplicata1, FmDuplicata1, afmoNegarComAviso) then
  begin
    FmDuplicata1.ShowModal;
    FmDuplicata1.Destroy;
  end;
end;

procedure TFmPrincipal2.AdvGlowButton96Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmPontos, FmPontos, afmoNegarComAviso) then
  begin
    FmPontos.ShowModal;
    FmPontos.Destroy;
  end;
}
end;

procedure TFmPrincipal2.AdvGlowButton97Click(Sender: TObject);
begin
  PedidosGerencia(False);
end;

procedure TFmPrincipal2.AdvGlowButton98Click(Sender: TObject);
begin
  CadastroMPIn();
end;

procedure TFmPrincipal2.AdvGlowButton99Click(Sender: TObject);
begin
  PedidosRelatorios;
end;

procedure TFmPrincipal2.AdvGlowButton9Click(Sender: TObject);
begin
  CadastroDeConjuntos();
end;

procedure TFmPrincipal2.AdvGlowMenuButton5Click(Sender: TObject);
(*
var
  Qry1: TmySQLQuery;
  //
  DataIni: String;
  Controle, GraGruX, GraGru1, GraCusPrc, Entidade, Ano: Integer;
  CustoPreco: Double;
  SQLType: TSQLType;
*)
begin
(*
  SQLType := stIns;
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT * FROM gragrux ',
    'WHERE GraGruY > 0 ',
    '']);
    Qry1.First;
    while not Qry1.Eof do
    begin
      for GraCusPrc := 1 to 8 do
      begin
        for Ano := 0 to 6 do
        begin
          GraGruX        := Qry1.FieldByName('Controle').AsInteger;
          GraGru1        := Qry1.FieldByName('GraGru1').AsInteger;
          //CustoPreco     := GraGruX / 1000 * GraCusPrc * Ano;
          CustoPreco     := (GraGruX / 100  * Ano / GraCusPrc) + 30;
          Entidade       := -11;
          DataIni        := Geral.FDT(EncodeDate(2010 + Ano, 3, 15), 1);

          //
          Controle := UMyMod.BPGS1I32('gragruval', 'Controle', '', '', tsPos, stIns, 0);
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
          'GraGruX', 'GraGru1', 'GraCusPrc',
          'CustoPreco', 'Entidade', 'DataIni'], [
          'Controle'], [
          GraGruX, GraGru1, GraCusPrc,
          CustoPreco, Entidade, DataIni], [
          Controle], True);
       end;
       //
     end;
     Qry1.Next;
  end;
  finally
    Qry1.Free;
  end;
*)
end;

procedure TFmPrincipal2.AdvPreviewMenu1Buttons0Click(Sender: TObject;
  Button: TButtonCollectionItem);
begin
  Close;
end;

procedure TFmPrincipal2.AdvPreviewMenu1Buttons1Click(Sender: TObject;
  Button: TButtonCollectionItem);
begin
  MostraBackup;
end;

procedure TFmPrincipal2.AdvPreviewMenu1Buttons2Click(Sender: TObject;
  Button: TButtonCollectionItem);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal2.AdvPreviewMenu1Buttons3Click(Sender: TObject;
  Button: TButtonCollectionItem);
begin
  VerificaBD();
end;

procedure TFmPrincipal2.AdvPreviewMenu1MenuItemClick(Sender: TObject;
  ItemIndex: Integer);
begin
  case ItemIndex of
    0: CadastroOpcoes;
    1: App_Jan.CadastroOpcoesBlueDerm();
    2: CadastroMatriz();
    3: MostraParamsEmp();
  end;
end;

procedure TFmPrincipal2.AdvToolBarButton1Click(Sender: TObject);
begin
  FmPrincipal.Enabled := False;
  //
  FmBlueDerm_dmk.Show;
  FmBlueDerm_dmk.EdLogin.Text   := '';
  FmBlueDerm_dmk.EdSenha.Text   := '';
  FmBlueDerm_dmk.EdEmpresa.Text := '';
  FmBlueDerm_dmk.EdLogin.SetFocus;
end;

procedure TFmPrincipal2.AdvToolBarButton2Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal2.AdvToolBarButton3Click(Sender: TObject);
begin
  MostraBackup;
end;

procedure TFmPrincipal2.AdvToolBarButton4Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPesqSeqPeca();
end;

procedure TFmPrincipal2.AdvToolBarButton7Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal2.AdvToolBarPager1Change(Sender: TObject);
begin
  if AdvToolBarPager1.ActivePage.Name = 'APRibeira' then
    AppPF.VerificaCadastroXxArtigoIncompleta();
end;

procedure TFmPrincipal2.AGB_SPED_ComparaClick(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmSPED_EFD_Compara, FmSPED_EFD_Compara, afmoNegarComAviso) then
  begin
    FmSPED_EFD_Compara.ShowModal;
    FmSPED_EFD_Compara.Destroy;
  end;
*)
end;

procedure TFmPrincipal2.AGB_SPED_EFD_ExpClick(Sender: TObject);
begin
  EfdIcmsIpi_Jan.MostraFormEfdIcmsIpiExporta();
end;

procedure TFmPrincipal2.AGB_SPED_EFD_ImpClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPED_EFD_Importa, FmSPED_EFD_Importa, afmoNegarComAviso) then
  begin
    FmSPED_EFD_Importa.ShowModal;
    FmSPED_EFD_Importa.Destroy;
  end;
end;

procedure TFmPrincipal2.AjustaEstiloImagem(Index: integer);
{
var
 Indice: Integer;
}
begin
{
  if Index >= 0 then Indice := Index else
  Indice := Geral.ReadAppKey('Estio_ImgPrincipal', Application.Title,
    ktInteger, 1, HKEY_LOCAL_MACHINE);
  case Indice of
    00: ImgPrincipal.Style := sbAutosize;
    01: ImgPrincipal.Style := sbCenter;
    02: ImgPrincipal.Style := sbKeepAspRatio;
    03: ImgPrincipal.Style := sbKeepHeight;
    04: ImgPrincipal.Style := sbKeepWidth;
    05: ImgPrincipal.Style := sbNone;
    06: ImgPrincipal.Style := sbStretch;
    07: ImgPrincipal.Style := sbTile;
    else ImgPrincipal.Style := sbCenter;
  end;
  ImgPrincipal.Tag       := Indice;
  Automtico1.Checked     := Indice = 00;
  Centralizado1.Checked  := Indice = 01;
  ManterProporo1.Checked := Indice = 02;
  ManterAltura1.Checked  := Indice = 03;
  ManterLargura1.Checked := Indice = 04;
  Nenhum1.Checked        := Indice = 05;
  Espichar1.Checked      := Indice = 06;
  Repetido1.Checked      := Indice = 07;
  //
  ImgPrincipal.Invalidate;
}
end;

procedure TFmPrincipal2.Antigo1Click(Sender: TObject);
begin
  MostraPQE(0);
end;

procedure TFmPrincipal2.Antigo2Click(Sender: TObject);
begin
  //VS_PF.MostraFormVSRclArtPrpOld(stIns);
end;

procedure TFmPrincipal2.AntigoPQ1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPQImp, FmPQImp, afmoNegarComAviso) then
  begin
    FmPQImp.ShowModal;
    FmPQImp.Destroy;
  end;
end;

procedure TFmPrincipal2.AppIdle(Sender: TObject; var Done: Boolean);
begin
  Done := True;
  //
  if DModG <> nil then
    DmodG.ExecutaPing(FmBlueDerm_Dmk, [Dmod.MyDB, DModG.MyPID_DB, DModG.AllID_DB]);
end;

procedure TFmPrincipal2.AtzSdoContas;
begin
  // Ver se e quando precisar
(*
  if DBCheck.CriaFm(TFmContasHistAtz2, FmContasHistAtz2, afmoNegarComAviso) then
  begin
    FmContasHistAtz2.FEntiCod := 0;
    FmContasHistAtz2.FGenero  := 0;
    FmContasHistAtz2.FPeriodo := -23999;
    //
    FmContasHistAtz2.ShowModal;
    FmContasHistAtz2.Destroy;
  end;
 *)
end;

procedure TFmPrincipal2.Automtico1Click(Sender: TObject);
begin
  Geral.WriteAppKey('Estio_ImgPrincipal', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  AjustaEstiloImagem(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal2.SBCadEntidadesClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal2.sd1FormSkin(Sender: TObject; aName: string;
  var DoSkin: Boolean);
begin
  if aName = VAR_FORMTDI_NAME then
    DoSkin := False;
end;

procedure TFmPrincipal2.Escolher2Click(Sender: TObject);
begin
  CadastroDeImpObs();
end;

procedure TFmPrincipal2.Textosdeobservaes1Click(Sender: TObject);
begin
  CadastroDeImpObs();
end;

procedure TFmPrincipal2.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmBlueDerm_dmk.Show;
  Enabled := False;
  FmBlueDerm_dmk.Refresh;
  FmBlueDerm_dmk.EdSenha.Refresh;
  FmBlueDerm_dmk.Refresh;
  //try
    Application.CreateForm(TDmod, Dmod);
{
  except
    Geral.MB_Erro('Imposs�vel criar Modulo de dados');
    Application.Terminate;
    Exit;
  end;
}
  try
    Application.CreateForm(TDmPediVda, DmPediVda);
  except
    Geral.MB_Erro('Imposs�vel criar M�dulo de vendas');
    Application.Terminate;
    Exit;
  end;
  FmBlueDerm_dmk.EdSenha.Refresh;
  FmBlueDerm_dmk.ReloadSkin;
  FmBlueDerm_dmk.EdLogin.Text := '';
  FmBlueDerm_dmk.EdSenha.Refresh;
  FmBlueDerm_dmk.EdLogin.ReadOnly := False;
  FmBlueDerm_dmk.EdSenha.ReadOnly := False;
  FmBlueDerm_dmk.EdLogin.SetFocus;
  //FmBlueDerm_dmk.ReloadSkin;
  FmBlueDerm_dmk.Refresh;

end;

procedure TFmPrincipal2.TmSuporteTimer(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    AdvToolBarButton7, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal2.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  //if Geral.MB_Pergunta('Verifica se h� nova versao?!') = ID_YES then
    if DmkWeb.RemoteConnection then
    begin
      if VerificaNovasVersoes(True) then
        DmkWeb.MostraBalloonHintMenuTopo(AdvToolBarButton2, BalloonHint1,
          'H� uma nova vers�o!', 'Clique aqui para atualizar.');
    end;
end;

procedure TFmPrincipal2.Todosnveis1Click(Sender: TObject);
begin
  CadastroDeNiveisPlano();
end;

procedure TFmPrincipal2.FornecedoresMP1Click(Sender: TObject);
begin
  CadastroEntiMP();
end;

procedure TFmPrincipal2.Fretesemretorno1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMOEnvAvuGer(0);
end;

procedure TFmPrincipal2.CadastroFluxos();
begin
  if DBCheck.CriaFm(TFmFluxosCab, FmFluxosCab, afmoNegarComAviso) then
 begin
    FmFluxosCab.ShowModal;
    FmFluxosCab.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroFormulas(Numero, Controle: Integer);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmFormulas2, FmFormulas2, afmoNegarComAviso) then
      begin
        FmFormulas2.ShowModal;
        FmFormulas2.Destroy;
      end;
    fggxPQ:
}
      if DBCheck.CriaFm(TFmFormulas, FmFormulas, afmoNegarComAviso) then
      begin
        if Numero <> 0 then
        begin
          if FmFormulas.TbFormulas.Locate('Numero', Numero, []) then
            FmFormulas.TbFormulasIts.Locate('Controle', Controle, []);
        end;
        FmFormulas.ShowModal;
        FmFormulas.Destroy;
      end;
{
  end;
}
end;

procedure TFmPrincipal2.MenuItem13Click(Sender: TObject);
begin
  VS_PF.MostraFormVSRclArtSel();
end;

procedure TFmPrincipal2.MenuItem1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal2.MenuItem6Click(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmCMPPVai, FmCMPPVai, afmoNegarComAviso) then
  begin
    DmPediVda.ReopenTabelasPedido(); // ?? N�o precisa todas. Mudar?
    FmCMPPVai.ShowModal;
    FmCMPPVai.Destroy;
  end;
*)
end;

procedure TFmPrincipal2.MenuItem7Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmCMPPVem, FmCMPPVem, afmoNegarComAviso) then
  begin
    DmPediVda.ReopenTabelasPedido(); // ?? N�o precisa todas Mudar?
    FmCMPPVem.ShowModal;
    FmCMPPVem.Destroy;
  end;
}
end;

procedure TFmPrincipal2.MenuItem8Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCECTInn, FmCECTInn, afmoNegarComAviso) then
  begin
    FmCECTInn.ShowModal;
    FmCECTInn.Destroy;
  end;
end;

procedure TFmPrincipal2.MenuItem9Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCECTOut, FmCECTOut, afmoNegarComAviso) then
  begin
    //DmPediVda.ReopenTabelasPedido(); // ?? N�o precisa todas. Mudar?
    FmCECTOut.ShowModal;
    FmCECTOut.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraBackup;
begin
  DModG.MostraBackup3();
  {
  if DBCheck.CriaFm(TFmBackup3, FmBackup3, afmoNegarComAviso) then
  begin
    FmBackup3.ShowModal;
    FmBackup3.Destroy;
  end;
  }
end;

procedure TFmPrincipal2.BaixaxestoquedeInNatura1Click(Sender: TObject);
begin
  AppPF.MostraFormCfgEstqInNatEFD(
    2(*ImporExpor*),
    EfdIcmsIpi_PF.AnoMesAnterior(),
    VAR_LIB_EMPRESA_SEL,
    0);
end;

procedure TFmPrincipal2.Balanascomunicveis1Click(Sender: TObject);
begin
  CadastroPortSerBal();
end;

procedure TFmPrincipal2.Bancos1Click(Sender: TObject);
begin
  CadastroBancos();
end;

procedure TFmPrincipal2.BitBtn1Click(Sender: TObject);
const
  chNFe = '15100404333952000188550010000000019456724199';
var
  IDCtrl: Integer;
  ID, Dir, Aviso: String;
begin
  QrCabA.Close;
  QrCabA.Params[00].AsInteger := -11;
  QrCabA.Params[01].AsString  := chNFe;
  UnDmkDAC_PF.AbreQuery(QrCabA, Dmod.MyDB);
  IDCtrl := QrCabAIDCtrl.Value;
  if IDCtrl > 0 then
  begin
    DModG.ReopenParamsEmp(-11);
    Id    := QrCabAinfProt_ID.Value;
    Dir   := DModG.QrParamsEmpDirSit.Value;
    Aviso := '';
    DmNFe_0000.AtualizaXML_No_BD_ConsultaNFe(chNFe, Id, IDCtrl, Dir, Aviso);
    if Aviso <> '' then Geral.MB_Aviso(
    'Os arquivos abaixo n�o foram localizados:' + sLineBreak + Aviso);
  end;
end;

procedure TFmPrincipal2.BitBtn2Click(Sender: TObject);
begin
  Geral.MB_Aviso(FloatToStr(Frac(1.234567 * 1000)));
end;

procedure TFmPrincipal2.BitBtn3Click(Sender: TObject);
begin
  ShowMessage('Ol� mundo!');
end;

procedure TFmPrincipal2.BtAjustePQwClick(Sender: TObject);
var
  Qual: Integer;
begin
(*
  Qual := MyObjects.SelRadioGroup('Ajuste de estoque por NF', 'Selevione o modo:',
  ['TODOS insumos', 'Somente ERRADOS'], 1, -1);
  case Qual of
    0: PQ_PF.AjustePQx_Todos();
    1: PQ_PF.AjustePQx_Errados();
  end;
*)
  PQ_PF.AtualizaTodosPQxPositivos();
end;

procedure TFmPrincipal2.BtEntiProSoftClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiLoad02, FmEntiLoad02, afmoSoAdmin) then
  begin
    FmEntiLoad02.ShowModal;
    FmEntiLoad02.Destroy;
  end;
end;

procedure TFmPrincipal2.BtImagemClick(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal2.MostraParamsEmp;
begin
  if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoBoss) then
  begin
    FmParamsEmp.ShowModal;
    FmParamsEmp.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraPediAcc;
begin
  if DBCheck.CriaFm(TFmPediAcc, FmPediAcc, afmoNegarComAviso) then
  begin
    FmPediAcc.ShowModal;
    FmPediAcc.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraPediPrzCab(Codigo: Integer);
begin
(*
  if VAR_LIB_ARRAY_EMPRESAS_CONTA > 1 then
  begin
    if DBCheck.CriaFm(TFmPediPrzCab2, FmPediPrzCab2, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmPediPrzCab2.LocCod(Codigo, Codigo);
      FmPediPrzCab2.ShowModal;
      FmPediPrzCab2.Destroy;
    end;
  end else
*)
  begin
    if DBCheck.CriaFm(TFmPediPrzCab1, FmPediPrzCab1, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmPediPrzCab1.LocCod(Codigo, Codigo);
      FmPediPrzCab1.ShowModal;
      FmPediPrzCab1.Destroy;
    end;
  end;
end;

procedure TFmPrincipal2.MostraRegioes;
begin
  if DBCheck.CriaFm(TFmRegioes, FmRegioes, afmoNegarComAviso) then
  begin
    FmRegioes.ShowModal;
    FmRegioes.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraTabePrcCab;
begin
  if DBCheck.CriaFm(TFmTabePrcCab, FmTabePrcCab, afmoNegarComAviso) then
  begin
    FmTabePrcCab.ShowModal;
    FmTabePrcCab.Destroy;
  end;
end;

procedure TFmPrincipal2.Mover_PrdGrupTip_do_GraGruY_Para_GraGrYPGT();
var
  Qry: TmySQLQuery;
  GraGruY, PrdGrupTip: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'DESCRIBE gragruy "PrdGrupTip"',
    '']);
    if Qry.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ggy.Codigo GraGruY, ggy.PrdGrupTip',
      'FROM gragruy ggy',
      'LEFT JOIN gragruypgt ygt ON ygt.GraGruY=ggy.Codigo',
      'WHERE ygt.PrdGrupTip IS NULL',
      'AND ggy.PrdGrupTip<>0',
      '']);
      //
      Qry.First;
      while not Qry.Eof do
      begin
        GraGruY    := Qry.FieldByName('GraGruY').AsInteger;
        PrdGrupTip := Qry.FieldByName('PrdGrupTip').AsInteger;
        //
        //if
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruypgt', False, [
        ], [
        'GraGruY', 'PrdGrupTip'], [
        ], [
        GraGruY, PrdGrupTip], True);
        //
        Qry.Next;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal2.Movimentaes1Click(Sender: TObject);
begin
  AppPF.MostraFormCfgMovEFD(
    2(*ImporExpor*),
    EfdIcmsIpi_PF.AnoMesAnterior(),
    VAR_LIB_EMPRESA_SEL,
    0);
end;

procedure TFmPrincipal2.N21RendimentoSemi1Click(Sender: TObject);
begin
  VS_PF.ImprimeRendimentoIMEIS([(*ImeiIni*)0], [False], [False], [], []);
end;

procedure TFmPrincipal2.N24RendimentoeMO1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSImpMOEnvRet2();
end;

procedure TFmPrincipal2.N26EstoqueCustoIntegrado1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSEstqCustoIntegr(0);
end;

procedure TFmPrincipal2.NFe1Click(Sender: TObject);
var
  NF, Empresa: Integer;
begin
  NF := MyVCLRef.GET_ValorDeObjeto_Inteiro();
  Empresa := 0; // Todas
  UnNFe_PF.PesquisaEMostraFormNFePesqEmitida(NF, Empresa);
end;

procedure TFmPrincipal2.NFeRecebida1Click(Sender: TObject);
var
  NF, Empresa: Integer;
begin
  NF := MyVCLRef.GET_ValorDeObjeto_Inteiro();
  Empresa := 0; // Todas
  UnNFe_PF.PesquisaEMostraFormNFePesqRecebida(NF, Empresa, 0);
end;

procedure TFmPrincipal2.nica1Click(Sender: TObject);
begin
  //EXIT;
  VS_PF.MostraFormVSFchRslCab(0, 0);
end;

procedure TFmPrincipal2.Nmeroainformar1Click(Sender: TObject);
var
  ValVar: Variant;
  Numero: Integer;
begin
  VAR_POPUP_ACTIVE_Numero := True;
  try
    MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, 0, 0, 0,
    '', '', True, 'C�digo', 'Informe o c�digo: ', 0, ValVar);
  finally
    VAR_POPUP_ACTIVE_Numero := False;
  end;
end;

procedure TFmPrincipal2.NovaConfiguraodeOC1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSRclArtPrpNew(stIns, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, 0, 0);
end;

procedure TFmPrincipal2.Novo1Click(Sender: TObject);
begin
  Grade_Jan.CriaFormEntradaCab(tnfUsoEConsumo, True, 0);
end;

procedure TFmPrincipal2.Novo2Click(Sender: TObject);
begin
  VS_PF.MostraFormVSRclArtPrpNew(stIns, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, 0, 0);
end;

procedure TFmPrincipal2.NovoGrade1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraImpLista, FmGraImpLista, afmoNegarComAviso) then
  begin
    FmGraImpLista.ShowModal;
    FmGraImpLista.Destroy;
  end;
end;

procedure TFmPrincipal2.IDAntes1Click(Sender: TObject);
begin
//
end;

procedure TFmPrincipal2.IdIPWatch1Status(ASender: TObject;
  const AStatus: TIdStatus; const AStatusText: string);
begin
//Pasta nao acessivel ao usuario
//C:\Arquivos de Programas\Dermatek
//Mudar ???
  IdIPWatch1.HistoryFileName := 'C:\Dermatek\iphist.dat';
  //IdIPWatch1.HistoryEnabled := False; -> se habilitar, como fica?
  IdIPWatch1.LocalIP;
end;

procedure TFmPrincipal2.IECInformaodeEntradadeCouros1Click(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmVSInfIEC, FmVSInfIEC, afmoNegarComAviso) then
  begin
    FmVSInfIEC.ShowModal;
    FmVSInfIEC.Destroy;
  end;
end;

procedure TFmPrincipal2.IMEC1Click(Sender: TObject);
begin
  VS_PF.MostroFormVSMovimCod(MyVCLRef.GET_ValorDeObjeto_Inteiro());
end;

procedure TFmPrincipal2.IMEI1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovIts(MyVCLRef.GET_ValorDeObjeto_Inteiro());
end;

procedure TFmPrincipal2.IMEIsGmeosorfos1Click(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmVSCorrigeMovimTwn, FmVSCorrigeMovimTwn, afmoNegarComAviso) then
  begin
    FmVSCorrigeMovimTwn.ShowModal;
    FmVSCorrigeMovimTwn.Destroy;
  end;
end;

procedure TFmPrincipal2.IMEIssemFornecedor1Click(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmVSCorrigeMulFrn, FmVSCorrigeMulFrn, afmoNegarComAviso) then
  begin
    FmVSCorrigeMulFrn.ShowModal;
    FmVSCorrigeMulFrn.Destroy;
  end;
end;

procedure TFmPrincipal2.ImpressaoDoPlanoDeContas;
begin
  if DBCheck.CriaFm(TFmContasSdoImp, FmContasSdoImp, afmoNegarComAviso) then
  begin
    FmContasSdoImp.ShowModal;
    FmContasSdoImp.Destroy;
  end;
end;

procedure TFmPrincipal2.ImprimeChequeAtual(ChAtu, ChMax, ChConfCab, CPI, TopoIni,
  Altura, MEsq: Integer; Dia, Mes, Ano, Vcto, Valor, Favorecido, Extenso1,
  Extenso2, Obs, Cidade: String);
var
  LinhaAtual: Integer;
  MyCPI, Texto, Espaco, Linha, FormataA, FormataI, FormataNI, FormataNF: String;
begin
  Screen.Cursor := crHourGlass;
  try
    LinhaAtual := 0;
    if ChAtu = 1 then
    begin
      AssignFile(FArqPrn, 'LPT1:');
      ReWrite(FArqPrn);
      Write(FArqPrn, #13);
      if CPI = 12 then MyCPI := 'M' else MyCPI := 'P';
      Write(FArqPrn, #27+MyCPI);
      FormataA  := #15;
      FormataI  := #15;
      FormataNI := #15;
      FormataNF := #15;
      //if (ChAtu < ChMax) then Substitui := 1 else Substitui := 0;
      if TopoIni < 0 then LinhaAtual := LinhaAtual - TopoIni else
        LinhaAtual := AvancaCarro(LinhaAtual, TopoIni);
    end;
    //////////////////////////////////////////////////////////////////////////
    QrTopos1.Close;
    QrTopos1.Params[0].AsInteger := ChConfCab;
    UnDmkDAC_PF.AbreQuery(QrTopos1, Dmod.MyDB);
    QrTopos1.First;
    while not QrTopos1.Eof do
    begin
      LinhaAtual := AvancaCarro(LinhaAtual, QrTopos1TopR.Value);
      if not FPrintedLine then
      begin
        QrView1.Close;
        QrView1.Params[0].AsInteger := ChConfCab;
        QrView1.Params[1].AsInteger := QrTopos1TopR.Value;
        UnDmkDAC_PF.AbreQuery(QrView1, Dmod.MyDB);
        QrView1.First;
        Linha := '';
        FTamLinha  := -MEsq;
        FFoiExpand := 0;
        while not QrView1.Eof do
          begin
          case QrView1FTam.Value of
            0: FormataA := #15;
            1: FormataA := #18;
            2: FormataA := #18#14;
          end;
          Espaco := EspacoEntreTextos(QrView1, CPI);
          //if QrView1Nulo.Value = 1 then Nulo := True else Nulo := False;
          //
          case QrView1Campo.Value of
            01: Texto := Valor;
            02: Texto := Extenso1;
            03: Texto := Extenso2;
            04: Texto := Favorecido;
            05: Texto := Dia;
            06: Texto := Mes;
            07: Texto := Ano;
            08: Texto := Vcto;
            09: Texto := Obs;
            10: Texto := Cidade;
            else Texto := '';
          end;
          Texto := FormataTexto(Texto, QrView1Pref.Value, QrView1Sufi.Value,
          '', QrView1PrEs.Value, 0, QrView1AliH.Value, QrView1Larg.Value,
          QrView1FTam.Value, 0(*Texto*) , CPI, True(*Nulo*));
          Linha := Linha + Espaco + FormataA + Texto;
          // J� usou expandido na mesma linha
          if QrView1FTam.Value = 2 then FFoiExpand := 100;
          QrView1.Next;
        end;
        Write(FarqPrn, #27+'3'+#0);
        //Write(FarqPrn, #27+'F');
        WriteLn(FArqPrn, Linha);
        FPrintedLine := True;
      end;
      QrTopos1.Next;
    end;
    //LinhaAtual :=
    AvancaCarro(LinhaAtual, Altura);
    //Write(FArqPrn, '_____________-------------------_________________---------------____________');
    if ChAtu = ChMax then
    begin
      Write(FArqPrn, #13);
      WriteLn(FArqPrn, #27+'0');
      //if CkEjeta.Checked then
      //Write(FArqPrn, #12);
      CloseFile(FArqPrn);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal2.Inclui1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPReclasIns, FmMPReclasIns, afmoNegarComAviso) then
  begin
    FmMPReclasIns.ImgTipo.SQLType := stIns;
    FmMPReclasIns.ShowModal;
    FmMPReclasIns.Destroy;
  end;
end;

procedure TFmPrincipal2.Inconsistnciadeinformao1Click(Sender: TObject);
begin
  Eiia_PF.InsereNovaInconsistenciaDeInformacao();
end;

procedure TFmPrincipal2.ipos1Click(Sender: TObject);
begin
  MostraDefeitostip(0);
end;

procedure TFmPrincipal2.iposdeArtigo1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadRegistroSimples(Dmod.MyDB, DmModVS.TbCouNiv1,
    DmModVS.DsCouNiv1, ncIdefinido, 'Cadastro de Partes de Material VS');
end;

function TFmPrincipal2.AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
var
  LinAtu: Integer;
begin
  if PosicaoAtual < NovaPosicao then
  begin
    LinAtu := Trunc((NovaPosicao - PosicaoAtual) / FAltLin);
    while LinAtu > 0 do
    begin
      if LinAtu > 216 then
      begin
        Write(FArqPrn, #27+'3'+#216);
        LinAtu := LinAtu - 216;
        WriteLn(FArqPrn,' ');
      end else begin
        Write(FArqPrn, MyPrinters.DefAlturaDotImp(3, LinAtu, ''));
        WriteLn(FArqPrn,' ');
        LinAtu := 0;
      end;
    end;
  end;
  Result := NovaPosicao;
  FPrintedLine := False;
end;

function TFmPrincipal2.FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
  EspacosPrefixo, Substitui, Alinhamento, Comprimento, Fonte, Formatacao, CPI:
  Integer; Nulo: Boolean): String;
var
  i, Tam, Letras, MyCPI, Vazios: Integer;
  Esq: Boolean;
  Txt, Vaz: String;
  Increm: Double;
begin
  if Substitui = 1 then Txt := Substituicao else
  begin
    if Texto = '' then Txt := ''
    else Txt := Formata(Texto, Prefixo, Sufixo, EspacosPrefixo, Formatacao, Nulo);
  end;
  if Txt = '' then
  Txt := ' '; // evitar erro de Margem Esquerda ao imprimir ??
  if CPI = 0 then MyCPI := 10 else MyCPI := CPI;
  Tam := Fonte + (MyCPI*10);
  Letras := 0;
  case Tam of
    100: Letras := 17;
    101: Letras := 10;
    102: Letras := 05;
    120: Letras := 20;
    121: Letras := 12;
    122: Letras := 06;
    else Geral.MB_Erro('CPI indefinido!');
  end;
  Increm := (CO_POLEGADA * 100) / Letras;
  Letras := Trunc(((Comprimento / 100) / CO_POLEGADA) * Letras);
  FTamLinha := FTamLinha + Trunc(Increm * Letras);
  Vazios := Letras - Length(Txt);
  Esq := True;
  // [0: Esq] [1: Centro] [2: Direita]
  if Alinhamento = 1 then
  begin
    for i := 1 to Vazios do
    begin
      if Esq then
      begin
        Txt := Txt + ' ';
        Esq := False;
      end else begin
        Txt := ' ' + Txt;
        Esq := True;
      end;
    end;
  end else begin
    for i := 1 to Vazios do Vaz := Vaz + ' ';
    if Alinhamento = 0 then Txt := Txt + Vaz else Txt := Vaz + Txt;
  end;
  Result := Txt;
end;

procedure TFmPrincipal2.FormatoA1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidades, fmcadEntidades);
end;

procedure TFmPrincipal2.FormatoB1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

function TFmPrincipal2.EspacoEntreTextos(QrView: TmySQLQuery; CPI: Integer): String;
var
  Texto: String;
  i, MyCPI, Letras: Integer;
  Soma: Double;
begin
  Texto := '';
  if FTamLinha > QrView.FieldByName('MEsq').AsInteger then
  begin
    // Erro !!!
  end;
  if FTamLinha < QrView.FieldByName('MEsq').AsInteger then
  begin
    Texto := #15;
    if CPI = 0 then MyCPI := 10 else MyCPI := CPI;
    MyCPI := MyCPI + FFoiExpand;
    case MyCPI of
     010: MyCPI := 17;
     012: MyCPI := 20;
     110: MyCPI := 10;
     112: MyCPI := 10;
    end;
    Soma := (CO_POLEGADA * 100)/ MyCPI;
    Letras := Trunc((QrView.FieldByName('MEsq').AsInteger - FTamLinha) /
      (CO_POLEGADA * 100) * MyCPI);
    for i := 1 to Letras do Texto := Texto + ' ';
    FTamLinha := FTamLinha + Trunc(Soma * Letras);
  end;
  Result := Texto;
end;

procedure TFmPrincipal2.ExtratosFinanceirosEmpresaUnica(TabLctA: String);
begin
  //Copiar codigo de outro projeto!!!
end;

function TFmPrincipal2.Formata(Texto, Prefixo, Sufixo: String; EspacosPrefixo,
 Formato: Integer; Nulo: Boolean): String;
var
  MeuTexto, MeuPrefixo: String;
  i: Integer;
begin
  if Trim(Texto) = '' then MeuTexto := Texto else
  begin
    case Formato of
      1: MeuTexto := Geral.TFT(Texto, 0, siNegativo);
      2: MeuTexto := Geral.TFT(Texto, 1, siNegativo);
      3: MeuTexto := Geral.TFT(Texto, 2, siNegativo);
      4: MeuTexto := Geral.TFT(Texto, 3, siNegativo);
      5: MeuTexto := Geral.TFT(Texto, 4, siNegativo);
      6: MeuTexto := FormatDateTime(VAR_FORMATDATE3, StrToDate(Texto));
      7: MeuTexto := FormatDateTime(VAR_FORMATDATE2, StrToDate(Texto));
      8: MeuTexto := FormatDateTime(VAR_FORMATTIME2, StrToTime(Texto));
      9: MeuTexto := FormatDateTime(VAR_FORMATTIME , StrToTime(Texto));
      else MeuTexto := Texto;
    end;
  end;
  if (Formato in ([1,2,3,4,5])) and Nulo then
  begin
    if Geral.DMV(MeuTexto) < 0.0001 then MeuTexto := '';
  end;
  if MeuTexto = '' then Result := '' else
  begin
    MeuPrefixo := '';
    if Length(Prefixo) > 0 then
      for i := 0 to EspacosPrefixo-1 do MeuPrefixo := MeuPrefixo + ' ';
    Result := Prefixo+MeuPrefixo+MeuTexto+Sufixo;
  end;
end;

procedure TFmPrincipal2.Embalagens1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEmbalagens, FmEmbalagens, afmoNegarComAviso) then
  begin
    FmEmbalagens.ShowModal;
    FmEmbalagens.Destroy;
  end;
end;

procedure TFmPrincipal2.Emitidos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmResultados1, FmResultados1, afmoNegarComAviso) then
  begin
    FmResultados1.FTabLctA := LAN_CTOS;
    FmResultados1.ShowModal;
    FmResultados1.Destroy;
  end;
end;

procedure TFmPrincipal2.Energiaeltricaguags1Click(Sender: TObject);
begin
  EfdIcmsIpi_Jan.MostraFormEFD_C001();
end;

procedure TFmPrincipal2.Entidade1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(
    MyVCLRef.GET_ValorDeObjeto_Inteiro(), fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal2.Entidades2Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal2.Entradas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCMPTInn, FmCMPTInn, afmoNegarComAviso) then
  begin
    FmCMPTInn.ShowModal;
    FmCMPTInn.Destroy;
  end;
end;

procedure TFmPrincipal2.Envioparaposteriorretorno1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMOEnvEnvGer(0);
end;

procedure TFmPrincipal2.EquipesClick(Sender: TObject);
begin
  CadastroEquipes();
end;

procedure TFmPrincipal2.Acabamento1Click(Sender: TObject);
begin
  PQ_PF.MostraFormFormulasImpFI();
end;

procedure TFmPrincipal2.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
  Entidade: Integer; Aba: Boolean);
begin
  if Entidade > 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Codigo');
    Dmod.QrAux.SQL.Add('FROM ponto');
    Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrAux.Params[0].AsInteger := Entidade;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    //
    if Dmod.QrAux.RecordCount = 0 then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT Codigo');
      Dmod.QrAux.SQL.Add('FROM entidades');
      Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrAux.SQL.Add('AND Fornece5="V"');
      Dmod.QrAux.Params[0].AsInteger := Entidade;
      UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
      //
      if Dmod.QrAux.RecordCount = 1 then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO ponto SET Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := Entidade;
        Dmod.QrUpd.ExecSQL;
      end;
    end;
  end;
end;

procedure TFmPrincipal2.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade1: TStringGrid);
begin
   // Compatibilidade
end;

procedure TFmPrincipal2.AcoesIniciaisDoAplicativo();
const
  AutoRegera = True;
begin
  PageControl1.ActivePageIndex := 0;
  //
  try
    Screen.Cursor := crHourGlass;
    //
    if DModG <> nil then
    begin
      // Provis�rio - Copiar o Campo "PrdGrupTip" para a tabela GraGruYPGT
      // Eliminar o Campo "PrdGrupTip" no Futuro!!!
      Mover_PrdGrupTip_do_GraGruY_Para_GraGrYPGT();
      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Definindo data m�nima de lan�amentos de Insumos');
      UnPQx.DefineVAR_Data_Insum_Movim();
      if DmodG.QrDonoCNPJ_CPF.Value = '02717861000110' then //Apenas o SoftCouro at� finalizar o Sped depois remover
        AGBConfEstq.Visible := True
      else
        AGBConfEstq.Visible := False;
      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Criando DB usu�rio');
      DModG.MyPID_DB_Cria();
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      //
      StatusBar.Panels[11].Text := Dmod.MyDB.DatabaseName;
      //
      MyObjects.Informa2(LaTopWarn2, LaTopWarn1, True, 'Criando favoritos');
      if not DModG.CriaFavoritos(AdvToolBarPager1, LaTopWarn2, LaTopWarn1,
        AGBEntidades, FmPrincipal)
      then
        if DModG.QrCtrlGeralAbaIniApp.Value > 0 then
          AdvToolBarPager1.ActivePageIndex := DModG.QrCtrlGeralAbaIniApp.Value;
      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Atualizando cota��es');
      DModG.AtualizaCotacoes();
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Verificando filiais');
      if (VAR_USUARIO  = -1) or (VAR_USUARIO = -3) then
      begin
        DModG.QrFiliaisSP.Close;
        UnDmkDAC_PF.AbreQuery(DModG.QrFiliaisSP, Dmod.MyDB);
        if DModG.QrFiliaisSP.RecordCount > 0 then
          MostraParamsEmp();
      end;
      //AtzPed();
      // verifica se h� gragrux para os PQs!
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Verificando necessidade de corre��es');
      VerificaPQ_GGX();
      // corrigir cadastro errado (eliminar ap�s atualizar o BD do IDEAL)
      VerificaMP_GGX();
      //
      //if DModG.QrCtrlGeralAtualizouPreEmail.AsInteger = 0 then
        //DModG.AtualizaPreEmail;
      //
      DmNFe_0000.VerificaNFeCabA();
      //DmodG.ConfiguraIconeAplicativo;
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Verificando alertas');
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      //
      DmodG.VerificaHorVerao();
      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Definindo Periodo do Balan�o VS');
      DMModVS.DefinePeriodoBalVSEmpresaLogada();
      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True,
        'Definindo a data m�nima para lan�amentos SPED EFD ICMS/IPI');
      VAR_Data_SPEDEFDEnce_MovimXX :=
        EfdIcmsIpi_PF.SPEDEFDEnce_DataMinMovim('MovimXX');
      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Definindo Aba inicial');
      // Temporario
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Modificando Empresas para Cliente MO');
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE entidades ',
      'SET Cliente2="V" ',
      'WHERE Codigo < -10 ',
      '']);
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, False, '...');
      // Fim temporario
      if VAR_USUARIO = -1 then
      begin
        Geral.MB_Info('Janela de selecao de DB e Server !' + sLineBreak +
          'Amostras' + sLineBreak +
          '###############' + sLineBreak +
          'Criar aviso de erro na exporta��o SPED do valor a recolher no E116 <> E110'
          + sLineBreak +
          '###############' + sLineBreak +
'Empresa e Natureza da operacao '
          + sLineBreak +
'ver materias que nao vao no K200 como carro e extintor e moldura de quadro! '
          + sLineBreak +
'Gerenciar atrelamentos de NF MO no artigo destino recurtimento sem atrelamento (-1)! '
          + sLineBreak +
'Mudar Uso e consumo em todo sistema'
          + sLineBreak +
'ver se esta importando texto extra do item nfe'
          + sLineBreak +
          'FRCampos.Field      := ?JmpMovID;'
          + sLineBreak +
          '');
      end;
      UFixBugs.MostraFixBugs(['Outros', 'Atualiza NFs de entrada de uso e consumo']);
      //
      DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtMostra);
      //
      if Dmod.QrControleVSPwdDdUser.Value = VAR_USUARIO then
      begin
        VS_PF.MostraFormVSPwdDd(AutoRegera);
      end;
      //
      if VAR_USUARIO = -1 then
      begin
        //VS_PF.VerificaFornecMO(PB1, LaTopWarn1, LaTopWarn2);
        VS_PF.VerificaCliForEmpty();
        VS_PF.VerificaStqCenLocEmpty();
        //...
      end;
      // Desmarcar por enquanto!
      //VS_PF.VerificaOpeSemOperacao();
      //
      VS_PF.VerificaAtzCaleados(PB1, LaTopWarn1, LaTopWarn2);
      // ver se realmente precisa fazer! Codigo feito em 2017-11-15 e funcionando!
      //VS_PF.VerificaAtzCurtidos(PB1, LaTopWarn1, LaTopWarn2);
      VS_PF.VerificaAtzCalPDA(PB1, LaTopWarn1, LaTopWarn2);
      VS_PF.VerificaAtzCalDTA(PB1, LaTopWarn1, LaTopWarn2);
      //
      VS_PF.VerificaGSP('GSPInnNiv2', emidCompra);
      VS_PF.VerificaGSP('GSPArtNiv2', emidIndsXX);
      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Verificando PQE sem Empresa');
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQE, Dmod.MyDB, [
      'SELECT COUNT(Codigo) Itens',
      'FROM pqe',
      'WHERE Empresa=0',
      '']);
      if QrPQE.FieldByName('Itens').AsInteger > 0 then
      begin
        if Geral.MB_Pergunta('Existem ' +
        Geral.FF0(QrPQE.FieldByName('Itens').AsInteger) +
        ' entradas de insumos sem EMPRESA.' + sLineBreak +
        'Deja corrigi-los agora?') = ID_Yes then
          Dmod.MyDB.Execute('UPDATE pqe SET Empresa=-11 WHERE Empresa=0');
      end;
      // Evitar erro catastrofico!
      TmVersao.Enabled := True;
      //
    end;
    MyObjects.Informa2(LaTopWarn1, LaTopWarn2, False, '...');
  finally
    TmSuporte.Enabled := True;
    Screen.Cursor     := crDefault;
  end;
end;

procedure TFmPrincipal2.VerificaPQ_GGX();
{
  function CriaPrdGrupTip(): Boolean;
  var
    Codigo: Integer;
  begin
    if Dmod.QrControlePQ_PrdGrup Tip.Value <> 0 then Result := True else
    begin
      Codigo := UMyMod.BuscaEmLivreY_Def('prdgruptip', 'Codigo', stIns, 0);
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgruptip', False, [
      'CodUsu', 'Nome', 'MadeBy',
      'Fracio', 'Gradeado', 'TipPrd',
      'NivCad', 'FaixaIni', 'FaixaFim',
      'TitNiv1', 'TitNiv2', 'TitNiv3',
      'Customizav'], [
      'Codigo'], [
      Codigo(*CodUsu*), 'Produtos Qu�micos'(*Nome*), 2(*MadeBy*),
      3(*Fracio*), 0(*Gradeado*), 3(*TipPrd*),
      1(*NivCad*), 1001(*FaixaIni*), 9999(*FaixaFim*),
      'Produto'(*TitNiv1*), ''(*TitNiv2*), ''(*TitNiv3*),
      0(*Customizav*)], [
      Codigo], True);
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE controle SET PQ_PrdGrup Tip=' + Geral.FF0(Codigo));
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrControle.Close;
      UnDmkDAC_PF.AbreQuery(Dmod.QrControle, Dmod.MyDB);
    end;
  end;
}
var
(*
  GraGru1, GraGruC, GraTamI, Controle, CodUsu, PrdGrupTip, GraTamCad: Integer;
  Nome: String;
*)
  MudouTabelas: Boolean;
begin
  // Parei aqui 09 10 08 restaurar backup para testar?
  DModG.ConsertaCST_PIS_COFINS();
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT Codigo FROM entidades WHERE Codigo=-11');
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  if Dmod.QrAux.RecordCount = 0 then
  begin
    if Geral.MB_Pergunta('Ser� feito modifica��es na estrutura de dados para a NFe!' +
    sLineBreak + 'Confirma e aceita estas altera��es?') <> ID_YES then Exit;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE entidades SET Codigo=-11, Filial=1 WHERE Codigo=-1;');
    Dmod.QrUpd.SQL.Add('UPDATE entidades SET CodUsu=-11, Filial=1 WHERE CodUsu=-1;');
    Dmod.QrUpd.SQL.Add('UPDATE controle SET Dono=-11;');
    Dmod.QrUpd.SQL.Add('UPDATE lct0001a SET CliInt=-11;');
    Dmod.QrUpd.SQL.Add('UPDATE pqcli SET CI=-11 WHERE CI=-1;');
    Dmod.QrUpd.SQL.Add('UPDATE pqx SET CliOrig=-11 WHERE CliOrig=-1;');
    Dmod.QrUpd.SQL.Add('UPDATE pqx SET CliDest=-11 WHERE CliDest=-1;');
    Dmod.QrUpd.SQL.Add('UPDATE formulas SET ClienteI=-11 WHERE ClienteI=-1;');
    Dmod.QrUpd.SQL.Add('UPDATE tintascab SET ClienteI=-11 WHERE ClienteI=-1;');
    //
    Dmod.QrUpd.SQL.Add('UPDATE entidades SET CodUsu=Codigo WHERE CodUsu=0;');
    Dmod.QrUpd.SQL.Add('DELETE FROM ufs;');
    Dmod.QrUpd.SQL.Add('DROP TABLE IF EXISTS cambios;');
    Dmod.QrUpd.ExecSQL;
    //
    MudouTabelas := True;
  end else MudouTabelas := False;
  //
{  Desabilitei 2011-05-08
  QrPQ_GGX.Close;
  QrPQ_GGX.Database := Dmod.MyDB;
  UnDmkDAC_PF.AbreQuery(QrPQ_GGX, Dmod.MyDB);
  if QrPQ_GGX.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(QrPQ_GGX.RecordCount) +
    ' insumos sem correspond�ncia em grade! Esta correspond�ncia ser� criada!');
    //
    Screen.Cursor := crHourGlass;
    try
      //Criar GraGru1, PrdGrupTip, e setar controle!
      QrPQ_GGX.First;
      while not QrPQ_GGX.Eof do
      begin
        CodUsu := QrPQ_GGXPQ.Value + 1000;
        Nome   := QrPQ_GGXNomePQ.Value;
        PrdGrupTip := -2;//Dmod.QrControlePQ_PrdGrup Tip.Value;
        GraGru1    := QrPq_GGXPQ.Value;
        GraTamCad  := 0;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragru1', False, [
          'Nivel3', 'Nivel2', 'CodUsu', 'Nome', 'PrdGrupTip', 'GraTamCad'
        ], ['Nivel1'], [
          0(*FNivel3*), 0(*FNivel2*), CodUsu, Nome, PrdGrupTip, GraTamCad
        ], [GraGru1], True);

        GraTamI := 1; // �nico
        GraGruC := DmProd.ObtemGraGruCDeIdx(GraGru1, 'PQCli', QrPQ_GGXNOMECLI.Value);
        //Controle := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', stIns, 0);
        Controle := QrPQ_GGXControle.Value;
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False, [
        'GraGruC', 'GraGru1', 'GraTamI'], [
        'Controle'], [GraGruC, GraGru1, GraTamI], [
        Controle], True) then
          Controle := 0;
        if (GraGru1 = 0) or (GraGruC = 0) or (Controle = 0) then
        begin
          Geral.MB_Erro('N�o foi poss�vel gerar o item de grade ' +
            'para o insumo ' + Geral.FF0(QrPQ_GGXPQ.Value) + ' (Controle ' +
            Geral.FF0(Controle) + ' )!');
          Screen.Cursor := crDefault;
          Exit;
        end;
        //
        QrPQ_GGX.Next;
      end;
      Geral.MB_Info('Atualiza��o realizada com sucesso!');
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  //
  QrPQ_GGX.Close;
  UnDmkDAC_PF.AbreQuery(QrPQ_GGX, Dmod.MyDB);
  DBGPQ_GGX.Visible := QrPQ_GGX.RecordCount > 0;
  //
}
  if MudouTabelas then Halt(0);
end;

procedure TFmPrincipal2.VerificaTabelasterceiros1Click(Sender: TObject);
begin
  ALL_Jan.MostraFormVerifiDBTerceiros(False);
  (*
  if DBCheck.CriaFm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros, afmoNegarComAviso) then
  begin
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.Destroy;
  end;
  *)
end;

procedure TFmPrincipal2.Vrias1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSImpClaMulRMP();
end;

procedure TFmPrincipal2.VSxPesagemPQ1Click(Sender: TObject);
begin
  AppPF.MostraFormCfgEstqVSxPQ_EFD(
    2(*ImporExpor*),
    EfdIcmsIpi_PF.AnoMesAnterior(),
    VAR_LIB_EMPRESA_SEL,
    0);
end;

procedure TFmPrincipal2.ReabreTabelas1Click(Sender: TObject);
begin
  UnDmkDAC_PF.ReabrirTabelasFormAtivo(Sender);
end;

procedure TFmPrincipal2.reaVSm1Click(Sender: TObject);
begin
//
end;

procedure TFmPrincipal2.reaVSm21Click(Sender: TObject);
const
  TemIMEIMrt = 0;
var
  Valor: Double;
begin
  Valor := MyVCLRef.GET_ValorDeObjeto_Double();
  VS_PF.PesquisaDoubleVS('AreaM2', Valor, TemIMEIMrt);
end;

procedure TFmPrincipal2.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa ainda!
end;

procedure TFmPrincipal2.ReclassificaoMassiva1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPaMulCabR(0);
end;

procedure TFmPrincipal2.RecomearclkassificaodeOCjconfigurada1Click(
  Sender: TObject);
begin
  VS_PF.MostraFormVSClaArtSel_IMEI();
end;

procedure TFmPrincipal2.RecomearclkassificaodeOCjconfigurada2Click(
  Sender: TObject);
begin
  VS_PF.MostraFormVSClaArtSel_FRMP();
end;

function TFmPrincipal2.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  Result := Ucriar.RecriaTabelaLocal(Tabela, Acao);
end;

procedure TFmPrincipal2.Recurtimento1Click(Sender: TObject);
begin
  PQ_PF.MostraFormFormulasImpWE();
end;

procedure TFmPrincipal2.Reduzido1Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruN_GGX(MyVCLRef.GET_ValorDeObjeto_Inteiro(), '');
end;

function TFmPrincipal2.CartaoDeFatura: Integer;
begin
  Geral.MB_Aviso('Faturando n�o implementado');
  Result := 0;
end;

procedure TFmPrincipal2.Carteiras1Click(Sender: TObject);
begin
  CadastroDeCarteira;
end;

procedure TFmPrincipal2.CFOP1Click(Sender: TObject);
begin
  CadastroCFOP2003();
end;

procedure TFmPrincipal2.CFOP2Click(Sender: TObject);
begin
  CadastroCFOP2003();
end;

procedure TFmPrincipal2.ClasificaoMassiva1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPaMulCabA(0);
end;

procedure TFmPrincipal2.ComearnovaOC1Click(Sender: TObject);
var
  Form, Max: Integer;
  Ord_Txt: String;
  Qry: TmySQLQuery;
  Pallets: TClass15Int;
begin
{
  Form := MyObjects.SelRadioGroup('Sele��o de Janela',
  'Selecione a Janela', [
  'Atual',
  'Novo (3)'], 0);
  case Form of
    0: VS_PF.MostraFormVSClaArtPrpMDz(0, 0, 0, 0, 0, 0, nil, 0, 0);
    1:
}
    begin //testar mais!
      VS_PF.LimpaArray15Int(Pallets);
      VS_PF.MostraFormVSClaArtPrpQnz(Pallets, nil, 0, 0, 0);
    end;
{
  end;
}
end;

function TFmPrincipal2.CompensacaoDeFatura: String;
begin
  Geral.MB_Aviso('Faturando n�o implementado');
  Result := '';
end;

procedure TFmPrincipal2.Componenteativo1Click(Sender: TObject);
begin
  VAR_POPUP_ACTIVE_CONTROL := Screen.ActiveForm.ActiveControl;
  VAR_POPUP_ACTIVE_FORM    := Screen.ActiveForm;
  MyObjects.MostraPopUpNoCentro(VAR_LOC_POPUP);
end;

procedure TFmPrincipal2.ComunicaoTelecomunicao1Click(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmEFD_D001, FmEFD_D001, afmoNegarComAviso) then
  begin
    FmEFD_D001.ShowModal;
    FmEFD_D001.Destroy;
  end;
*)
end;

procedure TFmPrincipal2.Conjuntos1Click(Sender: TObject);
begin
  CadastroDeConjuntos();
end;

procedure TFmPrincipal2.Consumo1Click(Sender: TObject);
begin
  UnPQx.GerenciaBaixaOutros(0);
end;

procedure TFmPrincipal2.Contas1Click(Sender: TObject);
begin
  CadastroDeContas;
end;

procedure TFmPrincipal2.MostraCambioMda;
begin
  if DBCheck.CriaFm(TFmCambioMda, FmCambioMda, afmoNegarComAviso) then
  begin
    FmCambioMda.ShowModal;
    FmCambioMda.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraCarteiras(Carteira: Integer);
begin
  // n�o � necess�rio
end;

procedure TFmPrincipal2.MostraCFOP2003;
begin
  if DBCheck.CriaFm(TFmCFOP2003, FmCFOP2003, afmoNegarComAviso) then
  begin
    FmCFOP2003.ShowModal;
    FmCFOP2003.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraDefeitosloc(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmDefeitosloc, FmDefeitosloc, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmDefeitosloc.LocCod(Codigo, Codigo);
    FmDefeitosloc.ShowModal;
    FmDefeitosloc.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraDefeitostip(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmDefeitostip, FmDefeitostip, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmDefeitostip.LocCod(Codigo, Codigo);
    FmDefeitostip.ShowModal;
    FmDefeitostip.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraFisRegCad();
begin
  if DBCheck.CriaFm(TFmFisRegCad, FmFisRegCad, afmoNegarComAviso) then
  begin
    FmFisRegCad.ShowModal;
    FmFisRegCad.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraFormBalancoPQ();
var
  PQBn: Integer;
begin
(*
  PQBn := MyObjects.SelRadioGroup('Balan�o de Insumos',
    'Selecione uma janela:', ['Nova (recomendada)', 'Antiga'], 2, 0);
  if PQBn > -1 then
  begin
    if PQBn = 0 then
    begin
*)
      if DBCheck.CriaFm(TFmPQB2, FmPQB2, afmoNegarComAviso) then
      begin
        FmPQB2.ShowModal;
        FmPQB2.Destroy;
      end;
(*
    end else
    begin
*)
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmStqBalCad, FmStqBalCad, afmoNegarComAviso) then
      begin
        FmStqBalCad.FMultiGrandeza := True;
        FmStqBalCad.ShowModal;
        FmStqBalCad.Destroy;
      end;
    fggxPQ:
}
(*
      if DBCheck.CriaFm(TFmPQB, FmPQB, afmoNegarComAviso) then
      begin
        FmPQB.ShowModal;
        FmPQB.Destroy;
      end;
*)
{
  end;
}
(*
    end;
  end;
*)
end;

procedure TFmPrincipal2.MostraFormOSsAberta;
begin
  if DBCheck.CriaFm(TFmOSsAbertas, FmOSsAbertas, afmoNegarComAviso) then
  begin
    FmOSsAbertas.ShowModal;
    FmOSsAbertas.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraFormVSIndsVS(SQLType: TSQLType; Controle, MPVIts,
  Emit, EmitCus, InsEmpr: Integer; QrWBMovIts: TmySQLQuery;
  FormaWBIndsWE: TFormaWBIndsWE);
begin
;
end;

procedure TFmPrincipal2.MostraFormWBAjsCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmWBAjsCab, FmWBAjsCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmWBAjsCab.LocCod(Codigo, Codigo);
      FmWBAjsCab.QrWBAjsIts.Locate('Controle', Controle, []);
    end;
    FmWBAjsCab.ShowModal;
    FmWBAjsCab.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraFormWBIndsWE(SQLType: TSQLType; Controle, MPVIts,
Emit, EmitCus, InsEmpr: Integer; QrWBMovIts: TmySQLQuery; FormaWBIndsWE:
  TFormaWBIndsWE);
var
  Qry: TmySQLQuery;
  Empresa: Integer;
begin
  if DBCheck.CriaFm(TFmWBIndsWE, FmWBIndsWE, afmoNegarComAviso) then
  begin
    FmWBIndsWE.FFormaWBIndsWE := FormaWBIndsWE;
    //fiwNone, fiwPesagem, fiwBaixaPrevia);
    FmWBIndsWE.ImgTipo.SQLType := SQLType;
    FmWBIndsWE.FQrWBMovIts := QrWBMovIts;
    if SQLType = stIns then
    begin
      FmWBIndsWE.EdCodigo.ValueVariant := MPVIts;
      FmWBIndsWE.EdLnkNivXtr1.ValueVariant := Emit;
      FmWBIndsWE.EdLnkNivXtr2.ValueVariant := EmitCus;
      //
      FmWBIndsWE.EdEmpresa.ValueVariant := InsEmpr;
      FmWBIndsWE.CBEmpresa.KeyValue     := InsEmpr;
    end else
    begin
      Qry := TmySQLQuery.Create(Self);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM wbmovits ',
        'WHERE Controle=' + Geral.FF0(Controle),
        '']);
        if TEstqMovimID(Qry.FieldByName('MovimID').AsInteger) <> emidIndsWE then
        begin
          FmWBIndsWE.Destroy;
          Geral.MB_Erro('ID Item ' + Geral.FF0(Controle) + ' n�o � de baixa for�ada!');
          Exit;
        end;
        //
        //Codigo         :=
        FmWBIndsWE.EdCodigo.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
        //Controle       :=
        FmWBIndsWE.EdControle.ValueVariant := Qry.FieldByName('Controle').AsInteger;
        //MovimCod       :=
        FmWBIndsWE.EdMovimCod.ValueVariant := Qry.FieldByName('MovimCod').AsInteger;
        (*Empresa*)
        Empresa := DModG.ObtemFilialDeEntidade(Qry.FieldByName('Empresa').AsInteger);
        FmWBIndsWE.EdEmpresa.ValueVariant := Empresa;
        FmWBIndsWE.CBEmpresa.KeyValue     := Empresa;
        //Fornecedor     :=
        FmWBIndsWE.EdFornecedor.ValueVariant := Qry.FieldByName('Terceiro').AsInteger;
        FmWBIndsWE.CBFornecedor.KeyValue     := Qry.FieldByName('Terceiro').AsInteger;
        //DataHora       := Geral.FDT(
        FmWBIndsWE.TPDataHora.Date := Qry.FieldByName(CO_DATA_HORA_GRL).AsDateTime;
        FmWBIndsWE.EdDataHora.ValueVariant  := Qry.FieldByName(CO_DATA_HORA_GRL).AsDateTime;
        //MovimID        := emid Baixa;
        //MovimNiv       := emin SemNiv;
        FmWBIndsWE.EdLnkNivXtr1.ValueVariant := Qry.FieldByName('LnkNivXtr1').AsInteger;
        FmWBIndsWE.EdLnkNivXtr2.ValueVariant := Qry.FieldByName('LnkNivXtr2').AsInteger;
        //Pallet         :=
        FmWBIndsWE.EdPallet.ValueVariant := Qry.FieldByName('Pallet').AsInteger;
        FmWBIndsWE.CBPallet.KeyValue     := Qry.FieldByName('Pallet').AsInteger;
        //GraGruX        :=
        FmWBIndsWE.EdGragruX.ValueVariant := Qry.FieldByName('GragruX').AsInteger;
        FmWBIndsWE.CBGragruX.KeyValue     := Qry.FieldByName('GragruX').AsInteger;
        //Pecas          := -
        FmWBIndsWE.EdPecas.ValueVariant  := -Qry.FieldByName('Pecas').AsFloat;
        //PesoKg         := -
        FmWBIndsWE.EdPesoKg.ValueVariant := -Qry.FieldByName('PesoKg').AsFloat;
        //AreaM2         := -
        FmWBIndsWE.EdAreaM2.ValueVariant := -Qry.FieldByName('AreaM2').AsFloat;
        //AreaP2         := -
        FmWBIndsWE.EdAreaP2.ValueVariant := -Qry.FieldByName('AreaP2').AsFloat;
        //ValorT         := -
        FmWBIndsWE.EdValorT.ValueVariant := -Qry.FieldByName('ValorT').AsFloat;
        //Observ         :=
        FmWBIndsWE.EdObserv.Text          := Qry.FieldByName('Observ').AsString;
      finally
        Qry.Free;
      end;
    end;
    FmWBIndsWE.ShowModal;
    FmWBIndsWE.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraFormWBInnCab(Codigo, WBInnIts, WBReclas: Integer);
begin
  if DBCheck.CriaFm(TFmWBInnCab, FmWBInnCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmWBInnCab.LocCod(Codigo, Codigo);
      FmWBInnCab.QrWBInnIts.Locate('Controle', WBInnIts, []);
      if not FmWBInnCab.QrReclasif.Locate('Controle', WBreclas, []) then
        // tenta o item par
        FmWBInnCab.QrReclasif.Locate('Controle', WBreclas + 1, []);
    end;
    FmWBInnCab.ShowModal;
    FmWBInnCab.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraFormWBOutCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmWBOutCab, FmWBOutCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmWBOutCab.LocCod(Codigo, Codigo);
      FmWBOutCab.QrWBOutIts.Locate('Controle', Controle, []);
    end;
    FmWBOutCab.ShowModal;
    FmWBOutCab.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraFormWBOutIts(SQLType: TSQLType; Controle: Integer;
QrCab, QrIts: TmySQLQuery);
var
  Qry: TmySQLQuery;
  Empresa: Integer;
begin
  if DBCheck.CriaFm(TFmWBOutIts, FmWBOutIts, afmoNegarComAviso) then
  begin
    FmWBOutIts.ImgTipo.SQLType := SQLType;
    FmWBOutIts.FQrCab := QrCab;
    //FmWBOutIts.FDsCab := DsCab;
    FmWBOutIts.FQrIts := QrIts;
    if SQLType = stIns then
    begin
      //
    end else
    begin
      Qry := TmySQLQuery.Create(Self);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM wbmovits ',
        'WHERE Controle=' + Geral.FF0(Controle),
        '']);
        if TEstqMovimID(Qry.FieldByName('MovimID').AsInteger) <> emidBaixa then
        begin
          FmWBOutIts.Destroy;
          Geral.MB_Erro('ID Item ' + Geral.FF0(Controle) + ' n�o � de baixa for�ada!');
          Exit;
        end;
        //
        //Codigo         :=
        FmWBOutIts.EdCodigo.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
        //Controle       :=
        FmWBOutIts.EdControle.ValueVariant := Qry.FieldByName('Controle').AsInteger;
        //MovimCod       :=
        FmWBOutIts.EdMovimCod.ValueVariant := Qry.FieldByName('MovimCod').AsInteger;
        (*Empresa*)
        Empresa := DModG.ObtemFilialDeEntidade(Qry.FieldByName('Empresa').AsInteger);
        FmWBOutIts.EdEmpresa.ValueVariant := Empresa;
        FmWBOutIts.CBEmpresa.KeyValue     := Empresa;
        //Fornecedor     :=
        FmWBOutIts.EdFornecedor.ValueVariant := Qry.FieldByName('Terceiro').AsInteger;
        FmWBOutIts.CBFornecedor.KeyValue     := Qry.FieldByName('Terceiro').AsInteger;
        //DataHora       := Geral.FDT(
        FmWBOutIts.TPDataHora.Date := Qry.FieldByName(CO_DATA_HORA_GRL).AsDateTime;
        FmWBOutIts.EdDataHora.ValueVariant  := Qry.FieldByName(CO_DATA_HORA_GRL).AsDateTime;
        //MovimID        := emid Baixa;
        //MovimNiv       := emin SemNiv;
        //Pallet         :=
        FmWBOutIts.EdPallet.ValueVariant := Qry.FieldByName('Pallet').AsInteger;
        FmWBOutIts.CBPallet.KeyValue     := Qry.FieldByName('Pallet').AsInteger;
        //GraGruX        :=
        FmWBOutIts.EdGragruX.ValueVariant := Qry.FieldByName('GragruX').AsInteger;
        FmWBOutIts.CBGragruX.KeyValue     := Qry.FieldByName('GragruX').AsInteger;
        //Pecas          := -
        FmWBOutIts.EdPecas.ValueVariant  := -Qry.FieldByName('Pecas').AsFloat;
        //PesoKg         := -
        FmWBOutIts.EdPesoKg.ValueVariant := -Qry.FieldByName('PesoKg').AsFloat;
        //AreaM2         := -
        FmWBOutIts.EdAreaM2.ValueVariant := -Qry.FieldByName('AreaM2').AsFloat;
        //AreaP2         := -
        FmWBOutIts.EdAreaP2.ValueVariant := -Qry.FieldByName('AreaP2').AsFloat;
        //ValorT         := -
        FmWBOutIts.EdValorT.ValueVariant := -Qry.FieldByName('ValorT').AsFloat;
        //Observ         :=
        FmWBOutIts.EdObserv.Text          := Qry.FieldByName('Observ').AsString;
      finally
        Qry.Free;
      end;
    end;
    FmWBOutIts.ShowModal;
    FmWBOutIts.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraFormWBPallet();
begin
{
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'wbpallet', 60,
  ncGerlSeq1, 'Cadastro de Pallet',
  [], False, Null, [], [], False);
}
  if DBCheck.CriaFm(TFmWBPallet, FmWBPallet, afmoNegarComAviso) then
  begin
    FmWBPallet.ShowModal;
    FmWBPallet.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraFormWBRclCab(Codigo, WBRclIts, WBReclas: Integer);
begin
  if DBCheck.CriaFm(TFmWBRclCab, FmWBRclCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmWBRclCab.LocCod(Codigo, Codigo);
      FmWBRclCab.QrWBRclIts.Locate('Controle', WBRclIts, []);
      if not FmWBRclCab.QrReclasif.Locate('Controle', WBreclas, []) then
        // tenta o item par
        FmWBRclCab.QrReclasif.Locate('Controle', WBreclas + 1, []);
    end;
    FmWBRclCab.ShowModal;
    FmWBRclCab.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraFormWBRclIns(SQLType: TSQLType; Filial, MovimCod,
  MovimTwn, Codigo, Controle: Integer; ValorT, AreaM2: Double;
  QrReopen: TmySQLQuery; SaldoPecas: Double; WBRclass: TWBRclass);
var
  //Filial,
  Empresa, ForneceA, ReduzidoA, GraGruXA: Integer;
  PalletA, SQLIns: String;
  Qry: TmySQLQuery;
  //
  //Tabela: String;
begin
  if (MovimTwn = 0) and (SQLType = stUpd) then
  begin
    Geral.MB_Erro('"MovimTwn" n�o definido!. Altera��o abortada!');
    Exit;
  end;
  Qry := TmySQLQuery.Create(Self);
  try
    if DBCheck.CriaFm(TFmWBRclIns, FmWBRclIns, afmoNegarComAviso) then
    begin
      FmWBRclIns.ImgTipo.SQLType := SQLType;
      FmWBRclIns.FWBRclass := WBRclass;
      FmWBRclIns.FSaldoPecas := SaldoPecas;
      FmWBRclIns.EdMovimTwn.ValueVariant := MovimTwn;
      //
      //
      FmWBRclIns.FValorT := ValorT;
      FmWBRclIns.FAreaM2 := AreaM2;
      if AreaM2 = 0 then
        FmWBRclIns.FPrecoM2 := 0
      else
        FmWBRclIns.FPrecoM2 := ValorT / AreaM2;
      //
      FmWBRclIns.FQrReopen := QrReopen;
      //
      if SQLType = stIns then
        SQLIns := 'AND Controle=' + Geral.FF0(Controle)
      else
        SQLIns := '';
      //

      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * FROM wbmovits ' ,
      'WHERE Codigo=' + Geral.FF0(Codigo),
      SQLIns,
      'AND MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimTwn=' + Geral.FF0(MovimTwn),
      'ORDER BY Controle']);
      //Filial    := Qry.FieldByName('Empresa').AsInteger;
      Empresa   := DModG.ObtemFilialDeEntidade(Filial);
      ForneceA  := Qry.FieldByName('Terceiro').AsInteger;
      GraGruXA  := Qry.FieldByName('GraGruX').AsInteger;
      PalletA   := Qry.FieldByName('Pallet').AsString;
      // Devem ser os primeiros
      FmWBRclIns.EdEmpresa.ValueVariant  := Empresa;
      FmWBRclIns.CBEmpresa.KeyValue      := Empresa;
      //
      FmWBRclIns.EdForneceA.ValueVariant := ForneceA;
      FmWBRclIns.CBForneceA.KeyValue     := ForneceA;
      FmWBRclIns.EdGraGruXA.ValueVariant := GraGruXA;
      FmWBRclIns.CBGraGruXA.KeyValue     := GraGruXA;
      //
      // Devem ser os �ltimos pois dependem dos outros
      FmWBRclIns.EdPalletA.ValueVariant  := PalletA;
      FmWBRclIns.CBPalletA.KeyValue      := PalletA;
      //
      FmWBRclIns.EdEmpresa.Enabled  := False;
      FmWBRclIns.CBEmpresa.Enabled  := False;
      //TWBRclass = (wbrEntrada, wbrEstoque);
      if WBRclass <> wbrEstoque then
      begin
        FmWBRclIns.EdForneceA.Enabled := False;
        FmWBRclIns.CBForneceA.Enabled := False;
        FmWBRclIns.EdGraGruXA.Enabled := False;
        FmWBRclIns.CBGraGruXA.Enabled := False;
        FmWBRclIns.EdPalletA.Enabled  := False;
        FmWBRclIns.CBPalletA.Enabled  := False;
      end;
      FmWBRclIns.EdForneceB.Enabled := False;
      FmWBRclIns.CBForneceB.Enabled := False;
      //
      FmWBRclIns.EdForneceB.ValueVariant := ForneceA;
      FmWBRclIns.CBForneceB.KeyValue     := ForneceA;
      //FmWBRclIns.EdGraGruXB.ValueVariant := GraGruXA;
      //FmWBRclIns.CBGraGruXB.KeyValue     := GraGruXA;
      //
      if SQLType = stIns then
      begin
        FmWBRclIns.FSrcMovID  := MovimCod;
        FmWBRclIns.FSrcNivel1 := Codigo;
        FmWBRclIns.FSrcNivel2 := Controle;
      end else
      begin
        FmWBRclIns.FSrcMovID  := Qry.FieldByName('SrcMovID').AsInteger;
        FmWBRclIns.FSrcNivel1 := Qry.FieldByName('SrcNivel1').AsInteger;
        FmWBRclIns.FSrcNivel2 := Qry.FieldByName('SrcNivel2').AsInteger;
        //
        FmWBRclIns.TPData.Date := Qry.FieldByName(CO_DATA_HORA_GRL).AsDateTime;
        FmWBRclIns.EdHora.ValueVariant := Qry.FieldByName(CO_DATA_HORA_GRL).AsDateTime;
        FmWBRclIns.EdCodigo.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
        FmWBRclIns.EdMovimCod.ValueVariant := Qry.FieldByName('MovimCod').AsInteger;
        //
        FmWBRclIns.EdControleA.ValueVariant := Qry.FieldByName('Controle').AsInteger;
        FmWBRclIns.EdPecasA.ValueVariant := -Qry.FieldByName('Pecas').AsFloat;
        FmWBRclIns.EdPesoKgA.ValueVariant := -Qry.FieldByName('PesoKg').AsFloat;
        FmWBRclIns.EdAreaM2A.ValueVariant := -Qry.FieldByName('AreaM2').AsFloat;
        FmWBRclIns.EdAreaP2A.ValueVariant := -Qry.FieldByName('AreaP2').AsFloat;
        FmWBRclIns.EdValorTA.ValueVariant := -Qry.FieldByName('ValorT').AsFloat;
        //
        Qry.Next;
        //
        FmWBRclIns.EdControleB.ValueVariant := Qry.FieldByName('Controle').AsInteger;
        FmWBRclIns.EdGraGruXB.ValueVariant  := Qry.FieldByName('GraGruX').AsInteger;
        FmWBRclIns.CBGraGruXB.KeyValue      := Qry.FieldByName('GraGruX').AsInteger;
        FmWBRclIns.EdPecasB.ValueVariant    := Qry.FieldByName('Pecas').AsFloat;
        FmWBRclIns.EdPesoKgB.ValueVariant   := Qry.FieldByName('PesoKg').AsFloat;
        FmWBRclIns.EdAreaM2B.ValueVariant   := Qry.FieldByName('AreaM2').AsFloat;
        FmWBRclIns.EdAreaP2B.ValueVariant   := Qry.FieldByName('AreaP2').AsFloat;
        FmWBRclIns.EdValorTB.ValueVariant   := Qry.FieldByName('ValorT').AsFloat;
        // Devem ser os �ltimos pois dependem dos outros
        FmWBRclIns.EdPalletB.ValueVariant   := Qry.FieldByName('Pallet').AsInteger;
        FmWBRclIns.CBPalletB.KeyValue       := Qry.FieldByName('Pallet').AsInteger;
      end;
      FmWBRclIns.ShowModal;
      FmWBRclIns.Destroy;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal2.MostraGeosite;
begin
  if DBCheck.CriaFm(TFmGeosite, FmGeosite, afmoNegarComAviso) then
  begin
    FmGeosite.ShowModal;
    FmGeosite.Destroy;
  end;
end;

procedure TFmPrincipal2.MostraModelosImp;
begin
  if DBCheck.CriaFm(TFmImprime, FmImprime, afmoNegarComAviso) then
  begin
    FmImprime.ShowModal;
    FmImprime.Destroy;
  end;
end;

procedure TFmPrincipal2.CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
begin
  Application.CreateForm(TFmCalcPercent, FmCalcPercent);
  with FmCalcPercent do
  begin
    if Calc = cpMulta    then LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;

(*
function TFmPrincipal.CriaFormEntradaCab(MaterialNF: TTipoMaterialNF;
ShowForm: Boolean; IDCtrl: Integer): Boolean;
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  case MaterialNF of
    tnfMateriaPrima:
    begin
      FTipoEntradaDig := VAR_FATID_0113;
      FTipoEntradaNFE := VAR_FATID_0013;
      FTipoEntradaEFD := VAR_FATID_0213;
      FTipoEntradTitu := 'Mat�ria-prima';
    end;
    tnfUsoEConsumo:
    begin
      FTipoEntradaDig := VAR_FATID_0151;
      FTipoEntradaNFe := VAR_FATID_0051;
      FTipoEntradaEFD := VAR_FATID_0251;
      FTipoEntradTitu := 'Uso e Consumo';
    end;
  end;
  Result := DBCheck.CriaFm(TFmEntradaCab, FmEntradaCab, afmoNegarComAviso);
  if Result and ShowForm then
  begin
    if IDCtrl <> 0 then
    begin
      FmEntradaCab.LocCod(IDCtrl, IDCtrl);
      if FmEntradaCab.QrNFeCabAIDCtrl.Value <> IDCtrl then
        Geral.MB_Aviso('N�o foi poss�vel localizar o lan�amento solicitado!');
    end;
    FmEntradaCab.ShowModal;
    FmEntradaCab.Destroy;
  end;
end;
*)

{
procedure TFmPrincipal.RefreshInsumos;
begin
  if DBCheck.CriaFm(TFmPQAtz, FmPQAtz, afmoNegarComAviso) then
  begin
    FmPQAtz.ShowModal;
    FmPQAtz.Destroy;
  end;
end;
}

procedure TFmPrincipal2.Relatrios1Click(Sender: TObject);
begin
  PedidosRelatorios;
end;

procedure TFmPrincipal2.PQCadastro1Click(Sender: TObject);
begin
  CadastroPQ(0);
end;

procedure TFmPrincipal2.VendasRelatorios;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmMPVImp, FmMPVImp);
  finally
    Screen.Cursor := Cursor;
  end;
  FmMPVImp.ShowModal;
  FmMPVImp.Destroy;
end;

procedure TFmPrincipal2.VerificaBD;
begin
  ALL_Jan.MostraFormVerifiDB(False);
(*
  if DBCheck.CriaFm(TFmVerifiDB, FmVerifiDB, afmoNegarComAviso) then
  begin
    FmVerifiDB.ShowModal;
    FmVerifiDB.Destroy;
  end;
*)
end;

procedure TFmPrincipal2.VerificaBDServidor1Click(Sender: TObject);
begin
  VerificaBD();
end;

procedure TFmPrincipal2.VerificaMP_GGX;
begin
{ DESABILITEI EM 2001-05-21 - esta causando erros?
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM gragrux');
  Dmod.QrUpd.SQL.Add('WHERE Controle < -100');
  Dmod.QrUpd.SQL.Add('AND (GraGruC = 0');
  Dmod.QrUpd.SQL.Add('OR GraTamI = 0)');
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM gragru1');
  Dmod.QrUpd.SQL.Add('WHERE Nivel1 < -100');
  Dmod.QrUpd.SQL.Add('AND GraTamCad = -1');
  Dmod.QrUpd.ExecSQL;
}
end;

function TFmPrincipal2.VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
var
  Versao: Integer;
  Arq: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'BlueDerm',
    'BlueDerm', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO,
    CO_DMKID_APP, DModG.ObtemAgora(), Memo3, dtExec, Versao, Arq, False,
    ApenasVerifica, BalloonHint1);
end;

procedure TFmPrincipal2.PedidosRelatorios;
begin
  if DBCheck.CriaFm(TFmMPPImp, FmMPPImp, afmoNegarComAviso) then
  begin
    FmMPPImp.ShowModal;
    FmMPPImp.Destroy;
  end;
end;

procedure TFmPrincipal2.Perdasdecouros1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmRolPerdas, FmRolPerdas, afmoNegarComAviso) then
  begin
    FmRolPerdas.ShowModal;
    FmRolPerdas.Destroy;
  end;
end;

procedure TFmPrincipal2.PesoVSkg1Click(Sender: TObject);
const
  TemIMEIMrt = 0;
var
  Valor: Double;
begin
  Valor := MyVCLRef.GET_ValorDeObjeto_Double();
  VS_PF.PesquisaDoubleVS('PesoKg', Valor, TemIMEIMrt);
end;

procedure TFmPrincipal2.Planos1Click(Sender: TObject);
begin
  CadastroDePlano();
end;

procedure TFmPrincipal2.VendasGerencia(Direto: Boolean);
begin
  case Dmod.QrControleVendaCouro.Value of
    1:
    begin
      if DBCheck.CriaFm(TFmMPVn, FmMPVn, afmoNegarComAviso) then
      begin
        FmMPVn.FDireto := Direto;
        FmMPVn.ShowModal;
        FmMPVn.Destroy;
      end;
    end;
    2:
    begin
      if DBCheck.CriaFm(TFmMPV2, FmMPV2, afmoNegarComAviso) then
      begin
        FmMPV2.FDireto := False;
        FmMPV2.ShowModal;
        FmMPV2.Destroy;
      end;
    end;
  end;
end;

procedure TFmPrincipal2.StatusPedido(Controle: Integer; DataAtual: TDateTime);
begin
  VAR_DATARESULT := DataAtual;
  MyObjects.Senha(TFmSenha, FmSenha, 0, '', '', '', '', '', True,
    'Data do t�rmino', Date, 'ItemPedidoOK');
  if VAR_SENHARESULT > -1 then 
  begin
    Screen.Cursor := crHourGlass;
    try
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE mpvits SET Pronto=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[0].AsString  := Geral.FDT(VAR_DATARESULT, 1);
      Dmod.QrUpd.Params[1].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPrincipal2.Subgrupos1Click(Sender: TObject);
begin
  CadastroDeSubgrupos;
end;

procedure TFmPrincipal2.SaldoDeContas;
begin
  if DBCheck.CriaFm(TFmContasHistSdo3, FmContasHistSdo3, afmoNegarComAviso) then
  begin
    FmContasHistSdo3.ShowModal;
    FmContasHistSdo3.Destroy;
  end;
end;

procedure TFmPrincipal2.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

function TFmPrincipal2.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal2.Padro1Click(Sender: TObject);
begin
  ImgPrincipal.Picture := nil;
  Geral.WriteAppKey('ImagemFundo', Application.Title, '', ktString, HKEY_LOCAL_MACHINE);
end;

procedure TFmPrincipal2.Padro3Click(Sender: TObject);
begin
  Geral.WriteAppKey('MenuStyle', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  SkinMenu(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal2.PageControl1Enter(Sender: TObject);
begin
(*
  if Dmod.QrOpcoesBugsATBAutoExp.Value = 1 then
  begin
    AdvToolBarPager1.Collaps;
  end;
*)
end;

procedure TFmPrincipal2.PageControl1MouseEnter(Sender: TObject);
begin
(*
  if Dmod.QrOpcoesBugsATBAutoExp.Value = 1 then
  begin
    AdvToolBarPager1.Collaps;
  end;
*)
end;

procedure TFmPrincipal2.PageControl1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbRight) then
  begin
    //Geral.MB_Aviso('X=' + Geral.FF0(X) + ' Y=' + Geral.FF0(Y));
    //Geral.MB_Aviso('Qtd Compos: ' + Geral.FF0(PageControl1.ActivePage.ComponentCount));
    if PageControl1.ActivePage.ComponentCount = 0 then
    begin
      if Geral.MB_Pergunta('Deseja excluir a aba "' +
        PageControl1.ActivePage.Caption + '"?') = ID_YES
      then
        PageControl1.ActivePage.Free;
    end;
  end;
end;

procedure TFmPrincipal2.PedidosGerencia(Direto: Boolean);
begin
  if DBCheck.CriaFm(TFmMPP, FmMPP, afmoNegarComAviso) then
  begin
    FmMPP.FDireto := Direto;
    FmMPP.ShowModal;
    FmMPP.Destroy;
  end;
end;

procedure TFmPrincipal2.Geral1Click(Sender: TObject);
begin
  MostraFormOSsAberta();
end;

procedure TFmPrincipal2.Geral2Click(Sender: TObject);
begin
  MostraFormOSsAberta();
end;

procedure TFmPrincipal2.Geral3Click(Sender: TObject);
begin
(*
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if DBCheck.CriaFm(TFmSintegra_Arq, FmSintegra_Arq, afmoNegarComAviso) then
  begin
    FmSintegra_Arq.ShowModal;
    FmSintegra_Arq.Destroy;
  end;
*)
end;

procedure TFmPrincipal2.Geraoxestoque1Click(Sender: TObject);
begin
  AppPF.MostraFormCfgEstqGeradoEFD(
    2(*ImporExpor*),
    EfdIcmsIpi_PF.AnoMesAnterior(),
    VAR_LIB_EMPRESA_SEL,
    0);
end;

procedure TFmPrincipal2.Gerencia1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMPReclasGer, FmMPReclasGer, afmoNegarComAviso) then
  begin
    FmMPReclasGer.ShowModal;
    FmMPReclasGer.Destroy;
  end;
end;

procedure TFmPrincipal2.Gerencia2Click(Sender: TObject);
begin
  VendasGerencia(False);
end;

procedure TFmPrincipal2.Gerenciamento1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSGerRclCab(0, 0);
end;

procedure TFmPrincipal2.Gerenciamento2Click(Sender: TObject);
begin
  VS_PF.MostraFormVSGerClaCab(0);
end;

procedure TFmPrincipal2.GerenciamentodePrReclasse1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPrePalCab(0);
end;

procedure TFmPrincipal2.Gerenciar2Click(Sender: TObject);
begin
  PedidosGerencia(False);
end;

procedure TFmPrincipal2.Grupos1Click(Sender: TObject);
begin
  CadastroDeGrupos;
end;

procedure TFmPrincipal2.Gruposqumicos1Click(Sender: TObject);
begin
  CadastroGruposQuimicos();
end;

procedure TFmPrincipal2.CadastrodeEspessuras();
begin
  if DBCheck.CriaFm(TFmEspessuras, FmEspessuras, afmoNegarComAviso) then
  begin
    FmEspessuras.ShowModal;
    FmEspessuras.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroDeSetores(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmListaSetores, FmListaSetores, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmListaSetores.LocCod(Codigo, Codigo);
    FmListaSetores.ShowModal;
    FmListaSetores.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroDeCambio();
begin
  if DBCheck.CriaFm(TFmCambioCot, FmCambioCot, afmoNegarComAviso) then
  begin
    FmCambioCot.ShowModal;
    FmCambioCot.Destroy;
  end;
end;

function TFmPrincipal2.ObtemUnidadeTxt(Unidade: Integer): String;
begin
  case Unidade of
    0: Result := 'kg';
    1: Result := 'pe�a';
    2: Result := 'm�';
    else Result := '???';
  end;
end;

procedure TFmPrincipal2.Operacoes1Click(Sender: TObject);
begin
  VS_PF.MostraFormOperacoes(0);
end;

procedure TFmPrincipal2.Operaes1Click(Sender: TObject);
begin
  VS_PF.MostraFormOperacoes(0);
end;

procedure TFmPrincipal2.Outrasbaixasxestoque1Click(Sender: TObject);
begin
  AppPF.MostraFormCfgEstqOthersEFD(
    2(*ImporExpor*),
    EfdIcmsIpi_PF.AnoMesAnterior(),
    VAR_LIB_EMPRESA_SEL, 0);
end;

procedure TFmPrincipal2.Outros2Click(Sender: TObject);
begin
  MostraModelosImp;
end;

function TFmPrincipal2.ObtemMoedaTxt(Moeda: Integer): String;
begin
  case Moeda of
    0: Result := 'R$';
    1: Result := 'U$';
    2: Result := '�$';
    3: Result := 'IDX';
    else Result := '???';
  end;
end;

function TFmPrincipal2.ObtemPeriodoTxt(Periodo: Integer): String;
begin
  case Periodo of
    0: Result := 'Di�rio';
    1: Result := 'Semanal';
    2: Result := 'Decendial';
    3: Result := 'Quinzenal';
    4: Result := 'Mensal';
    else Result := '???';
  end;
end;

function TFmPrincipal2.AvisaFaltaDeContaDoPlano(Item: Integer): Boolean;
var
  Descricao: String;
  Conta: Integer;
begin
  // Permitir sempre
  Result := False;
  //
  case Item of
     VAR_FATID_1001 : Conta := Dmod.QrControleCtaPQCompr.Value;
     VAR_FATID_1002 : Conta := Dmod.QrControleCtaPQFrete.Value;
     VAR_FATID_1003 : Conta := Dmod.QrControleCtaMPCompr.Value;
     VAR_FATID_1004 : Conta := Dmod.QrControleCtaMPFrete.Value;
     VAR_FATID_1007 : Conta := Dmod.QrControleCtaCVVenda.Value;
     VAR_FATID_1008 : Conta := Dmod.QrControleCtaCVFrete.Value;
     else Conta := 0;
  end;
  if Conta = 0 then
  begin
    //Result := True;
    case Item of
      VAR_FATID_1001 : Descricao := VAR_MSG_INSUMOQUIMICO;
      VAR_FATID_1002 : Descricao := VAR_MSG_FRETEPQ;
      VAR_FATID_1003 : Descricao := VAR_MSG_MATERIAPRIMA;
      VAR_FATID_1004 : Descricao := VAR_MSG_FRETEMP;
      VAR_FATID_1005 : Descricao := VAR_MSG_INSUMOQUIMICOPS;
      VAR_FATID_1006 : Descricao := VAR_MSG_FRETEPQPS;
      VAR_FATID_1007 : Descricao := 'Venda de Couro Verde';
      VAR_FATID_1008 : Descricao := 'Frete de venda de couro verde';
      else Descricao := '??';
    end;
    if Geral.MB_Pergunta('� necess�rio cadastrar a conta do plano ' +
      'de contas em "Ferramentas > Op��es > Espec�ficos" para a conta"' +
      Descricao + '". Deseja faz�-lo agora?') = ID_YES then
        App_Jan.CadastroOpcoesBlueDerm();
  end //else Result := False;
end;

procedure TFmPrincipal2.Button10Click(Sender: TObject);
var
  GGXSubstituto, NivelExclusao: Integer;
begin
  DmProd.ObtemNovoGraGruXdeExcluido(EdGGXExluido.ValueVariant,
  GGXSubstituto, NivelExclusao);
  EdGGXSubstituto.ValueVariant := GGXSubstituto;
  EdNivelExclusao.ValueVariant := NivelExclusao;
  GGXSubstituto := EdGGXExluido.ValueVariant div 100;
  NivelExclusao := EdGGXExluido.ValueVariant mod 100;
  Geral.MB_Aviso(FormatFloat('0', GGXSubstituto) + '-' + FormatFloat('0', NivelExclusao));
end;

procedure TFmPrincipal2.Button11Click(Sender: TObject);
//var
  //Teste: LongWord;
begin
  //Teste := dmkPF.TestePorta();
  //Geral.MB_Aviso(Geral.FF0(Teste));
end;

procedure TFmPrincipal2.Button1Click(Sender: TObject);
begin
  {
  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE mpin mpi, mpinits its');
  Dmod.QrUpdM.SQL.Add('SET its.Fornece=mpi.Procedencia');
  Dmod.QrUpdM.SQL.Add('WHERE its.Controle=mpi.Controle');
  Dmod.QrUpdM.SQL.Add('AND its.Fornece=0');
  Dmod.QrUpdM.ExecSQL;
  }
  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE mpinits its');
  Dmod.QrUpdM.SQL.Add('SET its.PNF_Fat=its.PNF');
  Dmod.QrUpdM.SQL.Add('WHERE its.PNF_Fat=0');
  Dmod.QrUpdM.ExecSQL;
end;

procedure TFmPrincipal2.Button2Click(Sender: TObject);
const
  Endereco = 'R. DO NASCENTE, 679';
var
  Numero, Compl, Bairro, xLogr, nLogr, xRua: String;
begin
  Geral.SeparaEndereco(0, Endereco, Numero, Compl, Bairro, xLogr, nLogr, xRua);
  Geral.MB_Info(
  'Numero: ' + Numero + sLineBreak +
  'Compl: ' + Compl + sLineBreak +
  'Bairro: ' + Bairro + sLineBreak +
  'xLogr: ' + xLogr + sLineBreak +
  'nLogr: ' + nLogr + sLineBreak +
  'xRua: ' + xRua + sLineBreak + Endereco);
end;

procedure TFmPrincipal2.Button3Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEMail_SSL, FmEMail_SSL, afmoNegarComAviso) then
  begin
    FmEMail_SSL.ShowModal;
    FmEMail_SSL.Destroy;
  end;
end;

procedure TFmPrincipal2.Button5Click(Sender: TObject);
begin
  Geral.MB_Info(FormatFloat('#,###,###,##0.0000000', CO_VERSAO mod 10000));
end;

procedure TFmPrincipal2.Button6Click(Sender: TObject);
var
  Texto: WideString;
  I : Integer;
begin
  Texto := dmkPF.LoadFileToText(Edit1.Text);
  for I := 1 to Length(Texto) do
    if Ord(Texto[I]) < 32 then
      Geral.MB_Aviso(Geral.FF0(I) + ' de ' + Geral.FF0(Length(Texto)) +
        sLineBreak + 'Caracter = ' + Geral.FF0(Ord(Texto[I])));
end;

procedure TFmPrincipal2.Button7Click(Sender: TObject);
var
  ICMS_vCredICMSSN, ICMS_pCredSN, ICMS_VBC: Double;
begin
  ICMS_pCredSN := EdICMS_Alq.ValueVariant;
  ICMS_VBC     := EdICMS_BC.ValueVariant;
  ICMS_vCredICMSSN := Geral.RoundC(ICMS_pCredSN * ICMS_VBC / 100, 2);
  EdICMS_Val.ValueVariant := ICMS_vCredICMSSN;
end;

procedure TFmPrincipal2.Button8Click(Sender: TObject);
begin
{
  while not QrFlds.Eof do
  begin
    //? := UMyMod.BuscaEmLivreY_Def('spedefdicmsipiflds', ''Bloco', 'Registro', 'Numero', 'VersaoIni', 'VersaoFim', ImgTipo.SQLType, CodAtual);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipiflds', False, [
'Campo', 'Tipo', 'Tam',
'TObrig', 'Decimais', 'CObrig',
'EObrig', 'SObrig', 'DescrLin1',
'DescrLin2', 'DescrLin3', 'DescrLin4',
'DescrLin5', 'DescrLin6', 'DescrLin7',
'DescrLin8'], [
'Bloco', 'Registro', 'Numero', 'VersaoIni', 'VersaoFim'], [
Campo, Tipo, Tam,
TObrig, Decimais, CObrig,
EObrig, SObrig, DescrLin1,
DescrLin2, DescrLin3, DescrLin4,
DescrLin5, DescrLin6, DescrLin7,
DescrLin8], [
Bloco, Registro, Numero, VersaoIni, VersaoFim], UserDataAlterweb?, IGNORE?
    //
    QrFlds.Next;
  end;
}
end;

procedure TFmPrincipal2.Button9Click(Sender: TObject);
begin
  QrFlds.Close;
  UnDmkDAC_PF.AbreQuery(QrFlds, Dmod.MyDB);
end;

procedure TFmPrincipal2.CadastroDeMPEstagios();
begin
  if DBCheck.CriaFm(TFmMPEstagios, FmMPEstagios, afmoNegarComAviso) then
  begin
    FmMPEstagios.ShowModal;
    FmMPEstagios.Destroy;
  end;
end;

procedure TFmPrincipal2.UnidadesdeMedida1Click(Sender: TObject);
begin
  CadastroDeDefPecas();
end;

procedure TFmPrincipal2.Unidadesdemedida2Click(Sender: TObject);
begin
  CadastroDeDefPecas();
end;

procedure TFmPrincipal2.Usoeconsumo1Click(Sender: TObject);
begin
  Grade_Jan.CriaFormEntradaCab(tnfUsoEConsumo, True, 0);
end;

procedure TFmPrincipal2.UsoeconsumoSPED1Click(Sender: TObject);
begin
  EfdIcmsIpi_Jan.MostraFormEFD_C001();
end;

procedure TFmPrincipal2.CadastroDeDefPecas();
begin
  if DBCheck.CriaFm(TFmDefPecas, FmDefPecas, afmoNegarComAviso) then
  begin
    FmDefPecas.ShowModal;
    FmDefPecas.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroBancos;
begin
  if DBCheck.CriaFm(TFmBancos, FmBancos, afmoNegarComAviso) then
  begin
    FmBancos.ShowModal;
    FmBancos.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroCFOP2003;
begin
  if DBCheck.CriaFm(TFmCFOP2003, FmCFOP2003, afmoNegarComAviso) then
  begin
    FmCFOP2003.ShowModal;
    FmCFOP2003.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroDeArtigosGrupos();
begin
  if DBCheck.CriaFm(TFmArtigosGrupos, FmArtigosGrupos, afmoNegarComAviso) then
  begin
    FmArtigosGrupos.ShowModal;
    FmArtigosGrupos.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroDeCarteiras(TipoCarteira, ItemCarteira, x: Integer);
begin
  VAR_CARTEIRA := TipoCarteira;
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    if x <> 0 then
      FmCarteiras.LocCod(x, x);
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroDeContas;
begin
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.ShowModal;
    FmContas.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroDeSubGrupos;
begin
  if DBCheck.CriaFm(TFmSubgrupos, FmSubgrupos, afmoNegarComAviso) then
  begin
    FmSubgrupos.ShowModal;
    FmSubgrupos.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroEntiMP;
begin
  if DBCheck.CriaFm(TFmEntiMP, FmEntiMP, afmoNegarComAviso) then
  begin
    FmEntiMP.ShowModal;
    FmEntiMP.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroEquipes;
begin
  if DBCheck.CriaFm(TFmEquipes, FmEquipes, afmoNegarComAviso) then
  begin
    FmEquipes.ShowModal;
    FmEquipes.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroFornecedorMP;
begin
  if DBCheck.CriaFm(TFmEntiMP, FmEntiMP, afmoNegarComAviso) then
  begin
    FmEntiMP.ShowModal;
    FmEntiMP.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroGruposQuimicos;
begin
  if DBCheck.CriaFm(TFmGruposQuimicos, FmGruposQuimicos, afmoNegarComAviso) then
  begin
    FmGruposQuimicos.ShowModal;
    FmGruposQuimicos.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroImp;
begin
  if DBCheck.CriaFm(TFmPQImp, FmPQImp, afmoNegarComAviso) then
  begin
    FmPQImp.ShowModal;
    FmPQImp.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroMatriz;
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoAdmin) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroMPIn;
begin
  if DBCheck.CriaFm(TFmMPIn, FmMPIn, afmoNegarComAviso) then
  begin
    FmMPIn.ShowModal;
    FmMPIn.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroOpcoes;
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoSoMaster) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroPallets;
begin
  if DBCheck.CriaFm(TFmPallets, FmPallets, afmoNegarComAviso) then
  begin
    FmPallets.ShowModal;
    FmPallets.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroPortSerBal;
begin
  if DBCheck.CriaFm(TFmPortSerBal, FmPortSerBal, afmoNegarComAviso) then
  begin
    FmPortSerBal.ShowModal;
    FmPortSerBal.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroPQ(Codigo: Integer);
begin
  AppPF.CadastroInsumo(0, '', Codigo);
end;

procedure TFmPrincipal2.CadastroPQImp(FormOrig: String);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
    begin
      if LowerCase(FormOrig) = 'fmpq' then
        MyObjects.MostraPopUpDeBotao(PMRelatUC, FmPQ.SbImprime)
      else if LowerCase(FormOrig) = 'fmpqb' then
        MyObjects.MostraPopUpDeBotao(PMRelatUC, FmPQB.SbImprime)
      else
        MyObjects.MostraPopUpDeBotao(PMRelatUC, AGBRelatUC);
    end;
    fggxPQ:
    begin
}
      if DBCheck.CriaFm(TFmPQImp, FmPQImp, afmoNegarComAviso) then
      begin
        FmPQImp.ShowModal;
        FmPQImp.Destroy;
      end;
{
    end;
  end;
}
end;

procedure TFmPrincipal2.CadastroTintas(Numero, Codigo, Controle: Integer);
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmTintasCab2, FmTintasCab2, afmoNegarComAviso) then
      begin
        FmTintasCab2.ShowModal;
        FmTintasCab2.Destroy;
      end;
    fggxPQ:
}
      if DBCheck.CriaFm(TFmTintasCab, FmTintasCab, afmoNegarComAviso) then
      begin
        if Numero <> 0 then
        begin
          FmTintasCab.LocCod(Numero, Numero);
          FmTintasCab.QrTintasTin.Locate('Codigo', Codigo, []);
          FmTintasCab.QrTintasIts.Locate('Controle', Controle, []);
        end;
        FmTintasCab.ShowModal;
        FmTintasCab.Destroy;
      end;
{
  end;
}
end;

procedure TFmPrincipal2.Caleirocurtimento1Click(Sender: TObject);
begin
  PQ_PF.MostraFormFormulasImpBH();
end;

procedure TFmPrincipal2.CamposdaNFe1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeNewVer();
end;

procedure TFmPrincipal2.CadastroDeGrupos;
begin
  if DBCheck.CriaFm(TFmGrupos, FmGrupos, afmoNegarComAviso) then
  begin
    FmGrupos.ShowModal;
    FmGrupos.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroDeCarteira;
begin
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
  end;
end;

procedure TFmPrincipal2.CriaImpressaoDiversos(Indice: Integer);
begin
  {
  if DBCheck.CriaFm(TFmFormularios, FmFormularios, afmoNegarComAviso) then
  begin
    FmFormularios.ShowModal;
    FmFormularios.Destroy;
  end;
  }
end;

procedure TFmPrincipal2.CTes1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMOEnvCTeGer(0, 0, 0, 0);
end;

procedure TFmPrincipal2.D100Frete07088B0910112627571Click(Sender: TObject);
begin
  EfdIcmsIpi_Jan.MostraFormEFD_D001();
end;

procedure TFmPrincipal2.DefineVarsCliInt(CliInt: Integer);
begin
  DmodG.QrCliIntUni.Close;
  DmodG.QrCliIntUni.Params[0].AsInteger := CliInt;
  UnDmkDAC_PF.AbreQuery(DmodG.QrCliIntUni, Dmod.MyDB);
  //
  FEntInt := DmodG.QrCliIntUniCodigo.Value;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntUniCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  //
end;

procedure TFmPrincipal2.Devolues1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCMPTOut, FmCMPTOut, afmoNegarComAviso) then
  begin
    DmPediVda.ReopenTabelasPedido(); // ?? N�o precisa todas. Mudar?
    FmCMPTOut.ShowModal;
    FmCMPTOut.Destroy;
  end;
end;

procedure TFmPrincipal2.DevoluoDefinitivaAoEstoque1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSDvlCab(0);
end;

procedure TFmPrincipal2.Devoluonovo1Click(Sender: TObject);
begin
  UnPQx.GerenciaBaixaDevolucao(0);
end;

procedure TFmPrincipal2.CadastroDeContasSdoLista();
begin
  {
  if DBCheck.CriaFm(TFmContasSdoAll, FmContasSdoAll, afmoNegarComAviso) then
  begin
    FmContasSdoAll.ShowModal;
    FmContasSdoAll.Destroy;
  end;
  }
end;

function TFmPrincipal2.CadastroDeContasSdoSimples(Entidade, Conta: Integer): Boolean;
begin
{
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    if Entidade <> 0 then
    begin
      FmContasSdoUni.EdEntidade.Text := Geral.FF0(Entidade);
      FmContasSdoUni.CBEntidade.KeyValue := Entidade;
    end;
    if Conta <> 0 then
    begin
      FmContasSdoUni.EdCodigo.Text := Geral.FF0(Conta);
      FmContasSdoUni.CBCodigo.KeyValue := Conta;
    end;
    FmContas.ShowModal;
    Result := FmContasSdoUni.FExecutou;
    FmContas.Destroy;
  end;
  }
  Result := True;
end;

procedure TFmPrincipal2.CadastroDeConjuntos();
begin
  if DBCheck.CriaFm(TFmConjuntos, FmConjuntos, afmoNegarComAviso) then
  begin
    FmConjuntos.ShowModal;
    FmConjuntos.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroDeNiveisPlano();
begin
  if DBCheck.CriaFm(TFmPlaCtas, FmPlaCtas, afmoNegarComAviso) then
  begin
    FmPlaCtas.ShowModal;
    FmPlaCtas.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroDePlano();
begin
  if DBCheck.CriaFm(TFmPlano, FmPlano, afmoNegarComAviso) then
  begin
    FmPlano.ShowModal;
    FmPlano.Destroy;
  end;
end;

procedure TFmPrincipal2.CadastroDeImpObs();
begin
  if DBCheck.CriaFm(TFmImpObs, FmImpObs, afmoNegarComAviso) then
  begin
    FmImpObs.ShowModal;
    FmImpObs.Destroy;
  end;
end;

procedure TFmPrincipal2.RetornoCNAB;
begin
  // Compatibilidade
end;

procedure TFmPrincipal2.RetornoParaRetrabalhoEPosteriorReenvio1Click(
  Sender: TObject);
begin
  VS_PF.MostraFormVSRtbCab(0);
end;

procedure TFmPrincipal2.ShowHint(Sender: TObject);
begin
  {
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
  }
end;

procedure TFmPrincipal2.SkinMenu(Index: integer);
begin
case Index of
    0:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2003Blue;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2003Blue;
    end;
    1:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2003Classic;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2003Classic;
    end;
    2:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2003Olive;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2003Olive;
    end;
    3:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2003Silver;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2003Silver;
    end;
    4:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2007Luna;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2007Luna;
    end;
    5:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2007Obsidian;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2007Obsidian;
    end;
    6:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2007Silver;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2007Silver;
    end;
    7:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOfficeXP;
      AdvPreviewMenuOfficeStyler1.Style := psOfficeXP;
    end;
    8:
    begin
      AdvToolBarOfficeStyler1.Style     := bsWhidbeyStyle;
      AdvPreviewMenuOfficeStyler1.Style := psWhidbeyStyle;
    end;
    9:
    begin
      AdvToolBarOfficeStyler1.Style     := bsWindowsXP;
      AdvPreviewMenuOfficeStyler1.Style := psWindowsXP;
    end;
  end;
end;

procedure TFmPrincipal2.SPED20101Click(Sender: TObject);
begin
(*
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if MyObjects.CriaForm_AcessoTotal(TFmImportaSintegraGer, FmImportaSintegraGer) then
  begin
    FmImportaSintegraGer.ShowModal;
    FmImportaSintegraGer.Destroy;
  end;
*)
end;

procedure TFmPrincipal2.CadastroDeContasNiv();
begin
  if DBCheck.CriaFm(TFmContasNiv, FmContasNiv, afmoNegarComAviso) then
  begin
    FmContasNiv.ShowModal;
    FmContasNiv.Destroy;
  end;
end;


{
 Parei aqui!

 #####
 Ver hist�rico do estoque do Couro pronto:
SELECT smia.GraGruX, GraTamI, GraGruC, smia.Qtde
FROM stqmovitsa smia
LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX
WHERE smia.Ativo=1
AND ggx.GraGru1=365
 #######
 Tirar units e forms da NFe antiga
 #######
 Estoque de PQ pelo pre�o m�dio
 #######

 }

 {
 - Recep��o:
https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeRecepcao2
- Cancelamento:
https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeCancelamento2
- Consulta Recibo:
https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeRetRecepcao2
- Consulta Chave Acesso:
https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeConsulta2
- Inutiliza��o:
https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeInutilizacao2
- Consulta Status do Servi�o:
https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeStatusServico2
}

{ TODO : A��es para testar EFD no IDEAL }
{
UPDATE gragru1
SET NCM="12345678"
WHERE NCM=0
OR NCM=""
OR LEFT(NCM, 1) = "0"
OR LEFT(NCM, 2) = "00";
UPDATE entidades
SET PCodMunici=1501402
WHERE Codigo=690;
UPDATE entidades
SET Fantasia="CURTUME IDEAL"
WHERE Codigo IN (-1,-11)

}


{ TODO : Guia de Manifesta��o do usui�rio na janela de adi��o de evento na janela de pesquisa de NF-e }
{
Tabela[gratamcad].RegistroObrigatorio[1] n�o existe.
/*' + TMeuDB + '.QrSQL*/
/*********** SQL ***********/
INSERT INTO gratamcad SET
Codigo=1,
CodUsu=1,
Nome="�NICO",
PrintTam=0

/*****Query sem parametros*******/

/********* FIM SQL *********/
Lista de valores:
Codigo|CodUsu|Nome|PrintTam
1|1|"�NICO"|0
INSERT INTO gratamcad SET
Codigo=1,
CodUsu=1,
Nome="�NICO",
PrintTam=0

Tabela[gratamcad].RegistroObrigatorio[1] ERRO ao incluir.
/*' + TMeuDB + '.QrSQL*/
/*********** SQL ***********/
INSERT INTO gratamits SET
Codigo=1,
Controle=1,
Nome="�NICO",
PrintTam=0

/*****Query sem parametros*******/

/********* FIM SQL *********/
Lista de valores:
Codigo|Controle|Nome|PrintTam
0|0|"�NICO"|0
1|1|"�NICO"|0
INSERT INTO gratamits SET
Codigo=1,
Controle=1,
Nome="�NICO",
PrintTam=0

Tabela[gratamits].RegistroObrigatorio[1] ERRO ao incluir.
Tabela[nfecsitcab].Campo[chNFe]  Diferen�a:"KEY"
}

// Unable to create process: A opera��o solicitada requer eleva��o.

//O hor�rio de ver�o come�ou a ser vigorado a partir de um decreto da Presid�ncia da Rep�blica, fundamentado em informa��es dadas ao Minist�rio das Minas e Energia que adotam o hor�rio de ver�o a partir de estudos t�cnicos e indicando que Estados dever�o adota este hor�rio de ver�o. Este hor�rio foi devidamente regulado em 2008 pela Casa Civil da Presid�ncia da Rep�blica pelo decreto n� 6558 que inclusive definiu a data de in�cio e t�rmino do hor�rio de ver�o. O in�cio ser� sempre no terceiro domingo de outubro e o t�rmino do hor�rio no terceiro domingo de fevereiro com exce��o se a data coincidir com o carnaval e ai o t�rmino ser� no domingo seguinte, o que ocorrer� em 2015 e, por isso, o termino ser� dia 22 de fevereiro.

(*
http://www.rfidsystems.com.br/dura-3000
http://www.automacaomaringa.com.br/novo/rfid/
*)







////////////////////////////////////////////////////////////////////////////////







{
    procedure InjetaDadosMovimCod(const IMEI, MovimID, MovimNiv: Integer;
              const NodePai: TTreeNode);
    procedure InjetaDadosMovimTwn(const IMEI, MovimID, MovimNiv: Integer;
              const NodePai: TTreeNode);
    procedure InjetaItensDeQryIts(const TwnOuCod: String; const QryIts:
              TmySQLQuery; const TvPai: TTreeNode);
    procedure InjetaMovimTwn(QryCab: TmySQLQuery; NodePai: TTreeNode);
procedure TFmVSArvoreArtigos.InjetaDadosMovimCod(const IMEI, MovimID, MovimNiv:
  Integer; const NodePai: TTreeNode);
var
  QryCab, QryIts, QryMix: TmySQLQuery;
  MovimCod, Controle, IDFilho, NivFilho, MovimTwn, Indice, SerieFch, Ficha,
  Terceiro: Integer;
  Texto, Marca: String;
  TvTwn_07_or_08_to_08: TTreeNode;
begin
  QryCab := TmySQLQuery.Create(Dmod);
  try
  QryIts := TmySQLQuery.Create(Dmod);
  try
  QryMix := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryCab, Dmod.MyDB, [
    'SELECT DISTINCT MovimCod, MovimTwn',
    'FROM v s m o v i t s ',
    'WHERE SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    QryCab.First;
    while not QryCab.Eof do
    begin
      MovimCod := QryCab.FieldByName('MovimCod').AsInteger;
      MovimTwn := QryCab.FieldByName('MovimTwn').AsInteger;
      IDFilho  := QryCab.FieldByName('MovimID').AsInteger;
      NivFilho := QryCab.FieldByName('MovimNiv').AsInteger;
      //
      DefineImageIndexETextoMovimA(IDFilho, NivFilho, Indice, Texto);
      TvTwn_07_or_08_to_08 := InjetaNodeAtual('IME-C ', Texto, MovimCod,
        Indice, NodePai, emitIME_C);
      //
      if MovimTwn = 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QryIts, Dmod.MyDB, [
        'SELECT *  ',
        'FROM v s m o v i t s ',
        'WHERE MovimCod=' + Geral.FF0(MovimCod),
        'AND MovimTwn = 0',
        '']);
        QryIts.First;
        //
        InjetaItensDeQryIts('MovimCod', QryIts, TvTwn_07_or_08_to_08);
      end else
      begin
        InjetaMovimTwn(QryCab, NodePai);
      end;
      //
      QryCab.Next;
    end;
  finally
    QryMix.Free;
  end;
  finally
    QryIts.Free;
  end;
  finally
    QryCab.Free;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaDadosMovimTwn(const IMEI, MovimID,
  MovimNiv: Integer; const NodePai: TTreeNode);
var
  QryCab, QryIts, QryMix: TmySQLQuery;
  MovimTwn, SerieFch, Ficha, Terceiro: Integer;
  Marca: String;
  TvTwn_07_or_08_to_08: TTreeNode;
begin
  QryCab := TmySQLQuery.Create(Dmod);
  try
  QryIts := TmySQLQuery.Create(Dmod);
  try
  QryMix := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryCab, Dmod.MyDB, [
    'SELECT DISTINCT MovimTwn',
    'FROM v s m o v i t s ',
    'WHERE Controle=' + Geral.FF0(IMEI),
    '']);
    QryCab.First;
    while not QryCab.Eof do
    begin
      InjetaMovimTwn(QryCab, NodePai);
      //
      QryCab.Next;
    end;
  finally
    QryMix.Free;
  end;
  finally
    QryIts.Free;
  end;
  finally
    QryCab.Free;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaItensDeQryIts(const TwnOuCod: String; const
  QryIts: TmySQLQuery; const TvPai: TTreeNode);
var
  Controle, IDFilho, NivFilho, MovimTwn, Indice: Integer;
  TvAtual: TTreeNode;
  Texto: String;
begin
  QryIts.First;
  while not QryIts.Eof do
  begin
    Controle := QryIts.FieldByName('Controle').AsInteger;
    IDFilho  := QryIts.FieldByName('MovimID').AsInteger;
    NivFilho := QryIts.FieldByName('MovimNiv').AsInteger;
    MovimTwn := QryIts.FieldByName('MovimTwn').AsInteger;
    //
    DefineImageIndexETextoMovimA(IDFilho, NivFilho, Indice, Texto);
    TvAtual := InjetaNodeAtual('IME-I ', Texto, Controle, Indice, TvPai, emitIME_I);
    //
    case TEstqMovimNiv(NivFilho) of
(*
      eminSorcClass: ;       // 1
        // Nada
      eminDestClass:         // 2
      begin
        InjetaDadosMovimCod(Controle, IDFilho, NivFilho, TvAtual);
          if MovimTwn <> 0 then
           // Nunca passou por "MovimTwn <> 0" !!!!  testar se precisar quando passar!!!
            //InjetaDadosMovimTwn(Controle, IDFilho, NivFilho, TvAtual)
            InjetaDadosMovimCod(Controle, IDFilho, NivFilho, TvAtual)
          else
            InjetaDadosMovimCod(Controle, IDFilho, NivFilho, TvAtual);
      end;
*)
      eminSorcClass: ;       // 1
        // Nada
      eminDestClass,         // 2
      eminSorcPreReclas:     // 11
        InjetaDadosMovimCod(Controle, IDFilho, NivFilho, TvAtual);
      eminDestPreReclas:     // 12
      begin
        if MovimTwn <> 0 then
         // Nunca passou por "MovimTwn <> 0" !!!!  testar se precisar quando passar!!!
          //InjetaDadosMovimTwn(Controle, IDFilho, NivFilho, TvAtual)
          InjetaDadosMovimCod(Controle, IDFilho, NivFilho, TvAtual)
        else
          InjetaDadosMovimCod(Controle, IDFilho, NivFilho, TvAtual);
      end;

      else Geral.MB_Info('"MovimNiv" sem inje��o definida em "' + TwnOuCod +
        '"!' + sLineBreak +
        'MovimID: ' + Geral.FF0(IDFilho) + sLineBreak +
        'MovimNiv: ' + Geral.FF0(NivFilho) + sLineBreak +
        'IMEI: ' + Geral.FF0(Controle));
    end;
    //
    QryIts.Next;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaMovimTwn(QryCab: TmySQLQuery; NodePai:
  TTreeNode);
var
  MovimTwn, Indice: Integer;
  TvTwn_07_or_08_to_08: TTreeNode;
  QryIts: TmySQLQuery;
  Texto: String;
begin
  QryIts := TmySQLQuery.Create(Dmod);
  try
    MovimTwn := QryCab.FieldByName('MovimTwn').AsInteger;
    //
    Indice := CO_IDX_IMG_GEMEOS;
    Texto  := 'Par de G�meos';
    TvTwn_07_or_08_to_08 := InjetaNodeAtual('IME-P ', Texto, MovimTwn,
      Indice, NodePai, emitIME_P);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QryIts, Dmod.MyDB, [
    'SELECT *  ',
    'FROM v s m o v i t s ',
    'WHERE MovimTwn<> 0',
    'AND MovimTwn=' + Geral.FF0(MovimTwn),
    '']);
    //
    InjetaItensDeQryIts('MovimTwn', QryIts, TvTwn_07_or_08_to_08);
  finally
    QryIts.Free;
  end;
end;
}

{ Meza 2015-09-03
SELECT Controle, DataHora, ValorT, AreaM2,
ValorT/AreaM2 Custo
FROM v s m o v i t s
WHERE ValorT/AreaM2 BETWEEN 18 AND 19
OR DataHora >= "2015-09-01"
}

(*

�rea financeira trata operacionalmente das finan�as das empresas, sendo que seu
setor esta normalmente dividido em duas se��es chaves que s�o o contas a pagar
e a receber. Contas a pagar como o pr�prio nome j� diz, a pessoa que cuida
dessa carteira deve ter o controle sobre as quest�es de pagamento da empresa
deve trabalhar em conjunto com o contas a receber que � onde os recursos s�o
devidamente captados para poss�veis despesas, investimentos entre outros.
Essas duas fun��es est�o intimamente ligadas com o gestor de contas chamado
controller.
A �rea fiscal trabalha de maneira a cumprir as obriga��es com o Estado em todas
as esferas (municipal, estadual e federal) trabalha diretamente ligago com a
contabilidade isso quando ao inv�s do um advogado n�o � o pr�prio contador quem
o faz pos condiz a sua �rea.
�rea cont�bil � onde as informa��es de todas as �reas da empresa s�o
processadas resultando no devido controle da empresa se portando como material
de an�lise para a tomada de decis�es dos gestores.

Todas essas �reas expostas nesta quest�o est�o ligadas ao controller, podendo
ter um s� departamento chamado de Administrativo.


Obs>: Lembrando que apesar de todos do setor serem subordinados ao controller ou gerente administrativo. O contador trabalha de forma independente, pois em legisla��o prev� que ele responda solidariamente com o respons�vel legal da empresa. Pois, muitos administradores querem administrar a empresa de modo contrario a legisla��o. Quando isso acontece que o contado n�o � ouvido ele tem imediatamente que comunicar ao conselho de contabilidade para que venha a se proteger de problemas futuros mediante a sua classe.


espero ter lhe ajudado
*)


end.


