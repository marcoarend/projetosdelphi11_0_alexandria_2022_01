object FmMPClasMulSub: TFmMPClasMulSub
  Left = 339
  Top = 185
  Caption = 'COU-CLASS-006 :: Gera'#231#227'o M'#250'ltipla de Sub-produtos'
  ClientHeight = 733
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 577
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 200
        Top = 8
        Width = 142
        Height = 13
        Caption = 'Centro de estoque de destino:'
      end
      object Label12: TLabel
        Left = 8
        Top = 8
        Width = 188
        Height = 13
        Caption = 'Data e hora do lan'#231'amento no estoque:'
      end
      object EdStqCenCad: TdmkEditCB
        Left = 200
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdStqCenCadChange
        DBLookupComboBox = CBStqCenCad
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStqCenCad: TdmkDBLookupComboBox
        Left = 256
        Top = 24
        Width = 409
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsStqCenCad
        TabOrder = 3
        dmkEditCB = EdStqCenCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object BtRateia: TBitBtn
        Left = 910
        Top = 8
        Width = 90
        Height = 40
        Caption = '&Rateia'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtRateiaClick
      end
      object TPData: TdmkEditDateTimePicker
        Left = 8
        Top = 24
        Width = 133
        Height = 21
        Date = 40638.507592673610000000
        Time = 40638.507592673610000000
        TabOrder = 0
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdHora: TdmkEdit
        Left = 144
        Top = 24
        Width = 53
        Height = 21
        TabOrder = 1
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object RGData: TRadioGroup
        Left = 672
        Top = 1
        Width = 233
        Height = 50
        Caption = ' Data de gera'#231#227'o: '
        ItemIndex = 0
        Items.Strings = (
          'Data informada'
          'Data da entrada da mat'#233'ria-prima + 2 dias'
          'Data da entrada da mat'#233'ria-prima + 3 dias')
        TabOrder = 5
      end
    end
    object DBGMPInClasMul: TDBGrid
      Left = 0
      Top = 52
      Width = 1008
      Height = 296
      Align = alTop
      DataSource = DsMPInClasMul
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Estq_Pecas'
          Title.Caption = 'Estq. Pe'#231'as'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Estq_Peso'
          Title.Caption = 'Estq. kg'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bxa_Pecas'
          Title.Caption = 'Bxa. Pe'#231'as'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bxa_M2'
          Title.Caption = 'Bxa. m'#178
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bxa_Peso'
          Title.Caption = 'Bxa peso'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data/Hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Clas_Pecas'
          Title.Caption = 'Clas. Pe'#231'as'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Clas_M2'
          Title.Caption = 'Clas. m'#178
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Clas_Peso'
          Title.Caption = 'Clas. Peso'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'StqMovIts'
          Title.Caption = 'ID Estoque'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Clas_P2'
          Title.Caption = 'Clas. ft'#178
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bxa_P2'
          Title.Caption = 'Bxa ft'#178
          Width = 60
          Visible = True
        end>
    end
    object Panel4: TPanel
      Left = 0
      Top = 348
      Width = 1008
      Height = 140
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object Label1: TLabel
        Left = 0
        Top = 0
        Width = 288
        Height = 16
        Align = alTop
        Alignment = taCenter
        Caption = 'Sub-produtos Gerados da Marca Selecionada'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBGrid3: TDBGrid
        Left = 0
        Top = 16
        Width = 1008
        Height = 124
        Align = alClient
        DataSource = DsMPInSubPMul
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'GGX_Ger'
            Title.Caption = 'Reduzido'
            Width = 51
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD'
            Title.Caption = 'Nome do Produto'
            Width = 329
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SIGLA'
            Title.Caption = 'Sigla'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Title.Caption = 'Peso kg'
            Visible = True
          end>
      end
    end
    object MeAvisos: TMemo
      Left = 0
      Top = 488
      Width = 1008
      Height = 89
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = [fsBold]
      Lines.Strings = (
        'Memo1')
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 3
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 415
        Height = 32
        Caption = 'Gera'#231#227'o M'#250'ltipla de Sub-produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 415
        Height = 32
        Caption = 'Gera'#231#227'o M'#250'ltipla de Sub-produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 415
        Height = 32
        Caption = 'Gera'#231#227'o M'#250'ltipla de Sub-produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 625
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 669
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 124
        Top = 16
        Width = 293
        Height = 13
        Caption = 'Quantidade de registros que ter'#227'o seus sub-produtos gerados:'
      end
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object EdRegA: TdmkEdit
        Left = 420
        Top = 12
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object DsMPInClasMul: TDataSource
    DataSet = QrMPInClasMul
    Left = 36
    Top = 4
  end
  object QrMPInClasMul: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMPInClasMulAfterOpen
    BeforeClose = QrMPInClasMulBeforeClose
    AfterScroll = QrMPInClasMulAfterScroll
    OnCalcFields = QrMPInClasMulCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM mpinclasmul')
    Left = 8
    Top = 4
    object QrMPInClasMulCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mpinclasmul.Codigo'
    end
    object QrMPInClasMulControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'mpinclasmul.Controle'
    end
    object QrMPInClasMulData: TDateField
      FieldName = 'Data'
      Origin = 'mpinclasmul.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPInClasMulClienteI: TIntegerField
      FieldName = 'ClienteI'
      Origin = 'mpinclasmul.ClienteI'
    end
    object QrMPInClasMulProcedencia: TIntegerField
      FieldName = 'Procedencia'
      Origin = 'mpinclasmul.Procedencia'
    end
    object QrMPInClasMulTransportadora: TIntegerField
      FieldName = 'Transportadora'
      Origin = 'mpinclasmul.Transportadora'
    end
    object QrMPInClasMulMarca: TWideStringField
      FieldName = 'Marca'
      Origin = 'mpinclasmul.Marca'
    end
    object QrMPInClasMulLote: TWideStringField
      FieldName = 'Lote'
      Origin = 'mpinclasmul.Lote'
      Size = 11
    end
    object QrMPInClasMulPecasOut: TFloatField
      FieldName = 'PecasOut'
      Origin = 'mpinclasmul.PecasOut'
    end
    object QrMPInClasMulPecasSal: TFloatField
      FieldName = 'PecasSal'
      Origin = 'mpinclasmul.PecasSal'
    end
    object QrMPInClasMulEstq_Pecas: TFloatField
      FieldName = 'Estq_Pecas'
      Origin = 'mpinclasmul.Estq_Pecas'
    end
    object QrMPInClasMulEstq_M2: TFloatField
      FieldName = 'Estq_M2'
      Origin = 'mpinclasmul.Estq_M2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrMPInClasMulEstq_P2: TFloatField
      FieldName = 'Estq_P2'
      Origin = 'mpinclasmul.Estq_P2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrMPInClasMulEstq_Peso: TFloatField
      FieldName = 'Estq_Peso'
      Origin = 'mpinclasmul.Estq_Peso'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrMPInClasMulStqMovIts: TIntegerField
      FieldName = 'StqMovIts'
      Origin = 'mpinclasmul.StqMovIts'
    end
    object QrMPInClasMulClas_Pecas: TFloatField
      FieldName = 'Clas_Pecas'
      Origin = 'mpinclasmul.Clas_Pecas'
    end
    object QrMPInClasMulClas_M2: TFloatField
      FieldName = 'Clas_M2'
      Origin = 'mpinclasmul.Clas_M2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrMPInClasMulClas_P2: TFloatField
      FieldName = 'Clas_P2'
      Origin = 'mpinclasmul.Clas_P2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrMPInClasMulClas_Peso: TFloatField
      FieldName = 'Clas_Peso'
      Origin = 'mpinclasmul.Clas_Peso'
    end
    object QrMPInClasMulDebCtrl: TIntegerField
      FieldName = 'DebCtrl'
      Origin = 'mpinclasmul.DebCtrl'
    end
    object QrMPInClasMulDebStqCenCad: TIntegerField
      FieldName = 'DebStqCenCad'
      Origin = 'mpinclasmul.DebStqCenCad'
    end
    object QrMPInClasMulDebGraGruX: TIntegerField
      FieldName = 'DebGraGruX'
      Origin = 'mpinclasmul.DebGraGruX'
    end
    object QrMPInClasMulPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'mpinclasmul.Pecas'
    end
    object QrMPInClasMulPLE: TFloatField
      FieldName = 'PLE'
      Origin = 'mpinclasmul.PLE'
    end
    object QrMPInClasMulBxa_Pecas: TFloatField
      FieldName = 'Bxa_Pecas'
    end
    object QrMPInClasMulBxa_M2: TFloatField
      FieldName = 'Bxa_M2'
    end
    object QrMPInClasMulBxa_P2: TFloatField
      FieldName = 'Bxa_P2'
    end
    object QrMPInClasMulBxa_Peso: TFloatField
      FieldName = 'Bxa_Peso'
    end
    object QrMPInClasMulGerBxaEstq: TIntegerField
      FieldName = 'GerBxaEstq'
    end
    object QrMPInClasMulClas_Qtde: TFloatField
      FieldName = 'Clas_Qtde'
    end
    object QrMPInClasMulBxa_Qtde: TFloatField
      FieldName = 'Bxa_Qtde'
    end
    object QrMPInClasMulDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrMPInClasMulNO_TIPIFICACAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPIFICACAO'
      Size = 50
      Calculated = True
    end
    object QrMPInClasMulNO_ANIMAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_ANIMAL'
      Size = 50
      Calculated = True
    end
    object QrMPInClasMulGraGruX: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'GraGruX'
      Calculated = True
    end
    object QrMPInClasMulTipificacao: TIntegerField
      FieldName = 'Tipificacao'
    end
    object QrMPInClasMulAnimal: TIntegerField
      FieldName = 'Animal'
    end
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(Estq_Pecas) Estq_Pecas, '
      'SUM(Estq_M2) Estq_M2, '
      'SUM(Estq_P2) Estq_P2, '
      'SUM(Estq_Peso) Estq_Peso,'
      'SUM(Clas_Pecas) Clas_Pecas, '
      'SUM(Clas_M2) Clas_M2, '
      'SUM(Clas_P2) Clas_P2, '
      'SUM(Clas_Peso) Clas_Peso'
      'FROM mpinclasmul')
    Left = 68
    Top = 4
    object QrSomaEstq_Pecas: TFloatField
      FieldName = 'Estq_Pecas'
    end
    object QrSomaEstq_M2: TFloatField
      FieldName = 'Estq_M2'
    end
    object QrSomaEstq_P2: TFloatField
      FieldName = 'Estq_P2'
    end
    object QrSomaEstq_Peso: TFloatField
      FieldName = 'Estq_Peso'
    end
    object QrSomaClas_Pecas: TFloatField
      FieldName = 'Clas_Pecas'
    end
    object QrSomaClas_M2: TFloatField
      FieldName = 'Clas_M2'
    end
    object QrSomaClas_P2: TFloatField
      FieldName = 'Clas_P2'
    end
    object QrSomaClas_Peso: TFloatField
      FieldName = 'Clas_Peso'
    end
  end
  object DsSoma: TDataSource
    DataSet = QrSoma
    Left = 92
    Top = 4
  end
  object QrStqCenCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 120
    Top = 8
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 148
    Top = 4
  end
  object QrMPSubProd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.GerBxaEstq GER_GerBxaEstq, msp.* '
      'FROM mpsubprod msp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=msp.GGX_Ger'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.Gragru1'
      'WHERE msp.GGX_Ori=:P0')
    Left = 728
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPSubProdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPSubProdNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrMPSubProdGGX_Ori: TIntegerField
      FieldName = 'GGX_Ori'
    end
    object QrMPSubProdGGX_Ger: TIntegerField
      FieldName = 'GGX_Ger'
    end
    object QrMPSubProdGrndzaBase: TSmallintField
      FieldName = 'GrndzaBase'
    end
    object QrMPSubProdFator: TFloatField
      FieldName = 'Fator'
    end
    object QrMPSubProdArredonda: TFloatField
      FieldName = 'Arredonda'
    end
    object QrMPSubProdGER_GerBxaEstq: TSmallintField
      FieldName = 'GER_GerBxaEstq'
    end
  end
  object QrMPInSubPMul: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad, '
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, '
      'med.Grandeza, smia.* '
      'FROM locbderm__1.mpinsubpmul smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GGX_Ger'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GGX_Ger')
    Left = 784
    Top = 8
    object QrMPInSubPMulNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 120
    end
    object QrMPInSubPMulSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrMPInSubPMulQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrMPInSubPMulPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrMPInSubPMulPeso: TFloatField
      FieldName = 'Peso'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrMPInSubPMulGGX_Ger: TIntegerField
      FieldName = 'GGX_Ger'
      Required = True
    end
  end
  object DsMPInSubPMul: TDataSource
    DataSet = QrMPInSubPMul
    Left = 812
    Top = 8
  end
end
