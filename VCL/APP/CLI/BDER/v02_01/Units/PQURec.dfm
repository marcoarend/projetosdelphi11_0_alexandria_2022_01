object FmPQURec: TFmPQURec
  Left = 339
  Top = 185
  Caption = 'QUI-INSUM-008 :: Edi'#231#227'o de Insumo em Previs'#227'o'
  ClientHeight = 315
  ClientWidth = 496
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 496
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 1008
    object GB_R: TGroupBox
      Left = 448
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 960
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 400
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 912
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 375
        Height = 32
        Caption = 'Edi'#231#227'o de Insumo em Previs'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 375
        Height = 32
        Caption = 'Edi'#231#227'o de Insumo em Previs'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 375
        Height = 32
        Caption = 'Edi'#231#227'o de Insumo em Previs'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 201
    Width = 496
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 435
    ExplicitWidth = 1008
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 492
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1004
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 245
    Width = 496
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 479
    ExplicitWidth = 1008
    object PnSaiDesis: TPanel
      Left = 350
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 862
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 348
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 860
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 496
    Height = 153
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 1008
    ExplicitHeight = 277
    object LaPQ: TLabel
      Left = 8
      Top = 4
      Width = 37
      Height = 13
      Caption = 'Insumo:'
    end
    object SbPQ: TSpeedButton
      Left = 444
      Top = 20
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbPQClick
    end
    object Label3: TLabel
      Left = 8
      Top = 96
      Width = 100
      Height = 13
      Caption = 'Peso kg pr'#233'-definido:'
    end
    object EdPQ: TdmkEditCB
      Left = 8
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdPQRedefinido
      DBLookupComboBox = CBPQ
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPQ: TdmkDBLookupComboBox
      Left = 64
      Top = 20
      Width = 377
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPQ
      TabOrder = 1
      dmkEditCB = EdPQ
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object PnInsumo: TPanel
      Left = 8
      Top = 48
      Width = 457
      Height = 45
      TabOrder = 2
      Visible = False
      object Label5: TLabel
        Left = 364
        Top = 4
        Width = 56
        Height = 13
        Caption = 'kg estoque:'
        FocusControl = DBEdPesoEstq
      end
      object Label4: TLabel
        Left = 280
        Top = 4
        Width = 65
        Height = 13
        Caption = 'kg liq. embal.:'
        FocusControl = DBEdKgLiqEmb
      end
      object Label2: TLabel
        Left = 4
        Top = 4
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
        FocusControl = DBEdIQ
      end
      object DBEdIQ: TDBEdit
        Left = 4
        Top = 20
        Width = 48
        Height = 21
        TabStop = False
        DataField = 'IQ'
        DataSource = DsPQ
        TabOrder = 0
      end
      object DBEdPesoEstq: TDBEdit
        Left = 364
        Top = 20
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'PesoEstq'
        DataSource = DsPQ
        TabOrder = 1
      end
      object DBEdKgLiqEmb: TDBEdit
        Left = 280
        Top = 20
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'KgLiqEmb'
        DataSource = DsPQ
        TabOrder = 2
      end
      object DBEdNomeIQ: TDBEdit
        Left = 56
        Top = 20
        Width = 221
        Height = 21
        TabStop = False
        DataField = 'NomeIQ'
        DataSource = DsPQ
        TabOrder = 3
      end
    end
    object EdPesComprPreDef: TdmkEdit
      Left = 8
      Top = 112
      Width = 105
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq_.Codigo, pq_.Nome, pq_.IQ, '
      'IF(iq_.Tipo=0, iq_.RazaoSocial, iq_.Nome) NomeIQ, '
      'pqc.KgLiqEmb, pqc.Peso PesoEstq '
      'FROM pq pq_ '
      'LEFT JOIN entidades iq_ ON pq_.IQ=iq_.Codigo '
      'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo AND pqc.CI=140 '
      'WHERE pq_.Codigo>0 '
      'AND pq_.GrupoQuimico & 55 '
      'ORDER BY pq_.Nome '
      '  ')
    Left = 216
    Top = 44
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPQIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQNomeIQ: TWideStringField
      FieldName = 'NomeIQ'
      Size = 100
    end
    object QrPQKgLiqEmb: TFloatField
      FieldName = 'KgLiqEmb'
    end
    object QrPQPesoEstq: TFloatField
      FieldName = 'PesoEstq'
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 216
    Top = 92
  end
  object QrDuplic: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq_.Codigo, pq_.Nome, pq_.IQ, '
      'IF(iq_.Tipo=0, iq_.RazaoSocial, iq_.Nome) NomeIQ, '
      'pqc.KgLiqEmb, pqc.Peso PesoEstq '
      'FROM pq pq_ '
      'LEFT JOIN entidades iq_ ON pq_.IQ=iq_.Codigo '
      'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo AND pqc.CI=140 '
      'WHERE pq_.Codigo>0 '
      'AND pq_.GrupoQuimico & 55 '
      'ORDER BY pq_.Nome '
      '  ')
    Left = 304
    Top = 48
    object QrDuplicInsumo: TIntegerField
      FieldName = 'Insumo'
    end
  end
end
