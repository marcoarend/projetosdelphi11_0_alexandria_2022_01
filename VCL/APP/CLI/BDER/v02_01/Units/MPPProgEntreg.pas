unit MPPProgEntreg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkDBLookupComboBox, dmkEditCB, Vcl.Mask, dmkDBGridZTO;

type
  TFmMPPProgEntreg = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    QrListaSetores: TMySQLQuery;
    DsListaListaSetores: TDataSource;
    EdSetor: TdmkEditCB;
    CBSetor: TdmkDBLookupComboBox;
    Label2: TLabel;
    QrListaSetoresCodigo: TIntegerField;
    QrListaSetoresNome: TWideStringField;
    QrListaSetoresm2Dia: TFloatField;
    Label1: TLabel;
    DBGrid1: TdmkDBGridZTO;
    QrMPVIts: TMySQLQuery;
    DsMPVIts: TDataSource;
    QrMPVItsEntrega: TDateField;
    QrMPVItsM2Pedido: TFloatField;
    QrMPVItsPercentual: TFloatField;
    EdM2DiaAcabPCP: TdmkEdit;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure EdSetorRedefinido(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdM2DiaAcabPCPRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure DenineM2Dia();
  public
    { Public declarations }
    procedure SetaAcabammento();
    procedure ReopenMPVIts();
  end;

  var
  FmMPPProgEntreg: TFmMPPProgEntreg;

implementation

uses UnMyObjects, DmkDAC_PF, Module, ModuleGeral, UnApp_Jan;

{$R *.DFM}

procedure TFmMPPProgEntreg.BtOKClick(Sender: TObject);
var
  DtHrAbert, DtHrFecha, ZtatusDtH: String;
  Codigo, Local, NrOP, SeqGrupo, NrReduzidoOP, OVcYnsMed, OVcYnsChk, LimiteChk, LimiteMed, ZtatusIsp, ZtatusMot: Integer;
  SQLType: TSQLType;
begin
{
  SQLType := ImgTipo.SQLType?;
  Codigo         := ;
  Local          := ;
  NrOP           := ;
  SeqGrupo       := ;
  NrReduzidoOP   := ;
  DtHrAbert      := ;
  DtHrFecha      := ;
  OVcYnsMed      := ;
  OVcYnsChk      := ;
  LimiteChk      := ;
  LimiteMed      := ;
  ZtatusIsp      := ;
  ZtatusDtH      := ;
  ZtatusMot      := ;

  //
? := UMyMod.BuscaEmLivreY_Def('ovgispgercab', 'Codigo', SQLType, CodAtual?);
ou > ? := UMyMod.BPGS1I32('ovgispgercab', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'ovgispgercab', auto_increment?[
'Local', 'NrOP', 'SeqGrupo',
'NrReduzidoOP', 'DtHrAbert', 'DtHrFecha',
'OVcYnsMed', 'OVcYnsChk', 'LimiteChk',
'LimiteMed', 'ZtatusIsp', 'ZtatusDtH',
'ZtatusMot'], [
'Codigo'], [
Local, NrOP, SeqGrupo,
NrReduzidoOP, DtHrAbert, DtHrFecha,
OVcYnsMed, OVcYnsChk, LimiteChk,
LimiteMed, ZtatusIsp, ZtatusDtH,
ZtatusMot], [
Codigo], UserDataAlterweb?, IGNORE?
}
end;

procedure TFmMPPProgEntreg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPPProgEntreg.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  RectBrush1_Width, RectBrush2_Width, HalfColWidth: Integer;
  s: String;
  CorBorda, CorBrush, CorFonte: TColor;
  Perc: Double;
begin
  if Column.FieldName = 'DrawPB_Percentual' then
  begin
    HalfColWidth := Column.Width div 2;
    if QrMPVItsPercentual.Value > 100 then
    begin
      CorBorda := $001800C1;
      CorFonte := $000D0062;
      CorBrush := $00CEC7FF;
    end else
    begin
      CorBorda := $003F9229;
      CorFonte := $001B3F12;
      CorBrush := $00CEEFC6;
    end;
    with DBGrid1.Canvas do
    begin
      Perc := QrMPVItsPercentual.Value;
      //Perc := 197 + QrMPVIts.RecNo;
      if Perc > 200 then
        Perc := 200;
      if (Perc <= 100) then
      begin
        RectBrush1_Width := Trunc(((HalfColWidth - 3) * Perc) / 100) + 5;
        RectBrush2_Width := 0;
      end else
      begin
        RectBrush1_Width := HalfColWidth + 2;
        RectBrush2_Width := Trunc((HalfColWidth * (Perc - 100)) / 100);
      end;
      //
      s := IntToStr(Round(QrMPVItsPercentual.Value)) + '%';

      // draw Rectangle Fundo 1
      Brush.Color := clWindow;
      Pen.Color := CorBorda; //clRed;
      Rectangle(Rect.Left + 2, Rect.Top + 2, Rect.Left + HalfColWidth, Rect.Top + 15);

      if RectBrush2_Width > 0 then
      begin
        // draw Rectangle Fundo 2
        Brush.Color := clWindow;
        Pen.Color := CorBorda; //clRed;
        Rectangle(Rect.Left + HalfColWidth, Rect.Top + 2, Rect.Left + Column.Width - 2, Rect.Top + 15);
      end;

      // draw Rectangle Percentual 1
      Brush.Color := CorBrush; //clLime;
      //Pen.Color := clRed;
      Rectangle(Rect.Left + 2, Rect.Top + 2, Rect.Left + RectBrush1_Width - 2, Rect.Top + 15);

      if RectBrush2_Width > 0 then
      begin
        // draw Rectangle Percentual 2
        Brush.Color := CorBrush; //clLime;
        //Pen.Color := clRed;
        Rectangle(Rect.Left + HalfColWidth, Rect.Top + 2, Rect.Left + HalfColWidth + RectBrush2_Width - 2, Rect.Top + 15);
      end;
      // Draw Text
      Brush.Style := bsClear;
      Font.Size := 8;
      Font.Color := CorFonte;
      TextOut((Rect.Left + Column.Width div 4) - TextWidth(s) div 2, Rect.Top + 2, s);
      //
    end;
  end;
end;

procedure TFmMPPProgEntreg.DenineM2Dia;
begin
  Dmod.ReopenControle();
  EdM2DiaAcabPCP.ValueVariant := Dmod.QrControleM2DiaAcabPCP.Value;
end;

procedure TFmMPPProgEntreg.EdM2DiaAcabPCPRedefinido(Sender: TObject);
begin
  ReopenMPVIts();
end;

procedure TFmMPPProgEntreg.EdSetorRedefinido(Sender: TObject);
begin
  ReopenMPVIts();
end;

procedure TFmMPPProgEntreg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPPProgEntreg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DenineM2Dia();
end;

procedure TFmMPPProgEntreg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPPProgEntreg.ReopenMPVIts();
var
  sM2Dia, Hoje: String;
begin
  //sM2Dia := Geral.FFT_Dot(QrListaSetoresm2Dia.Value, 2, siPositivo);
  sM2Dia := Geral.FFT_Dot(EdM2DiaAcabPCP.ValueVariant, 2, siPositivo);
  Hoje   := Geral.FDT(DModG.ObtemAgora(), 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMPVIts, Dmod.MyDB, [
  'SELECT Entrega, SUM(M2Pedido) M2Pedido,',
  '(SUM(M2Pedido) / ' + sM2Dia + ' * 100) Percentual ',
  'FROM mpvits',
  'WHERE Entrega >= "' + Hoje + '"',
  'GROUP BY Entrega',
  'ORDER BY Entrega ',
  '']);
  //
end;

procedure TFmMPPProgEntreg.SetaAcabammento();
begin
(*
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrListaSetores, Dmod.MyDB, [
  'SELECT Codigo, Nome, m2Dia  ',
  'FROM listasetores  ',
  'ORDER BY TpReceita DESC, m2Dia DESC ',
  '']);
  EdSetor.ValueVariant := QrListaSetoresCodigo.Value;
  CBSetor.KeyValue     := QrListaSetoresCodigo.Value;
*)
end;

procedure TFmMPPProgEntreg.SpeedButton1Click(Sender: TObject);
begin
  App_Jan.CadastroOpcoesBlueDerm();
end;

end.
