object FmMPReclasImp: TFmMPReclasImp
  Left = 339
  Top = 185
  Caption = 'COU-CLASS-005 :: Gerenciamento de Reclassifica'#231#227'o'
  ClientHeight = 510
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 354
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 48
      Width = 784
      Height = 306
      Align = alClient
      DataSource = DsEstq
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_PRD'
          Title.Caption = 'Nome Produto'
          Width = 293
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SIGLA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QTDE'
          Title.Caption = 'Qtd. Estq.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PECAS'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PESO'
          Title.Caption = 'Peso kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AREAM2'
          Title.Caption = #193'rea m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AREAP2'
          Title.Caption = #193'rea ft'#178
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label3: TLabel
        Left = 8
        Top = 4
        Width = 79
        Height = 13
        Caption = 'Tipo de Produto:'
      end
      object EdPrdGrupTip: TdmkEditCB
        Left = 8
        Top = 20
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPrdGrupTipChange
        DBLookupComboBox = CBPrdGrupTip
        IgnoraDBLookupComboBox = False
      end
      object CBPrdGrupTip: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 709
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsPrdGrupTip
        TabOrder = 1
        dmkEditCB = EdPrdGrupTip
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 420
        Height = 32
        Caption = 'Gerenciamento de Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 420
        Height = 32
        Caption = 'Gerenciamento de Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 420
        Height = 32
        Caption = 'Gerenciamento de Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 402
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 446
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
      end
    end
  end
  object QrEstq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT smia.Empresa,  smia.GraGruX, SUM(smia.Qtde) QTDE, SUM(smi' +
        'a.Pecas) PECAS, '
      
        'SUM(smia.Peso) PESO, SUM(smia.AreaM2) AREAM2, SUM(smia.AreaP2) A' +
        'REAP2,'
      ' gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad, '
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'LEFT JOIN nfecaba nfa ON nfa.FatID=smia.Tipo AND'
      '  nfa.FatNum=smia.OriCodi AND nfa.Empresa=smia.Empresa'
      'WHERE smia.Tipo =1'
      'AND pgt.Codigo=:P0'
      'AND smia.DataHora > :P1 AND smia.DataHora <:P2'
      'GROUP BY smia.Empresa, smia.GraGruX'
      'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GraGruX, DataHora')
    Left = 12
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEstqEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstqGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstqQTDE: TFloatField
      FieldName = 'QTDE'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrEstqPECAS: TFloatField
      FieldName = 'PECAS'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrEstqPESO: TFloatField
      FieldName = 'PESO'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrEstqAREAM2: TFloatField
      FieldName = 'AREAM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEstqAREAP2: TFloatField
      FieldName = 'AREAP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEstqNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrEstqNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 120
    end
    object QrEstqPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrEstqUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrEstqNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrEstqSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrEstqNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrEstqNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrEstqGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrEstqGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrEstqGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstqGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
  end
  object DsEstq: TDataSource
    DataSet = QrEstq
    Left = 40
    Top = 12
  end
  object QrPrdGrupTip: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM prdgruptip'
      'ORDER BY Nome')
    Left = 140
    Top = 188
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 168
    Top = 188
  end
end
