unit Prosoft1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, ComCtrls, DmkDAC_PF,
  dmkEditDateTimePicker, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkCheckGroup, frxClass, frxDBSet, UnDmkProcFunc, Variants,
  dmkImage, UnDmkEnums;

type
  TFmProsoft1 = class(TForm)
    Panel1: TPanel;
    Panel4: TPanel;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    PnGraCusPrc: TPanel;
    Label7: TLabel;
    EdGraCusPrc: TdmkEditCB;
    CBGraCusPrc: TdmkDBLookupComboBox;
    Panel21: TPanel;
    QrCabA: TmySQLQuery;
    QrCabAemit_CNPJ: TWideStringField;
    QrCabAemit_CPF: TWideStringField;
    QrCabACNPJ_CPF_emit: TWideStringField;
    QrCabAide_dEmi: TDateField;
    QrCabAide_dSaiEnt: TDateField;
    QrCabAide_mod: TSmallintField;
    QrCabAide_serie: TIntegerField;
    QrCabAide_nNF: TIntegerField;
    QrCabAICMSTot_vProd: TFloatField;
    QrCabAICMSTot_vBC: TFloatField;
    QrCabAICMSTot_vICMS: TFloatField;
    QrCabAICMSTot_vBCST: TFloatField;
    QrCabAICMSTot_vST: TFloatField;
    QrCabAICMSTot_vFrete: TFloatField;
    QrCabAICMSTot_vSeg: TFloatField;
    QrCabAICMSTot_vDesc: TFloatField;
    QrCabAICMSTot_vII: TFloatField;
    QrCabAICMSTot_vIPI: TFloatField;
    QrCabAICMSTot_vPIS: TFloatField;
    QrCabAICMSTot_vCOFINS: TFloatField;
    QrCabAICMSTot_vOutro: TFloatField;
    QrCabAICMSTot_vNF: TFloatField;
    QrCabAide_indPag: TSmallintField;
    QrCabAInfAdic_InfAdFisco: TWideMemoField;
    QrCabAemit_IE: TWideStringField;
    QrCabAemit_UF: TWideStringField;
    QrCabAModFrete: TSmallintField;
    QrCabAId: TWideStringField;
    QrCabAide_cUF: TSmallintField;
    QrCabAide_cNF: TIntegerField;
    QrCabAide_natOp: TWideStringField;
    QrCabAide_tpNF: TSmallintField;
    QrCabAide_cMunFG: TIntegerField;
    QrCabAide_tpImp: TSmallintField;
    QrCabAide_tpEmis: TSmallintField;
    QrCabAide_cDV: TSmallintField;
    QrCabAide_tpAmb: TSmallintField;
    QrCabAide_finNFe: TSmallintField;
    QrCabAide_procEmi: TSmallintField;
    QrCabAide_verProc: TWideStringField;
    QrCabAemit_xNome: TWideStringField;
    QrCabAemit_xFant: TWideStringField;
    QrCabAemit_xLgr: TWideStringField;
    QrCabAemit_nro: TWideStringField;
    QrCabAemit_xCpl: TWideStringField;
    QrCabAemit_xBairro: TWideStringField;
    QrCabAemit_cMun: TIntegerField;
    QrCabAemit_xMun: TWideStringField;
    QrCabAemit_CEP: TIntegerField;
    QrCabAemit_cPais: TIntegerField;
    QrCabAemit_xPais: TWideStringField;
    QrCabAemit_fone: TWideStringField;
    QrCabAemit_IEST: TWideStringField;
    QrCabAemit_IM: TWideStringField;
    QrCabAemit_CNAE: TWideStringField;
    QrCabAdest_CNPJ: TWideStringField;
    QrCabAdest_CPF: TWideStringField;
    QrCabAdest_xNome: TWideStringField;
    QrCabAdest_xLgr: TWideStringField;
    QrCabAdest_nro: TWideStringField;
    QrCabAdest_xCpl: TWideStringField;
    QrCabAdest_xBairro: TWideStringField;
    QrCabAdest_cMun: TIntegerField;
    QrCabAdest_xMun: TWideStringField;
    QrCabAdest_UF: TWideStringField;
    QrCabAdest_CEP: TWideStringField;
    QrCabAdest_cPais: TIntegerField;
    QrCabAdest_xPais: TWideStringField;
    QrCabAdest_fone: TWideStringField;
    QrCabAdest_IE: TWideStringField;
    QrCabAdest_ISUF: TWideStringField;
    CGExportar: TdmkCheckGroup;
    QrPQE: TmySQLQuery;
    QrPQECodigo: TIntegerField;
    QrPQEData: TDateField;
    QrPQEIQ: TIntegerField;
    QrPQECI: TIntegerField;
    QrPQETransportadora: TIntegerField;
    QrPQENF: TIntegerField;
    QrPQEFrete: TFloatField;
    QrPQEPesoB: TFloatField;
    QrPQEPesoL: TFloatField;
    QrPQEValorNF: TFloatField;
    QrPQERICMS: TFloatField;
    QrPQERICMSF: TFloatField;
    QrPQEConhecimento: TIntegerField;
    QrPQEPedido: TIntegerField;
    QrPQEDataE: TDateField;
    QrPQEJuros: TFloatField;
    QrPQEICMS: TFloatField;
    QrPQECancelado: TWideStringField;
    QrPQELk: TIntegerField;
    QrPQEDataCad: TDateField;
    QrPQEDataAlt: TDateField;
    QrPQEUserCad: TIntegerField;
    QrPQEUserAlt: TIntegerField;
    QrPQEAlterWeb: TSmallintField;
    QrPQEAtivo: TSmallintField;
    QrPQETipoNF: TSmallintField;
    QrPQErefNFe: TWideStringField;
    QrPQEmodNF: TSmallintField;
    QrPQESerie: TIntegerField;
    QrPQEValProd: TFloatField;
    QrPQEVolumes: TIntegerField;
    QrPQEIPI: TFloatField;
    QrPQEPIS: TFloatField;
    QrPQECOFINS: TFloatField;
    QrPQESeguro: TFloatField;
    QrPQEDesconto: TFloatField;
    QrPQEOutros: TFloatField;
    QrPQEDataS: TDateField;
    QrEnti: TmySQLQuery;
    QrEntiTipo: TSmallintField;
    QrEntiIE: TWideStringField;
    QrEntiCNPJ_CPF: TWideStringField;
    QrEntiUF_Cod: TLargeintField;
    QrPQEIts: TmySQLQuery;
    QrPQEItsCUSTOITEM: TFloatField;
    QrPQEItsVALORKG: TFloatField;
    QrPQEItsTOTALKGBRUTO: TFloatField;
    QrPQEItsCUSTOKG: TFloatField;
    QrPQEItsPRECOKG: TFloatField;
    QrPQEItsCodigo: TIntegerField;
    QrPQEItsControle: TIntegerField;
    QrPQEItsConta: TIntegerField;
    QrPQEItsVolumes: TIntegerField;
    QrPQEItsPesoVB: TFloatField;
    QrPQEItsPesoVL: TFloatField;
    QrPQEItsValorItem: TFloatField;
    QrPQEItsIPI: TFloatField;
    QrPQEItsRIPI: TFloatField;
    QrPQEItsCFin: TFloatField;
    QrPQEItsICMS: TFloatField;
    QrPQEItsRICMS: TFloatField;
    QrPQEItsTotalCusto: TFloatField;
    QrPQEItsInsumo: TIntegerField;
    QrPQEItsTotalPeso: TFloatField;
    QrPQEItsNOMEPQ: TWideStringField;
    QrPQEItsprod_cProd: TWideStringField;
    QrPQEItsprod_cEAN: TWideStringField;
    QrPQEItsprod_xProd: TWideStringField;
    QrPQEItsprod_NCM: TWideStringField;
    QrPQEItsprod_EX_TIPI: TWideStringField;
    QrPQEItsprod_genero: TSmallintField;
    QrPQEItsprod_CFOP: TWideStringField;
    QrPQEItsprod_uCom: TWideStringField;
    QrPQEItsprod_qCom: TFloatField;
    QrPQEItsprod_vUnCom: TFloatField;
    QrPQEItsprod_vProd: TFloatField;
    QrPQEItsprod_cEANTrib: TWideStringField;
    QrPQEItsprod_uTrib: TWideStringField;
    QrPQEItsprod_qTrib: TFloatField;
    QrPQEItsprod_vUnTrib: TFloatField;
    QrPQEItsprod_vFrete: TFloatField;
    QrPQEItsprod_vSeg: TFloatField;
    QrPQEItsprod_vDesc: TFloatField;
    QrPQEItsICMS_Orig: TSmallintField;
    QrPQEItsICMS_CST: TSmallintField;
    QrPQEItsICMS_modBC: TSmallintField;
    QrPQEItsICMS_pRedBC: TFloatField;
    QrPQEItsICMS_vBC: TFloatField;
    QrPQEItsICMS_pICMS: TFloatField;
    QrPQEItsICMS_vICMS: TFloatField;
    QrPQEItsICMS_modBCST: TSmallintField;
    QrPQEItsICMS_pMVAST: TFloatField;
    QrPQEItsICMS_pRedBCST: TFloatField;
    QrPQEItsICMS_vBCST: TFloatField;
    QrPQEItsICMS_pICMSST: TFloatField;
    QrPQEItsICMS_vICMSST: TFloatField;
    QrPQEItsIPI_cEnq: TWideStringField;
    QrPQEItsIPI_CST: TSmallintField;
    QrPQEItsIPI_vBC: TFloatField;
    QrPQEItsIPI_qUnid: TFloatField;
    QrPQEItsIPI_vUnid: TFloatField;
    QrPQEItsIPI_pIPI: TFloatField;
    QrPQEItsIPI_vIPI: TFloatField;
    QrPQEItsPIS_CST: TSmallintField;
    QrPQEItsPIS_vBC: TFloatField;
    QrPQEItsPIS_pPIS: TFloatField;
    QrPQEItsPIS_vPIS: TFloatField;
    QrPQEItsPIS_qBCProd: TFloatField;
    QrPQEItsPIS_vAliqProd: TFloatField;
    QrPQEItsPISST_vBC: TFloatField;
    QrPQEItsPISST_pPIS: TFloatField;
    QrPQEItsPISST_qBCProd: TFloatField;
    QrPQEItsPISST_vAliqProd: TFloatField;
    QrPQEItsPISST_vPIS: TFloatField;
    QrPQEItsCOFINS_CST: TSmallintField;
    QrPQEItsCOFINS_vBC: TFloatField;
    QrPQEItsCOFINS_pCOFINS: TFloatField;
    QrPQEItsCOFINS_qBCProd: TFloatField;
    QrPQEItsCOFINS_vAliqProd: TFloatField;
    QrPQEItsCOFINS_vCOFINS: TFloatField;
    QrPQEItsCOFINSST_vBC: TFloatField;
    QrPQEItsCOFINSST_pCOFINS: TFloatField;
    QrPQEItsCOFINSST_qBCProd: TFloatField;
    QrPQEItsCOFINSST_vAliqProd: TFloatField;
    QrPQEItsCOFINSST_vCOFINS: TFloatField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    RECabE: TRichEdit;
    REItsE: TRichEdit;
    QrItsI: TmySQLQuery;
    QrCabAFatID: TIntegerField;
    QrCabAFatNum: TIntegerField;
    QrCabAEmpresa: TIntegerField;
    QrItsIprod_cProd: TWideStringField;
    QrItsIprod_cEAN: TWideStringField;
    QrItsIprod_xProd: TWideStringField;
    QrItsIprod_NCM: TWideStringField;
    QrItsIprod_EXTIPI: TWideStringField;
    QrItsIprod_genero: TSmallintField;
    QrItsIprod_CFOP: TIntegerField;
    QrItsIprod_uCom: TWideStringField;
    QrItsIprod_qCom: TFloatField;
    QrItsIprod_vUnCom: TFloatField;
    QrItsIprod_vProd: TFloatField;
    QrItsIprod_cEANTrib: TWideStringField;
    QrItsIprod_uTrib: TWideStringField;
    QrItsIprod_qTrib: TFloatField;
    QrItsIprod_vUnTrib: TFloatField;
    QrItsIprod_vFrete: TFloatField;
    QrItsIprod_vSeg: TFloatField;
    QrItsIprod_vDesc: TFloatField;
    QrItsITem_IPI: TSmallintField;
    QrItsI_Ativo_: TSmallintField;
    QrItsIInfAdCuztm: TIntegerField;
    QrItsIEhServico: TIntegerField;
    QrItsILk: TIntegerField;
    QrItsIDataCad: TDateField;
    QrItsIDataAlt: TDateField;
    QrItsIUserCad: TIntegerField;
    QrItsIUserAlt: TIntegerField;
    QrItsIAlterWeb: TSmallintField;
    QrItsIAtivo: TSmallintField;
    QrItsIICMSRec_pRedBC: TFloatField;
    QrItsIICMSRec_vBC: TFloatField;
    QrItsIICMSRec_pAliq: TFloatField;
    QrItsIICMSRec_vICMS: TFloatField;
    QrItsIIPIRec_pRedBC: TFloatField;
    QrItsIIPIRec_vBC: TFloatField;
    QrItsIIPIRec_pAliq: TFloatField;
    QrItsIIPIRec_vIPI: TFloatField;
    QrItsIPISRec_pRedBC: TFloatField;
    QrItsIPISRec_vBC: TFloatField;
    QrItsIPISRec_pAliq: TFloatField;
    QrItsIPISRec_vPIS: TFloatField;
    QrItsICOFINSRec_pRedBC: TFloatField;
    QrItsICOFINSRec_vBC: TFloatField;
    QrItsICOFINSRec_pAliq: TFloatField;
    QrItsICOFINSRec_vCOFINS: TFloatField;
    QrItsIMeuID: TIntegerField;
    QrItsINivel1: TIntegerField;
    QrItsO: TmySQLQuery;
    QrItsOnItem: TIntegerField;
    QrItsOIPI_vIPI: TFloatField;
    QrItsN: TmySQLQuery;
    QrItsNnItem: TIntegerField;
    QrItsNICMS_Orig: TSmallintField;
    QrItsNICMS_CST: TSmallintField;
    QrItsNICMS_vBC: TFloatField;
    QrItsNICMS_pICMS: TFloatField;
    QrItsNICMS_vICMS: TFloatField;
    QrItsNICMS_vBCST: TFloatField;
    QrItsNICMS_vICMSST: TFloatField;
    QrItsInItem: TIntegerField;
    QrItsOIPI_pIPI: TFloatField;
    QrItsOIPI_CST: TSmallintField;
    QrItsOIPI_vBC: TFloatField;
    RGGeraRegistro88: TRadioGroup;
    QrItsQ: TmySQLQuery;
    QrItsS: TmySQLQuery;
    QrItsQPIS_CST: TSmallintField;
    QrItsSCOFINS_CST: TSmallintField;
    QrItsOIPI_cEnq: TWideStringField;
    RGQuebraDeLinha: TRadioGroup;
    QrCabACNPJ_CPF_dest: TWideStringField;
    QrCabANotaCancelada: TSmallintField;
    QrCabAinfCanc_cStat: TIntegerField;
    QrItsIICMS_CST: TWideStringField;
    QrProd: TmySQLQuery;
    QrProdGraGru1: TIntegerField;
    QrProdNivel3: TIntegerField;
    QrProdNivel2: TIntegerField;
    QrProdNivel1: TIntegerField;
    QrProdCodUsu: TIntegerField;
    QrProdNome: TWideStringField;
    QrProdPrdGrupTip: TIntegerField;
    QrProdGraTamCad: TIntegerField;
    QrProdUnidMed: TIntegerField;
    QrProdCST_A: TSmallintField;
    QrProdCST_B: TSmallintField;
    QrProdNCM: TWideStringField;
    QrProdPeso: TFloatField;
    QrProdIPI_Alq: TFloatField;
    QrProdIPI_CST: TSmallintField;
    QrProdIPI_cEnq: TWideStringField;
    QrProdIPI_vUnid: TFloatField;
    QrProdIPI_TpTrib: TSmallintField;
    QrProdTipDimens: TSmallintField;
    QrProdPerCuztMin: TFloatField;
    QrProdPerCuztMax: TFloatField;
    QrProdMedOrdem: TIntegerField;
    QrProdPartePrinc: TIntegerField;
    QrProdInfAdProd: TWideStringField;
    QrProdSiglaCustm: TWideStringField;
    QrProdHowBxaEstq: TSmallintField;
    QrProdGerBxaEstq: TSmallintField;
    QrProdPIS_CST: TSmallintField;
    QrProdPIS_AlqP: TFloatField;
    QrProdPIS_AlqV: TFloatField;
    QrProdPISST_AlqP: TFloatField;
    QrProdPISST_AlqV: TFloatField;
    QrProdCOFINS_CST: TSmallintField;
    QrProdCOFINS_AlqP: TFloatField;
    QrProdCOFINS_AlqV: TFloatField;
    QrProdCOFINSST_AlqP: TFloatField;
    QrProdCOFINSST_AlqV: TFloatField;
    QrProdICMS_modBC: TSmallintField;
    QrProdICMS_modBCST: TSmallintField;
    QrProdICMS_pRedBC: TFloatField;
    QrProdICMS_pRedBCST: TFloatField;
    QrProdICMS_pMVAST: TFloatField;
    QrProdICMS_pICMSST: TFloatField;
    QrProdICMS_Pauta: TFloatField;
    QrProdICMS_MaxTab: TFloatField;
    QrProdIPI_pIPI: TFloatField;
    QrProdLk: TIntegerField;
    QrProdDataCad: TDateField;
    QrProdDataAlt: TDateField;
    QrProdUserCad: TIntegerField;
    QrProdUserAlt: TIntegerField;
    QrProdAlterWeb: TSmallintField;
    QrProdAtivo: TSmallintField;
    QrProdcGTIN_EAN: TWideStringField;
    QrProdEX_TIPI: TWideStringField;
    QrProdPIS_pRedBC: TFloatField;
    QrProdPISST_pRedBCST: TFloatField;
    QrProdCOFINS_pRedBC: TFloatField;
    QrProdCOFINSST_pRedBCST: TFloatField;
    QrProdICMSRec_pRedBC: TFloatField;
    QrProdIPIRec_pRedBC: TFloatField;
    QrProdPISRec_pRedBC: TFloatField;
    QrProdCOFINSRec_pRedBC: TFloatField;
    QrProdICMSRec_pAliq: TFloatField;
    QrProdIPIRec_pAliq: TFloatField;
    QrProdPISRec_pAliq: TFloatField;
    QrProdCOFINSRec_pAliq: TFloatField;
    QrProdICMSRec_tCalc: TSmallintField;
    QrProdIPIRec_tCalc: TSmallintField;
    QrProdPISRec_tCalc: TSmallintField;
    QrProdCOFINSRec_tCalc: TSmallintField;
    QrProdICMSAliqSINTEGRA: TFloatField;
    QrProdICMSST_BaseSINTEGRA: TFloatField;
    QrProdFatorClas: TIntegerField;
    QrProdGraGruX: TIntegerField;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    RECabS: TRichEdit;
    REItsS: TRichEdit;
    REInventario: TRichEdit;
    REProdutos: TRichEdit;
    QrEntiSemDoc: TmySQLQuery;
    QrEntiSemDocCodigo: TIntegerField;
    QrEntiSemDocNome: TWideStringField;
    frxEntiSemDoc: TfrxReport;
    frxDsEntiSemDoc: TfrxDBDataset;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    CkRecriarMP: TCheckBox;
    QrExcl: TmySQLQuery;
    QrExclFatID: TIntegerField;
    QrExclFatNum: TIntegerField;
    QrExclStatus: TIntegerField;
    QrExclEmpresa: TIntegerField;
    QrNivel1: TmySQLQuery;
    QrNivel1Nome: TWideStringField;
    QrNivel1NCM: TWideStringField;
    QrGraGruX_: TmySQLQuery;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    IntegerField9: TIntegerField;
    StringField3: TWideStringField;
    IntegerField10: TIntegerField;
    IntegerField11: TIntegerField;
    IntegerField12: TIntegerField;
    SmallintField1: TSmallintField;
    SmallintField2: TSmallintField;
    StringField4: TWideStringField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    SmallintField3: TSmallintField;
    StringField5: TWideStringField;
    FloatField3: TFloatField;
    SmallintField4: TSmallintField;
    SmallintField5: TSmallintField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    IntegerField13: TIntegerField;
    IntegerField14: TIntegerField;
    StringField6: TWideStringField;
    StringField7: TWideStringField;
    SmallintField6: TSmallintField;
    SmallintField7: TSmallintField;
    SmallintField8: TSmallintField;
    FloatField6: TFloatField;
    FloatField7: TFloatField;
    FloatField8: TFloatField;
    FloatField9: TFloatField;
    SmallintField9: TSmallintField;
    FloatField10: TFloatField;
    FloatField11: TFloatField;
    FloatField12: TFloatField;
    FloatField13: TFloatField;
    SmallintField10: TSmallintField;
    SmallintField11: TSmallintField;
    FloatField14: TFloatField;
    FloatField15: TFloatField;
    FloatField16: TFloatField;
    FloatField17: TFloatField;
    FloatField18: TFloatField;
    FloatField19: TFloatField;
    FloatField20: TFloatField;
    IntegerField15: TIntegerField;
    DateField1: TDateField;
    DateField2: TDateField;
    IntegerField16: TIntegerField;
    IntegerField17: TIntegerField;
    SmallintField12: TSmallintField;
    SmallintField13: TSmallintField;
    StringField8: TWideStringField;
    StringField9: TWideStringField;
    FloatField21: TFloatField;
    FloatField22: TFloatField;
    FloatField23: TFloatField;
    FloatField24: TFloatField;
    FloatField25: TFloatField;
    FloatField26: TFloatField;
    FloatField27: TFloatField;
    FloatField28: TFloatField;
    FloatField29: TFloatField;
    FloatField30: TFloatField;
    FloatField31: TFloatField;
    FloatField32: TFloatField;
    SmallintField14: TSmallintField;
    SmallintField15: TSmallintField;
    SmallintField16: TSmallintField;
    SmallintField17: TSmallintField;
    FloatField33: TFloatField;
    FloatField34: TFloatField;
    IntegerField18: TIntegerField;
    CkConsertaNome: TCheckBox;
    EdNCM_Padrao: TdmkEdit;
    Label2: TLabel;
    EdCFOPa: TdmkEdit;
    EdCFOPb: TdmkEdit;
    EdCFOPc: TdmkEdit;
    EdCFOPd: TdmkEdit;
    EdCFOPe: TdmkEdit;
    Label3: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    QrPQsemCFOP: TmySQLQuery;
    QrPQsemCFOPAtivo: TSmallintField;
    QrPQsemCFOPciUF: TLargeintField;
    QrPQsemCFOPiqUF: TLargeintField;
    QrPQsemCFOPCI: TIntegerField;
    QrPQsemCFOPIQ: TIntegerField;
    QrPQsemCFOPControle: TIntegerField;
    QrSemNF_MP: TmySQLQuery;
    frxMPSemNF: TfrxReport;
    frxSemNF_MP: TfrxDBDataset;
    QrSemNF_MPData: TDateField;
    QrSemNF_MPProcedencia: TIntegerField;
    QrSemNF_MPMarca: TWideStringField;
    QrSemNF_MPLote: TWideStringField;
    QrSemNF_MPNOMEFORNECEDOR: TWideStringField;
    QrSemNF_MPNOMEPROCEDENCIA: TWideStringField;
    QrMPI: TmySQLQuery;
    QrMPIClienteI: TIntegerField;
    QrMPIData: TDateField;
    QrMPIQuemEmitiu: TLargeintField;
    QrMPINF_modelo: TWideStringField;
    QrMPINF_Serie: TWideStringField;
    QrMPINF: TIntegerField;
    QrMPICMPValor: TFloatField;
    QrMPIPecasNF: TFloatField;
    QrMPIConta: TIntegerField;
    QrMPIEmissao: TDateField;
    QrMPIIts: TmySQLQuery;
    QrMPIItsClienteI: TIntegerField;
    QrMPIItsData: TDateField;
    QrMPIItsTipificacao: TIntegerField;
    QrMPIItsAnimal: TSmallintField;
    QrMPIItsCFOP: TWideStringField;
    QrMPIItsPecasNF: TFloatField;
    QrMPIItsCMPValor: TFloatField;
    QrMPIItsConta: TIntegerField;
    QrSemNF_MPCNPJ_CPF: TWideStringField;
    EdModeloNF: TdmkEdit;
    Label11: TLabel;
    QrCabASigla: TWideStringField;
    QrSemNF_MPNF: TIntegerField;
    TabSheet7: TTabSheet;
    DBGrid1: TDBGrid;
    DsCabA: TDataSource;
    QrCabAide_dEmi_TXT: TWideStringField;
    QrCabAide_dSaiEnt_TXT: TWideStringField;
    QrProdTipo_Item: TSmallintField;
    QrProdGenero: TIntegerField;
    QrProdSigla: TWideStringField;
    CkRecriarPQ: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    EdPasta: TEdit;
    BtSaida: TBitBtn;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure QrCabACalcFields(DataSet: TDataSet);
    procedure RGQuebraDeLinhaClick(Sender: TObject);
    procedure QrItsICalcFields(DataSet: TDataSet);
    procedure frxEntiSemDocGetValue(const VarName: string; var Value: Variant);
    procedure QrCabAAfterScroll(DataSet: TDataSet);
    procedure QrCabABeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    FIL_Ini, FIL_Tam: Integer;
    FEmpresa: Integer;
    FNFS_Entrada, FNFS_Saida, FItens_NFs_Entrada, FItens_NFs_Saida, FInventario,
    FProdutos, FCNPJ, FLin, FExp, FDataIni, FDataFim: String;
    FPular: Boolean;
    FQuebraDeLinha: Integer;
    function CSF(Filler: String; Tam, Casas: Integer; Valor: Double; AlinhaDireita: Boolean = True): String;
    function CSI(Filler: String; Tam, Inteiro: Integer; AlinhaDireita: Boolean = True): String;
    function CSS(Filler: String; Tam: Integer; Texto: String; AlinhaEsquerda: Boolean = True): String;
    function FD6(Data: TDateTime): String;
    function FD8(Data: TDateTime): String;
    //function CSI(Filler: String; Tam: Integer; Texto: String): String;
    procedure IL(const Ini, Tam: Integer; const Texto: String);
    procedure NotasDeEntrada();
    procedure NotasDeSaida();
    procedure ItensDeNotaDeEntrada();
    procedure ItensDeNotaDeSaida();
    procedure Inventario();
    procedure Produtos();
    //
    function VerificaCNPJs(): Boolean;
    procedure VerificaPQE();
    function VerificaMPI(): Boolean;
    function MPSemNF(): Boolean;
    procedure ReopenCabA();
  public
    { Public declarations }
  end;

  var
  FmProsoft1: TFmProsoft1;
  const
  FDirProsoft = 'C:\Dermatek\Prosoft\';

implementation

uses UnMyObjects, Module, ModuleGeral, dmkGeral, UnInternalConsts, UnGrade_Tabs,
UMySQLModule, ModuleNFe_0000, ModProd;

{$R *.DFM}

procedure TFmProsoft1.BtOKClick(Sender: TObject);
var
  NCM, NomePer: String;
begin
  FDataIni := Geral.FDT(TPDataIni.Date, 1);
  FDataFim := Geral.FDT(TPDataFim.Date, 1);
  Screen.Cursor := crHourGlass;
  try
    NCM := Geral.SoNumero_TT(EdNCM_Padrao.Text);
    if Length(NCM) > 0 then
    begin
      if Length(NCM) = 8 then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Setando NCM');
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE gragru1 SET NCM=:P0');
        Dmod.QrUpd.SQL.Add('WHERE NCM=""');
        Dmod.QrUpd.Params[0].AsString := EdNCM_Padrao.Text;
        Dmod.QrUpd.ExecSQL;
      end else
      begin
        Geral.MB_Aviso('O NCM deve ter 8 d�gitos!');
        Exit;
      end;
    end;
    if CkConsertaNome.Checked then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo nomes de uso e consumo');
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE gragru1 gg1, PQ pq');
      Dmod.QrUpd.SQL.Add('SET gg1.Nome=pq.Nome');
      Dmod.QrUpd.SQL.Add('WHERE pq.Codigo=gg1.Nivel1');
      Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
      Dmod.QrUpd.ExecSQL;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Nomes de uso e consumo corrigidos.');
    end;

    //  Cria registros a for�a
    VerificaPQE();
    //  Cria registros a for�a
    if not VerificaMPI() then Exit;
    //
    if not VerificaCNPJs() then Exit;
    FPular := False;
    NomePer := '_' + FormatDateTime('YYY_MM_DD', TPDataIni.Date) + '_a_' +
      FormatDateTime('YYY_MM_DD', TPDataIni.Date) + '.txt';
    FNFS_Entrada        := FDirProsoft + 'NFs_Entrada' + NomePer;
    FNFS_Saida          := FDirProsoft + 'NFs_Saida' + NomePer;
    FItens_NFs_Entrada  := FDirProsoft + 'Itens_NFs_Entrada' + NomePer;
    FItens_NFs_Saida    := FDirProsoft + 'Itens_NFs_Saida' + NomePer;
    FInventario         := FDirProsoft + 'Inventario' + NomePer;
    FProdutos           := FDirProsoft + 'Produtos' + NomePer;
    if FileExists(FNFS_Entrada      ) then DeleteFile(FNFS_Entrada      );
    if FileExists(FNFS_Saida        ) then DeleteFile(FNFS_Saida        );
    if FileExists(FItens_NFs_Entrada) then DeleteFile(FItens_NFs_Entrada);
    if FileExists(FItens_NFs_Saida  ) then DeleteFile(FItens_NFs_Saida  );
    if FileExists(FInventario       ) then DeleteFile(FInventario       );
    if FileExists(FProdutos         ) then DeleteFile(FProdutos         );

    //

    if Geral.IntInConjunto(1, CGExportar.Value) then
      NotasDeEntrada();
    //

    if FPular then Exit;
    if Geral.IntInConjunto(2, CGExportar.Value) then
      NotasDeSaida();
    //

    if FPular then Exit;
    if Geral.IntInConjunto(4, CGExportar.Value) then
      Inventario();
    //

    if FPular then Exit;
    if Geral.IntInConjunto(8, CGExportar.Value) then
      Produtos();
    //

    if FPular then Exit;

  finally
    Geral.MB_Info('Os arquivos que foram salvos foram salvos na pasta "' +
    FDirProsoft + '"');
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmProsoft1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmProsoft1.CSF(Filler: String; Tam, Casas: Integer; Valor: Double;
  AlinhaDireita: Boolean): String;
var
  Fmt: String;
  Val: Double;
  I: Integer;
begin
  if FPular then Exit;
  Fmt := '';
  while Length(Fmt) < Casas do
    Fmt := Fmt + '0';
  if Fmt <> '' then
    Fmt := '.' + Fmt;
  Fmt := '0' + Fmt;
  //
  //
  Val := Valor;
  if Val < 0 then
    Val := Val * -1;
  Result := FormatFloat(Fmt, Val);
  while Length(Result) < Tam do
  begin
    if AlinhaDireita then
      Result := Filler + Result
    else
      Result := Result + Filler;
  end;
  for I := 1 to Length(Result) do
    if Result[I] = ',' then
      Result[I] := '.';
end;

function TFmProsoft1.CSI(Filler: String; Tam, Inteiro: Integer;
  AlinhaDireita: Boolean): String;
begin
  if FPular then Exit;
  // Permitir negativos por causa do GraGru1 (Nivel1)
  Result := FormatFloat('0', Inteiro);
  while Length(Result) < Tam do
  begin
    if AlinhaDireita then
      Result := Filler + Result
    else
      Result := Result + Filler;
  end;
end;

function TFmProsoft1.CSS(Filler: String; Tam: Integer; Texto: String; AlinhaEsquerda: Boolean = True): String;
begin
  if FPular then Exit;
  Result := Texto;
  while Length(Result) < Tam do
  begin
    if AlinhaEsquerda then
      Result := Result + Filler
    else
      Result := Filler + Result;
  end;
end;

procedure TFmProsoft1.EdEmpresaChange(Sender: TObject);
begin
  FEmpresa := DModG.QrEmpresasCodigo.Value;
  FCNPJ    := DModG.QrEmpresasCNPJ_CPF.Value;
end;

function TFmProsoft1.FD6(Data: TDateTime): String;
begin
  if FPular then Exit;
  if Data < 2 then
  begin
    Result := '000000';
    FPular := True;
    Geral.MB_Aviso('Data inv�lida: ' + Result + sLineBreak + 'Posi��o: ' +
    FormatFloat('0', FIL_Ini + FIl_Tam) + sLineBreak + FExp);
  end else
    Result := FormatDateTime('ddmmyy', Data);
end;

function TFmProsoft1.FD8(Data: TDateTime): String;
begin
  if FPular then Exit;
  if Data < 2 then
  begin
    Result := '000000';
    FPular := True;
    Geral.MB_Aviso('Data inv�lida: ' + Result + sLineBreak + 'Posi��o: ' +
    FormatFloat('0', FIL_Ini + FIl_Tam) + sLineBreak + FExp);
  end else
    Result := FormatDateTime('yyyymmdd', Data);
end;

procedure TFmProsoft1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProsoft1.FormCreate(Sender: TObject);
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  ImgTipo.SQLType := stLok;
  //
  EdPasta.Text := 'Os arquivos ser�o salvos na pasta: ' + FDirProsoft;
  QrProd.Close;
  UnDmkDAC_PF.AbreQuery(QrProd, Dmod.MyDB);
  QrEntiSemDoc.Database := DModG.MyPID_DB;
  //CGExportar.SetMaxValue;
  CGExportar.Value := 0;
  FQuebraDeLinha := 1310;
  FEmpresa := 0;
  FCNPJ    := '';
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa);
  TPDataIni.Date := Geral.PrimeiroDiaDoMes(IncMonth(Date, -1));
  TPDataFim.Date := Geral.PrimeiroDiaDoMes(Date) -1;
end;

procedure TFmProsoft1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProsoft1.frxEntiSemDocGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := CBEmpresa.Text;
end;

procedure TFmProsoft1.IL(const Ini, Tam: Integer; const Texto: String);
begin
  FIL_Ini := Ini;
  FIL_Tam := Tam;
  //
  if FPular then Exit;
  //
  if Length(FLin) <> Ini-1 then
  begin
    Geral.MB_Aviso('Tamanho inicial inv�lido!' + sLineBreak +
    'Deveria ser ' + FormatFloat('0', Ini) + ' mas � ' + FormatFloat('0',
    Length(FLin)+1) + sLineBreak + FExp);
    FPular := True;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if Length(Texto) <> Tam then
  begin
    Geral.MB_Aviso('Tamanho do texto "'+Texto+'" � inv�lido!' + sLineBreak +
    'Deveria ser ' + FormatFloat('0', Tam) + ' mas � ' + FormatFloat('0',
    Length(Texto)) + sLineBreak + 'Posi��o inicial na coluna: ' +
    FormatFloat('0', Ini) + sLineBreak + FExp);
    FPular := True;
    Screen.Cursor := crDefault;
    Exit;
  end;
  FLin := FLin + Texto
end;

procedure TFmProsoft1.QrCabAAfterScroll(DataSet: TDataSet);
begin
  QrItsI.Close;
  QrItsI.Params[00].AsInteger := QrCabAFatID.Value;
  QrItsI.Params[01].AsInteger := QrCabAFatNum.Value;
  QrItsI.Params[02].AsInteger := QrCabAEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrItsI, Dmod.MyDB);
end;

procedure TFmProsoft1.QrCabABeforeClose(DataSet: TDataSet);
begin
  QrItsI.Close;
  QrItsN.Close;
  QrItsO.Close;
  QrItsQ.Close;
  QrItsS.Close;
end;

procedure TFmProsoft1.QrCabACalcFields(DataSet: TDataSet);
begin
  if Length(Geral.SoNumero1a9_TT(QrCabAemit_CNPJ.Value)) > 0 then
    QrCabACNPJ_CPF_emit.Value := QrCabAemit_CNPJ.Value
  else
    QrCabACNPJ_CPF_emit.Value := QrCabAemit_CPF.Value;
  //
  if Length(Geral.SoNumero1a9_TT(QrCabAdest_CNPJ.Value)) > 0 then
    QrCabACNPJ_CPF_dest.Value := QrCabAdest_CNPJ.Value
  else
    QrCabACNPJ_CPF_dest.Value := QrCabAdest_CPF.Value;
  //
  // 0-N�o Cancelada, 1-Cancelada
  if QrCabAinfCanc_cStat.Value = 101 then
    QrCabANotaCancelada.Value := 1
  else
    QrCabANotaCancelada.Value := 0;
  //
  //Mudei 2010-10-15
  {
  QrCabAEspecieDoc.Value := FormatFloat('0', QrCabAide_mod.Value);
  if QrCabAEspecieDoc.Value = '0' then
    QrCabAEspecieDoc.Value := '1';
  }
  //
  QrCabAide_dEmi_TXT.Value    := dmkPF.FDT_NULO(QrCabAide_dEmi.Value, 3);
  QrCabAide_dSaiEnt_TXT.Value := dmkPF.FDT_NULO(QrCabAide_dSaiEnt.Value, 3);
end;

procedure TFmProsoft1.QrItsICalcFields(DataSet: TDataSet);
begin
  QrItsIICMS_CST.Value := FormatFloat('0', QrItsNICMS_Orig.Value) + FormatFloat('00', QrItsNICMS_CST.Value);
end;

procedure TFmProsoft1.ReopenCabA;
begin
  QrCabA.Close;
  QrCabA.SQL.Clear;
{
  QrCabA.SQL.Add('SELECT * FROM nfecaba');
  QrCabA.SQL.Add('WHERE empresa=:P0');
  QrCabA.SQL.Add('AND dest_CNPJ=:P1');
  QrCabA.SQL.Add('AND DataFiscal BETWEEN :P2 AND :P3');
}
  QrCabA.SQL.Add('SELECT nmf.Sigla, nfa.*');
  QrCabA.SQL.Add('FROM nfecaba nfa');
  QrCabA.SQL.Add('LEFT JOIN nfmodocfis nmf ON nmf.CodUsu=nfa.ide_mod');
  QrCabA.SQL.Add('WHERE nfa.empresa=:P0');
  QrCabA.SQL.Add('AND nfa.dest_CNPJ=:P1');
  QrCabA.SQL.Add('AND nfa.DataFiscal BETWEEN :P2 AND :P3');
  QrCabA.Params[00].AsInteger := FEmpresa;
  QrCabA.Params[01].AsString  := FCNPJ;
  QrCabA.Params[02].AsString  := FDataIni;
  QrCabA.Params[03].AsString  := FDataFim;
  UnDmkDAC_PF.AbreQuery(QrCabA, Dmod.MyDB);
end;

procedure TFmProsoft1.RGQuebraDeLinhaClick(Sender: TObject);
begin
  case RGQuebraDeLinha.ItemIndex of
    0: FQuebraDeLinha := 0;
    1: FQuebraDeLinha := 13;
    2: FQuebraDeLinha := 10;
    3: FQuebraDeLinha := 1310;
    4: FQuebraDeLinha := 1013;
  end;
end;

function TFmProsoft1.VerificaCNPJs: Boolean;
begin
  QrEntiSemDoc.Close;
  QrEntiSemDoc.Params[00].AsInteger := FEmpresa;
  QrEntiSemDoc.Params[01].AsString  := FDataIni;
  QrEntiSemDoc.Params[02].AsString  := FDataFim;
  UnDmkDAC_PF.AbreQuery(QrEntiSemDoc, DModG.MyPID_DB);
  //
  if QrEntiSemDoc.RecordCount > 0 then
  begin
    Result := False;
    MyObjects.frxMostra(frxEntiSemDoc, 'Entidades sem documento');
  end else Result := True;
end;

function TFmProsoft1.VerificaMPI(): Boolean;
var
  ide_serie, ide_mod, Versao, IDCtrl, FatID, FatNum, Empresa, ide_nNF,
  CriAForca,   CodInfoDest, CodInfoEmit: Integer;
  Id, ide_verProc, ide_natOp, ide_tpNF, ide_dEmi, ide_dSaiEnt, DataFiscal,
  ide_indPag, emit_CNPJ, emit_CPF, emit_IE, dest_CNPJ, dest_CPF, dest_IE: String;
  ICMSTot_vICMS, ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc,
  ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro, ICMSTot_vNF:
  Double;
  // Itens
  MeuID, Nivel1, nItem, GraGruX, UnidMedTrib, UnidMedCom: Integer;
  prod_CFOP, prod_uCom, prod_uTrib, prod_cProd: String;
  prod_qCom, prod_vUnCom, prod_vProd, prod_qTrib, prod_vUnTrib: Double;
begin
  Result := False;
  CriAForca := 1;
  //
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE mpinits SET NF_Modelo="1" WHERE NF_Modelo=""');
  DMod.QrUpd.ExecSQL;
  //
  if CkRecriarMP.Checked then
  begin
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando notas de mat�ria-prima a serem excluidas');
    QrExcl.Close;
    QrExcl.Params[00].AsInteger := VAR_FATID_0113;
    QrExcl.Params[01].AsInteger := FEmpresa;
    QrExcl.Params[02].AsString  := FDataIni;
    QrExcl.Params[03].AsString  := FDataFim;
    UnDmkDAC_PF.AbreQuery(QrExcl, Dmod.MyDB);
    //
    if QrExcl.RecordCount > 0 then
    begin
      PB1.Max := QrExcl.RecordCount;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo notas de mat�ria-prima geradas a for�a');
      while not QrExcl.Eof do
      begin
        PB1.Position := PB1.Position + 1;
        DmNFe_0000.ExcluiNfe(QrExclStatus.Value, QrExclFatID.Value,
          QrExclFatNum.Value, QrExclEmpresa.Value, True, True);
        //
        QrExcl.Next;
      end;
    end;
  end;
  //
  // deve ser depois de definir as datas
  if MPSemNF() then Exit;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando entrada de mat�ria-prima sem dados fiscais');
  QrMPI.Close;
{
SELECT mpe.ClienteI, mpe.Procedencia, mpe.Data,
mpe.Tipificacao, mpe.Animal, mpi.*
FROM mpinits mpi
LEFT JOIN mpin mpe ON mpe.Controle=mpi.Controle
LEFT JOIN nfecaba nca ON nca.FatNum=mpi.Conta
AND nca.FatID in (:P0,:P1)
WHERE nca.FatID IS NULL
AND mpe.ClienteI=:P2
AND mpi.NF > 0
AND mpi.NF_Inn_Fut=0
  QrMPI.Params[00].AsInteger := VAR_FATID_0013;
  QrMPI.Params[01].AsInteger := VAR_FATID_0113;
  QrMPI.Params[02].AsInteger := FEmpresa;
}
  QrMPI.Params[00].AsInteger := VAR_FATID_0013;
  QrMPI.Params[01].AsInteger := VAR_FATID_0113;
  QrMPI.Params[02].AsInteger := VAR_FATID_0213;
  QrMPI.Params[03].AsInteger := FEmpresa;
  QrMPI.Params[04].AsString  := FDataIni;
  QrMPI.Params[05].AsString  := FDataFim;
  //
  UnDmkDAC_PF.AbreQuery(QrMPI, Dmod.MyDB);
  PB1.Max := QrMPI.RecordCount;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando dados fiscais para mat�ria-prima');
  QrMPI.First;
  while not QrMPI.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando dados fiscais para mat�ria-prima ' +
    IntToStr(QrMPI.RecNo) + ' de ' + IntToStr(QrMPI.RecordCount));
    //

    FatID                       := VAR_FATID_0113;
    FatNum                      := QrMPIConta.Value;
    Empresa                     := QrMPIClienteI.Value;
    CodInfoDest                 := QrMPIClienteI.Value;
    CodInfoEmit                 := QrMPIQuemEmitiu.Value;
    Id                          := '';//QrMPIrefNFe.Value;
    ide_verProc                 := '';
    ide_natOp                   := '';
    ide_mod                     := Geral.IMV('0' + Geral.SoNumero_TT(QrMPINF_Modelo.Value));
    ide_serie                   := Geral.IMV('0' + Geral.SoNumero_TT(QrMPINF_Serie.Value));
    ide_tpNF                    := '1';
    ide_indPag                  := '1';
    ide_nNF                     := QrMPINF.Value;
    ide_dEmi                    := Geral.FDT(QrMPIEmissao.Value, 1);
    ide_dSaiEnt                 := Geral.FDT(QrMPIEmissao.Value, 1);
    DataFiscal                  := Geral.FDT(QrMPIData.Value, 1);
    //versao                      := 0;

    if QrMPIEmissao.Value < 2 then ide_dEmi := DataFiscal;
    if QrMPIData.Value < 2 then ide_dSaiEnt := ide_dEmi;

    QrEnti.Close;
    QrEnti.Params[0].AsInteger  := QrMPIQuemEmitiu.Value;
    UnDmkDAC_PF.AbreQuery(QrEnti, Dmod.MyDB);
    if QrEntiCNPJ_CPF.Value <> '' then
    begin
      QrMPIIts.Close;
      QrMPIIts.SQL.Clear;
      QrMPIIts.SQL.Add('SELECT mpe.ClienteI, mpe.Data, mpe.Tipificacao, mpe.Animal,');
      QrMPIIts.SQL.Add('mpi.CFOP, mpi.PecasNF, mpi.CMPValor, mpi.Conta');
      QrMPIIts.SQL.Add('FROM mpinits mpi');
      QrMPIIts.SQL.Add('LEFT JOIN mpin mpe ON mpe.Controle=mpi.Controle');
      QrMPIIts.SQL.Add('LEFT JOIN nfecaba nca ON nca.FatNum=mpi.Conta');
      QrMPIIts.SQL.Add('AND nca.FatID in (:P0,:P1,:P2)');
      QrMPIIts.SQL.Add('WHERE nca.FatID IS NULL');
      QrMPIIts.SQL.Add('AND mpe.ClienteI=:P3');
      QrMPIIts.SQL.Add('AND IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece)=:P4');
      QrMPIIts.SQL.Add('AND mpi.NF_modelo=:P5');
      QrMPIIts.SQL.Add('AND mpi.NF_Serie=:P6');
      QrMPIIts.SQL.Add('AND mpi.NF=:P7');
      QrMPIIts.SQL.Add('AND mpe.Data BETWEEN :P8 AND :P9');
      QrMPIIts.SQL.Add('ORDER BY mpe.Data, mpe.Controle, mpi.Conta');
      QrMPIIts.SQL.Add('');
      QrMPIIts.Params[00].AsInteger := VAR_FATID_0013;
      QrMPIIts.Params[01].AsInteger := VAR_FATID_0113;
      QrMPIIts.Params[02].AsInteger := VAR_FATID_0213;
      QrMPIIts.Params[03].AsInteger := FEmpresa;
      QrMPIIts.Params[04].AsInteger := QrMPIQuemEmitiu.Value;
      QrMPIIts.Params[05].AsString  := QrMPINF_Modelo.Value;
      QrMPIIts.Params[06].AsString  := QrMPINF_Serie.Value;
      QrMPIIts.Params[07].AsInteger := QrMPINF.Value;
      QrMPIIts.Params[08].AsString  := FDataIni;
      QrMPIIts.Params[09].AsString  := FDataFim;
      UnDmkDAC_PF.AbreQuery(QrMPIIts, Dmod.MyDB);
      //
      //ShowMessage(IntToStr(QrMPIIts.RecordCount));
      //
      if QrEntiTipo.Value = 0 then
      begin
        emit_CNPJ := QrEntiCNPJ_CPF.Value;
        emit_CPF  := '';
      end else begin
        emit_CNPJ := '';
        emit_CPF  := QrEntiCNPJ_CPF.Value;
      end;
      emit_IE                     := QrEntiIE.Value;
      dest_CNPJ                   := DModG.QrEmpresasCNPJ_CPF.Value;
      dest_CPF                    := '';
      dest_IE                     := DModG.QrEmpresasIE.Value;

      ICMSTot_vICMS               := 0; //QrMPIICMS.Value;
      ICMSTot_vProd               := QrMPICMPValor.Value; //QrMPIValProd.Value;
      ICMSTot_vFrete              := 0; //QrMPIFrete.Value;
      ICMSTot_vSeg                := 0; //QrMPISeguro.Value;
      ICMSTot_vDesc               := 0; //QrMPIDesconto.Value;
      ICMSTot_vIPI                := 0; //QrMPIIPI.Value;
      ICMSTot_vPIS                := 0; //QrMPIPIS.Value;
      ICMSTot_vCOFINS             := 0; //QrMPICOFINS.Value;
      ICMSTot_vOutro              := 0; //QrMPIOutros.Value;
      ICMSTot_vNF                 := QrMPICMPValor.Value; //QrMPIValorNF.Value;

      IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);

      //ide_CUF                     := Geral.GetSiglaUF_do_CodigoUF(QrMPI);
      //EdFatNum.ValueVariant := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeavulsa', '', EdFatNum.ValueVariant);


      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False,[
  'IDCtrl', (*'LoteEnv', 'versao',*)
  'Id', (*'ide_cUF', 'ide_cNF',*)
  'ide_natOp', 'ide_indPag', 'ide_mod',
  'ide_serie', 'ide_nNF', 'ide_dEmi',
  'ide_dSaiEnt', 'ide_tpNF', (*'ide_cMunFG',
  'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
  'ide_tpAmb', 'ide_finNFe', 'ide_procEmi',*)
  'ide_verProc', 'emit_CNPJ', 'emit_CPF',
  (*'emit_xNome', 'emit_xFant', 'emit_xLgr',
  'emit_nro', 'emit_xCpl', 'emit_xBairro',
  'emit_cMun', 'emit_xMun', 'emit_UF',
  'emit_CEP', 'emit_cPais', 'emit_xPais',
  'emit_fone',*) 'emit_IE', (*'emit_IEST',
  'emit_IM', 'emit_CNAE',*) 'dest_CNPJ',
  'dest_CPF', (*'dest_xNome', 'dest_xLgr',
  'dest_nro', 'dest_xCpl', 'dest_xBairro',
  'dest_cMun', 'dest_xMun', 'dest_UF',
  'dest_CEP', 'dest_cPais', 'dest_xPais',
  'dest_fone',*) 'dest_IE', (*'dest_ISUF',
  'ICMSTot_vBC',*) 'ICMSTot_vICMS', (*'ICMSTot_vBCST',
  'ICMSTot_vST',*) 'ICMSTot_vProd', 'ICMSTot_vFrete',
  'ICMSTot_vSeg', 'ICMSTot_vDesc', (*'ICMSTot_vII',*)
  'ICMSTot_vIPI', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
  'ICMSTot_vOutro', 'ICMSTot_vNF', (*'ISSQNtot_vServ',
  'ISSQNtot_vBC', 'ISSQNtot_vISS', 'ISSQNtot_vPIS',
  'ISSQNtot_vCOFINS', 'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS',
  'RetTrib_vRetCSLL', 'RetTrib_vBCIRRF', 'RetTrib_vIRRF',
  'RetTrib_vBCRetPrev', 'RetTrib_vRetPrev', 'ModFrete',
  'Transporta_CNPJ', 'Transporta_CPF', 'Transporta_XNome',
  'Transporta_IE', 'Transporta_XEnder', 'Transporta_XMun',
  'Transporta_UF', 'RetTransp_vServ', 'RetTransp_vBCRet',
  'RetTransp_PICMSRet', 'RetTransp_vICMSRet', 'RetTransp_CFOP',
  'RetTransp_CMunFG', 'VeicTransp_Placa', 'VeicTransp_UF',
  'VeicTransp_RNTC', 'Cobr_Fat_nFat', 'Cobr_Fat_vOrig',
  'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq', 'InfAdic_InfAdFisco',
  'InfAdic_InfCpl', 'Exporta_UFEmbarq', 'Exporta_XLocEmbarq',
  'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
  'Status', 'infProt_Id', 'infProt_tpAmb',
  'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
  'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
  'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
  'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
  'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
  'infCanc_xJust', '_Ativo_', 'FisRegCad',
  'CartEmiss', 'TabelaPrc', 'CondicaoPg',
  'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',
  'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
  'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
  'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
  'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
  'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
  'DataFiscal', (*'protNFe_versao', 'retCancNFe_versao',
  'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta', 'SINTEGRA_ExpNat',
  'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta', 'SINTEGRA_ExpConhNum',
  'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip', 'SINTEGRA_ExpPais',
  'SINTEGRA_ExpAverDta',*) 'CodInfoEmit', 'CodInfoDest',
  'CriAForca'], [
  'FatID', 'FatNum', 'Empresa'], [
  IDCtrl, (*LoteEnv, versao,*)
  Id, (*ide_cUF, ide_cNF,*)
  ide_natOp, ide_indPag, ide_mod,
  ide_serie, ide_nNF, ide_dEmi,
  ide_dSaiEnt, ide_tpNF, (*ide_cMunFG,
  ide_tpImp, ide_tpEmis, ide_cDV,
  ide_tpAmb, ide_finNFe, ide_procEmi,*)
  ide_verProc, emit_CNPJ, emit_CPF,
  (*emit_xNome, emit_xFant, emit_xLgr,
  emit_nro, emit_xCpl, emit_xBairro,
  emit_cMun, emit_xMun, emit_UF,
  emit_CEP, emit_cPais, emit_xPais,
  emit_fone,*) emit_IE, (*emit_IEST,
  emit_IM, emit_CNAE,*) dest_CNPJ,
  dest_CPF, (*dest_xNome, dest_xLgr,
  dest_nro, dest_xCpl, dest_xBairro,
  dest_cMun, dest_xMun, dest_UF,
  dest_CEP, dest_cPais, dest_xPais,
  dest_fone,*) dest_IE, (*dest_ISUF,
  ICMSTot_vBC,*) ICMSTot_vICMS, (*ICMSTot_vBCST,
  ICMSTot_vST,*) ICMSTot_vProd, ICMSTot_vFrete,
  ICMSTot_vSeg, ICMSTot_vDesc, (*ICMSTot_vII,*)
  ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
  ICMSTot_vOutro, ICMSTot_vNF, (*ISSQNtot_vServ,
  ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,
  ISSQNtot_vCOFINS, RetTrib_vRetPIS, RetTrib_vRetCOFINS,
  RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,
  RetTrib_vBCRetPrev, RetTrib_vRetPrev, ModFrete,
  Transporta_CNPJ, Transporta_CPF, Transporta_XNome,
  Transporta_IE, Transporta_XEnder, Transporta_XMun,
  Transporta_UF, RetTransp_vServ, RetTransp_vBCRet,
  RetTransp_PICMSRet, RetTransp_vICMSRet, RetTransp_CFOP,
  RetTransp_CMunFG, VeicTransp_Placa, VeicTransp_UF,
  VeicTransp_RNTC, Cobr_Fat_nFat, Cobr_Fat_vOrig,
  Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,
  InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq,
  Compra_XNEmp, Compra_XPed, Compra_XCont,
  Status, infProt_Id, infProt_tpAmb,
  infProt_verAplic, infProt_dhRecbto, infProt_nProt,
  infProt_digVal, infProt_cStat, infProt_xMotivo,
  infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
  infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,
  infCanc_cStat, infCanc_xMotivo, infCanc_cJust,
  infCanc_xJust, _Ativo_, FisRegCad,
  CartEmiss, TabelaPrc, CondicaoPg,
  FreteExtra, SegurExtra, ICMSRec_pRedBC,
  ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
  IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
  IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
  PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
  COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
  DataFiscal, (*protNFe_versao, retCancNFe_versao,
  SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_ExpNat,
  SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum,
  SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais,
  SINTEGRA_ExpAverDta,*) CodInfoEmit, CodInfoDest,
  CriAForca], [
  FatID, FatNum, Empresa], True) then
      begin
        nItem := 0;
        QrMPIIts.First;
        while not QrMPIIts.Eof do
        begin
          nItem        := nItem + 1;
          MeuID        := QrMPIItsConta.Value;
          GraGruX      := Dmod.ObtemGraGruXDeCouro(QrMPIItsTipificacao.Value, QrMPIItsAnimal.Value);
          Nivel1       := GraGruX;(*GraGruX > � o mesmo que o Nivel1*)
          prod_CFOP    := QrMPIItsCFOP.Value; //'0000';

          DmProd.Obtem_prod_uTribdeGraGru1(Nivel1, prod_uCom, UnidMedCom);
          UnidMedTrib := UnidMedCom;
          prod_uTrib   := prod_uCom;
          prod_qCom    := QrMPIItsPecasNF.Value;
          prod_qTrib   := QrMPIItsPecasNF.Value;
          if QrMPIItsPecasNF.Value > 0 then
            prod_vUnCom  := QrMPIItsCMPValor.Value / QrMPIItsPecasNF.Value
          else prod_vUnCom  := 0;
          prod_vUnTrib := prod_vUnCom;
          prod_vProd   := QrMPIItsCMPValor.Value;
          prod_cProd   := FormatFloat('0', Dmod.ObtemGraGruXDeCouro(QrMPIItsTipificacao.Value, QrMPIItsAnimal.Value));

          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
  'prod_cProd', (*'prod_cEAN', 'prod_xProd',
  'prod_NCM', 'prod_EXTIPI', 'prod_genero',*)
  'prod_CFOP', 'prod_uCom', 'prod_qCom',
  'prod_vUnCom', 'prod_vProd', (*'prod_cEANTrib',*)
  'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
  (*'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
  'Tem_IPI', '_Ativo_', 'InfAdCuztm',
  'EhServico', 'ICMSRec_pRedBC', 'ICMSRec_vBC',
  'ICMSRec_pAliq', 'ICMSRec_vICMS', 'IPIRec_pRedBC',
  'IPIRec_vBC', 'IPIRec_pAliq', 'IPIRec_vIPI',
  'PISRec_pRedBC', 'PISRec_vBC', 'PISRec_pAliq',
  'PISRec_vPIS', 'COFINSRec_pRedBC', 'COFINSRec_vBC',
  'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*) 'MeuID',
  'Nivel1', 'GraGruX', 'UnidMedCom', 'UnidMedTrib'], [
  'FatID', 'FatNum', 'Empresa', 'nItem'], [
  prod_cProd, (*prod_cEAN, prod_xProd,
  prod_NCM, prod_EXTIPI, prod_genero,*)
  prod_CFOP, prod_uCom, prod_qCom,
  prod_vUnCom, prod_vProd, (*prod_cEANTrib,*)
  prod_uTrib, prod_qTrib, prod_vUnTrib,
  (*prod_vFrete, prod_vSeg, prod_vDesc,
  Tem_IPI, _Ativo_, InfAdCuztm,
  EhServico, ICMSRec_pRedBC, ICMSRec_vBC,
  ICMSRec_pAliq, ICMSRec_vICMS, IPIRec_pRedBC,
  IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI,
  PISRec_pRedBC, PISRec_vBC, PISRec_pAliq,
  PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
  COFINSRec_pAliq, COFINSRec_vCOFINS,*) MeuID,
  Nivel1, GraGruX, UnidMedCom, UnidMedTrib], [
  FatID, FatNum, Empresa, nItem], True);
          //
          QrMPIIts.Next;
        end;
      end;
    end;
    //
    QrMPI.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  Result := True;
end;

procedure TFmProsoft1.VerificaPQE();
var
  ide_serie, ide_mod, Versao, IDCtrl, FatID, FatNum, Empresa, ide_nNF,
  CriAForca,   CodInfoDest, CodInfoEmit, GraGruX: Integer;
  Id, ide_verProc, ide_natOp, ide_tpNF, ide_dEmi, ide_dSaiEnt, DataFiscal,
  ide_indPag, emit_CNPJ, emit_CPF, emit_IE, dest_CNPJ, dest_CPF, dest_IE: String;
  ICMSTot_vICMS, ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc,
  ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro, ICMSTot_vNF:
  Double;
  // Itens
  MeuID, Nivel1, nItem, UnidMedCom, UnidMedTrib: Integer;
  prod_CFOP, prod_uCom, prod_uTrib: String;
  prod_qCom, prod_vUnCom, prod_vProd, prod_qTrib, prod_vUnTrib: Double;
  //
  CFOP, CFOPa, CFOPb, CFOPc, CFOPd, CFOPe: String;
begin
  PB1.Position := 0;
  CriAForca := 1;
  if CkRecriarPQ.Checked then
  begin
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando notas de uso e consumo a serem excluidas');
    QrExcl.Close;
    QrExcl.Params[00].AsInteger := VAR_FATID_0151;
    QrExcl.Params[01].AsInteger := FEmpresa;
    QrExcl.Params[02].AsString  := FDataIni;
    QrExcl.Params[03].AsString  := FDataFim;
    UnDmkDAC_PF.AbreQuery(QrExcl, Dmod.MyDB);
    //
    if QrExcl.RecordCount > 0 then
    begin
      PB1.Max := QrExcl.RecordCount;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo notas de uso e consumo geradas a for�a');
      while not QrExcl.Eof do
      begin
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        DmNFe_0000.ExcluiNfe(QrExclStatus.Value, QrExclFatID.Value,
          QrExclFatNum.Value, QrExclEmpresa.Value, True, True);
        //
        QrExcl.Next;
      end;
    end;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando uso e consumo sem CFOP');
  PB1.Position := 0;
  QrPQsemCFOP.Close;
  UnDmkDAC_PF.AbreQuery(QrPQsemCFOP, Dmod.MyDB);
  if QrPQsemCFOP.RecordCount > 0 then
  begin
    PB1.Max := QrPQsemCFOP.RecordCount;
    CFOPa := FormatFloat('0000', EdCFOPa.ValueVariant);
    CFOPb := FormatFloat('0000', EdCFOPb.ValueVariant);
    CFOPc := FormatFloat('0000', EdCFOPc.ValueVariant);
    CFOPd := FormatFloat('0000', EdCFOPd.ValueVariant);
    CFOPe := FormatFloat('0000', EdCFOPe.ValueVariant);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE pqeits SET prod_CFOP=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    QrPQsemCFOP.First;
    while not QrPQsemCFOP.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      case QrPQsemCFOPAtivo.Value of
        1,2:
        begin
          if QrPQsemCFOPciUF.Value = QrPQsemCFOPiqUF.Value then
            CFOP := CFOPa
          else
            CFOP := CFOPb
        end;
        3,4:
        begin
          if QrPQsemCFOPciUF.Value = QrPQsemCFOPiqUF.Value then
            CFOP := CFOPc
          else
            CFOP := CFOPd
        end;
        else CFOP := CFOPe;
      end;
      if CFOP <> '0000' then
      begin
        Dmod.QrUpd.Params[00].AsString  := CFOP;
        Dmod.QrUpd.Params[01].AsInteger := QrPQsemCFOPControle.Value;
        Dmod.QrUpd.ExecSQL;
      end;
      QrPQsemCFOP.Next;
    end;
  end;
  PB1.Position := 0;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando entrada de uso e consumo sem dados fiscais');
  QrPQE.Close;
  QrPQE.Params[00].AsInteger := VAR_FATID_0051;
  QrPQE.Params[01].AsInteger := VAR_FATID_0151;
  QrPQE.Params[02].AsInteger := FEmpresa;
  UnDmkDAC_PF.AbreQuery(QrPQE, Dmod.MyDB);
  PB1.Max := QrPQE.RecordCount;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando dados fiscais para uso e consumo');
  QrPQE.First;
  while not QrPQE.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando dados fiscais para uso e consumo ' +
    IntToStr(QrPQE.RecNo) + ' de ' + IntToStr(QrPQE.RecordCount));
    //

    FatID                       := VAR_FATID_0151;
    FatNum                      := QrPQECodigo.Value;
    Empresa                     := QrPQECI.Value;
    CodInfoDest                 := QrPQECI.Value;
    CodInfoEmit                 := QrPQEIQ.Value;
    Id                          := QrPQErefNFe.Value;
    ide_verProc                 := '';
    ide_natOp                   := '';
    ide_mod                     := QrPQEmodNF.Value;
    ide_serie                   := QrPQESerie.Value;
    ide_tpNF                    := '1';
    ide_indPag                  := '1';
    ide_nNF                     := QrPQENF.Value;
    ide_dEmi                    := Geral.FDT(QrPQEDataE.Value, 1);
    ide_dSaiEnt                 := Geral.FDT(QrPQEDataS.Value, 1);
    DataFiscal                  := Geral.FDT(QrPQEData.Value, 1);
    versao                      := 0;

    if QrPQEDataE.Value < 2 then ide_dEmi := DataFiscal;
    if QrPQEDataS.Value < 2 then ide_dSaiEnt := DataFiscal;

    QrEnti.Close;
    QrEnti.Params[0].AsInteger  := QrPQEIQ.Value;
    UnDmkDAC_PF.AbreQuery(QrEnti, Dmod.MyDB);
    if QrEntiTipo.Value = 0 then
    begin
      emit_CNPJ := QrEntiCNPJ_CPF.Value;
      emit_CPF  := '';
    end else begin
      emit_CNPJ := '';
      emit_CPF  := QrEntiCNPJ_CPF.Value;
    end;
    emit_IE                     := QrEntiIE.Value;
    dest_CNPJ                   := DModG.QrEmpresasCNPJ_CPF.Value;
    dest_CPF                    := '';
    dest_IE                     := DModG.QrEmpresasIE.Value;

    ICMSTot_vICMS               := QrPQEICMS.Value;
    ICMSTot_vProd               := QrPQEValProd.Value;
    ICMSTot_vFrete              := QrPQEFrete.Value;
    ICMSTot_vSeg                := QrPQESeguro.Value;
    ICMSTot_vDesc               := QrPQEDesconto.Value;
    ICMSTot_vIPI                := QrPQEIPI.Value;
    ICMSTot_vPIS                := QrPQEPIS.Value;
    ICMSTot_vCOFINS             := QrPQECOFINS.Value;
    ICMSTot_vOutro              := QrPQEOutros.Value;
    ICMSTot_vNF                 := QrPQEValorNF.Value;

    IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);

    //ide_CUF                     := Geral.GetSiglaUF_do_CodigoUF(QrPQE);
    //EdFatNum.ValueVariant := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeavulsa', '', EdFatNum.ValueVariant);


    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False,[
'IDCtrl', (*'LoteEnv', 'versao',*)
'Id', (*'ide_cUF', 'ide_cNF',*)
'ide_natOp', 'ide_indPag', 'ide_mod',
'ide_serie', 'ide_nNF', 'ide_dEmi',
'ide_dSaiEnt', 'ide_tpNF', (*'ide_cMunFG',
'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
'ide_tpAmb', 'ide_finNFe', 'ide_procEmi',*)
'ide_verProc', 'emit_CNPJ', 'emit_CPF',
(*'emit_xNome', 'emit_xFant', 'emit_xLgr',
'emit_nro', 'emit_xCpl', 'emit_xBairro',
'emit_cMun', 'emit_xMun', 'emit_UF',
'emit_CEP', 'emit_cPais', 'emit_xPais',
'emit_fone',*) 'emit_IE', (*'emit_IEST',
'emit_IM', 'emit_CNAE',*) 'dest_CNPJ',
'dest_CPF', (*'dest_xNome', 'dest_xLgr',
'dest_nro', 'dest_xCpl', 'dest_xBairro',
'dest_cMun', 'dest_xMun', 'dest_UF',
'dest_CEP', 'dest_cPais', 'dest_xPais',
'dest_fone',*) 'dest_IE', (*'dest_ISUF',
'ICMSTot_vBC',*) 'ICMSTot_vICMS', (*'ICMSTot_vBCST',
'ICMSTot_vST',*) 'ICMSTot_vProd', 'ICMSTot_vFrete',
'ICMSTot_vSeg', 'ICMSTot_vDesc', (*'ICMSTot_vII',*)
'ICMSTot_vIPI', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
'ICMSTot_vOutro', 'ICMSTot_vNF', (*'ISSQNtot_vServ',
'ISSQNtot_vBC', 'ISSQNtot_vISS', 'ISSQNtot_vPIS',
'ISSQNtot_vCOFINS', 'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS',
'RetTrib_vRetCSLL', 'RetTrib_vBCIRRF', 'RetTrib_vIRRF',
'RetTrib_vBCRetPrev', 'RetTrib_vRetPrev', 'ModFrete',
'Transporta_CNPJ', 'Transporta_CPF', 'Transporta_XNome',
'Transporta_IE', 'Transporta_XEnder', 'Transporta_XMun',
'Transporta_UF', 'RetTransp_vServ', 'RetTransp_vBCRet',
'RetTransp_PICMSRet', 'RetTransp_vICMSRet', 'RetTransp_CFOP',
'RetTransp_CMunFG', 'VeicTransp_Placa', 'VeicTransp_UF',
'VeicTransp_RNTC', 'Cobr_Fat_nFat', 'Cobr_Fat_vOrig',
'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq', 'InfAdic_InfAdFisco',
'InfAdic_InfCpl', 'Exporta_UFEmbarq', 'Exporta_XLocEmbarq',
'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
'Status', 'infProt_Id', 'infProt_tpAmb',
'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
'infCanc_xJust', '_Ativo_', 'FisRegCad',
'CartEmiss', 'TabelaPrc', 'CondicaoPg',
'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',
'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
'DataFiscal', (*'protNFe_versao', 'retCancNFe_versao',
'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta', 'SINTEGRA_ExpNat',
'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta', 'SINTEGRA_ExpConhNum',
'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip', 'SINTEGRA_ExpPais',
'SINTEGRA_ExpAverDta',*) 'CodInfoEmit', 'CodInfoDest',
'CriAForca'], [
'FatID', 'FatNum', 'Empresa'], [
IDCtrl, (*LoteEnv, versao,*)
Id, (*ide_cUF, ide_cNF,*)
ide_natOp, ide_indPag, ide_mod,
ide_serie, ide_nNF, ide_dEmi,
ide_dSaiEnt, ide_tpNF, (*ide_cMunFG,
ide_tpImp, ide_tpEmis, ide_cDV,
ide_tpAmb, ide_finNFe, ide_procEmi,*)
ide_verProc, emit_CNPJ, emit_CPF,
(*emit_xNome, emit_xFant, emit_xLgr,
emit_nro, emit_xCpl, emit_xBairro,
emit_cMun, emit_xMun, emit_UF,
emit_CEP, emit_cPais, emit_xPais,
emit_fone,*) emit_IE, (*emit_IEST,
emit_IM, emit_CNAE,*) dest_CNPJ,
dest_CPF, (*dest_xNome, dest_xLgr,
dest_nro, dest_xCpl, dest_xBairro,
dest_cMun, dest_xMun, dest_UF,
dest_CEP, dest_cPais, dest_xPais,
dest_fone,*) dest_IE, (*dest_ISUF,
ICMSTot_vBC,*) ICMSTot_vICMS, (*ICMSTot_vBCST,
ICMSTot_vST,*) ICMSTot_vProd, ICMSTot_vFrete,
ICMSTot_vSeg, ICMSTot_vDesc, (*ICMSTot_vII,*)
ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
ICMSTot_vOutro, ICMSTot_vNF, (*ISSQNtot_vServ,
ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,
ISSQNtot_vCOFINS, RetTrib_vRetPIS, RetTrib_vRetCOFINS,
RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,
RetTrib_vBCRetPrev, RetTrib_vRetPrev, ModFrete,
Transporta_CNPJ, Transporta_CPF, Transporta_XNome,
Transporta_IE, Transporta_XEnder, Transporta_XMun,
Transporta_UF, RetTransp_vServ, RetTransp_vBCRet,
RetTransp_PICMSRet, RetTransp_vICMSRet, RetTransp_CFOP,
RetTransp_CMunFG, VeicTransp_Placa, VeicTransp_UF,
VeicTransp_RNTC, Cobr_Fat_nFat, Cobr_Fat_vOrig,
Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,
InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq,
Compra_XNEmp, Compra_XPed, Compra_XCont,
Status, infProt_Id, infProt_tpAmb,
infProt_verAplic, infProt_dhRecbto, infProt_nProt,
infProt_digVal, infProt_cStat, infProt_xMotivo,
infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,
infCanc_cStat, infCanc_xMotivo, infCanc_cJust,
infCanc_xJust, _Ativo_, FisRegCad,
CartEmiss, TabelaPrc, CondicaoPg,
FreteExtra, SegurExtra, ICMSRec_pRedBC,
ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
DataFiscal, (*protNFe_versao, retCancNFe_versao,
SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_ExpNat,
SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum,
SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais,
SINTEGRA_ExpAverDta,*) CodInfoEmit, CodInfoDest,
CriAForca], [
FatID, FatNum, Empresa], True) then
    begin
      QrPQEIts.Close;
      QrPQEIts.Params[0].AsInteger := QrPQECodigo.Value;
      UnDmkDAC_PF.AbreQuery(QrPQEIts, Dmod.MyDB);
      nItem := 0;
      while not QrPQEIts.Eof do
      begin
        nItem        := nItem + 1;
        MeuID        := QrPQEItsControle.Value;
        Nivel1       := QrPQEItsInsumo.Value;
        DmProd.ObtemPrimeiroGraGruXdeGraGru1(FatID, FatNum, nItem,
          QrPQEItsNOMEPQ.Value, True, Nivel1, GraGruX);
        prod_CFOP    := QrPQEItsprod_CFOP.Value;
        DmProd.Obtem_prod_uTribdeGraGru1(Nivel1, prod_uCom, UnidMedCom);
        UnidMedTrib  := UnidMedCom;
        prod_uTrib   := prod_uTrib;
        prod_qCom    := QrPQEItsTotalPeso.Value;
        prod_qTrib   := QrPQEItsTotalPeso.Value;
        if QrPQEItsTotalPeso.Value > 0 then
          prod_vUnCom  := QrPQEItsTotalCusto.Value / QrPQEItsTotalPeso.Value
        else prod_vUnCom  := 0;
        prod_vUnTrib := prod_vUnCom;
        prod_vProd   := QrPQEItsTotalCusto.Value;

        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
(*'prod_cProd', 'prod_cEAN', 'prod_xProd',
'prod_NCM', 'prod_EXTIPI', 'prod_genero',*)
'prod_CFOP', 'prod_uCom', 'prod_qCom',
'prod_vUnCom', 'prod_vProd', (*'prod_cEANTrib',*)
'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
(*'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
'Tem_IPI', '_Ativo_', 'InfAdCuztm',
'EhServico', 'ICMSRec_pRedBC', 'ICMSRec_vBC',
'ICMSRec_pAliq', 'ICMSRec_vICMS', 'IPIRec_pRedBC',
'IPIRec_vBC', 'IPIRec_pAliq', 'IPIRec_vIPI',
'PISRec_pRedBC', 'PISRec_vBC', 'PISRec_pAliq',
'PISRec_vPIS', 'COFINSRec_pRedBC', 'COFINSRec_vBC',
'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*) 'MeuID',
'Nivel1', 'GraGruX', 'UnidMedCom', 'UnidMedTrib'], [
'FatID', 'FatNum', 'Empresa', 'nItem'], [
(*prod_cProd, prod_cEAN, prod_xProd,
prod_NCM, prod_EXTIPI, prod_genero,*)
prod_CFOP, prod_uCom, prod_qCom,
prod_vUnCom, prod_vProd, (*prod_cEANTrib,*)
prod_uTrib, prod_qTrib, prod_vUnTrib,
(*prod_vFrete, prod_vSeg, prod_vDesc,
Tem_IPI, _Ativo_, InfAdCuztm,
EhServico, ICMSRec_pRedBC, ICMSRec_vBC,
ICMSRec_pAliq, ICMSRec_vICMS, IPIRec_pRedBC,
IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI,
PISRec_pRedBC, PISRec_vBC, PISRec_pAliq,
PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
COFINSRec_pAliq, COFINSRec_vCOFINS,*) MeuID,
Nivel1, GraGruX, UnidMedCom, UnidMedTrib], [
FatID, FatNum, Empresa, nItem], True);
        //
        QrPQEIts.Next;
      end;
    end;
    //
    QrPQE.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmProsoft1.NotasDeEntrada();
//var
  //Especie: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando pesquisa para exportar notas de entrada');
  PB1.Position := 0;
  RECabE.Lines.Clear;
  REItsE.Lines.Clear;
  //

  ReopenCabA();

  PB1.Max := QrCabA.RecordCount;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exportando notas de entrada');
  while not QrCabA.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    FExp := 'Notas de entrada';
    FLin := '';
{

}
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Notas de Entrada
//Descri��o do Campo
//Posi��es
//Inicial Tamanho
//Observa��es
//Filler 1 4 Deixar em branco
    IL(001, 004, CSS(' ', 004, ''));
//CNPJ do emitente 5 14 Preencher sem pontua��o
    IL(005, 014, CSS('0', 014, QrCabACNPJ_CPF_emit.Value, False));
//Data da entrada 19 6 Formato DDMMAA
    IL(019, 006, FD6(QrCabAide_dSaiEnt.Value));
//Data da emiss�o 25 6 Formato DDMMAA
    IL(025, 006, FD6(QrCabAide_dEmi.Value));
//N�mero do documento 31 6 Num�rico com zeros a esquerda
    IL(031, 006, CSS('0', 006, Copy(FormatFloat('000000000', QrCabAide_nNF.Value), 4, 6)));
//Esp�cie do documento 37 3 Alfanum�rico-0 a 9/A a Z
    IL(037, 003, CSS(' ', 003, QrCabASigla.Value));
//S�rie e subs�rie 40 3 Alfanum�rico-0 a 9/A a Z
    IL(040, 003, CSS(' ', 003, FormatFloat('0',QrCabAide_serie.Value)));
//Item de desdobramento 43 1 Alfanum�rico-0 a 9/A a Z
    IL(043, 001, CSI('0', 001, 0)); // Parei aqui: Item de desdobramento - deve ser zero quando n�o tem
//C�digo cont�bil (P.Contas) 44 5 Num�rico com zeros a esquerda
    IL(044, 005, CSS(' ', 005, '')); // Parei aqui: C�digo cont�bil para contas
//CFOP antigo de 3 d�gitos 49 3 Num�rico com zeros a esquerda
    IL(049, 003, '000'); // Parei aqui! CFOP antigo
//Valor total mercadorias 52 14 ###########.##
    IL(052, 014, CSF('0', 014, 002, QrCabAICMSTot_vProd.Value));
//Base de c�lculo do ICMS 66 14 ###########.##
    IL(066, 014, CSF('0', 014, 002, QrCabAICMSTot_vBC.Value));
//ICMS creditado 80 14 ###########.##
    IL(080, 014, CSF('0', 014, 002, QrCabAICMSTot_vICMS.Value));
//Isentas / n�o tributadas 94 14 ###########.##
    IL(094, 014, CSF('0', 014, 002, 0)); // Parei aqui! Isentas/n�o tributadas
//Outras 108 14 ###########.##
    IL(108, 014, CSF('0', 014, 002, QrCabAICMSTot_vProd.Value - QrCabAICMSTot_vBC.Value)); // Parei aqui! Outras
//IPI n�o creditado 122 14 ###########.##
    IL(122, 014, CSF('0', 014, 002, 0)); // Parei aqui! IPI n�o creditado
//Al�quota do ICMS 136 5 ##.##
    IL(136, 005, CSF('0', 005, 002, 0)); // Parei aqui! Al�quota de ICMS
//Base de c�lculo do IPI 141 14 ###########.##
    IL(141, 014, CSF('0', 014, 002, 0)); // Parei aqui! Base de c�lculo do IPI
//IPI Creditado 155 14 ###########.##
    IL(155, 014, CSF('0', 014, 002, 0)); // Parei aqui! IPI Creditado
//IPI Creditado 50% 169 14 ###########.##
    IL(169, 014, CSF('0', 014, 002, 0)); // Parei aqui! IPI Creditado 50%
//Isentas / N�o tributadas 183 14 ###########.##
    IL(183, 014, CSF('0', 014, 002, 0)); // Parei aqui! Isentas / n�o tributadas
//Outras 197 14 ###########.##
    IL(197, 014, CSF('0', 014, 002, 0)); // Parei aqui! Outras
//Val.total nota (Contabil) 211 14 ###########.##
    IL(211, 014, CSF('0', 014, 002, QrCabAICMSTot_vNF.Value));
//Condi��es de pagamento 225 1 0(� vista) 1(� prazo)
    IL(225, 001, CSI('0', 001, QrCabAide_indPag.Value));

//Classif.Contab.-Integra��o 226 2 Num�rico com zeros a esquerda
    IL(226, 002, '00'); // Parei aqui! Classif. Contab - Integracao
//Sit.especial - c�digo 228 3 Num�rico com zeros a esquerda
    IL(228, 003, CSS(' ', 003, '')); // Parei aqui! Situa��o especial - C�digo
//Sit.especial - valor 1 231 12 #########.##
    IL(231, 012, CSS(' ', 012, '')); // Parei aqui! - Valor 1
//Sit.especial - valor 2 243 12 #########.##
    IL(243, 012, CSS(' ', 012, '')); // Parei aqui! - Valor 2
//Sit.especial - valor 3 255 12 #########.##
    IL(255, 012, CSS(' ', 012, '')); // Parei aqui! - Valor 3
//Sit.especial - valor 4 267 12 #########.##
    IL(267, 012, CSS(' ', 012, '')); // Parei aqui! - Valor 4
//Observa��o 279 100 Alfanum�rico-0 a 9/A a Z
    IL(279, 100, CSS(' ', 100, dmkPF.SoTextoLayout(QrCabAInfAdic_InfAdFisco.Value)));
    // D E S A T I V A D O
//1o Vencimento 379 6 Desativado ver 'Faturas'
    IL(379, 006, CSS(' ', 006, '')); // Desativado ver faturas
//1a Parcela 385 14 Desativado ver 'Faturas'
    IL(385, 014, CSS(' ', 014, '')); // Desativado ver faturas
//2o Vencimento 399 6 Desativado ver 'Faturas'
    IL(399, 006, CSS(' ', 006, '')); // Desativado ver faturas
//2a Parcela 405 14 Desativado ver 'Faturas'
    IL(405, 014, CSS(' ', 014, '')); // Desativado ver faturas
//3o Vencimento 419 6 Desativado ver 'Faturas'
    IL(419, 006, CSS(' ', 006, '')); // Desativado ver faturas
//3a Parcela 425 14 Desativado ver 'Faturas'
    IL(425, 014, CSS(' ', 014, '')); // Desativado ver faturas
//4o Vencimento 439 6 Desativado ver 'Faturas'
    IL(439, 006, CSS(' ', 006, '')); // Desativado ver faturas
//4a Parcela 445 14 Desativado ver 'Faturas'
    IL(445, 014, CSS(' ', 014, '')); // Desativado ver faturas
//5o Vencimento 459 6 Desativado ver 'Faturas'
    IL(459, 006, CSS(' ', 006, '')); // Desativado ver faturas
//5a Parcela 465 14 Desativado ver 'Faturas'
    IL(465, 014, CSS(' ', 014, '')); // Desativado ver faturas
//6o Vencimento 479 6 Desativado ver 'Faturas'
    IL(479, 006, CSS(' ', 006, '')); // Desativado ver faturas
//6a Parcela 485 14 Desativado ver 'Faturas'
    IL(485, 014, CSS(' ', 014, '')); // Desativado ver faturas
    // Fim desativado
//Insc.Estadual fornecedor 499 18
    IL(499, 018, CSS(' ', 018, QrCabAemit_IE.Value));
//UF Insc.Estad.fornecedor 517 2 Alfanum�rico A a Z
    IL(517, 002, CSS(' ', 002, QrCabAemit_UF.Value));
//Tipo de Frete 519 1 1=CIF, 2=FOB
    IL(519, 001, CSI(' ', 001, QrCabAModFrete.Value + 1));
    // Campos espec�ficos para os estados do Esp�rito Santo e Minas Gerais
//Campos espec�ficos para o Estado do Esp�rito Santo e Minas Gerais.
//P�gina 1 de 22
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Notas de Entrada
//Descri��o do Campo
//Posi��es
//Inicial Tamanho
//Observa��es
//Sigla Sit.Trib.Saida (1) 520 5
    IL(520, 005, CSS(' ', 005, '')); // Sigla sit. Trib. Saida 1
//Valor Sit.Trib.Saida (1) 525 14 ###########.##
    IL(525, 014, CSF('0', 014, 002, 0)); // Valor sit. Trib. Saida 1
    //
//D�gito adicional para CFOP x99 no Estado de S�o Paulo 539 1 Deixar em branco para outras CFOPs ou empresas de outros Estados.
    IL(539, 001, ' '); // D�gito adicional para CFOP x99 no Estado de S�o Paulo
    // Campos espec�ficos para o Estado de Minas Gerais.
//Campos espec�ficos para o Estado de Minas Gerais.
//Sigla Sit.Trib.Saida (2) 540 5
    IL(540, 005, CSS(' ', 005, '')); // Sigla sit. Trib. Saida 2
//Valor Sit.Trib.Saida (2) 545 14 ###########.##
    IL(545, 014, CSF('0', 014, 002, 0)); // Valor sit. Trib. Saida 2
//Sigla Sit.Trib.Saida (3) 559 5
    IL(559, 005, CSS(' ', 005, '')); // Sigla sit. Trib. Saida 3
//Valor Sit.Trib.Saida (3) 564 14 ###########.##
    IL(564, 014, CSF('0', 014, 002, 0)); // Valor sit. Trib. Saida 3
//Sigla Sit.Trib.Saida (4) 578 5
    IL(578, 005, CSS(' ', 005, '')); // Sigla sit. Trib. Saida 4
//Valor Sit.Trib.Saida (4) 583 14 ###########.##
    IL(583, 014, CSF('0', 014, 002, 0)); // Valor sit. Trib. Saida 4
//Valor Parcela Isenta Sa�da 597 14 ###########.##
    IL(597, 014, CSF('0', 014, 002, 0)); // Valor Parcela Isenta Sa�da
//D�gito adicional para CFOP x99 outros Estados 611 3 01 a 99 - Deixar em branco para outras CFOPs - Alinhado a esquerda com
//brancos a direita.
    IL(611, 003, '   '); // D�gito adicional para CFOP x99 outros Estados
//Ano da AIDF 614 4 Somente para Sergipe
    IL(614, 004, '0000'); // Somente para Sergipe
//N�mero da AIDF 618 6 Somente para Sergipe
    IL(618, 006, '000000'); // Somente para Sergipe
//CFOP Novo de 4 d�gitos 624 4 Num�rico
    IL(624, 004, CSI('0', 004, QrItsIprod_CFOP.Value)); // Num�rico
//C�digo Munic�pio-Forneced. 628 10 Complemento cpos 44 e 45
    IL(628, 010, CSI('0', 010, QrCabAemit_cMun.Value)); // C�digo Munic�pio-Forneced.
    // - TRANSPORTADOR - (S� COMBUST�VEIS)
    // Local de Saida (Origem)
//- TRANSPORTADOR - (S� COMBUST�VEIS)
//Local de Saida (Origem)
//CNPJ/CPF
    IL(638, 014, CSS(' ', 014, '')); // Preencher sem pontua��o, texto alinhado � direita completar com brancos
//Inscri��o Estadual
    IL(652, 018, CSS(' ', 018, ''));
//UF
    IL(670, 002, '  ');
//Local de Entrega/Recebto (Destino)
//CNPJ/CPF
    IL(672, 014, CSS(' ', 014, ''));// Preencher sem pontua��o, texto alinhado � direita completar com brancos
//Inscri��o Estadual
    IL(686, 018, CSS(' ', 018, ''));
//UF
    IL(704, 002, '  ');
//Dados do Transportador
//CNPJ
    IL(706, 014, CSS(' ', 014, ''));// Preencher sem pontua��o
//Inscri��o Estadual
    IL(720, 018, CSS(' ', 018, ''));
//UF
    IL(738, 002, '  ');
//Modal
    IL(740, 001, '1');  //>>>>>>> Tabela na p�gina 4 <<<<<<<<
//Placa 1
    IL(741, 007, CSS(' ', 007, ''));
//UF da Placa 1
    IL(748, 002, '  ');
//Placa 2
    IL(750, 007, CSS(' ', 007, ''));
//UF da Placa 2
    IL(757, 002, '  ');
//Placa 3
    IL(759, 007, CSS(' ', 007, ''));
//UF da Placa 3
    IL(766, 002, '  ');
//Movimenta��o f�sica do combust�vel
    IL(768, 001, ' ');// 'S'/'N'/' '
//Class.Contab.-Integra��o
    IL(769, 004, '0000');// Num�rico com zeros a esquerda
//C�digo da Dipam
    IL(773, 002, '00');// C�digo Oficial da Dipam: 11,12,13,22,23,24,25,26,31,35,36,37 e 38
//Base de C�lculo Subst. Tribut.
    IL(775, 014, CSF('0', 014, 002, 0)); // ###########.##
//Imposto Retido Subst. Tribut.
    IL(789, 014, CSF('0', 014, 002, 0)); //###########.##
//C�digo Recolhimento PIS/COFINS
    IL(803, 006, CSS(' ', 006, '')); // Se n�o for informado, deixar em branco
//Nota Cancelada
    IL(809, 001, '0'); //0-N�o Cancelada, 1-Cancelada
//Espa�o reservado
    IL(810, 006, CSS(' ', 006, '')); // Preencher com brancos
//Valor do Frete
    IL(816, 014, CSF('0', 014, 002, QrCabAICMSTot_vFrete.Value)); // ###########.##
//Valor do Seguro
    IL(830, 014, CSF('0', 014, 002, QrCabAICMSTot_vSeg.Value)); // ###########.##
{
P�gina 2 de 22

L e i a u t e d e I mp o r t a � � o
Ar q u i v o T e x t o
Modelo: Notas de Entrada
Descri��o do Campo
Posi��es
Inicial Tamanho
Observa��es
}
//Valor Outras Despesas
    IL(844, 014, CSF('0', 014, 002,  QrCabAICMSTot_vOutro.Value)); //  ###########.##
//Esp�cie do documento (Windows)
    IL(858, 005, CSS(' ', 005, '')); //  Alfanum�rico-0 a 9/A a Z
//IPI na Base do ICMS
    IL(863, 001, '0'); // 0-N�o, 1-Sim
//S�rie
    IL(864, 003, CSS(' ', 003, '')); // Alfanum�rico-0 a 9/A a Z
//Subs�rie
    IL(867, 002, CSS(' ', 002, '')); // Alfanum�rico-0 a 9/A a Z
//N�mero do documento com 10 d�gitos
    IL(869, 010, CSI('0', 010, 0)); // Num�rico com zeros a esquerda
//N�mero do Dispositivo Inicial
    IL(879, 010, CSI('0', 010, 0)); // Num�rico com zeros a esquerda
//N�mero do Dispositivo Final
    IL(889, 010, CSI('0', 010, 0)); // Num�rico com zeros a esquerda
//C�digo Centro de Custo
    IL(899, 005, CSI('0', 005, 0)); // Num�rico, formato 99999
//C�digo da Tabela Simples Nacional  >>>>>>>>>>>>> ver tabela na p�gina 5 <<<<<<<<<<<<<<<
    IL(904, 003, CSI('0', 003, 10)); // N�merico com zeros a esquerda (999). Campo n�o obrigat�rio
//Hora de Emiss�o NF
    IL(907, 008, '00:00:00'); // Formato: HH:MM:SS
//Hora da Entrada
    IL(915, 008, '00:00:00'); // Formato: HH:MM:SS
//SPED - Valor Cobrado de Terceiros
    IL(923, 014, CSF('0', 014, 002, 0)); //  ###########.##
//MG.GAM57 - Valor do Frete
    IL(937, 014, CSF('0', 014, 002, 0)); //  ###########.##
//MG.GAM57 - Aliquota ICMS Frete
    IL(951, 008, CSF('0', 008, 004, 0)); //  ###.####
//MG.GAM57 - Valor do ICMS Sobre Frete
    IL(959, 014, CSF('0', 014, 002, 0)); //  ###########.##
//SPED - Chave Fiscal Eletr�nica
    IL(973, 044, CSS(' ', 044, QrCabAId.Value)); // Alfanum�rico-0 a 9/A a Z
//SPED.Tipo de t�tulo de cobran�a
    IL(1017, 002, '00');
//00 - Duplicata
//01 - Cheque
//02 - Promiss�ria
//03 - Recibo
//99 - Outros(descrever)
//SPED.Descri��o t�tulo de cobran�a
    IL(1019, 040, CSS(' ', 040, '')); // Alfanum�rico (somente se "Tipo de t�tuo de cobran�a" for igual a 99)
//SPED.N�mero do processo
    IL(1059, 030, CSS(' ', 030, '')); // Alfanum�rico
//SPED.Indicador Origem do processo
    IL(1089, 001, '1');
//1 - Sefaz
//2 - Justi�a Estadual
//3 - Justi�a Federal
//4 - Secex/RFB
//9 - Outros
//Substituto Tribut�rio
    IL(1090, 001, '0');
//0 = N�o Substituto;
//1 = Substituto
//Tipo de Nota Associada
    IL(1091, 003, CSS(' ', 003, '')); // Preencher conforme tabela constante nas Observa��es
{
P�gina 3 de 22

L e i a u t e d e I mp o r t a � � o
Ar q u i v o T e x t o
Outras Observa��es
}
//Campo: Posi��o 1091 x 003 => Tipo de Nota Associada
//C�d UF Correspondente
//1 Zona Franca de Manaus
//2 Entrada de Produtor Rural
//3 Entr.Interestadual Dif.Al�quota
//4 Sa�da de Produtor Rural
//5 ES FUNDAP - Esp�rito Santo
//6 Entrada de sa�da sem d�bito
//7 Sa�da n�o entra no c�lc.CIAP
//8 Sa�da 'Outras' n�o tributada
//9 Transfer�ncia de D�bito
//10 Transfer�ncia de Cr�dito
//11 Ressarcimento
//12 Transfer�ncia de D�bito IPI
//13 Transfer�ncia de Cr�dito IPI
//14 Ressarcimento de IPI
//15 Nota Entrada emitida Contribuinte
//16 Transfer�ncia Ativo Imobilizado
//17 Outras Entradas p/ Agregado
//18 NF N�o Comp�e C�lculo
//19 MG Ressarcimento ST
//20 MG Estorno de D�bito
//21 MG Compensa��o Saldo
//22 MG Opera��es com Caf�
//23 MG Sa�da p/ Com�rcio
//24 MG Sa�da p/ Ind�stria
//25 MG Cred.Aprov.DECONCAF�
//28 ES Sa�da c/ICMS Recolhido no Ato
//29 ES Sa�da N�o Comp�e Base Enquadramento
//30 ES Compra N�o Contribuinte
//31 ES N�o Inclui na DOT
//32 RS Entrada s/ Cr�d.
//33 RS Sa�da s/ D�b.
//34 SE Utiliza��o do Cr�dito Acumulado
//35 Sa�da Consumidor Final
//36 ES Devolu��o Cr�dito ICMS(ES)
//37 PR Devolu��o de Compras comp�e Receita do m�s (PR)
//38 IPI na Base do ICMS
//39 RJ Entrada s/Reten��o ST
//40 GO PROTEGE (GO)
//41 GO SUCATA
//42 SC Venda do Ativo Imobilizado comp�e a Receita do m�s
//43 CE Terceiro Consignat�rio
//44 CE Terceiro Consignante
//45 CE Cr�dito Antecipado
//46 CE ICMS antecipado a recolher
//47 CE ICMS Difer.Al�quota a recolher
//48 CE ICMS S.T.entradas a recolher
//49 CE Prod.prim�rio/Regime especial
//50 PB ICMS-S.T.Entradas j� recolhida
//51 PB ICMS Retido na Fonte
//52 N�o Incid�ncia / Imunidade
//53 SC Presta��o de Servi�os ISS, p/ Qd01-Entradas
//54 SC 25% Transfer�cia recebidas a preco de venda varejo
//55 SC IPI Aquis.Mat.Prima/Merc.se lan�ada no Qd01-Entrad
//56 SC ICMS Ret.Sub.Tribut�ria se lan�ada no Qd01-Entrada
//57 SC Subs�dios concedidos por governos fed./estad./muni
//58 PR Sa�das com Isen��o/Imunidade/Suspens�o Imposto(PR)
//59 CE Estorno Cr�dito e D�bito
//60 PB PBSim Entradas j� recolhida
//61 PB PBSim Entradas a recolher
//62 PB PBSim Substitui��o por sa�das
//63 PB PBSim D�bitos por sa�das recolhimento Fonte
//64 RS Sa�das com Diferimento
//65 SC Sa�da - DIME Quadro 48
//67 BA BA-Icms retido sub.trib.-DMA
//Campo: Posi��o 740 x 001 => Modalidade de Transporte
//1 - Rodovi�rio
//2 - Ferrovi�rio
//3 - Rodo-Ferrovi�rio
//4 - Aquavi�rio
//5 - Dutovi�rio
//6 - A�reo
//7 - Outras
//P�gina 4 de 22
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Campo: Posi��o 904 x 003 => C�digo Tabela Simples Nacional
//ANEXO I - PARTILHA DO SIMPLES NACIONAL - COM�RCIO
//C�digo Anexo Se��o Tabela Descri��o
//001 01 01 01 Revenda Merc. Sem Subst.Tribut.
//002 01 02 01 Revenda Merc. Com Subst.Tribut. So do ICMS
//003 01 02 02 Revenda Merc. Com Subst.Tribut. De PIS, ICMS
//004 01 02 03 Revenda Merc. Com Subst.Tribut. De COFINS, ICMS
//005 01 02 04 Revenda Merc. Com Subst.Tribut. De COFINS, Pis/Pasep, ICMS
//006 01 02 05 Revenda Merc. Com Subst.Tribut. So do Pis/Pasep
//007 01 02 06 Revenda Merc. Com Subst.Tribut. So da COFINS
//008 01 02 07 Revenda Merc. Com Subst.Tribut. De Pis/Pasep, COFINS
//009 01 03 01 Revenda Merc. Para exportacao
//ANEXO II - PARTILHA DO SIMPLES NACIONAL - IND�STRIA
//C�digo Anexo Se��o Tabela Descri��o
//010 02 01 01 Venda Merc.Indust. Sem substitui��o tribut�ria
//011 02 02 01 Venda Merc.Indust. Com Subs.Trib. So do IPI
//012 02 02 02 Venda Merc.Indust. Com Subs.Trib. De IPI, ICMS
//013 02 02 03 Venda Merc.Indust. Com Subs.Trib. De IPI, PIS/PASEP
//014 02 02 04 Venda Merc.Indust. Com Subs.Trib. De IPI, COFINS
//015 02 02 05 Venda Merc.Indust. Com Subs.Trib. De IPI, PIS/PASEP, COFINS
//016 02 02 06 Venda Merc.Indust. Com Subs.Trib. De IPI, ICMS, PIS/PASEP
//017 02 02 07 Venda Merc.Indust. Com Subs.Trib. De IPI, ICMS, COFINS
//018 02 02 08 Venda Merc.Indust. Com Subs.Trib. De IPI, ICMS,PIS/COFINS
//019 02 02 09 Venda Merc.Indust. Com Subs.Trib. So do ICMS
//020 02 02 10 Venda Merc.Indust. Com Subs.Trib. De ICMS, PIS/PASEP
//021 02 02 11 Venda Merc.Indust. Com Subs.Trib. De ICMS, COFINS
//022 02 02 12 Venda Merc.Indust. Com Subs.Trib. De ICMS, PIS/PASEP, COFINS
//023 02 02 13 Venda Merc.Indust. Com Subs.Trib. So do PIS/PASEP
//024 02 02 14 Venda Merc.Indust. Com Subs.Trib. De PIS/PASEP, COFINS
//025 02 02 15 Venda Merc.Indust. Com Subs.Tribut. So da COFINS
//026 02 03 01 Venda Merc.Indust. Para exporta��o
//ANEXO III - PARTILHA DO SIMPLES NACIONAL - SERVI�OS E LOCA��O DE BENS M�VEIS
//C�digo Anexo Se��o Tabela Descri��o
//027 03 01 01 Loca��o de bens m�veis
//028 03 02 01 Serv.Inc. I a XII Sem reten��o, ISS devido a outro Munic�pio
//029 03 03 01 Serv.Inc. I a XII Sem reten��o, ISS devido ao pr�prio Munic
//030 03 04 01 Serv.Inc. I a XII Com reten��o
//040 03 05 01 Serv.Transp.intermunicip. e interestadual de carga sem reten��o a partir de 01/2008
//041 03 05 02 Serv.Transp.intermunicip. e interestadual de carga com reten��o a partir de 01/2008
//042 03 06 01 Servi�os Escrit�rios Cont�beis a partir de 01/2009
//ANEXO IV - PARTILHA DO SIMPLES NACIONAL - SERVI�OS
//C�digo Anexo Se��o Tabela Descri��o
//031 04 01 01 Serv.Inc. XIII a XVIII Sem reten��o, ISS dev. Outro Munic�p
//032 04 02 01 Serv.Inc. XIII a XVIII Sem reten��o, ISS dev.Pr�prio Munic�p
//033 04 03 01 Serv.Inc. XIII a XVIII Com reten��o
//ANEXO V - PARTILHA DO SIMPLES NACIONAL - SERVI�OS
//C�digo Anexo Se��o Tabela Descri��o
//034 05 01 Serv.Inc. XIX a XIVI e XXVI Sem reten��o, ISS dev outro Munic.
//035 05 02 Serv.Inc. XIX a XIVI e XXVI Sem reten��o,ISS dev.pr�prio Munic
//036 05 03 Serv.Inc. XIX a XIVI e XXVI Com reten��o
//037 05 04 Escrit�rios de servi�os cont�beis at� 12/2008
//038 05 05 Serv.Transp.intermunicip. e interestadual de carga sem reten��o at� 12/2007
//039 05 06 Serv.Transp.intermunicip. e interestadual de carga com reten��o at� 12/2007
//P�gina 5 de 22
    //
    //
    RECabE.Lines.Add(FLin);
    //
    ItensDeNotaDeEntrada();
    //
    QrCabA.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando exporta��o de notas de entrada');
  MyObjects.ExportaMemoToFileExt(TMemo(RECabE), FNFS_Entrada, True, False, etxtsaSemAcento, FQuebraDeLinha, Null);
  MyObjects.ExportaMemoToFileExt(TMemo(REItsE), FItens_NFs_Entrada, True, False, etxtsaSemAcento, FQuebraDeLinha, Null);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  PB1.Position := 0;
end;

procedure TFmProsoft1.NotasDeSaida();
var
  Data: TDateTime;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando pesquisa para exportar notas de entrada');
  PB1.Position := 0;
  RECabS.Lines.Clear;
  REItsS.Lines.Clear;
  //
  ReopenCabA();
  {
  QrCabA.Close;
  QrCabA.SQL.Clear;
  QrCabA.SQL.Add('SELECT * FROM nfecaba');
  QrCabA.SQL.Add('WHERE empresa=:P0');
  QrCabA.SQL.Add('AND emit_CNPJ=:P1');
  QrCabA.SQL.Add('AND DataFiscal BETWEEN :P2 AND :P3');
  QrCabA.Params[00].AsInteger := FEmpresa;
  QrCabA.Params[01].AsString  := FCNPJ;
  QrCabA.Params[02].AsString  := FDataIni;
  QrCabA.Params[03].AsString  := FDataFim;
  UnDmkDAC_PF.AbreQuery(QrCabA, Dmod.MyDB);
  }
  PB1.Max := QrCabA.RecordCount;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exportando notas de sa�da');
  while not QrCabA.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    FExp := 'Notas de sa�da';
    FLin := '';
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Notas de Sa�da
//Descri��o do Campo
//Posi��es
//Inicial Tamanho
//Observa��es
//Esp�cie do documento
    IL(001, 003, CSS(' ', 003, QrCabASigla.Value)); // Alfanum�rico - esp�cie da nota ou *CT
//S�rie e subs�rie
    IL(004, 003, CSS(' ', 003, '')); // ver campos 984 3 a 987 2 FormatFloat('0',QrCabAide_serie.Value))); // Alfanum�rico-0 a 9/A a Z
//N�mero da Nota Fiscal ( Inicial )
    IL(007, 006, CSS(' ', 006, '')); // Copy(FormatFloat('000000000', QrCabAide_nNF.Value), 4, 6))); // Num�rico com zeros a esquerda
//N�mero da Nota Fiscal ( Final )
    IL(013, 006, CSS(' ', 006, '')); // Copy(FormatFloat('000000000', QrCabAide_nNF.Value), 4, 6))); // Num�rico com zeros a esquerda
//Item de desdobramento
    IL(019, 001, CSI('0', 001, 0)); // Alfanum�rico-0 a 9/A a Z - Zero quando n�o tem
//Data da sa�da
    Data := QrCabAide_dSaiEnt.Value;
    if Data < 2 then Data :=  QrCabAide_dEmi.Value;
    IL(020, 006, FD6(Data)); // Formato DDMMAA
//C�digo cont�bil
    IL(026, 005, CSS(' ', 005, '')); // Num�rico com zeros a esquerda. Deixar em branco quando n�o tem
//CFO antigo com 3 digitos
    IL(031, 003, CSI('0', 003, 0)); // Num�rico com zeros a esquerda
//Valor cont�bil da nota
    IL(034, 014, CSF('0', 014, 002, QrCabAICMSTot_vNF.Value)); // ###########.##
//Valor das mercadorias
    IL(048, 014, CSF('0', 014, 002, QrCabAICMSTot_vProd.Value)); // ###########.##
//Base de c�lculo do ICMS
    IL(062, 014, CSF('0', 014, 002, QrCabAICMSTot_vBC.Value)); // ###########.##
//ICMS debitado
    IL(076, 014, CSF('0', 014, 002, QrCabAICMSTot_vICMS.Value)); // ###########.##
//Isentas/n�o tribut. ICMS
    IL(090, 014, CSF('0', 014, 002, 0)); // ###########.##
//Outras ICMS
    IL(104, 014, CSF('0', 014, 002, QrCabAICMSTot_vNF.Value - QrCabAICMSTot_vBC.Value)); // ###########.##
//Base de c�lculo do IPI
    IL(118, 014, CSF('0', 014, 002, 0)); // ###########.## Parei aqui
//IPI debitado
    IL(132, 014, CSF('0', 014, 002, QrCabAICMSTot_vIPI.Value)); // ###########.##
//Isentas/n�o tribut. IPI
    IL(146, 014, CSF('0', 014, 002, 0)); // ###########.##
//Outras IPI
    IL(160, 014, CSF('0', 014, 002, QrCabAICMSTot_vNF.Value - QrCabAICMSTot_vBC.Value)); // ###########.## Parei aqui
//IPI n.creditado/Cupons Cancelados
    IL(174, 014, CSF('0', 014, 002, 0)); // ###########.## Parei aqui
//Al�quota do ICMS
    IL(188, 005, CSF('0', 005, 002, 0)); // ##.## - Parei aqui
//Classif.Contab.-Integra��o
    IL(193, 002, '00'); // Num�rico com zeros a esquerda
//Sit.Especial - c�digo
    IL(195, 003, CSS(' ', 003, '')); // Num�rico com zeros a esquerda
//Sit.Especial - valor 1
    IL(198, 014, CSS(' ', 014, '')); // ###########.##
//Sit.Especial - valor 2
    IL(212, 014, CSS(' ', 014, '')); // ###########.##
//Sit.Especial - valor 3
    IL(226, 014, CSS(' ', 014, '')); // ###########.##
//Sit.Especial - valor 4
    IL(240, 014, CSS(' ', 014, '')); // ###########.##
//Cliente (CNPJ)
    IL(254, 014, CSS('0', 014, QrCabACNPJ_CPF_dest.Value)); // Preencher sem pontua��o
//Cliente (c�digo cont�bil)
    IL(268, 005, CSI('0', 005, 0)); // Num�rico com zeros a esquerda
//Cliente (UF)
    IL(273, 002, CSS(' ', 002, QrCabAdest_UF.Value)); // Alfab�tico A a Z
//C�digo do Munic�pio
    IL(275, 010, CSI('0', 010, QrCabAdest_cMun.Value)); //Alfanum�rico-0 a 9/A a Z
//Observa��o
    IL(285, 110, CSS(' ', 110, '')); // Alfab�tico-0 a 9/A a Z
//Zera acumulado
    IL(395, 001, ' '); // 'Z' ou ' '
//Remetente frete (CNPJ)
    IL(396, 014, CSS(' ', 014, '')); //  Preencher sem pontua��o
//Remetente frete (Insc.Est)
    IL(410, 018, CSS(' ', 018, '')); //   Alfanum�rico-0 a 9/A a Z
//Remetente frete (UF)
    IL(428, 002, CSS(' ', 002, '')); //   Alfab�tico
//Remetente frete (nome)
    IL(430, 034, CSS(' ', 034, '')); //   Alfab�tico
//Destinat. frete (CNPJ)
    IL(464, 014, CSS(' ', 14, '')); //   Preencher sem pontua��o
//Destinat. frete (Insc.Est)
    IL(478, 018, CSS(' ', 018, '')); //  Alfanum�rico-0 a 9/A a Z
//Destinat. frete (UF)
    IL(496, 002, CSS(' ', 002, '')); //   Alfab�tico
//Destinat. frete (nome)
    IL(498, 034, CSS(' ', 034, '')); //   Alfab�tico
//Tipo de frete
    //IL(532, 001, CSI(' ', 001, QrCabAModFrete.Value + 1)); // 1=CIF / 2=FOB
    IL(532, 001, ' ');
//Nota mercadoria (n�mero)
    IL(533, 006, CSS(' ', 006, '')); //  Num�rico com zeros a esquerda
//Nota mercad. (s�rie)
    IL(539, 003, CSS(' ', 003, '')); //
//Nota mercad. (tipo docto)
    IL(542, 002, '  '); // 'NF' ou 'OU'
//Nota mercad. (esp�cie)
    IL(544, 003, CSS(' ', 003, '')); //
//Nota mercad. (dt.emiss�o)
    IL(547, 006, CSS(' ', 006, '')); // Formato DDMMAA
//Reservado
    IL(553, 002, '  '); // N�o preencher
//P�gina 6 de 22
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Notas de Sa�da
//Descri��o do Campo
//Posi��es
//Inicial Tamanho
//Observa��es
//Nota mercad. (valor ctb)
    IL(555, 014, CSS(' ', 014, '')); //  ###########.##
//Condi��o de Venda
    IL(569, 001, CSI('0', 001, QrCabAide_indPag.Value)); // 0 - A vista / 1 - A prazo
//Valor do Servi�o
    IL(570, 014, CSF('0', 014, 002, 0)); //  ###########.##
//1o Vencimento
    IL(584, 006, CSS(' ', 006, '')); // Desativado ver 'Faturas'
//1a Parcela
    IL(590, 014, CSS(' ', 014, '')); // Desativado ver 'Faturas'
//2o Vencimento
    IL(604, 006, CSS(' ', 006, '')); // Desativado ver 'Faturas'
//2a Parcela
    IL(610, 014, CSS(' ', 014, '')); // Desativado ver 'Faturas'
//3o Vencimento
    IL(624, 006, CSS(' ', 006, '')); // Desativado ver 'Faturas'
//3a Parcela
    IL(630, 014, CSS(' ', 014, '')); // Desativado ver 'Faturas'
//4o Vencimento
    IL(644, 006, CSS(' ', 006, '')); // Desativado ver 'Faturas'
//4a Parcela
    IL(650, 014, CSS(' ', 014, '')); // Desativado ver 'Faturas'
//5o Vencimento
    IL(664, 006, CSS(' ', 006, '')); // Desativado ver 'Faturas'
//5a Parcela
    IL(670, 014, CSS(' ', 014, '')); // Desativado ver 'Faturas'
//6o Vencimento
    IL(684, 006, CSS(' ', 006, '')); // Desativado ver 'Faturas'
//6a Parcela
    IL(690, 014, CSS(' ', 014, '')); // Desativado ver 'Faturas'
//Cod.CTB - Sit.Especial
    IL(704, 001, ' '); // +,- ou ' '
//Al�quota ICMS - 4 decimais
    IL(705, 006, CSS(' ', 006, '')); // #.#### - S� para aliquota 2.4375%, 2.1526% e 3.1008%, usada pelas EPPs de
//S�o Paulo. Esse campo deve conter '2.4375', '2.1526' ou '3.1008'. Se a al�quota �
//diferente, esse campo deve ser deixado em branco e serem usadas as posi��es 188 a 192 para informar a al�quota.
//Descontos ECF/CF
    IL(711, 011, CSS(' ', 011, '')); // S� se Esp�cies='ECF'/'CF'
//Tipo de Frete
    IL(722, 001, ' '); // '1'=CIF, '2'=FOB - S� para Nt.Fiscal Serv. Transporte cujo modelo de documento '07'
//D�gito adicional para CFOP x99 (199/299 outros Est.)
    IL(723, 003, CSS(' ', 003, '')); // '1' ou '9' p/SP - '01' a '99' p/outros Est. Deixar em branco para outras CFOPs.
//Alinhados a esquerda com brancos a direita
//Desdobramento da Inscri��o Estadual por UF
    IL(726, 002, '  '); // Obrigat�rio para clientes com v�rias Insc.Estaduais
//Data documento
    IL(728, 006, FD6(QrCabAide_dEmi.Value)); // Formato DDMMAA
//CFOP novo com 4 d�gitos
    IL(734, 004, CSI('0', 004, QrItsIprod_CFOP.Value)); // Num�rico com zeros a esquerda
//C�digo para Sa�das Isentas
    IL(738, 003, CSS(' ', 003, '')); // Num�rico com zeros a esquerda. S� Rio Grande do Sul
//C�digo para Outras Sa�das
    IL(741, 003, CSS(' ', 003, '')); //  Num�rico com zeros a esquerda. S� Rio Grande do Sul
//- TRANSPORTADOR - (S� COMBUST�VEIS)
//Local de Saida (Origem)
//CNPJ
    IL(744, 014, CSS(' ', 014, '')); // Preencher sem pontua��o
//Inscri��o Estadual
    IL(758, 018, CSS(' ', 018, '')); //
//UF
    IL(776, 002, '  ');
//Local de Entrega/Recebto (Destino)
//CNPJ
    IL(778, 014, CSS(' ', 014, '')); // Preencher sem pontua��o
//Inscri��o Estadual
    IL(792, 018, CSS(' ', 018, '')); //
//UF
    IL(810, 002, '  ');
//Dados do Transportador
//CNPJ
    IL(812, 014, CSS(' ', 014, '')); //  Preencher sem pontua��o
//Inscri��o Estadual
    IL(826, 018, CSS(' ', 018, '')); //
//UF
    IL(844, 002, '  ');
//Modal
    IL(846, 001, '1'); // Modalidade de Transporte
//Placa 1
    IL(847, 007, CSS(' ', 007, '')); //
//UF da Placa 1
    IL(854, 002, '  ');
//Placa 2
    IL(856, 007, CSS(' ', 007, '')); //
//UF da Placa 2
    IL(863, 002, '  ');
//Placa 3
    IL(865, 007, CSS(' ', 007, '')); //
//P�gina 7 de 22
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Notas de Sa�da
//Descri��o do Campo
//Posi��es
//Inicial Tamanho
//Observa��es
//UF da Placa 3
    IL(872, 002, '  ');
//Movimenta��o f�sica do combust�vel
    IL(874, 001, ' '); // 'S'/'N'/' ' - ' ' = n�o � combust�vel
//TipoReceita NF.Comunica��o
    IL(875, 001, ' '); // ' '/'1'/'2' - S� p/ notas comunica��o/Telecomunica��o modelos 21 e 22
//Complmto N� NF.Comunica��o
    IL(876, 004, CSS(' ', 004, '')); //Pode deixar em branco. S� p/ notas comunica��o
//Classif.Contab.-Integra��o
    IL(880, 004, CSS(' ', 004, '')); // Num�rico com zeros a esquerda
//C�digo da Dipam
    IL(884, 002, CSS(' ', 002, '')); // C�digo Oficial da Dipam: 11,12,13,22,23,24,25,26,31,35,36,37 e 38
//C�digo do Munic�pio de Coleta
    IL(886, 010, CSS(' ', 010, '')); // Num�rico com zeros a esquerda
//Base de C�lculo Subst. Tribut.
    IL(896, 014, CSF('0', 014, 002, 0)); // ###########.##
//Imposto Retido Subst. Tribut.
    IL(910, 014, CSF('0', 014, 002, 0)); // ###########.##
//C�digo Recolhimento PIS/COFINS
    IL(924, 006, CSI('0', 006, 0)); // Num�rico com zeros a esquerda - Se n�o existir, deixar em branco
//Nota Cancelada
    IL(930, 001, CSI('0', 001, QrCabANotaCancelada.Value)); // 0-N�o Cancelada, 1-Cancelada
//Espa�o reservado
    IL(931, 005, CSS(' ', 005, '')); // Preencher com brancos
//Valor do Frete
    IL(936, 014, CSF('0', 014, 002, QrCabAICMSTot_vFrete.Value)); // ###########.##
//Valor do Seguro
    IL(950, 014, CSF('0', 014, 002, QrCabAICMSTot_vSeg.Value)); //###########.##
//Valor Outras Despesas
    IL(964, 014, CSF('0', 014, 002, QrCabAICMSTot_vOutro.Value)); //###########.##
//Esp�cie do documento (Windows)
    IL(978, 005, CSS(' ', 005, '')); // Alfanum�rico - esp�cie da nota ou *CT
//IPI na Base do ICMS
    IL(983, 001, '0'); // 0-N�o, 1-Sim
//S�rie
    IL(984, 003, CSI('0', 003, QrCabAide_serie.Value)); // Alfanum�rico-0 a 9/A a Z
//Subs�rie
    IL(987, 002, CSS(' ', 002, '')); // Alfanum�rico-0 a 9/A a Z
//N�mero da Nota Fiscal ( Inicial )
    IL(989, 010, CSI('0', 010, QrCabAide_nNF.Value)); // Num�rico com zeros a esquerda - 10 d�gitos
//N�mero da Nota Fiscal ( Final )
    IL(999, 010, CSI('0', 010, QrCabAide_nNF.Value)); // Num�rico com zeros a esquerda - 10 d�gitos
//N�mero do Dispositivo Inicial
    IL(1009, 010, CSI('0', 010, 0)); // Num�rico com zeros a esquerda
//N�mero do Dispositivo Final
    IL(1019, 010, CSI('0', 010, 0)); // Num�rico com zeros a esquerda
//C�digo Centro de Custo
    IL(1029, 005, CSI('0', 005, 0)); // Num�rico com zeros a esquerda 99999
//C�digo da Tabela Simples Nacional
    IL(1034, 003, CSI('0', 003, 0)); // N�merico com zeros a esquerda (999). Campo n�o obrigat�rio
//Cliente Inscri��o Estadual
    IL(1037, 018, CSS(' ', 018, QrCabAdest_IE.Value));
//Hora de Emiss�o NF
    IL(1055, 008, '00:00:00'); // Formato: HH:MM:SS
//Hora da Sa�da
    IL(1063, 008, '00:00:00'); // Formato: HH:MM:SS
//Tipo de Nota Associada
    IL(1071, 003, '   '); // Preencher conforme tabela constante nas Observa��es
//UF da Nota
    IL(1074, 002, '  '); // UF que ser� indicada no campo 'UF' da Escritura��o, caso n�o preenchida, ser� adotada a UF do campo 'Cliente(UF)'
//SPED.Munic�pio do local de coleta referente a Armazenagem/Origem
//Sigla UF
    IL(1076, 002, '  '); // Alfanum�rico
//C�digo IBGE do Munic�pio
    IL(1078, 005, CSS(' ', 005, '')); // ##### - Utilizado na gera��o do registro C115 quando o local de coleta �
//diferente do endere�o do emitente do documento ou o local de entrega � diferente
//do endere�o do destinat�rio
//SPED.Munic�pio do local de entrega referente a Armazenagem/Origem
//Sigla UF
    IL(1083, 002, '  '); //Alfanum�rico
//C�digo IBGE do Munic�pio
    IL(1085, 005, CSS(' ', 005, '')); // ##### - Utilizado na gera��o do registro C115 quando o local de coleta �
//diferente do endere�o do emitente do documento ou o local de entrega � diferente
//do endere�o do destinat�rio
//SPED.Local Coleta.CPF Contribuinte referente a Armazenagem/Origem
    IL(1090, 011, CSS(' ', 011, '')); // ###########
//SPED.Local Entrega.CPF Contribuinte referente a Armazenagem/Origem
    IL(1101, 011, CSS(' ', 011, '')); // ###########
//SPED.Combust�vel.C�digo da Autoriza��o fornecido pela SEFAZ
    IL(1112, 020, CSS(' ', 020, '')); // Alfanum�rico
//SPED.Combust�vel.N�mero do Passe Fiscal
    IL(1132, 020, CSS(' ', 020, '')); // Alfanum�rico
//SPED.Combust�vel.Temperatura
    IL(1152, 005, CSF('0', 005, 001, 0)); // ###.# Em graus Celsius
//SPED.Combust�vel.Peso bruto
    IL(1157, 014, CSF('0', 014, 002, 0)); // ###########.## Em Kg
//SPED.Combust�vel.Peso L�quido
    IL(1171, 014, CSF('0', 014, 002, 0)); // ###########.## Em Kg
//SPED.Combust�vel.Nome do Motorista
    IL(1185, 040, CSS(' ', 040, '')); // Alfanum�rico
//P�gina 8 de 22
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Notas de Sa�da
//Descri��o do Campo
//Posi��es
//Inicial Tamanho
//Observa��es
//SPED.Combust�vel.CPF do Motorista
    IL(1225, 011, CSS(' ', 011, '')); // ###########
//SPED.Energia El�trica.C�digo da classe de consumo
    IL(1236, 002, '  '); // ## Tabela SPED 4.4.5
//SPED.Energia El�trica.Consumo total em KWh
    IL(1238, 014, CSF('0', 014, 002, 0)); // ############## Em KWh
//SPED.Valor Cobrado de terceiros
    IL(1252, 014, CSF('0', 014, 002, 0)); // ###########.## Em Reais
//SPED.Comunica��o.Tipo de servi�o Prestado
    IL(1266, 002, '  ');
//0 - Telefonia
//1 - Comunica��o de dados
//2 - TV por assinatura
//3 - Provimento de acesso � Internet
//4 - Multim�dia
//9 - Outros
//SPED.Comunica��o.Data de in�cio de presta��o do servi�o
    IL(1268, 008, '00000000'); // DDMMAAAA
//SPED.Comunica��o.Data de final de presta��o do servi�o
    IL(1276, 008, '00000000'); // DDMMAAAA
//SPED.Comunica��o.Per�odo Fiscal da Presta��o do servi�o
    IL(1284, 006, '000000'); // MMAAAA
//SPED.Comunica��o.C�digo de �rea
    IL(1290, 030, CSS(' ', 030, '')); // Alfanum�rico
//SPED.Comunica��o.Identifica��o do Terminal
    IL(1320, 019, CSS(' ', 019, '')); // Alfanum�rico
//SPED.Comunica��o.C�digo da Classe do Consumo
    IL(1339, 002, '  '); // ## Tabela SPED 4.4.4
//MG.GAM57.Valor do frete
    IL(1341, 014, CSF('0', 014, 002, 0)); //###########.##
//MG.GAM57.Al�quota ICMS Frete
    IL(1355, 008, CSF('0', 008, 004, 0)); //###.####
//MG.GAM57.Valor do ICMS sobre Frete
    IL(1363, 014, CSF('0', 014, 002, 0)); //###########.##
//SPED.Chave nota Fiscal Eletr�nica
    IL(1377, 044, CSS(' ', 044, QrCabAId.Value)); // Alfanum�rico
//SPED.Munic�pio Presta��o do Servi�o Cominica��o/Energia El�trica
//Sigla UF
    IL(1421, 002, '  '); // Alfanum�rico
//C�digo IBGE do Munic�pio
    IL(1423, 005, CSI('0', 005, 0)); // Num�rico
//SPED.Tipo de t�tulo de cobran�a
    IL(1428, 002, '00'); //
//00 - Duplicata
//01 - Cheque
//02 - Promiss�ria
//03 - Recibo
//99 - Outros(descrever)
//SPED.Descri��o t�tulo de cobran�a
    IL(1430, 040, CSS(' ', 040, '')); // Alfanum�rico (somente se "Tipo de t�tuo de cobran�a" for igual a 99)
//SPED.N�mero do processo
    IL(1470, 030, CSS(' ', 030, '')); //Alfanum�rico
//SPED.Indicador Origem do processo
    IL(1500, 001, ' ');
//1 - Sefaz
//2 - Justi�a Federal
//2 - Justi�a Estadual
//4 - Secex/RFB
//9 - Outros
//P�gina 9 de 22
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Outras Observa��es
//Campo: Posi��o 0001 x 003 => Esp�cie
//Para conhecimentos de transporte, deve ser preenchido:
//- Com a esp�cie do conhecimento ('CTR', por exemplo), podendo informar os dados da Nota Fiscal a ele vinculada nos respectivos campos.
//- Com '*CT', quando se tratar de Notas Fiscais vinculadas a conhecimentos.
//Quando um conhecimento tiver mais de uma Nota Vinculada, a primeira deve ser importada junto com o pr�prio conhecimento.
//As outras s�o informadas em quantos registros forem necess�rios, tendo a esp�cie igual a '*CT' e os seguintes campos preenchidos:
//S�rie e Subs�rie do Conhecimento (posi��o 4 x 3)
//N�mero da Nota Fiscal ( Inicial ) (posi��o 7 x 6)
//Item de desdobramento (posi��o 19 x 1)
//Data da Sa�da (posi��o 20 x 6)
//N�mero da Nota Fiscal de Mercadoria (posi��o 533 x 6)
//S�rie da NF de Mercadoria (posi��o 539 x 3)
//Tipo de Documento NF de Mercadoria (posi��o 542 x 2)
//Esp�cie da NF de Mercadoria (posi��o 544 x 3)
//Data Emiss�o da NF de Mercadoria (posi��o 547 x 6)
//Valor Cont�bil da NF de Mercadoria (posi��o 555 x 14)
//Os demais campos podem ficar em branco.
//Aten��o! Quando a empresa n�o tiver opera��es de Conhecimento de Transporte, as posi��es no lay-out 396 a 568 n�o dever�o ser preenchidas, preencher com brancos.
//Campo: Posi��o 0004 x 003 => S�rie e Subs�rie
//Este campo foi desmembrado em dois campos distintos:
//Posi��o Tamanho
//984 3 S�rie
//987 2 Subs�rie
//Utilizando estes novos campos deve-se manter a Posi��o 4 x 3 em branco.
//Campo: Posi��o 0007 x 006 => N�mero da Nota Fiscal ( Inicial )
//Esta posi��o foi substitu�da pela Posi��o 989 x 10, permitindo informar n�mero de nota com 10 d�gitos
//Campo: Posi��o 0013 x 006 => N�mero da Nota Fiscal ( Final )
//Esta posi��o foi substitu�da pela Posi��o 999 x 10, permitindo informar n�mero de nota com 10 d�gitos
//Campo: Posi��o 1034 x 003 => C�digo Tabela Simples Nacional
//P�gina 10 de 22
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//ANEXO I - PARTILHA DO SIMPLES NACIONAL - COM�RCIO
//C�digo Anexo Se��o Tabela Descri��o
//001 01 01 01 Revenda Merc. Sem Subst.Tribut.
//002 01 02 01 Revenda Merc. Com Subst.Tribut. So do ICMS
//003 01 02 02 Revenda Merc. Com Subst.Tribut. De PIS, ICMS
//004 01 02 03 Revenda Merc. Com Subst.Tribut. De COFINS, ICMS
//005 01 02 04 Revenda Merc. Com Subst.Tribut. De COFINS, Pis/Pasep, ICMS
//006 01 02 05 Revenda Merc. Com Subst.Tribut. So do Pis/Pasep
//007 01 02 06 Revenda Merc. Com Subst.Tribut. So da COFINS
//008 01 02 07 Revenda Merc. Com Subst.Tribut. De Pis/Pasep, COFINS
//009 01 03 01 Revenda Merc. Para exportacao
//ANEXO II - PARTILHA DO SIMPLES NACIONAL - IND�STRIA
//C�digo Anexo Se��o Tabela Descri��o
//010 02 01 01 Venda Merc.Indust. Sem substitui��o tribut�ria
//011 02 02 01 Venda Merc.Indust. Com Subs.Trib. So do IPI 
//012 02 02 02 Venda Merc.Indust. Com Subs.Trib. De IPI, ICMS 
//013 02 02 03 Venda Merc.Indust. Com Subs.Trib. De IPI, PIS/PASEP 
//014 02 02 04 Venda Merc.Indust. Com Subs.Trib. De IPI, COFINS 
//015 02 02 05 Venda Merc.Indust. Com Subs.Trib. De IPI, PIS/PASEP, COFINS 
//016 02 02 06 Venda Merc.Indust. Com Subs.Trib. De IPI, ICMS, PIS/PASEP 
//017 02 02 07 Venda Merc.Indust. Com Subs.Trib. De IPI, ICMS, COFINS 
//018 02 02 08 Venda Merc.Indust. Com Subs.Trib. De IPI, ICMS,PIS/COFINS 
//019 02 02 09 Venda Merc.Indust. Com Subs.Trib. So do ICMS
//020 02 02 10 Venda Merc.Indust. Com Subs.Trib. De ICMS, PIS/PASEP
//021 02 02 11 Venda Merc.Indust. Com Subs.Trib. De ICMS, COFINS 
//022 02 02 12 Venda Merc.Indust. Com Subs.Trib. De ICMS, PIS/PASEP, COFINS 
//023 02 02 13 Venda Merc.Indust. Com Subs.Trib. So do PIS/PASEP 
//024 02 02 14 Venda Merc.Indust. Com Subs.Trib. De PIS/PASEP, COFINS 
//025 02 02 15 Venda Merc.Indust. Com Subs.Tribut. So da COFINS 
//026 02 03 01 Venda Merc.Indust. Para exporta��o 
//ANEXO III - PARTILHA DO SIMPLES NACIONAL - SERVI�OS E LOCA��O DE BENS M�VEIS 
//C�digo Anexo Se��o Tabela Descri��o
//027 03 01 01 Loca��o de bens m�veis 
//028 03 02 01 Serv.Inc. I a XII Sem reten��o, ISS devido a outro Munic�pio
//029 03 03 01 Serv.Inc. I a XII Sem reten��o, ISS devido ao pr�prio Munic 
//030 03 04 01 Serv.Inc. I a XII Com reten��o 
//040 03 05 01 Serv.Transp.intermunicip. e interestadual de carga sem reten��o a partir de 01/2008
//041 03 05 02 Serv.Transp.intermunicip. e interestadual de carga com reten��o a partir de 01/2008
//042 03 06 01 Servi�os Escrit�rios Cont�beis a partir de 01/2009 
//ANEXO IV - PARTILHA DO SIMPLES NACIONAL - SERVI�OS 
//C�digo Anexo Se��o Tabela Descri��o 
//031 04 01 01 Serv.Inc. XIII a XVIII Sem reten��o, ISS dev. Outro Munic�p 
//032 04 02 01 Serv.Inc. XIII a XVIII Sem reten��o, ISS dev.Pr�prio Munic�p 
//033 04 03 01 Serv.Inc. XIII a XVIII Com reten��o 
//ANEXO V - PARTILHA DO SIMPLES NACIONAL - SERVI�OS 
//C�digo Anexo Se��o Tabela Descri��o 
//034 05 01 Serv.Inc. XIX a XIVI e XXVI Sem reten��o, ISS dev outro Munic. 
//035 05 02 Serv.Inc. XIX a XIVI e XXVI Sem reten��o,ISS dev.pr�prio Munic 
//036 05 03 Serv.Inc. XIX a XIVI e XXVI Com reten��o 
//037 05 04 Escrit�rios de servi�os cont�beis at� 12/2008 
//038 05 05 Serv.Transp.intermunicip. e interestadual de carga sem reten��o at� 12/2007 
//039 05 06 Serv.Transp.intermunicip. e interestadual de carga com reten��o at� 12/2007 
//Campo: Posi��o 1071 x 003 => Tipo de Nota Associada 
//C�d UF Correspondente 
//1 Zona Franca de Manaus 
//2 Entrada de Produtor Rural 
//3 Entr.Interestadual Dif.Al�quota
//4 Sa�da de Produtor Rural 
//5 ES FUNDAP - Esp�rito Santo 
//6 Entrada de sa�da sem d�bito 
//7 Sa�da n�o entra no c�lc.CIAP 
//8 Sa�da 'Outras' n�o tributada 
//9 Transfer�ncia de D�bito 
//10 Transfer�ncia de Cr�dito 
//11 Ressarcimento 
//12 Transfer�ncia de D�bito IPI
//13 Transfer�ncia de Cr�dito IPI 
//14 Ressarcimento de IPI 
//15 Nota Entrada emitida Contribuinte 
//16 Transfer�ncia Ativo Imobilizado 
//17 Outras Entradas p/ Agregado 
//18 NF N�o Comp�e C�lculo
//P�gina 11 de 22
//
//L e i a u t e d e I mp o r t a � � o 
//Ar q u i v o T e x t o 
//19 MG Ressarcimento ST 
//20 MG Estorno de D�bito 
//21 MG Compensa��o Saldo 
//22 MG Opera��es com Caf� 
//23 MG Sa�da p/ Com�rcio 
//24 MG Sa�da p/ Ind�stria 
//25 MG Cred.Aprov.DECONCAF�
//28 ES Sa�da c/ICMS Recolhido no Ato
//29 ES Sa�da N�o Comp�e Base Enquadramento 
//30 ES Compra N�o Contribuinte 
//31 ES N�o Inclui na DOT 
//32 RS Entrada s/ Cr�d. 
//33 RS Sa�da s/ D�b. 
//34 SE Utiliza��o do Cr�dito Acumulado 
//35 Sa�da Consumidor Final 
//36 ES Devolu��o Cr�dito ICMS(ES)
//37 PR Devolu��o de Compras comp�e Receita do m�s (PR) 
//38 IPI na Base do ICMS
//39 RJ Entrada s/Reten��o ST 
//40 GO PROTEGE (GO) 
//41 GO SUCATA
//42 SC Venda do Ativo Imobilizado comp�e a Receita do m�s
//43 CE Terceiro Consignat�rio 
//44 CE Terceiro Consignante 
//45 CE Cr�dito Antecipado 
//46 CE ICMS antecipado a recolher 
//47 CE ICMS Difer.Al�quota a recolher 
//48 CE ICMS S.T.entradas a recolher 
//49 CE Prod.prim�rio/Regime especial
//50 PB ICMS-S.T.Entradas j� recolhida
//51 PB ICMS Retido na Fonte
//52 N�o Incid�ncia / Imunidade
//53 SC Presta��o de Servi�os ISS, p/ Qd01-Entradas
//54 SC 25% Transfer�cia recebidas a preco de venda varejo
//55 SC IPI Aquis.Mat.Prima/Merc.se lan�ada no Qd01-Entrad
//56 SC ICMS Ret.Sub.Tribut�ria se lan�ada no Qd01-Entrada
//57 SC Subs�dios concedidos por governos fed./estad./muni
//58 PR Sa�das com Isen��o/Imunidade/Suspens�o Imposto(PR)
//59 CE Estorno Cr�dito e D�bito
//60 PB PBSim Entradas j� recolhida
//61 PB PBSim Entradas a recolher
//62 PB PBSim Substitui��o por sa�das
//63 PB PBSim D�bitos por sa�das recolhimento Fonte
//64 RS Sa�das com Diferimento
//65 SC Sa�da - DIME Quadro 48
//67 BA BA-Icms retido sub.trib.-DMA
//Campo: Posi��o 846 x 001 => Modalidade de Transporte
//1 - Rodovi�rio
//2 - Ferrovi�rio
//3 - Rodo-Ferrovi�rio
//4 - Aquavi�rio
//5 - Dutovi�rio
//6 - A�reo
//7 - Outras
//P�gina 12 de 22
    //
    //
    RECabS.Lines.Add(FLin);
    //
    ItensDeNotaDeSaida();
    //
    QrCabA.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando exporta��o de notas de sa�da');
  MyObjects.ExportaMemoToFileExt(TMemo(RECabS), FNFS_Saida, True, False, etxtsaSemAcento, FQuebraDeLinha, Null);
  MyObjects.ExportaMemoToFileExt(TMemo(REItsS), FItens_NFs_Saida, True, False, etxtsaSemAcento, FQuebraDeLinha, Null);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  PB1.Position := 0;
end;

procedure TFmProsoft1.ItensDeNotaDeEntrada();
var
  Data: TDateTime;
begin
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Itens das Notas de Entrada
//Descri��o do Campo
//Posi��es
//Inicial Tamanho
//Observa��es
  FExp := 'Itens de nota de entrada';
  //
  QrItsI.First;
  //
  while not QrItsI.Eof do
  begin
    FLin := '';
    QrItsN.Close;
    QrItsN.Params[00].AsInteger := QrCabAFatID.Value;
    QrItsN.Params[01].AsInteger := QrCabAFatNum.Value;
    QrItsN.Params[02].AsInteger := QrCabAEmpresa.Value;
    QrItsN.Params[03].AsInteger := QrItsInItem.Value;
    UnDmkDAC_PF.AbreQuery(QrItsN, Dmod.MyDB);
    //
    QrNivel1.Close;
    QrNivel1.Params[0].AsInteger := QrItsINivel1.Value;
    UnDmkDAC_PF.AbreQuery(QrNivel1, Dmod.MyDB);
    {
    QrItsO.Close;
    QrItsO.Params[00].AsInteger := QrCabAFatID.Value;
    QrItsO.Params[01].AsInteger := QrCabAFatNum.Value;
    QrItsO.Params[02].AsInteger := QrCabAEmpresa.Value;
    UnDmkDAC_PF.AbreQuery(QrItsO, Dmod.MyDB);
    }
    //
    //
//CNPJ do emitente
    IL(001, 014, CSS('0', 014, QrCabACNPJ_CPF_emit.Value, False));
    //IL(001, 014, CSS('0', 014, QrCabACNPJ_CPF_emit.Value, False));
//Data da Entrada
    Data := QrCabAide_dSaiEnt.Value;
    if Data < 2 then Data :=  QrCabAide_dEmi.Value;
    IL(015, 008, FD8(Data)); // AAAAMMDD
//N�mero do documento
    IL(023, 006, CSS(' ', 006, Copy(FormatFloat('000000000', QrCabAide_nNF.Value), 4, 6))); // Num�rico com zeros a esquerda
//S�rie e subs�rie
    IL(029, 003, CSS(' ', 003, FormatFloat('0',QrCabAide_serie.Value))); // Alfanum�rico
//Item desdobramento da nota
    IL(032, 001, CSI('0', 001, 0)); // Parei aqui: Item de desdobramento
//N�mero do item
    IL(033, 002, CSI('0', 002, QrItsI.RecNo)); // // 01 a 99
//C�digo do produto
    IL(035, 020, CSS(' ', 020, FormatFloat('0', QrItsINivel1.Value))); // Alfanum�rico. C�digo que a empresa efetivamente usa para o produto
//Natureza da opera��o
    IL(055, 003,  '000'); // C�digo oficial da Natureza da operacao
//Item desdob.Nat.Opera��o
    IL(058, 001, '1'); // 1 a 9 - Conforme cadastrado na Tabela Cod.Nat.Opera��o S� contribuintes do IPI
//Unidade de medida
    IL(059, 003, CSS(' ', 003, Copy(QrItsIprod_uCom.Value, 1, 3))); // Alfanum�rico
//Situa��o tribut�ria
    IL(062, 003, QrItsIICMS_CST.Value); // Num�rico. Se informado, com 2 dig�tos alinhar a esquerda
//Quantidade
    IL(065, 014, CSF('0', 014, 003, QrItsIprod_qCom.Value)); // 9999999999.999
//Preco unit�rio
    IL(079, 014, CSF('0', 014, 004, QrItsIprod_vUnCom.Value)); // 999999999.9999
//Descontos
    IL(093, 014, CSF('0', 014, 002, QrItsIprod_vDesc.Value)); // 99999999999.99
//Valor do item
    IL(107, 014, CSF('0', 014, 003, QrItsIprod_vProd.Value)); // 99999999999.99
//Al�quota do IPI
    IL(121, 005, CSF('0', 005, 002, QrItsOIPI_pIPI.Value)); // 99.99
//Valor do IPI
    IL(126, 014, CSF('0', 014, 002, QrItsOIPI_vIPI.Value)); // 99999999999.99
//Base de C�lculo ICMS
    IL(140, 014, CSF('0', 014, 002, QrItsNICMS_vBC.Value)); // 99999999999.99
//ICMS S.T. - Base de C�lculo ST
    IL(154, 014, CSF('0', 014, 003, QrItsNICMS_vBCST.Value)); // 99999999999.99
//Para cadastrar produtos atrav�s da importa��o de itens, efetue o
//preenchimento de todos os campo que tiverem como descricao inicial a
//palavra PRODUTO
//PRODUTO - Descri��o do produto
    IL(168, 080, CSS(' ', 080, QrNivel1Nome.Value)); // Caracteres min�sculos s�o convertidos para mai�sculos. (Obrigat�rio)
//PRODUTO - Grupo do produto (DOS)
    IL(248, 002, '  '); // 99. Se diferente de '00' e de ' ' precisa estar cadastrado na tabela de grupos de
//produtos
//PRODUTO - Cod.Classifica��o Fiscal
    IL(250, 010, CSS(' ', 010, Geral.SoNumero_TT(QrNivel1NCM.Value))); // Sem m�scara, alinhado a esquerda
//PRODUTO - Item desdobramento do CCF
    IL(260, 002, CSS(' ', 002, '')); // Alfanum�rico
//PRODUTO - Al�quota ICMS Op.internas
    IL(262, 005, CSF('0', 005, 002, 0)); // 99.99
//PRODUTO - % Redu��o base do ICMS
    IL(267, 005, CSF('0', 005, 002, 0)); // 99.99
//PRODUTO - Base Calc.Sub.Tribut�ria
    IL(272, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//PRODUTO - Al�quota IPI
    IL(286, 005, CSF('0', 005, 002, 0)); // 99.99
//PRODUTO - Esp�cie produto IN.59/99
    IL(291, 002, CSI('0', 002, 0)); // 99
//PRODUTO - Situa��o tribut�ria
    IL(293, 003, CSI('0', 003, 0)); // Num�rico. Se informado, com 2 d�gitos alinhar a esquerda
//RESERVADO
    IL(296, 030, CSS(' ', 030, '')); // Antigo 'Nome Fantasia' - N�o Utilizado
//Num. do Item de 3 d�gitos
    IL(326, 003, CSI('0', 003, QrItsI.RecNo)); // N�o sendo informado este campo ser� lido o campo 06 - N�mero do Item (Num�rico de 001 a 900)
//Movimentou Mercadoria
    IL(329, 001, 'S'); // 'S' Sim / 'N' N�o
//Base de C�lculo do IPI
    IL(330, 014, CSF('0', 014, 002, QrItsOIPI_vBC.Value)); // 99999999999.99
//Indicador Tributa��o IPI
    IL(344, 001, 'O'); // Deve ser: 'T' = Tributado, 'I' = Isento, 'O' = Outro
//Al�quota do ICMS
    IL(345, 005, CSF('0', 005, 002, QrItsNICMS_pICMS.Value)); //99.99
//Valor do ICMS
    IL(350, 014, CSF('0', 014, 002, QrItsNICMS_vICMS.Value)); // 99999999999.99
//Indicador Tributa��o ICMS
    IL(364, 001, 'O'); //  Deve ser: 'T'=Tributado, 'I'= Isento, 'O'= Outro
//ICMS S.T. - ICMS ST
    IL(365, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//- COMBUST�VEL -
//Base Calc.ST.Interestadual
    IL(379, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//ICMS ST.Interestadual
    IL(393, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//ICMS ST.Inter. A Completar
    IL(407, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//Base C�lculo de Reten��o
    IL(421, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//P�gina 13 de 22
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Itens das Notas de Entrada
//Descri��o do Campo
//Posi��es
//Inicial Tamanho
//Observa��es
//Parcela do Imp. Retido
    IL(435, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//PRODUTO - Grupo do Produto(3 d�gitos) (DOS)
    IL(449, 003, '000'); // 999
//PRODUTO - PRODEPE C�digo Apura��o
    IL(452, 002, '00'); // 99 - Espec�fico PE
//PRODUTO - PRODEPE.Indic.Incentivo.Entr.
    IL(454, 002, '00'); // 99 - Espec�fico PE
//Reserva P/Dados do Produto
    IL(456, 096, CSS(' ', 096, ''));
//N�mero da DI
    IL(552, 010, CSS(' ', 010, '')); // 9999999999 - Brancos a esquerda
//Data da DI
    IL(562, 008, CSS('0', 008, '')); // DDMMAAAA - Espec�fico RJ
//Cod.Inf.Reg.88 GT
    IL(570, 001, ' '); // ' '/A/B/C - Espec�fico RJ
//ICMS Diferido/Dilatado
    IL(571, 014, CSF('0', 014, 002, 0)); // 99999999999.99 - Espec�fico RJ
//PRODEPE. C�digo Apura��o
    IL(585, 002, '00'); // 99 - Espec�fico PE
//Gera Registro 88
    IL(587, 001, FormatFloat('0', RGGeraRegistro88.ItemIndex)); // 1 0-N�o / 1-Sim
//Isentas/N�o tribut. ICMS
    IL(588, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//Outras ICMS
    IL(602, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//Isentas/N�o tribut. IPI
    IL(616, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//Outras IPI
    IL(630, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//PRODUTO - Grupo do Produto
    IL(644, 004, CSS(' ', 004, '')); //  #### Se diferente de '0000' e de ' ' precisa estar cadastrado na tabela de grupos de produtos.
//S�rie
    IL(648, 003, CSS(' ', 003, '')); // Alfanum�rico
//Subs�rie
    IL(651, 002, CSS(' ', 002, '')); // Alfanum�rico
//N�mero do documento com 10 d�gitos
    IL(653, 010, CSI('0', 010, QrCabAide_nNF.Value)); //  Num�rico com zeros a esquerda
//Valor Despesas Acessorias
    IL(663, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//Os campos abaixo somente ser�o obrigat�rios se a nota fiscal de entrada foi
//gravada com um endere�o diferente do endere�o principal.
//Inscri��o Estadual Emitente
    IL(677, 018, CSS(' ', 018, ''));
//UF Emitente
    IL(695, 002, CSS(' ', 002, ''));
//C�digo Situa��o Tribut�ria IPI
    IL(697, 002, CSS(' ', 002, ''(*QrItsOIPI_CST.Value*))); // ## Tabela SPED 4.3.2
//C�digo Situa��o Tribut�ria PIS
    IL(699, 002, CSS(' ', 002, ''(*QrItsQPIS_CST.Value*))); // ## Tabela SPED 4.3.3
//C�digo Situa��o Tribut�ria COFINS
    IL(701, 002, CSS(' ', 002, ''(*QrItsSCOFINS_CST.Value*))); // ## Tabela SPED 4.3.4
//Medicamento - N�mero Lote Fabrica��o
    IL(703, 021, CSS(' ', 021, '')); // Alfanum�rico
//Medicamento - Quantidade Item p/ Lote
    IL(724, 016, CSS(' ', 016, '')); // ################
//Medicamento - Data de Fabrica��o
    IL(740, 008, CSS(' ', 008, '')); // DDMMAAAA
//Medicamento - Data de Validade
    IL(748, 008, CSS(' ', 008, '')); // DDMMAAAA
//Medicamento - Pre�o Tabelado ou Valor do Pre�o M�ximo
    IL(756, 014, CSF('0', 014, 002, 0)); // ###########.##
//Ve�culo.Indicador Tipo de Opera��o
    IL(770, 001, ' ');
//0 - Venda para Concession�ria
//1 - Faturamento Direto
//2 - Venda Direta
//3 - Venda da Concession�ria
//9 - Outros
//Dados do documento de entrada a que se refere o ressarcimento do ICMS Substitui��o tribut�ria
//Data Escritura��o
    IL(771, 008, CSI('0', 008, 0)); // DDMMAAAA
//N�mero da Nota
    IL(779, 010, CSI('0', 010, 0)); //  ##########
//S�rie
    IL(789, 006, CSS(' ', 006, '')); // '' Alfanum�rico
//Subserie
    IL(795, 006, CSS(' ', 006, '')); // Alfanum�rico
//N�mero do Item de desdobramento da nota
    IL(801, 003, CSS(' ', 003, '')); // ###
//CNPJ do fornecedor
    IL(804, 014, CSI('0', 014, 0)); // ##############
//Inscri��o Estadual do fornecedor
    IL(818, 018, CSS(' ', 018, '')); // Alfanum�rico
//Ressarcimento Subst. Tribut�ria Ano da NF �ltima Entrada
    IL(836, 004, CSI('0', 004, 0)); // AAAA
//Ressarcimento Subst. Tribut�ria Quantidade do item relativa �ltima entrada
    IL(840, 015, CSF('0', 015, 002, 0)); // ############.##
//Ressarcimento Subst. Tribut�ria Valor unit�rio da mercadoria da NF relativa a �ltima entrada
    IL(855, 014, CSF('0', 014, 002, 0)); // ###########.##
//P�gina 14 de 22
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Itens das Notas de Entrada
//Descri��o do Campo
//Posi��es
//Inicial Tamanho
//Observa��es
//Ressarcimento Subst. Tribut�ria Valor unit�rio da Base de c�lculo do imposto
    IL(869, 014, CSF('0', 014, 002, 0)); // ############.##
//Valor da redu��o da Base de C�lculo
    IL(883, 014, CSF('0', 014, 002, 0)); // ############.##
//Campo cujos nomes comecem com PRODUTO servem para cadastrar
//produtos que ainda n�o existem no cadastro de produtos
//PRODUTO - SPED.Tipo do Item
    IL(897, 002, '  ');
//00 - Mercadoria para Revenda
//01 - Mat�ria-Prima
//02 - Embalagem
//03 - Produto em Processo
//04 - Produto Acabado
//05 - Subproduto
//06 - Produto Intermedi�rio
//07 - Material de Uso e Consumo
//08 - Ativo Imobilizado
//09 - Servi�os
//10 - Outros insumos
//99 - Outras
//PRODUTO - C�digo do Selo Controle do IPI
    IL(899, 006, CSS(' ', 006, '')); // Alfanum�rico Tabela SPED 4.5.2
//PRODUTO - Classe Enquadramento IPI
    IL(905, 005, CSS(' ', 005, '')); // Alfanum�rico Tabela SPED 4.5.1
//PRODUTO - Medicamento Refer�ncia Base C�lculo
    IL(910, 001, ' ');
//0 - Pre�o tabelado ou pre�o m�ximo sugerido
//1 - Margem de valor agregado
//2 - Lista Negativa
//3 - Lista Positiva
//4 - Lista Neutra
//PRODUTO - Medicamento.Tipo Produto
    IL(911, 001, ' ');
//0 - Similar
//1 - Gen�rico
//2 - �tico ou de marca
//PRODUTO - Medicamento.Pre�o tabelado ou pre�os m�ximo
    IL(912, 014, CSF('0', 014, 002, 0)); // ############.## Em Reais
//PRODUTO - Cominica��o.C�digo de Classifica��o do Item
    IL(926, 004, CSS(' ', 004, '')); // #### Tabela SPED 4.4.1
//PRODUTO - Comunica��o.Tipo de Receita
    IL(930, 001, ' ');
//0 - Pr�pria - Servi�os Prestados
//1 - Pr�pria - Cobran�a de D�bitos
//2 - Pr�pria - Vendas de Mercadorias
//3 - Pr�pria - Vendas de Servi�o pr�-pago
//4 - Outras receitas pr�prias
//5 - Receitas de terceiros
//9 - Outras receitas de terceiros
//PRODUTO - Energia El�trica.C�digo de Classifica��o do Item
    IL(931, 004, CSS(' ', 004, '')); // #### Tabela SPED 4.4.1
//PRODUTO - Energia El�trica.Tipo de Receita
    IL(935, 001, ' ');
//0 - Receita pr�pria
//1 - Receita de terceiros
//PRODUTO - Ve�culo.N�mero do Chassi
    IL(936, 017, CSS(' ', 017, '')); // Alfanum�rico
//PRODUTO - IPI.Valor por Unidade Padr�o de Tributa��o
    IL(953, 014, CSF('0', 014, 002, 0)); // ############.## Em Reais
//C�digo Enquadramento Legal do IPI
    IL(967, 003, CSS(' ', 003, QrItsOIPI_cEnq.Value)); // Alfanum�rico Tabela SPED 4.5.3
//C�digo de movimento
    IL(970, 001, ' ');
//1-No pr�prio estabelecimento;
//2-Fora do Estabelecimento;
//3-Diversas;
//Tipo de Produto
    IL(971, 001, '1');
//0 - Outros;
//1 - Insumos;
//2 - Envasados;
//3 - Combust�vel/Solvente;
//4 - Hortifrut�culas;
//5 - Gado e Subprodutos;
//6 - Sisal;
//7 - Agropecu�rios;
//PRODUTO - C�d. Produtos Relevantes
    IL(972, 007, CSS(' ', 007, ''));
//PRODUTO - C�d. Sefaz (Registro 88 - SP)
    IL(979, 004, CSS(' ', 004, ''));
//Preco unit�rio Com 6 Casas Decimais
    IL(983, 016, CSF('0', 016, 006, 0)); // #999999999.999999
//PRODUTO - Tipo Produto/Servi�o (DIEF - CE)
    IL(999, 001, ' '); // Espec�fico CEAR�
//1 - Mercadoria;
//2 - Servi�o com incid�ncia de ICMS;
//3 - Servi�o sem incid�ncia de ICMS;
//PRODUTO - ANP - C�digo de Produto ( Tabela 12 SIMP )
    IL(1000, 010, CSS(' ', 010, ''));  // Espec�fico para Tipo de Produto '3' - Combust�vel/Solvente.
//O c�digo deve estar previamente cadastrado na rotina Tabelas Fiscais para
//Arquivos Magn�ticos
//ICMS S.T. - Al�quota ST
    IL(1010, 005, CSF('0', 005, 002, 0)); // #99.99
//P�gina 15 de 22
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Itens das Notas de Entrada
//Descri��o do Campo
//Posi��es
//Inicial Tamanho
//Observa��es
//PRODUTO - Classe Recolhimento PIS/COFINS
    IL(1015, 006, CSS(' ', 006, '')); // 999999
//P�gina 16 de 22
//
    REItsE.Lines.Add(FLin);
    QrItsI.Next;
  end;
end;

procedure TFmProsoft1.ItensDeNotaDeSaida();
var
  Data: TDateTime;
begin

//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Itens das Notas de Sa�da
//Descri��o do Campo
//Posi��es
//Inicial Tamanho
//Observa��es
//Filler
  FExp := 'Itens de nota de saida';
  QrItsI.Close;
  QrItsI.Params[00].AsInteger := QrCabAFatID.Value;
  QrItsI.Params[01].AsInteger := QrCabAFatNum.Value;
  QrItsI.Params[02].AsInteger := QrCabAEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrItsI, Dmod.MyDB);
  //
  while not QrItsI.Eof do
  begin
    FLin := '';
    QrItsN.Close;
    QrItsN.Params[00].AsInteger := QrCabAFatID.Value;
    QrItsN.Params[01].AsInteger := QrCabAFatNum.Value;
    QrItsN.Params[02].AsInteger := QrCabAEmpresa.Value;
    QrItsN.Params[03].AsInteger := QrItsInItem.Value;
    UnDmkDAC_PF.AbreQuery(QrItsN, Dmod.MyDB);
    //
    {
    QrGraGruX.Close;
    QrGraGruX.Params[0].AsInteger := Geral.IMV(Geral.SoNumero_TT(QrItsIprod_cProd.Value));
    UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
    }
    {
    QrItsO.Close;
    QrItsO.Params[00].AsInteger := QrCabAFatID.Value;
    QrItsO.Params[01].AsInteger := QrCabAFatNum.Value;
    QrItsO.Params[02].AsInteger := QrCabAEmpresa.Value;
    UnDmkDAC_PF.AbreQuery(QrItsO, Dmod.MyDB);
    }
    //
    //
    IL(001, 014, CSS(' ', 014, '')); // Deixar em branco
//Data da Sa�da
    Data := QrCabAide_dSaiEnt.Value;
    if Data < 2 then Data :=  QrCabAide_dEmi.Value;
    IL(015, 008, FD8(Data)); // AAAAMMDD
//N�mero do documento
    IL(023, 006, CSS(' ', 006, Copy(FormatFloat('000000000', QrCabAide_nNF.Value), 4, 6))); // Num�rico com zeros a esquerda
//S�rie e subs�rie
    IL(029, 003, CSS(' ', 003, FormatFloat('0',QrCabAide_serie.Value))); // Alfanum�rico
//Item desdobramento da nota
    IL(032, 001, ' '); // 0 a 9/A a Z
//N�mero do item
    IL(033, 002, '  '); // 01 a 99
//C�digo do produto
    IL(035, 020, CSS(' ', 020, QrItsIprod_cProd.Value)); // Alfanum�rico. C�digo que a empresa efetivamente usa para o produto
//Natureza da opera��o
    IL(055, 003,  '000'); // C�digo oficial da Natureza da operacao
//Item desdob.Nat.Opera��o
    IL(058, 001, '0'); // 1 a 9 - Conforme cadastrado na Tabela Cod.Nat.Opera��o S� contribuintes do IPI
//Unidade de medida
    IL(059, 003, CSS(' ', 003, Copy(QrItsIprod_uCom.Value, 1, 3))); // Alfanum�rico
//Situa��o tribut�ria
    IL(062, 003, QrItsIICMS_CST.Value); // Num�rico. Se informado, com 2 dig�tos alinhar a esquerda
//Quantidade
    IL(065, 014, CSF('0', 014, 003, QrItsIprod_qCom.Value)); // 9999999999.999
//Preco unit�rio
    IL(079, 014, CSF('0', 014, 004, QrItsIprod_vUnCom.Value)); // 999999999.9999
//Descontos
    IL(093, 014, CSF('0', 014, 002, QrItsIprod_vDesc.Value)); // 99999999999.99
//Valor do item
    IL(107, 014, CSF('0', 014, 003, QrItsIprod_vProd.Value)); // 99999999999.99
//Al�quota do IPI
    IL(121, 005, CSF('0', 005, 002, QrItsOIPI_pIPI.Value)); // 99.99
//Valor do IPI
    IL(126, 014, CSF('0', 014, 002, QrItsOIPI_vIPI.Value)); // 99999999999.99
//Base de C�lculo ICMS
    IL(140, 014, CSF('0', 014, 002, QrItsNICMS_vBC.Value)); // 99999999999.99
//ICMS S.T. - Base de C�lculo ST
    IL(154, 014, CSF('0', 014, 003, QrItsNICMS_vBCST.Value)); // 99999999999.99
//Para cadastrar produtos atrav�s da importa��o de itens, efetue o
//preenchimento de todos os campo que tiverem como descricao inicial a
//palavra PRODUTO.
//PRODUTO - Descri��o do produto
    IL(168, 080, CSS(' ', 080, QrItsIprod_xProd.Value)); // Caracteres min�sculos s�o convertidos para mai�sculos. (Obrigat�rio)
//PRODUTO - Grupo do produto (DOS)
    IL(248, 002, '  '); // 99. Se diferente de '00' e de ' ' precisa estar cadastrado na tabela de grupos de
//produtos
//PRODUTO - Cod.Classifica��o Fiscal
    IL(250, 010, CSS(' ', 010, QrItsIprod_NCM.Value)); // Sem m�scara, alinhado a esquerda
//PRODUTO - Item desdobramento do CCF
    IL(260, 002, CSS(' ', 002, '')); // Alfanum�rico
//PRODUTO - Al�quota ICMS Op.internas
    IL(262, 005, CSF('0', 005, 002, 0)); // 99.99
//PRODUTO - % Redu��o base do ICMS
    IL(267, 005, CSF('0', 005, 002, 0)); // 99.99
//PRODUTO - Base Calc.Sub.Tribut�ria
    IL(272, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//PRODUTO - Al�quota IPI
    IL(286, 005, CSF('0', 005, 002, 0)); // 99.99
//PRODUTO - Esp�cie produto IN.59/99
    IL(291, 002, CSI('0', 002, 0)); // 99
//PRODUTO - Situa��o tribut�ria
    IL(293, 003, CSI('0', 003, 0)); // Num�rico. Se informado, com 2 d�gitos alinhar a esquerda
//RESERVADO
    IL(296, 030, CSS(' ', 030, '')); // Antigo 'Nome Fantasia' - N�o Utilizado
//Num. do Item de 3 d�gitos
    IL(326, 003, CSI('0', 003, QrItsI.RecNo)); // N�o sendo informado este campo ser� lido o campo 06 - N�mero do Item (Num�rico de 001 a 900)
//Movimentou Mercadoria
    IL(329, 001, 'S'); // 'S' Sim / 'N' N�o
//Base de C�lculo do IPI
    IL(330, 014, CSF('0', 014, 002, QrItsOIPI_vBC.Value)); // 99999999999.99
//Indicador Tributa��o IPI
    IL(344, 001, 'O'); // Deve ser: 'T' = Tributado, 'I' = Isento, 'O' = Outro
//Al�quota do ICMS
    IL(345, 005, CSF('0', 005, 002, QrItsNICMS_pICMS.Value)); //99.99
//Valor do ICMS
    IL(350, 014, CSF('0', 014, 002, QrItsNICMS_vICMS.Value)); // 99999999999.99
//Indicador Tributa��o ICMS
    IL(364, 001, 'O'); //  Deve ser: 'T'=Tributado, 'I'= Isento, 'O'= Outro
//ICMS S.T. - ICMS ST
    IL(365, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//- COMBUST�VEL -
//Base Calc.ST.Interestadual
    IL(379, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//ICMS ST.Interestadual
    IL(393, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//ICMS ST.Inter. A Completar
    IL(407, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//Base C�lculo de Reten��o
    IL(421, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//P�gina 17 de 22
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Itens das Notas de Sa�da
//Descri��o do Campo 
//Posi��es 
//Inicial Tamanho 
//Observa��es 
//Parcela do Imp. Retido
    IL(435, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//PRODUTO - Grupo do Produto(3 d�gitos) (DOS)
    IL(449, 003, '000'); // 999
//PRODUTO - PRODEPE C�digo Apura��o
    IL(452, 002, '00'); // 99 - Espec�fico PE
//PRODUTO - PRODEPE.Indic.Incentivo.Entr.
    IL(454, 002, '00'); // 99 - Espec�fico PE
//Reserva P/Dados do Produto
    IL(456, 056, CSS(' ', 056, ''));
//CNPJ Operadora destino
    IL(512, 014, CSS(' ', 014, '')); // S� para nota comunica��o/telecomunica��o modelos 21 e 22
//C�digo N�mero Terminal
    IL(526, 010, CSS(' ', 010, '')); // S� para nota comunica��o/telecomunica��o modelos 21 e 22
//PRODEPE. C�digo Apura��o
    IL(536, 002, '  '); // 99 - Espec�fico PE
//Gera Registro 88
    IL(538, 001, FormatFloat('0', RGGeraRegistro88.ItemIndex)); // 0-N�o / 1-Sim
//Isentas/N�o tribut. ICMS
    IL(539, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//Outras ICMS
    IL(553, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//Isentas/N�o tribut. IPI
    IL(567, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//Outras IPI
    IL(581, 014, CSF('0', 014, 002, 0)); // 99999999999.99
//PRODUTO - Grupo do Produto
    IL(595, 004, CSS(' ', 004, '')); // #### Se diferente de '0000' e de ' ' precisa estar cadastrado na tabela de grupos de produtos.
//S�rie
    IL(599, 003, CSS(' ', 003, FormatFloat('0', QrCabAide_serie.Value))); // Alfanum�rico
//Subs�rie
    IL(602, 002, '  '); // Alfanum�rico
//N�mero do documento com 10 d�gitos
    IL(604, 010, CSI('0', 010, QrCabAide_nNF.Value)); // Num�rico com zeros a esquerda
//Valor Despesas Acessorias
    IL(614, 014, CSF('0', 014, 002, 0)); //99999999999.99
//C�digo Situa��o Tribut�ria IPI
{
//Medicamento - Pre�o Tabelado ou Valor do Pre�o M�ximo
    IL(756, 014, CSF('0', 014, 002, 0)); // ###########.##
//Ve�culo.Indicador Tipo de Opera��o
    IL(770, 001, ' ');
}
    IL(628, 002, CSI('0', 002, QrItsOIPI_CST.Value)); // ## Tabela SPED 4.3.2
//C�digo Situa��o Tribut�ria PIS
    IL(630, 002, CSI('0', 002, QrItsQPIS_CST.Value)); // ## Tabela SPED 4.3.3
//C�digo Situa��o Tribut�ria COFINS
    IL(632, 002, CSI('0', 002, QrItsSCOFINS_CST.Value)); // ## Tabela SPED 4.3.4
//Medicamento.N�mero Lote Fabrica��o
    IL(634, 021, CSS(' ', 021, '')); // Alfanum�rico
//Medicamento.Quantidade Item p/ Lote
    IL(655, 016, CSS(' ', 016, '')); // ################
//Medicamento.Data de Fabrica��o
    IL(671, 008, CSI('0', 008, 0)); // DDMMAAAA
//Medicamento.Data de Validade
    IL(679, 008, CSI('0', 008, 0)); // DDMMAAAA
//Medicamento.Pre�o Tabelado ou Valor do pre�o m�ximo
    IL(687, 014, CSF('0', 014, 002, 0)); // ###########.##
//Ve�culo.Indicador Tipo de Opera��o
    IL(701, 001, ' ');
//0 - Venda para Concession�ria
//1 - Faturamento direto
//2 - Venda direta
//3 - Venda da concession�ria
//9 - Outros
//IPI.Quantidade de Selo Controle aplicada
    IL(702, 012, CSI('0', 012, 0)); //############
//IPI.Valor por Unidade de padr�o de Tributa��o
    IL(714, 014, CSF('0', 014, 002, 0)); // ###########.##
//IPI.Quantidade na Unidade padr�o de Tributa��o
    IL(728, 015, CSF('0', 015, 003, 0)); // ###########.###
//Dados do documento de sa�da a que se refere o ressarcimento do ICMS
//Substitui��o Tribut�ria
//Data da escritura��o
    IL(743, 008, CSS(' ', 008, '')); // DDMMAAAA
//N�mero da Nota
    IL(751, 010, CSS('0', 010, '')); // ##########
//S�rie
    IL(761, 006, CSS(' ', 006, '')); // Alfanum�rico
//Subserie
    IL(767, 006, CSS(' ', 006, '')); // Alfanum�rico
//N�mero do Item de desdobramento da nota
    IL(773, 003, '000'); // ###
//CNPJ do fornecedor
    IL(776, 014, CSS(' ', 014, '')); // ##############
//Inscri��o Estadual do fornecedor
    IL(790, 018, CSS(' ', 018, '')); // Alfanum�rico
//Ressarcimento Subst. Tribut�ria Ano da NF �ltima Entrada
    IL(808, 004, CSS(' ', 004, '')); // AAAA
//Ressarcimento Subst. Tribut�ria Quantidade do item relativa �ltima entrada
    IL(812, 015, CSF('0', 015, 003, 0)); // ############.###
//Ressarcimento Subst. Tribut�ria Valor unit�rio da mercadoria da NF
//relativa a �ltima entrada
    IL(827, 014, CSF('0', 014, 002, 0)); // ############.##
//Ressarcimento Subst. Tribut�ria Valor unit�rio da Base de C�lculo do
//imposto
    IL(841, 014, CSF('0', 014, 002, 0)); // ############.##
//Valor da redu��o da Base de C�lculo
    IL(855, 014, CSF('0', 014, 002, 0)); // ############.##
//P�gina 18 de 22
//
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Itens das Notas de Sa�da
//Descri��o do Campo
//Posi��es
//Inicial Tamanho
//Observa��es
//Campos cujos nomes comecem com PRODUTO servem para cadastrar
//produtos que ainda n�o existem no cadastro de produtos
//PRODUTO - SPED.Tipo do Item
    IL(869, 002, '99'); // Parei aqui
//00 - Mercadoria para Revenda
//01 - Mat�ria-Prima
//02 - Embalagem
//03 - Produto em Processo
//04 - Produto Acabado
//05 - Subproduto
//06 - Produto Intermedi�rio
//07 - Material de Uso e Consumo
//08 - Ativo Imobilizado
//09 - Servi�os
//10 - Outros insumos
//99 - Outras
//C�digo do Selo de Controle IPI
    IL(871, 006, CSS(' ', 006, '')); // Alfanum�rico Tabela SPED 4.5.2
//Classe Enquadramento do IPI
    IL(877, 005, CSS(' ', 005, '')); // Alfanum�rico Tabela SPED 4.5.1
//Medicamento Refer�ncia Base C�lculo
    IL(882, 001, ' ');
//0 - Pre�o tabelado ou pre�o m�ximo sugerido
//1 - Margem de valr agragado
//2 - Lista Negativa
//3 - Lista Positiva
//4 - Lista Neutra
//Medicamentos.Tipo de Produto
    IL(883, 001, ' ');
//0 - Similar
//1 - Gen�rico
//2 -�tico ou de marca
//Medicamento.Pre�o Tabelado ou pre�o M�ximo
    IL(884, 014, CSF('0', 014, 002, 0)); // ###########.## Em Reais
//Comunica��o.C�digo de Classifica��o do Item
    IL(898, 004, CSS(' ', 004, '')); // #### Tabela SPED 4.4.1
//PRODUTO - Comunica��o.Tipo de Receita
    IL(902, 001, ' ');
//0 - Pr�pria - Servi�os Prestados
//1 - Pr�pria - Cobran�a de D�bitos
//2 - Pr�pria - Vendas de Mercadorias
//3 - Pr�pria - Vendas de Servi�o pr�-pago
//4 - Outras receitas pr�prias
//5 - Receitas de terceiros (co-faturamento)
//9 - Outras receitas de terceiros
//Energia El�trica.C�digo de Classifica��o do Item
    IL(903, 004, CSS(' ', 004, '')); // #### Tabela SPED 4.4.1
//Energia El�trica.Tipo de Receita
    IL(907, 001, ' ');
//0 - Receita Pr�pria
//1 - Receita de Terceiros
//Ve�culo.N�mero do Chassi
    IL(908, 017, CSS(' ', 017, '')); //Alfanum�rico
//IPI.Valor por unidade Padr�o de Tributa��o
    IL(925, 014, CSF('0', 014, 002, 0)); // ###########.## Em Reais
//C�digo Enquadramento Legal do IPI
    IL(939, 003, CSS(' ', 003, '')); // Alfanum�rico Tabela 4.5.3
//C�digo de movimento
    IL(942, 001, '3'); // Parei aqui
//1-No pr�prio estabelecimento;
//2-Fora do Estabelecimento;
//3-Diversas;
//Tipo de Produto
    IL(943, 001, '0');//
//0 - Outros;
//1 - Insumos;
//2 - Envasados;
//3 - Combust�vel/Solvente;
//4 - Hortifrut�culas;
//5 - Gado e Subprodutos;
//6 - Sisal;
//7 - Agropecu�rios;
//PRODUTO - C�d. Produtos Relevantes
    IL(944, 007, CSS(' ', 007, '')); //
//PRODUTO - C�d. Sefaz (Registro 88 - SP)
    IL(951, 004, CSS(' ', 004, '')); //
//Preco unit�rio Com 6 Casas Decimais
    IL(955, 016, CSF('0', 016, 006, 0)); // 999999999.999999
//PRODUTO - Tipo Produto/Servi�o (DIEF - CE)
    IL(971, 001, ' '); // Espec�fico CEAR�
//1 - Mercadoria;
//2 - Servi�o com incid�ncia de ICMS;
//3 - Servi�o sem incid�ncia de ICMS;
//PRODUTO - ANP - C�digo de Produto ( Tabela 12 SIMP )
    IL(972, 010, CSS(' ', 010, '')); // Espec�fico para Tipo de Produto '3' - Combust�vel/Solvente.
//O c�digo deve estar previamente cadastrado na rotina Tabelas Fiscais para
//Arquivos Magn�ticos
//ICMS S.T. - Al�quota ST
    IL(982, 005, CSF('0', 005, 002, 0)); // 99.99
//PRODUTO - Classe Recolhimento PIS/COFINS
    IL(987, 006, CSS(' ', 006, '')); // 999999
//P�gina 19 de 22
//
    REItsS.Lines.Add(FLin);
    QrItsI.Next;
  end;
end;

function TFmProsoft1.MPSemNF(): Boolean;
begin
  QrSemNF_MP.Close;
  QrSemNF_MP.Params[00].AsInteger := VAR_FATID_0013;
  QrSemNF_MP.Params[01].AsInteger := VAR_FATID_0113;
  QrSemNF_MP.Params[02].AsInteger := VAR_FATID_0213;
  QrSemNF_MP.Params[03].AsInteger := FEmpresa;
  QrSemNF_MP.Params[04].AsString  := FDataIni;
  QrSemNF_MP.Params[05].AsString  := FDataFim;
  UnDmkDAC_PF.AbreQuery(QrSemNF_MP, Dmod.MyDB);
  //
  Result := QrSemNF_MP.RecordCount > 0;
  if Result then
  begin
    case Geral.MB_Pergunta('Existem ' + FormatFloat('0', QrSemNF_MP.RecordCount) +
    ' entradas de mat�ria-prima (couro) sem NF ou sem CNPJ / CPF no cadastro do'+
    sLineBreak+'procedente/emissor da NF. Deseja visualizar o relat�rio?') of
      ID_YES: MyObjects.frxMostra(frxMPSemNF, 'Entradas de MP sem NF');
      ID_NO : Result := False;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmProsoft1.Inventario();
begin
//L e i a u t e d e I mp o r t a � � o
//Ar q u i v o T e x t o
//Modelo: Invent�rio
//Descri��o do Campo
//Posi��es
//Inicial Tamanho
//Observa��es
//Data do Invent�rio 1 8 Formato: DDMMAAAA
//M�s/Ano inicial de refer�ncia 9 4 Formato: MMAA
//M�s/Ano final de refer�ncia 13 4 Formato: MMAA
//C�digo do Produto da Empresa 17 20 Alfanum�rico
//Situa��o do produto 37 1 1=Da empresa em seu poder
//2=Da empresa com Terceiros
//3=De Terceiros c/ a Empresa
//4=Estoque pr�prio em Tr�nsito
//5=Estoque pr�prio inaproveit�vel
//CNPJ do Terceiro 38 14 Se campo acima = 1, 4 e 5 move zeros, sen�o move CNPJ (sem m�scara, s�
//n�meros)
//Inscri��o Estadual do Terceiro 52 20 Se CNPJ for preenchido este tamb�m deve ser
//UF do Terceiro 72 2 Se Insc. Est. estiver preenchida este tamb�m deve ser
//Quantidade 74 21 15 inteiros e 6 decimais
//Valor Unit�rio 95 17 13 inteiros e 4 decimais
//Valor Total 112 17 15 inteiros e 2 decimais
//ICMS a Recuperar 129 17 15 inteiros e 2 decimais
//Observa��o 146 60 Alfanum�rico
//Descri��o do Produto 206 80 Alfanum�rico
//Grupo de Produto 286 4 Num�rico
//Classifica��o Fiscal (NCM) 290 10 Num�rico - sem m�scara
//RESERVADO 300 30 Antigo 'Nome Fantasia' - N�o Utilizado
//Unidade de Medida 330 3 Alfanum�rico
//Descri��o Grupo Produto 333 30 AlfaNum�rico
//P�gina 20 de 22
end;

procedure TFmProsoft1.Produtos();
var
  CodProd, CTB: String;
  //TipoItem: String;
  MyCursor: TCursor;
  ItensSemCTB: Integer;
begin
  ItensSemCTB := 0;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exportando cadastro de produtos');
    REProdutos.Lines.Clear;
    QrProd.Close;
    UnDmkDAC_PF.AbreQuery(QrProd, Dmod.MyDB);
    PB1.Max := QrProd.RecordCount;
    QrProd.First;
    while not QrProd.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exportando cadastro de produtos - registro ' + IntToStr(QrProd.RecNo));
      Application.ProcessMessages;
      //
      FLin := '';
  //
  //L e i a u t e d e I mp o r t a � � o
  //Ar q u i v o T e x t o
  //Modelo: Produtos
  //Descri��o do Campo
  //Posi��es
  //Inicial Tamanho
  //Observa��es
  //C�digo do produto
  {
      if QrProdPrdGrupTip.Value < 0 then
        CodProd := FormatFloat('0', QrProdGraGru1.Value)
      else
  }
      CodProd := FormatFloat('0', QrProdGraGruX.Value);
      IL(001, 020, CSS(' ', 020, CodProd)); // Codigo do produto utilizado efetivamente pela empresa.
  //Descri��o do produto
      IL(021, 080, CSS(' ', 080, QrProdNome.Value));// Caracteres minusculos sao convertidos para maiusculos. Obrigatorio
  //Grupo do produto (DOS)
      IL(101, 002, '  '); // ## - Se diferente de '00' e de precisa estar cadastrado na tabela de grupos de produtos.
  //Unidade de medida
      IL(103, 003, CSS(' ', 003, Copy(QrProdSigla.Value, 1, 3))); // Precisa estar cadastrada na Tabela de Unidades de Medida.
  //C�d.Classifica��o Fiscal
      IL(106, 010, CSS(' ', 010, Geral.SoNumero_TT(QrProdNCM.Value))); // Sem mascara. Se e' o codigo NCM de 8 digitos deve estar alinhado a esquerda.
  //Item desdobramento do CCF
      IL(116, 002, '  '); // Alfanumerico. Somente se e' usado na Tabela de Codigos de Classificacao Fiscal.
  //Deixar em branco se nao e' utilizado.
  //Al�quota ICMS Op.internas
      IL(118, 005, CSF('0', 005, 002, 0)); // ##.##
  //% Redu��o base do ICMS
      IL(123, 005, CSF('0', 005, 002, 0)); // ##.##
  //Base Calc.Sub.Tributaria
      IL(128, 014, CSF('0', 014, 002, 0)); // ###########.##
  //Al�quota IPI
      IL(142, 005, CSF('0', 005, 002, 0)); // ##.##
  //Especie produto IN.63/01
      IL(147, 002, '  '); // ##
  //Situa��o Tribut�ria
      IL(149, 003, '000'); // Num�rico. Se informado com 2 digitos alinhar a esquerda.
  //C�digo Medida Padr�o
      IL(152, 002, '00'); // Num�rico. 11 ou 12
  //Fator de Convers�o
      IL(154, 014, CSF('0', 014, 002, 1)); // ##########.###
  //C�digo EAN
      IL(168, 014, CSS('0', 014, '')); //
  //C�d. Produtos Relevantes
      IL(182, 007, CSS('0', 007, '')); //
  //Tipo de Produto
      IL(189, 001, ' '); // Utilizado para gera��o da DIF Papel Imune, op��es:
  //'L' Publica��o
  //'P' Papel
  //'S' Sobras
  //Codigo Descricao Produto
      IL(190, 005, CSS(' ', 005, '')); // Utilizado para geracao da DIF Papel Imune, informar conforme programa oficial
  //Incentivo Fiscal (MS)
      IL(195, 001, ' '); // Informe: 'S' = Sim ou 'N' = Nao
  //Sit.Tribut. Cupom Fiscal
      IL(196, 004, CSS(' ', 004, '')); // Informe:
  //'F' - Subst.Tribut.
  //'T' - Tributado
  //'I' - Isento
  //'N' - N�o Incidenc.
  //'CANC' - Cancelamentos
  //'DESC' - Descontos
  //'ISS' - ISSQN
  //DIF-BEBIDAS-Tipo Produtos
      IL(200, 002, CSS(' ', 002, '')); // 01-Insumos / 02-Envasados
  //DIF-BEBIDAS-C�digo Insumo
      IL(202, 003, CSS(' ', 003, '')); // ###
  //DIF-BEBIDAS-Capac.Recipie.
      IL(205, 005, CSS(' ', 005, '')); // #####
  //DIF-BEBIDAS-C�digo SRF
      IL(210, 007, CSS(' ', 007, '')); // ###.###
  //DNF-C�digo do Produto
      IL(217, 003, CSS(' ', 003, '')); // ###
  //C�digo SEFAZ
      IL(220, 004, CSS(' ', 004, '')); // Num�rico - SP.C�d.Combust.p/Reg.88
  //Grupo do Produto Novo (DOS)
      IL(224, 003, CSS(' ', 003, '')); // ### Se diferente de '000' e de ' ' precisa estar cadastrado na tabela de grupos de produtos.
  //Al�q.Pis Recup.Inventario
      IL(227, 005, CSS(' ', 005, '')); // ##.##
  //Al�q.Cofins Rec.Inventario
      IL(232, 005, CSS(' ', 005, '')); // ##.##
  //PE.PRODEPE.Cod.Apura��o
      IL(237, 002, CSS(' ', 002, '')); // ##
  //PE.PRODEPE.Incent.Entrada
      IL(239, 002, CSS(' ', 002, '')); // ##
  //C�digo Recolhimento PIS/COFINS
      IL(241, 006, CSS(' ', 006, '')); // Num�rico
  //RESERVADO
      IL(247, 030, CSS(' ', 030, '')); // Antigo 'Nome Fantasia' - N�o Utilizado
  //Descri��o do Grupo
      IL(277, 030, CSS(' ', 030, '')); //
  //C�digo do Grupo Pai
      IL(307, 004, CSS(' ', 004, '')); //
  //Grupo do Produto
      IL(311, 004, CSS(' ', 004, '')); // ### Se diferente de '0000' e de ' ' precisa estar cadastrado na tabela de grupos de produtos.
  //P�gina 21 de 22
  //
  //L e i a u t e d e I mp o r t a � � o
  //Ar q u i v o T e x t o
  //Modelo: Produtos
  //Descri��o do Campo
  //Posi��es
  //Inicial Tamanho
  //Observa��es
  //TARE - Item da Portaria
      IL(315, 005, CSS(' ', 005, '')); // Alfanum�rico (aceita branco)
  //% Redu��o Oper. Internas
      IL(320, 008, CSF('0', 008, 004, 0)); // Formato ###.#### (aceita zeros)
  //Sujeito � Substitui��o Tribut�ria
      IL(328, 001, ' '); // Informe: 'S' = Sim ou 'N' = Nao , Se igual ' ' assumir� o valor 'N'.
  //PRODUTO - SPED.Tipo do Item
      IL(329, 002, CSI('0', 002, QrProdTipo_Item.Value));
  //00 - Mercadoria para Revenda
  //01 - Mat�ria-Prima
  //02 - Embalagem
  //03 - Produto em Processo
  //04 - Produto Acabado
  //05 - Subproduto
  //06 - Produto Intermedi�rio
  //07 - Material de Uso e Consumo
  //08 - Ativo Imobilizado
  //09 - Servi�os
  //10 - Outros insumos
  //99 - Outras
  //C�digo do Selo de Controle IPI
      IL(331, 006, CSS(' ', 006, '')); // Alfanum�rico Tabela SPED 4.5.2
  //Classe Enquadramento do IPI
      IL(337, 005, CSS(' ', 005, '')); // Alfanum�rico Tabela SPED 4.5.1
  //Medicamento Refer�ncia Base C�lculo
      IL(342, 001, ' ');
  //0 - Pre�o tabelado ou pre�o m�ximo sugerido
  //1 - Margem de valr agragado
  //2 - Lista Negativa
  //3 - Lista Positiva
  //4 - Lista Neutra
  //Medicamentos.Tipo de Produto
      IL(343, 001, ' ');
  //0 - Similar
  //1 - Gen�rico
  //2 -�tico ou de marca
  //Medicamento.Pre�o Tabelado/M�ximo
      IL(344, 014, CSF('0', 014, 002, 0)); // ###########.## Em Reais
  //Comunica��o.C�digo de Classifica��o do Item
      IL(358, 004, CSS(' ', 004, '')); // #### Tabela SPED 4.4.1
  //PRODUTO - Comunica��o.Tipo de Receita
      IL(362, 001, ' ');
  //0 - Pr�pria - Servi�os Prestados
  //1 - Pr�pria - Cobran�a de D�bitos
  //2 - Pr�pria - Vendas de Mercadorias
  //3 - Pr�pria - Vendas de Servi�o pr�-pago
  //4 - Outras receitas pr�prias
  //5 - Receitas de terceiros (co-faturamento)
  //9 - Outras receitas de terceiros
  //Energia El�trica.C�digo de Classifica��o do Item
      IL(363, 004, CSS(' ', 004, '')); // #### Tabela SPED 4.4.1
  //Energia El�trica.Tipo de Receita
      IL(367, 001, ' ');
  //0 - Receita Pr�pria
  //1 - Receita de Terceiros
  //Ve�culo.N�meor do Chassi
      IL(368, 017, CSS(' ', 017, '')); // Alfanum�rico
  //IPI.Valor por unidade Padr�o de Tributa��o
      IL(385, 014, CSF(' ', 014, 002, 0)); // ###########.## Em Reais
  //C�digo EX, conforme a TIPI
      IL(399, 004, CSS(' ', 004, QrProdEX_TIPI.Value)); // Num�rico
  //Tipo de Produto
      IL(403, 001, '0');
  //0 - Outros;
  //1 - Insumos;
  //2 - Envasados;
  //3 - Combust�vel/Solvente;
  //4 - Hortifrut�culas;
  //5 - Gado e Subprodutos;
  //6 - Sisal;
  //7 - Agropecu�rios;
  //PRODUTO - Tipo Produto/Servi�o (DIEF - CE)
      IL(404, 001, ' '); // Espec�fico CEAR�
  //1 - Mercadoria;
  //2 - Servi�o com incid�ncia de ICMS;
  //3 - Servi�o sem incid�ncia de ICMS;
  //ANP - C�digo de Produto (Tabela 12 SIMP)
      IL(405, 010, CSS(' ', 010, '')); // Espec�fico para tipo de produto '3' - Combust�vel/Solvente.
  //O c�digo deve estar previamente cadastrado na rotina Tabelas Fiscais para
  //Arquivos Magn�ticos
  //RESERVADO
      CTB := DmProd.ObtemCTB(
        QrProdNivel2.Value, QrProdNivel3.Value, QrProdGenero.Value);
      if CTB = '' then
        ItensSemCTB := ItensSemCTB + 1 ;
      IL(415, 060, CSS(' ', 060, CTB)); // RESERVADO
  //P�gina 22 de 22
  //
      REProdutos.Lines.Add(FLin);
      QrProd.Next;
    end;
    ShowMessage(IntToStr(ItensSemCTB) + ' itens n�o tem CTB cadastrado!');
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando exporta��o de produtos');
    MyObjects.ExportaMemoToFileExt(TMemo(REProdutos), FProdutos, True, False, etxtsaSemAcento, FQuebraDeLinha, Null);
  finally
    Screen.Cursor := MyCursor;
  end;
end;

end.
