unit PQTLodo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, UnMySQLCuringa, UnAppPF, dmkDBLookupCbUM,
  dmkEditUM, dmkEditCbUM;

type
  TFmPQTLodo = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrVeiculos: TMySQLQuery;
    DsVeiculos: TDataSource;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label1: TLabel;
    EdVeiculo: TdmkEditCB;
    CBVeiculo: TdmkDBLookupComboBox;
    SbVeiculo: TSpeedButton;
    Panel5: TPanel;
    Label2: TLabel;
    EdCodigo: TdmkEdit;
    EdNO_: TdmkEdit;
    Label3: TLabel;
    EdMotorista: TdmkEditCB;
    CBMotorista: TdmkDBLookupComboBox;
    SbMotorista: TSpeedButton;
    Label4: TLabel;
    EdLocal: TdmkEditCB;
    CBLocal: TdmkDBLookupComboBox;
    SbLocal: TSpeedButton;
    SbUnidMed: TSpeedButton;
    EdQtde: TdmkEdit;
    Label7: TLabel;
    QrVeiculosCodigo: TIntegerField;
    QrVeiculosDescricao: TWideStringField;
    QrMotorista: TMySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    QrStqCenLoc: TMySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    QrUnidMed: TMySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    Label17: TLabel;
    QrPesq1: TMySQLQuery;
    QrPesq1CodUsu: TIntegerField;
    QrPesq2: TMySQLQuery;
    QrPesq2Sigla: TWideStringField;
    QrVeiculosMotorisPadr: TIntegerField;
    EdUnidMed: TdmkEditCbUM;
    EdSigla: TdmkEditUM;
    CBUnidMed: TdmkDBLookupCbUM;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbVeiculoClick(Sender: TObject);
    procedure SbMotoristaClick(Sender: TObject);
    procedure SbLocalClick(Sender: TObject);
    procedure SbUnidMedClick(Sender: TObject);
    procedure EdVeiculoRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPQTLodo(Controle: Integer);
  public
    { Public declarations }
    FQrIts: TmySQLQuery;
  end;

  var
  FmPQTLodo: TFmPQTLodo;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModPediVda, UnGrade_PF, UnGrade_Jan, ModuleGeral, UnApp_Jan;

{$R *.DFM}

procedure TFmPQTLodo.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Veiculo, Motorista, Local, Unidade: Integer;
  Qtde: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  Veiculo        := EdVeiculo.ValueVariant;
  Motorista      := EdMotorista.ValueVariant;
  Local          := EdLocal.ValueVariant;
  Qtde           := EdQtde.ValueVariant;
  Unidade        := EdUnidMed.ValueVariant;
  //
  if MyObjects.FIC(Local = 0, EdLocal, 'Informe o local!') then
    Exit;
  if MyObjects.FIC(Qtde = 0, EdQtde, 'Informe a quantidade!') then
    Exit;
  if MyObjects.FIC(Unidade = 0, EdLocal, 'Informe a unidade!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('pqtlodo', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, SQLType, 'pqtlodo', False, [
  'Codigo', 'Veiculo', 'Motorista',
  'Local', 'Qtde', 'Unidade'], [
  'Controle'], [
  Codigo, Veiculo, Motorista,
  Local, Qtde, Unidade], [
  Controle],True) then
  begin
    ReopenPQTLodo(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdVeiculo.ValueVariant   := 0;
      CBVeiculo.KeyValue       := Null;
      EdMotorista.ValueVariant := 0;
      CBMotorista.KeyValue     := Null;
      EdLocal.ValueVariant     := 0;
      CBLocal.KeyValue         := Null;
      EdUnidMed.ValueVariant   := 0;
      CBUnidMed.KeyValue       := Null;
      EdQtde.ValueVariant      := '';
      //
      EdVeiculo.SetFocus;
    end else Close;
  end;
end;

procedure TFmPQTLodo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQTLodo.EdVeiculoRedefinido(Sender: TObject);
var
  Motorista: Integer;
begin
  if EdMotorista.ValueVariant = 0 then
  begin
    Motorista := QrVeiculosMotorisPadr.Value;
    if Motorista <> 0 then
    begin
      EdMotorista.ValueVariant := Motorista;
      CBMotorista.KeyValue := Motorista;
    end;
  end;
end;

procedure TFmPQTLodo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQTLodo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrVeiculos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenLoc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  //
  CBVeiculo.LocF7SQLText.Text := AppPF.SQLPesqVeiculo();
end;

procedure TFmPQTLodo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQTLodo.ReopenPQTLodo(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmPQTLodo.SbLocalClick(Sender: TObject);
begin
  VAR_CADASTRO2 := 0;
  Grade_Jan.MostraFormStqCenCad((*StqCenCad*)0, EdLocal.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdLocal, CBLocal, QrStqCenloc, VAR_CADASTRO2, 'Controle');
end;

procedure TFmPQTLodo.SbMotoristaClick(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  DModG.CadastroDeEntidade(EdMotorista.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  UMyMod.SetaCodigoPesquisado(EdMotorista, CBMotorista, QrMotorista, VAR_ENTIDADE);
end;

procedure TFmPQTLodo.SbUnidMedClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if Grade_Jan.MostraFormUnidMed(EdUnidMed.ValueVariant) then
  UMyMod.SetaCodigoPesquisado(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO);
end;

procedure TFmPQTLodo.SbVeiculoClick(Sender: TObject);
var
  Veiculo: Integer;
begin
  Veiculo := EdVeiculo.ValueVariant;
  VAR_CADASTRO := 0;
  App_Jan.MostraFormVeiculos(Veiculo);
  UMyMod.SetaCodigoPesquisado(EdVeiculo, CBVeiculo, QrVeiculos, VAR_CADASTRO);
end;

end.
