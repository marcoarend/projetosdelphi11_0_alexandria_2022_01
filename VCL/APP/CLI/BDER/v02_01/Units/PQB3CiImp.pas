unit PQB3CiImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkEditDateTimePicker,
  frxClass, frxDBSet;

type
  TFmPQB3CiImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel5: TPanel;
    EdPeriodo: TdmkEdit;
    EdPeriodo2: TdmkEdit;
    Label2: TLabel;
    EdDataAnt: TdmkEdit;
    Label3: TLabel;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    Label1: TLabel;
    TPDataAtu: TdmkEditDateTimePicker;
    frxQUI_BALAN_013_01: TfrxReport;
    frxDsIterm: TfrxDBDataset;
    QrInterm: TMySQLQuery;
    QrIntermNO_ENT: TWideStringField;
    QrIntermNO_PQ: TWideStringField;
    QrIntermAnt_Empresa: TIntegerField;
    QrIntermAnt_CliInt: TIntegerField;
    QrIntermAnt_Insumo: TIntegerField;
    QrIntermAnt_Tipo: TIntegerField;
    QrIntermAnt_DstCodi: TIntegerField;
    QrIntermAnt_DstCtrl: TIntegerField;
    QrIntermAnt_PesoDife: TFloatField;
    QrIntermAnt_Lancar: TSmallintField;
    QrIntermEmpresa: TIntegerField;
    QrIntermCliOrig: TIntegerField;
    QrIntermInsumo: TIntegerField;
    QrIntermPesoInic: TFloatField;
    QrIntermValorInic: TFloatField;
    QrIntermPesoAtuA: TFloatField;
    QrIntermValorAtuA: TFloatField;
    QrIntermPesoAtuB: TFloatField;
    QrIntermValorAtuB: TFloatField;
    QrIntermPesoAtu: TFloatField;
    QrIntermValorAtuC: TFloatField;
    QrIntermPesoPeIn: TFloatField;
    QrIntermValorPeIn: TFloatField;
    QrIntermPesoPeBx: TFloatField;
    QrIntermValorPeBx: TFloatField;
    QrIntermPesoAjIn: TFloatField;
    QrIntermValorAjIn: TFloatField;
    QrIntermPesoAjBx: TFloatField;
    QrIntermValorAjBx: TFloatField;
    QrIntermPesoInfo: TFloatField;
    QrIntermValorInfo: TFloatField;
    QrIntermPesoDife: TFloatField;
    QrIntermValorDife: TFloatField;
    QrIntermSerie: TIntegerField;
    QrIntermNF: TIntegerField;
    QrIntermxLote: TWideStringField;
    QrIntermdFab: TDateField;
    QrIntermdVal: TDateField;
    QrIntermAtivo: TSmallintField;
    QrIntermNO_Lancar: TWideStringField;
    QrIntermPesoAtuT: TFloatField;
    QrIntermValorAtuT: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxQUI_BALAN_013_01GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    FPqbPrevia: String;
    //
    procedure RecriaSaldoFinalDoDiaAnterior(DtBal, DtIni, DtAtu: TDateTime);
    procedure SaldosDeItensAlteradosBalancoIntermediarioAnterior();
  public
    { Public declarations }
  end;

  var
  FmPQB3CiImp: TFmPQB3CiImp;

implementation

uses UnMyObjects, Module, DmkDAC_PF, CreateBlueDerm, ModuleGeral, UnDmkProcFunc,
  UMySQLModule;

{$R *.DFM}

procedure TFmPQB3CiImp.BtOKClick(Sender: TObject);
var
  DataAnt, DataAtu, DataBal: String;
  Codigo, PeriAnt: Integer;
  //SQLType: TSQLType;
  DtBal, DtAnt, DtAtu, _DtBal: TDateTime;
  Ano, MesB, MesF, Dia: Word;
  //Conta: Integer;
begin
  Codigo         := EdPeriodo.ValueVariant;
  //
(*
  SQLType        := ImgTipo.SQLType;
  Controle       := 0;
*)
  DtAnt          := Trunc(EdDataAnt.ValueVariant);
  DtAtu          := Trunc(TPDataAtu.Date);
  DtBal          := dmkPF.PrimeiroDiaDoPeriodo_Date(Codigo);
  DecodeDate(DtBal, Ano, MesB, Dia);
  DecodeDate(DtAtu, Ano, MesF, Dia);
  //
  if MyObjects.FIC(DtAtu - DtAnt < 1, TPDataAtu,
    'Data do balan�o deve ser maior que a data inicial!') then Exit;
  // desmarcar se necess�rio
  if MyObjects.FIC(MesB <> MesF, TPDataAtu,
    'Data do balan�o intermedi�rio deve ser dentro do m�s do balan�o!') then Exit;
  if MyObjects.FIC(DtAnt < 2, nil,
    'Ainda n�o existe nenhum ciclo criado! Ser� considerada a data do balan�o como data inicial!') then
  begin
    DtAnt := DtBal;
  end;
  //
  DataAnt        := Geral.FDT(DtAnt, 1);
  DataAtu        := Geral.FDT(DtAtu, 1);
  DataBal        := Geral.FDT(DtBal, 1);
  //
  if DtAnt = DtAtu then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT MAX(Periodo) Periodo  ',
    'FROM balancos ',
    'WHERE Periodo<' + Geral.FF0(Codigo),
    '']);
    PeriAnt := Dmod.QrAux.Fields[0].AsInteger;
    _DtBal := dmkPF.PrimeiroDiaDoPeriodo_Date(PeriAnt);
  end else
    _DtBal := DtBal;
  //
  RecriaSaldoFinalDoDiaAnterior(_DtBal, DtAnt, DtAtu);
  //
  SaldosDeItensAlteradosBalancoIntermediarioAnterior();
  //
  MyObjects.frxDefineDatasets(frxQUI_BALAN_013_01, [
    DModG.frxDsDono,
    frxDsIterm
  ]);
  MyObjects.frxMostra(frxQUI_BALAN_013_01, 'Balan�o Intermedi�rio');
  //
  Close;
end;

procedure TFmPQB3CiImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQB3CiImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQB3CiImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPDataAtu.Date := Trunc(DModG.ObtemAgora());
end;

procedure TFmPQB3CiImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQB3CiImp.frxQUI_BALAN_013_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VAR_DATA_REF' then
    Value := Geral.FDT(TPDataAtu.Date, 2)
  else
end;

procedure TFmPQB3CiImp.RecriaSaldoFinalDoDiaAnterior(DtBal, DtIni, DtAtu: TDateTime);
var
  DataBal, DataIni, DataFim, SQL: String;
begin
  DataBal := Geral.FDT(DtBal, 1);
  DataIni := Geral.FDT(DtIni, 1);
  DataFim := Geral.FDT(DtAtu - 1, 1);
  //
  FPqbPrevia :=
    UnCreateBlueDerm.RecriaTempTableNovo(ntrttPQBCiPrevia, DmodG.QrUpdPID1, False);
  //
  SQL := Geral.ATS([
  'DELETE FROM ' +  FPqbPrevia + ';',
  'INSERT INTO ' +  FPqbPrevia,
  'SELECT Empresa, CliOrig, Insumo, ',
  'SUM(IF(DataX<"' + DataIni + '" OR Tipo IN (0,20,120), Peso, 0)) PesoInic,  ',
  'SUM(IF(DataX<"' + DataIni + '" OR Tipo IN (0,20,120), Valor, 0)) ValorInic,  ',
  'SUM(Peso) PesoAtuA, SUM(Valor) ValorAtuA, ',
  '0.000 PesoAtuB, 0.00 ValorAtuB,',
  '0.000 PesoAtuC, 0.00 ValorAtuC,',
  'SUM(Peso) PesoAtuT, SUM(Valor) ValorAtuT, ',
  '',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso>0 AND (NOT Tipo IN (0,20,120)), Peso, 0)) PesoPeIn,  ',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso>0 AND (NOT Tipo IN (0,20,120)), Valor, 0)) ValorPeIn,  ',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso<0 AND (NOT Tipo IN (0,20,120)), Peso, 0)) PesoPeBx,  ',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso<0 AND (NOT Tipo IN (0,20,120)), Valor, 0)) ValorPeBx,',
  '',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso>0 AND (Tipo IN (20,120)), Peso, 0)) PesoAjIn,  ',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso>0 AND (Tipo IN (20,120)), Valor, 0)) ValorAjIn,  ',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso<0 AND (Tipo IN (20,120)), Peso, 0)) PesoAjBx,  ',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso<0 AND (Tipo IN (20,120)), Valor, 0)) ValorAjBx,',
  '',
  '0.000 PesoInfo, 0.00 ValorInfo, 0.000 PesoDife, 0.00 ValorDife,',
  '0 Serie, 0 NF, "                    " xLote, ',
  'DATE("0000-00-00") dFab, DATE("0000-00-00") dVal, 1 Ativo   ',
  'FROM ' + TMeuDB + '.pqx ',
  'WHERE DataX BETWEEN "' + DataBal + '" AND "' + DataFim + '" ',
  //'AND CliOrig=140 ',
  'GROUP BY Empresa, CliOrig, Insumo ',
  '']);
  //
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  //Geral.MB_teste(SQL);
  //
end;

procedure TFmPQB3CiImp.SaldosDeItensAlteradosBalancoIntermediarioAnterior;
var
  //Periodo, Controle,
  CtrlAnt: Integer;
  sDataAtu: String;
begin
(*
  if MyObjects.FIC(QrPQBCiCab.RecordCount = 0, nil,
  'N�o h� ciclo a ser impresso neste balan�o') then Exit;
  //
  // ir para o �ltimo balanco intermedi�rio do per�odo
  //QrPQBCiCab.First;
  if QrPQBCiCab.RecNo <> 0 then
    if Geral.MB_Pergunta(
    'O balan�o intermedi�rio selecionado n�o � o �ltimo feito.' + sLineBreak +
    'Deseja continuar assim mesmo?') <> ID_YES then Exit;
  //
  Periodo  := QrBalancosPeriodo.Value;
  Controle := QrPQBCiCabControle.Value;
*)
  sDataAtu := Geral.FDT(EdDataAnt.ValueVariant, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Controle',
  'FROM pqbcicab',
(*
  'WHERE Codigo=' + Geral.FF0(Periodo),
  'AND DataAtu < "' + sDataAtu + '"',
*)
  'WHERE DataAtu = "' + sDataAtu + '"',
  '']);
  CtrlAnt := Dmod.QrAux.Fields[0].AsInteger;
  if CtrlAnt = 0 then
    CtrlAnt := -999999999;
  UnDmkDAC_PF.AbreMySQLQuery0(QrInterm, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _pqbciits_ant_',
  ';',
  'CREATE TABLE _pqbciits_ant_',
  'SELECT Empresa Ant_Empresa, CliInt Ant_CliInt, ',
  'Insumo Ant_Insumo, Tipo Ant_Tipo, ',
  'DstCodi Ant_DstCodi, DstCtrl Ant_DstCtrl,',
  'PesoDife Ant_PesoDife, Lancar Ant_Lancar',
  'FROM ' + TMeuDB + '.pqbciits',
  'WHERE Controle=' + Geral.FF0(CtrlAnt),
  'AND PesoDife <> 0',
  ';',
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,',
  'pq.Nome NO_PQ, ant.*, atu.*, ',
  'IF(ABS(ant.Ant_PesoDife)>=0.001, IF(ant.Ant_Lancar=0, "N�o", "Sim"), "N/A") NO_Lancar ',
  //'FROM ' + TMeuDB + '.pqbciits atu',
  'FROM ' + FPqbPrevia + ' atu ',
  'LEFT JOIN _pqbciits_ant_ ant ON ',
  '  ant.Ant_Empresa=atu.Empresa',
  '  AND ant.Ant_CliInt=atu.CliOrig',
  '  AND ant.Ant_Insumo=atu.Insumo',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=atu.CliOrig',
  'LEFT JOIN ' + TMeuDB + '.pq pq ON pq.Codigo=atu.Insumo',
  //'WHERE atu.Controle=' + Geral.FF0(Controle),
  'ORDER BY NO_ENT, CliOrig, NO_PQ, Insumo',
  '']);
  //
  //Geral.MB_Teste(QrInterm.SQL.Text);
  //
end;

end.
