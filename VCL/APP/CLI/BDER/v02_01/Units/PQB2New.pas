unit PQB2New;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DBCtrls, UnGOTOy, UnInternalConsts, UnMsgInt, StdCtrls,
  Db, (*DBTables,*) Buttons, UMySQLModule, mySQLDbTables, dmkGeral, dmkImage,
  UnDmkEnums, unDmkProcFunc, DmkDAC_PF, PQx, dmkEdit, dmkDBLookupComboBox,
  dmkEditCB, UnProjGroup_Vars;

type
  TFmPQB2New = class(TForm)
    QrLocPeriodo: TmySQLQuery;
    QrLocPeriodoPeriodo: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel1: TPanel;
    CBAno: TComboBox;
    Label3: TLabel;
    Label2: TLabel;
    CBMes: TComboBox;
    GBRodape: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    BtConfirma: TBitBtn;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmPQB2New: TFmPQB2New;

implementation

uses UnMyObjects, Module, Principal, PQB2, ModuleGeral;

{$R *.DFM}

procedure TFmPQB2New.FormCreate(Sender: TObject);
var
  Ano, Mes, Dia : Word;
begin
  ImgTipo.SQLType := stLok;
  //
  with CBMes.Items do
  begin
    Add(FIN_JANEIRO);
    Add(FIN_FEVEREIRO);
    Add(FIN_MARCO);
    Add(FIN_ABRIL);
    Add(FIN_MAIO);
    Add(FIN_JUNHO);
    Add(FIN_JULHO);
    Add(FIN_AGOSTO);
    Add(FIN_SETEMBRO);
    Add(FIN_OUTUBRO);
    Add(FIN_NOVEMBRO);
    Add(FIN_DEZEMBRO);
  end;
  DecodeDate(Date, Ano, Mes, Dia);
  with CBAno.Items do
  begin
    Add(IntToStr(Ano-2));
    Add(IntToStr(Ano-1));
    Add(IntToStr(Ano));
    Add(IntToStr(Ano+1));
    Add(IntToStr(Ano+2));
  end;
  CBAno.ItemIndex := 2;
  CBMes.ItemIndex := (Mes - 1);
  //UnDmkDAC_PF.AbreQuery(QrTerceiros, Dmod.MyDB);
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  // Erro! if EdEmpresa.ValueVariant > 0 then
    //CBMes.SetFocus;
end;

procedure TFmPQB2New.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQB2New.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQB2New.BtConfirmaClick(Sender: TObject);
var
  Ano, Mes, Dia : word;
  Periodo, Atual, GrupoBal: Integer;
  //
  Codigo, CodUsu, Empresa, PrdGrupTip, StqCenCad, CasasProd: Integer;
  Nome, Abertura, Encerrou: String;
  Acao: Integer;
begin
  //VAR_PERIODO_BAL := -2;
  Ano := Geral.IMV(CBAno.Text);
  if Ano = 0 then Exit;
  Mes := CBMes.ItemIndex + 1;
  Periodo := ((Ano - 2000) * 12) + Mes;
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  //
(*
  QrLocPeriodo.Close;
  QrLocPeriodo.Params[0].AsInteger := Periodo;
  UnDmkDAC_PF.AbreQuery(QrLocPeriodo, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocPeriodo, Dmod.MyDB, [
  'SELECT Periodo ',
  'FROM balancos ',
  'WHERE Periodo=' + Geral.FF0(Periodo),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  if MyObjects.FIC(QrLocPeriodo.RecordCount > 0, CBMes,
  'Este per�odo de balan�o j� existe!') then
    Exit;
  // A - Impede criar do m�s futuro (pela data do sistema)
  DecodeDate(Date, Ano, Mes, Dia);
  Atual := ((Ano - 2000) * 12) + Mes;
  if MyObjects.FIC(Periodo > Atual, CBMes, 'M�s inv�lido. [Futuro]') then
    Exit;
  // fim A
  // B - Impede criar m�s anterior ao m�ximo
  if MyObjects.FIC(Periodo < UnPQx.VerificaBalanco, CBMes,
  'M�s inv�lido. J� existe m�s posterior ao desejado.') Then
    Exit;
  //Fim B
  //
  if not DModG.BuscaProximoCodigoInt_Novo('Controle', 'GrupoBal', '', 0, '', GrupoBal) then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
(*
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO Balancos SET Periodo=:P0, GrupoBal=:P1');
  Dmod.QrUpdM.Params[00].AsInteger := Periodo;
  Dmod.QrUpdM.Params[01].AsInteger := GrupoBal;
  Dmod.QrUpdM.ExecSQL;
*)
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'balancos', False, [
  'Periodo', 'GrupoBal', 'Empresa'], [
  ], [
  Periodo, GrupoBal, Empresa], [
  ], True) then
  begin
    //  Cadastro da balanco em StqBalCad (Grade)
    Codigo := UMyMod.BuscaEmLivreY_Def('stqbalcad', 'Codigo', stIns, 0);
    CodUsu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqBalCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, nil);
    Nome := 'Balan�o de Uso e Consumo de ' + Geral.Maiusculas(
      dmkPF.PrimeiroDiaDoPeriodo(Periodo, dtTexto), False);
    PrdGrupTip := -2; // Produtos qu�micos
    StqCenCad  := 1;
    CasasProd  := 3;
    Abertura   := dmkPF.PrimeiroDiaDoPeriodo(Periodo, dtSystem);
    Encerrou   := Abertura;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqbalcad', False, [
    'CodUsu', 'Nome', 'Empresa',
    'PrdGrupTip', 'StqCenCad', 'CasasProd',
    'Abertura', 'Encerrou', 'GrupoBal'], [
    'Codigo'], [
    CodUsu, Nome, Empresa,
    PrdGrupTip, StqCenCad, CasasProd,
    Abertura, Encerrou, GrupoBal], [
    Codigo], True);
    //
    UnPQx.DefineVAR_Data_Insum_Movim();
    QrLocPeriodo.Close;
    VAR_PERIODO := Periodo;
    FmPQB2.DefParams;
    FmPQB2.LocCod(FmPQB2.QrBalancosPeriodo.Value, Periodo);
  //
    Acao := MyObjects.SelRadioGroup('A��o',
    'Selecione o que fazer', ['Resgata estoque', 'Insere itens manualmente'], 1);
  //
    //FmPQB2.MostraEdicao(True, stIns, 0);
  //
    case Acao of
       0: FmPQB2.Resgata(False); // Resgata
       1: FmPQB2.MostraEdicao(True, stIns, 0); // Manualmente (Antigo)
    end;
  //
    UMyMod.UpdLockY(Periodo, Dmod.MyDB, 'Balancos', 'Periodo');
    Close;
  end;
end;

procedure TFmPQB2New.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

end.
