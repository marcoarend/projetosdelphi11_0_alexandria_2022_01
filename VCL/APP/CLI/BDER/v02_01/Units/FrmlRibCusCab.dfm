object FmFrmlRibCusCab: TFmFrmlRibCusCab
  Left = 368
  Top = 194
  Caption = 'QUI-RECEI-028 :: Custo Artigo Ribeira'
  ClientHeight = 649
  ClientWidth = 1264
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1264
    Height = 553
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1264
      Height = 237
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 16
        Top = 44
        Width = 89
        Height = 13
        Caption = 'Receita de caleiro:'
      end
      object Label4: TLabel
        Left = 508
        Top = 44
        Width = 107
        Height = 13
        Caption = 'Receita de curtimento:'
      end
      object Label10: TLabel
        Left = 388
        Top = 92
        Width = 67
        Height = 13
        Caption = 'Peso Entrada:'
      end
      object Label5: TLabel
        Left = 320
        Top = 92
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label6: TLabel
        Left = 838
        Top = 92
        Width = 46
        Height = 13
        Caption = 'M'#233'dia m'#178':'
      end
      object Label8: TLabel
        Left = 906
        Top = 92
        Width = 44
        Height = 13
        Caption = 'M'#233'dia ft'#178':'
      end
      object Label11: TLabel
        Left = 564
        Top = 92
        Width = 61
        Height = 13
        Caption = 'Peso caleiro:'
      end
      object Label12: TLabel
        Left = 740
        Top = 92
        Width = 79
        Height = 13
        Caption = 'Peso curtimento:'
      end
      object Label13: TLabel
        Left = 484
        Top = 92
        Width = 74
        Height = 13
        Caption = '% Quebra PDA:'
      end
      object Label14: TLabel
        Left = 660
        Top = 92
        Width = 73
        Height = 13
        Caption = '% Quebra PTA:'
      end
      object Label15: TLabel
        Left = 16
        Top = 136
        Width = 52
        Height = 13
        Caption = 'BRL/USD:'
      end
      object Label16: TLabel
        Left = 120
        Top = 136
        Width = 54
        Height = 13
        Caption = 'USD/EUR:'
      end
      object Label17: TLabel
        Left = 16
        Top = 176
        Width = 47
        Height = 13
        Caption = 'BRL/???:'
      end
      object Label18: TLabel
        Left = 120
        Top = 176
        Width = 57
        Height = 13
        Caption = 'Moeda ???:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label34: TLabel
        Left = 692
        Top = 4
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 20
        Width = 613
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdReceiCal: TdmkEditCB
        Left = 16
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReceiCal'
        UpdCampo = 'ReceiCal'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBReceiCal
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBReceiCal: TdmkDBLookupComboBox
        Left = 74
        Top = 60
        Width = 428
        Height = 21
        Color = clWhite
        KeyField = 'Numero'
        ListField = 'Nome'
        ListSource = DsFormulasCal
        TabOrder = 5
        dmkEditCB = EdReceiCal
        QryCampo = 'ReceiCal'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdReceiCur: TdmkEditCB
        Left = 508
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReceiCur'
        UpdCampo = 'ReceiCur'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBReceiCur
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBReceiCur: TdmkDBLookupComboBox
        Left = 566
        Top = 60
        Width = 428
        Height = 21
        Color = clWhite
        KeyField = 'Numero'
        ListField = 'Nome'
        ListSource = DsFormulasCur
        TabOrder = 7
        dmkEditCB = EdReceiCur
        QryCampo = 'ReceiCur'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object RGOrigemPrc: TdmkRadioGroup
        Left = 16
        Top = 86
        Width = 301
        Height = 47
        Caption = ' Origem pre'#231'os: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Estoque'
          #218'ltima compra'
          'Lista / Cota'#231#227'o')
        TabOrder = 8
        QryCampo = 'OrigemPrc'
        UpdCampo = 'OrigemPrc'
        UpdType = utYes
        OldValor = 0
      end
      object MeObserv: TdmkMemo
        Left = 224
        Top = 148
        Width = 761
        Height = 77
        MaxLength = 511
        TabOrder = 21
        WordWrap = False
        QryCampo = 'Observ'
        UpdCampo = 'Observ'
        UpdType = utYes
      end
      object EdPecas: TdmkEdit
        Left = 320
        Top = 108
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 1
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPesoNat: TdmkEdit
        Left = 388
        Top = 108
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoNat'
        UpdCampo = 'PesoNat'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdPesoNatChange
      end
      object EdQbrNatPDA: TdmkEdit
        Left = 484
        Top = 108
        Width = 77
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMax = '100'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'QbrNatPDA'
        UpdCampo = 'QbrNatPDA'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdQbrNatPDAChange
      end
      object EdPesoPDA: TdmkEdit
        Left = 564
        Top = 108
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoPDA'
        UpdCampo = 'PesoPDA'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdPesoPDAChange
      end
      object EdQbrNatPTA: TdmkEdit
        Left = 660
        Top = 108
        Width = 77
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMax = '100'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'QbrNatPTA'
        UpdCampo = 'QbrNatPTA'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdQbrNatPTAChange
      end
      object EdPesoPTA: TdmkEdit
        Left = 740
        Top = 108
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoPTA'
        UpdCampo = 'PesoPTA'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdPesoPTAChange
      end
      object EdBRL_USD: TdmkEdit
        Left = 16
        Top = 152
        Width = 100
        Height = 21
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'BRL_USD'
        UpdCampo = 'BRL_USD'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdUSD_EUR: TdmkEdit
        Left = 120
        Top = 152
        Width = 100
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'USD_EUR'
        UpdCampo = 'USD_EUR'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdBRL_OTH: TdmkEdit
        Left = 16
        Top = 192
        Width = 100
        Height = 21
        Alignment = taRightJustify
        TabOrder = 19
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'BRL_OTH'
        UpdCampo = 'BRL_OTH'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdSiglaOTH: TdmkEdit
        Left = 120
        Top = 192
        Width = 97
        Height = 21
        MaxLength = 3
        TabOrder = 20
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'SiglaOTH'
        UpdCampo = 'SiglaOTH'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdMediaM2: TdmkEditCalc
        Left = 836
        Top = 108
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'MediaM2'
        UpdCampo = 'MediaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdMediaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdMediaP2: TdmkEditCalc
        Left = 904
        Top = 108
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'MediaP2'
        UpdCampo = 'MediaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdMediaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
      object EdClienteI: TdmkEditCB
        Left = 692
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClienteI'
        UpdCampo = 'ClienteI'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClienteI
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClienteI: TdmkDBLookupComboBox
        Left = 752
        Top = 20
        Width = 241
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECI'
        ListSource = DsCliInt
        TabOrder = 3
        dmkEditCB = EdClienteI
        QryName = 'ClienteI'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 490
      Width = 1264
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1125
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1264
    Height = 553
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1264
      Height = 221
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 12
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 12
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label19: TLabel
        Left = 16
        Top = 52
        Width = 89
        Height = 13
        Caption = 'Receita de caleiro:'
      end
      object Label20: TLabel
        Left = 508
        Top = 52
        Width = 107
        Height = 13
        Caption = 'Receita de curtimento:'
      end
      object Label21: TLabel
        Left = 320
        Top = 96
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label22: TLabel
        Left = 388
        Top = 96
        Width = 67
        Height = 13
        Caption = 'Peso Entrada:'
      end
      object Label23: TLabel
        Left = 484
        Top = 96
        Width = 74
        Height = 13
        Caption = '% Quebra PDA:'
      end
      object Label24: TLabel
        Left = 564
        Top = 96
        Width = 61
        Height = 13
        Caption = 'Peso caleiro:'
      end
      object Label25: TLabel
        Left = 660
        Top = 96
        Width = 73
        Height = 13
        Caption = '% Quebra PTA:'
      end
      object Label26: TLabel
        Left = 740
        Top = 96
        Width = 79
        Height = 13
        Caption = 'Peso curtimento:'
      end
      object Label27: TLabel
        Left = 838
        Top = 96
        Width = 46
        Height = 13
        Caption = 'M'#233'dia m'#178':'
      end
      object Label28: TLabel
        Left = 906
        Top = 96
        Width = 44
        Height = 13
        Caption = 'M'#233'dia ft'#178':'
      end
      object Label29: TLabel
        Left = 16
        Top = 140
        Width = 52
        Height = 13
        Caption = 'BRL/USD:'
      end
      object Label30: TLabel
        Left = 120
        Top = 140
        Width = 54
        Height = 13
        Caption = 'USD/EUR:'
      end
      object Label31: TLabel
        Left = 16
        Top = 180
        Width = 47
        Height = 13
        Caption = 'BRL/???:'
      end
      object Label32: TLabel
        Left = 120
        Top = 180
        Width = 57
        Height = 13
        Caption = 'Moeda ???:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label33: TLabel
        Left = 876
        Top = 12
        Width = 60
        Height = 13
        Caption = 'Data / Hora:'
        FocusControl = DBEdit17
      end
      object Label35: TLabel
        Left = 812
        Top = 12
        Width = 35
        Height = 13
        Caption = 'Cli. Int.:'
        FocusControl = DBEdit18
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 28
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsFrmlRibCusCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 28
        Width = 733
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsFrmlRibCusCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 68
        Width = 56
        Height = 21
        DataField = 'ReceiCal'
        DataSource = DsFrmlRibCusCab
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 76
        Top = 68
        Width = 428
        Height = 21
        DataField = 'NO_ReceiCal'
        DataSource = DsFrmlRibCusCab
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 508
        Top = 68
        Width = 56
        Height = 21
        DataField = 'ReceiCur'
        DataSource = DsFrmlRibCusCab
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 568
        Top = 68
        Width = 421
        Height = 21
        DataField = 'NO_ReceiCur'
        DataSource = DsFrmlRibCusCab
        TabOrder = 5
      end
      object DBRGOrigemPrc: TDBRadioGroup
        Left = 16
        Top = 90
        Width = 301
        Height = 47
        Caption = ' Origem pre'#231'os: '
        Columns = 3
        DataField = 'OrigemPrc'
        DataSource = DsFrmlRibCusCab
        Items.Strings = (
          'Estoque'
          'Cadastro'
          'Manual')
        TabOrder = 6
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object DBEdit5: TDBEdit
        Left = 320
        Top = 112
        Width = 64
        Height = 21
        DataField = 'Pecas'
        DataSource = DsFrmlRibCusCab
        TabOrder = 7
      end
      object DBEdit6: TDBEdit
        Left = 388
        Top = 112
        Width = 92
        Height = 21
        DataField = 'PesoNat'
        DataSource = DsFrmlRibCusCab
        TabOrder = 8
      end
      object DBEdit7: TDBEdit
        Left = 484
        Top = 112
        Width = 77
        Height = 21
        DataField = 'QbrNatPDA'
        DataSource = DsFrmlRibCusCab
        TabOrder = 9
      end
      object DBEdit9: TDBEdit
        Left = 660
        Top = 112
        Width = 77
        Height = 21
        DataField = 'PesoPTA'
        DataSource = DsFrmlRibCusCab
        TabOrder = 10
      end
      object DBEdit8: TDBEdit
        Left = 564
        Top = 112
        Width = 92
        Height = 21
        DataField = 'PesoPDA'
        DataSource = DsFrmlRibCusCab
        TabOrder = 11
      end
      object DBEdit10: TDBEdit
        Left = 740
        Top = 112
        Width = 92
        Height = 21
        DataField = 'PesoPTA'
        DataSource = DsFrmlRibCusCab
        TabOrder = 12
      end
      object DBEdit11: TDBEdit
        Left = 836
        Top = 112
        Width = 64
        Height = 21
        DataField = 'MediaM2'
        DataSource = DsFrmlRibCusCab
        TabOrder = 13
      end
      object DBEdit12: TDBEdit
        Left = 908
        Top = 112
        Width = 80
        Height = 21
        DataField = 'MediaP2'
        DataSource = DsFrmlRibCusCab
        TabOrder = 14
      end
      object DBEdit13: TDBEdit
        Left = 16
        Top = 156
        Width = 100
        Height = 21
        DataField = 'BRL_USD'
        DataSource = DsFrmlRibCusCab
        TabOrder = 15
      end
      object DBEdit14: TDBEdit
        Left = 120
        Top = 156
        Width = 100
        Height = 21
        DataField = 'USD_EUR'
        DataSource = DsFrmlRibCusCab
        TabOrder = 16
      end
      object DBEdit15: TDBEdit
        Left = 120
        Top = 196
        Width = 100
        Height = 21
        DataField = 'SiglaOTH'
        DataSource = DsFrmlRibCusCab
        TabOrder = 17
      end
      object DBEdit16: TDBEdit
        Left = 16
        Top = 196
        Width = 100
        Height = 21
        DataField = 'BRL_OTH'
        DataSource = DsFrmlRibCusCab
        TabOrder = 18
      end
      object DBMemo2: TDBMemo
        Left = 224
        Top = 140
        Width = 765
        Height = 77
        DataField = 'Observ'
        DataSource = DsFrmlRibCusCab
        TabOrder = 19
      end
      object DBEdit17: TDBEdit
        Left = 876
        Top = 28
        Width = 112
        Height = 21
        DataField = 'DataHora'
        DataSource = DsFrmlRibCusCab
        TabOrder = 20
      end
      object DBEdit18: TDBEdit
        Left = 812
        Top = 28
        Width = 61
        Height = 21
        DataField = 'ClienteI'
        DataSource = DsFrmlRibCusCab
        TabOrder = 21
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 489
      Width = 1264
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 378
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 552
        Top = 15
        Width = 710
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 577
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtRecalcula: TBitBtn
          Tag = 20
          Left = 376
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Recalcula'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtRecalculaClick
        end
      end
    end
    object PnGrades: TPanel
      Left = 0
      Top = 221
      Width = 701
      Height = 268
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
      object DBGCal: TDBGrid
        Left = 605
        Top = 0
        Width = 96
        Height = 268
        Align = alClient
        DataSource = DsCurtimento
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Ordem'
            Title.Caption = 'Seq.'
            Width = 25
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Percent'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Insumo'
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PQ'
            Title.Caption = 'Nome insumo'
            Width = 207
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Total'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Custo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PercPart'
            Title.Caption = '% Partic.'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CustPart'
            Title.Caption = '$ partic.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Visible = True
          end>
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 605
        Height = 268
        Align = alLeft
        DataSource = DsCaleiro
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Ordem'
            Title.Caption = 'Seq.'
            Width = 25
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Percent'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Insumo'
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PQ'
            Title.Caption = 'Nome insumo'
            Width = 207
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Total'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Custo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PercPart'
            Title.Caption = '% Partic.'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CustPart'
            Title.Caption = '$ partic.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1216
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 1000
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 244
        Height = 32
        Caption = 'Custo Artigo Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 244
        Height = 32
        Caption = 'Custo Artigo Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 244
        Height = 32
        Caption = 'Custo Artigo Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1264
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1260
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrFrmlRibCusCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFrmlRibCusCabBeforeOpen
    AfterOpen = QrFrmlRibCusCabAfterOpen
    BeforeClose = QrFrmlRibCusCabBeforeClose
    AfterScroll = QrFrmlRibCusCabAfterScroll
    SQL.Strings = (
      'SELECT cal.Nome NO_ReceiCal, cur.Nome NO_ReceiCur, '
      'cur.Tipificacao, rcc.*'
      'FROM frmlribcuscab rcc'
      'LEFT JOIN formulas cal ON cal.Numero=rcc.ReceiCal'
      'LEFT JOIN formulas cur ON cur.Numero=rcc.ReceiCal'
      'WHERE rcc.Codigo>0')
    Left = 484
    Top = 4
    object QrFrmlRibCusCabNO_ReceiCal: TWideStringField
      FieldName = 'NO_ReceiCal'
      Size = 30
    end
    object QrFrmlRibCusCabNO_ReceiCur: TWideStringField
      FieldName = 'NO_ReceiCur'
      Size = 30
    end
    object QrFrmlRibCusCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFrmlRibCusCabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrFrmlRibCusCabReceiCal: TIntegerField
      FieldName = 'ReceiCal'
      Required = True
    end
    object QrFrmlRibCusCabReceiCur: TIntegerField
      FieldName = 'ReceiCur'
      Required = True
    end
    object QrFrmlRibCusCabOrigemPrc: TIntegerField
      FieldName = 'OrigemPrc'
      Required = True
    end
    object QrFrmlRibCusCabDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrFrmlRibCusCabPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrFrmlRibCusCabPesoNat: TFloatField
      FieldName = 'PesoNat'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrFrmlRibCusCabPesoPDA: TFloatField
      FieldName = 'PesoPDA'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrFrmlRibCusCabPesoPTA: TFloatField
      FieldName = 'PesoPTA'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrFrmlRibCusCabQbrNatPDA: TFloatField
      FieldName = 'QbrNatPDA'
      Required = True
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrFrmlRibCusCabQbrNatPTA: TFloatField
      FieldName = 'QbrNatPTA'
      Required = True
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrFrmlRibCusCabMediaM2: TFloatField
      FieldName = 'MediaM2'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFrmlRibCusCabMediaP2: TFloatField
      FieldName = 'MediaP2'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFrmlRibCusCabBRL_USD: TFloatField
      FieldName = 'BRL_USD'
      Required = True
    end
    object QrFrmlRibCusCabUSD_EUR: TFloatField
      FieldName = 'USD_EUR'
      Required = True
    end
    object QrFrmlRibCusCabSiglaOTH: TWideStringField
      FieldName = 'SiglaOTH'
      Size = 3
    end
    object QrFrmlRibCusCabBRL_OTH: TFloatField
      FieldName = 'BRL_OTH'
      Required = True
    end
    object QrFrmlRibCusCabObserv: TWideStringField
      FieldName = 'Observ'
      Size = 512
    end
    object QrFrmlRibCusCabLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrFrmlRibCusCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrmlRibCusCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrmlRibCusCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrFrmlRibCusCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrFrmlRibCusCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrFrmlRibCusCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrFrmlRibCusCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrFrmlRibCusCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrFrmlRibCusCabClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrFrmlRibCusCabTipificacao: TIntegerField
      FieldName = 'Tipificacao'
    end
  end
  object DsFrmlRibCusCab: TDataSource
    DataSet = QrFrmlRibCusCab
    Left = 484
    Top = 48
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 12
    Top = 65520
  end
  object QrFormulasCal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM formulas'
      'ORDER BY Nome')
    Left = 576
    Top = 4
    object QrFormulasCalHHMM_P: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_P'
      Size = 10
      Calculated = True
    end
    object QrFormulasCalHHMM_R: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_R'
      Size = 10
      Calculated = True
    end
    object QrFormulasCalHHMM_T: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_T'
      Size = 10
      Calculated = True
    end
    object QrFormulasCalNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrFormulasCalNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrFormulasCalClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrFormulasCalTipificacao: TIntegerField
      FieldName = 'Tipificacao'
    end
    object QrFormulasCalDataI: TDateField
      FieldName = 'DataI'
    end
    object QrFormulasCalDataA: TDateField
      FieldName = 'DataA'
    end
    object QrFormulasCalTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrFormulasCalTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrFormulasCalTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrFormulasCalTempoT: TIntegerField
      FieldName = 'TempoT'
    end
    object QrFormulasCalHorasR: TIntegerField
      FieldName = 'HorasR'
    end
    object QrFormulasCalHorasP: TIntegerField
      FieldName = 'HorasP'
    end
    object QrFormulasCalHorasT: TIntegerField
      FieldName = 'HorasT'
    end
    object QrFormulasCalHidrica: TIntegerField
      FieldName = 'Hidrica'
    end
    object QrFormulasCalLinhaTE: TIntegerField
      FieldName = 'LinhaTE'
    end
    object QrFormulasCalCaldeira: TIntegerField
      FieldName = 'Caldeira'
    end
    object QrFormulasCalSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrFormulasCalEspessura: TIntegerField
      FieldName = 'Espessura'
    end
    object QrFormulasCalPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrFormulasCalQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrFormulasCalLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFormulasCalDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFormulasCalDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFormulasCalUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFormulasCalUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFormulasCalAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFormulasCalRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
    end
  end
  object DsFormulasCal: TDataSource
    DataSet = QrFormulasCal
    Left = 576
    Top = 48
  end
  object QrFormulasCur: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM formulas'
      'ORDER BY Nome')
    Left = 668
    Top = 4
    object QrFormulasCurHHMM_P: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_P'
      Size = 10
      Calculated = True
    end
    object QrFormulasCurHHMM_R: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_R'
      Size = 10
      Calculated = True
    end
    object QrFormulasCurHHMM_T: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_T'
      Size = 10
      Calculated = True
    end
    object QrFormulasCurNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrFormulasCurNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrFormulasCurClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrFormulasCurTipificacao: TIntegerField
      FieldName = 'Tipificacao'
    end
    object QrFormulasCurDataI: TDateField
      FieldName = 'DataI'
    end
    object QrFormulasCurDataA: TDateField
      FieldName = 'DataA'
    end
    object QrFormulasCurTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrFormulasCurTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrFormulasCurTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrFormulasCurTempoT: TIntegerField
      FieldName = 'TempoT'
    end
    object QrFormulasCurHorasR: TIntegerField
      FieldName = 'HorasR'
    end
    object QrFormulasCurHorasP: TIntegerField
      FieldName = 'HorasP'
    end
    object QrFormulasCurHorasT: TIntegerField
      FieldName = 'HorasT'
    end
    object QrFormulasCurHidrica: TIntegerField
      FieldName = 'Hidrica'
    end
    object QrFormulasCurLinhaTE: TIntegerField
      FieldName = 'LinhaTE'
    end
    object QrFormulasCurCaldeira: TIntegerField
      FieldName = 'Caldeira'
    end
    object QrFormulasCurSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrFormulasCurEspessura: TIntegerField
      FieldName = 'Espessura'
    end
    object QrFormulasCurPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrFormulasCurQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrFormulasCurLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFormulasCurDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFormulasCurDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFormulasCurUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFormulasCurUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFormulasCurAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFormulasCurRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
    end
  end
  object DsFormulasCur: TDataSource
    DataSet = QrFormulasCur
    Left = 668
    Top = 48
  end
  object QrRec: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pqc.*'
      'FROM formulasits its'
      'LEFT JOIN pq pq_ ON pq_.Codigo=its.Produto'
      'LEFT JOIN pqcli pqc ON pqc.PQ=its.Produto AND pqc.CI=-11'
      'WHERE its.Numero=2'
      'AND (NOT pqc.Controle IS NULL'
      '  OR'
      '  (its.Porcent > 0 AND its.Produto > 0 AND pq_.GGXNiv2 IN (2))'
      ')')
    Left = 384
    Top = 276
    object QrRecPQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrRecCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
    end
    object QrRecCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrRecCustoMan: TFloatField
      FieldName = 'CustoMan'
    end
    object QrRecSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrRecOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrRecPorcent: TFloatField
      FieldName = 'Porcent'
    end
    object QrRecMoedaPadrao: TSmallintField
      FieldName = 'MoedaPadrao'
    end
    object QrRecMoedaMan: TSmallintField
      FieldName = 'MoedaMan'
    end
  end
  object QrCaleiro: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq_.Nome NO_PQ, its.* '
      'FROM frmlribcusits its'
      'LEFT JOIN pq pq_ ON pq_.Codigo=its.Insumo'
      'WHERE its.Numero=2')
    Left = 84
    Top = 408
    object QrCaleiroNO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrCaleiroCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCaleiroControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCaleiroNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrCaleiroSetor: TIntegerField
      FieldName = 'Setor'
      Required = True
    end
    object QrCaleiroOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrCaleiroInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrCaleiroPercent: TFloatField
      FieldName = 'Percent'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrCaleiroPeso: TFloatField
      FieldName = 'Peso'
      Required = True
      DisplayFormat = '#,##0.000;-#,##0.000; '
    end
    object QrCaleiroCusto: TFloatField
      FieldName = 'Custo'
      Required = True
      DisplayFormat = '#,##0.0000;-#,##0.0000; '
    end
    object QrCaleiroTotal: TFloatField
      FieldName = 'Total'
      Required = True
    end
    object QrCaleiroPercPart: TFloatField
      FieldName = 'PercPart'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrCaleiroCustPart: TFloatField
      FieldName = 'CustPart'
      Required = True
      DisplayFormat = '#,##0.0000;-#,##0.0000; '
    end
    object QrCaleiroLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrCaleiroDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCaleiroDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCaleiroUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrCaleiroUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrCaleiroAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrCaleiroAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrCaleiroAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrCaleiroAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsCaleiro: TDataSource
    DataSet = QrCaleiro
    Left = 84
    Top = 452
  end
  object QrCurtimento: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq_.Nome NO_PQ, its.* '
      'FROM frmlribcusits its'
      'LEFT JOIN pq pq_ ON pq_.Codigo=its.Insumo'
      'WHERE its.Numero=2')
    Left = 492
    Top = 412
    object QrCurtimentoNO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrCurtimentoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCurtimentoControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCurtimentoNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrCurtimentoSetor: TIntegerField
      FieldName = 'Setor'
      Required = True
    end
    object QrCurtimentoOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrCurtimentoInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrCurtimentoPercent: TFloatField
      FieldName = 'Percent'
      Required = True
    end
    object QrCurtimentoPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrCurtimentoCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrCurtimentoTotal: TFloatField
      FieldName = 'Total'
      Required = True
    end
    object QrCurtimentoPercPart: TFloatField
      FieldName = 'PercPart'
      Required = True
    end
    object QrCurtimentoCustPart: TFloatField
      FieldName = 'CustPart'
      Required = True
    end
    object QrCurtimentoLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrCurtimentoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCurtimentoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCurtimentoUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrCurtimentoUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrCurtimentoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrCurtimentoAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrCurtimentoAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrCurtimentoAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsCurtimento: TDataSource
    DataSet = QrCurtimento
    Left = 492
    Top = 456
  end
  object frxQUI_RECEI_028_001: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxQUI_RECEI_028_001GetValue
    Left = 88
    Top = 348
    Datasets = <
      item
        DataSet = frxDsCaleiro
        DataSetName = 'frxDsCaleiro'
      end
      item
        DataSet = frxDsCurtimento
        DataSetName = 'frxDsCurtimento'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsFrmlRibCusCab
        DataSetName = 'frxDsFrmlRibCusCab'
      end
      item
        DataSet = frxDsT
        DataSetName = 'frxDsT'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 566.929500000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 139.842600240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708554020000000000
          Width = 188.976441420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Receita de curtimento:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Top = 105.826674020000000000
          Width = 188.976390160000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."NO_ReceiCur"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 124.724324020000000000
          Width = 264.566929130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165471500000000000
          Top = 124.724324020000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ partic.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'CUSTO DE ARTIGO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 60.472480000000000000
          Width = 113.385900000000000000
          Height = 30.236240000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."DataHora"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 124.724490000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Processo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 188.976441420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Receita de caleiro:')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590600000000000000
          Width = 188.976390160000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."NO_ReceiCal"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 124.724490000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Uso')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 124.724490000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 124.724490000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Partic.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 340.157700000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338631500000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'BRL/USD')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472645980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg/pe'#231'a')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 60.472645980000000000
          Width = 75.590551181102360000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 60.472645980000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 60.472645980000000000
          Width = 49.133858267716540000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Quebra')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 75.590600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'In Natura:')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 90.708720000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Caleiro PDA:')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 105.826840000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Curtimento [VARF_TIPIFICACAO_CUR]:')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338631500000000000
          Top = 75.590600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.6n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."BRL_USD"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 75.590765980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsFrmlRibCusCab."Pecas"> > 0, <frxDsFrmlRibCusCab."Peso' +
              'Nat">/<frxDsFrmlRibCusCab."Pecas">, 0)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 75.590765980000000000
          Width = 75.590551181102360000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."PesoNat"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 75.590765980000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."Pecas"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 75.590765980000000000
          Width = 49.133858267716540000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338631500000000000
          Top = 90.708720000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'USD/EUR')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 90.708885980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsFrmlRibCusCab."Pecas"> > 0, <frxDsFrmlRibCusCab."Peso' +
              'PDA">/<frxDsFrmlRibCusCab."Pecas">, 0)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 90.708885980000000000
          Width = 75.590551181102360000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."PesoPDA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 90.708885980000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'm'#178'/pe'#231'a')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 90.708885980000000000
          Width = 49.133858267716540000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."QbrNatPDA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338631500000000000
          Top = 105.826840000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.6n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."USD_EUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 105.827005980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsFrmlRibCusCab."Pecas"> > 0, <frxDsFrmlRibCusCab."Peso' +
              'PTA">/<frxDsFrmlRibCusCab."Pecas">, 0)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 105.827005980000000000
          Width = 75.590551181102360000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."PesoPTA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 105.827005980000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."MediaM2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 105.827005980000000000
          Width = 49.133858267716540000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."QbrNatPTA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'DADOS')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 105.826840000000000000
          Width = 113.385851180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ORIGEM_PRECOS]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 90.708720000000000000
          Width = 113.385851180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Origem dos Pre'#231'os:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 37.795300000000000000
          Width = 340.157700000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_NO_CLIINT]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 476.220780000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779527560000000000
          Width = 56.692740080000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total BRL:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 3.779530000000000000
          Width = 105.826630080000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsT."Total"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 3.779530000000000000
          Width = 49.133680080000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'BRL/kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Top = 3.779532440000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsFrmlRibCusCab."PesoNat"> > 0, <frxDsT."Total"> / <frx' +
              'DsFrmlRibCusCab."PesoNat">, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Top = 3.779530000000000000
          Width = 56.692740080000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'BRL/pe'#231'a:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 3.779532440000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsFrmlRibCusCab."Pecas"> > 0, <frxDsT."Total"> / <frxDs' +
              'FrmlRibCusCab."Pecas">, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 3.779530000000000000
          Width = 79.369920080000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'BRL/m'#178' ([frxDsFrmlRibCusCab."MediaM2"]):')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 480.000310000000000000
          Top = 3.779532440000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsFrmlRibCusCab."MediaM2"> > 0, IIF(<frxDsFrmlRibCusCab' +
              '."Pecas"> > 0, <frxDsT."Total"> / <frxDsFrmlRibCusCab."Pecas">, ' +
              '0) / <frxDsFrmlRibCusCab."MediaM2">, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Top = 3.779530000000000000
          Width = 75.590390080000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'USD/ft'#178' ([frxDsFrmlRibCusCab."MediaP2"]):')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 3.779532440000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsFrmlRibCusCab."MediaP2"> > 0, IIF(<frxDsFrmlRibCusCab' +
              '."BRL_USD"> > 0, IIF(<frxDsFrmlRibCusCab."Pecas"> > 0, <frxDsT."' +
              'Total"> / <frxDsFrmlRibCusCab."Pecas">, 0)  / <frxDsFrmlRibCusCa' +
              'b."BRL_USD">, 0) / <frxDsFrmlRibCusCab."MediaP2">, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MDCal: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 260.787570000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCaleiro
        DataSetName = 'frxDsCaleiro'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Width = 264.566929130000000000
          Height = 15.118110240000000000
          DataField = 'NO_PQ'
          DataSet = frxDsCaleiro
          DataSetName = 'frxDsCaleiro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCaleiro."NO_PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 514.015816380000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataField = 'PercPart'
          DataSet = frxDsCaleiro
          DataSetName = 'frxDsCaleiro'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCaleiro."PercPart"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165398270000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataField = 'CustPart'
          DataSet = frxDsCaleiro
          DataSetName = 'frxDsCaleiro'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCaleiro."CustPart"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataSet = frxDsCaleiro
          DataSetName = 'frxDsCaleiro'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCaleiro."Percent"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataField = 'Custo'
          DataSet = frxDsCaleiro
          DataSetName = 'frxDsCaleiro'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCaleiro."Custo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataField = 'Total'
          DataSet = frxDsCaleiro
          DataSetName = 'frxDsCaleiro'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCaleiro."Total"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 298.582870000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 264.566929130000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total Receita:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCaleiro."PercPart">,MDCal)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165681420000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCaleiro."CustPart">,MDCal)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCaleiro."Percent">,MDCal)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCaleiro."Total">,MDCal)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCaleiro."Numero"'
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315346300000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."NO_ReceiCal"]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 347.716760000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCaleiro."Numero"'
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315346300000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFrmlRibCusCab."NO_ReceiCur"]')
          ParentFont = False
        end
      end
      object MDCur: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 389.291590000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCurtimento
        DataSetName = 'frxDsCurtimento'
        RowCount = 0
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Width = 264.566929130000000000
          Height = 15.118110240000000000
          DataField = 'NO_PQ'
          DataSet = frxDsCurtimento
          DataSetName = 'frxDsCurtimento'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCurtimento."NO_PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 514.015816380000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataField = 'PercPart'
          DataSet = frxDsCurtimento
          DataSetName = 'frxDsCurtimento'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCurtimento."PercPart"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165398270000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataField = 'CustPart'
          DataSet = frxDsCurtimento
          DataSetName = 'frxDsCurtimento'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCurtimento."CustPart"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataField = 'Percent'
          DataSet = frxDsCurtimento
          DataSetName = 'frxDsCurtimento'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCurtimento."Percent"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataField = 'Custo'
          DataSet = frxDsCurtimento
          DataSetName = 'frxDsCurtimento'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCurtimento."Custo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataField = 'Total'
          DataSet = frxDsCurtimento
          DataSetName = 'frxDsCurtimento'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCurtimento."Total"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 427.086890000000000000
        Width = 680.315400000000000000
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 264.566929130000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total Receita:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCurtimento."PercPart">,MDCur)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165681420000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCurtimento."CustPart">,MDCur)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCurtimento."Percent">,MDCur)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCurtimento."Total">,MDCur)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsFrmlRibCusCab: TfrxDBDataset
    UserName = 'frxDsFrmlRibCusCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_ReceiCal=NO_ReceiCal'
      'NO_ReceiCur=NO_ReceiCur'
      'Codigo=Codigo'
      'Nome=Nome'
      'ReceiCal=ReceiCal'
      'ReceiCur=ReceiCur'
      'OrigemPrc=OrigemPrc'
      'DataHora=DataHora'
      'Pecas=Pecas'
      'PesoNat=PesoNat'
      'PesoPDA=PesoPDA'
      'PesoPTA=PesoPTA'
      'QbrNatPDA=QbrNatPDA'
      'QbrNatPTA=QbrNatPTA'
      'MediaM2=MediaM2'
      'MediaP2=MediaP2'
      'BRL_USD=BRL_USD'
      'USD_EUR=USD_EUR'
      'SiglaOTH=SiglaOTH'
      'BRL_OTH=BRL_OTH'
      'Observ=Observ'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'ClienteI=ClienteI')
    DataSet = QrFrmlRibCusCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 484
    Top = 97
  end
  object frxDsCaleiro: TfrxDBDataset
    UserName = 'frxDsCaleiro'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PQ=NO_PQ'
      'Codigo=Codigo'
      'Controle=Controle'
      'Numero=Numero'
      'Setor=Setor'
      'Ordem=Ordem'
      'Insumo=Insumo'
      'Percent=Percent'
      'Peso=Peso'
      'Custo=Custo'
      'Total=Total'
      'PercPart=PercPart'
      'CustPart=CustPart'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo')
    DataSet = QrCaleiro
    BCDToCurrency = False
    DataSetOptions = []
    Left = 80
    Top = 501
  end
  object frxDsCurtimento: TfrxDBDataset
    UserName = 'frxDsCurtimento'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PQ=NO_PQ'
      'Codigo=Codigo'
      'Controle=Controle'
      'Numero=Numero'
      'Setor=Setor'
      'Ordem=Ordem'
      'Insumo=Insumo'
      'Percent=Percent'
      'Peso=Peso'
      'Custo=Custo'
      'Total=Total'
      'PercPart=PercPart'
      'CustPart=CustPart'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo')
    DataSet = QrCurtimento
    BCDToCurrency = False
    DataSetOptions = []
    Left = 492
    Top = 505
  end
  object QrT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pqc.*'
      'FROM formulasits its'
      'LEFT JOIN pq pq_ ON pq_.Codigo=its.Produto'
      'LEFT JOIN pqcli pqc ON pqc.PQ=its.Produto AND pqc.CI=-11'
      'WHERE its.Numero=2'
      'AND (NOT pqc.Controle IS NULL'
      '  OR'
      '  (its.Porcent > 0 AND its.Produto > 0 AND pq_.GGXNiv2 IN (2))'
      ')')
    Left = 312
    Top = 412
    object QrTTotal: TFloatField
      FieldName = 'Total'
    end
  end
  object QrI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pqc.*'
      'FROM formulasits its'
      'LEFT JOIN pq pq_ ON pq_.Codigo=its.Produto'
      'LEFT JOIN pqcli pqc ON pqc.PQ=its.Produto AND pqc.CI=-11'
      'WHERE its.Numero=2'
      'AND (NOT pqc.Controle IS NULL'
      '  OR'
      '  (its.Porcent > 0 AND its.Produto > 0 AND pq_.GGXNiv2 IN (2))'
      ')')
    Left = 312
    Top = 368
    object QrITotal: TFloatField
      FieldName = 'Total'
    end
    object QrIControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object frxDsT: TfrxDBDataset
    UserName = 'frxDsT'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Total=Total')
    DataSet = QrT
    BCDToCurrency = False
    DataSetOptions = []
    Left = 312
    Top = 461
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECI, ci.Codigo'
      'FROM entidades ci'
      'WHERE ci.Cliente2="V"')
    Left = 228
    Top = 356
    object QrCliIntNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 228
    Top = 404
  end
end
