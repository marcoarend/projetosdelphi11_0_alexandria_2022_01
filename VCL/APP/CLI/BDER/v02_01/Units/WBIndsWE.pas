unit WBIndsWE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, Vcl.ComCtrls,
  dmkEditDateTimePicker, UnAppEnums;

type
  TFmWBIndsWE = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label4: TLabel;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    DsDefPecas: TDataSource;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SBPallet: TSpeedButton;
    QrWBPallet: TmySQLQuery;
    DsWBPallet: TDataSource;
    QrWBPalletCodigo: TIntegerField;
    QrWBPalletNome: TWideStringField;
    Label9: TLabel;
    EdObserv: TdmkEdit;
    DsFornecedor: TDataSource;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    Panel3: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label52: TLabel;
    TPDataHora: TdmkEditDateTimePicker;
    EdDataHora: TdmkEdit;
    Label2: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    EdMovimCod: TdmkEdit;
    Label3: TLabel;
    Label5: TLabel;
    EdValorT: TdmkEdit;
    EdLnkNivXtr1: TdmkEdit;
    Label10: TLabel;
    Label11: TLabel;
    EdLnkNivXtr2: TdmkEdit;
    SbOS: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBPalletClick(Sender: TObject);
    procedure SbOSClick(Sender: TObject);
    procedure EdPecasChange(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    //
    procedure ReopenWBMovIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure VerificaAreaEValor();
  public
    { Public declarations }
    FFormaWBIndsWE: TFormaWBIndsWE;
    FQrWBMovIts: TmySQLQuery;
    //FDsCab: TDataSource;
  end;

  var
  FmWBIndsWE: TFmWBIndsWE;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
Principal, ModuleGeral, ModEmit;

{$R *.DFM}

procedure TFmWBIndsWE.BtOKClick(Sender: TObject);
const
  SrcMovID  = 0;
  SrcNivel1 = 0;
  SrcNivel2 = 0;
  CliVenda  = 0;
  MovimTwn  = 0;
var
  DataHora, Observ: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Fornecedor, LnkNivXtr1,
  LnkNivXtr2: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
begin
  Codigo         := EdCodigo.ValueVariant;
  if MyObjects.FIC(Codigo = 0, nil, 'Defina uma OS!') then
    Exit;
  Controle       := EdControle.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Fornecedor     := EdFornecedor.ValueVariant;
  DataHora       := Geral.FDT_TP_Ed(TPDataHora.Date, EdDataHora.Text);
  MovimID        := emidIndsWE;
  MovimNiv       := eminSemNiv;
  Pallet         := EdPallet.ValueVariant;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  Pecas          := -EdPecas.ValueVariant;
  PesoKg         := -EdPesoKg.ValueVariant;
  AreaM2         := -EdAreaM2.ValueVariant;
  AreaP2         := -EdAreaP2.ValueVariant;
  ValorT         := -EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  LnkNivXtr1     := EdLnkNivXtr1.ValueVariant;
  LnkNivXtr2     := EdLnkNivXtr2.ValueVariant;
  //
  if Dmod.WBFic(GraGruX, Empresa, Fornecedor, Pallet, Pecas, AreaM2, PesoKg, ValorT,
  EdGraGruX, EdPallet, EdPecas, EdAreaM2, EdValorT) then
    Exit;
  //
  MovimCod := UMyMod.BPGS1I32('wbmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  Controle := UMyMod.BPGS1I32('wbmovits', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if Dmod.InsUpdWBMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle) then
  begin
    // 2014-02-28 : Por causa da baixa de MP sem Pesagem!!!
    DmModEmit.AtualizaMPVIts(Codigo);
    // FIM 2014-02-28
    //Dmod.AtualizaTotaisWBXxxCab('wboutcab', MovimCod);
    //Dmod.AtualizaSaldoVirtualWBMovIts(SrcNivel2);
    //FmWBOutCab.LocCod(Codigo, Codigo);
    ReopenWBMovIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdPecas.ValueVariant       := 0;
      EdPesoKg.ValueVariant      := 0;
      EdAreaM2.ValueVariant      := 0;
      EdAreaP2.ValueVariant      := 0;
      EdValorT.ValueVariant      := 0;
      EdPallet.ValueVariant      := 0;//DefineProximoPallet();
      EdObserv.Text              := '';
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmWBIndsWE.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWBIndsWE.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

{
function TFmWBIndsWE.DefineProximoPallet(): String;
var
  Txt: String;
begin
  Result := '';
  Txt := EdPallet.Text;
  if Geral.SoNumero_TT(Txt) = Txt then
  begin
    try
      Result := Geral.FI64(Geral.I64(EdPallet.Text) + 1);
    except
      Result := '';
    end;
  end else
    Result := '';
end;
}

procedure TFmWBIndsWE.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmWBIndsWE.EdPecasChange(Sender: TObject);
begin
  VerificaAreaEValor();
end;

procedure TFmWBIndsWE.FormActivate(Sender: TObject);
begin
{
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  DBEdFornecedor.DataSource := FDsCab;
  //
}
  MyObjects.CorIniComponente();
end;

procedure TFmWBIndsWE.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FUltGGX := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED ',
  'FROM wbmprcab wmp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBPallet, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM wbpallet ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  TPDataHora.Date := DModG.ObtemAgora();
  EdDataHora.Text := Geral.FDT(DModG.ObtemAgora(), 100);
end;

procedure TFmWBIndsWE.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWBIndsWE.ReopenWBMovIts(Controle: Integer);
begin
  if (FQrWBMovIts <> nil)then
  begin
    UnDmkDAC_PF.AbreQuery(FQrWBMovIts, FQrWBMovIts.Database);
    if Controle <> 0 then
      FQrWBMovIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmWBIndsWE.SbOSClick(Sender: TObject);
var
  OS: Integer;
begin
  if FFormaWBIndsWE <> fiwBaixaPrevia then
  begin
    Geral.MB_Aviso('Edi��o de OS n�o permitida!' + sLineBreak +
    'A forma que a janela foi chamada n�o permite a edi��o da OS!');
    //
    Exit;
  end;
  //
  OS := Dmod.SelecionaOSAberta();
  if OS <> 0 then
    EdCodigo.ValueVariant := OS;
end;

procedure TFmWBIndsWE.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormWBPallet();
  UMyMod.SetaCodigoPesquisado(EdPallet, CBPallet, QrWBPallet, VAR_CADASTRO);
end;

procedure TFmWBIndsWE.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

procedure TFmWBIndsWE.VerificaAreaEValor;
var
  M2Medio, Pecas, AreaM2, BRLMedM2, ValorT: Double;
  GraGruX, Fornece: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  Fornece := EdFornecedor.ValueVariant;
  if Dmod.QrControleUsaM2Medio.Value = 1 then
  begin
    if (GraGruX <> 0) and (Fornece <> 0) then
    begin
      M2Medio := Dmod.M2MedioWBFornecedor(GraGruX, Fornece);
      if M2Medio < 0.01 then
        MyObjects.Informa2(LaAviso1, LaAviso2, False,
        '�rea m�dia n�o informada na configura��o de mat�ria-prima!')
      else
      begin
        Pecas := EdPecas.ValueVariant;
        MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
        //
        AreaM2 := M2Medio * Pecas;
        EdAreaM2.ValueVariant := AreaM2;
        //
        if Dmod.QrControleUsaBRLMedM2.Value = 1 then
        begin
          BRLMedM2 := Dmod.BRLMedioWB(GraGruX);
          ValorT := AreaM2 * BRLMedM2;
          EdValorT.ValueVariant := ValorT;
        end;
      end;
    end;
  end;
end;

end.
