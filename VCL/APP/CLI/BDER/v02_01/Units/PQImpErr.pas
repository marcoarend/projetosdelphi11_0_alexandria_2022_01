unit PQImpErr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, UnDmkProcFunc, UnAppPF, dmkDBGridZTO,
  Vcl.Menus, Variants, dmkDBLookupComboBox, dmkEditCB, dmkEditDateTimePicker;

type
  TFmPQImpErr = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    QrErrE10: TMySQLQuery;
    QrErrE10Controle: TIntegerField;
    QrErrE10NO_PQ: TWideStringField;
    QrErrE10DataX: TDateField;
    QrErrE10OrigemCodi: TIntegerField;
    QrErrE10OrigemCtrl: TIntegerField;
    QrErrE10Tipo: TIntegerField;
    QrErrE10CliOrig: TIntegerField;
    QrErrE10CliDest: TIntegerField;
    QrErrE10Insumo: TIntegerField;
    QrErrE10Peso: TFloatField;
    QrErrE10Valor: TFloatField;
    DsErrE10: TDataSource;
    PCErrEstq: TPageControl;
    TabSheet4: TTabSheet;
    Panel25: TPanel;
    Label15: TLabel;
    Panel26: TPanel;
    BtDelErrE10: TBitBtn;
    DBGErrE10: TDBGrid;
    TabSheet12: TTabSheet;
    QrErrDta: TMySQLQuery;
    DsErrDta: TDataSource;
    QrErrCli: TMySQLQuery;
    DsErrCli: TDataSource;
    DBGErrDta: TDBGrid;
    MySQLQuery2: TMySQLQuery;
    DataSource2: TDataSource;
    TabSheet1: TTabSheet;
    DBGErrCli: TDBGrid;
    Panel3: TPanel;
    BtCorrigeDta: TBitBtn;
    Panel5: TPanel;
    QrErrDtaDataX: TDateField;
    QrErrDtaOrigemCodi: TIntegerField;
    QrErrDtaOrigemCtrl: TIntegerField;
    QrErrDtaTipo: TIntegerField;
    QrErrDtaCliOrig: TIntegerField;
    QrErrDtaCliDest: TIntegerField;
    QrErrDtaInsumo: TIntegerField;
    QrErrDtaPeso: TFloatField;
    QrErrDtaValor: TFloatField;
    QrErrCliDataX: TDateField;
    QrErrCliOrigemCodi: TIntegerField;
    QrErrCliOrigemCtrl: TIntegerField;
    QrErrCliTipo: TIntegerField;
    QrErrCliCliOrig: TIntegerField;
    QrErrCliCliDest: TIntegerField;
    QrErrCliInsumo: TIntegerField;
    QrErrCliPeso: TFloatField;
    QrErrCliValor: TFloatField;
    Query: TMySQLQuery;
    PB1: TProgressBar;
    QrErrE10Empresa: TIntegerField;
    BtCorrigeCli: TBitBtn;
    TabSheet2: TTabSheet;
    QrPQ: TMySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsPQ: TDataSource;
    Panel6: TPanel;
    LaPQ: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    DBGrid1: TDBGrid;
    Grade: TStringGrid;
    Panel7: TPanel;
    BtExecuta03: TBitBtn;
    TPData03: TdmkEditDateTimePicker;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrErrE10AfterOpen(DataSet: TDataSet);
    procedure QrErrE10BeforeClose(DataSet: TDataSet);
    procedure DBGErrE10DblClick(Sender: TObject);
    procedure BtCorrigeDtaClick(Sender: TObject);
    procedure QrErrCliAfterOpen(DataSet: TDataSet);
    procedure BtDelErrE10Click(Sender: TObject);
    procedure BtCorrigeCliClick(Sender: TObject);
    procedure BtExecuta03Click(Sender: TObject);
  private
    { Private declarations }
    procedure EntradasOrfas();
    procedure DatasZeradas();
    procedure CliOrigOuDestZerados();
    procedure PreeencreTeste();
  public
    { Public declarations }
  end;

  var
  FmPQImpErr: TFmPQImpErr;

implementation

uses UnMyObjects, Module, DmkDAC_PF, PQx, MyDBCheck;

{$R *.DFM}

procedure TFmPQImpErr.BtCorrigeCliClick(Sender: TObject);
  procedure DefineCliInt(Texto, FldUpd: String; OrigemCtrl: Integer);
  const
    Aviso  = '...';
    Titulo = 'Sele��o do';
    Prompt = 'Informe o ';
    Campo  = 'Descricao';
  var
    Resp: Variant;
    CliInt: Integer;
  begin
    Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo + Texto, Prompt + Texto
    + ' ID: ' + Geral.FF0(OrigemCtrl), nil, nil, Campo, 0, [
    'SELECT Codigo, RazaoSocial ' + Campo,
    'FROM entidades',
    'WHERE Cliente2="V"',
    'ORDER BY ' + Campo,
    ''], Dmod.MyDB, True);
    if Resp <> Null then
    begin
      CliInt := Resp;
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE pqx SET ' + FldUpd + '="' +
       Geral.FF0(CliInt) + '" WHERE OrigemCtrl=' + Geral.FF0(OrigemCtrl));
    end;
  end;

const
  sProcName = 'TFmPQImpErr.BtCorrigeCliClick()';
  //
  function CliIntDaQuery(): Integer;
  begin
    if Query.RecordCount > 0 then
    begin
      if Query.Fields[0].AsDateTime > 1 then
        Result := Query.Fields[0].AsInteger
      else
      if Query.Fields[1].AsDateTime > 1 then
        Result := Query.Fields[1].AsInteger
      else Result := 0;
    end else
      Result := 0;
  end;
  //
  procedure MsgNaoDa();
  begin
    if Geral.MB_Pergunta(
    'O tipo de baixa n�o permite descobrir o cliente interno para o lan�amento ' +
    Geral.FF0(QrErrCliOrigemCtrl.Value) + '. Deseja informar manualmente?') =
    ID_YES then
    begin
      if (QrErrCliCliOrig.Value = 0) then
        DefineCliInt('Cliente interno de origem', 'CliOrig', QrErrCliOrigemCtrl.Value);
      if (QrErrCliCliDest.Value = 0) then
        DefineCliInt('Cliente interno de destino', 'CliDest', QrErrCliOrigemCtrl.Value);
    end;
  end;

  procedure CorrigeAtual();
  var
    CliOrig, CliDest: Integer;
    Tabela: String;
  begin
    CliOrig := 0;
    CliDest := 0;
    case QrErrCliTipo.Value of
      VAR_FATID_0000, VAR_FATID_0020, VAR_FATID_0120:
         MsgNaoDa();
      VAR_FATID_0110:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Query, DMod.MyDB, [
        'SELECT Cli_Orig, Cli_Dest',
        'FROM emitits',
        'WHERE Controle=' + Geral.FF0(QrErrCliOrigemCtrl.Value),
        '']);
        CliOrig := Query.Fields[0].AsInteger;
        CliDest := Query.Fields[1].AsInteger;
      end;
      VAR_FATID_0150, // pqt
      VAR_FATID_0180, // pqi
      VAR_FATID_0185, // pqn
      VAR_FATID_0190: // pqo
      begin
        MsgNaoDa();
      end;
      else
        Geral.MB_Erro('FatID n�o implementado em ' + sProcName);
    end;
    //
    if (QrErrCliCliOrig.Value = 0) and (CliOrig <> 0) then
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE pqx SET CliOrig="' + Geral.FF0(CliOrig) +
      '" WHERE OrigemCtrl=' + Geral.FF0(QrErrCliOrigemCtrl.Value));
    //
    if (QrErrCliCliDest.Value = 0) and (CliDest <> 0) then
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE pqx SET CliDest="' + Geral.FF0(CliDest) +
      '" WHERE OrigemCtrl=' + Geral.FF0(QrErrCliOrigemCtrl.Value));
  end;
var
  I: Integer;
begin
  if QrErrCli.RecordCount > 0 then
  begin
    if DBGErrCli.SelectedRows.Count < 2 then
      CorrigeAtual()
    else
    begin
      Screen.Cursor := crHourGlass;
      try
        PB1.Position := 0;
        PB1.Max := DBGErrCli.SelectedRows.Count;
        with DBGErrCli.DataSource.DataSet do
        for I := 0 to DBGErrCli.SelectedRows.Count-1 do
        begin
          GotoBookmark(DBGErrCli.SelectedRows.Items[I]);
          //
          CorrigeAtual();
          //
          MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
  CliOrigOuDestZerados();
end;

procedure TFmPQImpErr.BtCorrigeDtaClick(Sender: TObject);
const
  sProcName = 'TFmPQImpErr.BtCorrigeDtaClick()';
  //
  function DataDaQuery(): TDateTime;
  begin
    if Query.RecordCount > 0 then
    begin
      if Query.Fields[0].AsDateTime > 1 then
        Result := Query.Fields[0].AsDateTime
      else
      if Query.Fields[1].AsDateTime > 1 then
        Result := Query.Fields[1].AsDateTime
      else Result := 0;
    end else
      Result := 0;
  end;
  //
  procedure CorrigeAtual();
  var
    Data: TDateTime;
    sDta, Tabela: String;
  begin
    Data := 0;
    case QrErrDtaTipo.Value of
      VAR_FATID_0000, VAR_FATID_0020, VAR_FATID_0120:
         dmkPF.PrimeiroDiaDoPeriodo_Date(QrErrDtaOrigemCodi.Value);
      VAR_FATID_0110:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Query, DMod.MyDB, [
        'SELECT DtaBaixa, DataEmis',
        'FROM emit',
        'WHERE Codigo=' + Geral.FF0(QrErrDtaOrigemCodi.Value),
        '']);
        Data := DataDaQuery();
      end;
      VAR_FATID_0150, // pqt
      VAR_FATID_0170, // pqd
      VAR_FATID_0180, // pqi
      VAR_FATID_0185, // pqn
      VAR_FATID_0190: // pqo
      begin
        Tabela := AppPF.ObtemTabelaDeFatID(QrErrDtaTipo.Value);
        UnDmkDAC_PF.AbreMySQLQuery0(Query, DMod.MyDB, [
        'SELECT DataB, DataCad, DataCad',
        'FROM ' + Tabela,
        'WHERE Codigo=' + Geral.FF0(QrErrDtaOrigemCodi.Value),
        '']);
        Data := DataDaQuery();
      end;
      else
        Geral.MB_Erro('FatID n�o implementado em ' + sProcName);
    end;
    //
    if Data > 1 then
    begin
      sDta := Geral.FDT(Data, 1);
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE pqx SET DataX="' + sDta +
      '" WHERE OrigemCtrl=' + Geral.FF0(QrErrDtaOrigemCtrl.Value));
    end;
  end;
var
  I: Integer;
begin
  if QrErrDta.RecordCount > 0 then
  begin
    if DBGErrDta.SelectedRows.Count < 2 then
      CorrigeAtual()
    else
    begin
      Screen.Cursor := crHourGlass;
      try
        PB1.Position := 0;
        PB1.Max := DBGErrDta.SelectedRows.Count;
        with DBGErrDta.DataSource.DataSet do
        for I := 0 to DBGErrDta.SelectedRows.Count-1 do
        begin
          GotoBookmark(DBGErrDta.SelectedRows.Items[I]);
          //
          CorrigeAtual();
          //
          MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
  DatasZeradas();
end;

procedure TFmPQImpErr.BtDelErrE10Click(Sender: TObject);
var
  DataBal: TDateTime;
  //
  procedure AtualizaEstoqueAtual();
  begin
    if QrErrE10DataX.Value >= DataBal then
      UnPQx.AtualizaEstoquePQ(QrErrE10CliOrig.Value, QrErrE10Insumo.Value,
      QrErrE10Empresa.Value, aeVAR, CO_VAZIO, False);
  end;
var
  I, PeriodoBal: Integer;
  Corda: String;
begin
  if QrErrE10.RecordCount > 0 then
  begin
    PeriodoBal := UnPQx.VerificaBalanco();
    DataBal := dmkPF.PrimeiroDiaDoPeriodo_Date(PeriodoBal);
    //
    if DBGErrE10.SelectedRows.Count < 2 then
      Corda := Geral.FF0(QrErrE10OrigemCtrl.Value)
    else
      Corda := MyObjects.CordaDeDBGridSelectedRows(DBGErrE10, QrErrE10, 'OrigemCtrl', True);
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      'DELETE FROM pqx WHERE Tipo=10 AND OrigemCtrl IN(' + Corda + ')');
    //
    if DBGErrE10.SelectedRows.Count < 2 then
      AtualizaEstoqueAtual()
    else
    begin
      Screen.Cursor := crHourGlass;
      try
        PB1.Position := 0;
        PB1.Max := DBGErrE10.SelectedRows.Count;
        with DBGErrE10.DataSource.DataSet do
        for I := 0 to DBGErrE10.SelectedRows.Count-1 do
        begin
          GotoBookmark(DBGErrE10.SelectedRows.Items[I]);
          //
          AtualizaEstoqueAtual();
          //
          MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
    EntradasOrfas();
  end;
end;

procedure TFmPQImpErr.BtExecuta03Click(Sender: TObject);
var
  I, Insumo: Integer;
  Preco: Double;
  xData: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  Screen.Cursor := crHourGlass;
  try
  xData := Geral.FDT(TPData03.Date, 1);
  //
  for I := 1 to Grade.RowCount -1 do
  begin
    Insumo := Geral.IMV(Grade.Cells[1, I]);
    Preco := Geral.DMV(Grade.Cells[2, I]);
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
    'UPDATE pqx SET ',
    'Valor=Peso*' + Geral.FFT_Dot(Preco, 2, siPositivo),
    'WHERE Insumo=' + Geral.FF0(Insumo),
    'AND DataX>="' + xData + '"',
    '']));
  end;
  finally
    Screen.Cursor := crDefault;
    Geral.MB_Info('Atualiza��o finalizada!');
  end;
end;

procedure TFmPQImpErr.BtOKClick(Sender: TObject);
const
  sProcName = 'TFmPQImp.PCErrEstqChange()';
begin
  case PCErrEstq.ActivePageIndex of
    0: EntradasOrfas();
    1: DatasZeradas();
    2: CliOrigOuDestZerados();
    3: PreeencreTeste();
    else Geral.MB_Erro('Aba n�o implementada! ' + sProcName)
  end;
end;

procedure TFmPQImpErr.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQImpErr.CliOrigOuDestZerados();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrCli, DMod.MyDB, [
  'SELECT DataX, OrigemCodi, OrigemCtrl, Tipo,',
  'CliOrig, CliDest, Insumo, Peso, Valor ',
  'FROM pqx',
  'WHERE CliOrig=0',
  'OR CliDest=0',
  'ORDER BY Tipo, OrigemCodi, OrigemCtrl',  '']);
end;

procedure TFmPQImpErr.DatasZeradas();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrDta, DMod.MyDB, [
  'SELECT DataX, OrigemCodi, OrigemCtrl, Tipo,',
  'CliOrig, CliDest, Insumo, Peso, Valor ',
  'FROM pqx',
  'WHERE DataX=0',
  'ORDER BY Tipo, OrigemCodi, OrigemCtrl ',
  '']);
  BtCorrigeDta.Enabled := QrErrDta.RecordCount > 0;
end;

procedure TFmPQImpErr.DBGErrE10DblClick(Sender: TObject);
var
  I: Integer;
begin
  if DBGErrE10.SelectedRows.Count > 0 then
  begin
    Screen.Cursor := crHourGlass;
    try
      with DBGErrE10.DataSource.DataSet do
      for I := 0 to DBGErrE10.SelectedRows.Count-1 do
      begin
        GotoBookmark(DBGErrE10.SelectedRows.Items[I]);
        //
        Dmod.MyDB.Execute('DELETE FROM pqx WHERE Tipo=10 AND OrigemCodi=' +
        Geral.FF0(QrErrE10OrigemCodi.Value) + ' AND OrigemCtrl=' +
        Geral.FF0(QrErrE10OrigemCtrl.Value) +
        EmptyStr);
        //
        UnPQx.AtualizaEstoquePQ(QrErrE10CliDest.Value, QrErrE10Insumo.Value,
          QrErrE10CliDest.Value, aeMsg, CO_VAZIO);
      end;
      //
      UnDmkDAC_PF.AbreQuery(QrErrE10, Dmod.MyDB);
    finally
      Screen.Cursor := crDefault;
    end;
  end else
    Geral.MB_Info('Nenhum item foi selecionado!');
end;

procedure TFmPQImpErr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQImpErr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PCErrEstq.ActivePageIndex := 0;
  //
  UnDMkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
end;

procedure TFmPQImpErr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQImpErr.PreeencreTeste();
var
  I: Integer;
begin
  // Corre��o Panorama 2023-12-16 com Vinicius
  Geral.MB_Aviso('ATEN��O!!!!!' + sLineBreak +
  'Preenchimento espec�fico! N�o � um teste! Exclusivo para a DERMATEK executar!');
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  //
  I := 0;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := 'PQ';
  Grade.Cells[02,I] := 'Pre�o';

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(45);
  Grade.Cells[02,I] := Geral.FFT(9.60, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(46);
  Grade.Cells[02,I] := Geral.FFT(8.99, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(48);
  Grade.Cells[02,I] := Geral.FFT(11.26, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(49);
  Grade.Cells[02,I] := Geral.FFT(7.83, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(53);
  Grade.Cells[02,I] := Geral.FFT(7.65, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(54);
  Grade.Cells[02,I] := Geral.FFT(8.26, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(58);
  Grade.Cells[02,I] := Geral.FFT(3.05, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(63);
  Grade.Cells[02,I] := Geral.FFT(0.73, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(64);
  Grade.Cells[02,I] := Geral.FFT(3.52, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(66);
  Grade.Cells[02,I] := Geral.FFT(86.90, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(69);
  Grade.Cells[02,I] := Geral.FFT(6.00, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(70);
  Grade.Cells[02,I] := Geral.FFT(7.01, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(74);
  Grade.Cells[02,I] := Geral.FFT(3.81, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(75);
  Grade.Cells[02,I] := Geral.FFT(3.95, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(76);
  Grade.Cells[02,I] := Geral.FFT(5.87, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(77);
  Grade.Cells[02,I] := Geral.FFT(7.34, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(81);
  Grade.Cells[02,I] := Geral.FFT(5.38, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(82);
  Grade.Cells[02,I] := Geral.FFT(4.40, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(96);
  Grade.Cells[02,I] := Geral.FFT(4.12, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(98);
  Grade.Cells[02,I] := Geral.FFT(9.17, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(103);
  Grade.Cells[02,I] := Geral.FFT(7.05, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(106);
  Grade.Cells[02,I] := Geral.FFT(7.21, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(109);
  Grade.Cells[02,I] := Geral.FFT(49.89, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(110);
  Grade.Cells[02,I] := Geral.FFT(19.76, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(114);
  Grade.Cells[02,I] := Geral.FFT(3.50, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(115);
  Grade.Cells[02,I] := Geral.FFT(13.00, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(120);
  Grade.Cells[02,I] := Geral.FFT(3.40, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(124);
  Grade.Cells[02,I] := Geral.FFT(4.41, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(161);
  Grade.Cells[02,I] := Geral.FFT(9.83, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(188);
  Grade.Cells[02,I] := Geral.FFT(0.24, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(192);
  Grade.Cells[02,I] := Geral.FFT(3.76, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(193);
  Grade.Cells[02,I] := Geral.FFT(7.69, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(194);
  Grade.Cells[02,I] := Geral.FFT(5.93, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(195);
  Grade.Cells[02,I] := Geral.FFT(6.11, 2, siPositivo);

  I := I + 1;
  Grade.Cells[00,I] := Geral.FF0(I);
  Grade.Cells[01,I] := Geral.FF0(196);
  Grade.Cells[02,I] := Geral.FFT(2.99, 2, siPositivo);

end;

procedure TFmPQImpErr.EntradasOrfas;
begin
  Screen.Cursor := crHourGlass;
  try
{
    desabilitado em 2023-10-14!!!
    Habilitar se precisar!

    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('DROP TABLE IF EXISTS _pq_a');
    DmodG.QrUpdPID1.ExecSQL;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('DROP TABLE IF EXISTS _pq_b');
    DmodG.QrUpdPID1.ExecSQL;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('CREATE TABLE _pq_a');
    DmodG.QrUpdPID1.SQL.Add('SELECT CASE WHEN ind.Tipo=0 THEN ind.RazaoSocial');
    DmodG.QrUpdPID1.SQL.Add('ELSE ind.Nome END NOMEFO,');
    DmodG.QrUpdPID1.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial');
    DmodG.QrUpdPID1.SQL.Add('ELSE cli.Nome END NOMECI, pq_.Nome NOMEPQ,');
    DmodG.QrUpdPID1.SQL.Add('lse.Nome NOMESE, pcl.*');
    DmodG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.pqcli pcl');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.pq pq_ ON pq_.Codigo=pcl.PQ');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades    cli ON cli.Codigo=pcl.CI');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.listasetores lse ON lse.Codigo=pq_.Setor');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades    ind ON ind.Codigo=pq_.IQ');
    //DmodG.QrUpdPID1.SQL.Add('WHERE pcl.CI=:P0'); Ver todos clientes internos!!!
    //DmodG.QrUpdPID1.SQL.Add('WHERE pcl.Empresa=' + Geral.FF0(FEmpresa)); Ver todas empresas ???
    DmodG.QrUpdPID1.ExecSQL;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('CREATE TABLE _pq_b');
    DmodG.QrUpdPID1.SQL.Add('SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,');
    DmodG.QrUpdPID1.SQL.Add('gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,');
    DmodG.QrUpdPID1.SQL.Add('gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,');
    DmodG.QrUpdPID1.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,');
    DmodG.QrUpdPID1.SQL.Add('SUM(smia.Qtde) QTDE, SUM(smia.Pecas) PECAS,');
    DmodG.QrUpdPID1.SQL.Add('SUM(smia.Peso) PESO, SUM(smia.AreaM2) AREAM2,');
    DmodG.QrUpdPID1.SQL.Add('SUM(smia.AreaP2) AREAP2, smia.GraGruX, gg1.NCM');
    DmodG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.stqmovitsa smia');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=smia.GraGruX');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed');
    DmodG.QrUpdPID1.SQL.Add('WHERE gg1.PrdGrupTip=-2');
    //DmodG.QrUpdPID1.SQL.Add('AND smia.Empresa=' + Geral.FF0(FEmpresa)); Ver todas empresas ???
    DmodG.QrUpdPID1.SQL.Add('GROUP BY smia.GraGruX');
    DmodG.QrUpdPID1.ExecSQL;
    //
//    QrErros8.Close;
    UnDmkDAC_PF.AbreMySQLQuery0(QrErros8, DModG.MyPID_DB, [
    'SELECT pqa.PQ, pqa.NOMEPQ, pqa.Controle, pqa.Peso kgA, ',
    'pqa.Valor, pqa.Custo, pqb.Nivel1, pqb.GraGruX, ',
    'pqb.NO_PRD, pqb.QTDE, pqb.PECAS, pqb.PESO kgB ',
    'FROM _pq_a pqa ',
    'LEFT JOIN _pq_b pqb ON pqb.Nivel1=pqa.PQ ',
    'WHERE pqa.Peso <> pqb.QTDE ',
    '']);
    //
}
    // Erros de (n�o) exclus�o na entrada
    UnDmkDAC_PF.AbreMySQLQuery0(QrErrE10, DMod.MyDB, [
    'SELECT its.Controle, pq_.Nome NO_PQ, pqx.*  ',
    'FROM pqx pqx ',
    'LEFT JOIN pqeits its ON its.Controle=pqx.OrigemCtrl ',
    'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo ',
    'WHERE pqx.Tipo=10 ',
    'AND its.Controle IS NULL ',
    EmptyStr]);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPQImpErr.QrErrCliAfterOpen(DataSet: TDataSet);
begin
  BtCorrigeCli.Enabled := QrErrCli.RecordCount > 0;
end;

procedure TFmPQImpErr.QrErrE10AfterOpen(DataSet: TDataSet);
begin
  BtDelErrE10.Enabled := QrErrE10.RecordCount > 0;
end;

procedure TFmPQImpErr.QrErrE10BeforeClose(DataSet: TDataSet);
begin
  BtDelErrE10.Enabled := False;
end;

end.
