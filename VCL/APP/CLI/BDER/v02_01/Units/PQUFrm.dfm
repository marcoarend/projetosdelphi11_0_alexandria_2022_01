object FmPQUFrm: TFmPQUFrm
  Left = 339
  Top = 185
  Caption = 'QUI-INSUM-007 :: Pedido de Produ'#231#227'o - Item de Produ'#231#227'o'
  ClientHeight = 535
  ClientWidth = 1126
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1126
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1078
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1030
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 483
        Height = 32
        Caption = 'Pedido de Produ'#231#227'o - Item de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 483
        Height = 32
        Caption = 'Pedido de Produ'#231#227'o - Item de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 483
        Height = 32
        Caption = 'Pedido de Produ'#231#227'o - Item de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 421
    Width = 1126
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1122
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 465
    Width = 1126
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 980
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 978
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 557
    Height = 373
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 3
    object GroupBox2: TGroupBox
      Left = 0
      Top = 64
      Width = 557
      Height = 277
      Align = alTop
      Caption = ' Dados do item: '
      TabOrder = 0
      object PnPedOpen: TPanel
        Left = 2
        Top = 15
        Width = 553
        Height = 260
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel34: TPanel
          Left = 0
          Top = 49
          Width = 553
          Height = 58
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object RGPedGrandeza: TRadioGroup
            Left = 0
            Top = 0
            Width = 553
            Height = 58
            Align = alClient
            Caption = ' Grandeza: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'Indefinido'
              #193'rea m'#178
              #193'rea ft'#178
              'Peso kg')
            TabOrder = 0
            OnClick = RGPedGrandezaClick
          end
        end
        object Panel35: TPanel
          Left = 0
          Top = 0
          Width = 553
          Height = 49
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label16: TLabel
            Left = 92
            Top = 8
            Width = 40
            Height = 13
            Caption = 'Receita:'
          end
          object Label6: TLabel
            Left = 8
            Top = 8
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label1: TLabel
            Left = 476
            Top = 8
            Width = 75
            Height = 13
            Caption = '% Rend. esper.:'
          end
          object EdReceita2: TdmkEditCB
            Left = 92
            Top = 24
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBReceita2
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBReceita2: TdmkDBLookupComboBox
            Left = 151
            Top = 24
            Width = 322
            Height = 21
            KeyField = 'Numero'
            ListField = 'Nome'
            ListSource = DsFormulas
            TabOrder = 2
            dmkEditCB = EdReceita2
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdControle: TdmkEdit
            Left = 8
            Top = 24
            Width = 80
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdRendEsperado: TdmkEdit
            Left = 476
            Top = 24
            Width = 73
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-99'
            ValMax = '50'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdPedQtPdGdzRedefinido
          end
        end
        object Panel29: TPanel
          Left = 0
          Top = 165
          Width = 553
          Height = 95
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Panel30: TPanel
            Left = 133
            Top = 0
            Width = 140
            Height = 95
            Align = alLeft
            BevelOuter = bvNone
            Enabled = False
            TabOrder = 0
            object Label25: TLabel
              Left = 4
              Top = 24
              Width = 36
              Height = 13
              Caption = #193'rea m'#178
            end
            object Label27: TLabel
              Left = 4
              Top = 48
              Width = 37
              Height = 13
              Caption = #193'rea ft'#178':'
            end
            object Label28: TLabel
              Left = 4
              Top = 72
              Width = 42
              Height = 13
              Caption = 'Peso kg:'
            end
            object Label26: TLabel
              Left = 12
              Top = 4
              Width = 93
              Height = 13
              Caption = 'Quantidade pedido:'
            end
            object EdPedPdArM2: TdmkEdit
              Left = 52
              Top = 19
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdPedPdArP2: TdmkEdit
              Left = 52
              Top = 43
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdPedPdPeKg: TdmkEdit
              Left = 52
              Top = 67
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object Panel36: TPanel
            Left = 273
            Top = 0
            Width = 140
            Height = 95
            Align = alLeft
            BevelOuter = bvNone
            Enabled = False
            TabOrder = 1
            object Label29: TLabel
              Left = 4
              Top = 24
              Width = 36
              Height = 13
              Caption = #193'rea m'#178
            end
            object Label31: TLabel
              Left = 4
              Top = 48
              Width = 37
              Height = 13
              Caption = #193'rea ft'#178':'
            end
            object Label32: TLabel
              Left = 4
              Top = 72
              Width = 42
              Height = 13
              Caption = 'Peso kg:'
            end
            object Label33: TLabel
              Left = 12
              Top = 4
              Width = 107
              Height = 13
              Caption = 'Quantidade produzido:'
            end
            object EdPedPrArM2: TdmkEdit
              Left = 52
              Top = 19
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdPedPrArP2: TdmkEdit
              Left = 52
              Top = 43
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdPedPrPeKg: TdmkEdit
              Left = 52
              Top = 67
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object Panel37: TPanel
            Left = 413
            Top = 0
            Width = 140
            Height = 95
            Align = alLeft
            BevelOuter = bvNone
            Enabled = False
            TabOrder = 2
            object Label30: TLabel
              Left = 12
              Top = 4
              Width = 107
              Height = 13
              Caption = 'Quantidade a produzir:'
            end
            object Label21: TLabel
              Left = 4
              Top = 24
              Width = 36
              Height = 13
              Caption = #193'rea m'#178
            end
            object Label22: TLabel
              Left = 4
              Top = 48
              Width = 37
              Height = 13
              Caption = #193'rea ft'#178':'
            end
            object Label19: TLabel
              Left = 4
              Top = 72
              Width = 42
              Height = 13
              Caption = 'Peso kg:'
            end
            object EdPedAreaM2: TdmkEdit
              Left = 52
              Top = 19
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdPedAreaP2: TdmkEdit
              Left = 52
              Top = 43
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EDPedPesoKg: TdmkEdit
              Left = 52
              Top = 67
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object Panel39: TPanel
            Left = 553
            Top = 0
            Width = 133
            Height = 95
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 3
          end
          object Panel38: TPanel
            Left = 0
            Top = 0
            Width = 133
            Height = 95
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 4
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 107
          Width = 553
          Height = 58
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
          object Panel33: TPanel
            Left = 0
            Top = 0
            Width = 93
            Height = 58
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Label20: TLabel
              Left = 8
              Top = 16
              Width = 36
              Height = 13
              Caption = 'Culatra:'
            end
            object Label17: TLabel
              Left = 48
              Top = 16
              Width = 40
              Height = 13
              Caption = 'Cabe'#231'a:'
            end
            object Label18: TLabel
              Left = 8
              Top = 0
              Width = 78
              Height = 13
              Caption = 'Rebaixe (linhas):'
            end
            object EdPedLinhasCul: TdmkEdit
              Left = 8
              Top = 32
              Width = 37
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnRedefinido = EdPedLinhasCulRedefinido
            end
            object EdPedLinhasCab: TdmkEdit
              Left = 48
              Top = 32
              Width = 41
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnRedefinido = EdPedLinhasCabRedefinido
            end
          end
          object Panel28: TPanel
            Left = 93
            Top = 0
            Width = 460
            Height = 58
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object LaQtPedido: TLabel
              Left = 0
              Top = 16
              Width = 33
              Height = 13
              Caption = '?????:'
            end
            object Label23: TLabel
              Left = 4
              Top = 0
              Width = 52
              Height = 13
              Caption = 'Qt. pedido:'
            end
            object Label24: TLabel
              Left = 108
              Top = 0
              Width = 63
              Height = 13
              Caption = 'Qt produzido:'
            end
            object LaQtProduzido: TLabel
              Left = 100
              Top = 16
              Width = 33
              Height = 13
              Caption = '?????:'
            end
            object Label1000: TLabel
              Left = 228
              Top = 0
              Width = 66
              Height = 13
              Caption = 'Qt. a produzir:'
            end
            object LaQtAProduzir: TLabel
              Left = 228
              Top = 16
              Width = 33
              Height = 13
              Caption = '?????:'
            end
            object SbPedQtPrGdz: TSpeedButton
              Left = 204
              Top = 31
              Width = 23
              Height = 22
              Caption = '?'
            end
            object Label8: TLabel
              Left = 332
              Top = 0
              Width = 53
              Height = 13
              Caption = 'Uso kg/m'#178':'
            end
            object Label9: TLabel
              Left = 332
              Top = 16
              Width = 102
              Height = 13
              Caption = '% Margem retrabalho:'
            end
            object EdPedQtPdGdz: TdmkEdit
              Left = 0
              Top = 32
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnRedefinido = EdPedQtPdGdzRedefinido
            end
            object EdPedQtPrGdz: TdmkEdit
              Left = 104
              Top = 32
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnRedefinido = EdPedQtPrGdzRedefinido
            end
            object EdPedQtdeGdz: TdmkEdit
              Left = 228
              Top = 32
              Width = 100
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnRedefinido = EdPedQtdeGdzRedefinido
            end
            object EdMargemDBI: TdmkEdit
              Left = 332
              Top = 32
              Width = 100
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnRedefinido = EdPedQtdeGdzRedefinido
            end
          end
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 557
      Height = 64
      Align = alTop
      Caption = ' Dados do cabe'#231'alho:'
      Enabled = False
      TabOrder = 1
      object Label5: TLabel
        Left = 12
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label3: TLabel
        Left = 72
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 36
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TDBEdit
        Left = 72
        Top = 36
        Width = 477
        Height = 21
        TabStop = False
        Color = clWhite
        DataField = 'Nome'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 341
      Width = 557
      Height = 24
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object CkContinuar: TCheckBox
        Left = 16
        Top = 4
        Width = 117
        Height = 17
        Caption = 'Continuar inserindo.'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
    end
  end
  object Panel5: TPanel
    Left = 557
    Top = 48
    Width = 569
    Height = 373
    Align = alClient
    Caption = 'Panel5'
    TabOrder = 4
    object DBGAreas: TdmkDBGridZTO
      Left = 1
      Top = 1
      Width = 567
      Height = 320
      Align = alClient
      DataSource = DsAreas
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
    end
    object Panel6: TPanel
      Left = 1
      Top = 321
      Width = 567
      Height = 51
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 40
        Top = 4
        Width = 117
        Height = 13
        Caption = #193'rea m'#178' com rendimento:'
        FocusControl = DBEdArM2ComRend
      end
      object Label4: TLabel
        Left = 320
        Top = 4
        Width = 94
        Height = 13
        Caption = 'Kg com rendimento:'
        FocusControl = DBEdPesoComRend
      end
      object Label7: TLabel
        Left = 180
        Top = 4
        Width = 115
        Height = 13
        Caption = #193'rea ft'#178' com rendimento:'
        FocusControl = DBEdArP2ComRend
      end
      object DBEdArM2ComRend: TDBEdit
        Left = 40
        Top = 20
        Width = 134
        Height = 21
        DataField = 'ArM2ComRend'
        DataSource = DsSumAreas
        TabOrder = 0
      end
      object DBEdPesoComRend: TDBEdit
        Left = 320
        Top = 20
        Width = 134
        Height = 21
        DataField = 'PesoComRend'
        DataSource = DsSumAreas
        TabOrder = 1
      end
      object DBEdArP2ComRend: TDBEdit
        Left = 180
        Top = 20
        Width = 134
        Height = 21
        DataField = 'ArP2ComRend'
        DataSource = DsSumAreas
        TabOrder = 2
      end
    end
  end
  object QrFormulas: TMySQLQuery
    SQL.Strings = (
      'SELECT Numero, Nome '
      'FROM formulas'
      'ORDER BY Nome')
    Left = 217
    Top = 61
    object QrFormulasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 217
    Top = 109
  end
  object QrAreas: TMySQLQuery
    SQL.Strings = (
      'SELECT emi.Peso, 110.000 Tipo,  '
      
        'Codigo, CAST(DataEmis + 0 AS DATETIME) Data, "Pesagem" TipoTXT, ' +
        ' '
      'VSMovCod, AreaM2, Qtde Pecas, 999999999.99 AreaPeso  '
      'FROM emit emi '
      'LEFT JOIN pqx pqx ON emi.Codigo=pqx.OrigemCodi '
      'WHERE EmitGru=1035 '
      'AND pqx.CliOrig=-11 '
      'AND emi.Numero=822 ')
    Left = 680
    Top = 149
    object QrAreasPeso: TFloatField
      FieldName = 'Peso'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrAreasTipo: TFloatField
      FieldName = 'Tipo'
      Required = True
    end
    object QrAreasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrAreasData: TDateTimeField
      FieldName = 'Data'
    end
    object QrAreasTipoTXT: TWideStringField
      FieldName = 'TipoTXT'
      Required = True
      Size = 7
    end
    object QrAreasVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
      Required = True
    end
    object QrAreasAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrAreasPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
      DisplayFormat = '#,###,##0'
    end
    object QrAreasAreaPeso: TFloatField
      FieldName = 'AreaPeso'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrAreasPesoComRend: TFloatField
      FieldName = 'PesoComRend'
      DisplayFormat = '#,###,##0.000'
    end
    object QrAreasArM2ComRend: TFloatField
      FieldName = 'ArM2ComRend'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAreasArP2ComRend: TFloatField
      FieldName = 'ArP2ComRend'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsAreas: TDataSource
    DataSet = QrAreas
    Left = 681
    Top = 197
  end
  object QrSumAreas: TMySQLQuery
    SQL.Strings = (
      'SELECT emi.Peso, 110.000 Tipo,  '
      
        'Codigo, CAST(DataEmis + 0 AS DATETIME) Data, "Pesagem" TipoTXT, ' +
        ' '
      'VSMovCod, AreaM2, Qtde Pecas, 999999999.99 AreaPeso  '
      'FROM emit emi '
      'LEFT JOIN pqx pqx ON emi.Codigo=pqx.OrigemCodi '
      'WHERE EmitGru=1035 '
      'AND pqx.CliOrig=-11 '
      'AND emi.Numero=822 ')
    Left = 768
    Top = 145
    object QrSumAreasArM2ComRend: TFloatField
      FieldName = 'ArM2ComRend'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumAreasArP2ComRend: TFloatField
      FieldName = 'ArP2ComRend'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumAreasPesoComRend: TFloatField
      FieldName = 'PesoComRend'
    end
  end
  object DsSumAreas: TDataSource
    DataSet = QrSumAreas
    Left = 769
    Top = 193
  end
end
