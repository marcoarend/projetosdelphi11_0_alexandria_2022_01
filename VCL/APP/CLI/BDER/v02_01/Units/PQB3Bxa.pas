unit PQB3Bxa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnInternalConsts,
  UnMsgInt, ExtCtrls, UnInternalConsts2, mySQLDbTables, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, UMySQLModule, dmkGeral, dmkLabel, dmkImage, UnDmkEnums,
  UnDmkProcFunc, DmkDAC_PF, Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmPQB3Bxa = class(TForm)
    PainelDados: TPanel;
    LaInsumo: TLabel;
    Label4: TLabel;
    EdQtde: TdmkEdit;
    Label10: TLabel;
    EdPreco: TdmkEdit;
    Label6: TLabel;
    EdVTota: TdmkEdit;
    QrPQ: TmySQLQuery;
    DsPQ: TDataSource;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    LaCliente: TLabel;
    QrCI: TmySQLQuery;
    QrCINome: TWideStringField;
    QrCICodigo: TIntegerField;
    DsCI: TDataSource;
    EdInsumo: TdmkEditCB;
    CBInsumo: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    LaEmpresa: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrSaldo: TMySQLQuery;
    QrSaldoPeso: TFloatField;
    QrSaldoValor: TFloatField;
    QrSaldoCUSTO: TFloatField;
    QrSaldoCI: TIntegerField;
    QrSaldoPQ: TIntegerField;
    DsSaldo: TDataSource;
    DBEdit7: TDBEdit;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    Label19: TLabel;
    Label20: TLabel;
    EdSaldoFut: TdmkEdit;
    Label7: TLabel;
    TPDataX: TdmkEditDateTimePicker;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdQtdeExit(Sender: TObject);
    procedure EdPrecoExit(Sender: TObject);
    procedure EdVTotaExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdInsumoEnter(Sender: TObject);
    procedure EdClienteEnter(Sender: TObject);
    procedure EdInsumoExit(Sender: TObject);
    procedure EdClienteExit(Sender: TObject);
    procedure EdInsumoChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdClienteRedefinido(Sender: TObject);
    procedure EdInsumoRedefinido(Sender: TObject);
    procedure EdQtdeRedefinido(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
  private
    { Private declarations }
    FPQ, FCI, FEmpresa: Integer;
    procedure BuscaPrecoCadastro();
    procedure CalculaOnEdit(Tipo: Integer);
    procedure CalculaSaldoFuturo();
  public
    { Public declarations }
    FBalanco, FControle: Integer;
  end;

var
  FmPQB3Bxa: TFmPQB3Bxa;

implementation

uses UnMyObjects, Module, PQB3, PQx, UnPQ_PF, ModuleGeral;

{$R *.DFM}


procedure TFmPQB3Bxa.CalculaOnEdit(Tipo: Integer);
var
  Qtde, Preco, VTota: Double;
begin
  Qtde := Geral.DMV(EdQtde.Text);
  Preco := Geral.DMV(EdPreco.Text);
  VTota := Geral.DMV(EdVTota.Text);
  //
  if Tipo <> 1 then VTota := Qtde * Preco
  else if Qtde <> 0 then Preco := VTota / Qtde
  else Preco := 0;
  //
  EdQtde.ValueVariant := Qtde;
  EdPreco.ValueVariant := Preco;
  EdVTota.ValueVariant := VTota;
end;

procedure TFmPQB3Bxa.CalculaSaldoFuturo();
var
  CI, PQ, PeriodoBal: Integer;
  Qtde, SaldoFut: Double;
  sData: String;
begin
  CI := Geral.IMV(EdCliente.Text);
  PQ := Geral.IMV(EdInsumo.Text);
  if (CI <> 0) and (PQ <> 0) then
  begin
    PeriodoBal := UnPQx.VerificaBalanco();
    if PeriodoBal = 0 then
      sData  := FormatDateTime(VAR_FORMATDATE, 2)
    else
      sData := dmkPF.PrimeiroDiaAposPeriodo(PeriodoBal, dtSystem); //2?
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSaldo, Dmod.MyDB, [
    // copiado do TUnPQx.AtualizaEstoquePQ(
    'SELECT CliOrig CI, Insumo PQ, ',
    'SUM(Peso) Peso, SUM(Valor) Valor, ',
    'SUM(Valor) / SUM(Peso) CUSTO ',
    'FROM pqx ',
    'WHERE CliOrig=' + Geral.FF0(CI),
    'AND Insumo=' + Geral.FF0(PQ),
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'AND DataX>="' + sData + '"',
    'AND Tipo>=0 ',
    // 2016-02-13
    'AND HowLoad<>2 ',
    '']);
    //
    Qtde := Geral.DMV(EdQtde.Text);
    SaldoFut := QrSaldoPeso.Value - Qtde;
    EdSaldoFut.ValueVariant := SaldoFut;
    //
    // Buscar Pre�o do estoque e n�o do cadastro!
    if ImgTipo.SQLType = stIns then
      EdPreco.ValueVariant := QrSaldoCUSTO.Value;
  end else
  begin
    EdSaldoFut.ValueVariant := 0.000;
    if ImgTipo.SQLType = stIns then
      EdPreco.ValueVariant := 0.00;
  end;
end;

procedure TFmPQB3Bxa.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQB3Bxa.BuscaPrecoCadastro();
begin
  // Buscar pre�o do estoque e n�o do cadastro!
  EXIT;
  //
  if ImgTipo.SQLType = stIns then
  begin
    FPQ := EdInsumo.ValueVariant;
    FCI := EdCliente.ValueVariant;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT CustoPadrao FROM pqcli');
    Dmod.QrAux.SQL.Add('WHERE PQ=:P0');
    Dmod.QrAux.SQL.Add('AND CI=:P1');
    Dmod.QrAux.Params[00].AsInteger := FPQ;
    Dmod.QrAux.Params[01].AsInteger := FCI;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    EdPreco.ValueVariant := Dmod.QrAux.FieldByName('CustoPadrao').AsFloat;
    CalculaOnEdit(0);
    //
  end;
end;

procedure TFmPQB3Bxa.BtConfirmaClick(Sender: TObject);
const
  Unitario = True;
var
  Controle: Integer;
  KG, RS, SF: Double;
  //
  DataX, DtCorrApo: String;
  OriCodi, OriCtrl, OriTipo, CliOrig, CliDest, Insumo, CliInt, Empresa: Integer;
  Peso, Valor: Double;
var
  Cursor : TCursor;
  Preco, Qtde, VTota, SaldoFut: Double;
  Cliente, Codigo: Integer;
  SQLType: TSQLType;
begin
  //A baixa dobrou o estoqu2! ver porque!
  //
  SQLType := ImgTipo.SQLType;
  DtCorrApo := Geral.FDT(0, 1);
  //DataX := dmkPF.PrimeiroDiaDoPeriodo(FmPQB3.QrBalancosPeriodo.Value, dtSystem);
  DataX := Geral.FDT(TPDataX.Date, 1);
  Qtde := Geral.DMV(EdQtde.Text);
  VTota := Geral.DMV(EdVTota.Text);
  //
  Cliente := Geral.IMV(EdCliente.Text);
  Preco := Geral.DMV(EdPreco.Text);
  Insumo := Geral.IMV(EdInsumo.Text);
  //
  SaldoFut := EdSaldoFut.ValueVariant;
  //
  if MyObjects.FIC((Preco <= 0) and (Cliente < 1), EdPreco,
    'Pre�o n�o pode ser negativo para este cliente interno.') then Exit;
  if MyObjects.FIC((VTota = 0) and (Cliente < 1), EdPreco,
    'Defina o valor.') then Exit;
  if MyObjects.FIC(Insumo = 0, EdInsumo, 'Defina o insumo.') then Exit;
  if MyObjects.FIC(SaldoFut < 0, EdQtde, 'Saldo insuficiente!') then Exit;
  if MyObjects.FIC(Qtde <= 0, EdQtde, 'Quantidade inv�lida!') then Exit;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    OriCodi := FBalanco;
    Codigo  := FBalanco;
    OriTipo := VAR_FATID_0120;
    CliOrig := Cliente;
    CliDest := Cliente;
    CLiInt  := Cliente;
    //Insumo  := Insumo;
    Peso    := Qtde;
    Valor   := VTota;
    //
    Controle := UMyMod.BPGS1I32('pqbbxa', 'Controle', '', '', tsPos, SQLType, FControle);
    OriCtrl := Controle;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqbbxa', False, [
    'Codigo', 'Empresa', 'CliInt',
    'Insumo', 'Peso', 'Valor',
    'DtCorrApo'], [
    'Controle'], [
    Codigo, Empresa, CliInt,
    Insumo, Peso, Valor,
    DtCorrApo], [
    Controle], True) then
    begin
      if ImgTipo.SQLType = stUpd then
        PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, True, Empresa);
      //
      PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, -Peso, -Valor, OriCodi,
        OriCtrl, OriTipo, Unitario, DtCorrApo, Empresa);
    end;
  except
    Screen.Cursor := Cursor;
    Exit;
  end;
  if CkContinuar.Checked then
  begin
    ImgTipo.SQLType        := stIns;
    FPQ                    := 0;
    FCI                    := 0;
    LaInsumo.Enabled       := True;
    EdInsumo.Enabled       := True;
    CBInsumo.Enabled       := True;
    EdInsumo.ValueVariant  := 0;
    CBInsumo.KeyValue      := 0;
    EdPreco.ValueVariant   := 0;
    EdQtde.ValueVariant   := 0;
    //
    CalculaOnEdit(0);
    EdInsumo.SetFocus;
    //FmPQB3.SomaBalanco();
    FmPQB3.ReopenPQBBxa(Controle);
    Screen.Cursor := crDefault;
  end else Close;
end;

procedure TFmPQB3Bxa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  CalculaOnEdit(1);
  EdPreco.SetFocus;
  EdQtde.SetFocus;
  if ImgTipo.SQLType = stIns then
  begin
    if EdCliente.ValueVariant = 0 then
      EdCliente.SetFocus
    else
      EdInsumo.SetFocus;
  end;
end;

procedure TFmPQB3Bxa.EdClienteChange(Sender: TObject);
begin
  if not EdCliente.Focused then
    BuscaPrecoCadastro();
end;

procedure TFmPQB3Bxa.EdClienteEnter(Sender: TObject);
begin
  FCI := EdCliente.ValueVariant;
end;

procedure TFmPQB3Bxa.EdClienteExit(Sender: TObject);
begin
  if FCI <> EdCliente.ValueVariant then
    BuscaPrecoCadastro();
end;

procedure TFmPQB3Bxa.EdClienteRedefinido(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmPQB3Bxa.EdEmpresaRedefinido(Sender: TObject);
var
  Filial: Integer;
begin
  Filial := EdEmpresa.ValueVariant;
  FEmpresa := DModG.ObtemEntidadeDeFilial(Filial);
end;

procedure TFmPQB3Bxa.EdInsumoChange(Sender: TObject);
begin
  if not EdInsumo.Focused then
    BuscaPrecoCadastro();
end;

procedure TFmPQB3Bxa.EdInsumoEnter(Sender: TObject);
begin
  FPQ := EdInsumo.ValueVariant;
end;

procedure TFmPQB3Bxa.EdInsumoExit(Sender: TObject);
begin
  if FPQ <> EdInsumo.ValueVariant then
    BuscaPrecoCadastro();
end;

procedure TFmPQB3Bxa.EdInsumoRedefinido(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmPQB3Bxa.EdQtdeExit(Sender: TObject);
begin
  CalculaOnEdit(0);
end;

procedure TFmPQB3Bxa.EdQtdeRedefinido(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmPQB3Bxa.EdPrecoExit(Sender: TObject);
begin
  CalculaOnEdit(0);
end;

procedure TFmPQB3Bxa.EdVTotaExit(Sender: TObject);
begin
  CalculaOnEdit(1);
end;

procedure TFmPQB3Bxa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQB3Bxa.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPDataX.Date := DModG.ObtemAgora();
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
end;

end.
