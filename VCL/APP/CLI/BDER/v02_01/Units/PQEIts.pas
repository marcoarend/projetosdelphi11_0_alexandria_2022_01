unit PQEIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnInternalConsts,
  UnMsgInt, ExtCtrls, UnInternalConsts2, mySQLDbTables, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, ComCtrls, dmkMemo, dmkRadioGroup,
  dmkLabel, dmkImage, UnDmkEnums, DmkDAC_PF, dmkEditDateTimePicker, UnPQ_PF,
  dmkDBEdit, Vcl.Menus, SPED_Listas, UnGrl_Consts;

type
  TFmPQEIts = class(TForm)
    QrPQ1: TmySQLQuery;
    DsPQ1: TDataSource;
    QrLocalizaPQ: TmySQLQuery;
    Panel1: TPanel;
    Painel1: TPanel;
    PainelDados: TPanel;
    EdInsumo: TdmkEditCB;
    CBInsumo: TdmkDBLookupComboBox;
    SBProduto: TSpeedButton;
    QrPQ1NOMEPQ: TWideStringField;
    QrPQ1PQ: TIntegerField;
    Panel3: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label2: TLabel;
    EdConta: TdmkEdit;
    Label1: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    Label9: TLabel;
    Label11: TLabel;
    QrPQCli: TmySQLQuery;
    QrPQCliCustoPadrao: TFloatField;
    QrPQCliMoedaPadrao: TIntegerField;
    QrPQCliNOMEMOEDAPADRAO: TWideStringField;
    DsPQCli: TDataSource;
    QrGraGru1: TmySQLQuery;
    QrGraGru1ICMSRec_pAliq: TFloatField;
    QrGraGru1PISRec_pAliq: TFloatField;
    QrGraGru1COFINSRec_pAliq: TFloatField;
    PMProduto: TPopupMenu;
    Cadastrodoproduto1: TMenuItem;
    Dadosfiscais1: TMenuItem;
    SpeedButton1: TSpeedButton;
    Panel6: TPanel;
    RGMoeda: TRadioGroup;
    LaCotacao: TLabel;
    EdCotacao: TdmkEdit;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    CkAmostra: TCheckBox;
    Panel7: TPanel;
    GBRecuImpost: TGroupBox;
    Panel8: TPanel;
    Label14: TLabel;
    EdVolumes: TdmkEdit;
    Label15: TLabel;
    EdPesoVL: TdmkEdit;
    Label19: TLabel;
    EdTotalkgLiq: TdmkEdit;
    Label3: TLabel;
    EdPesoVB: TdmkEdit;
    Label6: TLabel;
    EdTotalkgBruto: TdmkEdit;
    Label10: TLabel;
    Label5: TLabel;
    EdVlrItem: TdmkEdit;
    EdValorkg: TdmkEdit;
    Label4: TLabel;
    EdIPI_pIPI: TdmkEdit;
    Label7: TLabel;
    EdIPI_vIPI: TdmkEdit;
    Label8: TLabel;
    EdCustoItem: TdmkEdit;
    Label12: TLabel;
    EdxLote: TdmkEdit;
    QrCFOP: TMySQLQuery;
    QrCFOPNome: TWideStringField;
    DsCFOP: TDataSource;
    QrCFOPCodigo: TLargeintField;
    EdCFOP: TdmkEditCB;
    Label66: TLabel;
    CBCFOP: TdmkDBLookupComboBox;
    SbCFOP: TSpeedButton;
    Edprod_vOutro: TdmkEdit;
    Label17: TLabel;
    Edprod_vProd: TdmkEdit;
    Label18: TLabel;
    Edprod_vDesc: TdmkEdit;
    Label21: TLabel;
    Edprod_vSeg: TdmkEdit;
    Label22: TLabel;
    Edprod_vFrete: TdmkEdit;
    Label23: TLabel;
    QrPQ1SIGLA: TWideStringField;
    Label25: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    Panel2: TPanel;
    Label13: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label16: TLabel;
    Label476: TLabel;
    Label477: TLabel;
    Label478: TLabel;
    Label480: TLabel;
    Label20: TLabel;
    EdRpICMS: TdmkEdit;
    EdRvICMS: TdmkEdit;
    EdRpPIS: TdmkEdit;
    EdRvPIS: TdmkEdit;
    EdRpCOFINS: TdmkEdit;
    EdRvCOFINS: TdmkEdit;
    EdRpIPI: TdmkEdit;
    EdRvIPI: TdmkEdit;
    EdICMS_CST: TdmkEdit;
    EdUCTextoB: TdmkEdit;
    EdIPI_CST: TdmkEdit;
    EdUCTextoIPI_CST: TdmkEdit;
    EdPIS_CST: TdmkEdit;
    EdUCTextoPIS_CST: TdmkEdit;
    EdCOFINS_CST: TdmkEdit;
    EdUCTextoCOFINS_CST: TdmkEdit;
    EdICMS_vBC: TdmkEdit;
    EdPIS_vBC: TdmkEdit;
    EdIPI_vBC: TdmkEdit;
    EdCOFINS_vBC: TdmkEdit;
    EdICMS_vBCST: TdmkEdit;
    EdRpICMSST: TdmkEdit;
    EdRvICMSST: TdmkEdit;
    EdICMS_Orig: TdmkEdit;
    GroupBox2: TGroupBox;
    Panel9: TPanel;
    Label27: TLabel;
    EdAjusteVL_BC_ICMS: TdmkEdit;
    EdAjusteALIQ_ICMS: TdmkEdit;
    EdAjusteVL_ICMS: TdmkEdit;
    EdAjusteVL_OUTROS: TdmkEdit;
    Label28: TLabel;
    EdAjusteVL_OPR: TdmkEdit;
    Label31: TLabel;
    Label32: TLabel;
    EdAjusteVL_RED_BC: TdmkEdit;
    Label29: TLabel;
    Label30: TLabel;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdVolumesChange(Sender: TObject);
    procedure EdPesoVBChange(Sender: TObject);
    procedure EdPesoVLChange(Sender: TObject);
    procedure EdVlrItemChange(Sender: TObject);
    procedure EdVolumesExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdTotalkgBrutoChange(Sender: TObject);
    procedure EdTotalkgLiqChange(Sender: TObject);
    procedure EdTotalkgLiqExit(Sender: TObject);
    procedure EdInsumoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBInsumoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBProdutoClick(Sender: TObject);
    procedure EdPesoVLExit(Sender: TObject);
    procedure EdIPI_pIPIChange(Sender: TObject);
    procedure EdIPI_vIPIChange(Sender: TObject);
    procedure CkDtCorrApoClick(Sender: TObject);
    procedure EdInsumoChange(Sender: TObject);
    procedure QrPQCliCalcFields(DataSet: TDataSet);
    procedure RGMoedaClick(Sender: TObject);
    procedure EdRpICMSChange(Sender: TObject);
    procedure EdRpPISChange(Sender: TObject);
    procedure EdRpCOFINSChange(Sender: TObject);
    procedure Cadastrodoproduto1Click(Sender: TObject);
    procedure Dadosfiscais1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SbCFOPClick(Sender: TObject);
    procedure EdICMS_CSTChange(Sender: TObject);
    procedure EdICMS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdIPI_CSTChange(Sender: TObject);
    procedure EdPIS_CSTChange(Sender: TObject);
    procedure EdCOFINS_CSTChange(Sender: TObject);
    procedure EdIPI_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPIS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCOFINS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdICMS_vBCChange(Sender: TObject);
    procedure EdIPI_vBCChange(Sender: TObject);
    procedure EdPIS_vBCChange(Sender: TObject);
    procedure EdCOFINS_vBCChange(Sender: TObject);
    procedure EdRpIPIChange(Sender: TObject);
    procedure EdUCTextoIPI_CSTRedefinido(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FVol, FkgU, FkgT: Double;
    FQuaisInsumosMostra: TPartOuTodo;
    procedure CalculaIPI();
    procedure ReopenPQCli();
    procedure ConfiguraCotacao(Moeda: Integer);
    procedure CalculaRetornoImpostos(Qual: TRetornImpost);
    procedure ConfiguraDados(Codigo: Integer);
    procedure ConfiguraImpostos(Insumo: Integer);
  public
    { Public declarations }
    FEmpresa, FSqLinked, FTES_ICMS, FTES_IPI, FTES_PIS, FTES_COFINS,
    FEFD_II_C195, FRegrFiscal: Integer;
    FData: TDateTime;
    //
    procedure CalculaOnEdit;
    //procedure MostraTodosPQs();
    procedure Pesos(Index: Integer);
    procedure ReopenPQ(Quais: TPartOuTodo);
    procedure ReopenCFOP();
  end;

var
  FmPQEIts: TFmPQEIts;

implementation

uses UnMyObjects, PQE, Module, Principal, BlueDermConsts, UMySQLModule,
  UnAppPF, UnMoedas, UnGrade_Jan, ModProd, GetValor, UnFinanceiro,
  ModuleGeral, ModuleNFe_0000, UnSPED_PF, UnMySQLCuringa;

{$R *.DFM}

const
  FEFDInnNFSMainFatID = VAR_FATID_0010;
  CO_MovimCod_ZERO    = 0;


procedure TFmPQEIts.CalculaOnEdit();
var
  Volumes, TotalkgLiq, PesoVB, CustoItem, ValorItem, IPI_vIPI,
  Valor, CFin, prod_vProd, prod_vDesc, prod_vFrete, prod_vSeg, prod_vOutro: Double;
begin
  Volumes    := EdVolumes.ValueVariant;
  PesoVB     := EdPesoVB.ValueVariant;
  // ini 2022-04-05 SPED EFD
  prod_vProd := Edprod_vProd.ValueVariant;
  prod_vDesc := Edprod_vDesc.ValueVariant;
  prod_vFrete := Edprod_vFrete.ValueVariant;
  prod_vSeg := Edprod_vSeg.ValueVariant;
  prod_vOutro := Edprod_vOutro.ValueVariant;
  //ValorItem  := EdVlrItem.ValueVariant;
  ValorItem  := prod_vProd + prod_vFrete + prod_vSeg + prod_vOutro - prod_vDesc;
  EdVlrItem.ValueVariant := ValorItem;
  // fim 2022-04-05 SPED EFD
  TotalkgLiq := EdTotalkgLiq.ValueVariant;
  //
  if FmPQE.QrPQEValorNF.Value > 0 then
    CFin := 1 + (FmPQE.QrPQEJuros.Value / FmPQE.QrPQEValorNF.Value)
  else
    CFin := 1;
  //
  IPI_vIPI  := EdIPI_vIPI.ValueVariant;
  CustoItem := (ValorItem + IPI_vIPI) * CFin;
  if TotalkgLiq > 0 then
    Valor := CustoItem / TotalkgLiq
  else
    Valor := 0;
  //

  EdCustoItem.ValueVariant     := CustoItem;
  if FTES_ICMS = 1 then
    EdICMS_vBC.ValueVariant      := ValorItem;
  if FTES_IPI = 1 then
    EdIPI_vBC.ValueVariant       := ValorItem;
  if FTES_PIS = 1 then
    EdPIS_vBC.ValueVariant       := ValorItem;
  if FTES_COFINS = 1 then
    EdCOFINS_vBC.ValueVariant    := ValorItem;
  //
  EdValorkg.ValueVariant       := Valor;
  EdTotalkgBruto.ValueVariant  := Volumes * PesoVB;
end;

procedure TFmPQEIts.CalculaRetornoImpostos(Qual: TRetornImpost);
const
  sProcName = 'FmPQEIts.CalculaRetornoImpostos()';
var
  pICMS, pIPI, pPIS, pCofins, vICMS, vPIS, vCofins, vIPI, ICMS_vBC, IPI_vBC,
  PIS_vBC, COFINS_vBC: Double;
begin
  //VlrItem   := EdVlrItem.ValueVariant;

  ICMS_vBC   := EdICMS_vBC.ValueVariant;
  IPI_vBC    := EdIPI_vBC.ValueVariant;
  PIS_vBC    := EdPIS_vBC.ValueVariant;
  COFINS_vBC := EdCOFINS_vBC.ValueVariant;

  pICMS     := EdRpICMS.ValueVariant;
  pIPI      := EdRpIPI.ValueVariant;
  pPIS      := EdRpPIS.ValueVariant;
  pCofins   := EdRpCofins.ValueVariant;
  vICMS     := Geral.RoundC(ICMS_vBC * pICMS / 100, 2);
  vIPI      := Geral.RoundC(IPI_vBC * pIPI / 100, 2);
  vPIS      := Geral.RoundC(PIS_vBC * pPIS / 100, 2);
  vCofins   := Geral.RoundC(COFINS_vBC * pCOFINS / 100, 2);
  case Qual of
    //retimpostIndef=1,
    //retimpostNenhum: Exit;
    retimpostTodos:
    begin
      EdRvICMS.ValueVariant := vICMS;
      EdRvIPI.ValueVariant := vIPI;
      EdRvPIS.ValueVariant := vPIS;
      EdRvCOFINS.ValueVariant := vCOFINS;
    end;
    retimpostICMS: EdRvICMS.ValueVariant := vICMS;
    retimpostIPI: EdRvIPI.ValueVariant := vIPI;
    retimpostPIS: EdRvPIS.ValueVariant := vPIS;
    retimpostCOFINS: EdRvCOFINS.ValueVariant := vCOFINS;
    //retimpostIPI=7);
    else Geral.MB_Erro('TRetornImpost n�o implementado em ' + sProcName);
  end;
end;

procedure TFmPQEIts.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQEIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSqLinked       := 0;
  //
  FQuaisInsumosMostra := TPartOuTodo.ptParcial;
  FTES_ICMS   := 0;
  FTES_IPI    := 0;
  FTES_PIS    := 0;
  FTES_COFINS := 0;
  //
  CBInsumo.LocF7SQLText.Text := Geral.ATS([
    'SELECT pqf.PQ _Codigo, pq_.Nome _Nome ',
    'FROM pqfor pqf ',
    'LEFT JOIN pq pq_ ON pq_.Codigo=pqf.PQ ',
    'WHERE pq_.Nome LIKE "%$#%"',
    '']);
  //
  ReopenCFOP();
  //
  RGMoeda.Items.Clear;
  RGMoeda.Items.AddStrings(UMoedas.ObtemListaMoedas());
  //TPDtCorrApo.Date  := DModG.ObtemAgora();
end;

procedure TFmPQEIts.BtConfirmaClick(Sender: TObject);
const
  sProcName = 'TFmPQEIts.BtConfirmaClick()';
  Formula = 0;
var
  Cursor : TCursor;
  TotalPeso, TotalCusto, ValorItem, IPI_pIPI, IPI_vIPI, PesoVL, PesoVB,
  Cotacao: Double;
  Controle, Conta, Insumo, Codigo, Volumes, Moeda: Integer;
  prod_CFOP: String;
  //Amostra: String;
  //Atualiza: Boolean;
  //RICMS, RICMSF, , Preco, Valor, Frete, Juros, CFin
  Permite: Boolean;
  //DataCA: TDateTime;
  DtCorrApo: String;
  //
  RpICMS, RpPIS, RpCOFINS, RpIPI, RvICMS, RvPIS, RvCOFINS, RvIPI: Double;
  xLote: String;
  ICMS_Orig, ICMS_CST, IPI_CST, PIS_CST, COFINS_CST: String;
  prod_vProd, prod_vDesc, prod_vFrete, prod_vSeg, prod_vOutro,
  ICMS_vBCST, RpICMSST, RvICMSST,
  ICMS_vBC, IPI_vBC, PIS_vBC, COFINS_vBC: Double;
  //
  UNID, IND_MOV, COD_NAT, IND_APUR, CST_IPI, COD_ENQ, COD_CTA
  (*, DtCorrApo, xLote*): String;
  MovFatID, MovFatNum, MovimCod, Empresa, MovimNiv, MovimTwn, GraGru1, GraGruX,
  CFOP: Integer;
  CST_ICMS, CST_PIS, CST_COFINS: String;
  //prod_vProd, prod_vFrete, prod_vSeg, prod_vOutro,
  QTD, VL_ITEM, VL_DESC, VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST,
  ALIQ_ST, VL_ICMS_ST, VL_BC_IPI, ALIQ_IPI, VL_IPI, VL_BC_PIS, ALIQ_PIS_p,
  QUANT_BC_PIS, ALIQ_PIS_r, VL_PIS, VL_BC_COFINS, ALIQ_COFINS_p,
  QUANT_BC_COFINS, ALIQ_COFINS_r, VL_COFINS, VL_ABAT_NT, Ori_IPIpIPI,
  Ori_IPIvIPI: Double;
  LnkTabName, LnkFldName: String;
  EFD_II_C195: Integer;
  AjusteVL_BC_ICMS, AjusteALIQ_ICMS, AjusteVL_ICMS,
  AjusteVL_OUTROS, AjusteVL_OPR, AjusteVL_RED_BC: Double;
  //
  function InsAltEfdInnNFsIts(): Boolean;
  var
    EfdSQLType: TSQLType;
    EfdInnNFsIts, EfdInnNFsCab, SqLinked, IsLinked, SqLnkID: Integer;
  begin
    Result := False;
    //
    //EfdSQLType      := Abaixo
    MovFatID       := FEFDInnNFSMainFatID;
    MovFatNum      := Codigo;
    MovimCod       := CO_MovimCod_ZERO;
    Empresa        := FEmpresa;
    MovimNiv       := 0;
    MovimTwn       := 0;
    //EfdInnNFsCab   := ; // EfdInnNFsCab.Controle
    DmNFe_0000.ReopenEfdLinkCab(MovFatID, MovFatNum, MovimCod, Empresa);
    if DmNFe_0000.QrEfdLinkCab.RecordCount = 0 then
    begin
      Geral.MB_Erro('N�o foi poss�vel localizar dados fiscais do cabe�alho em '
        + sProcName);
      Exit;
    end;
    EfdInnNFsCab := DmNFe_0000.QrEfdLinkCabControle.Value;
    //Conta          := ;
    IsLinked  := 1; // True
    if ImgTipo.SQLType = stIns then
    begin
      EfdSQLType := stIns;
      SqLinked   := 0; //FSqLinked; // Buscar l� na inclus�o!
      EfdInnNFsIts   := 0; // Buscar!
    end else
    begin
      if FSqLinked > 0 then
      begin
        DmNFe_0000.ReopenEfdLinkIts(MovFatID, MovFatNum, MovimCod, Empresa, FSqLinked);
        if DmNFe_0000.QrEfdLinkCab.RecordCount > 0 then
        begin
          EfdSQLType := stUpd;
          SqLinked     := DmNFe_0000.QrEfdLinkItsSqLinked.Value;
          EfdInnNFsIts := DmNFe_0000.QrEfdLinkItsConta.Value;
        end else
        begin
          EfdSQLType := stIns;
          SqLinked  := 0; // Buscar l� na inclus�o!
          EfdInnNFsIts   := 0; // Buscar!
        end;
      end else
      begin
        EfdSQLType := stIns;
        SqLinked  := 0; // Buscar l� na inclus�o!
        EfdInnNFsIts   := 0; // Buscar!
      end;
    end;
    //
    GraGru1        := Insumo;
    GraGruX        := Dmod.ObtemGraGruXDeGraGru1(GraGru1);
    //prod_vProd     := J� definido acima!
    //prod_vFrete    := J� definido acima!
    //prod_vSeg      := J� definido acima!
    //prod_vOutro    := J� definido acima!
    QTD            := PesoVL;
    UNID           := QrPQ1SIGLA.Value;
    VL_ITEM        := ValorItem;
    VL_DESC        := prod_vDesc;
    IND_MOV        := '0'; // sim
    CST_ICMS       := ICMS_Orig + ICMS_CST;
    CFOP           := Geral.IMV('0' + Geral.SoNumero_TT(prod_CFOP));
    COD_NAT        := Geral.FF0(FRegrFiscal);
    VL_BC_ICMS     := ICMS_vBC;
    ALIQ_ICMS      := RpICMS;
    VL_ICMS        := RvICMS;
    VL_BC_ICMS_ST  := ICMS_vBCST;
    ALIQ_ST        := RpICMSST;
    VL_ICMS_ST     := RvICMSST;
    IND_APUR       := '';
    CST_IPI        := IPI_CST;
    COD_ENQ        := '';
    VL_BC_IPI      := IPI_vBC;
    ALIQ_IPI       := RpIPI;
    VL_IPI         := RvIPI;
    CST_PIS        := PIS_CST;
    VL_BC_PIS      := PIS_vBC;
    ALIQ_PIS_p     := RpPIS;
    QUANT_BC_PIS   := 0.000;
    ALIQ_PIS_r     := 0.0000;
    VL_PIS         := RvPIS;
    CST_COFINS     := COFINS_CST;
    VL_BC_COFINS   := COFINS_vBC;
    ALIQ_COFINS_p  := RpCOFINS;
    QUANT_BC_COFINS:= 0.000;
    ALIQ_COFINS_r  := 0.0000;
    VL_COFINS      := RvCOFINS;
    COD_CTA        := '';
    VL_ABAT_NT     := 0.00;
    //DtCorrApo      := ; Abaixo
    //xLote          := ; Abaixo
    Ori_IPIpIPI    := IPI_pIPI;
    Ori_IPIvIPI    := IPI_vIPI;
    SqLnkID        := Controle;
    //
    LnkTabName := 'pqeits';
    LnkFldName := 'Controle';
    //
    Result := SPED_PF.InsAltEfdInnNFsIts(EfdSQLType, MovFatID, MovFatNum,
  MovimCod, Empresa, MovimNiv, MovimTwn,
  //Controle, Conta,
    (*Controle*)EfdInnNFsCab, (*Conta*)EfdInnNFsIts,
  SqLinked, GraGru1,
  GraGruX, (*: Integer;*) prod_vProd, prod_vFrete, prod_vSeg, prod_vOutro,
  QTD, (*: Double;*) UNID, (*: String;*) VL_ITEM, VL_DESC, (*: Double;*) IND_MOV, (*: String;*)
  CST_ICMS, CFOP, (*: Integer;*) COD_NAT, (*: String;*) VL_BC_ICMS, ALIQ_ICMS, VL_ICMS,
  VL_BC_ICMS_ST, ALIQ_ST, VL_ICMS_ST, (*: Double;*)
  IND_APUR, (*: String;*)
  CST_IPI,(*:
  Integer;*) COD_ENQ, (*: String;*)
  VL_BC_IPI,
  ALIQ_IPI,
  VL_IPI, (*: Double;*)
  CST_PIS,(*:
  Integer;*) VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS, ALIQ_PIS_r, VL_PIS, CST_COFINS,(*:
  Integer;*) VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS, ALIQ_COFINS_r,
  VL_COFINS, (*: Double;*) COD_CTA, (*: String;*) VL_ABAT_NT, (*: Double;*) DtCorrApo, xLote,(*:
  String;*) Ori_IPIpIPI, Ori_IPIvIPI, (*: Double;*) LnkTabName, LnkFldName, (*: String;*)
  SqLnkID, FEFD_II_C195, AjusteVL_BC_ICMS, AjusteALIQ_ICMS, AjusteVL_ICMS,
  AjusteVL_OUTROS, AjusteVL_OPR, AjusteVL_RED_BC);
    DmNFe_0000.AtualizaTotaisEfdInnNFs(EfdInnNFsCab);

(*
    ou > ? := UMyMod.BPGS1I32('efdinnnfsits', 'Conta', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
    if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'efdinnnfsits', auto_increment?[
    'MovFatID', 'MovFatNum', 'MovimCod',
    'Empresa', 'MovimNiv', 'MovimTwn',
    'Controle', 'GraGru1', 'GraGruX',
    'prod_vProd', 'prod_vFrete', 'prod_vSeg',
    'prod_vOutro', 'QTD', 'UNID',
    'VL_ITEM', 'VL_DESC', 'IND_MOV',
    'CST_ICMS', 'CFOP', 'COD_NAT',
    'VL_BC_ICMS', 'ALIQ_ICMS', 'VL_ICMS',
    'VL_BC_ICMS_ST', 'ALIQ_ST', 'VL_ICMS_ST',
    'IND_APUR', 'CST_IPI', 'COD_ENQ',
    'VL_BC_IPI', 'ALIQ_IPI', 'VL_IPI',
    'CST_PIS', 'VL_BC_PIS', 'ALIQ_PIS_p',
    'QUANT_BC_PIS', 'ALIQ_PIS_r', 'VL_PIS',
    'CST_COFINS', 'VL_BC_COFINS', 'ALIQ_COFINS_p',
    'QUANT_BC_COFINS', 'ALIQ_COFINS_r', 'VL_COFINS',
    'COD_CTA', 'VL_ABAT_NT', 'DtCorrApo',
    'xLote', 'Ori_IPIpIPI', 'Ori_IPIvIPI'], [
    'Conta'], [
    MovFatID, MovFatNum, MovimCod,
    Empresa, MovimNiv, MovimTwn,
    Controle, GraGru1, GraGruX,
    prod_vProd, prod_vFrete, prod_vSeg,
    prod_vOutro, QTD, UNID,
    VL_ITEM, VL_DESC, IND_MOV,
    CST_ICMS, CFOP, COD_NAT,
    VL_BC_ICMS, ALIQ_ICMS, VL_ICMS,
    VL_BC_ICMS_ST, ALIQ_ST, VL_ICMS_ST,
    IND_APUR, CST_IPI, COD_ENQ,
    VL_BC_IPI, ALIQ_IPI, VL_IPI,
    CST_PIS, VL_BC_PIS, ALIQ_PIS_p,
    QUANT_BC_PIS, ALIQ_PIS_r, VL_PIS,
    CST_COFINS, VL_BC_COFINS, ALIQ_COFINS_p,
    QUANT_BC_COFINS, ALIQ_COFINS_r, VL_COFINS,
    COD_CTA, VL_ABAT_NT, DtCorrApo,
    xLote, Ori_IPIpIPI, Ori_IPIvIPI], [
    Conta], UserDataAlterweb?, IGNORE?
*)
  end;
begin
  Insumo := CBInsumo.KeyValue;
  //
  if MyObjects.FIC(Insumo = 0, EdInsumo, 'Produto n�o definido!') then Exit;
  //
  if PQ_PF.InsumosSemCadastroParaClienteInterno(FmPQE.QrPQECI.Value, FEmpresa,
    Insumo, Formula, TTabPqNaoCad.tpncPQEIts, False) then
  begin
    if Geral.MB_Pergunta('Deseja incluir o cliente interno no cadastro do produto?') = ID_YES then
        AppPF.CadastroInsumo(0, '', Insumo, tsfpqCI);
    //
    //UnDmkDAC_PF.AbreQueryApenas(QrPQ1);
    ReopenPQ(ptIndef);
    //
    Exit;
  end;
  //
  Cursor        := Screen.Cursor;
  //Screen.Cursor := crHourglass;
  try
    Codigo := FmPQE.QrPQECodigo.Value;
    Conta  := EdConta.ValueVariant;
    //
    (*if CkDtCorrApo.Checked then
      DataCA := Int(TPDtCorrApo.Date)
    else
      DataCA := 0;*)
    //
    Insumo     := EdInsumo.ValueVariant;
    Volumes    := EdVolumes.ValueVariant;
    PesoVL     := EdPesoVL.ValueVariant;
    PesoVB     := EdPesoVB.ValueVariant;
    ValorItem  := EdVlrItem.ValueVariant;
    IPI_pIPI   := EdIPI_pIPI.ValueVariant;
    IPI_vIPI   := EdIPI_vIPI.ValueVariant;
    TotalCusto := EdCustoItem.ValueVariant;
    TotalPeso  := Volumes * PesoVL;
    //DtCorrApo  := Geral.FDT(DataCA, 1);
    DtCorrApo  := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
    Moeda      := RGMoeda.ItemIndex;

    RpICMS     := EdRpICMS.ValueVariant;
    RpPIS      := EdRpPIS.ValueVariant;
    RpCOFINS   := EdRpCOFINS.ValueVariant;
    RpIPI      := EdRpIPI.ValueVariant;

    RvICMS     := EdRvICMS.ValueVariant;
    RvPIS      := EdRvPIS.ValueVariant;
    RvCOFINS   := EdRvCOFINS.ValueVariant;
    RvIPI      := EdRvIPI.ValueVariant;

    xLote      := EdxLote.ValueVariant;

    ICMS_Orig   := EdICMS_Orig.Text;
    ICMS_CST    := EdICMS_CST.Text;
    IPI_CST     := EdIPI_CST.ValueVariant;
    PIS_CST     := EdPIS_CST.ValueVariant;
    COFINS_CST  := EdCOFINS_CST.ValueVariant;

    prod_vProd  := Edprod_vProd.ValueVariant;
    prod_vDesc  := Edprod_vDesc.ValueVariant;
    prod_vFrete := Edprod_vFrete.ValueVariant;
    prod_vSeg   := Edprod_vSeg.ValueVariant;
    prod_vOutro := Edprod_vOutro.ValueVariant;

    ICMS_vBC   := EdICMS_vBC.ValueVariant;
    IPI_vBC    := EdIPI_vBC.ValueVariant;
    PIS_vBC    := EdPIS_vBC.ValueVariant;
    COFINS_vBC := EdCOFINS_vBC.ValueVariant;
    ICMS_vBCST := EdICMS_vBCST.ValueVariant;
    RpICMSST   := EdRpICMSST.ValueVariant;
    RvICMSST   := EdRvICMSST.ValueVariant;
    //
    AjusteVL_BC_ICMS := EdAjusteVL_BC_ICMS.ValueVariant;
    AjusteALIQ_ICMS  := EdAjusteALIQ_ICMS.ValueVariant;
    AjusteVL_ICMS    := EdAjusteVL_ICMS.ValueVariant;
    AjusteVL_OUTROS  := EdAjusteVL_OUTROS.ValueVariant;
    AjusteVL_OPR     := EdAjusteVL_OPR.ValueVariant;
    AjusteVL_RED_BC  := EdAjusteVL_RED_BC.ValueVariant;
    //
    if Moeda <> QrPQCliMoedaPadrao.Value then
    begin
      if Geral.MB_Pergunta('Voc� selecionou uma moeda diferente da informada no cadastro do produto!' +
        sLineBreak + 'Ao confirmar, a moeda ser� atualizada no cadastro do produto!' +
        sLineBreak + 'Deseja continuar?') <> ID_YES
      then
        Exit;
    end;
    //
    if Moeda <> 0 then
    begin
      Cotacao := EdCotacao.ValueVariant;
      //
      if MyObjects.FIC(Cotacao = 0, EdCotacao, 'Informe a cota��o para a moeda selecionada!') then Exit;
    end else
      Cotacao := 0;
    //
    Permite := (ValorItem > 0) or (FmPQE.QrPQECI.Value > 0); // >> Cliente interno diferente da empresa
    //
    if MyObjects.FIC(Insumo=0, EdInsumo, 'Informe o insumo!') then Exit;
    if MyObjects.FIC(not Permite, EdVlrItem, 'Defina o valor do item!') then Exit;
    if MyObjects.FIC(Volumes <= 0, EdVolumes, 'Defina os volumes do item!') then Exit;
    if MyObjects.FIC((EdVolumes.ValueVariant * 1000) <> (Volumes * 1000),
      EdVolumes, 'Volumes devem ser um n�mero inteiro!') then Exit;
    //if MyObjects.FIC(Edprod_CFOP.ValueVariant = 0, Edprod_CFOP, 'Informe o CFOP!') then Exit;
    if MyObjects.FIC(EdCFOP.ValueVariant = 0, EdCFOP, 'Informe o CFOP!') then Exit;
    //
    //prod_CFOP := FormatFloat('0000', Edprod_CFOP.ValueVariant);
    prod_CFOP := FormatFloat('0000', EdCFOP.ValueVariant);

    if DModG.ExigePisCofins() then
    begin
       if EdRvPIS.ValueVariant >= 0.01 then
         if MyObjects.FIC(Length(EdPIS_CST.Text) <> 2, EdPIS_CST,
         'Informe o CST do PIS com dois d�gitos!') then Exit;
       //
       if EdRvCOFINS.ValueVariant >= 0.01 then
         if MyObjects.FIC(Length(EdCOFINS_CST.Text) <> 2, EdCOFINS_CST,
         'Informe o CST da COFINS com dois d�gitos!') then Exit;
    end;
    //
    if ImgTipo.SQLType = stIns then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE pqeits SET Conta=Conta+1 ',
        'WHERE Conta>=' + Geral.FF0(Conta),
        'AND Codigo=' + Geral.FF0(Codigo),
        '']);
      //
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'PQEIts','PQEIts','Controle');
      //
      //
      FmPQE.FControle := Controle;
    end else
    begin
      Controle := FmPQE.QrPQEItsControle.Value;
      //
      //
      FmPQE.FControle := Controle;
    end;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pqeits', False, [
      'Insumo', 'Volumes', 'PesoVB',
      'PesoVL', 'ValorItem', 'IPI_pIPI',
      'IPI_vIPI', 'TotalCusto', 'TotalPeso',
      'prod_CFOP', 'Codigo', 'Conta',
      'DtCorrApo', 'Moeda', 'Cotacao',
      'RpICMS', 'RpPIS', 'RpCOFINS',
      'RvICMS', 'RvPIS', 'RvCOFINS',
      'RpIPI', 'RvIPI', 'xLote',
      'ICMS_Orig', 'ICMS_CST', 'IPI_CST', 'PIS_CST', 'COFINS_CST',
      'prod_vProd', 'prod_vDesc', 'prod_vSeg', 'prod_vOutro',
      'ICMS_vBC', 'IPI_vBC', 'PIS_vBC', 'COFINS_vBC',
      'ICMS_vBCST', 'RpICMSST', 'RvICMSST',
      'AjusteVL_BC_ICMS', 'AjusteALIQ_ICMS', 'AjusteVL_ICMS',
      'AjusteVL_OUTROS', 'AjusteVL_OPR', 'AjusteVL_RED_BC'

      ], [
      'Controle'], [
      Insumo, Volumes, PesoVB,
      PesoVL, ValorItem, IPI_pIPI,
      IPI_vIPI, TotalCusto, TotalPeso,
      prod_CFOP, Codigo, Conta,
      DtCorrApo, Moeda, Cotacao,
      RpICMS, RpPIS, RpCOFINS,
      RvICMS, RvPIS, RvCOFINS,
      RpIPI, RvIPI, xLote,
      ICMS_Orig, ICMS_CST, IPI_CST, PIS_CST, COFINS_CST,
      prod_vProd, prod_vDesc, prod_vSeg, prod_vOutro,
      ICMS_vBC, IPI_vBC, PIS_vBC, COFINS_vBC,
      ICMS_vBCST, RpICMSST, RvICMSST,
      AjusteVL_BC_ICMS, AjusteALIQ_ICMS, AjusteVL_ICMS,
      AjusteVL_OUTROS, AjusteVL_OPR, AjusteVL_RED_BC
      ], [
      FmPQE.FControle], True) then
    begin
      if DModG.QrCtrlGeralUsarEntraFiscal.Value = 1 then
      begin
        if not InsAltEfdInnNFsIts() then
        begin
          Screen.Cursor := crDefault;
          Exit;
        end;
      end;
      Screen.Cursor := crDefault;
      if ImgTipo.SQLType = stIns then
      begin
        FmPQE.ReopenPQEIts(FmPQE.QrPQECodigo.Value);
        ReopenPQ(ptParcial);
        //
        EdInsumo.ValueVariant      := 0;
        CBInsumo.KeyValue          := NULL;
        EdPesoVB.ValueVariant      := 0;
        EdPesoVL.ValueVariant      := 0;
        EdTotalkgLiq.ValueVariant  := 0;
        EdVolumes.ValueVariant     := 0;
        EdIPI_pIPI.ValueVariant    := 0;
        EdIPI_vIPI.ValueVariant    := 0;
        EdVlrItem.ValueVariant     := 0;
        EdRpICMS.ValueVariant      := 0;
        EdRpPIS.ValueVariant       := 0;
        EdRpCOFINS.ValueVariant    := 0;
        EdRpIPI.ValueVariant       := 0;
        EdRvICMS.ValueVariant      := 0;
        EdRvPIS.ValueVariant       := 0;
        EdRvCOFINS.ValueVariant    := 0;
        EdRvIPI.ValueVariant       := 0;
        EdxLote.ValueVariant       := '';
        Edprod_vProd.ValueVariant  := 0;
        Edprod_vDesc.ValueVariant  := 0;
        EdICMS_vBC.ValueVariant    := 0;
        EdIPI_vBC.ValueVariant     := 0;
        EdPIS_vBC.ValueVariant     := 0;
        EdCOFINS_vBC.ValueVariant  := 0;
        //
        EdUCTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(EdIPI_CST.Text);
        //
        EdConta.ValMax       := EdConta.ValueVariant + 1;
        EdConta.ValueVariant := EdConta.ValMax;
        //
        EdInsumo.SetFocus;
        //
        //
      end else Close;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmPQEIts.EdVolumesChange(Sender: TObject);
begin
  if EdVolumes.Focused then
  begin
    Pesos(2);
    CalculaOnEdit;
  end;
end;

procedure TFmPQEIts.EdIPI_pIPIChange(Sender: TObject);
begin
  CalculaIPI();
end;

procedure TFmPQEIts.EdIPI_vBCChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostCOFINS);
end;

procedure TFmPQEIts.EdIPI_vIPIChange(Sender: TObject);
begin
  CalculaOnEdit();
end;

procedure TFmPQEIts.EdPesoVBChange(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmPQEIts.EdPesoVLChange(Sender: TObject);
begin
  if EdPesoVL.Focused then
  begin
    Pesos(0);
    CalculaOnEdit;
  end;
end;

procedure TFmPQEIts.EdPesoVLExit(Sender: TObject);
begin
  EdPesoVL.ValueVariant := FkgU;
end;

procedure TFmPQEIts.EdRpCOFINSChange(Sender: TObject);
begin
  if (EdRpCOFINS.ValueVariant > 0) and (EdCOFINS_vBC.ValueVariant < 0.01) then
    EdCOFINS_vBC.ValueVariant := EdCustoItem.ValueVariant;
  //
  CalculaRetornoImpostos(TRetornImpost.retimpostCOFINS);
end;

procedure TFmPQEIts.EdRpICMSChange(Sender: TObject);
begin
  if (EdRpICMS.ValueVariant > 0) and (EdICMS_vBC.ValueVariant < 0.01) then
    EdICMS_vBC.ValueVariant := EdCustoItem.ValueVariant;
  //
  CalculaRetornoImpostos(TRetornImpost.retimpostICMS);
end;

procedure TFmPQEIts.EdRpIPIChange(Sender: TObject);
begin
  if (EdRpIPI.ValueVariant > 0) and (EdIPI_vBC.ValueVariant < 0.01) then
    EdIPI_vBC.ValueVariant := EdCustoItem.ValueVariant;
  //
  CalculaRetornoImpostos(TRetornImpost.retimpostIPI);
end;

procedure TFmPQEIts.EdRpPISChange(Sender: TObject);
begin
  if (EdRpPIS.ValueVariant > 0) and (EdPIS_vBC.ValueVariant < 0.01) then
    EdPIS_vBC.ValueVariant := EdCustoItem.ValueVariant;
  //
  CalculaRetornoImpostos(TRetornImpost.retimpostPIS);
end;

procedure TFmPQEIts.EdVlrItemChange(Sender: TObject);
begin
  CalculaIPI();
  CalculaOnEdit;
  CalculaRetornoImpostos(TRetornImpost.retimpostTodos);
end;

procedure TFmPQEIts.EdVolumesExit(Sender: TObject);
begin
  EdVolumes.ValueVariant := FVol;
end;

procedure TFmPQEIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQEIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQEIts.FormShow(Sender: TObject);
begin
  EdUCTextoB.Text := DmNFe_0000.ObtemDescricao_TabGov_1(
  ((EdICMS_Orig.ValueVariant * 100) +  EdICMS_CST.ValueVariant),
    CO_NOME_tbspedefd_ICMS_CST, FData);
end;

procedure TFmPQEIts.EdTotalkgBrutoChange(Sender: TObject);
begin
 //if EdTotalkgBruto.Focused then FOnEditA := 3;
  CalculaOnEdit;
end;

procedure TFmPQEIts.EdTotalkgLiqChange(Sender: TObject);
begin
  if EdTotalkgLiq.Focused then
  begin
    Pesos(1);
    CalculaOnEdit;
  end;
end;

procedure TFmPQEIts.EdTotalkgLiqExit(Sender: TObject);
begin
  EdTotalkgLiq.ValueVariant := FkgT;
end;

procedure TFmPQEIts.EdUCTextoIPI_CSTRedefinido(Sender: TObject);
begin
  if (EdUCTextoIPI_CST.Text = EmptyStr) then
  begin
    EdIPI_CST.ValueVariant := '';
  end;
end;

procedure TFmPQEIts.EdCOFINS_CSTChange(Sender: TObject);
begin
  EdUCTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(EdCOFINS_CST.Text);
end;

procedure TFmPQEIts.EdCOFINS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCOFINS_CST.Text := UFinanceiro.ListaDeCST_COFINS();
end;

procedure TFmPQEIts.EdCOFINS_vBCChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostCOFINS);
end;

procedure TFmPQEIts.EdICMS_CSTChange(Sender: TObject);
begin
  //EdUCTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdICMS_CST.Text));
  EdUCTextoB.Text := DmNFe_0000.ObtemDescricao_TabGov_1(
  ((EdICMS_Orig.ValueVariant * 100) +  EdICMS_CST.ValueVariant),
    CO_NOME_tbspedefd_ICMS_CST, FData);
end;

procedure TFmPQEIts.EdICMS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  ICMS_CST: Variant;
begin
  //if Key = VK_F3 then
  //  EdICMS_CST.Text := UFinanceiro.ListaDeTributacaoPeloICMS();
  //
  if Key=VK_F3 then
  begin

    CuringaLoc.Pesquisa2('Codigo', 'Nome', CO_NOME_tbspedefd_ICMS_CST, DModG.AllID_DB,
    ''(*Extra*), ICMS_CST, dmktfInteger);
    if ICMS_CST <> null then
    begin
      //if ICMS_CST > 100 then
      begin
        EdICMS_Orig.ValueVariant := ICMS_CST div 100;
        EdICMS_CST.ValueVariant := ICMS_CST - (100 * EdICMS_Orig.ValueVariant);
      end;
    end;
  end;
end;

procedure TFmPQEIts.EdICMS_vBCChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostICMS);
end;

procedure TFmPQEIts.EdIPI_CSTChange(Sender: TObject);
begin
  EdUCTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(EdIPI_CST.Text);
end;

procedure TFmPQEIts.EdIPI_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdIPI_CST.Text := UFinanceiro.ListaDeCST_IPI();
end;

procedure TFmPQEIts.EdPIS_CSTChange(Sender: TObject);
begin
  EdUCTextoPIS_CST.Text := UFinanceiro.CST_PIS_Get(EdPIS_CST.Text);
end;

procedure TFmPQEIts.EdPIS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdPIS_CST.Text := UFinanceiro.ListaDeCST_PIS();
end;

procedure TFmPQEIts.EdPIS_vBCChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostPIS);
end;

procedure TFmPQEIts.Pesos(Index: Integer);
var
  kgU, kgT, Vol: Double;
begin
  kgU := EdPesoVL.ValueVariant;
  kgT := EdTotalkgLiq.ValueVariant;
  Vol := EdVolumes.ValueVariant;
  //
  case Index of
    // Qtde por volume
    0: {if (kgT <> 0) and (kgU > 0) then
        Vol := kgT / kgU
      else}
        kgT := kgU * Vol;
    // Qtde total
    1: {if (kgT <> 0) and (kgU > 0) then Vol := kgT / kgU else
      begin
        if (Vol>0) then
        begin
          if kgU > 0 then
            kgT := kgU * Vol
          else}
            kgU := kgT / Vol;
        {end;
      end;}
    // Volumes
    2: {
    if (kgT <> 0) and (Vol > 0) then
        kgU := kgT / Vol
      else}
        kgT := kgU * Vol;
  end;
  FkgU := kgU;
  FkgT := kgT;
  FVol := Vol;
  //
  if Index <> 0 then EdPesoVL.ValueVariant     := FkgU;
  if Index <> 1 then EdTotalkgLiq.ValueVariant := FkgT;
  if Index <> 2 then EdVolumes.ValueVariant    := FVol;
end;

procedure TFmPQEIts.QrPQCliCalcFields(DataSet: TDataSet);
begin
  QrPQCliNOMEMOEDAPADRAO.Value := UMoedas.ObtemNomeMoeda(QrPQCliMoedaPadrao.Value);
end;

procedure TFmPQEIts.ReopenCFOP();
begin
  //WHERE Codigo=CONCAT(SUBSTRING(1234, 1, 1), '.', SUBSTRING(1234, 2, 4))
  UnDmkDAC_PF.AbreMySQLQuery0(QrCFOP, Dmod.MyDB, [
  'SELECT CAST(CONCAT(SUBSTRING(cfp.Codigo, 1, 1), SUBSTRING(cfp.Codigo, 3, 3)) AS SIGNED) Codigo,',
  'cfp.Nome',
  'FROM cfop2003 cfp',
  'ORDER BY cfp.Nome',
  '']);
end;

procedure TFmPQEIts.ReopenPQ(Quais: TPartOuTodo);
var
  SQL_IQ: String;
begin
  if Quais = ptIndef then
    UnDmkDAC_PF.AbreQuery(QrPQ1, Dmod.MyDB)
  else
  begin
    if Quais = ptTotal then
      SQL_IQ := ''
    else
      SQL_IQ := 'AND pfo.IQ=' + Geral.FF0(FmPQE.QrPQEIQ.Value);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPQ1, Dmod.MyDB, [
    'SELECT DISTINCT med.SIGLA, pq.Nome NOMEPQ, pfo.* ',
    'FROM pqfor pfo',
    'LEFT JOIN pq pq ON pfo.PQ=pq.Codigo',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pq.Codigo',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.Unidmed',
    'WHERE pfo.CI=' + Geral.FF0(FmPQE.QrPQECI.Value),
    SQL_IQ,
    'AND pfo.Empresa=' + Geral.FF0(FEmpresa),
    'AND pq.Ativo = 1 ',
    'AND pq.GGXNiv2 > 0 ',
    'ORDER BY pq.Nome ',
    '']);
  end;
end;

procedure TFmPQEIts.ReopenPQCli();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQCli, Dmod.MyDB, [
    'SELECT CustoPadrao, MoedaPadrao ',
    'FROM pqcli ',
    'WHERE PQ=' + Geral.FF0(QrPQ1PQ.Value),
    'AND CI=' + Geral.FF0(FmPQE.QrPQECI.Value),
    '']);
end;

procedure TFmPQEIts.RGMoedaClick(Sender: TObject);
begin
  ConfiguraCotacao(RGMoeda.ItemIndex);
end;

procedure TFmPQEIts.EdInsumoChange(Sender: TObject);
var
  Insumo: Integer;
begin
  ReopenPQCli();
  ConfiguraCotacao(QrPQCliMoedaPadrao.Value);
  if ImgTipo.SQLType = stIns then
  begin
    Insumo := EdInsumo.ValueVariant;
    if Insumo <> 0 then
      ConfiguraImpostos(Insumo);
  end;
end;

procedure TFmPQEIts.ConfiguraImpostos(Insumo: Integer);
begin
  if Dmod.QrControleNaoRecupImposPQ.Value = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
      'SELECT ICMSRec_pAliq, PISRec_pAliq,  ',
      'COFINSRec_pAliq  ',
      'FROM gragru1 ',
      'WHERE Nivel1=' + Geral.FF0(Insumo),
      '']);
    //
    EdRpICMS.ValueVariant   := QrGraGru1ICMSRec_pAliq.Value;
    EdRpPIS.ValueVariant    := QrGraGru1PISRec_pAliq.Value;
    EdRpCOFINS.ValueVariant := QrGraGru1COFINSRec_pAliq.Value;
  end else
  begin
    EdRpICMS.ValueVariant   := 0.00;
    EdRpPIS.ValueVariant    := 0.00;
    EdRpCOFINS.ValueVariant := 0.00;

    GBRecuImpost.Enabled := False;
    GBRecuImpost.Caption := 'Recupera��o de impostos: Op��es espec�ficas -> Geral - > N�o recupera impostos na entrada de insumos.!!!!!!!!!!!!!!';
  end;
end;

procedure TFmPQEIts.EdInsumoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  (* 2017-11-07 => Desativado para evitar de atualizar o custo do cliente interno errado
  if Key = VK_F4 then
    MostraTodosPQs();
  *)
  // ini 2019-02-06
  if Key = VK_F4 then
    FmPQEIts.ReopenPQ(ptTotal);
  // fim 2019-02-06
end;

procedure TFmPQEIts.CBInsumoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  (* 2017-11-07 => Desativado para evitar de atualizar o custo do cliente interno errado
  if Key = VK_F4 then
    MostraTodosPQs();
  *)
  // ini 2019-02-06
  if Key = VK_F4 then
    FmPQEIts.ReopenPQ(ptTotal);
  // fim 2019-02-06
end;

procedure TFmPQEIts.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmPQEIts.ConfiguraCotacao(Moeda: Integer);
var
  Visi: Boolean;
begin
  Visi              := Moeda <> 0;
  RGMoeda.ItemIndex := Moeda;
  //
  LaCotacao.Visible := Visi;
  EdCotacao.Visible := Visi;
end;

procedure TFmPQEIts.ConfiguraDados(Codigo: Integer);
begin
  QrPQ1.Close;
  ReopenPQ(ptIndef);
  //UnDmkDAC_PF.AbreQuery(QrPQ1, Dmod.MyDB);
  //
  EdInsumo.ValueVariant := Codigo;
  CBinsumo.KeyValue     := Codigo;
  EdInsumo.SetFocus;
end;

procedure TFmPQEIts.Dadosfiscais1Click(Sender: TObject);
var
  Insumo: Integer;
begin
  VAR_CADASTRO := 0;
  Insumo       := EdInsumo.ValueVariant;
  //
  Grade_Jan.MostraFormGraGruN(Insumo, 0, tpGraCusPrc, tsfDadosFiscais);
  //
  if BDC_PQCAD <> 0 then
    ConfiguraDados(BDC_PQCAD)
  else if VAR_CADASTRO <> 0 then
    ConfiguraDados(VAR_CADASTRO);
  //
  ConfiguraImpostos(Insumo);
end;

procedure TFmPQEIts.Cadastrodoproduto1Click(Sender: TObject);
var
  Insumo: Integer;
begin
  VAR_CADASTRO := 0;
  Insumo       := EdInsumo.ValueVariant;
  //
  FmPrincipal.CadastroPQ(Insumo);
  //
  if BDC_PQCAD <> 0 then
    ConfiguraDados(BDC_PQCAD)
  else if VAR_CADASTRO <> 0 then
    ConfiguraDados(VAR_CADASTRO);
  //
  ConfiguraImpostos(Insumo);
end;

procedure TFmPQEIts.CalculaIPI();
begin
  EdIPI_vIPI.ValueVariant :=
  EdIPI_pIPI.ValueVariant *
  EdVlrItem.ValueVariant / 100;
end;

(* 2017-11-07 => Desativado para evitar de atualizar o custo do cliente interno errado
procedure TFmPQEIts.MostraTodosPQs();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQ1, Dmod.MyDB, [
  'SELECT DISTINCT pq.Nome NOMEPQ, pfo.PQ ',
  'FROM pqfor pfo ',
  'LEFT JOIN pq pq ON pfo.PQ=pq.Codigo ',
  'WHERE pq.Codigo>0',
  'AND pq.Ativo = 1 ',
  'AND pq.GGXNiv2 > 0 ',
  'AND pfo.Empresa=' + Geral.FF0(FEmpresa),
  'ORDER BY pq.Nome ',
  '']);
end;
*)

procedure TFmPQEIts.SbCFOPClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormCFOP2003();
  if VAR_CADASTRO <> 0 then
  begin
    ReopenCFOP();
    UMyMod.SetaCodigoPesquisado(EdCFOP, CBCFOP, QrCFOP, VAR_CADASTRO);
  end;
end;

procedure TFmPQEIts.SBProdutoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProduto, SBProduto);
end;

procedure TFmPQEIts.SpeedButton1Click(Sender: TObject);
var
  Val: Variant;
  Default, CustoPadrao: Double;
  Insumo, PQ, CI, Empresa: Integer;
begin
  Insumo := EdInsumo.ValueVariant;
  if Insumo > 0 then
  begin
    Default := QrPQCliCustoPadrao.Value;
    if not MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, Default,
    4, 0, '', '', True, 'Custo padr�o', 'Informe o Custo Padr�o: ', 0, Val) then
      Exit;
    CustoPadrao := Val;
    if CustoPadrao <> Default then
    begin
      PQ := Insumo;
      CI := FmPQE.QrPQECI.Value;
      Empresa := FEmpresa;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, StUpd, 'pqcli', False, [
      'CustoPadrao'], ['PQ', 'CI', 'Empresa'], [
      CustoPadrao], [PQ, CI, Empresa], True) then
      begin
        UnDmkDAC_PF.AbreQuery(QrPQ1, Dmod.MyDB);
        EdInsumo.ValueVariant := 0;
        CBInsumo.KeyValue     := Null;
        //
        EdInsumo.ValueVariant := Insumo;
        CBInsumo.KeyValue     := Insumo;
      end;
    end;
  end else
    Geral.MB_Aviso('Produto n�o definido!');
end;

(*
object QrPQ1Controle: TIntegerField
  FieldName = 'Controle'
  Required = True
end
object QrPQ1IQ: TIntegerField
  FieldName = 'IQ'
end
object QrPQ1CI: TIntegerField
  FieldName = 'CI'
end
object QrPQ1Prazo: TWideStringField
  FieldName = 'Prazo'
end
object QrPQ1Lk: TIntegerField
  FieldName = 'Lk'
end
object QrPQ1DataCad: TDateField
  FieldName = 'DataCad'
end
object QrPQ1DataAlt: TDateField
  FieldName = 'DataAlt'
end
object QrPQ1UserCad: TIntegerField
  FieldName = 'UserCad'
end
object QrPQ1UserAlt: TIntegerField
  FieldName = 'UserAlt'
end
*)
end.


