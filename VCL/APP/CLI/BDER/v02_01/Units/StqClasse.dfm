object FmStqClasse: TFmStqClasse
  Left = 339
  Top = 185
  Caption = 'STQ-CLASS-001 :: Classifica'#231#227'o'
  ClientHeight = 660
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 496
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 124
      Width = 1008
      Height = 372
      ActivePage = TabSheet3
      Align = alClient
      TabOrder = 0
      object TabSheet4: TTabSheet
        Caption = ' Pe'#231'as '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 246
        object GradeQI: TStringGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 344
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          TabOrder = 0
          OnDrawCell = GradeQIDrawCell
          OnKeyDown = GradeQIKeyDown
          ExplicitHeight = 246
          RowHeights = (
            18
            18)
        end
      end
      object TabSheet5: TTabSheet
        Caption = ' '#193'rea (? ? ?) '
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 246
        object GradeQA: TStringGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 344
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          TabOrder = 0
          OnDrawCell = GradeQADrawCell
          OnKeyDown = GradeQAKeyDown
          ExplicitHeight = 246
          RowHeights = (
            18
            18)
        end
      end
      object TabSheet6: TTabSheet
        Caption = ' Peso (kg) '
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 246
        object GradeQK: TStringGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 344
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          TabOrder = 0
          OnDrawCell = GradeQKDrawCell
          OnKeyDown = GradeQKKeyDown
          ExplicitHeight = 246
          RowHeights = (
            18
            18)
        end
      end
      object TabSheet1: TTabSheet
        Caption = ' C'#243'digos '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 246
        object GradeC: TStringGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 344
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = GradeCDrawCell
          ExplicitHeight = 246
          RowHeights = (
            18
            18)
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Ativos '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 246
        object GradeA: TStringGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 344
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = GradeADrawCell
          ExplicitHeight = 246
          RowHeights = (
            18
            18)
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' X '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeX: TStringGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 344
          Align = alClient
          ColCount = 2
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
          TabOrder = 0
          OnDrawCell = GradeXDrawCell
          RowHeights = (
            18
            18)
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 124
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 408
        Top = 44
        Width = 40
        Height = 13
        Caption = 'Produto:'
      end
      object Label2: TLabel
        Left = 8
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label3: TLabel
        Left = 8
        Top = 44
        Width = 90
        Height = 13
        Caption = 'Centro de estoque:'
      end
      object EdGraGru1: TdmkEditCB
        Left = 408
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGru1Change
        OnExit = EdGraGru1Exit
        DBLookupComboBox = CBGraGru1
        IgnoraDBLookupComboBox = False
      end
      object CBGraGru1: TdmkDBLookupComboBox
        Left = 468
        Top = 60
        Width = 321
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGraGru1
        TabOrder = 1
        dmkEditCB = EdGraGru1
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFilial: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFilial
        IgnoraDBLookupComboBox = False
      end
      object CBFilial: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 721
        Height = 21
        KeyField = 'Filial'
        ListField = 'NomeEmp'
        ListSource = DModG.DsFiliLog
        TabOrder = 3
        dmkEditCB = EdFilial
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdStqCenCad: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenCad
        IgnoraDBLookupComboBox = False
      end
      object CBStqCenCad: TdmkDBLookupComboBox
        Left = 68
        Top = 60
        Width = 337
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsStqCenCad
        TabOrder = 5
        dmkEditCB = EdStqCenCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGGrandeza: TRadioGroup
        Left = 792
        Top = 14
        Width = 93
        Height = 68
        Caption = ' Grandeza '#225'rea:'
        ItemIndex = 0
        Items.Strings = (
          '? ? ?'
          'Metros (m'#178')'
          'P'#233's (ft'#178')')
        TabOrder = 6
        OnClick = RGGrandezaClick
      end
      object RGFatorClas: TRadioGroup
        Left = 888
        Top = 14
        Width = 109
        Height = 68
        Caption = ' Divis'#227'o'#185' (1: ?): '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8')
        TabOrder = 7
      end
      object StaticText1: TStaticText
        Left = 0
        Top = 93
        Width = 1008
        Height = 31
        Align = alBottom
        Alignment = taCenter
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 
          'Divis'#227'o'#185': '#201' a quantidade de partes por qual ser'#225' dividido a quan' +
          'tidade de pe'#231'as informadas. Exemplo: '#13#10'De inteiros para inteiros' +
          ' informe "1". De inteiros para meios informe "2". De meios para ' +
          'meios informe "1".'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 160
        Height = 32
        Caption = 'Classifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 160
        Height = 32
        Caption = 'Classifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 160
        Height = 32
        Caption = 'Classifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 544
    Width = 1008
    Height = 52
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 35
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 18
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 596
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 36
    Top = 8
  end
  object QrGraGru1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1, '
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, '
      'HowBxaEstq, GerBxaEstq, gtc.Nome NOMEGRATAMCAD, '
      'gtc.CodUsu CODUSUGRATAMCAD, gg1.CST_A, gg1.CST_B, '
      'gg1.UnidMed, gg1.NCM, gg1.Peso, gg1.FatorClas,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, '
      'unm.Nome NOMEUNIDMED '
      'FROM gragru1 gg1'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pgt.TipPrd=1 '
      'ORDER BY gg1.Nome')
    Left = 8
    Top = 8
    object QrGraGru1Codusu: TIntegerField
      FieldName = 'Codusu'
      Required = True
    end
    object QrGraGru1Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGru1Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Required = True
    end
    object QrGraGru1NOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGru1CODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGru1CST_A: TSmallintField
      FieldName = 'CST_A'
      Required = True
    end
    object QrGraGru1CST_B: TSmallintField
      FieldName = 'CST_B'
      Required = True
    end
    object QrGraGru1UnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
    object QrGraGru1NCM: TWideStringField
      FieldName = 'NCM'
      Required = True
      Size = 10
    end
    object QrGraGru1Peso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrGraGru1SIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 3
    end
    object QrGraGru1CODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGru1NOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGru1HowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGru1GerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGru1FatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
  end
  object QrStqCenCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 64
    Top = 8
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 92
    Top = 8
  end
end
