unit MPInAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnInternalConsts, UnMsgInt,
  ExtCtrls, mySQLDbTables, ComCtrls, dmkEdit, Menus, dmkGeral, dmkEditCB,
  dmkDBLookupComboBox, DateUtils, dmkLabel, dmkImage, UnDmkEnums,
  UnDeprecated_PF, DmkDAC_PF;

type
  TFmMPInAdd = class(TForm)
    QrDonos: TmySQLQuery;
    DsDonos: TDataSource;
    DsProcedencias: TDataSource;
    QrProcedencias: TmySQLQuery;
    QrDonosCodigo: TIntegerField;
    QrDonosNOMEENTIDADE: TWideStringField;
    QrProcedenciasCodigo: TIntegerField;
    QrProcedenciasNOMEENTIDADE: TWideStringField;
    QrTransporte: TmySQLQuery;
    DsTransporte: TDataSource;
    QrTransporteCodigo: TIntegerField;
    QrTransporteNOMEENTIDADE: TWideStringField;
    QrLoc: TmySQLQuery;
    QrLocControle: TIntegerField;
    QrProcedenciasPreDescarn: TSmallintField;
    QrProcedenciasMaskLetras: TWideStringField;
    QrProcedenciasMaskFormat: TWideStringField;
    QrCorretores: TmySQLQuery;
    QrCorretoresCodigo: TIntegerField;
    QrCorretoresNOMEENTI: TWideStringField;
    DsCorretores: TDataSource;
    QrProcedenciasCorretor: TIntegerField;
    QrProcedenciasComissao: TFloatField;
    QrProcedenciasDescAdiant: TFloatField;
    QrProcedenciasCondPagto: TWideStringField;
    QrProcedenciasCondComis: TWideStringField;
    QrProcedenciasLocalEntrg: TWideStringField;
    QrProcedenciasAbate: TIntegerField;
    QrProcedenciasTransporte: TIntegerField;
    PMFornece: TPopupMenu;
    Entidades1: TMenuItem;
    FornecedoresdeMP1: TMenuItem;
    Panel8: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    Label15: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton3: TSpeedButton;
    Label8: TLabel;
    Label6: TLabel;
    Label16: TLabel;
    Label2: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label9: TLabel;
    SpeedButton4: TSpeedButton;
    CBClienteI: TdmkDBLookupComboBox;
    EdClienteI: TdmkEditCB;
    EdProcedencia: TdmkEditCB;
    CBProcedencia: TdmkDBLookupComboBox;
    RGAbateTipo: TRadioGroup;
    EdTransporte: TdmkEditCB;
    CBTransporte: TdmkDBLookupComboBox;
    dmkEdComisPer: TdmkEdit;
    CBCorretor: TdmkDBLookupComboBox;
    EdCorretor: TdmkEditCB;
    EdCondComis: TEdit;
    TPData: TDateTimePicker;
    EdFicha: TdmkEdit;
    EdLote: TdmkEdit;
    EdMarca: TdmkEdit;
    dmkEdDescAdiant: TdmkEdit;
    BitBtn1: TBitBtn;
    Panel4: TPanel;
    Panel6: TPanel;
    Panel5: TPanel;
    Label7: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    dmkEdRecorte_PDA: TdmkEdit;
    dmkEdRecorte_PTA: TdmkEdit;
    dmkEdRaspa_PTA: TdmkEdit;
    Panel7: TPanel;
    Label11: TLabel;
    Label10: TLabel;
    RGTipificacao: TRadioGroup;
    RGAparasCabelo: TRadioGroup;
    RGAnimal: TRadioGroup;
    RGSeboPreDescarne: TRadioGroup;
    EdLocalEntrg: TEdit;
    EdCondPagto: TEdit;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    MeObserv: TMemo;
    Label17: TLabel;
    dmkEdPTA: TdmkEdit;
    Label18: TLabel;
    Label19: TLabel;
    dmkEdFimM2: TdmkEdit;
    dmkEdFimP2: TdmkEdit;
    Label20: TLabel;
    QrProcedenciasLoteLetras: TWideStringField;
    QrProcedenciasLoteFormat: TWideStringField;
    QrLocCodigo: TIntegerField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel12: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel13: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtUsarEste: TBitBtn;
    Label21: TLabel;
    EdGraGruX: TdmkEdit;
    EdNO_GraGru1: TdmkEdit;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdProcedenciaChange(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdFichaExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure TPDataChange(Sender: TObject);
    procedure TPDataClick(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure Entidades1Click(Sender: TObject);
    procedure FornecedoresdeMP1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure dmkEdFimM2Exit(Sender: TObject);
    procedure dmkEdFimP2Exit(Sender: TObject);
    procedure BtUsarEsteClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure RGTipificacaoClick(Sender: TObject);
    procedure RGAnimalClick(Sender: TObject);
    procedure EdClienteIRedefinido(Sender: TObject);
  private
    { Private declarations }
    FM2, FP2: Double;
    FTabLctA: String;
    procedure GeraMarca();
    procedure DefineGraGruX();
  public
    { Public declarations }
    FControle, FPeriodo: Integer;
  end;

var
  FmMPInAdd: TFmMPInAdd;

implementation

uses UnMyObjects, Module, Principal, UnInternalConsts2, UMySQLModule, EntiMP,
  MyDBCheck, MPIn, ModuleGeral, AppListas;

{$R *.DFM}

procedure TFmMPInAdd.BtDesisteClick(Sender: TObject);
begin
  FPeriodo  := 0;
  FControle := 0;
  Close;
end;

procedure TFmMPInAdd.BtUsarEsteClick(Sender: TObject);
begin
  //FPeriodo  := ?
  //FControle := ?
  //Close;
end;

procedure TFmMPInAdd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FM2 := 0;
  FP2 := 0;
  TPData.Date := Date;
  UnDmkDAC_PF.AbreQuery(QrDonos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcedencias, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporte, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCorretores, Dmod.MyDB);
  UnAppListas.ConfiguraTipificacao(-3, RGTipificacao, 4, 0);
  UnAppListas.ConfiguraTipificacao(-5, RGAnimal, 3, 0);
end;

procedure TFmMPInAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if ImgTipo.SQLType = stIns then
  begin
    RGAparasCabelo.Items.Clear;
    RGAparasCabelo.Items.Add('Gera');
    RGAparasCabelo.Items.Add('N�o gera');
    RGAparasCabelo.Items.Add('J� gerou');
    RGAparasCabelo.ItemIndex := 0;
    //
    RGSeboPreDescarne.Items.Clear;
    RGSeboPreDescarne.Items.Add('Gera');
    RGSeboPreDescarne.Items.Add('N�o gera');
    RGSeboPreDescarne.Items.Add('J� gerou');
    RGSeboPredescarne.ItemIndex := 0;
  end;
end;

procedure TFmMPInAdd.DefineGraGruX;
begin
  EdGraGruX.ValueVariant :=
    Dmod.ObtemGraGruXDeCouro(RGTipificacao.ItemIndex, RGAnimal.ItemIndex);
end;

////////////////////////////////////////////////////////

procedure TFmMPInAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPInAdd.EdProcedenciaChange(Sender: TObject);
begin
  RGSeboPreDescarne.ItemIndex := QrProcedenciasPreDescarn.Value;
  GeraMarca;
end;

procedure TFmMPInAdd.BtConfirmaClick(Sender: TObject);
var
  Res, ClienteI, Proceden, Periodo, Transporte: Integer;
begin
  QrLoc.Close;
  QrLoc.Params[0].AsString := EdMarca.Text;
  if ImgTipo.SQLType = stIns then
    QrLoc.Params[1].AsInteger := 0
  else
    QrLoc.Params[1].AsInteger := FControle;
  UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
  if QrLoc.RecordCount > 0 then
  begin
    Res := Geral.MB_Pergunta('A marca j� exite na OS '+
    IntToStr(QrLocControle.Value)+'.' + sLineBreak +
    'Deseja utilizar esta OS para adicionar a NF?');
    if Res = ID_YES then
    begin
      FPeriodo  := QrLocCodigo.Value;
      FControle := QrLocControle.Value;
      //if not
      FmMPIn.QrMPIn.Locate('Controle', FControle, []);
      // then
        //Geral.MB_Aviso('N�o foi poss�vel lozalizar a OS '
      Close;
    end;
    Exit;
  end;
  //
  ClienteI := Geral.IMV(EdClienteI.Text);
  if ClienteI = 0 then
  begin
    Application.MessageBox('Informe o dono do couro!', 'Erro',
      MB_OK+MB_ICONERROR);
    EdClienteI.SetFocus;
    Exit;
  end;
  Proceden := Geral.IMV(EdProcedencia.Text);
  if Proceden = 0 then
  begin
    Application.MessageBox('Informe o dono do couro!', 'Erro',
      MB_OK+MB_ICONERROR);
    EdProcedencia.SetFocus;
    Exit;
  end;
  Transporte := Geral.IMV(EdTransporte.Text);
  if Transporte = 0 then
  begin
    Application.MessageBox('Informe o transportador do couro!', 'Erro',
      MB_OK+MB_ICONERROR);
    EdTransporte.SetFocus;
    Exit;
  end;
  if RGTipificacao.ItemIndex = 0 then
  begin
    Application.MessageBox('Informe a Tipifica��o!', 'Erro',
      MB_OK+MB_ICONERROR);
    RGTipificacao.SetFocus;
    Exit;
  end;
  if FPeriodo > 0 then Periodo := FPeriodo else
    Periodo := Geral.Periodo2000(TPData.Date);
  //
  if FControle = 0 then
    FControle :=
      UMyMod.BuscaEmLivreY_Def('mpin', 'Controle', ImgTipo.SQLType, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'mpin', False,
  [
    'Data', 'Marca', 'ClienteI',
    'Procedencia', 'Tipificacao', 'AparasCabelo',
    'SeboPreDescarne', 'Ficha',
    'AbateTipo' , 'Transportadora', 'Lote',
    'Recorte_PDA', 'PTA',
    'Recorte_PTA', 'Raspa_PTA',
    'Corretor',
    'ComisPer', 'DescAdiant',
    'CondPagto', 'CondComis',
    'LocalEntrg', 'Observ', 'Animal',
    'FimM2', 'FimP2'
    ],
  [ 'Codigo', 'Controle'], [
    FormatDateTime(VAR_FORMATDATE, TPData.Date), EdMarca.Text, ClienteI,
    Proceden, RGTipificacao.ItemIndex, RGAparasCabelo.ItemIndex,
    RGSeboPreDescarne.ItemIndex, Geral.IMV(EdFicha.Text),
    RGAbateTipo.ItemIndex, Transporte, EdLote.Text,
    dmkEdRecorte_PDA.ValueVariant, dmkEdPTA.ValueVariant,
    dmkEdRecorte_PTA.ValueVariant, dmkEdRaspa_PTA.ValueVariant,
    Geral.IMV(EdCorretor.Text),
    dmkEdComisPer.ValueVariant, dmkEdDescAdiant.ValueVariant,
    EdCondPagto.Text, EdCondComis.Text,
    EdLocalEntrg.Text, MeObserv.Text, RGAnimal.ItemIndex,
    dmkEdFimM2.ValueVariant, dmkEdFimP2.ValueVariant
  ], [Periodo, FControle], True) then
  begin
    FPeriodo := Periodo;
    FmMPIn.AtualizaMPIn(FControle);
    Close;
  end;
end;

procedure TFmMPInAdd.EdClienteIRedefinido(Sender: TObject);
(*
var
  Cliente: Integer;
*)
begin
(*
  Cliente := EdClienteI.ValueVariant;
  //
  if ClienteI <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Cliente)
  else
    FTabLctA := sTabLctErr;
*)
end;

procedure TFmMPInAdd.EdFichaExit(Sender: TObject);
begin
  EdFicha.Text := Geral.TFT(EdFicha.Text, 0, siPositivo);
end;

procedure TFmMPInAdd.EdGraGruXChange(Sender: TObject);
begin
  if EdGraGruX.ValueVariant = 0 then
    EdNO_GraGru1.Text := ''
  else begin
    QrGraGruX.Close;
    QrGraGruX.Params[0].AsInteger := EdGraGruX.ValueVariant;
    UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
    EdNO_GraGru1.Text := QrGraGruXNome.Value;
  end;
end;

procedure TFmMPInAdd.SpeedButton1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  QrDonos.Close;
  UnDmkDAC_PF.AbreQuery(QrDonos, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdClienteI.Text := IntToStr(VAR_ENTIDADE);
    CBClienteI.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmMPInAdd.SpeedButton3Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  QrTransporte.Close;
  UnDmkDAC_PF.AbreQuery(QrTransporte, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdTransporte.Text := IntToStr(VAR_ENTIDADE);
    CBTransporte.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmMPInAdd.TPDataChange(Sender: TObject);
begin
  GeraMarca;
end;

procedure TFmMPInAdd.TPDataClick(Sender: TObject);
begin
  GeraMarca;
end;

procedure TFmMPInAdd.GeraMarca;
var
  Marca, Lote, Fmt, Sem: String;
begin
  if ImgTipo.SQLType <> stIns then Exit;

  //

  Marca := QrProcedenciasMaskLetras.Value;
  Fmt := QrProcedenciasMaskFormat.Value;
  if Fmt <> '' then
  begin
    if pos('sa', Fmt) > 0 then
    begin
      Sem := FormatFloat('00', WeekOfTheYear(TPData.Date));
      Fmt := Geral.Substitui(Fmt, 'sa', sem);
    end;
    if pos('sm', Fmt) > 0 then
    begin
      Sem := FormatFloat('00', WeekOfTheMonth(TPData.Date));
      Fmt := Geral.Substitui(Fmt, 'sm', sem);
    end;
    Fmt := Geral.Substitui(Fmt, 'a', 'y');
    Fmt := Geral.Substitui(Fmt, 'A', 'Y');
    //
    Marca := Marca + FormatDateTime(Fmt, TPData.Date);
  end;
  Marca := Trim(Marca);
  if Marca <> '' then
    EdMarca.Text := Marca;

  //

  Lote := QrProcedenciasLoteLetras.Value;
  Fmt := QrProcedenciasLoteFormat.Value;
  if Fmt <> '' then
  begin
    if pos('sa', Fmt) > 0 then
    begin
      Sem := FormatFloat('00', WeekOfTheYear(TPData.Date));
      Fmt := Geral.Substitui(Fmt, 'sa', sem);
    end;
    if pos('sm', Fmt) > 0 then
    begin
      Sem := FormatFloat('00', WeekOfTheMonth(TPData.Date));
      Fmt := Geral.Substitui(Fmt, 'sm', sem);
    end;
    Fmt := Geral.Substitui(Fmt, 'a', 'y');
    Fmt := Geral.Substitui(Fmt, 'A', 'Y');
    //
    Lote := Lote + FormatDateTime(Fmt, TPData.Date);
  end;
  Lote := Trim(Lote);
  if Lote <> '' then
    EdLote.Text := Lote;

  //

  EdCorretor.Text                := IntToStr(QrProcedenciasCorretor.Value);
  CBCorretor.KeyValue            := QrProcedenciasCorretor.Value;
  EdTransporte.Text              := IntToStr(QrProcedenciasTransporte.Value);
  CBTransporte.KeyValue          := QrProcedenciasTransporte.Value;
  dmkEdComisPer.ValueVariant     := QrProcedenciasComissao.Value;
  dmkEdDescAdiant.ValueVariant   := QrProcedenciasDescAdiant.Value;
  EdCondPagto.Text               := QrProcedenciasCondPagto.Value;
  EdCondComis.Text               := QrProcedenciasCondComis.Value;
  EdLocalEntrg.Text              := QrProcedenciasLocalEntrg.Value;
  RGAbateTipo.ItemIndex          := QrProcedenciasAbate.Value;
  //

end;

procedure TFmMPInAdd.RGAnimalClick(Sender: TObject);
begin
  DefineGraGruX();
end;

procedure TFmMPInAdd.RGTipificacaoClick(Sender: TObject);
begin
  DefineGraGruX();
end;

procedure TFmMPInAdd.SpeedButton4Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  QrCorretores.Close;
  UnDmkDAC_PF.AbreQuery(QrCorretores, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdCorretor.Text := IntToStr(VAR_ENTIDADE);
    CBCorretor.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmMPInAdd.Entidades1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  QrProcedencias.Close;
  UnDmkDAC_PF.AbreQuery(QrProcedencias, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdProcedencia.Text := IntToStr(VAR_ENTIDADE);
    CBProcedencia.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmMPInAdd.FornecedoresdeMP1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiMP, FmEntiMP, afmoNegarComAviso) then
  begin
    FmEntiMP.ShowModal;
    FmEntiMP.Destroy;
  end;
  QrProcedencias.Close;
  UnDmkDAC_PF.AbreQuery(QrProcedencias, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdProcedencia.Text := IntToStr(VAR_ENTIDADE);
    CBProcedencia.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmMPInAdd.BitBtn1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFornece, BitBtn1);
end;

procedure TFmMPInAdd.dmkEdFimM2Exit(Sender: TObject);
begin
  Deprecated_PF.VerificaAlteracaoMetrica_dmk(
    cgMtoFT, dmkEdFimM2, dmkEdFimP2, FM2, FP2);
end;

procedure TFmMPInAdd.dmkEdFimP2Exit(Sender: TObject);
begin
  Deprecated_PF.VerificaAlteracaoMetrica_dmk(
    cgFttoM, dmkEdFimM2, dmkEdFimP2, FM2, FP2);
end;

end.
