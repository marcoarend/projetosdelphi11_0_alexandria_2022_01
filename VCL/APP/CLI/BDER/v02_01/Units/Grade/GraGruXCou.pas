unit GraGruXCou;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  UnInternalConsts, mySQLDbTables, Menus, UnDmkProcFunc, dmkImage, UnDmkEnums,
  Vcl.Mask, dmkRadioGroup, dmkCheckBox;

type
  TFmGraGruXCou = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Panel4: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    GroupBox2: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    CkRespeitarPai: TCheckBox;
    DBCkGradeado: TDBCheckBox;
    PMInsAlt: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    LaNivel1: TLabel;
    QrGraGruXCou: TmySQLQuery;
    DsGraGruXCou: TDataSource;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrGraGruXCouNO_CouNiv1: TWideStringField;
    QrGraGruXCouNO_CouNiv2: TWideStringField;
    QrGraGruXCouNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXCouGraGruX: TIntegerField;
    QrGraGruXCouCouNiv1: TIntegerField;
    QrGraGruXCouCouNiv2: TIntegerField;
    QrGraGruXCouArtigoImp: TWideStringField;
    QrGraGruXCouClasseImp: TWideStringField;
    QrGraGruXCouPrevPcPal: TIntegerField;
    QrGraGruXCouLk: TIntegerField;
    QrGraGruXCouDataCad: TDateField;
    QrGraGruXCouDataAlt: TDateField;
    QrGraGruXCouUserCad: TIntegerField;
    QrGraGruXCouUserAlt: TIntegerField;
    QrGraGruXCouAlterWeb: TSmallintField;
    QrGraGruXCouAtivo: TSmallintField;
    QrGraGruXCouMediaMinM2: TFloatField;
    QrGraGruXCouMediaMaxM2: TFloatField;
    QrGraGruXCouMediaMinKg: TFloatField;
    QrGraGruXCouMediaMaxKg: TFloatField;
    QrGraGruXCouPrevAMPal: TFloatField;
    QrGraGruXCouPrevKgPal: TFloatField;
    QrGraGruXCouGrandeza: TSmallintField;
    QrGraGruXCouBastidao: TSmallintField;
    QrGraGruXCouGGXPronto: TIntegerField;
    QrGraGruXCouAWServerID: TIntegerField;
    QrGraGruXCouAWStatSinc: TSmallintField;
    QrGraGruXCouGraGru1: TIntegerField;
    QrGraGruXCouCU_GG1: TIntegerField;
    GBCouNiv: TGroupBox;
    Label1: TLabel;
    Label4: TLabel;
    Label11: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    EdCouNiv2: TdmkEditCB;
    CBCouNiv2: TdmkDBLookupComboBox;
    EdCouNiv1: TdmkEditCB;
    CBCouNiv1: TdmkDBLookupComboBox;
    EdPrevPcPal: TdmkEdit;
    EdPrevAMPal: TdmkEdit;
    EdPrevKgPal: TdmkEdit;
    RGBastidao: TdmkRadioGroup;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label3: TLabel;
    Label2: TLabel;
    EdArtigoImp: TdmkEdit;
    EdClasseImp: TdmkEdit;
    Label13: TLabel;
    EdMediaMinM2: TdmkEdit;
    Label14: TLabel;
    EdMediaMaxM2: TdmkEdit;
    EdMediaMinKg: TdmkEdit;
    Label19: TLabel;
    Label20: TLabel;
    EdMediaMaxKg: TdmkEdit;
    RGGrandeza: TdmkRadioGroup;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    QrRmsGGX: TmySQLQuery;
    QrRmsGGXGraGru1: TIntegerField;
    QrRmsGGXControle: TIntegerField;
    QrRmsGGXNO_PRD_TAM_COR: TWideStringField;
    QrRmsGGXSIGLAUNIDMED: TWideStringField;
    QrRmsGGXCODUSUUNIDMED: TIntegerField;
    QrRmsGGXNOMEUNIDMED: TWideStringField;
    DsRmsGGX: TDataSource;
    Panel3: TPanel;
    LaFatorNota: TLabel;
    EdFatorNota: TdmkEdit;
    LaGGXInProc: TLabel;
    EdGGXInProc: TdmkEditCB;
    CBGGXInProc: TdmkDBLookupComboBox;
    QrGraGruXCouGraGruY: TIntegerField;
    Qry: TmySQLQuery;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    Qr2: TmySQLQuery;
    CkUsaSifDipoa: TdmkCheckBox;
    QrGraGruXCouUsaSifDipoa: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrGraGruXCouAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    //
  public
    { Public declarations }
    procedure ReopenGraGruX(GragruX: Integer);
  end;

  var
  FmGraGruXCou: TFmGraGruXCou;

implementation

uses UnMyObjects, Module, UMySQLModule, GraGruN, DmkDAC_PF, ModProd, UnAppPF,
  UnGrade_Jan, UnGrade_PF, UnVS_PF, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmGraGruXCou.BtOKClick(Sender: TObject);
const
  sProcName = 'TFmGraGruXCou.BtOKClick()';
  //
  procedure AtualizaCampo(Tabela, Campo: String; GraGruX: Integer;
  Valor: Variant; Aviso: String);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'DESCRIBE ' + Tabela + ' "' + Campo + '"',
    '']);
    if Qry.RecordCount > 0 then
    begin
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
      Campo], ['GraGruX'], [Valor], [GraGruX], True);
    end else
      if Valor <> 0 then
        Geral.MB_Aviso(Aviso);
  end;
var
  ArtigoImp, ClasseImp: String;
  GGXInProc,
  GraGruX, CouNiv1, CouNiv2, PrevPcPal, Grandeza, Bastidao, GraGruY,
  UsaSifDipoa: Integer;
  FatorNota,
  MediaMinM2, MediaMaxM2, MediaMinKg, MediaMaxKg, PrevAMPal, PrevKgPal: Double;
  SQLType: TSQLType;
  Tabela: String;
begin
  SQLType        := ImgTipo.SQLType;
  //GraGru1        := QrGraGruXCouGraGru1.Value;
  GraGruY        := QrGraGruXCouGraGruY.Value;
  //
  GraGruX        := QrGraGruXCouGraGruX.Value;
  CouNiv1        := EdCouNiv1.ValueVariant;
  CouNiv2        := EdCouNiv2.ValueVariant;
  ArtigoImp      := EdArtigoImp.ValueVariant;
  ClasseImp      := EdClasseImp.ValueVariant;
  PrevPcPal      := EdPrevPcPal.ValueVariant;
  PrevAMPal      := EdPrevAMPal.ValueVariant;
  PrevKgPal      := EdPrevKgPal.ValueVariant;
  MediaMinM2     := EdMediaMinM2.ValueVariant;
  MediaMaxM2     := EdMediaMaxM2.ValueVariant;
  MediaMinKg     := EdMediaMinKg.ValueVariant;
  MediaMaxKg     := EdMediaMaxKg.ValueVariant;
  Grandeza       := RGGrandeza.ItemIndex;
  Bastidao       := RGBastidao.ItemIndex;
  FatorNota      := EdFatorNota.ValueVariant;
  GGXInProc      := EdGGXInProc.ValueVariant;
  UsaSifDipoa    := Geral.BoolToInt(CkUsaSifDipoa.Checked);
  //GGXPronto      := ;

  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruxcou', False, [
  'CouNiv1', 'CouNiv2', 'ArtigoImp',
  'ClasseImp', 'PrevPcPal', 'MediaMinM2',
  'MediaMaxM2', 'MediaMinKg', 'MediaMaxKg',
  'PrevAMPal', 'PrevKgPal', 'Grandeza',
  'Bastidao'(*, 'GGXPronto',*), 'UsaSifDipoa'
  ], [
  'GraGruX'], [
  CouNiv1, CouNiv2, ArtigoImp,
  ClasseImp, PrevPcPal, MediaMinM2,
  MediaMaxM2, MediaMinKg, MediaMaxKg,
  PrevAMPal, PrevKgPal, Grandeza,
  Bastidao(*, GGXPronto,*), UsaSifDipoa], [
  GraGruX], True) then
  begin
    Tabela := AppPF.ObtemNomeTabelaGraGruY(GraGruY);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'DESCRIBE ' + Tabela,
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('GraGruX') then    else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('Lk') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('DataCad') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('DataAlt') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('UserCad') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('UserAlt') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('AlterWeb') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('Ativo') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('AWServerID') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('AWStatSinc') then else
      //
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('FatorNota') then
        AtualizaCampo(Tabela, 'FatorNota', GraGruX, FatorNota,
        'O Grupo de Estoque do reduzido selecionado n�o possui "Fator Nota MPAG"!')
      //
      else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('FatorNota') then
        AtualizaCampo(Tabela, 'GGXInProc', GraGruX, GGXInProc,
        'O Grupo de Estoque do reduzido selecionado n�o possui "Couro a produzir"!')
      //
      else
        Geral.MB_Erro('Campo n�o implementado: "' + Qry.Fields[0].AsString +
        '" em: "' + sProcName + '"');
      //
      Qry.Next;
    end;
    Close;
  end;
end;

procedure TFmGraGruXCou.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruXCou.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruXCou.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stUpd;
  //
  VAR_CADASTRO := 0;
  //
  VS_PF.ConfiguraRGGrandezaVS(RGGrandeza, (*Colunas*)3, (*Default*)0);
  VS_PF.ConfiguraRGVSBastidao(RGBastidao, True(*Habilita*), (*Default*)-1);
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
end;

procedure TFmGraGruXCou.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruXCou.QrGraGruXCouAfterOpen(DataSet: TDataSet);
var
  TabDst: String;
begin
  QrRmsGGX.Close;
  //
  TabDst := VS_CRC_PF.ObtemNomeDestinoGraGruY(QrGraGruXCouGraGruY.Value);
  //
  if TabDst <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrRmsGGX, Dmod.MyDB, [
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
    'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED ',
    'FROM ' + TabDst + ' wmp ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
  end;
end;

procedure TFmGraGruXCou.ReopenGraGruX(GragruX: Integer);
var
  Tabela, TabDst: String;
begin
  LaFatorNota.Enabled := True;
  EdFatorNota.Enabled := True;
  //
  LaGGXInProc.Enabled := True;
  EdGGXInProc.Enabled := True;
  CBGGXInProc.Enabled := True;
  //
  LaGGXInProc.Caption := '.....';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruXCou, Dmod.MyDB, [
  'SELECT cn1.Nome NO_CouNiv1, cn2.Nome NO_CouNiv2, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR,',
  'xco.*, ggx.GraGru1, gg1.CodUsu CU_GG1, ggx.GraGruY ',
  'FROM gragruxcou xco',
  'LEFT JOIN gragrux ggx ON ggx.Controle=xco.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
  'WHERE xco.GraGruX=' + Geral.FF0(GraGruX),
  '']);
  //
  EdCouNiv2.ValueVariant := QrGraGruXCouCouNiv2.Value;
  CBCouNiv2.KeyValue     := QrGraGruXCouCouNiv2.Value;
  //
  EdCouNiv1.ValueVariant := QrGraGruXCouCouNiv1.Value;
  CBCouNiv1.KeyValue     := QrGraGruXCouCouNiv1.Value;
  //
  EdArtigoImp.ValueVariant  := QrGraGruXCouArtigoImp.Value;
  EdClasseImp.ValueVariant  := QrGraGruXCouClasseImp.Value;
  EdPrevPcPal.ValueVariant  := QrGraGruXCouPrevPcPal.Value;
  EdPrevAMPal.ValueVariant  := QrGraGruXCouPrevAMPal.Value;
  EdPrevKgPal.ValueVariant  := QrGraGruXCouPrevKgPal.Value;
  EdMediaMinM2.ValueVariant := QrGraGruXCouMediaMinM2.Value;
  EdMediaMaxM2.ValueVariant := QrGraGruXCouMediaMaxM2.Value;
  EdMediaMinKg.ValueVariant := QrGraGruXCouMediaMinKg.Value;
  EdMediaMaxKg.ValueVariant := QrGraGruXCouMediaMaxKg.Value;
  RGGrandeza.ItemIndex      := QrGraGruXCouGrandeza.Value;
  RGBastidao.ItemIndex      := QrGraGruXCouBastidao.Value;
  CkUsaSifDipoa.Checked     := Geral.IntToBool(QrGraGruXCouUsaSifDipoa.Value);
  //GGXPronto      := ;
  Tabela := AppPF.ObtemNomeTabelaGraGruY(QrGraGruXCouGraGruY.Value);
  TabDst := VS_CRC_PF.ObtemNomeDestinoGraGruY(QrGraGruXCouGraGruY.Value);
  //
  if Tabela <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr2, Dmod.MyDB, [
    'SELECT * FROM ' + Tabela,
    'WHERE GraGruX=' + Geral.FF0(QrGraGruXCouGraGruX.Value),
    '']);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'DESCRIBE ' + Tabela,
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('GraGruX') then    else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('Lk') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('DataCad') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('DataAlt') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('UserCad') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('UserAlt') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('AlterWeb') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('Ativo') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('AWServerID') then   else
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('AWStatSinc') then else
      //
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('FatorNota') then
      begin
        LaFatorNota.Enabled := True;
        EdFatorNota.Enabled := True;
        //
        EdFatorNota.ValueVariant := Qr2.FieldByName('FatorNota').AsFloat;
      end;
      if Uppercase(Qry.Fields[0].AsString) = Uppercase('GGXInProc') then
      begin
        LaGGXInProc.Enabled := True;
        EdGGXInProc.Enabled := True;
        CBGGXInProc.Enabled := True;
        //
        LaGGXInProc.Caption := 'Mat�ria-prima PDA:';
        EdGGXInProc.ValueVariant := Qr2.FieldByName('GGXInProc').AsFloat;
        CBGGXInProc.KeyValue     := Qr2.FieldByName('GGXInProc').AsFloat;
      end;
      //
      //
      //
      Qry.Next;
    end;
  end;
end;

end.

