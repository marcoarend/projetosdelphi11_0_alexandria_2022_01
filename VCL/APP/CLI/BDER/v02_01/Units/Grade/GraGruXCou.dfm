object FmGraGruXCou: TFmGraGruXCou
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-212 :: Configura'#231#227'o de Reduzido de Couro'
  ClientHeight = 544
  ClientWidth = 930
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 930
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 882
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 834
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 441
        Height = 32
        Caption = 'Configura'#231#227'o de Reduzido de Couro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 441
        Height = 32
        Caption = 'Configura'#231#227'o de Reduzido de Couro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 441
        Height = 32
        Caption = 'Configura'#231#227'o de Reduzido de Couro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 930
    Height = 382
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 930
      Height = 61
      Align = alTop
      Caption = ' Produto em edi'#231#227'o: '
      TabOrder = 0
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 926
        Height = 44
        Align = alClient
        BevelOuter = bvNone
        FullRepaint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label5: TLabel
          Left = 652
          Top = -1
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label6: TLabel
          Left = 68
          Top = -1
          Width = 31
          Height = 13
          Caption = 'Nome:'
        end
        object Label7: TLabel
          Left = 712
          Top = -1
          Width = 49
          Height = 13
          Caption = 'ID N'#237'vel1:'
        end
        object LaNivel1: TLabel
          Left = 8
          Top = 0
          Width = 48
          Height = 13
          Caption = 'Reduzido:'
        end
        object Label8: TLabel
          Left = 772
          Top = -1
          Width = 26
          Height = 13
          Caption = 'GGY:'
        end
        object DBEdit1: TDBEdit
          Left = 652
          Top = 16
          Width = 56
          Height = 21
          DataField = 'GraGru1'
          DataSource = DsGraGruXCou
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 712
          Top = 16
          Width = 56
          Height = 21
          DataField = 'CU_GG1'
          DataSource = DsGraGruXCou
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 68
          Top = 16
          Width = 577
          Height = 21
          DataField = 'NO_PRD_TAM_COR'
          DataSource = DsGraGruXCou
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          DataField = 'GraGruX'
          DataSource = DsGraGruXCou
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 772
          Top = 16
          Width = 56
          Height = 21
          DataField = 'GraGruY'
          DataSource = DsGraGruXCou
          TabOrder = 4
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 61
      Width = 930
      Height = 321
      Align = alClient
      TabOrder = 1
      object GBCouNiv: TGroupBox
        Left = 2
        Top = 15
        Width = 926
        Height = 190
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 16
          Width = 78
          Height = 13
          Caption = 'Tipo de material:'
        end
        object Label4: TLabel
          Left = 340
          Top = 16
          Width = 82
          Height = 13
          Caption = 'Parte do material:'
        end
        object Label11: TLabel
          Left = 672
          Top = 16
          Width = 28
          Height = 13
          Caption = 'PPP*:'
        end
        object Label21: TLabel
          Left = 720
          Top = 16
          Width = 30
          Height = 13
          Caption = 'PMP*:'
        end
        object Label22: TLabel
          Left = 776
          Top = 16
          Width = 28
          Height = 13
          Caption = 'PKP*:'
        end
        object Label13: TLabel
          Left = 16
          Top = 56
          Width = 70
          Height = 13
          Caption = 'M'#233'dia m'#237'n. m'#178':'
        end
        object Label14: TLabel
          Left = 100
          Top = 56
          Width = 71
          Height = 13
          Caption = 'M'#233'dia m'#225'x. m'#178':'
        end
        object Label19: TLabel
          Left = 184
          Top = 56
          Width = 71
          Height = 13
          Caption = 'M'#233'dia m'#237'n. kg:'
        end
        object Label20: TLabel
          Left = 268
          Top = 56
          Width = 72
          Height = 13
          Caption = 'M'#233'dia m'#225'x. kg:'
        end
        object EdCouNiv2: TdmkEditCB
          Left = 16
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrGrGruXCou'
          QryCampo = 'CouNiv2'
          UpdCampo = 'CouNiv2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCouNiv2
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCouNiv2: TdmkDBLookupComboBox
          Left = 72
          Top = 32
          Width = 264
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCouNiv2
          TabOrder = 1
          dmkEditCB = EdCouNiv2
          QryName = 'QrGrGruXCou'
          QryCampo = 'CouNiv2'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCouNiv1: TdmkEditCB
          Left = 340
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrGrGruXCou'
          QryCampo = 'CouNiv1'
          UpdCampo = 'CouNiv1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCouNiv1
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCouNiv1: TdmkDBLookupComboBox
          Left = 396
          Top = 32
          Width = 264
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCouNiv1
          TabOrder = 3
          dmkEditCB = EdCouNiv1
          QryName = 'QrGrGruXCou'
          QryCampo = 'CouNiv1'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdPrevPcPal: TdmkEdit
          Left = 672
          Top = 32
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '110'
          QryCampo = 'PrevPcPal'
          UpdCampo = 'PrevPcPal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 110
          ValWarn = False
        end
        object EdPrevAMPal: TdmkEdit
          Left = 720
          Top = 32
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'PrevAMPal'
          UpdCampo = 'PrevAMPal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPrevKgPal: TdmkEdit
          Left = 776
          Top = 32
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'PrevKgPal'
          UpdCampo = 'PrevKgPal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGBastidao: TdmkRadioGroup
          Left = 16
          Top = 95
          Width = 813
          Height = 41
          Caption = ' Bastid'#227'o: '
          Columns = 8
          ItemIndex = 0
          Items.Strings = (
            'N/D'
            'Integral'
            'Laminado'
            'Dividido tripa'
            'Dividido curtido'
            'Rebaixado '
            'Dividido semi'
            'Rebaix. em semi')
          TabOrder = 11
          QryCampo = 'Grandeza'
          UpdCampo = 'Grandeza'
          UpdType = utYes
          OldValor = 0
        end
        object EdMediaMinM2: TdmkEdit
          Left = 16
          Top = 72
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdMediaMaxM2: TdmkEdit
          Left = 100
          Top = 72
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdMediaMinKg: TdmkEdit
          Left = 184
          Top = 72
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdMediaMaxKg: TdmkEdit
          Left = 268
          Top = 72
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGGrandeza: TdmkRadioGroup
          Left = 16
          Top = 139
          Width = 701
          Height = 41
          Caption = ' Grandeza: '
          Columns = 8
          ItemIndex = 0
          Items.Strings = (
            'Pe'#231'a'
            #193'rea (m'#178')'
            'Peso (kg)'
            'Volume ( m'#179')'
            'Linear (m)'
            '? ? ? (outros)'
            #193'rea (ft'#178')'
            'Peso (t)')
          TabOrder = 12
          QryCampo = 'Grandeza'
          UpdCampo = 'Grandeza'
          UpdType = utYes
          OldValor = 0
        end
        object CkUsaSifDipoa: TdmkCheckBox
          Left = 724
          Top = 152
          Width = 97
          Height = 17
          Caption = 'Usa SIF/DIPOA.'
          TabOrder = 13
          QryCampo = 'UsaSifDipoa'
          UpdCampo = 'UsaSifDipoa'
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
      end
      object GroupBox3: TGroupBox
        Left = 2
        Top = 205
        Width = 926
        Height = 64
        Align = alTop
        Caption = ' Impress'#227'o para clientes: '
        TabOrder = 1
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 922
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 12
            Top = 4
            Width = 30
            Height = 13
            Caption = 'Artigo:'
          end
          object Label2: TLabel
            Left = 360
            Top = 4
            Width = 34
            Height = 13
            Caption = 'Classe:'
          end
          object EdArtigoImp: TdmkEdit
            Left = 12
            Top = 20
            Width = 344
            Height = 21
            MaxLength = 50
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'ArtigoImp'
            UpdCampo = 'ArtigoImp'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdClasseImp: TdmkEdit
            Left = 360
            Top = 20
            Width = 300
            Height = 21
            MaxLength = 30
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'ClasseImp'
            UpdCampo = 'ClasseImp'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
      object Panel3: TPanel
        Left = 2
        Top = 269
        Width = 926
        Height = 50
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        object LaFatorNota: TLabel
          Left = 16
          Top = 0
          Width = 87
          Height = 13
          Caption = 'Fator Nota MPAG:'
          Enabled = False
        end
        object LaGGXInProc: TLabel
          Left = 120
          Top = 0
          Width = 24
          Height = 13
          Caption = '????'
          Enabled = False
        end
        object EdFatorNota: TdmkEdit
          Left = 16
          Top = 16
          Width = 100
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 8
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1,00000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1.000000000000000000
          ValWarn = False
        end
        object EdGGXInProc: TdmkEditCB
          Left = 120
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdCampo = 'GGXInProc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBGGXInProc
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnFormActivate
        end
        object CBGGXInProc: TdmkDBLookupComboBox
          Left = 176
          Top = 16
          Width = 657
          Height = 21
          Enabled = False
          KeyField = 'Controle'
          ListField = 'NO_PRD_TAM_COR'
          ListSource = DsRmsGGX
          TabOrder = 2
          dmkEditCB = EdGGXInProc
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 474
    Width = 930
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 784
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 782
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkRespeitarPai: TCheckBox
        Left = 152
        Top = 4
        Width = 161
        Height = 17
        Caption = 'Respeitar filia'#231#227'o dos n'#237'veis.'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
      object DBCkGradeado: TDBCheckBox
        Left = 152
        Top = 27
        Width = 165
        Height = 17
        Caption = 'N'#227'o criar reduzido em inclus'#227'o.'
        DataField = 'Gradeado'
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 430
    Width = 930
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 926
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PMInsAlt: TPopupMenu
    Left = 388
    Top = 336
    object Inclui1: TMenuItem
      Caption = '&Inclui'
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
    end
  end
  object QrGraGruXCou: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGraGruXCouAfterOpen
    SQL.Strings = (
      'SELECT cn1.Nome NO_CouNiv1, cn2.Nome NO_CouNiv2, '
      'CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nom' +
        'e))) '
      'NO_PRD_TAM_COR,'
      'xco.*, ggx.GraGru1, gg1.CodUsu CU_GG1 '
      'FROM gragruxcou xco'
      'LEFT JOIN gragrux ggx ON ggx.Controle=xco.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY '
      'LEFT JOIN couniv2    cn2 ON cn2.Codigo=xco.CouNiv2'
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1'
      '')
    Left = 276
    Top = 88
    object QrGraGruXCouNO_CouNiv1: TWideStringField
      FieldName = 'NO_CouNiv1'
      Size = 60
    end
    object QrGraGruXCouNO_CouNiv2: TWideStringField
      FieldName = 'NO_CouNiv2'
      Size = 60
    end
    object QrGraGruXCouNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXCouGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXCouCouNiv1: TIntegerField
      FieldName = 'CouNiv1'
    end
    object QrGraGruXCouCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrGraGruXCouArtigoImp: TWideStringField
      FieldName = 'ArtigoImp'
      Size = 50
    end
    object QrGraGruXCouClasseImp: TWideStringField
      FieldName = 'ClasseImp'
      Size = 30
    end
    object QrGraGruXCouPrevPcPal: TIntegerField
      FieldName = 'PrevPcPal'
    end
    object QrGraGruXCouLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGruXCouDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGruXCouDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGruXCouUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGruXCouUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGruXCouAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGruXCouAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGraGruXCouMediaMinM2: TFloatField
      FieldName = 'MediaMinM2'
    end
    object QrGraGruXCouMediaMaxM2: TFloatField
      FieldName = 'MediaMaxM2'
    end
    object QrGraGruXCouMediaMinKg: TFloatField
      FieldName = 'MediaMinKg'
    end
    object QrGraGruXCouMediaMaxKg: TFloatField
      FieldName = 'MediaMaxKg'
    end
    object QrGraGruXCouPrevAMPal: TFloatField
      FieldName = 'PrevAMPal'
    end
    object QrGraGruXCouPrevKgPal: TFloatField
      FieldName = 'PrevKgPal'
    end
    object QrGraGruXCouGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrGraGruXCouBastidao: TSmallintField
      FieldName = 'Bastidao'
    end
    object QrGraGruXCouGGXPronto: TIntegerField
      FieldName = 'GGXPronto'
    end
    object QrGraGruXCouAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrGraGruXCouAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrGraGruXCouGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXCouCU_GG1: TIntegerField
      FieldName = 'CU_GG1'
    end
    object QrGraGruXCouGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrGraGruXCouUsaSifDipoa: TSmallintField
      FieldName = 'UsaSifDipoa'
    end
  end
  object DsGraGruXCou: TDataSource
    DataSet = QrGraGruXCou
    Left = 276
    Top = 136
  end
  object QrCouNiv1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv1'
      'ORDER BY Nome')
    Left = 528
    object QrCouNiv1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCouNiv1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCouNiv1: TDataSource
    DataSet = QrCouNiv1
    Left = 528
    Top = 48
  end
  object QrCouNiv2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv2'
      'ORDER BY Nome')
    Left = 596
    object QrCouNiv2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCouNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCouNiv2: TDataSource
    DataSet = QrCouNiv2
    Left = 596
    Top = 48
  end
  object QrRmsGGX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsribcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 660
    object QrRmsGGXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrRmsGGXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrRmsGGXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrRmsGGXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrRmsGGXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrRmsGGXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsRmsGGX: TDataSource
    DataSet = QrRmsGGX
    Left = 660
    Top = 52
  end
  object Qry: TMySQLQuery
    Database = Dmod.MyDB
    Left = 524
    Top = 108
  end
  object Qr2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 720
    Top = 324
  end
end
