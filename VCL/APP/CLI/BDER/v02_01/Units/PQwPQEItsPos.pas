unit PQwPQEItsPos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, mySQLDbTables,
  frxClass, frxDBSet, Variants, Vcl.Menus;

type
  TFmPQwPQEItsPos = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    frxPQ_NF_Err: TfrxReport;
    frxDsPQ_NF_Err: TfrxDBDataset;
    QrPQ_NF_Err: TmySQLQuery;
    QrPQ_NF_ErrPeso: TFloatField;
    QrPQ_NF_ErrNO_PQ: TWideStringField;
    QrPQ_NF_ErrCliOrig: TIntegerField;
    QrPQ_NF_ErrInsumo: TIntegerField;
    QrPQ_NF_ErrSdoPeso: TFloatField;
    QrPQ_NF_ErrNO_EMP: TWideStringField;
    PB1: TProgressBar;
    DsPQ_NF_Err: TDataSource;
    BtImprime: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrPQ_NF_ErrSdoValr: TFloatField;
    QrPQ_NF_ErrValor: TFloatField;
    BtAtualiza: TBitBtn;
    PMAtualiza: TPopupMenu;
    odosinsumos1: TMenuItem;
    Splitter1: TSplitter;
    PCPsqErros: TPageControl;
    TabSheet2: TTabSheet;
    DBGNFBxaOrf: TDBGrid;
    Panel5: TPanel;
    Bt00Psq: TBitBtn;
    QrNFBxaOrf: TMySQLQuery;
    DsNFBxaOrf: TDataSource;
    QrNFBxaOrfTipo: TIntegerField;
    QrNFBxaOrfOrigemCodi: TIntegerField;
    QrNFBxaOrfOrigemCtrl: TIntegerField;
    QrNFBxaOrfOriTipo: TIntegerField;
    QrNFBxaOrfOriCodi: TIntegerField;
    QrNFBxaOrfOriCtrl: TIntegerField;
    Bt00Corrige: TBitBtn;
    PM00Corrige: TPopupMenu;
    CorrigeSelecionado1: TMenuItem;
    CorrigeTodos1: TMenuItem;
    Query1: TMySQLQuery;
    QrNFBxaOrfDstTipo: TIntegerField;
    QrNFBxaOrfDstCodi: TIntegerField;
    QrNFBxaOrfDstCtrl: TIntegerField;
    Query2: TMySQLQuery;
    BtReabre: TBitBtn;
    TabSheet3: TTabSheet;
    Panel6: TPanel;
    DBGrid2: TDBGrid;
    Panel7: TPanel;
    Bt01Psq: TBitBtn;
    Bt01Recalc: TBitBtn;
    QrAllNFs: TMySQLQuery;
    QrAllNFsDataX: TDateField;
    QrAllNFsTipo: TIntegerField;
    QrAllNFsOrigemCodi: TIntegerField;
    QrAllNFsOrigemCtrl: TIntegerField;
    QrAllNFsCliOrig: TIntegerField;
    QrAllNFsInsumo: TIntegerField;
    QrAllNFsPeso: TFloatField;
    QrAllNFsValor: TFloatField;
    QrAllNFsSdoPeso: TFloatField;
    QrAllNFsSdoValr: TFloatField;
    QrAllNFsSerie: TIntegerField;
    QrAllNFsNF: TIntegerField;
    QrAllNFsxLote: TWideStringField;
    QrAllNFsNO_PQ: TWideStringField;
    QrAllNFsNO_CliInt: TWideStringField;
    DsAllNFs: TDataSource;
    QrPQ_NF_ErrEmpresa: TIntegerField;
    TabSheet4: TTabSheet;
    Panel8: TPanel;
    DBGDtaSuspei: TDBGrid;
    Panel9: TPanel;
    BtDtaSuspeitaPsq: TBitBtn;
    BtDtaSuspeitaCorrige: TBitBtn;
    earg: TMySQLQuery;
    QrTipos: TMySQLQuery;
    QrTiposOrigemCntl: TIntegerField;
    QrTiposTipo: TIntegerField;
    QrDtaSuspei: TMySQLQuery;
    QrDtaSuspeiDataX: TDateField;
    QrDtaSuspeiOrigemCodi: TIntegerField;
    QrDtaSuspeiOrigemctrl: TIntegerField;
    QrDtaSuspeiTipo: TIntegerField;
    QrDtaSuspeiInsumo: TIntegerField;
    QrDtaSuspeiPeso: TFloatField;
    QrDtaSuspeiValor: TFloatField;
    QrDtaSuspeiNO_PQ: TWideStringField;
    QrDtaSuspeiNO_TIPO: TWideStringField;
    DsDtaSuspei: TDataSource;
    PM02Corrige: TPopupMenu;
    Corrigeatual1: TMenuItem;
    CorrigeSelecionados1: TMenuItem;
    CorrigeTodos2: TMenuItem;
    QrDtaSuspeiEmpresa: TIntegerField;
    QrDtaSuspeiCliOrig: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrPQ_NF_ErrBeforeClose(DataSet: TDataSet);
    procedure QrPQ_NF_ErrAfterOpen(DataSet: TDataSet);
    procedure BtAtualizaClick(Sender: TObject);
    procedure odosinsumos1Click(Sender: TObject);
    procedure Bt00CorrigeClick(Sender: TObject);
    procedure Bt00PsqClick(Sender: TObject);
    procedure CorrigeSelecionado1Click(Sender: TObject);
    procedure CorrigeTodos1Click(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure Bt01PsqClick(Sender: TObject);
    procedure Bt01RecalcClick(Sender: TObject);
    procedure BtDtaSuspeitaPsqClick(Sender: TObject);
    procedure QrDtaSuspeiCalcFields(DataSet: TDataSet);
    procedure DBGDtaSuspeiDblClick(Sender: TObject);
    procedure Corrigeatual1Click(Sender: TObject);
    procedure CorrigeSelecionados1Click(Sender: TObject);
    procedure CorrigeTodos2Click(Sender: TObject);
    procedure BtDtaSuspeitaCorrigeClick(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaTodosPQxPositivos();
    procedure ImprimeDiferencasEstoqueNFs();
    procedure Corrige00();
    procedure Corrige02(Quais: TSelType);
    procedure Pesquisa00();

  public
    { Public declarations }
    FAtualizaTodos: Boolean;
    //
    procedure ReopenPQ_NF_Err();
  end;

  var
  FmPQwPQEItsPos: TFmPQwPQEItsPos;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnPQ_PF, ModuleGeral, UMySQLModule, PQx,
  UnVS_PF, MyDBCheck;

{$R *.DFM}

procedure TFmPQwPQEItsPos.AtualizaTodosPQxPositivos();
var
  Qry1, Qry2: TmySQLQuery;
  DstCodi, DstCtrl, DstTipo: Integer;
begin
  if Geral.MB_Pergunta(
  'A atualiza��o de todos itens de NF com saldo positivo pode demorar alguns minutos. Confirma a atualiza��o assim mesmo?') = ID_YES
  then
  begin
    Qry1 := TmySQLQuery.Create(Dmod);
    try
    Qry2 := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT * ',
      'FROM pqx',
      'WHERE SdoPeso > 0',
      '']);
      PB1.Position := 0;
      PB1.Max := Qry1.RecordCount;
      Qry1.First;
      while not Qry1.Eof do
      begin
        MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
        //
        DstCodi   := Qry1.FieldByName('OrigemCodi').AsInteger;
        DstCtrl   := Qry1.FieldByName('OrigemCtrl').AsInteger;
        DstTipo   := Qry1.FieldByName('Tipo').AsInteger;
        PQ_PF.AtualizaSdoPQx(Qry2, DstCodi, DstCtrl, DstTipo);
        //
        Qry1.Next;
      end;
      PB1.Position := 0;
      PB1.Max := 0;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      ReopenPQ_NF_Err();
    finally
      Qry2.Free;
    end;
    finally
      Qry1.Free;
    end;
  end;
end;

procedure TFmPQwPQEItsPos.Bt00CorrigeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PM00Corrige, Bt00Corrige);
end;

procedure TFmPQwPQEItsPos.Bt00PsqClick(Sender: TObject);
begin
  Pesquisa00();
end;

procedure TFmPQwPQEItsPos.Bt01PsqClick(Sender: TObject);
begin
   UnDmkDAC_PF.AbreMySQLQuery0(QrAllNFs, Dmod.MyDB, [
  'SELECT pqx.DataX, pqx.Tipo, pqx.OrigemCodi, pqx.OrigemCtrl,  ',
  'pqx.CliOrig, pqx.Insumo, pqx.Peso, pqx.Valor, ',
  'pqx.SdoPeso, pqx.SdoValr, pqx.Serie, ',
  'pqx.NF, pqx.xLote, pq.Nome NO_PQ, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CliInt  ',
  'FROM pqx pqx ',
  'LEFT JOIN entidades cli ON cli.Codigo=pqx.CliOrig ',
  'LEFT JOIN pq pq ON pq.Codigo=pqx.Insumo ',
  'WHERE pqx.Insumo=' + Geral.FF0(QrPQ_NF_ErrInsumo.Value),
  'AND pqx.CliOrig=' + Geral.FF0(QrPQ_NF_ErrCliOrig.Value),
  'AND pqx.Empresa=' + Geral.FF0(QrPQ_NF_ErrEmpresa.Value),
  'AND pqx.Tipo<>0 ',
  'AND pqx.Peso > 0 ',
  'ORDER BY pqx.DataX DESC, pqx.Tipo DESC,  ',
  '  pqx.OrigemCtrl DESC ',
  '']);
end;

procedure TFmPQwPQEItsPos.Bt01RecalcClick(Sender: TObject);
var
  NovoSdoPeso: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    QrAllNFs.First;
    while not QrAllNFs.Eof do
    begin
      NovoSdoPeso := PQ_PF.AtualizaSdoPQx(Query1, QrAllNFsOrigemCodi.Value,
        QrAllNFsOrigemCtrl.Value, QrAllNFsTipo.Value);
      if ABS(NovoSdoPeso - QrAllNFsSdoPeso.Value) >= 0.001 then
      begin
        if Geral.MB_Pergunta(
        'Foi encontrado diverg�ncia de saldo de quantidade no item:' +
        sLineBreak + 'Tipo: ' + Geral.FF0(QrAllNFsTipo.Value) + sLineBreak +
        'C�digo: ' + Geral.FF0(QrAllNFsOrigemCodi.Value) + sLineBreak +
        'Controle: ' + Geral.FF0(QrAllNFsOrigemCtrl.Value) + sLineBreak +
        'Qtd anterior: ' + Geral.FFT(QrAllNFsSdoPeso.Value, 3, siNegativo) + sLineBreak +
        'Qtd atual: ' + Geral.FFT(NovoSdoPeso, 3, siNegativo) + sLineBreak +
        'Diferen�a: ' + Geral.FFT(NovoSdoPeso - QrAllNFsSdoPeso.Value, 3, siNegativo) +
        sLineBreak + sLineBreak + 'Deseja continuar?') <> ID_YES then Exit;
      end;
      //
      QrAllNFs.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPQwPQEItsPos.BtAtualizaClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMAtualiza, BtAtualiza);
end;

procedure TFmPQwPQEItsPos.BtDtaSuspeitaCorrigeClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PM02Corrige, BtDtaSuspeitaCorrige);
end;

procedure TFmPQwPQEItsPos.BtDtaSuspeitaPsqClick(Sender: TObject);
var
  DataBal: TDateTime;
  xDtaBal, SQL_Tipos: String;
begin
  DataBal := UnPQx.ObtemDataBal();
  xDtaBal := Geral.FDT(DataBal, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTipos, Dmod.MyDB, [
  'SELECT MIN(OrigemCtrl) OrigemCntl, Tipo',
  'FROM pqx',
  'WHERE DataX>="' + xDtaBal + '"',
  'GROUP BY Tipo',
  '']);
  //
  SQL_Tipos := '';
  if QrTipos.RecordCount > 0 then
  begin
    while not QrTipos.Eof do
    begin
      if QrTipos.RecNo > 1 then
        SQL_Tipos := SQL_Tipos + '  OR ' + sLineBreak;
      SQL_Tipos := SQL_Tipos + '  (Tipo = ' +
        Geral.FF0(QrTiposTipo.Value) +
        ' AND OrigemCtrl > ' +
        Geral.FF0(QrTiposOrigemCntl.Value) +
        ')';
      //
      QrTipos.Next;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrDtaSuspei, Dmod.MyDB, [
    'SELECT pqx.DataX, pqx.OrigemCodi, pqx.Origemctrl,',
    'pqx.Tipo, pqx.Insumo, pqx.Peso, pqx.Valor,',
    'pqx.Empresa, pqx.CliOrig, ',
    'pq.Nome NO_PQ  ',
    'FROM pqx pqx',
    'LEFT JOIN pq pq ON pq.Codigo=pqx.Insumo',
    'WHERE DataX<"' + xDtaBal + '" ',
    'AND  ',
    '( ',
    SQL_Tipos,
    ')',
    '']);
  end else
    QrDtaSuspei.Close;
end;

procedure TFmPQwPQEItsPos.BtImprimeClick(Sender: TObject);
begin
  ImprimeDiferencasEstoqueNFs();
end;

procedure TFmPQwPQEItsPos.BtReabreClick(Sender: TObject);
begin
  ReopenPQ_NF_Err();
end;

procedure TFmPQwPQEItsPos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQwPQEItsPos.Corrigeatual1Click(Sender: TObject);
begin
  Corrige02(TSelType.istAtual);
end;

procedure TFmPQwPQEItsPos.Corrige00();
begin
  Screen.Cursor := crHourGlass;
  try
    // Confirmar que realmente n�o existe mais o lan�amento no pqx, apenas no pqw!
    UnDmkDAC_PF.AbreMySQLQuery0(Query1, Dmod.MyDB, [
    'SELECT OrigemCtrl ',
    'FROM pqx ',
    'WHERE Tipo=' + Geral.FF0(QrNFBxaOrfOriTipo.Value),
    'AND OrigemCodi=' + Geral.FF0(QrNFBxaOrfOriCodi.Value),
    'AND OrigemCtrl=' + Geral.FF0(QrNFBxaOrfOriCtrl.Value),
    '']);
    // se realmente n�o existe...
    if Query1.Fields[0].AsInteger = 0 then
    begin
      // ... ent�o excluir no pqw...
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM pqw WHERE OriTipo=' +
        Geral.FF0(QrNFBxaOrfOriTipo.Value) + ' AND OriCodi=' +
        Geral.FF0(QrNFBxaOrfOriCodi.Value) + ' AND OriCtrl=' +
        Geral.FF0(QrNFBxaOrfOriCtrl.Value) + ' AND DstTipo=' +
        Geral.FF0(QrNFBxaOrfDstTipo.Value) + ' AND DstCodi=' +
        Geral.FF0(QrNFBxaOrfDstCodi.Value) + ' AND DstCtrl=' +
        Geral.FF0(QrNFBxaOrfDstCtrl.Value) + '');
      // ... e recalcular o saldo do pqx de entrada do pwq exclu�do.
      PQ_PF.AtualizaSdoPQx(Query2, QrNFBxaOrfDstCodi.Value,
        QrNFBxaOrfDstCtrl.Value, QrNFBxaOrfDstTipo.Value);
    end;
  finally
    Screen.Cursor := crHourGlass;
  end;
end;

procedure TFmPQwPQEItsPos.Corrige02(Quais: TSelType);
var
  MinData, Agora, DtSelect: TDateTime;
  I: Integer;
  xDtSelect: String;
  //
  procedure AtualizaAtual();
  begin
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
    'UPDATE pqx SET DataX="' + xDtSelect + '" ',
    'WHERE Tipo=' + Geral.FF0(QrDtaSuspeiTipo.Value),
    'AND Origemctrl=' + Geral.FF0(QrDtaSuspeiOrigemctrl.Value),
    '']));
    //
    UnPQx.AtualizaEstoquePQ(QrDtaSuspeiCliOrig.Value, QrDtaSuspeiInsumo.Value,
      QrDtaSuspeiEmpresa.Value, aeMsg, CO_VAZIO);
  end;
begin
  if (QrDtaSuspei.State = dsInactive) or (QrDtaSuspei.RecordCount = 0) then
  begin
    Geral.MB_Info('Nenhum item foi selecionado!');
  end else
  begin
    if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
    //
    MinData := UnPQx.ObtemDataBal();
    Agora := DModG.ObtemAgora();
    if not DBCheck.ObtemData(Agora, DtSelect, MinData) then Exit;
    xDtSelect := Geral.FDT(DtSelect, 1);
    //
    QrDtaSuspei.DisableControls;
    Screen.Cursor := crHourGlass;
    try
      case Quais of
        istSelecionados:
        begin
          with DBGDtaSuspei.DataSource.DataSet do
          for I := 0 to DBGDtaSuspei.SelectedRows.Count-1 do
          begin
            GotoBookmark(DBGDtaSuspei.SelectedRows.Items[I]);
            //
            AtualizaAtual();
          end;
        end;
        istTodos:
        begin
          QrDtaSuspei.First;
          while not QrDtaSuspei.Eof do
          begin
            AtualizaAtual();
            //
            QrDtaSuspei.Next;
          end;
        end;
        istAtual: AtualizaAtual();
      end;
      //
      BtDtaSuspeitaPsqClick(Self);
    finally
      QrDtaSuspei.EnableControls;
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPQwPQEItsPos.CorrigeSelecionado1Click(Sender: TObject);
begin
  Corrige00();
  Pesquisa00();
  if QrNFBxaOrf.RecordCount = 0 then
    ReopenPQ_NF_Err();
end;

procedure TFmPQwPQEItsPos.CorrigeSelecionados1Click(Sender: TObject);
begin
  Corrige02(TSelType.istSelecionados);
end;

procedure TFmPQwPQEItsPos.CorrigeTodos1Click(Sender: TObject);
begin
  QrNFBxaOrf.First;
  while not QrNFBxaOrf.Eof do
  begin
    Corrige00();
    //
    QrNFBxaOrf.Next;
  end;
  Pesquisa00();
  if QrNFBxaOrf.RecordCount = 0 then
    ReopenPQ_NF_Err();
end;

procedure TFmPQwPQEItsPos.CorrigeTodos2Click(Sender: TObject);
begin
  Corrige02(TSelType.istTodos);
end;

procedure TFmPQwPQEItsPos.DBGDtaSuspeiDblClick(Sender: TObject);
begin
// Pare aqui 2024-01-03
end;

procedure TFmPQwPQEItsPos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ReopenPQ_NF_Err();
  if FAtualizaTodos then
  begin
    FAtualizaTodos := False;
    AtualizaTodosPQxPositivos();
  end;
end;

procedure TFmPQwPQEItsPos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualizaTodos := False;
  PCPsqErros.ActivePageIndex := 0;
end;

procedure TFmPQwPQEItsPos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQwPQEItsPos.ImprimeDiferencasEstoqueNFs();
begin
  MyObjects.frxDefineDataSets(frxPQ_NF_Err, [
    frxDsPQ_NF_Err,
    DModG.frxDsDono]);
  //
  MyObjects.frxMostra(frxPQ_NF_Err, 'Diferen�as esoque NFs');
end;

procedure TFmPQwPQEItsPos.odosinsumos1Click(Sender: TObject);
begin
  AtualizaTodosPQxPositivos();
end;

procedure TFmPQwPQEItsPos.Pesquisa00();
begin
  if (QrPQ_NF_Err.State <> dsInactive) and (QrPQ_NF_Err.RecordCount > 0) then
  begin
    Screen.Cursor := crHourGlass;
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QrNFBxaOrf, DModG.MyPID_DB, [
      'DROP TABLE IF EXISTS _pqx_pqw_a_; ',
      'CREATE TABLE _pqx_pqw_a_ ',
      'SELECT DISTINCT Tipo, OrigemCodi, OrigemCtrl ',
      'FROM ' + TMeuDB + '.pqx ',
      'WHERE Insumo=' + Geral.FF0(QrPQ_NF_ErrInsumo.Value),
      'AND CliOrig=' + Geral.FF0(QrPQ_NF_ErrCliOrig.Value),
      'AND Empresa=' + Geral.FF0(QrPQ_NF_ErrEmpresa.Value),
      'AND Peso<0 ',
      'AND Tipo <> 0 ',
      '; ',
      'DROP TABLE IF EXISTS _pqx_pqw_b_; ',
      'CREATE TABLE _pqx_pqw_b_ ',
      'SELECT DISTINCT OriTipo, OriCodi, OriCtrl, ',
      'DstTipo, DstCodi, DstCtrl ',
      'FROM ' + TMeuDB + '.pqw ',
      'WHERE DstInsumo=' + Geral.FF0(QrPQ_NF_ErrInsumo.Value),
      'AND DstCliInt=' + Geral.FF0(QrPQ_NF_ErrCliOrig.Value),
      'AND DstEmpresa=' + Geral.FF0(QrPQ_NF_ErrEmpresa.Value),
      '; ',
      'SELECT a.*, b.* ',
      'FROM _pqx_pqw_b_ b ',
      'LEFT JOIN _pqx_pqw_a_ a ON  ',
      '  a.Tipo=b.OriTipo  ',
      '  AND a.OrigemCodi=b.OriCodi ',
      '  AND a.OrigemCtrl=b.OriCtrl ',
      'WHERE a.OrigemCtrl IS NULL ',
      '']);
    finally
      Screen.Cursor := crDefault;
    end;
  end else
    Geral.MB_Aviso('N�o h� item para pesquisa!');
end;

procedure TFmPQwPQEItsPos.QrDtaSuspeiCalcFields(DataSet: TDataSet);
begin
  QrDtaSuspeiNO_TIPO.Value := DmkEnums.NomeFatID_All(QrDtaSuspeiTipo.Value);
end;

procedure TFmPQwPQEItsPos.QrPQ_NF_ErrAfterOpen(DataSet: TDataSet);
begin
  //BtAcoes.Enabled := True;
  BtImprime.Enabled := True;
  BtAtualiza.Enabled := True;
end;

procedure TFmPQwPQEItsPos.QrPQ_NF_ErrBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
  BtAtualiza.Enabled := False;
end;

procedure TFmPQwPQEItsPos.ReopenPQ_NF_Err();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQ_NF_Err, DmodG.MyPID_DB, [
  'DROP TABLE IF EXISTS _pq_compara_estq_nfs_; ',
  'CREATE TABLE _pq_compara_estq_nfs_ ',
  'SELECT pqx.Empresa, pqx.CliOrig, pqx.Insumo, ',
  'SUM(pqx.SdoPeso) SdoPeso, ',
  'SUM(pqx.SdoValr) SdoValr ',
  'FROM ' + TMeuDB + '.pqx pqx ',
  'WHERE pqx.SdoPeso>0 ',
  'GROUP BY pqx.CliOrig, pqx.Insumo; ',
  'SELECT pcl.Peso, pcl.Valor, pq_.Nome NO_PQ, pqx.*,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMP ',
  'FROM _pq_compara_estq_nfs_ pqx',
  'LEFT JOIN ' + TMeuDB + '.pqcli pcl ON pcl.CI=pqx.CliOrig ',
  'LEFT JOIN ' + TMeuDB + '.pq pq_ ON pq_.Codigo=pqx.Insumo',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=CliOrig',
  'WHERE pcl.PQ=pqx.Insumo  ',
  'AND pcl.Peso <> pqx.SdoPeso ',
  '']);
  //Geral.MB_Teste(QrPQ_NF_Err.SQL.Text);
end;

end.
