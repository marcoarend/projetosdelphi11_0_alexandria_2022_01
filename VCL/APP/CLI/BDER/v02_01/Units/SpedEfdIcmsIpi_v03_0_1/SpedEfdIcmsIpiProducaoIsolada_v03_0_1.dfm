object FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1: TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_1
  Left = 0
  Top = 0
  Caption = 'FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1'
  ClientHeight = 432
  ClientWidth = 894
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object QrDatas: TmySQLQuery
    Database = Dmod.MyDB
    Left = 40
    Top = 4
    object QrDatasMinDH: TDateTimeField
      FieldName = 'MinDH'
    end
    object QrDatasMaxDH: TDateTimeField
      FieldName = 'MaxDH'
    end
  end
  object QrOri: TmySQLQuery
    Database = Dmod.MyDB
    Left = 40
    Top = 52
  end
  object QrMae: TmySQLQuery
    Database = Dmod.MyDB
    Left = 40
    Top = 100
  end
  object QrMC: TmySQLQuery
    Database = Dmod.MyDB
    Left = 140
    Top = 4
    object QrMCMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
  end
  object QrProdRib: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _SPED_EFD_K2XX_O_P;'
      'CREATE TABLE _SPED_EFD_K2XX_O_P'
      'SELECT DATE(vmi.DataHora) Data,  cab.GGXDst GGX_Dst, '
      'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, med.Grandeza'
      'FROM bluederm_meza.vsopecab cab'
      
        'LEFT JOIN bluederm_meza.vsmovits vmi ON vmi.MovimCod=cab.MovimCo' +
        'd'
      'LEFT JOIN bluederm_meza.gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm_meza.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm_meza.unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59"'
      'AND cab.GGXDst <> vmi.GragruX'
      'AND MovimNiv=9'
      'UNION'
      'SELECT DATE(vmi.DataHora) Data,  cab.GGXDst GGX_Dst, '
      'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, med.Grandeza'
      'FROM bluederm_meza.vspwecab cab'
      
        'LEFT JOIN bluederm_meza.vsmovits vmi ON vmi.MovimCod=cab.MovimCo' +
        'd'
      'LEFT JOIN bluederm_meza.gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm_meza.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm_meza.unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59"'
      'AND cab.GGXDst <> vmi.GragruX'
      'AND MovimNiv=22'
      ';'
      'SELECT * FROM _SPED_EFD_K2XX_O_P')
    Left = 136
    Top = 52
    object QrProdRibData: TDateField
      FieldName = 'Data'
    end
    object QrProdRibGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrProdRibGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrProdRibPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrProdRibAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrProdRibPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrProdRibMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrProdRibCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdRibMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrProdRibControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProdRibEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrProdRibQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrProdRibQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrProdRibQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrProdRibsTipoEstq: TFloatField
      FieldName = 'sTipoEstq'
    end
    object QrProdRibdTipoEstq: TFloatField
      FieldName = 'dTipoEstq'
    end
    object QrProdRibMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrProdRibDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrProdRibClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrProdRibFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrProdRibEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
  end
  object QrObtCliForSite: TmySQLQuery
    Database = Dmod.MyDB
    Left = 136
    Top = 100
  end
  object QrIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 240
    Top = 4
  end
  object QrCalToCur: TmySQLQuery
    Database = Dmod.MyDB
    Left = 336
    Top = 4
    object QrCalToCurDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
  end
  object QrConsParc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes,'
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, Sum(PesoKg) PesoKg '
      'FROM vsmovits '
      'GROUP BY Ano, Mes'
      '')
    Left = 336
    Top = 52
    object QrConsParcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrConsParcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrConsParcPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrConsParcAno: TFloatField
      FieldName = 'Ano'
    end
    object QrConsParcMes: TFloatField
      FieldName = 'Mes'
    end
  end
  object QrProdParc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes,'
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, Sum(PesoKg) PesoKg '
      'FROM vsmovits '
      'GROUP BY Ano, Mes'
      '')
    Left = 336
    Top = 100
    object QrProdParcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrProdParcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrProdParcPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrProdParcClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrProdParcFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrProdParcControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProdParcAno: TFloatField
      FieldName = 'Ano'
    end
    object QrProdParcMes: TFloatField
      FieldName = 'Mes'
    end
  end
  object QrPeriodos: TmySQLQuery
    Database = Dmod.MyDB
    Left = 336
    Top = 148
    object QrPeriodosAno: TFloatField
      FieldName = 'Ano'
    end
    object QrPeriodosMes: TFloatField
      FieldName = 'Mes'
    end
    object QrPeriodosPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
  end
  object QrVmiGArCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE(DataHora) Data, MovimCod, GraGruX, '
      'Pecas, AreaM2, PesoKg, MovimTwn '
      'FROM vsmovits '
      'WHERE MovimNiv=14 '
      'AND DATE(DataHora) BETWEEN "2016-09-01" AND "2016-09-30" '
      'AND Empresa=-11 '
      'ORDER BY Data, MovimCod, GraGruX ')
    Left = 432
    Top = 4
    object QrVmiGArCabData: TDateField
      FieldName = 'Data'
    end
    object QrVmiGArCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVmiGArCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVmiGArCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVmiGArCabPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVmiGArCabAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVmiGArCabPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVmiGArCabMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVmiGArCabUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrVmiGArCabmed_grandeza: TIntegerField
      FieldName = 'med_grandeza'
    end
    object QrVmiGArCabxco_grandeza: TIntegerField
      FieldName = 'xco_grandeza'
    end
    object QrVmiGArCabGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVmiGArCabCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrVmiGArCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVmiGArCabTipoEstq: TFloatField
      FieldName = 'TipoEstq'
    end
    object QrVmiGArCabDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVmiGArCabClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVmiGArCabFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVmiGArCabEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
  end
  object QrSumGAR: TmySQLQuery
    Database = Dmod.MyDB
    Left = 432
    Top = 52
    object QrSumGARPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumGARAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumGARPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object QrVmiGArIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE(DataHora) Data, MovimCod, GraGruX, '
      'Pecas, AreaM2, PesoKg, MovimTwn '
      'FROM vsmovits '
      'WHERE MovimNiv=14 '
      'AND DATE(DataHora) BETWEEN "2016-09-01" AND "2016-09-30" '
      'AND Empresa=-11 '
      'ORDER BY Data, MovimCod, GraGruX ')
    Left = 432
    Top = 100
    object QrVmiGArItsData: TDateField
      FieldName = 'Data'
    end
    object QrVmiGArItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVmiGArItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVmiGArItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVmiGArItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVmiGArItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVmiGArItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVmiGArItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVmiGArItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrVmiGArItsmed_grandeza: TIntegerField
      FieldName = 'med_grandeza'
    end
    object QrVmiGArItsxco_grandeza: TIntegerField
      FieldName = 'xco_grandeza'
    end
    object QrVmiGArItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVmiGArItsCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrVmiGArItsTipoEstq: TLargeintField
      FieldName = 'TipoEstq'
    end
    object QrVmiGArItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVmiGArItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVmiGArItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVmiGArItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVmiGArItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVmiGArItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
  end
end
