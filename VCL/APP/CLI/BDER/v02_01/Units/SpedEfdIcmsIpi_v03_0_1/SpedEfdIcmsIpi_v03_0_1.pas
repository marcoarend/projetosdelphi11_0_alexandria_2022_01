unit SpedEfdIcmsIpi_v03_0_1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB,
  frxClass, frxDBSet, mySQLDbTables, Vcl.StdCtrls, Vcl.Buttons,
  DmkGeral, UnDmkEnums, UnDmkProcFunc, UnAppPF, UnProjGroup_Vars, AppListas,
  UnProjGroup_Consts, SPED_Listas, UnMyObjects;

type
  TDfSEII_v03_0_1 = class(TForm)
    QrE001: TmySQLQuery;
    QrE001NO_ENT: TWideStringField;
    QrE001MES_ANO: TWideStringField;
    QrE001ImporExpor: TSmallintField;
    QrE001AnoMes: TIntegerField;
    QrE001Empresa: TIntegerField;
    QrE001LinArq: TIntegerField;
    QrE001REG: TWideStringField;
    QrE001IND_MOV: TWideStringField;
    DsE001: TDataSource;
    QrE100: TmySQLQuery;
    QrE100ImporExpor: TSmallintField;
    QrE100AnoMes: TIntegerField;
    QrE100Empresa: TIntegerField;
    QrE100LinArq: TIntegerField;
    QrE100REG: TWideStringField;
    QrE100DT_INI: TDateField;
    QrE100DT_FIN: TDateField;
    DsE100: TDataSource;
    QrE110: TmySQLQuery;
    QrE110ImporExpor: TSmallintField;
    QrE110AnoMes: TIntegerField;
    QrE110Empresa: TIntegerField;
    QrE110LinArq: TIntegerField;
    QrE110E100: TIntegerField;
    QrE110REG: TWideStringField;
    QrE110VL_TOT_DEBITOS: TFloatField;
    QrE110VL_AJ_DEBITOS: TFloatField;
    QrE110VL_TOT_AJ_DEBITOS: TFloatField;
    QrE110VL_TOT_CREDITOS: TFloatField;
    QrE110VL_ESTORNOS_CRED: TFloatField;
    QrE110VL_AJ_CREDITOS: TFloatField;
    QrE110VL_TOT_AJ_CREDITOS: TFloatField;
    QrE110VL_ESTORNOS_DEB: TFloatField;
    QrE110VL_SLD_CREDOR_ANT: TFloatField;
    QrE110VL_SLD_APURADO: TFloatField;
    QrE110VL_TOT_DED: TFloatField;
    QrE110VL_ICMS_RECOLHER: TFloatField;
    QrE110VL_SLD_CREDOR_TRANSPORTAR: TFloatField;
    QrE110DEB_ESP: TFloatField;
    DsE110: TDataSource;
    QrE111: TmySQLQuery;
    QrE111ImporExpor: TSmallintField;
    QrE111AnoMes: TIntegerField;
    QrE111Empresa: TIntegerField;
    QrE111LinArq: TIntegerField;
    QrE111E110: TIntegerField;
    QrE111REG: TWideStringField;
    QrE111COD_AJ_APUR: TWideStringField;
    QrE111DESCR_COMPL_AJ: TWideStringField;
    QrE111VL_AJ_APUR: TFloatField;
    DsE111: TDataSource;
    QrE115: TmySQLQuery;
    QrE115ImporExpor: TSmallintField;
    QrE115AnoMes: TIntegerField;
    QrE115Empresa: TIntegerField;
    QrE115LinArq: TIntegerField;
    QrE115E110: TIntegerField;
    QrE115REG: TWideStringField;
    QrE115COD_INF_ADIC: TWideStringField;
    QrE115VL_INF_ADIC: TFloatField;
    QrE115DESCR_COMPL_AJ: TWideStringField;
    DsE115: TDataSource;
    QrE116: TmySQLQuery;
    QrE116ImporExpor: TSmallintField;
    QrE116AnoMes: TIntegerField;
    QrE116Empresa: TIntegerField;
    QrE116LinArq: TIntegerField;
    QrE116E110: TIntegerField;
    QrE116REG: TWideStringField;
    QrE116COD_OR: TWideStringField;
    QrE116VL_OR: TFloatField;
    QrE116DT_VCTO: TDateField;
    QrE116COD_REC: TWideStringField;
    QrE116NUM_PROC: TWideStringField;
    QrE116IND_PROC: TWideStringField;
    QrE116PROC: TWideStringField;
    QrE116TXT_COMPL: TWideStringField;
    QrE116MES_REF: TDateField;
    DsE116: TDataSource;
    QrE112: TmySQLQuery;
    QrE112ImporExpor: TSmallintField;
    QrE112AnoMes: TIntegerField;
    QrE112Empresa: TIntegerField;
    QrE112LinArq: TIntegerField;
    QrE112E111: TIntegerField;
    QrE112REG: TWideStringField;
    QrE112NUM_DA: TWideStringField;
    QrE112NUM_PROC: TWideStringField;
    QrE112IND_PROC: TWideStringField;
    QrE112PROC: TWideStringField;
    QrE112TXT_COMPL: TWideStringField;
    DsE112: TDataSource;
    QrE113: TmySQLQuery;
    QrE113ImporExpor: TSmallintField;
    QrE113AnoMes: TIntegerField;
    QrE113Empresa: TIntegerField;
    QrE113LinArq: TIntegerField;
    QrE113E111: TIntegerField;
    QrE113REG: TWideStringField;
    QrE113COD_PART: TWideStringField;
    QrE113COD_MOD: TWideStringField;
    QrE113SER: TWideStringField;
    QrE113SUB: TWideStringField;
    QrE113NUM_DOC: TIntegerField;
    QrE113DT_DOC: TDateField;
    QrE113COD_ITEM: TWideStringField;
    QrE113VL_AJ_ITEM: TFloatField;
    DsE113: TDataSource;
    QrE500: TmySQLQuery;
    QrE500ImporExpor: TSmallintField;
    QrE500AnoMes: TIntegerField;
    QrE500Empresa: TIntegerField;
    QrE500LinArq: TIntegerField;
    QrE500REG: TWideStringField;
    QrE500IND_APUR: TWideStringField;
    QrE500DT_INI: TDateField;
    QrE500DT_FIN: TDateField;
    QrE500NO_IND_APUR: TWideStringField;
    DsE500: TDataSource;
    QrE510: TmySQLQuery;
    QrE510ImporExpor: TSmallintField;
    QrE510AnoMes: TIntegerField;
    QrE510Empresa: TIntegerField;
    QrE510LinArq: TIntegerField;
    QrE510E500: TIntegerField;
    QrE510REG: TWideStringField;
    QrE510CFOP: TIntegerField;
    QrE510CST_IPI: TWideStringField;
    QrE510VL_CONT_IPI: TFloatField;
    QrE510VL_BC_IPI: TFloatField;
    QrE510VL_IPI: TFloatField;
    QrE510Lk: TIntegerField;
    QrE510DataCad: TDateField;
    QrE510DataAlt: TDateField;
    QrE510UserCad: TIntegerField;
    QrE510UserAlt: TIntegerField;
    QrE510AlterWeb: TSmallintField;
    QrE510Ativo: TSmallintField;
    DsE510: TDataSource;
    QrE520: TmySQLQuery;
    QrE520ImporExpor: TSmallintField;
    QrE520AnoMes: TIntegerField;
    QrE520Empresa: TIntegerField;
    QrE520LinArq: TIntegerField;
    QrE520E500: TIntegerField;
    QrE520REG: TWideStringField;
    QrE520VL_SD_ANT_IPI: TFloatField;
    QrE520VL_DEB_IPI: TFloatField;
    QrE520VL_CRED_IPI: TFloatField;
    QrE520VL_OD_IPI: TFloatField;
    QrE520VL_OC_IPI: TFloatField;
    QrE520VL_SC_IPI: TFloatField;
    QrE520VL_SD_IPI: TFloatField;
    QrE520Lk: TIntegerField;
    QrE520DataCad: TDateField;
    QrE520DataAlt: TDateField;
    QrE520UserCad: TIntegerField;
    QrE520UserAlt: TIntegerField;
    QrE520AlterWeb: TSmallintField;
    QrE520Ativo: TSmallintField;
    DsE520: TDataSource;
    QrE530: TmySQLQuery;
    QrE530ImporExpor: TSmallintField;
    QrE530AnoMes: TIntegerField;
    QrE530Empresa: TIntegerField;
    QrE530LinArq: TIntegerField;
    QrE530E520: TIntegerField;
    QrE530REG: TWideStringField;
    QrE530IND_AJ: TWideStringField;
    QrE530VL_AJ: TFloatField;
    QrE530COD_AJ: TWideStringField;
    QrE530IND_DOC: TWideStringField;
    QrE530NUM_DOC: TWideStringField;
    QrE530DESCR_AJ: TWideStringField;
    DsE530: TDataSource;
    QrH005: TmySQLQuery;
    QrH005ImporExpor: TSmallintField;
    QrH005AnoMes: TIntegerField;
    QrH005Empresa: TIntegerField;
    QrH005LinArq: TIntegerField;
    QrH005REG: TWideStringField;
    QrH005DT_INV: TDateField;
    QrH005VL_INV: TFloatField;
    QrH005MOT_INV: TWideStringField;
    QrH005NO_MOT_INV: TWideStringField;
    QrH005Lk: TIntegerField;
    QrH005DataCad: TDateField;
    QrH005DataAlt: TDateField;
    QrH005UserCad: TIntegerField;
    QrH005UserAlt: TIntegerField;
    QrH005AlterWeb: TSmallintField;
    QrH005Ativo: TSmallintField;
    DsH005: TDataSource;
    QrH010: TmySQLQuery;
    QrH010ImporExpor: TSmallintField;
    QrH010AnoMes: TIntegerField;
    QrH010Empresa: TIntegerField;
    QrH010H005: TIntegerField;
    QrH010LinArq: TIntegerField;
    QrH010REG: TWideStringField;
    QrH010COD_ITEM: TWideStringField;
    QrH010UNID: TWideStringField;
    QrH010QTD: TFloatField;
    QrH010VL_UNIT: TFloatField;
    QrH010VL_ITEM: TFloatField;
    QrH010IND_PROP: TWideStringField;
    QrH010COD_PART: TWideStringField;
    QrH010TXT_COMPL: TWideStringField;
    QrH010COD_CTA: TWideStringField;
    QrH010Lk: TIntegerField;
    QrH010DataCad: TDateField;
    QrH010DataAlt: TDateField;
    QrH010UserCad: TIntegerField;
    QrH010UserAlt: TIntegerField;
    QrH010AlterWeb: TSmallintField;
    QrH010Ativo: TSmallintField;
    QrH010VL_ITEM_IR: TFloatField;
    QrH010NO_PRD_TAM_COR: TWideStringField;
    QrH010NO_PART: TWideStringField;
    QrH010BalID: TIntegerField;
    QrH010BalNum: TIntegerField;
    QrH010BalItm: TIntegerField;
    QrH010BalEnt: TIntegerField;
    DsH010: TDataSource;
    QrH020: TmySQLQuery;
    QrH020ImporExpor: TSmallintField;
    QrH020AnoMes: TIntegerField;
    QrH020Empresa: TIntegerField;
    QrH020H010: TIntegerField;
    QrH020LinArq: TIntegerField;
    QrH020REG: TWideStringField;
    QrH020CST_ICMS: TIntegerField;
    QrH020BC_ICMS: TFloatField;
    QrH020VL_ICMS: TFloatField;
    QrH020Lk: TIntegerField;
    QrH020DataCad: TDateField;
    QrH020DataAlt: TDateField;
    QrH020UserCad: TIntegerField;
    QrH020UserAlt: TIntegerField;
    QrH020AlterWeb: TSmallintField;
    QrH020Ativo: TSmallintField;
    DsH020: TDataSource;
    frxErrEmpresa: TfrxReport;
    frxDsErrEmpresa: TfrxDBDataset;
    QrErrEmpresa: TmySQLQuery;
    QrErrEmpresaSrcMovID: TIntegerField;
    QrErrEmpresaSrcNivel1: TIntegerField;
    QrErrEmpresaSrcNivel2: TIntegerField;
    QrErrEmpresaCodigo: TIntegerField;
    QrErrEmpresaMovimID: TIntegerField;
    QrErrEmpresaMovimCod: TIntegerField;
    QrErrEmpresaEmpresa: TIntegerField;
    QrErrEmpresaGraGruX: TIntegerField;
    QrErrEmpresaPecas: TFloatField;
    QrErrEmpresaAreaM2: TFloatField;
    QrErrEmpresaPesoKg: TFloatField;
    QrErrEmpresaGrandeza: TSmallintField;
    QrErrEmpresaNO_PRD_TAM_COR: TWideStringField;
    DsK220: TDataSource;
    QrK220: TmySQLQuery;
    DsK200: TDataSource;
    QrK200: TmySQLQuery;
    DsK100: TDataSource;
    QrK100: TmySQLQuery;
    QrK210: TmySQLQuery;
    DsK210: TDataSource;
    QrK215: TmySQLQuery;
    DsK215: TDataSource;
    QrK230: TmySQLQuery;
    DsK230: TDataSource;
    QrK235: TmySQLQuery;
    DsK235: TDataSource;
    frxE_K220_001: TfrxReport;
    frxDsK220: TfrxDBDataset;
    frxDsK270_220: TfrxDBDataset;
    frxDsK275_220: TfrxDBDataset;
    frxDsK255: TfrxDBDataset;
    frxDsK250: TfrxDBDataset;
    frxDsK235: TfrxDBDataset;
    frxDsK230: TfrxDBDataset;
    frxE_K230_001: TfrxReport;
    DsK255: TDataSource;
    QrK255: TmySQLQuery;
    DsK250: TDataSource;
    QrK250: TmySQLQuery;
    Qr1010: TmySQLQuery;
    Qr1010ImporExpor: TSmallintField;
    Qr1010AnoMes: TIntegerField;
    Qr1010Empresa: TIntegerField;
    Qr1010LinArq: TIntegerField;
    Qr1010REG: TWideStringField;
    Qr1010IND_EXP: TWideStringField;
    Qr1010IND_CCRF: TWideStringField;
    Qr1010IND_COMB: TWideStringField;
    Qr1010IND_USINA: TWideStringField;
    Qr1010IND_VA: TWideStringField;
    Qr1010IND_EE: TWideStringField;
    Qr1010IND_CART: TWideStringField;
    Qr1010IND_FORM: TWideStringField;
    Qr1010IND_AER: TWideStringField;
    Qr1010Lk: TIntegerField;
    Qr1010DataCad: TDateField;
    Qr1010DataAlt: TDateField;
    Qr1010UserCad: TIntegerField;
    Qr1010UserAlt: TIntegerField;
    Qr1010AlterWeb: TSmallintField;
    Qr1010Ativo: TSmallintField;
    Ds1010: TDataSource;
    QrK270_220: TmySQLQuery;
    DsK270_220: TDataSource;
    QrK275_220: TmySQLQuery;
    DsK275_220: TDataSource;
    frxE_K_VERIF_001: TfrxReport;
    frxDsK_ConfG: TfrxDBDataset;
    QrK270_230: TmySQLQuery;
    DsK270_230: TDataSource;
    QrK275_235: TmySQLQuery;
    DsK275_235: TDataSource;
    QrK270_250: TmySQLQuery;
    DsK270_250: TDataSource;
    QrK275_255: TmySQLQuery;
    QrK275_255Grandeza: TSmallintField;
    DsK275_255: TDataSource;
    QrK260: TmySQLQuery;
    DsK260: TDataSource;
    QrK265: TmySQLQuery;
    DsK265: TDataSource;
    QrK270_260: TmySQLQuery;
    DsK270_260: TDataSource;
    QrK275_265: TmySQLQuery;
    QrK275_265Grandeza: TSmallintField;
    DsK275_265: TDataSource;
    frxDsK270_230: TfrxDBDataset;
    frxDsK275_235: TfrxDBDataset;
    frxDsK210: TfrxDBDataset;
    frxDsK215: TfrxDBDataset;
    QrK280: TmySQLQuery;
    DsK280: TDataSource;
    QrK_ConfG: TmySQLQuery;
    QrK_ConfGGraGruX: TIntegerField;
    QrK_ConfGSigla: TWideStringField;
    QrK_ConfGNO_PRD_TAM_COR: TWideStringField;
    QrK_ConfGGrandeza: TSmallintField;
    QrK_ConfGSdo_Ini: TFloatField;
    QrK_ConfGCompra: TFloatField;
    QrK_ConfGProducao: TFloatField;
    QrK_ConfGEntrouClasse: TFloatField;
    QrK_ConfGConsumo: TFloatField;
    QrK_ConfGSaiuClasse: TFloatField;
    QrK_ConfGVenda: TFloatField;
    QrK_ConfGFinal: TFloatField;
    QrK_ConfGDiferenca: TFloatField;
    QrK_ConfGIndevido: TFloatField;
    QrK_ConfGSubProduto: TFloatField;
    QrK_ConfGErrEmpresa: TFloatField;
    QrK_ConfGETE: TFloatField;
    QrK_ConfGBalancoIndev: TFloatField;
    QrK_ConfGESTSTabSorc: TIntegerField;
    QrK_ConfGSaiuDesmonte: TFloatField;
    QrK_ConfGEntrouDesmonte: TFloatField;
    QrK_ConfGProduReforma: TFloatField;
    QrK_ConfGInsumReforma: TFloatField;
    DsK_ConfG: TDataSource;
    QrPsq01: TmySQLQuery;
    DsPsq01: TDataSource;
    QrPsq02: TmySQLQuery;
    DsPsq02: TDataSource;
    QrK270_210: TmySQLQuery;
    DsK270_210: TDataSource;
    QrK275_215: TmySQLQuery;
    DsK275_215: TDataSource;
    QrK100ImporExpor: TSmallintField;
    QrK100AnoMes: TIntegerField;
    QrK100Empresa: TIntegerField;
    QrK100PeriApu: TIntegerField;
    QrK100DT_INI: TDateField;
    QrK100DT_FIN: TDateField;
    QrK100Lk: TIntegerField;
    QrK100DataCad: TDateField;
    QrK100DataAlt: TDateField;
    QrK100UserCad: TIntegerField;
    QrK100UserAlt: TIntegerField;
    QrK100AlterWeb: TSmallintField;
    QrK100Ativo: TSmallintField;
    QrK280NO_KndTab: TWideStringField;
    QrK200NO_PART: TWideStringField;
    QrK200Sigla: TWideStringField;
    QrK200NO_PRD_TAM_COR: TWideStringField;
    QrK200NO_IND_EST: TWideStringField;
    QrK200NO_KndTab: TWideStringField;
    QrK200ImporExpor: TSmallintField;
    QrK200AnoMes: TIntegerField;
    QrK200Empresa: TIntegerField;
    QrK200PeriApu: TIntegerField;
    QrK200KndTab: TIntegerField;
    QrK200KndCod: TIntegerField;
    QrK200KndNSU: TIntegerField;
    QrK200KndItm: TIntegerField;
    QrK200KndAID: TIntegerField;
    QrK200KndNiv: TIntegerField;
    QrK200DT_EST: TDateField;
    QrK200COD_ITEM: TWideStringField;
    QrK200QTD: TFloatField;
    QrK200IND_EST: TWideStringField;
    QrK200COD_PART: TWideStringField;
    QrK200GraGruX: TIntegerField;
    QrK200Grandeza: TIntegerField;
    QrK200Pecas: TFloatField;
    QrK200AreaM2: TFloatField;
    QrK200PesoKg: TFloatField;
    QrK200Entidade: TIntegerField;
    QrK200Tipo_Item: TIntegerField;
    QrK200Lk: TIntegerField;
    QrK200DataCad: TDateField;
    QrK200DataAlt: TDateField;
    QrK200UserCad: TIntegerField;
    QrK200UserAlt: TIntegerField;
    QrK200AlterWeb: TSmallintField;
    QrK200Ativo: TSmallintField;
    QrK280NO_PART: TWideStringField;
    QrK280Sigla: TWideStringField;
    QrK280NO_PRD_TAM_COR: TWideStringField;
    QrK280NO_IND_EST: TWideStringField;
    QrK280NO_OriSPEDEFDKnd: TWideStringField;
    QrK280ImporExpor: TSmallintField;
    QrK280AnoMes: TIntegerField;
    QrK280Empresa: TIntegerField;
    QrK280PeriApu: TIntegerField;
    QrK280KndTab: TIntegerField;
    QrK280KndCod: TIntegerField;
    QrK280KndNSU: TIntegerField;
    QrK280KndItm: TIntegerField;
    QrK280KndAID: TIntegerField;
    QrK280KndNiv: TIntegerField;
    QrK280DT_EST: TDateField;
    QrK280COD_ITEM: TWideStringField;
    QrK280QTD_COR_POS: TFloatField;
    QrK280QTD_COR_NEG: TFloatField;
    QrK280IND_EST: TWideStringField;
    QrK280COD_PART: TWideStringField;
    QrK280DebCred: TSmallintField;
    QrK280GraGruX: TIntegerField;
    QrK280Grandeza: TIntegerField;
    QrK280Pecas: TFloatField;
    QrK280AreaM2: TFloatField;
    QrK280PesoKg: TFloatField;
    QrK280Entidade: TIntegerField;
    QrK280Tipo_Item: TIntegerField;
    QrK280RegisPai: TWideStringField;
    QrK280RegisAvo: TWideStringField;
    QrK280ESTSTabSorc: TIntegerField;
    QrK280OriOpeProc: TSmallintField;
    QrK280OrigemIDKnd: TIntegerField;
    QrK280OriSPEDEFDKnd: TIntegerField;
    QrK280Lk: TIntegerField;
    QrK280DataCad: TDateField;
    QrK280DataAlt: TDateField;
    QrK280UserCad: TIntegerField;
    QrK280UserAlt: TIntegerField;
    QrK280AlterWeb: TSmallintField;
    QrK280Ativo: TSmallintField;
    QrK220NO_PRD_TAM_COR_ORI: TWideStringField;
    QrK220NO_PRD_TAM_COR_DEST: TWideStringField;
    QrK220Sigla: TWideStringField;
    QrK220ImporExpor: TSmallintField;
    QrK220AnoMes: TIntegerField;
    QrK220Empresa: TIntegerField;
    QrK220PeriApu: TIntegerField;
    QrK220KndTab: TIntegerField;
    QrK220KndCod: TIntegerField;
    QrK220KndNSU: TIntegerField;
    QrK220KndItmOri: TIntegerField;
    QrK220KndItmDst: TIntegerField;
    QrK220KndAID: TIntegerField;
    QrK220KndNiv: TIntegerField;
    QrK220ID_SEK: TSmallintField;
    QrK220DT_MOV: TDateField;
    QrK220COD_ITEM_ORI: TWideStringField;
    QrK220COD_ITEM_DEST: TWideStringField;
    QrK220QTD: TFloatField;
    QrK220GGXDst: TIntegerField;
    QrK220GGXOri: TIntegerField;
    QrK220ESOMIEM: TIntegerField;
    QrK220OrigemIDKnd: TIntegerField;
    QrK220Lk: TIntegerField;
    QrK220DataCad: TDateField;
    QrK220DataAlt: TDateField;
    QrK220UserCad: TIntegerField;
    QrK220UserAlt: TIntegerField;
    QrK220AlterWeb: TSmallintField;
    QrK220Ativo: TSmallintField;
    QrK270_210Grandeza: TSmallintField;
    QrK270_210Sigla: TWideStringField;
    QrK270_210NO_PRD_TAM_COR: TWideStringField;
    QrK270_210ImporExpor: TSmallintField;
    QrK270_210AnoMes: TIntegerField;
    QrK270_210Empresa: TIntegerField;
    QrK270_210PeriApu: TIntegerField;
    QrK270_210IDSeq1: TIntegerField;
    QrK270_210KndTab: TIntegerField;
    QrK270_210KndCod: TIntegerField;
    QrK270_210KndNSU: TIntegerField;
    QrK270_210KndItm: TIntegerField;
    QrK270_210KndAID: TIntegerField;
    QrK270_210KndNiv: TIntegerField;
    QrK270_210DT_INI_AP: TDateField;
    QrK270_210DT_FIN_AP: TDateField;
    QrK270_210COD_OP_OS: TWideStringField;
    QrK270_210COD_ITEM: TWideStringField;
    QrK270_210QTD_COR_POS: TFloatField;
    QrK270_210QTD_COR_NEG: TFloatField;
    QrK270_210ORIGEM: TWideStringField;
    QrK270_210GraGruX: TIntegerField;
    QrK270_210OriOpeProc: TSmallintField;
    QrK270_210OrigemIDKnd: TIntegerField;
    QrK270_210OriSPEDEFDKnd: TIntegerField;
    QrK270_210Lk: TIntegerField;
    QrK270_210DataCad: TDateField;
    QrK270_210DataAlt: TDateField;
    QrK270_210UserCad: TIntegerField;
    QrK270_210UserAlt: TIntegerField;
    QrK270_210AlterWeb: TSmallintField;
    QrK270_210Ativo: TSmallintField;
    QrK270_220Sigla: TWideStringField;
    QrK270_220NO_PRD_TAM_COR: TWideStringField;
    QrK270_220ImporExpor: TSmallintField;
    QrK270_220AnoMes: TIntegerField;
    QrK270_220Empresa: TIntegerField;
    QrK270_220PeriApu: TIntegerField;
    QrK270_220IDSeq1: TIntegerField;
    QrK270_220KndTab: TIntegerField;
    QrK270_220KndCod: TIntegerField;
    QrK270_220KndNSU: TIntegerField;
    QrK270_220KndItm: TIntegerField;
    QrK270_220KndAID: TIntegerField;
    QrK270_220KndNiv: TIntegerField;
    QrK270_220DT_INI_AP: TDateField;
    QrK270_220DT_FIN_AP: TDateField;
    QrK270_220COD_OP_OS: TWideStringField;
    QrK270_220COD_ITEM: TWideStringField;
    QrK270_220QTD_COR_POS: TFloatField;
    QrK270_220QTD_COR_NEG: TFloatField;
    QrK270_220ORIGEM: TWideStringField;
    QrK270_220GraGruX: TIntegerField;
    QrK270_220OriOpeProc: TSmallintField;
    QrK270_220OrigemIDKnd: TIntegerField;
    QrK270_220OriSPEDEFDKnd: TIntegerField;
    QrK270_220Lk: TIntegerField;
    QrK270_220DataCad: TDateField;
    QrK270_220DataAlt: TDateField;
    QrK270_220UserCad: TIntegerField;
    QrK270_220UserAlt: TIntegerField;
    QrK270_220AlterWeb: TSmallintField;
    QrK270_220Ativo: TSmallintField;
    QrK270_230Grandeza: TSmallintField;
    QrK270_230Sigla: TWideStringField;
    QrK270_230NO_PRD_TAM_COR: TWideStringField;
    QrK270_230ImporExpor: TSmallintField;
    QrK270_230AnoMes: TIntegerField;
    QrK270_230Empresa: TIntegerField;
    QrK270_230PeriApu: TIntegerField;
    QrK270_230IDSeq1: TIntegerField;
    QrK270_230KndTab: TIntegerField;
    QrK270_230KndCod: TIntegerField;
    QrK270_230KndNSU: TIntegerField;
    QrK270_230KndItm: TIntegerField;
    QrK270_230KndAID: TIntegerField;
    QrK270_230KndNiv: TIntegerField;
    QrK270_230DT_INI_AP: TDateField;
    QrK270_230DT_FIN_AP: TDateField;
    QrK270_230COD_OP_OS: TWideStringField;
    QrK270_230COD_ITEM: TWideStringField;
    QrK270_230QTD_COR_POS: TFloatField;
    QrK270_230QTD_COR_NEG: TFloatField;
    QrK270_230ORIGEM: TWideStringField;
    QrK270_230GraGruX: TIntegerField;
    QrK270_230OriOpeProc: TSmallintField;
    QrK270_230OrigemIDKnd: TIntegerField;
    QrK270_230OriSPEDEFDKnd: TIntegerField;
    QrK270_230Lk: TIntegerField;
    QrK270_230DataCad: TDateField;
    QrK270_230DataAlt: TDateField;
    QrK270_230UserCad: TIntegerField;
    QrK270_230UserAlt: TIntegerField;
    QrK270_230AlterWeb: TSmallintField;
    QrK270_230Ativo: TSmallintField;
    QrK270_250Grandeza: TSmallintField;
    QrK270_250Sigla: TWideStringField;
    QrK270_250NO_PRD_TAM_COR: TWideStringField;
    QrK270_250ImporExpor: TSmallintField;
    QrK270_250AnoMes: TIntegerField;
    QrK270_250Empresa: TIntegerField;
    QrK270_250PeriApu: TIntegerField;
    QrK270_250IDSeq1: TIntegerField;
    QrK270_250KndTab: TIntegerField;
    QrK270_250KndCod: TIntegerField;
    QrK270_250KndNSU: TIntegerField;
    QrK270_250KndItm: TIntegerField;
    QrK270_250KndAID: TIntegerField;
    QrK270_250KndNiv: TIntegerField;
    QrK270_250DT_INI_AP: TDateField;
    QrK270_250DT_FIN_AP: TDateField;
    QrK270_250COD_OP_OS: TWideStringField;
    QrK270_250COD_ITEM: TWideStringField;
    QrK270_250QTD_COR_POS: TFloatField;
    QrK270_250QTD_COR_NEG: TFloatField;
    QrK270_250ORIGEM: TWideStringField;
    QrK270_250GraGruX: TIntegerField;
    QrK270_250OriOpeProc: TSmallintField;
    QrK270_250OrigemIDKnd: TIntegerField;
    QrK270_250OriSPEDEFDKnd: TIntegerField;
    QrK270_250Lk: TIntegerField;
    QrK270_250DataCad: TDateField;
    QrK270_250DataAlt: TDateField;
    QrK270_250UserCad: TIntegerField;
    QrK270_250UserAlt: TIntegerField;
    QrK270_250AlterWeb: TSmallintField;
    QrK270_250Ativo: TSmallintField;
    QrK270_260Grandeza: TSmallintField;
    QrK270_260Sigla: TWideStringField;
    QrK270_260NO_PRD_TAM_COR: TWideStringField;
    QrK270_260ImporExpor: TSmallintField;
    QrK270_260AnoMes: TIntegerField;
    QrK270_260Empresa: TIntegerField;
    QrK270_260PeriApu: TIntegerField;
    QrK270_260IDSeq1: TIntegerField;
    QrK270_260KndTab: TIntegerField;
    QrK270_260KndCod: TIntegerField;
    QrK270_260KndNSU: TIntegerField;
    QrK270_260KndItm: TIntegerField;
    QrK270_260KndAID: TIntegerField;
    QrK270_260KndNiv: TIntegerField;
    QrK270_260DT_INI_AP: TDateField;
    QrK270_260DT_FIN_AP: TDateField;
    QrK270_260COD_OP_OS: TWideStringField;
    QrK270_260COD_ITEM: TWideStringField;
    QrK270_260QTD_COR_POS: TFloatField;
    QrK270_260QTD_COR_NEG: TFloatField;
    QrK270_260ORIGEM: TWideStringField;
    QrK270_260GraGruX: TIntegerField;
    QrK270_260OriOpeProc: TSmallintField;
    QrK270_260OrigemIDKnd: TIntegerField;
    QrK270_260OriSPEDEFDKnd: TIntegerField;
    QrK270_260Lk: TIntegerField;
    QrK270_260DataCad: TDateField;
    QrK270_260DataAlt: TDateField;
    QrK270_260UserCad: TIntegerField;
    QrK270_260UserAlt: TIntegerField;
    QrK270_260AlterWeb: TSmallintField;
    QrK270_260Ativo: TSmallintField;
    QrK275_220NO_PRD_TAM_COR: TWideStringField;
    QrK275_220Sigla: TWideStringField;
    QrK275_220ImporExpor: TSmallintField;
    QrK275_220AnoMes: TIntegerField;
    QrK275_220Empresa: TIntegerField;
    QrK275_220PeriApu: TIntegerField;
    QrK275_220IDSeq1: TIntegerField;
    QrK275_220IDSeq2: TIntegerField;
    QrK275_220KndTab: TIntegerField;
    QrK275_220KndCod: TIntegerField;
    QrK275_220KndNSU: TIntegerField;
    QrK275_220KndItm: TIntegerField;
    QrK275_220KndAID: TIntegerField;
    QrK275_220KndNiv: TIntegerField;
    QrK275_220ID_SEK: TSmallintField;
    QrK275_220COD_ITEM: TWideStringField;
    QrK275_220QTD_COR_POS: TFloatField;
    QrK275_220QTD_COR_NEG: TFloatField;
    QrK275_220COD_INS_SUBST: TWideStringField;
    QrK275_220GraGruX: TIntegerField;
    QrK275_220ESTSTabSorc: TIntegerField;
    QrK275_220ORIGEM: TWideStringField;
    QrK275_220OriOpeProc: TSmallintField;
    QrK275_220OrigemIDKnd: TIntegerField;
    QrK275_220OriSPEDEFDKnd: TIntegerField;
    QrK275_220Lk: TIntegerField;
    QrK275_220DataCad: TDateField;
    QrK275_220DataAlt: TDateField;
    QrK275_220UserCad: TIntegerField;
    QrK275_220UserAlt: TIntegerField;
    QrK275_220AlterWeb: TSmallintField;
    QrK275_220Ativo: TSmallintField;
    QrK275_215NO_PRD_TAM_COR: TWideStringField;
    QrK275_215Sigla: TWideStringField;
    QrK275_215ImporExpor: TSmallintField;
    QrK275_215AnoMes: TIntegerField;
    QrK275_215Empresa: TIntegerField;
    QrK275_215PeriApu: TIntegerField;
    QrK275_215IDSeq1: TIntegerField;
    QrK275_215IDSeq2: TIntegerField;
    QrK275_215KndTab: TIntegerField;
    QrK275_215KndCod: TIntegerField;
    QrK275_215KndNSU: TIntegerField;
    QrK275_215KndItm: TIntegerField;
    QrK275_215KndAID: TIntegerField;
    QrK275_215KndNiv: TIntegerField;
    QrK275_215ID_SEK: TSmallintField;
    QrK275_215COD_ITEM: TWideStringField;
    QrK275_215QTD_COR_POS: TFloatField;
    QrK275_255NO_PRD_TAM_COR: TWideStringField;
    QrK275_255Sigla: TWideStringField;
    QrK275_255ImporExpor: TSmallintField;
    QrK275_255AnoMes: TIntegerField;
    QrK275_255Empresa: TIntegerField;
    QrK275_255PeriApu: TIntegerField;
    QrK275_255IDSeq1: TIntegerField;
    QrK275_255IDSeq2: TIntegerField;
    QrK275_255KndTab: TIntegerField;
    QrK275_255KndCod: TIntegerField;
    QrK275_255KndNSU: TIntegerField;
    QrK275_255KndItm: TIntegerField;
    QrK275_255KndAID: TIntegerField;
    QrK275_255KndNiv: TIntegerField;
    QrK275_255ID_SEK: TSmallintField;
    QrK275_255COD_ITEM: TWideStringField;
    QrK275_255QTD_COR_POS: TFloatField;
    QrK275_255QTD_COR_NEG: TFloatField;
    QrK275_255COD_INS_SUBST: TWideStringField;
    QrK275_255GraGruX: TIntegerField;
    QrK275_255ESTSTabSorc: TIntegerField;
    QrK275_255ORIGEM: TWideStringField;
    QrK275_255OriOpeProc: TSmallintField;
    QrK275_255OrigemIDKnd: TIntegerField;
    QrK275_255OriSPEDEFDKnd: TIntegerField;
    QrK275_255Lk: TIntegerField;
    QrK275_255DataCad: TDateField;
    QrK275_255DataAlt: TDateField;
    QrK275_255UserCad: TIntegerField;
    QrK275_255UserAlt: TIntegerField;
    QrK275_255AlterWeb: TSmallintField;
    QrK275_255Ativo: TSmallintField;
    QrK275_265NO_PRD_TAM_COR: TWideStringField;
    QrK275_265Sigla: TWideStringField;
    QrK275_265ImporExpor: TSmallintField;
    QrK275_265AnoMes: TIntegerField;
    QrK275_265Empresa: TIntegerField;
    QrK275_265PeriApu: TIntegerField;
    QrK275_265IDSeq1: TIntegerField;
    QrK275_265IDSeq2: TIntegerField;
    QrK275_265KndTab: TIntegerField;
    QrK275_265KndCod: TIntegerField;
    QrK275_265KndNSU: TIntegerField;
    QrK275_265KndItm: TIntegerField;
    QrK275_265KndAID: TIntegerField;
    QrK275_265KndNiv: TIntegerField;
    QrK275_265ID_SEK: TSmallintField;
    QrK275_265COD_ITEM: TWideStringField;
    QrK275_265QTD_COR_POS: TFloatField;
    QrK275_265QTD_COR_NEG: TFloatField;
    QrK275_265COD_INS_SUBST: TWideStringField;
    QrK275_265GraGruX: TIntegerField;
    QrK275_265ESTSTabSorc: TIntegerField;
    QrK275_265ORIGEM: TWideStringField;
    QrK275_265OriOpeProc: TSmallintField;
    QrK275_265OrigemIDKnd: TIntegerField;
    QrK275_265OriSPEDEFDKnd: TIntegerField;
    QrK275_265Lk: TIntegerField;
    QrK275_265DataCad: TDateField;
    QrK275_265DataAlt: TDateField;
    QrK275_265UserCad: TIntegerField;
    QrK275_265UserAlt: TIntegerField;
    QrK275_265AlterWeb: TSmallintField;
    QrK275_265Ativo: TSmallintField;
    QrK210Grandeza: TSmallintField;
    QrK210Sigla: TWideStringField;
    QrK210NO_PRD_TAM_COR: TWideStringField;
    QrK210DT_FIN_OS_Txt: TWideStringField;
    QrK210ImporExpor: TSmallintField;
    QrK210AnoMes: TIntegerField;
    QrK210Empresa: TIntegerField;
    QrK210PeriApu: TIntegerField;
    QrK210KndTab: TIntegerField;
    QrK210KndCod: TIntegerField;
    QrK210KndNSU: TIntegerField;
    QrK210KndItm: TIntegerField;
    QrK210KndAID: TIntegerField;
    QrK210KndNiv: TIntegerField;
    QrK210IDSeq1: TIntegerField;
    QrK210ID_SEK: TSmallintField;
    QrK210DT_INI_OS: TDateField;
    QrK210DT_FIN_OS: TDateField;
    QrK210COD_DOC_OS: TWideStringField;
    QrK210COD_ITEM_ORI: TWideStringField;
    QrK210QTD_ORI: TFloatField;
    QrK210GraGruX: TIntegerField;
    QrK210OriOpeProc: TSmallintField;
    QrK210OrigemIDKnd: TIntegerField;
    QrK210Lk: TIntegerField;
    QrK210DataCad: TDateField;
    QrK210DataAlt: TDateField;
    QrK210UserCad: TIntegerField;
    QrK210UserAlt: TIntegerField;
    QrK210AlterWeb: TSmallintField;
    QrK210Ativo: TSmallintField;
    QrK230Grandeza: TSmallintField;
    QrK230Sigla: TWideStringField;
    QrK230NO_PRD_TAM_COR: TWideStringField;
    QrK230DT_FIN_OP_Txt: TWideStringField;
    QrK230ImporExpor: TSmallintField;
    QrK230AnoMes: TIntegerField;
    QrK230Empresa: TIntegerField;
    QrK230PeriApu: TIntegerField;
    QrK230KndTab: TIntegerField;
    QrK230KndCod: TIntegerField;
    QrK230KndNSU: TIntegerField;
    QrK230KndItm: TIntegerField;
    QrK230KndAID: TIntegerField;
    QrK230KndNiv: TIntegerField;
    QrK230IDSeq1: TIntegerField;
    QrK230ID_SEK: TSmallintField;
    QrK230DT_INI_OP: TDateField;
    QrK230DT_FIN_OP: TDateField;
    QrK230COD_DOC_OP: TWideStringField;
    QrK230COD_ITEM: TWideStringField;
    QrK230QTD_ENC: TFloatField;
    QrK230GraGruX: TIntegerField;
    QrK230OriOpeProc: TSmallintField;
    QrK230Lk: TIntegerField;
    QrK230DataCad: TDateField;
    QrK230DataAlt: TDateField;
    QrK230UserCad: TIntegerField;
    QrK230UserAlt: TIntegerField;
    QrK230AlterWeb: TSmallintField;
    QrK230Ativo: TSmallintField;
    QrK235Grandeza: TSmallintField;
    QrK235Sigla: TWideStringField;
    QrK235NO_PRD_TAM_COR: TWideStringField;
    QrK235ImporExpor: TSmallintField;
    QrK235AnoMes: TIntegerField;
    QrK235Empresa: TIntegerField;
    QrK235PeriApu: TIntegerField;
    QrK235KndTab: TIntegerField;
    QrK235KndCod: TIntegerField;
    QrK235KndNSU: TIntegerField;
    QrK235KndItm: TIntegerField;
    QrK235KndAID: TIntegerField;
    QrK235KndNiv: TIntegerField;
    QrK235IDSeq1: TIntegerField;
    QrK235IDSeq2: TIntegerField;
    QrK235ID_SEK: TSmallintField;
    QrK235DT_SAIDA: TDateField;
    QrK235COD_ITEM: TWideStringField;
    QrK235QTD: TFloatField;
    QrK235COD_INS_SUBST: TWideStringField;
    QrK235GraGruX: TIntegerField;
    QrK235ESTSTabSorc: TIntegerField;
    QrK235OriOpeProc: TSmallintField;
    QrK235Lk: TIntegerField;
    QrK235DataCad: TDateField;
    QrK235DataAlt: TDateField;
    QrK235UserCad: TIntegerField;
    QrK235UserAlt: TIntegerField;
    QrK235AlterWeb: TSmallintField;
    QrK235Ativo: TSmallintField;
    QrK275_235Grandeza: TSmallintField;
    QrK275_235Sigla: TWideStringField;
    QrK275_235NO_PRD_TAM_COR: TWideStringField;
    QrK275_235ImporExpor: TSmallintField;
    QrK275_235AnoMes: TIntegerField;
    QrK275_235Empresa: TIntegerField;
    QrK275_235PeriApu: TIntegerField;
    QrK275_235IDSeq1: TIntegerField;
    QrK275_235IDSeq2: TIntegerField;
    QrK275_235KndTab: TIntegerField;
    QrK275_235KndCod: TIntegerField;
    QrK275_235KndNSU: TIntegerField;
    QrK275_235KndItm: TIntegerField;
    QrK275_235KndAID: TIntegerField;
    QrK275_235KndNiv: TIntegerField;
    QrK275_235ID_SEK: TSmallintField;
    QrK275_235COD_ITEM: TWideStringField;
    QrK275_235QTD_COR_POS: TFloatField;
    QrK275_235QTD_COR_NEG: TFloatField;
    QrK275_235COD_INS_SUBST: TWideStringField;
    QrK275_235GraGruX: TIntegerField;
    QrK275_235ESTSTabSorc: TIntegerField;
    QrK275_235ORIGEM: TWideStringField;
    QrK275_235OriOpeProc: TSmallintField;
    QrK275_235OrigemIDKnd: TIntegerField;
    QrK275_235OriSPEDEFDKnd: TIntegerField;
    QrK275_235Lk: TIntegerField;
    QrK275_235DataCad: TDateField;
    QrK275_235DataAlt: TDateField;
    QrK275_235UserCad: TIntegerField;
    QrK275_235UserAlt: TIntegerField;
    QrK275_235AlterWeb: TSmallintField;
    QrK275_235Ativo: TSmallintField;
    QrK275_235RegisPai: TWideStringField;
    QrK215Grandeza: TSmallintField;
    QrK215Sigla: TWideStringField;
    QrK215NO_PRD_TAM_COR: TWideStringField;
    QrK215ImporExpor: TSmallintField;
    QrK215AnoMes: TIntegerField;
    QrK215Empresa: TIntegerField;
    QrK215PeriApu: TIntegerField;
    QrK215KndTab: TIntegerField;
    QrK215KndCod: TIntegerField;
    QrK215KndNSU: TIntegerField;
    QrK215KndItm: TIntegerField;
    QrK215KndAID: TIntegerField;
    QrK215KndNiv: TIntegerField;
    QrK215IDSeq1: TIntegerField;
    QrK215IDSeq2: TIntegerField;
    QrK215COD_ITEM_DES: TWideStringField;
    QrK215QTD_DES: TFloatField;
    QrK215GraGruX: TIntegerField;
    QrK215ESTSTabSorc: TIntegerField;
    QrK215OriOpeProc: TSmallintField;
    QrK215OrigemIDKnd: TIntegerField;
    QrK215Lk: TIntegerField;
    QrK215DataCad: TDateField;
    QrK215DataAlt: TDateField;
    QrK215UserCad: TIntegerField;
    QrK215UserAlt: TIntegerField;
    QrK215AlterWeb: TSmallintField;
    QrK215Ativo: TSmallintField;
    QrK265Grandeza: TSmallintField;
    QrK265Sigla: TWideStringField;
    QrK265NO_PRD_TAM_COR: TWideStringField;
    QrK265ImporExpor: TSmallintField;
    QrK265AnoMes: TIntegerField;
    QrK265Empresa: TIntegerField;
    QrK265PeriApu: TIntegerField;
    QrK265KndTab: TIntegerField;
    QrK265KndCod: TIntegerField;
    QrK265KndNSU: TIntegerField;
    QrK265KndItm: TIntegerField;
    QrK265KndAID: TIntegerField;
    QrK265KndNiv: TIntegerField;
    QrK265IDSeq1: TIntegerField;
    QrK265IDSeq2: TIntegerField;
    QrK265ID_SEK: TSmallintField;
    QrK265COD_ITEM: TWideStringField;
    QrK265QTD_CONS: TFloatField;
    QrK265QTD_RET: TFloatField;
    QrK265GraGruX: TIntegerField;
    QrK265ESTSTabSorc: TIntegerField;
    QrK265OriOpeProc: TSmallintField;
    QrK265Lk: TIntegerField;
    QrK265DataCad: TDateField;
    QrK265DataAlt: TDateField;
    QrK265UserCad: TIntegerField;
    QrK265UserAlt: TIntegerField;
    QrK265AlterWeb: TSmallintField;
    QrK265Ativo: TSmallintField;
    QrK260Grandeza: TSmallintField;
    QrK260Sigla: TWideStringField;
    QrK260NO_PRD_TAM_COR: TWideStringField;
    QrK260DT_RET_Txt: TWideStringField;
    QrK260ImporExpor: TSmallintField;
    QrK260AnoMes: TIntegerField;
    QrK260Empresa: TIntegerField;
    QrK260PeriApu: TIntegerField;
    QrK260KndTab: TIntegerField;
    QrK260KndCod: TIntegerField;
    QrK260KndNSU: TIntegerField;
    QrK260KndItm: TIntegerField;
    QrK260KndAID: TIntegerField;
    QrK260KndNiv: TIntegerField;
    QrK260IDSeq1: TIntegerField;
    QrK260ID_SEK: TSmallintField;
    QrK260COD_OP_OS: TWideStringField;
    QrK260COD_ITEM: TWideStringField;
    QrK260DT_SAIDA: TDateField;
    QrK260QTD_SAIDA: TFloatField;
    QrK260DT_RET: TDateField;
    QrK260QTD_RET: TFloatField;
    QrK260GraGruX: TIntegerField;
    QrK260OriOpeProc: TSmallintField;
    QrK260Lk: TIntegerField;
    QrK260DataCad: TDateField;
    QrK260DataAlt: TDateField;
    QrK260UserCad: TIntegerField;
    QrK260UserAlt: TIntegerField;
    QrK260AlterWeb: TSmallintField;
    QrK260Ativo: TSmallintField;
    QrK255Grandeza: TSmallintField;
    QrK255Sigla: TWideStringField;
    QrK255NO_PRD_TAM_COR: TWideStringField;
    QrK255ImporExpor: TSmallintField;
    QrK255AnoMes: TIntegerField;
    QrK255Empresa: TIntegerField;
    QrK255PeriApu: TIntegerField;
    QrK255KndTab: TIntegerField;
    QrK255KndCod: TIntegerField;
    QrK255KndNSU: TIntegerField;
    QrK255KndItm: TIntegerField;
    QrK255KndAID: TIntegerField;
    QrK255KndNiv: TIntegerField;
    QrK255IDSeq1: TIntegerField;
    QrK255IDSeq2: TIntegerField;
    QrK255ID_SEK: TSmallintField;
    QrK255DT_CONS: TDateField;
    QrK255COD_ITEM: TWideStringField;
    QrK255QTD: TFloatField;
    QrK255COD_INS_SUBST: TWideStringField;
    QrK255GraGruX: TIntegerField;
    QrK255ESTSTabSorc: TIntegerField;
    QrK255OriOpeProc: TSmallintField;
    QrK255Lk: TIntegerField;
    QrK255DataCad: TDateField;
    QrK255DataAlt: TDateField;
    QrK255UserCad: TIntegerField;
    QrK255UserAlt: TIntegerField;
    QrK255AlterWeb: TSmallintField;
    QrK255Ativo: TSmallintField;
    QrK250Grandeza: TSmallintField;
    QrK250Sigla: TWideStringField;
    QrK250NO_PRD_TAM_COR: TWideStringField;
    QrK250DT_PROD_Txt: TWideStringField;
    QrK250ImporExpor: TSmallintField;
    QrK250AnoMes: TIntegerField;
    QrK250Empresa: TIntegerField;
    QrK250PeriApu: TIntegerField;
    QrK250KndTab: TIntegerField;
    QrK250KndCod: TIntegerField;
    QrK250KndNSU: TIntegerField;
    QrK250KndItm: TIntegerField;
    QrK250KndAID: TIntegerField;
    QrK250KndNiv: TIntegerField;
    QrK250IDSeq1: TIntegerField;
    QrK250ID_SEK: TSmallintField;
    QrK250DT_PROD: TDateField;
    QrK250COD_ITEM: TWideStringField;
    QrK250QTD: TFloatField;
    QrK250COD_DOC_OP: TWideStringField;
    QrK250GraGruX: TIntegerField;
    QrK250OriOpeProc: TSmallintField;
    QrK250Lk: TIntegerField;
    QrK250DataCad: TDateField;
    QrK250DataAlt: TDateField;
    QrK250UserCad: TIntegerField;
    QrK250UserAlt: TIntegerField;
    QrK250AlterWeb: TSmallintField;
    QrK250Ativo: TSmallintField;
    QrVMI: TmySQLQuery;
    DsVMI: TDataSource;
    QrK200IDSeq1: TIntegerField;
    QrK290: TmySQLQuery;
    QrK290DT_FIN_OP_Txt: TWideStringField;
    QrK290ImporExpor: TSmallintField;
    QrK290AnoMes: TIntegerField;
    QrK290Empresa: TIntegerField;
    QrK290PeriApu: TIntegerField;
    QrK290KndTab: TIntegerField;
    QrK290KndCod: TIntegerField;
    QrK290KndNSU: TIntegerField;
    QrK290KndItm: TIntegerField;
    QrK290KndAID: TIntegerField;
    QrK290KndNiv: TIntegerField;
    QrK290IDSeq1: TIntegerField;
    QrK290ID_SEK: TSmallintField;
    QrK290DT_INI_OP: TDateField;
    QrK290DT_FIN_OP: TDateField;
    QrK290COD_DOC_OP: TWideStringField;
    QrK290OriOpeProc: TSmallintField;
    QrK290Lk: TIntegerField;
    QrK290DataCad: TDateField;
    QrK290DataAlt: TDateField;
    QrK290UserCad: TIntegerField;
    QrK290UserAlt: TIntegerField;
    QrK290AlterWeb: TSmallintField;
    QrK290Ativo: TSmallintField;
    DsK290: TDataSource;
    QrK300: TmySQLQuery;
    QrK300ImporExpor: TSmallintField;
    QrK300AnoMes: TIntegerField;
    QrK300Empresa: TIntegerField;
    QrK300PeriApu: TIntegerField;
    QrK300KndTab: TIntegerField;
    QrK300KndCod: TIntegerField;
    QrK300KndNSU: TIntegerField;
    QrK300KndItm: TIntegerField;
    QrK300KndAID: TIntegerField;
    QrK300KndNiv: TIntegerField;
    QrK300IDSeq1: TIntegerField;
    QrK300ID_SEK: TSmallintField;
    QrK300DT_PROD: TDateField;
    QrK300OriOpeProc: TSmallintField;
    QrK300Lk: TIntegerField;
    QrK300DataCad: TDateField;
    QrK300DataAlt: TDateField;
    QrK300UserCad: TIntegerField;
    QrK300UserAlt: TIntegerField;
    QrK300AlterWeb: TSmallintField;
    QrK300Ativo: TSmallintField;
    DsK300: TDataSource;
    QrK291: TmySQLQuery;
    QrK291Grandeza: TSmallintField;
    QrK291Sigla: TWideStringField;
    QrK291NO_PRD_TAM_COR: TWideStringField;
    QrK291ImporExpor: TSmallintField;
    QrK291AnoMes: TIntegerField;
    QrK291Empresa: TIntegerField;
    QrK291PeriApu: TIntegerField;
    QrK291KndTab: TIntegerField;
    QrK291KndCod: TIntegerField;
    QrK291KndNSU: TIntegerField;
    QrK291KndItm: TIntegerField;
    QrK291KndAID: TIntegerField;
    QrK291KndNiv: TIntegerField;
    QrK291IDSeq1: TIntegerField;
    QrK291ID_SEK: TSmallintField;
    QrK291COD_ITEM: TWideStringField;
    QrK291QTD: TFloatField;
    QrK291GraGruX: TIntegerField;
    QrK291OriOpeProc: TSmallintField;
    QrK291Lk: TIntegerField;
    QrK291DataCad: TDateField;
    QrK291DataAlt: TDateField;
    QrK291UserCad: TIntegerField;
    QrK291UserAlt: TIntegerField;
    QrK291AlterWeb: TSmallintField;
    QrK291Ativo: TSmallintField;
    DsK291: TDataSource;
    QrK291IDSeq2: TIntegerField;
    QrK292: TmySQLQuery;
    QrK292Grandeza: TSmallintField;
    QrK292Sigla: TWideStringField;
    QrK292NO_PRD_TAM_COR: TWideStringField;
    QrK292ImporExpor: TSmallintField;
    QrK292AnoMes: TIntegerField;
    QrK292Empresa: TIntegerField;
    QrK292PeriApu: TIntegerField;
    QrK292KndTab: TIntegerField;
    QrK292KndCod: TIntegerField;
    QrK292KndNSU: TIntegerField;
    QrK292KndItm: TIntegerField;
    QrK292KndAID: TIntegerField;
    QrK292KndNiv: TIntegerField;
    QrK292IDSeq1: TIntegerField;
    QrK292IDSeq2: TIntegerField;
    QrK292ID_SEK: TSmallintField;
    QrK292COD_ITEM: TWideStringField;
    QrK292QTD: TFloatField;
    QrK292GraGruX: TIntegerField;
    QrK292OriOpeProc: TSmallintField;
    QrK292Lk: TIntegerField;
    QrK292DataCad: TDateField;
    QrK292DataAlt: TDateField;
    QrK292UserCad: TIntegerField;
    QrK292UserAlt: TIntegerField;
    QrK292AlterWeb: TSmallintField;
    QrK292Ativo: TSmallintField;
    DsK292: TDataSource;
    QrK301: TmySQLQuery;
    QrK301Grandeza: TSmallintField;
    QrK301Sigla: TWideStringField;
    QrK301NO_PRD_TAM_COR: TWideStringField;
    QrK301ImporExpor: TSmallintField;
    QrK301AnoMes: TIntegerField;
    QrK301Empresa: TIntegerField;
    QrK301PeriApu: TIntegerField;
    QrK301KndTab: TIntegerField;
    QrK301KndCod: TIntegerField;
    QrK301KndNSU: TIntegerField;
    QrK301KndItm: TIntegerField;
    QrK301KndAID: TIntegerField;
    QrK301KndNiv: TIntegerField;
    QrK301IDSeq1: TIntegerField;
    QrK301IDSeq2: TIntegerField;
    QrK301ID_SEK: TSmallintField;
    QrK301COD_ITEM: TWideStringField;
    QrK301QTD: TFloatField;
    QrK301GraGruX: TIntegerField;
    QrK301OriOpeProc: TSmallintField;
    QrK301Lk: TIntegerField;
    QrK301DataCad: TDateField;
    QrK301DataAlt: TDateField;
    QrK301UserCad: TIntegerField;
    QrK301UserAlt: TIntegerField;
    QrK301AlterWeb: TSmallintField;
    QrK301Ativo: TSmallintField;
    DsK301: TDataSource;
    QrK302: TmySQLQuery;
    QrK302Grandeza: TSmallintField;
    QrK302Sigla: TWideStringField;
    QrK302NO_PRD_TAM_COR: TWideStringField;
    QrK302ImporExpor: TSmallintField;
    QrK302AnoMes: TIntegerField;
    QrK302Empresa: TIntegerField;
    QrK302PeriApu: TIntegerField;
    QrK302KndTab: TIntegerField;
    QrK302KndCod: TIntegerField;
    QrK302KndNSU: TIntegerField;
    QrK302KndItm: TIntegerField;
    QrK302KndAID: TIntegerField;
    QrK302KndNiv: TIntegerField;
    QrK302IDSeq1: TIntegerField;
    QrK302IDSeq2: TIntegerField;
    QrK302ID_SEK: TSmallintField;
    QrK302COD_ITEM: TWideStringField;
    QrK302QTD: TFloatField;
    QrK302GraGruX: TIntegerField;
    QrK302OriOpeProc: TSmallintField;
    QrK302Lk: TIntegerField;
    QrK302DataCad: TDateField;
    QrK302DataAlt: TDateField;
    QrK302UserCad: TIntegerField;
    QrK302UserAlt: TIntegerField;
    QrK302AlterWeb: TSmallintField;
    QrK302Ativo: TSmallintField;
    DsK302: TDataSource;
    QrK270_291: TmySQLQuery;
    QrK270_291Grandeza: TSmallintField;
    QrK270_291Sigla: TWideStringField;
    QrK270_291NO_PRD_TAM_COR: TWideStringField;
    QrK270_291ImporExpor: TSmallintField;
    QrK270_291AnoMes: TIntegerField;
    QrK270_291Empresa: TIntegerField;
    QrK270_291PeriApu: TIntegerField;
    QrK270_291IDSeq1: TIntegerField;
    QrK270_291KndTab: TIntegerField;
    QrK270_291KndCod: TIntegerField;
    QrK270_291KndNSU: TIntegerField;
    QrK270_291KndItm: TIntegerField;
    QrK270_291KndAID: TIntegerField;
    QrK270_291KndNiv: TIntegerField;
    QrK270_291DT_INI_AP: TDateField;
    QrK270_291DT_FIN_AP: TDateField;
    QrK270_291COD_OP_OS: TWideStringField;
    QrK270_291COD_ITEM: TWideStringField;
    QrK270_291QTD_COR_POS: TFloatField;
    QrK270_291QTD_COR_NEG: TFloatField;
    QrK270_291ORIGEM: TWideStringField;
    QrK270_291GraGruX: TIntegerField;
    QrK270_291OriOpeProc: TSmallintField;
    QrK270_291OrigemIDKnd: TIntegerField;
    QrK270_291OriSPEDEFDKnd: TIntegerField;
    QrK270_291Lk: TIntegerField;
    QrK270_291DataCad: TDateField;
    QrK270_291DataAlt: TDateField;
    QrK270_291UserCad: TIntegerField;
    QrK270_291UserAlt: TIntegerField;
    QrK270_291AlterWeb: TSmallintField;
    QrK270_291Ativo: TSmallintField;
    DsK270_291: TDataSource;
    QrK270_292: TmySQLQuery;
    QrK270_292Grandeza: TSmallintField;
    QrK270_292Sigla: TWideStringField;
    QrK270_292NO_PRD_TAM_COR: TWideStringField;
    QrK270_292ImporExpor: TSmallintField;
    QrK270_292AnoMes: TIntegerField;
    QrK270_292Empresa: TIntegerField;
    QrK270_292PeriApu: TIntegerField;
    QrK270_292IDSeq1: TIntegerField;
    QrK270_292KndTab: TIntegerField;
    QrK270_292KndCod: TIntegerField;
    QrK270_292KndNSU: TIntegerField;
    QrK270_292KndItm: TIntegerField;
    QrK270_292KndAID: TIntegerField;
    QrK270_292KndNiv: TIntegerField;
    QrK270_292DT_INI_AP: TDateField;
    QrK270_292DT_FIN_AP: TDateField;
    QrK270_292COD_OP_OS: TWideStringField;
    QrK270_292COD_ITEM: TWideStringField;
    QrK270_292QTD_COR_POS: TFloatField;
    QrK270_292QTD_COR_NEG: TFloatField;
    QrK270_292ORIGEM: TWideStringField;
    QrK270_292GraGruX: TIntegerField;
    QrK270_292OriOpeProc: TSmallintField;
    QrK270_292OrigemIDKnd: TIntegerField;
    QrK270_292OriSPEDEFDKnd: TIntegerField;
    QrK270_292Lk: TIntegerField;
    QrK270_292DataCad: TDateField;
    QrK270_292DataAlt: TDateField;
    QrK270_292UserCad: TIntegerField;
    QrK270_292UserAlt: TIntegerField;
    QrK270_292AlterWeb: TSmallintField;
    QrK270_292Ativo: TSmallintField;
    DsK270_292: TDataSource;
    QrK270_301: TmySQLQuery;
    QrK270_301Grandeza: TSmallintField;
    QrK270_301Sigla: TWideStringField;
    QrK270_301NO_PRD_TAM_COR: TWideStringField;
    QrK270_301ImporExpor: TSmallintField;
    QrK270_301AnoMes: TIntegerField;
    QrK270_301Empresa: TIntegerField;
    QrK270_301PeriApu: TIntegerField;
    QrK270_301IDSeq1: TIntegerField;
    QrK270_301KndTab: TIntegerField;
    QrK270_301KndCod: TIntegerField;
    QrK270_301KndNSU: TIntegerField;
    QrK270_301KndItm: TIntegerField;
    QrK270_301KndAID: TIntegerField;
    QrK270_301KndNiv: TIntegerField;
    QrK270_301DT_INI_AP: TDateField;
    QrK270_301DT_FIN_AP: TDateField;
    QrK270_301COD_OP_OS: TWideStringField;
    QrK270_301COD_ITEM: TWideStringField;
    QrK270_301QTD_COR_POS: TFloatField;
    QrK270_301QTD_COR_NEG: TFloatField;
    QrK270_301ORIGEM: TWideStringField;
    QrK270_301GraGruX: TIntegerField;
    QrK270_301OriOpeProc: TSmallintField;
    QrK270_301OrigemIDKnd: TIntegerField;
    QrK270_301OriSPEDEFDKnd: TIntegerField;
    QrK270_301Lk: TIntegerField;
    QrK270_301DataCad: TDateField;
    QrK270_301DataAlt: TDateField;
    QrK270_301UserCad: TIntegerField;
    QrK270_301UserAlt: TIntegerField;
    QrK270_301AlterWeb: TSmallintField;
    QrK270_301Ativo: TSmallintField;
    DsK270_301: TDataSource;
    QrK270_302: TmySQLQuery;
    QrK270_302Grandeza: TSmallintField;
    QrK270_302Sigla: TWideStringField;
    QrK270_302NO_PRD_TAM_COR: TWideStringField;
    QrK270_302ImporExpor: TSmallintField;
    QrK270_302AnoMes: TIntegerField;
    QrK270_302Empresa: TIntegerField;
    QrK270_302PeriApu: TIntegerField;
    QrK270_302IDSeq1: TIntegerField;
    QrK270_302KndTab: TIntegerField;
    QrK270_302KndCod: TIntegerField;
    QrK270_302KndNSU: TIntegerField;
    QrK270_302KndItm: TIntegerField;
    QrK270_302KndAID: TIntegerField;
    QrK270_302KndNiv: TIntegerField;
    QrK270_302DT_INI_AP: TDateField;
    QrK270_302DT_FIN_AP: TDateField;
    QrK270_302COD_OP_OS: TWideStringField;
    QrK270_302COD_ITEM: TWideStringField;
    QrK270_302QTD_COR_POS: TFloatField;
    QrK270_302QTD_COR_NEG: TFloatField;
    QrK270_302ORIGEM: TWideStringField;
    QrK270_302GraGruX: TIntegerField;
    QrK270_302OriOpeProc: TSmallintField;
    QrK270_302OrigemIDKnd: TIntegerField;
    QrK270_302OriSPEDEFDKnd: TIntegerField;
    QrK270_302Lk: TIntegerField;
    QrK270_302DataCad: TDateField;
    QrK270_302DataAlt: TDateField;
    QrK270_302UserCad: TIntegerField;
    QrK270_302UserAlt: TIntegerField;
    QrK270_302AlterWeb: TSmallintField;
    QrK270_302Ativo: TSmallintField;
    DsK270_302: TDataSource;
    QrK300DT_PROD_Txt: TWideStringField;
    QrK_ConfGProducaoCompartilhada: TFloatField;
    QrK_ConfGConsumoCompartilhado: TFloatField;
    QrK280IDSeq1: TIntegerField;
    QrK280OriBalID: TIntegerField;
    QrK280OriKndReg: TIntegerField;
    QrK220IDSeq1: TIntegerField;
    QrK200ID_SEK: TIntegerField;
    QrK215ID_SEK: TSmallintField;
    QrK220QTD_ORI: TFloatField;
    QrK220QTD_DEST: TFloatField;
    QrK280ID_SEK: TIntegerField;
    QrK200GRUPO: TWideStringField;
    QrK280GRUPO: TWideStringField;
    frxE_K200_001: TfrxReport;
    frxDsK200: TfrxDBDataset;
    frxDsK280: TfrxDBDataset;
    procedure QrE001AfterScroll(DataSet: TDataSet);
    procedure QrE001BeforeClose(DataSet: TDataSet);
    procedure QrE001AfterOpen(DataSet: TDataSet);
    procedure QrE100AfterOpen(DataSet: TDataSet);
    procedure QrE100AfterScroll(DataSet: TDataSet);
    procedure QrE100BeforeClose(DataSet: TDataSet);
    procedure QrE110AfterScroll(DataSet: TDataSet);
    procedure QrE110BeforeClose(DataSet: TDataSet);
    procedure QrE111AfterScroll(DataSet: TDataSet);
    procedure QrE111BeforeClose(DataSet: TDataSet);
    procedure QrE500AfterScroll(DataSet: TDataSet);
    procedure QrE500AfterOpen(DataSet: TDataSet);
    procedure QrE500BeforeClose(DataSet: TDataSet);
    procedure QrE520AfterOpen(DataSet: TDataSet);
    procedure QrE520AfterScroll(DataSet: TDataSet);
    procedure QrE520BeforeClose(DataSet: TDataSet);
    procedure QrH005AfterOpen(DataSet: TDataSet);
    procedure QrH005AfterScroll(DataSet: TDataSet);
    procedure QrH005BeforeClose(DataSet: TDataSet);
    procedure QrH010AfterOpen(DataSet: TDataSet);
    procedure QrH010AfterScroll(DataSet: TDataSet);
    procedure QrH010BeforeClose(DataSet: TDataSet);
    procedure QrK100AfterOpen(DataSet: TDataSet);
    procedure QrK100AfterScroll(DataSet: TDataSet);
    procedure QrK100BeforeClose(DataSet: TDataSet);
    procedure QrK210AfterScroll(DataSet: TDataSet);
    procedure QrK210BeforeClose(DataSet: TDataSet);
    procedure QrK230AfterScroll(DataSet: TDataSet);
    procedure QrK230BeforeClose(DataSet: TDataSet);
    procedure QrK250AfterScroll(DataSet: TDataSet);
    procedure QrK250BeforeClose(DataSet: TDataSet);
    procedure QrK260AfterScroll(DataSet: TDataSet);
    procedure QrK260BeforeClose(DataSet: TDataSet);
    procedure QrK270_210AfterScroll(DataSet: TDataSet);
    procedure QrK270_210BeforeClose(DataSet: TDataSet);
    procedure QrK270_220AfterScroll(DataSet: TDataSet);
    procedure QrK270_220BeforeClose(DataSet: TDataSet);
    procedure QrK270_230AfterScroll(DataSet: TDataSet);
    procedure QrK270_230BeforeClose(DataSet: TDataSet);
    procedure QrK270_250AfterScroll(DataSet: TDataSet);
    procedure QrK270_250BeforeClose(DataSet: TDataSet);
    procedure QrK270_260AfterScroll(DataSet: TDataSet);
    procedure QrK270_260BeforeClose(DataSet: TDataSet);
    procedure frxE_K200_001GetValue(const VarName: string; var Value: Variant);
    procedure QrK290AfterScroll(DataSet: TDataSet);
    procedure QrK290BeforeClose(DataSet: TDataSet);
    procedure QrK300AfterScroll(DataSet: TDataSet);
    procedure QrK300BeforeClose(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FBtE100, FBtE110, FBtE111, FBtE115, FBtE116, FBtE500, FBtE520, FBtE530,
    FBtH005, FBtH010, FBtH020, FBtK100, FBtK200, FBtK220, FBtK230, FBtK280,
    FBt1010, FBtVerifica, FBtDifGera, FBtDifImprime: TBitBtn;
    FVeriKItsShow, FImporExpor, FEmpresa: Integer;
    FNO_Empresa: String;
    //
    procedure ImprimeErrEmpresa();
    procedure ImprimeItensProduzidos();
    procedure ImprimeK200_K280();
    procedure ImprimeK220();
    procedure ImprimeVerificacaoBlocoK();
    //
    procedure ReopenE001(AnoMes: Integer);
    procedure ReopenE100(LinArq: Integer);
    procedure ReopenE110();
    procedure ReopenE111(LinArq: Integer);
    procedure ReopenE112(LinArq: Integer);
    procedure ReopenE113(LinArq: Integer);
    procedure ReopenE115(LinArq: Integer);
    procedure ReopenE116(LinArq: Integer);
    procedure ReopenE500(LinArq: Integer);
    procedure ReopenE510(LinArq: Integer);
    procedure ReopenE520();
    procedure ReopenE530(LinArq: Integer);
    procedure ReopenH005(LinArq: Integer);
    procedure ReopenH010(LinArq: Integer);
    procedure ReopenH020(LinArq: Integer);
    procedure ReopenK100(PeriApu: Integer);
    procedure ReopenK200(KndTab, KndItm: Integer; DT_EST: TDateTime; ReopenKind: TReopenKind);
    procedure ReopenK210(IDSeq1: Integer);
    procedure ReopenK215(IDSeq2: Integer);
    procedure ReopenK220(KndItmOri, KndItmDst: Integer);
    procedure ReopenK230(IDSeq1: Integer);
    procedure ReopenK235(IDSeq2: Integer);
    procedure ReopenK250(IDSeq1: Integer);
    procedure ReopenK255(IDSeq2: Integer);
    procedure ReopenK260(IDSeq1: Integer);
    procedure ReopenK265(IDSeq2: Integer);
    procedure ReopenK270_210(IDSeq1: Integer);
    procedure ReopenK270_220(IDSeq1: Integer);
    procedure ReopenK270_230(IDSeq1: Integer);
    procedure ReopenK270_250(IDSeq1: Integer);
    procedure ReopenK270_260(IDSeq1: Integer);
    procedure ReopenK270_291(IDSeq1: Integer);
    procedure ReopenK270_292(IDSeq1: Integer);
    procedure ReopenK270_301(IDSeq1: Integer);
    procedure ReopenK270_302(IDSeq1: Integer);
    procedure ReopenK275_215(IDSeq2: Integer);
    procedure ReopenK275_220(IDSeq2: Integer);
    procedure ReopenK275_235(IDSeq2: Integer);
    procedure ReopenK275_255(IDSeq2: Integer);
    procedure ReopenK275_265(IDSeq2: Integer);
    procedure ReopenK280(KndTab, KndItm: Integer; DT_EST: TDateTime; ReopenKind: TReopenKind);
    procedure ReopenK290(IDSeq1: Integer);
    procedure ReopenK291(IDSeq2: Integer);
    procedure ReopenK292(IDSeq2: Integer);
    procedure ReopenK300(IDSeq1: Integer);
    procedure ReopenK301(IDSeq2: Integer);
    procedure ReopenK302(IDSeq2: Integer);

    procedure ReopenK_ConfG(GraGruX: Integer);
    procedure Reopen1010();
  end;

var
  DfSEII_v03_0_1: TDfSEII_v03_0_1;

implementation

{$R *.dfm}

uses Module, DmkDAC_PF, ModuleGeral;

procedure TDfSEII_v03_0_1.FormCreate(Sender: TObject);
begin
  FVeriKItsShow := 0;
end;

procedure TDfSEII_v03_0_1.frxE_K200_001GetValue(const VarName: string;
  var Value: Variant);
(*
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
*)
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', FNO_Empresa(*CBEmpresa.Text*), FEmpresa(*EdEmpresa.ValueVariant*), 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_SPED' then
    Value := QrE001Mes_Ano.Value
  else
  if VarName ='VARF_DATA_IMP' then
    Value := Now()
  else
  if VarName ='VARF_NoMovimCod_220' then
    Value := AppPF.ObtemNomeDeIDMovIts(QrK220KndAID.Value)
  else
  if VarName ='VARF_NoMovimCod_270' then
    Value := AppPF.ObtemNomeDeIDMovIts(QrK270_220KndAID.Value);
end;

procedure TDfSEII_v03_0_1.ImprimeErrEmpresa();
var
  SQL_Periodo: String;
  DiaIni, DiaFim: TDateTime;
begin
  DiaIni      := Geral.AnoMesToData(QrE001AnoMes.Value, 1);
  DiaFim      := IncMonth(DiaIni, 1) -1;
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataHora ', DiaIni, DiaFim, True, True);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrEmpresa, Dmod.MyDB, [
 'SELECT vmi.SrcMovID, vmi.SrcNivel1, vmi.SrcNivel2, ',
 'vmi.Codigo, vmi.MovimID, vmi.MovimCod, vmi.Empresa,  ',
 'vmi.GraGruX, Pecas, AreaM2, PesoKg, med.Grandeza, ',
 'CONCAT(gg1.Nome, IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
 'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) NO_PRD_TAM_COR ',
 'FROM ' + CO_SEL_TAB_VMI + ' vmi   ',
 'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX   ',
 'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1   ',
 'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed   ',
 'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
 'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
 'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
 SQL_Periodo,
 'AND Empresa>-11 ', // N�o � uma empresa!
 '']);
 //
  MyObjects.frxDefineDataSets(frxErrEmpresa, [
  DModG.frxDsDono,
  frxDsErrEmpresa
  ]);
  //
  MyObjects.frxMostra(frxErrEmpresa, 'Itens erros de empresa');
end;

procedure TDfSEII_v03_0_1.ImprimeItensProduzidos();
begin
  //ReopenK230(0, rkPrint);
  //
  MyObjects.frxDefineDataSets(frxE_K230_001, [
  DModG.frxDsDono,
  frxDsK210,
  frxDsK215,
  frxDsK230,
  frxDsK235,
  frxDsK250,
  frxDsK255,
  //frxDsK260,
  //frxDsK265,
  //frxDsK290,
  //frxDsK291,
  //frxDsK292,
  //frxDsK300,
  //frxDsK301,
  //frxDsK302,
  frxDsK270_230,
  frxDsK275_235
  //frxDsK270_291,
  //frxDsK275_292
  //frxDsK270_301,
  //frxDsK275_302
  //frxDsK280;
  ]);
  //
  MyObjects.frxMostra(frxE_K230_001, 'Itens produzidos');
end;

procedure TDfSEII_v03_0_1.ImprimeK200_K280();
begin
  DfSEII_v03_0_1.ReopenK200(0, 0, 0, rkPrint);
  DfSEII_v03_0_1.ReopenK280(0, 0, 0, rkPrint);
  //
  MyObjects.frxDefineDataSets(frxE_K200_001, [
  DModG.frxDsDono,
  frxDsK200,
  frxDsK280
  ]);
  //
  MyObjects.frxMostra(frxE_K200_001, 'K200');
end;

procedure TDfSEII_v03_0_1.ImprimeK220();
begin
  //ReopenK220(0, rkPrint);
  //
  MyObjects.frxDefineDataSets(frxE_K220_001, [
  DModG.frxDsDono,
  frxDsK220,
  frxDsK270_220,
  frxDsK275_220
  ]);
  //
  MyObjects.frxMostra(frxE_K220_001, 'K220');
end;

procedure TDfSEII_v03_0_1.ImprimeVerificacaoBlocoK();
begin
  MyObjects.frxDefineDataSets(frxE_K_VERIF_001, [
  DModG.frxDsDono,
  frxDsK_ConfG
  ]);
  //
  MyObjects.frxMostra(frxE_K_VERIF_001, 'Verifica bloco K');
end;

procedure TDfSEII_v03_0_1.QrE001AfterOpen(DataSet: TDataSet);
begin
  FBtE100.Enabled := True;
  FBtE500.Enabled := True;
  FBtH005.Enabled := True;
  FBtK100.Enabled := True;
  FBt1010.Enabled := True;
end;

procedure TDfSEII_v03_0_1.QrE001AfterScroll(DataSet: TDataSet);
begin
  ReopenE100(0);
  ReopenE500(0);
  ReopenH005(0);
  ReopenK100(0);
  Reopen1010();
end;

procedure TDfSEII_v03_0_1.QrE001BeforeClose(DataSet: TDataSet);
begin
  FBtE100.Enabled := False;
  FBtE500.Enabled := False;
  FBtH005.Enabled := False;
  FBtK100.Enabled := False;
  FBt1010.Enabled := False;
  //
  QrE100.Close;
  QrE500.Close;
  QrH005.Close;
  QrK100.Close;
  Qr1010.Close;
end;

procedure TDfSEII_v03_0_1.QrE100AfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrE100.RecordCount > 0;
  FBtE110.Enabled := Habilita;
  FBtE111.Enabled := Habilita;
  FBtE115.Enabled := Habilita;
  FBtE116.Enabled := Habilita;
end;

procedure TDfSEII_v03_0_1.QrE100AfterScroll(DataSet: TDataSet);
begin
  ReopenE110();
end;

procedure TDfSEII_v03_0_1.QrE100BeforeClose(DataSet: TDataSet);
begin
  FBtE110.Enabled := False;
  QrE110.Close;
end;

procedure TDfSEII_v03_0_1.QrE110AfterScroll(DataSet: TDataSet);
begin
  ReopenE111(0);
  ReopenE115(0);
  ReopenE116(0);
end;

procedure TDfSEII_v03_0_1.QrE110BeforeClose(DataSet: TDataSet);
begin
  FBtE110.Enabled := False;
  FBtE110.Enabled := False;
  FBtE116.Enabled := False;
  QrE111.Close;
  QrE115.Close;
  QrE116.Close;
end;

procedure TDfSEII_v03_0_1.QrE111AfterScroll(DataSet: TDataSet);
begin
  ReopenE112(0);
  ReopenE113(0);
end;

procedure TDfSEII_v03_0_1.QrE111BeforeClose(DataSet: TDataSet);
begin
  //BtE112.Enabled := False;
  //BtE113.Enabled := False;
  QrE112.Close;
  QrE113.Close;
end;

procedure TDfSEII_v03_0_1.QrE500AfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrE500.RecordCount > 0;
  FBtE520.Enabled := Habilita;
  FBtE530.Enabled := Habilita;
end;

procedure TDfSEII_v03_0_1.QrE500AfterScroll(DataSet: TDataSet);
begin
  ReopenE510(0);
  ReopenE520();
end;

procedure TDfSEII_v03_0_1.QrE500BeforeClose(DataSet: TDataSet);
begin
  //BtE510.Enabled := False;
  FBtE520.Enabled := False;
  QrE510.Close;
  QrE520.Close;
end;

procedure TDfSEII_v03_0_1.QrE520AfterOpen(DataSet: TDataSet);
begin
  FBtE530.Enabled := QrE520.RecordCount > 0;
end;

procedure TDfSEII_v03_0_1.QrE520AfterScroll(DataSet: TDataSet);
begin
  ReopenE530(0);
end;

procedure TDfSEII_v03_0_1.QrE520BeforeClose(DataSet: TDataSet);
begin
  FBtE530.Enabled := False;
  QrE530.Close;
end;

procedure TDfSEII_v03_0_1.QrH005AfterOpen(DataSet: TDataSet);
begin
  FBtH010.Enabled := QrH005.RecordCount > 0;
end;

procedure TDfSEII_v03_0_1.QrH005AfterScroll(DataSet: TDataSet);
begin
  ReopenH010(0);
end;

procedure TDfSEII_v03_0_1.QrH005BeforeClose(DataSet: TDataSet);
begin
  QrH010.Close;
  FBtH010.Enabled := False;
end;

procedure TDfSEII_v03_0_1.QrH010AfterOpen(DataSet: TDataSet);
begin
  FBtH020.Enabled := QrH010.RecordCount > 0;
end;

procedure TDfSEII_v03_0_1.QrH010AfterScroll(DataSet: TDataSet);
begin
  ReopenH020(0);
end;

procedure TDfSEII_v03_0_1.QrH010BeforeClose(DataSet: TDataSet);
begin
  QrH020.Close;
  FBtH020.Enabled := False;
end;

procedure TDfSEII_v03_0_1.QrK100AfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrK100.RecordCount > 0;
  FBtK200.Enabled := Habilita;
  FBtK220.Enabled := Habilita;
  FBtK230.Enabled := Habilita;
  FBtK280.Enabled := Habilita;
  FBtVerifica.Enabled := Habilita;
  FBtDifGera.Enabled := Habilita;
  FBtDifImprime.Enabled := Habilita;
end;

procedure TDfSEII_v03_0_1.QrK100AfterScroll(DataSet: TDataSet);
begin
  ReopenK200(0, 0, 0, rkGrade);
  ReopenK210(0);
  ReopenK220(0, 0);
  ReopenK230(0);
  ReopenK250(0);
  ReopenK260(0);
  ReopenK290(0);
  ReopenK300(0);
  ReopenK270_210(0);
  ReopenK270_220(0);
  ReopenK270_230(0);
  ReopenK270_250(0);
  ReopenK270_260(0);
  ReopenK270_291(0);
  ReopenK270_292(0);
  ReopenK270_301(0);
  ReopenK270_302(0);
  ReopenK_ConfG(0);
  ReopenK280(0, 0, 0, rkGrade);
end;

procedure TDfSEII_v03_0_1.QrK100BeforeClose(DataSet: TDataSet);
begin
  FBtK200.Enabled := False;
  FBtK220.Enabled := False;
  FBtK230.Enabled := False;
  FBtVerifica.Enabled := False;
  FBtDifGera.Enabled := False;
  FBtDifImprime.Enabled := False;
  //
  QrK200.Close;
  QrK220.Close;
  QrK230.Close;
  QrK250.Close;
  QrK_ConfG.Close;
end;

procedure TDfSEII_v03_0_1.QrK210AfterScroll(DataSet: TDataSet);
begin
 ReopenK215(0);
end;

procedure TDfSEII_v03_0_1.QrK210BeforeClose(DataSet: TDataSet);
begin
  QrK215.Close;
end;

procedure TDfSEII_v03_0_1.QrK230AfterScroll(DataSet: TDataSet);
begin
 ReopenK235(0);
end;

procedure TDfSEII_v03_0_1.QrK230BeforeClose(DataSet: TDataSet);
begin
  QrK235.Close;
end;

procedure TDfSEII_v03_0_1.QrK250AfterScroll(DataSet: TDataSet);
begin
  ReopenK255(0);
end;

procedure TDfSEII_v03_0_1.QrK250BeforeClose(DataSet: TDataSet);
begin
  QrK255.Close;
end;

procedure TDfSEII_v03_0_1.QrK260AfterScroll(DataSet: TDataSet);
begin
  ReopenK265(0);
end;

procedure TDfSEII_v03_0_1.QrK260BeforeClose(DataSet: TDataSet);
begin
  QrK265.Close;
end;

procedure TDfSEII_v03_0_1.QrK270_210AfterScroll(DataSet: TDataSet);
begin
  ReopenK275_215(0);
end;

procedure TDfSEII_v03_0_1.QrK270_210BeforeClose(DataSet: TDataSet);
begin
  QrK275_215.Close;
end;

procedure TDfSEII_v03_0_1.QrK270_220AfterScroll(DataSet: TDataSet);
begin
  ReopenK275_220(0);
end;

procedure TDfSEII_v03_0_1.QrK270_220BeforeClose(DataSet: TDataSet);
begin
  QrK275_220.Close;
end;

procedure TDfSEII_v03_0_1.QrK270_230AfterScroll(DataSet: TDataSet);
begin
  ReopenK275_235(0);
end;

procedure TDfSEII_v03_0_1.QrK270_230BeforeClose(DataSet: TDataSet);
begin
  QrK275_235.Close;
end;

procedure TDfSEII_v03_0_1.QrK270_250AfterScroll(DataSet: TDataSet);
begin
  ReopenK275_255(0);
end;

procedure TDfSEII_v03_0_1.QrK270_250BeforeClose(DataSet: TDataSet);
begin
  QrK275_255.Close;
end;

procedure TDfSEII_v03_0_1.QrK270_260AfterScroll(DataSet: TDataSet);
begin
  ReopenK275_265(0);
end;

procedure TDfSEII_v03_0_1.QrK270_260BeforeClose(DataSet: TDataSet);
begin
  QrK275_255.Close;
end;

procedure TDfSEII_v03_0_1.QrK290AfterScroll(DataSet: TDataSet);
begin
  ReopenK291(0);
  ReopenK292(0);
end;

procedure TDfSEII_v03_0_1.QrK290BeforeClose(DataSet: TDataSet);
begin
  QrK291.Close;
  QrK292.Close;
end;

procedure TDfSEII_v03_0_1.QrK300AfterScroll(DataSet: TDataSet);
begin
  ReopenK301(0);
  ReopenK302(0);
end;

procedure TDfSEII_v03_0_1.QrK300BeforeClose(DataSet: TDataSet);
begin
  QrK301.Close;
  QrK302.Close;
end;

procedure TDfSEII_v03_0_1.Reopen1010();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr1010, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipi1010 l010 ',
  'WHERE l010.ImporExpor=' + Geral.FF0(QrE001ImporExpor.Value),
  'AND l010.Empresa=' + Geral.FF0(QrE001Empresa.Value),
  'AND l010.AnoMes=' + Geral.FF0(QrE001AnoMes.Value),
  '']);
end;

procedure TDfSEII_v03_0_1.ReopenE001(AnoMes: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrE001, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT, ',
  'CONCAT(RIGHT(e001.AnoMes, 2), "/", ',
  'LEFT(LPAD(e001.AnoMes, 6, "0"), 4)) MES_ANO, e001.* ',
  'FROM efdicmsipie001 e001 ',
  'LEFT JOIN entidades ent ON ent.Codigo=e001.Empresa ',
  'WHERE e001.ImporExpor=' + Geral.FF0(FImporExpor),
  'AND e001.Empresa=' + Geral.FF0(FEmpresa),
  'ORDER BY e001.AnoMes DESC ',
  '']);
  //
  QrE001.Locate('AnoMes', AnoMes, []);
end;

procedure TDfSEII_v03_0_1.ReopenE100(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrE100, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie100 e100 ',
  'WHERE e100.ImporExpor=' + Geral.FF0(QrE001ImporExpor.Value),
  'AND e100.Empresa=' + Geral.FF0(QrE001Empresa.Value),
  'AND e100.AnoMes=' + Geral.FF0(QrE001AnoMes.Value),
  'ORDER BY e100.DT_INI',
  '']);
  //
  QrE100.Locate('LinArq', LinArq, []);
end;

procedure TDfSEII_v03_0_1.ReopenE110();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrE110, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie110 E110 ',
  'WHERE E110.ImporExpor=' + Geral.FF0(QrE001ImporExpor.Value),
  'AND E110.Empresa=' + Geral.FF0(QrE001Empresa.Value),
  'AND E110.AnoMes=' + Geral.FF0(QrE001AnoMes.Value),
  'AND E110.LinArq=' + Geral.FF0(QrE100LinArq.Value),
  '']);
end;

procedure TDfSEII_v03_0_1.ReopenE111(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrE111, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie111 E111 ',
  'WHERE E111.ImporExpor=' + Geral.FF0(QrE110ImporExpor.Value),
  'AND E111.Empresa=' + Geral.FF0(QrE110Empresa.Value),
  'AND E111.AnoMes=' + Geral.FF0(QrE110AnoMes.Value),
  'AND E111.E110=' + Geral.FF0(QrE110LinArq.Value),
  '']);
end;

procedure TDfSEII_v03_0_1.ReopenE112(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrE112, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie112 E112 ',
  'WHERE E112.ImporExpor=' + Geral.FF0(QrE111ImporExpor.Value),
  'AND E112.Empresa=' + Geral.FF0(QrE111Empresa.Value),
  'AND E112.AnoMes=' + Geral.FF0(QrE111AnoMes.Value),
  'AND E112.E111=' + Geral.FF0(QrE111LinArq.Value),
  '']);
end;

procedure TDfSEII_v03_0_1.ReopenE113(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrE113, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie113 E113 ',
  'WHERE E113.ImporExpor=' + Geral.FF0(QrE111ImporExpor.Value),
  'AND E113.Empresa=' + Geral.FF0(QrE111Empresa.Value),
  'AND E113.AnoMes=' + Geral.FF0(QrE111AnoMes.Value),
  'AND E113.E111=' + Geral.FF0(QrE111LinArq.Value),
  '']);
end;

procedure TDfSEII_v03_0_1.ReopenE115(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrE115, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie115 E115 ',
  'WHERE E115.ImporExpor=' + Geral.FF0(QrE110ImporExpor.Value),
  'AND E115.Empresa=' + Geral.FF0(QrE110Empresa.Value),
  'AND E115.AnoMes=' + Geral.FF0(QrE110AnoMes.Value),
  'AND E115.E110=' + Geral.FF0(QrE110LinArq.Value),
  '']);
end;

procedure TDfSEII_v03_0_1.ReopenE116(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrE116, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie116 E116 ',
  'WHERE E116.ImporExpor=' + Geral.FF0(QrE110ImporExpor.Value),
  'AND E116.Empresa=' + Geral.FF0(QrE110Empresa.Value),
  'AND E116.AnoMes=' + Geral.FF0(QrE110AnoMes.Value),
  'AND E116.E110=' + Geral.FF0(QrE110LinArq.Value),
  '']);
end;

procedure TDfSEII_v03_0_1.ReopenE500(LinArq: Integer);
var
  ATT_IND_APUR: String;
begin
  ATT_IND_APUR := dmkPF.ArrayToTexto('e500.IND_APUR', 'NO_IND_APUR', pvPos, True,
    sEFD_IND_APUR);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrE500, Dmod.MyDB, [
  //'SELECT ELT(e500.IND_APUR + 1, "Mensal", ',
  //'"Decendial", "???") NO_IND_APUR, e500.* ',
  'SELECT ',
  ATT_IND_APUR,
  'e500.* ',
  'FROM efdicmsipie500 e500 ',
  'WHERE e500.ImporExpor=' + Geral.FF0(QrE001ImporExpor.Value),
  'AND e500.Empresa=' + Geral.FF0(QrE001Empresa.Value),
  'AND e500.AnoMes=' + Geral.FF0(QrE001AnoMes.Value),
  'ORDER BY e500.DT_INI, e500.DT_FIN',
  '']);
  //
  QrE500.Locate('LinArq', LinArq, []);
end;

procedure TDfSEII_v03_0_1.ReopenE510(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrE510, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie510 E510 ',
  'WHERE E510.ImporExpor=' + Geral.FF0(QrE001ImporExpor.Value),
  'AND E510.Empresa=' + Geral.FF0(QrE001Empresa.Value),
  'AND E510.AnoMes=' + Geral.FF0(QrE001AnoMes.Value),
  //'AND E510.LinArq=' + Geral.FF0(QrE500LinArq.Value),
  'AND E510.E500=' + Geral.FF0(QrE500LinArq.Value),
  '']);
  //
  QrE510.Locate('LinArq', LinArq, []);
end;

procedure TDfSEII_v03_0_1.ReopenE520;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrE520, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie520 E520 ',
  'WHERE E520.ImporExpor=' + Geral.FF0(QrE001ImporExpor.Value),
  'AND E520.Empresa=' + Geral.FF0(QrE001Empresa.Value),
  'AND E520.AnoMes=' + Geral.FF0(QrE001AnoMes.Value),
  //'AND E520.LinArq=' + Geral.FF0(QrE500LinArq.Value),
  'AND E520.E500=' + Geral.FF0(QrE500LinArq.Value),
  '']);
end;

procedure TDfSEII_v03_0_1.ReopenE530(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrE530, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie530 E530 ',
  'WHERE E530.ImporExpor=' + Geral.FF0(QrE520ImporExpor.Value),
  'AND E530.Empresa=' + Geral.FF0(QrE520Empresa.Value),
  'AND E530.AnoMes=' + Geral.FF0(QrE520AnoMes.Value),
  'AND E530.E520=' + Geral.FF0(QrE520LinArq.Value),
  '']);
  //
  QrE530.Locate('LinArq', LinArq, []);
end;

procedure TDfSEII_v03_0_1.ReopenH005(LinArq: Integer);
var
  ATT_MOT_INV: String;
begin
  ATT_MOT_INV := dmkPF.ArrayToTexto('h005.MOT_INV', 'NO_MOT_INV', pvPos, True,
    sEFD_MOT_INV);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrH005, Dmod.MyDB, [
  'SELECT ',
  ATT_MOT_INV,
  'h005.* ',
  'FROM efdicmsipih005 h005 ',
  'WHERE h005.ImporExpor=' + Geral.FF0(QrE001ImporExpor.Value),
  'AND h005.Empresa=' + Geral.FF0(QrE001Empresa.Value),
  'AND h005.AnoMes=' + Geral.FF0(QrE001AnoMes.Value),
  'ORDER BY h005.DT_INV',
  '']);
  //
  Qrh005.Locate('LinArq', LinArq, []);
end;

procedure TDfSEII_v03_0_1.ReopenH010(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrH010, Dmod.MyDB, [
  'SELECT IF(h010.COD_PART="", "", IF(ter.Tipo=0, ',
  'ter.RazaoSocial, ter.Nome)) NO_PART, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, h010.* ',
  'FROM efdicmsipih010 h010 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=h010.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'LEFT JOIN entidades  ter ON ter.Codigo=h010.COD_PART ',
  'WHERE h010.ImporExpor=' + Geral.FF0(QrE001ImporExpor.Value),
  'AND h010.Empresa=' + Geral.FF0(QrE001Empresa.Value),
  'AND h010.AnoMes=' + Geral.FF0(QrE001AnoMes.Value),
  'AND h010.H005=' + Geral.FF0(QrH005LinArq.Value),
  '']);
  //
  //Geral.MB_SQL(Self, QrH010);
  QrH010.Locate('LinArq', LinArq, []);
end;

procedure TDfSEII_v03_0_1.ReopenH020(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrH020, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipih020 H020 ',
  'WHERE H020.ImporExpor=' + Geral.FF0(QrE001ImporExpor.Value),
  'AND H020.Empresa=' + Geral.FF0(QrE001Empresa.Value),
  'AND H020.AnoMes=' + Geral.FF0(QrE001AnoMes.Value),
  'AND H020.H010=' + Geral.FF0(QrH010LinArq.Value),
  '']);
  //
  QrH020.Locate('LinArq', LinArq, []);
end;

procedure TDfSEII_v03_0_1.ReopenK100(PeriApu: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK100, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipik100 k100 ',
  'WHERE k100.ImporExpor=' + Geral.FF0(QrE001ImporExpor.Value),
  'AND k100.Empresa=' + Geral.FF0(QrE001Empresa.Value),
  'AND k100.AnoMes=' + Geral.FF0(QrE001AnoMes.Value),
  'ORDER BY k100.DT_INI, k100.DT_FIN ',
  '']);
  //
  QrK100.Locate('PeriApu', PeriApu, []);
end;

procedure TDfSEII_v03_0_1.ReopenK200(KndTab, KndItm: Integer; DT_EST: TDateTime; ReopenKind: TReopenKind);
var
  ATT_IND_EST, ATT_SORC_TAB, Ordem: String;
begin
  ATT_IND_EST := dmkPF.ArrayToTexto('k200.IND_EST', 'NO_IND_EST', pvPos, True,
    sEFD_IND_EST);
  //
  ATT_SORC_TAB := dmkPF.ArrayToTexto('k200.KndTab', 'NO_KndTab', pvPos, True,
    sEFD_SORC_TAB);
  //
  case ReopenKind of
    rkGrade: Ordem := 'ORDER BY DT_EST ';
    rkPrint: Ordem := 'ORDER BY IND_EST, NO_PART, GRUPO, NO_PRD_TAM_COR, DT_EST';
    else Ordem := '???';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrK200, Dmod.MyDB, [
  'SELECT IF(K200.COD_PART="", "", IF(ter.Tipo=0, ',
  'ter.RazaoSocial, ter.Nome)) NO_PART, ',
  'unm.Sigla, CONCAT(gg1.Nome, ',
  'IF(gtc.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  ATT_IND_EST,
  ATT_SORC_TAB,
  'K200.*, IF(gg1.PrdGrupTip=-2, "Insumo", "Produto") GRUPO  ',
  'FROM efdicmsipiK200 K200 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K200.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gratamcad  gtc ON gtc.Codigo=gti.Codigo ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN entidades  ter ON ter.Codigo=K200.COD_PART ',
  'WHERE k200.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k200.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k200.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k200.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  Ordem,
  '']);
  //
  //Geral.MB_SQL(Self, QrK200);
  //QrK200.Locate('PeriApu', PeriApu, []);
  QrK200.Locate('KndTab;KndItm;DT_EST', VarArrayOf([KndTab, KndItm, DT_EST]), []);
end;

procedure TDfSEII_v03_0_1.ReopenK210(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK210, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  //'IF(k210.DT_FIN_OS = 0, "", ',
  'IF(k210.DT_FIN_OS  < "1900-01-01", "", ',
  'DATE_FORMAT(k210.DT_FIN_OS, "%d/%m/%Y")) DT_FIN_OS_Txt, ',
  'K210.* ',
  'FROM efdicmsipiK210 k210 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K210.COD_ITEM_ORI ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE k210.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k210.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k210.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k210.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'ORDER BY DT_INI_OS, DT_FIN_OS, COD_DOC_OS, COD_ITEM ',
  '']);
  //
  //Geral.MB_SQL(Self, QrK210);
  QrK210.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK215(IDSeq2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK215, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'k215.* ',
  'FROM efdicmsipik215 k215 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k215.COD_ITEM_DES ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE k215.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k215.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k215.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k215.IDSeq1=' + Geral.FF0(QrK210.FieldByName('IDSeq1').AsInteger), //.Value),
  '']);
  //Geral.MB_SQL(Self, QrK215);
  //
  QrK215.Locate('IDSeq2', IDSeq2, []);
end;

procedure TDfSEII_v03_0_1.ReopenK220(KndItmOri, KndItmDst: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK220, Dmod.MyDB, [
  'SELECT  ',
  'CONCAT(gg1o.Nome, ',
  'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), ',
  'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) ',
  'NO_PRD_TAM_COR_ORI, ',
  'CONCAT(gg1d.Nome, ',
  'IF(gtid.PrintTam=0, "", CONCAT(" ", gtid.Nome)), ',
  'IF(gccd.PrintCor=0,"", CONCAT(" ", gccd.Nome))) ',
  'NO_PRD_TAM_COR_DEST, ',
  'IF(gg1o.UnidMed=gg1d.UnidMed, unmo.Sigla, "???") Sigla, ',
  'K220.* ',
  'FROM efdicmsipiK220 K220 ',
  'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K220.COD_ITEM_ORI ',
  'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC ',
  'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad ',
  'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI ',
  'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 ',
  'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed ',
  ' ',
  'LEFT JOIN gragrux    ggxd ON ggxd.Controle=K220.COD_ITEM_DEST ',
  'LEFT JOIN gragruc    ggcd ON ggcd.Controle=ggxd.GraGruC ',
  'LEFT JOIN gracorcad  gccd ON gccd.Codigo=ggcd.GraCorCad ',
  'LEFT JOIN gratamits  gtid ON gtid.Controle=ggxd.GraTamI ',
  'LEFT JOIN gragru1    gg1d ON gg1d.Nivel1=ggxd.GraGru1 ',
  'LEFT JOIN unidmed    unmd ON unmd.Codigo=gg1d.UnidMed ',
  'WHERE k220.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k220.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k220.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k220.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'ORDER BY k220.KndNSU, k220.KndCod, k220.DT_MOV, COD_ITEM_ORI, COD_ITEM_DEST ',
  '']);
  //Geral.MB_SQL(Self, QrK220);
  //
  QrK220.Locate('KndItmOri;KndItmDst', VarArrayOf([KndItmOri, KndItmDst]), []);
end;

procedure TDfSEII_v03_0_1.ReopenK230(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK230, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
//  'IF(k230.DT_FIN_OP = 0, "", ',
  'IF(k230.DT_FIN_OP  < "1900-01-01", "", ',
  'DATE_FORMAT(k230.DT_FIN_OP, "%d/%m/%Y")) DT_FIN_OP_Txt,',
  'K230.* ',
  'FROM efdicmsipiK230 k230 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K230.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k230.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k230.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k230.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k230.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'ORDER BY DT_INI_OP, DT_FIN_OP, COD_DOC_OP, COD_ITEM ',
  '']);
  //Geral.MB_SQL(Self, QrK230);
  QrK230.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK235(IDSeq2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK235, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,  ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, ',
  'k235.*  ',
  'FROM efdicmsipik235 k235 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k235.COD_ITEM  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   ',
  'WHERE k235.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k235.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k235.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k235.IDSeq1=' + Geral.FF0(QrK230IDSeq1.Value),
  '']);
  //Geral.MB_SQL(Self, QrK235);
  //
  QrK235.Locate('IDSeq2', IDSeq2, []);
end;

procedure TDfSEII_v03_0_1.ReopenK250(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK250, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
  //'IF(k250.DT_PROD = 0, "", ',
  'IF(k250.DT_PROD  < "1900-01-01", "", ',
  'DATE_FORMAT(k250.DT_PROD, "%d/%m/%Y")) DT_PROD_Txt,',
  'K250.* ',
  'FROM efdicmsipiK250 k250 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K250.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k250.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k250.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k250.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k250.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'ORDER BY DT_PROD, COD_DOC_OP, COD_ITEM ',
  '']);
  //
  QrK250.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK255(IDSeq2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK255, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,  ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, ',
  'k255.*  ',
  'FROM efdicmsipik255 k255 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k255.COD_ITEM  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   ',
  'WHERE k255.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k255.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k255.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k255.IDSeq1=' + Geral.FF0(QrK250IDSeq1.Value),
  '']);
  //
  QrK255.Locate('IDSeq2', IDSeq2, []);
end;

procedure TDfSEII_v03_0_1.ReopenK260(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK260, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,',
  'CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR,',
//  'IF(k260.DT_RET = 0, "",',
  'IF(k260.DT_RET  < "1900-01-01", "",',
  'DATE_FORMAT(k260.DT_RET, "%d/%m/%Y")) DT_RET_Txt,',
  'K260.*',
  'FROM efdicmsipiK260 k260',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K260.COD_ITEM',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed',
  'WHERE k260.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k260.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k260.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k260.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'ORDER BY DT_SAIDA, DT_RET, COD_OP_OS, COD_ITEM ',
  '']);
  //
  QrK260.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK265(IDSeq2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK265, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'k265.* ',
  'FROM efdicmsipik265 k265 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k265.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE k265.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k265.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k265.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k265.IDSeq1=' + Geral.FF0(QrK260IDSeq1.Value),
  '']);
  //
  QrK265.Locate('IDSeq2', IDSeq2, []);
  //Geral.MB_SQL(Self, QrK265);
end;

procedure TDfSEII_v03_0_1.ReopenK270_210(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK270_210, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, K270.* ',
  'FROM efdicmsipiK270 k270 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k270.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'AND k270.ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK210K215)),
  'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ',  '']);
  //
  //Geral.MB_SQL(Self, QrK270_210);
  QrK270_210.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK270_220(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK270_220, Dmod.MyDB, [
  'SELECT  ',
  'CONCAT(gg1o.Nome, ',
  'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), ',
  'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) ',
  'NO_PRD_TAM_COR, unmo.Sigla, ',
  'K270.* ',
  'FROM efdicmsipiK270 K270 ',
  'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K270.COD_ITEM',
  'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC ',
  'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad ',
  'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI ',
  'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 ',
  'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed ',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k270.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'AND k270.ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK220)),
  'ORDER BY k270.KndNSU, k270.KndCod, k270.DT_INI_AP, DT_FIN_AP, COD_ITEM ',
  '']);
  //Geral.MB_SQL(Self, QrK270);
  //
  QrK270_220.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK270_230(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK270_230, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
  'K270.* ',
  'FROM efdicmsipiK270 k270 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k270.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK230K235)),
  'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ',
  '']);
  //
  QrK270_230.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK270_250(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK270_250, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,',
  'CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, K270.*',
  'FROM efdicmsipiK270 k270',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k270.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK250K255)),
  'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM',
  '']);
  //
  QrK270_250.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK270_260(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK270_260, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,',
  'CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, K270.*',
  'FROM efdicmsipiK270 k270',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k270.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK260K265)),
  'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM',
  '']);
  //
  QrK270_260.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK270_291(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK270_291, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
  'K270.* ',
  'FROM efdicmsipiK270 k270 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k270.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK291)),
  'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ',
  '']);
  //
  QrK270_291.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK270_292(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK270_292, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
  'K270.* ',
  'FROM efdicmsipiK270 k270 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k270.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK292)),
  'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ',
  '']);
  //
  QrK270_292.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK270_301(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK270_301, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
  'K270.* ',
  'FROM efdicmsipiK270 k270 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k270.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK301)),
  'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ',
  '']);
  //
  QrK270_301.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK270_302(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK270_302, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
  'K270.* ',
  'FROM efdicmsipiK270 k270 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k270.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK302)),
  'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ',
  '']);
  //
  QrK270_302.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK275_215(IDSeq2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK275_215, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'k275.* ',
  'FROM efdicmsipik275 k275 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE k275.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k275.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k275.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k275.IDSeq1=' + Geral.FF0(QrK270_210IDSeq1.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK210K215)),
  '']);
  //Geral.MB_SQL(Self, QrK275);
  //
  QrK275_215.Locate('IDSeq2', IDSeq2, []);
end;

procedure TDfSEII_v03_0_1.ReopenK275_220(IDSeq2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK275_220, Dmod.MyDB, [
  'SELECT ',
  'CONCAT(gg1o.Nome, ',
  'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), ',
  'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) ',
  'NO_PRD_TAM_COR, unmo.Sigla, ',
  'K275.* ',
  'FROM efdicmsipiK275 K275 ',
  'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K275.COD_ITEM ',
  'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC ',
  'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad ',
  'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI ',
  'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 ',
  'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed ',
  'WHERE k275.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k275.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k275.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k275.IDSeq1=' + Geral.FF0(QrK270_220IDSeq1.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK220)),
  '']);
  //Geral.MB_SQL(Self, QrK275_220);
  //
  QrK275_220.Locate('IDSeq2', IDSeq2, []);
end;

procedure TDfSEII_v03_0_1.ReopenK275_235(IDSeq2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK275_235, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,  ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, ',
  'k275.*  ',
  'FROM efdicmsipik275 k275 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   ',
  'WHERE k275.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k275.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k275.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k275.IDSeq1=' + Geral.FF0(QrK270_230IDSeq1.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK230K235)),
  '']);
  //Geral.MB_SQL(Self, QrK275);
  //
  QrK275_235.Locate('IDSeq2', IDSeq2, []);
end;

procedure TDfSEII_v03_0_1.ReopenK275_255(IDSeq2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK275_255, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,  ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, ',
  'k275.*  ',
  'FROM efdicmsipik275 k275 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   ',
  'WHERE k275.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k275.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k275.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k275.IDSeq1=' + Geral.FF0(QrK270_250IDSeq1.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK250K255)),
  '']);
  //Geral.MB_SQL(Self, QrK275_255);
  //
  QrK275_255.Locate('IDSeq2', IDSeq2, []);
end;

procedure TDfSEII_v03_0_1.ReopenK275_265(IDSeq2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK275_265, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,  ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, ',
  'k275.*  ',
  'FROM efdicmsipik275 k275 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   ',
  'WHERE k275.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k275.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k275.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k275.IDSeq1=' + Geral.FF0(QrK270_260IDSeq1.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_KndRegOrigem.sek2708oriK260K265)),
  '']);
  //
  QrK275_265.Locate('IDSeq2', IDSeq2, []);
end;

procedure TDfSEII_v03_0_1.ReopenK280(KndTab, KndItm: Integer; DT_EST: TDateTime; ReopenKind: TReopenKind);
var
  ATT_IND_EST, ATT_SORC_TAB, ATT_OrigemSPEDEFDKnd, Ordem: String;
begin
  ATT_IND_EST := dmkPF.ArrayToTexto('k280.IND_EST', 'NO_IND_EST', pvPos, True,
    sEFD_IND_EST);
  //
  ATT_OrigemSPEDEFDKnd := dmkPF.ArrayToTexto('k280.OriSPEDEFDKnd',
  'NO_OriSPEDEFDKnd', pvPos, True, sOrigemSPEDEFDKnd);
  //
  ATT_SORC_TAB := dmkPF.ArrayToTexto('k280.KndTab', 'NO_KndTab', pvPos, True,
    sEFD_SORC_TAB);
  //
  case ReopenKind of
    rkGrade: Ordem := 'ORDER BY DT_EST ';
    rkPrint: Ordem := 'ORDER BY IND_EST, NO_PART, GRUPO, NO_PRD_TAM_COR, DT_EST';
    else Ordem := '???';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrK280, Dmod.MyDB, [
  'SELECT IF(K280.COD_PART="", "", IF(ter.Tipo=0, ',
  'ter.RazaoSocial, ter.Nome)) NO_PART, ',
  'unm.Sigla, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  ATT_IND_EST,
  ATT_SORC_TAB,
  ATT_OrigemSPEDEFDKnd,
  'K280.*, IF(gg1.PrdGrupTip=-2, "Insumo", "Produto") GRUPO',
  'FROM efdicmsipiK280 K280 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K280.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN entidades  ter ON ter.Codigo=K280.COD_PART ',
  'WHERE k280.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k280.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k280.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k280.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  Ordem,
  '']);
  //
  //Geral.MB_SQL(Self, QrK280);
  QrK280.Locate('KndTab;KndItm;DT_EST', VarArrayOf([KndTab, KndItm, DT_EST]), []);
end;

procedure TDfSEII_v03_0_1.ReopenK290(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK290, Dmod.MyDB, [
  //'SELECT IF(k290.DT_FIN_OP = 0, "", ',
  'SELECT IF(k290.DT_FIN_OP  < "1900-01-01", "", ',
  'DATE_FORMAT(k290.DT_FIN_OP, "%d/%m/%Y")) DT_FIN_OP_Txt,',
  'K290.* ',
  'FROM efdicmsipiK290 k290 ',
  'WHERE k290.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k290.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k290.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k290.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'ORDER BY DT_INI_OP, DT_FIN_OP, COD_DOC_OP ',
  '']);
  //Geral.MB_SQL(Self, QrK290);
  QrK290.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK291(IDSeq2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK291, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
  'K291.* ',
  'FROM efdicmsipiK291 k291 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K291.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k291.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k291.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k291.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k291.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'AND k291.IDSeq1=' + Geral.FF0(QrK290IDSeq1.Value),
  'ORDER BY GraGruX ',
  '']);
  //Geral.MB_SQL(Self, QrK291);
  QrK291.Locate('IDSeq2', IDSeq2, []);
end;

procedure TDfSEII_v03_0_1.ReopenK292(IDSeq2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK292, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
  'K292.* ',
  'FROM efdicmsipiK292 k292 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K292.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k292.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k292.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k292.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k292.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'AND k292.IDSeq1=' + Geral.FF0(QrK290IDSeq1.Value),
  'ORDER BY GraGruX ',
  '']);
  //Geral.MB_SQL(Self, QrK292);
  QrK292.Locate('IDSeq2', IDSeq2, []);
end;

procedure TDfSEII_v03_0_1.ReopenK300(IDSeq1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK300, Dmod.MyDB, [
  //'SELECT IF(k300.DT_PROD = 0, "", ',
  'SELECT IF(k300.DT_PROD  < "1900-01-01", "", ',
  'DATE_FORMAT(k300.DT_PROD, "%d/%m/%Y")) DT_PROD_Txt,',
  'K300.* ',
  'FROM efdicmsipiK300 k300 ',
  'WHERE k300.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k300.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k300.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k300.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'ORDER BY DT_PROD, KndNSU, KndCod ',
  '']);
  //Geral.MB_SQL(Self, QrK300);
  QrK300.Locate('IDSeq1', IDSeq1, []);
end;

procedure TDfSEII_v03_0_1.ReopenK301(IDSeq2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK301, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
  'K301.* ',
  'FROM efdicmsipiK301 k301 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K301.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k301.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k301.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k301.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k301.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'AND k301.IDSeq1=' + Geral.FF0(QrK300IDSeq1.Value),
  'ORDER BY GraGruX ',
  '']);
  //Geral.MB_SQL(Self, QrK301);
  QrK301.Locate('IDSeq2', IDSeq2, []);
end;

procedure TDfSEII_v03_0_1.ReopenK302(IDSeq2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrK302, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
  'K302.* ',
  'FROM efdicmsipiK302 k302 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K302.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k302.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND k302.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND k302.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  'AND k302.PeriApu=' + Geral.FF0(QrK100PeriApu.Value),
  'AND k302.IDSeq1=' + Geral.FF0(QrK300IDSeq1.Value),
  'ORDER BY GraGruX ',
  '']);
  //Geral.MB_SQL(Self, QrK302);
  QrK302.Locate('IDSeq2', IDSeq2, []);
end;

procedure TDfSEII_v03_0_1.ReopenK_ConfG(GraGruX: Integer);
var
  SQL_MPouPQ: String;
begin
  case FVeriKItsShow of
    1: SQL_MPouPQ := 'AND ggx.GraGruY <> 0';
    2: SQL_MPouPQ := 'AND ggx.GraGruY = 0';
    else SQL_MPouPQ := '';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrK_ConfG, Dmod.MyDB, [
  'SELECT ekc.GraGruX,  ',
  '  SUM(ekc.Movim01) Sdo_Ini, ',
  '  SUM(ekc.Movim02) Compra,  ',
  '  SUM(ekc.Movim03) Producao, ',
  '  SUM(ekc.Movim04) EntrouClasse,  ',
  '  SUM(ekc.Movim05) ProducaoCompartilhada, ',
  '  SUM(ekc.Movim06) EntrouDesmonte, ',
  '  SUM(ekc.Movim07) ProduReforma, ',

  //
  '  SUM(ekc.Movim21) Final,  ',
  '  SUM(ekc.Movim22) Venda, ',
  '  SUM(ekc.Movim23) Consumo, ',
  '  SUM(ekc.Movim24) SaiuClasse,  ',
  '  SUM(ekc.Movim25) ConsumoCompartilhado, ',
  '  SUM(ekc.Movim26) SaiuDesmonte, ',
  '  SUM(ekc.Movim27) InsumReforma, ',
  //
  '  SUM(ekc.Movim41) ETE, ',
  '  SUM(ekc.Movim42) BalancoIndev,  ',
  '  SUM(ekc.Movim43) SubProduto, ',
  //
  '  SUM(ekc.Movim45) ErrEmpresa,  ',
  '  SUM(ekc.Movim46) Indevido,  ',
  //
  ' (',
  '    SUM(ekc.Movim01) ',
  '  + SUM(ekc.Movim02) ',
  '  + SUM(ekc.Movim03) ',
  '  + SUM(ekc.Movim04) ',
  '  + SUM(ekc.Movim05) ',
  '  + SUM(ekc.Movim06) ',
  '  + SUM(ekc.Movim07) ',
  '  + SUM(ekc.Movim08) ',
  '  + SUM(ekc.Movim09) ',
  '  + SUM(ekc.Movim10) ',
  '  + SUM(ekc.Movim11) ',
  '  + SUM(ekc.Movim12) ',
  '  + SUM(ekc.Movim13) ',
  '  + SUM(ekc.Movim15) ',
  '  + SUM(ekc.Movim16) ',
  '  + SUM(ekc.Movim17) ',
  '  + SUM(ekc.Movim18) ',
  '  + SUM(ekc.Movim19) ',
  '  + SUM(ekc.Movim20) ',
  //
  ' ) + (',
  //
  '  - SUM(ekc.Movim21) ',
  '  + SUM(ekc.Movim22) ',
  '  + SUM(ekc.Movim23) ',
  '  + SUM(ekc.Movim24) ',
  '  + SUM(ekc.Movim25) ',
  '  + SUM(ekc.Movim26) ',
  '  - SUM(ekc.Movim27) ',
  '  + SUM(ekc.Movim28) ',
  '  + SUM(ekc.Movim29) ',
  '  + SUM(ekc.Movim30) ',
  '  + SUM(ekc.Movim31) ',
  '  + SUM(ekc.Movim32) ',
  '  + SUM(ekc.Movim33) ',
  '  + SUM(ekc.Movim35) ',
  '  + SUM(ekc.Movim36) ',
  '  + SUM(ekc.Movim37) ',
  '  + SUM(ekc.Movim38) ',
  '  + SUM(ekc.Movim39) ',
  '  + SUM(ekc.Movim40) ',
  //
  ' ) + (',
  //
  '    SUM(ekc.Movim41) ',
  '  + SUM(ekc.Movim42) ',
  '  + SUM(ekc.Movim43) ',
  '  + SUM(ekc.Movim44) ',
  '  + SUM(ekc.Movim45) ',
  '  + SUM(ekc.Movim46) ',
  '  + SUM(ekc.Movim47) ',
  '  + SUM(ekc.Movim48) ',
  '  + SUM(ekc.Movim49) ',
  '  + SUM(ekc.Movim50) ',
  //
  ' ) Diferenca,  ',
  //
  'med.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, ',
  'med.Grandeza, ekc.ESTSTabSorc  ',
  'FROM efdicmsipik_confg ekc  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=ekc.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed  ',
  'WHERE ekc.ImporExpor=' + Geral.FF0(QrK100ImporExpor.Value),
  'AND ekc.Empresa=' + Geral.FF0(QrK100Empresa.Value),
  'AND ekc.AnoMes=' + Geral.FF0(QrK100AnoMes.Value),
  SQL_MPouPQ,
  'GROUP BY GraGruX ',
  'ORDER BY GraGruX ',
  '']);
  //Geral.MB_SQL(Self, QrK_ConfG);
  //
  QrK_ConfG.Locate('GraGruX', GraGruX, []);
end;

end.
