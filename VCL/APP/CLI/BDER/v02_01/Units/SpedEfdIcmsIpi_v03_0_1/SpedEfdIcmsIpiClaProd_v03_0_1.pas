unit SpedEfdIcmsIpiClaProd_v03_0_1;
(* Origem: C:\_Compilers\Delphi_XE2\VCL\APP\CLI\BDER\v02_01\Units\SPED_EFD_K2XX.pas*)
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkCheckGroup, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Variants, dmkEditDateTimePicker,
  UnProjGroup_Consts, dmkDBGridZTO, SPED_Listas, TypInfo, UnAppEnums;

type
  TFmSpedEfdIcmsIpiClaProd_v03_0_1 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnPeriodo: TPanel;
    LaMes: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    LaAno: TLabel;
    PCRegistro: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    LaData: TLabel;
    TPData: TdmkEditDateTimePicker;
    MeAviso: TMemo;
    QrIDNiv: TmySQLQuery;
    QrIDNivMovimID: TIntegerField;
    QrIDNivMovimNiv: TIntegerField;
    QrConsParc: TmySQLQuery;
    QrConsParcPecas: TFloatField;
    QrConsParcAreaM2: TFloatField;
    QrReclMul: TmySQLQuery;
    QrReclMulData: TDateField;
    QrReclMulGGX_Dst: TIntegerField;
    QrReclMulGGX_Src: TIntegerField;
    QrReclMulPecas: TFloatField;
    QrReclMulAreaM2: TFloatField;
    QrReclMulPesoKg: TFloatField;
    PageControl1: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    dmkDBGridZTO2: TdmkDBGridZTO;
    dmkDBGridZTO3: TdmkDBGridZTO;
    QrClasMul: TmySQLQuery;
    QrClasMulData: TDateField;
    QrClasMulGGX_Dst: TIntegerField;
    QrClasMulGGX_Src: TIntegerField;
    QrClasMulPecas: TFloatField;
    QrClasMulAreaM2: TFloatField;
    QrClasMulPesoKg: TFloatField;
    DsCacIts: TDataSource;
    DsReclMul: TDataSource;
    DsClasMul: TDataSource;
    QrVSCalCab: TmySQLQuery;
    DsVSCalCab: TDataSource;
    QrVSCurCab: TmySQLQuery;
    DsVSCurCab: TDataSource;
    QrVSOpeCab: TmySQLQuery;
    DsVSOpeCab: TDataSource;
    QrSecO_P: TmySQLQuery;
    QrSecO_PData: TDateField;
    QrSecO_PGGX_Dst: TIntegerField;
    QrSecO_PGGX_Src: TIntegerField;
    QrSecO_PPecas: TFloatField;
    QrSecO_PAreaM2: TFloatField;
    QrSecO_PPesoKg: TFloatField;
    QrSecO_PGrandeza: TSmallintField;
    QrSecO_PMovimID: TIntegerField;
    TabSheet7: TTabSheet;
    DsSecO_P: TDataSource;
    dmkDBGridZTO5: TdmkDBGridZTO;
    PageControl2: TPageControl;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    TabSheet11: TTabSheet;
    dmkDBGridZTO4: TdmkDBGridZTO;
    dmkDBGridZTO6: TdmkDBGridZTO;
    dmkDBGridZTO7: TdmkDBGridZTO;
    dmkDBGridZTO8: TdmkDBGridZTO;
    QrVSPWECab: TmySQLQuery;
    DsVSPWECab: TDataSource;
    QrCacIts: TmySQLQuery;
    QrCacItsData: TDateField;
    QrCacItsGGX_Src: TIntegerField;
    QrCacItsGGX_Dst: TIntegerField;
    QrCacItsPecas: TFloatField;
    QrCacItsAreaM2: TFloatField;
    QrCacItsAreaP2: TFloatField;
    QrConsParcPesoKg: TFloatField;
    TabSheet12: TTabSheet;
    dmkDBGridZTO9: TdmkDBGridZTO;
    DsVmiGArIts: TDataSource;
    QrCacItsCodigo: TIntegerField;
    QrSecO_PCodigo: TIntegerField;
    QrSecO_PMovimCod: TIntegerField;
    QrCacItsVMI_Dest: TIntegerField;
    QrCacItsCacID: TIntegerField;
    QrSecO_PControle: TIntegerField;
    TabSheet13: TTabSheet;
    QrGGXRcl: TmySQLQuery;
    DsGGXRcl: TDataSource;
    dmkDBGridZTO10: TdmkDBGridZTO;
    QrGGXRclGGX_Src: TIntegerField;
    QrGGXRclGGX_Dst: TIntegerField;
    QrGGXRclGrandeza: TIntegerField;
    QrGGXRclPecas: TFloatField;
    QrGGXRclAreaM2: TFloatField;
    QrGGXRclPesoKg: TFloatField;
    QrGGXRclData: TDateField;
    QrGGXRclMovimID: TIntegerField;
    QrGGXRclCodigo: TIntegerField;
    QrGGXRclMovimCod: TIntegerField;
    QrGGXRclControle: TIntegerField;
    TabSheet14: TTabSheet;
    dmkDBGridZTO11: TdmkDBGridZTO;
    QrDesclasse: TmySQLQuery;
    DsDesclasse: TDataSource;
    QrDesclasseGGX_Dst: TIntegerField;
    QrDesclasseGdz_Dst: TIntegerField;
    QrDesclassePecas: TFloatField;
    QrDesclasseAreaM2: TFloatField;
    QrDesclassePesoKg: TFloatField;
    QrDesclasseData: TDateField;
    QrDesclasseMovimID: TIntegerField;
    QrDesclasseCodigo: TIntegerField;
    QrDesclasseMovimCod: TIntegerField;
    QrDesclasseControle: TIntegerField;
    QrDesclasseDstMovID: TIntegerField;
    QrDesclasseDstNivel1: TIntegerField;
    QrDesclasseDstNivel2: TIntegerField;
    QrDesclasseGGX_Src: TIntegerField;
    QrDesclasseGdz_Src: TIntegerField;
    QrCeR2: TmySQLQuery;
    QrSubPrdOP: TmySQLQuery;
    QrSubPrdOPData: TDateField;
    QrSubPrdOPGGX_Dst: TIntegerField;
    QrSubPrdOPGGX_Src: TIntegerField;
    QrSubPrdOPPecas: TFloatField;
    QrSubPrdOPAreaM2: TFloatField;
    QrSubPrdOPPesoKg: TFloatField;
    QrSubPrdOPGrandeza: TSmallintField;
    QrSubPrdOPMovimID: TIntegerField;
    QrSubPrdOPCodigo: TIntegerField;
    QrSubPrdOPMovimCod: TIntegerField;
    QrSubPrdOPControle: TIntegerField;
    QrFMO: TmySQLQuery;
    QrFMOFornecMO: TIntegerField;
    QrSecO_PEmpresa: TIntegerField;
    QrSubPrdOPEmpresa: TIntegerField;
    QrGGXRclMovimNiv: TIntegerField;
    QrSecO_PMovimNiv: TIntegerField;
    QrDesclasseMovimNiv: TIntegerField;
    QrIDs: TmySQLQuery;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    Panel6: TPanel;
    PB2: TProgressBar;
    LaAviso3: TLabel;
    LaAviso4: TLabel;
    QrVSPSPCab: TmySQLQuery;
    DsVSPSPCab: TDataSource;
    Panel7: TPanel;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    QrSecO_PDtCorrApo: TDateTimeField;
    QrSubPrdOPDtCorrApo: TDateTimeField;
    QrGGXRclDtCorrApo: TDateTimeField;
    QrDesclasseDtCorrApo: TDateTimeField;
    QrVSRRMCab: TmySQLQuery;
    QrProdParc: TmySQLQuery;
    QrProdParcPecas: TFloatField;
    QrProdParcAreaM2: TFloatField;
    QrProdParcPesoKg: TFloatField;
    QrSecO_PClientMO: TIntegerField;
    QrSecO_PFornecMO: TIntegerField;
    QrSubPrdOPClientMO: TIntegerField;
    QrSubPrdOPFornecMO: TIntegerField;
    QrProdParcClientMO: TIntegerField;
    QrProdParcFornecMO: TIntegerField;
    QrGGXRclClientMO: TIntegerField;
    QrGGXRclFornecMO: TIntegerField;
    QrDesclasseClientMO: TIntegerField;
    QrDesclasseFornecMO: TIntegerField;
    QrGGXRclEntiSitio: TIntegerField;
    QrSecO_PEntiSitio: TIntegerField;
    QrDesclasseEntiSitio: TIntegerField;
    QrProdParcControle: TIntegerField;
    QrConsParcAno: TFloatField;
    QrConsParcMes: TFloatField;
    QrProdParcAno: TFloatField;
    QrProdParcMes: TFloatField;
    QrBuscaCtrl: TmySQLQuery;
    QrBuscaCtrlControle: TIntegerField;
    CGItensK220: TdmkCheckGroup;
    Panel8: TPanel;
    BtTodos220: TBitBtn;
    BtNenhum220: TBitBtn;
    QrSumGAR: TmySQLQuery;
    QrSumGARPecas: TFloatField;
    QrSumGARAreaM2: TFloatField;
    QrSumGARPesoKg: TFloatField;
    Panel9: TPanel;
    CGImportar2xx: TdmkCheckGroup;
    RGProducao: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure PCRegistroChanging(Sender: TObject; var AllowChange: Boolean);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtTodos220Click(Sender: TObject);
    procedure BtNenhum220Click(Sender: TObject);
  private
    { Private declarations }
    FTipoPeriodoFiscal: TTipoPeriodoFiscal;
    FVersaoIsldCnjt: Integer;
    procedure Mensagem(ProcName: String; MovimCod, MovimID: Integer; TextoBase:
              String; Query: TmySQLQuery);
    function  DataFimObrigatoria(DtHrFimOpe: TDateTime): TDateTime;
    function  ErrGrandeza(TabSrc, GGxSrc, GGXDst: String): Boolean;
    function  GrandezasInconsistentes(): Boolean;
    function  ImportaClasseEReclasseVMI(SQL_Periodo: String): Boolean;
    function  ImportaClasseOpeProcC(SQL_Periodo: String): Boolean;
    function  ImportaClasseOpeProcB(SQL_Periodo: String): Boolean;
    function  ImportaClasseOpeProcA(SQL_Periodo: String): Boolean;
    function  ImportaClasseGGXRcl(SQL_Periodo: String): Boolean;
    function  ImportaDesclasseOpeProc(SQL_Periodo: String): Boolean;
{
    function  InsereItemAtual_K210(const MovimID, Codigo, MovimCod, Controle: Integer;
              const DataIni, DataFim, DtMovim, DtCorrApo: TDateTime; const
              GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde:
              Double; const OrigemOpeProc: TOrigemOpeProc; const OriIDKnd:
              TOrigemIDKnd; var IDSeq1: Integer; Agrega: Boolean = True): Boolean;
}
    function  InsereItemAtual_K220(MovimID: Integer; Data: TDateTime;
              GGXOri, GGXDst: Integer; AreaM2: Double; Codigo, MovimCod,
              CtrlDst, _CtrlOri, ClientMO, FornecMO, EntiSitio: Integer;
              ESOMIEM: TEstqSPED220OMIEM; MovimNiv: Integer;
              OrigemIDKnd: TOrigemIDKnd; DtCorrApo: TDateTime): Boolean;
{
    function  InsereItemAtual_K230(const MovimID, Codigo, MovimCod, Controle:
              Integer; const DataIni, DataFim, DtMovim, DtCorrApo: TDateTime;
              const GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde:
              Double; const OrigemOpeProc: TOrigemOpeProc; var IDSeq1:
              Integer; Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K250(const MovimID, Codigo, MovimCod, Controle:
              Integer; const DtHrFimOpe, DtMovim, DtCorrApo: TDateTime; const
              GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde:
              Double; const OrigemOpeProc: TOrigemOpeProc; var IDSeq1:
              Integer; Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K260(const MovimID, Codigo, MovimCod, Controle:
              Integer; const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd,
              DtCorrApoCons: TDateTime; const GraGruX(*, CtrlDst, CtrlBxa*),
              ClientMO, FornecMO, EntiSitio: Integer; const QtdeProd, QtdeCons:
              Double; const OrigemOpeProc: TOrigemOpeProc; var IDSeq1: Integer;
              const Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K215(IDSeq1: Integer; Data, DtMovim, DtCorrApo:
              TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
              ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
              EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriIDKnd:
              TOrigemIDKnd; OriESTSTabSorc: TEstqSPEDTabSorc): Boolean;
    function  InsereItemAtual_K235(IDSeq1: Integer; Data, DtCorrApo:
              TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
              ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
              EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
              TEstqSPEDTabSorc): Boolean;
    function  InsereItemAtual_K255(IDSeq1: Integer; Data, DtCorrApo:
              TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
              ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
              EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
              TEstqSPEDTabSorc): Boolean;
    function  InsereItemAtual_K265(IDSeq1: Integer; Data, DtCorrApo:
              TDateTime; GraGruX: Integer; QtdeCons, QtdeRet: Double;
              COD_INS_SUBST: String;
              ID_Item, MovimID, Codigo, MovimCod, Controle: Integer;
              OrigemOpeProc: TOrigemOpeProc; ESTSTabSorc: TEstqSPEDTabSorc):
              Boolean;
}
    function  InsereItemAtual_K23SubPrd(const MovimID, Codigo, MovimCod: Integer;
              const DataIni, DataFim: TDateTime; const GraGruX: Integer; const
              Qtde: Double; var IDSeq1: Integer): Boolean;
    function  InsereItemAtual_K25SubPrd(const MovimID, Codigo, MovimCod: Integer;
              const Data: TDateTime; const GraGruX: Integer; const Qtde: Double;
              var IDSeq1: Integer): Boolean;
{
    function  InsereItemAtual_K270(const RegOri: String; const Data, DtCorrApo:
              TDateTime; const MovimID, Codigo, MovimCod, Controle, GraGruX:
              Integer; const Qtde: Double; const SPED_EFD_KndRegOrigem:
              TSPED_EFD_KndRegOrigem; OrigemOpeProc: TOrigemOpeProc;
              OrigemIDKnd: TOrigemIDKnd; const OriSPEDEFDKnd: TOrigemSPEDEFDKnd;
              var IDSeq1: Integer): Boolean;
    function  InsereItemAtual_K275(RegOri: String;  MovimID, Codigo, MovimCod,
              Controle, GraGruX, IDSeq1: Integer; Qtde: Double;
              COD_INS_SUBST: String; SPED_EFD_KndRegOrigem:
              TSPED_EFD_KndRegOrigem; ESTSTabSorc: TEstqSPEDTabSorc;
              OrigemOpeProc: TOrigemOpeProc; OriIDKnd: TOrigemIDKnd;
              const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd): Boolean;

}
//

    procedure VerificaGGXDstZerados();
    procedure ReopenSecO_P(Qry: TmySQLQuery; SQL_Periodo, Filtro: String);
//  Copiados!!
{
    function  Insere_OrigeProcPQ(TipoRegSPEDProd: TTipoRegSPEDProd; MovimID:
              TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod: Integer;
              IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc:
              TOrigemOpeProc; ClientMO, FornecMO, EntiSitio: Integer): Boolean;
    procedure ReopenOriPQx(MovimCod: Integer; SQL_PeriodoPQ: String;
              ShowSQL: Boolean);
    function  Insere_OrigeProcVS_210(const TipoRegSPEDProd: TTipoRegSPEDProd;
              const MovimID: TEstqMovimID; const MovimNivInn, MovimNivBxa:
              TEstqMovimNiv; const QrCab: TmySQLQuery; var IDSeq1: Integer;
              const SQL_PeriodoVS: String; const OrigemOpeProc: TOrigemOpeProc):
              Boolean;
    function  Insere_EmOpeProc_215(const TipoRegSPEDProd: TTipoRegSPEDProd;
              const MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; const QrCab:
              TmySQLQuery; (*const Fator: Double;*) const SQL_Periodo: String;
              const OrigemOpeProc: TOrigemOpeProc; var IDSeq1: Integer):
              Boolean;
    procedure Insere_EmOpeProc_Prd(const TipoRegSPEDProd: TTipoRegSPEDProd;
              const MovimID: TEstqMovimID; const Qry: TmySQLQuery; const Fator:
              Double; const SQL_Periodo, SQL_PeriodoPQ: String; const
              OrigemOpeProc: TOrigemOpeProc; const MovimNiv: TEstqMovimNiv);
}
/////////////////////////// Produ��o Isolada //////////////////////////////////

(*
    function  Insere_OrigeRibPDA_210(const TipoRegSPEDProd:
              TTipoRegSPEDProd; const MovimID: TEstqMovimID; const MovimNiv:
              TEstqMovimNiv; const SQL_PeriodoVS: String; const OrigemOpeProc:
              TOrigemOpeProc): Boolean;
    function  ProducaoIsolada_ImportaOpeProcB(QrCab: TMySQLQuery; PsqMovimNiv: TEstqMovimNiv;
              SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ProducaoIsolada_Insere_OrigeRibDTA_210(const TipoRegSPEDProd: TTipoRegSPEDProd;
              const MovimID: TEstqMovimID; const MovimNiv: TEstqMovimNiv;
              const SQL_PeriodoVS: String; const OrigemOpeProc: TOrigemOpeProc):
              Boolean;
    function  ProducaoIsolada_ImportaOpeProcA(QrCab: TMySQLQuery; MovimID: TEstqMovimID;
              SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
    function  ProducaoIsolada_ImportaGerArt(SQL_PeriodoComplexo, SQL_Periodo, SQL_DtHrFimOpe:
              String; OrigemOpeProc: TOrigemOpeProc): Boolean;
*)

    function  SetaItensK220Corretos(): Integer;
  public
    { Public declarations }
    FImporExpor, FAnoMes, FEmpresa, FPeriApu: Integer;
    FDiaIni, FDiaFim, FDiaPos, FData: TDateTime;
  end;

  var
  FmSpedEfdIcmsIpiClaProd_v03_0_1: TFmSpedEfdIcmsIpiClaProd_v03_0_1;

implementation

uses UnMyObjects, Module, UnDmkProcFunc, DmkDAC_PF, UMySQLModule, UnFinanceiro,
  ModuleFin, ModuleGeral, UnVS_PF, ModProd, AppListas, ModAppGraG1,
  UnEfdIcmsIpi_PF, UnEfdIcmsIpi_PF_v03_0_1, UnEfdIcmsIpi_Jan,
  GraGruX, UnGrade_PF,
  SpedEfdIcmsIpiProducaoIsolada_v03_0_1, SpedEfdIcmsIpiProducaoConjunta_v03_0_1,
  ModVS;

{$R *.DFM}

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.BtNenhum220Click(Sender: TObject);
begin
  CGItensK220.Value := 0;
end;

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.BtNenhumClick(Sender: TObject);
begin
  CGImportar2xx.Value := 0;
end;

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.BtOKClick(Sender: TObject);
const
  sProcName = 'FmSPED_EFD_K2XX.BtOKClick()';
  procedure Informa2xx(var Itm: Integer);
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando "' +
    CGImportar2xx.items[Itm] + '"');
    Itm := Itm + 1;
  end;
  procedure AvisoOpcEspecif(Itm: Integer);
  begin
    Geral.MB_Aviso('Configura��o inv�lida em op��es espec�ficas para "' +
    CGImportar2xx.items[Itm-1] + '"');
  end;
  procedure InformaK220(var Itm: Integer);
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Importando "' +
    CGItensK220.items[Itm] + '"');
    Itm := Itm + 1;
  end;
var
  Erro: Boolean;
  Item: Integer;
  //
  UnidMedPc, UnidMedKg, UnidMedM2, Continua: Integer;
  SQL_PeriodoVS, SQL_PeriodoPQ, SQL_PeriodoComplexo, SQL_DtHrFimOpe: String;
  //
  procedure ProducaoIsolada();
  begin
    Application.CreateForm(TFmSpedEfdIcmsIpiProducaoIsolada_v03_0_1, FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1);
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.FImporExpor         := FImporExpor;
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.FEmpresa            := FEmpresa;
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.FAnoMes             := FAnoMes;
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.FPeriApu            := FPeriApu;
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.FDiaIni             := FDiaIni;
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.FDiaFim             := FDiaFim;
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.FDiaPos             := FDiaPos;
    (*FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.FData*)
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.FTipoPeriodoFiscal  := FTipoPeriodoFiscal;
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.PB2                 := PB2;
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.LaAviso1            := LaAviso1;
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.LaAviso2            := LaAviso2;
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.LaAviso3            := LaAviso3;
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.LaAviso4            := LaAviso4;
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.MeAviso             := MeAviso;

/////////////////////////////////////////////////////////////////////////////////
    //  Desmonte: Pre-descarne
    //           -> Sai couro verde e salgado
    //           -> Entra Subprodutos e couro pre-descarnado e aparado
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(1, CGImportar2xx.Value) then
      FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.Insere_OrigeRibPDA_210(
        TTipoRegSPEDProd.trsp21X, TEstqMovimID.emidEmRibPDA,
        TEstqMovimNiv.eminDestPDA, SQL_PeriodoVS, oopPredescarne);
/////////////////////////////////////////////////////////////////////////////////
    //Producao: Processo de caleiro
    //         -> Consome couro pre-descarnado e insumos
    //         -> Produz couro em caleiro
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(2, CGImportar2xx.Value) then
      FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.ImportaOpeProcB(QrVSCalCab, TEstqmovimNiv.eminSorcCal,//TEstqMovimID.emidEmProcCal,
        SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
        oopProcCaleiro);
/////////////////////////////////////////////////////////////////////////////////
    // Producao: Cria caleado a partir de couro em caleiro para poder a seguir desmontar e gerar Tripa e subprodutos
    //         -> Consome couro em caleiro
    //         -> Produz couro caleirado
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(4, CGImportar2xx.Value) then
      FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.ImportaOpeProcB(QrVSCalCab, TEstqmovimNiv.eminDestCal, //TEstqMovimID.emidEmProcCal,
        SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
        oopCouroCaleado);
/////////////////////////////////////////////////////////////////////////////////
    //  Desmonte: Redescarne
    //           -> Sai couro caleado
    //           -> Entra Subprodutos e couro tripa (redescarnado, dividido e aparado)
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(8, CGImportar2xx.Value) then
      FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.Insere_OrigeRibDTA_210((*TipoRegSPEDProd*)trsp21X,
        TEstqMovimID.emidEmRibDTA, TEstqMovimNiv.eminDestDTA, (*QrCab,*)
        (*LinArqPai,*) SQL_PeriodoVS, oopRedescarne);
/////////////////////////////////////////////////////////////////////////////////
    //Producao: Processo de curtimento:
    //         -> Consome couro tripa e insumos
    //         -> Produz couro em curtimento
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(16, CGImportar2xx.Value) then
      FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.ImportaOpeProcB(QrVSCalCab, TEstqmovimNiv.eminSorcCur, //TEstqMovimID.emidEmProcCal,
        SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
        oopProcCurtimento);
/////////////////////////////////////////////////////////////////////////////////
    // Curtido????
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(32, CGImportar2xx.Value) then
      FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.ImportaOpeProcA(QrVSCurCab, TEstqMovimID.emidEmProcCur,
        SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
        oopCouroCurtido);
/////////////////////////////////////////////////////////////////////////////////
    //  Gera artigo
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(64, CGImportar2xx.Value) then
      FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.ImportaGerArt(SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_DtHrFimOpe,
      oopGeracaoArtigo);
/////////////////////////////////////////////////////////////////////////////////
    // Operacao
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(128, CGImportar2xx.Value) then
    //  Parei aqui Registro 210 e 215 !
      FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.ImportaOpeProcA(QrVSOpeCab, TEstqMovimID.emidEmOperacao,
        SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
        oopOperacao);
/////////////////////////////////////////////////////////////////////////////////
    // Recurtimento
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(256, CGImportar2xx.Value) then
      FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.ImportaOpeProcA(QrVSPWECab, TEstqMovimID.emidEmProcWE,
        SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
        oopProcRecurtimento);
/////////////////////////////////////////////////////////////////////////////////
    // Processo de subproduto
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(512, CGImportar2xx.Value) then
      FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.ImportaOpeProcA(QrVSPSPCab, TEstqMovimID.emidEmProcSP,
        SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
        oopProcSubProduto);
/////////////////////////////////////////////////////////////////////////////////
    // Reprocessamento / reapro
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(1024, CGImportar2xx.Value) then
      FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.ImportaOpeProcA(QrVSRRMCab, TEstqMovimID.emidEmReprRM,
        SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
        oopReprocReparo);
/////////////////////////////////////////////////////////////////////////////////
    FmSpedEfdIcmsIpiProducaoIsolada_v03_0_1.Destroy;
  end;
  //
  procedure ProducaoConjunta();
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando dados para importar produ��o');
    Application.CreateForm(TFmSpedEfdIcmsIpiProducaoConjunta_v03_0_1, FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1);
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.FImporExpor         := FImporExpor;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.FEmpresa            := FEmpresa;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.FAnoMes             := FAnoMes;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.FPeriApu            := FPeriApu;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.FDiaIni             := FDiaIni;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.FDiaFim             := FDiaFim;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.FDiaPos             := FDiaPos;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.FSQL_PeriodoPQ      := SQL_PeriodoPQ;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.FSPED_EFD_Producao  :=
      EfdIcmsIpi_PF.ObtemTipoDeProducaoSPED(FEmpresa);
    (*FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.FData*)
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.FTipoPeriodoFiscal  := FTipoPeriodoFiscal;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.PB2                 := PB2;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.LaAviso1            := LaAviso1;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.LaAviso2            := LaAviso2;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.LaAviso3            := LaAviso3;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.LaAviso4            := LaAviso4;
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.MeAviso             := MeAviso;
    // prepara ...
    //FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.PesquisaIMECsRelacionados(SQL_PeriodoVS, SQL_PeriodoPQ);

/////////////////////////////////////////////////////////////////////////////////
    //  Desmonte: Pre-descarne
    //           -> Sai couro verde e salgado
    //           -> Entra Subprodutos e couro pre-descarnado e aparado
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(1, CGImportar2xx.Value) then
      if Dmod.QrControleSPED_II_PDA.Value = 0 then
        FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.Insere_OrigeRibPDA_210(
        TTipoRegSPEDProd.trsp21X, TEstqMovimID.emidEmRibPDA,
        TEstqMovimNiv.eminDestPDA, SQL_PeriodoVS, oopPredescarne)
      else
        AvisoOpcEspecif(Item);
/////////////////////////////////////////////////////////////////////////////////
{ }
    //Producao: Processo de caleiro
    //         -> Consome couro pre-descarnado e insumos
    //         -> Produz couro em caleiro
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(2, CGImportar2xx.Value) then
      if Dmod.QrControleSPED_II_EmCal.Value in ([2,3]) then
        FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaOpeProcB(QrVSCalCab,
        TEstqmovimID.emidEmProcCal, TEstqmovimID.emidEmProcCal, TEstqmovimID.emidEmProcCal,
        TEstqmovimNiv.eminSorcCal, TEstqmovimNiv.eminSorcCal,
        SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
        oopProcCaleiro)
      else
        AvisoOpcEspecif(Item);
/////////////////////////////////////////////////////////////////////////////////
    // Producao: Cria caleado a partir de couro em caleiro para poder a seguir desmontar e gerar Tripa e subprodutos
    //         -> Consome couro em caleiro
    //         -> Produz couro caleirado
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(4, CGImportar2xx.Value) then
      if Dmod.QrControleSPED_II_Caleado.Value in ([2,3]) then
        FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaOpeProcB(QrVSCalCab,
        //TEstqmovimID.emidEmProcCal, TEstqmovimNiv.eminDestCal, //TEstqMovimID.emidEmProcCal,
        TEstqmovimID.emidCaleado, TEstqmovimID.emidCaleado, TEstqmovimID.emidCaleado,
        //TEstqmovimID.emidCaleado, TEstqmovimID.emidCaleado, TEstqmovimID.emidEmProcCal,
        TEstqmovimNiv.eminDestCal, TEstqmovimNiv.eminDestCal,
        SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
        oopCouroCaleado)
      else
        AvisoOpcEspecif(Item);
/////////////////////////////////////////////////////////////////////////////////
    //  Desmonte: Redescarne
    //           -> Sai couro caleado
    //           -> Entra Subprodutos e couro tripa (redescarnado, dividido e aparado)
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(8, CGImportar2xx.Value) then
      if Dmod.QrControleSPED_II_DTA.Value = 0 then
        FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.Insere_OrigeRibDTA_210((*TipoRegSPEDProd*)trsp21X,
        TEstqMovimID.emidEmRibDTA, TEstqMovimNiv.eminDestDTA, (*QrCab,*)
        (*LinArqPai,*) SQL_PeriodoVS, oopRedescarne)
      else
        AvisoOpcEspecif(Item);
/////////////////////////////////////////////////////////////////////////////////
    //Producao: Processo de curtimento:
    //         -> Consome couro tripa e insumos
    //         -> Produz couro em curtimento
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(16, CGImportar2xx.Value) then
      if Dmod.QrControleSPED_II_EmCur.Value in ([2,3]) then
        FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaOpeProcB(QrVSCalCab,
        TEstqmovimID.emidEmProcCur, TEstqmovimID.emidEmProcCur, TEstqmovimID.emidEmProcCur,
        TEstqmovimNiv.eminSorcCur, TEstqmovimNiv.eminSorcCur,
        SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
        oopProcCurtimento)
      else
        AvisoOpcEspecif(Item);
/////////////////////////////////////////////////////////////////////////////////
    // Producao: Cria curtido a partir de couro em curtimento para poder a seguir gerar wet blue
    //         -> Consome couro em curtimento
    //         -> Produz couro curtido
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(32, CGImportar2xx.Value) then
      if Dmod.QrControleSPED_II_Curtido.Value in ([2,3]) then
        FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaOpeProcB(QrVSCalCab,
        TEstqmovimID.emidCurtido, //TEstqmovimNiv.eminDestCur,
        TEstqmovimID.emidIndsXX, TEstqmovimID.emidEmProcCur,
        TEstqmovimNiv.eminDestCur, TEstqmovimNiv.eminBaixCurtiXX,
        SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
        oopCouroCurtido)
      else
        AvisoOpcEspecif(Item);
/////////////////////////////////////////////////////////////////////////////////
    //  Gera artigo
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(64, CGImportar2xx.Value) then
      case Dmod.QrControleSPED_II_ArtCur.Value of
        2: FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaGerArtIsolado(
           SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_DtHrFimOpe, oopGeracaoArtigo);
        3: FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaGerArtConjunto(
           SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_DtHrFimOpe, oopGeracaoArtigo);
        else AvisoOpcEspecif(Item);
      end;
/////////////////////////////////////////////////////////////////////////////////
    // Operacao
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(128, CGImportar2xx.Value) then
      case Dmod.QrControleSPED_II_Operacao.Value of
    //  Registro 210 e 215 !
        0: FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaOpeProcA(QrVSOpeCab,
           TEstqMovimID.emidEmOperacao, SQL_PeriodoComplexo, SQL_PeriodoVS,
           SQL_PeriodoPQ, SQL_DtHrFimOpe, oopOperacao);
        3: FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaOpeProcB(QrVSOpeCab,
           TEstqMovimID.emidEmOperacao, //TEstqmovimNiv.eminDestCur,
           TEstqMovimID.emidEmOperacao, TEstqMovimID.emidEmOperacao,
           TEstqmovimNiv.eminDestOper, TEstqmovimNiv.eminDestOper,
           SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
           oopOperacao)
        else AvisoOpcEspecif(Item);
     end;

/////////////////////////////////////////////////////////////////////////////////
    // Recurtimento
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(256, CGImportar2xx.Value) then
      case Dmod.QrControleSPED_II_Recurt.Value of
        2: FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaOpeProcA(QrVSPWECab,
           TEstqMovimID.emidEmProcWE, SQL_PeriodoComplexo, SQL_PeriodoVS,
           SQL_PeriodoPQ, SQL_DtHrFimOpe, oopProcRecurtimento);
        3: FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaOpeProcB(QrVSPWECab,
           TEstqMovimID.emidEmProcWE, //TEstqmovimNiv.eminDestCur,
           TEstqMovimID.emidEmProcWE, TEstqMovimID.emidEmProcWE,
           TEstqmovimNiv.eminDestWEnd, TEstqmovimNiv.eminDestWEnd,
           SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
           oopProcRecurtimento);
         else AvisoOpcEspecif(Item);
       end;
/////////////////////////////////////////////////////////////////////////////////
    // Processo de subproduto
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(512, CGImportar2xx.Value) then
      case Dmod.QrControleSPED_II_SubProd.Value of
        2: FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaOpeProcA(QrVSPSPCab,
           TEstqMovimID.emidEmProcSP, SQL_PeriodoComplexo, SQL_PeriodoVS,
           SQL_PeriodoPQ, SQL_DtHrFimOpe, oopProcSubProduto);
        3: FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaOpeProcB(QrVSPSPCab,
           TEstqMovimID.emidEmProcSP, //TEstqmovimNiv.eminDestPSP,
           TEstqMovimID.emidEmProcSP, TEstqMovimID.emidEmProcSP,
           TEstqmovimNiv.eminDestPSP, TEstqmovimNiv.eminDestPSP,
           SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
           oopProcSubProduto);
         else AvisoOpcEspecif(Item);
      end;
/////////////////////////////////////////////////////////////////////////////////
    // Reprocessamento / reapro
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(1024, CGImportar2xx.Value) then
      case Dmod.QrControleSPED_II_Reparo.Value of
        4: FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaOpeProcA(QrVSRRMCab,
           TEstqMovimID.emidEmReprRM, SQL_PeriodoComplexo, SQL_PeriodoVS,
           SQL_PeriodoPQ, SQL_DtHrFimOpe, oopReprocReparo);
(*
        3: FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaOpeProcB(QrVSRRMCab,
           TEstqMovimID.emidEmProcSP, //TEstqmovimNiv.eminDestRRM,
           TEstqMovimID.emidEmProcSP, TEstqMovimID.emidEmProcSP,
           TEstqmovimNiv.eminDestRRM, TEstqmovimNiv.eminDestRRM,
           SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
           oopReprocReparo);
*)
         else AvisoOpcEspecif(Item);
      end;
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
    // Dilui��o / mistura de insumos qu�micos
    Informa2xx(Item);
    if DmkPF.IntInConjunto2(2048, CGImportar2xx.Value) then
      FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.ImportaDiluicaoMistura(
      SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ, SQL_DtHrFimOpe,
      oopDiluMistInsum);
         //else AvisoOpcEspecif(Item);
      //end;
/////////////////////////////////////////////////////////////////////////////////
    FmSpedEfdIcmsIpiProducaoConjunta_v03_0_1.Destroy;
{}   Geral.MB_Info('Produ��o conjunta implementada parcialmente!');
  end;
  //
begin
  if PCRegistro.ActivePageIndex = 2 then
  begin
    if not (RGProducao.ItemIndex in ([0,1])) then
    begin
       Geral.MB_Aviso('Selecione a forma de produ��o (isolada ou conjunta)!');
       Exit;
    end;
  end;
  //
  Geral.MB_Info('Se usar produ��o Conjunta, importar todos itens de classe tamb�m!');
  //
  FTipoPeriodoFiscal := TTipoPeriodoFiscal(
    EfdIcmsIpi_PF.ObtemTipoPeriodoRegistro(FEmpresa, 'K100', sProcName));
  if FTipoPeriodoFiscal = TTipoPeriodoFiscal.spedperIndef then
    Exit;
  //
  VAR_SHOW_ALL_SQL_TEXT := 0;
  VAR_MASK_ALL_SQL_TEXT := 'GROUP BY';
  Item := 0;
  if not VS_PF.VerificaCalCurDstGGX(LaAviso1, LaAviso2, PB1) then
    Exit;
  // deve ser antes do "VerificaCliForEmpty()"
  if not VS_PF.VerificaFornecMO(PB1, LaAviso1, LaAviso2) then
    Exit;
  if not VS_PF.VerificaCliForEmpty() then
    Exit;
  if not VS_PF.VerificaDatasDeEncerradasAposAbertura() then
    Exit;
  FDiaIni := EncodeDate(Geral.IMV(CBAno.Text), CBMes.ItemIndex + 1, 1);
  FDiaFim := IncMonth(FDiaIni, 1) -1;
  FDiaPos := FDiaFim + 1;
  //
  Erro := False;
  MeAviso.Lines.Clear;
  if MyObjects.FIC(CGImportar2xx.Value = 0, CGImportar2xx,
    'Selecione pelo menos um tipo de item a ser importado!') then Exit;
  if not DmProd.ObtemCodigoPecaPc(UnidMedPc) then Exit;
  if not DmProd.ObtemCodigoPesoKg(UnidMedKg) then Exit;
  if not DmProd.ObtemCodigoAreaM2(UnidMedM2) then Exit;
  if (UnidMedKg = 0) or (UnidMedM2 = 0) then Exit;
  PB1.Position := 0;
  PB1.Max := 12;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  if PCRegistro.ActivePageIndex > 0 then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando novos IDs implementados.');
    //
    SQL_PeriodoVS := dmkPF.SQL_Periodo('WHERE DataHora ', FDiaIni, FDiaFim, True, True);
    SQL_PeriodoPQ := dmkPF.SQL_Periodo('WHERE DataX ', FDiaIni, FDiaFim, True, True);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrIDNiv, Dmod.MyDB, [
    'SELECT MovimID, MovimNiv, COUNT(Controle) IMEIs  ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    SQL_PeriodoVS,
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'GROUP BY MovimID, MovimNiv ',
    'ORDER BY MovimID, MovimNiv ',
    '']);
    //
    QrIDNiv.First;
    while not QrIDNiv.Eof do
    begin
      case TEstqMovimID(QrIDNivMovimID.Value) of
        (*00*)emidAjuste,
        (*01*)emidCompra,
        (*02*)emidVenda,
        //(*03*)emidReclasWE,
        //(*04*)emidBaixa=4,
        //(*05*)emidIndsWE=5,
        (*09*)emidForcado,
        //(*10*)emidSemOrigem=10,
        (*12*)emidResiduoReclas,
        (*13*)emidInventario,
        (*16*)emidEntradaPlC,
        (*17*)emidExtraBxa,
        //(*18*)emidSaldoAnterior=18,
        (*21*)emidDevolucao,
        (*22*)emidRetrabalho,
        (*23*)emidGeraSubProd,
        (*25*)emidTransfLoc: Continua := QrIDNivMovimID.Value;
        //
        (*06*)emidIndsXX,
        (*07*)emidClassArtXXUni,
        (*08*)emidReclasXXUni,
        (*11*)emidEmOperacao,
        (*14*)emidClassArtXXMul,
        (*15*)emidPreReclasse,
        (*19*)emidEmProcWE,
        (*20*)emidFinished,
        (*24*)emidReclasXXMul,
        (*26*)emidEmProcCal,
        (*27*)emidEmProcCur,
        (*28*)emidDesclasse,
        (*29*)emidCaleado,
        (*30*)emidEmRibPDA,
        (*31*)emidEmRibDTA,
        (*32*)emidEmProcSP,
        (*33*)emidEmReprRM: Continua := QrIDNivMovimID.Value;
        else Continua := -1;
      end;
      if Continua < 0 then
      begin
        Geral.MB_Erro('ID de movimento n�o implementado: ' + slineBreak +
          Geral.FF0(QrIDNivMovimID.Value) + ' - ' +
          sEstqMovimID[QrIDNivMovimID.Value]);
        // Liberar quando implementar 30 e 31
        //Exit;
      end;
      //
      QrIDNiv.Next;
    end;
    //QrIDNiv.First;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Importando.');
////////  ITENS EXCLUSOS   /////////////////////////////////////////////////////
    //00 - emidAjuste,
    //01 - emidCompra,
    //02 - emidVenda,
    ////03 - emidReclasWE,
    ////04 - emidBaixa=4,
    ////05 - emidIndsWE=5,
    //09 - emidForcado,
    ////10 - emidSemOrigem=10,
    //11 - emidEmOperacao,
    ////12 - emidResiduoReclas=12,
    //13 - emidInventario,
    //15 - emidPreReclasse,
    //16 - emidEntradaPlC,
    //17 - emidExtraBxa,
    ////18 - emidSaldoAnterior=18,
    //19 - emidEmProcWE,
    //20 - emidFinished,
    //21 - emidDevolucao,
    //22 - emidRetrabalho,
    //23 - emidGeraSubProd,
    //25 - emidTransfLoc: ; // Nada! N�o v�o neste registro!
    //26 - emidEmProcCal,
    //27 - emidEmProcCur: Continua := QrIDNivMovimID.Value;
    //
    //06 - emidIndsXX: ; // Registros 230 a 255!
////////////////////////////////////////////////////////////////////////////////
/// ////  CLASSE E RECLASSE  ///////////////////////////////////////////////////
    //07*)emidClassArtVSUni,
    //08*)emidReclasVSUni
    case PCRegistro.ActivePageIndex of
      1:
      begin
        Item := 0;
        if MyObjects.FIC(CGItensK220.Value = 0, CGItensK220,
        'Selecione pelo menos um tipo de item K220 a ser importado!') then Exit;
        //ImportaClasseEReclasseCaC(SQL_Periodo);
        //
        // Classe e reclasse
        Informa2xx(Item);
        if DmkPF.IntInConjunto2(1, CGItensK220.Value) then
          ImportaClasseEReclasseVMI(SQL_PeriodoVS);
        //
        // Couro posto para ope��o/processo
        Informa2xx(Item);
        if DmkPF.IntInConjunto2(2, CGItensK220.Value) then
          ImportaClasseOpeProcA(SQL_PeriodoVS);
        //
        // Coprodu��o de produtos
        Informa2xx(Item);
        if DmkPF.IntInConjunto2(4, CGItensK220.Value) then
          ImportaClasseOpeProcB(SQL_PeriodoVS);
        // Coprodu��o de subprodutos
        Informa2xx(Item);
        if DmkPF.IntInConjunto2(8, CGItensK220.Value) then
          ImportaClasseOpeProcC(SQL_PeriodoVS);
        // Redirecionamento
        Informa2xx(Item);
        if DmkPF.IntInConjunto2(16, CGItensK220.Value) then
          ImportaClasseGGXRcl(SQL_PeriodoVS);
        //
        // Desclassifica��o em opera��o/processo
        Informa2xx(Item);
        if DmkPF.IntInConjunto2(32, CGItensK220.Value) then
          ImportaDesclasseOpeProc(SQL_PeriodoVS);
      end;
      2:
      begin
        Item := 0;
        Geral.MB_Erro('Problemas conhecidos:' + sLineBreak +
        '1. PQx.DtCorrApo > 0 n�o busca para K270 pois este n�o existe quando n�o movimentou VSMovIts!'
        + sLineBreak +
        '');
        PB1.Max := CGImportar2xx.Items.Count;
        //
        SQL_PeriodoComplexo := DmkPF.SQL_PeriodoComplexo('WHERE (',
          'DtHrAberto', 'DtHrFimOpe', FDiaIni, FDiaFim, True, true, True, True);
        SQL_PeriodoVS := dmkPF.SQL_Periodo('AND DataHora ', FDiaIni, FDiaFim, True, True);
        SQL_PeriodoPQ := dmkPF.SQL_Periodo('AND DataX ', FDiaIni, FDiaFim, True, True);
        SQL_DtHrFimOpe := dmkPF.SQL_Periodo('WHERE DtHrFimOpe ', FDiaIni, FDiaFim, True, True);
        //
        case RGProducao.ItemIndex of
          0: ProducaoIsolada();
          1: ProducaoConjunta();
          else Geral.MB_Erro(
          'Tipo de produ��o (RGProducao.ItemIndex) n�o implementado!');
        end;
      end;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end else
    Geral.MB_Erro('Guia n�o implementada para VS Couro!');
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Importa��o finalizada!!!');
  if Trim(MeAviso.Text) <> '' then
    Geral.MB_Aviso(MeAviso.Text);
  Close;
end;

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.BtTodos220Click(Sender: TObject);
begin
  CGItensK220.Value := SetaItensK220Corretos();
end;

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.BtTudoClick(Sender: TObject);
begin
  CGImportar2xx.Value := CGImportar2xx.MaxValue;
end;

function TFmSpedEfdIcmsIpiClaProd_v03_0_1.DataFimObrigatoria(DtHrFimOpe: TDateTime): TDateTime;
begin
  if DtHrFimOpe < 2 then
    Result :=  FDiaFim
  else
  if DtHrFimOpe > FDiaFim then
    Result :=  FDiaFim
  else
    Result := DtHrFimOpe;
end;

function TFmSpedEfdIcmsIpiClaProd_v03_0_1.ErrGrandeza(TabSrc, GGxSrc, GGXDst: String): Boolean;
  function ParteSQL(GGXStr: String): String;
  begin
    Result := Geral.ATS([
    'SELECT ggx.GraGru1 Nivel1, ggx.Controle Reduzido, ',
    'CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_GG1, med.Sigla, ',
    'CAST(med.Grandeza AS DECIMAL(11,0)) Grandeza, ',
    'CAST(xco.Grandeza AS DECIMAL(11,0)) xco_Grandeza,  ',
    'CAST(IF(xco.Grandeza > 0, xco.Grandeza,  ',
    '  IF((ggx.GraGruY<2048 OR  ',
    '  xco.CouNiv2<>1), 2,  ',
    '  IF(xco.CouNiv2=1, 1, -1))) AS DECIMAL(11,0))  dif_Grandeza ',
    'FROM ' + TabSrc + ' ses ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=ses.' + GGXStr,
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    nv1 ON nv1.Codigo=xco.CouNiv1  ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    nv2 ON nv2.Codigo=xco.CouNiv2  ',
    'WHERE (IF(xco.Grandeza>0, xco.Grandeza, ',
    '  IF((ggx.GraGruY<2048 OR xco.CouNiv2<>1), 2, ',
    '    IF(xco.CouNiv2=1, 1, -1))) <> med.Grandeza) ',
    '']);
  end;
var
  SQLSrc, SQLDst, SQLUni: String;
begin
  SQLSrc := '';
  SQLDst := '';
  SQLUni := '';
  if GGxSrc <> '' then
    SQLSrc := ParteSQL(GGXSrc);
  if (GGxSrc <> '') and (GGXDst <> '') then
    SQLUni := 'UNION ';
  if GGxDst <> '' then
    SQLDst := ParteSQL(GGXDst);
  Result := DfModAppGraG1.GrandesasInconsistentes([
  ' DROP TABLE IF EXISTS _couros_err_grandeza_; ',
  'CREATE TABLE _couros_err_grandeza_ ',
  ' ',
  SQLSrc,
  SQLUni,
  SQLDst,
  '; ',
  'SELECT * FROM _couros_err_grandeza_ ',
  ''], DModG.MyPID_DB);
end;

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  VerificaGGXDstZerados();
end;

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PCRegistro.ActivePageIndex := 0;
  MyObjects.ConfiguraCheckGroup(CGImportar2xx, sSPED_KPROD_SEGM, 3, High(Integer), False);
  CGItensK220.Value := SetaItensK220Corretos(); //CGItensK220.MaxValue;
  CGImportar2xx.Value := CGImportar2xx.MaxValue;
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  MeAviso.Lines.Clear;
end;

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.FormDestroy(Sender: TObject);
begin
  VAR_SHOW_ALL_SQL_TEXT := 0;
  VAR_MASK_ALL_SQL_TEXT := '';
end;

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmSpedEfdIcmsIpiClaProd_v03_0_1.GrandezasInconsistentes(): Boolean;
var
  SQL_PeriodoVMI: String;
begin
  Result := False;
  SQL_PeriodoVMI := dmkPF.SQL_Periodo('WHERE vmi.DataHora ', FDiaIni, FDiaFim, True, True);
  //
  if DfModAppGraG1.GrandesasInconsistentes([
  'SELECT DISTINCT ggx.GraGru1 Nivel1, ggx.Controle Reduzido,  ',
  'gg1.Nome NO_GG1, med.Sigla, med.Grandeza, ',
  'xco.Grandeza xco_Grandeza,  ',
  'CAST(IF(xco.Grandeza > 0, xco.Grandeza,  ',
  '  IF((ggx.GraGruY<2048 OR  ',
  '  xco.CouNiv2<>1), 2,  ',
  '  IF(xco.CouNiv2=1, 1, -1))) AS DECIMAL(11,0)) dif_Grandeza ',
  ' ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1  ',
  'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2  ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  SQL_PeriodoVMI,
  'AND vmi.GraGruX <> 0 ',
  'AND ((IF(xco.Grandeza > 0, xco.Grandeza,  ',
  '  IF((ggx.GraGruY<2048 OR  ',
  '  xco.CouNiv2<>1), 2,  ',
  '  IF(xco.CouNiv2=1, 1, -1))) <> med.Grandeza)) ',
  ''], Dmod.MyDB) then
  begin
    Result := True;
    Close;
  end;
end;

function TFmSpedEfdIcmsIpiClaProd_v03_0_1.ImportaClasseEReclasseVMI(
  SQL_Periodo: String): Boolean;
var
  Codigo, MovimCod, CtrlDst, CtrlOri, GGXDst, GGXOri, MovimID, MovimNiv,
  ClientMO, FornecMO, EntiSitio: Integer;
  Data, DtCorrApo: TDateTime;
  AreaM2: Double;
begin
  PB1.Position := PB1.Position + 1;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando Tabela tempor�ria de classe e reclasse VMI');
  UnDmkDAC_PF.AbreMySQLQuery0(QrCeR2, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_K2XX_CLA_RCL_VMI_;  ',
  'CREATE TABLE _SPED_EFD_K2XX_CLA_RCL_VMI_  ',
  'SELECT vmi.Codigo, vmi.MovimCod, vmi.MovimID,  vmi.MovimNiv,  ',
  'DATE(vmi.DataHora) DATA, vmi.Pecas, vmi.AreaM2, vmi.PesoKg,  ',
  'vmi.GraGruX GGXOri, vmi.Controle CtrlOri,  ',
  'vmi.DstGGX GGXDst, vmi.DstNivel2 CtrlDst,  ',
  'vmi.DtCorrApo, vmi.ClientMO, vmi.FornecMO,  ',
  'scc.EntiSitio ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
  SQL_Periodo,
  'AND MovimID IN (7,8,14,24)  ',
  'AND MovimNiv=1  ',
  ';  ',
  'SELECT * FROM _SPED_EFD_K2XX_CLA_RCL_VMI_  ',
  '']);
  //Geral.MB_SQL(Self, QrCeR2);
  if ErrGrandeza('_SPED_EFD_K2XX_CLA_RCL_VMI_', 'GGxOri', 'GGXDst') then
    Exit;
  PB2.Max := QrCeR2.RecordCount;
  PB2.Position := 0;
  QrCeR2.First;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Importando registros de classe e reclase de VMI');
  while not QrCeR2.Eof do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    GGXDst   := QrCeR2.FieldByName('GGXDst').AsInteger;
    GGXOri   := QrCeR2.FieldByName('GGXOri').AsInteger;
    if GGXOri <> GGXDst then
    begin
      Codigo   := QrCeR2.FieldByName('Codigo').AsInteger;
      MovimID  := QrCeR2.FieldByName('MovimID').AsInteger;
      MovimNiv  := QrCeR2.FieldByName('MovimNiv').AsInteger;
      MovimCod := VS_PF.ObtemMovimCodDeMovimIDECodigo(TEstqMovimID(MovimID),//TEstqMovimID.emidClassArtVSUni,
                  Codigo);
      CtrlDst  := QrCeR2.FieldByName('CtrlDst').AsInteger;
      Data     := QrCeR2.FieldByName('DATA').AsDateTime;
      AreaM2   := - QrCeR2.FieldByName('AreaM2').AsFloat;
      CtrlOri  := QrCeR2.FieldByName('CtrlOri').AsInteger;
      DtCorrApo := QrCeR2.FieldByName('DtCorrApo').AsDateTime;
      ClientMO  := QrCeR2.FieldByName('ClientMO').AsInteger;
      FornecMO  := QrCeR2.FieldByName('FornecMO').AsInteger;
      EntiSitio  := QrCeR2.FieldByName('EntiSitio').AsInteger;
      //
      InsereItemAtual_K220(MovimID, //Integer(TEstqMovimID.emidClassArtVSUni),
      Data, GGXOri, GGXDst, AreaM2, Codigo, MovimCod, CtrlDst, CtrlOri, ClientMO,
      FornecMO, EntiSitio, esomiemCRUni, MovimNiv, oidk220CR_VMI, DtCorrApo);
    end;
    //
    QrCeR2.Next;
  end;
  Result := True;
end;

function TFmSpedEfdIcmsIpiClaProd_v03_0_1.ImportaClasseGGXRcl(SQL_Periodo: String): Boolean;
const
  Fator = -1;
var
  Qtde: Double;
begin
  PB1.Position := PB1.Position + 1;
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Importando registros com troca de reduzido em NFe (Reclasse direta)');

  UnDmkDAC_PF.AbreMySQLQuery0(QrGGXRcl, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_K2XX_GGX_RCL;',
  'CREATE TABLE _SPED_EFD_K2XX_GGX_RCL',
  '',
  'SELECT vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
  'DATE(vmi.DataHora) Data,  vmi.GraGruX GGX_Src, vmi.MovimNiv, ',
  'vmi.GGXRcl GGX_Dst, Pecas, AreaM2, PesoKg, vmi.DtCorrApo, ',
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN ' + TMeuDB + '.unidmed med ON med.Codigo=gg1.UnidMed',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
  SQL_Periodo,
  'AND vmi.GGXRcl <> 0 ',
  'AND vmi.MovimID <> 11 ', // Desmonte  210_215
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
  ';',
  '',
  'SELECT * FROM _SPED_EFD_K2XX_GGX_RCL',
  '']);
  //
  //Geral.MB_SQL(Self, QrGGXRcl);
  //
  if ErrGrandeza('_SPED_EFD_K2XX_GGX_RCL', 'GGx_Src', 'GGX_Dst') then
    Exit;
  //
  PB2.Max := QrGGXRcl.RecordCount;
  PB2.Position := 0;
  QrGGXRcl.First;
  while not QrGGXRcl.Eof do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    if QrGGXRclGGX_Src.Value <> QrGGXRclGGX_Dst.Value then
    begin
      case  QrGGXRclGrandeza.Value of
        0: Qtde := QrGGXRclPecas.Value * Fator;
        1: Qtde := QrGGXRclAreaM2.Value * Fator;
        2: Qtde := QrGGXRclPesoKg.Value * Fator;
      end;
      begin
        InsereItemAtual_K220(QrGGXRclMovimID.Value,
        QrGGXRclData.Value, QrGGXRclGGX_Src.Value, QrGGXRclGGX_Dst.Value, Qtde,
        QrGGXRclCodigo.Value, QrGGXRclMovimCod.Value, QrGGXRclControle.Value,
        QrGGXRclControle.Value, // mesmo controle!!!!?????
        QrGGXRclClientMO.Value, QrGGXRclFornecMO.Value, QrGGXRclEntiSitio.Value,
        esomiemGGXRcl, QrGGXRclMovimNiv.Value, oidk220RCL_A,
        QrGGXRclDtCorrApo.Value);
      end;
      // Inverte novamente em caso de Operacao e processo
      if (QrGGXRclMovimID.Value = 11)
      or (QrGGXRclMovimID.Value = 19)
      then
      begin
        InsereItemAtual_K220(QrGGXRclMovimID.Value,
        QrGGXRclData.Value, QrGGXRclGGX_Dst.Value, QrGGXRclGGX_Src.Value, Qtde,
        QrGGXRclCodigo.Value, QrGGXRclMovimCod.Value, QrGGXRclControle.Value,
        QrGGXRclControle.Value, // mesmo controle!!!!?????
        QrGGXRclClientMO.Value, QrGGXRclFornecMO.Value, QrGGXRclEntiSitio.Value,
        esomiemGGXRcl, QrGGXRclMovimNiv.Value, oidk220RCL_B,
        QrGGXRclDtCorrApo.Value);
      end;
    end;
    //
    QrGGXRcl.Next;
  end;
  Result := True;
end;

function TFmSpedEfdIcmsIpiClaProd_v03_0_1.ImportaClasseOpeProcA(SQL_Periodo: String): Boolean;
const
  Fator = -1;
var
  Qtde: Double;
  CtrlOri: Integer;
begin
  PB1.Position := PB1.Position + 1;
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Importando registros de artigos em opera��es e processos');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSecO_P, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_K2XX_O_P; ',
  'CREATE TABLE _SPED_EFD_K2XX_O_P ',
  //
  'SELECT vmi.Empresa, vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle,  ',
  'DATE(vmi.DataHora) Data,  vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
  'cab.GraGruX GGX_Dst, vmi.MovimNiv, ',
  'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, vmi.DtCorrApo, med.Grandeza ',
  'FROM ' + TMeuDB + '.vsopecab cab',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON vmi.MovimCod=cab.MovimCod',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN ' + TMeuDB + '.unidmed med ON med.Codigo=gg1.UnidMed',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
  SQL_Periodo,
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
  'AND vmi.MovimNiv=7 ',
  //
  'UNION ',
  //
  'SELECT vmi.Empresa, vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle,  ',
  'DATE(vmi.DataHora) Data,  vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
  'cab.GraGruX GGX_Dst, vmi.MovimNiv, ',
  'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, vmi.DtCorrApo, med.Grandeza ',
  'FROM ' + TMeuDB + '.vspwecab cab',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON vmi.MovimCod=cab.MovimCod',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN ' + TMeuDB + '.unidmed med ON med.Codigo=gg1.UnidMed',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
  SQL_Periodo,
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
  'AND vmi.MovimNiv=20 ',
  //
  'UNION ',
  //
  'SELECT vmi.Empresa, vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle,  ',
  'DATE(vmi.DataHora) Data,  vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
  'cab.GraGruX GGX_Dst, vmi.MovimNiv, ',
  'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg,  vmi.DtCorrApo, med.Grandeza ',
  'FROM ' + TMeuDB + '.vspspcab cab',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON vmi.MovimCod=cab.MovimCod',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN ' + TMeuDB + '.unidmed med ON med.Codigo=gg1.UnidMed',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
  SQL_Periodo,
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
  'AND vmi.MovimNiv=49 ',
  //
  'UNION ',
  //
  'SELECT vmi.Empresa, vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle,  ',
  'DATE(vmi.DataHora) Data,  vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
  'cab.GraGruX GGX_Dst, vmi.MovimNiv, ',
  'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg,  vmi.DtCorrApo, med.Grandeza ',
  'FROM ' + TMeuDB + '.vsrrmcab cab',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON vmi.MovimCod=cab.MovimCod',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN ' + TMeuDB + '.unidmed med ON med.Codigo=gg1.UnidMed',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
  SQL_Periodo,
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
  'AND vmi.MovimNiv=54 ',
  '; ',
  'SELECT * FROM _SPED_EFD_K2XX_O_P ',
  '']);
  //
  //Geral.MB_SQL(Self, QrSecO_P);
  //
  if ErrGrandeza('_SPED_EFD_K2XX_O_P', 'GGx_Src', 'GGX_Dst') then
    Exit;
  //
  PB2.Max := QrSecO_P.RecordCount;
  PB2.Position := 0;
  QrSecO_P.First;
  while not QrSecO_P.Eof do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    if (QrSecO_PGGX_Src.Value <> QrSecO_PGGX_Dst.Value) or
    // 2017-10-14
    (QrSecO_PMovimNiv.Value = 54) then
    // 2017-10-14
    begin
      case  QrSecO_PGrandeza.Value of
        0: Qtde := Fator * QrSecO_PPecas.Value;
        1: Qtde := Fator * QrSecO_PAreaM2.Value;
        2: Qtde := Fator * QrSecO_PPesoKg.Value;
      end;
      CtrlOri := QrSecO_PControle.Value;
      InsereItemAtual_K220(QrSecO_PMovimID.Value,
      QrSecO_PData.Value, QrSecO_PGGX_Src.Value, QrSecO_PGGX_Dst.Value, Qtde,
      QrSecO_PCodigo.Value, QrSecO_PMovimCod.Value, QrSecO_PControle.Value,
      CtrlOri,
      QrSecO_PClientMO.Value, QrSecO_PFornecMO.Value, QrSecO_PEntiSitio.Value,
      esomiemOpeProcInn, QrSecO_PMovimNiv.Value, oidk220COP_A,
      QrSecO_PDtCorrApo.Value);
    end;
    //
    QrSecO_P.Next;
  end;
  Result := True;
end;

function TFmSpedEfdIcmsIpiClaProd_v03_0_1.ImportaClasseOpeProcB(SQL_Periodo: String): Boolean;
var
  Qtde: Double;
  CtrlOri: Integer;
begin
  PB1.Position := PB1.Position + 1;
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Importando registros de classes secund�rias de opera��es e processos');
  // Nao buscar subproduto!
  ReopenSecO_P(QrSecO_P, SQL_Periodo, '<>');
  if ErrGrandeza('_SPED_EFD_K2XX_O_P', 'GGx_Src', 'GGX_Dst') then
    Exit;
  //
  PB2.Max := QrSecO_P.RecordCount;
  PB2.Position := 0;
  QrSecO_P.First;
  while not QrSecO_P.Eof do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    if QrSecO_PGGX_Src.Value <> QrSecO_PGGX_Dst.Value then
    begin
      case  QrSecO_PGrandeza.Value of
        0: Qtde := QrSecO_PPecas.Value;
        1: Qtde := QrSecO_PAreaM2.Value;
        2: Qtde := QrSecO_PPesoKg.Value;
      end;
      CtrlOri := QrSecO_PControle.Value;
      InsereItemAtual_K220(QrSecO_PMovimID.Value,
      QrSecO_PData.Value, QrSecO_PGGX_Src.Value, QrSecO_PGGX_Dst.Value, Qtde,
      QrSecO_PCodigo.Value, QrSecO_PMovimCod.Value, QrSecO_PControle.Value,
      CtrlOri,
      QrSecO_PClientMO.Value, QrSecO_PFornecMO.Value, QrSecO_PEntiSitio.Value,
      esomiemOpeProcDst, QrSecO_PMovimNiv.Value, oidk220COP_B,
      QrSecO_PDtCorrApo.Value);
    end;
    //
    QrSecO_P.Next;
  end;
  Result := True;
end;

function TFmSpedEfdIcmsIpiClaProd_v03_0_1.ImportaClasseOpeProcC(SQL_Periodo: String): Boolean;
const
  sProcName = 'ImportaClasseOpeProcC()';
  MyFator = 1;
var
  Qtde, Pecas, AreaM2, PesoKg: Double;
  DtHrAberto, DtHrFimOpe: TDateTime;
  MovimID, Codigo, MovimCod, TipoEstq, GGXDst, LinArqPai: Integer;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  //
begin
  PB1.Position := PB1.Position + 1;
  LinArqPai := 0;
  //  SubProdutos para uso em conferencia estoque x movimento: frxEFD_SPEDE_K_VERIF_001
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Importando registros de classes secund�rias de opera��es e processos');
  // Buscar apenas subprodutos!
  ReopenSecO_P(QrSubPrdOP, SQL_Periodo, '=');
  if ErrGrandeza('_SPED_EFD_K2XX_O_P', 'GGx_Src', 'GGX_Dst') then
    Exit;
  //
  //Geral.MB_Info(Geral.FF0(QrSubPrdOP.RecordCount));
  //Geral.MB_SQL(Self, QrSubPrdOP);
  QrSubPrdOP.First;
  while not QrSubPrdOP.Eof do
  begin
    MovimID    := QrSubPrdOPMovimID.Value;
    Codigo     := QrSubPrdOPCodigo.Value;
    MovimCod   := QrSubPrdOPMovimCod.Value;
    DtHrAberto := QrSubPrdOPData.Value;
    DtHrFimOpe := QrSubPrdOPData.Value;
    AreaM2     := QrSubPrdOPAreaM2.Value;
    PesoKg     := QrSubPrdOPPesoKg.Value;
    Pecas      := QrSubPrdOPPecas.Value;
    TipoEstq   := QrSubPrdOPGrandeza.Value;
    GGXDst     := QrSubPrdOPGGX_Dst.Value;
    //
    if ((Pecas <> 0) or (MovimID = Integer(emidEmProcSP))) and (DtHrFimOpe > 1) then
    begin
      case TipoEstq of
        1: Qtde := AreaM2;
        2: Qtde := PesoKg;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrSubPrdOP.SQL.Text);
          //
          Qtde := Pecas;
        end;
      end;
      Qtde := Qtde * MyFator;
    end else
      Qtde := 0;
    //
    if Qtde < 0 then
      Mensagem(sProcName, MovimCod, MovimID,
      'Quantidade n�o pode ser negativa!!! (1)' +
      sLineBreak + 'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrSubPrdOP)
    else if (Qtde = 0) and (DtHrFimOpe > 1) then
    begin
      case TEstqMovimID(MovimID) of
        emidEmProcCal: DtHrFimOpe := 0;
        emidEmProcCur: DtHrFimOpe := 0;
        else Mensagem(sprocName, MovimCod, MovimID,
        'Quantidade zerada para OP encerrada!!! (1)', QrSubPrdOP);
      end;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrFMO, Dmod.MyDB, [
    'SELECT FornecMO  ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv IN (8, 21) ',
    '']);
    if QrSubPrdOPEmpresa.Value = QrFMOFornecMO.Value then
      TipoRegSPEDProd := trsp23X
    else
      TipoRegSPEDProd := trsp25X;
     //
     case TipoRegSPEDProd of
      trsp23X:
      begin
        InsereItemAtual_K23SubPrd(MovimID, Codigo, MovimCod, DtHrAberto,
        DtHrFimOpe, GGXDst, Qtde, LinArqPai);
      end;
      trsp25X:
      begin
        InsereItemAtual_K25SubPrd(Integer(MovimID), Codigo, MovimCod, DtHrFimOpe,
        GGXDst, Qtde, LinArqPai);
      end;
      else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(1)');
    end;
    //
    QrSubPrdOP.Next;
  end;
  Result := True;
end;

function TFmSpedEfdIcmsIpiClaProd_v03_0_1.ImportaDesclasseOpeProc(SQL_Periodo: String): Boolean;
var
  Qtde: Double;
  Qry: TmySQLQuery;
begin
  PB1.Position := PB1.Position + 1;
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Importando registros de desclassificados em opera��es e processos');
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrDesclasse, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _SPED_EFD_K2XX_DESCL1;  ',
    'CREATE TABLE _SPED_EFD_K2XX_DESCL1 ',
    'SELECT vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
    'DATE(vmi.DataHora) Data, vmi.MovimNiv, ',
    'vmi.GraGruX GGX_Dst, Pecas, AreaM2, PesoKg, ',
    'vmi.DtCorrApo, vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza Gdz_Dst, ',
    'vmi.DstMovID, vmi.DstNivel1, vmi.DstNivel2 ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN ' + TMeuDB + '.gragrux   ggx ON ggx.Controle=vmi.GraGruX',
    'LEFT JOIN ' + TMeuDB + '.gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN ' + TMeuDB + '.unidmed   med ON med.Codigo=gg1.UnidMed',
    'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
    SQL_Periodo,
    'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
    'AND vmi.MovimId=28  ',
    'AND vmi.MovimNiv=2  ',
    ';  ',
    'DROP TABLE IF EXISTS _SPED_EFD_K2XX_DESCL2;  ',
    'CREATE TABLE _SPED_EFD_K2XX_DESCL2 ',
    'SELECT dsc.*, vmi.GraGruX GGX_Src, med.Grandeza Gdz_Src ',
    'FROM _SPED_EFD_K2XX_DESCL1 dsc',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=dsc.DstNivel2',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN ' + TMeuDB + '.unidmed med ON med.Codigo=gg1.UnidMed',
    ';  ',
    'SELECT * FROM _SPED_EFD_K2XX_DESCL2 dsc;',
    '']);
    //
    //Geral.MB_SQL(Self, QrDesclasse);
    //
    if ErrGrandeza('_SPED_EFD_K2XX_DESCL2', 'GGx_Src', 'GGX_Dst') then
      Exit;
    //
    PB2.Max := QrDesclasse.RecordCount;
    PB2.Position := 0;
    QrDesclasse.First;
    while not QrDesclasse.Eof do
    begin
      MyObjects.UpdPB(PB2, nil, nil);
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT med.Grandeza  ',
      'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
      'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN ' + TMeuDB + '.unidmed med ON med.Codigo=gg1.UnidMed ',
      'WHERE vmi.Controle=' + Geral.FF0(QrDesclasseControle.Value),
      '']);
      if QrDesclasseGGX_Src.Value <> QrDesclasseGGX_Dst.Value then
      begin
        case  QrDesclasseGdz_Src.Value of
          0: Qtde := QrDesclassePecas.Value;
          1: Qtde := QrDesclasseAreaM2.Value;
          2: Qtde := QrDesclassePesoKg.Value;
        end;
        InsereItemAtual_K220(QrDesclasseMovimID.Value,
        QrDesclasseData.Value, QrDesclasseGGX_Src.Value, QrDesclasseGGX_Dst.Value, Qtde,
        QrDesclasseCodigo.Value, QrDesclasseMovimCod.Value, QrDesclasseControle.Value,
        QrDesclasseDstNivel2.Value,
        QrDesclasseClientMO.Value, QrDesclasseFornecMO.Value, QrDesclasseEntiSitio.Value,
        esomiemDesclasse, QrDesclasseMovimNiv.Value, oidk220IDO_A,
        QrDesclasseDtCorrApo.Value);
      end;
      //
      QrDesclasse.Next;
    end;
    Result := True;
  finally
    Qry.Free;
  end;
end;

{
function TFmSpedEfdIcmsIpiClaProd.ProducaoIsolada_ImportaGerArt(SQL_PeriodoComplexo,
  SQL_Periodo, SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  MovimID = TEstqMovimID.emidIndsXX;
  ESTSTabSorc = estsVMI;
  sProcName = 'FmSPED_EFD_K2XX.ProducaoIsolada_ImportaGerArt()';
  //
  function FiltroReg(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := '=';
      trsp25X: Result := '<>';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (1)');
        Result := '#ERR';
      end;
    end;
  end;
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := SQL_Periodo;
      //trsp25X: Result := SQL_DtHrFimOpe;
      //trsp25X: Result := SQL_Periodo;
      trsp25X: Result := '';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido!');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
  //
  function InsereRegistros(TipoRegSPEDProd: TTipoRegSPEDProd): Boolean;
  var
    MovimCod, MovimTwn, Codigo, GraGruX, TipoEstq, MyFatorCab, MyFAtorIts,
    IDSeq1, ID_Item, Controle, ClientMO, FornecMO, EntiSitio: Integer;
    DtHrAberto, DtHrFimOpe: TDateTime;
    AreaM2, PesoKg, Pecas, Qtde: Double;
    COD_INS_SUBST: String;
    //
    DtMovim, DtCorrApo: TDateTime;
  begin
    COD_INS_SUBST := '';
    MyFatorCab := 1;
    MyFatorIts := -1;
    //
////////////////////////////////////////////////////////////////////////////////
///  Cabecalho > Registro K230 ou K250
////////////////////////////////////////////////////////////////////////////////
    UnDmkDAC_PF.AbreMySQLQuery0(QrVmiGArCab, Dmod.MyDB, [
    'SELECT gg1.UnidMed, med.Grandeza med_grandeza, ',
    'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'TipoEstq', True,
    'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'DATE(vmi.DataHora) Data, vmi.Codigo, vmi.MovimCod, ',
    'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg, ',
    'vmi.MovimTwn, vmi.Controle, DtCorrApo, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio ',
    'FROM ' + CO_SEL_TAB_VMI + '        vmi ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCurtiVS)), // 14
    SQL_Periodo,
    'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
    'AND vmi.MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimNiv=13 ',
    '  AND FornecMO' + FiltroReg(TipoRegSPEDProd) + Geral.FF0(FEmpresa),
    ') ',
    'ORDER BY vmi.DtCorrApo, vmi.MovimCod, vmi.GraGruX ',
    '']);
    //Geral.MB_SQL(Self, QrVmiGArCab);
    PB2.Position := 0;
    PB2.Max := QrVmiGArCab.RecordCount;
    QrVmiGArCab.First;
    while not QrVmiGArCab.Eof do
    begin
      MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
      //
      Codigo     := QrVmiGArCabCodigo.Value;
      MovimCod   := QrVmiGArCabMovimCod.Value;
      MovimTwn   := QrVmiGArCabMovimTwn.Value;
      DtHrAberto := QrVmiGArCabData.Value;
      DtHrFimOpe := QrVmiGArCabData.Value;
      DtMovim    := QrVmiGArCabData.Value;
      DtCorrApo  := QrVmiGArCabDtCorrApo.Value;
      GraGruX    := QrVmiGArCabGraGruX.Value;
      TipoEstq   := Trunc(QrVmiGArCabTipoEstq.Value);
      ClientMO   := QrVmiGArCabClientMO.Value;
      FornecMO   := QrVmiGArCabFornecMO.Value;
      EntiSitio  := QrVmiGArCabEntiSitio.Value;
      Controle   := QrVmiGArCabControle.Value;
      //
(*
      UnDmkDAC_PF.AbreMySQLQuery0(QrSumGAR, Dmod.MyDB, [
      'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,  ',
      'SUM(AreaM2) AreaM2 ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestCurtiVS)),
      'AND GraGruX=' + Geral.FF0(GraGruX),
      '']);
      AreaM2     := QrSumGARAreaM2.Value;
      PesoKg     := QrSumGARPesoKg.Value;
      Pecas      := QrSumGARPecas.Value;
*)
(*
      AreaM2     := QrVmiGArCabAreaM2.Value;
      PesoKg     := QrVmiGArCabPesoKg.Value;
      Pecas      := QrVmiGArCabPecas.Value;
*)
      UnDmkDAC_PF.AbreMySQLQuery0(QrSumGAR, Dmod.MyDB, [
      'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,  ',
      'SUM(AreaM2) AreaM2 ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestCurtiVS)),
      'AND GraGruX=' + Geral.FF0(GraGruX),
      '']);
      AreaM2     := QrSumGARAreaM2.Value;
      PesoKg     := QrSumGARPesoKg.Value;
      Pecas      := QrSumGARPecas.Value;
      //
      if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
      begin
        case TipoEstq of
          1: Qtde := AreaM2;
          2: Qtde := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
            //
            Qtde := Pecas;
          end;
        end;
        Qtde := Qtde * MyFatorCab;
      end else
        Qtde := 0;
      //
      if Qtde < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade n�o pode ser negativa!!! (2)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrVmiGArCab)
      else if (Qtde = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidIndsXX: DtHrFimOpe := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade zerada para OP encerrada!!! (2)', QrVmiGArCab);
        end;
      end;
      //
      case TipoRegSPEDProd of
        trsp23X:
        begin
          InsereItemAtual_K230(Integer(MovimID), Codigo, MovimCod,
          Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GraGruX, ClientMO,
          FornecMO, EntiSitio, Qtde, OrigemOpeProc, IDSeq1, False);
        end;
        trsp25X:
        begin
          InsereItemAtual_K250(Integer(MovimID), Codigo, MovimCod,
          Controle,
          DtHrFimOpe,
          DtMovim, DtCorrApo, GraGruX, ClientMO, FornecMO, EntiSitio,
          Qtde, OrigemOpeProc, IDSeq1, False);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(2)');
      end;
////////////////////////////////////////////////////////////////////////////////
///  Item > Registro K235 ou K255
////////////////////////////////////////////////////////////////////////////////
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrVmiGArIts, Dmod.MyDB, [
      'SELECT vmi.SrcMovID, vmi.SrcNivel2,  vmi.SrcGGX, vmi.DstGGX, ',
      'gg1.UnidMed, med.Grandeza med_grandeza, ',
      'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
      VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
      'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
      'DATE(vmi.DataHora) Data, vmi.DtCorrApo, vmi.Codigo, vmi.MovimCod,  ',
      'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg,  ',
      'vmi.MovimTwn, vmi.Controle  ',
      'FROM ' + CO_SEL_TAB_VMI + '        vmi  ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
      'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiVS)), // 15
      'AND vmi.MovimCod=' + Geral.FF0(MovimCod),
      'AND vmi.MovimTwn=' + Geral.FF0(MovimTwn),
      'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
      'AND (vmi.FornecMO=0 ',
      ' OR vmi.FornecMO' + FiltroReg(TipoRegSPEDProd) + Geral.FF0(FEmpresa),
      ')',
      FiltroPeriodo(TipoRegSPEDProd),
      'ORDER BY Data, vmi.MovimCod, vmi.GraGruX ',
      '']);
      //if QrVmiGArIts.RecordCount > 1 then
      //Geral.MB_SQL(Self, QrVmiGArIts);
      QrVmiGArIts.First;
      while not QrVmiGArIts.Eof do
      begin
        if TEstqMovimID(QrVmiGArItsSrcMovID.Value) = emidEmProcCur then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrCalToCur, Dmod.MyDB, [
          'SELECT DstGGX ',
          'FROM ' + CO_SEL_TAB_VMI,
          'WHERE Controle=' + Geral.FF0(QrVmiGArItsSrcNivel2.Value),
          '']);
          GraGruX := QrCalToCurDstGGX.Value;
        end
        else
          GraGruX := QrVmiGArItsGraGruX.Value;
        //
        Codigo     := QrVmiGArItsCodigo.Value;
        MovimCod   := QrVmiGArItsMovimCod.Value;
        MovimTwn   := QrVmiGArItsMovimTwn.Value;
        DtHrAberto := QrVmiGArItsData.Value;
        DtHrFimOpe := QrVmiGArItsData.Value;
        TipoEstq   := QrVmiGArItsTipoEstq.Value;
        Controle   := QrVmiGArItsControle.Value;
        ID_Item    := Controle;
        //
        AreaM2     := QrVmiGArItsAreaM2.Value;
        PesoKg     := QrVmiGArItsPesoKg.Value;
        Pecas      := QrVmiGArItsPecas.Value;
        //
        DtCorrApo  := QrVmiGArItsDtCorrApo.Value;
        //
        if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (DtHrFimOpe > 1) then
        begin
          case TipoEstq of
            1: Qtde := AreaM2;
            2: Qtde := PesoKg;
            else
            begin
              Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
              + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq]);
              //
              Qtde := Pecas;
            end;
          end;
          Qtde := Qtde * MyFatorIts;
        end else
          Qtde := 0;
        //
        if Qtde < 0 then
          Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade n�o pode ser negativa!!! (3)' + sLineBreak +
          'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo), QrVmiGArIts)
        else if (Qtde = 0) and (DtHrFimOpe > 1) then
        begin
          case MovimID of
            emidIndsXX: DtHrFimOpe := 0;
            else Mensagem(sProcName, MovimCod, Integer(MovimID),
            'Quantidade zerada para OP encerrada!!! (3)', QrVmiGArIts);
          end;
        end;
        //
        case TipoRegSPEDProd of
          //trsp21X:
          trsp23X:
          begin
            InsereItemAtual_K235((*LinArqPai*)IDSeq1, DtHrAberto, DtCorrApo, GraGruX,
            Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
            Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
            ESTSTabSorc);
          end;
          trsp25X:
          begin
            InsereItemAtual_K255((*LinArqPai*)IDSeq1, DtHrAberto, DtCorrApo, GraGruX,
            Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
            Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc,
            ESTSTabSorc);
          end;
          else
            Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
            TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(3)');
        end;
        //
        QrVmiGArIts.Next;
      end;
      //
      QrVmiGArCab.Next;
    end;
  end;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_K2XX_GAR;',
  'CREATE TABLE _SPED_EFD_K2XX_GAR',
  'SELECT vmi.DstGGX, vmi.SrcGGX, ',
  'gg1.UnidMed, med.Grandeza med_grandeza, ',
  'xco.Grandeza xco_Grandeza, ggx.GraGruY, xco.CouNiv2, ',
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  'DATE(vmi.DataHora) Data, vmi.Codigo, vmi.MovimCod,  ',
  'vmi.GraGruX, vmi.Pecas, vmi.AreaM2, vmi.PesoKg,  ',
  'vmi.MovimTwn  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '        vmi   ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiVS)),      //15
  SQL_Periodo,
  'ORDER BY Data, vmi.MovimCod, vmi.GraGruX ;',
  '']);
  if ErrGrandeza('_SPED_EFD_K2XX_GAR', 'SrcGGX', 'DstGGX') then
    Exit;
  //
  InsereRegistros(trsp23X);
  InsereRegistros(trsp25X);
end;
}

{
function TFmSpedEfdIcmsIpiClaProd.ProducaoIsolada_ImportaOpeProcA(QrCab: TMySQLQuery; MovimID:
  TEstqMovimID; SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ,
  SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
var
  TabSrc, SQL_FLD, SQL_AND: String;
  MovNivInn, MovNivBxa: TEstqMovimNiv;
  //
  function FiltroReg(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := '=';
      trsp25X: Result := '<>';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (2)');
        Result := '#ERR';
      end;
    end;
  end;
  //
  function FiltroCond(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := ' OR ';
      trsp25X: Result := ' AND ';
      else
      begin
        Geral.MB_Erro('"FiltroCond" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (2a)');
        Result := '#ERR';
      end;
    end;
  end;
  //
  function ReabreEmProcesso21X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    SQL_PeriodoComplexo,
    'OR MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    '  ' + SQL_PeriodoVS,
    ')) ',
    SQL_AND,

    ' ',
    'AND MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv=' + Geral.FF0(Integer(MovNivInn)),
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    //'  AND FornecMO' + FiltroReg(trsp21X) + Geral.FF0(FEmpresa) +
    '  )',
    ') ',
    ' ',
    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function ReabreEmProcesso23X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    //'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    SQL_PeriodoComplexo,
    'OR MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    '  ' + SQL_PeriodoVS,
    ')) ',
    SQL_AND,

    ' ',
    'AND MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv=' + Geral.FF0(Integer(MovNivInn)),
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    '  AND (FornecMO' + FiltroReg(trsp23X) + Geral.FF0(FEmpresa),
    '  ' + FiltroCond(trsp23X) + ' FornecMO' + FiltroReg(trsp23X) + '0) ) ',
    ') ',
    ' ',

    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
(*
    if Query.RecordCount > 0 then Geral.MB_SQL(Self, Query);
*)
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function ReabreEmProcesso25X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    'WHERE  MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv IN (' + Geral.FF0(Integer(MovNivInn)) + ',' +
    Geral.FF0(Integer(MovNivBxa)) + ') ',
    //
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    '  AND (FornecMO' + FiltroReg(trsp25X) + Geral.FF0(FEmpresa),
    '  ' + FiltroCond(trsp25X) + ' FornecMO' + FiltroReg(trsp25X) + '0) ) ',
    ' ' + SQL_PeriodoVS,
    ') ',
    ' ',
    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
(*
    if Query.RecordCount > 0 then Geral.MB_SQL(Self, Query);
*)
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function ReabreEmProcesso26X(Query: TmySQLQuery): Boolean;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    SQL_FLD,
    'FROM ' + TabSrc +  ' vsx',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GGXDst ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    SQL_PeriodoComplexo,
    'OR MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    '  ' + SQL_PeriodoVS,
    ')) ',
    SQL_AND,

    ' ',
    'AND MovimCod IN (  ',
    '  SELECT MovimCod  ',
    '  FROM ' + CO_SEL_TAB_VMI + '  ',
    '  WHERE MovimNiv=' + Geral.FF0(Integer(MovNivInn)),
    '  AND (Empresa = ' + Geral.FF0(FEmpresa),
    '  )',
    ') ',
    ' ',
    //Operacao que nao tem tem itens de origem nem destino, ou seja, estah vazia
    'AND (PecasSrc <> 0 ',
    '     OR ',
    '     PecasDst <> 0 ',
    '     OR ',
    '     (PesoKgSrc <>0 AND ggx.GraGruY < 1024)) ',
    '']);
(*
    if Query.RecordCount > 0 then Geral.MB_SQL(Self, Query);
*)
    //Geral.MB_SQL(Self, Query);
    Result := True;
  end;
  //
  function InsereRegistros(TipoRegSPEDProd: TTipoRegSPEDProd): Boolean;
  var
    LinArqPai, MovimCod: Integer;
    Fator: Double;
    Abriu: Boolean;
  begin
    Result := False;
    case TipoRegSPEDProd of
      trsp21X: Abriu := ReabreEmProcesso21X(QrCab);
      trsp23X: Abriu := ReabreEmProcesso23X(QrCab);
      trsp25X: Abriu := ReabreEmProcesso25X(QrCab);
      trsp26X: Abriu := ReabreEmProcesso26X(QrCab);
      else
      begin
        Geral.MB_Aviso('Tipo de registro indefinido em "InsereRegistros()"');
        Abriu := False;
      end;
    end;
    if not Abriu then
      Exit;
    //
    PB2.Max := QrCab.RecordCount;
    PB2.Position := 0;
    QrCab.First;
    while not QrCab.Eof do
    begin
      MyObjects.UpdPB(PB2, nil, nil);
      MovimCod  := QrCab.FieldByName('MovimCod').AsInteger;
      if TipoRegSPEDProd = trsp21X then
      begin
        case MovimID of
(*         Ja tem proprio: ProducaoIsolada_Insere_OrigeRibPDA_210()
          emidEmRibPDA:
          begin
            Fator := 1;
            ProducaoIsolada_Insere_OrigeRibPDA_210(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminDestPDA, QrCab, LinArqPai, SQL_PeriodoVS, OrigemOpeProc);
          end;
*)
          emidEmProcCal: ; // nada!!
          emidEmProcCur: ; // nada!!
          emidEmOperacao:
          begin
            Fator := 1;
            Insere_OrigeProcVS_210(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminEmOperInn, TEstqMovimNiv.eminEmOperBxa, QrCab, LinArqPai, SQL_PeriodoVS, OrigemOpeProc);
            Insere_EmOpeProc_215(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminDestOper, QrCab, (*Fator,*) SQL_PeriodoVS, OrigemOpeProc, LinArqPai);
          end;
          emidEmProcWE: ; // nada!!
          else
          begin
            Result := False;
            Geral.MB_Erro('MovimID ' + Geral.FF0(Integer(MovimID)) + ' n�o implementado em "ImportaOpeProc().21x"');
            Exit;
          end;
        end;
      end else
      begin
        case MovimID of
          emidEmProcCal:
          begin
            Fator := -1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmCalBxa);
          end;
          emidEmProcCur:
          begin
            Fator := 1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmCurBxa);
          end;
          emidEmProcWE:
          begin
            Fator := 1;

            //criar query que separa periodos + Periodos de correcao!
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmWEndBxa);
          end;
          emidEmProcSP:
          begin
            Fator := 1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmPSPBxa);
          end;
          emidEmReprRM:
          begin
            Fator := 1;
            Insere_EmOpeProc_Prd(TipoRegSPEDProd, MovimID, QrCab, Fator,
              SQL_PeriodoVS, SQL_PeriodoPQ, OrigemOpeProc, TEstqMovimNiv.eminEmRRMBxa);
           // Geral.MB_SQL(Self, QrCab);
          end;
          else
          begin
            Result := False;
            // MovimID 30 n�o implementado em "ImportaOpeProc().2xx"
            Geral.MB_Erro('MovimID ' + Geral.FF0(Integer(MovimID)) + ' n�o implementado em "ImportaOpeProc().2xx"');
            Exit;
          end;
        end;
      end;
      QrCab.Next;
    end;
    Result := True;
  end;
begin
  //PB1.Position := PB1.Position + 1;
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando "DtHrFimOpe" do MovimID: ' +
    Geral.FF0(Integer(MovimID)));
  VS_PF.AtualizaDtHrFimOpe_MovimID(MovimID);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Importando MovimID: ' +
    Geral.FF0(Integer(MovimID)));
  TabSrc    := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
  // Recurtimento!!!
  MovNivInn := VS_PF.ObtemMovimNivInnDeMovimID(MovimID);
  MovNivBxa := VS_PF.ObtemMovimNivBxaDeMovimID(MovimID);
  // ?????
  //MovNivInn := VS_PF.ObtemMovimNivBxaDeMovimID(MovimID);
  if MovNivInn = eminSemNiv then
    Exit;
  //
  SQL_FLD := Geral.ATS([
  'SELECT DISTINCT ggx.GraGru1 Nivel1, ggx.Controle Reduzido, ',
  'gg1.Nome NO_GG1, med.Sigla, med.Grandeza ',
  '']);
  SQL_AND := Geral.ATS([
  'AND ((IF(xco.Grandeza > 0, xco.Grandeza, ',
  '  IF((ggx.GraGruY<2048 OR ',
  '  xco.CouNiv2<>1), 2, ',
  '  IF(xco.CouNiv2=1, 1, -1))) <> med.Grandeza))',
  '']);
  if GrandezasInconsistentes() then
    Exit;
  //
  SQL_FLD := Geral.ATS([
  'SELECT  ',
  //
  VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
  'vsx.AreaDstM2', 'vsx.PesoKgDst', 'vsx.PecasDst'),
  //
  'vsx.* ',
  '']);
  SQL_AND := '';
  //
  case MovimID of
    //emidEmRibPDA, Ja tem proprio: ProducaoIsolada_Insere_OrigeRibPDA_210()
    //emidEmRibDTA,
    emidEmOperacao:
    begin
      InsereRegistros(trsp21X);
    end;
    emidEmProcCal,
    emidEmProcCur,
    emidEmProcWE,
    emidEmProcSP:
    begin
      InsereRegistros(trsp23X);
      InsereRegistros(trsp25X);
    end;
    emidEmReprRM:
    begin
      InsereRegistros(trsp26X);
    end;
    else Geral.MB_Erro('"MovimID" n�o implementado em "ProducaoIsolada_ImportaOpeProcA()"');
  end;
  //
  Result := True;
end;
}

{
function TFmSpedEfdIcmsIpiClaProd.ProducaoIsolada_ImportaOpeProcB(QrCab: TMySQLQuery;
  PsqMovimNiv: TEstqMovimNiv; SQL_PeriodoComplexo, SQL_PeriodoVS, SQL_PeriodoPQ,
  SQL_DtHrFimOpe: String; OrigemOpeProc: TOrigemOpeProc): Boolean;
  //
  function ParteSQL(Tabela, FldSrc, FldDst, sArea, sPeso, sPeca, dArea, dPeso, dPeca: String; MovimNiv:
  TEstqmovimNiv): String;
  var
    SQL_IF: String;
  begin
    SQL_IF := 'IF(' + Copy(SQL_PeriodoVS, 4) + ', ';
    //
    Result := Geral.ATS([
    'SELECT vmi.Empresa, vmi.MovimID, vmi.Codigo, vmi.MovimCod,  ',
    'vmi.MovimNiv, vmi.Controle, ' +
    SQL_IF + sPeca + ', 0.000) Pecas, ' +
    SQL_IF + sArea + ', 0.000) AreaM2, ' +
    SQL_IF + sPeso + ', 0.000) PesoKg, ',
    'DATE(vmi.DataHora) Data,  ' + FldDst + ' GGX_Dst, ', //+ FldOri + ', ',
    FldSrc + ' GGX_Src, ',
    '  ',
    //
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'sTipoEstq', True,
    sArea, sPeso, sPeca, 's'),
    //
    VS_PF.SQL_TipoEstq_DefinirCodi(True, 'dTipoEstq', True,
    dArea, dPeso, dPeca, 'd'),
    //
    ' ',
    SQL_IF + dPeca + ', 0.000) QtdAntPeca, ' +
    SQL_IF + dArea + ', 0.000) QtdAntArM2, ' +
    SQL_IF + dPeso + ', 0.000) QtdAntPeso, ',
    'vmi.DtCorrApo, vmi.ClientMO, vmi.FornecMO, scc.EntiSitio ',
    'FROM ' + TMeuDB + '.' + Tabela + ' cab ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '   vmi ON vmi.MovimCod=cab.MovimCod ',
    'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    sggx ON sggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    sgg1 ON sgg1.Nivel1=sggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    smed ON smed.Codigo=sgg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    sggc ON sggc.Controle=sggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  sgcc ON sgcc.Codigo=sggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  sgti ON sgti.Controle=sggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou sxco ON sxco.GraGruX=sggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    snv1 ON snv1.Codigo=sxco.CouNiv1   ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    snv2 ON snv2.Codigo=sxco.CouNiv2   ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    dggx ON dggx.Controle=cab.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    dgg1 ON dgg1.Nivel1=dggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    dmed ON dmed.Codigo=dgg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    dggc ON dggc.Controle=dggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  dgcc ON dgcc.Codigo=dggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  dgti ON dgti.Controle=dggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou dxco ON dxco.GraGruX=dggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    dnv1 ON dnv1.Codigo=dxco.CouNiv1   ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    dnv2 ON dnv2.Codigo=dxco.CouNiv2   ',
    ' ',
    'WHERE (vmi.Empresa=' + Geral.FF0(FEmpresa),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    SQL_PeriodoVS,
  ') OR ',
  '( ',
  'vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  'AND cab.MovimCod IN ( ',
  'SELECT DISTINCT VSMovCod  ',
  'FROM ' + TMeuDB + '.emit  ',
  'WHERE Codigo IN (   ',
  '  SELECT OrigemCodi  ',
  '  FROM ' + TMeuDB + '.pqx  ',
  '  WHERE Tipo=110     ',
  '  ' + SQL_PeriodoPQ,
  ')   ',
  'AND VSMovCod <> 0  ',
  '  ',
  'UNION   ',
  '  ',
  'SELECT DISTINCT VSMovCod  ',
  'FROM ' + TMeuDB + '.pqo  ',
  'WHERE Codigo IN (   ',
  '  SELECT OrigemCodi  ',
  '  FROM ' + TMeuDB + '.pqx  ',
  '  WHERE Tipo=110     ',
  '  ' + SQL_PeriodoPQ,
  ')   ',
  'AND VSMovCod <> 0  ',
  ////
  ') ',
  ') ',
  '']);
  end;
const
  sProcName = 'FmSPED_EFD_K2XX.ProducaoIsolada_ImportaOpeProcB()';
  //SrcFator = -1;
  DstFator = 1;
  ESTSTabSorc = estsVMI;
var
  Pecas, AreaM2, PesoKg, QtdeSrc, QtdeDst, QtdAntArM2, QtdAntPeso, QtdAntPeca:
  Double;
  Data, DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  SrcTipoEstq, DstTipoEstq, GGXSrc, GGXDst, Codigo, MovimCod, IDSeq1,
  ID_Item, Controle, SrcFator, ClientMO, FornecMO, EntiSitio: Integer;
  MovimNiv: TEstqMovimNiv;
  TipoRegSPEDProd: TTipoRegSPEDProd;
  COD_INS_SUBST, SQLx: String;
  MovimID: TEstqMovimID;
  MovimNivInn: TEstqMovimNiv;
begin
  COD_INS_SUBST := '';
  //
  case PsqMovimNiv of
    TEstqmovimNiv.eminSorcCal: SrcFator := 1;
    TEstqmovimNiv.eminDestCal: SrcFator := -1;
    TEstqmovimNiv.eminSorcCur: SrcFator := 1;
    else
    begin
      SrcFator := -1;
      Geral.MB_Erro('"SrcFator" n�o implementado em "ProducaoIsolada_ImportaOpeProcB()"');
    end;
  end;
  case PsqMovimNiv of
    TEstqmovimNiv.eminSorcCal: SQLx :=
      ParteSQL('vscalcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCal);
    TEstqmovimNiv.eminDestCal: SQLx :=
      ParteSQL('vscaljmp', 'vmi.JmpGGX', 'vmi.GraGruX', '-vmi.QtdGerArM2',
      '-vmi.QtdGerPeso', '-vmi.QtdGerPeca',  'vmi.QtdGerArM2',
      'vmi.QtdGerPeso', 'vmi.QtdGerPeca', TEstqmovimNiv.eminDestCal);
    TEstqmovimNiv.eminSorcCur: SQLx :=
      ParteSQL('vscurcab', 'vmi.RmsGGX', 'cab.GraGruX',
      'vmi.QtdAntArM2', 'vmi.QtdAntPeso', 'vmi.QtdAntPeca', 'vmi.QtdAntArM2',
      'vmi.QtdAntPeso', 'vmi.QtdAntPeca', TEstqmovimNiv.eminSorcCur);
    else
    begin
      SQLx := 'SELECT ???? FROM ????';
      Geral.MB_Erro('"MovimNiv" n�o implementado em "ProducaoIsolada_ImportaOpeProcB()"');
    end;
  end;

  UnDmkDAC_PF.AbreMySQLQuery0(QrMC, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_K2XX_O_P;  ',
  'CREATE TABLE _SPED_EFD_K2XX_O_P  ',
  //
  SQLx,
  '; ',
  ' ',
  'SELECT DISTINCT MovimCod  ',
  'FROM _SPED_EFD_K2XX_O_P ',
  'ORDER BY MovimCod;  ',
  '']);
  //Geral.MB_SQL(Self, QrMC); //Parei aqui
  //
  if ErrGrandeza('_SPED_EFD_K2XX_O_P', 'GGx_Src', 'GGX_Dst' ) then
    Exit;
  //
  PB2.Position := 0;
  PB2.Max := QrMC.RecordCount;
  QrMC.First;
  //if QrMC.RecordCount > 0 then
    //Geral.MB_Aviso('DEPRRCAR!!! OpeProcA - 1');
  while not QrMC.Eof do
  begin
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    UnDmkDAC_PF.AbreMySQLQuery0(QrProdRib, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM _SPED_EFD_K2XX_O_P ',
    'WHERE MovimCod=' + Geral.FF0(QrMCMovimCod.Value),
    '']);
    //Geral.MB_SQL(Self, QrProdRib); //Parei aqui!
    QrProdRib.First;
    while not QrProdRib.Eof do
    begin
      MovimID     := TEstqMovimID(QrProdRibMovimID.Value);
      AreaM2      := QrProdRibAreaM2.Value;
      PesoKg      := QrProdRibPesoKg.Value;
      Pecas       := QrProdRibPecas.Value;
      QtdAntArM2  := QrProdRibQtdAntArM2.Value;
      QtdAntPeso  := QrProdRibQtdAntPeso.Value;
      QtdAntPeca  := QrProdRibQtdAntPeca.Value;
      Data        := QrProdRibData.Value;
      SrcTipoEstq := Trunc(QrProdRibsTipoEstq.Value);
      DstTipoEstq := Trunc(QrProdRibdTipoEstq.Value);
      GGXSrc      := QrProdRibGGX_Src.Value;
      GGXDst      := QrProdRibGGX_Dst.Value;
      MovimCod    := QrProdRibMovimCod.Value;
      MovimNiv    := TEstqMovimNiv(QrProdRibMovimNiv.Value);
      Codigo      := QrProdRibCodigo.Value;
      Controle    := QrProdRibControle.Value;
      ID_Item     := Controle;
      //
      DtMovim     := Data;
      DtCorrApo   := QrProdRibDtCorrApo.Value;
      //
      ClientMO    := QrProdRibClientMO.Value;
      FornecMO    := QrProdRibFornecMO.Value;
      EntiSitio    := QrProdRibEntiSitio.Value;
      //
      if (ClientMO =0) or (FornecMO=0) or (EntiSitio=0) then
      begin
        MovimNivInn := VS_PF.ObtemMovimNivInnDeMovimID(MovimID);
        UnDmkDAC_PF.AbreMySQLQuery0(QrObtCliForSite, Dmod.MyDB, [
        'SELECT vmi.ClientMO, vmi.FornecMO, scc.EntiSitio  ',
        'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
        'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc  ',
        'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo  ',
        'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
        'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
        '']);
        ClientMO  := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
        FornecMO  := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
        EntiSitio  := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
      end;
      //
      //if (Pecas <> 0) and (DtHrFimOpe > 1) then
      if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) and (Data > 1) then
      begin
        case SrcTipoEstq of
          1: QtdeSrc := AreaM2;
          2: QtdeSrc := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(SrcTipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[SrcTipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrProdRib.SQL.Text);
            //
            QtdeDst := Pecas;
          end;
        end;
        QtdeSrc := QtdeSrc * SrcFator;

        //
        case DstTipoEstq of
          1: QtdeDst := QtdAntArM2;
          2: QtdeDst := QtdAntPeso;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(SrcTipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[SrcTipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrProdRib.SQL.Text);
            //
            QtdeDst := QtdAntPeca;
          end;
        end;
        QtdeDst := QtdeDst * DstFator;
      end else
      begin
        QtdeSrc := 0;
        QtdeDst := 0;
      end;
      //
      if QtdeSrc < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade "Src" n�o pode ser negativa!!! (5)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo), QrProdRib);
      if QtdeDst < 0 then
        Mensagem(sProcName, MovimCod, Integer(MovimID),
        'Quantidade "Dst" n�o pode ser negativa!!! (6)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo), QrProdRib)
      else if (QtdeDst = 0) and (Data > 1) then
      begin
        case MovimID of
          emidEmProcCal: Data := 0;
          emidEmProcCur: Data := 0;
          else Mensagem(sProcName, MovimCod, Integer(MovimID),
          'Quantidade zerada para OP encerrada!!! (4)', QrProdRib);
        end;
      end;
      //
      if QrSubPrdOPEmpresa.Value = QrFMOFornecMO.Value then
        TipoRegSPEDProd := trsp23X
      else
        TipoRegSPEDProd := trsp25X;
      //
      DtHrAberto := Data;
      DtHrAberto := Data;
      case TipoRegSPEDProd of
        trsp23X:
        begin
          InsereItemAtual_K230(Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO,
          FornecMO, EntiSitio, QtdeDst, OrigemOpeProc, IDSeq1);
          //
          InsereItemAtual_K235((*LinArqPai*)IDSeq1, DtHrAberto, DtCorrApo, GGXSrc,
          QtdeSrc, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, ESTSTabSorc);
          //
          Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv, MovimCod,
          (*LinArqPai*)IDSeq1, SQL_PeriodoPQ, OrigemOpeProc, ClientMO, FornecMO, EntiSitio);
        end;
        trsp25X:
        begin
          InsereItemAtual_K250(Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrFimOpe, DtMovim, DtCorrApo, GGXDst, ClientMO, FornecMO, EntiSitio,
          QtdeDst, OrigemOpeProc, (*LinArqPai*)IDSeq1);
          //
          InsereItemAtual_K255((*LinArqPai*)IDSeq1, DtHrAberto, DtCorrApo, GGXSrc,
          QtdeSrc, COD_INS_SUBST, ID_Item,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, ESTSTabSorc);
          //
          Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv, MovimCod,
          (*LinArqPai*)IDSeq1, SQL_PeriodoPQ, OrigemOpeProc, ClientMO, FornecMO, EntiSitio);
        end;
        else Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(5)');
      end;
      QrProdRib.Next;
    end;
    QrMC.Next;
  end;
end;
}

{
function TFmSpedEfdIcmsIpiClaProd.InsereItemAtual_K210(const MovimID, Codigo,
  MovimCod, Controle: Integer; const DataIni, DataFim, DtMovim, DtCorrApo: TDateTime;
  const GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde: Double;
  const OrigemOpeProc: TOrigemOpeProc; const OriIDKnd: TOrigemIDKnd;
  var IDSeq1: Integer; Agrega: Boolean): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik210';
  REG      = 'K210';
  //OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_INI_OS, DT_FIN_OS, COD_DOC_OS, COD_ITEM_ORI: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, OriOpeProc, OrigemIDKnd: Integer;
  QTD_ORI: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  SQLType        := stNil;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0;//Integer(MovimNiv);
  //
  IDSeq1         := 0;
  //ID_SEK         := ;
  DT_INI_OS      := Geral.FDT(DataIni, 1);
  if Int(DataFim) > FDiaFim then
    DT_FIN_OS    := '0000-00-00'
  else
    DT_FIN_OS    := Geral.FDT(DataFim, 1);
  COD_DOC_OS     := Geral.FF0(MovimCod);
  COD_ITEM_ORI   := Geral.FF0(GraGruX);
  QTD_ORI        := Qtde;
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  OrigemIDKnd    := Integer(OriIDKnd);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OriIDKnd, TOrigemSPEDEFDKnd.osekProducao, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    FTipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo, MovimCod,
    Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
    OriIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND AnoMes=' + Geral.FF0(FAnoMes),
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    '']);
    //Geral.MB_SQL(Self, QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      MeAviso.Lines.Add('Item j� importado > ID: ' + Geral.FF0(KndItm) +
      ' Reduzido: ' + Geral.FF0(GraGruX));
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_INI_OS', 'DT_FIN_OS',
    'COD_DOC_OS', 'COD_ITEM_ORI', 'QTD_ORI',
    'GraGruX', 'OriOpeProc', 'OrigemIDKnd'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_INI_OS, DT_FIN_OS,
    COD_DOC_OS, COD_ITEM_ORI, QTD_ORI,
    GraGruX, OriOpeProc, OrigemIDKnd], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TFmSpedEfdIcmsIpiClaProd.InsereItemAtual_K215(IDSeq1: Integer; Data,
  DtMovim, DtCorrApo: TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST:
  String; ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriIDKnd: TOrigemIDKnd;
  OriESTSTabSorc: TEstqSPEDTabSorc): Boolean;
const
  TabDst   = 'efdicmsipik215';
  REG      = 'K215';
  RegPai   = 'K210';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  COD_ITEM_DES: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq2, ESTSTabSorc, OriOpeProc, OrigemIDKnd: Integer;
  QTD_DES: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  Result := False;
  SQLType        := stNil;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  COD_ITEM_DES   := Geral.FF0(GraGruX);
  QTD_DES        := Qtde;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  OrigemIDKnd    := Integer(OriIDKnd);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K275(REG, MovimID, Codigo, MovimCod, Controle,
    GraGruX, (*LinArqPai*)IDSeq1, Qtde, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
    OriESTSTabSorc, OrigemOpeProc, OriIDKnd, TOrigemSPEDEFDKnd.osekProducao);
    //
    EfdIcmsIpi_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    FTipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
    OriIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND AnoMes=' + Geral.FF0(FAnoMes),
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    '']);
    //Geral.MB_SQL(Self, QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      MeAviso.Lines.Add('Item j� importado > ID: ' + Geral.FF0(KndItm) +
      ' Reduzido: ' + Geral.FF0(GraGruX));
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'COD_ITEM_DES', 'QTD_DES',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc',
    'OrigemIDKnd'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, COD_ITEM_DES, QTD_DES,
    GraGruX, ESTSTabSorc, OriOpeProc,
    OrigemIDKnd], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;
}

function TFmSpedEfdIcmsIpiClaProd_v03_0_1.InsereItemAtual_K220(MovimID: Integer; Data:
  TDateTime; GGXOri, GGXDst: Integer; AreaM2: Double; Codigo, MovimCod,
  CtrlDst, _CtrlOri, ClientMO, FornecMO, EntiSitio: Integer;
  ESOMIEM: TEstqSPED220OMIEM;
  MovimNiv: Integer; OrigemIDKnd: TOrigemIDKnd; DtCorrApo: TDateTime): Boolean;
const
  REG = 'K220';
  TabDst = 'efdicmsipik220';
  ID_SEK = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  COD_INS_SUBST = '';
  ESTSTabSorc = estsVMI;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekClasse;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItmOri,
  KndItmDst, KndAID, KndNiv: Integer;
  SQLType: TSQLType;
var
  DT_MOV, COD_ITEM_ORI, COD_ITEM_DEST: String;
  QTD, QTD_ORI, QTD_DEST: Double;
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  OrigemOpeProc: TOrigemOpeProc;
  MovimIDPsq: TEstqMovimID;
  MovimNivInn: TEstqMovimNiv;
  CtrlOri, IDSeq1: Integer;
begin
  OrigemOpeProc := oopClassReclass;
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemIDKnd = ' +
    GetEnumName(TypeInfo(TOrigemIDKnd),Integer(OrigemIDKnd)));
  Result := False;
  //SQLType        := ImgTipo.SQLType?;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItmOri      := CtrlOri;
  KndItmDst      := CtrlDst;
  KndAID         := Integer(MovimID);
  KndNiv         := Integer(MovimNiv);
  //
  DT_MOV         := Geral.FDT(Data, 1);
  COD_ITEM_ORI   := Geral.FF0(GGXOri);
  COD_ITEM_DEST  := Geral.FF0(GGXDst);
  QTD            := AreaM2; //Geral.FFT_Dot(AreaM2, 2, siNegativo);
  QTD_ORI        := QTD;
  QTD_DEST       := QTD;
  //MovimID        := Geral.FF0(MovimID);
  if _CtrlOri = 0 then
  begin
    if TEstqMovimID(MovimID) = TEstqMovimID.emidFinished then
      MovimIDPsq := TEstqMovimID.emidEmProcWE
    else
      MovimIDPsq := TEstqMovimID(MovimID);

    MovimNivInn := VS_PF.ObtemMovimNivInnDeMovimID(MovimIDPsq);
    //
    if MovimNivInn <> TEstqMovimNiv.eminSemNiv then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrBuscaCtrl, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimID=' + Geral.FF0(Integer(MovimIDPsq)),
      'AND MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
      'AND Codigo=' + Geral.FF0(Codigo),
      'AND MovimCod=' + Geral.FF0(MovimCod),
      '']);
      CtrlOri := QrBuscaCtrlControle.Value;
    end;
  end else
    CtrlOri := _CtrlOri;
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    if TEstqMovimID(MovimID) <> (*33*)emidEmReprRM then // n�o � conserto!
    begin
      EfdIcmsIpi_PF_v03_0_1.InsereItemAtual_K270(ImporExpor, AnoMes, Empresa,
      PeriApu, REG, Data, DtCorrApo, MovimID, Codigo, MovimCod,
      CtrlOri, GGXOri, AreaM2, SPED_EFD_KndRegOrigem, OrigemOpeProc,
      OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
      //
      //n�o tem FornecMO nem Sitio?
      //
      EfdIcmsIpi_PF_v03_0_1.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
      FTipoPeriodoFiscal, ImporExpor, AnoMes, Empresa, PeriApu, 'K270', REG,
      Data, DtCorrApo, MovimID, Codigo, MovimCod, CtrlOri, GGXOri, ClientMO,
      FornecMO, EntiSitio, slkPrefSitio, -AreaM2, sebalVSMovItX,
      SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
      OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    end;
    //
    EfdIcmsIpi_PF_v03_0_1.InsereItemAtual_K275(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, MovimID, Codigo, MovimCod, CtrlDst, GGXDst,
    IDSeq1, AreaM2, COD_INS_SUBST, SPED_EFD_KndRegOrigem, ESTSTabSorc,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso); // item de destino
    //
    //
    EfdIcmsIpi_PF_v03_0_1.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    FTipoPeriodoFiscal, ImporExpor, AnoMes, Empresa, PeriApu, 'K275', REG,
    Data, DtCorrApo, MovimID, Codigo, MovimCod, CtrlDst, GGXDst, ClientMO,
    FornecMO, EntiSitio, slkPrefSitio, AreaM2, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND AnoMes=' + Geral.FF0(FAnoMes),
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItmOri=' + Geral.FF0(KndItmOri),
    'AND KndItmDst=' + Geral.FF0(KndItmDst),
    '']);
    //Geral.MB_SQL(Self, QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      MeAviso.Lines.Add('Item j� importado > ID origem: ' + Geral.FF0(KndItmOri) +
      ' Reduzido origem: ' + Geral.FF0(GGXOri) + ' ID destino: ' +
      Geral.FF0(KndItmDst) + ' Reduzido destino: ' + Geral.FF0(GGXDst));
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'ID_SEK',
    'DT_MOV', 'COD_ITEM_ORI', 'COD_ITEM_DEST',
    'QTD', 'GGXDst', 'GGXOri',
    'ESOMIEM', 'OrigemIDKnd', 'IDSeq1',
    'QTD_ORI', 'QTD_DEST'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItmOri', 'KndItmDst'], [
    KndAID, KndNiv, ID_SEK,
    DT_MOV, COD_ITEM_ORI, COD_ITEM_DEST,
    QTD, GGXDst, GGXOri,
    ESOMIEM, OrigemIDKnd, IDSeq1,
    QTD_ORI, QTD_DEST], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItmOri, KndItmDst], True);
  end;
end;
{
function TFmSpedEfdIcmsIpiClaProd.InsereItemAtual_K230(const MovimID, Codigo, MovimCod,
  Controle: Integer; const DataIni, DataFim, DtMovim, DtCorrApo: TDateTime;
  const GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde: Double;
  const OrigemOpeProc: TOrigemOpeProc; var IDSeq1: Integer; Agrega: Boolean):
  Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik230';
  REG      = 'K230';
  //Controle = 0;
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_INI_OP, DT_FIN_OP, COD_DOC_OP, COD_ITEM: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, OriOpeProc: Integer;
  QTD_ENC: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  Result         := False;
  //
  SQLType        := stNil;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  DT_INI_OP      := Geral.FDT(DataIni, 1);
  if Int(DataFim) > FDiaFim then
    DT_FIN_OP    := '0000-00-00'
  else
    DT_FIN_OP    := Geral.FDT(DataFim, 1);
  COD_DOC_OP     := Geral.FF0(MovimCod);
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD_ENC        := Qtde;
  //
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_1.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    FTipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND AnoMes=' + Geral.FF0(FAnoMes),
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    '']);
    //Geral.MB_SQL(Self, QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      MeAviso.Lines.Add('Item j� importado > ID: ' + Geral.FF0(KndItm) +
      ' Reduzido: ' + Geral.FF0(GraGruX));
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_INI_OP', 'DT_FIN_OP',
    'COD_DOC_OP', 'COD_ITEM', 'QTD_ENC',
    'GraGruX', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_INI_OP, DT_FIN_OP,
    COD_DOC_OP, COD_ITEM, QTD_ENC,
    GraGruX, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TFmSpedEfdIcmsIpiClaProd.InsereItemAtual_K235(IDSeq1: Integer; Data,
  DtCorrApo: TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
  ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO, EntiSitio:
  Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc):
  Boolean;
const
  TabDst   = 'efdicmsipik235';
  REG      = 'K235';
  RegPai   = 'K230';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
(*
var
  DT_SAIDA, COD_ITEM: String;
  ImporExpor, AnoMes, Empresa, LinArq, K100, K2X0: Integer;
  QTD: Double;
  SQLType: TSQLType;
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
*)
var
  DT_SAIDA, COD_ITEM: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq2, ESTSTabSorc, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  SQLType        := stNil;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  DT_SAIDA       := Geral.FDT(Data, 1);
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K275(REG, MovimID, Codigo, MovimCod, Controle,
    GraGruX, (*LinArqPai*)IDSeq1, Qtde, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
    OriESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd); // item de destino
    //
    //
    EfdIcmsIpi_PF_v03_0_1.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    FTipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, Data, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND AnoMes=' + Geral.FF0(FAnoMes),
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    '']);
    //Geral.MB_SQL(Self, QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      MeAviso.Lines.Add('Item j� importado > ID: ' + Geral.FF0(KndItm) +
      ' Reduzido: ' + Geral.FF0(GraGruX));
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'DT_SAIDA',
    'COD_ITEM', 'QTD', 'COD_INS_SUBST',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, DT_SAIDA,
    COD_ITEM, QTD, COD_INS_SUBST,
    GraGruX, ESTSTabSorc, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;
}
function TFmSpedEfdIcmsIpiClaProd_v03_0_1.InsereItemAtual_K23SubPrd(const MovimID, Codigo,
  MovimCod: Integer; const DataIni, DataFim: TDateTime; const GraGruX: Integer;
  const Qtde: Double; var IDSeq1: Integer): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik23subprd';
  REG      = 'K230';
var
  ImporExpor, AnoMes, Empresa, K100, LinArq: Integer;
  SQLType: TSQLType;
var
  DT_INI_OP, DT_FIN_OP, COD_DOC_OP, COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, LinArq, K100, ID_SEK, MovimID, Codigo, MovimCod: Integer;
  QTD_ENC: Double;
begin
  IDSeq1      := -1;
  Result         := False;
  //SQLType        := ImgTipo.SQLType?;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  K100           := FPeriApu;
  //
  DT_INI_OP      := Geral.FDT(DataIni, 1);
  if Int(DataFim) > FDiaFim then
    DT_FIN_OP    := '0000-00-00'
  else
    DT_FIN_OP    := Geral.FDT(DataFim, 1);
  COD_DOC_OP     := Geral.FF0(MovimCod);
  COD_ITEM       := Geral.FF0(GraGruX);
  //QTD_ENC        := Geral.FFT_Dot(Qtde, 3, siNegativo);
  QTD_ENC        := Qtde;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + TabDst,
  'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
  'AND AnoMes=' + Geral.FF0(FAnoMes),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND K100=' + Geral.FF0(K100),
  'AND MovimID=' + Geral.FF0(MovimID),
  'AND COD_DOC_OP=' + COD_DOC_OP,
  'AND COD_ITEM=' + COD_ITEM,
  'AND ID_SEK=' + Geral.FF0(ID_SEK),
  '']);
  //Geral.MB_SQL(Self, QrX999);
  if DmProd.QrX999.RecordCount > 0 then
  begin
    LinArq  := DmProd.QrX999.FieldByName('LinArq').AsInteger;
    QTD_ENC := QTD_ENC + DmProd.QrX999.FieldByName('QTD_ENC').AsFloat;
    SQLType := stUpd;
  end else
  begin
    LinArq := 0;
    LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'LinArq', [
    (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
    stIns, LinArq, siPositivo, nil);
    //
    SQLType := stIns;
  end;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
  'REG', 'DT_INI_OP', 'DT_FIN_OP',
  'COD_DOC_OP', 'COD_ITEM', 'QTD_ENC',
  'K100', 'ID_SEK', 'MovimID',
  'Codigo', 'MovimCod', 'GraGruX'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  REG, DT_INI_OP, DT_FIN_OP,
  COD_DOC_OP, COD_ITEM, QTD_ENC,
  K100, ID_SEK, MovimID,
  Codigo, MovimCod, COD_ITEM], [
  ImporExpor, AnoMes, Empresa, LinArq], True);
  //
  if Result then
    IDSeq1 := LinArq;
end;
{
function TFmSpedEfdIcmsIpiClaProd.InsereItemAtual_K250(const MovimID, Codigo, MovimCod,
  Controle: Integer; const DtHrFimOpe, DtMovim, DtCorrApo: TDateTime; const
  GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde: Double; const
  OrigemOpeProc: TOrigemOpeProc; var IDSeq1: Integer; Agrega: Boolean):
  Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik250';
  REG      = 'K250';
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
(*var
  ImporExpor, AnoMes, Empresa, K100, LinArq: Integer;
  SQLType: TSQLType;
  DT_PROD, COD_DOC_OP, COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, LinArq, K100, ID_SEK, MovimID, Codigo, MovimCod: Integer;
  QTD: Double;
  Data: TDateTime;
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
*)
var
  DT_PROD, COD_ITEM, COD_DOC_OP: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  Data: TDateTime;
begin
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  Result         := False;
  //
  SQLType        := stNil;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  Data           := DataFimObrigatoria(DtHrFimOpe);
  DT_PROD        := Geral.FDT(Data, 1);
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD            := Qtde;
  COD_DOC_OP     := Geral.FF0(MovimCod);
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_1.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    FTipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND AnoMes=' + Geral.FF0(FAnoMes),
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    '']);
    //Geral.MB_SQL(Self, QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      MeAviso.Lines.Add('Item j� importado > ID: ' + Geral.FF0(KndItm) +
      ' Reduzido: ' + Geral.FF0(GraGruX));
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_PROD', 'COD_ITEM',
    'QTD', 'COD_DOC_OP', 'GraGruX',
    'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_PROD, COD_ITEM,
    QTD, COD_DOC_OP, GraGruX,
    OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TFmSpedEfdIcmsIpiClaProd.InsereItemAtual_K255(IDSeq1: Integer;
  Data, DtCorrApo: TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
  ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO, EntiSitio: Integer;
  OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc): Boolean;
const
  TabDst   = 'efdicmsipik255';
  REG      = 'K255';
  RegPai   = 'K250';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_CONS, COD_ITEM: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq2, ESTSTabSorc, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  //
  SQLType        := stNil;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  DT_CONS        := Geral.FDT(Data, 1);
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K275(REG, MovimID, Codigo, MovimCod, Controle,
    GraGruX, (*LinArqPai*)IDSeq1, Qtde, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
    OriESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd); // item de destino
    //
    //
    EfdIcmsIpi_PF_v03_0_1.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    FTipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, Data, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND AnoMes=' + Geral.FF0(FAnoMes),
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    '']);
    //Geral.MB_SQL(Self, QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      MeAviso.Lines.Add('Item j� importado > ID: ' + Geral.FF0(KndItm) +
      ' Reduzido: ' + Geral.FF0(GraGruX));
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'DT_CONS',
    'COD_ITEM', 'QTD', 'COD_INS_SUBST',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, DT_CONS,
    COD_ITEM, QTD, COD_INS_SUBST,
    GraGruX, ESTSTabSorc, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;
}
function TFmSpedEfdIcmsIpiClaProd_v03_0_1.InsereItemAtual_K25SubPrd(const MovimID, Codigo,
  MovimCod: Integer; const Data: TDateTime; const GraGruX: Integer;
  const Qtde: Double; var IDSeq1: Integer): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik25subprd';
  REG      = 'K250';
var
  ImporExpor, AnoMes, Empresa, K100, LinArq: Integer;
  SQLType: TSQLType;
var
  DT_PROD, COD_DOC_OP, COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, LinArq, K100, ID_SEK, MovimID, Codigo, MovimCod: Integer;
  QTD: Double;
begin
  IDSeq1      := -1;
  Result         := False;
  //SQLType        := ImgTipo.SQLType?;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  K100           := FPeriApu;
  //
  DT_PROD        := Geral.FDT(Data, 1);
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD            := Qtde;
  COD_DOC_OP     := Geral.FF0(MovimCod);

  //
  UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + TabDst,
  'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
  'AND AnoMes=' + Geral.FF0(FAnoMes),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND K100=' + Geral.FF0(K100),
  'AND MovimID=' + Geral.FF0(MovimID),
  'AND COD_DOC_OP=' + COD_DOC_OP,
  'AND COD_ITEM=' + COD_ITEM,
  'AND ID_SEK=' + Geral.FF0(ID_SEK),
  '']);
  //Geral.MB_SQL(Self, QrX999);
  if DmProd.QrX999.RecordCount > 0 then
  begin
    LinArq  := DmProd.QrX999.FieldByName('LinArq').AsInteger;
    QTD     := QTD + DmProd.QrX999.FieldByName('QTD').AsFloat;
    SQLType := stUpd;
  end else
  begin
    LinArq := 0;
    LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'LinArq', [
    (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
    stIns, LinArq, siPositivo, nil);
    //
    SQLType := stIns;
  end;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
  'REG', 'K100', 'DT_PROD',
  'COD_ITEM', 'QTD', 'ID_SEK',
  'MovimID', 'Codigo', 'MovimCod',
  'COD_DOC_OP', 'GraGruX'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  REG, K100, DT_PROD,
  COD_ITEM, QTD, ID_SEK,
  MovimID, Codigo, MovimCod,
  COD_DOC_OP, COD_ITEM], [
  ImporExpor, AnoMes, Empresa, LinArq], True);
  //
  if Result then
    IDSeq1 := LinArq;
end;
{
function TFmSpedEfdIcmsIpiClaProd.InsereItemAtual_K260(const MovimID, Codigo,
  MovimCod, Controle: Integer; const DtHrAberto, DtHrFimOpe, DtMovim,
  DtCorrApoProd, DtCorrApoCons: TDateTime; const GraGruX(*, CtrlDst, CtrlBxa*),
  ClientMO, FornecMO, EntiSitio: Integer; const QtdeProd, QtdeCons: Double;
  const OrigemOpeProc: TOrigemOpeProc; var IDSeq1: Integer; const Agrega:
  Boolean): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik260';
  REG      = 'K260';
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  COD_OP_OS, COD_ITEM, DT_SAIDA, DT_RET: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, OriOpeProc: Integer;
  QTD_SAIDA, QTD_RET: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result         := False;
  if DtCorrApoProd = -1 then
    Geral.MB_Aviso('DtCorrApoProd = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  if DtCorrApoCons = -1 then
    Geral.MB_Aviso('DtCorrApoCons = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  //
  SQLType        := stNil;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  COD_OP_OS      := Geral.FF0(MovimCod);
  COD_ITEM       := Geral.FF0(GraGruX);
  DT_SAIDA       := Geral.FDT(DtHrAberto, 1);
  QTD_SAIDA      := QtdeCons;
  DT_RET         := Geral.FDT(DtHrFimOpe, 1);
  QTD_RET        := QtdeProd;
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApoProd > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    //QtdPos e Neg no mesmo registro n�o pode!
    Result := InsereItemAtual_K270(REG, DtMovim, DtCorrApoProd, MovimID, Codigo,
    MovimCod, Controle,  GraGruX, QtdeProd, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, IDSeq1); // Item de origem
    //
    if Result then
    begin
      //
    //
      EfdIcmsIpi_PF_v03_0_1.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
      FTipoPeriodoFiscal, ImporExpor, AnoMes,
      Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApoProd, MovimID, Codigo, MovimCod,
      Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
      QtdeProd, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
      OrigemIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
      //
      Result := InsereItemAtual_K270(REG, DtMovim, DtCorrApoCons, MovimID, Codigo,
      MovimCod, Controle,  GraGruX, -QtdeCons, SPED_EFD_KndRegOrigem,
      OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, IDSeq1); // Item de origem
      //
    //
      EfdIcmsIpi_PF_v03_0_1.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
      FTipoPeriodoFiscal, ImporExpor, AnoMes,
      Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApoCons, MovimID, Codigo, MovimCod,
      Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
      -QtdeCons, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
      OrigemIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
      //
    end;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND AnoMes=' + Geral.FF0(FAnoMes),
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    '']);
    //Geral.MB_SQL(Self, QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      MeAviso.Lines.Add('Item j� importado > ID: ' + Geral.FF0(KndItm) +
      ' Reduzido: ' + Geral.FF0(GraGruX));
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'COD_OP_OS', 'COD_ITEM',
    'DT_SAIDA', 'QTD_SAIDA', 'DT_RET',
    'QTD_RET', 'GraGruX', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, COD_OP_OS, COD_ITEM,
    DT_SAIDA, QTD_SAIDA, DT_RET,
    QTD_RET, GraGruX, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TFmSpedEfdIcmsIpiClaProd.InsereItemAtual_K265(IDSeq1: Integer; Data,
  DtCorrApo: TDateTime; GraGruX: Integer; QtdeCons, QtdeRet: Double;
  COD_INS_SUBST: String;
  ID_Item, MovimID, Codigo, MovimCod, Controle: Integer;
  OrigemOpeProc: TOrigemOpeProc; ESTSTabSorc: TEstqSPEDTabSorc): Boolean;
const
  TabDst   = 'efdicmsipik265';
  REG      = 'K265';
  RegPai   = 'K260';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
var
  COD_ITEM: String;
  ImporExpor, AnoMes, Empresa, LinArq, K260: Integer;
  QTD_CONS, QTD_RET: Double;
  SQLType: TSQLType;
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  //SQLType        := ImgTipo.SQLType?;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  //K2X0           := FPeriApu;
  //
  //REG            := K235 ou K265;
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD_CONS       := QtdeCons;
  QTD_RET        := QtdeRet;
  //COD_INS_SUBST  :=
  K260           := IDSeq1;
  ID_Item        := Controle;
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    if QTD_CONS <> 0 then
      Result := InsereItemAtual_K275(REG, MovimID, Codigo, MovimCod, Controle,
      GraGruX, (*LinArqPai*)IDSeq1, QTD_CONS, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
      ESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd); // item de destino
    //
    if QTD_RET <> 0 then
      Result := InsereItemAtual_K275(REG, MovimID, Codigo, MovimCod, Controle,
      GraGruX, (*LinArqPai*)IDSeq1, QTD_RET, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
      ESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd); // item de destino
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND AnoMes=' + Geral.FF0(FAnoMes),
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'AND ' + RegPai + '=' + Geral.FF0(K260),
    'AND COD_ITEM=' + COD_ITEM,
    'AND ID_SEK=' + Geral.FF0(ID_SEK),
    '']);
    //Geral.MB_SQL(Self, QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      LinArq   := DmProd.QrX999.FieldByName('LinArq').AsInteger;
///////////////////////////// CUIDADO //////////////////////////////////////////
      // Est� duplicando!
      // - Quando tem consumo sem produ���o ?
      // - Quando tem corre��o de apontamento?
      (*
      QTD_CONS := QTD_CONS + DmProd.QrX999.FieldByName('QTD_CONS').AsFloat;
      QTD_RET  := QTD_RET  + DmProd.QrX999.FieldByName('QTD_RET').AsFloat;
      *)
////////////////////////////////////////////////////////////////////////////////
      //
      SQLType  := stUpd;
    end else
    begin
      LinArq := 0;
      LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'LinArq', [
      (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
      stIns, LinArq, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'REG', 'K260', 'ID_SEK',
    'COD_ITEM', 'QTD_CONS', 'QTD_RET',
    'ID_Item', 'MovimID', 'Codigo',
    'MovimCod', 'Controle', 'GraGruX',
    'ESTSTabSorc', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
    REG, K260, ID_SEK,
    COD_ITEM, QTD_CONS, QTD_RET,
    ID_Item, MovimID, Codigo,
    MovimCod, Controle, GraGruX,
    ESTSTabSorc, OrigemOpeProc], [
    ImporExpor, AnoMes, Empresa, LinArq], True);
  end;
end;

function TFmSpedEfdIcmsIpiClaProd.InsereItemAtual_K270(const RegOri: String; const Data,
  DtCorrApo: TDateTime; const MovimID, Codigo, MovimCod, Controle, GraGruX:
  Integer; const Qtde: Double; const SPED_EFD_KndRegOrigem:
  TSPED_EFD_KndRegOrigem; OrigemOpeProc: TOrigemOpeProc; OrigemIDKnd:
  TOrigemIDKnd; const OriSPEDEFDKnd: TOrigemSPEDEFDKnd; var IDSeq1: Integer): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik270';
  REG      = 'K270';
  ValAntes = 0;
  TabSorc  = TEstqSPEDTabSorc.estsVMI;
var
  DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM, ORIGEM, RegisPai: String;
  ImporExpor, AnoMes, Empresa, OriOpeProc,
  PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv: Integer;
  QTD_COR_POS, QTD_COR_NEG, ValNovo: Double;
  SQLType: TSQLType;
  //
  ThatDtIni, ThatDtFim: TDateTime;
  SQL_POS_NEG: String;
begin
  IDSeq1         := 0;
  //SQLType        := ImgTipo.SQLType?;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  EfdIcmsIpi_PF.DefineDatasPeriCorrApoSPED(FEmpresa, 'K100', Data, DtCorrApo, ThatDtIni, ThatDtFim);
  //MovimID        := ;
  //Codigo         := ;
  //MovimCod       := ;
  DT_INI_AP      := Geral.FDT(ThatDtIni, 1);
  DT_FIN_AP      := Geral.FDT(ThatDtFim, 1);
  COD_OP_OS      := Geral.FF0(MovimCod);
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD_COR_POS    := 0;
  QTD_COR_NEG    := 0;
  RegisPai       := RegOri;
  ValNovo := Qtde;
  EfdIcmsIpi_PF.DefineQuantidadePositivaOuNegativa(RegOri, REG, ValAntes, ValNovo,
    caflpnMovNormEsquecido, QTD_COR_POS, QTD_COR_NEG);
  ORIGEM         := Geral.FF0(Integer(SPED_EFD_KndRegOrigem));
  //GraGruX        := ; GGXOri
  OriOpeProc     := Integer(OrigemOpeProc);
  if QTD_COR_POS > 0 then
    SQL_POS_NEG := 'AND QTD_COR_POS > 0 '
  else
    SQL_POS_NEG := 'AND QTD_COR_NEG > 0 ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
  'SELECT * ',
  'FROM efdicmsipik270 ',
  'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
  'AND AnoMes=' + Geral.FF0(FAnoMes),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND PeriApu=' + Geral.FF0(PeriApu),
  'AND KndTab=' + Geral.FF0(KndTab),
  'AND KndCod=' + Geral.FF0(KndCod),
  'AND KndNSU=' + Geral.FF0(KndNSU),
  'AND KndItm=' + Geral.FF0(KndItm),
  '']);
  //Geral.MB_SQL(Self, QrX999);
  IDSeq1 := 0;
  if DmProd.QrX999.RecordCount > 0 then
  begin
    MeAviso.Lines.Add('Item j� importado > ID: ' + Geral.FF0(KndItm) +
    ' Reduzido: ' + Geral.FF0(GraGruX));
    Exit;
  end else
  begin
    IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
    'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
    ImporExpor, AnoMes, Empresa, PeriApu],
    stIns, IDSeq1, siPositivo, nil);
    //
    SQLType := stIns;
  end;
    //
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
  'IDSeq1', 'KndAID', 'KndNiv',
  'DT_INI_AP', 'DT_FIN_AP', 'COD_OP_OS',
  'COD_ITEM', 'QTD_COR_POS', 'QTD_COR_NEG',
  'ORIGEM', 'GraGruX', 'OriOpeProc',
  'OrigemIDKnd', 'OriSPEDEFDKnd', 'RegisPai'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'], [
  IDSeq1, KndAID, KndNiv,
  DT_INI_AP, DT_FIN_AP, COD_OP_OS,
  COD_ITEM, QTD_COR_POS, QTD_COR_NEG,
  ORIGEM, GraGruX, OriOpeProc,
  OrigemIDKnd, OriSPEDEFDKnd, RegisPai], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True);
end;

function TFmSpedEfdIcmsIpiClaProd.InsereItemAtual_K275(RegOri: String;  MovimID,
  Codigo, MovimCod, Controle, GraGruX, IDSeq1: Integer; Qtde: Double; COD_INS_SUBST:
  String; SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem; ESTSTabSorc:
  TEstqSPEDTabSorc; OrigemOpeProc: TOrigemOpeProc; OriIDKnd: TOrigemIDKnd;
  const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik275';
  REG      = 'K275';
  ValAntes = 0;
  TabSorc  = TEstqSPEDTabSorc.estsVMI;
var
  COD_ITEM, ORIGEM, RegisPai: String;
  ImporExpor, AnoMes, Empresa, PeriApu, IDSeq2, KndTab, KndCod, KndNSU, KndItm,
  KndAID, KndNiv, OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd: Integer;
  QTD_COR_POS, QTD_COR_NEG: Double;
  SQLType: TSQLType;
  //
  ValNovo: Double;
begin
  SQLType        := stNil;
  ImporExpor     := FImporExpor;
  AnoMes         := FAnoMes;
  Empresa        := FEmpresa;
  PeriApu        := FPeriApu;
  //IDSeq1         := ;
  IDSeq2         := 0;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //ID_SEK         := ;
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD_COR_POS    := 0;
  QTD_COR_NEG    := 0;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  //ESTSTabSorc    := ;
  ORIGEM         := Geral.FF0(Integer(SPED_EFD_KndRegOrigem));
  OriOpeProc     := Integer(OrigemOpeProc);
  OrigemIDKnd    := Integer(OriIDKnd);
  OriSPEDEFDKnd  := Integer(OrigemSPEDEFDKnd);
  RegisPai       := RegOri;
  ValNovo := Qtde;
  EfdIcmsIpi_PF.DefineQuantidadePositivaOuNegativa(RegOri, REG, ValAntes, ValNovo,
    caflpnMovNormEsquecido, QTD_COR_POS, QTD_COR_NEG);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
  'SELECT * ',
  'FROM efdicmsipik275 ',
  'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
  'AND AnoMes=' + Geral.FF0(FAnoMes),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND PeriApu=' + Geral.FF0(PeriApu),
  'AND KndTab=' + Geral.FF0(KndTab),
  'AND KndCod=' + Geral.FF0(KndCod),
  'AND KndNSU=' + Geral.FF0(KndNSU),
  'AND KndItm=' + Geral.FF0(KndItm),
  '']);
  IDSeq2 := 0;
  if DmProd.QrX999.RecordCount > 0 then
  begin
    MeAviso.Lines.Add('Item j� importado > ID: ' + Geral.FF0(KndItm) +
    ' Reduzido: ' + Geral.FF0(GraGruX));
    Exit;
  end else
  begin
    IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
    'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
    ImporExpor, AnoMes, Empresa, PeriApu],
    stIns, IDSeq2, siPositivo, nil);
    //
    SQLType := stIns;
  end;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
  'IDSeq1', 'IDSeq2', 'KndAID',
  'KndNiv', 'ID_SEK', 'COD_ITEM',
  'QTD_COR_POS', 'QTD_COR_NEG', 'COD_INS_SUBST',
  'GraGruX', 'ESTSTabSorc', 'ORIGEM',
  'OriOpeProc', 'OrigemIDKnd', 'OriSPEDEFDKnd',
  'RegisPai'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'], [
  IDSeq1, IDSeq2, KndAID,
  KndNiv, ID_SEK, COD_ITEM,
  QTD_COR_POS, QTD_COR_NEG, COD_INS_SUBST,
  GraGruX, ESTSTabSorc, ORIGEM,
  OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd,
  RegisPai], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True);
end;
}

{
function TFmSpedEfdIcmsIpiClaProd.Insere_EmOpeProc_215(const TipoRegSPEDProd:
  TTipoRegSPEDProd; const MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv;
  const QrCab: TmySQLQuery; (*const Fator: Double;*) const SQL_Periodo: String;
  const OrigemOpeProc: TOrigemOpeProc; var IDSeq1: Integer): Boolean;
const
  ESTSTabSorc = estsVMI;
  sProcName = 'FmSPED_EFD_K2XX.Insere_OrigeProc_215';
var
  DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
  GraGruX, GGXCabDst, ID_Item, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer;
  MyQtd, Qtde, Fator: Double;
  COD_INS_SUBST, TabCab: String;
  //
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp21X: Result := SQL_Periodo;
      //trsp23X: Result := SQL_Periodo;
      //trsp25X: Result := SQL_Periodo;
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (3)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
begin
  Result := True;
  //TabCab := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT cab.GraGruX GGXSrc, cab.GGXDst,  ',
  'vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.GraGruX, vmi.SrcNivel2,  ',
  'SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,   ',
  'SUM(AreaM2) AreaM2, DATE(DataHora) DATA, vmi.DtCorrApo, ',
      VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
      'SUM(AreaM2)', 'SUM(PesoKg)', 'SUM(Pecas)'),
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
  'LEFT JOIN vsopecab   cab ON cab.MovimCod=vmi.MovimCod  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  //'LEFT JOIN gragrux ggx ON ggx.Controle=cab.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrCab.FieldByName('MovimCod').AsInteger),
  SQL_Periodo,
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  'GROUP BY vmi.Controle ',
  //'GROUP BY DATA, GGXSrc ',
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT cab.GraGruX GGXSrc, cab.GGXDst,  ',
  'vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.GraGruX, vmi.SrcNivel2,  ',
  'Pecas, PesoKg, AreaM2, DATE(DataHora) DATA, vmi.DtCorrApo, ',
      VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
      'AreaM2', 'PesoKg', 'Pecas'),
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
  'LEFT JOIN vsopecab   cab ON cab.MovimCod=vmi.MovimCod  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  //'LEFT JOIN gragrux ggx ON ggx.Controle=cab.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrCab.FieldByName('MovimCod').AsInteger),
  SQL_Periodo,
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  while not QrOri.Eof do
  begin
    Codigo    := QrOri.FieldByName('Codigo').AsInteger;
    MovimCod  := QrOri.FieldByName('MovimCod').AsInteger;
    Controle  := QrOri.FieldByName('Controle').AsInteger;
    ID_Item   := Controle;
    //
    Fator         := 1;
    Data          := QrOri.FieldByName('DATA').AsDateTime;
    GGXCabDst     := QrOri.FieldByName('GGXDst').AsInteger;
    DtMovim       := Data;
    DtCorrApo     := QrOri.FieldByName('DtCorrApo').AsDateTime;
    ClientMO      := QrOri.FieldByName('ClientMO').AsInteger;
    FornecMO      := QrOri.FieldByName('FornecMO').AsInteger;
    EntiSitio     := QrOri.FieldByName('EntiSitio').AsInteger;
    if MovimID = emidEmProcCur then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrCalToCur, Dmod.MyDB, [
      'SELECT DstGGX ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(QrOri.FieldByName('SrcNivel2').AsInteger),
      '']);
      GraGruX := QrCalToCurDstGGX.Value;
    end
    else
      GraGruX     := QrOri.FieldByName('GraGruX').AsInteger;
    COD_INS_SUBST := '';
    case MovimNiv of
      //eminEmOperBxa,
      //eminEmWEndBxa,
      eminDestOper:
      begin
        Fator := 1;
        if GGXCabDst <> GraGruX then
          if GGXCabDst <> 0 then
            COD_INS_SUBST := Geral.FF0(GGXCabDst);
      end;
(*
      eminDestCal,
      eminDestCur:
      begin
       COD_INS_SUBST := ''; // Nada??? Eh o GraGruX!
       Fator := ?;
      end;
*)
      else
      begin
        Result := False;
        Geral.MB_Erro('"MovimNiv": ' + Geral.FF0(Integer(MovimNiv)) +
        ' n�o implemetado em "Insere_OrigeProc()" (1)');
      end;
    end;
    //qual usar? area ou peso?
    MyQtd := 0;
    case  QrOri.FieldByName('Grandeza').AsInteger of
      0: MyQtd := QrOri.FieldByName('Pecas').AsFloat;
      1: MyQtd := QrOri.FieldByName('AreaM2').AsFloat;
      2: MyQtd := QrOri.FieldByName('PesoKg').AsFloat;
      else
      begin
        Geral.MB_Aviso('Grandeza n�o implementada: ' +
        sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak);
      end;
    end;
    //
    Qtde := MyQtd * Fator;
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (5)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
      '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
      'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) +
      sLineBreak + 'Avise a DERMATEK!!!');
      ID_Item := QrOri.FieldByName('Controle').AsInteger;
    if Qtde < 0.001 then
      Geral.MB_ERRO('Quantidade n�o pode ser zero!!! (3)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak + sLineBreak +
      'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
      '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
      'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    //
    case TipoRegSPEDProd of
      trsp21X:
      begin
        InsereItemAtual_K215((*LinArqPai*)IDSeq1, Data, DtMovim, DtCorrApo, GraGruX,
        Qtde, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
        Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210ProcVS,
        ESTSTabSorc);
      end;
      else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(6)');
    end;
    //
    QrOri.Next;
  end;
end;
}

{
procedure TFmSpedEfdIcmsIpiClaProd.Insere_EmOpeProc_Prd(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  const Qry: TmySQLQuery; const Fator: Double; const SQL_Periodo, SQL_PeriodoPQ:
  String; const OrigemOpeProc: TOrigemOpeProc; const MovimNiv: TEstqMovimNiv);
  //
  procedure Insere_EmOpeProc(const TipoRegSPEDProd: TTipoRegSPEDProd; const
            MovimID: TEstqMovimID; const Qry: TmySQLQuery; const Fator:
            Double; const SQL_Periodo, SQL_CorrApo: String;
            const OrigemOpeProc: TOrigemOpeProc;
            const InsumoSemProducao: Boolean; var IDSeq1: Integer);
  const
    sProcName = 'FmSPED_EFD_K2XX.Insere_EmOpeProc_Prd > Insere_EmOpeProc()';
  var
    DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd, DtCorrApoCons: TDateTime;
    QtdeProd, QtdeCons, Pecas, PesoKg, AreaM2: Double;
    MyFator, TipoEstq, Codigo, MovimCod, GGXDst, ClientMO, FornecMO,
    EntiSitio: Integer; ESTSTabSorc: TEstqSPEDTabSorc; ID_Item, Controle: Integer;
    //
    procedure Mensagem(TextoBase: String);
    begin
      Geral.MB_ERRO(TextoBase + sLineBreak +
      'MovimCod: ' + Geral.FF0(MovimCod) + sLineBreak +
      'MovimID: ' + Geral.FF0(Integer(MovimID)) + sLineBreak +
      sProcName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!' + sLineBreak +
      QrProdParc.SQL.Text);
    end;

    function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
    begin
      case TipoRegSPEDProd of
        trsp21X: Result := '';
        trsp23X: Result := SQL_Periodo;
        //trsp25X: Result := '';
        trsp25X: Result := SQL_Periodo;
        trsp26X: Result := SQL_Periodo;
        else
        begin
          Geral.MB_Erro('"FiltroReg" indefinido: ' +
            Geral.FF0(Integer(TipoRegSPEDProd)) + '! (4)');
          Result := '#ERR_PERIODO_';
        end;
      end;
    end;

    procedure InsereAtual_2X0();
    begin
      DtMovim    := FDiaIni; // ver aqui quando periodo nao for mensal!
      //Parei aqui !!!  � asimm???
      if (Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          1: QtdeProd := AreaM2;
          2: QtdeProd := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            Qry.SQL.Text);
            //
            QtdeProd := Pecas;
          end;
        end;
        QtdeProd := QtdeProd * MyFator;
      end else
        QtdeProd := 0;
      //
      if QtdeProd < 0 then
        Mensagem('Quantidade n�o pode ser negativa!!! (8)' + sLineBreak +
        'Qtde: ' + Geral.FFT(QtdeProd, 3, siNegativo))
      else if (QtdeProd = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidEmProcCal: DtHrFimOpe := 0;
          emidEmProcCur: DtHrFimOpe := 0;
          else Mensagem('Quantidade zerada para OP encerrada!!! (6)');
        end;
      end;
      //
      case TipoRegSPEDProd of
    (*
        trsp21X:
        begin
      ESTSTabSorc := TEstqSPEDTabSorc.estsND;
      ID_Item     := 0;
      Controle    := 0;
          InsereItemAtual_K215(LinArqPai, DtHrAberto(*Data*), GGXDst(*GraGruX*),
          Qtde, ''(*COD_INS_SUBST*), ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ESTSTabSorc);
          //
        end;
    *)
        trsp23X:
        begin
          InsereItemAtual_K230(Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd, GGXDst, ClientMO,
          FornecMO, EntiSitio, QtdeProd, OrigemOpeProc, IDSeq1);
        end;
        trsp25X:
        begin
          InsereItemAtual_K250(Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrFimOpe, DtMovim, DtCorrApoProd, GGXDst, ClientMO, FornecMO,
          EntiSitio, QtdeProd, OrigemOpeProc, IDSeq1);
        end;
        trsp26X:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrConsParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE MovimCod=' + Geral.FF0(MovimCod),
          'AND MovimNiv IN (57) ', //eminEmRRMBxa=57,
          //'AND MovimTwn=' + Geral.FF0(), /
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          'GROUP BY Ano, Mes', // MovimNiv >>> 54 ou 56 para RRM!!!
          '']);
         // Geral.MB_SQL(Self, QrConsParc);
          //
          if QrConsParc.RecordCount > 1 then
            Geral.MB_Erro('ERRO! Avise a Dermatek! QrConsParc.RecordCount > 1!!!');
          //while not QrConsParc.Eof do
          begin
            if QrConsParcAno.Value = 0 then
              DtCorrApoCons := 0
            else
              DtCorrApoCons := EncodeDate(Trunc(QrConsParcAno.Value), Trunc(QrConsParcMes.Value), 1);
            //
            AreaM2 := QrConsParcAreaM2.Value;
            PesoKg := QrConsParcPesoKg.Value;
            Pecas  := QrConsParcPecas.Value;
            //
            //Parei aqui !!!  � asimm???
            if (Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP) (*and (DtHrFimOpe > 1)*) then
            begin
              case TipoEstq of
                1: QtdeCons := AreaM2;
                2: QtdeCons := PesoKg;
                else
                begin
                  Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                  + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                  'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                  'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                  Qry.SQL.Text);
                  //
                  QtdeCons := Pecas;
                end;
              end;
              QtdeCons := QtdeCons * (*MyFator*) -1;
            end else
              QtdeCons := 0;
            //
            if QtdeCons < 0 then
              Mensagem('Quantidade n�o pode ser negativa!!! (9)' + sLineBreak +
              'Qtde: ' + Geral.FFT(QtdeCons, 3, siNegativo))
          end;
          InsereItemAtual_K260(Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd, DtCorrApoCons, GGXDst,
          ClientMO, FornecMO, EntiSitio,
          //CtrlDst, CtrlBxa,
          QtdeProd, QtdeCons, OrigemOpeProc, IDSeq1);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(9)');
      end;
    end;
  begin
    MyFator    := -1;
    QtdeProd   := 0;
    QtdeCons   := 0;
    TipoEstq   := Qry.FieldByName('Tipoestq').AsInteger;
    Codigo     := Qry.FieldByName('Codigo').AsInteger;
    MovimCod   := Qry.FieldByName('MovimCod').AsInteger;
    DtHrAberto := Qry.FieldByName('DtHrAberto').AsDateTime;
    DtHrFimOpe := Qry.FieldByName('DtHrFimOpe').AsDateTime;
    GGXDst     := Qry.FieldByName('GGXDst').AsInteger;
    //
    if InsumoSemProducao then
    begin
      DtCorrApoProd := 0;
      AreaM2 := 0;
      PesoKg := 0;
      Pecas  := 0;
      //
      InsereAtual_2X0();
      //
    end else
    begin
      case MovimID of
        emidEmProcCal:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
(*
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          //'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, ',
          'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, Sum(PesoKg) PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE SrcMovID=' + Geral.FF0(Integer(MovimID)),
          'AND SrcNivel1 <> 0 ',
          'AND SrcNivel1=' + Geral.FF0(Codigo),
          // Entradas no estoque pela producao ( sem doc fiscal de compra, remessa, devolucao, etc)
          'AND MovimID IN (' + CO_ALL_CODS_NO_INN_SPED_VS + ')',
          //SQL_Periodo,
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          //'GROUP BY Ano, Mes, ClientMO, FornecMO ',
          'GROUP BY Controle ',
          '']);
*)
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE SrcMovID=' + Geral.FF0(Integer(MovimID)),
          'AND SrcNivel1 <> 0 ',
          'AND SrcNivel1=' + Geral.FF0(Codigo),
          // Entradas no estoque pela producao ( sem doc fiscal de compra, remessa, devolucao, etc)
          'AND MovimID IN (' + CO_ALL_CODS_NO_INN_SPED_VS + ')',
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          '']);
        end;
        emidEmProcCur:
        begin
          // Igual a emidEmProcCal!!!
          UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
(*
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          //'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, ',
          'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, Sum(PesoKg) PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE SrcMovID=' + Geral.FF0(Integer(MovimID)),
          'AND SrcNivel1 <> 0 ',
          'AND SrcNivel1=' + Geral.FF0(Codigo),
          // Entradas no estoque pela producao ( sem doc fiscal de compra, remessa, devolucao, etc)
          'AND MovimID IN (' + CO_ALL_CODS_NO_INN_SPED_VS + ')',
          //SQL_Periodo,
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          //'GROUP BY Ano, Mes, ClientMO, FornecMO ',
          'GROUP BY Controle ',
          '']);
*)
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE SrcMovID=' + Geral.FF0(Integer(MovimID)),
          'AND SrcNivel1 <> 0 ',
          'AND SrcNivel1=' + Geral.FF0(Codigo),
          // Entradas no estoque pela producao ( sem doc fiscal de compra, remessa, devolucao, etc)
          'AND MovimID IN (' + CO_ALL_CODS_NO_INN_SPED_VS + ')',
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          '']);
        end;
        emidEmOperacao, emidEmProcWE, emidEmProcSP, emidEmReprRM:
        begin
          MyFator := 1;
(*
          UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          //'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, ',
          'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, Sum(PesoKg) PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE MovimCod=' + Geral.FF0(MovimCod),
          //'AND MovimNiv IN (9,22,51,54,56) ', //eminDestOper=9, //eminDestWEnd=22, //eminDestPSP=51,//eminSorcRRM=54,//eminDestRRM=56,
          'AND MovimNiv IN (9,22,51,56) ', //eminDestOper=9, //eminDestWEnd=22, //eminDestPSP=51,//eminDestRRM=56,
          //'WHERE DataHora < "' + Geral.FDT(FDiaPos, 1) + '" ',
          //SQL_Periodo,
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          //'GROUP BY Ano, Mes, ClientMO, FornecMO ',
          'GROUP BY Controle ',
          '']);
*)
          UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
          'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
          'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          'Pecas, AreaM2, PesoKg, ',
          'ClientMO, FornecMO, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE MovimCod=' + Geral.FF0(MovimCod),
          'AND MovimNiv IN (9,22,51,56) ', //eminDestOper=9, //eminDestWEnd=22, //eminDestPSP=51,//eminDestRRM=56,
          FiltroPeriodo(TipoRegSPEDProd),
          SQL_CorrApo,
          '']);
          //Geral.MB_SQL(Self, QrProdParc);
        end;
        else
        begin
          //Result := False;
          Geral.MB_ERRO(
          '"MovimID" n�o implementado em ' + sProcName);
          //Close;
          Exit;
        end;
      end;
      //Geral.MB_SQL(self, QrProdParc);
      //
      QrProdParc.First;
      while not QrProdParc.Eof do
      begin
        //
        if QrProdParcAno.Value = 0 then
          DtCorrApoProd := 0
        else
          DtCorrApoProd := EncodeDate(Trunc(QrProdParcAno.Value), Trunc(QrProdParcMes.Value), 1);
        //
        AreaM2   := QrProdParcAreaM2.Value;
        PesoKg   := QrProdParcPesoKg.Value;
        Pecas    := QrProdParcPecas.Value;
        ClientMO := QrProdParcClientMO.Value;
        FornecMO := QrProdParcFornecMO.Value;
        Controle := QrProdParcControle.Value;
        //
        InsereAtual_2X0();
        //
        QrProdParc.Next;
      end;
    end;
  end;
  //
  function  Insere_OrigeProcVS(TipoRegSPEDProd: TTipoRegSPEDProd; MovimID:
            TEstqMovimID; MovimNiv: TEstqMovimNiv; QrCab: TmySQLQuery;
            IDSeq1: Integer; SQL_PeriodoVS, SQL_CorrApo: String;
            OrigemOpeProc: TOrigemOpeProc; InsumoSemProducao: Boolean): Boolean;
  const
    ESTSTabSorc = estsVMI;
    sProcName = 'FmSPED_EFD_K2XX.Insere_EmOpeProc_Prd > Insere_OrigeProc()';
    //
    procedure InsereAtual_2X5(Data, DtCorrApo: TDateTime; GraGruX: Integer;
    Qtde: Double; COD_INS_SUBST: String; ID_Item, Codigo, Controle, ClientMO,
    FornecMO, EntiSitio, MovimCod: Integer);
    begin
      case TipoRegSPEDProd of
        //trsp21X:
        trsp23X:
        begin
          InsereItemAtual_K235((*LinArqPai*)IDSeq1, Data, DtCorrApo, GraGruX, Qtde,
          COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, ESTSTabSorc);
        end;
        trsp25X:
        begin
          InsereItemAtual_K255((*LinArqPai*)IDSeq1, Data, DtCorrApo, GraGruX, Qtde,
          COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, ESTSTabSorc);
        end;
        trsp26X:
        begin
          //
(*        N�o tem como retornar nada ao estoque, pois a mercadoria foi transfor-
          mada e n�o montada (com um motor por exemplo)!
          //
          InsereItemAtual_K265(LinArqPai, Data, DtCorrApo, GraGruX, QtdeCons,
          QtdeRet, COD_INS_SUBST, ID_Item, MovimID, Codigo, MovimCod, Controle,
          OrigemOpeProc, ESTSTabSorc);
*)
          Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, MovimNiv, MovimCod,
          IDSeq1, SQL_PeriodoPQ, OrigemOpeProc, ClientMO, FornecMO, EntiSitio);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(10)');
      end;
    end;
  var
    Data, DtCorrApo, DtHrAberto, DtHrFimOpe: TDateTime;
    GraGruX, GGXCabDst, ID_Item, Codigo, MovimCod, Controle, ClientMO, FornecMO,
    EntiSitio: Integer;
    MyQtd, Qtde, Fator: Double;
    COD_INS_SUBST, TabCab: String;
    MovimNivInn: TEstqMovimNiv;
    //
  var
    SQL_PeriodoApo: String;
    DtCorrIni, DtCorrFim: TDateTime;
  begin
    Result := True;
    TabCab := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
    'SELECT cab.GraGruX GGXSrc, cab.GGXDst,  ',
    'vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.GraGruX, vmi.SrcNivel2,  ',
    //'SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, SUM(AreaM2) AreaM2, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(DataHora) DATA, vmi.DtCorrApo, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
    'med.Grandeza  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
    'LEFT JOIN vsopecab  cab ON cab.MovimCod=vmi.MovimCod  ',
    'LEFT JOIN gragrux   ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed   med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.MovimCod=' + Geral.FF0(QrCab.FieldByName('MovimCod').AsInteger),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    SQL_PeriodoVS,
    SQL_CorrApo,
    //'GROUP BY vmi.Controle ', // Sem Soma!
    '']);
    //Geral.MB_SQL(Self, QrOri);
    if InsumoSemProducao then
    begin
      //Geral.MB_SQL(Self, QrCab);
      case TipoRegSPEDProd of
        //trsp21X:
        trsp23X,
        //trsp25X:
        trsp26X:
        begin
          Data      := 0;
          DtCorrApo := 0;
          GraGruX   := QrCab.FieldByName('GraGruX').AsInteger;
          Qtde      := 0;
          COD_INS_SUBST := '';
          ID_Item   := 0;
          Codigo    := QrCab.FieldByName('Codigo').AsInteger;
          Controle  := 0;
          MovimCod  := QrCab.FieldByName('MovimCod').AsInteger;
          // N�o testado!!
          MovimNivInn := VS_PF.ObtemMovimNivInnDeMovimID(MovimID);
          UnDmkDAC_PF.AbreMySQLQuery0(QrObtCliForSite, Dmod.MyDB, [
          'SELECT vmi.ClientMO, vmi.FornecMO, scc.EntiSitio  ',
          'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
          'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc  ',
          'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo  ',
          'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
          'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
          '']);
          ClientMO  := QrObtCliForSite.FieldByName('ClientMO').AsInteger;
          FornecMO  := QrObtCliForSite.FieldByName('FornecMO').AsInteger;
          EntiSitio  := QrObtCliForSite.FieldByName('EntiSitio').AsInteger;
          //
          InsereAtual_2X5(Data, DtCorrApo, GraGruX, Qtde, COD_INS_SUBST,
          ID_Item, Codigo, Controle, ClientMO, FornecMO, EntiSitio, MovimCod);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(11)');
      end;
    end else
    begin
      if QrOri.RecordCount > 0 then
      begin
        QrOri.First;
        while not QrOri.Eof do
        begin
          Codigo    := QrOri.FieldByName('Codigo').AsInteger;
          MovimCod  := QrOri.FieldByName('MovimCod').AsInteger;
          Controle  := QrOri.FieldByName('Controle').AsInteger;
          ID_Item   := Controle;
          //
          Fator         := 1;
          Data          := QrOri.FieldByName('DATA').AsDateTime;
          DtCorrApo     := QrOri.FieldByName('DtCorrApo').AsDateTime;
          GGXCabDst     := QrOri.FieldByName('GGXDst').AsInteger;
          // J� testado!!
          ClientMO      := QrOri.FieldByName('ClientMO').AsInteger;
          FornecMO      := QrOri.FieldByName('FornecMO').AsInteger;
          if MovimID = emidEmProcCur then
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(QrCalToCur, Dmod.MyDB, [
            'SELECT DstGGX ',
            'FROM ' + CO_SEL_TAB_VMI,
            'WHERE Controle=' + Geral.FF0(QrOri.FieldByName('SrcNivel2').AsInteger),
            '']);
            GraGruX := QrCalToCurDstGGX.Value;
          end
          else
            GraGruX     := QrOri.FieldByName('GraGruX').AsInteger;
          COD_INS_SUBST := '';
          case MovimNiv of
            eminEmOperBxa,
            eminEmWEndBxa,
            eminEmPSPBxa:
            begin
              Fator := -1;
              if GGXCabDst <> GraGruX then
                if GGXCabDst <> 0 then
                  COD_INS_SUBST := Geral.FF0(GGXCabDst);
            end;
            eminEmCalBxa,
            eminEmCurBxa:
            begin
             COD_INS_SUBST := ''; // Nada??? Eh o GraGruX!
             Fator := -1;
            end;
            eminEmRRMBxa:
            begin
             COD_INS_SUBST := ''; // N�o existe no manual
             Fator := -1;
            end;
            else
            begin
              Result := False;
              Geral.MB_Erro('"MovimNiv": ' + Geral.FF0(Integer(MovimNiv)) +
              ' n�o implemetado em "Insere_OrigeProc()" (3)');
            end;
          end;
          //qual usar? area ou peso?
          MyQtd := 0;
          case  QrOri.FieldByName('Grandeza').AsInteger of
            0: MyQtd := QrOri.FieldByName('Pecas').AsFloat;
            1: MyQtd := QrOri.FieldByName('AreaM2').AsFloat;
            2: MyQtd := QrOri.FieldByName('PesoKg').AsFloat;
            else
            begin
              Geral.MB_Aviso('Grandeza n�o implementada: ' +
              sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
            'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak);
            end;
          end;
          //
          Qtde := MyQtd * Fator;
          if Qtde < 0 then
            Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (10)' + sLineBreak +
            'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
            sprocName + sLineBreak + sLineBreak +
            'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
            'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
            'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
            '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
            'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
            'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
            Geral.FFT(Fator, 3, siNegativo) + ' = ' +
            Geral.FFT(Qtde, 3, siNegativo) +
            sLineBreak + 'Avise a DERMATEK!!!');
            ID_Item := QrOri.FieldByName('Controle').AsInteger;
          if Qtde < 0.001 then
            Geral.MB_ERRO('Quantidade n�o pode ser zero!!! (3)' + sLineBreak +
            'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak + sLineBreak +
            'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
            'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
            'Pe�as: ' + Geral.FFT(QrOri.FieldByName('Pecas').AsFloat, 3, siNegativo) + sLineBreak +
            '�rea m�: ' + Geral.FFT(QrOri.FieldByName('AreaM2').AsFloat, 3, siNegativo) + sLineBreak +
            'Peso kg: ' + Geral.FFT(QrOri.FieldByName('PesoKg').AsFloat, 3, siNegativo) + sLineBreak + sLineBreak +
            'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
            Geral.FFT(Fator, 3, siNegativo) + ' = ' +
            Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
            sprocName + sLineBreak +
            sLineBreak + 'Avise a DERMATEK!!!');
            //
            //InsereAtual_2X5();
            InsereAtual_2X5(Data, DtCorrApo, GraGruX, Qtde, COD_INS_SUBST,
            ID_Item, Codigo, Controle, ClientMO, FornecMO, EntiSitio, MovimCod);
          //
          QrOri.Next;
        end;
      end else
      // ver se gastou insumo sem produzir produto acabado!
      begin
        // ?!
      end;
    end;
  end;
  //
  var
    SQL_PeriodoApo: String;
    DtCorrIni, DtCorrFim: TDateTime;
  procedure InsereAtualOpeProc_Prd(DtApoIni, DtApoFim: TDateTime);
  var
    SQL_PeriodoApo: String;
    IDSeq1: Integer;
    InsumoSemProducao: Boolean;
  begin
    SQL_PeriodoApo := dmkPF.SQL_Periodo('AND DtCorrApo ', DtApoIni, DtApoFim,
      True, True);
    //
    IDSeq1 := 0;
    InsumoSemProducao := False;
    Insere_EmOpeProc(TipoRegSPEDProd, MovimID, Qry, Fator,
    SQL_Periodo, SQL_PeriodoApo, OrigemOpeProc, InsumoSemProducao, IDSeq1);
    //
    InsumoSemProducao := IDSeq1 = 0;
    if InsumoSemProducao then
    begin
      // CUIDADO!!! Aqui esta duplicando!!!
      //Geral.MB_Info(Geral.FF0(Qry.FieldByName('MovimCod').AsInteger));
      ReopenOriPQx(Qry.FieldByName('MovimCod').AsInteger, SQL_PeriodoPQ, False);
      InsumoSemProducao := QrOri.RecordCount > 0;
      if InsumoSemProducao then
      begin
        //Geral.MB_Info(Geral.FF0(Qry.FieldByName('MovimCod').AsInteger));
        //
        Insere_EmOpeProc(TipoRegSPEDProd, MovimID, Qry, Fator, SQL_Periodo,
        SQL_PeriodoApo, OrigemOpeProc, InsumoSemProducao, IDSeq1);
      end;
      //
    end;
    Insere_OrigeProcVS(TipoRegSPEDProd, MovimID, MovimNiv, Qry, IDSeq1,
    SQL_Periodo, SQL_PeriodoApo, OrigemOpeProc, InsumoSemProducao);
    // Como fazer??
    //Insere_OrigeProcPQ(TipoRegSPEDProd, MovimID, TEstqMovimNiv.eminEmWEndBxa, MovimCod, LinArqPai, SQL_PeriodoPQ);
  end;
begin
 // Criar query que separa periodos + Periodos de correcao!
   UnDmkDAC_PF.AbreMySQLQuery0(QrPeriodos, Dmod.MyDB, [
  'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
  'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          //'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, ',
   EfdIcmsIpi_PF.SQLPeriodoFiscal(
     FTipoPeriodoFiscal, 'DtCorrApo', 'Periodo', False),
   'FROM ' + CO_TAB_VMI,
   'WHERE MovimCod>0 ',
   'AND DtCorrApo > 0 ',
   'GROUP BY Ano, Mes, Periodo ',
   '']);
   //
   DtCorrIni  := 0;
   DtCorrFim  := 0;
   InsereAtualOpeProc_Prd(0, 0); // Registro normal sem Correcao de apontamento!
   QrPeriodos.First;
   while not QrPeriodos.Eof do
   begin
     EfdIcmsIpi_PF.ObtemDatasDePeriodoSPED(Trunc(QrPeriodosAno.Value),
     Trunc(QrPeriodosMes.Value), QrPeriodosPeriodo.Value, FTipoPeriodoFiscal,
     DtCorrIni, DtCorrFim);
     // Registros com Correcao de apontamento!
     InsereAtualOpeProc_Prd(DtCorrIni, DtCorrFim);
     QrPeriodos.Next
   end;
end;
}

{
function TFmSpedEfdIcmsIpiClaProd.Insere_OrigeProcPQ(TipoRegSPEDProd: TTipoRegSPEDProd;
  MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv; MovimCod: Integer;
  IDSeq1: Integer; SQL_PeriodoPQ: String; OrigemOpeProc: TOrigemOpeProc;
  ClientMO, FornecMO, EntiSitio: Integer): Boolean;
const
  Fator = -1;
  ESTSTabSorc = estsPQx;
  sProcName = 'FmSPED_EFD_K2XX.Insere_OrigeProcPQ';
var
  Data, DtCorrApo: TDateTime;
  Codigo, Controle, ID_Item, GraGruX(*, ClientMO, FornecMO, EntiSitio*): Integer;
  MyQtd, Qtde: Double;
  //
  COD_INS_SUBST: String;
  //
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp23X: Result := SQL_PeriodoPQ;
      trsp25X: Result := '';
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (6)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
begin
  Result := True;
  ReopenOriPQx(MovimCod, SQL_PeriodoPQ, False);
  //
  //Geral.MB_SQL(Self, QrOri); //Parei Aqui!
  QrOri.First;
  while not QrOri.Eof do
  begin
    COD_INS_SUBST := '';
    Codigo   := QrOri.FieldByName('OrigemCodi').AsInteger;
    Controle := QrOri.FieldByName('OrigemCtrl').AsInteger;
    GraGruX  := UnPQx.ObtemGGXdeInsumo(QrOri.FieldByName('Insumo').AsInteger);
    //qual usar? area ou peso?
    MyQtd     := QrOri.FieldByName('Peso').AsFloat;
    Qtde      := MyQtd * Fator;
    Data      := QrOri.FieldByName('DataX').AsDateTime;
    DtCorrApo := QrOri.FieldByName('DtCorrApo').AsDateTime;
    ID_Item   := QrOri.FieldByName('OrigemCtrl').AsInteger;
    //
    if Qtde < 0 then
      Geral.MB_ERRO('Quantidade n�o pode ser negativa!!! (12)' + sLineBreak +
      'MovimNiv: ' + Geral.FF0(Integer(MovimNiv)) + sLineBreak +
      sprocName + sLineBreak + sLineBreak +
      //'Grandeza: ' + sGrandezaUnidMed[QrOri.FieldByName('Grandeza').AsInteger] + sLineBreak +
      'ID Item: ' + Geral.FF0(ID_Item) + sLineBreak +
      'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
      'Peso: ' + Geral.FFT(MyQtd, 3, siNegativo) + sLineBreak + sLineBreak +
      'Qtde: ' + Geral.FFT(MyQtd, 3, siNegativo) + ' * ' +
      Geral.FFT(Fator, 3, siNegativo) + ' = ' +
      Geral.FFT(Qtde, 3, siNegativo) + sLineBreak +
      sprocName + sLineBreak +
      sLineBreak + 'Avise a DERMATEK!!!');
    if Qtde > 0 then
    begin
      case TipoRegSPEDProd of
        //trsp21X:
        trsp23X:
        begin
          InsereItemAtual_K235((*LinArqPai*)IDSeq1, Data, DtCorrApo, GraGruX, Qtde,
          COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod, Controle,
          ClientMO, FornecMO, EntiSitio, OrigemOpeProc, ESTSTabSorc);
        end;
        trsp25X:
        begin
          InsereItemAtual_K255((*LinArqPai*)IDSeq1, Data, DtCorrApo, GraGruX,
          Qtde, COD_INS_SUBST, ID_Item,
          Integer(MovimID), Codigo, MovimCod, Controle, ClientMO, FornecMO,
          EntiSitio, OrigemOpeProc, ESTSTabSorc);
        end;
        trsp26X:
        begin
          InsereItemAtual_K265(IDSeq1, Data, DtCorrApo, GraGruX,
          (*QtdeCons*) Qtde, (*QtdeRet*)0, COD_INS_SUBST, ID_Item,
          Integer(MovimID), Codigo, MovimCod, Controle, OrigemOpeProc,
          ESTSTabSorc);
        end;
        else
          Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
          TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(14)');
      end;
    end;
    //
    QrOri.Next;
  end;
end;
}

{
function TFmSpedEfdIcmsIpiClaProd.Insere_OrigeProcVS_210(const TipoRegSPEDProd:
  TTipoRegSPEDProd; const MovimID: TEstqMovimID; const MovimNivInn,
  MovimNivBxa: TEstqMovimNiv; const QrCab: TmySQLQuery; var IDSeq1: Integer;
  const SQL_PeriodoVS: String; const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  ESTSTabSorc = estsVMI;
  sProcName = 'FmSPED_EFD_K2XX.Insere_OrigeProcVS_210';
var
  DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  Qtde, Pecas, PesoKg, AreaM2: Double;
  MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, ClientMO, FornecMO, EntiSitio,
  ID_Item, Controle: Integer;
  //
  procedure Mensagem(TextoBase: String);
  begin
    Geral.MB_ERRO(TextoBase + sLineBreak +
    'MovimCod: ' + Geral.FF0(MovimCod) + sLineBreak +
    'MovimID: ' + Geral.FF0(Integer(MovimID)) + sLineBreak +
    sProcName + sLineBreak +
    sLineBreak + 'Avise a DERMATEK!!!' + sLineBreak +
    QrProdParc.SQL.Text);
  end;

  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp21X: Result := SQL_PeriodoVS;
      //trsp23X: Result := SQL_PeriodoVS;
      //trsp25X: Result := SQL_PeriodoVS;
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (7)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
begin
  Result := True;
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT cab.GraGruX GGXSrc, cab.GGXDst, ',
  'vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.GraGruX, vmi.SrcNivel2,  ',
  //'SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, SUM(AreaM2) AreaM2, ',
  'DATE(DataHora) DATA, vmi.DtCorrApo, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, ',
      VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
      //'SUM(AreaM2)', 'SUM(PesoKg)', 'SUM(Pecas)'),
      'AreaM2', 'PesoKg', 'Pecas'),
  'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
  'LEFT JOIN vsopecab   cab ON cab.MovimCod=vmi.MovimCod  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=cab.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrCab.FieldByName('MovimCod').AsInteger),
  SQL_PeriodoVS,
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
  'GROUP BY DATA, Ano, Mes, GGXSrc ',
  '']);
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  while not QrOri.Eof do
  begin
    MyFator := -1;
    Qtde := 0;
    TipoEstq   := QrOri.FieldByName('Tipoestq').AsInteger;
    Codigo     := QrCab.FieldByName('Codigo').AsInteger;
    MovimCod   := QrCab.FieldByName('MovimCod').AsInteger;
    DtHrAberto := QrCab.FieldByName('DtHrAberto').AsDateTime;
    DtHrFimOpe := QrCab.FieldByName('DtHrFimOpe').AsDateTime;
    GGXSrc     := QrOri.FieldByName('GGXSrc').AsInteger;
    ClientMO   := QrOri.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrOri.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrOri.FieldByName('EntiSitio').AsInteger;
    //
    DtMovim    := QrOri.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrOri.FieldByName('DtCorrApo').AsDateTime;
    case MovimID of
      //emidEmProcCal:
      //emidEmProcCur:
      emidEmOperacao:
      begin
        MyFator := 1;
(*
        UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
        'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
        'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
          //'SELECT YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, ',
        'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, Sum(PesoKg) PesoKg, ',
        'ClientMO, FornecMO, Controle ',
        'FROM ' + CO_SEL_TAB_VMI + ' ',
        'WHERE MovimCod=' + Geral.FF0(MovimCod),
        'AND MovimNiv=' + Geral.FF0(Integer(MovimNivBxa)),
        //'AND MovimNiv IN (9,22,51) ', //eminDestOper=9, //eminDestWEnd=22,
        //'WHERE DataHora < "' + Geral.FDT(FDiaPos, 1) + '" ',
        //SQL_Periodo,
        FiltroPeriodo(TipoRegSPEDProd),
        //'GROUP BY Ano, Mes, ClientMO, FornecMO ',
        'GROUP BY Controle ',
        '']);
*)
        UnDmkDAC_PF.AbreMySQLQuery0(QrProdParc, Dmod.MyDB, [
        'SELECT CAST(YEAR(DtCorrApo) AS DECIMAL(11,0)) Ano, ',
        'CAST(MONTH(DtCorrApo) AS DECIMAL(11,0)) Mes, ',
        'Pecas, AreaM2, PesoKg, ',
        'ClientMO, FornecMO, Controle ',
        'FROM ' + CO_SEL_TAB_VMI + ' ',
        'WHERE MovimCod=' + Geral.FF0(MovimCod),
        'AND MovimNiv=' + Geral.FF0(Integer(MovimNivBxa)),
        FiltroPeriodo(TipoRegSPEDProd),
        '']);
        //Geral.MB_SQL(Self, QrProdParc);
      end;
      //emidEmProcWE:
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //Geral.MB_SQL(self, QrProdParc);
    //
    QrProdParc.First;
    while not QrProdParc.Eof do
    begin
      AreaM2 := -QrProdParcAreaM2.Value;
      PesoKg := -QrProdParcPesoKg.Value;
      Pecas  := -QrProdParcPecas.Value;
      Controle := QrProdParcControle.Value;
      //
      //Parei aqui !!!  � asimm???
      if ((Pecas <> 0) or (MovimID=TEstqMovimID.emidEmProcSP)) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          1: Qtde := AreaM2;
          2: Qtde := PesoKg;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrOri.SQL.Text);
            //
            Qtde := Pecas;
          end;
        end;
        Qtde := Qtde * MyFator;
      end else
        Qtde := 0;
      //
      if Qtde < 0 then
        Mensagem('Quantidade n�o pode ser negativa!!! (13)' + sLineBreak +
        'Qtde: ' + Geral.FFT(Qtde, 3, siNegativo))
      else if (Qtde = 0) and (DtHrFimOpe > 1) then
      begin
        case MovimID of
          emidEmProcCal: DtHrFimOpe := 0;
          emidEmProcCur: DtHrFimOpe := 0;
          else Mensagem('Quantidade zerada para OP encerrada!!! (7)');
        end;
      end;
      //
      case TipoRegSPEDProd of
        trsp21X:
        begin
          InsereItemAtual_K210(Integer(MovimID), Codigo, MovimCod, Controle,
          DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO,
          FornecMO, EntiSitio, Qtde, OrigemOpeProc, oidk210ProcVS, IDSeq1);
        end;
  (*
        trsp23X:
        begin
          InsereItemAtual_K230(Integer(MovimID), Codigo, MovimCod, DtHrAberto,
          DtHrFimOpe, GGXDst, Qtde, LinArqPai);
        end;
        trsp25X:
        begin
          InsereItemAtual_K250(Integer(MovimID), Codigo, MovimCod, DtHrFimOpe,
          GGXDst, Qtde, LinArqPai);
        end;
        else
        Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
        TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(15)');
  *)
      end;
      QrProdParc.Next;
    end;
    //
    QrOri.Next;
  end;
end;
}

{
function TFmSpedEfdIcmsIpiClaProd.ProducaoIsolada_Insere_OrigeRibDTA_210(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  const MovimNiv: TEstqMovimNiv; const SQL_PeriodoVS: String;
  const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  COD_INS_SUBST = '';
  ESTSTabSorc = estsVMI;
  sProcName = 'FmSPED_EFD_K2XX.ProducaoIsolada_Insere_OrigeRibDTA_210';
  //
  procedure ObtemDataIniEFim(const Codigo: Integer; var Ini, Fim: TDateTime);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDatas, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _PRE_DESC_ORI_MP_; ',
    'CREATE TABLE _PRE_DESC_ORI_MP_ ',
    'SELECT Controle  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND Codigo=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _PRE_DESC_DTIF_; ',
    ' ',
    'CREATE TABLE _PRE_DESC_DTIF_ ',
    'SELECT vmi.Controle, vmi.DataHora ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND vmi.SrcNivel1=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'INSERT IGNORE INTO _PRE_DESC_DTIF_ ',
    ' ',
    'SELECT vmi.Controle, vmi.DataHora  ',
    'FROM _PRE_DESC_ORI_MP_ ori  ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    '  ON ori.Controle=vmi.GSPJmpNiv2; ',
    ' ',
    'SELECT MIN(DataHora) MinDH,  ',
    'MAX(DataHora) MaxDH  ',
    'FROM _PRE_DESC_DTIF_ ',
    'WHERE Controle > 0 ',
    '']);
    //Geral.MB_SQL(Self, QrDatas);
    //
    Ini := QrDatasMinDH.Value;
    Fim := QrDatasMaxDH.Value;
  end;
  //
  function RegistroK210(const OriMovID: TEstqMovimID; const DtIniOS, DtFimOS:
  TDateTime; var IDSeq1: Integer): Boolean;
  var
  DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
  QtdeSrc, QtdeDst, PecasSrc, PesoKgSrc, AreaM2Src, PecasDst, PesoKgDst,
  AreaM2Dst: Double;
  MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, GGXDst, ID_Item, Controle,
  ClientMO, FornecMO, EntiSitio: Integer;
  EhDoMes: Boolean;
  procedure Mensagem(TextoBase: String);
  begin
    Geral.MB_ERRO(TextoBase + sLineBreak +
    'MovimCod: ' + Geral.FF0(MovimCod) + sLineBreak +
    'MovimID: ' + Geral.FF0(Integer(MovimID)) + sLineBreak +
    sProcName + sLineBreak +
    sLineBreak + 'Avise a DERMATEK!!!' + sLineBreak +
    QrProdParc.SQL.Text);
  end;
  begin
    IDSeq1     := 0;
    AreaM2Src  := 0;
    PesoKgSrc  := 0;
    PecasSrc   := 0;
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeSrc    := 0;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    DtHrAberto := DtIniOS;
    DtHrFimOpe := DtFimOS;
    GGXSrc     := QrIts.FieldByName('GGXSrc').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GGXDst').AsInteger;
    Controle   := QrIts.FieldByName('RmsNivel2').AsInteger;
    ID_Item    := Controle;
    // 270
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibDTA:
      begin
        AreaM2Src := -QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgSrc := -QrIts.FieldByName('PesoKg').AsFloat;
        PecasSrc  := -QrIts.FieldByName('Pecas').AsFloat;
        //
(*
        AreaM2Src := QrIts.FieldByName('QtdAntArM2').AsFloat;
        PesoKgSrc := QrIts.FieldByName('QtdAntPeso').AsFloat;
        PecasSrc  := QrIts.FieldByName('QtdAntPeca').AsFloat;
*)
        //
        AreaM2Dst := QrIts.FieldByName('QtdAntArM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('QtdAntPeso').AsFloat;
        PecasDst  := QrIts.FieldByName('QtdAntPeca').AsFloat;
      end;
      else
      begin
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        Exit;
      end;
    end;
    //
    EhDoMes := (Data >= FDiaIni) and (Data < FDiaPos);
    if EhDoMes then
    begin
      if (PecasSrc <> 0) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          1: QtdeSrc := AreaM2Src;
          2: QtdeSrc := PesoKgSrc;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrIts.SQL.Text);
            //
            QtdeSrc := PecasSrc;
          end;
        end;
        QtdeSrc := QtdeSrc * MyFator;
      end else
        QtdeSrc := 0;
    end else
      QtdeSrc := 0;
    //
    if QtdeSrc < 0 then
      Mensagem('Quantidade n�o pode ser negativa!!! (15)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo));
    //
    case TipoRegSPEDProd of
      trsp21X:
      begin
        InsereItemAtual_K210(Integer(OriMovID), Codigo, MovimCod, Controle,
        DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO, FornecMO,
        EntiSitio, QtdeSrc, OrigemOpeProc, oidk210RibDTA, IDSeq1);
        if EhDoMes then
        begin
          if (PecasDst <> 0) (*and (DtHrFimOpe > 1)*) then
          begin
            case TipoEstq of
              1: QtdeDst := AreaM2Dst;
              2: QtdeDst := PesoKgDst;
              else
              begin
                Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                QrIts.SQL.Text);
                //
                QtdeDst := PecasDst;
              end;
            end;
            //QtdeDst := QtdeDst * MyFator;
          end else
            QtdeDst := 0;
          //
          if QtdeDst < 0 then
            Mensagem('Quantidade n�o pode ser negativa!!! (16)' + sLineBreak +
            'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo))
          else if (QtdeDst = 0) and (DtHrFimOpe > 1) then
          begin
            case MovimID of
              emidEmProcCal: DtHrFimOpe := 0;
              emidEmProcCur: DtHrFimOpe := 0;
              else Mensagem('Quantidade zerada para OP encerrada!!! (8)');
            end;
          end;
          //
          InsereItemAtual_K215((*LinArqPai*)IDSeq1, Data, DtMovim, DtCorrApo, GGXDst,
          QtdeDst, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibDTA,
          ESTSTabSorc);
        end;
      end;
      else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(17)');
    end;
  end;
  //
  //
  function RegistroK215_SubPrd(const IDSeq1: Integer): Boolean;
  var
    Data, DtMovim, DtCorrApo: TDateTime;
    QtdeDst, PecasDst, PesoKgDst, AreaM2Dst: Double;
    Codigo, MovimCod, MyFator, TipoEstq, GGXDst, ID_Item, Controle, MyMovimID,
    ClientMO, FornecMO, EntiSitio: Integer;
  procedure Mensagem(TextoBase: String);
  begin
    Geral.MB_ERRO(TextoBase + sLineBreak +
    'MovimCod: ' + Geral.FF0(MovimCod) + sLineBreak +
    'MovimID: ' + Geral.FF0(Integer(MovimID)) + sLineBreak +
    sProcName + sLineBreak +
    sLineBreak + 'Avise a DERMATEK!!!' + sLineBreak +
    QrProdParc.SQL.Text);
  end;
  begin
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    MyMovimID  := QrIts.FieldByName('MovimID').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GraGruX').AsInteger;
    Controle   := QrIts.FieldByName('Controle').AsInteger;
    ID_Item    := Controle;
    //
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibDTA:
      begin
        AreaM2Dst := QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('PesoKg').AsFloat;
        PecasDst  := QrIts.FieldByName('Pecas').AsFloat;
      end;
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //
    //Parei aqui !!!  � asimm???
    if (PecasDst <> 0) or (PesoKgDst <> 0)(*and (DtHrFimOpe > 1)*) then
    begin
      case TipoEstq of
        1: QtdeDst := AreaM2Dst;
        2: QtdeDst := PesoKgDst;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrIts.SQL.Text);
          //
          QtdeDst := PecasDst;
        end;
      end;
      //QtdeDst := QtdeDst * MyFator;
      QtdeDst := QtdeDst;
    end else
      QtdeDst := 0;
    //
    if QtdeDst < 0 then
      Mensagem('Quantidade n�o pode ser negativa!!! (17)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo));
    //
    InsereItemAtual_K215((*LinArqPai*)IDSeq1, Data, DtMovim, DtCorrApo, GGXDst, QtdeDst,
    COD_INS_SUBST, ID_Item, MyMovimID, Codigo, MovimCod, Controle,
    ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibDTA, ESTSTabSorc);
  end;
  var
    LinArqK210: Integer;
    //Data
    DtIniOS, DtFimOS: TDateTime;
begin
  Result := True;
  // 1. Codigos das entradas de materia-prima que caleirou dentro do mes
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _PRE_GPS_; ',
  'CREATE TABLE _PRE_GPS_ ',
  'SELECT GSPJmpNiv2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE GSPJmpNiv2<>0 ',
  SQL_PeriodoVS,
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_; ',
  'CREATE TABLE _PRE_DESC_ ',
  ' ',
  'SELECT DISTINCT SrcNivel1, 1 Normal ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCur)),
  'AND SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  SQL_PeriodoVS,
  ' ',
  'UNION ',
  ' ',
  'SELECT DISTINCT vmi.Codigo, 2 Normal ',
  'FROM _PRE_GPS_ gps ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  '  ON gps.GSPJmpNiv2=vmi.Controle ',
  '; ',
  ' ',
  'SELECT DISTINCT SrcNivel1 ',
  'FROM _PRE_DESC_ ',
  'ORDER BY SrcNivel1; ',
  ' ',
  '']);
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  PB2.Position := 0;
  PB2.Max := QrOri.RecordCount;
  while not QrOri.Eof do
  begin
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    // 1a. Caleiros das entradas de couro in natura do codigo de entrada atual
    //     para saber o PDA(DTA) (campo RmsGGX)!
    UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
    'SELECT vmi.RmsGGX GGXDst, vmi.JmpGGX GGXSrc, ',
    'cab.Codigo, cab.MovimCod, vmi.SrcNivel2, ',
    'vmi.Controle VMICal, vmi.GraGruX GGXOriCal, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
       VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
       'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'vmi.QtdAntPeso, vmi.QtdAntPeca, vmi.QtdAntArM2, vmi.RmsNivel2, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN vscalcab   cab ON cab.Codigo=vmi.SrcNivel1 ',  // colorado!!!
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND vmi.SrcNivel1=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),  // 1...270...n
    '']);
    //Geral.MB_SQL(Self, QrIts);
    ObtemDataIniEFim(QrOri.FieldByName('SrcNivel1').AsInteger, DtIniOS, DtFimOS);
    //
    QrIts.First;
    while not QrIts.Eof do
    begin
      RegistroK210(TEstqMovimID.emidEmProcCal, DtIniOS, DtFimOS, LinArqK210);
      //
      QrIts.Next;
    end;
    //
    // 1b. Itens de caleiro do codigo atual
    UnDmkDAC_PF.AbreMySQLQuery0(QrMae, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
    'AND Codigo=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),
    //SQL_PeriodoVS,
    '']);
    QrMae.First;
    while not QrMae.Eof do
    begin
      // 1b1. Subprodutos gerados do item de entrada atual
      UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
      'SELECT vmi.GraGruX, ',
      'vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
      'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
         VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
         'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
      'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
      'WHERE vmi.GSPJmpNiv2=' + Geral.FF0(QrMae.FieldByName('Controle').AsInteger),
      SQL_PeriodoVS,
      '']);
      //if QrIts.RecordCount > 0 then
        //Geral.MB_SQL(Self, QrIts);
      QrIts.First;
      while not QrIts.Eof do
      begin
        RegistroK215_SubPrd(LinArqK210);
        //
        QrIts.Next;
      end;
      //
      QrMae.Next;
    end;
    //
    QrOri.Next;
  end;
end;
}

{
function TFmSpedEfdIcmsIpiClaProd.ProducaoIsolada_Insere_OrigeRibPDA_210(
  const TipoRegSPEDProd: TTipoRegSPEDProd; const MovimID: TEstqMovimID;
  const MovimNiv: TEstqMovimNiv; const SQL_PeriodoVS: String;
  const OrigemOpeProc: TOrigemOpeProc): Boolean;
const
  COD_INS_SUBST = '';
  ESTSTabSorc = estsVMI;
  sProcName = 'FmSPED_EFD_K2XX.ProducaoIsolada_Insere_OrigeRibPDA_210';
  //
  function FiltroPeriodo(TipoRegSPEDProd: TTipoRegSPEDProd): String;
  begin
    case TipoRegSPEDProd of
      trsp21X: Result := SQL_PeriodoVS;
      //trsp23X: Result := SQL_PeriodoVS;
      //trsp25X: Result := SQL_PeriodoVS;
      else
      begin
        Geral.MB_Erro('"FiltroReg" indefinido: ' +
          Geral.FF0(Integer(TipoRegSPEDProd)) + '! (7)');
        Result := '#ERR_PERIODO_';
      end;
    end;
  end;
  //
  // Obtem data inicial e final de movimentos de caleiro e geracao de subprodutos da entrada selecionada!
  procedure ObtemDataIniEFim(const Codigo: Integer; var Ini, Fim: TDateTime);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDatas, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _PRE_DESC_ORI_MP_; ',
    'CREATE TABLE _PRE_DESC_ORI_MP_ ',
    'SELECT Controle  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND Codigo=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _PRE_DESC_DTIF_; ',
    ' ',
    'CREATE TABLE _PRE_DESC_DTIF_ ',
    'SELECT vmi.Controle, vmi.DataHora ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND vmi.SrcNivel1=' + Geral.FF0(Codigo),
    '; ',
    ' ',
    'INSERT IGNORE INTO _PRE_DESC_DTIF_ ',
    ' ',
    'SELECT vmi.Controle, vmi.DataHora  ',
    'FROM _PRE_DESC_ORI_MP_ ori  ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    '  ON ori.Controle=vmi.GSPSrcNiv2 ',
    'WHERE vmi.GSPSrcNiv2 <> 0 ',
    'AND vmi.GSPJmpNiv2=0; ',
    ' ',
    'SELECT MIN(DataHora) MinDH,  ',
    'MAX(DataHora) MaxDH  ',
    'FROM _PRE_DESC_DTIF_ ',
    'WHERE Controle > 0 ',
    '']);
    //Geral.MB_SQL(Self, QrDatas);
    //
    Ini := QrDatasMinDH.Value;
    Fim := QrDatasMaxDH.Value;
  end;
  //
  function RegistroK210(const OriMovID: TEstqMovimID; const DtIniOS, DtFimOS:
  TDateTime; var IDSeq1: Integer): Boolean;
  var
  DtHrAberto, DtHrFimOpe, Data, DtMovim, DtCorrApo: TDateTime;
  QtdeSrc, QtdeDst, PecasSrc, PesoKgSrc, AreaM2Src, PecasDst, PesoKgDst,
  AreaM2Dst: Double;
  MyFator, TipoEstq, Codigo, MovimCod, GGXSrc, GGXDst, ID_Item, Controle,
  ClientMO, FornecMO, EntiSitio: Integer;
  EhDoMes: Boolean;
  procedure Mensagem(TextoBase: String);
  begin
    Geral.MB_ERRO(TextoBase + sLineBreak +
    'MovimCod: ' + Geral.FF0(MovimCod) + sLineBreak +
    'MovimID: ' + Geral.FF0(Integer(MovimID)) + sLineBreak +
    sProcName + sLineBreak +
    sLineBreak + 'Avise a DERMATEK!!!' + sLineBreak +
    QrProdParc.SQL.Text);
  end;
  begin
    IDSeq1     := 0;
    AreaM2Src  := 0;
    PesoKgSrc  := 0;
    PecasSrc   := 0;
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := -1;
    QtdeSrc    := 0;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    DtHrAberto := DtIniOS;
    DtHrFimOpe := DtFimOS;
    GGXSrc     := QrIts.FieldByName('GGXSrc').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GGXDst').AsInteger;
    Controle   := QrIts.FieldByName('RmsNivel2').AsInteger;
    ID_Item    := Controle;
    // 270
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibPDA:
      begin
        AreaM2Src := QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgSrc := QrIts.FieldByName('PesoKg').AsFloat;
        PecasSrc  := QrIts.FieldByName('Pecas').AsFloat;
        //
        AreaM2Dst := QrIts.FieldByName('QtdAntArM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('QtdAntPeso').AsFloat;
        PecasDst  := QrIts.FieldByName('QtdAntPeca').AsFloat;
      end;
      else
      begin
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        Exit;
      end;
    end;
    //
    EhDoMes := (Data >= FDiaIni) and (Data < FDiaPos);
    if EhDoMes then
    begin
      if (PecasSrc <> 0) (*and (DtHrFimOpe > 1)*) then
      begin
        case TipoEstq of
          1: QtdeSrc := AreaM2Src;
          2: QtdeSrc := PesoKgSrc;
          else
          begin
            Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
            + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
            'Reduzido = ' + Geral.FF0(GGXSrc) + sLineBreak +
            'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
            QrIts.SQL.Text);
            //
            QtdeSrc := PecasSrc;
          end;
        end;
        QtdeSrc := QtdeSrc * MyFator;
      end else
        QtdeSrc := 0;
    end else
      QtdeSrc := 0;
    //
    if QtdeSrc < 0 then
      Mensagem('Quantidade n�o pode ser negativa!!! (18)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeSrc, 3, siNegativo));
(*
    else if (QtdeSrc = 0) and (DtHrFimOpe > 1) then
    begin
      case MovimID of
        emidEmProcCal: DtHrFimOpe := 0;
        emidEmProcCur: DtHrFimOpe := 0;
        else Mensagem('Quantidade zerada para OP encerrada!!! (8)');
      end;
    end;
*)
    //
    case TipoRegSPEDProd of
      trsp21X:
      begin
        InsereItemAtual_K210(Integer(OriMovID), Codigo, MovimCod, Controle,
        DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApo, GGXSrc, ClientMO, FornecMO,
        EntiSitio, QtdeSrc, OrigemOpeProc, oidk210RibPDA, IDSeq1);
        //
        if EhDoMes then
        begin
          if (PecasDst <> 0) (*and (DtHrFimOpe > 1)*) then
          begin
            case TipoEstq of
              1: QtdeDst := AreaM2Dst;
              2: QtdeDst := PesoKgDst;
              else
              begin
                Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
                + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
                'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
                'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
                QrIts.SQL.Text);
                //
                QtdeDst := PecasDst;
              end;
            end;
            //QtdeDst := QtdeDst * MyFator;
          end else
            QtdeDst := 0;
          //
          if QtdeDst < 0 then
            Mensagem('Quantidade n�o pode ser negativa!!! (19)' + sLineBreak +
            'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo))
          else if (QtdeDst = 0) and (DtHrFimOpe > 1) then
          begin
            case MovimID of
              emidEmProcCal: DtHrFimOpe := 0;
              emidEmProcCur: DtHrFimOpe := 0;
              else Mensagem('Quantidade zerada para OP encerrada!!! (8)');
            end;
          end;
          //
          InsereItemAtual_K215((*LinArqPai*)IDSeq1, Data, DtMovim, DtCorrApo, GGXDst,
          QtdeDst, COD_INS_SUBST, ID_Item, Integer(MovimID), Codigo, MovimCod,
          Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibPDA,
          ESTSTabSorc);
        end;
      end;
      else
      Geral.MB_Erro('Tipo de registro SPED EFD (' + Geral.FF0(Integer(
      TipoRegSPEDProd)) + ') n�o implementado em ' + sProcName + '(18)');
    end;
  end;
  //
  //
  function RegistroK215_SubPrd(const IDSeq1: Integer): Boolean;
  var
    Data, DtMovim, DtCorrApo: TDateTime;
    QtdeDst, PecasDst, PesoKgDst, AreaM2Dst: Double;
    Codigo, MovimCod, MyFator, TipoEstq, GGXDst, ID_Item, Controle,
    ClientMO, FornecMO, EntiSitio, MyMovimID: Integer;
  procedure Mensagem(TextoBase: String);
  begin
    Geral.MB_ERRO(TextoBase + sLineBreak +
    'MovimCod: ' + Geral.FF0(MovimCod) + sLineBreak +
    'MovimID: ' + Geral.FF0(Integer(MovimID)) + sLineBreak +
    sProcName + sLineBreak +
    sLineBreak + 'Avise a DERMATEK!!!' + sLineBreak +
    QrProdParc.SQL.Text);
  end;
  begin
    AreaM2Dst  := 0;
    PesoKgDst  := 0;
    PecasDst   := 0;
    MyFator    := 1;
    QtdeDst    := 0;
    TipoEstq   := QrIts.FieldByName('TipoEstq').AsInteger;
    MyMovimID  := QrIts.FieldByName('MovimID').AsInteger;
    Codigo     := QrIts.FieldByName('Codigo').AsInteger;
    MovimCod   := QrIts.FieldByName('MovimCod').AsInteger;
    // 215
    Data       := QrIts.FieldByName('DATA').AsDateTime;
    GGXDst     := QrIts.FieldByName('GraGruX').AsInteger;
    Controle   := QrIts.FieldByName('Controle').AsInteger;
    ID_Item    := Controle;
    //
    DtMovim    := QrIts.FieldByName('DATA').AsDateTime;
    DtCorrApo  := QrIts.FieldByName('DtCorrApo').AsDateTime;
    //
    ClientMO   := QrIts.FieldByName('ClientMO').AsInteger;
    FornecMO   := QrIts.FieldByName('FornecMO').AsInteger;
    EntiSitio  := QrIts.FieldByName('EntiSitio').AsInteger;
    //
    case MovimID of
      emidEmRibPDA:
      begin
        AreaM2Dst := QrIts.FieldByName('AreaM2').AsFloat;
        PesoKgDst := QrIts.FieldByName('PesoKg').AsFloat;
        PecasDst  := QrIts.FieldByName('Pecas').AsFloat;
      end;
      else
      begin
        //Result := False;
        Geral.MB_ERRO(
        '"MovimID" n�o implementado em ' + sProcName);
        //Close;
        Exit;
      end;
    end;
    //
    //Parei aqui !!!  � asimm???
    if (PecasDst <> 0) or (PesoKgDst <> 0)(*and (DtHrFimOpe > 1)*) then
    begin
      case TipoEstq of
        1: QtdeDst := AreaM2Dst;
        2: QtdeDst := PesoKgDst;
        else
        begin
          Geral.MB_Erro('"TipoEstq" = ' + Geral.FF0(Tipoestq) + ' indefinido em '
          + sProcName + sLineBreak + sGRANDEZA_UNIDMED[TipoEstq] + sLineBreak +
          'Reduzido = ' + Geral.FF0(GGXDst) + sLineBreak +
          'MovimCod = ' + Geral.FF0(MovimCod) + sLineBreak + sLineBreak +
          QrIts.SQL.Text);
          //
          QtdeDst := PecasDst;
        end;
      end;
      //QtdeDst := QtdeDst * MyFator;
      QtdeDst := QtdeDst;
    end else
      QtdeDst := 0;
    //
    if QtdeDst < 0 then
      Mensagem('Quantidade n�o pode ser negativa!!! (20)' + sLineBreak +
      'Qtde: ' + Geral.FFT(QtdeDst, 3, siNegativo));
    //
    InsereItemAtual_K215((*LinArqPai*)IDSeq1, Data, DtMovim, DtCorrApo, GGXDst, QtdeDst,
    COD_INS_SUBST, ID_Item, MyMovimID, Codigo, MovimCod, Controle,
    ClientMO, FornecMO, EntiSitio, OrigemOpeProc, oidk210RibPDA, ESTSTabSorc);
  end;
  var
    LinArqK210: Integer;
    //Data
    DtIniOS, DtFimOS: TDateTime;
begin
  Result := True;
  // 1. Codigos das entradas de materia-prima que caleirou dentro do mes
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _PRE_GPS_; ',
  'CREATE TABLE _PRE_GPS_ ',
  'SELECT GSPSrcNiv2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE GSPSrcNiv2<>0 ',
  'AND GSPJmpNiv2=0 ',
  SQL_PeriodoVS,
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRE_DESC_; ',
  'CREATE TABLE _PRE_DESC_ ',
  ' ',
  'SELECT DISTINCT SrcNivel1, 1 Normal ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)),
  'AND SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  SQL_PeriodoVS,
  ' ',
  'UNION ',
  ' ',
  'SELECT DISTINCT vmi.Codigo, 2 Normal ',
  'FROM _PRE_GPS_ gps ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  '  ON gps.GSPSrcNiv2=vmi.Controle ',
  '; ',
  ' ',
  'SELECT DISTINCT SrcNivel1 ',
  'FROM _PRE_DESC_ ',
  'ORDER BY SrcNivel1; ',
  ' ',
  '']);
  //if QrOri.RecordCount > 0 then
    //Geral.MB_Aviso('DEPRRCAR!!! 210 - 1');
  //
  //Geral.MB_SQL(Self, QrOri);
  QrOri.First;
  PB2.Position := 0;
  PB2.Max := QrOri.RecordCount;
  while not QrOri.Eof do
  begin
    MyObjects.UpdPB(PB2, LaAviso3, LaAviso4);
    // 1a. Caleiros das entradas de couro in natura do codigo de entrada atual
    //     para saber o PDA (campo RmsGGX)!
    UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
    'SELECT vmi.RmsGGX GGXDst, vmi.GraGruX GGXSrc, ',
    'cab.Codigo, cab.MovimCod, vmi.SrcNivel2, ',
    'vmi.Controle VMICal, vmi.GraGruX GGXOriCal, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
       VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
       'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
    'vmi.QtdAntPeso, vmi.QtdAntPeca, vmi.QtdAntArM2, vmi.RmsNivel2, ',
    'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN vsinncab   cab ON cab.Codigo=vmi.SrcNivel1 ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND vmi.SrcNivel1=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),  // 1...270...n
    //SQL_PeriodoVS,
    '']);
    //Geral.MB_SQL(Self, QrIts);
    ObtemDataIniEFim(QrOri.FieldByName('SrcNivel1').AsInteger, DtIniOS, DtFimOS);
    //
    QrIts.First;
    while not QrIts.Eof do
    begin
      //Data := QrIts.FieldByName('DATA').AsDateTime;
      //if (Data >= DiaIni) and (Data < DiaPos) then
      RegistroK210(TEstqMovimID.emidCompra, DtIniOS, DtFimOS, LinArqK210);
      //
      QrIts.Next;
    end;
    //
    // 1b. Itens de entrada de materia-prima do codigo atual
    UnDmkDAC_PF.AbreMySQLQuery0(QrMae, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
    'AND Codigo=' + Geral.FF0(QrOri.FieldByName('SrcNivel1').AsInteger),  // 1...270...n
    //SQL_PeriodoVS,
    '']);
    //Geral.MB_SQL(Self, QrMae);
    QrMae.First;
    while not QrMae.Eof do
    begin
      // 1b1. Subprodutos gerados do item de entrada atual
      UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
      'SELECT vmi.GraGruX, ',
      'vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
      'DATE(vmi.DataHora) DATA, vmi.DtCorrApo, ',
         VS_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
         'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
      'vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, med.Grandeza ',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
      'WHERE vmi.GSPJmpNiv2=0 ',
      'AND vmi.GSPSrcNiv2=' + Geral.FF0(QrMae.FieldByName('Controle').AsInteger),
      SQL_PeriodoVS,
      '']);
      //Geral.MB_SQL(Self, QrIts);
      QrIts.First;
      while not QrIts.Eof do
      begin
        RegistroK215_SubPrd(LinArqK210);
        //
        QrIts.Next;
      end;
      //
      QrMae.Next;
    end;
    //
    QrOri.Next;
  end;
end;
}

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.Mensagem(ProcName: String; MovimCod, MovimID: Integer; TextoBase:
  String; Query: TmySQLQuery);
begin
  Geral.MB_ERRO(TextoBase + sLineBreak +
  'MovimCod: ' + Geral.FF0(MovimCod) + sLineBreak +
  'MovimID: ' + Geral.FF0(Integer(MovimID)) + sLineBreak +
  ProcName + sLineBreak +
  sLineBreak + 'Avise a DERMATEK!!!' + sLineBreak +
  ProcName + sLineBreak +
  Query.SQL.Text);
end;

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.PCRegistroChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  AllowChange := False;
end;

{
procedure TFmSpedEfdIcmsIpiClaProd.ReopenOriPQx(MovimCod: Integer; SQL_PeriodoPQ:
  String; ShowSQL: Boolean);
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, SUM(Peso) Peso, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
  'CliDest ClientMO ',
  'FROM pqx ',
  'WHERE ( ',
  '  Tipo=110   ',
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM emit ',
  '    WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'OR ',
  '( ',
  '  Tipo=190   ',
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM PQO ',
  '    WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'AND Peso <> 0',
  SQL_PeriodoPQ,
  'GROUP BY OrigemCodi, Insumo, DataX, ANO, MES ',
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, Peso, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
  'CliDest ClientMO ',
  'FROM pqx ',
  'WHERE ( ',
  '  Tipo=110   ',
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM emit ',
  '    WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'OR ',
  '( ',
  '  Tipo=190   ',
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM PQO ',
  '    WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'AND Peso <> 0',
  SQL_PeriodoPQ,
  '']);
  //
  if ShowSQL then
    Geral.MB_Info(QrOri.SQL.Text);
end;
}

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.ReopenSecO_P(Qry: TmySQLQuery; SQL_Periodo, Filtro: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_K2XX_O_P;',
  'CREATE TABLE _SPED_EFD_K2XX_O_P',
  '',
  'SELECT vmi.Empresa, ',
  'vmi.MovimID, vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
  'DATE(vmi.DataHora) Data, vmi.ClientMO, vmi.FornecMO, scc.EntiSitio, ',
  'cab.GGXDst GGX_Src, vmi.MovimNiv, ',
  'vmi.GraGruX GGX_Dst, Pecas, AreaM2, PesoKg, vmi.DtCorrApo, med.Grandeza',
  'FROM ' + TMeuDB + '.vspwecab cab',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON vmi.MovimCod=cab.MovimCod',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN ' + TMeuDB + '.unidmed med ON med.Codigo=gg1.UnidMed',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad scc ON scc.Codigo=scl.Codigo ',
  SQL_Periodo,
  'AND cab.GGXDst <> vmi.GragruX',
  'AND MovimNiv=' + Geral.FF0(Integer(eminDestWEnd)), //22
  'AND MovimTwn' + Filtro + '0 ',
  ';',
  '',
  'SELECT * FROM _SPED_EFD_K2XX_O_P',
  '']);
  //
  //Geral.MB_SQL(Self, QrSecO_P);
end;

function TFmSpedEfdIcmsIpiClaProd_v03_0_1.SetaItensK220Corretos(): Integer;
begin
//CGItensK220.MaxValue;
  Result :=
     1     //  Classe e reclasse
    +2     //  Couro posto para ope��o/processo
    //+4   //  Coprodu��o de produtos
    //+8   //  Coprodu��o de subprodutos
    +16    //  Redirecionamento
    +32;   //  Desclassifica��o em ope��o/processo
end;

procedure TFmSpedEfdIcmsIpiClaProd_v03_0_1.VerificaGGXDstZerados;
var
  Qry1, Qry2: TmySQLQuery;
  Codigo: Integer;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqmovimNiv;
  Tabela: String;
  //
  procedure AtualizaAtual();
  var
    GGXDst: Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
    'SELECT MIN(GraGruX) GraGruX  ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimCod=' + Geral.FF0(Qry1.FieldByName('MovimCod').AsInteger),
    'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    'AND GraGruX>0 ',
    '']);
    //
    GGXDst := Qry2.FieldByName('GraGruX').AsInteger;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
    'GGXDst'], ['Codigo'], [
    GGXDst], [Codigo], True);
  end;

begin
  Qry1 := TmySQLQuery.Create(Dmod);
  Qry2 := TmySQLQuery.Create(Dmod);
  try
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
  'SELECT vmc.Codigo MovimCod, vmc.MovimID, ',
  'cab.Codigo, GGXDst',
  'FROM vsopecab cab ',
  'LEFT JOIN vsmovcab vmc ON vmc.CodigoID=cab.Codigo ',
  'WHERE cab.GGXDst=0 ',
  'AND vmc.MovimID=11 ',
  'AND PecasDst >0',
  '',
  ' ',
  'UNION  ',
  ' ',
  'SELECT vmc.Codigo MovimCod, vmc.MovimID, ',
  'cab.Codigo, GGXDst',
  'FROM vspwecab cab ',
  'LEFT JOIN vsmovcab vmc ON vmc.CodigoID=cab.Codigo ',
  'WHERE cab.GGXDst=0 ',
  'AND vmc.MovimID=19 ',
  'AND PecasDst >0',
  ' ',
  'UNION  ',
  ' ',
  'SELECT vmc.Codigo MovimCod, vmc.MovimID, ',
  'cab.Codigo, GGXDst',
  'FROM vscalcab cab ',
  'LEFT JOIN vsmovcab vmc ON vmc.CodigoID=cab.Codigo ',
  'WHERE cab.GGXDst=0 ',
  'AND vmc.MovimID=26 ',
  'AND PecasDst >0',
  ' ',
  'UNION  ',
  ' ',
  'SELECT vmc.Codigo MovimCod, vmc.MovimID, ',
  'cab.Codigo, GGXDst',
  'FROM vscurcab cab ',
  'LEFT JOIN vsmovcab vmc ON vmc.CodigoID=cab.Codigo ',
  'WHERE cab.GGXDst=0 ',
  'AND vmc.MovimID=27 ',
  'AND PecasDst >0',
  '',
  'ORDER BY MovimCod',
  '']);
    //
    if Qry1.RecordCount > 0 then
    begin
      Screen.Cursor := crHourGlass;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Definindo GGXDst em opera��es e processos!');
      PB2.Position := 0;
      PB2.Max := Qry1.RecordCount;
      Qry1.First;
      while not Qry1.Eof do
      begin
        MyObjects.UpdPBOnly(PB2);
        MovimID := TEstqMovimID(Qry1.FieldByName('MovimID').AsInteger);
        Codigo  := Qry1.FieldByName('Codigo').AsInteger;
        Tabela := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
        //
        case MovimID of
          emidEmOperacao: MovimNiv := eminDestOper;
          emidEmProcWE: MovimNiv := eminDestWEnd;
          emidEmProcCal: MovimNiv := eminEmCalInn;
          emidEmProcCur: MovimNiv := eminEmCurInn;
          else
          begin
            MovimNiv := eminSemNiv;
            Geral.MB_Erro('MovimID n�o implementado em "FmSPED_EFD_K2XX.VerificaGGXDstZerados"');
          end;
        end;
        if MovimNiv <> eminSemNiv then
          AtualizaAtual();
        //
        Qry1.Next;
      end;
    end;
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Qry2.Free;
    Screen.Cursor := crDefault;
  end;
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Qry1.Free;
    Screen.Cursor := crDefault;
  end;
end;

(*
object QrSumGAR: TmySQLQuery
  Database = Dmod.MyDB
  Left = 844
  Top = 364
  object QrSumGARPecas: TFloatField
    FieldName = 'Pecas'
  end
  object QrSumGARAreaM2: TFloatField
    FieldName = 'AreaM2'
  end
  object QrSumGARPesoKg: TFloatField
    FieldName = 'PesoKg'
  end
end
*)
end.
