unit TintasPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Db, mySQLDbTables, Grids,
  DBGrids, dmkImage, UnDmkEnums, DmkDAC_PF, dmkDBGrid, frxClass, frxDBSet,
  dmkDBGridZTO;

type
  TFmTintasPesq = class(TForm)
    Panel1: TPanel;
    QrTintasCab: TmySQLQuery;
    DsTintasCab: TDataSource;
    QrTintasCabNumero: TIntegerField;
    QrTintasCabNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtPesquisa: TBitBtn;
    BtLocaliza: TBitBtn;
    BtSaida: TBitBtn;
    QrTintasCabAtivo: TSmallintField;
    QrTintasCabSetor_TXT: TWideStringField;
    frxDsTintasCab: TfrxDBDataset;
    BtImprime: TBitBtn;
    frxTintasCab: TfrxReport;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    Edit1: TEdit;
    RGMascara: TRadioGroup;
    RGAtivo: TRadioGroup;
    LaTotal: TLabel;
    DBGrid1: TdmkDBGridZTO;
    BtExclui: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtLocalizaClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxTintasCabGetValue(const VarName: string; var Value: Variant);
    procedure QrTintasCabAfterScroll(DataSet: TDataSet);
    procedure QrTintasCabBeforeClose(DataSet: TDataSet);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure Edit1Change(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPesquisa(Numero: Integer);
    procedure AtualizaAtivo(Numero, Ativo: Integer);
  public
    { Public declarations }
    FPesq: Integer;
    FExclui: Boolean;
  end;

  var
  FmTintasPesq: TFmTintasPesq;

implementation

{$R *.DFM}

uses UnMyObjects, Module, ModuleGeral, TintasCab, UMySQLModule, UnMySQLCuringa,
  UnPQ_PF;

procedure TFmTintasPesq.BtSaidaClick(Sender: TObject);
begin
  FPesq := 0;
  Close;
end;

procedure TFmTintasPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  Edit1.SetFocus;
end;

procedure TFmTintasPesq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FExclui := False;
  FPesq   := 0;
end;

procedure TFmTintasPesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTintasPesq.FormShow(Sender: TObject);
begin
  if FExclui = False then
  begin
    BtExclui.Visible   := False;
    BtLocaliza.Visible := True;
    BtImprime.Visible  := True;
    DBGrid1.Options    := [dgTitles, dgIndicator, dgColumnResize, dgColLines,
                          dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit,
                          dgTitleClick, dgTitleHotTrack];
  end else
  begin
    BtExclui.Visible   := True;
    BtLocaliza.Visible := False;
    BtImprime.Visible  := False;
    DBGrid1.Options    := [dgTitles, dgIndicator, dgColumnResize, dgColLines,
                          dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit,
                          dgTitleClick, dgTitleHotTrack, dgMultiSelect];
  end;
end;

procedure TFmTintasPesq.frxTintasCabGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Date
end;

procedure TFmTintasPesq.QrTintasCabAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrTintasCab.RecordCount)
end;

procedure TFmTintasPesq.QrTintasCabBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmTintasPesq.ReopenPesquisa(Numero: Integer);
var
  Texto, Ativo_Str: String;
begin
  case RGAtivo.ItemIndex of
    1:
      Ativo_Str := 'AND fml.Ativo=1';
    2:
      Ativo_Str := 'AND fml.Ativo=0';
    else
      Ativo_Str := '';
  end;
  //
  Texto := CuringaLoc.ObtemSQLPesquisaText(RGMascara.ItemIndex, Edit1.Text);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTintasCab, Dmod.MyDB, [
    'SELECT fml.Numero, fml.Nome, ',
    'lse.Nome Setor_TXT, fml.Ativo ',
    'FROM tintascab fml ',
    'LEFT JOIN listasetores lse ON lse.Codigo=fml.Setor ',
    'WHERE fml.Nome LIKE "' + Texto + '"',
    'AND fml.Numero<> 0 ',
    Ativo_Str,
    '']);
  if Numero <> 0 then
    QrTintasCab.Locate('Numero', Numero, []);
end;

procedure TFmTintasPesq.BtPesquisaClick(Sender: TObject);
begin
  ReopenPesquisa(0);
end;

procedure TFmTintasPesq.BtExcluiClick(Sender: TObject);
var
  I, Numero: Integer;
begin
  if DBGrid1.SelectedRows.Count > 0 then
  begin
    if Geral.MB_Pergunta('Deseja excluir TODAS as receitas selecionadas (tamb�m seus itens e fluxo)?') <> ID_YES then
      Exit;
    //
    with DBGrid1.DataSource.DataSet do
    for I := 0 to DBGrid1.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGrid1.SelectedRows.Items[I]));
      GotoBookmark(DBGrid1.SelectedRows.Items[I]);
      //
      Numero := QrTintasCabNumero.Value;
      //
      PQ_PF.ExcluiReceita(Numero);
    end;
    ReopenPesquisa(0);
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

procedure TFmTintasPesq.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxTintasCab, [
    DmodG.frxDsDono,
    frxDsTintasCab
    ]);
  MyObjects.frxMostra(frxTintasCab, 'Pesquisa de receitas');
end;

procedure TFmTintasPesq.BtLocalizaClick(Sender: TObject);
begin
  FPesq := QrTintasCabNumero.Value;
  Close;
end;

procedure TFmTintasPesq.DBGrid1CellClick(Column: TColumn);
var
  Numero, Ativo: Integer;
begin
  if (Column.FieldName = 'Ativo') and (FExclui = False) then
  begin
    Ativo  := QrTintasCabAtivo.Value;
    Numero := QrTintasCabNumero.Value;
    //
    AtualizaAtivo(Numero, Ativo);
    //
    ReopenPesquisa(Numero);
  end;
end;

procedure TFmTintasPesq.AtualizaAtivo(Numero, Ativo: Integer);
begin
  if Ativo = 0 then
    Ativo := 1
  else
    Ativo := 0;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tintascab', False,
    ['Ativo'], ['Numero'],
    [Ativo], [Numero], True);
end;

procedure TFmTintasPesq.DBGrid1DblClick(Sender: TObject);
begin
  if FExclui = False then
    BtLocalizaClick(Self);
end;

procedure TFmTintasPesq.Edit1Change(Sender: TObject);
var
  Txt: String;
begin
  Txt := Edit1.Text;
  //
  if (Txt <> '') and (Length(Txt) >= 3) then
    ReopenPesquisa(0);
end;

end.
