object FmEmitCab: TFmEmitCab
  Left = 339
  Top = 185
  Caption = 'QUI-RECEI-015 :: Edi'#231#227'o de Dados de Pesagem'
  ClientHeight = 505
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 367
        Height = 32
        Caption = 'Edi'#231#227'o de Dados de Pesagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 367
        Height = 32
        Caption = 'Edi'#231#227'o de Dados de Pesagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 367
        Height = 32
        Caption = 'Edi'#231#227'o de Dados de Pesagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 343
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 444
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 343
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 444
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 101
        Align = alTop
        Caption = ' Dados atuais:'
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 84
          Align = alClient
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 0
          object Label3: TLabel
            Left = 4
            Top = 4
            Width = 47
            Height = 13
            Caption = 'Pesagem:'
          end
          object Label5: TLabel
            Left = 72
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Emiss'#227'o:'
            FocusControl = DBEdit1
          end
          object Label6: TLabel
            Left = 496
            Top = 4
            Width = 40
            Height = 13
            Caption = 'Receita:'
            FocusControl = DBEdit2
          end
          object Label7: TLabel
            Left = 128
            Top = 44
            Width = 35
            Height = 13
            Caption = 'Cliente:'
            FocusControl = DBEdit4
          end
          object Label8: TLabel
            Left = 208
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Peso:'
            FocusControl = DBEdit5
          end
          object Label9: TLabel
            Left = 276
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
            FocusControl = DBEdit6
          end
          object Label15: TLabel
            Left = 84
            Top = 44
            Width = 29
            Height = 13
            Caption = 'Ful'#227'o:'
            FocusControl = DBEdit10
          end
          object Label1: TLabel
            Left = 392
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
            FocusControl = DBEdit8
          end
          object Label2: TLabel
            Left = 4
            Top = 44
            Width = 52
            Height = 13
            Caption = 'Espessura:'
            FocusControl = DBEdit11
          end
          object DBEdit1: TDBEdit
            Left = 72
            Top = 20
            Width = 133
            Height = 21
            DataField = 'DataEmis'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 496
            Top = 20
            Width = 40
            Height = 21
            DataField = 'Numero'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 536
            Top = 20
            Width = 237
            Height = 21
            DataField = 'NOME'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 128
            Top = 60
            Width = 488
            Height = 21
            DataField = 'NOMECI'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 3
          end
          object DBEdit5: TDBEdit
            Left = 208
            Top = 20
            Width = 64
            Height = 21
            DataField = 'Peso'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 4
          end
          object DBEdit6: TDBEdit
            Left = 276
            Top = 20
            Width = 40
            Height = 21
            DataField = 'Qtde'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 5
          end
          object DBEdit10: TDBEdit
            Left = 84
            Top = 60
            Width = 40
            Height = 21
            DataField = 'Fulao'
            DataSource = DsEmit
            Enabled = False
            TabOrder = 6
          end
          object DBEdit7: TDBEdit
            Left = 4
            Top = 20
            Width = 64
            Height = 21
            DataField = 'Codigo'
            DataSource = DsEmit
            TabOrder = 7
          end
          object DBEdit8: TDBEdit
            Left = 392
            Top = 20
            Width = 100
            Height = 21
            DataField = 'AreaM2'
            DataSource = DsEmit
            TabOrder = 8
          end
          object DBEdit9: TDBEdit
            Left = 316
            Top = 20
            Width = 73
            Height = 21
            DataField = 'DefPeca'
            DataSource = DsEmit
            TabOrder = 9
          end
          object DBEdit11: TDBEdit
            Left = 4
            Top = 60
            Width = 77
            Height = 21
            DataField = 'Espessura'
            DataSource = DsEmit
            TabOrder = 10
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 101
        Width = 1008
        Height = 242
        Align = alClient
        Caption = ' Dados edit'#225'veis:'
        TabOrder = 1
        ExplicitHeight = 343
        object PainelEscolhas: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 86
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label4: TLabel
            Left = 4
            Top = 2
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label11: TLabel
            Left = 72
            Top = 2
            Width = 28
            Height = 13
            Caption = 'Pe'#231'a:'
          end
          object Label13: TLabel
            Left = 452
            Top = 2
            Width = 29
            Height = 13
            Caption = 'Ful'#227'o:'
          end
          object Label14: TLabel
            Left = 496
            Top = 4
            Width = 98
            Height = 13
            Caption = 'Espessura desejada:'
          end
          object Label16: TLabel
            Left = 294
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object Label17: TLabel
            Left = 370
            Top = 4
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object Label10: TLabel
            Left = 4
            Top = 44
            Width = 88
            Height = 13
            Caption = 'Grupo de emiss'#227'o:'
          end
          object Label24: TLabel
            Left = 636
            Top = 4
            Width = 99
            Height = 13
            Caption = 'Rebaixe (espessura):'
          end
          object Label25: TLabel
            Left = 312
            Top = 44
            Width = 40
            Height = 13
            Caption = 'Cor [F4]:'
          end
          object EdQtde: TdmkEdit
            Left = 4
            Top = 20
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object CBPeca: TdmkDBLookupComboBox
            Left = 108
            Top = 20
            Width = 181
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsDefPecas1
            TabOrder = 2
            dmkEditCB = EdPeca
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdFulao: TdmkEdit
            Left = 452
            Top = 20
            Width = 40
            Height = 21
            MaxLength = 5
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdPeca: TdmkEditCB
            Left = 72
            Top = 20
            Width = 36
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPeca
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdAreaM2: TdmkEditCalc
            Left = 292
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdAreaM2Change
            dmkEditCalcA = EdAreaP2
            CalcType = ctM2toP2
            CalcFrac = cfQuarto
          end
          object EdAreaP2: TdmkEditCalc
            Left = 368
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdAreaM2
            CalcType = ctP2toM2
            CalcFrac = cfCento
          end
          object EdEmitGru: TdmkEditCB
            Left = 4
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmitGru
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmitGru: TdmkDBLookupComboBox
            Left = 60
            Top = 60
            Width = 249
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEmitGru
            TabOrder = 11
            dmkEditCB = EdEmitGru
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdEspessura: TdmkEditCB
            Left = 496
            Top = 20
            Width = 32
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEspessura
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEspessura: TdmkDBLookupComboBox
            Left = 528
            Top = 20
            Width = 104
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Linhas'
            ListSource = DsEspessuras1
            TabOrder = 7
            dmkEditCB = EdEspessura
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdSemiCodEspReb: TdmkEditCB
            Left = 636
            Top = 20
            Width = 32
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBSemiCodEspReb
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBSemiCodEspReb: TdmkDBLookupComboBox
            Left = 668
            Top = 20
            Width = 104
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Linhas'
            ListSource = DsRebaixe
            TabOrder = 9
            dmkEditCB = EdSemiCodEspReb
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdGraCorCad: TdmkEdit
            Left = 312
            Top = 60
            Width = 62
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 12
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnKeyDown = EdGraCorCadKeyDown
          end
          object EdNoGraCorCad: TdmkEdit
            Left = 376
            Top = 60
            Width = 397
            Height = 21
            Enabled = False
            TabOrder = 13
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 101
          Width = 1004
          Height = 139
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitHeight = 240
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 496
            Height = 139
            Align = alLeft
            BevelOuter = bvNone
            Caption = 'Panel8'
            TabOrder = 0
            ExplicitHeight = 240
            object GroupBox3: TGroupBox
              Left = 0
              Top = 24
              Width = 496
              Height = 115
              Align = alClient
              Caption = ' Semi acabado:'
              TabOrder = 0
              ExplicitHeight = 216
              object Panel7: TPanel
                Left = 2
                Top = 15
                Width = 492
                Height = 98
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                ExplicitHeight = 199
                object Label12: TLabel
                  Left = 4
                  Top = 2
                  Width = 58
                  Height = 13
                  Caption = 'Quantidade:'
                end
                object Label18: TLabel
                  Left = 72
                  Top = 2
                  Width = 28
                  Height = 13
                  Caption = 'Pe'#231'a:'
                end
                object Label20: TLabel
                  Left = 308
                  Top = 4
                  Width = 52
                  Height = 13
                  Caption = 'Espessura:'
                end
                object Label21: TLabel
                  Left = 218
                  Top = 48
                  Width = 39
                  Height = 13
                  Caption = #193'rea m'#178':'
                end
                object Label22: TLabel
                  Left = 294
                  Top = 48
                  Width = 37
                  Height = 13
                  Caption = #193'rea ft'#178':'
                end
                object Label23: TLabel
                  Left = 132
                  Top = 46
                  Width = 42
                  Height = 13
                  Caption = 'Peso kg:'
                end
                object Label19: TLabel
                  Left = 376
                  Top = 46
                  Width = 71
                  Height = 13
                  Caption = '% Rendimento:'
                end
                object EdSemiQtde: TdmkEdit
                  Left = 4
                  Top = 20
                  Width = 64
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object CBSemiDefPeca: TdmkDBLookupComboBox
                  Left = 132
                  Top = 20
                  Width = 169
                  Height = 21
                  Color = clWhite
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsDefPecas2
                  TabOrder = 3
                  dmkEditCB = EdSemiDefPeca
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object EdSemiDefPeca: TdmkEditCB
                  Left = 72
                  Top = 20
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBSemiDefPeca
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object EdSemiAreaM2: TdmkEditCalc
                  Left = 216
                  Top = 64
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 7
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSemiAreaM2Change
                  dmkEditCalcA = EdSemiAreaP2
                  CalcType = ctM2toP2
                  CalcFrac = cfQuarto
                end
                object EdSemiAreaP2: TdmkEditCalc
                  Left = 292
                  Top = 64
                  Width = 80
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 8
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  dmkEditCalcA = EdSemiAreaM2
                  CalcType = ctP2toM2
                  CalcFrac = cfCento
                end
                object EdSemiEspessura: TdmkEditCB
                  Left = 308
                  Top = 20
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBSemiEspessura
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBSemiEspessura: TdmkDBLookupComboBox
                  Left = 364
                  Top = 20
                  Width = 121
                  Height = 21
                  Color = clWhite
                  KeyField = 'Codigo'
                  ListField = 'Linhas'
                  ListSource = DsEspessuras2
                  TabOrder = 5
                  dmkEditCB = EdSemiEspessura
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object EdSemiPeso: TdmkEdit
                  Left = 132
                  Top = 64
                  Width = 80
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 6
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object BtCopiar: TBitBtn
                  Tag = 104
                  Left = 4
                  Top = 48
                  Width = 120
                  Height = 40
                  Caption = '&Copiar'
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtCopiarClick
                end
                object EdSemiRendim: TdmkEdit
                  Left = 376
                  Top = 64
                  Width = 109
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 9
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 10
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000000000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
            object Panel9: TPanel
              Left = 0
              Top = 0
              Width = 496
              Height = 24
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object CkRetrabalho: TdmkCheckBox
                Left = 6
                Top = 4
                Width = 81
                Height = 17
                Caption = #201' retrabalho.'
                TabOrder = 0
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
            end
          end
          object Panel10: TPanel
            Left = 496
            Top = 0
            Width = 508
            Height = 139
            Align = alClient
            TabOrder = 1
            ExplicitHeight = 240
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 391
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 492
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 435
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 536
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEmit: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emi.*, gru.Nome NO_EmitGru '
      'FROM emit emi'
      'LEFT JOIN emitgru gru ON gru.Codigo=emi.EmitGru')
    Left = 56
    Top = 76
    object QrEmitCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitDataEmis: TDateTimeField
      FieldName = 'DataEmis'
    end
    object QrEmitStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrEmitNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrEmitNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrEmitNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrEmitTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrEmitNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrEmitClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrEmitTipificacao: TSmallintField
      FieldName = 'Tipificacao'
    end
    object QrEmitTempoR: TSmallintField
      FieldName = 'TempoR'
    end
    object QrEmitTempoP: TSmallintField
      FieldName = 'TempoP'
    end
    object QrEmitSetor: TSmallintField
      FieldName = 'Setor'
    end
    object QrEmitEspessura: TWideStringField
      FieldName = 'Espessura'
      Size = 15
    end
    object QrEmitPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEmitQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmitLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmitDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmitDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmitUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmitAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,#00.00'
    end
    object QrEmitFulao: TWideStringField
      FieldName = 'Fulao'
      Size = 5
    end
    object QrEmitDefPeca: TWideStringField
      FieldName = 'DefPeca'
      Size = 15
    end
    object QrEmitCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrEmitObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
    object QrEmitAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEmitTipific: TSmallintField
      FieldName = 'Tipific'
    end
    object QrEmitAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitSetrEmi: TSmallintField
      FieldName = 'SetrEmi'
    end
    object QrEmitCod_Espess: TIntegerField
      FieldName = 'Cod_Espess'
    end
    object QrEmitCodDefPeca: TIntegerField
      FieldName = 'CodDefPeca'
    end
    object QrEmitSourcMP: TSmallintField
      FieldName = 'SourcMP'
    end
    object QrEmitCustoTo: TFloatField
      FieldName = 'CustoTo'
    end
    object QrEmitCustoKg: TFloatField
      FieldName = 'CustoKg'
    end
    object QrEmitCustoM2: TFloatField
      FieldName = 'CustoM2'
    end
    object QrEmitCusUSM2: TFloatField
      FieldName = 'CusUSM2'
    end
    object QrEmitEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrEmitRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
    end
    object QrEmitNO_EmitGru: TWideStringField
      FieldName = 'NO_EmitGru'
      Size = 60
    end
    object QrEmitSemiCodPeca: TIntegerField
      FieldName = 'SemiCodPeca'
    end
    object QrEmitSemiTxtPeca: TWideStringField
      FieldName = 'SemiTxtPeca'
      Size = 15
    end
    object QrEmitSemiPeso: TFloatField
      FieldName = 'SemiPeso'
    end
    object QrEmitSemiQtde: TFloatField
      FieldName = 'SemiQtde'
    end
    object QrEmitSemiAreaM2: TFloatField
      FieldName = 'SemiAreaM2'
    end
    object QrEmitSemiRendim: TFloatField
      FieldName = 'SemiRendim'
    end
    object QrEmitSemiCodEspe: TIntegerField
      FieldName = 'SemiCodEspe'
    end
    object QrEmitSemiTxtEspe: TWideStringField
      FieldName = 'SemiTxtEspe'
      Size = 15
    end
    object QrEmitBRL_USD: TFloatField
      FieldName = 'BRL_USD'
    end
    object QrEmitBRL_EUR: TFloatField
      FieldName = 'BRL_EUR'
    end
    object QrEmitDtaCambio: TDateField
      FieldName = 'DtaCambio'
    end
    object QrEmitVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
    end
    object QrEmitHoraIni: TTimeField
      FieldName = 'HoraIni'
    end
    object QrEmitGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrEmitNoGraCorCad: TWideStringField
      FieldName = 'NoGraCorCad'
      Size = 30
    end
    object QrEmitSemiCodEspReb: TIntegerField
      FieldName = 'SemiCodEspReb'
    end
  end
  object DsEmit: TDataSource
    DataSet = QrEmit
    Left = 56
    Top = 120
  end
  object QrEmitGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM emitgru'
      'WHERE Ativo=1'
      'ORDER BY Nome'
      '')
    Left = 108
    Top = 72
    object QrEmitGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEmitGru: TDataSource
    DataSet = QrEmitGru
    Left = 108
    Top = 116
  end
  object DsDefPecas1: TDataSource
    DataSet = QrDefPecas1
    Left = 160
    Top = 120
  end
  object QrDefPecas1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * From DefPecas')
    Left = 160
    Top = 76
    object QrDefPecas1Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.defpecas.Codigo'
    end
    object QrDefPecas1Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.defpecas.Nome'
      Size = 6
    end
    object QrDefPecas1Grandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'DBMBWET.defpecas.Grandeza'
    end
    object QrDefPecas1Lk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMBWET.defpecas.Lk'
    end
  end
  object DsEspessuras1: TDataSource
    DataSet = QrEspessuras1
    Left = 312
    Top = 124
  end
  object QrEspessuras1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * From Espessuras')
    Left = 312
    Top = 80
    object QrEspessuras1Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.espessuras.Codigo'
    end
    object QrEspessuras1Linhas: TWideStringField
      DisplayWidth = 20
      FieldName = 'Linhas'
      Origin = 'DBMBWET.espessuras.Linhas'
    end
    object QrEspessuras1EMCM: TFloatField
      FieldName = 'EMCM'
      Origin = 'DBMBWET.espessuras.EMCM'
    end
    object QrEspessuras1Lk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMBWET.espessuras.Lk'
    end
  end
  object QrDefPecas2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * From DefPecas')
    Left = 232
    Top = 80
    object QrDefPecas2Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.defpecas.Codigo'
    end
    object QrDefPecas2Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.defpecas.Nome'
      Size = 6
    end
    object QrDefPecas2Grandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'DBMBWET.defpecas.Grandeza'
    end
    object QrDefPecas2Lk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMBWET.defpecas.Lk'
    end
  end
  object DsDefPecas2: TDataSource
    DataSet = QrDefPecas2
    Left = 232
    Top = 124
  end
  object QrEspessuras2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * From Espessuras')
    Left = 408
    Top = 80
    object QrEspessuras2Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.espessuras.Codigo'
    end
    object QrEspessuras2Linhas: TWideStringField
      DisplayWidth = 20
      FieldName = 'Linhas'
      Origin = 'DBMBWET.espessuras.Linhas'
    end
    object QrEspessuras2EMCM: TFloatField
      FieldName = 'EMCM'
      Origin = 'DBMBWET.espessuras.EMCM'
    end
    object QrEspessuras2Lk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMBWET.espessuras.Lk'
    end
  end
  object DsEspessuras2: TDataSource
    DataSet = QrEspessuras2
    Left = 408
    Top = 124
  end
  object QrRebaixe: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Linhas, EMCM'
      'FROM espessuras'
      'ORDER BY EMCM')
    Left = 440
    Top = 65524
    object QrRebaixeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRebaixeLinhas: TWideStringField
      FieldName = 'Linhas'
      Size = 7
    end
    object QrRebaixeEMCM: TFloatField
      FieldName = 'EMCM'
    end
  end
  object DsRebaixe: TDataSource
    DataSet = QrRebaixe
    Left = 440
    Top = 36
  end
  object QrGraCorCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gracorcad'
      'ORDER BY Nome')
    Left = 496
    Top = 65528
    object QrGraCorCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCorCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGraCorCad: TDataSource
    DataSet = QrGraCorCad
    Left = 492
    Top = 40
  end
end
