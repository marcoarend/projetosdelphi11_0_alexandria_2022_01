unit WBRclIns;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DBCtrls, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkEditCalc,
  ComCtrls, dmkEditDateTimePicker, dmkLabel, UnInternalConsts, dmkImage,
  UnDmkEnums, UnAppEnums;

type
  TGrupoPallet = (grupalA, grupalB);
  TFmWBRclIns = class(TForm)
    Panel1: TPanel;
    QrGraGruXA: TmySQLQuery;
    QrGraGruXANivel3: TIntegerField;
    QrGraGruXANivel2: TIntegerField;
    QrGraGruXANivel1: TIntegerField;
    QrGraGruXANome: TWideStringField;
    QrGraGruXAPrdGrupTip: TIntegerField;
    QrGraGruXAGraTamCad: TIntegerField;
    QrGraGruXANOMEGRATAMCAD: TWideStringField;
    QrGraGruXACODUSUGRATAMCAD: TIntegerField;
    QrGraGruXACST_A: TSmallintField;
    QrGraGruXACST_B: TSmallintField;
    QrGraGruXAUnidMed: TIntegerField;
    QrGraGruXANCM: TWideStringField;
    QrGraGruXAPeso: TFloatField;
    QrGraGruXASIGLAUNIDMED: TWideStringField;
    QrGraGruXACODUSUUNIDMED: TIntegerField;
    QrGraGruXANOMEUNIDMED: TWideStringField;
    QrGraGruXAHowBxaEstq: TSmallintField;
    QrGraGruXAGerBxaEstq: TSmallintField;
    QrGraGruXAFatorClas: TIntegerField;
    DsGraGruXA: TDataSource;
    QrForneceA: TmySQLQuery;
    DsForneceA: TDataSource;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    EdForneceA: TdmkEditCB;
    CBForneceA: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdGraGruXA: TdmkEditCB;
    CBGraGruXA: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    QrForneceB: TmySQLQuery;
    DsForneceB: TDataSource;
    DsGraGruXB: TDataSource;
    QrGraGruXB: TmySQLQuery;
    QrGraGruXBNivel3: TIntegerField;
    QrGraGruXBNivel2: TIntegerField;
    QrGraGruXBNivel1: TIntegerField;
    QrGraGruXBNome: TWideStringField;
    QrGraGruXBPrdGrupTip: TIntegerField;
    QrGraGruXBGraTamCad: TIntegerField;
    QrGraGruXBHowBxaEstq: TSmallintField;
    QrGraGruXBGerBxaEstq: TSmallintField;
    QrGraGruXBNOMEGRATAMCAD: TWideStringField;
    QrGraGruXBCODUSUGRATAMCAD: TIntegerField;
    QrGraGruXBCST_A: TSmallintField;
    QrGraGruXBCST_B: TSmallintField;
    QrGraGruXBUnidMed: TIntegerField;
    QrGraGruXBNCM: TWideStringField;
    QrGraGruXBPeso: TFloatField;
    QrGraGruXBFatorClas: TIntegerField;
    QrGraGruXBSIGLAUNIDMED: TWideStringField;
    QrGraGruXBCODUSUUNIDMED: TIntegerField;
    QrGraGruXBNOMEUNIDMED: TWideStringField;
    Panel3: TPanel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label7: TLabel;
    EdEmpresa: TdmkEditCB;
    Panel4: TPanel;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    LaPeso: TLabel;
    EdPecasA: TdmkEdit;
    EdAreaM2A: TdmkEditCalc;
    EdAreaP2A: TdmkEditCalc;
    EdPesoKgA: TdmkEdit;
    Panel5: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdPecasB: TdmkEdit;
    EdAreaM2B: TdmkEditCalc;
    EdAreaP2B: TdmkEditCalc;
    EdPesoKgB: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    Label12: TLabel;
    EdHora: TdmkEdit;
    QrGraGruXAGraGruX: TIntegerField;
    QrGraGruXBGraGruX: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    CkContinuar: TCheckBox;
    BtSaida: TBitBtn;
    QrForneceBCodigo: TIntegerField;
    QrForneceBNOMEENTIDADE: TWideStringField;
    QrForneceACodigo: TIntegerField;
    QrForneceANOMEENTIDADE: TWideStringField;
    Label15: TLabel;
    EdPalletA: TdmkEditCB;
    QrWBPalletB: TmySQLQuery;
    QrWBPalletBCodigo: TIntegerField;
    QrWBPalletBNome: TWideStringField;
    DsWBPalletB: TDataSource;
    Label16: TLabel;
    EdPalletB: TdmkEditCB;
    CBPalletB: TdmkDBLookupComboBox;
    SBPallet: TSpeedButton;
    EdCodigo: TdmkEdit;
    Label14: TLabel;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Panel2: TPanel;
    Label4: TLabel;
    EdForneceB: TdmkEditCB;
    CBForneceB: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdGraGruXB: TdmkEditCB;
    CBGraGruXB: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdControleB: TdmkEdit;
    EdValorTA: TdmkEdit;
    Label18: TLabel;
    EdControleA: TdmkEdit;
    Label2: TLabel;
    EdValorTB: TdmkEdit;
    Label19: TLabel;
    CBPalletA: TdmkDBLookupComboBox;
    QrWBPalletA: TmySQLQuery;
    QrWBPalletACodigo: TIntegerField;
    QrWBPalletANome: TWideStringField;
    DsWBPalletA: TDataSource;
    Label17: TLabel;
    EdMovimTwn: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGruXAChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPecasAChange(Sender: TObject);
    procedure EdAreaM2AChange(Sender: TObject);
    procedure EdAreaP2AChange(Sender: TObject);
    procedure EdPesoKgAChange(Sender: TObject);
    procedure SBPalletClick(Sender: TObject);
    procedure EdValorTAChange(Sender: TObject);
    procedure EdForneceARedefinido(Sender: TObject);
    procedure EdGraGruXARedefinido(Sender: TObject);
    procedure EdForneceBRedefinido(Sender: TObject);
    procedure EdGraGruXBRedefinido(Sender: TObject);
    procedure EdForneceAChange(Sender: TObject);
  private
    { Private declarations }
    //FGraGru1A, FGraGru1B: Integer;
    FDescosideraFocus: Boolean;
    procedure HabilitaOK();
    procedure ReopenWBPallet(GrupoPallet: TGrupoPallet);

  public
    { Public declarations }
    FSMIMultIns: Integer;
    FForcaReopen: Boolean;
    FSrcMovID, FSrcNivel1, FSrcNivel2: Integer;
    FQrReopen: TmySQLQuery;
    FValorT, FAreaM2, FPrecoM2: Double;
    FSaldoPecas: Double;
    FWBRclass: TWBRclass;
  end;

  var
  FmWBRclIns: TFmWBRclIns;

implementation

uses UnMyObjects, ModuleGeral, UMySQLModule, Module, UnGrade_Tabs, MPReclasGer,
(*MPReclasImp,*) DmkDAC_PF, Principal, UnWB_PF;

{$R *.DFM}

procedure TFmWBRclIns.BtOKClick(Sender: TObject);
  function ObtemCodigo(Codigo: Integer; Tabela: String): Integer;
  begin
    Result := UMyMod.BPGS1I32(
        Tabela, 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  end;
  function ObtemMovimCod(MovimCod: Integer): Integer;
  begin
    Result := UMyMod.BPGS1I32(
        'wbmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  end;
  function ObtemMovimTwn(MovimTwn: Integer): Integer;
  begin
    Result := UMyMod.BPGS1I32(
        'wbmovits', 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
  end;
const
  Observ = '';
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda = 0;
var
  DataHora: String;
  Pallet, Codigo, Controle, MovimCod, MovimTwn, Empresa, GraGruX, Fornecedor:
  Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  Tabela: String;
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa,
    'Informe a empresa!') then Exit;
  //
  if MyObjects.FIC(EdGraGruXA.ValueVariant = 0, EdGraGruXA,
    'Informe o produto de origem!') then Exit;
  //
  if MyObjects.FIC(EdGraGruXB.ValueVariant = 0, EdGraGruXB,
    'Informe o produto de destino!') then Exit;
  //
  case FWBRclass of
    wbrEntrada: Tabela := 'wbinncab';
    wbrEstoque: Tabela := 'wbrclcab';
    else Tabela := '?FWBRclass?';
  end;
  //
  DataHora       := Geral.FDT(TPData.Date, 1) + ' ' + EdHora.Text;
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControleA.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Fornecedor     := EdForneceA.ValueVariant;
  MovimID        := emidReclasWE;
  MovimNiv       := eminSorcClass;
  Pallet         := EdPalletA.ValueVariant;
  GraGruX        := EdGragruXA.ValueVariant;
  Pecas          := -EdPecasA.ValueVariant;
  PesoKg         := -EdPesoKgA.ValueVariant;
  AreaM2         := -EdAreaM2A.ValueVariant;
  AreaP2         := -EdAreaP2A.ValueVariant;
  ValorT         := -EdValorTA.ValueVariant;
  //
  //
  if Dmod.WBFic(GraGruX, Empresa, Fornecedor, Pallet, Pecas, AreaM2, PesoKg, ValorT,
  EdGraGruXA, EdPalletA, EdPecasA, EdAreaM2A, EdValorTA) then
    Exit;
  if Dmod.WBFic(EdGraGruXB.ValueVariant, Empresa, Fornecedor, EdPalletB.ValueVariant,
  EdPecasB.ValueVariant, EdAreaM2B.ValueVariant, EdPesoKgB.ValueVariant,
  EdValorTB.ValueVariant, EdGraGruXB, EdPalletB, EdPecasB, EdAreaM2B, EdValorTB) then
    Exit;
  //
  case FWBRclass of
    wbrEntrada:
    begin
      Codigo   := ObtemCodigo(Codigo, Tabela);
      MovimCod := ObtemMovimCod(MovimCod);
      UMyMod.BPGS1I32(
        'wbmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
    end;
    wbrEstoque:
    begin
      Codigo   := FSrcNivel1;
      //MovimCod := ObtemMovimCod(MovimCod);
      MovimCod := FSrcMovID;
    end
    else
    begin
      Geral.MB_Erro('"FWBRclass" n�o definido!');
      Exit;
    end;
  end;
  MovimTwn := ObtemMovimTwn(MovimTwn);
  //
  //
  Controle := UMyMod.BPGS1I32('wbmovits', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if Dmod.InsUpdWBMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, FSrcMovID, FSrcNivel1, FSrcNivel2, Observ,
  LnkNivXtr1, LnkNivXtr2, CliVenda, Controle) then
  begin
    Controle       := EdControleB.ValueVariant;
    Fornecedor     := EdForneceB.ValueVariant;
    MovimID        := emidReclasWE;
    MovimNiv       := eminDestClass;
    Pallet         := EdPalletB.ValueVariant;
    GraGruX        := EdGragruXB.ValueVariant;
    Pecas          := EdPecasB.ValueVariant;
    PesoKg         := EdPesoKgB.ValueVariant;
    AreaM2         := EdAreaM2B.ValueVariant;
    AreaP2         := EdAreaP2B.ValueVariant;
    ValorT         := EdValorTB.ValueVariant;
    //
    Controle := UMyMod.BPGS1I32('wbmovits', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
    if Dmod.InsUpdWBMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
    Empresa, Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg,
    AreaM2, AreaP2, ValorT, DataHora, FSrcMovID,
    FSrcNivel1, FSrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
    CliVenda, Controle) then
    begin
     Controle := FSrcNivel2;
     Dmod.AtualizaSaldoVirtualWBMovIts(Controle);
     //
     if CkContinuar.Checked then
     begin
       ImgTipo.SQLType         := stIns;
       FDescosideraFocus       := True;
       EdCodigo.ValueVariant   := 0;
       EdMovimCod.ValueVariant := 0;
       //
       EdControleA.ValueVariant := 0;
       {
       EdGraGruXA.ValueVariant  := 0;
       CBGraGruXA.KeyValue      := 0;
       EdPalletA.ValueVariant   := '';
       }
       EdPecasA.ValueVariant    := 0;
       EdAreaP2A.ValueVariant   := 0;
       EdPesoKgA.ValueVariant   := 0;
       EdValorTA.ValueVariant   := 0;
       //
       EdControleB.ValueVariant := 0;
       EdGraGruXB.ValueVariant  := 0;
       CBGraGruXB.KeyValue      := 0;
       EdPecasB.ValueVariant    := 0;
       EdAreaP2B.ValueVariant   := 0;
       EdPesoKgB.ValueVariant   := 0;
       EdValorTB.ValueVariant   := 0;
       EdPalletB.ValueVariant   := '';
       //
       EdHora.ValueVariant := Now();
       FDescosideraFocus := False;
       //
{
       if FForcaReopen then
         FmMPReclasGer.ReopenSMI(SMIMultIns);
}
       if EdGraGruXA.Enabled then
         EdGraGruXA.SetFocus
       else
       if EdPecasA.Enabled then
         EdPecasA.SetFocus;
       //
       if FQrReopen <> nil then
       begin
         UnDMkDAC_PF.AbreQuery(FQrReopen, FQrReopen.Database);
         FSaldoPecas := FSaldoPecas - Pecas;
         if FSaldoPecas < 0.001 then
           Close;
       end;
     end else
       Close;
    end;
  end;
end;

procedure TFmWBRclIns.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWBRclIns.EdAreaM2AChange(Sender: TObject);
begin
  EdAreaM2B.ValueVariant := EdAreaM2A.ValueVariant;
  EdValorTA.ValueVariant := EdAreaM2A.ValueVariant * FPrecoM2;
end;

procedure TFmWBRclIns.EdAreaP2AChange(Sender: TObject);
begin
  EdAreaP2B.ValueVariant := EdAreaP2A.ValueVariant;
end;

procedure TFmWBRclIns.EdForneceAChange(Sender: TObject);
begin
  EdForneceB.ValueVariant := EdForneceA.ValueVariant;
  CBForneceB.KeyValue     := EdForneceA.ValueVariant;
end;

procedure TFmWBRclIns.EdForneceARedefinido(Sender: TObject);
begin
  ReopenWBPallet(grupalA);
end;

procedure TFmWBRclIns.EdForneceBRedefinido(Sender: TObject);
begin
  ReopenWBPallet(grupalB);
end;

procedure TFmWBRclIns.EdGraGruXAChange(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmWBRclIns.EdGraGruXARedefinido(Sender: TObject);
begin
  ReopenWBPallet(grupalA);
end;

procedure TFmWBRclIns.EdGraGruXBRedefinido(Sender: TObject);
begin
  ReopenWBPallet(grupalB);
end;

procedure TFmWBRclIns.EdPecasAChange(Sender: TObject);
begin
  EdPecasB.ValueVariant := EdPecasA.ValueVariant;
end;

procedure TFmWBRclIns.EdPesoKgAChange(Sender: TObject);
begin
  EdPesoKgB.ValueVariant := EdPesoKgA.ValueVariant;
end;

procedure TFmWBRclIns.EdValorTAChange(Sender: TObject);
begin
  EdValorTB.ValueVariant := EdValorTA.ValueVariant;
end;

procedure TFmWBRclIns.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWBRclIns.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSrcMovID  := 0;
  FSrcNivel1 := 0;
  FSrcNivel2 := 0;
  FQrReopen  := nil;
  FSaldoPecas := 0;

  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  FDescosideraFocus := False;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruXA, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrForneceA, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruXB, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrForneceB, Dmod.MyDB);
  //
  TPData.Date := Date;
  EdHora.ValueVariant := Now();
end;

procedure TFmWBRclIns.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWBRclIns.HabilitaOK;
begin
  //
end;

procedure TFmWBRclIns.ReopenWBPallet(GrupoPallet: TGrupoPallet);
var
  Qry: TmySQLQuery;
  EdPallet: TdmkEditCB;
  CBPallet: TdmkDBLookupComboBox;
  Empresa, Fornece, GraGruX: Integer;
begin
  if GrupoPallet = grupalA then
  begin
    Qry      := QrWBPalletA;
    EdPallet := EdPalletA;
    CBPallet := CBPalletA;
    Fornece  := EdForneceA.ValueVariant;
    GraGruX  := EdGraGruXA.ValueVariant;
  end else
  begin
    Qry      := QrWBPalletB;
    EdPallet := EdPalletB;
    CBPallet := CBPalletB;
    Fornece  := EdForneceB.ValueVariant;
    GraGruX  := EdGraGruXB.ValueVariant;
  end;
  Empresa := DModG.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
  //
  WB_PF.ReopenWBPallet(Qry, EdPallet, CBPallet, Empresa, Fornece, GraGruX);
end;

procedure TFmWBRclIns.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormWBPallet();
  UMyMod.SetaCodigoPesquisado(EdPalletB, CBPalletB, QrWBPalletB, VAR_CADASTRO);
end;

end.
