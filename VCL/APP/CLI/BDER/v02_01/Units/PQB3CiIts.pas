unit PQB3CiIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnInternalConsts,
  UnMsgInt, ExtCtrls, UnInternalConsts2, mySQLDbTables, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, UMySQLModule, dmkGeral, dmkLabel, dmkImage, UnDmkEnums,
  UnDmkProcFunc, DmkDAC_PF, Vcl.ComCtrls, dmkEditDateTimePicker, dmkCheckBox;

type
  TFmPQB3CiIts = class(TForm)
    PainelDados: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    QrSaldo: TMySQLQuery;
    QrSaldoPeso: TFloatField;
    QrSaldoValor: TFloatField;
    QrSaldoCUSTO: TFloatField;
    QrSaldoCI: TIntegerField;
    QrSaldoPQ: TIntegerField;
    DsSaldo: TDataSource;
    Qry: TMySQLQuery;
    PnDadosAdx: TPanel;
    Label1: TLabel;
    EdSerie: TdmkEdit;
    EdNF: TdmkEdit;
    Label12: TLabel;
    EdxLote: TdmkEdit;
    SpeedButton1: TSpeedButton;
    Panel5: TPanel;
    LaEmpresa: TLabel;
    LaCliente: TLabel;
    LaInsumo: TLabel;
    Panel6: TPanel;
    EdPesoInfo: TdmkEdit;
    EdPreco: TdmkEdit;
    Label10: TLabel;
    Label4: TLabel;
    CkLancar: TdmkCheckBox;
    EdPesoAtuT: TdmkEdit;
    Label2: TLabel;
    EdPesoDife: TdmkEdit;
    Label7: TLabel;
    EdValorDife: TdmkEdit;
    LaValorDife: TLabel;
    EdValorInfo: TdmkEdit;
    LaValorInfo: TLabel;
    EdTipo: TdmkEdit;
    Label9: TLabel;
    EdDstCodi: TdmkEdit;
    Label11: TLabel;
    EdDstCtrl: TdmkEdit;
    Label13: TLabel;
    EdEmpresa: TdmkEdit;
    EdCliInt: TdmkEdit;
    EdInsumo: TdmkEdit;
    EdNO_Empresa: TdmkEdit;
    EdNO_CliInt: TdmkEdit;
    EdNO_Insumo: TdmkEdit;
    EdValorAtuT: TdmkEdit;
    Label14: TLabel;
    SbValorInfo: TSpeedButton;
    SbValorDife: TSpeedButton;
    Label3: TLabel;
    TPdFab: TdmkEditDateTimePicker;
    TPdVal: TdmkEditDateTimePicker;
    Label5: TLabel;
    PnDadosBxa: TPanel;
    Label17: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit9: TDBEdit;
    EdSaldoFut: TdmkEdit;
    Label20: TLabel;
    PnVazio: TPanel;
    QrExiste: TMySQLQuery;
    QrExisteControle: TIntegerField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdPrecoExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdInsumoEnter(Sender: TObject);
    procedure EdCliIntEnter(Sender: TObject);
    procedure EdInsumoExit(Sender: TObject);
    procedure EdInsumoChange(Sender: TObject);
    procedure EdCliIntChange(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdValorInfoExit(Sender: TObject);
    procedure EdValorDifeExit(Sender: TObject);
    procedure EdPesoDifeRedefinido(Sender: TObject);
    procedure EdPesoInfoExit(Sender: TObject);
    procedure EdPesoDifeExit(Sender: TObject);
    procedure EdValorInfoEnter(Sender: TObject);
    procedure EdValorDifeEnter(Sender: TObject);
    procedure SbValorInfoClick(Sender: TObject);
    procedure SbValorDifeClick(Sender: TObject);
    procedure CkLancarClick(Sender: TObject);
  private
    { Private declarations }
    FValorInfo, FValorDife: Double;
    //
    procedure BuscaPrecoCadastro();
    procedure CalculaOnEdit(Tipo: Integer);
    procedure CalculaSaldoFuturo();
    procedure ConfiguraComponentes();

  public
    { Public declarations }
    FPQ, FCI, FEmpresa, FConta, FCodigo, FAntLancar: Integer;
    FDataX: TDateTime;
  end;

var
  FmPQB3CiIts: TFmPQB3CiIts;

implementation

uses UnMyObjects, Module, PQB3, PQB3AdSel, PQx, UnPQ_PF, ModuleGeral, MyDBCheck;

{$R *.DFM}


procedure TFmPQB3CiIts.CalculaOnEdit(Tipo: Integer);
var
  PesoInfo, PesoDife, Preco, ValorInfo, ValorDife: Double;
begin
  PesoInfo := Geral.DMV(EdPesoInfo.Text);
  PesoDife := Geral.DMV(EdPesoDife.Text);
  Preco := Geral.DMV(EdPreco.Text);
(*
  ValorInfo := Geral.DMV(EdValorInfo.Text);
  ValorDife := Geral.DMV(EdValorDife.Text);
  //
  if Tipo = 0 then
  begin
    ValorInfo := PesoInfo * Preco;
    ValorDife := PesoDife * Preco;
  end else
  if Tipo = 1 then
  begin
    if FValorInfo <> ValorInfo then
      if PesoInfo <> 0 then
        PrecoDife := ValorInfo / PesoInfo;
    ValorDife := PesoDife * Preco;
  end else
  if Tipo = 2 then
  begin
    if PesoDife <> 0 then
      Preco := ValorDife / PesoDife;
    ValorInfo := PesoInfo * Preco;
  end
  else Preco := 0;
*)
  ValorInfo := PesoInfo * Preco;
  ValorDife := PesoDife * Preco;
  //
  //EdPreco.ValueVariant := Preco;
  EdPesoInfo.ValueVariant := PesoInfo;
  EdValorInfo.ValueVariant := ValorInfo;
  EdPesoDife.ValueVariant := PesoDife;
  EdValorDife.ValueVariant := ValorDife;
end;

procedure TFmPQB3CiIts.CalculaSaldoFuturo();
var
  CI, PQ: Integer;
  Qtde, SaldoFut: Double;
begin
  CI := EdCliInt.ValueVariant;
  PQ := EdInsumo.ValueVariant;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSaldo, Dmod.MyDB, [
  'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO ',
  'FROM pqcli ',
  'WHERE CI=' + Geral.FF0(CI),
  'AND PQ=' + Geral.FF0(PQ),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  '']);
  //
  Qtde := EdPesoDife.ValueVariant;
  SaldoFut := QrSaldoPeso.Value + Qtde;
  EdSaldoFut.ValueVariant := SaldoFut;
  //
end;


procedure TFmPQB3CiIts.CkLancarClick(Sender: TObject);
begin
  ConfiguraComponentes();
end;

procedure TFmPQB3CiIts.EdPesoDifeExit(Sender: TObject);
begin
  EdPesoInfo.ValueVariant := EdPesoAtuT.ValueVariant + EdPesoDife.ValueVariant;
  CalculaOnEdit(0);
end;

procedure TFmPQB3CiIts.EdPesoDifeRedefinido(Sender: TObject);
begin
  ConfiguraComponentes();
  //
  CalculaSaldoFuturo();
end;

procedure TFmPQB3CiIts.EdPesoInfoExit(Sender: TObject);
begin
  EdPesoDife.ValueVariant := EdPesoInfo.ValueVariant - EdPesoAtuT.ValueVariant;
  CalculaOnEdit(0);
end;

procedure TFmPQB3CiIts.EdValorDifeEnter(Sender: TObject);
begin
  FValorDife := EdValorDife.ValueVariant;
end;

procedure TFmPQB3CiIts.EdValorDifeExit(Sender: TObject);
begin
  CalculaOnEdit(2);
end;

procedure TFmPQB3CiIts.EdValorInfoEnter(Sender: TObject);
begin
  FValorInfo := EdValorInfo.ValueVariant;
end;

procedure TFmPQB3CiIts.EdValorInfoExit(Sender: TObject);
begin
  CalculaOnEdit(1);
end;

procedure TFmPQB3CiIts.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQB3CiIts.BuscaPrecoCadastro();
begin
  // Buscar pre�o do estoque e n�o do cadastro!
  EXIT;
  //
  if ImgTipo.SQLType = stIns then
  begin
    FPQ := EdInsumo.ValueVariant;
    FCI := EdCliInt.ValueVariant;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT CustoPadrao FROM pqcli');
    Dmod.QrAux.SQL.Add('WHERE PQ=:P0');
    Dmod.QrAux.SQL.Add('AND CI=:P1');
    Dmod.QrAux.Params[00].AsInteger := FPQ;
    Dmod.QrAux.Params[01].AsInteger := FCI;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    EdPreco.ValueVariant := Dmod.QrAux.FieldByName('CustoPadrao').AsFloat;
    CalculaOnEdit(0);
    //
  end;
end;

procedure TFmPQB3CiIts.BtConfirmaClick(Sender: TObject);
const
  HowLoad = 0;
  DtCorrApo = '0000-00-00';
  Unitario = True;
var
  xLote, dFab, dVal: String;
  Conta, Tipo, DstCodi, DstCtrl,
  Serie, NF, Lancar: Integer;
  PesoAtuT, PesoInfo, ValorInfo, PesoDife, ValorDife: Double;
  SQLType, MovSQLType: TSQLType;
  //
  Controle, OriCtrl, Codigo, CliInt, Empresa, Insumo, ide_serie, ide_nNF,
  OriCodi, OriTipo, CliOrig, CliDest, PQBCiIts: Integer;
  Peso, Valor, SaldoFut: Double;
  DataX, Tabela: String;
  PesPos, PesNeg: Boolean;
  //
  procedure ExcluiMovimento(_Tipo: Integer);
  var
    _CliInt, _Insumo, _OriCodi, _OriCtrl, _Empresa: Integer;
  begin
    _CliInt   := EdCliInt.ValueVariant;
    _Insumo   := EdInsumo.ValueVariant;
    _OriCodi  := FCodigo;
    //OriCtrl  :=
    _OriCtrl := EdDstCtrl.ValueVariant;
    //OriTipo  := EdTipo.ValueVariant;
    _Empresa  := FEmpresa;
    PQ_PF.ExcluiPQx_Itm(
      _CliInt, _Insumo, _OriCodi, _OriCtrl, _Tipo, True, _Empresa);
  end;

begin
  SQLType        := ImgTipo.SQLType;
  Conta          := FConta;
  Tipo           := EdTipo.ValueVariant;
  DstCodi        := EdDstCodi.ValueVariant;
  DstCtrl        := EdDstCtrl.ValueVariant;
  PesoInfo       := EdPesoInfo.ValueVariant;
  ValorInfo      := EdValorInfo.ValueVariant;
  PesoDife       := EdPesoDife.ValueVariant;
  ValorDife      := EdValorDife.ValueVariant;
  Serie          := 0;
  NF             := 0;
  xLote          := EmptyStr;
  dFab           := '0000-00-00';
  dVal           := '0000-00-00';
  Lancar         := Geral.BoolToInt(CkLancar.Checked);
  PesPos         := PesoDife >= 0.001;
  PesNeg         := PesoDife <= -0.001;
  //
  if (
   (Tipo = 20) and  (PesNeg =  True)
  ) or (
   (Tipo = 120) and  (PesPos =  True)
  ) then
  begin
    Geral.MB_Aviso(
    'N�o � possivel alterar de baixa para re-entrada (ou vice versa) diretamente!' +
    sLineBreak + 'Primeiramete zere o peso e confirme e volte a editar!');
    Exit;
  end;
  // Permitir zero e excluir lct atrelado se negativo ou se positivo sem baixa!
  //if MyObjects.FIC(PesoDife = 0, EdPesoDife, 'Qtde diferen�a inv�lida!') then Exit;
  if PesPos = True then
  begin
    Serie          := EdSerie.ValueVariant;
    NF             := EdNF.ValueVariant;
    xLote          := EdxLote.ValueVariant;
    dFab           := Geral.FDT(TPdFab.Date, 1);
    dVal           := Geral.FDT(TPdVal.Date, 1);
    // For�ar visualiza��o!
    //PnDadosAdx.Visible = True; Precisa?
    if MyObjects.FIC(NF = 0, EdNF, 'Informe a NF (VP)!') then Exit;
    if MyObjects.FIC(xLote = EmptyStr, EdxLote, 'Informe o Lote!') then Exit;
  end else
  if PesNeg = True then
  begin
    SaldoFut := EdSaldoFut.ValueVariant;
    if MyObjects.FIC(SaldoFut < 0, EdPesoDife, 'Saldo insuficiente!') then Exit;
  end else
  begin
    Lancar := 0;
    (*
    Tipo    := -1;
    DstCodi := 0;
    DstCtrl := 0;
    *)
  end;
  //
  Controle := EdDstCtrl.ValueVariant;
  if Controle <> 0 then
  begin
    MovSQLType := stUpd;
    OriCtrl    := Controle;
    Codigo     := EdDstCodi.ValueVariant;
    //
    if PesPos = True then
    begin
      if Tipo = VAR_FATID_0120 then
      begin
        // excluir pois passou de baixa para re-entrada
        UnDmkDAC_PF.ExecutaDB(DMod.MyDB, 'DELETE FROM pqbbxa WHERE Controle=' + Geral.FF0(Controle));
        ExcluiMovimento(VAR_FATID_0120);
      end;
    end;
    if PesNeg = True then
    begin
      if Tipo = VAR_FATID_0020 then
      begin
        // excluir pois passou re-entrada para baixa
        UnDmkDAC_PF.ExecutaDB(DMod.MyDB, 'DELETE FROM pqbadx WHERE Controle=' + Geral.FF0(Controle));
        ExcluiMovimento(VAR_FATID_0020);
      end;
    end;
    if Lancar = 0 then
    begin
      if Tipo = VAR_FATID_0120 then
      begin
        // excluir pois passou de baixa para re-entrada
        UnDmkDAC_PF.ExecutaDB(DMod.MyDB, 'DELETE FROM pqbbxa WHERE Controle=' + Geral.FF0(Controle));
        ExcluiMovimento(VAR_FATID_0120);
      end;
      if Tipo = VAR_FATID_0020 then
      begin
        // excluir pois passou re-entrada para baixa
        UnDmkDAC_PF.ExecutaDB(DMod.MyDB, 'DELETE FROM pqbadx WHERE Controle=' + Geral.FF0(Controle));
        ExcluiMovimento(VAR_FATID_0020);
      end;
    end;
  end else
  begin
    MovSQLType := stIns;
    if Lancar = 1 then
    begin
      if PesPos = True then
      begin
        Controle   := UMyMod.BPGS1I32('pqbadx', 'Controle', '', '', tsPos, MovSQLType, Controle);
        Tipo := VAR_FATID_0020;
      end else
      if PesNeg = True then
      begin
        Controle   := UMyMod.BPGS1I32('pqbbxa', 'Controle', '', '', tsPos, MovSQLType, Controle);
        Tipo := VAR_FATID_0120;
      end;
    end;
    OriCtrl    := Controle;
    Codigo     := FCodigo;
  end;
  //
  if (PesNeg =  False) and (PesPos = False) then
  begin
    Tipo    := -1;
    DstCodi := 0;
    DstCtrl := 0;
    Serie   := 0;
    NF      := 0;
    xLote   := EmptyStr;
    dFab    := '0000-00-00';
    dVal    := '0000-00-00';
    Lancar  := 0;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqbciits', False, [
    'Tipo',
    'DstCodi', 'DstCtrl', 'PesoInfo',
    'ValorInfo', 'PesoDife', 'ValorDife',
    'Serie', 'NF', 'xLote',
    'dFab', 'dVal', 'Lancar'], [
    'Conta'], [
    Tipo,
    DstCodi, DstCtrl, PesoInfo,
    ValorInfo, PesoDife, ValorDife,
    Serie, NF, xLote,
    dFab, dVal, Lancar], [
    Conta], True) then
    begin
      Close;
    end;
    //
    Exit;
  end;
  //
  if Lancar = 1 then
  begin
    DstCodi := FCodigo;
    DstCtrl := OriCtrl;
  end else begin
    DstCodi := 0;
    DstCtrl := 0;
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqbciits', False, [
  'Tipo',
  'DstCodi', 'DstCtrl', 'PesoInfo',
  'ValorInfo', 'PesoDife', 'ValorDife',
  'Serie', 'NF', 'xLote',
  'dFab', 'dVal', 'Lancar'], [
  'Conta'], [
  Tipo,
  DstCodi, DstCtrl, PesoInfo,
  ValorInfo, PesoDife, ValorDife,
  Serie, NF, xLote,
  dFab, dVal, Lancar], [
  Conta], True) then
  begin
    Tipo      := EdTipo.ValueVariant;
    CliInt    := EdCliInt.ValueVariant;
    Empresa   := FEmpresa;
    Insumo    := EdInsumo.ValueVariant;
    Valor     := EdValorDife.ValueVariant;
    ide_serie := Serie;
    ide_nNF   := NF;
    OriCodi   := Codigo;
    DataX     := Geral.FDT(FDataX, 1);
    CliOrig   := CliInt;
    CliDest   := CliInt;
    PQBCiIts  := Conta;
    //
    if Lancar = 1 then
    begin
      if FAntLancar = 0 then
        MovSQLType := stIns;
      if MovSQLType = stUpd then
      begin
        if PesPos = True then
          Tabela := 'pqbadx'
        else
          Tabela := 'pqbbxa';
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
        'SELECT Controle FROM ' + Tabela + ' WHERE Controle=' + Geral.FF0(Controle)]);
        if QrExisteControle.Value = 0 then
          MovSQLType := stIns;
      end;
      if PesPos = True then
      begin
        Peso    := PesoDife;
        Valor   := ValorDife;
        OriTipo := VAR_FATID_0020;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, MovSQLType, 'pqbadx', False, [
        'Codigo', 'Empresa', 'CliInt',
        'Insumo', 'Peso', 'Valor',
        'ide_serie', 'ide_nNF', 'DtCorrApo',
        'xLote', 'dFab', 'dVal',
        'HowLoad', 'PQBCiIts'], [
        'Controle'], [
        Codigo, Empresa, CliInt,
        Insumo, Peso, Valor,
        ide_serie, ide_nNF, DtCorrApo,
        xLote, dFab, dVal,
        HowLoad, PQBCiIts], [
        Controle], True) then
        begin
          //if MovSQLType = stUpd then
            //PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, True, Empresa);
            ExcluiMovimento(VAR_FATID_0020);
          //
          PQ_PF.InserePQx_Inn(Qry, DataX, CliOrig, CliDest, Insumo, Peso, Valor,
            HowLoad, OriCodi, OriCtrl, OriTipo, DtCorrApo, Empresa,
            ide_serie, ide_nNF, xLote, dFab, dVal);
          UnPQx.AtualizaEstoquePQ(CliOrig, Insumo, Empresa, aeMsg, CO_VAZIO);
        end;
      end else
      if PesNeg = True then
      begin
        Peso  := -PesoDife;
        Valor := -ValorDife;
        OriTipo := VAR_FATID_0120;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, MovSQLType, 'pqbbxa', False, [
        'Codigo', 'Empresa', 'CliInt',
        'Insumo', 'Peso', 'Valor',
        'DtCorrApo', 'PQBCiIts'], [
        'Controle'], [
        Codigo, Empresa, CliInt,
        Insumo, Peso, Valor,
        DtCorrApo, PQBCiIts], [
        Controle], True) then
        begin
          //if MovSQLType = stUpd then
            //PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, True, Empresa);
            ExcluiMovimento(VAR_FATID_0120);
          //
          PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, -Peso, -Valor, OriCodi,
            OriCtrl, OriTipo, Unitario, DtCorrApo, Empresa);
        end;
      end;
    end else
    begin
      //excluir pqx e pqw
      ExcluiMovimento(Tipo)
    end;
    Close;
  end;
end;

procedure TFmPQB3CiIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  CalculaOnEdit(1);
  CalculaOnEdit(2);
  //EdPreco.SetFocus;
  EdPesoInfo.SetFocus;
end;

procedure TFmPQB3CiIts.EdCliIntChange(Sender: TObject);
begin
  if not EdCliInt.Focused then
    BuscaPrecoCadastro();
end;

procedure TFmPQB3CiIts.EdCliIntEnter(Sender: TObject);
begin
  FCI := EdCliInt.ValueVariant;
end;

procedure TFmPQB3CiIts.EdEmpresaRedefinido(Sender: TObject);
var
  Filial: Integer;
begin
  Filial := EdEmpresa.ValueVariant;
  FEmpresa := DModG.ObtemEntidadeDeFilial(Filial);
end;

procedure TFmPQB3CiIts.EdInsumoChange(Sender: TObject);
begin
  if not EdInsumo.Focused then
    BuscaPrecoCadastro();
end;

procedure TFmPQB3CiIts.EdInsumoEnter(Sender: TObject);
begin
  FPQ := EdInsumo.ValueVariant;
end;

procedure TFmPQB3CiIts.EdInsumoExit(Sender: TObject);
begin
  if FPQ <> EdInsumo.ValueVariant then
    BuscaPrecoCadastro();
end;

procedure TFmPQB3CiIts.EdPrecoExit(Sender: TObject);
begin
  CalculaOnEdit(0);
end;

procedure TFmPQB3CiIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQB3CiIts.ConfiguraComponentes();
var
  PesPos, PesNeg: Boolean;
begin
  PesPos := EdPesoDife.ValueVariant >= 0.001;
  PesNeg := EdPesoDife.ValueVariant <= -0.001;
  //
  CkLancar.Enabled := (PesPos = True) or (PesNeg = True);
  //PnVazio.Visible := True;
  //
  PnDadosAdx.Visible := PesPos; // and (CkLancar.Checked = True);
  PnDadosBxa.Visible := PesNeg;
  PnVazio.Visible := (PesPos = False) and (PesNeg = False);
end;

procedure TFmPQB3CiIts.SbValorDifeClick(Sender: TObject);
begin
  LaValorDife.Enabled := True;
  EdValorDife.Enabled := True;
  EdValorDife.SetFocus;
end;

procedure TFmPQB3CiIts.SbValorInfoClick(Sender: TObject);
begin
  LaValorInfo.Enabled := True;
  EdValorInfo.Enabled := True;
  EdValorInfo.SetFocus;
end;

procedure TFmPQB3CiIts.SpeedButton1Click(Sender: TObject);
var
  Empresa, CliInt, Insumo: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  CliInt  := EdCliInt.ValueVariant;
  Insumo  := EdInsumo.ValueVariant;
  //
  if DBCheck.CriaFm(TFmPQB3AdSel, FmPQB3AdSel, afmoNegarComAviso) then
  begin
    FmPQB3AdSel.EdEmpresa.ValueVariant := Empresa;
    FmPQB3AdSel.EdNO_Empresa.Text      := EdNO_Empresa.Text;
    FmPQB3AdSel.EdCliInt.ValueVariant  := CliInt;
    FmPQB3AdSel.EdNO_CliInt.Text       := EdNO_CliInt.Text;
    FmPQB3AdSel.EdInsumo.ValueVariant  := Insumo;
    FmPQB3AdSel.EdNO_Insumo.Text       := EdNO_Insumo.Text;
    //
    FmPQB3AdSel.ReopenPqx(Empresa, CliInt, Insumo);
    //
    FmPQB3AdSel.ShowModal;
    //
    if FmPQB3AdSel.FSelecionou = True then
    begin
      EdSerie.ValueVariant := FmPQB3AdSel.QrPqxSerie.Value;
      EdNF.ValueVariant    := FmPQB3AdSel.QrPqxNF.Value;
      EdxLote.ValueVariant := FmPQB3AdSel.QrPqxxLote.Value;
      EdPreco.ValueVariant := FmPQB3AdSel.QrPqxCustoUnit.Value;
      TPdFab.Date          := FmPQB3AdSel.QrPqxdFab.Value;
      TPdVal.Date          := FmPQB3AdSel.QrPqxdVal.Value;
    end;
    //
    FmPQB3AdSel.Destroy;
  end;
end;

procedure TFmPQB3CiIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TPdFab.Date := 0;
  TPdVal.Date := 0;
  //
  PnDadosAdx.Align := alClient;
  PnDadosBxa.Align := alClient;
  PnVazio.Align := alClient;
end;

end.
