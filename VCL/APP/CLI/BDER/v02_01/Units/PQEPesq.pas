unit PQEPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, Db, (*DBTables,*) StdCtrls, Buttons,
  ComCtrls, DBCtrls, UnInternalConsts, mySQLDbTables, Variants, dmkGeral,
  dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkImage, UnDmkEnums,
  dmkEditDateTimePicker, frxClass, UnDmkProcFunc, frxDBSet;

type
  TFmPQEPesq = class(TForm)
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    QrPQE: TmySQLQuery;
    DsPQE: TDataSource;
    QrFornecedores: TmySQLQuery;
    DsFornecedores: TDataSource;
    QrInsumos: TmySQLQuery;
    DsInsumos: TDataSource;
    QrInsumosCodigo: TIntegerField;
    QrInsumosNome: TWideStringField;
    QrPQECodigo: TIntegerField;
    QrPQEControle: TIntegerField;
    QrPQEConta: TIntegerField;
    QrPQEInsumo: TIntegerField;
    QrPQEVolumes: TIntegerField;
    QrPQEPesoVB: TFloatField;
    QrPQEPesoVL: TFloatField;
    QrPQEValorItem: TFloatField;
    QrPQEIPI: TFloatField;
    QrPQERIPI: TFloatField;
    QrPQETotalCusto: TFloatField;
    QrPQETotalPeso: TFloatField;
    QrPQECFin: TFloatField;
    QrPQEICMS: TFloatField;
    QrPQERICMS: TFloatField;
    QrPQENOMEFORNECEDOR: TWideStringField;
    QrPQENOMEINSUMO: TWideStringField;
    QrPQENF: TIntegerField;
    QrPQEData: TDateField;
    QrPQEConhecimento: TIntegerField;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtSaida: TBitBtn;
    QrCI: TmySQLQuery;
    DsCI: TDataSource;
    QrCICodigo: TIntegerField;
    QrCINome: TWideStringField;
    PainelDados: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    RGOrdem2: TRadioGroup;
    RGOrdem1: TRadioGroup;
    CBIQ: TdmkDBLookupComboBox;
    CBInsumo: TdmkDBLookupComboBox;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    EdLancto: TdmkEdit;
    EdIQ: TdmkEditCB;
    EdInsumo: TdmkEditCB;
    EdConhecimento: TdmkEdit;
    RGOrdem3: TRadioGroup;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    EdNF: TdmkEdit;
    EdNF_RP: TdmkEdit;
    Label10: TLabel;
    EdNF_CC: TdmkEdit;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrPQENF_RP: TIntegerField;
    QrPQENF_CC: TIntegerField;
    QrPQEIQ: TIntegerField;
    QrPQECI: TIntegerField;
    frxQUI_ENTRA_005_001: TfrxReport;
    frxDsPQE: TfrxDBDataset;
    BtImprime: TBitBtn;
    QrPQENOMECLIINT: TWideStringField;
    EdEmpresa: TdmkEditCB;
    LaEmpresa: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    LaAviso3: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdLanctoExit(Sender: TObject);
    procedure EdNFExit(Sender: TObject);
    procedure EdConhecimentoExit(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure EdIQChange(Sender: TObject);
    procedure EdInsumoChange(Sender: TObject);
    procedure TPIniClick(Sender: TObject);
    procedure TPFimClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure frxQUI_ENTRA_005_001GetValue(const VarName: string;
      var Value: Variant);
    procedure BtImprimeClick(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisa;
    procedure ReopenInsumos();

  public
    { Public declarations }
  end;

var
  FmPQEPesq: TFmPQEPesq;

implementation

{$R *.DFM}

uses Module, ModuleGeral, UnMyObjects, DmkDAC_PF;

procedure TFmPQEPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQEPesq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFornecedores, Dmod.MyDB, [
  'SELECT Codigo, ',
  'CASE WHEN Tipo=0 THEN RazaoSocial ',
  'ELSE Nome END Nome ',
  'FROM entidades ',
  'WHERE Fornece2="V" ',
  'ORDER BY Nome ',
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrCI, Dmod.MyDB, [
  'SELECT Codigo, ',
  'CASE WHEN Tipo=0 THEN RazaoSocial ',
  'ELSE Nome END Nome ',
  'FROM entidades ',
  'WHERE Cliente2="V" ',
  'ORDER BY Nome ',
  '']);
  ReopenInsumos();
  //
  TPIni.Date := Date - 90;
  TPFim.Date := Date;
end;

procedure TFmPQEPesq.EdLanctoExit(Sender: TObject);
begin
  if EdLancto.Text <> CO_VAZIO then
     EdLancto.Text := Geral.TFT(EdLancto.Text, 0, siPositivo);
  Pesquisa;
end;

procedure TFmPQEPesq.EdNFExit(Sender: TObject);
begin
  if EdNF.Text <> CO_VAZIO then
     EdNF.Text := Geral.TFT(EdNF.Text, 0, siPositivo);
  Pesquisa;
end;

procedure TFmPQEPesq.EdConhecimentoExit(Sender: TObject);
begin
  if EdConhecimento.Text <> CO_VAZIO then
     EdConhecimento.Text := Geral.TFT(EdConhecimento.Text, 0, siPositivo);
  Pesquisa;
end;

procedure TFmPQEPesq.Pesquisa();
  function Ordena(Ordem: Integer): String;
  begin
    case Ordem of
      0: Result := 'NOMECLIINT';
      1: Result := 'NOMEFORNECEDOR';
      2: Result := 'NOMEINSUMO';
      3: Result := 'pe.NF';
      4: Result := 'pe.NF_RP';
      5: Result := 'pe.NF_CC';
      6: Result := 'pe.Data';
      7: Result := 'pe.Codigo';
      8: Result := 'pe.Conhecimento';
      else Result := '???';
    end;
  end;
var
  Codigo, NF, NF_RP, NF_CC, Conhecimento, IQ, CI, Insumo, Empresa: Integer;
  Ini, Fim, Ordem, SQL_PQE, SQL_NF, SQL_NF_RP, SQL_NF_CC, SQL_Conhecimento,
  SQL_IQ, SQL_CI, SQL_Insumo: String;
begin
  MyObjects.Informa(LaAviso3, True, 'Reabrindo pesquisa');
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then
  begin
    MyObjects.Informa(LaAviso3, False, '');
    //
    Exit;
  end;
  //
  Ordem := 'ORDER BY ' +
    Ordena(RGOrdem1.ItemIndex) + ', ' +
    Ordena(RGOrdem2.ItemIndex) + ', ' +
    Ordena(RGOrdem3.ItemIndex);
(*
  case RGOrdem1.ItemIndex of
    0: Ordem1 := 'fo.Nome';
    1: Ordem1 := 'pq.Nome';
    2: Ordem1 := 'pe.NF';
    3: Ordem1 := 'pe.Data';
    4: Ordem1 := 'pe.Codigo';
    5: Ordem1 := 'pe.Conhecimento';
  end;
  case RGOrdem2.ItemIndex of
    0: Ordem2 := 'fo.Nome';
    1: Ordem2 := 'pq.Nome';
    2: Ordem2 := 'pe.NF';
    3: Ordem2 := 'pe.Data';
    4: Ordem2 := 'pe.Codigo';
    5: Ordem2 := 'pe.Conhecimento';
  end;
  case RGOrdem3.ItemIndex of
    0: Ordem3 := 'fo.Nome';
    1: Ordem3 := 'pq.Nome';
    2: Ordem3 := 'pe.NF';
    3: Ordem3 := 'pe.Data';
    4: Ordem3 := 'pe.Codigo';
    5: Ordem3 := 'pe.Conhecimento';
  end;
*)
  Ini := Geral.FDT(TPIni.Date, 1);
  Fim := Geral.FDT(TPFim.Date, 1);
  ////////////
(*
  QrPQE.Close;
  QrPQE.SQL.Clear;
  QrPQE.SQL.Add('SELECT ei.*, ');
  QrPQE.SQL.Add('CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial');
  QrPQE.SQL.Add('ELSE fo.Nome END NOMEFORNECEDOR,');
  QrPQE.SQL.Add('pq.Nome NOMEINSUMO, pe.NF, pe.Data,');
  QrPQE.SQL.Add('pe.Conhecimento');
  QrPQE.SQL.Add('FROM pqe pe, PQEIts ei,');
  QrPQE.SQL.Add('Entidades fo, PQ pq');
  QrPQE.SQL.Add('WHERE pe.Data BETWEEN :P0 AND :P1');
  QrPQE.SQL.Add('AND ei.Codigo=pe.Codigo');
  QrPQE.SQL.Add('AND fo.Codigo=pe.IQ');
  QrPQE.SQL.Add('AND pq.Codigo=ei.Insumo');
  if EdLancto.Text <> CO_VAZIO then
   QrPQE.SQL.Add('AND pe.Codigo='+
   IntToStr(Geral.IMV(EdLancto.Text)));
  if EdNF.Text <> CO_VAZIO then
    QrPQE.SQL.Add('AND pe.NF='+
    IntToStr(Geral.IMV(EdNF.Text)));
  if EdConhecimento.Text <> CO_VAZIO then
    QrPQE.SQL.Add('AND pe.Conhecimento='+
    IntToStr(Geral.IMV(EdConhecimento.Text)));
  if CBFornecedor.KeyValue <> NULL then
    QrPQE.SQL.Add('AND pe.IQ='''+IntToStr(CBFornecedor.KeyValue)+'''');
  if CBInsumo.KeyValue <> NULL then
    QrPQE.SQL.Add('AND ei.Insumo='''+IntToStr(CBInsumo.KeyValue)+'''');
  QrPQE.SQL.Add('ORDER BY '+Ordem1+', '+Ordem2+', '+Ordem3);
  QrPQE.Params[0].AsString := Ini;
  QrPQE.Params[1].AsString := Fim;
  UnDmkDAC_PF.AbreQuery(QrPQE, Dmod.MyDB);
*)
  Codigo := EdLancto.ValueVariant;
  NF     := EdNF.ValueVariant;
  NF_RP  := EdNF_RP.ValueVariant;
  NF_CC  := EdNF_CC.ValueVariant;
  Conhecimento := EdConhecimento.ValueVariant;
  IQ     := EdIQ.ValueVariant;
  CI     := EdCI.ValueVariant;
  Insumo := EdInsumo.ValueVariant;
  //
  SQL_PQE    := '';
  SQL_NF     := '';
  SQL_NF_RP  := '';
  SQL_NF_CC  := '';
  SQL_Conhecimento := '';
  SQL_IQ     := '';
  SQL_CI     := '';
  SQL_Insumo := '';
  //
  if Codigo <> 0 then
    SQL_PQE := 'AND pe.Codigo=' + Geral.FF0(Codigo);
  if NF <> 0 then
    SQL_NF := 'AND pe.NF=' + Geral.FF0(NF);
  if NF_RP <> 0 then
    SQL_NF_RP := 'AND pe.NF_RP=' + Geral.FF0(NF_RP);
  if NF_CC <> 0 then
    SQL_NF_CC := 'AND pe.NF_CC=' + Geral.FF0(NF_CC);
  if Conhecimento <> 0 then
    SQL_Conhecimento := 'AND pe.Conhecimento=' + Geral.FF0(Conhecimento);
  if IQ <> 0 then
    SQL_IQ := 'AND pe.IQ=' + Geral.FF0(IQ);
  if CI <> 0 then
    SQL_CI := 'AND pe.CI=' + Geral.FF0(CI);
  if Insumo <> 0 then
    SQL_Insumo := 'AND ei.Insumo=' + Geral.FF0(Insumo);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQE, Dmod.MyDB, [
  'SELECT ei.*,  ',
  'CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial ',
  'ELSE ci.Nome END NOMECLIINT, ',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOMEFORNECEDOR, ',
  'pq.Nome NOMEINSUMO, pe.NF, pe.Data, ',
  'pe.Conhecimento, pe.NF_RP, pe.NF_CC, pe.IQ, pe.CI ',
  'FROM pqeits ei  ',
  'LEFT JOIN pqe pe ON ei.Codigo=pe.Codigo  ',
  'LEFT JOIN Entidades ci ON ci.Codigo=pe.CI ',
  'LEFT JOIN Entidades fo ON fo.Codigo=pe.IQ ',
  'LEFT JOIN pq pq ON pq.Codigo=ei.Insumo ',
  'WHERE pe.Data BETWEEN "' + Ini + '" AND "' + Fim + '" ',
  'AND pe.Empresa=' + Geral.FF0(Empresa),
  SQL_PQE,
  SQL_NF,
  SQL_NF_RP,
  SQL_NF_CC,
  SQL_Conhecimento,
  SQL_IQ,
  SQL_CI,
  SQL_Insumo,
  Ordem,
  '']);
  MyObjects.Informa(LaAviso3, False, '');
end;

procedure TFmPQEPesq.ReopenInsumos();
var
  IQ, CI: Integer;
  SQL_IQ, LEF_CI, SQL_CI: String;
begin
  MyObjects.Informa(LaAviso3, True, 'Reabrindo insumos');
  //
  EdInsumo.ValueVariant := 0;
  CBInsumo.KeyValue     := 0;
  //
  IQ := EdIQ.ValueVariant;
  CI := EdCI.ValueVariant;
  //
  if IQ <> 0 then
    SQL_IQ := 'AND pq.IQ=' + Geral.FF0(IQ)
  else
    SQL_IQ := '';
  //
  if CI <> 0 then
  begin
    LEF_CI := 'LEFT JOIN pqcli ci ON ci.PQ=pq.Codigo';
    SQL_CI := 'AND ci.CI=' + Geral.FF0(CI);
  end else
  begin
    LEF_CI := '';
    SQL_CI := '';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrInsumos, Dmod.MyDB, [
  'SELECT pq.Codigo, pq.Nome ',
  'FROM pq pq ',
  LEF_CI,
  'WHERE pq.Codigo > 0 ',
  SQL_IQ,
  SQL_CI,
  'ORDER BY Nome ',
  '']);
  MyObjects.Informa(LaAviso3, False, '');
end;

procedure TFmPQEPesq.RGOrdem1Click(Sender: TObject);
begin
  Pesquisa;
end;

procedure TFmPQEPesq.EdIQChange(Sender: TObject);
begin
  ReopenInsumos();
  Pesquisa;
end;

procedure TFmPQEPesq.EdInsumoChange(Sender: TObject);
begin
  Pesquisa;
end;

procedure TFmPQEPesq.TPIniClick(Sender: TObject);
begin
  Pesquisa;
end;

procedure TFmPQEPesq.TPFimClick(Sender: TObject);
begin
  Pesquisa;
end;

procedure TFmPQEPesq.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxQUI_ENTRA_005_001, [
  DmodG.frxDsDono,
  frxDsPQE
  ]);
  //
  MyObjects.frxMostra(frxQUI_ENTRA_005_001, 'Entradas de Insumos');
end;

procedure TFmPQEPesq.BtSaidaClick(Sender: TObject);
begin
  VAR_PQELANCTO := -1;
  Close;
end;

procedure TFmPQEPesq.BtConfirmaClick(Sender: TObject);
begin
  VAR_PQELANCTO := QrPQECodigo.Value;
  Close;
end;

procedure TFmPQEPesq.DBGrid1DblClick(Sender: TObject);
begin
  VAR_PQELANCTO := QrPQECodigo.Value;
  Close;
end;

procedure TFmPQEPesq.TPFimChange(Sender: TObject);
begin
  Pesquisa;
end;

procedure TFmPQEPesq.TPIniChange(Sender: TObject);
begin
  Pesquisa;
end;

procedure TFmPQEPesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQEPesq.frxQUI_ENTRA_005_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt('', CBCI.Text, EdCI.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_FORNECE' then
    Value := dmkPF.ParValueCodTxt('', CBIQ.Text, EdIQ.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_PRODUTO' then
    Value := dmkPF.ParValueCodTxt('', CBInsumo.Text, EdInsumo.ValueVariant, 'TODOS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp1(TPIni.Date, TPFim.Date, True, True, '', 'at�', '')
  else

end;

end.

