object FmMPP: TFmMPP
  Left = 368
  Top = 194
  Caption = 'VEN-COURO-101 :: Cadastro de Pedidos'
  ClientHeight = 718
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelIts: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 622
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    Visible = False
    object GroupBox1: TGroupBox
      Left = 0
      Top = 559
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 0
      object BtConfirma2: TBitBtn
        Tag = 14
        Left = 12
        Top = 16
        Width = 120
        Height = 41
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirma2Click
      end
      object Panel4: TPanel
        Left = 867
        Top = 15
        Width = 139
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste2: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesiste2Click
        end
      end
    end
    object GBItem: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 429
      Align = alTop
      TabOrder = 1
      object PnItem_: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 412
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 1004
          Height = 412
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox3: TGroupBox
            Left = 0
            Top = 0
            Width = 1004
            Height = 101
            Align = alTop
            Caption = ' Ordem de produ'#231#227'o: '
            TabOrder = 0
            object Label7: TLabel
              Left = 8
              Top = 16
              Width = 56
              Height = 13
              Caption = 'Mercadoria:'
            end
            object Label13: TLabel
              Left = 428
              Top = 16
              Width = 30
              Height = 13
              Caption = 'Texto:'
            end
            object Label31: TLabel
              Left = 248
              Top = 60
              Width = 34
              Height = 13
              Caption = 'Classe:'
            end
            object Label33: TLabel
              Left = 168
              Top = 60
              Width = 39
              Height = 13
              Caption = #193'rea m'#178':'
            end
            object Label32: TLabel
              Left = 593
              Top = 60
              Width = 40
              Height = 13
              Caption = 'Entrega:'
            end
            object Label34: TLabel
              Left = 328
              Top = 60
              Width = 52
              Height = 13
              Caption = 'Espessura:'
            end
            object Label37: TLabel
              Left = 8
              Top = 60
              Width = 19
              Height = 13
              Caption = 'Cor:'
            end
            object Label3: TLabel
              Left = 407
              Top = 60
              Width = 45
              Height = 13
              Caption = 'Pre'#231'o m'#178':'
            end
            object Label25: TLabel
              Left = 490
              Top = 60
              Width = 50
              Height = 13
              Caption = 'Data semi:'
            end
            object SpeedButton5: TSpeedButton
              Left = 692
              Top = 74
              Width = 137
              Height = 25
              Caption = 'Programa'#231#227'o entregas'
              OnClick = SpeedButton5Click
            end
            object EdMP: TdmkEditCB
              Left = 8
              Top = 32
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdMPChange
              DBLookupComboBox = CBMP
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBMP: TdmkDBLookupComboBox
              Left = 63
              Top = 32
              Width = 360
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsMPs
              TabOrder = 1
              dmkEditCB = EdMP
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdTexto: TdmkEdit
              Left = 428
              Top = 32
              Width = 401
              Height = 21
              MaxLength = 50
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdClasse: TdmkEdit
              Left = 248
              Top = 76
              Width = 77
              Height = 21
              MaxLength = 15
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdM2Pedido: TdmkEdit
              Left = 168
              Top = 76
              Width = 77
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object TPEntrega: TDateTimePicker
              Left = 593
              Top = 76
              Width = 97
              Height = 21
              Date = 39013.000000000000000000
              Time = 0.576212557898543300
              TabOrder = 9
            end
            object EdEspesTxt: TdmkEdit
              Left = 328
              Top = 76
              Width = 77
              Height = 21
              MaxLength = 10
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCorTxt: TdmkEdit
              Left = 8
              Top = 76
              Width = 157
              Height = 21
              MaxLength = 15
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPrecoPed: TdmkEdit
              Left = 407
              Top = 76
              Width = 77
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object TPDtaCrust: TDateTimePicker
              Left = 490
              Top = 76
              Width = 97
              Height = 21
              Date = 39013.000000000000000000
              Time = 0.576212557898543300
              TabOrder = 8
            end
          end
          object GroupBox4: TGroupBox
            Left = 0
            Top = 277
            Width = 1004
            Height = 135
            Align = alClient
            Caption = ' Outras informa'#231#245'es:'
            TabOrder = 2
            object Label4: TLabel
              Left = 8
              Top = 16
              Width = 264
              Height = 13
              Caption = 'Observa'#231#245'es que ser'#227'o impressas na ordem de servi'#231'o:'
            end
            object MeObserv: TMemo
              Left = 8
              Top = 35
              Width = 501
              Height = 89
              Lines.Strings = (
                'MeObserv')
              TabOrder = 0
            end
            object RGTipoProd: TRadioGroup
              Left = 516
              Top = 28
              Width = 105
              Height = 69
              Caption = ' Tipo: '
              Items.Strings = (
                'Venda'
                'M'#227'o de obra')
              TabOrder = 1
            end
            object CkContinuar: TCheckBox
              Left = 520
              Top = 104
              Width = 121
              Height = 17
              Caption = 'Continuar inserindo.'
              TabOrder = 2
            end
          end
          object GroupBox5: TGroupBox
            Left = 0
            Top = 101
            Width = 1004
            Height = 176
            Align = alTop
            Caption = ' Artigo:'
            TabOrder = 1
            object Label11: TLabel
              Left = 8
              Top = 12
              Width = 110
              Height = 13
              Caption = 'Configura'#231#227'o de artigo:'
            end
            object Label12: TLabel
              Left = 8
              Top = 92
              Width = 116
              Height = 13
              Caption = 'Receita de recurtimento:'
            end
            object Label15: TLabel
              Left = 8
              Top = 132
              Width = 117
              Height = 13
              Caption = 'Receita de acabamento:'
            end
            object Label8: TLabel
              Left = 8
              Top = 52
              Width = 91
              Height = 13
              Caption = 'Fluxo de produ'#231#227'o:'
            end
            object Label14: TLabel
              Left = 420
              Top = 92
              Width = 189
              Height = 13
              Caption = 'Receita de recurtimento - segunda fase:'
            end
            object Label16: TLabel
              Left = 420
              Top = 52
              Width = 165
              Height = 13
              Caption = 'Classes de mat'#233'ria-prima utilizadas:'
            end
            object Label26: TLabel
              Left = 420
              Top = 12
              Width = 95
              Height = 13
              Caption = 'Artigo em opera'#231#227'o:'
            end
            object LaFluxPcpCab: TLabel
              Left = 420
              Top = 132
              Width = 255
              Height = 13
              Caption = 'Fluxo de PCP (Planejamento e Controle da Produ'#231#227'o):'
            end
            object SbAlteraFluxPcpCab: TSpeedButton
              Left = 832
              Top = 148
              Width = 53
              Height = 22
              Caption = 'Altera'
              OnClick = SbAlteraFluxPcpCabClick
            end
            object LaLastFluxPcpCab: TLabel
              Left = 688
              Top = 133
              Width = 6
              Height = 13
              Caption = '0'
              Enabled = False
            end
            object EdVSArtCab: TdmkEditCB
              Left = 8
              Top = 28
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'VSArtCab'
              UpdCampo = 'VSArtCab'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdVSArtCabChange
              DBLookupComboBox = CBVSArtCab
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBVSArtCab: TdmkDBLookupComboBox
              Left = 64
              Top = 28
              Width = 352
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsVSArtCab
              TabOrder = 1
              dmkEditCB = EdVSArtCab
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdReceiRecu: TdmkEditCB
              Left = 8
              Top = 108
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'ReceiRecu'
              UpdCampo = 'ReceiRecu'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBReceiRecu
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBReceiRecu: TdmkDBLookupComboBox
              Left = 64
              Top = 108
              Width = 352
              Height = 21
              KeyField = 'Numero'
              ListField = 'Nome'
              ListSource = DsReceiRecu
              TabOrder = 8
              dmkEditCB = EdReceiRecu
              QryCampo = 'ReceiRecu'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdReceiAcab: TdmkEditCB
              Left = 8
              Top = 148
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 11
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'ReceiAcab'
              UpdCampo = 'ReceiAcab'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBReceiAcab
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBReceiAcab: TdmkDBLookupComboBox
              Left = 64
              Top = 148
              Width = 352
              Height = 21
              KeyField = 'Numero'
              ListField = 'Nome'
              ListSource = DsReceiAcab
              TabOrder = 12
              dmkEditCB = EdReceiAcab
              QryCampo = 'ReceiAcab'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdFluxo: TdmkEditCB
              Left = 8
              Top = 68
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBFluxo
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBFluxo: TdmkDBLookupComboBox
              Left = 64
              Top = 68
              Width = 352
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsFluxos
              TabOrder = 5
              dmkEditCB = EdFluxo
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdReceiRefu: TdmkEditCB
              Left = 420
              Top = 108
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'ReceiRefu'
              UpdCampo = 'ReceiRefu'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBReceiRefu
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBReceiRefu: TdmkDBLookupComboBox
              Left = 476
              Top = 108
              Width = 352
              Height = 21
              KeyField = 'Numero'
              ListField = 'Nome'
              ListSource = DsReceiRefu
              TabOrder = 10
              dmkEditCB = EdReceiRefu
              QryCampo = 'ReceiRefu'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdTxtMPs: TdmkEdit
              Left = 420
              Top = 68
              Width = 407
              Height = 21
              MaxLength = 255
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'TxtMPs'
              UpdCampo = 'TxtMPs'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdGraGruX: TdmkEditCB
              Left = 420
              Top = 28
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'VSArtGGX'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGraGruX
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBGraGruX: TdmkDBLookupComboBox
              Left = 476
              Top = 28
              Width = 352
              Height = 21
              KeyField = 'GraGruX'
              ListField = 'ART_NO_PRD_TAM_COR'
              ListSource = DsGraGruX
              TabOrder = 3
              dmkEditCB = EdGraGruX
              QryCampo = 'Cargo'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdFluxPcpCab: TdmkEditCB
              Left = 420
              Top = 148
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 13
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'ReceiAcab'
              UpdCampo = 'ReceiAcab'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBFluxPcpCab
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBFluxPcpCab: TdmkDBLookupComboBox
              Left = 476
              Top = 148
              Width = 352
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsFluxPcpCab
              TabOrder = 14
              dmkEditCB = EdFluxPcpCab
              QryCampo = 'ReceiAcab'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 622
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdit: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 245
      Align = alTop
      TabOrder = 1
      object PnEdit: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 228
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label9: TLabel
          Left = 8
          Top = 8
          Width = 36
          Height = 13
          Caption = 'Pedido:'
        end
        object Label10: TLabel
          Left = 80
          Top = 8
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object Label5: TLabel
          Left = 8
          Top = 48
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label22: TLabel
          Left = 112
          Top = 48
          Width = 49
          Height = 13
          Caption = 'Vendedor:'
        end
        object Label38: TLabel
          Left = 8
          Top = 92
          Width = 293
          Height = 13
          Caption = 'Observa'#231#245'es internas de uso da empresa (n'#227'o ser'#225' impresso):'
        end
        object Label19: TLabel
          Left = 476
          Top = 8
          Width = 75
          Height = 13
          Caption = 'Transportadora:'
        end
        object LaCondicaoPG: TLabel
          Left = 476
          Top = 48
          Width = 119
          Height = 13
          Caption = 'Condi'#231#227'o de pagamento:'
        end
        object Label23: TLabel
          Left = 8
          Top = 164
          Width = 330
          Height = 13
          Caption = 
            'OC - Ordem de compra (ou ordem de presta'#231#227'o de servi'#231'o) do clien' +
            'te:'
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 24
          Width = 69
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdCliente: TdmkEditCB
          Left = 80
          Top = 24
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 135
          Top = 24
          Width = 338
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsClientes
          TabOrder = 2
          dmkEditCB = EdCliente
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object TPDataF: TDateTimePicker
          Left = 8
          Top = 64
          Width = 101
          Height = 21
          Date = 38579.000000000000000000
          Time = 0.624225613399175900
          TabOrder = 5
        end
        object EdVendedor: TdmkEditCB
          Left = 112
          Top = 64
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBVendedor
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBVendedor: TdmkDBLookupComboBox
          Left = 167
          Top = 64
          Width = 306
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsVendedores
          TabOrder = 7
          dmkEditCB = EdVendedor
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object MeObz: TMemo
          Left = 8
          Top = 108
          Width = 989
          Height = 51
          MaxLength = 254
          TabOrder = 10
        end
        object EdTransp: TdmkEditCB
          Left = 476
          Top = 24
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBTransp
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBTransp: TdmkDBLookupComboBox
          Left = 531
          Top = 24
          Width = 466
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsTransp
          TabOrder = 4
          dmkEditCB = EdTransp
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBCondicaoPG: TdmkDBLookupComboBox
          Left = 532
          Top = 64
          Width = 465
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsPediPrzCab
          TabOrder = 9
          dmkEditCB = EdCondicaoPG
          UpdType = utNil
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCondicaoPG: TdmkEditCB
          Left = 476
          Top = 64
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCondicaoPG
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdPedidCli: TdmkEdit
          Left = 8
          Top = 180
          Width = 552
          Height = 21
          MaxLength = 60
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'PedidCli'
          UpdCampo = 'PedidCli'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 559
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel8: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 622
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Panel6: TPanel
      Left = 0
      Top = 191
      Width = 1008
      Height = 20
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'Mat'#233'ria-prima utilizada no setor de recurtimento'
      TabOrder = 5
    end
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 96
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 72
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdNome
      end
      object Label6: TLabel
        Left = 632
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Data:'
        FocusControl = DBEdit02
      end
      object Label17: TLabel
        Left = 480
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
        FocusControl = DBEdit1
      end
      object Label18: TLabel
        Left = 556
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Valor:'
        FocusControl = DBEdit2
      end
      object Label20: TLabel
        Left = 336
        Top = 4
        Width = 69
        Height = 13
        Caption = 'Transportador:'
        FocusControl = DBEdit3
      end
      object Label21: TLabel
        Left = 700
        Top = 4
        Width = 119
        Height = 13
        Caption = 'Condi'#231#227'o de pagamento:'
        FocusControl = DBEdit4
      end
      object Label24: TLabel
        Left = 904
        Top = 4
        Width = 85
        Height = 13
        Caption = 'Pedido do cliente:'
        FocusControl = DBEdit5
      end
      object DBEdCodigo: TDBEdit
        Left = 4
        Top = 20
        Width = 65
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsMPP
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 72
        Top = 20
        Width = 260
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NOMECLIENTE'
        DataSource = DsMPP
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit02: TDBEdit
        Left = 632
        Top = 20
        Width = 65
        Height = 21
        Color = clWhite
        DataField = 'DataF'
        DataSource = DsMPP
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
      object DBEdit1: TDBEdit
        Left = 480
        Top = 20
        Width = 72
        Height = 21
        DataField = 'Qtde'
        DataSource = DsMPP
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 556
        Top = 20
        Width = 72
        Height = 21
        DataField = 'Valor'
        DataSource = DsMPP
        TabOrder = 4
      end
      object DBMemo2: TDBMemo
        Left = 0
        Top = 45
        Width = 1008
        Height = 51
        Align = alBottom
        DataField = 'Obz'
        DataSource = DsMPP
        TabOrder = 5
      end
      object DBEdit3: TDBEdit
        Left = 336
        Top = 20
        Width = 140
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NOMETRANSP'
        DataSource = DsMPP
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
      end
      object DBEdit4: TDBEdit
        Left = 700
        Top = 20
        Width = 200
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NO_CONDICAOPG'
        DataSource = DsMPP
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
      end
      object DBEdit5: TDBEdit
        Left = 904
        Top = 20
        Width = 100
        Height = 21
        DataField = 'PedidCli'
        DataSource = DsMPP
        TabOrder = 8
      end
    end
    object PainelPanels: TPanel
      Left = 0
      Top = 494
      Width = 1008
      Height = 128
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object GBTrava: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 63
        Align = alTop
        TabOrder = 1
        Visible = False
        object BtTrava: TBitBtn
          Tag = 14
          Left = 8
          Top = 12
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Trava'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtTravaClick
        end
        object BtMercadoria: TBitBtn
          Tag = 224
          Left = 192
          Top = 12
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Mercad.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtMercadoriaClick
        end
      end
      object GBCntrl: TGroupBox
        Left = 0
        Top = 64
        Width = 1008
        Height = 64
        Align = alBottom
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 172
          Height = 47
          Align = alLeft
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object SpeedButton4: TBitBtn
            Tag = 4
            Left = 128
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = SpeedButton4Click
          end
          object SpeedButton3: TBitBtn
            Tag = 3
            Left = 88
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = SpeedButton3Click
          end
          object SpeedButton2: TBitBtn
            Tag = 2
            Left = 48
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = SpeedButton2Click
          end
          object SpeedButton1: TBitBtn
            Tag = 1
            Left = 8
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = SpeedButton1Click
          end
        end
        object LaRegistro: TStaticText
          Left = 174
          Top = 15
          Width = 235
          Height = 47
          Align = alClient
          BevelInner = bvLowered
          BevelKind = bkFlat
          Caption = '[N]: 0'
          TabOrder = 2
        end
        object Panel3: TPanel
          Left = 409
          Top = 15
          Width = 597
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object Panel9: TPanel
            Left = 488
            Top = 0
            Width = 109
            Height = 47
            Align = alRight
            Alignment = taRightJustify
            BevelOuter = bvNone
            TabOrder = 0
            object BtSaida: TBitBtn
              Tag = 13
              Left = 4
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
          object BtInclui: TBitBtn
            Tag = 10
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Inclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtIncluiClick
          end
          object BtAltera: TBitBtn
            Tag = 11
            Left = 124
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Altera'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtAlteraClick
          end
          object BtImprime: TBitBtn
            Tag = 5
            Left = 244
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = 'I&mprime'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = BtImprimeClick
          end
          object BtExclui: TBitBtn
            Tag = 12
            Left = 364
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Exclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = BtExcluiClick
          end
        end
      end
    end
    object DBGrid4: TDBGrid
      Left = -2
      Top = 102
      Width = 832
      Height = 64
      DataSource = DsMPPIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'OS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEMP'
          Title.Caption = 'Artigo'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Title.Caption = 'Descri'#231#227'o'
          Width = 232
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CorTxt'
          Title.Caption = 'Cor'
          Width = 132
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EspesTxt'
          Title.Caption = 'Espessura'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Classe'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'M2Pedido'
          Title.Caption = 'Pedido m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoPed'
          Title.Caption = 'Pre'#231'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorPed'
          Title.Caption = 'Total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtaCrust'
          Title.Caption = 'Data semi.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Entrega'
          Title.Caption = 'Entregar em'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFLUXO'
          Title.Caption = 'Fluxo'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoPQ'
          Title.Caption = 'Custo PQ'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoWB'
          Title.Caption = 'Custo MP'
          Visible = False
        end>
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 389
      Width = 1008
      Height = 105
      Align = alBottom
      Caption = '     Informa'#231#245'es que ser'#227'o impressas na ordem de servi'#231'o: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 3
      object DBMemo1: TDBMemo
        Left = 2
        Top = 15
        Width = 609
        Height = 88
        Align = alLeft
        DataField = 'Observ'
        DataSource = DsMPPIts
        TabOrder = 0
      end
      object dmkDBGridZTO1: TdmkDBGridZTO
        Left = 611
        Top = 15
        Width = 395
        Height = 88
        Align = alClient
        DataSource = DsMPVPss
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'DataSeq'
            Title.Caption = 'Data'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_IniStg'
            Title.Caption = 'Est'#225'gio inicial do dia'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_FimStg'
            Title.Caption = 'Est'#225'gio final do dia'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DiaSeq'
            Title.Caption = 'Dia seq.'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Conta'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DtHrReal'
            Title.Caption = 'Data real feito'
            Visible = True
          end>
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 211
      Width = 1008
      Height = 74
      Align = alBottom
      DataSource = DsWBMovIts
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Pallet'
          Title.Caption = 'ID Pallet'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PALLET'
          Title.Caption = 'Pallet'
          Width = 95
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
          Width = 240
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea m'#178
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorT'
          Title.Caption = 'Valor total'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID item'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LnkNivXtr1'
          Title.Caption = 'Pesagem (SP)'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LnkNivXtr2'
          Title.Caption = 'Item custo SP'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Observ'
          Title.Caption = 'Observa'#231#245'es'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaP2'
          Title.Caption = #193'rea ft'#178
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Peso kg'
          Width = 72
          Visible = True
        end>
    end
    object Panel7: TPanel
      Left = 0
      Top = 285
      Width = 1008
      Height = 20
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'Receitas utilizadas nos setores de recurtimento e acabamento'
      TabOrder = 6
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 305
      Width = 1008
      Height = 84
      Align = alBottom
      DataSource = DsEmitCus
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Pesagem'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID Custo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataEmis'
          Title.Caption = 'Emiss'#227'o'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESETOR'
          Title.Caption = 'Setor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Numero'
          Title.Caption = 'Receita'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME'
          Title.Caption = 'Descri'#231#227'o da receita'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Title.Caption = 'Peso kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Custo'
          Title.Caption = 'Custo BRL'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PercTotCus'
          Title.Caption = '% da receita'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Setor'
          Title.Caption = 'C'#243'd. setor'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 18
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 254
        Height = 32
        Caption = 'Cadastro de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 254
        Height = 32
        Caption = 'Cadastro de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 254
        Height = 32
        Caption = 'Cadastro de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsMPP: TDataSource
    DataSet = QrMPP
    Left = 544
    Top = 61
  end
  object QrMPP: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrMPPBeforeOpen
    AfterOpen = QrMPPAfterOpen
    AfterScroll = QrMPPAfterScroll
    SQL.Strings = (
      'SELECT'
      'CASE WHEN ve.Tipo=0 THEN ve.RazaoSocial'
      'ELSE ve.Nome END NOMEVENDEDOR,'
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE WHEN tr.Tipo=0 THEN tr.RazaoSocial'
      'ELSE tr.Nome END NOMETRANSP,'
      'pc.Nome NO_CONDICAOPG,'
      'MPP.*'
      'FROM mpp MPP'
      'LEFT JOIN entidades cl ON cl.Codigo=MPP.Cliente'
      'LEFT JOIN entidades ve ON ve.Codigo=MPP.Vendedor'
      'LEFT JOIN entidades tr ON tr.Codigo=MPP.Transp'
      'LEFT JOIN pediprzcab pc ON pc.Codigo=mpp.CondicaoPg'
      'WHERE MPP.Codigo > 0')
    Left = 544
    Top = 17
    object QrMPPNOMEVENDEDOR: TWideStringField
      FieldName = 'NOMEVENDEDOR'
      Size = 100
    end
    object QrMPPNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrMPPCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mpp.Codigo'
    end
    object QrMPPCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'mpp.Cliente'
    end
    object QrMPPVendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'mpp.Vendedor'
    end
    object QrMPPDataF: TDateField
      FieldName = 'DataF'
      Origin = 'mpp.DataF'
    end
    object QrMPPQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'mpp.Qtde'
    end
    object QrMPPValor: TFloatField
      FieldName = 'Valor'
      Origin = 'mpp.Valor'
    end
    object QrMPPObz: TWideStringField
      FieldName = 'Obz'
      Origin = 'mpp.Obz'
      Size = 255
    end
    object QrMPPLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'mpp.Lk'
    end
    object QrMPPDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'mpp.DataCad'
    end
    object QrMPPDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'mpp.DataAlt'
    end
    object QrMPPUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'mpp.UserCad'
    end
    object QrMPPUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'mpp.UserAlt'
    end
    object QrMPPAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'mpp.AlterWeb'
    end
    object QrMPPAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMPPTransp: TIntegerField
      FieldName = 'Transp'
    end
    object QrMPPCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrMPPNOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrMPPNO_CONDICAOPG: TWideStringField
      FieldName = 'NO_CONDICAOPG'
      Size = 50
    end
    object QrMPPPedidCli: TWideStringField
      FieldName = 'PedidCli'
      Size = 60
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE (Cliente1="V" OR Cliente2="V")'
      'ORDER BY NOMEENTIDADE')
    Left = 645
    Top = 229
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 645
    Top = 273
  end
  object QrMPs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM artigosgrupos'
      'ORDER BY Nome'
      '')
    Left = 281
    Top = 29
    object QrMPsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMPs: TDataSource
    DataSet = QrMPs
    Left = 281
    Top = 73
  end
  object QrMPPIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrMPPItsBeforeClose
    AfterScroll = QrMPPItsAfterScroll
    OnCalcFields = QrMPPItsCalcFields
    SQL.Strings = (
      'SELECT pec.Nome NOMEUNIDADE, '
      'ag.Nome NOMEMP, flu.Nome NOMEFLUXO, mvi.*,'
      'flu.Nome NO_Fluxo, fo1.Nome NO_ReceiRecu,'
      'fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM mpvits mvi'
      'LEFT JOIN defpecas pec ON pec.Codigo=mvi.Unidade'
      'LEFT JOIN artigosgrupos ag ON ag.Codigo=mvi.MP'
      'LEFT JOIN fluxos flu ON flu.Codigo=mvi.Fluxo'
      'LEFT JOIN gragrux ggx ON ggx.Controle=mvi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN formulas fo1 ON fo1.Numero=mvi.ReceiRecu'
      'LEFT JOIN formulas fo2 ON fo2.Numero=mvi.ReceiRefu'
      'LEFT JOIN tintascab ti1 ON ti1.Numero=mvi.ReceiAcab'
      'WHERE mvi.Pedido<>0'
      'AND mvi.Pedido=:P0')
    Left = 600
    Top = 17
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPPItsNOMEMP: TWideStringField
      FieldName = 'NOMEMP'
      Origin = 'artigosgrupos.Nome'
      Required = True
      Size = 50
    end
    object QrMPPItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'MPVIts.Codigo'
      Required = True
    end
    object QrMPPItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'MPVIts.Controle'
      Required = True
    end
    object QrMPPItsMP: TIntegerField
      FieldName = 'MP'
      Origin = 'MPVIts.MP'
      Required = True
    end
    object QrMPPItsQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'MPVIts.Qtde'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'MPVIts.Preco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsValor: TFloatField
      FieldName = 'Valor'
      Origin = 'MPVIts.Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'MPVIts.Lk'
    end
    object QrMPPItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'MPVIts.DataCad'
    end
    object QrMPPItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'MPVIts.DataAlt'
    end
    object QrMPPItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'MPVIts.UserCad'
    end
    object QrMPPItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'MPVIts.UserAlt'
    end
    object QrMPPItsTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'MPVIts.Texto'
      Required = True
      Size = 50
    end
    object QrMPPItsDesco: TFloatField
      FieldName = 'Desco'
      Origin = 'MPVIts.Desco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsSubTo: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SubTo'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMPPItsEntrega: TDateField
      FieldName = 'Entrega'
      Origin = 'MPVIts.Entrega'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPPItsPronto: TDateField
      FieldName = 'Pronto'
      Origin = 'MPVIts.Pronto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPPItsPRONTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PRONTO_TXT'
      Size = 10
      Calculated = True
    end
    object QrMPPItsStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'MPVIts.Status'
      Required = True
    end
    object QrMPPItsFluxo: TIntegerField
      FieldName = 'Fluxo'
      Origin = 'MPVIts.Fluxo'
      Required = True
    end
    object QrMPPItsClasse: TWideStringField
      FieldName = 'Classe'
      Origin = 'MPVIts.Classe'
      Size = 15
    end
    object QrMPPItsM2Pedido: TFloatField
      FieldName = 'M2Pedido'
      Origin = 'MPVIts.M2Pedido'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'MPVIts.Pecas'
      Required = True
    end
    object QrMPPItsUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'MPVIts.Unidade'
      Required = True
    end
    object QrMPPItsObserv: TWideMemoField
      FieldName = 'Observ'
      Origin = 'MPVIts.Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPPItsNOMEUNIDADE: TWideStringField
      FieldName = 'NOMEUNIDADE'
      Origin = 'defpecas.Nome'
      Size = 30
    end
    object QrMPPItsEspesTxt: TWideStringField
      FieldName = 'EspesTxt'
      Origin = 'MPVIts.EspesTxt'
      Size = 10
    end
    object QrMPPItsCorTxt: TWideStringField
      FieldName = 'CorTxt'
      Origin = 'MPVIts.CorTxt'
      Size = 30
    end
    object QrMPPItsPedido: TIntegerField
      FieldName = 'Pedido'
      Required = True
    end
    object QrMPPItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrMPPItsPrecoPed: TFloatField
      FieldName = 'PrecoPed'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsValorPed: TFloatField
      FieldName = 'ValorPed'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsTipoProd: TSmallintField
      FieldName = 'TipoProd'
      Required = True
    end
    object QrMPPItsNOMETIPOPROD: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPOPROD'
      Size = 15
      Calculated = True
    end
    object QrMPPItsNOMEFLUXO: TWideStringField
      FieldName = 'NOMEFLUXO'
      Size = 100
    end
    object QrMPPItsDescricao: TWideMemoField
      FieldName = 'Descricao'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPPItsComplementacao: TWideMemoField
      FieldName = 'Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPPItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrMPPItsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrMPPItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrMPPItsReceiRecu: TIntegerField
      FieldName = 'ReceiRecu'
    end
    object QrMPPItsReceiRefu: TIntegerField
      FieldName = 'ReceiRefu'
    end
    object QrMPPItsReceiAcab: TIntegerField
      FieldName = 'ReceiAcab'
    end
    object QrMPPItsTxtMPs: TWideStringField
      FieldName = 'TxtMPs'
      Size = 255
    end
    object QrMPPItsNO_Fluxo: TWideStringField
      FieldName = 'NO_Fluxo'
      Size = 100
    end
    object QrMPPItsNO_ReceiRecu: TWideStringField
      FieldName = 'NO_ReceiRecu'
      Size = 30
    end
    object QrMPPItsNO_ReceiRefu: TWideStringField
      FieldName = 'NO_ReceiRefu'
      Size = 30
    end
    object QrMPPItsNO_ReceiAcab: TWideStringField
      FieldName = 'NO_ReceiAcab'
      Size = 30
    end
    object QrMPPItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrMPPItsCustoWB: TFloatField
      FieldName = 'CustoWB'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrMPPItsDtaCrust: TDateField
      FieldName = 'DtaCrust'
    end
    object QrMPPItsVSArtCab: TIntegerField
      FieldName = 'VSArtCab'
    end
    object QrMPPItsVSArtGGX: TIntegerField
      FieldName = 'VSArtGGX'
    end
    object QrMPPItsVSArtCab_TXT: TWideStringField
      FieldName = 'VSArtCab_TXT'
      Size = 255
    end
    object QrMPPItsFluxPcpCab: TIntegerField
      FieldName = 'FluxPcpCab'
    end
  end
  object DsMPPIts: TDataSource
    DataSet = QrMPPIts
    Left = 600
    Top = 65
  end
  object QrTotais: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(M2Pedido) Qtde, SUM(ValorPed) Valor'
      'FROM mpvits'
      'WHERE Pedido=:P0')
    Left = 769
    Top = 273
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotaisQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrTotaisValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFornecedorCalcFields
    SQL.Strings = (
      'SELECT '
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOME,'
      'CASE WHEN fo.Tipo=0 THEN fo.ERua'
      'ELSE fo.PRua END RUA,'
      'CASE WHEN fo.Tipo=0 THEN fo.ENumero'
      'ELSE fo.PNumero END NUMERO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECompl'
      'ELSE fo.PCompl END COMPL,'
      'CASE WHEN fo.Tipo=0 THEN fo.EBairro'
      'ELSE fo.PBairro END BAIRRO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECidade'
      'ELSE fo.PCidade END CIDADE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EPais'
      'ELSE fo.PPais END PAIS,'
      'CASE WHEN fo.Tipo=0 THEN fo.ETe1'
      'ELSE fo.PTe1 END TELEFONE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EFax'
      'ELSE fo.PPais END FAX,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECel'
      'ELSE fo.PCel END Celular,'
      'CASE WHEN fo.Tipo=0 THEN fo.CNPJ'
      'ELSE fo.CPF END CNPJ,'
      'CASE WHEN fo.Tipo=0 THEN fo.IE'
      'ELSE fo.RG END IE,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECEP'
      'ELSE fo.PCEP END CEP,'
      'CASE WHEN fo.Tipo=0 THEN fo.EContato'
      'ELSE fo.PContato END Contato,'
      ''
      'CASE WHEN fo.Tipo=0 THEN fo.EUF'
      'ELSE fo.PUF END UF,'
      'CASE WHEN fo.Tipo=0 THEN uf1.Nome'
      'ELSE uf2.Nome END NOMEUF,'
      'EEMail, PEmail,'
      ''
      'CASE WHEN fo.Tipo=0 THEN ll1.Nome'
      'ELSE ll2.Nome END LOGRAD'
      ''
      'FROM entidades fo'
      'LEFT JOIN ufs uf1 ON uf1.Codigo=fo.Euf'
      'LEFT JOIN ufs uf2 ON uf2.Codigo=fo.Puf'
      'LEFT JOIN listalograd ll1 ON ll1.Codigo=fo.ELograd'
      'LEFT JOIN listalograd ll2 ON ll2.Codigo=fo.PLograd'
      'WHERE fo.Codigo=:P0')
    Left = 768
    Top = 140
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFornecedorFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrFornecedorCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Calculated = True
    end
    object QrFornecedorCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Calculated = True
    end
    object QrFornecedorTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrFornecedorCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrFornecedorNOME: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOME'
      Size = 100
    end
    object QrFornecedorRUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 30
    end
    object QrFornecedorNUMERO: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'NUMERO'
    end
    object QrFornecedorCOMPL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'COMPL'
      Size = 30
    end
    object QrFornecedorBAIRRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrFornecedorCIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrFornecedorPAIS: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'PAIS'
    end
    object QrFornecedorTELEFONE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TELEFONE'
    end
    object QrFornecedorFAX: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FAX'
    end
    object QrFornecedorCelular: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Celular'
    end
    object QrFornecedorCNPJ: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrFornecedorIE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'IE'
    end
    object QrFornecedorCEP: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'CEP'
    end
    object QrFornecedorContato: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Contato'
      Size = 60
    end
    object QrFornecedorUF: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'UF'
    end
    object QrFornecedorNOMEUF: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrFornecedorEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 100
    end
    object QrFornecedorPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrFornecedorNUMEROTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMEROTXT'
      Size = 40
      Calculated = True
    end
    object QrFornecedorENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrFornecedorLOGRAD: TWideStringField
      FieldName = 'LOGRAD'
      Size = 10
    end
  end
  object QrTransportador: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTransportadorCalcFields
    SQL.Strings = (
      'SELECT '
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOME,'
      'CASE WHEN fo.Tipo=0 THEN fo.ERua'
      'ELSE fo.PRua END RUA,'
      'CASE WHEN fo.Tipo=0 THEN fo.ENumero'
      'ELSE fo.PNumero END NUMERO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECompl'
      'ELSE fo.PCompl END COMPL,'
      'CASE WHEN fo.Tipo=0 THEN fo.EBairro'
      'ELSE fo.PBairro END BAIRRO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECidade'
      'ELSE fo.PCidade END CIDADE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EPais'
      'ELSE fo.PPais END PAIS,'
      'CASE WHEN fo.Tipo=0 THEN fo.ETe1'
      'ELSE fo.PTe1 END TELEFONE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EFax'
      'ELSE fo.PPais END FAX,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECel'
      'ELSE fo.PCel END Celular,'
      'CASE WHEN fo.Tipo=0 THEN fo.CNPJ'
      'ELSE fo.CPF END CNPJ,'
      'CASE WHEN fo.Tipo=0 THEN fo.IE'
      'ELSE fo.RG END IE,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECEP'
      'ELSE fo.PCEP END CEP,'
      'CASE WHEN fo.Tipo=0 THEN fo.EContato'
      'ELSE fo.PContato END Contato,'
      ''
      'CASE WHEN fo.Tipo=0 THEN fo.EUF'
      'ELSE fo.PUF END UF,'
      'CASE WHEN fo.Tipo=0 THEN uf1.Nome'
      'ELSE uf2.Nome END NOMEUF,'
      'EEMail, PEmail,'
      ''
      'CASE WHEN fo.Tipo=0 THEN ll1.Nome'
      'ELSE ll2.Nome END LOGRAD'
      ''
      'FROM entidades fo'
      'LEFT JOIN ufs uf1 ON uf1.Codigo=fo.Euf'
      'LEFT JOIN ufs uf2 ON uf2.Codigo=fo.Puf'
      'LEFT JOIN listalograd ll1 ON ll1.Codigo=fo.ELograd'
      'LEFT JOIN listalograd ll2 ON ll2.Codigo=fo.PLograd'
      'WHERE fo.Codigo=:P0')
    Left = 768
    Top = 228
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTransportadorTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrTransportadorCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Calculated = True
    end
    object QrTransportadorCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Calculated = True
    end
    object QrTransportadorFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrTransportadorCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrTransportadorNOME: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOME'
      Size = 100
    end
    object QrTransportadorRUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 30
    end
    object QrTransportadorNUMERO: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'NUMERO'
    end
    object QrTransportadorCOMPL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'COMPL'
      Size = 30
    end
    object QrTransportadorBAIRRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrTransportadorCIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrTransportadorPAIS: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'PAIS'
    end
    object QrTransportadorTELEFONE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TELEFONE'
    end
    object QrTransportadorFAX: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FAX'
    end
    object QrTransportadorCelular: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Celular'
    end
    object QrTransportadorCNPJ: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTransportadorIE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'IE'
    end
    object QrTransportadorCEP: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'CEP'
    end
    object QrTransportadorContato: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Contato'
      Size = 60
    end
    object QrTransportadorUF: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'UF'
    end
    object QrTransportadorNOMEUF: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrTransportadorNUMEROTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMEROTXT'
      Size = 40
      Calculated = True
    end
    object QrTransportadorENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrTransportadorEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 100
    end
    object QrTransportadorPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrTransportadorLOGRAD: TWideStringField
      FieldName = 'LOGRAD'
      Size = 10
    end
  end
  object QrVendedores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece6="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 713
    Top = 65
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 713
    Top = 109
  end
  object PMEdita: TPopupMenu
    Left = 556
    Top = 348
    object MenuItem1: TMenuItem
      Caption = '&Mercadoria'
    end
    object MenuItem2: TMenuItem
      Caption = '&Pagamento'
    end
  end
  object QrSumIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtde) Qtde, SUM(Desco) Desco,'
      'SUM(Valor) Valor, SUM(Valor) / SUM(Qtde) PRECO'
      'FROM mpvits '
      'WHERE Codigo=:P0')
    Left = 661
    Top = 61
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumItsQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSumItsDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrSumItsValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumItsPRECO: TFloatField
      FieldName = 'PRECO'
    end
  end
  object PMExclui: TPopupMenu
    Left = 600
    Top = 348
    object MenuItem3: TMenuItem
      Caption = '&Mercadoria'
      OnClick = MenuItem3Click
    end
    object MenuItem4: TMenuItem
      Caption = '&Pagamento'
    end
  end
  object PMMercadoria: TPopupMenu
    OnPopup = PMMercadoriaPopup
    Left = 200
    Top = 360
    object Incluinovamercadoria1: TMenuItem
      Caption = '&Inclui nova mercadoria'
      OnClick = Incluinovamercadoria1Click
    end
    object Alteradadosdamercadoriaatual1: TMenuItem
      Caption = '&Altera dados da mercadoria atual'
      OnClick = Alteradadosdamercadoriaatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Excluimercadoriaatual1: TMenuItem
      Caption = '&Exclui mercadoria atual'
      OnClick = Excluimercadoriaatual1Click
    end
  end
  object QrDefPecas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM defpecas'
      'ORDER BY Nome')
    Left = 345
    Top = 29
    object QrDefPecasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDefPecasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrDefPecasGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrDefPecasNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrDefPecasValor: TFloatField
      FieldName = 'Valor'
    end
    object QrDefPecasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrDefPecasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDefPecasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDefPecasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDefPecasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsDefPecas: TDataSource
    DataSet = QrDefPecas
    Left = 345
    Top = 73
  end
  object frxDsMPVIts: TfrxDBDataset
    Enabled = False
    UserName = 'frxDsMPVIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEMP=NOMEMP'
      'Codigo=Codigo'
      'Controle=Controle'
      'MP=MP'
      'Qtde=Qtde'
      'Preco=Preco'
      'Valor=Valor'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'Texto=Texto'
      'Desco=Desco'
      'SubTo=SubTo'
      'Entrega=Entrega'
      'Pronto=Pronto'
      'PRONTO_TXT=PRONTO_TXT'
      'Status=Status'
      'Fluxo=Fluxo'
      'Classe=Classe'
      'M2Pedido=M2Pedido'
      'Pecas=Pecas'
      'Unidade=Unidade'
      'Observ=Observ'
      'NOMEUNIDADE=NOMEUNIDADE'
      'EspesTxt=EspesTxt'
      'CorTxt=CorTxt'
      'Pedido=Pedido'
      'AlterWeb=AlterWeb'
      'PrecoPed=PrecoPed'
      'ValorPed=ValorPed'
      'TipoProd=TipoProd'
      'NOMETIPOPROD=NOMETIPOPROD'
      'NOMEFLUXO=NOMEFLUXO'
      'Descricao=Descricao'
      'Complementacao=Complementacao'
      'Ativo=Ativo'
      'CustoPQ=CustoPQ'
      'GraGruX=GraGruX'
      'ReceiRecu=ReceiRecu'
      'ReceiRefu=ReceiRefu'
      'ReceiAcab=ReceiAcab'
      'TxtMPs=TxtMPs'
      'NO_Fluxo=NO_Fluxo'
      'NO_ReceiRecu=NO_ReceiRecu'
      'NO_ReceiRefu=NO_ReceiRefu'
      'NO_ReceiAcab=NO_ReceiAcab'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'CustoWB=CustoWB'
      'DtaCrust=DtaCrust'
      'VSArtCab=VSArtCab'
      'VSArtGGX=VSArtGGX'
      'VSArtCab_TXT=VSArtCab_TXT')
    DataSet = QrMPPIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 601
    Top = 105
  end
  object frxOS_1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39496.531362662000000000
    ReportOptions.LastChange = 39496.531362662000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 757
    Top = 21
    Datasets = <
      item
        DataSet = FmVSImpOSFluxo.frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMPP
        DataSetName = 'frxDsMPP'
      end
      item
        DataSet = frxDsMPVIts
        DataSetName = 'frxDsMPVIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 283.464750000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 351.496045910000000000
          Height = 18.897650000000000000
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 234.330860000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'impresso em: [Date] [Time]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 15.118120000000000000
          Width = 366.614410000000000000
          Height = 30.236240000000000000
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'OS: [frxDsMPVIts."Controle"]    [frxDsMPVIts."NOMETIPOPROD"] ')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 75.590600000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 75.590600000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 75.590600000000000000
          Width = 298.582870000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 75.590600000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 593.386210000000000000
          Top = 75.590600000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Espessura')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Top = 75.590600000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Top = 75.590600000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 94.488250000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ''
            ''
            ''
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39',<frxDsMPVIts."Peca' +
              's">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 94.488250000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ''
            ''
            ''
            '[frxDsMPVIts."NOMEUNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 94.488250000000000000
          Width = 298.582870000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 94.488250000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."CorTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 593.386210000000000000
          Top = 94.488250000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."EspesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Top = 94.488250000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Classe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Top = 94.488250000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."M2Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 113.385900000000000000
          Width = 699.213050000000000000
          Height = 102.047310000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Observ"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 215.433210000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data da entrega: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Top = 215.433210000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Entrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 215.433210000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 215.433210000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  [FormatFloat('#39'000'#39',<frxDsMPVIts."Pedido">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 49.133889999999990000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cliente: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 49.133889999999990000
          Width = 430.866420000000000000
          Height = 18.897650000000000000
          DataField = 'NOMECLIENTE'
          DataSet = frxDsMPP
          DataSetName = 'frxDsMPP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."NOMECLIENTE"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 113.385900000000000000
          Width = 18.897650000000000000
          Height = 102.047310000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          Rotation = 90
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 215.433210000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data do pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 215.433210000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."DataF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000000000
          Top = 49.133889999999990000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 49.133889999999990000
          Width = 196.535560000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NOMEMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrPesqV: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mpvits'
      'WHERE Codigo<>0'
      'AND Pedido=:P0')
    Left = 661
    Top = 17
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object frxDsMPP: TfrxDBDataset
    Enabled = False
    UserName = 'frxDsMPP'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEVENDEDOR=NOMEVENDEDOR'
      'NOMECLIENTE=NOMECLIENTE'
      'Codigo=Codigo'
      'Cliente=Cliente'
      'Vendedor=Vendedor'
      'DataF=DataF'
      'Qtde=Qtde'
      'Valor=Valor'
      'Obz=Obz'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Transp=Transp'
      'CondicaoPg=CondicaoPg'
      'NOMETRANSP=NOMETRANSP'
      'NO_CONDICAOPG=NO_CONDICAOPG'
      'PedidCli=PedidCli')
    DataSet = QrMPP
    BCDToCurrency = False
    DataSetOptions = []
    Left = 545
    Top = 125
  end
  object QrFluxos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM fluxos'
      'ORDER BY Nome')
    Left = 408
    Top = 32
    object QrFluxosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFluxosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsFluxos: TDataSource
    DataSet = QrFluxos
    Left = 408
    Top = 76
  end
  object QrFluxosIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ope.Nome NOMEOPERACAO, fli.*'
      'FROM fluxosits fli'
      'LEFT JOIN operacoes ope ON ope.Codigo=fli.Operacao'
      'WHERE fli.Codigo=:P0'
      'ORDER BY fli.Ordem, fli.Controle DESC')
    Left = 476
    Top = 17
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFluxosItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFluxosItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFluxosItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFluxosItsOperacao: TIntegerField
      FieldName = 'Operacao'
    end
    object QrFluxosItsAcao1: TWideStringField
      FieldName = 'Acao1'
      Size = 30
    end
    object QrFluxosItsAcao2: TWideStringField
      FieldName = 'Acao2'
      Size = 30
    end
    object QrFluxosItsAcao3: TWideStringField
      FieldName = 'Acao3'
      Size = 30
    end
    object QrFluxosItsAcao4: TWideStringField
      FieldName = 'Acao4'
      Size = 30
    end
    object QrFluxosItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFluxosItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFluxosItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFluxosItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFluxosItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFluxosItsNOMEOPERACAO: TWideStringField
      FieldName = 'NOMEOPERACAO'
      Size = 30
    end
    object QrFluxosItsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object DsFluxosIts: TDataSource
    DataSet = QrFluxosIts
    Left = 476
    Top = 61
  end
  object frxDsFluxosIts: TfrxDBDataset
    Enabled = False
    UserName = 'frxDsFluxosIts'
    CloseDataSource = False
    DataSet = QrFluxosIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 476
    Top = 108
  end
  object frxOS_2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39496.531362662000000000
    ReportOptions.LastChange = 39496.531362662000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  Page2.Visible := <VAR_OSImpInfWB>;                            ' +
        '                     '
      'end.')
    OnGetValue = frxOS_2GetValue
    Left = 821
    Top = 21
    Datasets = <
      item
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
      end
      item
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMPP
        DataSetName = 'frxDsMPP'
      end
      item
        DataSet = frxDsMPVIts
        DataSetName = 'frxDsMPVIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 778.583009130000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 309.921215910000000000
          Height = 18.897650000000000000
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em: [Date] [Time]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Width = 366.614410000000000000
          Height = 30.236240000000000000
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'OS: [frxDsMPVIts."Controle"]    [frxDsMPVIts."NOMETIPOPROD"] ')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 52.913420000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 52.913420000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 52.913420000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 52.913420000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 52.913420000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Espes.')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 52.913420000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 52.913420000000000000
          Width = 68.031540000000010000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811070000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ''
            ''
            ''
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39',<frxDsMPVIts."Peca' +
              's">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 71.811070000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            ''
            ''
            ''
            '[frxDsMPVIts."NOMEUNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 71.811070000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 71.811070000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."CorTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 71.811070000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."EspesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 71.811070000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Classe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 71.811070000000000000
          Width = 68.031540000000010000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."M2Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 90.708720000000000000
          Width = 661.417750000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Observ"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 162.519790000000000000
          Width = 113.385900000000000000
          Height = 45.354360000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data da'
            'entrega: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 162.519790000000000000
          Width = 566.929499999999900000
          Height = 45.354360000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -37
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."Entrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 143.622140000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 143.622140000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '  [FormatFloat('#39'000'#39',<frxDsMPVIts."Pedido">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236239999999990000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cliente: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 30.236239999999990000
          Width = 393.070866140000000000
          Height = 18.897650000000000000
          DataField = 'NOMECLIENTE'
          DataSet = frxDsMPP
          DataSetName = 'frxDsMPP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."NOMECLIENTE"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708720000000000000
          Width = 18.897650000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          Rotation = 90
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Top = 143.622140000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Dta pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Top = 143.622140000000000000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."DataF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 30.236239999999990000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tipo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 30.236239999999990000
          Width = 196.535560000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NOMEMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object frxDsFluxosCodigo: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 143.622140000000000000
          Width = 351.496290000000000000
          Height = 18.897650000000000000
          DataSetName = 'frxDsFluxos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              '[FormatFloat('#39'000'#39', <frxDsMPVIts."Fluxo">)] - [frxDsMPVIts."NOME' +
              'FLUXO"]'
            '')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 143.622140000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Fluxo:')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Top = 207.874150000000000000
          Width = 45.354176930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 207.874150000000000000
          Width = 634.960856930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."GraGruX"] - [frxDsMPVIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 226.771800000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita recurtimento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 226.771800000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 226.771800000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita acabamento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 226.771800000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 226.771800000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NO_ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 226.771800000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NO_ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Top = 321.260050000000000000
          Width = 684.094930000000000000
          Height = 181.417269130000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Top = 283.464750000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Top = 283.464750000000000000
          Width = 196.535560000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 283.464750000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo136: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 283.464750000000000000
          Width = 177.637910000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          AllowVectorExport = True
          Top = 283.464750000000000000
          Width = 215.433210000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Entrada Wet Blue')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Top = 540.472790000000000000
          Width = 684.094930000000000000
          Height = 185.196799130000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Top = 502.677489999999900000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 502.677489999999900000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 502.677489999999900000
          Width = 177.637910000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Top = 502.677489999999900000
          Width = 215.433210000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Entrada no Acabamento')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 54.803174020000000000
        Top = 895.748610000000000000
        Width = 680.315400000000000000
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
        RowCount = 0
        object frxDsFluxosItsOrdem: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 17.007874020000000000
          DataField = 'Ordem'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Ordem"]')
          ParentFont = False
        end
        object frxDsFluxosItsNOMEOPERACAO: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 219.212740000000000000
          Height = 17.007874020000000000
          DataField = 'NOMEOPERACAO'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."NOMEOPERACAO"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao1: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao1'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao1"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao2: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao2'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao2"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao3: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao3'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao3"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao4: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao4'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao4"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 17.007874020000030000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data entrada:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 17.007874020000030000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 17.007874020000030000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hora entrada:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 17.007874020000030000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 17.007874020000030000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 17.007874020000030000
          Width = 257.007874020000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 34.015770000000310000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data t'#233'rmino:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 34.015770000000310000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 34.015770000000310000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hora t'#233'rmino:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 34.015770000000310000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 34.015770000000310000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 34.015770000000310000
          Width = 257.007874020000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 820.158010000000000000
        Width = 680.315400000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Opera'#231#227'o / Processo')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 1')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 2')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 3')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 4')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle2: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 260.787555350000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 238.110390000000000000
          Width = 139.842585590000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID e Nome do Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Top = 238.110390000000000000
          Width = 79.370098270000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 238.110390000000000000
          Width = 71.811038270000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Top = 238.110390000000000000
          Width = 52.913400470000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 196.535560000000000000
          Width = 75.590556060000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Top = 238.110390000000000000
          Width = 158.740223390000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID e Nome do fornecedor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Width = 328.818865910000000000
          Height = 18.897650000000000000
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em: [Date] [Time]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Top = 52.913420000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 52.913420000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 52.913420000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 52.913420000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 52.913420000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Espes.')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 52.913420000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 52.913420000000000000
          Width = 68.031540000000010000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811070000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ''
            ''
            ''
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39',<frxDsMPVIts."Peca' +
              's">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 71.811070000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            ''
            ''
            ''
            '[frxDsMPVIts."NOMEUNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 71.811070000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 71.811070000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."CorTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 71.811070000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."EspesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 71.811070000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Classe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 71.811070000000000000
          Width = 68.031540000000010000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."M2Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 90.708720000000000000
          Width = 661.417750000000000000
          Height = 52.913385830000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -29
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'FICHA DE BAIXA DO ESTOQUE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Top = 162.519790000000000000
          Width = 113.385900000000000000
          Height = 34.015770000000010000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data da'
            'entrega: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 162.519790000000000000
          Width = 566.929499999999900000
          Height = 34.015770000000010000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."Entrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 143.622140000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 143.622140000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '  [FormatFloat('#39'000'#39',<frxDsMPVIts."Pedido">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236239999999990000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cliente: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 30.236239999999990000
          Width = 393.070866140000000000
          Height = 18.897650000000000000
          DataField = 'NOMECLIENTE'
          DataSet = frxDsMPP
          DataSetName = 'frxDsMPP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."NOMECLIENTE"]')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708720000000000000
          Width = 18.897650000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          Rotation = 90
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Top = 143.622140000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Dta pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Top = 143.622140000000000000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."DataF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 30.236239999999990000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tipo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 30.236239999999990000
          Width = 196.535560000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NOMEMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 143.622140000000000000
          Width = 351.496290000000000000
          Height = 18.897650000000000000
          DataSetName = 'frxDsFluxos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              '[FormatFloat('#39'000'#39', <frxDsMPVIts."Fluxo">)] - [frxDsMPVIts."NOME' +
              'FLUXO"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 143.622140000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Fluxo:')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Top = 196.535560000000000000
          Width = 45.354176930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 196.535560000000000000
          Width = 445.984356930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."GraGruX"] - [frxDsMPVIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Top = 215.433210000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita recurtimento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 215.433210000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 215.433210000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita acabamento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 215.433210000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 215.433210000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NO_ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 215.433210000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NO_ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 351.496290000000000000
          Height = 30.236240000000000000
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'OS: [frxDsMPVIts."Controle"]    [frxDsMPVIts."NOMETIPOPROD"] ')
          ParentFont = False
          WordWrap = False
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 196.535560000000000000
          Width = 113.385856060000000000
          Height = 18.897632910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 238.110390000000000000
          Width = 177.637866060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID e nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692910000000000
        Top = 340.157700000000000000
        Width = 680.315400000000000000
        RowCount = 10
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Width = 60.472455590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Width = 79.370098270000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Width = 71.811038270000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 52.913400470000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Width = 117.165381180000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913388270000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 105.826803390000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Width = 56.692925590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Width = 83.149611180000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vag.Controle VSArtGGX, ggx.Controle GraGruX, '
      'CONCAT(vac.Nome, " > ", gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'ART_NO_PRD_TAM_COR, vac.Fluxo, vac.ReceiRecu,'
      'vac.ReceiRefu, vac.ReceiAcab, vac.TxtMPs,'
      'vac.Observa'
      'FROM vsartggx vag'
      'LEFT JOIN vsartcab   vac ON vag.Codigo=vac.Codigo'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vag.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY ART_NO_PRD_TAM_COR, GraGruX')
    Left = 20
    Top = 32
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXART_NO_PRD_TAM_COR: TWideStringField
      FieldName = 'ART_NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXVSArtGGX: TIntegerField
      FieldName = 'VSArtGGX'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 20
    Top = 76
  end
  object QrReceiRecu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome '
      'FROM formulas'
      'ORDER BY Nome')
    Left = 85
    Top = 30
    object QrReceiRecuNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrReceiRecuNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsReceiRecu: TDataSource
    DataSet = QrReceiRecu
    Left = 81
    Top = 74
  end
  object QrReceirefu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome '
      'FROM formulas'
      'ORDER BY Nome')
    Left = 149
    Top = 30
    object QrReceirefuNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrReceirefuNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsReceiRefu: TDataSource
    DataSet = QrReceirefu
    Left = 149
    Top = 74
  end
  object QrReceiAcab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome'
      'FROM tintascab'
      'ORDER BY Nome'
      ''
      '')
    Left = 216
    Top = 28
    object QrReceiAcabNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrReceiAcabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsReceiAcab: TDataSource
    DataSet = QrReceiAcab
    Left = 216
    Top = 72
  end
  object PMLocNumero: TPopupMenu
    Left = 124
    Top = 120
    object Pedido1: TMenuItem
      Caption = '&Pedido'
      OnClick = Pedido1Click
    end
    object OSitemdepedido1: TMenuItem
      Caption = '&OS (item de pedido)'
      OnClick = OSitemdepedido1Click
    end
  end
  object QrWBMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM wbmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 876
    Top = 285
    object QrWBMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWBMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrWBMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrWBMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrWBMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrWBMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrWBMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWBMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWBMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWBMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWBMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWBMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWBMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWBMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrWBMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrWBMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrWBMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrWBMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrWBMovItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrWBMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrWBMovItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrWBMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
  end
  object DsWBMovIts: TDataSource
    DataSet = QrWBMovIts
    Left = 876
    Top = 329
  end
  object frxReport1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39496.531362662000000000
    ReportOptions.LastChange = 39496.531362662000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  Page2.Visible := <VAR_OSImpInfWB>;                            ' +
        '                     '
      'end.')
    OnGetValue = frxOS_2GetValue
    Left = 797
    Top = 69
    Datasets = <
      item
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
      end
      item
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMPP
        DataSetName = 'frxDsMPP'
      end
      item
        DataSet = frxDsMPVIts
        DataSetName = 'frxDsMPVIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 317.480520000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 309.921215910000000000
          Height = 18.897650000000000000
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em: [Date] [Time]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Width = 366.614410000000000000
          Height = 30.236240000000000000
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'OS: [frxDsMPVIts."Controle"]    [frxDsMPVIts."NOMETIPOPROD"] ')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 52.913420000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 52.913420000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 52.913420000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 52.913420000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 52.913420000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Espes.')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 52.913420000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 52.913420000000000000
          Width = 68.031540000000010000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811070000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ''
            ''
            ''
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39',<frxDsMPVIts."Peca' +
              's">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 71.811070000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            ''
            ''
            ''
            '[frxDsMPVIts."NOMEUNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 71.811070000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 71.811070000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."CorTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 71.811070000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."EspesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 71.811070000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Classe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 71.811070000000000000
          Width = 68.031540000000010000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."M2Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 90.708720000000000000
          Width = 661.417750000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Observ"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 162.519790000000000000
          Width = 113.385900000000000000
          Height = 45.354360000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data da'
            'entrega: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 162.519790000000000000
          Width = 566.929499999999900000
          Height = 45.354360000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -37
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."Entrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 143.622140000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 143.622140000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '  [FormatFloat('#39'000'#39',<frxDsMPVIts."Pedido">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236239999999990000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cliente: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 30.236239999999990000
          Width = 393.070866140000000000
          Height = 18.897650000000000000
          DataField = 'NOMECLIENTE'
          DataSet = frxDsMPP
          DataSetName = 'frxDsMPP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."NOMECLIENTE"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708720000000000000
          Width = 18.897650000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          Rotation = 90
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Top = 143.622140000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Dta pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Top = 143.622140000000000000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."DataF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 30.236239999999990000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tipo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 30.236239999999990000
          Width = 196.535560000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NOMEMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object frxDsFluxosCodigo: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 143.622140000000000000
          Width = 351.496290000000000000
          Height = 18.897650000000000000
          DataSetName = 'frxDsFluxos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              '[FormatFloat('#39'000'#39', <frxDsMPVIts."Fluxo">)] - [frxDsMPVIts."NOME' +
              'FLUXO"]'
            '')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 143.622140000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Fluxo:')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Top = 207.874150000000000000
          Width = 45.354176930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 207.874150000000000000
          Width = 634.960856930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."GraGruX"] - [frxDsMPVIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 226.771800000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita recurtimento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 226.771800000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 226.771800000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita acabamento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 226.771800000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 226.771800000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NO_ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 226.771800000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NO_ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Top = 245.669450000000000000
          Width = 75.590600000000000000
          Height = 71.811070000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Entrega: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 245.669450000000000000
          Width = 604.724800000000000000
          Height = 45.354360000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 245.669450000000000000
          Width = 151.181200000000000000
          Height = 22.677170240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Recurtido')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 245.669450000000000000
          Width = 151.181200000000000000
          Height = 22.677170240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Semi-acabado')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 245.669450000000000000
          Width = 151.181200000000000000
          Height = 22.677170240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pr'#233'-acabado')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Top = 245.669450000000000000
          Width = 151.181200000000000000
          Height = 22.677170240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Acabado')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 268.346630000000000000
          Width = 83.149660000000000000
          Height = 22.677170240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 268.346630000000000000
          Width = 68.031540000000010000
          Height = 22.677170240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 268.346630000000000000
          Width = 83.149660000000000000
          Height = 22.677170240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Top = 268.346630000000000000
          Width = 68.031540000000010000
          Height = 22.677170240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 268.346630000000000000
          Width = 83.149660000000000000
          Height = 22.677170240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 268.346630000000000000
          Width = 68.031540000000010000
          Height = 22.677170240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Top = 268.346630000000000000
          Width = 83.149660000000000000
          Height = 22.677170240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 268.346630000000000000
          Width = 68.031540000000010000
          Height = 22.677170240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 291.023810000000000000
          Width = 83.149660000000000000
          Height = 26.456692910000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 291.023810000000000000
          Width = 68.031540000000010000
          Height = 26.456692910000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 291.023810000000000000
          Width = 83.149660000000000000
          Height = 26.456692910000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Top = 291.023810000000000000
          Width = 68.031540000000010000
          Height = 26.456692910000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 291.023810000000000000
          Width = 83.149660000000000000
          Height = 26.456692910000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 291.023810000000000000
          Width = 68.031540000000010000
          Height = 26.456692910000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Top = 291.023810000000000000
          Width = 83.149660000000000000
          Height = 26.456692910000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 291.023810000000000000
          Width = 68.031540000000010000
          Height = 26.456692910000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 54.803174020000000000
        Top = 434.645950000000000000
        Width = 680.315400000000000000
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
        RowCount = 0
        object frxDsFluxosItsOrdem: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 17.007874020000000000
          DataField = 'Ordem'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Ordem"]')
          ParentFont = False
        end
        object frxDsFluxosItsNOMEOPERACAO: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 219.212740000000000000
          Height = 17.007874020000000000
          DataField = 'NOMEOPERACAO'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."NOMEOPERACAO"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao1: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao1'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao1"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao2: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao2'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao2"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao3: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao3'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao3"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao4: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao4'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao4"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 17.007874019999920000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data entrada:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 17.007874019999920000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 17.007874019999920000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hora entrada:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 17.007874019999920000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 17.007874019999920000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 17.007874019999920000
          Width = 257.007874020000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 34.015770000000150000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data t'#233'rmino:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 34.015770000000150000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 34.015770000000150000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hora t'#233'rmino:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 34.015770000000150000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 34.015770000000150000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 34.015770000000150000
          Width = 257.007874020000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Opera'#231#227'o / Processo')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 1')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 2')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 3')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 4')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle2: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 260.787555350000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 238.110390000000000000
          Width = 139.842585590000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID e Nome do Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Top = 238.110390000000000000
          Width = 79.370098270000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 238.110390000000000000
          Width = 71.811038270000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Top = 238.110390000000000000
          Width = 52.913400470000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 196.535560000000000000
          Width = 75.590556060000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Top = 238.110390000000000000
          Width = 158.740223390000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID e Nome do fornecedor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Width = 328.818865910000000000
          Height = 18.897650000000000000
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em: [Date] [Time]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Top = 52.913420000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 52.913420000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 52.913420000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 52.913420000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 52.913420000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Espes.')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 52.913420000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 52.913420000000000000
          Width = 68.031540000000010000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811070000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ''
            ''
            ''
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39',<frxDsMPVIts."Peca' +
              's">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 71.811070000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            ''
            ''
            ''
            '[frxDsMPVIts."NOMEUNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 71.811070000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 71.811070000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."CorTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 71.811070000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."EspesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 71.811070000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Classe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 71.811070000000000000
          Width = 68.031540000000010000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."M2Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 90.708720000000000000
          Width = 661.417750000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Observ"]')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Top = 162.519790000000000000
          Width = 113.385900000000000000
          Height = 34.015770000000010000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data da'
            'entrega: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 162.519790000000000000
          Width = 566.929499999999900000
          Height = 34.015770000000010000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."Entrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 143.622140000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 143.622140000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '  [FormatFloat('#39'000'#39',<frxDsMPVIts."Pedido">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236239999999990000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cliente: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 30.236239999999990000
          Width = 393.070866140000000000
          Height = 18.897650000000000000
          DataField = 'NOMECLIENTE'
          DataSet = frxDsMPP
          DataSetName = 'frxDsMPP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."NOMECLIENTE"]')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708720000000000000
          Width = 18.897650000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          Rotation = 90
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Top = 143.622140000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Dta pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Top = 143.622140000000000000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."DataF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 30.236239999999990000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tipo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 30.236239999999990000
          Width = 196.535560000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NOMEMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 143.622140000000000000
          Width = 351.496290000000000000
          Height = 18.897650000000000000
          DataSetName = 'frxDsFluxos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              '[FormatFloat('#39'000'#39', <frxDsMPVIts."Fluxo">)] - [frxDsMPVIts."NOME' +
              'FLUXO"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 143.622140000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Fluxo:')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Top = 196.535560000000000000
          Width = 45.354176930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 196.535560000000000000
          Width = 445.984356930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."GraGruX"] - [frxDsMPVIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Top = 215.433210000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita recurtimento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 215.433210000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 215.433210000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita acabamento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 215.433210000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 215.433210000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NO_ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 215.433210000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NO_ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 351.496290000000000000
          Height = 30.236240000000000000
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'OS: [frxDsMPVIts."Controle"]    [frxDsMPVIts."NOMETIPOPROD"] ')
          ParentFont = False
          WordWrap = False
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 196.535560000000000000
          Width = 113.385856060000000000
          Height = 18.897632910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 238.110390000000000000
          Width = 177.637866060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID e nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692910000000000
        Top = 340.157700000000000000
        Width = 680.315400000000000000
        RowCount = 10
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Width = 60.472455590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Width = 79.370098270000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Width = 71.811038270000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 52.913400470000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Width = 117.165381180000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913388270000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 105.826803390000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Width = 56.692925590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Width = 83.149611180000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrEmitCus: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emt.Codigo, emt.DataEmis, emt.Numero, emt.Setor,'
      'emt.NOMESETOR, emt.NOME, cus.Controle, cus.Custo,'
      'cus.Pecas, cus.Peso, cus.AreaM2, cus.PercTotCus'
      'FROM emitcus cus'
      'LEFT JOIN emit emt ON emt.Codigo=cus.Codigo'
      'WHERE cus.MPVIts=0')
    Left = 200
    Top = 264
    object QrEmitCusCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmitCusDataEmis: TDateTimeField
      FieldName = 'DataEmis'
    end
    object QrEmitCusNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrEmitCusSetor: TSmallintField
      FieldName = 'Setor'
    end
    object QrEmitCusNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrEmitCusNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrEmitCusControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEmitCusCusto: TFloatField
      FieldName = 'Custo'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEmitCusPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEmitCusPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEmitCusAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEmitCusPercTotCus: TFloatField
      FieldName = 'PercTotCus'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsEmitCus: TDataSource
    DataSet = QrEmitCus
    Left = 200
    Top = 312
  end
  object frxOS_3: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39496.531362662000000000
    ReportOptions.LastChange = 39496.531362662000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  Page2.Visible := <VAR_OSImpInfWB>;                            ' +
        '                     '
      'end.')
    OnGetValue = frxOS_2GetValue
    Left = 885
    Top = 21
    Datasets = <
      item
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
      end
      item
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMPP
        DataSetName = 'frxDsMPP'
      end
      item
        DataSet = frxDsMPVIts
        DataSetName = 'frxDsMPVIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 487.559340710000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 309.921215910000000000
          Height = 18.897650000000000000
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em: [Date] [Time]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Width = 366.614410000000000000
          Height = 30.236240000000000000
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'OS: [frxDsMPVIts."Controle"]    [frxDsMPVIts."NOMETIPOPROD"] ')
          ParentFont = False
          WordWrap = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 79.370130000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 79.370130000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 79.370130000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000000000
          Top = 79.370130000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Espes.')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 79.370130000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 79.370130000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Top = 98.267780000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ''
            ''
            ''
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39',<frxDsMPVIts."Peca' +
              's">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 98.267780000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            ''
            ''
            ''
            '[frxDsMPVIts."NOMEUNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 98.267780000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 98.267780000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."CorTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000000000
          Top = 98.267780000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."EspesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 98.267780000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Classe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 98.267780000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."M2Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 117.165430000000000000
          Width = 661.417750000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Observ"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 188.976500000000000000
          Width = 113.385900000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data semi: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 188.976500000000000000
          Width = 226.771653540000000000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."DtaCrust"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 170.078850000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 170.078850000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '  [FormatFloat('#39'000'#39',<frxDsMPVIts."Pedido">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236239999999990000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 30.236239999999990000
          Width = 627.401726140000000000
          Height = 18.897650000000000000
          DataField = 'NOMECLIENTE'
          DataSet = frxDsMPP
          DataSetName = 'frxDsMPP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."NOMECLIENTE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 117.165430000000000000
          Width = 18.897650000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          Rotation = 90
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Top = 170.078850000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Dta pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Top = 170.078850000000000000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."DataF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 49.133890000000010000
          Width = 37.795300000000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tipo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 49.133890000000010000
          Width = 196.535560000000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NOMEMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object frxDsFluxosCodigo: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 170.078850000000000000
          Width = 351.496290000000000000
          Height = 18.897650000000000000
          DataSetName = 'frxDsFluxos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              '[FormatFloat('#39'000'#39', <frxDsMPVIts."Fluxo">)] - [frxDsMPVIts."NOME' +
              'FLUXO"]'
            '')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 170.078850000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Fluxo:')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Top = 245.669450000000000000
          Width = 86.929006930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 245.669450000000000000
          Width = 593.386026930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."GraGruX"] - [frxDsMPVIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 264.567100000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita recurtimento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 264.567100000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 264.567100000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita acabamento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 264.567100000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 264.567100000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NO_ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 264.567100000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NO_ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Top = 359.055350000000000000
          Width = 680.314960630000000000
          Height = 45.354330710000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Top = 321.260050000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Top = 321.260050000000000000
          Width = 196.535560000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 321.260050000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo136: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 321.260050000000000000
          Width = 177.637910000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          AllowVectorExport = True
          Top = 321.260050000000000000
          Width = 215.433210000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Entrada Wet Blue')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Top = 442.205010000000000000
          Width = 680.314960630000000000
          Height = 45.354330710000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Top = 404.409710000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 404.409710000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 404.409710000000000000
          Width = 177.637910000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Top = 404.409710000000000000
          Width = 215.433210000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Entrada no Acabamento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Top = 49.133890000000010000
          Width = 98.267780000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Transportador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 49.133890000000010000
          Width = 347.716506140000000000
          Height = 15.118110236220470000
          DataSet = frxDsMPP
          DataSetName = 'frxDsMPP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."NOMETRANSP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo121: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 98.267780000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Condi'#231#227'o pagto.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 64.252010000000000000
          Width = 347.716760000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."NO_CONDICAOPG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 64.252010000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'OC:')
          ParentFont = False
          WordWrap = False
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 64.252010000000000000
          Width = 196.535560000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."PedidCli"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 188.976500000000000000
          Width = 113.385900000000000000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data da'
            'entrega: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo126: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 188.976500000000000000
          Width = 226.771653540000000000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."Entrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Top = 226.771800000000000000
          Width = 86.929006930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Configura'#231#227'o:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 226.771800000000000000
          Width = 593.386026930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."VSArtCab_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 54.803174020000000000
        Top = 604.724800000000000000
        Width = 680.315400000000000000
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
        RowCount = 0
        object frxDsFluxosItsOrdem: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 17.007874020000000000
          DataField = 'Ordem'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Ordem"]')
          ParentFont = False
        end
        object frxDsFluxosItsNOMEOPERACAO: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 219.212740000000000000
          Height = 17.007874020000000000
          DataField = 'NOMEOPERACAO'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."NOMEOPERACAO"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao1: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao1'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao1"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao2: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao2'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao2"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao3: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao3'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao3"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao4: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao4'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao4"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 17.007874020000030000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data entrada:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 17.007874020000030000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 17.007874020000030000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hora entrada:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 17.007874020000030000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 17.007874020000030000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 17.007874020000030000
          Width = 257.007874020000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 34.015770000000310000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data t'#233'rmino:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 34.015770000000310000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 34.015770000000310000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hora t'#233'rmino:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 34.015770000000310000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 34.015770000000310000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 34.015770000000310000
          Width = 257.007874020000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 529.134200000000000000
        Width = 680.315400000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Opera'#231#227'o / Processo')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 1')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 2')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 3')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 4')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle2: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 260.787555350000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 238.110390000000000000
          Width = 139.842585590000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID e Nome do Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Top = 238.110390000000000000
          Width = 79.370098270000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 238.110390000000000000
          Width = 71.811038270000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Top = 238.110390000000000000
          Width = 52.913400470000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 196.535560000000000000
          Width = 75.590556060000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Top = 238.110390000000000000
          Width = 158.740223390000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID e Nome do fornecedor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Width = 328.818865910000000000
          Height = 18.897650000000000000
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em: [Date] [Time]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Top = 52.913420000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 52.913420000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 52.913420000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 52.913420000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 52.913420000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Espes.')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 52.913420000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 52.913420000000000000
          Width = 68.031540000000010000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811070000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ''
            ''
            ''
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39',<frxDsMPVIts."Peca' +
              's">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 71.811070000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            ''
            ''
            ''
            '[frxDsMPVIts."NOMEUNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 71.811070000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 71.811070000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."CorTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 71.811070000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."EspesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 71.811070000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Classe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 71.811070000000000000
          Width = 68.031540000000010000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."M2Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 90.708720000000000000
          Width = 661.417750000000000000
          Height = 52.913385830000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -29
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'FICHA DE BAIXA DO ESTOQUE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Top = 162.519790000000000000
          Width = 113.385900000000000000
          Height = 34.015770000000010000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data da'
            'entrega: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 162.519790000000000000
          Width = 566.929499999999900000
          Height = 34.015770000000010000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."Entrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 143.622140000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 143.622140000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '  [FormatFloat('#39'000'#39',<frxDsMPVIts."Pedido">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236239999999990000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cliente: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 30.236239999999990000
          Width = 393.070866140000000000
          Height = 18.897650000000000000
          DataField = 'NOMECLIENTE'
          DataSet = frxDsMPP
          DataSetName = 'frxDsMPP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."NOMECLIENTE"]')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708720000000000000
          Width = 18.897650000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          Rotation = 90
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Top = 143.622140000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Dta pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Top = 143.622140000000000000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."DataF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 30.236239999999990000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tipo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 30.236239999999990000
          Width = 196.535560000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NOMEMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 143.622140000000000000
          Width = 351.496290000000000000
          Height = 18.897650000000000000
          DataSetName = 'frxDsFluxos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              '[FormatFloat('#39'000'#39', <frxDsMPVIts."Fluxo">)] - [frxDsMPVIts."NOME' +
              'FLUXO"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 143.622140000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Fluxo:')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Top = 196.535560000000000000
          Width = 45.354176930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 196.535560000000000000
          Width = 445.984356930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."GraGruX"] - [frxDsMPVIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Top = 215.433210000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita recurtimento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 215.433210000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 215.433210000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita acabamento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 215.433210000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 215.433210000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NO_ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 215.433210000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NO_ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 351.496290000000000000
          Height = 30.236240000000000000
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'OS: [frxDsMPVIts."Controle"]    [frxDsMPVIts."NOMETIPOPROD"] ')
          ParentFont = False
          WordWrap = False
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 196.535560000000000000
          Width = 113.385856060000000000
          Height = 18.897632910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 238.110390000000000000
          Width = 177.637866060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID e nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692910000000000
        Top = 340.157700000000000000
        Width = 680.315400000000000000
        RowCount = 10
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Width = 60.472455590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Width = 79.370098270000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Width = 71.811038270000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 52.913400470000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Width = 117.165381180000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913388270000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 105.826803390000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Width = 56.692925590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Width = 83.149611180000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrPediPrzCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, MaxDesco,'
      'JurosMes, Parcelas, Percent1, Percent2'
      'FROM pediprzcab'
      'WHERE Aplicacao & :P0 <> 0'
      'ORDER BY Nome')
    Left = 868
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 940
    Top = 76
  end
  object QrTransp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 909
    Top = 137
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransp: TDataSource
    DataSet = QrTransp
    Left = 909
    Top = 181
  end
  object QrVSArtCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSArtCabBeforeClose
    AfterScroll = QrVSArtCabAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM vsartcab'
      'ORDER BY Nome, Codigo')
    Left = 208
    Top = 448
    object QrVSArtCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSArtCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSArtCabReceiRecu: TIntegerField
      FieldName = 'ReceiRecu'
      Required = True
    end
    object QrVSArtCabReceiRefu: TIntegerField
      FieldName = 'ReceiRefu'
      Required = True
    end
    object QrVSArtCabReceiAcab: TIntegerField
      FieldName = 'ReceiAcab'
      Required = True
    end
    object QrVSArtCabTxtMPs: TWideStringField
      FieldName = 'TxtMPs'
      Size = 255
    end
    object QrVSArtCabObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrVSArtCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSArtCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSArtCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSArtCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSArtCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSArtCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrVSArtCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrVSArtCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrVSArtCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrVSArtCabFluxProCab: TIntegerField
      FieldName = 'FluxProCab'
      Required = True
    end
    object QrVSArtCabFluxPcpCab: TIntegerField
      FieldName = 'FluxPcpCab'
      Required = True
    end
    object QrVSArtCabLinCulReb: TIntegerField
      FieldName = 'LinCulReb'
      Required = True
    end
    object QrVSArtCabLinCabReb: TIntegerField
      FieldName = 'LinCabReb'
      Required = True
    end
    object QrVSArtCabLinCulSem: TIntegerField
      FieldName = 'LinCulSem'
      Required = True
    end
    object QrVSArtCabLinCabSem: TIntegerField
      FieldName = 'LinCabSem'
      Required = True
    end
  end
  object DsVSArtCab: TDataSource
    DataSet = QrVSArtCab
    Left = 208
    Top = 496
  end
  object QrFluxPcpCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM fluxpcpcab'
      'ORDER BY Nome')
    Left = 564
    Top = 532
    object QrFluxPcpCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFluxPcpCabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsFluxPcpCab: TDataSource
    DataSet = QrFluxPcpCab
    Left = 564
    Top = 580
  end
  object QrFluxPcpIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DiaSeq, IniStg, FimStg  '
      'FROM fluxpcpits '
      'WHERE Codigo=1 '
      'ORDER BY DiaSeq ')
    Left = 376
    Top = 576
    object QrFluxPcpItsDiaSeq: TIntegerField
      FieldName = 'DiaSeq'
      Required = True
    end
    object QrFluxPcpItsIniStg: TIntegerField
      FieldName = 'IniStg'
      Required = True
    end
    object QrFluxPcpItsFimStg: TIntegerField
      FieldName = 'FimStg'
      Required = True
    end
  end
  object QrFeriado: TMySQLQuery
    Database = Dmod.MyDB
    Left = 736
    Top = 572
  end
  object QrMPVPss: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fpi.Nome NO_IniStg, fpf.Nome NO_FimStg, mps.* '
      'FROM mpvpss mps'
      'LEFT JOIN fluxpcpstg fpi ON fpi.Codigo=mps.IniStg'
      'LEFT JOIN fluxpcpstg fpf ON fpf.Codigo=mps.FimStg'
      'WHERE mps.MPVIts=:P0')
    Left = 872
    Top = 509
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPVPssNO_IniStg: TWideStringField
      FieldName = 'NO_IniStg'
      Size = 30
    end
    object QrMPVPssNO_FimStg: TWideStringField
      FieldName = 'NO_FimStg'
      Size = 30
    end
    object QrMPVPssMPVIts: TIntegerField
      FieldName = 'MPVIts'
      Required = True
    end
    object QrMPVPssDiaSeq: TIntegerField
      FieldName = 'DiaSeq'
      Required = True
    end
    object QrMPVPssConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrMPVPssDataSeq: TDateField
      FieldName = 'DataSeq'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPVPssIniStg: TIntegerField
      FieldName = 'IniStg'
      Required = True
    end
    object QrMPVPssFimStg: TIntegerField
      FieldName = 'FimStg'
      Required = True
    end
    object QrMPVPssDtHrReal: TDateTimeField
      FieldName = 'DtHrReal'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsMPVPss: TDataSource
    DataSet = QrMPVPss
    Left = 876
    Top = 553
  end
end
