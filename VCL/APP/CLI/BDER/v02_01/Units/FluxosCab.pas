unit FluxosCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmFluxosCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrFluxos: TmySQLQuery;
    QrFluxosLk: TIntegerField;
    QrFluxosDataCad: TDateField;
    QrFluxosDataAlt: TDateField;
    QrFluxosUserCad: TIntegerField;
    QrFluxosUserAlt: TIntegerField;
    QrFluxosCodigo: TSmallintField;
    QrFluxosNome: TWideStringField;
    DsFluxos: TDataSource;
    QrFluxosIts: TmySQLQuery;
    QrFluxosItsCodigo: TIntegerField;
    QrFluxosItsControle: TIntegerField;
    QrFluxosItsOrdem: TIntegerField;
    QrFluxosItsOperacao: TIntegerField;
    QrFluxosItsLk: TIntegerField;
    QrFluxosItsDataCad: TDateField;
    QrFluxosItsDataAlt: TDateField;
    QrFluxosItsUserCad: TIntegerField;
    QrFluxosItsUserAlt: TIntegerField;
    QrFluxosItsNOMEOPERACAO: TWideStringField;
    QrFluxosItsSEQ: TIntegerField;
    DsFluxosIts: TDataSource;
    QrOrdena: TmySQLQuery;
    QrOrdenaCodigo: TIntegerField;
    QrOrdenaControle: TIntegerField;
    QrOrdenaOrdem: TIntegerField;
    QrOrdenaOperacao: TIntegerField;
    QrOrdenaAcao1: TWideStringField;
    QrOrdenaAcao2: TWideStringField;
    QrOrdenaAcao3: TWideStringField;
    QrOrdenaAcao4: TWideStringField;
    QrOrdenaLk: TIntegerField;
    QrOrdenaDataCad: TDateField;
    QrOrdenaDataAlt: TDateField;
    QrOrdenaUserCad: TIntegerField;
    QrOrdenaUserAlt: TIntegerField;
    QrOrdenaNOMEOPERACAO: TWideStringField;
    QrOrdenaSEQ: TIntegerField;
    DBGFluxosIct: TDBGrid;
    QrFluxosSet: TmySQLQuery;
    DsFluxosSet: TDataSource;
    DBGrid2: TDBGrid;
    QrFluxosSetNO_SETOR: TWideStringField;
    QrFluxosSetCodigo: TIntegerField;
    QrFluxosSetControle: TIntegerField;
    QrFluxosSetOrdem: TIntegerField;
    QrFluxosSetSetor: TIntegerField;
    BtSetor: TBitBtn;
    PMSetor: TPopupMenu;
    Adicionasetor1: TMenuItem;
    Removeosetorselecionado1: TMenuItem;
    N1: TMenuItem;
    Duplicaoperaoatual1: TMenuItem;
    N2: TMenuItem;
    ReordenaOperacoes1: TMenuItem;
    QrFluxosItsAcao1: TWideStringField;
    QrFluxosItsAcao2: TWideStringField;
    QrFluxosItsAcao3: TWideStringField;
    QrFluxosItsAcao4: TWideStringField;
    N3: TMenuItem;
    Duplicafluxoatual1: TMenuItem;
    N4: TMenuItem;
    Gerenciaritens1: TMenuItem;
    BtCtr: TBitBtn;
    PMCtr: TPopupMenu;
    Incluinovocontrole1: TMenuItem;
    Alteracontroleatual1: TMenuItem;
    Excluicontroleatual1: TMenuItem;
    QrFluxosICt: TMySQLQuery;
    DsFluxosICt: TDataSource;
    QrFluxosICtCodigo: TIntegerField;
    QrFluxosICtControle: TIntegerField;
    QrFluxosICtConta: TIntegerField;
    QrFluxosICtAcao1: TWideStringField;
    QrFluxosICtOrdem: TIntegerField;
    DBGFluxosIts: TDBGrid;
    QrFluxosICtSEQ: TIntegerField;
    Splitter1: TSplitter;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFluxosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFluxosBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrFluxosAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrFluxosItsCalcFields(DataSet: TDataSet);
    procedure Adicionasetor1Click(Sender: TObject);
    procedure Removeosetorselecionado1Click(Sender: TObject);
    procedure BtSetorClick(Sender: TObject);
    procedure QrFluxosBeforeClose(DataSet: TDataSet);
    procedure ItsExclui1Click(Sender: TObject);
    procedure Duplicaoperaoatual1Click(Sender: TObject);
    procedure ReordenaOperacoes1Click(Sender: TObject);
    procedure Duplicafluxoatual1Click(Sender: TObject);
    procedure Gerenciaritens1Click(Sender: TObject);
    procedure Incluinovocontrole1Click(Sender: TObject);
    procedure Alteracontroleatual1Click(Sender: TObject);
    procedure Excluicontroleatual1Click(Sender: TObject);
    procedure QrFluxosICtCalcFields(DataSet: TDataSet);
    procedure QrFluxosItsAfterScroll(DataSet: TDataSet);
    procedure QrFluxosItsBeforeClose(DataSet: TDataSet);
    procedure BtCtrClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormFluxosIts(SQLType: TSQLType);
    procedure MostraFormFluxosItsMul();
    procedure MostraFormFluxosSet(SQLType: TSQLType);
    procedure MostraFormFluxosICt(SQLType: TSQLType);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenFluxosIts(Controle: Integer);
    procedure ReopenFluxosICt(Conta: Integer);
    procedure ReopenFluxosSet(Controle: Integer);
    procedure Reordena(Pula: Boolean; Posicao: Integer);
  end;

var
  FmFluxosCab: TFmFluxosCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, ModuleGeral, Module, MyDBCheck, DmkDAC_PF, UnReordena,
  FluxosIts, FluxosSet, FluxosItsMul, FluxosICt;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFluxosCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFluxosCab.MostraFormFluxosICt(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFluxosICt, FmFluxosICt, afmoNegarComAviso) then
  begin
    FmFluxosICt.ImgTipo.SQLType := SQLType;
    FmFluxosICt.EdCodCodi.ValueVariant := QrFluxosCodigo.Value;
    FmFluxosICt.EdCodNome.ValueVariant := QrFluxosNome.Value;
    FmFluxosICt.EdCtrCodi.ValueVariant := QrFluxosItsControle.Value;
    FmFluxosICt.EdCtrNome.ValueVariant := QrFluxosItsNOMEOPERACAO.Value;
    if SQLType = stIns then
    begin
      FmFluxosICt.EdOrdem.ValueVariant := QrFluxosICtOrdem.Value + 1;
    end else
    begin
      FmFluxosICt.EdConta.ValueVariant := QrFluxosICtConta.Value;
      //
      FmFluxosICt.EdOrdem.ValueVariant := QrFluxosICtOrdem.Value;

      FmFluxosICt.EdAcao1.Text := QrFluxosICtAcao1.Value;
      FmFluxosICt.CkContinuar.Checked := False;

    end;
    FmFluxosICt.ShowModal;
    FmFluxosICt.Destroy;
  end;
end;

procedure TFmFluxosCab.MostraFormFluxosIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFluxosIts, FmFluxosIts, afmoNegarComAviso) then
  begin
    FmFluxosIts.ImgTipo.SQLType := SQLType;
    FmFluxosIts.FQrCab := QrFluxos;
    FmFluxosIts.FDsCab := DsFluxos;
    FmFluxosIts.FQrIts := QrFluxosIts;
    if SQLType = stIns then
      //
    else
    begin
      FmFluxosIts.EdControle.ValueVariant := QrFluxosItsControle.Value;
      //
      FmFluxosIts.EdOperacao.ValueVariant := QrFluxosItsOperacao.Value;
      FmFluxosIts.CBOperacao.KeyValue := QrFluxosItsOperacao.Value;
      FmFluxosIts.EdOrdem.ValueVariant := QrFluxosItsOrdem.Value;

      FmFluxosIts.EdAcao1.Text := QrFluxosItsAcao1.Value;
      FmFluxosIts.EdAcao2.Text := QrFluxosItsAcao2.Value;
      FmFluxosIts.EdAcao3.Text := QrFluxosItsAcao3.Value;
      FmFluxosIts.EdAcao4.Text := QrFluxosItsAcao4.Value;

    end;
    FmFluxosIts.ShowModal;
    FmFluxosIts.Destroy;
  end;
end;

procedure TFmFluxosCab.MostraFormFluxosItsMul();
begin
  if DBCheck.CriaFm(TFmFluxosItsMul, FmFluxosItsMul, afmoNegarComAviso) then
  begin
    FmFluxosItsMul.FCodigo := QrFluxosCodigo.Value;
    FmFluxosItsMul.ShowModal;
    FmFluxosItsMul.Destroy;
    //
    ReopenFluxosIts(0);
  end;
end;

procedure TFmFluxosCab.MostraFormFluxosSet(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFluxosSet, FmFluxosSet, afmoNegarComAviso) then
  begin
    FmFluxosSet.ImgTipo.SQLType := SQLType;
    FmFluxosSet.FQrCab := QrFluxos;
    FmFluxosSet.FDsCab := DsFluxos;
    FmFluxosSet.FQrIts := QrFluxosSet;
    if SQLType = stIns then
      //
    else
    begin
      FmFluxosSet.EdControle.ValueVariant := QrFluxosSetControle.Value;
      //
      FmFluxosSet.EdSetor.ValueVariant := QrFluxosSetSetor.Value;
      FmFluxosSet.CBSetor.KeyValue := QrFluxosSetSetor.Value;
      FmFluxosSet.EdOrdem.ValueVariant := QrFluxosSetOrdem.Value;

    end;
    FmFluxosSet.ShowModal;
    FmFluxosSet.Destroy;
  end;
end;

procedure TFmFluxosCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrFluxos);
  MyObjects.HabilitaMenuItemCabUpd(Duplicafluxoatual1, QrFluxos);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrFluxos, QrFluxosIts);
end;

procedure TFmFluxosCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrFluxos);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrFluxosIts);
  MyObjects.HabilitaMenuItemItsUpd(Gerenciaritens1, QrFluxos);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrFluxosIts);
  MyObjects.HabilitaMenuItemItsDel(Duplicaoperaoatual1, QrFluxosIts);
  MyObjects.HabilitaMenuItemItsDel(ReordenaOperacoes1, QrFluxosIts);
end;

procedure TFmFluxosCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFluxosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFluxosCab.DefParams;
begin
  VAR_GOTOTABELA := 'Fluxos';
  VAR_GOTOMYSQLTABLE := QrFluxos;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM fluxos');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome LIKE :P0');
  //
end;

procedure TFmFluxosCab.Duplicafluxoatual1Click(Sender: TObject);
var
  Codigo, Controle: Integer;
  Nome: String;
begin
  if Geral.MB_Pergunta('Confirma a duplica��o do fluxo atual?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    QrFluxos.DisableControls;
    QrFluxosIts.DisableControls;
    QrFluxosSet.DisableControls;
    //
    try
      Codigo := UMyMod.BuscaEmLivreY_Def('fluxos', 'Codigo', stIns, 0);
      Nome   := Copy('<c�pia> ' + QrFluxosNome.Value, 1, 100);
      //
      if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'fluxos', TMeuDB,
        ['Codigo'], [QrFluxosCodigo.Value],
        ['Codigo', 'Nome'],
        [Codigo, Nome],
        '', True, LaAviso1, LaAviso2) then
      begin
        QrFluxosIts.First;
        while not QrFluxosIts.Eof do
        begin
          Controle := UMyMod.BuscaEmLivreY_Def('fluxosits', 'Controle', stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'fluxosits', TMeuDB,
            ['Controle'], [QrFluxosItsControle.Value],
            ['Codigo', 'Controle'],
            [Codigo, Controle],
            '', True, LaAviso1, LaAviso2);
          //
          QrFluxosIts.Next;
        end;
        QrFluxosSet.First;
        while not QrFluxosSet.Eof do
        begin
          Controle := UMyMod.BPGS1I32('fluxosset', 'Controle', '', '', tsPos, stIns, 0);
          //
          if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'fluxosset', TMeuDB,
          ['Controle'], [QrFluxosSetControle.Value],
          ['Controle', 'Codigo'],
          [Controle, Codigo],
          '', True, LaAviso1, LaAviso2) then
          begin
            //
          end;
          //
          QrFluxosSet.Next;
        end;
      end;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      LocCod(Codigo, Codigo);
      //
      Screen.Cursor := crDefault;
      Geral.MB_Info('Duplica��o conclu�da com sucesso!');
    finally
      QrFluxos.EnableControls;
      QrFluxosIts.EnableControls;
      QrFluxosSet.EnableControls;
    end;
  end;
end;

procedure TFmFluxosCab.Duplicaoperaoatual1Click(Sender: TObject);
var
  Controle, Ordem, Numero: Integer;
begin
  if (QrFluxosIts.State <> dsInactive) and (QrFluxosIts.RecordCount > 0) then
  begin
    if Geral.MB_Pergunta('Deseja realmente duplicar esta opera��o?') <> ID_YES then Exit;
    //
    Screen.Cursor := crHourGlass;
    try
      QrFluxosIts.DisableControls;
      QrFluxosIts.DisableControls;
      //
      Controle := UMyMod.BuscaEmLivreY_Def('fluxosits', 'Controle', stIns,
                    QrFluxosItsControle.Value);
      Ordem    := UReordena.VerificaOrdem(Dmod.QrAux, Dmod.MyDB, 'Codigo',
                    'Ordem', 'fluxosits', QrFluxosCodigo.Value, 0, True);
      //
      if not UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'fluxosits', TMeuDB,
        ['Controle', 'Ordem'], [QrFluxosItsControle.Value, QrFluxosItsOrdem.Value],
        ['Controle', 'Ordem'], [Controle, Ordem],
        '', True, LaAviso1, LaAviso2) then
      begin
        Geral.MB_Erro('Falha ao duplicar opera��o!');
        Exit;
      end;
    finally
      QrFluxosIts.EnableControls;
      QrFluxosIts.EnableControls;
      //
      ReopenFluxosIts(Controle);
      //
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmFluxosCab.Excluicontroleatual1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrFluxosIct, DBGFluxosICt,
    'fluxosict', ['Conta'], ['Conta'], istPergunta, '');
end;

procedure TFmFluxosCab.Incluinovocontrole1Click(Sender: TObject);
begin
  MostraFormFluxosICt(stIns);
end;

procedure TFmFluxosCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormFluxosIts(stUpd);
end;

procedure TFmFluxosCab.ItsExclui1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrFluxosIts, DBGFluxosIts,
    'fluxosits', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmFluxosCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmFluxosCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFluxosCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFluxosCab.Removeosetorselecionado1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a retirada do setor selecionado?',
  'fluxosset', 'Controle', QrFluxosSetControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFluxosSet,
      QrFluxosSetControle, QrFluxosSetControle.Value);
    ReopenFluxosSet(Controle);
  end;
end;

procedure TFmFluxosCab.ReopenFluxosICt(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFluxosIct, Dmod.MyDB, [
  'SELECT * ',
  'FROM fluxosict flc ',
  'WHERE flc.Controle=' + Geral.FF0(QrFluxosItsControle.Value),
  'ORDER BY flc.Ordem, flc.Conta DESC ',  '']);
  //
  QrFluxosICt.Locate('Conta', Conta, []);
end;

procedure TFmFluxosCab.ReopenFluxosIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFluxosIts, Dmod.MyDB, [
  'SELECT ope.Nome NOMEOPERACAO, fli.* ',
  'FROM fluxosits fli ',
  'LEFT JOIN operacoes ope ON ope.Codigo=fli.Operacao ',
  'WHERE fli.Codigo=' + Geral.FF0(QrFluxosCodigo.Value),
  'ORDER BY fli.Ordem, fli.Controle DESC ',
  '']);
  //
  QrFluxosIts.Locate('Controle', Controle, []);
end;


procedure TFmFluxosCab.ReopenFluxosSet(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFluxosSet, Dmod.MyDB, [
  'SELECT lse.Nome NO_SETOR, fls.* ',
  'FROM fluxosset fls ',
  'LEFT JOIN listasetores lse ON lse.Codigo=fls.Setor ',
  'WHERE fls.Codigo=' + Geral.FF0(QrFluxosCodigo.Value),
  'ORDER BY fls.Ordem, fls.Controle DESC ',
  '']);
  //
  QrFluxosSet.Locate('Controle', Controle, []);
end;

procedure TFmFluxosCab.Reordena(Pula: Boolean; Posicao: Integer);
var
  Ordem, Real: Integer;
begin
  QrOrdena.Close;
  QrOrdena.Params[0].AsInteger := QrFluxosCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrOrdena, Dmod.MyDB);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE fluxosits SET Ordem=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 AND Controle=:P2');
  //
  Ordem := 0;
  while not QrOrdena.Eof do
  begin
    Ordem := Ordem + 1;
    Real := Ordem;
    if Pula then if Real >= Posicao then Real := Real + 1;
    Dmod.QrUpd.Params[00].AsInteger := Real;
    Dmod.QrUpd.Params[01].AsInteger := QrFluxosCodigo.Value;
    Dmod.QrUpd.Params[02].AsInteger := QrOrdenaControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrOrdena.Next;
  end;
end;

procedure TFmFluxosCab.ReordenaOperacoes1Click(Sender: TObject);
begin
  if (QrFluxosIts.State <> dsInactive) and(QrFluxosIts.RecordCount > 0) then
  begin
    UReordena.ReordenaItens(QrFluxosIts, Dmod.QrUpd, 'fluxosits',
      'Ordem', 'Controle', 'NOMEOPERACAO', '', '', '', nil);
    //
    ReopenFluxosIts(0);
  end;
end;

procedure TFmFluxosCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFluxosCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFluxosCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFluxosCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFluxosCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFluxosCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFluxosCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFluxosCodigo.Value;
  Close;
end;

procedure TFmFluxosCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormFluxosIts(stIns);
end;

procedure TFmFluxosCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFluxos, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'fluxos');
end;

procedure TFmFluxosCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('fluxos', 'Codigo', ImgTipo.SQLType,
    QrFluxosCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'fluxos',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmFluxosCab.BtCtrClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCtr, BtCtr);
end;

procedure TFmFluxosCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'fluxos', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'fluxos', 'Codigo');
end;

procedure TFmFluxosCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmFluxosCab.Adicionasetor1Click(Sender: TObject);
begin
  MostraFormFluxosSet(stIns);
end;

procedure TFmFluxosCab.BtSetorClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSetor, BtSetor);
end;

procedure TFmFluxosCab.Alteracontroleatual1Click(Sender: TObject);
begin
  MostraFormFluxosICt(stUpd);
end;

procedure TFmFluxosCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmFluxosCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DBGFluxosIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmFluxosCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFluxosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFluxosCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFluxosCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFluxosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFluxosCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFluxosCab.QrFluxosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFluxosCab.QrFluxosAfterScroll(DataSet: TDataSet);
begin
  ReopenFluxosIts(0);
  ReopenFluxosSet(0);
end;

procedure TFmFluxosCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrFluxosCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmFluxosCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFluxosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'fluxos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFluxosCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFluxosCab.Gerenciaritens1Click(Sender: TObject);
begin
  MostraFormFluxosItsMul();
end;

procedure TFmFluxosCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFluxos, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'fluxos');
end;

procedure TFmFluxosCab.QrFluxosBeforeClose(DataSet: TDataSet);
begin
  QrFluxosIts.Close;
  QrFluxosSet.Close;
end;

procedure TFmFluxosCab.QrFluxosBeforeOpen(DataSet: TDataSet);
begin
  QrFluxosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFluxosCab.QrFluxosICtCalcFields(DataSet: TDataSet);
begin
  QrFluxosICtSEQ.Value := QrFluxosIct.RecNo;
end;

procedure TFmFluxosCab.QrFluxosItsAfterScroll(DataSet: TDataSet);
begin
  ReopenFluxosICt(0);
end;

procedure TFmFluxosCab.QrFluxosItsBeforeClose(DataSet: TDataSet);
begin
  QrFluxosICt.Close;
end;

procedure TFmFluxosCab.QrFluxosItsCalcFields(DataSet: TDataSet);
begin
  QrFluxosItsSEQ.Value := QrFluxosIts.RecNo;
end;

end.

