object FmFormulasCorrigeGrupo: TFmFormulasCorrigeGrupo
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Corrige Grupo em Produtos de Uso e Consumo'
  ClientHeight = 420
  ClientWidth = 685
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 685
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 691
    object GB_R: TGroupBox
      Left = 637
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 643
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 589
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 595
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 338
        Height = 32
        Caption = 'Corrige Grupo em F'#243'rmulas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 338
        Height = 32
        Caption = 'Corrige Grupo em F'#243'rmulas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 338
        Height = 32
        Caption = 'Corrige Grupo em F'#243'rmulas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 685
    Height = 145
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 691
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 685
      Height = 145
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 691
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 685
        Height = 145
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 691
        object LaAviso: TLabel
          Left = 2
          Top = 15
          Width = 681
          Height = 16
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Align = alTop
          Caption = 
            'Segue a baixo lista de produtos que est'#227'o sem grupo definido e p' +
            'recisam ser regularizados!'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitWidth = 641
        end
        object dmkDBGrid1: TdmkDBGrid
          Left = 2
          Top = 77
          Width = 681
          Height = 66
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Numero'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsFormulas
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Numero'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 31
          Width = 681
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitWidth = 687
          object Label1: TLabel
            Left = 140
            Top = 0
            Width = 36
            Height = 13
            Caption = 'Origem:'
          end
          object Label2: TLabel
            Left = 448
            Top = 1
            Width = 100
            Height = 13
            Caption = 'Pesagens a partir de:'
          end
          object Label22: TLabel
            Left = 563
            Top = 2
            Width = 28
            Height = 13
            Caption = 'Setor:'
          end
          object EdOri: TdmkEditCB
            Left = 140
            Top = 17
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdOriRedefinido
            DBLookupComboBox = CBOri
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBOri: TdmkDBLookupComboBox
            Left = 197
            Top = 17
            Width = 248
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsOri
            TabOrder = 1
            dmkEditCB = EdOri
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object TpPesaIni: TdmkEditDateTimePicker
            Left = 448
            Top = 17
            Width = 112
            Height = 21
            Date = 45226.000000000000000000
            Time = 0.435635532405285600
            TabOrder = 2
            OnClick = TpPesaIniClick
            OnChange = TpPesaIniChange
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CBSetor: TDBLookupComboBox
            Left = 564
            Top = 17
            Width = 107
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsListaSetores
            TabOrder = 3
            OnClick = CBSetorClick
          end
          object RGCampo: TRadioGroup
            Left = 4
            Top = 0
            Width = 129
            Height = 41
            Caption = 'Campo:'
            Columns = 2
            Items.Strings = (
              'Grupo'
              'Setor')
            TabOrder = 4
            OnClick = RGCampoClick
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 306
    Width = 685
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 691
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 681
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 687
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 440
        Height = 17
        Caption = 
          'Selecione os produtos na grade e em seguida clique no bot'#227'o: A'#231#227 +
          'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 440
        Height = 17
        Caption = 
          'Selecione os produtos na grade e em seguida clique no bot'#227'o: A'#231#227 +
          'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 350
    Width = 685
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 691
    object PnSaiDesis: TPanel
      Left = 539
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 545
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 537
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 543
      object BtAcao: TBitBtn
        Tag = 294
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&A'#231#227'o'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAcaoClick
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 193
    Width = 685
    Height = 113
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alBottom
    TabOrder = 4
    ExplicitWidth = 691
    object LaGrupo: TLabel
      Left = 12
      Top = 4
      Width = 39
      Height = 13
      Caption = 'Destino:'
    end
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 12
      Top = 66
      Width = 120
      Height = 40
      Caption = '&Confirma'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 138
      Top = 66
      Width = 120
      Height = 40
      Caption = '&Desiste'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtDesisteClick
    end
    object PB1: TProgressBar
      Left = 12
      Top = 45
      Width = 246
      Height = 17
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      TabOrder = 2
    end
    object EdDst: TdmkEditCB
      Left = 12
      Top = 21
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBDst
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBDst: TdmkDBLookupComboBox
      Left = 77
      Top = 21
      Width = 310
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsOri
      TabOrder = 4
      dmkEditCB = EdDst
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object QrFormulas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome, Ativo'
      'FROM Formulas')
    Left = 344
    Top = 200
    object QrFormulasNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrFormulasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 348
    Top = 248
  end
  object PMAcao: TPopupMenu
    OnPopup = PMAcaoPopup
    Left = 72
    Top = 456
    object Corrigirgrupo1: TMenuItem
      Caption = '&Corrigir grupo de produtos de uso e consumo selecionados'
      OnClick = Corrigirgrupo1Click
    end
  end
  object QrOri: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM formulasgru'
      'ORDER BY Nome')
    Left = 560
    Top = 120
    object QrOriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOriNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsOri: TDataSource
    DataSet = QrOri
    Left = 560
    Top = 168
  end
  object QrDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM formulasgru'
      'ORDER BY Nome')
    Left = 564
    Top = 220
    object QrDstCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDstNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsDst: TDataSource
    DataSet = QrDst
    Left = 564
    Top = 268
  end
  object QrListaSetores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM listasetores'
      'ORDER BY Nome')
    Left = 456
    Top = 188
    object QrListaSetoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'listasetores.Codigo'
    end
    object QrListaSetoresNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'listasetores.Nome'
    end
  end
  object DsListaSetores: TDataSource
    DataSet = QrListaSetores
    Left = 456
    Top = 236
  end
  object Query: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 204
    Top = 205
  end
end
