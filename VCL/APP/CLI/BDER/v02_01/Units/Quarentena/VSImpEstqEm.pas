unit VSImpEstqEm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  frxClass, frxDBSet, Data.DB, mySQLDbTables,
  dmkGeral, DmkEditCB, dmkDBGridZTO, UnDmkProcFunc, AppListas, UnInternalConsts,
  UnProjGroup_Consts, Vcl.Grids, Vcl.DBGrids, UnDmkEnums, Vcl.StdCtrls,
  Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TFmVSImpEstqEm = class(TForm)
    QrOpePWE: TmySQLQuery;
    QrOpePWEPecas: TFloatField;
    QrOpePWEAreaM2: TFloatField;
    QrOpePWEPesoKg: TFloatField;
    mySQLQuery1: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    QrOpePWEMID: TIntegerField;
    QrOpePWECod: TIntegerField;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FVmiEstqEmVmi, FVmiEstqEmAtzA, FVmiEstqEmAtzB, FVmiEstqEmS01: String;
    //
    procedure Atualiza_Ope_e_PWE();
    procedure AtualizaZerados();
    procedure GeraItensEm();
    procedure InsereVMIsDeTabela(TabelaSrc: String);
  public
    { Public declarations }
    FEntidade, FFilial, Ed00Terceiro_ValueVariant, RG00_Ordem1_ItemIndex,
    RG00_Ordem2_ItemIndex, RG00_Ordem3_ItemIndex, RG00_Agrupa_ItemIndex,
    Ed00StqCenCad_ValueVariant, RG00ZeroNegat_ItemIndex: Integer;
    FNO_EMPRESA, CB00StqCenCad_Text, CB00Terceiro_Text: String;
    TPDataRelativa_Date: TDateTime;
    Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked, FMostraFrx: Boolean;
    //EdEmpresa: TDmkEditCB;
    DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2: TdmkDBGridZTO;
    Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2: TmySQLQuery;
    //
    FLaAviso1, FLaAviso2: TLabel;
    //
    FDataEm: TDateTime;
    //
    function  ImprimeEstoqueEm(): Boolean;
  end;

var
  FmVSImpEstqEm: TFmVSImpEstqEm;

implementation

{$R *.dfm}

uses ModuleGeral, CreateBlueDerm, UnMyObjects, DmkDAC_PF, UMySQLModule, Module,
  MyDBCheck, UnVS_PF;

{ TFmVSImpEstqEm }

procedure TFmVSImpEstqEm.AtualizaZerados();
const
  Zerado = 1;
  SdoVrtPeca = 0;
  SdoVrtPeso = 0;
  SdoVrtArM2 = 0;
begin
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True,
    'Gerando saldo zerado de zerados for�ados pelo usu�rio');
  //
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FVmiEstqEmVmi, False, [
  'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], [
  'Zerado'], [
  SdoVrtPeca, SdoVrtPeso, SdoVrtArM2], [
  Zerado], False);
end;

procedure TFmVSImpEstqEm.Atualiza_Ope_e_PWE();
var
  //MovimCod,
  Codigo, MovimID, MovimNiv: Integer;
  Pecas, PesoKg, AreaM2(*, AreaP2, ValorT*): Double;
begin
  FVmiEstqEmAtzA := '_vmi_estq_em_atz_a';
  FVmiEstqEmAtzB := '_vmi_estq_em_atz_b';
  UnDMkDAC_PF.AbreMySQLQuery0(QrOpePWE, DModG.MyPID_DB, [
////////////////////////////////////////////////////////////////////////////////
  'DROP TABLE IF EXISTS ' + FVmiEstqEmAtzA + ';  ',
  'CREATE TABLE ' + FVmiEstqEmAtzA + ' ',
  'SELECT IF(MovimID IN (11,19), MovimID, SrcMovID) MID,  ',
  'IF(MovimID IN (11,19), Codigo, SrcNivel1) Cod,  ',
  'SUM(IF(MovimNiv IN (7,20), -1, 1) * Pecas) Pecas,  ',
  'SUM(IF(MovimNiv IN (7,20), -1, 1) * AreaM2) AreaM2,  ',
  'SUM(IF(MovimNiv IN (7,20), -1, 1) * AreaP2) AreaP2,  ',
  'SUM(IF(MovimNiv IN (7,20), -1, 1) * PesoKg) PesoKg,  ',
  'SUM(IF(MovimNiv IN (7,20), -1, 1) * ValorT) ValorT  ',
  'FROM ' + FVmiEstqEmVmi,
  'WHERE ( ',
  '  MovimID IN (11,19)  ',
  '  AND MovimNiv IN (7,10,20,23)  ',
  ') OR ( ',
  '    MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + ')  ',
  '    AND SrcNivel2 IN ',
  '    ( ',
  '      SELECT Controle ',
  '      FROM ' + FVmiEstqEmVmi + ' ',
  '      WHERE MovimID IN (11,19) ',
  '    ) ',
  ') ',
  'GROUP BY MID, Cod ',
  ';  ',
  'DROP TABLE IF EXISTS ' + FVmiEstqEmAtzB + ';  ',
  'CREATE TABLE ' + FVmiEstqEmAtzB + ' ',
  'SELECT MID, Cod, SUM(Pecas) Pecas, ',
  'SUM(PesoKg) PesoKg, SUM(AreaM2) AreaM2  ',
  'FROM ' + FVmiEstqEmAtzA + ' ',
  'GROUP BY MID, Cod ',
  '; ',
  'SELECT * FROM ' + FVmiEstqEmAtzB + ' ',
  'WHERE Pecas>0  ',
  '']);
  Geral.MB_SQL(Self, QrOpePWE);

  QrOpePWE.First;
  while not QrOpePWE.Eof do
  begin
    Codigo := QrOpePWECod.Value;
    MovimID := QrOpePWEMID.Value;
    case TEstqMovimID(QrOpePWEMID.Value) of
      emidEmOperacao: MovimNiv := Integer(TEstqMovimNiv.eminEmOperInn);//8
      emidEmProcWE:   MovimNiv := Integer(TEstqMovimNiv.eminEmWEndInn);//21
      else
      begin
        MovimNiv := -1;
        Geral.MB_Erro(
        '"MovimID" n�o implementado em "FmVSImpEstqEm.Atualiza_Ope_e_PWE()"');
      end;
    end;
    //
    Pecas  := QrOpePWEPecas .Value;
    PesoKg := QrOpePWEPesoKg.Value;
    AreaM2 := QrOpePWEAreaM2.Value;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FVmiEstqEmVmi, False, [
    'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], [
    'Codigo', 'MovimID', 'MovimNiv'], [
    Pecas, PesoKg, AreaM2], [
    Codigo, MovimID, MovimNiv], False);
    //
    QrOpePWE.Next;
  end;
end;

procedure TFmVSImpEstqEm.FormCreate(Sender: TObject);
begin
  FMostraFrx := True;
end;

procedure TFmVSImpEstqEm.GeraItensEm();
var
  ID_e_Niv: String;
begin
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Copiando dados de IMEIS do per�odo');
  FVmiEstqEmVmi := UnCreateBlueDerm.RecriaTempTableNovo(ntrttVmiEstqEmVmi,
    DModG.QrUpdPID1, False);
  FVmiEstqEmS01 := '_vmi_estq_em_s01_';
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Copiando dados de IMEIS do per�odo - Morto');
  InsereVMIsDeTabela(CO_TAB_VMB);
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Copiando dados de IMEIS do per�odo - Ativo');
  InsereVMIsDeTabela(CO_TAB_VMI);
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Gerando saldos de In Natura');
  // MovimID=1 (Entrada ID=1Niv=0 e baixa por ID6Niv14 + IDs9Niv0 e ID17Niv0
  UnDMkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'UPDATE ' + FVmiEstqEmVmi,
  'SET SdoVrtPeca=Pecas, SdoVrtPeso=PesoKg, SdoVrtArM2=AreaM2 ',
  'WHERE MovimID=1;',
  '',
  'DROP TABLE IF EXISTS ' + FVmiEstqEmS01 + '; ',
  'CREATE TABLE ' + FVmiEstqEmS01 + ' ',
  'SELECT SrcNivel2,  ',
  'SUM(vmi.Pecas) Pecas,  ',
  'SUM(vmi.PesoKg) PesoKg,  ',
  'SUM(vmi.AreaM2) AreaM2 ',
  'FROM ' + FVmiEstqEmVmi + ' vmi  ',
  'WHERE vmi.SrcNivel2 IN ( ',
  '  SELECT Controle ',
  '  FROM ' + FVmiEstqEmVmi + ' ',
  '  WHERE MovimID=1 ',
  ') ',
  'AND (vmi.Pecas < 0  ',
  'OR vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + ')  ',
  ') ',
  'GROUP BY SrcNivel2 ',
  '; ',
  'UPDATE ' + FVmiEstqEmVmi + ' vmi  ',
  'LEFT JOIN ' + FVmiEstqEmS01 + ' s01 ON vmi.Controle = s01.SrcNivel2 ',
  'SET  ',
  '  vmi.SdoVrtPeca = vmi.Pecas  + s01.Pecas, ',
  '  vmi.SdoVrtPeso = vmi.PesoKg + s01.PesoKg, ',
  '  vmi.SdoVrtArM2 = vmi.AreaM2 + s01.AreaM2 ',
  'WHERE vmi.MovimID=1 ',
  'AND NOT s01.Pecas IS NULL ',
  '; ',
  '']);
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Gerando saldos de movimentos');
  ID_e_Niv := VS_PF.SQL_MovIDeNiv_Pos_All();
  UnDMkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'UPDATE ' + FVmiEstqEmVmi,
  'SET SdoVrtPeca=Pecas, SdoVrtPeso=PesoKg, SdoVrtArM2=AreaM2 ',
  'WHERE (' + ID_e_Niv + ');',
  '',
  'DROP TABLE IF EXISTS ' + FVmiEstqEmS01 + '; ',
  'CREATE TABLE ' + FVmiEstqEmS01 + ' ',
  'SELECT vmi.SrcNivel2, SUM(vmi.Pecas) Pecas,  ',
  'SUM(vmi.PesoKg) PesoKg,  ',
  'SUM(vmi.AreaM2) AreaM2,  ',
  'SUM(vmi.QtdGerPeca) QtdGerPeca,  ',
  'SUM(vmi.QtdGerPeso) QtdGerPeso,  ',
  'SUM(vmi.QtdGerArM2) QtdGerArM2,  ',
  'SUM(vmi.QtdGerArP2) QtdGerArP2  ',
  'FROM ' + FVmiEstqEmVmi + ' vmi  ',
  'WHERE vmi.SrcNivel2 IN (  ',
  '  SELECT Controle  ',
  '  FROM ' + FVmiEstqEmVmi + '  ',
  '  WHERE (' + ID_e_Niv + ') ',
  ')  ',
  'AND (vmi.Pecas + vmi.PesoKg < 0  ',
  'OR vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + ')  ',
  ')  ',
  'GROUP BY SrcNivel2  ',
  '; ',
  'UPDATE ' + FVmiEstqEmVmi + ' vmi  ',
  'LEFT JOIN ' + FVmiEstqEmS01 + ' s01 ON vmi.Controle = s01.SrcNivel2 ',
  'SET  ',
  '  vmi.SdoVrtPeca = vmi.Pecas  + s01.Pecas, ',
  '  vmi.SdoVrtPeso = vmi.PesoKg + s01.PesoKg, ',
  '  vmi.SdoVrtArM2 = vmi.AreaM2 + s01.AreaM2 ',
  'WHERE ((' + ID_e_Niv + '))',
  'AND NOT s01.Pecas IS NULL ',
  '; ',
  '']);
  //
end;

function TFmVSImpEstqEm.ImprimeEstoqueEm(): Boolean;
var
  DataRetroativa: String;
begin
  Result := False;
  GeraItensEm();
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Gerando saldos de OOs e OPs');
  Atualiza_Ope_e_PWE();
  AtualizaZerados();
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Gerando relat�rio de estoque em');
  DataRetroativa := Geral.FDT(FDataEm, 2);
  Result := VS_PF.ImprimeEstoqueReal(FEntidade, FFilial, Ed00Terceiro_ValueVariant,
  RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex, RG00_Ordem3_ItemIndex,
  RG00_Agrupa_ItemIndex, Ed00StqCenCad_ValueVariant, RG00ZeroNegat_ItemIndex,
  FNO_EMPRESA, CB00StqCenCad_Text, CB00Terceiro_Text, TPDataRelativa_Date,
  Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked,
  DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2, Qr00GraGruY,
  Qr00GraGruX, Qr00CouNiv2, FVmiEstqEmVmi, DataRetroativa, FMostraFrx);
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, False, '...');
end;

procedure TFmVSImpEstqEm.InsereVMIsDeTabela(TabelaSrc: String);
begin
  UnDMkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FVmiEstqEmVmi,
  'SELECT  ',
  'Codigo, Controle, ',
  'MovimCod, MovimNiv,  ',
  'MovimTwn, Empresa, Terceiro,  ',
  'CliVenda, MovimID, LnkIDXtr,  ',
  'LnkNivXtr1, LnkNivXtr2, DataHora,  ',
  'Pallet, GraGruX, Pecas,  ',
  'PesoKg, AreaM2, AreaP2,  ',
  'ValorT, SrcMovID, SrcNivel1,  ',
  'SrcNivel2, SrcGGX, 0 SdoVrtPeca,  ',
  '0 SdoVrtPeso, 0 SdoVrtArM2, Observ,  ',
  'SerieFch, Ficha, Misturou,  ',
  'FornecMO, CustoMOKg, CustoMOTot,  ',
  'ValorMP, DstMovID, DstNivel1,  ',
  'DstNivel2, DstGGX, QtdGerPeca,  ',
  'QtdGerPeso, QtdGerArM2, QtdGerArP2,  ',
  'QtdAntPeca, QtdAntPeso, QtdAntArM2,  ',
  'QtdAntArP2, AptoUso, NotaMPAG,  ',
  'Marca, TpCalcAuto, Zerado,  ',
  'EmFluxo, NotFluxo, FatNotaVNC,  ',
  'FatNotaVRC, PedItsLib, PedItsFin,  ',
  'PedItsVda, GSPInnNiv2, GSPArtNiv2,  ',
  'CustoMOM2, ReqMovEstq, StqCenLoc,  ',
  'ItemNFe, VSMorCab, VSMulFrnCab, ',
  'ClientMO, ',
  'Ativo ',
  'FROM ' + TMeuDB + '.' + TabelaSrc,
  ' ',
  'WHERE DataHora<"' + Geral.FDT(Int(FDataEm + 1), 1) + '" ',
  '; ',
  '']);
end;

end.
