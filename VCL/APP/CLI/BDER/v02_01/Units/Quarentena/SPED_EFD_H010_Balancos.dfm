object FmSPED_EFD_H010_Balancos: TFmSPED_EFD_H010_Balancos
  Left = 339
  Top = 185
  Caption = 'SPE-D_BAL-010 :: Importa'#231#227'o de Balan'#231'o para o SPED'
  ClientHeight = 496
  ClientWidth = 560
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 560
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 512
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 464
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 446
        Height = 32
        Caption = 'Importa'#231#227'o de Balan'#231'o para o SPED'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 446
        Height = 32
        Caption = 'Importa'#231#227'o de Balan'#231'o para o SPED'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 446
        Height = 32
        Caption = 'Importa'#231#227'o de Balan'#231'o para o SPED'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 560
    Height = 334
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 331
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 560
      Height = 334
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 331
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 560
        Height = 334
        Align = alClient
        TabOrder = 0
        ExplicitHeight = 331
        object LaMes: TLabel
          Left = 48
          Top = 20
          Width = 23
          Height = 13
          Caption = 'M'#234's:'
        end
        object LaAno: TLabel
          Left = 240
          Top = 20
          Width = 22
          Height = 13
          Caption = 'Ano:'
        end
        object CBMes: TComboBox
          Left = 48
          Top = 37
          Width = 182
          Height = 21
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          Text = 'CBMes'
        end
        object CBAno: TComboBox
          Left = 240
          Top = 37
          Width = 90
          Height = 21
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Text = 'CBAno'
        end
        object CGImportar: TdmkCheckGroup
          Left = 48
          Top = 60
          Width = 461
          Height = 45
          Caption = ' Itens a importar: '
          Columns = 2
          Items.Strings = (
            'Uso e Consumo'
            'Couros (VS)')
          TabOrder = 2
          OnClick = CGImportarClick
          UpdType = utYes
          Value = 0
          OldValor = 0
        end
        object GBImportar1: TGroupBox
          Left = 48
          Top = 108
          Width = 461
          Height = 105
          Caption = ' Uso e consumo: '
          TabOrder = 3
          Visible = False
          object RGNivPlaCta1: TRadioGroup
            Left = 2
            Top = 15
            Width = 457
            Height = 41
            Align = alTop
            Caption = ' N'#237'vel do plano de contas: '
            Columns = 6
            ItemIndex = 0
            Items.Strings = (
              'Nenhum'
              'Conta'
              'Sub-grupo'
              'Grupo'
              'Conjunto'
              'Plano')
            TabOrder = 0
            OnClick = RGNivPlaCta1Click
          end
          object Panel5: TPanel
            Left = 2
            Top = 56
            Width = 457
            Height = 47
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Label1: TLabel
              Left = 4
              Top = 4
              Width = 140
              Height = 13
              Caption = 'G'#234'nero do n'#237'vel selecionado:'
            end
            object EdGenPlaCta1: TdmkEditCB
              Left = 4
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGenPlaCta1
              IgnoraDBLookupComboBox = False
            end
            object CBGenPlaCta1: TdmkDBLookupComboBox
              Left = 60
              Top = 20
              Width = 392
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsNivelSel1
              TabOrder = 1
              dmkEditCB = EdGenPlaCta1
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
        object GBImportar2: TGroupBox
          Left = 48
          Top = 216
          Width = 461
          Height = 105
          Caption = ' Couros (VS) '
          TabOrder = 4
          Visible = False
          object RGNivPlaCta2: TRadioGroup
            Left = 2
            Top = 15
            Width = 457
            Height = 41
            Align = alTop
            Caption = ' N'#237'vel do plano de contas: '
            Columns = 6
            ItemIndex = 0
            Items.Strings = (
              'Nenhum'
              'Conta'
              'Sub-grupo'
              'Grupo'
              'Conjunto'
              'Plano')
            TabOrder = 0
            OnClick = RGNivPlaCta2Click
          end
          object Panel6: TPanel
            Left = 2
            Top = 56
            Width = 457
            Height = 47
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Label2: TLabel
              Left = 4
              Top = 4
              Width = 140
              Height = 13
              Caption = 'G'#234'nero do n'#237'vel selecionado:'
            end
            object EdGenPlaCta2: TdmkEditCB
              Left = 4
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGenPlaCta2
              IgnoraDBLookupComboBox = False
            end
            object CBGenPlaCta2: TdmkDBLookupComboBox
              Left = 60
              Top = 20
              Width = 392
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsNivelSel2
              TabOrder = 1
              dmkEditCB = EdGenPlaCta2
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 382
    Width = 560
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 379
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 556
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 426
    Width = 560
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 423
    object PnSaiDesis: TPanel
      Left = 414
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 412
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 16
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPQx: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT /*pqc.Controle PQC,*/ pqx.Insumo, '
      'ggx.Controle GraGruX, '
      'SUM(pqx.Peso) Peso, SUM(pqx.Valor) Valor '
      'FROM pqx pqx'
      '/*LEFT JOIN pqcli pqc ON pqc.PQ=pqx.Insumo '
      '  AND pqc.CI=pqx.CliOrig*/'
      'LEFT JOIN gragrux ggx ON ggx.GraGru1=pqx.Insumo'
      'WHERE pqx.CliOrig=-11 '
      'AND pqx.Tipo=0 '
      'AND pqx.DataX="2014-01-01" '
      'GROUP BY pqx.Insumo, GraGruX '
      '')
    Left = 512
    Top = 52
    object QrPQxInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQxCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQxPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPQxValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    Left = 512
    Top = 100
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrGraGruXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
  end
  object QrH010: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM efd_h010'
      'WHERE ImporExpor=3'
      'AND AnoMes=201401'
      'AND Empresa=-11'
      'AND H005=1'
      'AND BalID=4'
      'AND BalNum=0'
      'AND BalItm=0')
    Left = 512
    Top = 148
    object QrH010ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrH010AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrH010Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrH010H005: TIntegerField
      FieldName = 'H005'
    end
    object QrH010LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrH010REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrH010COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrH010UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrH010QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrH010VL_UNIT: TFloatField
      FieldName = 'VL_UNIT'
    end
    object QrH010VL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
    end
    object QrH010IND_PROP: TWideStringField
      FieldName = 'IND_PROP'
      Size = 1
    end
    object QrH010COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrH010TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
    object QrH010COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrH010VL_ITEM_IR: TFloatField
      FieldName = 'VL_ITEM_IR'
    end
    object QrH010BalID: TIntegerField
      FieldName = 'BalID'
    end
    object QrH010BalNum: TIntegerField
      FieldName = 'BalNum'
    end
    object QrH010BalItm: TIntegerField
      FieldName = 'BalItm'
    end
  end
  object QrNivelSel1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 512
    Top = 196
    object QrNivelSel1Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.contas.Codigo'
    end
    object QrNivelSel1Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.contas.Nome'
      Size = 25
    end
  end
  object DsNivelSel1: TDataSource
    DataSet = QrNivelSel1
    Left = 512
    Top = 244
  end
  object QrNivelSel2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 512
    Top = 292
    object QrNivelSel2Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.contas.Codigo'
    end
    object QrNivelSel2Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.contas.Nome'
      Size = 25
    end
  end
  object DsNivelSel2: TDataSource
    DataSet = QrNivelSel2
    Left = 512
    Top = 340
  end
  object QrVMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT med.Sigla, CouNiv2, GraGruY, GragRuX, GraGru1, IF((GraGru' +
        'Y<2048 OR  '
      'CouNiv2<>1) AND SdoVrtPeso > 0, "Kg",   '
      'IF(CouNiv2=1 AND SdoVrtArM2 > 0, "m'#178'", "Erro!")) UnMedTxt,   '
      'IF((GraGruY<2048 OR  '
      'CouNiv2<>1) AND SdoVrtPeso > 0, 1,   '
      'IF(CouNiv2=1 AND SdoVrtArM2 > 0, 2, -1)) TipoEstq,   '
      'SdoVrtPeca,   '
      'SdoVrtArM2,   '
      'SdoVrtPeso   '
      'FROM _vsmovimp1_ vmi  '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragru1 gg1 ON gg1.Nivel1=vmi.' +
        'GraGru1  '
      
        'LEFT JOIN bluederm_2_1_cialeather.UnidMed med ON med.Codigo=gg1.' +
        'UnidMed  '
      '  '
      'WHERE SdoVrtPeca > 0  '
      'AND (  '
      '  ((GraGruY<2048 OR CouNiv2<>1) AND SdoVrtPeso > 0)  '
      '   OR  '
      '  (CouNiv2=1 AND SdoVrtArM2 > 0)  '
      ')  ')
    Left = 512
    Top = 388
    object QrVMISigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrVMICouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrVMIGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVMIGragRuX: TIntegerField
      FieldName = 'GragRuX'
    end
    object QrVMIGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVMIUnMedTxt: TWideStringField
      FieldName = 'UnMedTxt'
      Required = True
      Size = 5
    end
    object QrVMITipoEstq: TLargeintField
      FieldName = 'TipoEstq'
      Required = True
    end
    object QrVMISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVMISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVMISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVMINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 512
    end
    object QrVMIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVMIGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrVMIUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrVMIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVMIValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
end
