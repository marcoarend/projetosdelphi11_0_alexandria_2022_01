unit VSImpRendPWE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditCB, UnDmkProcFunc,
  dmkDBLookupComboBox, dmkEdit, mySQLDbTables, AppListas, dmkDBGridZTO, frxClass,
  frxDBSet;

type
  TFmVSImpRendPWE = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrRend: TmySQLQuery;
    QrIndef: TmySQLQuery;
    QrIndefGraGruX: TIntegerField;
    QrIndefCouNiv1: TIntegerField;
    QrIndefCouNiv2: TIntegerField;
    QrIndefArtigoImp: TWideStringField;
    QrIndefClasseImp: TWideStringField;
    QrIndefPrevPcPal: TIntegerField;
    QrIndefMediaMinM2: TFloatField;
    QrIndefMediaMaxM2: TFloatField;
    QrIndefLk: TIntegerField;
    QrIndefDataCad: TDateField;
    QrIndefDataAlt: TDateField;
    QrIndefUserCad: TIntegerField;
    QrIndefUserAlt: TIntegerField;
    QrIndefAlterWeb: TSmallintField;
    QrIndefAtivo: TSmallintField;
    QrIndefMediaMinKg: TFloatField;
    QrIndefMediaMaxKg: TFloatField;
    QrIndefPrevAMPal: TFloatField;
    QrIndefPrevKgPal: TFloatField;
    QrIndefGrandeza: TSmallintField;
    QrIndefBastidao: TSmallintField;
    QrIndefNO_CouNiv1: TWideStringField;
    QrIndefNO_CouNiv2: TWideStringField;
    QrIndefGraGruY: TIntegerField;
    QrIndefNO_GGY: TWideStringField;
    QrIndefNO_PRD_TAM_COR: TWideStringField;
    DBGIndef: TdmkDBGridZTO;
    DsIndef: TDataSource;
    frxWET_CURTI_153_A: TfrxReport;
    frxDsRend: TfrxDBDataset;
    QrRendBastidao: TSmallintField;
    QrRendNO_FORNECE: TWideStringField;
    QrRendGGX20: TIntegerField;
    QrRendNO_PRD_TAM_COR_22: TWideStringField;
    QrRendNO_PRD_TAM_COR_20: TWideStringField;
    QrRendRendimento: TFloatField;
    QrRendNFeRem: TIntegerField;
    QrRendCodigo: TIntegerField;
    QrRendMovimCod: TIntegerField;
    QrRendCtrl22: TIntegerField;
    QrRendMovimTwn: TIntegerField;
    QrRendEmpresa: TIntegerField;
    QrRendTerceiro: TIntegerField;
    QrRendMovimID: TIntegerField;
    QrRendDataHora: TDateTimeField;
    QrRendPallet: TIntegerField;
    QrRendGGX22: TIntegerField;
    QrRendPecas_22: TFloatField;
    QrRendAreaM2_22: TFloatField;
    QrRendCtrl23: TIntegerField;
    QrRendGGX23: TIntegerField;
    QrRendPecas_23: TFloatField;
    QrRendAreaM2_23: TFloatField;
    QrRendDATA: TDateField;
    QrRendNO_Bastidao: TWideStringField;
    QrRendCtrl20: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_153_AGetValue(const VarName: string;
      var Value: Variant);
    function frxWET_CURTI_153_AUserFunction(const MethodName: string;
      var Params: Variant): Variant;
  private
    { Private declarations }
    procedure ReopenRend();
    procedure AlteraAtual(Bastidao: Integer);
    //
    procedure MyFunc(IMEI22: Integer);
  public
    { Public declarations }
    FEmpresa_Cod, FFornece_Cod, FOPIni, FOPFim, FAgrupa, FOrdem1, FOrdem2,
    FOrdem3, FOrdem4, FBastidoes: Integer;
    FEmpresa_Txt, FFornece_Txt: String;
    FDataIni, FDataFim: TDateTime;
    FUsaDtIni, FUsaDtFim, FUsaOPIni, FUsaOPFim: Boolean;
    FDBG21CouNiv2: TdmkDBGridZTO;
    FQr21CouNiv2: TmySQLQuery;
    //
    procedure ReopenIndef(Verifica: Boolean);
    procedure ImprimeRendimentoSemi();
  end;

  var
  FmVSImpRendPWE: TFmVSImpRendPWE;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UMySQLModule, UnVS_PF;

var
  VAR_TESTE: Integer;

{$R *.DFM}

procedure TFmVSImpRendPWE.AlteraAtual(Bastidao: Integer);
var
  GraGruX: Integer;
begin
  GraGruX := QrIndefGraGruX.Value;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragruxcou', False, [
  'Bastidao'], ['GraGruX'], [Bastidao], [GraGruX], True);
end;

procedure TFmVSImpRendPWE.BtOKClick(Sender: TObject);
var
  I, Bastidao: Integer;
begin
  if MyObjects.FIC(DBGIndef.SelectedRows.Count = 0, nil,
  'Selecione pelo menos um item!') then
    Exit;
  //
  Bastidao := MyObjects.SelRadioGroup('Bastid�o', 'Selecione a bastid�o', sVSBastidao, 1);
  if (Bastidao < 0) then
    Exit;
  //
  QrIndef.DisableControls;
  try
    with DBGIndef.DataSource.DataSet do
    for I := 0 to DBGIndef.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(DBGIndef.SelectedRows.Items[I]));
      //
      AlteraAtual(Bastidao);
    end;
  finally
    QrIndef.EnableControls;
  end;
  //
  ReopenIndef(False);
end;

procedure TFmVSImpRendPWE.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpRendPWE.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpRendPWE.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  frxWET_CURTI_153_A.AddFunction(
    'Procedure MyFunc(IMEI22: Integer)');
  //
end;

procedure TFmVSImpRendPWE.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpRendPWE.frxWET_CURTI_153_AGetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', FEmpresa_Txt, FEmpresa_Cod, 'TODAS')
  else
(*
  if VarName = 'VARF_NO_STQCENCAD' then
    Value := dmkPF.ParValueCodTxt('Centro de estoque: ',
    CB00StqCenCad_Text, Ed00StqCenCad_ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_NO_FORNECE' then
    Value := dmkPF.ParValueCodTxt('Fornecedor: ',
    CB00Terceiro_Text, Ed00Terceiro_ValueVariant, 'TODOS')
  else
*)
  if VarName ='VARF_DATA' then
    Value := Now()
  else
(*
  if VarName ='VARF_DATA_MEU' then
    Value := TPDataRelativa_Date
  else
*)
  if VarName ='VARF_DATA_IMP' then
    Value := Now()
  else
  if VarName ='VARF_VSNIVGER' then
    Value := '00000' //Dmod.QrControleVSNivGer.Value    <=== Nao usa! Tirar!
  else
end;

function TFmVSImpRendPWE.frxWET_CURTI_153_AUserFunction(
  const MethodName: string; var Params: Variant): Variant;
begin
  if MethodName = 'MYFUNC' then
    MyFunc(Params[0]);
end;

procedure TFmVSImpRendPWE.ImprimeRendimentoSemi();
var
  Index: Integer;
  Ordem, GroupBy, NO_Arq: String;
  Grupo0, Grupo1, Grupo2, Grupo3: TfrxGroupHeader;
  Futer0, Futer1, Futer2, Futer3: TfrxGroupFooter;
  Me_GH0, Me_FT0, Me_GH1, Me_FT1, Me_GH2, Me_FT2, Me_GH3, Me_FT3,
  MeValNome, MeTitNome, MeTitCodi, MeValCodi: TfrxMemoView;
  frxReport: TfrxReport;
  //
  procedure SetaItens(Ordem: Integer; Grupo: TfrxGroupHeader; Me_GH, Me_FT:
  TfrxMemoView);
  begin
    case Ordem of
      0:
      begin
        Grupo.Condition := 'frxDsRend."Terceiro"';
        Me_GH.Memo.Text := '[frxDsRend."Terceiro"] - [frxDsRend."NO_FORNECE"]';
        Me_FT.Memo.Text := '[frxDsRend."Terceiro"] - [frxDsRend."NO_FORNECE"]: ';
      end;
      1:
      begin
        Grupo.Condition := 'frxDsRend."NFeRem"';
        Me_GH.Memo.Text := '[frxDsRend."NFeRem"]';
        Me_FT.Memo.Text := '[frxDsRend."NFeRem"]: ';
      end;
      2:
      begin
        Grupo.Condition := 'frxDsRend."DATA"';
        Me_GH.Memo.Text := '[frxDsRend."DATA"]';
        Me_FT.Memo.Text := '[frxDsRend."DATA"]: ';
      end;
      3:
      begin
        Grupo.Condition := 'frxDsRend."GGX20"';
        Me_GH.Memo.Text := '[frxDsRend."GGX20"] - [frxDsRend."NO_PRD_TAM_COR_20"]';
        Me_FT.Memo.Text := '[frxDsRend."GGX20"] - [frxDsRend."NO_PRD_TAM_COR_20"]: ';
      end;
      4:
      begin
        Grupo.Condition := 'frxDsRend."GGX22"';
        Me_GH.Memo.Text := '[frxDsRend."GGX22"] - [frxDsRend."NO_PRD_TAM_COR_22"]';
        Me_FT.Memo.Text := '[frxDsRend."GGX22"] - [frxDsRend."NO_PRD_TAM_COR_22"]: ';
      end;
      5:
      begin
        Grupo.Condition := 'frxDsRend."Bastidao"';
        Me_GH.Memo.Text := '[frxDsRend."Bastidao"] - [frxDsRend."NO_Bastidao"]';
        Me_FT.Memo.Text := '[frxDsRend."Bastidao"] - [frxDsRend."NO_Bastidao"]: ';
      end;
      6:
      begin
        Grupo.Condition := 'frxDsRend."MovimCod"';
        Me_GH.Memo.Text := '[frxDsRend."MovimCod"]';
        Me_FT.Memo.Text := 'OP [frxDsRend."MovimCod"]: ';
      end;
      7:
      begin
        Grupo.Condition := 'frxDsRend."Ctrl20"';
        Me_GH.Memo.Text := '[frxDsRend."Ctrl20"]';
        Me_FT.Memo.Text := 'IME-I origem [frxDsRend."Ctrl20"]: ';
      end;
      8:
      begin
        Grupo.Condition := 'frxDsRend."Ctrl22"';
        Me_GH.Memo.Text := '[frxDsRend."Ctrl22"]';
        Me_FT.Memo.Text := 'IME-I destino [frxDsRend."Ctrl22"]: ';
      end;
      else Grupo.Condition := '***ERRO***';
    end;
  end;
begin
  ReopenRend();
  //
  frxReport := frxWET_CURTI_153_A;
  //
  Grupo0 := frxReport.FindObject('GH_00') as TfrxGroupHeader;
  Grupo0.Visible := FAgrupa > 0;
  Futer0 := frxReport.FindObject('FT_00') as TfrxGroupFooter;
  Futer0.Visible := Grupo0.Visible;
  Me_GH0 := frxReport.FindObject('Me_GH0') as TfrxMemoView;
  Me_FT0 := frxReport.FindObject('Me_FT0') as TfrxMemoView;
  SetaItens(FOrdem1, Grupo0, Me_GH0, Me_FT0);
  //
  Grupo1 := frxReport.FindObject('GH_01') as TfrxGroupHeader;
  Grupo1.Visible := FAgrupa > 1;
  Futer1 := frxReport.FindObject('FT_01') as TfrxGroupFooter;
  Futer1.Visible := Grupo1.Visible;
  Me_GH1 := frxReport.FindObject('Me_GH1') as TfrxMemoView;
  Me_FT1 := frxReport.FindObject('Me_FT1') as TfrxMemoView;
  SetaItens(FOrdem2, Grupo1, Me_GH1, Me_FT1);
  //
  Grupo2 := frxReport.FindObject('GH_02') as TfrxGroupHeader;
  Grupo2.Visible := FAgrupa > 2;
  Futer2 := frxReport.FindObject('FT_02') as TfrxGroupFooter;
  Futer2.Visible := Grupo2.Visible;
  Me_GH2 := frxReport.FindObject('Me_GH2') as TfrxMemoView;
  Me_FT2 := frxReport.FindObject('Me_FT2') as TfrxMemoView;
  SetaItens(FOrdem3, Grupo2, Me_GH2, Me_FT2);
  //
  Grupo3 := frxReport.FindObject('GH_03') as TfrxGroupHeader;
  Grupo3.Visible := FAgrupa > 3;
  Futer3 := frxReport.FindObject('FT_03') as TfrxGroupFooter;
  Futer3.Visible := Grupo3.Visible;
  Me_GH3 := frxReport.FindObject('Me_GH3') as TfrxMemoView;
  Me_FT3 := frxReport.FindObject('Me_FT3') as TfrxMemoView;
  SetaItens(FOrdem4, Grupo3, Me_GH3, Me_FT3);
  //
  MeValNome := frxReport.FindObject('MeValNome') as TfrxMemoView;
  MeTitNome := frxReport.FindObject('MeTitNome') as TfrxMemoView;
  MeTitCodi := frxReport.FindObject('MeTitCodi') as TfrxMemoView;
  MeValCodi := frxReport.FindObject('MeValCodi') as TfrxMemoView;
  //
  MeTitCodi.Memo.Text := 'Reduzido';
  MeValCodi.Memo.Text := '[frxDsRend."GGX20"]';
  MeTitNome.Memo.Text := 'Nome da mat�ria-prima';
  MeValNome.Memo.Text := '[frxDsRend."NO_PRD_TAM_COR_20"]';
  //
  MyObjects.frxDefineDataSets(frxReport, [
    DModG.frxDsDono,
    frxDsRend
  ]);
  //
  NO_Arq := VS_PF.ObtemNomeFrxEstoque('', FAgrupa,
    FOrdem1, FOrdem2, FOrdem3,
    FOrdem4, 0, '');
  MyObjects.frxMostra(frxReport, 'Rendimento de Semi Acabado', NO_Arq);
end;

procedure TFmVSImpRendPWE.MyFunc(IMEI22: Integer);
begin
    VS_PF.MostraFormVSMovimID(IMEI22);
end;

procedure TFmVSImpRendPWE.ReopenIndef(Verifica: Boolean);
  procedure Reabre();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrIndef, Dmod.MyDB, [
    'SELECT cou.*, nv1.Nome NO_CouNiv1, nv2.Nome NO_CouNiv2,  ',
    'ggx.GraGruY, ggy.Nome NO_GGY, CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
    'NO_PRD_TAM_COR ',
    'FROM gragruxcou cou ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=cou.GraGruX  ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
    'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 ',
    'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2 ',
    'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
    'WHERE Bastidao=-1 ',
    '']);
  end;
var
  Bastidao, GraGruX: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Reabre();
    if (QrIndef.RecordCount > 0) and Verifica then
    begin
      while not QrIndef.Eof do
      begin
        Bastidao := -1;
        case QrIndefGraGruY.Value of
          CO_GraGruY_0512_VSSubPrd: Bastidao := 0;
          CO_GraGruY_1024_VSNatCad: Bastidao := 1;
          CO_GraGruY_1365_VSProCal: Bastidao := 1;
          CO_GraGruY_1536_VSCouCal: Bastidao := -1;
          CO_GraGruY_1707_VSProCur: Bastidao := -1;
          CO_GraGruY_1877_VSCouCur: Bastidao := -1;
          CO_GraGruY_2048_VSRibCad: Bastidao := -1;
          CO_GraGruY_3072_VSRibCla: Bastidao := -1;
          CO_GraGruY_4096_VSRibOpe: Bastidao := 5;
          CO_GraGruY_5120_VSWetEnd: Bastidao := 5;
          CO_GraGruY_6144_VSFinCla: Bastidao := 5;
          else
            Geral.MB_Erro('Grupo de estoque n�o implementado! [4]');
        end;
        if Bastidao <> - 1 then
          AlteraAtual(Bastidao);
        QrIndef.Next;
      end;
      Reabre();
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSImpRendPWE.ReopenRend();
const
  Agrups: array[0..8] of String = ('Terceiro', 'NFeRem', 'DATA', 'GGX20', 'GGX22', 'Bastidao', 'MovimCod', 'Ctrl20', 'Ctrl23');
  Ordens: array[0..8] of String = ('NO_FORNECE', 'NFeRem', 'DATA', 'NO_PRD_TAM_COR_20', 'NO_PRD_TAM_COR_22', 'NO_Bastidao', 'MovimCod', 'Ctrl20', 'Ctrl23');
var
  SQL_Bastidoes, SQL_Terceiro, SQL_Periodo, SQL_OPs, GroupBy, Ordem,
  ATT_Bastidao, CouNiv2s, SQL_CouNiv2: String;
  I: Integer;
begin
  ATT_Bastidao := dmkPF.ArrayToTexto('cou0.Bastidao', 'NO_Bastidao', pvPos, True,
  sVSBastidao);
(* Bastidoes
1 N/D
2 Integral
3 Laminado
4 Dividido tripa
5 Dividido curtido
6 Rebaixado
7 Dividido semi
8 Rebaix. em semi
*)
  SQL_Bastidoes := '';
  if Geral.IntInConjunto(1, FBastidoes) then
    SQL_Bastidoes := SQL_Bastidoes + ',0';
  if Geral.IntInConjunto(2, FBastidoes) then
    SQL_Bastidoes := SQL_Bastidoes + ',1';
  if Geral.IntInConjunto(4, FBastidoes) then
    SQL_Bastidoes := SQL_Bastidoes + ',2';
  if Geral.IntInConjunto(8, FBastidoes) then
    SQL_Bastidoes := SQL_Bastidoes + ',3';
  if Geral.IntInConjunto(16, FBastidoes) then
    SQL_Bastidoes := SQL_Bastidoes + ',4';
  if Geral.IntInConjunto(32, FBastidoes) then
    SQL_Bastidoes := SQL_Bastidoes + ',5';
  if Geral.IntInConjunto(64, FBastidoes) then
    SQL_Bastidoes := SQL_Bastidoes + ',6';
  if Geral.IntInConjunto(128, FBastidoes) then
    SQL_Bastidoes := SQL_Bastidoes + ',7';
  if Geral.IntInConjunto(256, FBastidoes) then
    SQL_Bastidoes := SQL_Bastidoes + ',8';
  if FBastidoes > 511 then
  begin
    Geral.MB_Aviso('"Bastidao" n�o implementado em ReopenRend!');
  end;
  SQL_Terceiro  := '';
  SQL_Periodo   := '';
  if FFornece_Cod <> 0 then
    SQL_Terceiro := 'AND _23.Terceiro=' + Geral.FF0(FFornece_Cod);
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE cab.DtHrAberto ',
    FDataIni, FDataFim, FUsaDtIni, FUsaDtFim);
  SQL_Bastidoes := 'AND cou0.Bastidao IN (-999999999' + SQL_Bastidoes + ')';
  SQL_OPs := dmkPF.SQL_IntervInt('AND cab.Codigo ', FOPIni, FOPFim, FUsaOPIni, FUsaOPFim);
  //
  //
  //
  CouNiv2s := '';
  if (FDBG21CouNiv2 <> nil) and (FQr21CouNiv2 <> nil) then
  begin
    if (FDBG21CouNiv2.SelectedRows.Count > 0) and
    (FDBG21CouNiv2.SelectedRows.Count < FQr21CouNiv2.RecordCount) then
    begin
      for I := 0 to FDBG21CouNiv2.SelectedRows.Count - 1 do
      begin
        FQr21CouNiv2.GotoBookmark(pointer(FDBG21CouNiv2.SelectedRows.Items[I]));
        //
        if CouNiv2s <> '' then
          CouNiv2s := CouNiv2s + ',';
        CouNiv2s := CouNiv2s + Geral.FF0(FQr21CouNiv2.FieldByName('Codigo').AsInteger);
      end;
    end;
  end;
  if CouNiv2s <> '' then
    SQL_CouNiv2 := 'AND cou2.CouNiv2 IN (' + CouNiv2s + ') '
  else
    SQL_CouNiv2 := '';
  //
  //
  if FAgrupa > 0 then
    GroupBy := 'GROUP BY ' + Agrups[FOrdem1];
  if FAgrupa > 0 then
    GroupBy := GroupBy + ', ' + Agrups[FOrdem2];
  if FAgrupa > 1 then
    GroupBy := GroupBy + ', ' + Agrups[FOrdem3];
(*
  if FAgrupa > 2 then
    GroupBy := GroupBy + ', ' + Agrups[FOrdem4];
  if FAgrupa > 3 then
    GroupBy := GroupBy + ', ' + Agrups[FOrdem5];
*)
  //
  Ordem := 'ORDER BY ' +
    Ordens[FOrdem1] + ', ' +
    Ordens[FOrdem2] + ', ' +
    Ordens[FOrdem3] + ', ' +
    Ordens[FOrdem4];
(*
     + ', ' +
    Ordens[FOrdem5];
*)
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRend, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _vmi_emin_22_; ',
  'CREATE TABLE _vmi_emin_22_ ',
  'SELECT * ',
  'FROM ' + TMeuDB + '.vsmovits ',
  'WHERE MovimNiv=22; ',
  ' ',
  'DROP TABLE IF EXISTS _vmi_emin_23_; ',
  'CREATE TABLE _vmi_emin_23_ ',
  'SELECT * ',
  'FROM ' + TMeuDB + '.vsmovits ',
  'WHERE MovimNiv=23; ',
  ' ',
  'DROP TABLE IF EXISTS _vmi_emin_20_; ',
  'CREATE TABLE _vmi_emin_20_ ',
  'SELECT DISTINCT MovimCod, GraGruX, Controle ',
  'FROM ' + TMeuDB + '.vsmovits ',
  'WHERE MovimNiv=20 ',
  'GROUP BY MovimCod; ',
  ' ',
  ' ',
  'SELECT cou0.Bastidao, IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  '_20.GraGruX GGX20, _20.Controle Ctrl20, CONCAT(gg11.Nome, ',
  'IF(gti1.PrintTam=0, "", CONCAT(" ", gti1.Nome)), ',
  'IF(gcc1.PrintCor=0,"", CONCAT(" ", gcc1.Nome))) ',
  'NO_PRD_TAM_COR_22, ',
  ' ',
  'CONCAT(gg12.Nome, ',
  'IF(gti2.PrintTam=0, "", CONCAT(" ", gti2.Nome)), ',
  'IF(gcc2.PrintCor=0,"", CONCAT(" ", gcc2.Nome))) ',
  'NO_PRD_TAM_COR_20, ',
  ' ',
  'IF(SUM(_23.AreaM2) <> 0, ',
  '(SUM(_22.AreaM2)+SUM(_23.AreaM2))/-SUM(_23.AreaM2)*100, 0) Rendimento, ',
  'cab.NFeRem, cab.Codigo, cab.MovimCod, _22.Controle Ctrl22, ',
  '_22.MovimTwn, _22.Empresa, _22.Terceiro, _22.MovimID, _22.DataHora, ',
  '_22.Pallet, _22.GraGruX GGX22, SUM(_22.Pecas) Pecas_22, ',
  'SUM(_22.AreaM2) AreaM2_22, _23.Controle Ctrl23, _23.GraGruX GGX23, ',
  '-SUM(_23.Pecas) Pecas_23, -SUM(_23.AreaM2) AreaM2_23, ',
  ATT_Bastidao,
  'DATE(_22.DataHora) DATA ',
  ' ',
  'FROM _vmi_emin_22_ _22 ',
  'LEFT JOIN _vmi_emin_20_ _20 ON _20.MovimCod=_22.MovimCod ',
  'LEFT JOIN _vmi_emin_23_ _23 ON _23.MovimTwn=_22.MovimTwn ',
  'LEFT JOIN ' + TMeuDB + '.vspwecab  cab ON cab.MovimCod=_22.MovimCod ',
  'LEFT JOIN ' + TMeuDB + '.entidades frn ON frn.Codigo=_22.Terceiro ',
  '',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou0 ON cou0.GraGruX=_20.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou2 ON cou2.GraGruX=_22.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou3 ON cou3.GraGruX=_23.GraGruX',
  '',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx1 ON ggx1.Controle=_22.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc1 ON ggc1.Controle=ggx1.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc1 ON gcc1.Codigo=ggc1.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti1 ON gti1.Controle=ggx1.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg11 ON gg11.Nivel1=ggx1.GraGru1 ',
  ' ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx2 ON ggx2.Controle=_20.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc2 ON ggc2.Controle=ggx2.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc2 ON gcc2.Codigo=ggc2.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti2 ON gti2.Controle=ggx2.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg12 ON gg12.Nivel1=ggx2.GraGru1 ',
  SQL_Periodo,
  //
  SQL_Bastidoes,
  SQL_Terceiro,
  SQL_OPS,
  SQL_CouNiv2,
  '',
  GroupBy,
  Ordem,
  '']);
  //
  Geral.MB_SQL(Self, QrRend);
end;

end.
