u n i t   S P E D _ E F D _ K 2 X X _ ;  
  
 i n t e r f a c e  
  
 u s e s  
     W i n d o w s ,   M e s s a g e s ,   S y s U t i l s ,   C l a s s e s ,   G r a p h i c s ,   C o n t r o l s ,   F o r m s ,   D i a l o g s ,   D B ,  
     E x t C t r l s ,   S t d C t r l s ,   B u t t o n s ,   G r i d s ,   D B G r i d s ,   d m k G e r a l ,   d m k L a b e l ,   d m k I m a g e ,  
     D B C t r l s ,   C o m C t r l s ,   U n I n t e r n a l C o n s t s ,   U n D m k E n u m s ,   d m k C h e c k G r o u p ,   m y S Q L D b T a b l e s ,  
     d m k D B L o o k u p C o m b o B o x ,   d m k E d i t ,   d m k E d i t C B ,   V a r i a n t s ,   d m k E d i t D a t e T i m e P i c k e r ,  
     U n P r o j G r o u p _ C o n s t s ,   d m k D B G r i d Z T O ,   S P E D _ L i s t a s ;  
  
 t y p e  
     T T i p o R e g S P E D P r o d   =   ( t r s p 2 3 X = 1 ,   t r s p 2 5 X = 2 ) ;  
     T F m S P E D _ E F D _ K 2 X X   =   c l a s s ( T F o r m )  
         P n C a b e c a :   T P a n e l ;  
         G B _ R :   T G r o u p B o x ;  
         I m g T i p o :   T d m k I m a g e ;  
         G B _ L :   T G r o u p B o x ;  
         G B _ M :   T G r o u p B o x ;  
         L a T i t u l o 1 A :   T L a b e l ;  
         L a T i t u l o 1 B :   T L a b e l ;  
         P a n e l 2 :   T P a n e l ;  
         P a n e l 3 :   T P a n e l ;  
         G r o u p B o x 1 :   T G r o u p B o x ;  
         G B A v i s o s 1 :   T G r o u p B o x ;  
         P a n e l 4 :   T P a n e l ;  
         L a A v i s o 1 :   T L a b e l ;  
         L a A v i s o 2 :   T L a b e l ;  
         G B R o d a P e :   T G r o u p B o x ;  
         P n S a i D e s i s :   T P a n e l ;  
         B t S a i d a :   T B i t B t n ;  
         P a n e l 1 :   T P a n e l ;  
         B t O K :   T B i t B t n ;  
         L a T i t u l o 1 C :   T L a b e l ;  
         P n P e r i o d o :   T P a n e l ;  
         L a M e s :   T L a b e l ;  
         C B M e s :   T C o m b o B o x ;  
         C B A n o :   T C o m b o B o x ;  
         L a A n o :   T L a b e l ;  
         P C R e g i s t r o :   T P a g e C o n t r o l ;  
         T a b S h e e t 1 :   T T a b S h e e t ;  
         T a b S h e e t 2 :   T T a b S h e e t ;  
         T a b S h e e t 3 :   T T a b S h e e t ;  
         L a D a t a :   T L a b e l ;  
         T P D a t a :   T d m k E d i t D a t e T i m e P i c k e r ;  
         P B 1 :   T P r o g r e s s B a r ;  
         M e A v i s o :   T M e m o ;  
         Q r I D N i v :   T m y S Q L Q u e r y ;  
         Q r I D N i v M o v i m I D :   T I n t e g e r F i e l d ;  
         Q r I D N i v M o v i m N i v :   T I n t e g e r F i e l d ;  
         Q r P r o d P a r c :   T m y S Q L Q u e r y ;  
         Q r P r o d P a r c P e c a s :   T F l o a t F i e l d ;  
         Q r P r o d P a r c A r e a M 2 :   T F l o a t F i e l d ;  
         Q r X 9 9 9 :   T m y S Q L Q u e r y ;  
         Q r X 9 9 9 L i n A r q :   T I n t e g e r F i e l d ;  
         P B 2 :   T P r o g r e s s B a r ;  
         Q r R e c l M u l :   T m y S Q L Q u e r y ;  
         Q r R e c l M u l D a t a :   T D a t e F i e l d ;  
         Q r R e c l M u l G G X _ D s t :   T I n t e g e r F i e l d ;  
         Q r R e c l M u l G G X _ S r c :   T I n t e g e r F i e l d ;  
         Q r R e c l M u l P e c a s :   T F l o a t F i e l d ;  
         Q r R e c l M u l A r e a M 2 :   T F l o a t F i e l d ;  
         Q r R e c l M u l P e s o K g :   T F l o a t F i e l d ;  
         P a g e C o n t r o l 1 :   T P a g e C o n t r o l ;  
         T a b S h e e t 4 :   T T a b S h e e t ;  
         T a b S h e e t 5 :   T T a b S h e e t ;  
         T a b S h e e t 6 :   T T a b S h e e t ;  
         d m k D B G r i d Z T O 1 :   T d m k D B G r i d Z T O ;  
         d m k D B G r i d Z T O 2 :   T d m k D B G r i d Z T O ;  
         d m k D B G r i d Z T O 3 :   T d m k D B G r i d Z T O ;  
         Q r C l a s M u l :   T m y S Q L Q u e r y ;  
         Q r C l a s M u l D a t a :   T D a t e F i e l d ;  
         Q r C l a s M u l G G X _ D s t :   T I n t e g e r F i e l d ;  
         Q r C l a s M u l G G X _ S r c :   T I n t e g e r F i e l d ;  
         Q r C l a s M u l P e c a s :   T F l o a t F i e l d ;  
         Q r C l a s M u l A r e a M 2 :   T F l o a t F i e l d ;  
         Q r C l a s M u l P e s o K g :   T F l o a t F i e l d ;  
         D s C a c I t s :   T D a t a S o u r c e ;  
         D s R e c l M u l :   T D a t a S o u r c e ;  
         D s C l a s M u l :   T D a t a S o u r c e ;  
         Q r V S C a l C a b :   T m y S Q L Q u e r y ;  
         D s V S C a l C a b :   T D a t a S o u r c e ;  
         Q r V S C u r C a b :   T m y S Q L Q u e r y ;  
         D s V S C u r C a b :   T D a t a S o u r c e ;  
         Q r V S O p e C a b :   T m y S Q L Q u e r y ;  
         D s V S O p e C a b :   T D a t a S o u r c e ;  
         Q r S e c O _ P :   T m y S Q L Q u e r y ;  
         Q r S e c O _ P D a t a :   T D a t e F i e l d ;  
         Q r S e c O _ P G G X _ D s t :   T I n t e g e r F i e l d ;  
         Q r S e c O _ P G G X _ S r c :   T I n t e g e r F i e l d ;  
         Q r S e c O _ P P e c a s :   T F l o a t F i e l d ;  
         Q r S e c O _ P A r e a M 2 :   T F l o a t F i e l d ;  
         Q r S e c O _ P P e s o K g :   T F l o a t F i e l d ;  
         Q r S e c O _ P G r a n d e z a :   T S m a l l i n t F i e l d ;  
         Q r S e c O _ P M o v i m I D :   T I n t e g e r F i e l d ;  
         T a b S h e e t 7 :   T T a b S h e e t ;  
         D s S e c O _ P :   T D a t a S o u r c e ;  
         d m k D B G r i d Z T O 5 :   T d m k D B G r i d Z T O ;  
         P a g e C o n t r o l 2 :   T P a g e C o n t r o l ;  
         T a b S h e e t 8 :   T T a b S h e e t ;  
         T a b S h e e t 9 :   T T a b S h e e t ;  
         T a b S h e e t 1 0 :   T T a b S h e e t ;  
         T a b S h e e t 1 1 :   T T a b S h e e t ;  
         d m k D B G r i d Z T O 4 :   T d m k D B G r i d Z T O ;  
         d m k D B G r i d Z T O 6 :   T d m k D B G r i d Z T O ;  
         d m k D B G r i d Z T O 7 :   T d m k D B G r i d Z T O ;  
         d m k D B G r i d Z T O 8 :   T d m k D B G r i d Z T O ;  
         Q r V S P W E C a b :   T m y S Q L Q u e r y ;  
         D s V S P W E C a b :   T D a t a S o u r c e ;  
         Q r C a c I t s :   T m y S Q L Q u e r y ;  
         Q r C a c I t s D a t a :   T D a t e F i e l d ;  
         Q r C a c I t s G G X _ S r c :   T I n t e g e r F i e l d ;  
         Q r C a c I t s G G X _ D s t :   T I n t e g e r F i e l d ;  
         Q r C a c I t s P e c a s :   T F l o a t F i e l d ;  
         Q r C a c I t s A r e a M 2 :   T F l o a t F i e l d ;  
         Q r C a c I t s A r e a P 2 :   T F l o a t F i e l d ;  
         Q r P r o d P a r c P e s o K g :   T F l o a t F i e l d ;  
         Q r O r i :   T m y S Q L Q u e r y ;  
         Q r C a l T o C u r :   T m y S Q L Q u e r y ;  
         Q r C a l T o C u r D s t G G X :   T I n t e g e r F i e l d ;  
         C G I m p o r t a r :   T d m k C h e c k G r o u p ;  
         T a b S h e e t 1 2 :   T T a b S h e e t ;  
         Q r V m i G A r C a b :   T m y S Q L Q u e r y ;  
         D s V m i G A r C a b :   T D a t a S o u r c e ;  
         d m k D B G r i d Z T O 9 :   T d m k D B G r i d Z T O ;  
         Q r V m i G A r C a b D a t a :   T D a t e F i e l d ;  
         Q r V m i G A r C a b M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b G r a G r u X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b P e c a s :   T F l o a t F i e l d ;  
         Q r V m i G A r C a b A r e a M 2 :   T F l o a t F i e l d ;  
         Q r V m i G A r C a b P e s o K g :   T F l o a t F i e l d ;  
         Q r V m i G A r C a b M o v i m T w n :   T I n t e g e r F i e l d ;  
         Q r C u r T o G A r :   T m y S Q L Q u e r y ;  
         Q r C u r T o G A r D s t G G X :   T I n t e g e r F i e l d ;  
         Q r C u r T o G A r S r c M o v I D :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b C o d i g o :   T I n t e g e r F i e l d ;  
         Q r C u r T o G A r G r a G r u X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b U n i d M e d :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b m e d _ g r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b x c o _ g r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b G r a G r u Y :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b C o u N i v 2 :   T I n t e g e r F i e l d ;  
         Q r V m i G A r C a b C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s :   T m y S Q L Q u e r y ;  
         Q r V m i G A r I t s D a t a :   T D a t e F i e l d ;  
         Q r V m i G A r I t s C o d i g o :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s G r a G r u X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s P e c a s :   T F l o a t F i e l d ;  
         Q r V m i G A r I t s A r e a M 2 :   T F l o a t F i e l d ;  
         Q r V m i G A r I t s P e s o K g :   T F l o a t F i e l d ;  
         Q r V m i G A r I t s M o v i m T w n :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s U n i d M e d :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s m e d _ g r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s x c o _ g r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s G r a G r u Y :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s C o u N i v 2 :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s T i p o E s t q :   T L a r g e i n t F i e l d ;  
         Q r V m i G A r I t s C o n t r o l e :   T I n t e g e r F i e l d ;  
         D s V m i G A r I t s :   T D a t a S o u r c e ;  
         Q r V m i G A r C a b T i p o E s t q :   T F l o a t F i e l d ;  
         Q r V m i G A r I t s D s t G G X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s S r c G G X :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s S r c N i v e l 2 :   T I n t e g e r F i e l d ;  
         Q r V m i G A r I t s S r c M o v I D :   T I n t e g e r F i e l d ;  
         Q r S u m G A R :   T m y S Q L Q u e r y ;  
         Q r S u m G A R P e c a s :   T F l o a t F i e l d ;  
         Q r S u m G A R A r e a M 2 :   T F l o a t F i e l d ;  
         Q r S u m G A R P e s o K g :   T F l o a t F i e l d ;  
         Q r C a c I t s C o d i g o :   T I n t e g e r F i e l d ;  
         Q r S e c O _ P C o d i g o :   T I n t e g e r F i e l d ;  
         Q r S e c O _ P M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r C a c I t s V M I _ D e s t :   T I n t e g e r F i e l d ;  
         Q r C a c I t s C a c I D :   T I n t e g e r F i e l d ;  
         Q r S e c O _ P C o n t r o l e :   T I n t e g e r F i e l d ;  
         T a b S h e e t 1 3 :   T T a b S h e e t ;  
         Q r G G X R c l :   T m y S Q L Q u e r y ;  
         D s G G X R c l :   T D a t a S o u r c e ;  
         d m k D B G r i d Z T O 1 0 :   T d m k D B G r i d Z T O ;  
         Q r G G X R c l G G X _ S r c :   T I n t e g e r F i e l d ;  
         Q r G G X R c l G G X _ D s t :   T I n t e g e r F i e l d ;  
         Q r G G X R c l G r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r G G X R c l P e c a s :   T F l o a t F i e l d ;  
         Q r G G X R c l A r e a M 2 :   T F l o a t F i e l d ;  
         Q r G G X R c l P e s o K g :   T F l o a t F i e l d ;  
         Q r G G X R c l D a t a :   T D a t e F i e l d ;  
         Q r G G X R c l M o v i m I D :   T I n t e g e r F i e l d ;  
         Q r G G X R c l C o d i g o :   T I n t e g e r F i e l d ;  
         Q r G G X R c l M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r G G X R c l C o n t r o l e :   T I n t e g e r F i e l d ;  
         p r o c e d u r e   B t S a i d a C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m A c t i v a t e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m R e s i z e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m C r e a t e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   B t O K C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   P C R e g i s t r o C h a n g i n g ( S e n d e r :   T O b j e c t ;   v a r   A l l o w C h a n g e :   B o o l e a n ) ;  
     p r i v a t e  
         {   P r i v a t e   d e c l a r a t i o n s   }  
         / / f u n c t i o n     E r r G r a n d e z a C a c ( ) :   B o o l e a n ;  
         f u n c t i o n     E r r G r a n d e z a ( T a b S r c ,   G G x S r c ,   G G X D s t :   S t r i n g ) :   B o o l e a n ;  
         f u n c t i o n     I m p o r t a C l a s s e E R e c l a s s e C a C ( S Q L _ P e r i o d o :   S t r i n g ) :   B o o l e a n ;  
         f u n c t i o n     I m p o r t a C l a s s e E R e c l a s s e M u l ( Q r y :   T M y S Q L Q u e r y ;   M o v i m I D :   T E s t q M o v i m I D ;  
                             S Q L _ P e r i o d o :   S t r i n g ) :   B o o l e a n ;  
         f u n c t i o n     I m p o r t a C l a s s e O p e P r o c ( S Q L _ P e r i o d o :   S t r i n g ) :   B o o l e a n ;  
         f u n c t i o n     I m p o r t a C l a s s e G G X R c l ( S Q L _ P e r i o d o :   S t r i n g ) :   B o o l e a n ;  
         f u n c t i o n     I m p o r t a G e r A r t ( S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e :   S t r i n g ) :   B o o l e a n ;  
         f u n c t i o n     I m p o r t a O p e P r o c ( Q r C a b :   T M y S Q L Q u e r y ;   M o v i m I D :   T E s t q M o v i m I D ;  
                             S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e :   S t r i n g ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e I t e m A t u a l _ K 2 2 0 ( M o v i m I D :   I n t e g e r ;   D a t a :   T D a t e T i m e ;   G G X _ S r c ,  
                             G G X _ D s t :   I n t e g e r ;   A r e a M 2 :   D o u b l e ;   C o d i g o ,   M o v i m C o d ,  
                             C t r l D s t ,   ( * C t r l O r i , * )   G G X D s t ,   G G X O r i :   I n t e g e r ;   E S O M I E M :  
                             T E s t q S P E D 2 2 0 O M I E M ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e I t e m A t u a l _ K 2 3 0 ( c o n s t   M o v i m I D ,   C o d i g o ,   M o v i m C o d :   I n t e g e r ;  
                             c o n s t   D a t a I n i ,   D a t a F i m :   T D a t e T i m e ;   c o n s t   G r a G r u X :   I n t e g e r ;   c o n s t  
                             Q t d e :   D o u b l e ;   v a r   L i n A r q P a i :   I n t e g e r ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e I t e m A t u a l _ K 2 5 0 ( c o n s t   M o v i m I D ,   C o d i g o ,   M o v i m C o d :   I n t e g e r ;  
                             c o n s t   D a t a :   T D a t e T i m e ;   c o n s t   G r a G r u X :   I n t e g e r ;   c o n s t   Q t d e :   D o u b l e ;  
                             v a r   L i n A r q P a i :   I n t e g e r ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e I t e m A t u a l _ K 2 3 5 ( L i n A r q P a i :   I n t e g e r ;   D a t a :   T D a t e T i m e ;   G r a G r u X :  
                             I n t e g e r ;   Q t d e :   D o u b l e ;   C O D _ I N S _ S U B S T :   S t r i n g ;   I D _ I t e m ,   M o v i m I D ,  
                             C o d i g o ,   M o v i m C o d ,   C o n t r o l e :   I n t e g e r ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e I t e m A t u a l _ K 2 5 5 ( L i n A r q P a i :   I n t e g e r ;   D a t a :   T D a t e T i m e ;   G r a G r u X :  
                             I n t e g e r ;   Q t d e :   D o u b l e ;   C O D _ I N S _ S U B S T :   S t r i n g ;   I D _ I t e m ,   M o v i m I D ,  
                             C o d i g o ,   M o v i m C o d ,   C o n t r o l e :   I n t e g e r ) :   B o o l e a n ;  
         p r o c e d u r e   I n s e r e _ E m O p e P r o c ( c o n s t   T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;   c o n s t  
                             M o v i m I D :   T E s t q M o v i m I D ;   c o n s t   Q r y :   T m y S Q L Q u e r y ;   c o n s t   F a t o r :  
                             D o u b l e ;   c o n s t   S Q L _ P e r i o d o :   S t r i n g ;   v a r   L i n A r q P a i :   I n t e g e r ) ;  
         f u n c t i o n     I n s e r e _ O r i g e P r o c ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ;   M o v i m I D :  
                             T E s t q M o v i m I D ;   M o v i m N i v :   T E s t q M o v i m N i v ;   Q r C a b :   T m y S Q L Q u e r y ;  
                             L i n A r q P a i :   I n t e g e r ;   S Q L _ P e r i o d o :   S t r i n g ) :   B o o l e a n ;  
         p r o c e d u r e   V e r i f i c a G G X D s t Z e r a d o s ( ) ;  
  
     p u b l i c  
         {   P u b l i c   d e c l a r a t i o n s   }  
         F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,   F R e g P a i :   I n t e g e r ;  
         F D i a I n i ,   F D i a F i m ,   F D i a P o s ,   F D a t a :   T D a t e T i m e ;  
     e n d ;  
  
     v a r  
     F m S P E D _ E F D _ K 2 X X :   T F m S P E D _ E F D _ K 2 X X ;  
  
 i m p l e m e n t a t i o n  
  
 u s e s   U n M y O b j e c t s ,   M o d u l e ,   U n D m k P r o c F u n c ,   D m k D A C _ P F ,   U M y S Q L M o d u l e ,   U n F i n a n c e i r o ,  
     M o d u l e F i n ,   M o d u l e G e r a l ,   U n V S _ P F ,   M o d P r o d ,   A p p L i s t a s ,   M o d A p p G r a G 1 ;  
  
 { $ R   * . D F M }  
  
 p r o c e d u r e   T F m S P E D _ E F D _ K 2 X X . B t O K C l i c k ( S e n d e r :   T O b j e c t ) ;  
 ( *  
 c o n s t  
     D B G 0 0 G r a G r u Y   =   n i l ;  
     D B G 0 0 G r a G r u X   =   n i l ;  
     D B G 0 0 C o u N i v 2   =   n i l ;  
     Q r 0 0 G r a G r u Y     =   n i l ;  
     Q r 0 0 G r a G r u X     =   n i l ;  
     Q r 0 0 C o u N i v 2     =   n i l ;  
     D a t a C o m p r a       =   F a l s e ;  
     E m P r o c e s s o B H   =   T r u e ;  
 v a r  
     D a t a B a l :   T D a t e T i m e ;  
     B a l _ T X T :   S t r i n g ;  
     N o U n i d :   I n t e g e r ;  
     / /  
     U n i d M e d ,   N i v e l 1 ,   B a l I D ,   B a l N u m ,   B a l I t m ,   B a l E n t :   I n t e g e r ;  
     C O D _ I T E M ,   U N I D ,   I N D _ P R O P ,   C O D _ P A R T ,   T X T _ C O M P L ,   C O D _ C T A :   S t r i n g ;  
     Q T D ,   V L _ U N I T ,   V L _ I T E M ,   V L _ I T E M _ I R ,   P r e c o :   D o u b l e ;  
     E n t i d a d e ,   F i l i a l ,   T e r c e i r o ,   O r d e m 1 ,   O r d e m 2 ,   O r d e m 3 ,   A g r u p a ,   S t q C e n C a d ,  
     Z e r o N e g a t :   I n t e g e r ;  
     T a b l e S r c ,   N O _ E m p r e s a ,   N O _ S t q C e n C a d ,   N O _ T e r c e i r o ,   S Q L _ P S Q :   S t r i n g ;  
     E s t o q u e E m ,   D a t a R e l a t i v a :   T D a t e T i m e ;  
     M o s t r a F r x :   B o o l e a n ;  
     / /  
     C o r d a :   S t r i n g ;  
     F a t o r ,   M y Q t d ,   M y V l U ,   M y B a s :   D o u b l e ;  
     T i p o :   T G r a n d e z a U n i d M e d ;  
     / /  
     D T _ E S T ,   I N D _ E S T :   S t r i n g ;  
 * )  
 v a r  
     E r r o :   B o o l e a n ;  
     / /  
     U n i d M e d P c ,   U n i d M e d K g ,   U n i d M e d M 2 ,   C o n t i n u a :   I n t e g e r ;  
     S Q L _ P e r i o d o ,   S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ D t H r F i m O p e :   S t r i n g ;  
 b e g i n  
     i f   n o t   V S _ P F . V e r i f i c a C a l C u r D s t G G X ( )   t h e n  
         E x i t ;  
     i f   n o t   V S _ P F . V e r i f i c a C l i F o r E m p t y ( )   t h e n  
         E x i t ;  
     i f   n o t   V S _ P F . V e r i f i c a D a t a s D e E n c e r r a d a s A p o s A b e r t u r a ( )   t h e n  
         E x i t ;  
  
     F D i a I n i   : =   E n c o d e D a t e ( G e r a l . I M V ( C B A n o . T e x t ) ,   C B M e s . I t e m I n d e x   +   1 ,   1 ) ;  
     F D i a F i m   : =   I n c M o n t h ( F D i a I n i ,   1 )   - 1 ;  
     F D i a P o s   : =   F D i a F i m   +   1 ;  
     / /  
     E r r o   : =   F a l s e ;  
     M e A v i s o . L i n e s . C l e a r ;  
     i f   M y O b j e c t s . F I C ( C G I m p o r t a r . V a l u e   =   0 ,   C G I m p o r t a r ,  
         ' S e l e c i o n e   p e l o   m e n o s   u m   t i p o   d e   i t e m   a   s e r   i m p o r t a d o ! ' )   t h e n   E x i t ;  
     i f   n o t   D m P r o d . O b t e m C o d i g o P e c a P c ( U n i d M e d P c )   t h e n   E x i t ;  
     i f   n o t   D m P r o d . O b t e m C o d i g o P e s o K g ( U n i d M e d K g )   t h e n   E x i t ;  
     i f   n o t   D m P r o d . O b t e m C o d i g o A r e a M 2 ( U n i d M e d M 2 )   t h e n   E x i t ;  
     i f   ( U n i d M e d K g   =   0 )   o r   ( U n i d M e d M 2   =   0 )   t h e n   E x i t ;  
     P B 1 . P o s i t i o n   : =   0 ;  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   F a l s e ,   ' . . . ' ) ;  
     i f   P C R e g i s t r o . A c t i v e P a g e I n d e x   >   0   t h e n  
     b e g i n  
         M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' V e r i f i c a n d o   n o v o s   I D s   i m p l e m e n t a d o s . ' ) ;  
         / /  
         S Q L _ P e r i o d o   : =   d m k P F . S Q L _ P e r i o d o ( ' W H E R E   D a t a H o r a   ' ,   F D i a I n i ,   F D i a F i m ,   T r u e ,   T r u e ) ;  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r I D N i v ,   D m o d . M y D B ,   [  
         ' S E L E C T   M o v i m I D ,   M o v i m N i v ,   C O U N T ( C o n t r o l e )   I M E I s     ' ,  
         ' F R O M   '   +   C O _ T A B _ V M I   +   '   ' ,  
         S Q L _ P e r i o d o ,  
         ' A N D   E m p r e s a = '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         ' G R O U P   B Y   M o v i m I D ,   M o v i m N i v   ' ,  
         ' O R D E R   B Y   M o v i m I D ,   M o v i m N i v   ' ,  
         ' ' ] ) ;  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q r I D N i v ) ;  
         / /  
         Q r I D N i v . F i r s t ;  
         w h i l e   n o t   Q r I D N i v . E o f   d o  
         b e g i n  
             c a s e   T E s t q M o v i m I D ( Q r I D N i v M o v i m I D . V a l u e )   o f  
                 ( * 0 0 * ) e m i d A j u s t e ,  
                 ( * 0 1 * ) e m i d C o m p r a ,  
                 ( * 0 2 * ) e m i d V e n d a ,  
                 / / ( * 0 3 * ) e m i d R e c l a s W E ,  
                 / / ( * 0 4 * ) e m i d B a i x a = 4 ,  
                 / / ( * 0 5 * ) e m i d I n d s W E = 5 ,  
                 ( * 0 9 * ) e m i d F o r c a d o ,  
                 / / ( * 1 0 * ) e m i d S e m O r i g e m = 1 0 ,  
                 ( * 1 2 * ) e m i d R e s i d u o R e c l a s ,  
                 ( * 1 3 * ) e m i d I n v e n t a r i o ,  
                 ( * 1 6 * ) e m i d E n t r a d a P l C ,  
                 ( * 1 7 * ) e m i d E x t r a B x a ,  
                 / / ( * 1 8 * ) e m i d S a l d o A n t e r i o r = 1 8 ,  
                 ( * 2 1 * ) e m i d D e v o l u c a o ,  
                 ( * 2 2 * ) e m i d R e t r a b a l h o ,  
                 ( * 2 3 * ) e m i d G e r a S u b P r o d ,  
                 ( * 2 5 * ) e m i d T r a n s f L o c :   C o n t i n u a   : =   Q r I D N i v M o v i m I D . V a l u e ;  
                 / /  
                 ( * 0 6 * ) e m i d I n d s V S ,  
                 ( * 0 7 * ) e m i d C l a s s A r t V S U n i ,  
                 ( * 0 8 * ) e m i d R e c l a s V S U n i ,  
                 ( * 1 1 * ) e m i d E m O p e r a c a o ,  
                 ( * 1 4 * ) e m i d C l a s s A r t V S M u l ,  
                 ( * 1 5 * ) e m i d P r e R e c l a s s e ,  
                 ( * 1 9 * ) e m i d E m P r o c W E ,  
                 ( * 2 0 * ) e m i d F i n i s h e d ,  
                 ( * 2 4 * ) e m i d R e c l a s V S M u l ,  
                 ( * 2 6 * ) e m i d E m P r o c C a l ,  
                 ( * 2 7 * ) e m i d E m P r o c C u r ,  
                 ( * 2 8 * ) e m i d D e s c l a s s e :   C o n t i n u a   : =   Q r I D N i v M o v i m I D . V a l u e ;  
                 e l s e   C o n t i n u a   : =   - 1 ;  
             e n d ;  
             i f   C o n t i n u a   <   0   t h e n  
             b e g i n  
                 G e r a l . M B _ E r r o ( ' I D   d e   m o v i m e n t o   n � o   i m p l e m e n t a d o :   '   +   s l i n e B r e a k   +  
                     G e r a l . F F 0 ( Q r I D N i v M o v i m I D . V a l u e )   +   '   -   '   +  
                     s E s t q M o v i m I D [ Q r I D N i v M o v i m I D . V a l u e ] ) ;  
                 E x i t ;  
             e n d ;  
             / /  
             Q r I D N i v . N e x t ;  
         e n d ;  
         / / Q r I D N i v . F i r s t ;  
         M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' I m p o r t a n d o . ' ) ;  
 / / / / / / / /     I T E N S   E X C L U S O S       / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         / / 0 0   -   e m i d A j u s t e ,  
         / / 0 1   -   e m i d C o m p r a ,  
         / / 0 2   -   e m i d V e n d a ,  
         / / / / 0 3   -   e m i d R e c l a s W E ,  
         / / / / 0 4   -   e m i d B a i x a = 4 ,  
         / / / / 0 5   -   e m i d I n d s W E = 5 ,  
         / / 0 9   -   e m i d F o r c a d o ,  
         / / / / 1 0   -   e m i d S e m O r i g e m = 1 0 ,  
         / / 1 1   -   e m i d E m O p e r a c a o ,  
         / / / / 1 2   -   e m i d R e s i d u o R e c l a s = 1 2 ,  
         / / 1 3   -   e m i d I n v e n t a r i o ,  
         / / 1 5   -   e m i d P r e R e c l a s s e ,  
         / / 1 6   -   e m i d E n t r a d a P l C ,  
         / / 1 7   -   e m i d E x t r a B x a ,  
         / / / / 1 8   -   e m i d S a l d o A n t e r i o r = 1 8 ,  
         / / 1 9   -   e m i d E m P r o c W E ,  
         / / 2 0   -   e m i d F i n i s h e d ,  
         / / 2 1   -   e m i d D e v o l u c a o ,  
         / / 2 2   -   e m i d R e t r a b a l h o ,  
         / / 2 3   -   e m i d G e r a S u b P r o d ,  
         / / 2 5   -   e m i d T r a n s f L o c :   ;   / /   N a d a !   N � o   v � o   n e s t e   r e g i s t r o !  
         / / 2 6   -   e m i d E m P r o c C a l ,  
         / / 2 7   -   e m i d E m P r o c C u r :   C o n t i n u a   : =   Q r I D N i v M o v i m I D . V a l u e ;  
         / /  
         / / 0 6   -   e m i d I n d s V S :   ;   / /   R e g i s t r o s   2 3 0   a   2 5 5 !  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
 / / /   / / / /     C L A S S E   E   R E C L A S S E     / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         / / 0 7 * ) e m i d C l a s s A r t V S U n i ,  
         / / 0 8 * ) e m i d R e c l a s V S U n i  
         c a s e   P C R e g i s t r o . A c t i v e P a g e I n d e x   o f  
             1 :  
             b e g i n  
                 I m p o r t a C l a s s e E R e c l a s s e C a C ( S Q L _ P e r i o d o ) ;  
                 I m p o r t a C l a s s e O p e P r o c ( S Q L _ P e r i o d o ) ;  
                 I m p o r t a C l a s s e G G X R c l ( S Q L _ P e r i o d o ) ;  
     ( *  
                 N � o   f a z e r ! ! !   j �   v a i   n o   I m p o r t a C l a s s e E R e c l a s s e C a C ( . . .   ! ! !  
                 / / 1 4   e m i d C l a s s A r t V S M u l ,  
                 I m p o r t a C l a s s e E R e c l a s s e M u l ( Q r C l a s M u l ,   T E s t q M o v i m I D . e m i d C l a s s A r t V S M u l ,   S Q L _ P e r i o d o ) ;  
                 / /   2 4   e m i d R e c l a s V S M u l ,  
                 I m p o r t a C l a s s e E R e c l a s s e M u l ( Q r R e c l M u l ,   T E s t q M o v i m I D . e m i d R e c l a s V S M u l ,   S Q L _ P e r i o d o ) ;  
     * )  
  
             e n d ;  
             2 :  
             b e g i n  
                 / /  
                 S Q L _ P e r i o d o C o m p l e x o   : =   D m k P F . S Q L _ P e r i o d o C o m p l e x o ( ' W H E R E   ( ' ,  
                     ' D t H r A b e r t o ' ,   ' D t H r F i m O p e ' ,   F D i a I n i ,   F D i a F i m ,   T r u e ,   t r u e ,   T r u e ,   T r u e ) ;  
                 S Q L _ P e r i o d o   : =   d m k P F . S Q L _ P e r i o d o ( ' A N D   D a t a H o r a   ' ,   F D i a I n i ,   F D i a F i m ,   T r u e ,   T r u e ) ;  
                 S Q L _ D t H r F i m O p e   : =   d m k P F . S Q L _ P e r i o d o ( ' W H E R E   D t H r F i m O p e   ' ,   F D i a I n i ,   F D i a F i m ,   T r u e ,   T r u e ) ;  
                 / /  
                 i f   D m k P F . I n t I n C o n j u n t o 2 ( 1 ,   C G I m p o r t a r . V a l u e )   t h e n  
                     / / i m p o r t a r   c o u r o s   e c o n s u m i d o s   e   P Q   c o n s u m i d o s !  
                     I m p o r t a O p e P r o c ( Q r V S C a l C a b ,   T E s t q M o v i m I D . e m i d E m P r o c C a l ,  
                         S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e ) ;  
                 i f   D m k P F . I n t I n C o n j u n t o 2 ( 2 ,   C G I m p o r t a r . V a l u e )   t h e n  
                     I m p o r t a O p e P r o c ( Q r V S C u r C a b ,   T E s t q M o v i m I D . e m i d E m P r o c C u r ,  
                         S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e ) ;  
                 i f   D m k P F . I n t I n C o n j u n t o 2 ( 4 ,   C G I m p o r t a r . V a l u e )   t h e n  
                     I m p o r t a G e r A r t ( S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e ) ;  
                 i f   D m k P F . I n t I n C o n j u n t o 2 ( 8 ,   C G I m p o r t a r . V a l u e )   t h e n  
                     I m p o r t a O p e P r o c ( Q r V S O p e C a b ,   T E s t q M o v i m I D . e m i d E m O p e r a c a o ,  
                         S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e ) ;  
                 i f   D m k P F . I n t I n C o n j u n t o 2 ( 1 6 ,   C G I m p o r t a r . V a l u e )   t h e n  
                     I m p o r t a O p e P r o c ( Q r V S P W E C a b ,   T E s t q M o v i m I D . e m i d E m P r o c W E ,  
                         S Q L _ P e r i o d o C o m p l e x o ,   S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e ) ;  
                 / / e m i d E m P r o c C a l :  
                 / / e m i d E m P r o c C u r  
                 ( *  
                 e m i d A j u s t e = 0 ,   e m i d C o m p r a = 1 ,   e m i d V e n d a = 2 ,  
                               e m i d R e c l a s W E = 3 ,   e m i d B a i x a = 4 ,   e m i d I n d s W E = 5 ,   e m i d I n d s V S = 6 ,  
                               e m i d C l a s s A r t V S U n i = 7 ,   e m i d R e c l a s V S U n i = 8 ,   e m i d F o r c a d o = 9 ,  
                               e m i d S e m O r i g e m = 1 0 ,   e m i d E m O p e r a c a o = 1 1 ,   e m i d R e s i d u o R e c l a s = 1 2 ,  
                               e m i d I n v e n t a r i o = 1 3 ,   e m i d C l a s s A r t V S M u l = 1 4 ,   e m i d P r e R e c l a s s e = 1 5 ,  
                               e m i d E n t r a d a P l C = 1 6 ,   e m i d E x t r a B x a = 1 7 ,   e m i d S a l d o A n t e r i o r = 1 8 ,  
                               e m i d E m P r o c W E = 1 9 ,   e m i d F i n i s h e d = 2 0 ,   e m i d D e v o l u c a o = 2 1 ,  
                               e m i d R e t r a b a l h o = 2 2 ,   e m i d G e r a S u b P r o d = 2 3 ,   e m i d R e c l a s V S M u l = 2 4 ,  
                               e m i d T r a n s f L o c = 2 5 ,   e m i d E m P r o c C a l = 2 6 ,   e m i d E m P r o c C u r = 2 7 ) ;  
                 * )  
  
             e n d ;  
         e n d ;  
         M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   F a l s e ,   ' . . . ' ) ;  
     e n d   e l s e  
         G e r a l . M B _ E r r o ( ' G u i a   n � o   i m p l e m e n t a d a   p a r a   V S   C o u r o ! ' ) ;  
     / /  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   F a l s e ,   ' I m p o r t a � � o   f i n a l i z a d a ! ! ! ' ) ;  
     C l o s e ;  
 e n d ;  
  
 p r o c e d u r e   T F m S P E D _ E F D _ K 2 X X . B t S a i d a C l i c k ( S e n d e r :   T O b j e c t ) ;  
 b e g i n  
     C l o s e ;  
 e n d ;  
  
 f u n c t i o n   T F m S P E D _ E F D _ K 2 X X . E r r G r a n d e z a ( T a b S r c ,   G G x S r c ,   G G X D s t :   S t r i n g ) :   B o o l e a n ;  
 b e g i n  
     / / U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( D f M o d A p p G r a G 1 . Q r E r r G r a n d z ,   D M o d G . M y P I D _ D B ,   [  
     R e s u l t   : =   D f M o d A p p G r a G 1 . G r a n d e s a s I n c o n s i s t e n t e s ( [  
     '   D R O P   T A B L E   I F   E X I S T S   _ c o u r o s _ e r r _ g r a n d e z a _ ;   ' ,  
     ' C R E A T E   T A B L E   _ c o u r o s _ e r r _ g r a n d e z a _   ' ,  
     '   ' ,  
     ' S E L E C T   g g x . G r a G r u 1   N i v e l 1 ,   g g x . C o n t r o l e   R e d u z i d o ,   ' ,  
     ' C O N C A T ( g g 1 . N o m e ,   ' ,  
     ' I F ( g t i . P r i n t T a m = 0 ,   " " ,   C O N C A T ( "   " ,   g t i . N o m e ) ) ,   ' ,  
     ' I F ( g c c . P r i n t C o r = 0 , " " ,   C O N C A T ( "   " ,   g c c . N o m e ) ) )   ' ,  
     ' N O _ G G 1 ,   m e d . S i g l a ,   m e d . G r a n d e z a ,   ' ,  
     ' x c o . G r a n d e z a   x c o _ G r a n d e z a ,     ' ,  
     ' C A S T ( I F ( x c o . G r a n d e z a   >   0 ,   x c o . G r a n d e z a ,     ' ,  
     '     I F ( ( g g x . G r a G r u Y < 2 0 4 8   O R     ' ,  
     '     x c o . C o u N i v 2 < > 1 ) ,   2 ,     ' ,  
     '     I F ( x c o . C o u N i v 2 = 1 ,   1 ,   - 1 ) ) )   A S   D E C I M A L ( 1 1 , 0 ) )     d i f _ G r a n d e z a   ' ,  
     ' F R O M   '   +   T a b S r c   +   '   s e s   ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x         g g x   O N   g g x . C o n t r o l e = s e s . '   +   G G X S r c ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u c         g g c   O N   g g c . C o n t r o l e = g g x . G r a G r u C ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a c o r c a d     g c c   O N   g c c . C o d i g o = g g c . G r a C o r C a d ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a t a m i t s     g t i   O N   g t i . C o n t r o l e = g g x . G r a T a m I ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1     ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2     ' ,  
     ' W H E R E   ( I F ( x c o . G r a n d e z a > 0 ,   x c o . G r a n d e z a ,   ' ,  
     '     I F ( ( g g x . G r a G r u Y < 2 0 4 8   O R   x c o . C o u N i v 2 < > 1 ) ,   2 ,   ' ,  
     '         I F ( x c o . C o u N i v 2 = 1 ,   1 ,   - 1 ) ) )   < >   m e d . G r a n d e z a )   ' ,  
     '   ' ,  
     ' U N I O N   ' ,  
     '   ' ,  
     ' S E L E C T   g g x . G r a G r u 1   N i v e l 1 ,   g g x . C o n t r o l e   R e d u z i d o ,   ' ,  
     ' C O N C A T ( g g 1 . N o m e ,   ' ,  
     ' I F ( g t i . P r i n t T a m = 0 ,   " " ,   C O N C A T ( "   " ,   g t i . N o m e ) ) ,   ' ,  
     ' I F ( g c c . P r i n t C o r = 0 , " " ,   C O N C A T ( "   " ,   g c c . N o m e ) ) )   ' ,  
     ' N O _ G G 1 ,   m e d . S i g l a ,   m e d . G r a n d e z a ,   ' ,  
     ' x c o . G r a n d e z a   x c o _ G r a n d e z a ,     ' ,  
     ' C A S T ( I F ( x c o . G r a n d e z a   >   0 ,   x c o . G r a n d e z a ,     ' ,  
     '     I F ( ( g g x . G r a G r u Y < 2 0 4 8   O R     ' ,  
     '     x c o . C o u N i v 2 < > 1 ) ,   2 ,     ' ,  
     '     I F ( x c o . C o u N i v 2 = 1 ,   1 ,   - 1 ) ) )   A S   S I G N E D )   d i f _ G r a n d e z a   ' ,  
     ' F R O M   '   +   T a b S r c   +   '   s e s   ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x   g g x   O N   g g x . C o n t r o l e = s e s . '   +   G G X D s t ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1   g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d   m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u c         g g c   O N   g g c . C o n t r o l e = g g x . G r a G r u C ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a c o r c a d     g c c   O N   g c c . C o d i g o = g g c . G r a C o r C a d ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a t a m i t s     g t i   O N   g t i . C o n t r o l e = g g x . G r a T a m I ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1     ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2     ' ,  
     ' W H E R E   ( I F ( x c o . G r a n d e z a > 0 ,   x c o . G r a n d e z a ,   ' ,  
     '     I F ( ( g g x . G r a G r u Y < 2 0 4 8   O R   x c o . C o u N i v 2 < > 1 ) ,   2 ,   ' ,  
     '         I F ( x c o . C o u N i v 2 = 1 ,   1 ,   - 1 ) ) )   < >   m e d . G r a n d e z a )   ' ,  
     '   ' ,  
     ' ;   ' ,  
     ' S E L E C T   *   F R O M   _ c o u r o s _ e r r _ g r a n d e z a _   ' ,  
     ' ' ] ,   D M o d G . M y P I D _ D B ) ;  
 ( *  
     / /  
     R e s u l t   : =   D f M o d A p p G r a G 1 . Q r E r r G r a n d z . R e c o r d C o u n t   >   0 ;  
     i f   R e s u l t   t h e n  
     b e g i n  
         M y O b j e c t s . f r x D e f i n e D a t a S e t s ( D f M o d A p p G r a G 1 . f r x E r r G r a n d z ,   [  
             D M o d G . f r x D s D o n o ,  
             D f M o d A p p G r a G 1 . f r x D s E r r G r a n d z  
         ] ) ;  
         M y O b j e c t s . f r x M o s t r a ( D f M o d A p p G r a G 1 . f r x E r r G r a n d z ,   ' G r a n d e z a s   i n e s p e r a d a s ' ) ;  
         C l o s e ;  
     e n d ;  
 * )  
 e n d ;  
  
 p r o c e d u r e   T F m S P E D _ E F D _ K 2 X X . F o r m A c t i v a t e ( S e n d e r :   T O b j e c t ) ;  
 b e g i n  
     M y O b j e c t s . C o r I n i C o m p o n e n t e ( ) ;  
     / /  
     V e r i f i c a G G X D s t Z e r a d o s ( ) ;  
 e n d ;  
  
 p r o c e d u r e   T F m S P E D _ E F D _ K 2 X X . F o r m C r e a t e ( S e n d e r :   T O b j e c t ) ;  
 b e g i n  
     I m g T i p o . S Q L T y p e   : =   s t L o k ;  
     P C R e g i s t r o . A c t i v e P a g e I n d e x   : =   0 ;  
     C G I m p o r t a r . V a l u e   : =   C G I m p o r t a r . M a x V a l u e ;  
     M y O b j e c t s . P r e e n c h e C B A n o E C B M e s ( C B A n o ,   C B M e s ,   - 1 ) ;  
     M e A v i s o . L i n e s . C l e a r ;  
 e n d ;  
  
 p r o c e d u r e   T F m S P E D _ E F D _ K 2 X X . F o r m R e s i z e ( S e n d e r :   T O b j e c t ) ;  
 b e g i n  
     M y O b j e c t s . E n t i t u l a ( G B _ M ,   [ L a T i t u l o 1 A ,   L a T i t u l o 1 B ,   L a T i t u l o 1 C ] ,  
     [ L a A v i s o 1 ,   L a A v i s o 2 ] ,   C a p t i o n ,   T r u e ,   t a C e n t e r ,   2 ,   1 0 ,   2 0 ) ;  
 e n d ;  
  
 f u n c t i o n   T F m S P E D _ E F D _ K 2 X X . I m p o r t a C l a s s e E R e c l a s s e C a C ( S Q L _ P e r i o d o :   S t r i n g ) :   B o o l e a n ;  
 v a r  
     C o d i g o ,   M o v i m C o d ,   C t r l D s t ,   C t r l O r i ,   G G X D s t ,   G G X O r i :   I n t e g e r ;  
 b e g i n  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' G e r a n d o   T a b e l a   t e m p o r � r i a   C a C I t s ' ) ;  
     U n D m k D A C _ P F . E x e c u t a M y S Q L Q u e r y 0 ( D M o d G . Q r U p d P I D 1 ,   D M o d G . M y P I D _ D B ,   [  
     ' D R O P   T A B L E   I F   E X I S T S   _ S P E D _ E F D _ K 2 X X _ C A C I T S _ ;   ' ,  
     '   ' ,  
     ' C R E A T E   T A B L E   _ S P E D _ E F D _ K 2 X X _ C A C I T S _   ' ,  
     '   ' ,  
     ' S E L E C T   C a c I D ,   C o d i g o ,   D A T E ( D a t a H o r a )   D a t a ,   ' ,  
     ' V M I _ S o r c ,   V M I _ D e s t ,   S U M ( P e c a s )   P e c a s ,   ' ,  
     ' S U M ( A r e a M 2 )   A r e a M 2 ,   S U M ( A r e a P 2 )   A r e a P 2   ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . v s c a c i t s a   ' ,  
     / / ' W H E R E   D a t a H o r a     B E T W E E N   " 2 0 1 6 - 0 1 - 0 1 "   A N D   " 2 0 1 6 - 0 1 - 3 1   2 3 : 5 9 : 5 9 "   ' ,  
     S Q L _ P e r i o d o ,  
     ' A N D   P e c a s   >   0   ' ,  
     ' G R O U P   B Y   V M I _ S o r c ,   V M I _ D e s t   ' ,  
     '   ' ,  
     ' U N I O N   ' ,  
     '   ' ,  
     ' S E L E C T   C a c I D ,   C o d i g o ,   D A T E ( D a t a H o r a )   D a t a ,   ' ,  
     ' V M I _ S o r c ,   V M I _ D e s t ,   S U M ( P e c a s )   P e c a s ,   ' ,  
     ' S U M ( A r e a M 2 )   A r e a M 2 ,   S U M ( A r e a P 2 )   A r e a P 2   ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . v s c a c i t s b   ' ,  
     / / ' W H E R E   D a t a H o r a     B E T W E E N   " 2 0 1 6 - 0 1 - 0 1 "   A N D   " 2 0 1 6 - 0 1 - 3 1   2 3 : 5 9 : 5 9 "   ' ,  
     S Q L _ P e r i o d o ,  
     ' A N D   P e c a s   >   0   ' ,  
     ' G R O U P   B Y   V M I _ S o r c ,   V M I _ D e s t ;   ' ,  
     ' ' ] ) ;  
     / /   / * S E L E C T   *   F R O M   _ S P E D _ E F D _ K 2 X X _ C A C I T S _ * /  
     / /  
     / /   v e r   n o   C a C I t s A     e   B  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' G e r a n d o   T a b e l a   t e m p o r � r i a   V M I   S r c ' ) ;  
     U n D m k D A C _ P F . E x e c u t a M y S Q L Q u e r y 0 ( D M o d G . Q r U p d P I D 1 ,   D M o d G . M y P I D _ D B ,   [  
     ' D R O P   T A B L E   I F   E X I S T S   _ S P E D _ E F D _ K 2 X X _ V M I _ S R C _ ;   ' ,  
     '   ' ,  
     ' C R E A T E   T A B L E   _ S P E D _ E F D _ K 2 X X _ V M I _ S R C _   ' ,  
     '   ' ,  
     ' S E L E C T   C o n t r o l e ,   G r a G r u X   ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . '   +   C O _ T A B _ V M I   +   '   ' ,  
     ' W H E R E   C o n t r o l e   I N   (   ' ,  
     '     S E L E C T   D I S T I N C T   V M I _ S o r c   ' ,  
     '     F R O M   _ s p e d _ e f d _ k 2 x x _ c a c i t s _   ' ,  
     ' )   ' ,  
     ' A N D   E m p r e s a = '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
     '   ' ,  
     ' U N I O N   ' ,  
     '   ' ,  
     ' S E L E C T   C o n t r o l e ,   G r a G r u X   ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . v s m o v i t b   ' ,  
     ' W H E R E   C o n t r o l e   I N   (   ' ,  
     '     S E L E C T   D I S T I N C T   V M I _ S o r c   ' ,  
     '     F R O M   _ s p e d _ e f d _ k 2 x x _ c a c i t s _   ' ,  
     ' )   ' ,  
     ' A N D   E m p r e s a = '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
     ' ' ] ) ;  
     / /  
  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' G e r a n d o   T a b e l a   t e m p o r � r i a   V M I   D s t ' ) ;  
     U n D m k D A C _ P F . E x e c u t a M y S Q L Q u e r y 0 ( D M o d G . Q r U p d P I D 1 ,   D M o d G . M y P I D _ D B ,   [  
     ' D R O P   T A B L E   I F   E X I S T S   _ S P E D _ E F D _ K 2 X X _ V M I _ D S T _ ;   ' ,  
     '   ' ,  
     ' C R E A T E   T A B L E   _ S P E D _ E F D _ K 2 X X _ V M I _ D S T _   ' ,  
     '   ' ,  
     ' S E L E C T   C o n t r o l e ,   G r a G r u X   ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . '   +   C O _ T A B _ V M I   +   '   ' ,  
     ' W H E R E   C o n t r o l e   I N   (   ' ,  
     '     S E L E C T   D I S T I N C T   V M I _ D e s t   ' ,  
     '     F R O M   _ s p e d _ e f d _ k 2 x x _ c a c i t s _   ' ,  
     ' )   ' ,  
     ' A N D   E m p r e s a = '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
     '   ' ,  
     ' U N I O N   ' ,  
     '   ' ,  
     ' S E L E C T   C o n t r o l e ,   G r a G r u X   ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . v s m o v i t b   ' ,  
     ' W H E R E   C o n t r o l e   I N   (   ' ,  
     '     S E L E C T   D I S T I N C T   V M I _ D e s t   ' ,  
     '     F R O M   _ s p e d _ e f d _ k 2 x x _ c a c i t s _   ' ,  
     ' )   ' ,  
     ' A N D   E m p r e s a = '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
     ' ' ] ) ;  
     / /  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' I m p o r t a n d o   r e g i s t r o s   d e   c l a s s e   e   r e c l a s e   d e   C a C ' ) ;  
     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r C a c I t s ,   D M o d G . M y P I D _ D B ,   [  
     ' D R O P   T A B L E   I F   E X I S T S   _ S P E D _ E F D _ K 2 X X _ S U M ;   ' ,  
     '   ' ,  
     ' C R E A T E   T A B L E   _ S P E D _ E F D _ K 2 X X _ S U M   ' ,  
     '   ' ,  
     ' S E L E C T   c a c . * ,   s r c . G r a G r u X   G G X _ S r c ,   d s t . G r a G r u X   G G X _ D s t     ' ,  
     ' F R O M   _ s p e d _ e f d _ k 2 x x _ c a c i t s _   c a c   ' ,  
     ' L E F T   J O I N   _ s p e d _ e f d _ k 2 x x _ v m i _ s r c _   s r c   O N   c a c . V M I _ S o r c = s r c . C o n t r o l e   ' ,  
     ' L E F T   J O I N   _ s p e d _ e f d _ k 2 x x _ v m i _ d s t _   d s t   O N   c a c . V M I _ D e s t = d s t . C o n t r o l e   ' ,  
     ' ;   ' ,  
     '   ' ,  
     ' S E L E C T   C a c I D ,   C o d i g o ,   D a t a ,   V M I _ D e s t ,   G G X _ S r c ,   G G X _ D s t ,   S U M ( P e c a s )   P e c a s ,   ' ,  
     ' S U M ( A r e a M 2 )   A r e a M 2 ,   S U M ( A r e a P 2 )   A r e a P 2   ' ,  
     ' F R O M   _ S P E D _ E F D _ K 2 X X _ S U M   ' ,  
     ' G R O U P   B Y   C a c I D ,   C o d i g o ,   D a t a ,   V M I _ D e s t ,   G G X _ S r c ,   G G X _ D s t     ' ,  
     ' O R D E R   B Y   C a c I D ,   C o d i g o ,   D a t a ,   V M I _ D e s t ,   G G X _ S r c ,   G G X _ D s t     ' ,  
     ' ' ] ) ;  
     / / G e r a l . M B _ S Q L ( S e l f ,   Q r C a c I t s ) ;  
 / /     i f   E r r G r a n d e z a C a c ( )   t h e n  
     i f   E r r G r a n d e z a ( ' _ S P E D _ E F D _ K 2 X X _ S U M ' ,   ' G G x _ S r c ' ,   ' G G X _ D s t ' )   t h e n  
         E x i t ;  
     P B 2 . M a x   : =   Q r C a C I t s . R e c o r d C o u n t ;  
     P B 2 . P o s i t i o n   : =   0 ;  
     Q r C a C I t s . F i r s t ;  
     w h i l e   n o t   Q r C a C I t s . E o f   d o  
     b e g i n  
         M y O b j e c t s . U p d P B ( P B 2 ,   n i l ,   n i l ) ;  
         i f   Q r C a c I t s G G X _ S r c . V a l u e   < >   Q r C a c I t s G G X _ D s t . V a l u e   t h e n  
         b e g i n  
             C o d i g o       : =   Q r C a c I t s C o d i g o . V a l u e ;  
             M o v i m C o d   : =   V S _ P F . O b t e m M o v i m C o d D e M o v i m I D E C o d i g o ( T E s t q M o v i m I D ( Q r C a c I t s C a c I D . V a l u e ) , / / T E s t q M o v i m I D . e m i d C l a s s A r t V S U n i ,  
                                     C o d i g o ) ;  
             C t r l D s t     : =   Q r C a c I t s V M I _ D e s t . V a l u e ;  
             / / / C t r l O r i     : =   Q r C a c I t s V M I _ S o r c . V a l u e ;  
             G G X D s t       : =   Q r C a c I t s G G X _ D s t . V a l u e ;  
             G G X O r i       : =   Q r C a c I t s G G X _ S r c . V a l u e ;  
             I n s e r e I t e m A t u a l _ K 2 2 0 ( Q r C a c I t s C a c I D . V a l u e ,   / / I n t e g e r ( T E s t q M o v i m I D . e m i d C l a s s A r t V S U n i ) ,  
             Q r C a c I t s D a t a . V a l u e ,   Q r C a c I t s G G X _ S r c . V a l u e ,   Q r C a c I t s G G X _ D s t . V a l u e ,  
             Q r C a c I t s A r e a M 2 . V a l u e ,  
             C o d i g o ,   M o v i m C o d ,   C t r l D s t ,  
             ( * C t r l O r i , * )   G G X D s t ,   G G X O r i ,   e s o m i e m C R U n i ) ;  
         e n d ;  
         / /  
         Q r C a C I t s . N e x t ;  
     e n d ;  
     R e s u l t   : =   T r u e ;  
 e n d ;  
  
 f u n c t i o n   T F m S P E D _ E F D _ K 2 X X . I m p o r t a C l a s s e E R e c l a s s e M u l ( Q r y :   T M y S Q L Q u e r y ;   M o v i m I D :  
     T E s t q M o v i m I D ;   S Q L _ P e r i o d o :   S t r i n g ) :   B o o l e a n ;  
 b e g i n  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' I m p o r t a n d o   M o v i m I D :   '   +  
         G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) ) ) ;  
 ( *  
     / /   m u d a r   p a r a   i t e n s   i s o l a d o s  
     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r y ,   D m o d . M y D B ,   [  
     ' S E L E C T   D A T E ( D a t a H o r a )   D a t a ,     G r a G r u X   G G X _ D s t ,   D s t G G X   G G X _ S r c ,   ' ,  
     ' S U M ( P e c a s )   P e c a s ,   S U M ( A r e a M 2 )   A r e a M 2 ,   S U M ( P e s o K g )   P e s o K g   ' ,  
     ' F R O M   '   +   C O _ T A B _ V M I   +   '   ' ,  
     S Q L _ P e r i o d o ,  
     ' A N D   E m p r e s a = '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
     ' A N D   M o v i m I D = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) ) ,   / / T E s t q M o v i m I D . e m i d C l a s s A r t V S M u l ) ) ,   / /   1 4  
     ' A N D   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( T E s t q M o v i m N i v . e m i n D e s t C l a s s ) ) ,   / /   2  
     ' G R O U P   B Y   D a t a ,   G r a G r u X ,   D s t G G X   ' ,  
     ' O R D E R   B Y   D a t a ,   G r a G r u X ,   D s t G G X   ' ,  
     ' ' ] ) ;  
 * )  
     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r y ,   D m o d . M y D B ,   [  
     ' S E L E C T   C o d i g o ,   M o v i m C o d ,   C o n t r o l e   C t r l D s t ,   D s t N i v e l 2   C t r l S r c ,   ' ,  
     ' D A T E ( D a t a H o r a )   D a t a ,     G r a G r u X   G G X _ D s t ,   D s t G G X   G G X _ S r c ,   ' ,  
     ' P e c a s ,   A r e a M 2 ,   P e s o K g   ' ,  
     ' F R O M   '   +   C O _ T A B _ V M I   +   '   ' ,  
     S Q L _ P e r i o d o ,  
     ' A N D   E m p r e s a = '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
     ' A N D   M o v i m I D = '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) ) ,   / / T E s t q M o v i m I D . e m i d C l a s s A r t V S M u l ) ) ,   / /   1 4  
     ' A N D   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( T E s t q M o v i m N i v . e m i n D e s t C l a s s ) ) ,   / /   2  
     ' O R D E R   B Y   D a t a ,   G r a G r u X ,   D s t G G X   ' ,  
     ' ' ] ) ;  
     G e r a l . M B _ S Q L ( S e l f ,   Q r y ) ;  
     / /  
     P B 2 . M a x   : =   Q r y . R e c o r d C o u n t ;  
     P B 2 . P o s i t i o n   : =   0 ;  
     Q r y . F i r s t ;  
     w h i l e   n o t   Q r y . E o f   d o  
     b e g i n  
         M y O b j e c t s . U p d P B ( P B 2 ,   n i l ,   n i l ) ;  
         i f   Q r y . F i e l d B y N a m e ( ' G G X _ S r c ' ) . A s I n t e g e r   < >   Q r y . F i e l d B y N a m e ( ' G G X _ D s t ' ) . A s I n t e g e r   t h e n  
         b e g i n  
             I n s e r e I t e m A t u a l _ K 2 2 0 ( I n t e g e r ( T E s t q M o v i m I D . e m i d C l a s s A r t V S M u l ) ,  
                 Q r y . F i e l d B y N a m e ( ' D a t a ' ) . A s D a t e T i m e ,  
                 Q r y . F i e l d B y N a m e ( ' G G X _ S r c ' ) . A s I n t e g e r ,  
                 Q r y . F i e l d B y N a m e ( ' G G X _ D s t ' ) . A s I n t e g e r ,  
                 Q r y . F i e l d B y N a m e ( ' A r e a M 2 ' ) . A s F l o a t ,  
                 Q r y . F i e l d B y N a m e ( ' C o d i g o ' ) . A s I n t e g e r ,  
                 Q r y . F i e l d B y N a m e ( ' M o v i m C o d ' ) . A s I n t e g e r ,  
                 / / Q r y . F i e l d B y N a m e ( ' C t r l S r c ' ) . A s I n t e g e r ,  
                 Q r y . F i e l d B y N a m e ( ' C t r l D s t ' ) . A s I n t e g e r ,  
                 Q r y . F i e l d B y N a m e ( ' G G X _ D s t ' ) . A s I n t e g e r ,  
                 Q r y . F i e l d B y N a m e ( ' G G X _ S r c ' ) . A s I n t e g e r ,  
                 e s o m i e m C R M u l ) ;  
         e n d ;  
         / /  
         Q r y . N e x t ;  
     e n d ;  
     R e s u l t   : =   T r u e ;  
 e n d ;  
  
 f u n c t i o n   T F m S P E D _ E F D _ K 2 X X . I m p o r t a C l a s s e G G X R c l ( S Q L _ P e r i o d o :   S t r i n g ) :   B o o l e a n ;  
 c o n s t  
     F a t o r   =   - 1 ;  
 v a r  
     Q t d e :   D o u b l e ;  
 b e g i n  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,  
         ' I m p o r t a n d o   r e g i s t r o s   c o m   t r o c a   d e   r e d u z i d o   e m   N F e   ( R e c l a s s e   d i r e t a ) ' ) ;  
  
     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r G G X R c l ,   D M o d G . M y P I D _ D B ,   [  
     ' D R O P   T A B L E   I F   E X I S T S   _ S P E D _ E F D _ K 2 X X _ G G X _ R C L ; ' ,  
     ' C R E A T E   T A B L E   _ S P E D _ E F D _ K 2 X X _ G G X _ R C L ' ,  
     ' ' ,  
     ' S E L E C T   v m i . M o v i m I D ,   v m i . C o d i g o ,   v m i . M o v i m C o d ,   v m i . C o n t r o l e ,   ' ,  
     ' D A T E ( v m i . D a t a H o r a )   D a t a ,     v m i . G r a G r u X   G G X _ S r c ,   ' ,  
     ' v m i . G G X R c l   G G X _ D s t ,   P e c a s ,   A r e a M 2 ,   P e s o K g ,   m e d . G r a n d e z a ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . '   +   C O _ T A B _ V M I   +   '   v m i   ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x   g g x   O N   g g x . C o n t r o l e = v m i . G r a G r u X ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1   g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d   m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
     S Q L _ P e r i o d o ,  
     ' A N D   v m i . G G X R c l   < >   0   ' ,  
     ' A N D   v m i . E m p r e s a = '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
     ' ; ' ,  
     ' ' ,  
     ' S E L E C T   *   F R O M   _ S P E D _ E F D _ K 2 X X _ G G X _ R C L ' ,  
     ' ' ] ) ;  
     / /  
     / / G e r a l . M B _ S Q L ( S e l f ,   Q r G G X R c l ) ;  
     / /  
     i f   E r r G r a n d e z a ( ' _ S P E D _ E F D _ K 2 X X _ G G X _ R C L ' ,   ' G G x _ S r c ' ,   ' G G X _ D s t ' )   t h e n  
         E x i t ;  
     / /  
     P B 2 . M a x   : =   Q r G G X R c l . R e c o r d C o u n t ;  
     P B 2 . P o s i t i o n   : =   0 ;  
     Q r G G X R c l . F i r s t ;  
     w h i l e   n o t   Q r G G X R c l . E o f   d o  
     b e g i n  
         M y O b j e c t s . U p d P B ( P B 2 ,   n i l ,   n i l ) ;  
         i f   Q r G G X R c l G G X _ S r c . V a l u e   < >   Q r G G X R c l G G X _ D s t . V a l u e   t h e n  
         b e g i n  
             c a s e     Q r G G X R c l G r a n d e z a . V a l u e   o f  
                 0 :   Q t d e   : =   Q r G G X R c l P e c a s . V a l u e   *   F a t o r ;  
                 1 :   Q t d e   : =   Q r G G X R c l A r e a M 2 . V a l u e   *   F a t o r ;  
                 2 :   Q t d e   : =   Q r G G X R c l P e s o K g . V a l u e   *   F a t o r ;  
             e n d ;  
             I n s e r e I t e m A t u a l _ K 2 2 0 ( Q r G G X R c l M o v i m I D . V a l u e ,  
             Q r G G X R c l D a t a . V a l u e ,   Q r G G X R c l G G X _ S r c . V a l u e ,   Q r G G X R c l G G X _ D s t . V a l u e ,   Q t d e ,  
             Q r G G X R c l C o d i g o . V a l u e ,   Q r G G X R c l M o v i m C o d . V a l u e ,   Q r G G X R c l C o n t r o l e . V a l u e ,  
             Q r G G X R c l G G X _ D s t . V a l u e ,   Q r G G X R c l G G X _ S r c . V a l u e ,   e s o m i e m G G X R c l ) ;  
         e n d ;  
         / /  
         Q r G G X R c l . N e x t ;  
     e n d ;  
     R e s u l t   : =   T r u e ;  
 e n d ;  
  
 f u n c t i o n   T F m S P E D _ E F D _ K 2 X X . I m p o r t a C l a s s e O p e P r o c ( S Q L _ P e r i o d o :   S t r i n g ) :   B o o l e a n ;  
 v a r  
     Q t d e :   D o u b l e ;  
 b e g i n  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,  
         ' I m p o r t a n d o   r e g i s t r o s   d e   c l a s s e s   s e c u n d � r i a s   d e   o p e r a � � e s   e   p r o c e s s o s ' ) ;  
     / /  
     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r S e c O _ P ,   D M o d G . M y P I D _ D B ,   [  
     ' D R O P   T A B L E   I F   E X I S T S   _ S P E D _ E F D _ K 2 X X _ O _ P ; ' ,  
     ' C R E A T E   T A B L E   _ S P E D _ E F D _ K 2 X X _ O _ P ' ,  
     ' ' ,  
     ' S E L E C T   v m i . M o v i m I D ,   v m i . C o d i g o ,   v m i . M o v i m C o d ,   v m i . C o n t r o l e ,   ' ,  
     ' D A T E ( v m i . D a t a H o r a )   D a t a ,     c a b . G G X D s t   G G X _ S r c ,   ' ,  
     ' v m i . G r a G r u X   G G X _ D s t ,   P e c a s ,   A r e a M 2 ,   P e s o K g ,   m e d . G r a n d e z a ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . v s o p e c a b   c a b ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . '   +   C O _ T A B _ V M I   +   '   v m i   O N   v m i . M o v i m C o d = c a b . M o v i m C o d ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x   g g x   O N   g g x . C o n t r o l e = v m i . G r a G r u X ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1   g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d   m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
     S Q L _ P e r i o d o ,  
     / / ' W H E R E   v m i . D a t a H o r a   B E T W E E N   " 2 0 1 6 - 0 1 - 0 1 "   A N D   " 2 0 1 6 - 0 1 - 3 1 "   ' ,  
     ' A N D   c a b . G G X D s t   < >   v m i . G r a g r u X ' ,  
     ' A N D   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( e m i n D e s t O p e r ) ) ,   / / 9  
     ' A N D   M o v i m T w n < > 0   ' ,   / /   N a o   b u s c a r   s u b - p r o d u t o !  
     ' ' ,  
     ' U N I O N ' ,  
     ' ' ,  
     ' S E L E C T   v m i . M o v i m I D ,   v m i . C o d i g o ,   v m i . M o v i m C o d ,   v m i . C o n t r o l e ,   ' ,  
     ' D A T E ( v m i . D a t a H o r a )   D a t a ,     c a b . G G X D s t   G G X _ S r c ,   ' ,  
     ' v m i . G r a G r u X   G G X _ D s t ,   P e c a s ,   A r e a M 2 ,   P e s o K g ,   m e d . G r a n d e z a ' ,  
     ' F R O M   '   +   T M e u D B   +   ' . v s p w e c a b   c a b ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . '   +   C O _ T A B _ V M I   +   '   v m i   O N   v m i . M o v i m C o d = c a b . M o v i m C o d ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x   g g x   O N   g g x . C o n t r o l e = v m i . G r a G r u X ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1   g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
     ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d   m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
     S Q L _ P e r i o d o ,  
     / / ' W H E R E   v m i . D a t a H o r a   B E T W E E N   " 2 0 1 6 - 0 1 - 0 1 "   A N D   " 2 0 1 6 - 0 1 - 3 1 "   ' ,  
     ' A N D   c a b . G G X D s t   < >   v m i . G r a g r u X ' ,  
     ' A N D   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( e m i n D e s t W E n d ) ) ,   / / 2 2  
     ' A N D   M o v i m T w n < > 0   ' ,   / /   N a o   b u s c a r   s u b - p r o d u t o !  
     ' ; ' ,  
     ' ' ,  
     ' S E L E C T   *   F R O M   _ S P E D _ E F D _ K 2 X X _ O _ P ' ,  
     ' ' ] ) ;  
     / /  
     / / G e r a l . M B _ S Q L ( S e l f ,   Q r S e c O _ P ) ;  
     / /  
     i f   E r r G r a n d e z a ( ' _ S P E D _ E F D _ K 2 X X _ O _ P ' ,   ' G G x _ S r c ' ,   ' G G X _ D s t ' )   t h e n  
         E x i t ;  
     / /  
     P B 2 . M a x   : =   Q r S e c O _ P . R e c o r d C o u n t ;  
     P B 2 . P o s i t i o n   : =   0 ;  
     Q r S e c O _ P . F i r s t ;  
     w h i l e   n o t   Q r S e c O _ P . E o f   d o  
     b e g i n  
         M y O b j e c t s . U p d P B ( P B 2 ,   n i l ,   n i l ) ;  
         i f   Q r S e c O _ P G G X _ S r c . V a l u e   < >   Q r S e c O _ P G G X _ D s t . V a l u e   t h e n  
         b e g i n  
             c a s e     Q r S e c O _ P G r a n d e z a . V a l u e   o f  
                 0 :   Q t d e   : =   Q r S e c O _ P P e c a s . V a l u e ;  
                 1 :   Q t d e   : =   Q r S e c O _ P A r e a M 2 . V a l u e ;  
                 2 :   Q t d e   : =   Q r S e c O _ P P e s o K g . V a l u e ;  
             e n d ;  
             I n s e r e I t e m A t u a l _ K 2 2 0 ( Q r S e c O _ P M o v i m I D . V a l u e ,  
             Q r S e c O _ P D a t a . V a l u e ,   Q r S e c O _ P G G X _ S r c . V a l u e ,   Q r S e c O _ P G G X _ D s t . V a l u e ,   Q t d e ,  
             Q r S e c O _ P C o d i g o . V a l u e ,   Q r S e c O _ P M o v i m C o d . V a l u e ,   Q r S e c O _ P C o n t r o l e . V a l u e ,  
             Q r S e c O _ P G G X _ D s t . V a l u e ,   Q r S e c O _ P G G X _ S r c . V a l u e ,   e s o m i e m O p e P r o c ) ;  
         e n d ;  
         / /  
         Q r S e c O _ P . N e x t ;  
     e n d ;  
     R e s u l t   : =   T r u e ;  
 e n d ;  
  
 f u n c t i o n   T F m S P E D _ E F D _ K 2 X X . I m p o r t a G e r A r t ( S Q L _ P e r i o d o C o m p l e x o ,  
     S Q L _ P e r i o d o ,   S Q L _ D t H r F i m O p e :   S t r i n g ) :   B o o l e a n ;  
 c o n s t  
     M o v i m I D   =   T E s t q M o v i m I D . e m i d I n d s V S ;  
     s P r o c N a m e   =   ' F m S P E D _ E F D _ K 2 X X . I m p o r t a G e r A r t ( ) ' ;  
     / /  
     f u n c t i o n   F i l t r o R e g ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
     b e g i n  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 3 X :   R e s u l t   : =   ' = ' ;  
             t r s p 2 5 X :   R e s u l t   : =   ' < > ' ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ E r r o ( ' " F i l t r o R e g "   i n d e f i n i d o ! ' ) ;  
                 R e s u l t   : =   ' # E R R ' ;  
             e n d ;  
         e n d ;  
     e n d ;  
     f u n c t i o n   F i l t r o P e r i o d o ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   S t r i n g ;  
     b e g i n  
         c a s e   T i p o R e g S P E D P r o d   o f  
             t r s p 2 3 X :   R e s u l t   : =   S Q L _ P e r i o d o ;  
             / / t r s p 2 5 X :   R e s u l t   : =   S Q L _ D t H r F i m O p e ;  
             / / t r s p 2 5 X :   R e s u l t   : =   S Q L _ P e r i o d o ;  
             t r s p 2 5 X :   R e s u l t   : =   ' ' ;  
             e l s e  
             b e g i n  
                 G e r a l . M B _ E r r o ( ' " F i l t r o R e g "   i n d e f i n i d o ! ' ) ;  
                 R e s u l t   : =   ' # E R R _ P E R I O D O _ ' ;  
             e n d ;  
         e n d ;  
     e n d ;  
     / /  
     f u n c t i o n   I n s e r e R e g i s t r o s ( T i p o R e g S P E D P r o d :   T T i p o R e g S P E D P r o d ) :   B o o l e a n ;  
     v a r  
         M o v i m C o d ,   M o v i m T w n ,   C o d i g o ,   G r a G r u X ,   T i p o E s t q ,   M y F a t o r C a b ,   M y F A t o r I t s ,  
         L i n A r q P a i ,   I D _ I t e m ,   C o n t r o l e :   I n t e g e r ;  
         D t H r A b e r t o ,   D t H r F i m O p e :   T D a t e T i m e ;  
         A r e a M 2 ,   P e s o K g ,   P e c a s ,   Q t d e :   D o u b l e ;  
         C O D _ I N S _ S U B S T :   S t r i n g ;  
         / /  
         p r o c e d u r e   M e n s a g e m ( T e x t o B a s e :   S t r i n g ;   Q u e r y :   T m y S Q L Q u e r y ) ;  
         b e g i n  
             G e r a l . M B _ E R R O ( T e x t o B a s e   +   s L i n e B r e a k   +  
             ' M o v i m C o d :   '   +   G e r a l . F F 0 ( M o v i m C o d )   +   s L i n e B r e a k   +  
             ' M o v i m I D :   '   +   G e r a l . F F 0 ( I n t e g e r ( M o v i m I D ) )   +   s L i n e B r e a k   +  
             s P r o c N a m e   +   s L i n e B r e a k   +  
             s L i n e B r e a k   +   ' A v i s e   a   D E R M A T E K ! ! ! '   +   s L i n e B r e a k   +  
             Q u e r y . S Q L . T e x t ) ;  
         e n d ;  
     b e g i n  
         C O D _ I N S _ S U B S T   : =   ' ' ;  
         M y F a t o r C a b   : =   1 ;  
         M y F a t o r I t s   : =   - 1 ;  
         / /  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
 / / /     C a b e c a l h o   >   R e g i s t r o   K 2 3 0   o u   K 2 5 0  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r V m i G A r C a b ,   D m o d . M y D B ,   [  
         ' S E L E C T   g g 1 . U n i d M e d ,   m e d . G r a n d e z a   m e d _ g r a n d e z a ,   ' ,  
         ' x c o . G r a n d e z a   x c o _ G r a n d e z a ,   g g x . G r a G r u Y ,   x c o . C o u N i v 2 ,   ' ,  
         V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( T r u e ,   ' T i p o E s t q ' ,   T r u e ,  
         ' v m i . A r e a M 2 ' ,   ' v m i . P e s o K g ' ,   ' v m i . P e c a s ' ) ,  
         ' D A T E ( v m i . D a t a H o r a )   D a t a ,   v m i . C o d i g o ,   v m i . M o v i m C o d ,     ' ,  
         ' v m i . G r a G r u X ,   v m i . P e c a s ,   v m i . A r e a M 2 ,   v m i . P e s o K g ,     ' ,  
         ' v m i . M o v i m T w n ,   v m i . C o n t r o l e     ' ,  
         ' F R O M   v s m o v i t s                 v m i     ' ,  
         ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v m i . G r a G r u X   ' ,  
         ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
         ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
         / / ' L E F T   J O I N   g r a g r u c         g g c   O N   g g c . C o n t r o l e = g g x . G r a G r u C ' ,  
         / / ' L E F T   J O I N   g r a c o r c a d     g c c   O N   g c c . C o d i g o = g g c . G r a C o r C a d ' ,  
         / / ' L E F T   J O I N   g r a t a m i t s     g t i   O N   g t i . C o n t r o l e = g g x . G r a T a m I ' ,  
         ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
         / / ' L E F T   J O I N   c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1     ' ,  
         / / ' L E F T   J O I N   c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2     ' ,  
         ' W H E R E   v m i . M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( e m i n S o r c C u r t i V S ) ) ,   / /   1 4  
         / /   2 0 1 6 - 1 2 - 2 4  
         S Q L _ P e r i o d o ,  
         / / F i l t r o P e r i o d o ( T i p o R e g S P E D P r o d ) ,  
         / /   2 0 1 6 - 1 2 - 2 3  
         / / ' A N D   v m i . E m p r e s a '   +   F i l t r o R e g ( T i p o R e g S P E D P r o d )   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         ' A N D   v m i . E m p r e s a = '   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         ' A N D   v m i . F o r n e c M O '   +   F i l t r o R e g ( T i p o R e g S P E D P r o d )   +   G e r a l . F F 0 ( F E m p r e s a ) ,  
         / /  
         ' O R D E R   B Y   D a t a ,   v m i . M o v i m C o d ,   v m i . G r a G r u X   ' ,  
         ' ' ] ) ;  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q r V m i G A r C a b ) ;  
         Q r V m i G A r C a b . F i r s t ;  
         w h i l e   n o t   Q r V m i G A r C a b . E o f   d o  
         b e g i n  
             C o d i g o           : =   Q r V m i G A r C a b C o d i g o . V a l u e ;  
             M o v i m C o d       : =   Q r V m i G A r C a b M o v i m C o d . V a l u e ;  
             M o v i m T w n       : =   Q r V m i G A r C a b M o v i m T w n . V a l u e ;  
             D t H r A b e r t o   : =   Q r V m i G A r C a b D a t a . V a l u e ;  
             D t H r F i m O p e   : =   Q r V m i G A r C a b D a t a . V a l u e ;  
             G r a G r u X         : =   Q r V m i G A r C a b G r a G r u X . V a l u e ;  
             T i p o E s t q       : =   T r u n c ( Q r V m i G A r C a b T i p o E s t q . V a l u e ) ;  
             / /  
 ( *  
             A r e a M 2           : =   Q r V m i G A r C a b A r e a M 2 . V a l u e ;  
             P e s o K g           : =   Q r V m i G A r C a b P e s o K g . V a l u e ;  
             P e c a s             : =   Q r V m i G A r C a b P e c a s . V a l u e ;  
 * )  
             U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r S u m G A R ,   D m o d . M y D B ,   [  
             ' S E L E C T   S U M ( P e c a s )   P e c a s ,   S U M ( P e s o K g )   P e s o K g ,     ' ,  
             ' S U M ( A r e a M 2 )   A r e a M 2   ' ,  
             ' F R O M   v s m o v i t s   ' ,  
             ' W H E R E   M o v i m C o d = '   +   G e r a l . F F 0 ( M o v i m C o d ) ,  
             ' A N D   M o v i m N i v = '   +   G e r a l . F F 0 ( I n t e g e r ( e m i n D e s t C u r t i V S ) ) ,  
             ' ' ] ) ;  
             A r e a M 2           : =   Q r S u m G A R A r e a M 2 . V a l u e ;  
             P e s o K g           : =   Q r S u m G A R P e s o K g . V a l u e ;  
             P e c a s             : =   Q r S u m G A R P e c a s . V a l u e ;  
             / /  
             i f   ( P e c a s   < >   0 )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
             b e g i n  
                 c a s e   T i p o E s t q   o f  
                     1 :   Q t d e   : =   A r e a M 2 ;  
                     2 :   Q t d e   : =   P e s o K g ;  
                     e l s e  
                     b e g i n  
                         G e r a l . M B _ E r r o ( ' " T i p o E s t q "   =   '   +   G e r a l . F F 0 ( T i p o e s t q )   +   '   i n d e f i n i d o   e m   '  
                         +   s P r o c N a m e   +   s L i n e B r e a k   +   s G R A N D E Z A _ U N I D M E D [ T i p o E s t q ] ) ;  
                         / /  
                         Q t d e   : =   P e c a s ;  
                     e n d ;  
                 e n d ;  
                 Q t d e   : =   Q t d e   *   M y F a t o r C a b ;  
             e n d   e l s e  
                 Q t d e   : =   0 ;  
             / /  
             i f   Q t d e   <   0   t h e n  
                 M e n s a g e m ( ' Q u a n t i d a d e   n � o   p o d e   s e r   n e g a t i v a ! ! !   ( 1 ) '   +   s L i n e B r e a k   +  
                 ' Q t d e :   '   +   G e r a l . F F T ( Q t d e ,   3 ,   s i N e g a t i v o ) ,   Q r V m i G A r C a b )  
             e l s e   i f   ( Q t d e   =   0 )   a n d   ( D t H r F i m O p e   >   1 )   t h e n  
             b e g i n  
                 c a s e   M o v i m I D   o f  
                     e m i d I n d s V S :   D t H r F i m O p e   : =   0 ;  
                     e l s e   M e n s a g e m ( ' Q u a n t i d a d e   z e r a d a   p a r a   O P   e n c e r r a d a ! ! !   ( 1 ) ' ,   Q r V m i G A r C a b ) ;  
                 e n d ;  
             e n d ;  
             / /  
             c a s e   T i p o R e g S P E D P r o d   o f  
                 t r s p 2 3 X :  
                 b e g i n  
                     I n s e r e I t e m A t u a l _ K 2 3 0 ( I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   D t H r A b e r t o ,  
                     D t H r F i m O p e ,   G r a G r u X ,   Q t d e ,   L i n A r q P a i ) ;  
                 e n d ;  
                 t r s p 2 5 X :  
                 b e g i n  
                     I n s e r e I t e m A t u a l _ K 2 5 0 ( I n t e g e r ( M o v i m I D ) ,   C o d i g o ,   M o v i m C o d ,   D t H r F i m O p e ,  
                     G r a G r u X ,   Q t d e ,   L i n A r q P a i ) ;  
                 e n d ;  
                 e l s e   G e r a l . M B _ E r r o ( ' T i p o   d e   r e g i s t r o   S P E D   E F D   ( '   +   G e r a l . F F 0 ( I n t e g e r (  
                 T i p o R e g S P E D P r o d ) )   +   ' )   n � o   i m p l e m e n t a d o   e m   '   +   s P r o c N a m e ) ;  
             e n d ;  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
 / / /     I t e m   >   R e g i s t r o   K 2 3 5   o u   K 2 5 5  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
             / /  
             U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r V m i G A r I t s ,   D m o d . M y D B ,   [  
             ' S E L E C T   v m i . S r c M o v I D ,   v m i . S r c N i v e l 2 ,     v m i . S r c G G X ,   v m i . D s t G G X ,   ' ,  
             ' g g 1 . U n i d M e d ,   m e d . G r a n d e z a   m e d _ g r a n d e z a ,   ' ,  
             ' x c o . G r a n d e z a   x c o _ G r a n d e z a ,   g g x . G r a G r u Y ,   x c o . C o u N i v 2 ,   ' ,  
             V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( F a l s e ,   ' T i p o E s t q ' ,   T r u e ,  
             ' v m i . A r e a M 2 ' ,   ' v m i . P e s o K g ' ,   ' v m i . P e c a s ' ) ,  
             ' D A T E ( v m i . D a t a H o r a )   D a t a ,   v m i . C o d i g o ,   v m i . M o v i m C o d ,     ' ,  
             ' v m i . G r a G r u X ,   v m i . P e c a s ,   v m i . A r e a M 2 ,   v m i . P e s o K g ,     ' ,  
             ' v m i . M o v i m T w n ,   v m i . C o n t r o l e     ' ,  
             ' F R O M   v s m o v i t s                 v m i     ' ,  
             ' L E F T   J O I N   g r a g r u x         g g x   O N   g g x . C o n t r o l e = v m i . G r a G r u X   ' ,  
             ' L E F T   J O I N   g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = g g x . G r a G r u 1 ' ,  
             ' L E F T   J O I N   u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
             / / ' L E F T   J O I N   g r a g r u c         g g c   O N   g g c . C o n t r o l e = g g x . G r a G r u C ' ,  
             / / ' L E F T   J O I N   g r a c o r c a d     g c c   O N   g c c . C o d i g o = g g c . G r a C o r C a d ' ,  
             / / ' L E F T   J O I N   g r a t a m i t s     g t i   O N   g t i . C o n t r o l e = g g x . G r a T a m I ' ,  
             ' L E F T   J O I N   g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = g g x . C o n t r o l e   ' ,  
             / / ' L E F T   J O I N   c o u n i v 1         n v 1   O N   n v 1 . C o d i g o = x c o . C o u N i v 1     ' ,  
             / / ' L E F T   J O I N   c o u n i v 2         n v 2   O N   n v 2 . C o d i g o = x c o . C o u N i v 2     ' ,  
             ' W H E R E   v m i . M o v i m N i v = '   +   G e r a l . F F 