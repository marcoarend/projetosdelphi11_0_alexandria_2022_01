u n i t   S P E D _ E F D _ X 9 9 9 _ B a l a n c o s ;  
  
 i n t e r f a c e  
  
 u s e s  
     W i n d o w s ,   M e s s a g e s ,   S y s U t i l s ,   C l a s s e s ,   G r a p h i c s ,   C o n t r o l s ,   F o r m s ,   D i a l o g s ,   D B ,  
     E x t C t r l s ,   S t d C t r l s ,   B u t t o n s ,   G r i d s ,   D B G r i d s ,   d m k G e r a l ,   d m k L a b e l ,   d m k I m a g e ,  
     D B C t r l s ,   C o m C t r l s ,   U n I n t e r n a l C o n s t s ,   U n D m k E n u m s ,   d m k C h e c k G r o u p ,   m y S Q L D b T a b l e s ,  
     d m k D B L o o k u p C o m b o B o x ,   d m k E d i t ,   d m k E d i t C B ,   V a r i a n t s ,   d m k E d i t D a t e T i m e P i c k e r ,  
     f r x C l a s s ,   f r x D B S e t ,   A p p L i s t a s ,   S P E D _ L i s t a s ,   U n S P E D _ E F D _ P F ;  
  
 t y p e  
     T F m S P E D _ E F D _ X 9 9 9 _ B a l a n c o s   =   c l a s s ( T F o r m )  
         P n C a b e c a :   T P a n e l ;  
         G B _ R :   T G r o u p B o x ;  
         I m g T i p o :   T d m k I m a g e ;  
         G B _ L :   T G r o u p B o x ;  
         G B _ M :   T G r o u p B o x ;  
         L a T i t u l o 1 A :   T L a b e l ;  
         L a T i t u l o 1 B :   T L a b e l ;  
         P a n e l 2 :   T P a n e l ;  
         P a n e l 3 :   T P a n e l ;  
         G r o u p B o x 1 :   T G r o u p B o x ;  
         G B A v i s o s 1 :   T G r o u p B o x ;  
         P a n e l 4 :   T P a n e l ;  
         L a A v i s o 1 :   T L a b e l ;  
         L a A v i s o 2 :   T L a b e l ;  
         G B R o d a P e :   T G r o u p B o x ;  
         P n S a i D e s i s :   T P a n e l ;  
         B t S a i d a :   T B i t B t n ;  
         P a n e l 1 :   T P a n e l ;  
         B t O K :   T B i t B t n ;  
         L a T i t u l o 1 C :   T L a b e l ;  
         Q r P Q x :   T m y S Q L Q u e r y ;  
         Q r P Q x I n s u m o :   T I n t e g e r F i e l d ;  
         Q r P Q x P e s o :   T F l o a t F i e l d ;  
         Q r P Q x V a l o r :   T F l o a t F i e l d ;  
         Q r G r a G r u X :   T m y S Q L Q u e r y ;  
         Q r G r a G r u X C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r X 9 9 9 :   T m y S Q L Q u e r y ;  
         Q r G r a G r u X S i g l a :   T S t r i n g F i e l d ;  
         Q r P Q x C l i O r i g :   T I n t e g e r F i e l d ;  
         Q r N i v e l S e l 1 :   T m y S Q L Q u e r y ;  
         Q r N i v e l S e l 1 C o d i g o :   T I n t e g e r F i e l d ;  
         Q r N i v e l S e l 1 N o m e :   T S t r i n g F i e l d ;  
         D s N i v e l S e l 1 :   T D a t a S o u r c e ;  
         Q r N i v e l S e l 2 :   T m y S Q L Q u e r y ;  
         Q r N i v e l S e l 2 C o d i g o :   T I n t e g e r F i e l d ;  
         Q r N i v e l S e l 2 N o m e :   T S t r i n g F i e l d ;  
         D s N i v e l S e l 2 :   T D a t a S o u r c e ;  
         Q r V M I :   T m y S Q L Q u e r y ;  
         Q r V M I S i g l a :   T S t r i n g F i e l d ;  
         Q r V M I C o u N i v 2 :   T I n t e g e r F i e l d ;  
         Q r V M I G r a G r u Y :   T I n t e g e r F i e l d ;  
         Q r V M I G r a g R u X :   T I n t e g e r F i e l d ;  
         Q r V M I G r a G r u 1 :   T I n t e g e r F i e l d ;  
         Q r V M I U n M e d T x t :   T S t r i n g F i e l d ;  
         Q r V M I T i p o E s t q :   T L a r g e i n t F i e l d ;  
         Q r V M I S d o V r t P e c a :   T F l o a t F i e l d ;  
         Q r V M I S d o V r t A r M 2 :   T F l o a t F i e l d ;  
         Q r V M I S d o V r t P e s o :   T F l o a t F i e l d ;  
         Q r V M I N O _ P R D _ T A M _ C O R :   T S t r i n g F i e l d ;  
         Q r G r a G r u X U n i d M e d :   T I n t e g e r F i e l d ;  
         Q r V M I E m p r e s a :   T I n t e g e r F i e l d ;  
         Q r V M I G r a n d e z a :   T S m a l l i n t F i e l d ;  
         Q r V M I U n i d M e d :   T I n t e g e r F i e l d ;  
         Q r V M I P e c a s :   T F l o a t F i e l d ;  
         Q r V M I A r e a M 2 :   T F l o a t F i e l d ;  
         Q r V M I P e s o K g :   T F l o a t F i e l d ;  
         Q r V M I V a l o r T :   T F l o a t F i e l d ;  
         P n P e r i o d o :   T P a n e l ;  
         L a M e s :   T L a b e l ;  
         C B M e s :   T C o m b o B o x ;  
         C B A n o :   T C o m b o B o x ;  
         L a A n o :   T L a b e l ;  
         P C R e g i s t r o :   T P a g e C o n t r o l ;  
         T a b S h e e t 1 :   T T a b S h e e t ;  
         T a b S h e e t 2 :   T T a b S h e e t ;  
         T a b S h e e t 3 :   T T a b S h e e t ;  
         P a n e l 5 :   T P a n e l ;  
         G B I m p o r t a r 1 :   T G r o u p B o x ;  
         R G N i v P l a C t a 1 :   T R a d i o G r o u p ;  
         P a n e l 8 :   T P a n e l ;  
         L a b e l 1 :   T L a b e l ;  
         E d G e n P l a C t a 1 :   T d m k E d i t C B ;  
         C B G e n P l a C t a 1 :   T d m k D B L o o k u p C o m b o B o x ;  
         G B I m p o r t a r 2 :   T G r o u p B o x ;  
         R G N i v P l a C t a 2 :   T R a d i o G r o u p ;  
         P a n e l 9 :   T P a n e l ;  
         L a b e l 2 :   T L a b e l ;  
         E d G e n P l a C t a 2 :   T d m k E d i t C B ;  
         C B G e n P l a C t a 2 :   T d m k D B L o o k u p C o m b o B o x ;  
         L a D a t a :   T L a b e l ;  
         T P D a t a :   T d m k E d i t D a t e T i m e P i c k e r ;  
         C G I m p o r t a r :   T d m k C h e c k G r o u p ;  
         P B 1 :   T P r o g r e s s B a r ;  
         M e A v i s o :   T M e m o ;  
         Q r V M I E n t i S i t i o :   T I n t e g e r F i e l d ;  
         Q r V M I S i t P r o d :   T S m a l l i n t F i e l d ;  
         Q r V M I C l i e n t M O :   T I n t e g e r F i e l d ;  
         D s V M I :   T D a t a S o u r c e ;  
         P a g e C o n t r o l 1 :   T P a g e C o n t r o l ;  
         T a b S h e e t 5 :   T T a b S h e e t ;  
         T a b S h e e t 6 :   T T a b S h e e t ;  
         D B G r i d 1 :   T D B G r i d ;  
         Q r V M I 2 :   T m y S Q L Q u e r y ;  
         Q r V M I 2 I M E I :   T I n t e g e r F i e l d ;  
         Q r G r a G r u X T i p o _ I t e m :   T I n t e g e r F i e l d ;  
         Q r V M I T i p o _ I t e m :   T I n t e g e r F i e l d ;  
         Q r C A _ V S :   T m y S Q L Q u e r y ;  
         Q r C A _ V S E n t i S i t i o :   T I n t e g e r F i e l d ;  
         Q r C A _ V S G r a G r u X :   T I n t e g e r F i e l d ;  
         Q r C A _ V S P e c a s :   T F l o a t F i e l d ;  
         Q r C A _ V S P e s o K g :   T F l o a t F i e l d ;  
         Q r C A _ V S A r e a M 2 :   T F l o a t F i e l d ;  
         Q r C A _ V S M o v i m I D :   T I n t e g e r F i e l d ;  
         Q r C A _ V S C o d i g o :   T I n t e g e r F i e l d ;  
         Q r C A _ V S M o v i m C o d :   T I n t e g e r F i e l d ;  
         Q r C A _ V S M o v i m N i v :   T I n t e g e r F i e l d ;  
         Q r C A _ V S C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r C A _ V S M e s :   T L a r g e i n t F i e l d ;  
         Q r C A _ V S A n o :   T L a r g e i n t F i e l d ;  
         Q r C A _ V S G r a n d e z a :   T I n t e g e r F i e l d ;  
         Q r C A _ V S D a t a H o r a :   T D a t e T i m e F i e l d ;  
         Q r C A _ V S D t C o r r A p o :   T D a t e T i m e F i e l d ;  
         Q r C A _ V S C l i e n t M O :   T I n t e g e r F i e l d ;  
         Q r C A _ V S F o r n e c M O :   T I n t e g e r F i e l d ;  
         C G R e g i s t r o s :   T d m k C h e c k G r o u p ;  
         p r o c e d u r e   B t S a i d a C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m A c t i v a t e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m R e s i z e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m C r e a t e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   B t O K C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   R G N i v P l a C t a 1 C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   C G I m p o r t a r C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   R G N i v P l a C t a 2 C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   P C R e g i s t r o C h a n g i n g ( S e n d e r :   T O b j e c t ;   v a r   A l l o w C h a n g e :   B o o l e a n ) ;  
     p r i v a t e  
         {   P r i v a t e   d e c l a r a t i o n s   }  
         f u n c t i o n     I n s e r e I t e m A t u a l _ H 0 1 0 ( B a l I D ,   B a l N u m ,   B a l I t m ,   B a l E n t :   I n t e g e r ;  
                             C O D _ I T E M ,   U N I D ,   I N D _ P R O P ,   C O D _ P A R T ,   T X T _ C O M P L ,   C O D _ C T A :   S t r i n g ;  
                             Q T D ,   V L _ U N I T ,   V L _ I T E M ,   V L _ I T E M _ I R :   D o u b l e ) :   B o o l e a n ;  
         f u n c t i o n     I n s e r e I t e m A t u a l _ K 2 0 0 ( B a l I D ,   B a l N u m ,   B a l I t m ,   B a l E n t :   I n t e g e r ;  
                             D T _ E S T ,   C O D _ I T E M :   S t r i n g ;   v a r   Q T D :   D o u b l e ;   I N D _ E S T ,   C O D _ P A R T :  
                             S t r i n g ;   G r a G r u X ,   G r a n d e z a :   I n t e g e r ;   v a r   P e c a s ,   A r e a M 2 ,   P e s o K g :  
                             D o u b l e ;   E n t i d a d e ,   T i p o _ I t e m :   I n t e g e r ) :  
                             B o o l e a n ;  
         / / f u n c t i o n     I n s u m o s S e m K g ( D a t a B a l :   S t r i n g ) :   B o o l e a n ;  
         f u n c t i o n     E r r U n i d M e d U s o C o n s u m o ( D a t a B a l :   S t r i n g ) :   B o o l e a n ;  
         f u n c t i o n     R e o p e n G r a G r u X ( N i v e l 1 :   I n t e g e r ) :   B o o l e a n ;  
     p u b l i c  
         {   P u b l i c   d e c l a r a t i o n s   }  
         F I m p o r E x p o r ,   F A n o M e s ,   F E m p r e s a ,   F R e g P a i :   I n t e g e r ;  
         F D a t a :   T D a t e T i m e ;  
     e n d ;  
  
     v a r  
     F m S P E D _ E F D _ X 9 9 9 _ B a l a n c o s :   T F m S P E D _ E F D _ X 9 9 9 _ B a l a n c o s ;  
  
 i m p l e m e n t a t i o n  
  
 u s e s   U n M y O b j e c t s ,   M o d u l e ,   U n D m k P r o c F u n c ,   D m k D A C _ P F ,   U M y S Q L M o d u l e ,   U n F i n a n c e i r o ,  
     M o d u l e F i n ,   M o d u l e G e r a l ,   U n V S _ P F ,   M o d P r o d ,   M o d A p p G r a G 1 ,   M y D B C h e c k ;  
  
 { $ R   * . D F M }  
  
 p r o c e d u r e   T F m S P E D _ E F D _ X 9 9 9 _ B a l a n c o s . B t O K C l i c k ( S e n d e r :   T O b j e c t ) ;  
 c o n s t  
     s P r o c N a m e   =   ' F m S P E D _ E F D _ X 9 9 9 _ B a l a n c o s . B t O K C l i c k ( ) ' ;  
     / /  
     f u n c t i o n   A b r e S Q L G r a G r u X ( Q u e r y :   T m y S Q L Q u e r y ;   S Q L _ F l d ,   S Q L _ P S Q :   S t r i n g ) :   B o o l e a n ;  
     c o n s t  
         F l d S d o V r t A r M 2   =   ' v m i . S d o V r t A r M 2 ' ;  
         F l d S d o V r t P e s o   =   ' v m i . S d o V r t P e s o ' ;  
         F l d S d o V r t P e c a   =   ' v m i . S d o V r t P e c a ' ;  
     v a r  
         P a r t e 1 ,   T i p o E s t q ,   A T T _ U n M e d T x t :   S t r i n g ;  
     b e g i n  
         i f   S Q L _ F l d   < >   ' '   t h e n  
             P a r t e 1   : =   S Q L _ F l d  
         e l s e  
         b e g i n  
             T i p o E s t q   : =   V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( F a l s e ,   ' ' ,   F a l s e ,  
             F l d S d o V r t A r M 2 ,   F l d S d o V r t P e s o ,   F l d S d o V r t P e c a ) ;  
             A T T _ U n M e d T x t   : =   d m k P F . A r r a y T o T e x t o ( T i p o E s t q ,   ' U n M e d T x t ' ,   p v P o s ,   F a l s e ,  
                 s G r a n d e z a U n i d M e d ,   2 ) ;  
             / /  
             P a r t e 1   : =   G e r a l . A T S ( [  
             ' S E L E C T   s c c . E n t i S i t i o ,   s c c . S i t P r o d ,   ' ,  
             ' m e d . S i g l a ,   m e d . G r a n d e z a ,   v m i . C o u N i v 2 ,   v m i . G r a G r u Y ,   ' ,  
             ' v m i . G r a g R u X ,   v m i . G r a G r u 1 ,   v m i . E m p r e s a ,   v m i . P e c a s ,   v m i . A r e a M 2 ,   ' ,  
             ' v m i . P e s o K g ,   v m i . V a l o r T ,   g g 1 . U n i d M e d ,   C O N C A T ( g g 1 . N o m e , ' ,  
             ' I F ( g t i . P r i n t T a m = 0 ,   " " ,   C O N C A T ( "   " ,   g t i . N o m e ) ) , ' ,  
             ' I F ( g c c . P r i n t C o r = 0 , " " ,   C O N C A T ( "   " ,   g c c . N o m e ) ) ) ' ,  
             ' N O _ P R D _ T A M _ C O R ,   ' ,  
             A T T _ U n M e d T x t ,  
             ( *  
             ' I F ( ( v m i . G r a G r u Y < 2 0 4 8   O R     ' ,  
             ' v m i . C o u N i v 2 < > 1 )   A N D   v m i . S d o V r t P e s o   >   0 ,   " K g " ,       ' ,  
             ' I F ( v m i . C o u N i v 2 = 1   A N D   v m i . S d o V r t A r M 2   >   0 ,   " m � " ,   " E r r o ! " ) )   U n M e d T x t ,       ' ,  
             * )  
             V S _ C R C _ P F . S Q L _ T i p o E s t q _ D e f i n i r C o d i ( F a l s e ,   ' T i p o E s t q ' ,   T r u e ,  
             F l d S d o V r t A r M 2 ,   F l d S d o V r t P e s o ,   F l d S d o V r t P e c a ) ,  
             ( *  
             ' I F ( ( v m i . G r a G r u Y < 2 0 4 8   O R     ' ,  
             ' v m i . C o u N i v 2 < > 1 )   A N D   v m i . S d o V r t P e s o   >   0 ,   2 ,       ' ,  
             ' I F ( v m i . C o u N i v 2 = 1   A N D   v m i . S d o V r t A r M 2   >   0 ,   1 ,   - 1 ) )   T i p o E s t q ,       ' ,  
             * )  
             ' v m i . S d o V r t P e c a ,   ' ,  
             ' v m i . S d o V r t A r M 2 ,   ' ,  
             ' v m i . S d o V r t P e s o ,   ' ,  
             ' v m i . C l i e n t M O   , ' ,  
             ' I F ( g g 2 . T i p o _ I t e m   > =   0 ,   g g 2 . T i p o _ I t e m ,   ' ,  
             ' p g t . T i p o _ I t e m )   T i p o _ I t e m   ' ,  
             ' ' ] ) ;  
         e n d ;  
         R e s u l t   : =   U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q u e r y ,   D M o d G . M y P I D _ D B ,   [  
         P a r t e 1 ,  
         ' F R O M   _ v s m o v i m p 1 _   v m i     ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 1         g g 1   O N   g g 1 . N i v e l 1 = v m i . G r a G r u 1 ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u 2         g g 2   O N   g g 2 . N i v e l 2 = g g 1 . N i v e l 2 ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . p r d g r u p t i p   p g t   O N   p g t . C o d i g o = g g 1 . P r d G r u p T i p ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . u n i d m e d         m e d   O N   m e d . C o d i g o = g g 1 . U n i d M e d ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x         g g x   O N   g g x . C o n t r o l e = v m i . G r a G r u X ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u c         g g c   O N   g g c . C o n t r o l e = g g x . G r a G r u C ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a c o r c a d     g c c   O N   g c c . C o d i g o = g g c . G r a C o r C a d ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a t a m i t s     g t i   O N   g t i . C o n t r o l e = g g x . G r a T a m I ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . s t q c e n c a d     s c c   O N   s c c . C o d i g o = v m i . S t q C e n C a d ' ,  
         ' L E F T   J O I N   '   +   T M e u D B   +   ' . g r a g r u x c o u   x c o   O N   x c o . G r a G r u X = v m i . G r a G r u X ' ,  
 ( *     I n i c i o   2 0 1 7 - 0 1 - 0 5 * )  
         / / ' W H E R E   v m i . S d o V r t P e c a   >   0     ' ,  
         ' W H E R E   (   ' ,  
         '     v m i . S d o V r t P e c a   >   0       ' ,  
         '     O R     ' ,  
         '     ( g g x . G r a G r u Y < 1 0 2 4   A N D   v m i . P e c a s = 0   A N D   v m i . S d o V r t P e s o   >   0 )   ' ,  
         ' )   ' ,  
 ( *     F i m   2 0 1 7 - 0 1 - 0 5 * )  
         ' A N D   (     ' ,  
         '     ( ( v m i . G r a G r u Y < 2 0 4 8   O R   v m i . C o u N i v 2 < > 1 )   A N D   v m i . S d o V r t P e s o   >   0 )     ' ,  
         '       O R     ' ,  
 / /         '     ( v m i . C o u N i v 2 = 1   A N D   v m i . S d o V r t A r M 2   >   0 )     ' ,  
         '     ( v m i . S d o V r t A r M 2   >   0 )     ' ,  
         ' ) ' ,  
         S Q L _ P S Q ,  
         ' ' ] ) ;  
         / / G e r a l . M B _ S Q L ( S e l f ,   Q u e r y ) ;  
     e n d ;  
 c o n s t  
     D B G 0 0 G r a G r u Y   =   n i l ;  
     D B G 0 0 G r a G r u X   =   n i l ;  
     D B G 0 0 C o u N i v 2   =   n i l ;  
     Q r 0 0 G r a G r u Y     =   n i l ;  
     Q r 0 0 G r a G r u X     =   n i l ;  
     Q r 0 0 C o u N i v 2     =   n i l ;  
     D a t a C o m p r a       =   F a l s e ;  
     E m P r o c e s s o B H   =   T r u e ;  
     G r a C u s P r c         =   0 ;  
     N F e I n i               =   0 ;  
     N F e F i m               =   0 ;  
     U s a S e r i e           =   F a l s e ;  
     S e r i e                 =   0 ;  
     M o v i m C o d           =   0 ;  
     O r i g e m S P E D E F D K n d   =   T O r i g e m S P E D E F D K n d . o s e k E s t o q u e ;  
 v a r  
     D a t a B a l :   T D a t e T i m e ;  
     B a l _ T X T :   S t r i n g ;  
     N o U n i d :   I n t e g e r ;  
  
     / /  
     B a l I D ,   B a l N u m ,   B a l I t m ,   B a l E n t :   I n t e g e r ;  
     C O D _ I T E M ,   U N I D ,   I N D _ P R O P ,   C O D _ P A R T ,   T X T _ C O M P L ,   C O D _ C T A :   S t r i n g ;  
     Q T D ,   V L _ U N I T ,   V L _ I T E M ,   V L _ I T E M _ I R ,   P r e c o :   D o u b l e ;  
 v a r  
     E m p r e s a ,   E n t i d a d e ,   F i l i a l ,   T e r c e i r o ,   O r d e m 1 ,   O r d e m 2 ,   O r d e m 3 ,   O r d e m 4 ,   O r d e m 5 ,  
     A g r u p a ,   S t q C e n C a d ,   Z e r o N e g a t :   I n t e g e r ;  
     T a b l e S r c ,   N O _ E m p r e s a ,   N O _ S t q C e n C a d ,   N O _ T e r c e i r o ,   S Q L _ P S Q :   S t r i n g ;  
     E s t o q u e E m ,   D a t a R e l a t i v a :   T D a t e T i m e ;  
     M o s t r a F r x :   B o o l e a n ;  
     / /  
     U n i d M e d ,   N i v e l 1 ,   U n i d M e d P c ,   U n i d M e d K g ,   U n i d M e d M 2 :   I n t e g e r ;  
     C o r d a :   S t r i n g ;  
     E r r o :   B o o l e a n ;  
     F a t o r ,   M y Q t d ,   M y V l U ,   M y B a s :   D o u b l e ;  
     T i p o :   T G r a n d e z a U n i d M e d ;  
     / /  
     D T _ E S T ,   I N D _ E S T ,   S Q L _ E R R ,   S Q L _ P e r i o d o P Q ,   S Q L _ P e r i o d o V S :   S t r i n g ;  
     D e s c r A g r u N o I t m :   B o o l e a n ;  
     I ,   G r a G r u X ,   G r a n d e z a ,   T i p o _ I t e m ,   L i n A r q :   I n t e g e r ;  
     P e c a s ,   A r e a M 2 ,   P e s o K g ,   Q t d e :   D o u b l e ;  
     / /  
     D i a I n i ,   D i a F i m ,   D i a P o s :   T D a t e T i m e ;  
     O r i B a l I D :   T S P E D _ E F D _ B a l ;  
     S P E D _ E F D _ K 2 7 0 _ 0 8 _ O r i g e m :   T S P E D _ E F D _ K 2 7 0 _ 0 8 _ O r i g e m ;  
     O r i E S T S T a b S o r c :   T E s t q S P E D T a b S o r c ;  
     O r i O r i g e m O p e P r o c :   T O r i g e m O p e P r o c ;  
     O r i O r i g e m I D K n d :   T O r i g e m I D K n d ;  
     T i p o P e r i o d o F i s c a l :   T T i p o P e r i o d o F i s c a l ;  
 b e g i n  
     G e r a l . M B _ I n f o ( ' F a l t a   f a z e r   K 2 8 0   n o   K 2 0 0   P Q ' ) ;  
     / /  
     T i p o P e r i o d o F i s c a l   : =   T T i p o P e r i o d o F i s c a l (  
         S P E D _ E F D _ P F . O b t e m T i p o P e r i o d o R e g i s t r o ( F E m p r e s a ,   ' K 1 0 0 ' ,   s P r o c N a m e ) ) ;  
     i f   ( T i p o P e r i o d o F i s c a l   =   T T i p o P e r i o d o F i s c a l . s p e d p e r I n d e f )   a n d  
     ( P C R e g i s t r o . A c t i v e P a g e I n d e x   =   2 )   t h e n   E x i t ;  
     / /  
     D i a I n i   : =   E n c o d e D a t e ( G e r a l . I M V ( C B A n o . T e x t ) ,   C B M e s . I t e m I n d e x   +   1 ,   1 ) ;  
     D i a F i m   : =   I n c M o n t h ( D i a I n i ,   1 )   - 1 ;  
     D i a P o s   : =   D i a F i m   +   1 ;  
     / /  
     E r r o   : =   F a l s e ;  
     M e A v i s o . L i n e s . C l e a r ;  
     i f   M y O b j e c t s . F I C ( C G I m p o r t a r . V a l u e   =   0 ,   C G I m p o r t a r ,  
         ' S e l e c i o n e   p e l o   m e n o s   u m   t i p o   d e   i t e m   a   s e r   i m p o r t a d o ! ' )   t h e n   E x i t ;  
     i f   n o t   D m P r o d . O b t e m C o d i g o P e c a P c ( U n i d M e d P c )   t h e n   E x i t ;  
     i f   n o t   D m P r o d . O b t e m C o d i g o P e s o K g ( U n i d M e d K g )   t h e n   E x i t ;  
     i f   n o t   D m P r o d . O b t e m C o d i g o A r e a M 2 ( U n i d M e d M 2 )   t h e n   E x i t ;  
     i f   ( U n i d M e d K g   =   0 )   o r   ( U n i d M e d M 2   =   0 )   t h e n   E x i t ;  
     / /   U s o   e   c o n s u m o  
     i f   D m k P F . I n t I n C o n j u n t o 2 ( 1 ,   C G I m p o r t a r . V a l u e )   t h e n  
     b e g i n  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /     B l o c o   H     / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         i f   ( P C R e g i s t r o . A c t i v e P a g e I n d e x   =   1 )   / /   H 0 1 0  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         o r   (  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /     B l o c o   K     / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
             ( P C R e g i s t r o . A c t i v e P a g e I n d e x   =   2 )                             / /   B l o c o   K  
             a n d                                                                                       / /   e  
             ( D m k P F . I n t I n C o n j u n t o 2 ( 1 ,   C G R e g i s t r o s . V a l u e ) )       / /   R e g i s t r o   K 2 0 0  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         )   t h e n  
         b e g i n  
             / /   H 0 1 0  
             c a s e   P C R e g i s t r o . A c t i v e P a g e I n d e x   o f  
                 1 :   i f   M y O b j e c t s . F I C ( E d G e n P l a C t a 1 . V a l u e V a r i a n t   =   0 ,   E d G e n P l a C t a 1 ,  
                       ' I n f o r m e   o   n � v e l   e   o   c � d i g o   d o   p l a n o   d e   c o n t a s ! ' )   t h e n   E x i t ;  
                 2 :   i f   M y O b j e c t s . F I C ( C G R e g i s t r o s . V a l u e   =   0 ,   C G R e g i s t r o s ,  
             ' S e l e c i o n e   p e l o   m e n o s   u m   t i p o   d e   r e g i s t r o   a   s e r   i m p o r t a d o ! ' )   t h e n   E x i t ;  
     / /   N a d a  
                 e l s e   i f   M y O b j e c t s . F I C ( T r u e ,   E d G e n P l a C t a 1 ,  
                       ' T i p o   d e   e s t o q u e   i n d e f i n i d o   p a r a   d e f i n i � � o   d e   n e c e s s i d a d e   d e   c o n t a ! ' )   t h e n   E x i t ;  
             e n d ;  
             c a s e   P C R e g i s t r o . A c t i v e P a g e I n d e x   o f  
                 1 :   D a t a B a l   : =   E n c o d e D a t e ( G e r a l . I M V ( C B A n o . T e x t ) ,   C B M e s . I t e m I n d e x   +   1 ,   1 ) ;  
                 / / 2 :   D a t a B a l   : =   T P D a t a . D a t e   +   1 ;  
                 2 :   D a t a B a l   : =   F D a t a   +   1 ;  
                 e l s e   i f   M y O b j e c t s . F I C ( T r u e ,   E d G e n P l a C t a 1 ,  
                       ' T i p o   d e   e s t o q u e   i n d e f i n i d o   p a r a   d e f i n i � � o   d e   d a t a ! ' )   t h e n   E x i t ;  
             e n d ;  
             / /  
             B a l _ T X T   : =   G e r a l . F D T ( D a t a B a l ,   1 ) ;  
             / /  
             / / i f   I n s u m o s S e m K g ( B a l _ T X T )   t h e n  
             i f   E r r U n i d M e d U s o C o n s u m o ( B a l _ T X T )   t h e n  
                 E x i t ;  
             / /  
             U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( Q r P Q x ,   D m o d . M y D B ,   [  
             ' S E L E C T   p q x . I n s u m o ,   p q x . C l i O r i g , ' ,  
             ' S U M ( p q x . P e s o )   P e s o ,   S U M ( p q x . V a l o r )   V a l o r   ' ,  
             ' F R O M   p q x   p q x ' ,  
             / / ' W H E R E   / * p q x . C l i O r i g = - 1 1   ' ,  
             / / ' A N D * /   p q x . T i p o = 0   ' ,  
             ' W H E R E   p q x . T i p o = 0   ' ,  
             ' A N D   p q x . D a t a X = " '   +   B a l _ T X T   +   ' "   ' ,  
             ' G R O U P   B Y   p q x . I n s u m o ,   p q x . C l i O r i g   ' ,  
             ' ' ] ) ;  
             / / G e r a l . M B _ S Q L ( S e l f ,   Q r P Q x ) ;  
             D T _ E S T     : =   G e r a l . F D T ( F D a t a ,   2 ) ;  
             i f   M y O b j e c t s . F I C ( Q r P Q x . R e c o r d C o u n t   =   0 ,   n i l ,  
             ' N � o   e x i s t e   b a l a n � o   d e   u s o   e   c o n s u m o   n o   d i a   '   +   D T _ E S T )   t h e n   E x i t ;  
             Q r P Q x . F i r s t ;  
             P B 1 . P o s i t i o n   : =   0 ;  
             P B 1 . M a x   : =   Q r P Q x . R e c o r d C o u n t ;  
             Q r P Q x . D i s a b l e C o n t r o l s ;  
             P C R e g i s t r o . V i s i b l e   : =   F a l s e ;  
             t r y  
                 w h i l e   n o t   Q r P Q x . E o f   d o  
                 / /   v e r  
                 / / T i p o _ I t e m   < >   C O _ S P E D _ T I P O _ I T E M _ K 2 0 0  
                 b e g i n  
                     M y O b j e c t s . U p d P B ( P B 1 ,   L a A v i s o 1 ,   L a A v i s o 2 ) ;  
                     / / R e a b r e   Q r G r a G r u X  
                     i f   Q r P Q x P e s o . V a l u e   >   0   t h e n  
                     b e g i n  
                         R e o p e n G r a G r u X ( Q r P Q x I n s u m o . V a l u e ) ;  
                         U n i d M e d   : =   Q r G r a G r u X U n i d M e d . V a l u e ;  
                         i f   U n i d M e d   =   0   t h e n  
                         b e g i n  
                             N i v e l 1     : =   Q r P Q x I n s u m o . V a l u e ;  
                             U n i d M e d   : =   U n i d M e d K G ;  
                             U M y M o d . S Q L I n s U p d ( D m o d . Q r U p d ,   s t U p d ,   ' g r a g r u 1 ' ,   F a l s e ,   [  
                             ' U n i d M e d ' ] ,   [ ' N i v e l 1 ' ] ,   [  
                             U n i d M e d ] ,   [ N i v e l 1 ] ,   T r u e ) ;  
                             / /  
                             R e o p e n G r a G r u X ( Q r P Q x I n s u m o . V a l u e ) ;  
                         e n d ;  
                         / /  
                         B a l I D                     : =   I n t e g e r ( T S P E D _ E F D _ B a l . s e b a l P Q x ) ;  
                         B a l N u m                   : =   F A n o M e s ;  
                         B a l I t m                   : =   Q r P Q x I n s u m o . V a l u e ;  
                         B a l E n t                   : =   Q r P Q x C l i O r i g . V a l u e ;  
                         G r a G r u X                 : =   Q r G r a G r u X C o n t r o l e . V a l u e ;  
                         C O D _ I T E M               : =   G e r a l . F F 0 ( G r a G r u X ) ;  
                         G r a n d e z a               : =   I n t e g e r ( T G r a n d e z a U n i d M e d . g u m P e s o K G ) ;   / /   2  
                         P e c a s                     : =   0 ;  
                         A r e a M 2                   : =   0 ;  
                         P e s o K g                   : =   Q r P Q x P e s o . V a l u e ;  
                         Q T D                         : =   P e s o K g ;  
                         E n t i d a d e               : =   Q r P Q x C l i O r i g . V a l u e ;  
                         T i p o _ I t e m             : =   Q r G r a G r u X T i p o _ I t e m . V a l u e ;  
                         / / C O D _ P A R T               : =   d m k P F . E s c o l h a D e 2 S t r ( F E m p r e s a   =   Q r P Q x C l i O r i g . V a l u e ,   ' ' ,   G e r a l . F F 0 ( Q r P Q x C l i O r i g . V a l u e ) ) ;  
                         C O D _ P A R T               : =   d m k P F . E s c o l h a D e 2 S t r ( F E m p r e s a   =   E n t i d a d e ,   ' ' ,   G e r a l . F F 0 ( E n t i d a d e ) ) ;  
                         i f   P C R e g i s t r o . A c t i v e P a g e I n d e x   =   1   t h e n   / /   H 0 1 0  
                         b e g i n  
                             U N I D                       : =   Q r G r a G r u X S i g l a . V a l u e ;  
                             I N D _ P R O P               : =   d m k P F . E s c o l h a D e 2 S t r ( F E m p r e s a   =   Q r P Q x C l i O r i g . V a l u e ,   ' 0 ' ,   ' 2 ' ) ;  
                             T X T _ C O M P L             : =   ' ' ;  
                             C O D _ C T A                 : =   U F i n a n c e i r o . M o n t a S P E D _ C O D _ C T A ( R G N i v P l a C t a 1 . I t e m I n d e x ,   E d G e n P l a C t a 1 . V a l u e V a r i a n t ) ;  
                             V L _ U N I T                 : =   Q r P Q x V a l o r . V a l u e   /   Q r P Q x P e s o . V a l u e ;  
                             V L _ I T E M                 : =   Q r P Q x V a l o r . V a l u e ;  
                             V L _ I T E M _ I R           : =   Q r P Q x V a l o r . V a l u e ;  
                             / /  
                             I n s e r e I t e m A t u a l _ H 0 1 0 ( B a l I D ,   B a l N u m ,   B a l I t m ,   B a l E n t ,   C O D _ I T E M ,   U N I D ,  
                             I N D _ P R O P ,   C O D _ P A R T ,   T X T _ C O M P L ,   C O D _ C T A ,   Q T D ,   V L _ U N I T ,   V L _ I T E M ,  
                             V L _ I T E M _ I R ) ;  
                         e n d   e l s e  
                         i f   P C R e g i s t r o . A c t i v e P a g e I n d e x   =   2   t h e n   / /   K 2 0 0  
                         b e g i n  
                             D T _ E S T     : =   G e r a l . F D T ( F D a t a ,   1 ) ;  
                             i f   F E m p r e s a   =   Q r P Q x C l i O r i g . V a l u e   t h e n  
                                 I N D _ E S T   : =   ' 0 '  
                             e l s e  
                                 I N D _ E S T   : =   ' 2 ' ;  
                             / /  
                             I n s e r e I t e m A t u a l _ K 2 0 0 ( B a l I D ,   B a l N u m ,   B a l I t m ,   B a l E n t ,   D T _ E S T ,   C O D _ I T E M ,  
                             Q T D ,   I N D _ E S T ,   C O D _ P A R T ,   G r a G r u X ,   G r a n d e z a ,   P e c a s ,   A r e a M 2 ,   P e s o K g ,  
                             E n t i d a d e ,   T i p o _ I t e m ) ;  
                         e n d ;  
                     e n d ;  
                     / /  
                     Q r P Q x . N e x t ;  
                 e n d ;  
             f i n a l l y  
                 P C R e g i s t r o . V i s i b l e   : =   T r u e ;  
                 Q r P Q x . E n a b l e C o n t r o l s ;  
             e n d ;  
             i f   N o U n i d   >   0   t h e n  
                 G e r a l . M B _ A v i s o ( G e r a l . F F 0 ( N o U n i d )   +  
                 '   i n s u m o s   n � o   t e m   d e f i n i d o   a   s i g l a   d a   u n i d a d e   e m   s e u   c a d a s t r o   d e   g r a d e ! '   +  
                 s l i n e B r e a k   +   ' P a r a   e s t e s   i n s u m o s   f o i   c o n s i d e r a d o   a   s i g l a   " K G " ! ' ) ;  
         e n d ;  
         i f   (  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /     B l o c o   K     / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
             ( P C R e g i s t r o . A c t i v e P a g e I n d e x   =   2 )                             / /   B l o c o   K  
             a n d                                                                                       / /   e  
             ( D m k P F . I n t I n C o n j u n t o 2 ( 2 ,   C G R e g i s t r o s . V a l u e ) )       / /   R e g i s t r o   K 2 8 0  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         )   t h e n  
         b e g i n  
             G e r a l . M B _ I n f o ( ' F a l t a   f a z e r   K 2 8 0   n o   K 2 0 0   P Q ' ) ;  
         e n d ;  
     e n d ;  
     P B 1 . P o s i t i o n   : =   0 ;  
     M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   F a l s e ,   ' . . . ' ) ;  
     / /   C o u r o s   V S  
     i f   D m k P F . I n t I n C o n j u n t o 2 ( 2 ,   C G I m p o r t a r . V a l u e )   t h e n  
     b e g i n  
         E m p r e s a                 : =   F E m p r e s a ;  
         F i l i a l                   : =   D M o d G . O b t e m F i l i a l D e E n t i d a d e ( F E m p r e s a ) ;  
         T e r c e i r o               : =   0 ;  
         O r d e m 1                   : =   3 ;   / /   I M E - I  
         O r d e m 2                   : =   0 ;  
         O r d e m 3                   : =   0 ;  
         O r d e m 4                   : =   0 ;  
         O r d e m 5                   : =   0 ;  
         A g r u p a                   : =   0 ;  
         D e s c r A g r u N o I t m   : =   T r u e ;  
         S t q C e n C a d             : =   0 ;  
         Z e r o N e g a t             : =   0 ;   / /   S o m e n t e   p o s i t i v o s !  
         N O _ E m p r e s a           : =   ' ' ;  
         N O _ S t q C e n C a d       : =   ' ' ;  
         N O _ T e r c e i r o         : =   ' ' ;  
         S Q L _ P e r i o d o V S     : =   d m k P F . S Q L _ P e r i o d o ( ' W H E R E   v m i . D a t a H o r a   ' ,   D i a I n i ,   D i a F i m ,   T r u e ,   T r u e ) ;  
         S Q L _ P e r i o d o P Q     : =   d m k P F . S Q L _ P e r i o d o ( ' W H E R E   p q x . D a t a X   ' ,   D i a I n i ,   D i a F i m ,   T r u e ,   T r u e ) ;  
         E s t o q u e E m             : =   I n c M o n t h ( E n c o d e D a t e ( G e r a l . I M V ( C B A n o . T e x t ) ,   C B M e s . I t e m I n d e x   +   1 ,   1 ) ,   1 )   - 1 ;  
         D a t a R e l a t i v a       : =   E s t o q u e E m ;  
         M o s t r a F r x             : =   F a l s e ;  
         / /  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /     B l o c o   H     / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         i f   ( P C R e g i s t r o . A c t i v e P a g e I n d e x   =   1 )   / /   H 0 1 0  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         o r   (  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /     B l o c o   K     / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
             ( P C R e g i s t r o . A c t i v e P a g e I n d e x   =   2 )                             / /   B l o c o   K  
             a n d                                                                                       / /   e  
             ( D m k P F . I n t I n C o n j u n t o 2 ( 1 ,   C G R e g i s t r o s . V a l u e ) )       / /   R e g i s t r o   K 2 0 0  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
         )   t h e n  
         b e g i n  
             i f   V S _ P F . I m p r i m e E s t o q u e E m ( E m p r e s a ,   F i l i a l ,   T e r c e i r o ,   O r d e m 1 ,   O r d e m 2 ,  
             O r d e m 3 ,   O r d e m 4 ,   O r d e m 5 ,   A g r u p a ,   D e s c r A g r u N o I t m ,   S t q C e n C a d ,   Z e r o N e g a t ,  
             N O _ E m p r e s a ,   N O _ S t q C e n C a d ,  
             N O _ T e r c e i r o ,   D a t a R e l a t i v a ,   D a t a C o m p r a ,   E m P r o c e s s o B H ,   D B G 0 0 G r a G r u Y ,  
             D B G 0 0 G r a G r u X ,   D B G 0 0 C o u N i v 2 ,   Q r 0 0 G r a G r u Y ,   Q r 0 0 G r a G r u X ,   Q r 0 0 C o u N i v 2 ,  
             E s t o q u e E m ,   G r a C u s P r c ,  
             N F e I n i ,   N F e F i m ,   U s a S e r i e ,   S e r i e ,   M o v i m C o d ,  
             L a A v i s o 1 ,   L a A v i s o 2 ,   F a l s e )   t h e n  
             b e g i n  
                 / /   G r a G r u 1   S e m   U n i d M e d  
                 M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' V e r i f i c a n d o   u n i d a d e s   d e   m e d i d a !   ( A ) ' ) ;  
                 S Q L _ P S Q   : =   ' A N D   g g 1 . U n i d M e d   <   1   ' ;  
                 A b r e S Q L G r a G r u X ( Q r V M I ,   ' ' ,   S Q L _ P S Q ) ;  
                 Q r V M I . F i r s t ;  
                 w h i l e   n o t   Q r V M I . E o f   d o  
                 b e g i n  
                     N i v e l 1   : =   Q r V M I G r a G r u 1 . V a l u e ;  
                     U n i d M e d   : =   0 ;  
                     c a s e   Q r V M I T i p o E s t q . V a l u e   o f  
                         1 ( * m 2 * ) :   U n i d M e d   : =   U n i d M e d M 2 ;  
                         2 ( * k g * ) :   U n i d M e d   : =   U n i d M e d K g ;  
                         e l s e  
                         b e g i n  
                             G e r a l . M B _ E r r o ( ' I m p o r t a � � o   a b o r t a d a ! '   +   s L i n e B r e a k   +  
                             ' E R R O !   T i p o   d e   m o v i m e n t a � � o   d e   e s t o q u e   d i f e r e n t e   d e   k g   e   m 2 ! '   +  
                             ' T i p o :   '   +   G e r a l . F F 0 ( Q r V M I G r a G r u 1 . V a l u e ) ) ;  
                             E r r o   : =   T r u e ;  
                         e n d ;  
                     e n d ;  
                     / /  
                     i f   U n i d M e d   >   0   t h e n  
                     b e g i n  
                         U M y M o d . S Q L I n s U p d ( D m o d . Q r U p d ,   s t U p d ,   ' g r a g r u 1 ' ,   F a l s e ,   [  
                         ' U n i d M e d ' ] ,   [ ' N i v e l 1 ' ] ,   [  
                         U n i d M e d ] ,   [ N i v e l 1 ] ,   T r u e ) ;  
                     e n d ;  
                     Q r V M I . N e x t ;  
                 e n d ;  
                 i f   E r r o   t h e n   E x i t ;  
                 / /  
                 / /   G r a G r u 1   c o m   U n i d M e d   s e m   r e l a � � o   c o m   p e � a ,   k g   o u   m 2  
                 M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' V e r i f i c a n d o   u n i d a d e s   d e   m e d i d a !   ( B ) ' ) ;  
                 S Q L _ P S Q   : =   ' A N D   N O T   ( m e d . G r a n d e z a   I N   ( 0 , 1 , 2 , 6 , 7 ) )   ' ;  
                 A b r e S Q L G r a G r u X ( Q r V M I ,   ' ' ,   S Q L _ P S Q ) ;  
                 i f   Q r V M I . R e c o r d C o u n t   >   0   t h e n  
                 b e g i n  
                     E r r o   : =   T r u e ;  
                     C o r d a   : =   G e r a l . F F 0 ( Q r V M I G r a G r u 1 . V a l u e ) ;  
                     Q r V M I . N e x t ;  
                     w h i l e   n o t   Q r V M I . E o f   d o  
                     b e g i n  
                         C o r d a   : =   C o r d a   +   ' , '   +   G e r a l . F F 0 ( Q r V M I G r a G r u 1 . V a l u e ) ;  
                         Q r V M I . N e x t ;  
                     e n d ;  
                     G e r a l . M B _ E R R O ( ' I m p o r t a � � o   a b o r t a d a ! '   +   s L i n e B r e a k   +  
                     ' A   u n i d a d e   d e   m e d i d a   d o s   p r o d u t o s   a b a i x o   ( N i v e l   1 )   t e m   c a d a s t r a d o   u n i d a d e s   d e   m e d i d a   i n v � l i d a s ! ' ) ;  
                     C l o s e ;  
                 e n d ;  
                 / /  
                 / /   G r a G r u 2   o u   P r d G r u p T i p   i n v a l i d o s  
                 M y O b j e c t s . I n f o r m a 2 ( L a A v i s o 1 ,   L a A v i s o 2 ,   T r u e ,   ' V e r i f i c a n d o   T i p o _ I t e m !   ' ) ;  
                 S Q L _ P S Q   : =     G e r a l . A T S ( [  
                     ' A N D   N O T   ( ' ,  
                     '     I F ( g g 2 . T i p o _ I t e m   > =   0 ,   g g 2 . T i p o _ I t e m ,   p g t . T i p o _ I t e m ) ' ,  
                     '     I N   ( '   +   C O _ S P E D _ T I P O _ I T E M _ K 2 0 0   +   ' ) ' ,  
                     ' )   ' ] ) ;  
                 A b r e S Q L G r a G r u X ( Q r V M I ,   ' ' ,   S Q L _ P S Q ) ;  
                 / / G e r a l . M B _ S Q L ( S e l f ,   Q r V M I ) ;  
                 i f   Q r V M I . R e c o r d C o u n t   >   0   t h e n  
                 b e g i n  
                     i f   D B C h e c k . E s c o l h e C o d i g o U n i G r i d (  
                     ( * A v i s o , * )   ' . . . ' ,  
                     ( *   T i t u l o , * )   ' I t e n s   c o m   " T i p o   d e   I t e m "   I n d e v i d o ' ,  
                     ( * P r o m p t * ) ' I m p o r t a � � o   a b o r t a d a ! ! !   I t e n s   c o m   T i p o   d e   i t e m   ( T i p o _ I t e m   d o   T i p o   d e   p r o d u t o   o u   n � v e l   2 )   i n d e v i d o ' ,  
                     D s V M I ,  