object FmSPED_EFD_K2XX: TFmSPED_EFD_K2XX
  Left = 339
  Top = 185
  Caption = 'SPE-D_K2X-001 :: Importa'#231#227'o de Movimenta'#231#245'es'
  ClientHeight = 564
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 998
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 950
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 902
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 372
        Height = 32
        Caption = 'Importa'#231#227'o de Movimenta'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 372
        Height = 32
        Caption = 'Importa'#231#227'o de Movimenta'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 372
        Height = 32
        Caption = 'Importa'#231#227'o de Movimenta'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 376
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 998
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 376
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 998
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 529
        Height = 376
        Align = alLeft
        TabOrder = 0
        object PnPeriodo: TPanel
          Left = 2
          Top = 15
          Width = 525
          Height = 42
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LaMes: TLabel
            Left = 48
            Top = 0
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
          end
          object LaAno: TLabel
            Left = 240
            Top = 0
            Width = 22
            Height = 13
            Caption = 'Ano:'
          end
          object LaData: TLabel
            Left = 336
            Top = 0
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object CBMes: TComboBox
            Left = 48
            Top = 17
            Width = 182
            Height = 21
            Color = clWhite
            DropDownCount = 12
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            Text = 'CBMes'
          end
          object CBAno: TComboBox
            Left = 240
            Top = 17
            Width = 90
            Height = 21
            Color = clWhite
            DropDownCount = 3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Text = 'CBAno'
          end
          object TPData: TdmkEditDateTimePicker
            Left = 336
            Top = 16
            Width = 112
            Height = 21
            Date = 42471.359051006950000000
            Time = 42471.359051006950000000
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
        end
        object PCRegistro: TPageControl
          Left = 2
          Top = 57
          Width = 525
          Height = 317
          ActivePage = TabSheet2
          Align = alClient
          TabOrder = 1
          OnChanging = PCRegistroChanging
          object TabSheet1: TTabSheet
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
          end
          object TabSheet2: TTabSheet
            Caption = 'K220 - Outras mov. internas entre mercadorias'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object PageControl1: TPageControl
              Left = 0
              Top = 0
              Width = 517
              Height = 289
              ActivePage = TabSheet13
              Align = alClient
              TabOrder = 0
              object TabSheet4: TTabSheet
                Caption = 'Classe e reclasse unit'#225'ria'
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO1: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 216
                  Align = alClient
                  DataSource = DsCacIts
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet5: TTabSheet
                Caption = 'Classe M'#250'ltipla'
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO2: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 216
                  Align = alClient
                  DataSource = DsClasMul
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet6: TTabSheet
                Caption = 'Reclasse M'#250'ltipla'
                ImageIndex = 2
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO3: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 216
                  Align = alClient
                  DataSource = DsReclMul
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet7: TTabSheet
                Caption = 'Opera'#231#245'es e processos (classes extras)'
                ImageIndex = 3
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO5: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 261
                  Align = alClient
                  DataSource = DsSecO_P
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet13: TTabSheet
                Caption = 'Reclasse direta NFe'
                ImageIndex = 4
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO10: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 261
                  Align = alClient
                  DataSource = DsGGXRcl
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'K230 - ????'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object PageControl2: TPageControl
              Left = 0
              Top = 45
              Width = 517
              Height = 244
              ActivePage = TabSheet12
              Align = alClient
              TabOrder = 0
              object TabSheet8: TTabSheet
                Caption = 'Caleiro'
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO4: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 216
                  Align = alClient
                  DataSource = DsVSCalCab
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet9: TTabSheet
                Caption = 'Curtimento'
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO6: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 216
                  Align = alClient
                  DataSource = DsVSCurCab
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet12: TTabSheet
                Caption = 'Gera'#231#227'o WB'
                ImageIndex = 2
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO9: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 216
                  Align = alClient
                  DataSource = DsVmiGArCab
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet10: TTabSheet
                Caption = 'Opera'#231#245'es'
                ImageIndex = 3
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO7: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 216
                  Align = alClient
                  DataSource = DsVSOpeCab
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
              object TabSheet11: TTabSheet
                Caption = 'Recurtimento / acabamento'
                ImageIndex = 4
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO8: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 216
                  Align = alClient
                  DataSource = DsVSPWECab
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
            end
            object CGImportar: TdmkCheckGroup
              Left = 0
              Top = 0
              Width = 517
              Height = 45
              Align = alTop
              Caption = ' Itens a importar: '
              Columns = 5
              ItemIndex = 2
              Items.Strings = (
                'Caleiro'
                'Curtimento'
                'Gera'#231#227'o WB'
                'Opera'#231#245'es'
                'Recurtimento')
              TabOrder = 1
              UpdType = utYes
              Value = 4
              OldValor = 0
            end
          end
        end
      end
      object MeAviso: TMemo
        Left = 529
        Top = 0
        Width = 479
        Height = 376
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Lines.Strings = (
          'MeAviso')
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 424
    Width = 1008
    Height = 70
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 998
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 994
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
        ExplicitWidth = 994
      end
      object PB2: TProgressBar
        Left = 0
        Top = 36
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 1
        ExplicitWidth = 994
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 494
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 998
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 852
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 850
      object BtOK: TBitBtn
        Tag = 14
        Left = 16
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrIDNiv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MovimID, MovimNiv, COUNT(Controle) IMEIs  '
      'FROM vsmovits '
      ' '
      'GROUP BY MovimID, MovimNiv '
      'ORDER BY MovimID, MovimNiv ')
    Left = 120
    Top = 224
    object QrIDNivMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrIDNivMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
  end
  object QrProdParc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _SPED_EFD_K2XX;'
      ''
      'CREATE TABLE _SPED_EFD_K2XX'
      ''
      'SELECT cac.*, src.GraGruX GGX_Src, dst.GraGruX GGX_Dst '
      'FROM _sped_efd_k2xx_cacits_ cac'
      
        'LEFT JOIN _sped_efd_k2xx_vmi_src_ src ON cac.VMI_Sorc=src.Contro' +
        'le'
      
        'LEFT JOIN _sped_efd_k2xx_vmi_dst_ dst ON cac.VMI_Dest=dst.Contro' +
        'le'
      ';'
      ''
      'SELECT Data, GGX_Src, GGX_Dst, SUM(Pecas) Pecas,'
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2  '
      'FROM _SPED_EFD_K2XX'
      'GROUP BY Data, GGX_Src, GGX_Dst '
      'ORDER BY Data, GGX_Src, GGX_Dst ')
    Left = 692
    Top = 344
    object QrProdParcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrProdParcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrProdParcPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object QrX999: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM efd_h010'
      'WHERE ImporExpor=3'
      'AND AnoMes=201401'
      'AND Empresa=-11'
      'AND H005=1'
      'AND BalID=4'
      'AND BalNum=0'
      'AND BalItm=0')
    Left = 184
    Top = 224
    object QrX999LinArq: TIntegerField
      FieldName = 'LinArq'
    end
  end
  object QrReclMul: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE(DataHora) Data,  GraGruX GGX_Dest, DstGGX GGX_Orig, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, SUM(PesoKg) PesoKg '
      'FROM vsmovits '
      'WHERE DataHora BETWEEN   "2014-01-01" AND "2016-10-31" '
      'AND Empresa = -11 '
      'AND MovimID=14 '
      'AND MovimNiv=2 '
      'GROUP BY Data, GraGruX, DstGGX '
      'ORDER BY Data, GraGruX, DstGGX ')
    Left = 184
    Top = 272
    object QrReclMulData: TDateField
      FieldName = 'Data'
    end
    object QrReclMulGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrReclMulGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrReclMulPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrReclMulAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrReclMulPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object QrClasMul: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE(DataHora) Data,  GraGruX GGX_Dest, DstGGX GGX_Orig, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, SUM(PesoKg) PesoKg '
      'FROM vsmovits '
      'WHERE DataHora BETWEEN   "2014-01-01" AND "2016-10-31" '
      'AND Empresa = -11 '
      'AND MovimID=14 '
      'AND MovimNiv=2 '
      'GROUP BY Data, GraGruX, DstGGX '
      'ORDER BY Data, GraGruX, DstGGX ')
    Left = 256
    Top = 272
    object QrClasMulData: TDateField
      FieldName = 'Data'
    end
    object QrClasMulGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrClasMulGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrClasMulPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrClasMulAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrClasMulPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object DsCacIts: TDataSource
    DataSet = QrProdParc
    Left = 116
    Top = 320
  end
  object DsReclMul: TDataSource
    DataSet = QrReclMul
    Left = 184
    Top = 320
  end
  object DsClasMul: TDataSource
    DataSet = QrClasMul
    Left = 256
    Top = 320
  end
  object QrVSCalCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'IF((ggx.GraGruY<2048 OR '
      'cou.CouNiv2<>1) AND vsx.PesoKgDst <> 0, 1, '
      'IF(cou.CouNiv2=1 AND vsx.AreaDstM2 <> 0, 2, '
      '  IF(vsx.PecasDst=0, 9, -1))) TipoEstq, vsx.* '
      'FROM vscalcab vsx '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2 '
      'WHERE  ( '
      '(DtHrAberto BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      ' OR (DtHrFimOpe BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      
        ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe > "2016-01-31 23:5' +
        '9:59") '
      ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe < "1900-01-01") '
      ') '
      ' '
      'OR MovimCod IN (  '
      '  SELECT MovimCod  '
      '  FROM vsmovits  '
      '  WHERE MovimID=26 '
      '  AND DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59" '
      ')  ')
    Left = 536
    Top = 72
  end
  object DsVSCalCab: TDataSource
    DataSet = QrVSCalCab
    Left = 536
    Top = 120
  end
  object QrVSCurCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'IF((ggx.GraGruY<2048 OR '
      'cou.CouNiv2<>1) AND vsx.PesoKgDst <> 0, 1, '
      'IF(cou.CouNiv2=1 AND vsx.AreaDstM2 <> 0, 2, '
      '  IF(vsx.PecasDst=0, 9, -1))) TipoEstq, vsx.* '
      'FROM vscalcab vsx '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2 '
      'WHERE  ( '
      '(DtHrAberto BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      ' OR (DtHrFimOpe BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      
        ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe > "2016-01-31 23:5' +
        '9:59") '
      ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe < "1900-01-01") '
      ') '
      ' '
      'OR MovimCod IN (  '
      '  SELECT MovimCod  '
      '  FROM vsmovits  '
      '  WHERE MovimID=26 '
      '  AND DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59" '
      ')  ')
    Left = 612
    Top = 72
  end
  object DsVSCurCab: TDataSource
    DataSet = QrVSCurCab
    Left = 612
    Top = 120
  end
  object QrVSOpeCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'IF((ggx.GraGruY<2048 OR '
      'cou.CouNiv2<>1) AND vsx.PesoKgDst <> 0, 1, '
      'IF(cou.CouNiv2=1 AND vsx.AreaDstM2 <> 0, 2, '
      '  IF(vsx.PecasDst=0, 9, -1))) TipoEstq, vsx.* '
      'FROM vscalcab vsx '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2 '
      'WHERE  ( '
      '(DtHrAberto BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      ' OR (DtHrFimOpe BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      
        ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe > "2016-01-31 23:5' +
        '9:59") '
      ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe < "1900-01-01") '
      ') '
      ' '
      'OR MovimCod IN (  '
      '  SELECT MovimCod  '
      '  FROM vsmovits  '
      '  WHERE MovimID=26 '
      '  AND DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59" '
      ')  ')
    Left = 684
    Top = 72
  end
  object DsVSOpeCab: TDataSource
    DataSet = QrVSOpeCab
    Left = 684
    Top = 120
  end
  object QrSecO_P: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _SPED_EFD_K2XX_O_P;'
      'CREATE TABLE _SPED_EFD_K2XX_O_P'
      'SELECT DATE(vmi.DataHora) Data,  cab.GGXDst GGX_Dst, '
      'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, med.Grandeza'
      'FROM bluederm_meza.vsopecab cab'
      
        'LEFT JOIN bluederm_meza.vsmovits vmi ON vmi.MovimCod=cab.MovimCo' +
        'd'
      'LEFT JOIN bluederm_meza.gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm_meza.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm_meza.unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59"'
      'AND cab.GGXDst <> vmi.GragruX'
      'AND MovimNiv=9'
      'UNION'
      'SELECT DATE(vmi.DataHora) Data,  cab.GGXDst GGX_Dst, '
      'vmi.GraGruX GGX_Src, Pecas, AreaM2, PesoKg, med.Grandeza'
      'FROM bluederm_meza.vspwecab cab'
      
        'LEFT JOIN bluederm_meza.vsmovits vmi ON vmi.MovimCod=cab.MovimCo' +
        'd'
      'LEFT JOIN bluederm_meza.gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm_meza.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm_meza.unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59"'
      'AND cab.GGXDst <> vmi.GragruX'
      'AND MovimNiv=22'
      ';'
      'SELECT * FROM _SPED_EFD_K2XX_O_P')
    Left = 696
    Top = 248
    object QrSecO_PData: TDateField
      FieldName = 'Data'
    end
    object QrSecO_PGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrSecO_PGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrSecO_PPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSecO_PAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSecO_PPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrSecO_PGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrSecO_PMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrSecO_PCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSecO_PMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrSecO_PControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsSecO_P: TDataSource
    DataSet = QrSecO_P
    Left = 696
    Top = 296
  end
  object QrVSPWECab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'IF((ggx.GraGruY<2048 OR '
      'cou.CouNiv2<>1) AND vsx.PesoKgDst <> 0, 1, '
      'IF(cou.CouNiv2=1 AND vsx.AreaDstM2 <> 0, 2, '
      '  IF(vsx.PecasDst=0, 9, -1))) TipoEstq, vsx.* '
      'FROM vscalcab vsx '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vsx.GraGruX '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2 '
      'WHERE  ( '
      '(DtHrAberto BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      ' OR (DtHrFimOpe BETWEEN "2016-01-01" AND "2016-01-31 23:59:59") '
      
        ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe > "2016-01-31 23:5' +
        '9:59") '
      ' OR (DtHrAberto < "2016-01-01" AND DtHrFimOpe < "1900-01-01") '
      ') '
      ' '
      'OR MovimCod IN (  '
      '  SELECT MovimCod  '
      '  FROM vsmovits  '
      '  WHERE MovimID=26 '
      '  AND DataHora  BETWEEN "2016-01-01" AND "2016-01-31 23:59:59" '
      ')  ')
    Left = 768
    Top = 72
  end
  object DsVSPWECab: TDataSource
    DataSet = QrVSPWECab
    Left = 768
    Top = 120
  end
  object QrCacIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _SPED_EFD_K2XX;'
      ''
      'CREATE TABLE _SPED_EFD_K2XX'
      ''
      'SELECT cac.*, src.GraGruX GGX_Src, dst.GraGruX GGX_Dst '
      'FROM _sped_efd_k2xx_cacits_ cac'
      
        'LEFT JOIN _sped_efd_k2xx_vmi_src_ src ON cac.VMI_Sorc=src.Contro' +
        'le'
      
        'LEFT JOIN _sped_efd_k2xx_vmi_dst_ dst ON cac.VMI_Dest=dst.Contro' +
        'le'
      ';'
      ''
      'SELECT Data, GGX_Src, GGX_Dst, SUM(Pecas) Pecas,'
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2  '
      'FROM _SPED_EFD_K2XX'
      'GROUP BY Data, GGX_Src, GGX_Dst '
      'ORDER BY Data, GGX_Src, GGX_Dst ')
    Left = 116
    Top = 276
    object QrCacItsData: TDateField
      FieldName = 'Data'
    end
    object QrCacItsGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrCacItsGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrCacItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrCacItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrCacItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrCacItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCacItsVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrCacItsCacID: TIntegerField
      FieldName = 'CacID'
    end
  end
  object QrOri: TmySQLQuery
    Database = Dmod.MyDB
    Left = 848
    Top = 76
  end
  object QrCalToCur: TmySQLQuery
    Database = Dmod.MyDB
    Left = 916
    Top = 72
    object QrCalToCurDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
  end
  object QrVmiGArCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE(DataHora) Data, MovimCod, GraGruX, '
      'Pecas, AreaM2, PesoKg, MovimTwn '
      'FROM vsmovits '
      'WHERE MovimNiv=14 '
      'AND DATE(DataHora) BETWEEN "2016-09-01" AND "2016-09-30" '
      'AND Empresa=-11 '
      'ORDER BY Data, MovimCod, GraGruX ')
    Left = 844
    Top = 260
    object QrVmiGArCabData: TDateField
      FieldName = 'Data'
    end
    object QrVmiGArCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVmiGArCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVmiGArCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVmiGArCabPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVmiGArCabAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVmiGArCabPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVmiGArCabMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVmiGArCabUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrVmiGArCabmed_grandeza: TIntegerField
      FieldName = 'med_grandeza'
    end
    object QrVmiGArCabxco_grandeza: TIntegerField
      FieldName = 'xco_grandeza'
    end
    object QrVmiGArCabGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVmiGArCabCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrVmiGArCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVmiGArCabTipoEstq: TFloatField
      FieldName = 'TipoEstq'
    end
  end
  object DsVmiGArCab: TDataSource
    DataSet = QrVmiGArCab
    Left = 844
    Top = 308
  end
  object QrCurToGAr: TmySQLQuery
    Database = Dmod.MyDB
    Left = 916
    Top = 120
    object QrCurToGArDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrCurToGArSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrCurToGArGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrVmiGArIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE(DataHora) Data, MovimCod, GraGruX, '
      'Pecas, AreaM2, PesoKg, MovimTwn '
      'FROM vsmovits '
      'WHERE MovimNiv=14 '
      'AND DATE(DataHora) BETWEEN "2016-09-01" AND "2016-09-30" '
      'AND Empresa=-11 '
      'ORDER BY Data, MovimCod, GraGruX ')
    Left = 920
    Top = 260
    object QrVmiGArItsData: TDateField
      FieldName = 'Data'
    end
    object QrVmiGArItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVmiGArItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVmiGArItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVmiGArItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVmiGArItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVmiGArItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVmiGArItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVmiGArItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrVmiGArItsmed_grandeza: TIntegerField
      FieldName = 'med_grandeza'
    end
    object QrVmiGArItsxco_grandeza: TIntegerField
      FieldName = 'xco_grandeza'
    end
    object QrVmiGArItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVmiGArItsCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrVmiGArItsTipoEstq: TLargeintField
      FieldName = 'TipoEstq'
    end
    object QrVmiGArItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVmiGArItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVmiGArItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVmiGArItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVmiGArItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
  end
  object DsVmiGArIts: TDataSource
    DataSet = QrVmiGArIts
    Left = 920
    Top = 308
  end
  object QrSumGAR: TmySQLQuery
    Database = Dmod.MyDB
    Left = 844
    Top = 364
    object QrSumGARPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumGARAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumGARPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object QrGGXRcl: TmySQLQuery
    Database = Dmod.MyDB
    Left = 324
    Top = 272
    object QrGGXRclGGX_Src: TIntegerField
      FieldName = 'GGX_Src'
    end
    object QrGGXRclGGX_Dst: TIntegerField
      FieldName = 'GGX_Dst'
    end
    object QrGGXRclGrandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrGGXRclPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrGGXRclAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrGGXRclPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrGGXRclData: TDateField
      FieldName = 'Data'
    end
    object QrGGXRclMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrGGXRclCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGGXRclMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrGGXRclControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsGGXRcl: TDataSource
    DataSet = QrGGXRcl
    Left = 324
    Top = 320
  end
end
