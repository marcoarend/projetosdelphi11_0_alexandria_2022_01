object FmPQwPQEIts: TFmPQwPQEIts
  Left = 339
  Top = 185
  Caption = 'QUI-ENTRA-007 :: Estoque N'#227'o Conforme de Item de NFe de Insumo'
  ClientHeight = 774
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 723
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Estoque N'#227'o Conforme de Item de NFe de Insumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 723
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Estoque N'#227'o Conforme de Item de NFe de Insumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 723
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Estoque N'#227'o Conforme de Item de NFe de Insumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 561
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 561
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1241
        Height = 561
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PageControl1: TPageControl
          Left = 2
          Top = 18
          Width = 1237
          Height = 541
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet1
          Align = alClient
          TabOrder = 0
          object TabSheet1: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Diferen'#231'as'
            object dmkDBGridZTO1: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 1229
              Height = 510
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsPQ_NF_Err
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CliOrig'
                  Title.Caption = 'Empresa'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_EMP'
                  Title.Caption = 'Nome da empresa'
                  Width = 199
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Insumo'
                  Width = 41
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PQ'
                  Title.Caption = 'Nome do insumo'
                  Width = 293
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Tipo'
                  Title.Caption = 'EMID'
                  Width = 34
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'OrigemCodi'
                  Title.Caption = 'IMEC'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'OrigemCtrl'
                  Title.Caption = 'IMEI'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Peso'
                  Title.Caption = 'Peso kg estoque'
                  Width = 92
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SdoPeso'
                  Title.Caption = 'Saldo kg NFs'
                  Width = 116
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 620
    Width = 1241
    Height = 68
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 48
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 27
        Width = 1237
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 688
    Width = 1241
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 1062
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1060
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAcoes: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Baixa'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAcoesClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 167
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtAtualiza: TBitBtn
        Tag = 11
        Left = 320
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Atualiza'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtAtualizaClick
      end
    end
  end
  object frxPQ_NF_Err: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '    '
      'end.')
    Left = 188
    Top = 128
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPQ_NF_Err
        DataSetName = 'frxDsPQ_NF_Err'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 25.653060000000000000
        Top = 211.653680000000000000
        Width = 971.339210000000000000
        object Memo53: TfrxMemoView
          Left = 795.008040000000000000
          Top = 0.094000000000000000
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 56.692940240000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Memo12: TfrxMemoView
          Top = 41.574664020000000000
          Width = 355.275556380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 789.921501500000000000
          Top = 41.574664020000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Estq kg geral')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 880.630221500000000000
          Top = 41.574664020000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Estq kg NFs')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Width = 963.780150000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 975.118740000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 676.535870000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'DIFEREN'#199'AS DE ESTOQUE PARA NFs')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 827.717070000000000000
          Top = 18.897650000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 355.275820000000000000
          Top = 41.574830000000000000
          Width = 275.905655830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente interno')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 631.181510000000000000
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 737.008350000000000000
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Ctrl')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 684.094930000000000000
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Cod')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 136.063080000000000000
        Width = 971.339210000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPQ_NF_Err
        DataSetName = 'frxDsPQ_NF_Err'
        RowCount = 0
        object Memo6: TfrxMemoView
          Left = 41.574795830000000000
          Width = 313.700760550000000000
          Height = 15.118110240000000000
          DataField = 'NO_PQ'
          DataSet = frxDsPQ_NF_Err
          DataSetName = 'frxDsPQ_NF_Err'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQ_NF_Err."NO_PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 789.921506380000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'Peso'
          DataSet = frxDsPQ_NF_Err
          DataSetName = 'frxDsPQ_NF_Err'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQ_NF_Err."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 880.630089690000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'SdoPeso'
          DataSet = frxDsPQ_NF_Err
          DataSetName = 'frxDsPQ_NF_Err'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQ_NF_Err."SdoPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Width = 41.574795830000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsPQ_NF_Err
          DataSetName = 'frxDsPQ_NF_Err'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQ_NF_Err."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 355.275820000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'CliOrig'
          DataSet = frxDsPQ_NF_Err
          DataSetName = 'frxDsPQ_NF_Err'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQ_NF_Err."CliOrig"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 408.189240000000000000
          Width = 222.992235830000000000
          Height = 15.118110240000000000
          DataField = 'NO_EMP'
          DataSet = frxDsPQ_NF_Err
          DataSetName = 'frxDsPQ_NF_Err'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQ_NF_Err."NO_EMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 631.181510000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Tipo'
          DataSet = frxDsPQ_NF_Err
          DataSetName = 'frxDsPQ_NF_Err'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQ_NF_Err."Tipo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 737.008350000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'OrigemCtrl'
          DataSet = frxDsPQ_NF_Err
          DataSetName = 'frxDsPQ_NF_Err'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQ_NF_Err."OrigemCtrl"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 684.094930000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'OrigemCodi'
          DataSet = frxDsPQ_NF_Err
          DataSetName = 'frxDsPQ_NF_Err'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQ_NF_Err."OrigemCodi"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPQ_NF_Err: TfrxDBDataset
    UserName = 'frxDsPQ_NF_Err'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Peso=Peso'
      'NO_PQ=NO_PQ'
      'CliOrig=CliOrig'
      'Insumo=Insumo'
      'OrigemCodi=OrigemCodi'
      'OrigemCtrl=OrigemCtrl'
      'Tipo=Tipo'
      'SdoPeso=SdoPeso'
      'NO_EMP=NO_EMP')
    DataSet = QrPQ_NF_Err
    BCDToCurrency = False
    Left = 188
    Top = 176
  end
  object QrPQ_NF_Err: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPQ_NF_ErrAfterOpen
    BeforeClose = QrPQ_NF_ErrBeforeClose
    SQL.Strings = (
      'DROP TABLE IF EXISTS _pq_compara_estq_nfs_; '
      'CREATE TABLE _pq_compara_estq_nfs_ '
      'SELECT pqx.CliOrig, pqx.Insumo, '
      'OrigemCodi, OrigemCtrl, Tipo, '
      'SUM(pqx.SdoPeso) SdoPeso '
      'FROM bluederm.pqx pqx '
      'WHERE pqx.SdoPeso>0 '
      'GROUP BY pqx.CliOrig, pqx.Insumo; '
      'SELECT pcl.Peso, pq_.Nome NO_PQ, pqx.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMP '
      'FROM _pq_compara_estq_nfs_ pqx'
      'LEFT JOIN bluederm.pqcli pcl ON pcl.CI=pqx.CliOrig '
      'LEFT JOIN bluederm.pq pq_ ON pq_.Codigo=pqx.Insumo'
      'LEFT JOIN bluederm.entidades ent ON ent.Codigo=CliOrig'
      'WHERE pcl.PQ=pqx.Insumo  '
      'AND pcl.Peso <> pqx.SdoPeso ')
    Left = 188
    Top = 224
    object QrPQ_NF_ErrPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPQ_NF_ErrNO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrPQ_NF_ErrCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQ_NF_ErrInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQ_NF_ErrOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQ_NF_ErrOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQ_NF_ErrTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPQ_NF_ErrSdoPeso: TFloatField
      FieldName = 'SdoPeso'
      DisplayFormat = '###,###,###,##0.000'
    end
    object QrPQ_NF_ErrNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrPQ_NF_ErrSdoValr: TFloatField
      FieldName = 'SdoValr'
      DisplayFormat = '###,###,###,##0.00'
    end
    object QrPQ_NF_ErrValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object DsPQ_NF_Err: TDataSource
    DataSet = QrPQ_NF_Err
    Left = 188
    Top = 272
  end
  object Qry: TmySQLQuery
    Database = Dmod.MyDB
    Left = 480
    Top = 264
  end
end
