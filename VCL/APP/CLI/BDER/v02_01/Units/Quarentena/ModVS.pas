unit ModVS;

interface

uses
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables,
  Vcl.StdCtrls, Vcl.ComCtrls,
  AppListas, dmkGeral, UnDmkEnums, UnInternalConsts,
  UnProjGroup_Consts, UnProjGroup_Vars;

type
  TDmModVS = class(TDataModule)
    Qr14GraGruX: TmySQLQuery;
    IntegerField5: TIntegerField;
    IntegerField11: TIntegerField;
    StringField5: TWideStringField;
    StringField11: TWideStringField;
    IntegerField12: TIntegerField;
    StringField12: TWideStringField;
    Ds14GraGruX: TDataSource;
    Qr14Fornecedor: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    Ds14Fornecedor: TDataSource;
    Qr14VSSerFch: TmySQLQuery;
    Qr14VSSerFchCodigo: TIntegerField;
    Qr14VSSerFchNome: TWideStringField;
    Ds14VSSerFch: TDataSource;
    Qr13Sintetico: TmySQLQuery;
    Qr13SinteticoGraGruY: TIntegerField;
    Qr13SinteticoInteiros: TFloatField;
    Qr13SinteticoSerieFch: TIntegerField;
    Qr13SinteticoFicha: TIntegerField;
    Qr13SinteticoIMEI_Src: TIntegerField;
    Qr13SinteticoPallet: TIntegerField;
    Qr13SinteticoAtivo: TSmallintField;
    Qr13SinteticoNO_SERFCH: TWideStringField;
    Qr13SinteticoNO_GGY: TWideStringField;
    Qr13DtHr: TmySQLQuery;
    Qr13DtHrDataHora: TDateTimeField;
    Qr13SinteticoOperacao: TIntegerField;
    Qr16Fatores: TmySQLQuery;
    Ds16Fatores: TDataSource;
    Qr16FatoresDtHrFimCla: TDateTimeField;
    Qr16FatoresCodigoClaRcl: TIntegerField;
    Qr16FatoresOC: TIntegerField;
    Qr16FatoresIDGeracao: TIntegerField;
    Qr16FatoresPallet_Dst: TIntegerField;
    Qr16FatoresFatorIntSrc: TFloatField;
    Qr16FatoresFatorIntDst: TFloatField;
    Qr16FatoresClaRcl: TFloatField;
    Qr16FatoresPallet_Src: TFloatField;
    Qr13VSSerFch: TmySQLQuery;
    Qr13VSSerFchCodigo: TIntegerField;
    Qr13VSSerFchNome: TWideStringField;
    Ds13VSSerFch: TDataSource;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsGraGruY: TIntegerField;
    QrVSMovItsReqMovEstq: TIntegerField;
    QrVSMovItsStqCenLoc: TIntegerField;
    QrVSBalEmp: TmySQLQuery;
    QrVSBalEmpPeriodo: TIntegerField;
    QrUniFrn: TmySQLQuery;
    QrMulFrn: TmySQLQuery;
    QrUniFrnTerceiro: TIntegerField;
    QrUniFrnVSMulFrnCab: TIntegerField;
    QrUniFrnPecas: TFloatField;
    QrLocMFI: TmySQLQuery;
    QrLocMFIControle: TIntegerField;
    QrLocMFC: TmySQLQuery;
    QrLocMFCCodigo: TIntegerField;
    QrMulFrnFornece: TIntegerField;
    QrMulFrnPecas: TFloatField;
    QrMulFrnSiglaVS: TWideStringField;
    QrTotFrn: TmySQLQuery;
    QrTotFrnPecas: TFloatField;
    QrMFDst: TmySQLQuery;
    QrMFSrc: TmySQLQuery;
    QrMFSrcControle: TIntegerField;
    QrMFOrfaos: TmySQLQuery;
    QrMFOrfaosControle: TSmallintField;
    QrMFOrfaosTerceiro: TIntegerField;
    QrMFOrfaosVSMulFrnCab: TIntegerField;
    QrIMEI: TmySQLQuery;
    QrIMEICodigo: TIntegerField;
    QrIMEIControle: TIntegerField;
    QrIMEIMovimCod: TIntegerField;
    QrIMEIMovimNiv: TIntegerField;
    QrIMEIMovimTwn: TIntegerField;
    QrIMEIEmpresa: TIntegerField;
    QrIMEITerceiro: TIntegerField;
    QrIMEICliVenda: TIntegerField;
    QrIMEIMovimID: TIntegerField;
    QrIMEILnkIDXtr: TIntegerField;
    QrIMEILnkNivXtr1: TIntegerField;
    QrIMEILnkNivXtr2: TIntegerField;
    QrIMEIDataHora: TDateTimeField;
    QrIMEIPallet: TIntegerField;
    QrIMEIGraGruX: TIntegerField;
    QrIMEIPecas: TFloatField;
    QrIMEIPesoKg: TFloatField;
    QrIMEIAreaM2: TFloatField;
    QrIMEIAreaP2: TFloatField;
    QrIMEIValorT: TFloatField;
    QrIMEISrcMovID: TIntegerField;
    QrIMEISrcNivel1: TIntegerField;
    QrIMEISrcNivel2: TIntegerField;
    QrIMEISrcGGX: TIntegerField;
    QrIMEISdoVrtPeca: TFloatField;
    QrIMEISdoVrtPeso: TFloatField;
    QrIMEISdoVrtArM2: TFloatField;
    QrIMEIObserv: TWideStringField;
    QrIMEISerieFch: TIntegerField;
    QrIMEIFicha: TIntegerField;
    QrIMEIMisturou: TSmallintField;
    QrIMEIFornecMO: TIntegerField;
    QrIMEICustoMOKg: TFloatField;
    QrIMEICustoMOTot: TFloatField;
    QrIMEIValorMP: TFloatField;
    QrIMEIDstMovID: TIntegerField;
    QrIMEIDstNivel1: TIntegerField;
    QrIMEIDstNivel2: TIntegerField;
    QrIMEIDstGGX: TIntegerField;
    QrIMEIQtdGerPeca: TFloatField;
    QrIMEIQtdGerPeso: TFloatField;
    QrIMEIQtdGerArM2: TFloatField;
    QrIMEIQtdGerArP2: TFloatField;
    QrIMEIQtdAntPeca: TFloatField;
    QrIMEIQtdAntPeso: TFloatField;
    QrIMEIQtdAntArM2: TFloatField;
    QrIMEIQtdAntArP2: TFloatField;
    QrIMEIAptoUso: TSmallintField;
    QrIMEINotaMPAG: TFloatField;
    QrIMEIMarca: TWideStringField;
    QrIMEITpCalcAuto: TIntegerField;
    QrIMEIZerado: TSmallintField;
    QrIMEIEmFluxo: TSmallintField;
    QrIMEINotFluxo: TIntegerField;
    QrIMEIFatNotaVNC: TFloatField;
    QrIMEIFatNotaVRC: TFloatField;
    QrIMEIPedItsLib: TIntegerField;
    QrIMEIPedItsFin: TIntegerField;
    QrIMEIPedItsVda: TIntegerField;
    QrIMEILk: TIntegerField;
    QrIMEIDataCad: TDateField;
    QrIMEIDataAlt: TDateField;
    QrIMEIUserCad: TIntegerField;
    QrIMEIUserAlt: TIntegerField;
    QrIMEIAlterWeb: TSmallintField;
    QrIMEIAtivo: TSmallintField;
    QrIMEIGSPInnNiv2: TIntegerField;
    QrIMEIGSPArtNiv2: TIntegerField;
    QrIMEICustoMOM2: TFloatField;
    QrIMEIReqMovEstq: TIntegerField;
    QrIMEIStqCenLoc: TIntegerField;
    QrIMEIItemNFe: TIntegerField;
    QrIMEIVSMorCab: TIntegerField;
    QrIMEIVSMulFrnCab: TIntegerField;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    TbCouNiv1: TmySQLTable;
    DsCouNiv1: TDataSource;
    TbCouNiv1Codigo: TIntegerField;
    TbCouNiv1Nome: TWideStringField;
    TbCouNiv1FatorInt: TFloatField;
    QrMulFrn2: TmySQLQuery;
    QrMulFrn2SiglaVS: TWideStringField;
    QrMulFrn2FrnCod: TLargeintField;
    QrMulFrn2Pecas: TFloatField;
    QrTotFrn2: TmySQLQuery;
    QrTotFrn2Pecas: TFloatField;
    QrSrcNiv2: TmySQLQuery;
    QrVSProQui: TmySQLQuery;
    QrSCus2: TmySQLQuery;
    QrSCus2MPIn: TIntegerField;
    QrSCus2Custo: TFloatField;
    QrSCus2Pecas: TFloatField;
    QrSCus2Peso: TFloatField;
    QrSCus2MinDta: TDateTimeField;
    QrSCus2MaxDta: TDateTimeField;
    QrSCus2Fuloes: TLargeintField;
    QrSCus2Setor: TIntegerField;
    QrVMI: TmySQLQuery;
    QrVMICodigo: TIntegerField;
    QrVMIControle: TIntegerField;
    QrVMIMovimCod: TIntegerField;
    QrVMIMovimNiv: TIntegerField;
    QrVMIMovimTwn: TIntegerField;
    QrVMIEmpresa: TIntegerField;
    QrVMITerceiro: TIntegerField;
    QrVMICliVenda: TIntegerField;
    QrVMIMovimID: TIntegerField;
    QrVMILnkNivXtr1: TIntegerField;
    QrVMILnkNivXtr2: TIntegerField;
    QrVMIDataHora: TDateTimeField;
    QrVMIPallet: TIntegerField;
    QrVMIGraGruX: TIntegerField;
    QrVMIPecas: TFloatField;
    QrVMIPesoKg: TFloatField;
    QrVMIAreaM2: TFloatField;
    QrVMIAreaP2: TFloatField;
    QrVMIValorT: TFloatField;
    QrVMISrcMovID: TIntegerField;
    QrVMISrcNivel1: TIntegerField;
    QrVMISrcNivel2: TIntegerField;
    QrVMISdoVrtPeca: TFloatField;
    QrVMISdoVrtArM2: TFloatField;
    QrVMIObserv: TWideStringField;
    QrVMIFicha: TIntegerField;
    QrVMIMisturou: TSmallintField;
    QrVMICustoMOKg: TFloatField;
    QrVMICustoMOTot: TFloatField;
    QrVMILk: TIntegerField;
    QrVMIDataCad: TDateField;
    QrVMIDataAlt: TDateField;
    QrVMIUserCad: TIntegerField;
    QrVMIUserAlt: TIntegerField;
    QrVMIAlterWeb: TSmallintField;
    QrVMIAtivo: TSmallintField;
    QrVMISrcGGX: TIntegerField;
    QrVMISdoVrtPeso: TFloatField;
    QrVMISerieFch: TIntegerField;
    QrVMIFornecMO: TIntegerField;
    QrVMIValorMP: TFloatField;
    QrVMIDstMovID: TIntegerField;
    QrVMIDstNivel1: TIntegerField;
    QrVMIDstNivel2: TIntegerField;
    QrVMIDstGGX: TIntegerField;
    QrVMIQtdGerPeca: TFloatField;
    QrVMIQtdGerPeso: TFloatField;
    QrVMIQtdGerArM2: TFloatField;
    QrVMIQtdGerArP2: TFloatField;
    QrVMIQtdAntPeca: TFloatField;
    QrVMIQtdAntPeso: TFloatField;
    QrVMIQtdAntArM2: TFloatField;
    QrVMIQtdAntArP2: TFloatField;
    QrVMIAptoUso: TSmallintField;
    QrVMINotaMPAG: TFloatField;
    QrVMIMarca: TWideStringField;
    QrVMITpCalcAuto: TIntegerField;
    QrVMIZerado: TSmallintField;
    QrVMIEmFluxo: TSmallintField;
    QrVMILnkIDXtr: TIntegerField;
    QrVMICustoMOM2: TFloatField;
    QrVMINotFluxo: TIntegerField;
    QrVMIFatNotaVNC: TFloatField;
    QrVMIFatNotaVRC: TFloatField;
    QrVMIPedItsLib: TIntegerField;
    QrVMIPedItsFin: TIntegerField;
    QrVMIPedItsVda: TIntegerField;
    QrVMIGSPInnNiv2: TIntegerField;
    QrVMIGSPArtNiv2: TIntegerField;
    QrVMIReqMovEstq: TIntegerField;
    QrVMIStqCenLoc: TIntegerField;
    QrVMIItemNFe: TIntegerField;
    QrVMIVSMorCab: TIntegerField;
    QrVMIVSMulFrnCab: TIntegerField;
    QrVSProQuiCodigo: TIntegerField;
    QrVSProQuiControle: TIntegerField;
    QrVSProQuiSetorID: TSmallintField;
    QrVSProQuiMovimCod: TIntegerField;
    QrVSProQuiMovimNiv: TIntegerField;
    QrVSProQuiMovimTwn: TIntegerField;
    QrVSProQuiEmpresa: TIntegerField;
    QrVSProQuiTerceiro: TIntegerField;
    QrVSProQuiMovimID: TIntegerField;
    QrVSProQuiPecas: TFloatField;
    QrVSProQuiPesoKg: TFloatField;
    QrVSProQuiAreaM2: TFloatField;
    QrVSProQuiCusPQ: TFloatField;
    QrVSProQuiFuloes: TIntegerField;
    QrVSProQuiMinDta: TDateField;
    QrVSProQuiMaxDta: TDateField;
    QrVSProQuiEstqPeca: TFloatField;
    QrVSProQuiEstqPeso: TFloatField;
    QrVSProQuiEstqArM2: TFloatField;
    QrVSProQuiLk: TIntegerField;
    QrVSProQuiDataCad: TDateField;
    QrVSProQuiDataAlt: TDateField;
    QrVSProQuiUserCad: TIntegerField;
    QrVSProQuiUserAlt: TIntegerField;
    QrVSProQuiAlterWeb: TSmallintField;
    QrVSProQuiAtivo: TSmallintField;
    QrVSMovItsClientMO: TIntegerField;
    QrSumEmit: TmySQLQuery;
    QrSumEmitCusto: TFloatField;
    QrVMIs: TmySQLQuery;
    QrVMIsControle: TIntegerField;
    QrSumVMI: TmySQLQuery;
    QrVMIsValorMP: TFloatField;
    QrSumEmitPeso: TFloatField;
    QrUniNFe: TmySQLQuery;
    QrUniNFeNFeSer: TSmallintField;
    QrUniNFeNFeNum: TIntegerField;
    QrUniNFeVSMulNFeCab: TIntegerField;
    QrUniNFePecas: TFloatField;
    QrMulNFe: TmySQLQuery;
    QrMulNFeNFeSer: TSmallintField;
    QrMulNFeNFeNum: TIntegerField;
    QrMulNFePecas: TFloatField;
    QrTotNFe: TmySQLQuery;
    QrTotNFePecas: TFloatField;
    QrVSCabNFe: TmySQLQuery;
    QrVSCabNFeSerie: TSmallintField;
    QrVSCabNFenNF: TIntegerField;
    QrMFSrcMovimID: TIntegerField;
    QrMFSrcMovimNiv: TIntegerField;
    QrMFSrcMovimCod: TIntegerField;
    QrVSMovCab: TmySQLQuery;
    QrVMIsQtde: TFloatField;
    QrSumVMIQtde: TFloatField;
    procedure TbCouNiv1AfterEdit(DataSet: TDataSet);
  private
    { Private declarations }
    function  AtualizaVSMovIts_VS_BH(MovimCod: Integer; MovimID: TEstqMovimID): Boolean;
    function  AtualizaVSMovIts_VS_WE(MovimCod: Integer; MovimID: TEstqMovimID): Boolean;
    procedure PreparaPalletParaReclass_InsereBaixaAtual(Codigo, MovimCod:
              Integer; DataHora: String; NewVMI: Integer; SQLType: TSQLType);
    procedure PreparaPalletParaReclass_InsereNovoVMI_DoPallet(Pallet, Controle,
              Codigo, MovimCod: Integer; DataHora: String; Pecas, PesoKg,
              AreaM2, AreaP2: Double; SerieFch, Ficha, Terceiro, VSMulFrnCab,
              FornecMO: Integer; Marca: String; CustoMOKg, CustoMOTot, ValorMP,
              ValorT: Double; Empresa, ClientMO, GraGruX, GraGruY, TpCalcAuto,
              PedItsLib, PedItsFin, PedItsVda, GSPInnNiv2, GSPArtNiv2,
              ReqMovEstq, StqCenLoc: Integer; SQLType: TSQLType);
  public
    { Public declarations }
    procedure Atualiza13SinteticoDtHr(Empresa: Integer; VSFluxIncon: String;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel);
    procedure AtualizaVSMulNFeCab(SinalOrig: TSinal;
              DefMulNFe: TEstqDefMulFldVS; MovTypeCod: Integer;
              MovimID: TEstqMovimID; MovimNivsOri,
              MovimNivsDst: array of TEstqMovimNiv);
    procedure AtualizaVSMulFrnCabNew(SinalOrig: TSinal; DefMulFrn: TEstqDefMulFldVS;
              MovTypeCod: Integer; MovimIDs: array of TEstqMovimID;
              MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv);
(*
    procedure AtualizaVSMulFrnCabOld(SinalOrig: TSinal; DefMulFrn: TEstqDefMulFldVS;
              MovTypeCod: Integer; MovimIDs: array of TEstqMovimID;
              MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv);
*)
    procedure DefinePeriodoBalVSEmpresaLogada();
    function  ObtemFornecedores_Pallet(Pallet: Integer): String;
    function  PreparaPalletParaReclassificar(const SQLType: TSQLType; const
              CkReclasse_Checked: Boolean; var Empresa, ClientMO, Codigo,
              MovimCod, Controle, Pallet, GraGruX, GraGruY: Integer;
              DtHr: TDateTime): Boolean;
    procedure Reopen16Fatores(TP16DataIni_Date, TP16DataFim_Date:
              TDateTime; Ck16DataIni_Checked, Ck16DataFim_Checked: Boolean);
    function  AtualizaVSMovIts_CusEmit(Controle: Integer): Boolean;
    function  AtualizaVSMovIts_VS_BHWE(MovimCod: Integer): Boolean;

  end;

var
  DmModVS: TDmModVS;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

{ TDmModVS }

uses
  DmkDAC_PF, ModuleGeral, UnMyObjects, UnVS_PF, UMySQLModule, Module,
  UnDmkProcFunc, StqCenLoc, GetValor, ModProd, GraGruX;

procedure TDmModVS.Atualiza13SinteticoDtHr(Empresa: Integer; VSFluxIncon: String;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel);
var
  LastDtHr, BalDtHr: String;
  SerieFch, Ficha, IMEI_Src, Pallet, Operacao: Integer;
  //
  procedure DefineLastDtHr();
  begin
(*
    if VS_PF.ImpedePeloBalanco(Qr13DtHrDataHora.Value, False) then
      LastDtHr := BalDtHr
    else
      LastDtHr := Geral.FDT(Qr13DtHrDataHora.Value, 109);
*)
    LastDtHr := Geral.FDT(Qr13DtHrDataHora.Value, 109);
  end;
begin
  LastDtHr := '';
  if VS_PF.ImpedePeloBalanco(Now(), False) then
    BalDtHr := Geral.FDT(VS_PF.ObtemDataBalanco(Empresa), 109)
  else
    BalDtHr := Geral.FDT(Now(), 109);
  UnDmkDAC_PF.AbreMySQLQuery0(Qr13Sintetico, DModG.MyPID_DB, [
  'SELECT vfi.*, vsf.Nome NO_SERFCH, ggy.Nome NO_GGY  ',
  'FROM ' + VSFluxIncon + ' vfi ',
  'LEFT JOIN bluederm.gragruy ggy ON ggy.Codigo=vfi.GraGruY ',
  'LEFT JOIN bluederm.vsserfch vsf ON vsf.Codigo=vfi.SerieFch ',
  'ORDER BY vfi.GraGruY, vfi.SerieFch,  ',
  'vfi.Ficha, vfi.IMEI_Src, vfi.Pallet ',
  '']);
  PB1.Position := 0;
  PB1.Max := Qr13Sintetico.RecordCount;
  Qr13Sintetico.First;
  while not Qr13Sintetico.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    case Qr13SinteticoGraGruY.Value of
      1024:
      begin
        SerieFch := Qr13SinteticoSerieFch.Value;
        Ficha    := Qr13SinteticoFicha.Value;
        if (SerieFch <> 0) and (Ficha <> 0) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qr13DtHr, Dmod.MyDB, [
          'SELECT MAX(DataHora) DataHora ',
          'FROM ' + CO_TAB_VMI + ' ',
          'WHERE SrcNivel2 IN ',
          '( ',
          '  SELECT Controle ',
          '  FROM ' + CO_TAB_VMI + ' ',
          '  WHERE SerieFch=' + Geral.FF0(SerieFch),
          '  AND Ficha=' + Geral.FF0(Ficha),
          '  AND MovimID=1 ',
          ') ',
          ' ']);
          //
          DefineLastDtHr();
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, VSFluxIncon, False, [
          'LastDtHr'], ['SerieFch', 'Ficha'], [
          LastDtHr], [SerieFch, Ficha], False);
        end;
      end;
      2048:
      begin
        IMEI_Src := Qr13SinteticoIMEI_Src.Value;
        if IMEI_Src <> 0 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qr13DtHr, Dmod.MyDB, [
          'SELECT MAX(DataHora) DataHora ',
          'FROM ' + CO_TAB_VMI + ' ',
          'WHERE SrcNivel2=' + Geral.FF0(IMEI_Src),
          '']);
          DefineLastDtHr();
          //
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, VSFluxIncon, False, [
          'LastDtHr'], ['IMEI_Src'], [
          LastDtHr], [IMEI_Src], False);
        end;
      end;
      3072:
      begin
        Pallet := Qr13SinteticoPallet.Value;
        if Pallet <> 0 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qr13DtHr, Dmod.MyDB, [
          'SELECT MAX(DataHora) DataHora  ',
          'FROM ' + CO_TAB_VMI + '  ',
          'WHERE SrcNivel2 IN  ',
          '(  ',
          '  SELECT Controle  ',
          '  FROM ' + CO_TAB_VMI + '  ',
          '  WHERE Pallet=' + Geral.FF0(Pallet),
          '  AND Pecas>0 ',
          ')  ',
          '']);
          DefineLastDtHr();
          //
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, VSFluxIncon, False, [
          'LastDtHr'], ['Pallet'], [
          LastDtHr], [Pallet], False);
        end;
      end;
      4096:
      begin
        Operacao := Qr13SinteticoOperacao.Value;
        if (Operacao <> 0) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qr13DtHr, Dmod.MyDB, [
          'SELECT MAX(DataHora) DataHora ',
          'FROM ' + CO_TAB_VMI + ' ',
(*
          'WHERE SrcNivel2 IN ',
          '( ',
          '  SELECT Controle ',
          '  FROM ' + CO_TAB_VMI + ' ',
*)
          '  WHERE Codigo=' + Geral.FF0(Operacao),
          '  AND MovimID=11 ',
(*
          '  AND MovimNiv=8 ',
          ') ',
*)
          ' ']);
          //
          DefineLastDtHr();
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, VSFluxIncon, False, [
          'LastDtHr'], ['Operacao'], [
          LastDtHr], [Operacao], False);
        end;
      end;
      else
      begin
        Geral.MB_Erro('"GraGruY" n�o implementado!');
        LastDtHr := '';
      end;
    end;
    Qr13Sintetico.Next;
  end;
end;

{
function TDmModVS.AtualizaVSMovIts_CusEmit(Controle: Integer): Boolean;
const
  TipoStqMovIts = VAR_FATID_0104;
var
  Qtde, Pecas, Peso, AreaM2, AreaP2: Double;
  EstqPC, EstqM2, EstqP2, EstqKg: Double;
  Encerrado: Integer;
//var
  PecasCal, PecasCur, PecasNeg,
  PesoCal, PesoCur, CusPQCal, CusPQCur, CustoPQ: Double;
  MinDtaCal, MinDtaCur, MinDtaEnx,
  MaxDtaCal, MaxDtaCur, MaxDtaEnx: String;
  FuloesCal, FuloesCur: Integer;
(*
var
  MinDtaCal, MinDtaCur, MinDtaRec, MinDtaEnx, MaxDtaCal, MaxDtaCur, MaxDtaRec, MaxDtaEnx: String;
  Controle, FuloesCal, FuloesCur, FuloesRec, Encerrado: Integer;
  PecasCal, PecasCur, PecasRec, PecasExp, M2Exp, P2Exp, PesoExp, PecasNeg, PesoCal, PesoCur, PesoRec, CusPQCal, CusPQCur, CusPQRec, Estq_Pecas, Estq_M2, Estq_P2, Estq_Peso: Double;
  SQLType: TSQLType;
*)begin
  //Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSProQui, Dmod.MyDB, [
  'SELECT *  ',
  'FROM vsproqui ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
  'SELECT *  ',
  'FROM ' + CO_TAB_VMI + ' ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
(*
  /
    //
  QrSumCus.Close;
  QrSumCus.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrSumCus, MyDB);
SELECT SUM(Custo) Custo
FROM emitcus
WHERE MPIn=:P0
  //
  QrSPerdas.Close;
  QrSPerdas.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrSPerdas, MyDB);
  PecasNeg := QrSPerdasPecas.Value;
  //
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrSCus2, Dmod.MyDB, [
  'SELECT emc.MPIn, SUM(emc.Custo) Custo,',
  'SUM(emc.Pecas) Pecas, SUM(emc.Peso) Peso,',
  'MIN(emc.DataEmis) MinDta, MAX(emc.DataEmis) MaxDta,',
  'COUNT(emc.MPIn) Fuloes, rec.Setor',
  'FROM emitcus emc',
  'LEFT JOIN formulas rec ON rec.Numero=emc.Formula',
  'WHERE emc.VSMovIts=' + Geral.FF0(Controle),
  'GROUP BY rec.Setor',
  '']);
  //
  PecasCal  := 0;
  PecasCur  := 0;
  PesoCal   := 0;
  PesoCur   := 0;
  CusPQCal  := 0;
  CusPQCur  := 0;
  FuloesCal := 0;
  FuloesCur := 0;
  MinDtaCal := '0000-00-00';
  MinDtaCur := '0000-00-00';
  MinDtaEnx := '0000-00-00';
  MaxDtaCal := '0000-00-00';
  MaxDtaCur := '0000-00-00';
  MaxDtaEnx := '0000-00-00';
  if TEstqMovimID(QrVMIMovimID.Value) = emidCompra then
  begin
    while not QrSCus2.Eof do
    begin
      if QrSCus2Setor.Value = Dmod.QrControleSetorCal.Value then
      begin
        PecasCal  := PecasCal  + QrSCus2Pecas.Value;
        PesoCal   := PesoCal   + QrSCus2Peso.Value;
        CusPQCal  := CusPQCal  + QrSCus2Custo.Value;
        FuloesCal := FuloesCal + QrSCus2Fuloes.Value;
        MinDtaCal := Geral.FDT(QrSCus2MinDta.Value, 1);
        MaxDtaCal := Geral.FDT(QrSCus2MaxDta.Value, 1);
      end else
      if QrSCus2Setor.Value = Dmod.QrControleSetorCur.Value then
      begin
        PecasCur  := PecasCur  + QrSCus2Pecas.Value;
        PesoCur   := PesoCur   + QrSCus2Peso.Value;
        CusPQCur  := CusPQCur  + QrSCus2Custo.Value;
        FuloesCur := FuloesCur + QrSCus2Fuloes.Value;
        MinDtaCur := Geral.FDT(QrSCus2MinDta.Value, 1);
        MaxDtaCur := Geral.FDT(QrSCus2MaxDta.Value, 1);
      end
      else Geral.MB_Aviso(
      'Setor inv�lido para caleiro ou curtimento na defini��o estoque de couros na emiss�o de pesagem!');
      //
      QrSCus2.Next;
    end;
  end;
  //
  EstqPC := 0;
  EstqM2 := 0;
  EstqP2 := 0;
  EstqKg := 0;
  //
(*
  DmProd.ObtemQuantidadesBaixa(TipoStqMovIts, Controle, Qtde, Pecas, Peso, AreaM2, AreaP2);
*)
{
  SQLType        := ImgTipo.SQLType?;
  Controle       := ;
  PecasCal       := ;
  PecasCur       := ;
  PecasRec       := ;
  PecasExp       := ;
  M2Exp          := ;
  P2Exp          := ;
  PesoExp        := ;
  PecasNeg       := ;
  PesoCal        := ;
  PesoCur        := ;
  PesoRec        := ;
  CusPQCal       := ;
  CusPQCur       := ;
  CusPQRec       := ;
  FuloesCal      := ;
  FuloesCur      := ;
  FuloesRec      := ;
  MinDtaCal      := ;
  MinDtaCur      := ;
  MinDtaRec      := ;
  MinDtaEnx      := ;
  MaxDtaCal      := ;
  MaxDtaCur      := ;
  MaxDtaRec      := ;
  MaxDtaEnx      := ;
  Estq_Pecas     := ;
  Estq_M2        := ;
  Estq_P2        := ;
  Estq_Peso      := ;
  Encerrado      := ;
  /
  Encerrado := QrMPInForcaEncer.Value;
  if Encerrado = 0  then
    if (Pecas + PecasNeg >=  QrMPInPecas.Value)
    and (QrMPInPecas.Value > 0) then
      Encerrado := 1;
  //
  if (Encerrado = 0) then
  begin
    EstqPC := QrMPInPecas.Value - QrMPInPecasNeg.Value - Pecas;
    if EstqPC > 0 then
    begin
      EstqM2 := QrMPInM2.Value - AreaM2;
      if EstqM2 < 0 then EstqM2 := 0;
      //
      EstqP2 := QrMPInP2.Value - AreaP2;
      if EstqP2 < 0 then EstqP2 := 0;
      //
      EstqKg := QrMPInPLE.Value - Peso;
      if EstqKg < 0 then EstqKg := 0;
      //
    end else EstqPC := 0;
  end;
  //
  CustoPQ := CusPQCal + CusPQCur;
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'vsproqui', auto_increment?[
'PecasCal', 'PecasCur', 'PecasRec',
'PecasExp', 'M2Exp', 'P2Exp',
'PesoExp', 'PecasNeg', 'PesoCal',
'PesoCur', 'PesoRec', 'CusPQCal',
'CusPQCur', 'CusPQRec', 'FuloesCal',
'FuloesCur', 'FuloesRec', 'MinDtaCal',
'MinDtaCur', 'MinDtaRec', 'MinDtaEnx',
'MaxDtaCal', 'MaxDtaCur', 'MaxDtaRec',
'MaxDtaEnx', 'Estq_Pecas', 'Estq_M2',
'Estq_P2', 'Estq_Peso', 'Encerrado'], [
'Controle'], [
PecasCal, PecasCur, PecasRec,
PecasExp, M2Exp, P2Exp,
PesoExp, PecasNeg, PesoCal,
PesoCur, PesoRec, CusPQCal,
CusPQCur, CusPQRec, FuloesCal,
FuloesCur, FuloesRec, MinDtaCal,
MinDtaCur, MinDtaRec, MinDtaEnx,
MaxDtaCal, MaxDtaCur, MaxDtaRec,
MaxDtaEnx, Estq_Pecas, Estq_M2,
Estq_P2, Estq_Peso, Encerrado], [
Controle], UserDataAlterweb?, IGNORE?

  UMyMod.SQLInsUpd(QrUpd, stUpd, 'mpin', False, [
    'PecasCal', 'PecasCur', 'PecasNeg',
    'PesoCal', 'PesoCur', 'CusPQCal', 'CusPQCur',
    'MinDtaCal', 'MinDtaCur', 'MinDtaEnx',
    'MaxDtaCal', 'MaxDtaCur', 'MaxDtaEnx',
    'FuloesCal', 'FuloesCur',
    'PecasExp', 'M2Exp', 'P2Exp', 'PesoExp',
    'Estq_Pecas', 'Estq_M2', 'Estq_P2', 'Estq_Peso',
    'CustoPQ', 'Encerrado'
  ], ['Controle'], [
    PecasCal, PecasCur, PecasNeg,
    PesoCal, PesoCur, CusPQCal, CusPQCur,
    MinDtaCal, MinDtaCur, MinDtaEnx,
    MaxDtaCal, MaxDtaCur, MaxDtaEnx,
    FuloesCal, FuloesCur,
    Pecas, AreaM2, AreaP2, Peso,
    EstqPC, EstqM2, EstqP2, EstqKg,
    CustoPQ, Encerrado
  ], [Controle], True);
  //
  Result := True;
end;
}

function TDmModVS.AtualizaVSMovIts_CusEmit(Controle: Integer): Boolean;
var
  MinDta, MaxDta: String;
  //Controle,
  Codigo, MovimCod, MovimNiv, MovimTwn, Empresa, Terceiro, MovimID, SetorID,
  FuloesCal, FuloesCur, FuloesRec, Fuloes: Integer;
  Pecas, PesoKg, AreaM2, CusPQ, EstqPeca, EstqPeso, EstqArM2: Double;
  SQLType: TSQLType;
  //
  MinDtaCal, MaxDtaCal, MinDtaCur, MaxDtaCur, MinDtaRec, MaxDtaRec: String;
  PecasCal, PesoKgCal, CusPQCal: Double;
  PecasCur, PesoKgCur, CusPQCur: Double;
  PecasRec, PesoKgRec, CusPQRec: Double;
  //
  AreaM2Cal, AreaM2Cur, AreaM2Rec: Double;
  //
  function ObtemSQLType(SetorX: Integer): TSQLType;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSProQui, Dmod.MyDB, [
    'SELECT *  ',
    'FROM vsproqui ',
    'WHERE Controle=' + Geral.FF0(Controle),
    'AND SetorID=' + Geral.FF0(SetorX),
    '']);
    if QrVSProQui.RecordCount > 0 then
      Result := stUpd
    else
      Result := stIns;
  end;
begin
  //Result := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
  'SELECT *  ',
  'FROM ' + CO_TAB_VMI + ' ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSCus2, Dmod.MyDB, [
  'SELECT emc.MPIn, SUM(emc.Custo) Custo,',
  'SUM(emc.Pecas) Pecas, SUM(emc.Peso) Peso,',
  'MIN(emc.DataEmis) MinDta, MAX(emc.DataEmis) MaxDta,',
  'COUNT(emc.MPIn) Fuloes, rec.Setor',
  'FROM emitcus emc',
  'LEFT JOIN formulas rec ON rec.Numero=emc.Formula',
  'WHERE emc.VSMovIts=' + Geral.FF0(Controle),
  'GROUP BY rec.Setor',
  '']);
  //
  //Controle       := ;
  Codigo         := QrVMICodigo.Value;
  MovimCod       := QrVMIMovimCod.Value;
  MovimNiv       := QrVMIMovimNiv.Value;
  MovimTwn       := QrVMIMovimTwn.Value;
  Empresa        := QrVMIEmpresa.Value;
  Terceiro       := QrVMITerceiro.Value;
  MovimID        := QrVMIMovimID.Value;
  SetorID        := 0;
  Pecas          := 0.000;
  PesoKg         := 0.000;
  AreaM2         := 0.00;
  CusPQ          := 0.00;
  Fuloes         := 0;
  MinDtaCal      := '0000-00-00';
  MaxDtaCal      := '0000-00-00';
  MinDtaCur      := '0000-00-00';
  MaxDtaCur      := '0000-00-00';
  MinDta         := '0000-00-00';
  MaxDta         := '0000-00-00';
  EstqPeca       := 0.000;
  EstqPeso       := 0.000;
  EstqArM2       := 0.00;
  //
  if TEstqMovimID(QrVMIMovimID.Value) = emidCompra then
  begin
    while not QrSCus2.Eof do
    begin
      if QrSCus2Setor.Value = Dmod.QrControleSetorCal.Value then
      begin
        //SetorID   := Integer(TEstqSetorID.esiCaleiro);
        PecasCal  := PecasCal  + QrSCus2Pecas.Value;
        PesoKgCal := PesoKgCal + QrSCus2Peso.Value;
        CusPQCal  := CusPQCal  + QrSCus2Custo.Value;
        FuloesCal := FuloesCal + QrSCus2Fuloes.Value;
        MinDtaCal := Geral.FDT(QrSCus2MinDta.Value, 1);
        MaxDtaCal := Geral.FDT(QrSCus2MaxDta.Value, 1);
        AreaM2Cal := 0;
      end else
      if QrSCus2Setor.Value = Dmod.QrControleSetorCur.Value then
      begin
        //SetorID   := Integer(TEstqSetorID.esiCurtimento);
        PecasCur  := PecasCur  + QrSCus2Pecas.Value;
        PesoKgCur := PesoKgCur + QrSCus2Peso.Value;
        CusPQCur  := CusPQCur  + QrSCus2Custo.Value;
        FuloesCur := FuloesCur + QrSCus2Fuloes.Value;
        MinDtaCur := Geral.FDT(QrSCus2MinDta.Value, 1);
        MaxDtaCur := Geral.FDT(QrSCus2MaxDta.Value, 1);
        AreaM2Cur := 0;
      end
      else Geral.MB_Aviso(
      'Setor inv�lido para caleiro ou curtimento na defini��o estoque de couros na emiss�o de pesagem!');
      //
      QrSCus2.Next;
    end;
    //
    //////// CALEIRO /////////////////////////////////////////////////////////////
    //
    SetorID  := Integer(TEstqSetorID.esiCaleiro);
    SQLType  := ObtemSQLType(SetorID);
    //
    //
    Pecas    := PecasCal;
    PesoKg   := PesoKgCal;
    AreaM2   := AreaM2Cal;
    CusPQ    := CusPQCal;
    Fuloes   := FuloesCal;
    MinDta   := MinDtaCal;
    MaxDta   := MaxDtaCal;
    EstqPeca := QrVMIPecas.Value  - Pecas;
    EstqPeso := QrVMIPesoKg.Value - PesoKg;
    EstqArM2 := QrVMIAreaM2.Value - AreaM2;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsproqui', False, [
    'Codigo', 'MovimCod', 'MovimNiv',
    'MovimTwn', 'Empresa', 'Terceiro',
    'MovimID', 'Pecas',
    'PesoKg', 'AreaM2', 'CusPQ',
    'Fuloes', 'MinDta', 'MaxDta',
    'EstqPeca', 'EstqPeso', 'EstqArM2'], [
    'Controle', 'SetorID'], [
    Codigo, MovimCod, MovimNiv,
    MovimTwn, Empresa, Terceiro,
    MovimID, Pecas,
    PesoKg, AreaM2, CusPQ,
    Fuloes, MinDta, MaxDta,
    EstqPeca, EstqPeso, EstqArM2], [
    Controle, SetorID], True);
    //
    //////// CURTIMENTO //////////////////////////////////////////////////////////
    //
    SetorID  := Integer(TEstqSetorID.esiCurtimento);
    SQLType  := ObtemSQLType(SetorID);
    Pecas    := PecasCur;
    PesoKg   := PesoKgCur;
    AreaM2   := AreaM2Cur;
    CusPQ    := CusPQCur;
    Fuloes   := FuloesCur;
    MinDta   := MinDtaCur;
    MaxDta   := MaxDtaCur;
    EstqPeca := QrVMIPecas.Value  - Pecas;
    EstqPeso := QrVMIPesoKg.Value - PesoKg;
    EstqArM2 := QrVMIAreaM2.Value - AreaM2;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsproqui', False, [
    'Codigo', 'MovimCod', 'MovimNiv',
    'MovimTwn', 'Empresa', 'Terceiro',
    'MovimID', 'Pecas',
    'PesoKg', 'AreaM2', 'CusPQ',
    'Fuloes', 'MinDta', 'MaxDta',
    'EstqPeca', 'EstqPeso', 'EstqArM2'], [
    'Controle', 'SetorID'], [
    Codigo, MovimCod, MovimNiv,
    MovimTwn, Empresa, Terceiro,
    MovimID, Pecas,
    PesoKg, AreaM2, CusPQ,
    Fuloes, MinDta, MaxDta,
    EstqPeca, EstqPeso, EstqArM2], [
    Controle, SetorID], True) then
      ; // Nao!!!!
      //VS_PF.AtualizaVSValorT(Controle);
    //
  end else
  if TEstqMovimID(QrVMIMovimID.Value) = emidEmProcWE then
  begin
    while not QrSCus2.Eof do
    begin
      //if QrSCus2Setor.Value = Dmod.QrControleSetorRec.Value then
      begin
        //SetorID   := Integer(TEstqSetorID.esiRecurtimento);
        PecasRec  := PecasRec  + QrSCus2Pecas.Value;
        PesoKgRec := PesoKgRec + QrSCus2Peso.Value;
        CusPQRec  := CusPQRec  + QrSCus2Custo.Value;
        FuloesRec := FuloesRec + QrSCus2Fuloes.Value;
        MinDtaRec := Geral.FDT(QrSCus2MinDta.Value, 1);
        MaxDtaRec := Geral.FDT(QrSCus2MaxDta.Value, 1);
        AreaM2Rec := 0;
      end;
      //else Geral.MB_Aviso(
      //'Setor inv�lido para caleiro ou curtimento na defini��o estoque de couros na emiss�o de pesagem!');
      //
      QrSCus2.Next;
    end;
    //////// RECURTIMENTO ////////////////////////////////////////////////////////
    //
    SetorID  := Integer(TEstqSetorID.esiRecurtimento);
    SQLType  := ObtemSQLType(SetorID);
    //
    Pecas    := PecasRec;
    PesoKg   := PesoKgRec;
    AreaM2   := AreaM2Rec;
    CusPQ    := CusPQRec;
    Fuloes   := FuloesRec;
    MinDta   := MinDtaRec;
    MaxDta   := MaxDtaRec;
    EstqPeca := QrVMIPecas.Value  - Pecas;
    EstqPeso := QrVMIPesoKg.Value - PesoKg;
    EstqArM2 := QrVMIAreaM2.Value - AreaM2;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsproqui', False, [
    'Codigo', 'MovimCod', 'MovimNiv',
    'MovimTwn', 'Empresa', 'Terceiro',
    'MovimID', 'Pecas',
    'PesoKg', 'AreaM2', 'CusPQ',
    'Fuloes', 'MinDta', 'MaxDta',
    'EstqPeca', 'EstqPeso', 'EstqArM2'], [
    'Controle', 'SetorID'], [
    Codigo, MovimCod, MovimNiv,
    MovimTwn, Empresa, Terceiro,
    MovimID, Pecas,
    PesoKg, AreaM2, CusPQ,
    Fuloes, MinDta, MaxDta,
    EstqPeca, EstqPeso, EstqArM2], [
    Controle, SetorID], True) then
      VS_PF.AtualizaVSValorT(Controle);
  end else
  Geral.MB_Aviso(
  'ID de Movimento n�o implementado na defini��o estoque de couros na emiss�o de pesagem!');
end;

function TDmModVS.AtualizaVSMovIts_VS_BH(MovimCod: Integer;
  MovimID: TEstqMovimID): Boolean;
const
  sProcName = 'DmModVS.AtualizaVSMovIts_VS_BH()';
var
  CustoKg, Peso, CustoPQ, ValorT, KgCouPQ, SumEmitCusto, SumEmitPeso: Double;
  Controle: Integer;
begin
  SumEmitCusto := 0;
  SumEmitPeso  := 0;
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumEmit, Dmod.MyDB, [
  'SELECT SUM(Custo) Custo, ',
  'SUM(IF(Retrabalho=0, Peso, 0)) Peso ',
  'FROM emit ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  SumEmitCusto := SumEmitCusto + QrSumEmitCusto.Value;
  SumEmitPeso  := SumEmitPeso  + QrSumEmitPeso.Value;
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumEmit, Dmod.MyDB, [
  'SELECT SUM(CustoTotal) Custo, 0.000 Peso ',
  'FROM pqo ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  SumEmitCusto := SumEmitCusto + QrSumEmitCusto.Value;
  SumEmitPeso  := SumEmitPeso  + QrSumEmitPeso.Value;
  QrSumEmit.Close;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumVMI, Dmod.MyDB, [
  'SELECT SUM(QtdAntPeso) Qtde ',
  'FROM ' + CO_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv IN (' +
    Geral.FF0(Integer(eminSorcCal)) + ',' +
    Geral.FF0(Integer(eminSorcCur)) +
  ')',
  '']);
  //
  if QrSumVMIQtde.Value > 0 then
    CustoKg := SumEmitCusto / QrSumVMIQtde.Value
  else
    CustoKg := 0;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrVMIs, Dmod.MyDB, [
  'SELECT Controle, QtdAntPeso Qtde, ValorMP ',
  'FROM ' + CO_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv IN (' +
    Geral.FF0(Integer(eminSorcCal)) + ',' +
    Geral.FF0(Integer(eminSorcCur)) +
  ')',
  '']);
  //
  while not QrVMIs.Eof do
  begin
    if QrSumVMIQtde.Value = 0 then
      KgCouPQ  := 0
    else
      KgCouPQ  := (QrVMIsQtde.Value / QrSumVMIQtde.Value) * SumEmitPeso;
    CustoPQ  := -QrVMIsQtde.Value * CustoKg;
    ValorT   := CustoPQ + QrVMIsValorMP.Value;
    Controle := QrVMIsControle.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TAB_VMI, False, [
    'KgCouPQ', 'CustoPQ', 'ValorT'], [
    'Controle'], [
    KgCouPQ, CustoPQ, ValorT], [
    Controle], True);
    //
    QrVMIs.Next;
  end;
end;

function TDmModVS.AtualizaVSMovIts_VS_BHWE(MovimCod: Integer): Boolean;
const
  sProcName = 'DmModVS.AtualizaVSMovIts_VS_BHWE()';
var
(*
  CustoKg, Peso, CustoPQ, ValorT: Double;
  Controle: Integer;
  Campo: String;
*)
  MovimID: TEstqMovimID;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovCab, Dmod.MyDB, [
  'SELECT MovimID ',
  'FROM vsmovcab ',
  'WHERE Codigo=' + Geral.FF0(MovimCod),
  '']);
  //
  MovimID := TEstqMovimID(QrVSMovCab.FieldByName('MovimID').AsInteger);
  //
  case MovimID of
    emidEmProcCal,
    emidEmProcCur: AtualizaVSMovIts_VS_BH(MovimCod, MovimID);
    emidEmProcWE:  AtualizaVSMovIts_VS_WE(MovimCod, MovimID);
    else Geral.MB_Erro('"MovimID" [1] n�o definido em ' + sProcName);
  end;
  case MovimID of
    emidEmProcCal: VS_PF.AtualizaTotaisVSCalCab(MovimCod);
    emidEmProcCur: VS_PF.AtualizaTotaisVSCurCab(MovimCod);
    emidEmProcWE:  VS_PF.AtualizaTotaisVSPWECab(MovimCod);
    else Geral.MB_Erro('"MovimID" [2] n�o definido em ' + sProcName);
  end;
end;

function TDmModVS.AtualizaVSMovIts_VS_WE(MovimCod: Integer;
  MovimID: TEstqMovimID): Boolean;
const
  sProcName = 'DmModVS.AtualizaVSMovIts_VS_BH()';
var
  CustoKg, Peso, CustoPQ, ValorT, KgCouPQ, SumEmitCusto, SumEmitPeso: Double;
  Controle: Integer;
  Campo: String;
  //Grandeza: TGrandezaUnidMed;
  //
  procedure ReopenSumVMI();
  begin
    UnDMkDAC_PF.AbreMySQLQuery0(QrSumVMI, Dmod.MyDB, [
    'SELECT SUM(-' + Campo + ') Qtde ',
    'FROM ' + CO_TAB_VMI + ' ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv IN (' +
      Geral.FF0(Integer(eminSorcWEnd)) +
    ')',
    '']);
  end;
begin
  SumEmitCusto := 0;
  SumEmitPeso  := 0;
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumEmit, Dmod.MyDB, [
  'SELECT SUM(Custo) Custo, ',
  'SUM(IF(Retrabalho=0, Peso, 0)) Peso ',
  'FROM emit ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  SumEmitCusto := SumEmitCusto + QrSumEmitCusto.Value;
  SumEmitPeso  := SumEmitPeso  + QrSumEmitPeso.Value;
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumEmit, Dmod.MyDB, [
  'SELECT SUM(CustoTotal) Custo, 0.000 Peso ',
  'FROM pqo ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  SumEmitCusto := SumEmitCusto + QrSumEmitCusto.Value;
  SumEmitPeso  := SumEmitPeso  + QrSumEmitPeso.Value;
  QrSumEmit.Close;
  //
  Campo := 'AreaM2';
  ReopenSumVMI();
  //
  CustoKg := 0;
  if QrSumVMIQtde.Value > 0 then
    CustoKg := SumEmitCusto / QrSumVMIQtde.Value
  else
  begin
    //Grandeza := gumPesoKG;
    Campo := 'PesoKg';
    ReopenSumVMI();
  end;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrVMIs, Dmod.MyDB, [
  'SELECT Controle, -' + Campo + ' Qtde, ValorMP ',
  'FROM ' + CO_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv IN (' +
    Geral.FF0(Integer(eminSorcWEnd)) +
  ')',
  '']);
  //
  while not QrVMIs.Eof do
  begin
    if QrSumVMIQtde.Value = 0 then
      KgCouPQ := 0
    else
      KgCouPQ := (QrVMIsQtde.Value / QrSumVMIQtde.Value) * SumEmitPeso;
    CustoPQ  := -QrVMIsQtde.Value * CustoKg;
    //ValorT   := CustoPQ + QrVMIsValorMP.Value;
    Controle := QrVMIsControle.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TAB_VMI, False, [
    'KgCouPQ', 'CustoPQ'(*, 'ValorT'*)], [
    'Controle'], [
    KgCouPQ, CustoPQ(*, ValorT*)], [
    Controle], True);
    //
    QrVMIs.Next;
  end;
end;

procedure TDmModVS.AtualizaVSMulFrnCabNew(SinalOrig: TSinal;
  DefMulFrn: TEstqDefMulFldVS; MovTypeCod: Integer; MovimIDs:
  array of TEstqMovimID; MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv);
  //
  function ReInsereNovoVSMulFrnCab(): Integer;
  const
    Pecas    = 0.000;
    //PerPecas = 0.0000;
  var
    Nome: String;
    Codigo, OriIMEC, OriIMEI: Integer;
  begin
    Result         := 0;
    Codigo         := 0;
    Nome           := '';
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFC, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vsmulfrncab',
    'WHERE MovimType=' + Geral.FF0(Integer(DefMulFrn)),
    'AND MovTypeCod=' + Geral.FF0(MovTypeCod),
    '']);
    if QrLocMFCCodigo.Value <> 0 then
    begin
      Codigo := QrLocMFCCodigo.Value;
      // Zera dados dos itens ja existentes!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulfrnits', False, [
      'Pecas'(*, 'PerPecas'*)], ['Codigo'], [
      Pecas(*, PerPecas*)], [Codigo], True) then
        Result := Codigo;
    end else
    begin
      Codigo := UMyMod.BPGS1I32('vsmulfrncab', 'Codigo', '', '', tsPos, stIns, 0);
      // Cria novo pois nao existe ainda!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmulfrncab', False, [
      'Nome', 'MovimType', 'MovTypeCod'], [
      'Codigo'], [
      Nome, DefMulFrn, MovTypeCod], [
      Codigo], True) then
        Result := Codigo;
    end;
  end;
  //
  function ReInsereNovoVSFrnMulIts(Codigo, Remistura, CodRemix, Fornece:
  Integer; OrigPecas: Double; Inverte: Boolean): Integer;
  var
    Controle: Integer;
    SQLType: TSQLType;
    Pecas: Double;
  begin
    //if Inverte and (SinalOrig = siNegativo) then
    if OrigPecas < 0 then
      Pecas := - OrigPecas
    else
      Pecas := OrigPecas;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFI, Dmod.MyDB, [
    'SELECT Controle',
    'FROM vsmulfrnits',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND Remistura=' + Geral.FF0(Remistura),
    'AND CodRemix=' + Geral.FF0(CodRemix),
    'AND Fornece=' + Geral.FF0(Fornece),
    '']);
    if QrLocMFIControle.Value <> 0 then
    begin
      SQLType := stUpd;
      Controle := QrLocMFIControle.Value
    end else
    begin
      SQLType := stIns;
      Controle :=
        UMyMod.BPGS1I32('vsmulfrnits', 'Controle', '', '', tsPos, stIns, 0);
    end;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsmulfrnits', False, [
    'Codigo', 'Remistura', 'CodRemix',
    'Fornece', 'Pecas'(*, 'PerPecas'*)], [
    'Controle'], [
    Codigo, Remistura, CodRemix,
    Fornece, Pecas(*, PerPecas*)], [
    Controle], True);
  end;
var
  SQLMovimID, SQLMovimNiv: String;
  Terceiro, VSMulFrnCab, I: Integer;
  //Terceiros: array of Integer;
  //Pecas: array of Double;
  //
  Codigo, Remistura, CodRemix, Fornece: Integer;
  Pecas, PerPecas, TotMul: Double;
  //
  SiglaVS, Nome, Percentual, FldUpd: String;
  sSrcNivel2: String;
begin
  SQLMovimID  := '';
  FldUpd      := '???';
  for I := Low(MovimIDs) to High(MovimIDs) do
  begin
    if SQLMovimID <> '' then
      SQLMovimID :=  SQLMovimID + ',';
    SQLMovimID :=  Geral.FF0(Integer(MovimIDs[I]));
  end;
  if SQLMovimID <> '' then
    SQLMovimID :=  'AND MovimID IN (' + SQLMovimID + ')';
  //
  SQLMovimNiv  := '';
  for I := Low(MovimNivsOri) to High(MovimNivsOri) do
  begin
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  SQLMovimNiv + ',';
    SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsOri[I]));
  end;
  if SQLMovimNiv <> '' then
    SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
  //
  // IMEIs de origem
  case DefMulFrn of
    TEstqDefMulFldVS.edmfSrcNiv2:
    begin
      UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
      '  SELECT SrcNivel2 ',
      '  FROM ' + CO_TAB_VMI,
      '  WHERE Controle=' + Geral.FF0(MovTypeCod),
      '']);
      //sSrcNivel2 := MyObjects.CordaDeQuery(QrSrcNiv2, 'SrcNivel2');
      //
(*
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, VSMulFrnCab, Pecas   ',
      'FROM ' + CO_TAB_VMI,
      'WHERE Controle=(',
      '  SELECT SrcNivel2 ',
      '  FROM ' + CO_TAB_VMI,
      '  WHERE Controle=' + Geral.FF0(MovTypeCod),
      ') ',
      '']);
*)
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, VSMulFrnCab, Pecas   ',
      'FROM ' + CO_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(QrSrcNiv2.FieldByName('SrcNivel2').AsInteger),
      '']);
      FldUpd := 'Controle';
    end;
    TEstqDefMulFldVS.edmfMovCod:
    begin
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, VSMulFrnCab, SUM(Pecas) Pecas  ',
      'FROM ' + CO_TAB_VMI,
      'WHERE MovimCod=' + Geral.FF0(MovTypeCod),
      SQLMovimID,
      SQLMovimNiv,
      'GROUP BY Terceiro, VSMulFrnCab ',
      'ORDER BY Terceiro,  VSMulFrnCab ',
      ' ',
      '']);
      FldUpd := 'MovimCod';
    end;
    else
    begin
      Geral.MB_Erro('"Defini��o de fornecedor abortada!' + sLineBreak +
      '"TEstqMovimType" n�o definido: ' + Geral.FF0(Integer(DefMulFrn)));
      Exit;
    end;
  end;
  //
  //Geral.MB_SQL(nil, QrUniFrn);
  // Se tiver Item sem fornecedor, desiste!
  if (QrUniFrnTerceiro.Value = 0) and (QrUniFrnVSMulFrnCab.Value = 0) then
  begin
    if VAR_MEMO_DEF_VS_MUL_FRN <> nil then
       VAR_MEMO_DEF_VS_MUL_FRN.Text := FldUpd + ' = ' + Geral.FF0(MovTypeCod) +
       sLineBreak + VAR_MEMO_DEF_VS_MUL_FRN.Text
    else
      Geral.MB_Erro('Defini��o de fornecedor abortada!' + sLineBreak +
      'Existe pelo menos um item de origem sem fornecedor definido!');
    Exit;
  end;
  Terceiro    := 0;
  VSMulFrnCab := 0;
  //
  // Se tiver s� um registro...
  if QrUniFrn.RecordCount = 1 then
  begin
    if QrUniFrnTerceiro.Value <> 0 then
      Terceiro := QrUniFrnTerceiro.Value
    else
      VSMulFrnCab := QrUniFrnVSMulFrnCab.Value;
  end else
  // Se tiver varios registros...
  begin
    Terceiro := 0;
    Codigo   := ReInsereNovoVSMulFrnCab();
    while not QrUniFrn.Eof do
    begin
      if (QrUniFrnTerceiro.Value <> 0) and (QrUniFrnVSMulFrnCab.Value = 0) then
      begin
        Remistura := 0;
        CodRemix  := 0;
        Fornece   := QrUniFrnTerceiro.Value;
        Pecas     := QrUniFrnPecas.Value;
        ReInsereNovoVSFrnMulIts(Codigo, Remistura, CodRemix, Fornece, Pecas, True);
      end else
      if (QrUniFrnTerceiro.Value = 0) and (QrUniFrnVSMulFrnCab.Value <> 0) then
      begin
        Remistura := 1;
        CodRemix  := QrUniFrnVSMulFrnCab.Value;
        //
        UnDMkDAC_PF.AbreMySQLQuery0(QrMulFrn, Dmod.MyDB, [
        'SELECT Fornece, SUM(Pecas) Pecas, "" SiglaVS',
        'FROM vsmulfrnits',
        'WHERE Codigo=' + Geral.FF0(CodRemix),
        'GROUP BY Fornece ',
        '']);
        TotMul := 0;
        QrMulFrn.First;
        while not QrMulFrn.Eof do
        begin
          TotMul := TotMul + QrMulFrnPecas.Value;
          //
          QrMulFrn.Next;
        end;
        QrMulFrn.First;
        while not QrMulFrn.Eof do
        begin
          //Pecas     := QrMulFrnPecas.Value;
          if TotMul <> 0 then
            Pecas := (QrMulFrnPecas.Value / TotMul) * QrUniFrnPecas.Value
          else
            Pecas := 0;
          //
          Fornece   := QrMulFrnFornece.Value;
          ReInsereNovoVSFrnMulIts(Codigo, Remistura, CodRemix, Fornece, Pecas, False);
          //
          QrMulFrn.Next;
        end;
      end;
      //
      QrUniFrn.Next;
    end;
    // Atualiza VSMulFrnCab!
    UnDMkDAC_PF.AbreMySQLQuery0(QrMulFrn, Dmod.MyDB, [
    'SELECT mfi.Fornece, SUM(mfi.Pecas) Pecas, vem.SiglaVS',
    'FROM vsmulfrnits mfi',
    'LEFT JOIN vsentimp vem ON vem.Codigo=mfi.Fornece',
    'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
    'GROUP BY mfi.Fornece',
    'ORDER BY Pecas DESC',
    '']);
    UnDMkDAC_PF.AbreMySQLQuery0(QrTotFrn, Dmod.MyDB, [
    'SELECT SUM(mfi.Pecas) Pecas ',
    'FROM vsmulfrnits mfi',
    'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
    '']);
    Nome := '';
    while not QrMulFrn.Eof do
    begin
      if QrMulFrnSiglaVS.Value = '' then
        SiglaVS := VS_PF.DefineSiglaVS_Frn(QrMulFrnFornece.Value)
      else
        SiglaVS := QrMulFrnSiglaVS.Value;
      //
      if QrTotFrnPecas.Value <> 0 then
      begin
        PerPecas   := QrMulFrnPecas.Value / QrTotFrnPecas.Value * 100;
        Percentual := Geral.FI64(Round(PerPecas));
      end else
      begin
        PerPecas   := 0;
        Percentual := '??';
      end;
      //
      Nome := Nome + SiglaVS + ' ' + Percentual + '% ';
      //
      QrMulFrn.Next;
    end;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulfrncab', False, [
    'Nome'], ['Codigo'], [
    Nome], [Codigo], True) then
      VSMulFrnCab := Codigo;
  end;
  SQLMovimNiv  := '';
  for I := Low(MovimNivsDst) to High(MovimNivsDst) do
  begin
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  SQLMovimNiv + ',';
    SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsDst[I]));
  end;
  if SQLMovimNiv <> '' then
    SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
  //
  UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE ' + CO_TAB_VMI,
  'SET Terceiro=' + Geral.FF0(Terceiro) + ', ',
  'VSMulFrnCab=' + Geral.FF0(VSMulFrnCab),
  'WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
  //SQLMovimID, PWE tem Finish
  SQLMovimNiv,
  '']);
  //  Atualizar descendentes!
  UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
  '  SELECT DISTINCT Controle',
  '  FROM ' + CO_TAB_VMI,
  '  WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
//     SQLMovimID,
     SQLMovimNiv,
  '  AND Controle <> 0 ',
  '']);
  sSrcNivel2 := MyObjects.CordaDeQuery(QrSrcNiv2, 'Controle');
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrMFSrc, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM ' + CO_TAB_VMI,
  'WHERE SrcNivel2 IN (',
  '  SELECT Controle',
  '  FROM ' + CO_TAB_VMI,
  '  WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
//     SQLMovimID,
     SQLMovimNiv,
  '  AND Controle <> 0 ',
  ') ',
  '']);
*)
  if Trim(sSrcNivel2) <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrMFSrc, Dmod.MyDB, [
    'SELECT MovimID, MovimNiv, MovimCod, Controle ',
    'FROM ' + CO_TAB_VMI,
    'WHERE SrcNivel2 IN (' + sSrcNivel2 + ') ',
    '']);
    QrMFSrc.First;
    while not QrMFSrc.Eof do
    begin
      UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE ' + CO_TAB_VMI,
      'SET Terceiro=' + Geral.FF0(Terceiro) + ', ',
      'VSMulFrnCab=' + Geral.FF0(VSMulFrnCab),
      'WHERE Controle=' + Geral.FF0(QrMFSrcControle.Value),
      '']);
      //
      QrMFSrc.Next;
    end;
  end;
(*
  // Orfaos!
  UnDMkDAC_PF.AbreMySQLQuery0(QrMFOrfaos, Dmod.MyDB, [
  'SELECT Controle, Terceiro, VSMulFrnCab',
  'FROM ' + CO_TAB_VMI,
  'WHERE Controle IN ( ',
  '  SELECT SrcNivel2 ',
  '  FROM ' + CO_TAB_VMI,
  '  WHERE SrcNivel2<>0',
  '  AND Terceiro=0',
  '  AND VSMulFrnCab=0',
  ') ',
  'AND (',
  '  Terceiro<>0',
  '  OR',
  '  VSMulFrnCab<>0',
  ')',
  '']);
  QrMFOrfaos.First;
  while not QrMFOrfaos.Eof do
  begin
    UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ' + CO_TAB_VMI,
    'SET Terceiro=' + Geral.FF0(QrMFOrfaosTerceiro.Value) + ', ',
    'VSMulFrnCab=' + Geral.FF0(QrMFOrfaosVSMulFrnCab.Value),
    'WHERE SrcNivel2<>0 ',
    'AND SrcNivel2=' + Geral.FF0(QrMFOrfaosControle.Value),
    'AND Terceiro=0 ',
    'AND VSMulFrnCab=0 ',
    '']);
    //
    QrMFOrfaos.Next;
  end;
*)
end;

procedure TDmModVS.AtualizaVSMulNFeCab(SinalOrig: TSinal;
  DefMulNFe: TEstqDefMulFldVS; MovTypeCod: Integer;
  MovimID: TEstqMovimID; MovimNivsOri,
  MovimNivsDst: array of TEstqMovimNiv);
 //
  function ReInsereNovoVSMulNFeCab(): Integer;
  const
    Pecas    = 0.000;
    //PerPecas = 0.0000;
  var
    Nome: String;
    Codigo, OriIMEC, OriIMEI: Integer;
  begin
    Result         := 0;
    Codigo         := 0;
    Nome           := '';
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFC, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vsmulnfecab',
    'WHERE MovimType=' + Geral.FF0(Integer(DefMulNFe)),
    'AND MovTypeCod=' + Geral.FF0(MovTypeCod),
    '']);
    if QrLocMFCCodigo.Value <> 0 then
    begin
      Codigo := QrLocMFCCodigo.Value;
      // Zera dados dos itens ja existentes!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulnfeits', False, [
      'Pecas'(*, 'PerPecas'*)], ['Codigo'], [
      Pecas(*, PerPecas*)], [Codigo], True) then
        Result := Codigo;
    end else
    begin
      Codigo := UMyMod.BPGS1I32('vsmulnfecab', 'Codigo', '', '', tsPos, stIns, 0);
      // Cria novo pois nao existe ainda!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmulnfecab', False, [
      'Nome', 'MovimType', 'MovTypeCod'], [
      'Codigo'], [
      Nome, DefMulNFe, MovTypeCod], [
      Codigo], True) then
        Result := Codigo;
    end;
  end;
  //
  function ReInsereNovoVSNFeMulIts(Codigo, Remistura, CodRemix, NFeSer, NFeNum:
  Integer; OrigPecas: Double; Inverte: Boolean): Integer;
  var
    Controle: Integer;
    SQLType: TSQLType;
    Pecas: Double;
  begin
    //if Inverte and (SinalOrig = siNegativo) then
    if OrigPecas < 0 then
      Pecas := - OrigPecas
    else
      Pecas := OrigPecas;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFI, Dmod.MyDB, [
    'SELECT Controle',
    'FROM vsmulnfeits',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND Remistura=' + Geral.FF0(Remistura),
    'AND CodRemix=' + Geral.FF0(CodRemix),
    'AND NFeSer=' + Geral.FF0(NFeSer),
    'AND NFeNum=' + Geral.FF0(NFeNum),
    '']);
    if QrLocMFIControle.Value <> 0 then
    begin
      SQLType := stUpd;
      Controle := QrLocMFIControle.Value
    end else
    begin
      SQLType := stIns;
      Controle :=
        UMyMod.BPGS1I32('vsmulnfeits', 'Controle', '', '', tsPos, stIns, 0);
    end;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsmulnfeits', False, [
    'Codigo', 'Remistura', 'CodRemix',
    'NFeSer', 'NFeNum',
    'Pecas'(*, 'PerPecas'*)], [
    'Controle'], [
    Codigo, Remistura, CodRemix,
    NFeSer, NFeNum,
    Pecas(*, PerPecas*)], [
    Controle], True);
  end;
var
  SQLMovimID, SQLMovimNiv: String;
  VSMulNFeCab, I: Integer;
  //
  Codigo, Remistura, CodRemix, NFeSer, NFeNum: Integer;
  Pecas, PerPecas, TotMul: Double;
  //
  Nome, Percentual, FldUpd: String;
  sSrcNivel2, SiglaNFe: String;
  //
  TabCab, FldCabSer, FldCabdNum: String;
begin
  if MyObjects.FIC(DefMulNFe <> edmfMovCod, nil,
  '"ID" deve ser do tipo "edmfMovCod"!') then
    Exit;
  TabCab := VS_PF.ObtemNomeTabelaVSXxxCab(MovimID);
  VS_PF.ObtemNomeCamposNFeVSXxxCab(MovimID, FldCabSer, FldCabdNum);
  //
  FldUpd := 'MovimCod';
  NFeSer := 0;
  if MovimID = TEstqMovimID.emidIndsVS then
  begin
    NFeSer      := 0;
    NFeNum      := 0;
    VSMulNFeCab := 0;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSCabNFe, Dmod.MyDB, [
    'SELECT ' + FldCabSer + ' Serie, ' + FldCabdNum + ' nNF ',
    'FROM ' + TabCab,
    'WHERE MovimCod=' + Geral.FF0(MovTypeCod),
    '']);
    if QrVSCabNFenNF.Value <> 0 then
    begin
      NFeSer      := QrVSCabNFeSerie.Value;
      NFeNum      := QrVSCabNFenNF.Value;
      VSMulNFeCab := 0;
    end;
  end;
  if NFeNum = 0 then
  begin
    SQLMovimID :=  'AND MovimID=' + Geral.FF0(Integer(MovimID));
    //
    SQLMovimNiv  := '';
    for I := Low(MovimNivsOri) to High(MovimNivsOri) do
    begin
      if SQLMovimNiv <> '' then
        SQLMovimNiv :=  SQLMovimNiv + ',';
      SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsOri[I]));
    end;
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
    //
    // IMEIs de origem
    case DefMulNFe of
      TEstqDefMulFldVS.edmfSrcNiv2:
      begin
        UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
        '  SELECT SrcNivel2 ',
        '  FROM ' + CO_TAB_VMI,
        '  WHERE Controle=' + Geral.FF0(MovTypeCod),
        '']);
        //
        UnDMkDAC_PF.AbreMySQLQuery0(QrUniNFe, Dmod.MyDB, [
        'SELECT NFeSer, NFeNum, VSMulNFeCab, Pecas   ',
        'FROM ' + CO_TAB_VMI,
        'WHERE Controle=' + Geral.FF0(QrSrcNiv2.FieldByName('SrcNivel2').AsInteger),
        '']);
        FldUpd := 'Controle';
      end;
      TEstqDefMulFldVS.edmfMovCod:
      begin
        UnDMkDAC_PF.AbreMySQLQuery0(QrUniNFe, Dmod.MyDB, [
        'SELECT NFeSer, NFeNum, VSMulNFeCab, SUM(Pecas) Pecas  ',
        'FROM ' + CO_TAB_VMI,
        'WHERE MovimCod=' + Geral.FF0(MovTypeCod),
        SQLMovimID,
        SQLMovimNiv,
        'GROUP BY NFeNum, VSMulNFeCab ',
        'ORDER BY NFeNum, VSMulNFeCab ',
        ' ',
        '']);
        FldUpd := 'MovimCod';
      end;
      else
      begin
        Geral.MB_Erro('"Defini��o de NFe abortada!' + sLineBreak +
        '"TEstqMovimType" n�o definido: ' + Geral.FF0(Integer(DefMulNFe)));
        Exit;
      end;
    end;
    //
    //Geral.MB_SQL(nil, QrUniNFe);
    // Se tiver Item sem NFe, desiste!
    if (QrUniNFeNFeNum.Value = 0) and (QrUniNFeVSMulNFeCab.Value = 0) then
    begin
      if VAR_MEMO_DEF_VS_MUL_NFE <> nil then
         VAR_MEMO_DEF_VS_MUL_NFE.Text := FldUpd + ' = ' + Geral.FF0(MovTypeCod) +
         sLineBreak + VAR_MEMO_DEF_VS_MUL_NFE.Text
      else
        Geral.MB_Erro('Defini��o de NFe abortada!' + sLineBreak +
        'Existe pelo menos um item de origem sem NFe definido!');
      Exit;
    end;
    NFeSer      := 0;
    NFeNum      := 0;
    VSMulNFeCab := 0;
    //
    // Se tiver s� um registro...
    if QrUniNFe.RecordCount = 1 then
    begin
      if QrUniNFeNFeNum.Value <> 0 then
      begin
        NFeSer := QrUniNFeNFeSer.Value;
        NFeNum := QrUniNFeNFeNum.Value;
      end else
        VSMulNFeCab := QrUniNFeVSMulNFeCab.Value;
    end else
    // Se tiver varios registros...
    begin
      NFeSer   := 0;
      NFeNum   := 0;
      Codigo   := ReInsereNovoVSMulNFeCab();
      while not QrUniNFe.Eof do
      begin
        if (QrUniNFeNFeNum.Value <> 0) and (QrUniNFeVSMulNFeCab.Value = 0) then
        begin
          Remistura := 0;
          CodRemix  := 0;
          NFeSer    := QrUniNFeNFeSer.Value;
          NFeNum    := QrUniNFeNFeNum.Value;
          Pecas     := QrUniNFePecas.Value;
          ReInsereNovoVSNFeMulIts(Codigo, Remistura, CodRemix, NFeSer, NFeNum, Pecas, True);
        end else
        if (QrUniNFeNFeNum.Value = 0) and (QrUniNFeVSMulNFeCab.Value <> 0) then
        begin
          Remistura := 1;
          CodRemix  := QrUniNFeVSMulNFeCab.Value;
          //
          UnDMkDAC_PF.AbreMySQLQuery0(QrMulNFe, Dmod.MyDB, [
          'SELECT MAX(NFeSer) NFeSer, NFeNum, SUM(Pecas) Pecas',
          'FROM vsmulnfeits',
          'WHERE Codigo=' + Geral.FF0(CodRemix),
          'GROUP BY NFeNum ',
          '']);
          TotMul := 0;
          QrMulNFe.First;
          while not QrMulNFe.Eof do
          begin
            TotMul := TotMul + QrMulNFePecas.Value;
            //
            QrMulNFe.Next;
          end;
          QrMulNFe.First;
          while not QrMulNFe.Eof do
          begin
            //Pecas     := QrMulNFePecas.Value;
            if TotMul <> 0 then
              Pecas := (QrMulNFePecas.Value / TotMul) * QrUniNFePecas.Value
            else
              Pecas := 0;
            //
            NFeSer   := QrMulNFeNFeSer.Value;
            NFeNum   := QrMulNFeNFeNum.Value;
            ReInsereNovoVSNFeMulIts(Codigo, Remistura, CodRemix, NFeSer, NFeNum, Pecas, False);
            //
            QrMulNFe.Next;
          end;
        end;
        //
        QrUniNFe.Next;
      end;
      // Atualiza VSMulNFeCab!
      UnDMkDAC_PF.AbreMySQLQuery0(QrMulNFe, Dmod.MyDB, [
      'SELECT mfi.NFeSer, mfi.NFeNum, ',
      'SUM(mfi.Pecas) Pecas ',
      'FROM vsmulnfeits mfi',
      'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
      'GROUP BY mfi.NFeNum',
      'ORDER BY Pecas DESC',
      '']);
      UnDMkDAC_PF.AbreMySQLQuery0(QrTotNFe, Dmod.MyDB, [
      'SELECT SUM(mfi.Pecas) Pecas ',
      'FROM vsmulnfeits mfi',
      'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
      '']);
      Nome := '';
      while not QrMulNFe.Eof do
      begin
        SiglaNFe := VS_PF.DefineSiglaVS_NFe(QrMulNFeNFeSer.Value, QrMulNFeNFeNum.Value);
        //
        if QrTotNFePecas.Value <> 0 then
        begin
          PerPecas   := QrMulNFePecas.Value / QrTotNFePecas.Value * 100;
          Percentual := Geral.FI64(Round(PerPecas));
        end else
        begin
          PerPecas   := 0;
          Percentual := '??';
        end;
        //
        Nome := Nome + SiglaNFe + ' ' + Percentual + '% ';
        //
        QrMulNFe.Next;
      end;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulnfecab', False, [
      'Nome'], ['Codigo'], [
      Nome], [Codigo], True) then
        VSMulNFeCab := Codigo;
    end;
  end;
  SQLMovimNiv  := '';
  for I := Low(MovimNivsDst) to High(MovimNivsDst) do
  begin
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  SQLMovimNiv + ',';
    SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsDst[I]));
  end;
  if SQLMovimNiv <> '' then
    SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
  //
  UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE ' + CO_TAB_VMI,
  'SET NFeSer=' + Geral.FF0(NFeSer) + ', ',
  'NFeNum=' + Geral.FF0(NFeNum) + ', ',
  'VSMulNFeCab=' + Geral.FF0(VSMulNFeCab),
  'WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
  //SQLMovimID, PWE tem Finish
  SQLMovimNiv,
  '']);
  //  Atualizar descendentes!
  UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
  '  SELECT DISTINCT Controle',
  '  FROM ' + CO_TAB_VMI,
  '  WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
//     SQLMovimID,
     SQLMovimNiv,
  '  AND Controle <> 0 ',
  '']);
  sSrcNivel2 := MyObjects.CordaDeQuery(QrSrcNiv2, 'Controle');
  //
  if Trim(sSrcNivel2) <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrMFSrc, Dmod.MyDB, [
    'SELECT MovimID, MovimNiv, MovimCod, Controle ',
    'FROM ' + CO_TAB_VMI,
    'WHERE SrcNivel2 IN (' + sSrcNivel2 + ') ',
    '']);
    QrMFSrc.First;
    while not QrMFSrc.Eof do
    begin
      case TEstqMovimID(QrMFSrcMovimID.Value) of
        (*6*)emidIndsVS:
        begin
          UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'UPDATE ' + CO_TAB_VMI,
          'SET NFeSer=' + Geral.FF0(NFeSer) + ', ',
          'NFeNum=' + Geral.FF0(NFeNum) + ', ',
          'VSMulNFeCab=' + Geral.FF0(VSMulNFeCab),
          'WHERE MovimCod=' + Geral.FF0(QrMFSrcMovimCod.Value),
          '']);
        end;
        else
        begin
          UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'UPDATE ' + CO_TAB_VMI,
          'SET NFeSer=' + Geral.FF0(NFeSer) + ', ',
          'NFeNum=' + Geral.FF0(NFeNum) + ', ',
          'VSMulNFeCab=' + Geral.FF0(VSMulNFeCab),
          'WHERE Controle=' + Geral.FF0(QrMFSrcControle.Value),
          '']);
        end;
      end;
      //
      QrMFSrc.Next;
    end;
  end;
end;

{
procedure TDmModVS.AtualizaVSMulFrnCabOld(SinalOrig: TSinal;
  DefMulFrn: TEstqDefMulFldVS; MovTypeCod: Integer; MovimIDs:
  array of TEstqMovimID; MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv);
  //
  function ReInsereNovoVSMulFrnCab(): Integer;
  const
    Pecas    = 0.000;
    //PerPecas = 0.0000;
  var
    Nome: String;
    Codigo, OriIMEC, OriIMEI: Integer;
  begin
    Result         := 0;
    Codigo         := 0;
    Nome           := '';
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFC, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vsmulfrncab',
    'WHERE MovimType=' + Geral.FF0(Integer(DefMulFrn)),
    'AND MovTypeCod=' + Geral.FF0(MovTypeCod),
    '']);
    if QrLocMFCCodigo.Value <> 0 then
    begin
      Codigo := QrLocMFCCodigo.Value;
      // Zera dados dos itens ja existentes!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulfrnits', False, [
      'Pecas'(*, 'PerPecas'*)], ['Codigo'], [
      Pecas(*, PerPecas*)], [Codigo], True) then
        Result := Codigo;
    end else
    begin
      Codigo := UMyMod.BPGS1I32('vsmulfrncab', 'Codigo', '', '', tsPos, stIns, 0);
      // Cria novo pois nao existe ainda!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmulfrncab', False, [
      'Nome', 'MovimType', 'MovTypeCod'], [
      'Codigo'], [
      Nome, DefMulFrn, MovTypeCod], [
      Codigo], True) then
        Result := Codigo;
    end;
  end;
  //
  function ReInsereNovoVSFrnMulIts(Codigo, Remistura, CodRemix, Fornece:
  Integer; OrigPecas: Double; Inverte: Boolean): Integer;
  var
    Controle: Integer;
    SQLType: TSQLType;
    Pecas: Double;
  begin
    //if Inverte and (SinalOrig = siNegativo) then
    if OrigPecas < 0 then
      Pecas := - OrigPecas
    else
      Pecas := OrigPecas;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFI, Dmod.MyDB, [
    'SELECT Controle',
    'FROM vsmulfrnits',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND Remistura=' + Geral.FF0(Remistura),
    'AND CodRemix=' + Geral.FF0(CodRemix),
    'AND Fornece=' + Geral.FF0(Fornece),
    '']);
    if QrLocMFIControle.Value <> 0 then
    begin
      SQLType := stUpd;
      Controle := QrLocMFIControle.Value
    end else
    begin
      SQLType := stIns;
      Controle :=
        UMyMod.BPGS1I32('vsmulfrnits', 'Controle', '', '', tsPos, stIns, 0);
    end;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsmulfrnits', False, [
    'Codigo', 'Remistura', 'CodRemix',
    'Fornece', 'Pecas'(*, 'PerPecas'*)], [
    'Controle'], [
    Codigo, Remistura, CodRemix,
    Fornece, Pecas(*, PerPecas*)], [
    Controle], True);
  end;
var
  SQLMovimID, SQLMovimNiv: String;
  Terceiro, VSMulFrnCab, I: Integer;
  //Terceiros: array of Integer;
  //Pecas: array of Double;
  //
  Codigo, Remistura, CodRemix, Fornece: Integer;
  Pecas, PerPecas, TotMul: Double;
  //
  SiglaVS, Nome, Percentual, FldUpd: String;
  sSrcNivel2: String;
begin
  SQLMovimID  := '';
  FldUpd      := '???';
  for I := Low(MovimIDs) to High(MovimIDs) do
  begin
    if SQLMovimID <> '' then
      SQLMovimID :=  SQLMovimID + ',';
    SQLMovimID :=  Geral.FF0(Integer(MovimIDs[I]));
  end;
  if SQLMovimID <> '' then
    SQLMovimID :=  'AND MovimID IN (' + SQLMovimID + ')';
  //
  SQLMovimNiv  := '';
  for I := Low(MovimNivsOri) to High(MovimNivsOri) do
  begin
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  SQLMovimNiv + ',';
    SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsOri[I]));
  end;
  if SQLMovimNiv <> '' then
    SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
  //
  // IMEIs de origem
  case DefMulFrn of
    TEstqDefMulFldVS.edmfSrcNiv2:
    begin
(**)
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, VSMulFrnCab, Pecas   ',
      'FROM ' + CO_TAB_VMI,
      'WHERE Controle=(',
      '  SELECT SrcNivel2 ',
      '  FROM ' + CO_TAB_VMI,
      '  WHERE Controle=' + Geral.FF0(MovTypeCod),
      ') ',
      '']);
(**)
(*
      UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
      '  SELECT SrcNivel2 ',
      '  FROM ' + CO_TAB_VMI,
      '  WHERE Controle=' + Geral.FF0(MovTypeCod),
      '']);
      //
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, VSMulFrnCab, Pecas   ',
      'FROM ' + CO_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(QrSrcNiv2.FieldByName('SrcNivel2').AsInteger),
      '']);
*)
      FldUpd := 'Controle';
    end;
    TEstqDefMulFldVS.edmfMovCod:
    begin
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, VSMulFrnCab, SUM(Pecas) Pecas  ',
      'FROM ' + CO_TAB_VMI,
      'WHERE MovimCod=' + Geral.FF0(MovTypeCod),
      SQLMovimID,
      SQLMovimNiv,
      'GROUP BY Terceiro, VSMulFrnCab ',
      'ORDER BY Terceiro,  VSMulFrnCab ',
      ' ',
      '']);
      FldUpd := 'MovimCod';
    end;
    else
    begin
      Geral.MB_Erro('"Defini��o de fornecedor abortada!' + sLineBreak +
      '"TEstqMovimType" n�o definido: ' + Geral.FF0(Integer(DefMulFrn)));
      Exit;
    end;
  end;
  //
  //Geral.MB_SQL(nil, QrUniFrn);
  // Se tiver Item sem fornecedor, desiste!
  if (QrUniFrnTerceiro.Value = 0) and (QrUniFrnVSMulFrnCab.Value = 0) then
  begin
    if VAR_MEMO_DEF_VS_MUL_FRN <> nil then
       VAR_MEMO_DEF_VS_MUL_FRN.Text := FldUpd + ' = ' + Geral.FF0(MovTypeCod) +
       sLineBreak + VAR_MEMO_DEF_VS_MUL_FRN.Text
    else
      Geral.MB_Erro('Defini��o de fornecedor abortada!' + sLineBreak +
      'Existe pelo menos um item de origem sem fornecedor definido!');
    Exit;
  end;
  Terceiro    := 0;
  VSMulFrnCab := 0;
  //
  // Se tiver s� um registro...
  if QrUniFrn.RecordCount = 1 then
  begin
    if QrUniFrnTerceiro.Value <> 0 then
      Terceiro := QrUniFrnTerceiro.Value
    else
      VSMulFrnCab := QrUniFrnVSMulFrnCab.Value;
  end else
  // Se tiver varios registros...
  begin
    Terceiro := 0;
    Codigo   := ReInsereNovoVSMulFrnCab();
    while not QrUniFrn.Eof do
    begin
      if (QrUniFrnTerceiro.Value <> 0) and (QrUniFrnVSMulFrnCab.Value = 0) then
      begin
        Remistura := 0;
        CodRemix  := 0;
        Fornece   := QrUniFrnTerceiro.Value;
        Pecas     := QrUniFrnPecas.Value;
        ReInsereNovoVSFrnMulIts(Codigo, Remistura, CodRemix, Fornece, Pecas, True);
      end else
      if (QrUniFrnTerceiro.Value = 0) and (QrUniFrnVSMulFrnCab.Value <> 0) then
      begin
        Remistura := 1;
        CodRemix  := QrUniFrnVSMulFrnCab.Value;
        //
        UnDMkDAC_PF.AbreMySQLQuery0(QrMulFrn, Dmod.MyDB, [
        'SELECT Fornece, SUM(Pecas) Pecas, "" SiglaVS',
        'FROM vsmulfrnits',
        'WHERE Codigo=' + Geral.FF0(CodRemix),
        'GROUP BY Fornece ',
        '']);
        TotMul := 0;
        QrMulFrn.First;
        while not QrMulFrn.Eof do
        begin
          TotMul := TotMul + QrMulFrnPecas.Value;
          //
          QrMulFrn.Next;
        end;
        QrMulFrn.First;
        while not QrMulFrn.Eof do
        begin
          //Pecas     := QrMulFrnPecas.Value;
          if TotMul <> 0 then
            Pecas := (QrMulFrnPecas.Value / TotMul) * QrUniFrnPecas.Value
          else
            Pecas := 0;
          //
          Fornece   := QrMulFrnFornece.Value;
          ReInsereNovoVSFrnMulIts(Codigo, Remistura, CodRemix, Fornece, Pecas, False);
          //
          QrMulFrn.Next;
        end;
      end;
      //
      QrUniFrn.Next;
    end;
    // Atualiza VSMulFrnCab!
    UnDMkDAC_PF.AbreMySQLQuery0(QrMulFrn, Dmod.MyDB, [
    'SELECT mfi.Fornece, SUM(mfi.Pecas) Pecas, vem.SiglaVS',
    'FROM vsmulfrnits mfi',
    'LEFT JOIN vsentimp vem ON vem.Codigo=mfi.Fornece',
    'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
    'GROUP BY mfi.Fornece',
    'ORDER BY Pecas DESC',
    '']);
    UnDMkDAC_PF.AbreMySQLQuery0(QrTotFrn, Dmod.MyDB, [
    'SELECT SUM(mfi.Pecas) Pecas ',
    'FROM vsmulfrnits mfi',
    'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
    '']);
    Nome := '';
    while not QrMulFrn.Eof do
    begin
      if QrMulFrnSiglaVS.Value = '' then
        SiglaVS := VS_PF.DefineSiglaVS(QrMulFrnFornece.Value)
      else
        SiglaVS := QrMulFrnSiglaVS.Value;
      //
      if QrTotFrnPecas.Value <> 0 then
      begin
        PerPecas   := QrMulFrnPecas.Value / QrTotFrnPecas.Value * 100;
        Percentual := Geral.FI64(Round(PerPecas));
      end else
      begin
        PerPecas   := 0;
        Percentual := '??';
      end;
      //
      Nome := Nome + SiglaVS + ' ' + Percentual + '% ';
      //
      QrMulFrn.Next;
    end;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulfrncab', False, [
    'Nome'], ['Codigo'], [
    Nome], [Codigo], True) then
      VSMulFrnCab := Codigo;
  end;
  SQLMovimNiv  := '';
  for I := Low(MovimNivsDst) to High(MovimNivsDst) do
  begin
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  SQLMovimNiv + ',';
    SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsDst[I]));
  end;
  if SQLMovimNiv <> '' then
    SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
  //
  UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE ' + CO_TAB_VMI,
  'SET Terceiro=' + Geral.FF0(Terceiro) + ', ',
  'VSMulFrnCab=' + Geral.FF0(VSMulFrnCab),
  'WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
  //SQLMovimID, PWE tem Finish
  SQLMovimNiv,
  '']);
  //  Atualizar descendentes!
(**)
  UnDmkDAC_PF.AbreMySQLQuery0(QrMFSrc, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM ' + CO_TAB_VMI,
  'WHERE SrcNivel2 IN (',
  '  SELECT Controle',
  '  FROM ' + CO_TAB_VMI,
  '  WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
     SQLMovimNiv,
  '  AND Controle <> 0 ',
  ') ',
  '']);
(**)
(*
  UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
  '  SELECT DISTINCT Controle',
  '  FROM ' + CO_TAB_VMI,
  '  WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
     SQLMovimNiv,
  '  AND Controle <> 0 ',
  '']);
  sSrcNivel2 := MyObjects.CordaDeQuery(QrSrcNiv2, 'Controle');
  UnDmkDAC_PF.AbreMySQLQuery0(QrMFSrc, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM ' + CO_TAB_VMI,
  'WHERE SrcNivel2 IN (' + sSrcNivel2 + ') ',
  '']);
*)
  QrMFSrc.First;
  while not QrMFSrc.Eof do
  begin
    UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ' + CO_TAB_VMI,
    'SET Terceiro=' + Geral.FF0(Terceiro) + ', ',
    'VSMulFrnCab=' + Geral.FF0(VSMulFrnCab),
    'WHERE Controle=' + Geral.FF0(QrMFSrcControle.Value),
    '']);
    //
    QrMFSrc.Next;
  end;
(**)
  // Orfaos!
  UnDMkDAC_PF.AbreMySQLQuery0(QrMFOrfaos, Dmod.MyDB, [
  'SELECT Controle, Terceiro, VSMulFrnCab',
  'FROM ' + CO_TAB_VMI,
  'WHERE Controle IN ( ',
  '  SELECT SrcNivel2 ',
  '  FROM ' + CO_TAB_VMI,
  '  WHERE SrcNivel2<>0',
  '  AND Terceiro=0',
  '  AND VSMulFrnCab=0',
  ') ',
  'AND (',
  '  Terceiro<>0',
  '  OR',
  '  VSMulFrnCab<>0',
  ')',
  '']);
  QrMFOrfaos.First;
  while not QrMFOrfaos.Eof do
  begin
    UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ' + CO_TAB_VMI,
    'SET Terceiro=' + Geral.FF0(QrMFOrfaosTerceiro.Value) + ', ',
    'VSMulFrnCab=' + Geral.FF0(QrMFOrfaosVSMulFrnCab.Value),
    'WHERE SrcNivel2<>0 ',
    'AND SrcNivel2=' + Geral.FF0(QrMFOrfaosControle.Value),
    'AND Terceiro=0 ',
    'AND VSMulFrnCab=0 ',
    '']);
    //
    QrMFOrfaos.Next;
  end;
(**)
end;
}

procedure TDmModVS.DefinePeriodoBalVSEmpresaLogada();
begin
  UnDmkDAC_PF.AbreMySQLQUery0(QrVSBalEmp, Dmod.MyDB, [
  'SELECT MAX(Periodo) Periodo ',
  'FROM vsbalemp ',
  'WHERE Empresa IN (' + VAR_LIB_EMPRESAS + ')',
  '']);
  //
  VAR_VS_PERIODO_BAL := QrVSBalEmpPeriodo.Value
end;

function TDmModVS.ObtemFornecedores_Pallet(Pallet: Integer): String;
  function SQL_Parcial_Its(Tab: TTabToWork): String;
  begin
    Result := Geral.ATS([
    'SELECT IF((vem.SiglaVS="") OR (vem.SiglaVS IS NULL),   ',
    '  CONCAT("FRN-", mfi.Fornece), vem.SiglaVS) SiglaVS,  ',
    'CAST(IF(vmi.VSMulFrnCab<>0, mfi.Fornece, vmi.Terceiro) AS SIGNED) FrnCod,   ',
    'CAST(SUM(IF(vmi.VSMulFrnCab<>0, mfi.Pecas, vmi.Pecas)) AS DECIMAL (15,3)) Pecas  ',
    'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
    'LEFT JOIN vsmulfrnits mfi ON vmi.VSMulFrnCab=mfi.Codigo  ',
    'LEFT JOIN vsentimp vem ON vem.Codigo=IF(vmi.VSMulFrnCab<>0, mfi.Fornece, vmi.Terceiro)  ',
    'WHERE vmi.Pallet=' + Geral.FF0(Pallet),
    'AND ( ' + VS_PF.SQL_MovIDeNiv_Pos_Inn() + ') ',
    'GROUP BY FrnCod  ',
    Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
  end;
  //
  function SQL_Parcial_Sum(Tab: TTabToWork): String;
  begin
    Result := Geral.ATS([
    'SELECT ',
    'CAST(SUM(IF(vmi.VSMulFrnCab<>0, mfi.Pecas, vmi.Pecas)) AS DECIMAL (15,3)) Pecas  ',
    'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
    'LEFT JOIN vsmulfrnits mfi ON vmi.VSMulFrnCab=mfi.Codigo  ',
    'WHERE vmi.Pallet=' + Geral.FF0(Pallet),
    'AND ( ' + VS_PF.SQL_MovIDeNiv_Pos_Inn() + ') ',
    Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
  end;
  //
var
  Fator, PerPecas: Double;
  SiglaVS, Percentual: String;
begin
  Result := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTotFrn2, Dmod.MyDB, [
  SQL_Parcial_Sum(ttwB),
  SQL_Parcial_Sum(ttwA),
  '']);
  Fator := 0;
  QrTotFrn2.First;
  while not QrTotFrn2.Eof do
  begin
    Fator := Fator + QrTotFrn2Pecas.Value;
    //
    QrTotFrn2.Next;
  end;
  if Fator > 0 then
    Fator := Fator / 100
  else
    Fator := High(Integer);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMulFrn2, Dmod.MyDB, [
  SQL_Parcial_Its(ttwB),
  SQL_Parcial_Its(ttwA),
  '',
  'ORDER BY Pecas DESC ',
  '']);
  QrMulFrn2.First;
  while not QrMulFrn2.Eof do
  begin
    SiglaVS := QrMulFrn2SiglaVS.Value;
    if QrTotFrn2Pecas.Value <> 0 then
    begin
      PerPecas   := QrMulFrn2Pecas.Value / Fator;
      Percentual := Geral.FI64(Round(PerPecas));
    end else
    begin
      PerPecas   := 0;
      Percentual := '??';
    end;
    //
    if (SiglaVS <> '') or (PerPecas <> 0) then
    begin
      if Trim(SiglaVS) = '' then
        SiglaVS := '???';
      Result := Result + SiglaVS + ' ' + Percentual + '% ';
    end;
    //
    QrMulFrn2.Next;
  end;
end;

function TDmModVS.PreparaPalletParaReclassificar(const SQLType: TSQLType; const
  CkReclasse_Checked: Boolean; var Empresa, ClientMO, Codigo, MovimCod,
  Controle, Pallet, GraGruX, GraGruY: Integer; DtHr: TDateTime): Boolean;
  function CalculaValorT(): Double;
  var
    CustoM2, ValorT: Double;
  begin
    if QrVSMovItsAreaM2.Value > 0 then
      Result := (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value) * QrVSMovItsSdoVrtArM2.Value
    else
    if QrVSMovItsPesoKg.Value > 0 then
      Result := (QrVSMovItsValorT.Value / QrVSMovItsPesoKg.Value) * QrVSMovItsSdoVrtPeso.Value
    else
    if QrVSMovItsPecas.Value > 0 then
      Result := (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value) * QrVSMovItsSdoVrtPeca.Value
    else
      Result := 0;
  end;
var
  NewVMI: Integer;
  DataHora: String;
  Pecas, PesoKg, AreaM2, AreaP2: Double;
  SerieFch, Ficha, Terceiro, FornecMO, TpCalcAuto, PedItsLib, PedItsFin,
  PedItsVda, GSPInnNiv2, GSPArtNiv2, ReqMovEstq, StqCenLoc,
  VSMulFrnCab: Integer;
  Marca: String;
  CustoMOKg, CustoMOTot, ValorMP, ValorT: Double;
var
  SQL_Rcl, SQL_15: String;
begin
  Result := False;
  if CkReclasse_Checked then
  begin
    SQL_Rcl := ' ';
    SQL_15  := ', 15';
  end else
  begin
    SQL_Rcl := 'AND vsp.GerRclCab = 0 ';
    SQL_15   := '';
  end;
  //
(*
  if MyObjects.FIC(EdPallet.ValueVariant = 0, EdPallet, 'Informe o Pallet!') then
    Exit;
  //
  Pallet   := EdPallet.ValueVariant;
*)
  UnDmkDAC_PF.AbreMySQLQUery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT wmi.*, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, ggx.GraGruY',
  'FROM ' + CO_TAB_VMI + ' wmi  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet  ',
  'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa  ',
  'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro ',
  'WHERE wmi.Pecas > 0 ',
  'AND wmi.Empresa=' + Geral.FF0(Empresa),
  'AND wmi.Pallet > 0 ',
  'AND wmi.Pallet=' + Geral.FF0(Pallet),
  'AND wmi.SdoVrtPeca > 0 ',
  SQL_Rcl,
  'AND MovimID IN (' + CO_ALL_CODS_CLASS_VS + SQL_15 + ') ',
  'ORDER BY Controle ',
  '']);
  if MyObjects.FIC(QrVSMovIts.RecordCount = 0, nil,
  'Pallet sem IME-Is com saldo!!') then
    Exit;
  //
  Pecas       := 0;
  PesoKg      := 0;
  AreaM2      := 0;
  AreaP2      := 0;
  SerieFch    := QrVSMovItsSerieFch.Value;
  Ficha       := QrVSMovItsFicha.Value;
  Terceiro    := QrVSMovItsTerceiro.Value;
  VSMulFrnCab := QrVSMovItsVSMulFrnCab.Value;
  FornecMO    := QrVSMovItsFornecMO.Value;
  Marca       := QrVSMovItsMarca.Value;;
  CustoMOKg   := 0;
  CustoMOTot  := 0;
  ValorMP     := 0;
  ValorT      := 0;

  if DtHr < 2 then
    DataHora := Geral.FDT(DModG.ObtemAgora(), 109)
  else
    DataHora := Geral.FDT(DtHr, 109);
  //Controle :=
  NewVMI := UMyMod.BPGS1I32(CO_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
  Codigo := UMyMod.BPGS1I32('VSPrePalCab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  QrVSMovIts.First;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsprepalcab', False, [
  'Pallet'], ['Codigo'], [Pallet], [Codigo], True) then
  begin
    // Parei Aqui 2015-10-03
    if SQLType = stIns then
      VS_PF.InsereVSMovCab(MovimCod, emidPreReclasse, Codigo);
    while not QrVSMovIts.Eof do
    begin
      Pecas  := Pecas + QrVSMovItsSdoVrtPeca.Value;
      PesoKg := PesoKg + QrVSMovItsSdoVrtPeso.Value;
      AreaM2 := AreaM2 + QrVSMovItsSdoVrtArM2.Value;
      ValorT := ValorT + CalculaValorT();
      //AreaP2 := 0;
      if SerieFch <> QrVSMovItsSerieFch.Value then
        SerieFch := 0;
      if Ficha <> QrVSMovItsFicha.Value then
        Ficha := 0;
      if Terceiro <> QrVSMovItsTerceiro.Value then
        Terceiro := 0;
      if FornecMO <> QrVSMovItsFornecMO.Value then
        FornecMO := 0;
      if Marca <> QrVSMovItsMarca.Value then
        Marca := '';
      //
      PreparaPalletParaReclass_InsereBaixaAtual(Codigo, MovimCod, DataHora,
        NewVMI, SQLType);
      //QrVSMovIts.First;
      //QrVSMovIts.First;
      //
      QrVSMovIts.Next;
    end;
    AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    //
    TpCalcAuto    := QrVSMovItsTpCalcAuto.Value;
    // ver o que fazer!!!
    PedItsLib     := 0; // QrVSMovItsPedItsLib.Value;
    PedItsFin     := 0; // QrVSMovItsPedItsFin.Value;
    PedItsVda     := 0; // QrVSMovItsPedItsVda.Value;
    GSPInnNiv2    := 0; // QrVSMovItsGSPInnNiv2.Value;
    GSPArtNiv2    := 0; // QrVSMovItsGSPArtNiv2.Value;
    ReqMovEstq    := QrVSMovItsReqMovEstq.Value;
    StqCenLoc     := QrVSMovItsStqCenLoc.Value;
    //
    PreparaPalletParaReclass_InsereNovoVMI_DoPallet(Pallet, NewVMI, Codigo,
      MovimCod, DataHora, Pecas, PesoKg, AreaM2, AreaP2, SerieFch, Ficha,
      Terceiro, VSMulFrnCab, FornecMO, Marca, CustoMOKg, CustoMOTot, ValorMP,
      ValorT, Empresa, ClientMO, GraGruX, GraGruY, TpCalcAuto, PedItsLib,
      PedItsFin, PedItsVda, GSPInnNiv2, GSPArtNiv2, ReqMovEstq, StqCenLoc,
      SQLType);
    Controle := NewVMI;
    Result := True;
  end;
end;

procedure TDmModVS.PreparaPalletParaReclass_InsereBaixaAtual(Codigo,
  MovimCod: Integer; DataHora: String; NewVMI: Integer; SQLType: TSQLType);
const
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  //
  TpCalcAuto = -1;
  //
  EdPalletX = nil;
  EdAreaM2X = nil;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPInnNiv2 = 0;
  GSPArtNiv2 = 0;
  //
  ReqMovEstq = 0;
  StqCenLoc  = 0;
  //
  ItemNFe    = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  //DataHora,
  Observ, Marca: String;
  //Codigo, MovimCod,
  Pallet, Controle, Empresa, GraGruX, Terceiro, Ficha, GraGruY, FornecMO,
  SerieFch, VSMulFrnCab, ClientMO: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP,
  ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  SrcMovID, DstMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2, SrcGGX, DstNivel1, DstNivel2, DstGGX: Integer;
begin
  //Codigo         := Codigo;
  //Controle       := ??;
  //MovimCod       := 0;
  Empresa        := QrVSMovItsEmpresa.Value;
  ClientMO       := QrVSMovItsClientMO.Value;
  MovimID        := emidPreReclasse;
  MovimNiv       := eminSorcPreReclas;
  GraGruX        := QrVSMovItsGraGruX.Value;
  Pallet         := QrVSMovItsPallet.Value;
  //DataHora       := Geral.FDT(Geral.ObtemAgora(), 109);
  Pecas          := QrVSMovItsSdoVrtPeca.Value;
  PesoKg         := QrVSMovItsSdoVrtPeso.Value;
  AreaM2         := QrVSMovItsSdoVrtArM2.Value;
  AreaP2         := Geral.ConverteArea(QrVSMovItsSdoVrtArM2.Value, ctM2toP2, cfQuarto);
  // Ver depois?
  SerieFch       := QrVSMovItsSerieFch.Value;
  Ficha          := QrVSMovItsFicha.Value;
  Terceiro       := QrVSMovItsTerceiro.Value;
  VSMulFrnCab    := QrVSMovItsVSMulFrnCab.Value;
  FornecMO       := QrVSMovItsFornecMO.Value;
  Marca          := QrVSMovItsMarca.Value;
  CustoMOKg      := 0;
  CustoMOM2      := 0;
  CustoMOTot     := 0;
  ValorMP        := 0;
  ValorT         := 0;
  Observ         := '';
  GraGruY        := QrVSMovItsGraGruY.Value;
  //
(*
  if VS_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPalletX, EdFicha, EdPecas, EdAreaM2X,
  EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca) then
    Exit;
*)
  //
  SrcMovID   := TEstqMovimID(QrVSMovItsMovimID.Value);
  SrcNivel1  := QrVSMovItsCodigo.Value;
  SrcNivel2  := QrVSMovItsControle.Value;
  SrcGGX     := QrVSMovItsGraGruX.Value;
  //
  DstMovID   := emidPreReclasse;
  DstNivel1  := Codigo;
  DstNivel2  := NewVMI;
  DstGGX     := QrVSMovItsGraGruX.Value;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
  //
  if VS_PF.InsUpdVSMovIts(SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro, MovimID, MovimNiv, Pallet, GraGruX, -Pecas, -PesoKg, -AreaM2, -AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CustoMOKg, CustoMOM2, CustoMOTot,
  ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPInnNiv2, GSPArtNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, CO_0_GGXRcl) then
  begin
    //VS_PF.AtualizaTotaisVSXxxCab('VSPrePalCab', MovimCod);
    VS_PF.AtualizaSaldoIMEI(SrcNivel2, True);
    //FmVSAjsCab.LocCod(Codigo, Codigo);
    //FmVSAjsCab.ReopenVSAjsIts(Controle);
    (*
    if CkContinuar.Checked then
    begin
      if GraGruX <> 0 then
        FUltGGX := GraGruX;
      ImgTipo.SQLType            := stIns;
      EdPallet.ValueVariant      := 0;
      CBPallet.KeyValue          := Null;
      //
      EdPallet.SetFocus;
    end else
      Close;
    *)
  end;
end;

procedure TDmModVS.PreparaPalletParaReclass_InsereNovoVMI_DoPallet(Pallet,
  Controle, Codigo, MovimCod: Integer; DataHora: String; Pecas, PesoKg, AreaM2,
  AreaP2: Double; SerieFch, Ficha, Terceiro, VSMulFrnCab, FornecMO: Integer;
  Marca: String; CustoMOKg, CustoMOTot, ValorMP, ValorT: Double; Empresa,
  ClientMO, GraGruX, GraGruY, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  GSPInnNiv2, GSPArtNiv2, ReqMovEstq, StqCenLoc: Integer; SQLType: TSQLType);
const
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  CustoMOM2  = 0;
  ItemNFe    = 0;
  //
  EdPalletX = nil;
  EdAreaM2X = nil;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  //DataHora, Marca,
  Observ: String;
  //Pallet, Codigo, MovimCod, Controle, Terceiro, Ficha, FornecMO, SerieFch,
  //GraGruX, GraGruY: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, CustoMOKg, CustoMOTot, ValorMP, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
begin
  //Codigo         := FCodigo;
  //Controle       := NewVMI;
  //MovimCod       := FMovimCod;
  //Empresa        := QrPalletsEmpresa.Value;
  MovimID        := emidPreReclasse;
  MovimNiv       := eminDestPreReclas;
  //GraGruX        := QrPalletsGraGruX.Value;
(*
  Pallet         := EdPallet.ValueVariant;
  DataHora       := Geral.FDT(Geral.ObtemAgora(), 109);
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  // Ver depois?
  SerieFch       := 0;
  Ficha          := 0;
  Terceiro       := 0;
  FornecMO       := 0;
  Marca          := '';
  CustoMOKg      := ;
  CustoMOTot     := ;
  ValorMP        := ;
  ValorT         := ;
*)
  Observ         := '';
  //GraGruY        := QrPalletsGraGruY.Value;
  //
(*
  if VS_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPalletX, EdFicha, EdPecas, EdAreaM2X,
  EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca) then
    Exit;
*)
  //
  //Codigo := UMyMod.BPGS1I32('VSPrePalCab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  //MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  //
  if VS_PF.InsUpdVSMovIts(SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CustoMOKg, CustoMOM2, CustoMOTot,
  ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPInnNiv2, GSPArtNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, CO_0_GGXRcl) then
  begin
    //VS_PF.AtualizaTotaisVSXxxCab('VSPrePalCab', MovimCod);
    VS_PF.AtualizaSaldoIMEI(Controle, True);
    //FmVSAjsCab.LocCod(Codigo, Codigo);
    //FmVSAjsCab.ReopenVSAjsIts(Controle);
    (*
    if CkContinuar.Checked then
    begin
      if GraGruX <> 0 then
        FUltGGX := GraGruX;
      ImgTipo.SQLType            := stIns;
      EdPallet.ValueVariant      := 0;
      CBPallet.KeyValue          := Null;
      //
      EdPallet.SetFocus;
    end else
      Close;
    *)
  end;
end;

procedure TDmModVS.Reopen16Fatores(TP16DataIni_Date, TP16DataFim_Date:
  TDateTime; Ck16DataIni_Checked, Ck16DataFim_Checked: Boolean);
var
  SQL_Periodo: String;
begin
  SQL_Periodo := dmkPF.SQL_Periodo('AND pra.DtHrFimCla ', TP16DataIni_Date,
  TP16DataFim_Date, Ck16DataIni_Checked, Ck16DataFim_Checked);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr16Fatores, Dmod.MyDB, [
  'SELECT 1.000 ClaRcl, pra.DtHrFimCla, pra.Codigo CodigoClaRcl, ',
  'pra.CacCod OC, pra.VSGerArt IDGeracao,',
  '0.000 Pallet_Src, pri.VSPallet Pallet_Dst, ',
  'c1s.FatorInt FatorIntSrc, c1d.FatorInt FatorIntDst ',
  'FROM vspaclaitsa pri',
  'LEFT JOIN vspaclacaba pra ON pra.Codigo=pri.Codigo',
  'LEFT JOIN ' + CO_TAB_VMI + '    vmi ON vmi.Controle=pra.VSMovIts',
  'LEFT JOIN vspalleta   pad ON pad.Codigo=pri.VSPallet',
  'LEFT JOIN gragruxcou  xcs ON xcs.GraGruX=vmi.GraGruX',
  'LEFT JOIN gragruxcou  xcd ON xcd.GraGruX=pad.GraGruX',
  'LEFT JOIN couniv1     c1s ON c1s.Codigo=xcs.CouNiv1 ',
  'LEFT JOIN couniv1     c1d ON c1d.Codigo=xcd.CouNiv1 ',
  'WHERE c1s.FatorInt <> c1d.FatorInt',
  SQL_Periodo,
  '',
  'UNION',
  '',
  'SELECT 2.000 ClaRcl, pra.DtHrFimCla, pra.Codigo CodigoClaRcl, ',
  'pra.CacCod OC, pra.VSGerRcl IDGeracao,',
  'pra.VSPallet + 0.000 Pallet_Src, pri.VSPallet Pallet_Dst, ',
  'c1s.FatorInt FatorIntSrc, c1d.FatorInt FatorIntDst ',
  'FROM vsparclitsa pri',
  'LEFT JOIN vsparclcaba pra ON pra.Codigo=pri.Codigo',
  'LEFT JOIN vspalleta   pas ON pas.Codigo=pra.VSPallet',
  'LEFT JOIN vspalleta   pad ON pad.Codigo=pri.VSPallet',
  'LEFT JOIN gragruxcou  xcs ON xcs.GraGruX=pas.GraGruX',
  'LEFT JOIN gragruxcou  xcd ON xcd.GraGruX=pad.GraGruX',
  'LEFT JOIN couniv1     c1s ON c1s.Codigo=xcs.CouNiv1 ',
  'LEFT JOIN couniv1     c1d ON c1d.Codigo=xcd.CouNiv1 ',
  'WHERE c1s.FatorInt <> c1d.FatorInt',
  SQL_Periodo,
  '',
  'ORDER BY DtHrFimCla',
  '']);
end;

procedure TDmModVS.TbCouNiv1AfterEdit(DataSet: TDataSet);
var
  ResVar: Variant;
  CasasDecimais: Integer;
  Qtde: Double;
begin
  Qtde          := TbCouNiv1FatorInt.Value;
  CasasDecimais := 3;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  Qtde, CasasDecimais, 0, '0', '1', True, 'Fator', 'Informe o fator: ',
  0, ResVar) then
  begin
    Qtde := Geral.DMV(ResVar);
    TbCouNiv1FatorInt.ReadOnly := False;
    TbCouNiv1FatorInt.Value := Qtde;
    TbCouNiv1.Post;
    TbCouNiv1FatorInt.ReadOnly := True;
  end;
end;

end.
