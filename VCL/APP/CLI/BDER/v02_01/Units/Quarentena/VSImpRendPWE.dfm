object FmVSImpRendPWE: TFmVSImpRendPWE
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-153 :: Impress'#227'o de Rendimento de Semi e Acabado'
  ClientHeight = 638
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 563
        Height = 32
        Caption = 'Impress'#227'o de Rendimento de Semi e Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 563
        Height = 32
        Caption = 'Impress'#227'o de Rendimento de Semi e Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 563
        Height = 32
        Caption = 'Impress'#227'o de Rendimento de Semi e Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 476
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 476
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 476
        Align = alClient
        TabOrder = 0
        object DBGIndef: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 1004
          Height = 459
          Align = alClient
          DataSource = DsIndef
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_CouNiv2'
              Title.Caption = 'Tipo de material'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CouNiv1'
              Title.Caption = 'Parte do material'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruY'
              Title.Caption = 'Grupo'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_GGY'
              Title.Caption = 'Nome do grupo'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Nome / tamenho / cor do artigo'
              Width = 360
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 524
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 568
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 11
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrRend: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _vmi_emin_22_; '
      'CREATE TABLE _vmi_emin_22_ '
      'SELECT * '
      'FROM bluederm_meza.vsmovits '
      'WHERE MovimNiv=22; '
      ' '
      'DROP TABLE IF EXISTS _vmi_emin_23_; '
      'CREATE TABLE _vmi_emin_23_ '
      'SELECT * '
      'FROM bluederm_meza.vsmovits '
      'WHERE MovimNiv=23; '
      ' '
      'DROP TABLE IF EXISTS _vmi_emin_20_; '
      'CREATE TABLE _vmi_emin_20_ '
      'SELECT DISTINCT MovimCod, GraGruX '
      'FROM bluederm_meza.vsmovits '
      'WHERE MovimNiv=20 '
      'GROUP BY MovimCod; '
      ' '
      ' '
      
        'SELECT cou3.Bastidao, IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) ' +
        'NO_FORNECE, '
      '_20.GraGruX, CONCAT(gg11.Nome, '
      'IF(gti1.PrintTam=0, "", CONCAT(" ", gti1.Nome)), '
      'IF(gcc1.PrintCor=0,"", CONCAT(" ", gcc1.Nome))) '
      'NO_PRD_TAM_COR_22, '
      ' '
      'CONCAT(gg12.Nome, '
      'IF(gti2.PrintTam=0, "", CONCAT(" ", gti2.Nome)), '
      'IF(gcc2.PrintCor=0,"", CONCAT(" ", gcc2.Nome))) '
      'NO_PRD_TAM_COR, '
      ' '
      'IF(_23.AreaM2 <> 0, '
      '(_22.AreaM2+_23.AreaM2)/-_23.AreaM2*100, 0) Rendimento, '
      'cab.NFeRem, cab.Codigo, cab.MovimCod, _22.Controle Ctrl22, '
      
        '_22.MovimTwn, _22.Empresa, _22.Terceiro, _22.MovimID, _22.DataHo' +
        'ra, '
      '_22.Pallet, _22.GraGruX GGX22, _22.Pecas Pecas_22, '
      '_22.AreaM2 AreaM2_22, _23.Controle Ctrl23, _23.GraGruX GGX23, '
      '-_23.Pecas Pecas_23, -_23.AreaM2 AreaM2_23 '
      ' '
      'FROM _vmi_emin_22_ _22 '
      'LEFT JOIN _vmi_emin_20_ _20 ON _20.MovimCod=_22.MovimCod '
      'LEFT JOIN _vmi_emin_23_ _23 ON _23.MovimTwn=_22.MovimTwn '
      
        'LEFT JOIN bluederm_meza.vspwecab  cab ON cab.MovimCod=_22.MovimC' +
        'od '
      
        'LEFT JOIN bluederm_meza.entidades frn ON frn.Codigo=_22.Terceiro' +
        ' '
      
        'LEFT JOIN bluederm_meza.gragruxcou cou3 ON cou3.GraGruX=_23.GraG' +
        'ruX'
      
        'LEFT JOIN bluederm_meza.gragrux    ggx1 ON ggx1.Controle=_22.Gra' +
        'GruX '
      
        'LEFT JOIN bluederm_meza.gragruc    ggc1 ON ggc1.Controle=ggx1.Gr' +
        'aGruC '
      
        'LEFT JOIN bluederm_meza.gracorcad  gcc1 ON gcc1.Codigo=ggc1.GraC' +
        'orCad '
      
        'LEFT JOIN bluederm_meza.gratamits  gti1 ON gti1.Controle=ggx1.Gr' +
        'aTamI '
      
        'LEFT JOIN bluederm_meza.gragru1    gg11 ON gg11.Nivel1=ggx1.GraG' +
        'ru1 '
      ' '
      
        'LEFT JOIN bluederm_meza.gragrux    ggx2 ON ggx2.Controle=_20.Gra' +
        'GruX '
      
        'LEFT JOIN bluederm_meza.gragruc    ggc2 ON ggc2.Controle=ggx2.Gr' +
        'aGruC '
      
        'LEFT JOIN bluederm_meza.gracorcad  gcc2 ON gcc2.Codigo=ggc2.GraC' +
        'orCad '
      
        'LEFT JOIN bluederm_meza.gratamits  gti2 ON gti2.Controle=ggx2.Gr' +
        'aTamI '
      
        'LEFT JOIN bluederm_meza.gragru1    gg12 ON gg12.Nivel1=ggx2.GraG' +
        'ru1 '
      ' '
      'AND cou3.Bastidao IN (-999999999,1,2,3,4,5,6,7,8)'
      'WHERE cab.DtHrAberto  >= "2016-03-07"'
      'ORDER BY NFeRem, DataHora, NO_PRD_TAM_COR_22'
      '')
    Left = 64
    Top = 288
    object QrRendBastidao: TSmallintField
      FieldName = 'Bastidao'
    end
    object QrRendNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrRendGGX20: TIntegerField
      FieldName = 'GGX20'
    end
    object QrRendNO_PRD_TAM_COR_22: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_22'
      Size = 157
    end
    object QrRendNO_PRD_TAM_COR_20: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_20'
      Size = 157
    end
    object QrRendRendimento: TFloatField
      FieldName = 'Rendimento'
    end
    object QrRendNFeRem: TIntegerField
      FieldName = 'NFeRem'
    end
    object QrRendCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRendMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrRendCtrl22: TIntegerField
      FieldName = 'Ctrl22'
    end
    object QrRendMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrRendEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrRendTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrRendMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrRendDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrRendPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrRendGGX22: TIntegerField
      FieldName = 'GGX22'
    end
    object QrRendPecas_22: TFloatField
      FieldName = 'Pecas_22'
    end
    object QrRendAreaM2_22: TFloatField
      FieldName = 'AreaM2_22'
    end
    object QrRendCtrl23: TIntegerField
      FieldName = 'Ctrl23'
    end
    object QrRendGGX23: TIntegerField
      FieldName = 'GGX23'
    end
    object QrRendPecas_23: TFloatField
      FieldName = 'Pecas_23'
    end
    object QrRendAreaM2_23: TFloatField
      FieldName = 'AreaM2_23'
    end
    object QrRendDATA: TDateField
      FieldName = 'DATA'
    end
    object QrRendNO_Bastidao: TWideStringField
      FieldName = 'NO_Bastidao'
      Size = 60
    end
    object QrRendCtrl20: TIntegerField
      FieldName = 'Ctrl20'
    end
  end
  object QrIndef: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cou.*, nv1.Nome NO_CouNiv1, nv2.Nome NO_CouNiv2, '
      'ggx.GraGruY, ggy.Nome NO_GGY, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM gragruxcou cou'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=cou.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1'
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'WHERE Bastidao=-1')
    Left = 252
    Top = 92
    object QrIndefGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrIndefCouNiv1: TIntegerField
      FieldName = 'CouNiv1'
    end
    object QrIndefCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrIndefArtigoImp: TWideStringField
      FieldName = 'ArtigoImp'
      Required = True
      Size = 50
    end
    object QrIndefClasseImp: TWideStringField
      FieldName = 'ClasseImp'
      Required = True
      Size = 30
    end
    object QrIndefPrevPcPal: TIntegerField
      FieldName = 'PrevPcPal'
    end
    object QrIndefMediaMinM2: TFloatField
      FieldName = 'MediaMinM2'
    end
    object QrIndefMediaMaxM2: TFloatField
      FieldName = 'MediaMaxM2'
    end
    object QrIndefLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrIndefDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrIndefDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrIndefUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrIndefUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrIndefAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrIndefAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrIndefMediaMinKg: TFloatField
      FieldName = 'MediaMinKg'
    end
    object QrIndefMediaMaxKg: TFloatField
      FieldName = 'MediaMaxKg'
    end
    object QrIndefPrevAMPal: TFloatField
      FieldName = 'PrevAMPal'
    end
    object QrIndefPrevKgPal: TFloatField
      FieldName = 'PrevKgPal'
    end
    object QrIndefGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrIndefBastidao: TSmallintField
      FieldName = 'Bastidao'
    end
    object QrIndefNO_CouNiv1: TWideStringField
      FieldName = 'NO_CouNiv1'
      Size = 60
    end
    object QrIndefNO_CouNiv2: TWideStringField
      FieldName = 'NO_CouNiv2'
      Size = 60
    end
    object QrIndefGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrIndefNO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object QrIndefNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsIndef: TDataSource
    DataSet = QrIndef
    Left = 252
    Top = 140
  end
  object frxWET_CURTI_153_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      
        'procedure Memo13OnPreviewDblClick(Sender: TfrxView; Button: TMou' +
        'seButton; Shift: Integer; var Modified: Boolean);'
      'begin'
      '  MyFunc(<frxDsRend."Ctrl22">);        '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_153_AGetValue
    OnUserFunction = frxWET_CURTI_153_AUserFunction
    Left = 64
    Top = 240
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsRend
        DataSetName = 'frxDsRend'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RENDIMENTO DE COURO SEMI ACABADO ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 143.622140000000000000
          Top = 64.252010000000000000
          Width = 170.078752360000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Left = 86.929190000000000000
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '????')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          Left = 529.134200000000000000
          Top = 64.252010000000000000
          Width = 52.913388270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 480.000310000000000000
          Top = 64.252010000000000000
          Width = 49.133850940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo19: TfrxMemoView
          Left = 313.700990000000000000
          Top = 64.252010000000000000
          Width = 56.692930470000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'NFeRem')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          Left = 430.866420000000000000
          Top = 64.252010000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo30: TfrxMemoView
          Left = 279.685220000000000000
          Top = 41.574830000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '???')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Top = 64.252010000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo70: TfrxMemoView
          Left = 650.079160000000000000
          Top = 64.252010000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo21: TfrxMemoView
          Left = 370.393940000000000000
          Top = 64.252010000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 582.047620000000000000
          Top = 64.252010000000000000
          Width = 52.913388270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Rendim.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 30.236240000000000000
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 340.157700000000000000
        Width = 680.315400000000000000
        DataSet = frxDsRend
        DataSetName = 'frxDsRend'
        RowCount = 0
        object Memo64: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
          WordWrap = False
        end
        object Memo67: TfrxMemoView
          Left = 22.677180000000000000
          Width = 634.960625040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15527148
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          Left = 30.236240000000000000
          Width = 619.842920000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          ParentFont = False
          WordWrap = False
        end
        object MeValNome: TfrxMemoView
          Left = 143.622140000000000000
          Width = 170.078752360000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR_20'
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRend."NO_PRD_TAM_COR_20"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object MeValCodi: TfrxMemoView
          Left = 86.929190000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'GGX20'
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRend."GGX20"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 430.866420000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."Pecas_23"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 370.393940000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."AreaM2_23"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 313.700990000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'NFeRem'
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."NFeRem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 529.134200000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."AreaM2_22"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 480.000310000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'Pecas_22'
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."Pecas_22"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 582.047620000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'Rendimento'
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."Rendimento"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 30.236240000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          OnPreviewDblClick = 'Memo13OnPreviewDblClick'
          DataField = 'Ctrl22'
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRend."Ctrl22"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 619.842920000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 566.929500000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000000000
          Width = 309.921264720000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 430.866420000000000000
          Top = 15.118120000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 370.393940000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 529.134200000000000000
          Top = 15.118120000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 480.000310000000000000
          Top = 15.118120000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 582.047620000000000000
          Top = 15.118120000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsRend."AreaM2_23">,MD002,1)<=0,0,'
            
              '  ((SUM(<frxDsRend."AreaM2_22">,MD002,1)-SUM(<frxDsRend."AreaM2_' +
              '23">,MD002,1))/SUM(<frxDsRend."AreaM2_23">,MD002,1)*100))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_00: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsRend."GraGruY"'
        object Memo33: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Me_GH0: TfrxMemoView
          Width = 680.314985040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRend."NO_GGY"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_02: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 249.448980000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsRend."GraGruX"'
        object Memo42: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Me_GH2: TfrxMemoView
          Left = 18.897650000000000000
          Width = 642.519685040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsRend."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 204.094620000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsRend."GraGruX"'
        object Memo35: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Me_GH1: TfrxMemoView
          Left = 11.338590000000000000
          Width = 657.637805040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsRend."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 453.543600000000000000
        Width = 680.315400000000000000
        object Memo49: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 430.866420000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT1: TfrxMemoView
          Left = 45.354360000000000000
          Width = 264.566929130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."NO_PRD_TAM_COR"] ')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu1AreaM2: TfrxMemoView
          Left = 370.393940000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 529.134200000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 480.000310000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FT_02: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 415.748300000000000000
        Width = 680.315400000000000000
        object Memo45: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Me_FT2: TfrxMemoView
          Left = 45.354360000000000000
          Width = 264.566929130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."NO_PRD_TAM_COR"] ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 430.866420000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSu2AreaM2: TfrxMemoView
          Left = 370.393940000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 529.134200000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 480.000310000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_03: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 294.803340000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsRend."GraGruX"'
        object Memo43: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 22.677180000000000000
          Width = 634.960625040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15527148
          ParentFont = False
        end
        object Me_GH3: TfrxMemoView
          Left = 26.456710000000000000
          Width = 623.622035040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsRend."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_03: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
        object Memo44: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 22.677180000000000000
          Width = 634.960625040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15527148
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 430.866420000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT3: TfrxMemoView
          Left = 45.354360000000000000
          Width = 264.566929130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."NO_GGY"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 370.393940000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 529.134200000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 480.000310000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FT_00: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 491.338900000000000000
        Width = 680.315400000000000000
        object Memo51: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 430.866420000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT0: TfrxMemoView
          Left = 45.354360000000000000
          Width = 264.566929130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."NO_GGY"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu0AreaM2: TfrxMemoView
          Left = 370.393940000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 529.134200000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 480.000310000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 582.047620000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsRend."AreaM2_23">,MD002,1)<=0,0,'
            
              '  ((SUM(<frxDsRend."AreaM2_22">,MD002,1)-SUM(<frxDsRend."AreaM2_' +
              '23">,MD002,1))/SUM(<frxDsRend."AreaM2_23">,MD002,1)*100))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsRend: TfrxDBDataset
    UserName = 'frxDsRend'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Bastidao=Bastidao'
      'NO_FORNECE=NO_FORNECE'
      'GGX20=GGX20'
      'NO_PRD_TAM_COR_22=NO_PRD_TAM_COR_22'
      'NO_PRD_TAM_COR_20=NO_PRD_TAM_COR_20'
      'Rendimento=Rendimento'
      'NFeRem=NFeRem'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Ctrl22=Ctrl22'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GGX22=GGX22'
      'Pecas_22=Pecas_22'
      'AreaM2_22=AreaM2_22'
      'Ctrl23=Ctrl23'
      'GGX23=GGX23'
      'Pecas_23=Pecas_23'
      'AreaM2_23=AreaM2_23'
      'DATA=DATA'
      'NO_Bastidao=NO_Bastidao'
      'Ctrl20=Ctrl20')
    DataSet = QrRend
    BCDToCurrency = False
    Left = 64
    Top = 336
  end
end
