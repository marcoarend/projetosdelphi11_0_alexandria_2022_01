object FmSPED_EFD_X999_Balancos: TFmSPED_EFD_X999_Balancos
  Left = 339
  Top = 185
  Caption = 'SPE-D_BAL-011 :: Importa'#231#227'o de Balan'#231'o para o SPED'
  ClientHeight = 525
  ClientWidth = 998
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 998
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 950
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 902
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 446
        Height = 32
        Caption = 'Importa'#231#227'o de Balan'#231'o para o SPED'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 446
        Height = 32
        Caption = 'Importa'#231#227'o de Balan'#231'o para o SPED'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 446
        Height = 32
        Caption = 'Importa'#231#227'o de Balan'#231'o para o SPED'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 998
    Height = 352
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 998
      Height = 352
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 529
        Height = 352
        Align = alLeft
        TabOrder = 0
        object PnPeriodo: TPanel
          Left = 2
          Top = 15
          Width = 525
          Height = 42
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LaMes: TLabel
            Left = 48
            Top = 0
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
          end
          object LaAno: TLabel
            Left = 240
            Top = 0
            Width = 22
            Height = 13
            Caption = 'Ano:'
          end
          object LaData: TLabel
            Left = 336
            Top = 0
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object CBMes: TComboBox
            Left = 48
            Top = 17
            Width = 182
            Height = 21
            Color = clWhite
            DropDownCount = 12
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            Text = 'CBMes'
          end
          object CBAno: TComboBox
            Left = 240
            Top = 17
            Width = 90
            Height = 21
            Color = clWhite
            DropDownCount = 3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Text = 'CBAno'
          end
          object TPData: TdmkEditDateTimePicker
            Left = 336
            Top = 16
            Width = 112
            Height = 21
            Date = 42471.359051006950000000
            Time = 42471.359051006950000000
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object PCRegistro: TPageControl
          Left = 2
          Top = 102
          Width = 525
          Height = 248
          ActivePage = TabSheet3
          Align = alClient
          TabOrder = 1
          OnChanging = PCRegistroChanging
          object TabSheet1: TTabSheet
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
          end
          object TabSheet2: TTabSheet
            Caption = 'H010 - Invent'#225'rio'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 517
              Height = 220
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object GBImportar1: TGroupBox
                Left = 40
                Top = 4
                Width = 461
                Height = 105
                Caption = ' Uso e consumo: '
                TabOrder = 0
                Visible = False
                object RGNivPlaCta1: TRadioGroup
                  Left = 2
                  Top = 15
                  Width = 457
                  Height = 41
                  Align = alTop
                  Caption = ' N'#237'vel do plano de contas: '
                  Columns = 6
                  ItemIndex = 0
                  Items.Strings = (
                    'Nenhum'
                    'Conta'
                    'Sub-grupo'
                    'Grupo'
                    'Conjunto'
                    'Plano')
                  TabOrder = 0
                  OnClick = RGNivPlaCta1Click
                end
                object Panel8: TPanel
                  Left = 2
                  Top = 56
                  Width = 457
                  Height = 47
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label1: TLabel
                    Left = 4
                    Top = 4
                    Width = 140
                    Height = 13
                    Caption = 'G'#234'nero do n'#237'vel selecionado:'
                  end
                  object EdGenPlaCta1: TdmkEditCB
                    Left = 4
                    Top = 20
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBGenPlaCta1
                    IgnoraDBLookupComboBox = False
                  end
                  object CBGenPlaCta1: TdmkDBLookupComboBox
                    Left = 60
                    Top = 20
                    Width = 392
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsNivelSel1
                    TabOrder = 1
                    dmkEditCB = EdGenPlaCta1
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
              end
              object GBImportar2: TGroupBox
                Left = 40
                Top = 112
                Width = 461
                Height = 105
                Caption = ' Couros (VS) '
                TabOrder = 1
                Visible = False
                object RGNivPlaCta2: TRadioGroup
                  Left = 2
                  Top = 15
                  Width = 457
                  Height = 41
                  Align = alTop
                  Caption = ' N'#237'vel do plano de contas: '
                  Columns = 6
                  ItemIndex = 0
                  Items.Strings = (
                    'Nenhum'
                    'Conta'
                    'Sub-grupo'
                    'Grupo'
                    'Conjunto'
                    'Plano')
                  TabOrder = 0
                  OnClick = RGNivPlaCta2Click
                end
                object Panel9: TPanel
                  Left = 2
                  Top = 56
                  Width = 457
                  Height = 47
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label2: TLabel
                    Left = 4
                    Top = 4
                    Width = 140
                    Height = 13
                    Caption = 'G'#234'nero do n'#237'vel selecionado:'
                  end
                  object EdGenPlaCta2: TdmkEditCB
                    Left = 4
                    Top = 20
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBGenPlaCta2
                    IgnoraDBLookupComboBox = False
                  end
                  object CBGenPlaCta2: TdmkDBLookupComboBox
                    Left = 60
                    Top = 20
                    Width = 392
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsNivelSel2
                    TabOrder = 1
                    dmkEditCB = EdGenPlaCta2
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
              end
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'K200 - Estoque'
            ImageIndex = 2
            object PageControl1: TPageControl
              Left = 0
              Top = 45
              Width = 517
              Height = 175
              ActivePage = TabSheet6
              Align = alClient
              TabOrder = 0
              OnChanging = PCRegistroChanging
              object TabSheet5: TTabSheet
                Caption = 'Uso e consumo'
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
              end
              object TabSheet6: TTabSheet
                Caption = 'Couro'
                ImageIndex = 2
                object DBGrid1: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 509
                  Height = 147
                  Align = alClient
                  DataSource = DsVMI
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                end
              end
            end
            object CGRegistros: TdmkCheckGroup
              Left = 0
              Top = 0
              Width = 517
              Height = 45
              Align = alTop
              Caption = ' Registros a importar: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'K200 - Normal'
                'K280 - Corre'#231#227'o')
              TabOrder = 1
              OnClick = CGImportarClick
              UpdType = utYes
              Value = 1
              OldValor = 0
            end
          end
        end
        object CGImportar: TdmkCheckGroup
          Left = 2
          Top = 57
          Width = 525
          Height = 45
          Align = alTop
          Caption = ' Itens a importar: '
          Columns = 2
          Items.Strings = (
            'Uso e Consumo'
            'Couros (VS)')
          TabOrder = 2
          OnClick = CGImportarClick
          UpdType = utYes
          Value = 0
          OldValor = 0
        end
      end
      object MeAviso: TMemo
        Left = 529
        Top = 0
        Width = 469
        Height = 352
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Lines.Strings = (
          'MeAviso')
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 400
    Width = 998
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 994
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 994
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 455
    Width = 998
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 852
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 850
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 16
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPQx: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT /*pqc.Controle PQC,*/ pqx.Insumo, '
      'ggx.Controle GraGruX, '
      'SUM(pqx.Peso) Peso, SUM(pqx.Valor) Valor '
      'FROM pqx pqx'
      '/*LEFT JOIN pqcli pqc ON pqc.PQ=pqx.Insumo '
      '  AND pqc.CI=pqx.CliOrig*/'
      'LEFT JOIN gragrux ggx ON ggx.GraGru1=pqx.Insumo'
      'WHERE pqx.CliOrig=-11 '
      'AND pqx.Tipo=0 '
      'AND pqx.DataX="2014-01-01" '
      'GROUP BY pqx.Insumo, GraGruX '
      '')
    Left = 512
    Top = 52
    object QrPQxInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQxCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQxPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPQxValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    Left = 512
    Top = 100
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrGraGruXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGruXTipo_Item: TIntegerField
      FieldName = 'Tipo_Item'
    end
  end
  object QrX999: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM efd_h010'
      'WHERE ImporExpor=3'
      'AND AnoMes=201401'
      'AND Empresa=-11'
      'AND H005=1'
      'AND BalID=4'
      'AND BalNum=0'
      'AND BalItm=0')
    Left = 512
    Top = 148
  end
  object QrNivelSel1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 512
    Top = 196
    object QrNivelSel1Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.contas.Codigo'
    end
    object QrNivelSel1Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.contas.Nome'
      Size = 25
    end
  end
  object DsNivelSel1: TDataSource
    DataSet = QrNivelSel1
    Left = 512
    Top = 244
  end
  object QrNivelSel2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 512
    Top = 292
    object QrNivelSel2Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.contas.Codigo'
    end
    object QrNivelSel2Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.contas.Nome'
      Size = 25
    end
  end
  object DsNivelSel2: TDataSource
    DataSet = QrNivelSel2
    Left = 512
    Top = 340
  end
  object QrVMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT med.Sigla, CouNiv2, GraGruY, GragRuX, GraGru1, IF((GraGru' +
        'Y<2048 OR  '
      'CouNiv2<>1) AND SdoVrtPeso > 0, "Kg",   '
      'IF(CouNiv2=1 AND SdoVrtArM2 > 0, "m'#178'", "Erro!")) UnMedTxt,   '
      'IF((GraGruY<2048 OR  '
      'CouNiv2<>1) AND SdoVrtPeso > 0, 1,   '
      'IF(CouNiv2=1 AND SdoVrtArM2 > 0, 2, -1)) TipoEstq,   '
      'SdoVrtPeca,   '
      'SdoVrtArM2,   '
      'SdoVrtPeso   '
      'FROM _vsmovimp1_ vmi  '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragru1 gg1 ON gg1.Nivel1=vmi.' +
        'GraGru1  '
      
        'LEFT JOIN bluederm_2_1_cialeather.UnidMed med ON med.Codigo=gg1.' +
        'UnidMed  '
      '  '
      'WHERE SdoVrtPeca > 0  '
      'AND (  '
      '  ((GraGruY<2048 OR CouNiv2<>1) AND SdoVrtPeso > 0)  '
      '   OR  '
      '  (CouNiv2=1 AND SdoVrtArM2 > 0)  '
      ')  ')
    Left = 512
    Top = 388
    object QrVMISigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrVMICouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrVMIGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVMIGragRuX: TIntegerField
      FieldName = 'GragRuX'
    end
    object QrVMIGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVMIUnMedTxt: TWideStringField
      FieldName = 'UnMedTxt'
      Required = True
      Size = 5
    end
    object QrVMITipoEstq: TLargeintField
      FieldName = 'TipoEstq'
      Required = True
    end
    object QrVMISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVMISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVMISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVMINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 512
    end
    object QrVMIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVMIGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrVMIUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrVMIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVMIValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVMIEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
    object QrVMISitProd: TSmallintField
      FieldName = 'SitProd'
    end
    object QrVMIClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVMITipo_Item: TIntegerField
      FieldName = 'Tipo_Item'
    end
  end
  object DsVMI: TDataSource
    DataSet = QrVMI
    Left = 512
    Top = 436
  end
  object QrVMI2: TmySQLQuery
    Database = Dmod.MyDB
    Left = 660
    Top = 128
    object QrVMI2IMEI: TIntegerField
      FieldName = 'IMEI'
    end
  end
  object QrCA_VS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT scc.EntiSitio, MONTH(vmi.DtCorrApo) Mes, YEAR(vmi.DtCorrA' +
        'po) Ano, '
      
        'vmi.GraGruX, SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, SUM(AreaM2) A' +
        'reaM2'
      'FROM vsmovits vmi '
      'LEFT JOIN stqcenloc scl ON scl.Controle=vmi.StqCenLoc'
      'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo'
      
        'WHERE vmi.DataHora BETWEEN "2017-02-01" AND "2017-02-28 23:59:59' +
        '"'
      'AND vmi.DtCorrApo > "1900-01-01"'
      'AND vmi.Empresa=-11'
      'AND vmi.DtCorrApo < "2017-02-01"'
      'AND MovimID IN (1,16,21,22)'
      'GROUP BY EntiSitio, Mes, Ano, GraGruX')
    Left = 196
    Top = 308
    object QrCA_VSEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
    object QrCA_VSGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrCA_VSPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrCA_VSPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrCA_VSAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrCA_VSMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrCA_VSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCA_VSMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrCA_VSMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrCA_VSControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCA_VSMes: TLargeintField
      FieldName = 'Mes'
    end
    object QrCA_VSAno: TLargeintField
      FieldName = 'Ano'
    end
    object QrCA_VSGrandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrCA_VSDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrCA_VSDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrCA_VSClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrCA_VSFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
  end
end
