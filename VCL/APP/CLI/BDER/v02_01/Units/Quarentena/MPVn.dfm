object FmMPVn: TFmMPVn
  Left = 364
  Top = 167
  Caption = 'VEN-COURO-151 :: Faturamento de Pedidos'
  ClientHeight = 646
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 600
  Constraints.MinWidth = 1024
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 90
    Width = 1008
    Height = 556
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 49
      Width = 1008
      Height = 256
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 20
        Top = -2
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 148
        Top = -2
        Width = 35
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente:'
      end
      object Label3: TLabel
        Left = 148
        Top = 39
        Width = 75
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Transportadora:'
      end
      object Label5: TLabel
        Left = 20
        Top = 39
        Width = 26
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data:'
      end
      object Label22: TLabel
        Left = 148
        Top = 80
        Width = 49
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Vendedor:'
      end
      object Label23: TLabel
        Left = 20
        Top = 80
        Width = 59
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Comiss'#227'o %:'
      end
      object LaDescoExtra: TLabel
        Left = 20
        Top = 122
        Width = 75
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Desconto extra:'
        Enabled = False
      end
      object Label29: TLabel
        Left = 148
        Top = 122
        Width = 43
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Volumes:'
      end
      object Label30: TLabel
        Left = 20
        Top = 168
        Width = 66
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Observa'#231#245'es:'
      end
      object EdCodigo: TdmkEdit
        Left = 20
        Top = 14
        Width = 123
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 148
        Top = 14
        Width = 65
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 215
        Top = 14
        Width = 377
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientes
        TabOrder = 2
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTransp: TdmkEditCB
        Left = 148
        Top = 55
        Width = 65
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransp
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransp: TdmkDBLookupComboBox
        Left = 215
        Top = 55
        Width = 377
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsTransp
        TabOrder = 5
        dmkEditCB = EdTransp
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPDataF: TDateTimePicker
        Left = 20
        Top = 55
        Width = 124
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 38579.000000000000000000
        Time = 0.624225613399175900
        TabOrder = 3
      end
      object EdVendedor: TdmkEditCB
        Left = 148
        Top = 96
        Width = 65
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBVendedor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBVendedor: TdmkDBLookupComboBox
        Left = 215
        Top = 96
        Width = 377
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsVendedores
        TabOrder = 8
        dmkEditCB = EdVendedor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdComissV_Per: TdmkEdit
        Left = 20
        Top = 96
        Width = 124
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdDescoExtra: TdmkEdit
        Left = 20
        Top = 141
        Width = 124
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdDescoExtraExit
      end
      object EdVolumes: TdmkEdit
        Left = 148
        Top = 141
        Width = 124
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Memo1: TMemo
        Left = 20
        Top = 186
        Width = 934
        Height = 63
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 254
        TabOrder = 11
      end
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 477
      Width = 1008
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object PainelConfirma: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 62
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 10
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
        object Panel10: TPanel
          Left = 840
          Top = 0
          Width = 164
          Height = 62
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste0: TBitBtn
            Tag = 15
            Left = 5
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesiste0Click
          end
        end
      end
    end
    object Panel12: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Label7: TLabel
        Left = 20
        Top = 5
        Width = 44
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 20
        Top = 21
        Width = 65
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 89
        Top = 21
        Width = 503
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 90
    Width = 1008
    Height = 556
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 277
      Width = 1008
      Height = 4
      Cursor = crVSplit
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      ExplicitTop = 300
      ExplicitWidth = 967
    end
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 189
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 20
        Top = 10
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 148
        Top = 10
        Width = 35
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 148
        Top = 51
        Width = 75
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Transportadora:'
        FocusControl = DBEdit01
      end
      object Label6: TLabel
        Left = 20
        Top = 51
        Width = 26
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data:'
        FocusControl = DBEdit02
      end
      object Label17: TLabel
        Left = 596
        Top = 10
        Width = 58
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Quantidade:'
        FocusControl = DBEdit1
      end
      object Label18: TLabel
        Left = 689
        Top = 10
        Width = 27
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Valor:'
        FocusControl = DBEdit2
      end
      object Label19: TLabel
        Left = 148
        Top = 92
        Width = 49
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Vendedor:'
        FocusControl = DBEdit03
      end
      object Label20: TLabel
        Left = 596
        Top = 92
        Width = 59
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Comiss'#227'o %:'
        FocusControl = DBEdit04
      end
      object Label21: TLabel
        Left = 689
        Top = 92
        Width = 27
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Valor:'
        FocusControl = DBEdit05
      end
      object Label26: TLabel
        Left = 783
        Top = 10
        Width = 55
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Desc.Extra:'
        FocusControl = DBEdit06
      end
      object Label27: TLabel
        Left = 876
        Top = 10
        Width = 27
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Total:'
        FocusControl = DBEdit07
      end
      object Label28: TLabel
        Left = 596
        Top = 51
        Width = 43
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Volumes:'
        FocusControl = DBEdit08
      end
      object Label13: TLabel
        Left = 20
        Top = 92
        Width = 71
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente Interno:'
        FocusControl = DBEdit13
      end
      object DBEdCodigo: TDBEdit
        Left = 20
        Top = 26
        Width = 123
        Height = 21
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsMPV
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        MaxLength = 4
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 148
        Top = 26
        Width = 444
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'NOMECLIENTE'
        DataSource = DsMPV
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 44
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit01: TDBEdit
        Left = 148
        Top = 67
        Width = 444
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'NOMETRANSP'
        DataSource = DsMPV
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 29
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
      object DBEdit02: TDBEdit
        Left = 20
        Top = 67
        Width = 124
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'DataF'
        DataSource = DsMPV
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
      end
      object DBEdit1: TDBEdit
        Left = 596
        Top = 26
        Width = 88
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Qtde'
        DataSource = DsMPV
        MaxLength = 5
        TabOrder = 4
      end
      object DBEdit2: TDBEdit
        Left = 689
        Top = 26
        Width = 89
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Valor'
        DataSource = DsMPV
        MaxLength = 6
        TabOrder = 5
      end
      object DBEdit03: TDBEdit
        Left = 148
        Top = 108
        Width = 444
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'NOMEVENDEDOR'
        DataSource = DsMPV
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 29
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
      end
      object DBEdit04: TDBEdit
        Left = 596
        Top = 108
        Width = 88
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'ComissV_Per'
        DataSource = DsMPV
        MaxLength = 6
        TabOrder = 7
      end
      object DBEdit05: TDBEdit
        Left = 689
        Top = 108
        Width = 89
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'ComissV_Val'
        DataSource = DsMPV
        MaxLength = 4
        TabOrder = 8
      end
      object DBEdit06: TDBEdit
        Left = 783
        Top = 26
        Width = 88
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'DescoExtra'
        DataSource = DsMPV
        MaxLength = 1
        TabOrder = 9
      end
      object DBEdit07: TDBEdit
        Left = 876
        Top = 26
        Width = 89
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TOTAL'
        DataSource = DsMPV
        TabOrder = 10
      end
      object DBEdit08: TDBEdit
        Left = 596
        Top = 67
        Width = 88
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Volumes'
        DataSource = DsMPV
        MaxLength = 1
        TabOrder = 11
      end
      object DBMemo1: TDBMemo
        Left = 0
        Top = 136
        Width = 1008
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        DataField = 'Obs'
        DataSource = DsMPV
        TabOrder = 12
      end
      object DBEdit13: TDBEdit
        Left = 20
        Top = 108
        Width = 124
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'EntInt'
        DataSource = DsMPV
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 13
      end
    end
    object DBGrid4: TDBGrid
      Left = 0
      Top = 189
      Width = 1008
      Height = 88
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      DataSource = DsMPVIts
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMEMP'
          Title.Caption = 'Artigo'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Title.Caption = 'Descri'#231#227'o'
          Width = 232
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CorTxt'
          Title.Caption = 'Cor'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Classe'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EspesTxt'
          Title.Caption = 'Espessura'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Title.Caption = 'Quantidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Caption = 'Pre'#231'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SubTo'
          Title.Caption = 'Sub-total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Desco'
          Title.Caption = 'Desconto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Title.Caption = 'Total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Entrega'
          Title.Caption = 'Entregar em'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'OS'
          Visible = True
        end>
    end
    object DBGrid1: TDBGrid
      Left = -9
      Top = 335
      Width = 973
      Height = 90
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataSource = DsEmissC
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'SEQ'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECARTEIRA'
          Title.Caption = 'Carteira'
          Width = 300
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'COMPENSA_TXT'
          Title.Alignment = taCenter
          Title.Caption = 'Compensado'
          Width = 72
          Visible = True
        end>
    end
    object GBTrava: TGroupBox
      Left = 0
      Top = 424
      Width = 1008
      Height = 65
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 3
      Visible = False
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 48
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel6: TPanel
          Left = 827
          Top = 0
          Width = 177
          Height = 48
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
        end
        object BtTrava: TBitBtn
          Tag = 14
          Left = 10
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Trava'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtTravaClick
        end
        object BtMercadoria: TBitBtn
          Tag = 224
          Left = 236
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Mercad.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtMercadoriaClick
        end
        object BtPagamento: TBitBtn
          Tag = 20
          Left = 350
          Top = 5
          Width = 110
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Pagto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtPagamentoClick
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 489
      Width = 1008
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 4
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 179
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 134
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 92
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 51
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 181
        Top = 15
        Width = 184
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 365
        Top = 15
        Width = 641
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel11: TPanel
          Left = 508
          Top = 0
          Width = 133
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 118
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtImprime: TBitBtn
          Tag = 5
          Left = 231
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'I&mprime'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtImprimeClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 345
          Top = 5
          Width = 110
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtExcluiClick
        end
      end
    end
  end
  object PnManual: TPanel
    Left = 0
    Top = 90
    Width = 1008
    Height = 556
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    Visible = False
    object Painel1: TPanel
      Left = 1
      Top = 1
      Width = 1009
      Height = 237
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      BevelOuter = bvNone
      TabOrder = 0
      object CkContinuar1: TCheckBox
        Left = 15
        Top = 156
        Width = 149
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Continuar inserindo.'
        TabOrder = 1
      end
      object GroupBox2: TGroupBox
        Left = 15
        Top = 10
        Width = 749
        Height = 143
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Ordem de produ'#231#227'o: '
        TabOrder = 0
        object Label50: TLabel
          Left = 10
          Top = 20
          Width = 56
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Mercadoria:'
        end
        object Label51: TLabel
          Left = 10
          Top = 57
          Width = 30
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Texto:'
        end
        object Label52: TLabel
          Left = 10
          Top = 98
          Width = 34
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Classe:'
        end
        object Label54: TLabel
          Left = 601
          Top = 20
          Width = 40
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Entrega:'
        end
        object Label55: TLabel
          Left = 108
          Top = 98
          Width = 52
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Espessura:'
        end
        object Label56: TLabel
          Left = 527
          Top = 57
          Width = 19
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cor:'
        end
        object Label8: TLabel
          Left = 207
          Top = 98
          Width = 58
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Quantidade:'
        end
        object Label11: TLabel
          Left = 305
          Top = 98
          Width = 31
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Pre'#231'o:'
        end
        object Label24: TLabel
          Left = 404
          Top = 98
          Width = 45
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Sub-total:'
        end
        object Label25: TLabel
          Left = 512
          Top = 98
          Width = 49
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Desconto:'
        end
        object Label12: TLabel
          Left = 610
          Top = 98
          Width = 27
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Total:'
        end
        object EdMP: TdmkEditCB
          Left = 10
          Top = 35
          Width = 65
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBMP
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBMP: TdmkDBLookupComboBox
          Left = 78
          Top = 35
          Width = 519
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsMPs
          TabOrder = 1
          dmkEditCB = EdMP
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdTexto: TdmkEdit
          Left = 10
          Top = 73
          Width = 513
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 50
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdClasse: TdmkEdit
          Left = 10
          Top = 114
          Width = 95
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 15
          ParentFont = False
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object TPEntrega: TDateTimePicker
          Left = 601
          Top = 35
          Width = 119
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 39013.000000000000000000
          Time = 0.576212557898543300
          TabOrder = 2
        end
        object EdEspesTxt: TdmkEdit
          Left = 108
          Top = 114
          Width = 95
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCorTxt: TdmkEdit
          Left = 527
          Top = 73
          Width = 193
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 15
          ParentFont = False
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdQtde1: TdmkEdit
          Left = 207
          Top = 114
          Width = 95
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdQtde1Change
          OnExit = EdQtde1Exit
        end
        object EdPreco1: TdmkEdit
          Left = 305
          Top = 114
          Width = 95
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdPreco1Change
          OnExit = EdPreco1Exit
        end
        object EdSubTo1: TdmkEdit
          Left = 404
          Top = 114
          Width = 104
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdDesco1: TdmkEdit
          Left = 512
          Top = 114
          Width = 95
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdDesco1Exit
        end
        object EdValor1: TdmkEdit
          Left = 610
          Top = 114
          Width = 110
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 11
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 486
      Width = 1008
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object Panel16: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 827
          Top = 0
          Width = 177
          Height = 53
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste1: TBitBtn
            Tag = 15
            Left = 15
            Top = 5
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesiste1Click
          end
        end
        object BtConfirma2: TBitBtn
          Tag = 14
          Left = 15
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirma2Click
        end
      end
    end
  end
  object PnPedido: TPanel
    Left = 0
    Top = 90
    Width = 1008
    Height = 556
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    Visible = False
    ExplicitLeft = 248
    ExplicitTop = 118
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 421
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Panel7: TPanel
        Left = 0
        Top = 57
        Width = 1008
        Height = 364
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel14: TPanel
          Left = 0
          Top = 299
          Width = 1008
          Height = 65
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
        end
        object GBProducao: TGroupBox
          Left = 0
          Top = 0
          Width = 1008
          Height = 241
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = ' Ordem de produ'#231#227'o: '
          Enabled = False
          TabOrder = 0
          Visible = False
          object Label37: TLabel
            Left = 10
            Top = 57
            Width = 56
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Mercadoria:'
          end
          object Label38: TLabel
            Left = 394
            Top = 57
            Width = 30
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Texto:'
          end
          object Label39: TLabel
            Left = 10
            Top = 139
            Width = 34
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Classe:'
          end
          object Label40: TLabel
            Left = 103
            Top = 98
            Width = 39
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = #193'rea m'#178':'
          end
          object Label41: TLabel
            Left = 10
            Top = 181
            Width = 40
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Entrega:'
          end
          object Label42: TLabel
            Left = 108
            Top = 139
            Width = 52
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Espessura:'
          end
          object Label44: TLabel
            Left = 118
            Top = 181
            Width = 45
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pre'#231'o m'#178':'
          end
          object Label33: TLabel
            Left = 773
            Top = 57
            Width = 19
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cor:'
            FocusControl = DBEdit6
          end
          object Label36: TLabel
            Left = 10
            Top = 20
            Width = 35
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente:'
            FocusControl = DBEdCliente
          end
          object Label43: TLabel
            Left = 507
            Top = 20
            Width = 49
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vendedor:'
            FocusControl = DBEdVendedor
          end
          object Label45: TLabel
            Left = 10
            Top = 98
            Width = 36
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pedido:'
            FocusControl = DBEdit9
          end
          object Label46: TLabel
            Left = 202
            Top = 98
            Width = 66
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Observa'#231#245'es:'
          end
          object DBEdit3: TDBEdit
            Left = 10
            Top = 73
            Width = 59
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'MP'
            DataSource = DsPed
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 69
            Top = 73
            Width = 320
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMEMP'
            DataSource = DsPed
            TabOrder = 1
          end
          object DBEdit5: TDBEdit
            Left = 394
            Top = 73
            Width = 374
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Texto'
            DataSource = DsPed
            TabOrder = 2
          end
          object DBEdit6: TDBEdit
            Left = 773
            Top = 73
            Width = 178
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CorTxt'
            DataSource = DsPed
            TabOrder = 3
          end
          object DBEdCliente: TDBEdit
            Left = 10
            Top = 35
            Width = 492
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMECLI'
            DataSource = DsPed
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
          end
          object DBEdVendedor: TDBEdit
            Left = 507
            Top = 35
            Width = 444
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMEVEN'
            DataSource = DsPed
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
          end
          object DBEdit9: TDBEdit
            Left = 10
            Top = 114
            Width = 90
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PEDIDO'
            DataSource = DsPed
            TabOrder = 6
          end
          object DBMemo2: TDBMemo
            Left = 202
            Top = 114
            Width = 749
            Height = 124
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Observ'
            DataSource = DsPed
            TabOrder = 7
          end
          object DBEdit7: TDBEdit
            Left = 103
            Top = 114
            Width = 95
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'M2Pedido'
            DataSource = DsPed
            TabOrder = 8
          end
          object DBEdit8: TDBEdit
            Left = 108
            Top = 155
            Width = 90
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'EspesTxt'
            DataSource = DsPed
            TabOrder = 9
          end
          object DBEdit10: TDBEdit
            Left = 10
            Top = 155
            Width = 95
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Classe'
            DataSource = DsPed
            TabOrder = 10
          end
          object DBEdit11: TDBEdit
            Left = 118
            Top = 196
            Width = 79
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PrecoPed'
            DataSource = DsPed
            TabOrder = 11
          end
          object DBEdit12: TDBEdit
            Left = 10
            Top = 196
            Width = 104
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Entrega'
            DataSource = DsPed
            TabOrder = 12
          end
        end
        object PCObs: TPageControl
          Left = 0
          Top = 241
          Width = 1008
          Height = 120
          ActivePage = TabSheet1
          Align = alTop
          TabHeight = 25
          TabOrder = 2
          Visible = False
          object TabSheet1: TTabSheet
            Caption = 'Observa'#231#245'es do pedido'
            object DBMemo3: TDBMemo
              Left = 0
              Top = 0
              Width = 1000
              Height = 85
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataField = 'Obz'
              DataSource = DsPed
              TabOrder = 0
            end
          end
          object TSFatParc: TTabSheet
            Caption = 'Faturamento parcial'
            ImageIndex = 1
            object LaFatParcEntrega: TLabel
              Left = 5
              Top = 25
              Width = 69
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Nova Entrega:'
            end
            object LaFatParcArea: TLabel
              Left = 132
              Top = 25
              Width = 75
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Qtde. Restante:'
            end
            object CkFatParc: TdmkCheckBox
              Left = 3
              Top = 3
              Width = 134
              Height = 17
              Caption = 'Fatura parcial'
              TabOrder = 0
              OnClick = CkFatParcClick
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
            object TPFatParcEntrega: TDateTimePicker
              Left = 5
              Top = 45
              Width = 120
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 39013.000000000000000000
              Time = 0.576212557898543300
              TabOrder = 1
            end
            object EdFatParcArea: TdmkEdit
              Left = 132
              Top = 45
              Width = 95
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 126
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label001: TLabel
            Left = 5
            Top = 5
            Width = 22
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'OS:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object LaOS_Cli: TLabel
            Left = 34
            Top = 5
            Width = 28
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '[F4]'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object LaOS_All: TLabel
            Left = 64
            Top = 5
            Width = 28
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '[F5]'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object LaControle: TLabel
            Left = 94
            Top = 5
            Width = 28
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '[F6]'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EdControle: TdmkEdit
            Left = 5
            Top = 25
            Width = 114
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdControleChange
            OnKeyDown = EdControleKeyDown
          end
          object CBOS_Cli: TDBLookupComboBox
            Left = -84
            Top = 17
            Width = 114
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Controle'
            ListField = 'Controle'
            ListSource = DsPesCli
            TabOrder = 1
            Visible = False
            OnKeyDown = CBOS_CliKeyDown
          end
          object CBOS_All: TDBLookupComboBox
            Left = -71
            Top = 33
            Width = 114
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Controle'
            ListField = 'Controle'
            ListSource = DsPesAll
            TabOrder = 2
            Visible = False
            OnKeyDown = CBOS_AllKeyDown
          end
        end
        object Panel9: TPanel
          Left = 126
          Top = 0
          Width = 882
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = 
            '          Informe o n'#250'mero da OS para editar os dados de faturam' +
            'ento.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaption
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          object PnDadosFat: TPanel
            Left = 0
            Top = 0
            Width = 882
            Height = 57
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentBackground = False
            ParentFont = False
            TabOrder = 0
            Visible = False
            ExplicitLeft = 32
            ExplicitTop = -4
            object Label15: TLabel
              Left = 5
              Top = 9
              Width = 58
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Quantidade:'
            end
            object Label16: TLabel
              Left = 103
              Top = 9
              Width = 31
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Pre'#231'o:'
            end
            object Label34: TLabel
              Left = 202
              Top = 9
              Width = 45
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Sub-total:'
            end
            object Label35: TLabel
              Left = 300
              Top = 9
              Width = 49
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Desconto:'
            end
            object Label32: TLabel
              Left = 399
              Top = 9
              Width = 27
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Total:'
            end
            object EdQtde2: TdmkEdit
              Left = 9
              Top = 25
              Width = 95
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdQtde2Change
              OnExit = EdQtde2Exit
            end
            object EdPreco2: TdmkEdit
              Left = 103
              Top = 25
              Width = 95
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdPreco2Change
              OnExit = EdPreco2Exit
            end
            object EdSubTo2: TdmkEdit
              Left = 202
              Top = 25
              Width = 95
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdDesco2: TdmkEdit
              Left = 300
              Top = 25
              Width = 95
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnExit = EdDesco2Exit
            end
            object EdValor2: TdmkEdit
              Left = 399
              Top = 25
              Width = 95
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object CkContinuar2: TCheckBox
              Left = 502
              Top = 25
              Width = 149
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Continuar inserindo.'
              Checked = True
              State = cbChecked
              TabOrder = 5
            end
          end
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 488
      Width = 1008
      Height = 68
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object Panel17: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 51
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label49: TLabel
          Left = 452
          Top = 8
          Width = 138
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[F6]: Digita'#231#227'o da OS aberta.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label48: TLabel
          Left = 164
          Top = 30
          Width = 208
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[F5]: Lista de OSs abertas de todos clientes.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label14: TLabel
          Left = 164
          Top = 8
          Width = 174
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[F4]: Lista de OSs abertas do cliente.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Panel18: TPanel
          Left = 835
          Top = 0
          Width = 169
          Height = 51
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste2: TBitBtn
            Tag = 15
            Left = 7
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesiste2Click
          end
        end
        object BitBtn1: TBitBtn
          Tag = 14
          Left = 10
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn1Click
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object GB_R: TGroupBox
      Left = 957
      Top = 0
      Width = 51
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 6
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 221
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 10
        Top = 2
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 50
        Top = 2
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 90
        Top = 2
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 2
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 2
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 221
      Top = 0
      Width = 736
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 297
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Faturamento de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 297
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Faturamento de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 297
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Faturamento de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 47
    Width = 1008
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 5
    object Panel15: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsMPV: TDataSource
    DataSet = QrMPV
    Left = 572
    Top = 17
  end
  object QrMPV: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrMPVBeforeOpen
    BeforeClose = QrMPVBeforeClose
    AfterScroll = QrMPVAfterScroll
    OnCalcFields = QrMPVCalcFields
    SQL.Strings = (
      'SELECT '
      'CASE WHEN ve.Tipo=0 THEN ve.RazaoSocial'
      'ELSE ve.Nome END NOMEVENDEDOR, '
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMECLIENTE, '
      'CASE WHEN tr.Tipo=0 THEN tr.RazaoSocial'
      'ELSE tr.Nome END NOMETRANSP,'
      'mpv.* '
      'FROM mpv mpv'
      'LEFT JOIN entidades cl ON cl.Codigo=mpv.Cliente'
      'LEFT JOIN entidades tr ON tr.Codigo=mpv.Transp'
      'LEFT JOIN entidades ve ON ve.Codigo=mpv.Vendedor'
      'WHERE mpv.Codigo > 0')
    Left = 544
    Top = 17
    object QrMPVNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrMPVNOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrMPVNOMEVENDEDOR: TWideStringField
      FieldName = 'NOMEVENDEDOR'
      Size = 100
    end
    object QrMPVCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mpv.Codigo'
    end
    object QrMPVCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'mpv.Cliente'
    end
    object QrMPVTransp: TIntegerField
      FieldName = 'Transp'
      Origin = 'mpv.Transp'
    end
    object QrMPVDataF: TDateField
      FieldName = 'DataF'
      Origin = 'mpv.DataF'
    end
    object QrMPVLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'mpv.Lk'
    end
    object QrMPVDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'mpv.DataCad'
    end
    object QrMPVDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'mpv.DataAlt'
    end
    object QrMPVUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'mpv.UserCad'
    end
    object QrMPVUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'mpv.UserAlt'
    end
    object QrMPVQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'mpv.Qtde'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVValor: TFloatField
      FieldName = 'Valor'
      Origin = 'mpv.Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVVendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'mpv.Vendedor'
    end
    object QrMPVComissV_Per: TFloatField
      FieldName = 'ComissV_Per'
      Origin = 'mpv.ComissV_Per'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrMPVComissV_Val: TFloatField
      FieldName = 'ComissV_Val'
      Origin = 'mpv.ComissV_Val'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVDescoExtra: TFloatField
      FieldName = 'DescoExtra'
      Origin = 'mpv.DescoExtra'
    end
    object QrMPVTOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL'
      Calculated = True
    end
    object QrMPVVolumes: TIntegerField
      FieldName = 'Volumes'
      Origin = 'mpv.Volumes'
    end
    object QrMPVObs: TWideStringField
      FieldName = 'Obs'
      Origin = 'mpv.Obs'
      Size = 255
    end
    object QrMPVEntInt: TIntegerField
      FieldName = 'EntInt'
    end
    object QrMPVCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
  end
  object QrTransp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 477
    Top = 409
    object QrTranspCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTranspNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransp: TDataSource
    DataSet = QrTransp
    Left = 529
    Top = 377
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 709
    Top = 61
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 737
    Top = 61
  end
  object QrMPs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM artigosgrupos'
      'ORDER BY Nome'
      '')
    Left = 277
    Top = 105
    object QrMPsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMPs: TDataSource
    DataSet = QrMPs
    Left = 305
    Top = 105
  end
  object QrMPVIts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMPVItsCalcFields
    SQL.Strings = (
      'SELECT ag.Nome NOMEMP, mvi.* '
      'FROM mpvits mvi'
      'LEFT JOIN artigosgrupos ag ON ag.Codigo=mvi.MP'
      'WHERE mvi.Codigo=:P0')
    Left = 604
    Top = 21
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPVItsNOMEMP: TWideStringField
      FieldName = 'NOMEMP'
      Origin = 'artigosgrupos.Nome'
      Required = True
      Size = 50
    end
    object QrMPVItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mpvits.Codigo'
      Required = True
    end
    object QrMPVItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'mpvits.Controle'
      Required = True
    end
    object QrMPVItsMP: TIntegerField
      FieldName = 'MP'
      Origin = 'mpvits.MP'
      Required = True
    end
    object QrMPVItsQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'mpvits.Qtde'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVItsPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'mpvits.Preco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVItsValor: TFloatField
      FieldName = 'Valor'
      Origin = 'mpvits.Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'mpvits.Lk'
    end
    object QrMPVItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'mpvits.DataCad'
    end
    object QrMPVItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'mpvits.DataAlt'
    end
    object QrMPVItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'mpvits.UserCad'
    end
    object QrMPVItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'mpvits.UserAlt'
    end
    object QrMPVItsTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'mpvits.Texto'
      Required = True
      Size = 50
    end
    object QrMPVItsDesco: TFloatField
      FieldName = 'Desco'
      Origin = 'mpvits.Desco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVItsSubTo: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SubTo'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMPVItsEntrega: TDateField
      FieldName = 'Entrega'
      Origin = 'mpvits.Entrega'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPVItsPronto: TDateField
      FieldName = 'Pronto'
      Origin = 'mpvits.Pronto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPVItsPRONTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PRONTO_TXT'
      Size = 10
      Calculated = True
    end
    object QrMPVItsStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'mpvits.Status'
      Required = True
    end
    object QrMPVItsFluxo: TIntegerField
      FieldName = 'Fluxo'
      Origin = 'mpvits.Fluxo'
      Required = True
    end
    object QrMPVItsClasse: TWideStringField
      FieldName = 'Classe'
      Origin = 'mpvits.Classe'
      Size = 15
    end
    object QrMPVItsPedido: TIntegerField
      FieldName = 'Pedido'
      Origin = 'mpvits.Pedido'
      Required = True
    end
    object QrMPVItsEspesTxt: TWideStringField
      FieldName = 'EspesTxt'
      Origin = 'mpvits.EspesTxt'
      Size = 10
    end
    object QrMPVItsCorTxt: TWideStringField
      FieldName = 'CorTxt'
      Origin = 'mpvits.CorTxt'
      Size = 30
    end
    object QrMPVItsM2Pedido: TFloatField
      FieldName = 'M2Pedido'
      Origin = 'mpvits.M2Pedido'
      Required = True
    end
    object QrMPVItsPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'mpvits.Pecas'
      Required = True
    end
    object QrMPVItsUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'mpvits.Unidade'
      Required = True
    end
    object QrMPVItsObserv: TWideMemoField
      FieldName = 'Observ'
      Origin = 'mpvits.Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPVItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'mpvits.AlterWeb'
      Required = True
    end
    object QrMPVItsPrecoPed: TFloatField
      FieldName = 'PrecoPed'
      Origin = 'mpvits.PrecoPed'
      Required = True
    end
    object QrMPVItsValorPed: TFloatField
      FieldName = 'ValorPed'
      Origin = 'mpvits.ValorPed'
      Required = True
    end
  end
  object DsMPVIts: TDataSource
    DataSet = QrMPVIts
    Left = 632
    Top = 21
  end
  object QrTotais: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtde) Qtde, SUM(Valor) Valor'
      'FROM mpvits'
      'WHERE Codigo=:P0')
    Left = 593
    Top = 109
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotaisQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrTotaisValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFornecedorCalcFields
    SQL.Strings = (
      'SELECT '
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOME,'
      'CASE WHEN fo.Tipo=0 THEN fo.ERua'
      'ELSE fo.PRua END RUA,'
      'CASE WHEN fo.Tipo=0 THEN fo.ENumero'
      'ELSE fo.PNumero END NUMERO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECompl'
      'ELSE fo.PCompl END COMPL,'
      'CASE WHEN fo.Tipo=0 THEN fo.EBairro'
      'ELSE fo.PBairro END BAIRRO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECidade'
      'ELSE fo.PCidade END CIDADE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EPais'
      'ELSE fo.PPais END PAIS,'
      'CASE WHEN fo.Tipo=0 THEN fo.ETe1'
      'ELSE fo.PTe1 END TELEFONE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EFax'
      'ELSE fo.PPais END FAX,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECel'
      'ELSE fo.PCel END Celular,'
      'CASE WHEN fo.Tipo=0 THEN fo.CNPJ'
      'ELSE fo.CPF END CNPJ,'
      'CASE WHEN fo.Tipo=0 THEN fo.IE'
      'ELSE fo.RG END IE,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECEP'
      'ELSE fo.PCEP END CEP,'
      'CASE WHEN fo.Tipo=0 THEN fo.EContato'
      'ELSE fo.PContato END Contato,'
      ''
      'CASE WHEN fo.Tipo=0 THEN fo.EUF'
      'ELSE fo.PUF END UF,'
      'CASE WHEN fo.Tipo=0 THEN uf1.Nome'
      'ELSE uf2.Nome END NOMEUF,'
      'EEMail, PEmail,'
      ''
      'CASE WHEN fo.Tipo=0 THEN ll1.Nome'
      'ELSE ll2.Nome END LOGRAD'
      ''
      'FROM entidades fo'
      'LEFT JOIN ufs uf1 ON uf1.Codigo=fo.Euf'
      'LEFT JOIN ufs uf2 ON uf2.Codigo=fo.Puf'
      'LEFT JOIN listalograd ll1 ON ll1.Codigo=fo.ELograd'
      'LEFT JOIN listalograd ll2 ON ll2.Codigo=fo.PLograd'
      'WHERE fo.Codigo=:P0')
    Left = 656
    Top = 400
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFornecedorFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrFornecedorCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Calculated = True
    end
    object QrFornecedorCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Calculated = True
    end
    object QrFornecedorTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrFornecedorCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrFornecedorNOME: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOME'
      Size = 100
    end
    object QrFornecedorRUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 30
    end
    object QrFornecedorCOMPL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'COMPL'
      Size = 30
    end
    object QrFornecedorBAIRRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrFornecedorCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 100
    end
    object QrFornecedorPAIS: TWideStringField
      FieldName = 'PAIS'
      Size = 100
    end
    object QrFornecedorTELEFONE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TELEFONE'
    end
    object QrFornecedorFAX: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FAX'
    end
    object QrFornecedorCelular: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Celular'
    end
    object QrFornecedorCNPJ: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrFornecedorIE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'IE'
    end
    object QrFornecedorContato: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Contato'
      Size = 60
    end
    object QrFornecedorNOMEUF: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrFornecedorEEMail: TWideStringField
      FieldName = 'EEMail'
      Origin = 'entidades.EEmail'
      Size = 100
    end
    object QrFornecedorPEmail: TWideStringField
      FieldName = 'PEmail'
      Origin = 'entidades.PEmail'
      Size = 100
    end
    object QrFornecedorNUMEROTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMEROTXT'
      Size = 40
      Calculated = True
    end
    object QrFornecedorENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrFornecedorLOGRAD: TWideStringField
      FieldName = 'LOGRAD'
      Size = 10
    end
    object QrFornecedorNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrFornecedorCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrFornecedorUF: TFloatField
      FieldName = 'UF'
    end
  end
  object QrTransportador: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTransportadorCalcFields
    SQL.Strings = (
      'SELECT '
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOME,'
      'CASE WHEN fo.Tipo=0 THEN fo.ERua'
      'ELSE fo.PRua END RUA,'
      '(CASE WHEN fo.Tipo=0 THEN fo.ENumero'
      'ELSE fo.PNumero END) + 0.000 NUMERO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECompl'
      'ELSE fo.PCompl END COMPL,'
      'CASE WHEN fo.Tipo=0 THEN fo.EBairro'
      'ELSE fo.PBairro END BAIRRO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECidade'
      'ELSE fo.PCidade END CIDADE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EPais'
      'ELSE fo.PPais END PAIS,'
      'CASE WHEN fo.Tipo=0 THEN fo.ETe1'
      'ELSE fo.PTe1 END TELEFONE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EFax'
      'ELSE fo.PPais END FAX,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECel'
      'ELSE fo.PCel END Celular,'
      'CASE WHEN fo.Tipo=0 THEN fo.CNPJ'
      'ELSE fo.CPF END CNPJ,'
      'CASE WHEN fo.Tipo=0 THEN fo.IE'
      'ELSE fo.RG END IE,'
      '(CASE WHEN fo.Tipo=0 THEN fo.ECEP'
      'ELSE fo.PCEP END) + 0.000 CEP,'
      'CASE WHEN fo.Tipo=0 THEN fo.EContato'
      'ELSE fo.PContato END Contato,'
      ''
      '(CASE WHEN fo.Tipo=0 THEN fo.EUF'
      'ELSE fo.PUF END) + 0.000 UF,'
      'CASE WHEN fo.Tipo=0 THEN uf1.Nome'
      'ELSE uf2.Nome END NOMEUF,'
      'EEMail, PEmail,'
      ''
      'CASE WHEN fo.Tipo=0 THEN ll1.Nome'
      'ELSE ll2.Nome END LOGRAD'
      ''
      'FROM entidades fo'
      'LEFT JOIN ufs uf1 ON uf1.Codigo=fo.Euf'
      'LEFT JOIN ufs uf2 ON uf2.Codigo=fo.Puf'
      'LEFT JOIN listalograd ll1 ON ll1.Codigo=fo.ELograd'
      'LEFT JOIN listalograd ll2 ON ll2.Codigo=fo.PLograd'
      'WHERE fo.Codigo=:P0')
    Left = 628
    Top = 400
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTransportadorTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrTransportadorCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Calculated = True
    end
    object QrTransportadorCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Calculated = True
    end
    object QrTransportadorFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrTransportadorCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrTransportadorNOME: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOME'
      Size = 100
    end
    object QrTransportadorRUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 30
    end
    object QrTransportadorCOMPL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'COMPL'
      Size = 30
    end
    object QrTransportadorCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 100
    end
    object QrTransportadorPAIS: TWideStringField
      FieldName = 'PAIS'
      Size = 100
    end
    object QrTransportadorBAIRRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrTransportadorTELEFONE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TELEFONE'
    end
    object QrTransportadorFAX: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FAX'
    end
    object QrTransportadorCelular: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Celular'
    end
    object QrTransportadorCNPJ: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTransportadorIE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'IE'
    end
    object QrTransportadorContato: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Contato'
      Size = 60
    end
    object QrTransportadorNOMEUF: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrTransportadorNUMEROTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMEROTXT'
      Size = 40
      Calculated = True
    end
    object QrTransportadorENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrTransportadorEEMail: TWideStringField
      FieldName = 'EEMail'
      Origin = 'entidades.EEmail'
      Size = 100
    end
    object QrTransportadorPEmail: TWideStringField
      FieldName = 'PEmail'
      Origin = 'entidades.PEmail'
      Size = 100
    end
    object QrTransportadorLOGRAD: TWideStringField
      FieldName = 'LOGRAD'
      Size = 10
    end
    object QrTransportadorNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrTransportadorCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrTransportadorUF: TFloatField
      FieldName = 'UF'
    end
  end
  object QrVendedores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece6="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 649
    Top = 61
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 677
    Top = 61
  end
  object QrSumC: TMySQLQuery
    Database = Dmod.MyDB
    Left = 512
    Top = 344
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumCCredito: TFloatField
      FieldName = 'Credito'
    end
  end
  object DsEmissC: TDataSource
    DataSet = QrEmissC
    Left = 380
    Top = 416
  end
  object QrEmissC: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmissCCalcFields
    Left = 380
    Top = 368
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEmissCData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissCTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmissCCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrEmissCAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrEmissCGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrEmissCDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrEmissCNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrEmissCDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissCCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissCCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissCDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrEmissCSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrEmissCVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissCLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmissCNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrEmissCNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 10
      Calculated = True
    end
    object QrEmissCNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrEmissCFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrEmissCCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEmissCSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrEmissCFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrEmissCFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrEmissCID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrEmissCFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrEmissCBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrEmissCLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrEmissCCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrEmissCLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrEmissCOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrEmissCLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrEmissCPago: TFloatField
      FieldName = 'Pago'
    end
    object QrEmissCMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEmissCMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrEmissCMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrEmissCProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrEmissCDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmissCDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmissCUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmissCUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmissCDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrEmissCNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrEmissCVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrEmissCAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrEmissCCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmissCCOMPENSA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSA_TXT'
      Calculated = True
    end
    object QrEmissCVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEmissCVALOR_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VALOR_TXT'
      Size = 30
      Calculated = True
    end
    object QrEmissCQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmissCFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrEmissCForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrEmissCICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrEmissCICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrEmissCDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrEmissCDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEmissCDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrEmissCEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrEmissCContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrEmissCCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrEmissCSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrEmissCControle: TFloatField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmissCDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrEmissCDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrEmissCNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrEmissCUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrEmissCAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrEmissCFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEmissCAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object PMEdita: TPopupMenu
    Left = 556
    Top = 348
    object MenuItem1: TMenuItem
      Caption = '&Mercadoria'
    end
    object MenuItem2: TMenuItem
      Caption = '&Pagamento'
    end
  end
  object PMImpressao: TPopupMenu
    Left = 60
    Top = 12
    object ImpressoNormal1: TMenuItem
      Caption = 'Impress'#227'o &Normal'
      object Antiga1: TMenuItem
        Caption = '&Antiga'
        OnClick = Antiga1Click
      end
      object Nova1: TMenuItem
        Caption = '&Nova'
        OnClick = Nova1Click
      end
    end
    object Impressodireta1: TMenuItem
      Caption = '&Impress'#227'o &Direta'
      OnClick = Impressodireta1Click
    end
  end
  object QrSumIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtde) Qtde, SUM(Desco) Desco,'
      'SUM(Valor) Valor, SUM(Valor) / SUM(Qtde) PRECO'
      'FROM mpvits '
      'WHERE Codigo=:P0')
    Left = 660
    Top = 20
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumItsQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSumItsDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrSumItsValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumItsPRECO: TFloatField
      FieldName = 'PRECO'
    end
  end
  object PMExclui: TPopupMenu
    Left = 600
    Top = 348
    object MenuItem3: TMenuItem
      Caption = '&Mercadoria'
      OnClick = MenuItem3Click
    end
    object MenuItem4: TMenuItem
      Caption = '&Pagamento'
      OnClick = MenuItem4Click
    end
  end
  object PMMercadoria: TPopupMenu
    OnPopup = PMMercadoriaPopup
    Left = 220
    Top = 364
    object Incluinovamercadoria1: TMenuItem
      Caption = '&Inclui nova mercadoria'
      OnClick = Incluinovamercadoria1Click
    end
    object Alteradadosdamercadoriaatual1: TMenuItem
      Caption = '&Altera dados da mercadoria atual'
      OnClick = Alteradadosdamercadoriaatual1Click
    end
    object EncerraOS1: TMenuItem
      Caption = '&Encerra &O.S.'
      OnClick = EncerraOS1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Excluimercadoriaatual1: TMenuItem
      Caption = '&Exclui mercadoria atual'
      OnClick = Excluimercadoriaatual1Click
    end
    object Desfazfaturamentodemercadoriaatual1: TMenuItem
      Caption = '&Desfaz faturamento de mercadoria atual'
      OnClick = Desfazfaturamentodemercadoriaatual1Click
    end
  end
  object PMPagtos: TPopupMenu
    OnPopup = PMPagtosPopup
    Left = 292
    Top = 360
    object Incluinovospagamentos1: TMenuItem
      Caption = '&Inclui novo(s) pagamento(s)'
      OnClick = Incluinovospagamentos1Click
    end
    object Alterapagamentoatual1: TMenuItem
      Caption = '&Altera pagamento atual'
      OnClick = Alterapagamentoatual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excluipagamentoatual1: TMenuItem
      Caption = '&Exclui pagamento atual'
      OnClick = Excluipagamentoatual1Click
    end
    object Excluitodospagamentos1: TMenuItem
      Caption = 'E&xclui todos pagamentos'
      OnClick = Excluitodospagamentos1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object CorrigeFatID1: TMenuItem
      Caption = '&Corrige Fat ID'
      OnClick = CorrigeFatID1Click
    end
  end
  object QrPed: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPedAfterOpen
    AfterClose = QrPedAfterClose
    OnCalcFields = QrPedCalcFields
    SQL.Strings = (
      'SELECT IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLI,'
      'IF(ve.Tipo=0, ve.RazaoSocial, ve.Nome) NOMEVEN, ag.Nome NOMEMP,'
      'pp.Codigo PEDIDO, pp.Cliente, pp.Vendedor, pp.Obz, mi.* '
      'FROM mpvits mi'
      'LEFT JOIN mpp pp ON pp.Codigo=mi.Pedido'
      'LEFT JOIN entidades cl ON cl.Codigo=pp.Cliente'
      'LEFT JOIN entidades ve ON ve.Codigo=pp.Vendedor'
      'LEFT JOIN artigosgrupos ag ON ag.Codigo=mi.MP'
      'WHERE mi.Codigo=0'
      'AND mi.Controle=:P0')
    Left = 478
    Top = 15
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPedNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrPedNOMEVEN: TWideStringField
      FieldName = 'NOMEVEN'
      Size = 100
    end
    object QrPedNOMEMP: TWideStringField
      FieldName = 'NOMEMP'
      Required = True
      Size = 50
    end
    object QrPedPEDIDO: TIntegerField
      FieldName = 'PEDIDO'
      Required = True
    end
    object QrPedCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPedVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrPedObz: TWideStringField
      FieldName = 'Obz'
      Size = 255
    end
    object QrPedCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPedControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPedMP: TIntegerField
      FieldName = 'MP'
      Required = True
    end
    object QrPedQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrPedPreco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
    object QrPedValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrPedTexto: TWideStringField
      FieldName = 'Texto'
      Required = True
      Size = 50
    end
    object QrPedLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPedDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPedDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPedUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPedUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPedDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrPedEntrega: TDateField
      FieldName = 'Entrega'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrPedPronto: TDateField
      FieldName = 'Pronto'
      Required = True
    end
    object QrPedStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object QrPedFluxo: TIntegerField
      FieldName = 'Fluxo'
      Required = True
    end
    object QrPedClasse: TWideStringField
      FieldName = 'Classe'
      Size = 15
    end
    object QrPedPedido_1: TIntegerField
      FieldName = 'Pedido_1'
      Required = True
    end
    object QrPedEspesTxt: TWideStringField
      FieldName = 'EspesTxt'
      Size = 10
    end
    object QrPedCorTxt: TWideStringField
      FieldName = 'CorTxt'
      Size = 30
    end
    object QrPedM2Pedido: TFloatField
      FieldName = 'M2Pedido'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPedPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrPedUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrPedObserv: TWideMemoField
      FieldName = 'Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPedAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPedPrecoPed: TFloatField
      FieldName = 'PrecoPed'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPedValorPed: TFloatField
      FieldName = 'ValorPed'
      Required = True
    end
  end
  object DsPed: TDataSource
    DataSet = QrPed
    Left = 507
    Top = 15
  end
  object QrPesCli: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPesCliAfterScroll
    SQL.Strings = (
      'SELECT mi.Controle '
      'FROM mpvits mi'
      'LEFT JOIN mpp pp ON pp.Codigo=mi.Pedido'
      'WHERE mi.Codigo=0'
      'AND mi.Pedido>0'
      'AND pp.Cliente=:P0'
      'ORDER BY mi.Controle')
    Left = 367
    Top = 15
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesCliControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object DsPesCli: TDataSource
    DataSet = QrPesCli
    Left = 395
    Top = 15
  end
  object QrPesAll: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPesAllAfterScroll
    SQL.Strings = (
      'SELECT mi.Controle '
      'FROM mpvits mi'
      'LEFT JOIN mpp pp ON pp.Codigo=mi.Pedido'
      'WHERE mi.Codigo=0'
      'AND mi.Pedido>0'
      'ORDER BY mi.Controle')
    Left = 423
    Top = 15
    object QrPesAllControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object DsPesAll: TDataSource
    DataSet = QrPesAll
    Left = 451
    Top = 15
  end
  object frxVenda: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39667.641905740700000000
    ReportOptions.LastChange = 39667.641905740700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo44OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Memo44.Visible := <VARF_DESEX>;  '
      'end;'
      ''
      'procedure Memo45OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Memo45.Visible := <VARF_DESEX>;  '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxVendaGetValue
    OnUserFunction = frxVendaUserFunction
    Left = 252
    Top = 316
    Datasets = <
      item
        DataSet = frxDsEmissC
        DataSetName = 'frxDsEmissC'
      end
      item
        DataSet = frxDsFornecedor
        DataSetName = 'frxDsFornecedor'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMPV
        DataSetName = 'frxDsMPV'
      end
      item
        DataSet = frxDsMPVIts
        DataSetName = 'frxDsMPVIts'
      end
      item
        DataSet = frxDsSumIts
        DataSetName = 'frxDsSumIts'
      end
      item
        DataSet = frxDsTransportador
        DataSetName = 'frxDsTransportador'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = []
      MirrorMode = []
      object Band1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 294.803340000000000000
        Width = 793.701300000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsMPVIts
        DataSetName = 'frxDsMPVIts'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795275590551200000
          Width = 37.795275590000000000
          Height = 24.000000000000000000
          DataField = 'MP'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."MP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590551181102400000
          Width = 94.488188980000000000
          Height = 24.000000000000000000
          DataField = 'NOMEMP'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."NOMEMP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897703700000000000
          Width = 56.692913390000000000
          Height = 24.000000000000000000
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Qtde"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590600000000000000
          Width = 56.692913390000000000
          Height = 24.000000000000000000
          DataField = 'Preco'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Preco"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283518270000000000
          Width = 56.692913390000000000
          Height = 24.000000000000000000
          DataField = 'Desco'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Desco"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 668.976431650000000000
          Width = 80.000000000000000000
          Height = 24.000000000000000000
          DataField = 'Valor'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Valor"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078740160000000000
          Width = 226.771653540000000000
          Height = 24.000000000000000000
          DataField = 'Texto'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."Texto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 102.047244094488000000
          Height = 24.000000000000000000
          DataField = 'CorTxt'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPVIts."CorTxt"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Band3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 253.228510000000000000
        Width = 793.701300000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795275590551200000
          Width = 37.795275590000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590551181102400000
          Width = 94.488188980000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897703700000000000
          Width = 56.692913390000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590600000000000000
          Width = 56.692913390000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pre'#231'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283518270000000000
          Width = 56.692913390000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 668.976431650000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Valor Total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078740160000000000
          Width = 226.771653540000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 99.559060000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 622.236240000000000000
          Top = 56.881880000000000000
          Width = 128.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES#]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 530.236240000000000000
          Top = 12.881880000000000000
          Width = 220.000000000000000000
          Height = 24.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 38.236240000000000000
          Top = 12.881880000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          Stretched = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 210.236240000000000000
          Top = 12.881880000000000000
          Width = 316.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 210.236240000000000000
          Top = 56.881880000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data pedido:')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 294.236240000000000000
          Top = 56.881880000000000000
          Width = 170.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMPV."DataF"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 210.236240000000000000
          Top = 34.881880000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pedido n'#186':')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 294.236240000000000000
          Top = 34.881880000000000000
          Width = 170.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMPV."Codigo"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 470.236240000000000000
          Top = 56.881880000000000000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Volumes:')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 526.236240000000000000
          Top = 56.881880000000000000
          Width = 86.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMPV."Volumes"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 51.055040000000000000
        Top = 139.842610000000000000
        Width = 793.701300000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 38.236240000000000000
          Top = 7.495980000000000000
          Width = 37.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
        end
        object meSetor: TfrxMemoView
          AllowVectorExport = True
          Left = 78.236240000000000000
          Top = 7.495980000000000000
          Width = 292.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFornecedor."NOME"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 38.236240000000000000
          Top = 27.495980000000000000
          Width = 37.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cidade: ')
          ParentFont = False
        end
        object meCor: TfrxMemoView
          AllowVectorExport = True
          Left = 78.236240000000000000
          Top = 27.495980000000000000
          Width = 240.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFornecedor."CIDADE"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 322.236240000000000000
          Top = 27.495980000000000000
          Width = 33.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'UF: ')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 574.236240000000000000
          Top = 7.495980000000000000
          Width = 24.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Fax: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 374.236240000000000000
          Top = 7.495980000000000000
          Width = 45.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Telefone: ')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 358.236240000000000000
          Top = 27.495980000000000000
          Width = 36.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFornecedor."NOMEUF"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 418.236240000000000000
          Top = 7.495980000000000000
          Width = 156.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFornecedor."TEL_TXT"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 598.236240000000000000
          Top = 7.495980000000000000
          Width = 156.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFornecedor."FAX_TXT"]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 398.236240000000000000
          Top = 27.495980000000000000
          Width = 69.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Transportador: ')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 470.236240000000000000
          Top = 27.495980000000000000
          Width = 288.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsTransportador."NOME"]')
          ParentFont = False
        end
      end
      object RodapeMestre1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 56.000000000000000000
        Top = 340.157700000000000000
        Width = 793.701300000000000000
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 98.236240000000000000
          Top = 26.235930000000000000
          Width = 144.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo44OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Desconto extra:')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 242.236240000000000000
          Top = 26.235930000000000000
          Width = 96.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo45OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPV."DescoExtra"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 669.354360000000000000
          Top = 26.235930000000000000
          Width = 80.881880000000000000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPV."TOTAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 415.747990000000000000
          Top = 26.235930000000000000
          Width = 249.826840000000000000
          Height = 24.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL DO PEDIDO')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897637800000000000
          Width = 56.692913390000000000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumIts."Qtde"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283464570000000000
          Width = 56.692913385826800000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumIts."Desco"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 668.976377950000000000
          Width = 80.000000000000000000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumIts."Valor"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 415.716760000000000000
          Width = 81.259740000000000000
          Height = 24.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL  ITENS')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590600000000000000
          Width = 56.692913385826800000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumIts."PRECO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 461.102660000000000000
        Width = 793.701300000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEmissC
        DataSetName = 'frxDsEmissC'
        RowCount = 0
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 38.236240000000000000
          Width = 96.000000000000000000
          Height = 24.000000000000000000
          DataField = 'VENCTO_TXT'
          DataSet = frxDsEmissC
          DataSetName = 'frxDsEmissC'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmissC."VENCTO_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 134.236240000000000000
          Width = 148.000000000000000000
          Height = 24.000000000000000000
          DataField = 'Credito'
          DataSet = frxDsEmissC
          DataSetName = 'frxDsEmissC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEmissC."Credito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 282.236240000000000000
          Width = 466.897650000000000000
          Height = 24.000000000000000000
          DataField = 'NOMECARTEIRA'
          DataSet = frxDsEmissC
          DataSetName = 'frxDsEmissC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEmissC."NOMECARTEIRA"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object CabecalhoMestre1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 419.527830000000000000
        Width = 793.701300000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 38.236240000000000000
          Width = 95.779530000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 134.015770000000000000
          Width = 148.220470000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 282.236240000000000000
          Width = 466.897650000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tipo de cobran'#231'a')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object RodapeMestre2: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 506.457020000000000000
        Width = 793.701300000000000000
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 134.236240000000000000
          Top = 3.779527560000020000
          Width = 148.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEmissC."Credito">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 38.236240000000000000
          Top = 3.779527560000020000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL ')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 586.236240000000000000
          Top = 3.779527560000020000
          Width = 144.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsMPV."TOTAL"> - SUM(<frxDsEmissC."Credito">,DadosMestre1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 494.236240000000000000
          Top = 3.779527560000020000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'PENDENTE')
          ParentFont = False
          WordWrap = False
        end
      end
      object SumarioDoRelatorio1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 89.007420000000000000
        Top = 589.606680000000000000
        Width = 793.701300000000000000
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 38.236240000000000000
          Top = 5.668829999999960000
          Width = 692.000000000000000000
          Height = 72.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMPV."Obs"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsEmissC: TfrxDBDataset
    UserName = 'frxDsEmissC'
    CloseDataSource = False
    DataSet = QrEmissC
    BCDToCurrency = False
    DataSetOptions = []
    Left = 280
    Top = 316
  end
  object frxDsMPVIts: TfrxDBDataset
    UserName = 'frxDsMPVIts'
    CloseDataSource = False
    DataSet = QrMPVIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 308
    Top = 316
  end
  object frxDsMPV: TfrxDBDataset
    UserName = 'frxDsMPV'
    CloseDataSource = False
    DataSet = QrMPV
    BCDToCurrency = False
    DataSetOptions = []
    Left = 336
    Top = 316
  end
  object frxDsSumIts: TfrxDBDataset
    UserName = 'frxDsSumIts'
    CloseDataSource = False
    DataSet = QrSumIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 364
    Top = 316
  end
  object frxDsFornecedor: TfrxDBDataset
    UserName = 'frxDsFornecedor'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FAX_TXT=FAX_TXT'
      'CEL_TXT=CEL_TXT'
      'CPF_TXT=CPF_TXT'
      'TEL_TXT=TEL_TXT'
      'CEP_TXT=CEP_TXT'
      'NOME=NOME'
      'RUA=RUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'PAIS=PAIS'
      'TELEFONE=TELEFONE'
      'FAX=FAX'
      'Celular=Celular'
      'CNPJ=CNPJ'
      'IE=IE'
      'Contato=Contato'
      'NOMEUF=NOMEUF'
      'EEMail=EEMail'
      'PEmail=PEmail'
      'NUMEROTXT=NUMEROTXT'
      'ENDERECO=ENDERECO'
      'LOGRAD=LOGRAD'
      'NUMERO=NUMERO'
      'CEP=CEP'
      'UF=UF')
    DataSet = QrFornecedor
    BCDToCurrency = False
    DataSetOptions = []
    Left = 392
    Top = 316
  end
  object frxDsTransportador: TfrxDBDataset
    UserName = 'frxDsTransportador'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TEL_TXT=TEL_TXT'
      'CEL_TXT=CEL_TXT'
      'CPF_TXT=CPF_TXT'
      'FAX_TXT=FAX_TXT'
      'CEP_TXT=CEP_TXT'
      'NOME=NOME'
      'RUA=RUA'
      'COMPL=COMPL'
      'CIDADE=CIDADE'
      'PAIS=PAIS'
      'BAIRRO=BAIRRO'
      'TELEFONE=TELEFONE'
      'FAX=FAX'
      'Celular=Celular'
      'CNPJ=CNPJ'
      'IE=IE'
      'Contato=Contato'
      'NOMEUF=NOMEUF'
      'NUMEROTXT=NUMEROTXT'
      'ENDERECO=ENDERECO'
      'EEMail=EEMail'
      'PEmail=PEmail'
      'LOGRAD=LOGRAD'
      'NUMERO=NUMERO'
      'CEP=CEP'
      'UF=UF')
    DataSet = QrTransportador
    BCDToCurrency = False
    DataSetOptions = []
    Left = 420
    Top = 316
  end
  object frxMovimento: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.533612013900000000
    ReportOptions.LastChange = 39717.533612013900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 224
    Top = 316
    Datasets = <
      item
        DataSet = frxDsEmissC
        DataSetName = 'frxDsEmissC'
      end
      item
        DataSet = frxDsFornecedor
        DataSetName = 'frxDsFornecedor'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMPV
        DataSetName = 'frxDsMPV'
      end
      item
        DataSet = frxDsMPVIts
        DataSetName = 'frxDsMPVIts'
      end
      item
        DataSet = frxDsSumIts
        DataSetName = 'frxDsSumIts'
      end
      item
        DataSet = frxDsTransportador
        DataSetName = 'frxDsTransportador'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object Band1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 264.567100000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsMPVIts
        DataSetName = 'frxDsMPVIts'
        RowCount = 0
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Width = 40.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo1OnBeforePrint'
          DataField = 'MP'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPVIts."MP"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 46.000000000000000000
          Width = 96.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo4OnBeforePrint'
          DataField = 'NOMEMP'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMPVIts."NOMEMP"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 398.000000000000000000
          Width = 80.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo6OnBeforePrint'
          DataField = 'Qtde'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Qtde"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 478.000000000000000000
          Width = 80.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo8OnBeforePrint'
          DataField = 'Preco'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Preco"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 558.000000000000000000
          Width = 80.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo12OnBeforePrint'
          DataField = 'Desco'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Desco"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 638.000000000000000000
          Width = 80.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo14OnBeforePrint'
          DataField = 'Valor'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPVIts."Valor"]')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 142.000000000000000000
          Width = 256.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo100OnBeforePrint'
          DataSet = frxDsMPVIts
          DataSetName = 'frxDsMPVIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMPVIts."Texto"] [frxDsMPVIts."CorTxt"]')
          ParentFont = False
        end
      end
      object Band3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 222.992270000000000000
        Width = 718.110700000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 46.000000000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 398.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 478.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            'Pre'#231'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 558.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 638.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            'Valor Total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 142.000000000000000000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 73.102350000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 586.220470000000000000
          Top = 53.102350000000000000
          Width = 128.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES#]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 494.220470000000000000
          Top = 9.102350000000001000
          Width = 220.000000000000000000
          Height = 24.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 9.102350000000001000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          Stretched = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 178.000000000000000000
          Top = 9.102350000000001000
          Width = 316.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 178.000000000000000000
          Top = 53.102350000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data pedido:')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 262.000000000000000000
          Top = 53.102350000000000000
          Width = 170.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMPV."DataF"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 178.000000000000000000
          Top = 31.102350000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pedido n'#186':')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 262.000000000000000000
          Top = 31.102350000000000000
          Width = 170.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMPV."Codigo"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 438.000000000000000000
          Top = 53.102350000000000000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Volumes:')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 494.000000000000000000
          Top = 53.102350000000000000
          Width = 86.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMPV."Volumes"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 48.000000000000000000
        Top = 113.385900000000000000
        Width = 718.110700000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 3.716449999999995000
          Width = 37.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
        end
        object meSetor: TfrxMemoView
          AllowVectorExport = True
          Left = 46.000000000000000000
          Top = 3.716449999999995000
          Width = 292.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFornecedor."Nome"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 23.716449999999990000
          Width = 37.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cidade: ')
          ParentFont = False
        end
        object meCor: TfrxMemoView
          AllowVectorExport = True
          Left = 46.000000000000000000
          Top = 23.716449999999990000
          Width = 240.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFornecedor."Cidade"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 290.000000000000000000
          Top = 23.716449999999990000
          Width = 33.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'UF: ')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 542.000000000000000000
          Top = 3.716449999999995000
          Width = 24.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Fax: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 342.000000000000000000
          Top = 3.716449999999995000
          Width = 45.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Telefone: ')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 326.000000000000000000
          Top = 23.716449999999990000
          Width = 36.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFornecedor."NOMEUF"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 386.000000000000000000
          Top = 3.716449999999995000
          Width = 156.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFornecedor."TEL_TXT"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 566.000000000000000000
          Top = 3.716449999999995000
          Width = 156.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFornecedor."FAX_TXT"]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 366.000000000000000000
          Top = 23.716449999999990000
          Width = 69.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Transportador: ')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 438.000000000000000000
          Top = 23.716449999999990000
          Width = 288.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsTransportador."Nome"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 5.559060000000000000
          Top = 43.716449999999990000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object RodapeMestre1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 54.015460000000000000
        Top = 309.921460000000000000
        Width = 718.110700000000000000
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 398.000000000000000000
          Top = 2.015460000000019000
          Width = 80.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo15OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumIts."Qtde"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 558.000000000000000000
          Top = 2.015460000000019000
          Width = 80.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo18OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumIts."Desco"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 638.000000000000000000
          Top = 2.015460000000019000
          Width = 80.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo19OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumIts."Valor"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 9.307050000000000000
          Top = 2.015460000000019000
          Width = 240.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo20OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'TOTAL  ITENS')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 478.000000000000000000
          Top = 2.015460000000019000
          Width = 80.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo16OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumIts."PRECO"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 9.307050000000000000
          Top = 30.015460000000020000
          Width = 144.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo44OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Desconto extra:')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 153.307050000000000000
          Top = 11.117810000000020000
          Width = 96.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo45OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPV."DescoExtra"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 565.307050000000000000
          Top = 30.015460000000020000
          Width = 96.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo46OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPV."TOTAL"]')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 421.307050000000000000
          Top = 30.015460000000020000
          Width = 144.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo89OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'TOTAL DO PEDIDO')
          ParentFont = False
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 427.086890000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEmissC
        DataSetName = 'frxDsEmissC'
        RowCount = 0
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Width = 96.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo21OnBeforePrint'
          DataField = 'Vencimento'
          DataSet = frxDsEmissC
          DataSetName = 'frxDsEmissC'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmissC."Vencimento"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 102.000000000000000000
          Width = 148.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo22OnBeforePrint'
          DataField = 'Credito'
          DataSet = frxDsEmissC
          DataSetName = 'frxDsEmissC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEmissC."Credito"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 250.000000000000000000
          Width = 448.000000000000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo23OnBeforePrint'
          DataField = 'NOMECARTEIRA'
          DataSet = frxDsEmissC
          DataSetName = 'frxDsEmissC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsEmissC."NOMECARTEIRA"]')
          ParentFont = False
        end
      end
      object CabecalhoMestre1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 385.512060000000000000
        Width = 718.110700000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 98.000000000000000000
          Width = 152.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor  ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 250.000000000000000000
          Width = 448.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            'Tipo de cobran'#231'a')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object RodapeMestre2: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 28.000000000000000000
        Top = 472.441250000000000000
        Width = 718.110700000000000000
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 102.000000000000000000
          Top = 2.802720000000022000
          Width = 148.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo27OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEmissC."Credito">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 2.802720000000022000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo28OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'TOTAL ')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 554.000000000000000000
          Top = 2.802720000000022000
          Width = 144.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo29OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[(<frxDsMPV."TOTAL">-(SUM(<frxDsEmissC."Credito">)))]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 462.000000000000000000
          Top = 2.802720000000022000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo47OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'PENDENTE')
          ParentFont = False
        end
      end
      object SumarioDoRelatorio1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 84.000000000000000000
        Top = 559.370440000000000000
        Width = 718.110700000000000000
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 5.668829999999957000
          Width = 692.000000000000000000
          Height = 72.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMPV."Obs"]')
          ParentFont = False
        end
      end
    end
  end
end
