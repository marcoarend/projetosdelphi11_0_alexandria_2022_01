unit Formulas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, DBCtrls, ExtCtrls, Mask, Buttons, Db, mySQLDbTables,
  ComCtrls, Menus, dmkLabel, dmkGeral, UnDmkProcFunc, UnDmkEnums, Variants,
  dmkImage, AppListas, dmkDBEdit, dmkEdit, UnInternalConsts;

type
  TFmFormulas = class(TForm)
    TbFormulas: TmySQLTable;
    DsFormulas: TDataSource;
    TbFormulasIts: TmySQLTable;
    DsFormulasIts: TDataSource;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    TbFormulasNumero: TIntegerField;
    TbFormulasNome: TWideStringField;
    TbFormulasClienteI: TIntegerField;
    TbFormulasTipificacao: TIntegerField;
    TbFormulasDataI: TDateField;
    TbFormulasDataA: TDateField;
    TbFormulasTecnico: TWideStringField;
    TbFormulasTempoR: TIntegerField;
    TbFormulasTempoP: TIntegerField;
    TbFormulasTempoT: TIntegerField;
    TbFormulasHidrica: TIntegerField;
    TbFormulasLinhaTE: TIntegerField;
    TbFormulasCaldeira: TIntegerField;
    TbFormulasSetor: TIntegerField;
    TbFormulasEspessura: TIntegerField;
    TbFormulasPeso: TFloatField;
    TbFormulasQtde: TFloatField;
    TbFormulasLk: TIntegerField;
    TbFormulasDataCad: TDateField;
    TbFormulasDataAlt: TDateField;
    TbFormulasUserCad: TIntegerField;
    TbFormulasUserAlt: TIntegerField;
    TbFormulasNOMECLIENTEI: TWideStringField;
    QrListaSetores: TmySQLQuery;
    DsListaSetores: TDataSource;
    TbFormulasNOMESETOR: TWideStringField;
    QrEspessuras: TmySQLQuery;
    DsEspessuras: TDataSource;
    QrEspessurasCodigo: TIntegerField;
    QrEspessurasLinhas: TWideStringField;
    QrEspessurasEMCM: TFloatField;
    QrListaSetoresCodigo: TIntegerField;
    QrListaSetoresNome: TWideStringField;
    TbFormulasLINHAS: TWideStringField;
    TbFormulasMEDIAPESO: TFloatField;
    TbFormulasEMCM: TFloatField;
    PainelDados: TPanel;
    PainelDadosReceita: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label30: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label26: TLabel;
    Label25: TLabel;
    Label24: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TdmkEdit;
    DBEdit26: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    DBRGTipificacao: TDBRadioGroup;
    EdDBClienteI: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    PainelGerencia: TPanel;
    PainelControle: TPanel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    BtDuplica: TBitBtn;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    CBClienteI: TDBLookupComboBox;
    CBEspessura: TDBLookupComboBox;
    CBSetor: TDBLookupComboBox;
    QrPQs: TmySQLQuery;
    DsPQs: TDataSource;
    TbFormulasItsNumero: TIntegerField;
    TbFormulasItsOrdem: TIntegerField;
    TbFormulasItsControle: TIntegerField;
    TbFormulasItsReordem: TIntegerField;
    TbFormulasItsPorcent: TFloatField;
    TbFormulasItsProduto: TIntegerField;
    TbFormulasItsTempoR: TIntegerField;
    TbFormulasItsTempoP: TIntegerField;
    TbFormulasItsObs: TWideStringField;
    TbFormulasItsSC: TIntegerField;
    TbFormulasItsReciclo: TIntegerField;
    TbFormulasItsDiluicao: TFloatField;
    TbFormulasItsCProd: TFloatField;
    TbFormulasItsCSolu: TFloatField;
    TbFormulasItsCusto: TFloatField;
    TbFormulasItsMinimo: TFloatField;
    TbFormulasItsMaximo: TFloatField;
    TbFormulasItsGraus: TFloatField;
    TbFormulasItsBoca: TWideStringField;
    TbFormulasItsNOMEPQ: TWideStringField;
    QrReordem: TmySQLQuery;
    QrSoma: TmySQLQuery;
    QrSomaTempoR: TFloatField;
    QrSomaTempoP: TFloatField;
    TbFormulasHorasR: TIntegerField;
    TbFormulasHorasP: TIntegerField;
    TbFormulasHorasT: TIntegerField;
    TbFormulasHHMM_P: TWideStringField;
    TbFormulasHHMM_R: TWideStringField;
    TbFormulasHHMM_T: TWideStringField;
    BtImportar: TBitBtn;
    PMImporta: TPopupMenu;
    Receitadeanlogos1: TMenuItem;
    DBGrid1: TDBGrid;
    QrDup: TmySQLQuery;
    QrIns: TmySQLQuery;
    TbFormulasItsProcesso: TWideStringField;
    TbFormulasItsObsProces: TWideStringField;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    TbFormulasAtivo: TSmallintField;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    TbFormulasItsAlterWeb: TSmallintField;
    TbFormulasItsAtivo: TSmallintField;
    TbFormulasItspH_Min: TFloatField;
    TbFormulasItspH_Max: TFloatField;
    TbFormulasItsBe_Min: TFloatField;
    TbFormulasItsBe_Max: TFloatField;
    TbFormulasItsGC_Min: TFloatField;
    TbFormulasItsGC_Max: TFloatField;
    QrPQsCodigo: TIntegerField;
    QrPQsNome: TWideStringField;
    QrSetor: TmySQLQuery;
    QrSetorTpReceita: TSmallintField;
    DBCheckBox2: TDBCheckBox;
    TbFormulasRetrabalho: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    DBRadioGroup1: TDBRadioGroup;
    TbFormulasBxaEstqVS: TSmallintField;
    EdDBGraGruX: TDBEdit;
    Label7: TLabel;
    CBGraGruX: TDBLookupComboBox;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    TbFormulasGraGruX: TIntegerField;
    LaAviso: TLabel;
    QrPQsStatus_TXT: TWideStringField;
    TbFormulasItsSTATUSPQ: TWideStringField;
    EdDBGraCorCad: TDBEdit;
    CBGraCorCad: TDBLookupComboBox;
    Label8: TLabel;
    TbFormulasGraCorCad: TIntegerField;
    QrGraCorCad: TmySQLQuery;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    DsGraCorCad: TDataSource;
    TbFormulasNoGraCorCad: TWideStringField;
    CBRebaixe: TDBLookupComboBox;
    Label9: TLabel;
    SpeedButton7: TSpeedButton;
    QrRebaixe: TmySQLQuery;
    QrRebaixeCodigo: TIntegerField;
    QrRebaixeLinhas: TWideStringField;
    QrRebaixeEMCM: TFloatField;
    DsRebaixe: TDataSource;
    TbFormulasEspRebaixe: TIntegerField;
    TbFormulasLinReb: TWideStringField;
    QrSeq: TMySQLQuery;
    QrSeqControle: TIntegerField;
    QrSeqOrdem: TIntegerField;
    QrSeqReordem: TIntegerField;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    CkNaoReordenaSempre: TCheckBox;
    Label10: TLabel;
    DBEdFormulasGru: TDBEdit;
    CBFormulasGru: TDBLookupComboBox;
    QrFormulasGru: TMySQLQuery;
    DsFormulasGru: TDataSource;
    QrFormulasGruCodigo: TIntegerField;
    QrFormulasGruNome: TWideStringField;
    TbFormulasFormulasGru: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure TbFormulasCalcFields(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure TbFormulasItsBeforePost(DataSet: TDataSet);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TbFormulasItsAfterPost(DataSet: TDataSet);
    procedure TbFormulasItsAfterDelete(DataSet: TDataSet);
    procedure TbFormulasBeforePost(DataSet: TDataSet);
    procedure BtImportarClick(Sender: TObject);
    procedure Caleiro1Click(Sender: TObject);
    procedure Receitadeanlogos1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure TbFormulasItsBeforeInsert(DataSet: TDataSet);
    procedure TbFormulasItsNewRecord(DataSet: TDataSet);
    procedure BtDuplicaClick(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure TbFormulasAfterInsert(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure TbFormulasAfterScroll(DataSet: TDataSet);
    procedure ImgTipoChange(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure CBSetorClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TbFormulasBeforeClose(DataSet: TDataSet);
    procedure SpeedButton7Click(Sender: TObject);
  private
    { Private declarations }
    FReordenendo: Boolean;
    FOrdem, FNumFormulaReordenando: Integer;
    procedure ReordenaLinhas(Forcar: Boolean);
    procedure AtualizaTbFormulas;
    //procedure DuplicaRegistro;
    procedure DuplicaRegistro2;
    procedure MostraFormulasimpBH();
    procedure MostraFormulasimpWE();
    procedure ReopenEspessura();
    procedure ReopenGraGruX();
    procedure ReopenPQs(SQLType: TSQLType);
  public
    { Public declarations }
    FRecPesq: Integer;
  end;

var
  FmFormulas: TFmFormulas;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UMySQLModule, (*UnInternalConsts,*) MeuDBUses, MyVCLSkin,
  FormulasImpBH, FormulasImpWE2, Principal, FormulasPesq, MyDBCheck, DmkDAC_PF,
  ModuleGeral, BlueDermConsts, UnPQ_PF;

procedure TFmFormulas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;
procedure TFmFormulas.SpeedButton1Click(Sender: TObject);
begin
  TbFormulas.First;
end;

procedure TFmFormulas.SpeedButton2Click(Sender: TObject);
begin
  TbFormulas.Prior;
end;

procedure TFmFormulas.SpeedButton3Click(Sender: TObject);
begin
  TbFormulas.Next;
end;

procedure TFmFormulas.SpeedButton4Click(Sender: TObject);
begin
  TbFormulas.Last;
end;

procedure TFmFormulas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TbFormulas.Database := Dmod.MyDB;
  TbFormulasIts.Database := Dmod.MyDB;
  QrEntidades.Database := Dmod.MyDB;
  QrListaSetores.Database := Dmod.MyDB;
  QrEspessuras.Database := Dmod.MyDB;
  QrPQs.Database := Dmod.MyDB;
  QrReordem.Database := Dmod.MyDB;
  QrSoma.Database := Dmod.MyDB;
  QrDup.Database := Dmod.MyDB;
  QrIns.Database := Dmod.MyDB;
  QrSetor.Database := Dmod.MyDB;
  QrGraGruX.Database := Dmod.MyDB;
  QrGraCorCad.Database := Dmod.MyDB;
  QrRebaixe.Database := Dmod.MyDB;
  QrSeq.Database := Dmod.MyDB;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
  PainelGerencia.Height := 48;
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  ReopenEspessura();
  UnDmkDAC_PF.AbreQuery(QrListaSetores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFormulasGru, Dmod.MyDB);
  TbFormulasIts.Close;
  //TbFormulasIts.ReadOnly := ImgTipo.SQLType <> stUpd;
  UnDmkDAC_PF.AbreTable(TbFormulas, Dmod.MyDB);
  ReopenPQs(ImgTipo.SQLType);
  TbFormulas.Last;
  UnDmkDAC_PF.AbreTable(TbFormulasIts, Dmod.MyDB);
end;

procedure TFmFormulas.ImgTipoChange(Sender: TObject);
var
  Controle: Integer;
begin
  DBGrid1.Visible  := ImgTipo.SQLType <> stIns;
  DBGrid1.ReadOnly := ImgTipo.SQLType = stLok;
  Controle         := TbFormulasItsControle.Value;
  //
  ReopenPQs(ImgTipo.SQLType);
  //
  TbFormulasIts.Close;
  TbFormulasIts.ReadOnly := ImgTipo.SQLType <> stUpd;
  UnDmkDAC_PF.AbreTable(TbFormulasIts, Dmod.MyDB);
  TbFormulasIts.Locate('Controle', Controle, []);
  //
  LaAviso.Visible := ImgTipo.SQLType = stUpd;
end;

procedure TFmFormulas.MostraFormulasimpBH();
begin
  Application.CreateForm(TFmFormulasImpBH, FmFormulasImpBH);
  VAR_LABEL1 := FmFormulasImpBH.LaAviso1;
  VAR_LABEL2 := FmFormulasImpBH.LaAviso2;
  MyObjects.Informa2VAR_LABELs('Setando janela "FormulasImpBH"');
  FmFormulasImpBH.PainelEscolhe.Visible := True;
  //FmFormulasImpBH.FMovimCod             := 0;
  FmFormulasImpBH.FEmit                 := 0;
  FmFormulasImpBH.FNomeForm             := 'Impress�o de Receita sem Baixa';
  FmFormulasImpBH.EdReceita.Text        := DBEdit12.Text;
  FmFormulasImpBH.CBReceita.KeyValue    := TbFormulasNumero.Value;
  FmFormulasImpBH.EdCliInt.ValueVariant := TbFormulasClienteI.Value;
  FmFormulasImpBH.CBCliInt.KeyValue     := TbFormulasClienteI.Value;
  //FmFormulasImpBH.PainelDefine.Visible  := True;
  //
  if VAR_SETOR = CO_VAZIO then
  begin
    FmFormulasImpBH.EdPeso.ValueVariant := TbFormulasPeso.Value;
    FmFormulasImpBH.EdQtde.ValueVariant := TbFormulasQtde.Value;
{
    FmFormulasImpBH.CBEspessura.KeyValue :=
      TbFormulasEspessura.Value;
    FmFormulasImpBH.CalculaArea;
}
  end;
  FmFormulasImpBH.ShowModal;
  FmFormulasImpBH.Destroy;
  VAR_LABEL1 := nil;
  VAR_LABEL2 := nil;
end;

procedure TFmFormulas.MostraFormulasimpWE();
begin
  if DBCheck.CriaFm(TFmFormulasImpWE2, FmFormulasImpWE2, afmoNegarComAviso) then
  begin
    FmFormulasImpWE2.FEmit := 0;
    //FmFormulasImpWE2.PnPedido.Enabled       := False;
    //FmFormulasImpWE2.PnMateriaPrima.Enabled := False;
    FmFormulasImpWE2.FNomeForm              := 'Impress�o de Receita sem Baixa';
    FmFormulasImpWE2.EdReceita.ValueVariant := DBEdit12.Text;
    FmFormulasImpWE2.CBReceita.KeyValue     := TbFormulasNumero.Value;
    FmFormulasImpWE2.EdCliInt.ValueVariant  := TbFormulasClienteI.Value;
    FmFormulasImpWE2.CBCliInt.KeyValue      := TbFormulasClienteI.Value;
    //FmFormulasImpWE2.PainelDefine.Visible   := True;
    FmFormulasImpWE2.LaData.Visible         := True;
    FmFormulasImpWE2.TPDataP.Visible        := True;
    if VAR_SETOR = CO_VAZIO then
    begin
      FmFormulasImpWE2.EdPeso.ValueVariant   := TbFormulasPeso.Value;
      FmFormulasImpWE2.EdPecas.ValueVariant  := TbFormulasQtde.Value;
      FmFormulasImpWE2.EdAreaM2.ValueVariant := 0;
    end;
    FmFormulasImpWE2.ShowModal;
    FmFormulasImpWE2.Destroy;
  end;
end;

procedure TFmFormulas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if CkNaoReordenaSempre.Checked then
    ReordenaLinhas(True);
  TbFormulas.Close;
end;

procedure TFmFormulas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulas.TbFormulasCalcFields(DataSet: TDataSet);
begin
  if TbFormulasQtde.Value > 0 then
    TbFormulasMEDIAPESO.Value :=
    TbFormulasPeso.Value /
    TbFormulasQtde.Value
  else
    TbFormulasMEDIAPESO.Value := 0;
  TbFormulasHHMM_P.Value := dmkPF.HorasMH(TbFormulasTempoP.Value, True);
  TbFormulasHHMM_R.Value := dmkPF.HorasMH(TbFormulasTempoR.Value, True);
  TbFormulasHHMM_T.Value := dmkPF.HorasMH(TbFormulasTempoT.Value, True);
end;

procedure TFmFormulas.BtIncluiClick(Sender: TObject);
begin
  UMyMod.IncluiRegistroTb(FmFormulas, TbFormulas, nil, ImgTipo);
end;

procedure TFmFormulas.BtAlteraClick(Sender: TObject);
begin
  UMyMod.AlteraRegistroTb(FmFormulas, TbFormulas, nil, ImgTipo);
  ReopenGraGruX();
end;

procedure TFmFormulas.ReopenEspessura();
  procedure Reabre(Qry: TmySQLQuery);
  begin
    //UnDmkDAC_PF.AbreQuery(QrEspessuras, Dmod.MyDB);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, Linhas, EMCM ',
    'FROM espessuras ',
    'ORDER BY EMCM ',
    '']);
  end;
begin
  Reabre(QrEspessuras);
  Reabre(QrRebaixe);
end;

procedure TFmFormulas.ReopenGraGruX();
var
  GraGruY: Integer;
begin
  QrGraGruX.Close;
  if CBSetor.KeyValue <> Null then
  begin
    if Dmod.QrControleSetorCal.Value = CBSetor.KeyValue then
      GraGruY := CO_GraGruY_1365_VSProCal
    else
    if Dmod.QrControleSetorCur.Value = CBSetor.KeyValue then
      GraGruY := CO_GraGruY_1707_VSProCur
    else
      GraGruY := 0;
    //
    if GraGruY <> 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED ',
      'FROM gragrux ggx',
      //'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.Controle',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
      'WHERE ggx.GragruY=' + Geral.FF0(GraGruY),
      //'ORDER BY ggy.Ordem, NO_PRD_TAM_COR, ggx.Controle ',
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
      '']);
    end;
  end;
end;

procedure TFmFormulas.ReopenPQs(SQLType: TSQLType);
var
  SQLCompl: String;
begin
  DBGrid1.Columns[0].Visible := SQLType <> stUpd;
  //
  if SQLType = stUpd then
    SQLCompl := 'WHERE Ativo = 1 AND (GGXNiv2>0 OR Codigo<0) '
  else
    SQLCompl := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQs, Dmod.MyDB, [
    'SELECT Codigo, Nome, IF((Ativo = 0 OR GGXNiv2 = 0), "'+ CO_Forml_Status_Prod_Irregular +'", "'+ CO_Forml_Status_Prod_Ok +'") Status_TXT ',
    'FROM pq ',
    SQLCompl,
    'ORDER BY Nome ',
    '']);
end;

{ Ini Original
procedure TFmFormulas.ReordenaLinhas;
var
  I, Item: Integer;
  MyCursor: TCursor;
begin
  if FReordenendo then
    Exit;
  FReordenendo := True;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  TbFormulasIts.DisableControls;
  Item := TbFormulasItsControle.Value;
  I := 0;
  TbFormulasIts.First;
  while not TbFormulasIts.Eof do
  begin
    I := I + 1;
    TbFormulasIts.Edit;
    TbFormulasItsReordem.Value := I;
    TbFormulasIts.Post;
    TbFormulasIts.Next;
  end;
  QrReordem.Database := Dmod.MyDB;
  QrReordem.Params[0].AsInteger := TbFormulasNumero.Value;
  QrReordem.ExecSQL;
  TbFormulasIts.Refresh;
  TbFormulasIts.Locate('Controle', Item, []);
  TbFormulasIts.EnableControls;
  FReordenendo := False;
  Screen.Cursor := MyCursor;
end;
Fim Original }

procedure TFmFormulas.ReordenaLinhas(Forcar: Boolean);
var
  I, Item, Numero, Controle: Integer;
  MyCursor: TCursor;
  AllSeq: array of Integer;
  Seq, sNum: String;
begin
  if FReordenendo then
    Exit;
  if (Forcar = False) and (CkNaoReordenaSempre.Checked) then
    Exit;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Reordenando linhas');
  Numero := TbFormulasNumero.Value;
  sNum := Geral.FF0(Numero);
  FReordenendo := True;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  TbFormulasIts.DisableControls;
  Item := TbFormulasItsControle.Value;
  Controle := Item;
  I := 0;


  UnDmkDAC_PF.AbreMySQLQuery0(QrSeq, Dmod.MyDB, [
  'SELECT Controle, Ordem, Reordem ',
  'FROM formulasits ',
  'WHERE Numero=' + sNum,
  'ORDER BY Ordem, Controle ',
  '']);
  QrSeq.First;
  while not QrSeq.Eof do
  begin
    Seq := Geral.FF0(QrSeq.RecNo);
    Dmod.MyDB.Execute(
    ' UPDATE formulasits SET Ordem=' + Seq +
    ', Reordem=' + Seq +
    ' WHERE Numero=' + sNum +
    ' AND Controle=' + Geral.FF0(QrSeqControle.Value));
    QrSeq.Next;
  end;
{
  SetLength(AllSeq, TbFormulasIts.RecordCount);
  TbFormulasIts.First;
  while not TbFormulasIts.Eof do
  begin
    AllSeq[I] := TbFormulasItsControle.Value;
    I := I + 1;
    (*
    TbFormulasIts.Edit;
    TbFormulasItsReordem.Value := I;
    TbFormulasIts.Post;
    TbFormulasIts.Next;
    *)
  end;
  for I := Low(AllSeq) to High(AllSeq) do
  begin
    Seq := Geral.FF0(I + 1);
    Dmod.MyDB.Execute(
    ' UPDATE formulasits SET Ordem=' + Seq +
    ' Reordem=' + Seq +
    ' WHERE Numero=' + sNum +
    ' AND Controle=' + Geral.FF0(AllSeq[I]));
  end;
(*
  QrReordem.Database := Dmod.MyDB;
  QrReordem.Params[0].AsInteger := TbFormulasNumero.Value;
  QrReordem.ExecSQL;
*)
}
  if TbFormulasNumero.Value <> Numero then
    TbFormulas.Locate('Numero', Numero, []);
  TbFormulasIts.Refresh;
  TbFormulasIts.Locate('Controle', Controle, []);
  TbFormulasIts.EnableControls;
  FReordenendo := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
  Screen.Cursor := MyCursor;
end;

procedure TFmFormulas.BtConfirmaClick(Sender: TObject);
var
  BxaEstqVS, Formula: Integer;
begin
  Screen.Cursor := crHourGlass;
  CkNaoReordenaSempre.Checked := False;
  Formula       := TbFormulasNumero.Value;
  BxaEstqVS     := TbFormulasBxaEstqVS.Value;
  if MyObjects.FIC(BxaEstqVS < Integer(TBxaEstqVS.bevsNao), nil,
  'Informe o item: ' + sLineBreak +
  '"Baixa estq VS In Natura"') then Exit;
  if CBSetor.KeyValue <> Null then
  begin
    if Dmod.QrControleSetorCal.Value = CBSetor.KeyValue then
    begin
      //if MyObjects.FIC(TbFormulasGraGruX.Value = 0, nil,
      if MyObjects.FIC(CBGraGruX.Text = '', EdDBGraGruX,
      'Informe o artigo padr�o em processo!') then Exit;
    end
    else
    if Dmod.QrControleSetorCur.Value = CBSetor.KeyValue then
    begin
      //if MyObjects.FIC(TbFormulasGraGruX.Value = 0, nil,
      if MyObjects.FIC(CBGraGruX.Text = '', EdDBGraGruX,
      'Informe o artigo padr�o em processo!"') then Exit;
    end;
  end;
  ReordenaLinhas(True);
  UMyMod.ConfirmaRegistroTb_Numero(FmFormulas, TbFormulas, ImgTipo,
  PainelDados, PainelConfirma, PainelControle);
  // Ini 2020-07-22
  if TbFormulasNumero.Value <> Formula then
    TbFormulas.Locate('Numero', Formula, []);
  // Fim 2020-07-22
  Screen.Cursor := crDefault;
end;

procedure TFmFormulas.BtDesisteClick(Sender: TObject);
begin
  UMyMod.DesisteRegistroTb(FmFormulas, TbFormulas, 'Formulas', 'Numero',
   ImgTipo, TbFormulasNumero.Value);
end;

procedure TFmFormulas.TbFormulasItsBeforePost(DataSet: TDataSet);
begin
  UMyMod.BELY_Tb(TbFormulasIts, belyControle);
end;

procedure TFmFormulas.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
  Bold: Boolean;
begin
  if DBGrid1.Columns[0].Visible = True then
  begin
    if (Column.FieldName = 'STATUSPQ')then
    begin
      if TbFormulasItsSTATUSPQ.Value = CO_Forml_Status_Prod_Irregular then
      begin
        Cor  := clRed;
        Bold := True;
      end else
      begin
        Cor  := clGreen;
        Bold := False;
      end;
      with DBGrid1.Canvas do
      begin
        if Bold then
          Font.Style := [fsBold]
        else
          Font.Style := [];
        Font.Color := Cor;
        FillRect(Rect);
        TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      end;
    end;
  end;
end;

procedure TFmFormulas.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Ordem: Integer;
begin
  if key = VK_INSERT then
  begin
    if DBGrid1.ReadOnly = False then
    begin
      Ordem := TbFormulasItsOrdem.Value-1;
      TbFormulasIts.Insert;
      TbFormulasItsOrdem.Value := Ordem;
      key := 0;
    end;
  end else
  if key = VK_F4 then
  begin
    if ImgTipo.SQLType in ([stIns, stUpd]) then
    begin
      VAR_CADASTRO := 0;
      FmPrincipal.CadastroPQ(TbFormulasItsProduto.Value);
      if VAR_CADASTRO <> 0 then
      begin
        Screen.Cursor := crHourGlass;
        try
          ReopenPQs(ImgTipo.SQLType);
          //
          TbFormulasIts.Edit;
          TbFormulasItsProduto.Value := VAR_CADASTRO;
          TbFormulasIts.Post;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
  end else
  if key = VK_F7 then
  begin
    if ImgTipo.SQLType in ([stIns, stUpd]) then
    begin
      FmMeuDBUses.PesquisaNome('pq', 'Nome', 'Codigo', '');
      //
      if VAR_CADASTRO <> 0 then
      begin
        Screen.Cursor := crHourGlass;
        try
          TbFormulasIts.Edit;
          TbFormulasItsProduto.Value := VAR_CADASTRO;
          TbFormulasIts.Post;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
  end;
end;

procedure TFmFormulas.TbFormulasItsAfterPost(DataSet: TDataSet);
begin
  if not FReordenendo then ReordenaLinhas(False);
  AtualizaTbFormulas;
end;

procedure TFmFormulas.TbFormulasItsAfterDelete(DataSet: TDataSet);
begin
  if not FReordenendo then ReordenaLinhas(False);
  AtualizaTbFormulas;
end;

procedure TFmFormulas.TbFormulasBeforeClose(DataSet: TDataSet);
begin
  DBEdit14.ValueVariant := '';
  TbFormulasIts.Close;
end;

procedure TFmFormulas.TbFormulasBeforePost(DataSet: TDataSet);
begin

  //if TbFormulas.State = dsInsert then TbFormulasAtivo.Value := 1;
end;

procedure TFmFormulas.AtualizaTbFormulas();
var
  Numero, Controle: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando totais de tempos da f�rmula');
  Numero := TbFormulasNumero.Value;
  Controle := TbFormulasItsControle.Value;
  QrSoma.Close;
  QrSoma.Database := Dmod.MyDB;
  QrSoma.Params[0].AsInteger := Numero;
  UnDmkDAC_PF.AbreQuery(QrSoma, Dmod.MyDB);
  //
  TbFormulas.Edit;
  TbFormulasTempoP.Value := Trunc(QrSomaTempoP.Value);
  TbFormulasTempoR.Value := Trunc(QrSomaTempoR.Value);
  TbFormulasTempoT.Value := TbFormulasTempoR.Value + TbFormulasTempoP.Value;
  TbFormulas.Post;
  //
  if TbFormulasNumero.Value <> Numero then
  begin
    TbFormulas.Locate('Numero', Numero, []);
    TbFormulasIts.Locate('COntrole', Controle, []);
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
end;

procedure TFmFormulas.BtImportarClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImporta, BtImportar);
end;

procedure TFmFormulas.Caleiro1Click(Sender: TObject);
begin
  //;
end;

procedure TFmFormulas.CBSetorClick(Sender: TObject);
begin
  ReopenGraGruX();
end;

procedure TFmFormulas.Receitadeanlogos1Click(Sender: TObject);
begin
  //FmPrincipal.ImportaReceita;
end;

procedure TFmFormulas.SbImprimeClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSetor, Dmod.MyDB, [
    'SELECT TpReceita ',
    'FROM listasetores ',
    'WHERE Codigo=' + Geral.FF0(TbFormulasSetor.Value),
    '']);
  //
  case TReceitaTipoSetor(QrSetorTpReceita.Value) of
    //recsetrNenhum:
    rectipsetrRibeira:      MostraFormulasimpBH();
    rectipsetrRecurtimento: MostraFormulasimpWE();
    //recsetrAcabamento:
    else
    begin
      Geral.MB_Aviso(
        'Nenhum tipo de receita foi definido no cadastro do setor!' + sLineBreak +
        'Ser� utilizado o de ribeira!');
      MostraFormulasimpBH();
    end;
  end;
  if VAR_CADASTRO <> 0 then
    TbFormulas.Locate('Numero', VAR_CADASTRO, []);
end;

procedure TFmFormulas.TbFormulasItsBeforeInsert(DataSet: TDataSet);
begin
  FOrdem := TbFormulasItsOrdem.Value;
end;

procedure TFmFormulas.TbFormulasItsNewRecord(DataSet: TDataSet);
begin
  if FOrdem > 0 then
    TbFormulasItsOrdem.Value := FOrdem
  else
    TbFormulasItsOrdem.Value := 1;
end;

procedure TFmFormulas.BtDuplicaClick(Sender: TObject);
begin
  DuplicaRegistro2;
end;

procedure TFmFormulas.BtExcluiClick(Sender: TObject);
const
  Motivo = 0;
begin
  PQ_PF.TransfereReceitaDeArquivo(TOrientTabArqBD.stabdAcessivelToMorto,
    TbFormulasNumero.Value, Motivo);
  TbFormulas.Refresh;
  TbFormulas.Last;
end;
(* 2017/08/31 => Substitu�do pela fun��o: DuplicaRegistro2
procedure TFmFormulas.DuplicaRegistro;
var
  Cursor: TCursor;
  Formula, Controle, Atual: Integer;
begin
  if DBCheck.AcessoNegadoAoForm_4(afmoNegarComAviso, 'QUI-RECEI-DUP') then Exit;
  //
  Atual := TbFormulasNumero.Value;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Formula := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Formulas', 'Formulas', 'Numero');
    QrDup.Close;
    QrDup.SQL.Clear;
    QrDup.SQL.Add('INSERT INTO Formulas SET Numero=:P0,');
    QrDup.SQL.Add('Nome=:P1, DataI=:P2, Tecnico=:P3, Hidrica=:P4,');
    QrDup.SQL.Add('LinhaTE=:P5, Caldeira=:P6, Setor=:P7, ');
    QrDup.SQL.Add('Espessura=:P8, Peso=:P9, Qtde=:P10');
    QrDup.Params[0].AsInteger := Formula;
    QrDup.Params[1].AsString := TbFormulasNome.Value+'[C�pia]';
    QrDup.Params[2].AsDate := Date;
    QrDup.Params[3].AsString := TbFormulasTecnico.Value;
    QrDup.Params[4].AsInteger := TbFormulasHidrica.Value;
    QrDup.Params[5].AsInteger := TbFormulasLinhaTE.Value;
    QrDup.Params[6].AsInteger := TbFormulasCaldeira.Value;
    QrDup.Params[7].AsInteger := TbFormulasSetor.Value;
    QrDup.Params[8].AsInteger := TbFormulasEspessura.Value;
    QrDup.Params[9].AsFloat := TbFormulasPeso.Value;
    QrDup.Params[10].AsFloat := TbFormulasQtde.Value;
    QrDup.ExecSQL;
    TbFormulasIts.First;
    while not TbFormulasIts.Eof do
    begin
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'FormulasIts','FormulasIts', 'Controle');

      QrIns.Params[00].AsInteger := TbFormulasItsOrdem.Value;
      QrIns.Params[01].AsFloat   := Controle;
      QrIns.Params[02].AsFloat   := TbFormulasItsPorcent.Value;
      QrIns.Params[03].AsInteger := TbFormulasItsProduto.Value;
      QrIns.Params[04].AsFloat   := TbFormulasItsGraus.Value;
      QrIns.Params[05].AsInteger := TbFormulasItsTempoR.Value;
      QrIns.Params[06].AsInteger := TbFormulasItsTempoP.Value;
      QrIns.Params[07].AsFloat   := TbFormulasItspH_Min.Value;
      QrIns.Params[08].AsFloat   := TbFormulasItspH_Max.Value;
      QrIns.Params[09].AsFloat   := TbFormulasItsBe_Min.Value;
      QrIns.Params[10].AsFloat   := TbFormulasItsBe_Max.Value;
      QrIns.Params[11].AsString  := TbFormulasItsObs.Value;
      QrIns.Params[12].AsInteger := TbFormulasItsSC.Value;
      QrIns.Params[13].AsInteger := TbFormulasItsReciclo.Value;
      QrIns.Params[14].AsFloat   := TbFormulasItsDiluicao.Value;
      QrIns.Params[15].AsFloat   := TbFormulasItsMinimo.Value;
      QrIns.Params[16].AsFloat   := TbFormulasItsMaximo.Value;
      QrIns.Params[17].AsFloat   := TbFormulasItsGC_Min.Value;
      QrIns.Params[18].AsFloat   := TbFormulasItsGC_Max.Value;
      QrIns.Params[19].AsString  := TbFormulasItsProcesso.Value;
      QrIns.Params[20].AsString  := TbFormulasItsObsProces.Value;
      //
      QrIns.Params[21].AsInteger := Formula;
      QrIns.ExecSQL;

      TbFormulasIts.Next;
    end;
  finally
    Screen.Cursor := Cursor;
  end;
  TbFormulas.Refresh;
  TbFormulas.Locate('Numero', Formula, []);
  AtualizaTbFormulas;
  TbFormulas.Refresh;
  //
  Geral.MB_Aviso('A receita n� ' + Geral.FF0(Atual) +
    ' foi duplicada com sucesso!'+sLineBreak+'O n�mero da nova f�rmula � ' +
    Geral.FF0(Formula) + '!');
end;
*)

procedure TFmFormulas.DuplicaRegistro2;
var
  Nome: String;
  Formula, Atual, Controle: Integer;
begin
  if DBCheck.AcessoNegadoAoForm_4(afmoNegarComAviso, 'QUI-RECEI-DUP') then Exit;
  //
  Atual := TbFormulasNumero.Value;
  //
  if(TbFormulas.State <> dsInactive) and (TbFormulas.RecordCount > 0) and (Atual <> 0) then
  begin
    if Geral.MB_Pergunta('Confirma a duplica��o da receita atual?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        TbFormulas.DisableControls;
        TbFormulasIts.DisableControls;
        //
        Formula := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Formulas', 'Formulas', 'Numero');
        Nome    := Copy(TbFormulasNome.Value, 1, 23) + CO_COPIA;
        //
        if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'formulas', TMeuDB,
          ['Numero'], [Atual],
          ['Numero', 'Nome', 'DataI', 'DataCad'],
          [Formula, Nome, Date, Date],
          '', True, nil, nil) then
        begin
          if (TbFormulasIts.State <> dsInactive) and (TbFormulasIts.RecordCount > 0) then
          begin
            TbFormulasIts.First;
            while not TbFormulasIts.Eof do
            begin
              Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'FormulasIts','FormulasIts', 'Controle');
              //
              if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'formulasits', TMeuDB,
                ['Controle'], [TbFormulasItsControle.Value],
                ['Numero', 'Controle'],
                [Formula, Controle],
                '', False, nil, nil) then
              begin
                //
              end;
              TbFormulasIts.Next;
            end;
          end;
        end;
      finally
        TbFormulas.EnableControls;
        TbFormulasIts.EnableControls;
        //
        Screen.Cursor := crDefault;
      end;
      TbFormulas.Refresh;
      TbFormulas.Locate('Numero', Formula, []);
      AtualizaTbFormulas;
      TbFormulas.Refresh;
      //
      Geral.MB_Aviso('A receita n� ' + Geral.FF0(Atual) +
        ' foi duplicada com sucesso!'+sLineBreak+'O n�mero da nova f�rmula � ' +
        Geral.FF0(Formula) + '!');
    end;
  end;
end;

procedure TFmFormulas.SbNumeroClick(Sender: TObject);
var
  Numero: String;
begin
  Numero := IntToStr(TbFormulasNumero.Value);
  if InputQuery('Localiza F�rmula', 'Informe o n�mero da f�rmula desejada:',
  Numero) then TbFormulas.Locate('Numero', Numero, []);
  if TbFormulasIts.State = dsInactive then
    UnDmkDAC_PF.AbreTable(TbFormulasIts, Dmod.MyDB);
end;

procedure TFmFormulas.SbQueryClick(Sender: TObject);
var
  NumeroAtu: Integer;
begin
  NumeroAtu := TbFormulasNumero.Value;
  FRecPesq  := 0;
  //
  Application.CreateForm(TFmFormulasPesq, FmFormulasPesq);
  FmFormulasPesq.ShowModal;
  FmFormulasPesq.Destroy;
  //
  UnDmkDAC_PF.AbreTable(TbFormulas, Dmod.MyDB);
  //
  if FRecPesq <> 0 then
    TbFormulas.Locate('Numero', FRecPesq, [])
  else if NumeroAtu <> 0 then
    TbFormulas.Locate('Numero', NumeroAtu, [])
  else
    TbFormulas.Last;
  if TbFormulasIts.State = dsInactive then
    UnDmkDAC_PF.AbreTable(TbFormulasIts, Dmod.MyDB);
end;

procedure TFmFormulas.TbFormulasAfterInsert(DataSet: TDataSet);
begin
  UMyMod.BELY_Tb(TbFormulas, belyNumero);
end;

procedure TFmFormulas.TbFormulasAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := TbFormulasNumero.Value > 0;
  //
  DBEdit14.ValueVariant := Geral.FDT(TbFormulasDataAlt.Value, 3, True);
  //
  ReopenGraGruX();
end;

procedure TFmFormulas.SpeedButton5Click(Sender: TObject);
begin
  FmPrincipal.CadastroDeEspessuras;
  ReopenEspessura();
  CBEspessura.KeyValue := QrEspessurasCodigo.Value;
end;

procedure TFmFormulas.SpeedButton6Click(Sender: TObject);
var
  Setor: Integer;
begin
  VAR_CADASTRO := 0;
  Setor        := CBSetor.KeyValue;
  //
  FmPrincipal.CadastroDeSetores(Setor);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrListaSetores.Close;
    UnDmkDAC_PF.AbreQuery(QrListaSetores, Dmod.MyDB);
    //
    QrListaSetores.Locate('Codigo', VAR_CADASTRO, []);
    //
    CBSetor.KeyValue := QrListaSetoresCodigo.Value;
    CBSetor.SetFocus;
  end;
end;

procedure TFmFormulas.SpeedButton7Click(Sender: TObject);
begin
  FmPrincipal.CadastroDeEspessuras;
  ReopenEspessura();
  CBRebaixe.KeyValue := QrRebaixeCodigo.Value;
end;

end.
