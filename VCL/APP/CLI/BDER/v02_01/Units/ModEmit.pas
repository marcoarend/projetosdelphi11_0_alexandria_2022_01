unit ModEmit;

interface

uses
  System.SysUtils, System.Classes, Vcl.ComCtrls, dmkGeral, Data.DB, Vcl.Forms,
  mySQLDbTables, frxClass, frxDBSet, Vcl.StdCtrls, dmkEdit, UnDmkEnums,
  UnProjGroup_Consts, Variants, UnAppEnums;

type
  TDmModEmit = class(TDataModule)
    QrLotes: TmySQLQuery;
    QrLotesControle: TIntegerField;
    QrLotesMPIn: TIntegerField;
    QrLotesPeso: TFloatField;
    QrLotesPecas: TFloatField;
    QrEmit: TmySQLQuery;
    QrEmitCodigo: TIntegerField;
    QrEmitDataEmis: TDateTimeField;
    QrEmitStatus: TSmallintField;
    QrEmitNumero: TIntegerField;
    QrEmitNOMECI: TWideStringField;
    QrEmitNOMESETOR: TWideStringField;
    QrEmitTecnico: TWideStringField;
    QrEmitNOME: TWideStringField;
    QrEmitClienteI: TIntegerField;
    QrEmitTipificacao: TSmallintField;
    QrEmitTempoR: TIntegerField;
    QrEmitTempoP: TIntegerField;
    QrEmitSetor: TSmallintField;
    QrEmitEspessura: TWideStringField;
    QrEmitDefPeca: TWideStringField;
    QrEmitPeso: TFloatField;
    QrEmitCusto: TFloatField;
    QrEmitQtde: TFloatField;
    QrEmitAreaM2: TFloatField;
    QrEmitFulao: TWideStringField;
    QrEmitObs: TWideStringField;
    QrEmitTipific: TSmallintField;
    QrEmitLk: TIntegerField;
    QrEmitDataCad: TDateField;
    QrEmitDataAlt: TDateField;
    QrEmitUserCad: TIntegerField;
    QrEmitUserAlt: TIntegerField;
    QrEmitAlterWeb: TSmallintField;
    QrEmitAtivo: TSmallintField;
    QrEmitFlu: TmySQLQuery;
    QrEmitFluCodigo: TIntegerField;
    QrEmitFluControle: TIntegerField;
    QrEmitFluOrdemTin: TIntegerField;
    QrEmitFluOrdemFlu: TIntegerField;
    QrEmitFluTintasFlu: TIntegerField;
    QrEmitFluNomeFlu: TWideStringField;
    QrEmitFluNomeTin: TWideStringField;
    QrEmitFluTintasTin: TIntegerField;
    QrEmitFluGramasM2: TFloatField;
    QrEmitFluGramasTo: TFloatField;
    QrEmitFluCustoTo: TFloatField;
    QrEmitFluCustoKg: TFloatField;
    QrEmitFluAreaM2: TFloatField;
    QrEmitFluCustoM2: TFloatField;
    QrEmitFluCusUSM2: TFloatField;
    QrEmitFluLk: TIntegerField;
    QrEmitFluDataCad: TDateField;
    QrEmitFluDataAlt: TDateField;
    QrEmitFluUserCad: TIntegerField;
    QrEmitFluUserAlt: TIntegerField;
    QrEmitFluAlterWeb: TSmallintField;
    QrEmitFluAtivo: TSmallintField;
    QrEmitIts: TmySQLQuery;
    QrEmitItsCodigo: TIntegerField;
    QrEmitItsControle: TIntegerField;
    QrEmitItsNomePQ: TWideStringField;
    QrEmitItsPQCI: TIntegerField;
    QrEmitItsCli_Orig: TIntegerField;
    QrEmitItsCli_Dest: TIntegerField;
    QrEmitItsCustoPadrao: TFloatField;
    QrEmitItsMoedaPadrao: TIntegerField;
    QrEmitItsNumero: TIntegerField;
    QrEmitItsOrdem: TIntegerField;
    QrEmitItsPorcent: TFloatField;
    QrEmitItsProduto: TIntegerField;
    QrEmitItsTempoR: TIntegerField;
    QrEmitItsTempoP: TIntegerField;
    QrEmitItspH: TFloatField;
    QrEmitItsBe: TFloatField;
    QrEmitItsObs: TWideStringField;
    QrEmitItsCusto: TFloatField;
    QrEmitItsGraus: TFloatField;
    QrEmitItsBoca: TWideStringField;
    QrEmitItsProcesso: TWideStringField;
    QrEmitItsObsProces: TWideStringField;
    QrEmitItsPeso_PQ: TFloatField;
    QrEmitItsProdutoCI: TIntegerField;
    QrEmitItspH_Min: TFloatField;
    QrEmitItspH_Max: TFloatField;
    QrEmitItsBe_Min: TFloatField;
    QrEmitItsBe_Max: TFloatField;
    QrEmitItsGC_Min: TFloatField;
    QrEmitItsGC_Max: TFloatField;
    QrEmitItsGraGruX: TIntegerField;
    QrEmitItsOrdemFlu: TIntegerField;
    QrEmitItsOrdemIts: TIntegerField;
    QrEmitItsTintas: TIntegerField;
    QrEmitItsGramasTi: TFloatField;
    QrEmitItsGramasKg: TFloatField;
    QrEmitItsGramasTo: TFloatField;
    QrEmitItsSiglaMoeda: TWideStringField;
    QrEmitItsPrecoMo_Kg: TFloatField;
    QrEmitItsCotacao_Mo: TFloatField;
    QrEmitItsCustoRS_Kg: TFloatField;
    QrEmitItsCustoRS_To: TFloatField;
    QrEmitItsLk: TIntegerField;
    QrEmitItsDataCad: TDateField;
    QrEmitItsDataAlt: TDateField;
    QrEmitItsUserCad: TIntegerField;
    QrEmitItsUserAlt: TIntegerField;
    QrEmitItsAlterWeb: TSmallintField;
    QrEmitItsAtivo: TSmallintField;
    QrEmitItsDiluicao: TFloatField;
    QrEmitItsMinimo: TFloatField;
    QrEmitItsMaximo: TFloatField;
    frxDsEmit: TfrxDBDataset;
    frxDsEmitFlu: TfrxDBDataset;
    frxDsEmitIts: TfrxDBDataset;
    frxQUI_RECEI_105_001: TfrxReport;
    frxQUI_RECEI_105_003: TfrxReport;
    QrFalta: TmySQLQuery;
    QrFaltaProduto: TIntegerField;
    QrFaltaNome: TWideStringField;
    QrFaltaQtdEstq: TFloatField;
    QrFaltaQtdGastar: TFloatField;
    QrFaltaQTD_SALDO: TFloatField;
    frxDsFalta: TfrxDBDataset;
    frxQUI_RECEI_105_002: TfrxReport;
    QrEmitSetrEmi: TSmallintField;
    QrEmitCod_Espess: TIntegerField;
    QrEmitCodDefPeca: TIntegerField;
    QrEmitCustoTo: TFloatField;
    QrEmitCustoKg: TFloatField;
    QrEmitCustoM2: TFloatField;
    QrEmitCusUSM2: TFloatField;
    QrSumEmitCus: TmySQLQuery;
    QrSumEmitCusPeso: TFloatField;
    QrSumEmitCusPecas: TFloatField;
    QrSumEmitCusAreaM2: TFloatField;
    QrSumMPVIts: TmySQLQuery;
    QrSumMPVItsCusto: TFloatField;
    QrEmitFluInfoCargM2: TFloatField;
    QrSumWBMovIts: TmySQLQuery;
    QrSumWBMovItsValorT: TFloatField;
    QrEmitFluDescriFlu: TWideStringField;
    frxQUI_RECEI_105_004: TfrxReport;
    QrPesaCab: TmySQLQuery;
    QrPesaCabTintasTin: TIntegerField;
    QrPesaCabNomeTin: TWideStringField;
    QrPesaIts: TmySQLQuery;
    QrPesaItsTintas: TIntegerField;
    QrPesaItsProduto: TIntegerField;
    QrPesaItsNomePQ: TWideStringField;
    QrPesaItsGramasTi: TFloatField;
    QrPesaItsGramasKg: TFloatField;
    QrPesaItsGramasTo: TFloatField;
    frxDsPesaCab: TfrxDBDataset;
    frxDsPesaIts: TfrxDBDataset;
    QrPesaCabOrdemFlu: TIntegerField;
    QrPesaCabCodigo: TIntegerField;
    QrPesaItsOrdem: TIntegerField;
    QrPesaCabGramasTo: TFloatField;
    QrEmitFluOpProc: TSmallintField;
    QrLotesSourcMP: TIntegerField;
    QrLotesAreaM2: TFloatField;
    procedure QrEmitFluAfterScroll(DataSet: TDataSet);
    procedure QrEmitFluBeforeClose(DataSet: TDataSet);
    procedure QrPesaCabAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    FEmitIts: String;
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo
    //
  public
    { Public declarations }
    //
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
    //
    function  AtualizaCustosEmit(SumC: Double; PreUsoCodigo, Emit: Integer): Boolean;
    procedure AtualizaQuantidadesSourcMP_WetSome(Tabela: String;Database:
              TmysqlDatabase; Codigo: Integer; EdPeso, EdPecas, EdAreaM2:
              TCustomMemo);
    procedure AtualizaMPVIts(MPVIts: Integer);
    function  CalculaAreaPelokg(Peso, Espessura, PEspecifico: Double; Grandeza:
              Integer): Double;//1-m� 2-ft�
    function  CalculaValorPQ2(Preco, ICMS, RICMS, IPI, RIPI, PIS, RPIS, COFINS,
              RCOFINS, Frete, PrecoUsuario: Double; Moeda: Integer): Double;
    function  EfetuaBaixa(PB1: TProgressBar; Data: TDateTime; TipoPreco,
              CliIntCod, Emit: Integer; QrPreuso, QrLotes: TmySQLQuery;
              QrPreusoMoedaPadrao: TIntegerField; QrPreusoCustoPadrao,
              QrPreusoPeso_PQ, QrPreusoCustoMedio: TFloatField;
              QrPreusoCli_Orig, QrPreusoProduto, QrPreUsoCodigo,
              QrPreusoControle: TIntegerField;
              SourcMP: TTipoSourceMP; DtCorrApo: String;
              Empresa: Integer): Boolean;
    procedure ExcluiLoteCus(Emit: Integer; MovimID: TEstqMovimID);
    function  MostraPrecosAlternativos(TipoReceita: TTipoReceitas;
              Receita: Integer): Boolean;
    procedure MostraReceitaDeAcabamento(Codigo, ModeloImp: Integer; TempTable: Boolean);
    function  PrecoEmReais(Moeda: Integer; Preco, PrecoUsuario: Double): Double;
    procedure ReopenEmit(DataBase: TmySQLDatabase; Tabela: String; Codigo: Integer);
    procedure ReopenEmitFlu(DataBase: TmySQLDatabase; Tabela, TabIts: String; Codigo: Integer);
    procedure ReopenEmitIts(DataBase: TmySQLDatabase; Tabela: String);
    procedure ReopenPesaCab(DataBase: TmySQLDatabase; Tabela: String; Codigo: Integer);
  end;

var
  DmModEmit: TDmModEmit;

implementation

{$R *.dfm}

{ TDmModEmit }

uses Module, DmkDAC_PF, UMySQLModule, PQx, UnInternalConsts, UnMyObjects,
ModuleGeral, ModVS, UnPQ_PF, ModVS_CRC;

function TDmModEmit.AtualizaCustosEmit(SumC: Double; PreUsoCodigo,
  Emit: Integer): Boolean;
const
  Status = 1;
var
  Codigo, Controle: Integer;
  Custo, PesoS, AreaS, Fator, PesoA, Custokg, PercTotCus: Double;
  Grandeza: TTipoCalcCouro;
  // = (ptcNaoInfo=0, ptcPecas=1, ptcPesoKg=2, ptcAreaM2=3, ptcAreaP2=4, ptcTotal=5);

begin
  Result := False;
  if SumC <= 0 then
  begin
    Geral.MB_Aviso('Valor do custo da pesagem ' + Geral.FF0(Emit) +
      ' n�o definido [1]!');
    Result := False;
    Exit;
  end;
{
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE Emit SET Custo=:P0, Status=1 WHERE Codigo=:P1');
  Dmod.QrUpdM.Params[0].AsFloat   := SumC;
  Dmod.QrUpdM.Params[1].AsInteger := Emit;
  Dmod.QrUpdM.ExecSQL;
}
  Codigo := Emit;
  Custo := SumC;
  if UMyMod.SQLInsUpd(Dmod.QrUpdM, stUpd, 'emit', False, [
  'Custo', 'Status'], [
  'Codigo'], [
  Custo, Status], [
  Codigo], True) then
  begin
{
    QrLotes.Close;
    QrLotes.Params[0].AsInteger := Emit;
    QrLotes.O p e n;
}
    UnDMkDAC_PF.AbreMySQLQuery0(QrLotes, DMod.MyDB, [
    'SELECT ec.Controle, ec.MPIn, ec.Peso, ec.Pecas, ',
    //'mi.Ficha, mi.Marca, mi.Lote, ',
    'ec.AreaM2, em.SourcMP ',
    'FROM emitcus ec ',
    //'LEFT JOIN mpin mi ON mi.Controle=ec.MPIn ',
    'LEFT JOIN emit em ON em.Codigo=ec.Codigo ',
    'WHERE ec.Codigo=' + Geral.FF0(Emit),
    '']);
    //
    PesoS := 0;
    AreaS := 0;
    QrLotes.First;
    while not QrLotes.Eof do
    begin
      PesoS := PesoS + QrLotesPeso.Value;
      AreaS := AreaS + QrLotesAreaM2.Value;
      Qrlotes.Next;
    end;
    //
    Grandeza := ptcNaoInfo;
  // = (ptcNaoInfo=0, ptcPecas=1, ptcPesoKg=2, ptcAreaM2=3, ptcAreaP2=4, ptcTotal=5);

    if (QrLotesSourcMP.Value = Integer(dmktfrmSourcMP_VS_FI)) and (AreaS > 0) then
      Grandeza := ptcAreaM2
    else
    if (QrLotesSourcMP.Value <> Integer(dmktfrmSourcMP_VS_FI)) and (PesoS > 0) then
      Grandeza :=  ptcPesoKg;
    if (Grandeza <> ptcNaoInfo) and (SumC > 0) then      ///  \
    begin                                  ///   \   C�lculo
      QrLotes.First;
      while not QrLotes.Eof do
      begin
        if Grandeza = ptcAreaM2 then
        begin
          Custo := QrLotesAreaM2.Value / AreaS * SumC;
          PercTotCus := QrLotesAreaM2.Value / AreaS * 100;
        end else
        begin
          Custo := QrLotesPeso.Value / PesoS * SumC;
          PercTotCus := QrLotesPeso.Value / PesoS * 100;
        end;
        //
        Controle := QrLotesControle.Value;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'emitcus', False, [
        'Custo', 'PercTotCus'], [
        'Controle'], [
        Custo, PercTotCus], [Controle], True);
        //
        Qrlotes.Next;
      end;
    end;
    Result := True;
  end;
end;

procedure TDmModEmit.AtualizaMPVIts(MPVIts: Integer);
var
  Controle: Integer;
  CustoPQ, CustoWB: Double;
begin
  Controle := MPVIts;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumMPVIts, Dmod.MyDB, [
  'SELECT SUM(Custo) Custo ',
  'FROM emitcus ',
  'WHERE MPVIts=' + Geral.FF0(MPVIts),
  '']);
  CustoPQ := QrSumMPVItsCusto.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumWBMovIts, Dmod.MyDB, [
  'SELECT SUM(wmi.ValorT) ValorT ',
  'FROM wbmovits wmi ',
  'WHERE wmi.MovimID=' + Geral.FF0(Integer(emidIndsWE)),
  'AND wmi.Codigo=' + Geral.FF0(MPVIts),
  '']);
  CustoWB := - QrSumWBMovItsValorT.Value;
  //
  Controle := MPVIts;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mpvits', False, [
  'CustoPQ', 'CustoWB'], ['Controle'], [
  CustoPQ, CustoWB], [Controle], True);
end;

procedure TDmModEmit.AtualizaQuantidadesSourcMP_WetSome(Tabela: String;Database:
TmysqlDatabase; Codigo: Integer; EdPeso, EdPecas, EdAreaM2: TCustomMemo);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumEmitCus, Database, [
  'SELECT SUM(Peso) Peso, ',
  'SUM(Pecas) Pecas, ',
  'SUM(AreaM2) AreaM2 ',
  'FROM ' + Tabela,
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  if (QrSumEmitCusPeso.Value >= 0.001)
  or (QrSumEmitCusAreaM2.Value >= 0.01) then
  begin
    if EdPecas <> nil then
      TdmkEdit(EdPecas).ValueVariant  := QrSumEmitCusPecas.Value;
    if EdPeso <> nil then
      TdmkEdit(EdPeso).ValueVariant   := QrSumEmitCusPeso.Value;
    if EdAreaM2 <> nil then
      TdmkEdit(EdAreaM2).ValueVariant := QrSumEmitCusAreaM2.Value;
  end;
end;

function TDmModEmit.CalculaAreaPelokg(Peso, Espessura, PEspecifico: Double;
  Grandeza: Integer): Double;
var
  m : Double;
begin
  m := 0;
  if (PEspecifico > 0) and (Espessura > 0) then
    m := Peso / PEspecifico / Espessura * 10000;
  if Grandeza = 2 then Result := m / VAR_PE_QUADRADO
  else Result := m;
end;

{
function TDmModEmit.CalculaValorPQ2(Preco, ICMS, RICMS, IPI, RIPI, PIS, RPIS,
  COFINS, RCOFINS, Frete, PrecoUsuario: Double; Moeda: Integer): Double;
var
  PISCOFINSX, RPISCOFINSX, ICMSX, RICMSX, IPIX, RIPIX, PrecoX, V_ICMS, V_RICMS,
  VPISCOFINS, VRPISCOFINS, V_IPI, V_RIPI: Double;
begin
  PISCOFINSX  := 1-((PIS + COFINS) /100);
  ICMSX       := 1-(ICMS / 100);
  RPISCOFINSX := 1-((RPIS + RCOFINS) /100);
  RICMSX      := 1-(RICMS / 100);
  IPIX        := IPI / 100;
  RIPIX       := RIPI / 100;
  PrecoX      := PrecoEmReais(Moeda, Preco, PrecoUsuario);
  if Moeda > -1 then
  begin
    VPISCOFINS  := (Trunc(((PrecoX / PISCOFINSX) - PrecoX) * 100 + 0.5))/100;
    VRPISCOFINS := (Trunc(((PrecoX / RPISCOFINSX) - PrecoX)* 100 + 0.5))/100;
    //
    PrecoX := PrecoX + VPISCOFINS;
    //
    V_ICMS      := (Trunc(((PrecoX / ICMSX) - PrecoX) * 100 + 0.5))/100;
    V_RICMS     := (Trunc(((PrecoX / RICMSX) - PrecoX)* 100 + 0.5))/100;
    //
    PrecoX := PrecoX + V_ICMS;
    //
    V_IPI   := PrecoX * IPIX;
    V_RIPI  := PrecoX * RIPIX;
    Result  := PrecoX + V_IPI + Frete - V_RICMS - V_RIPI - VRPISCOFINS;
  end else Result := PrecoUsuario;
end;
}

function TDmModEmit.CalculaValorPQ2(Preco, ICMS, RICMS, IPI, RIPI, PIS, RPIS,
  COFINS, RCOFINS, Frete, PrecoUsuario: Double; Moeda: Integer): Double;
var
  PISCOFINSX, RPISCOFINSX, ICMSX, RICMSX, IPIX, RIPIX, PrecoX, V_ICMS, V_RICMS,
  VPISCOFINS, VRPISCOFINS, V_IPI, V_RIPI, Impostos, PrecoA: Double;
begin
  PISCOFINSX  := ((PIS + COFINS) /100);
  ICMSX       := (ICMS / 100);
  RPISCOFINSX := ((RPIS + RCOFINS) /100);
  RICMSX      := (RICMS / 100);
  IPIX        := IPI / 100;
  RIPIX       := RIPI / 100;
  PrecoX      := PrecoEmReais(Moeda, Preco, PrecoUsuario);
  if Moeda > -1 then
  begin
    Impostos := 1 - ( (PISCOFINSX + ICMSX) - (RPISCOFINSX + RICMSX) );
    if Impostos > 0 then
      PrecoA := Geral.RoundC(PrecoX / Impostos, 2)
    else
      PrecoA := PrecoX;
    //
    V_IPI   := PrecoA * IPIX;
    V_RIPI  := PrecoA * RIPIX;
    //
    Result  := PrecoA + V_IPI + Frete;
    //
  end else Result := PrecoUsuario;
end;

procedure TDmModEmit.DefineProperties(Filer: TFiler);
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

function TDmModEmit.EfetuaBaixa(PB1: TProgressBar; Data: TDateTime; TipoPreco,
  CliIntCod, Emit: Integer; QrPreuso, QrLotes: TmySQLQuery;
  QrPreusoMoedaPadrao: TIntegerField; QrPreusoCustoPadrao, QrPreusoPeso_PQ,
  QrPreusoCustoMedio: TFloatField; QrPreusoCli_Orig, QrPreusoProduto,
  QrPreUsoCodigo, QrPreusoControle: TIntegerField; SourcMP: TTipoSourceMP;
  DtCorrApo: String; Empresa: Integer): Boolean;
const
  Unitario = False;
var
  DataP: String;
  Custo, SumC: Double;
  DataX: String;
  //Retorno, StqMovIts,
  OrigemCodi, OrigemCtrl, Tipo, CliOrig, CliDest, Insumo: Integer;
  Peso, Valor: Double;
begin
  try
    SumC := 0;
    if PB1 <> nil then
    begin
      PB1.Position := 0;
      PB1.Max := QrPreuso.RecordCount;
    end;
    DataP := Geral.FDT(Data, 1);
    QrPreuso.First;
    while not QrPreuso.Eof do
    begin
      if PB1 <> nil then
      begin
        PB1.Position := PB1.Position - 1;
        PB1.Update;
      end;
      Application.ProcessMessages;
      //
      case TipoPreco of
        1: Custo := QrPreusoPeso_PQ.Value * PrecoEmReais(
           QrPreusoMoedaPadrao.Value, QrPreusoCustoPadrao.Value, 0);
        2: Custo := 0;
        else Custo := QrPreusoPeso_PQ.Value * QrPreusoCustoMedio.Value;
      end;
      SumC := SumC + Custo;
{
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('INSERT INTO p q x SET DataX=:P0, ');
      Dmod.QrUpdM.SQL.Add('CliOrig=:P1, CliDest=:P2, Insumo=:P3, Peso=:P4, ');
      Dmod.QrUpdM.SQL.Add('Valor=:P5, OrigemCodi=:P6, OrigemCtrl=:P7, Tipo=:P8 ');
      Dmod.QrUpdM.Params[00].AsString  := DataP;
      Dmod.QrUpdM.Params[01].AsInteger := QrPreusoCli_Orig.Value;
      Dmod.QrUpdM.Params[02].AsInteger := CliIntCod;
      Dmod.QrUpdM.Params[03].AsInteger := QrPreusoProduto.Value;
      Dmod.QrUpdM.Params[04].AsFloat   := -QrPreusoPeso_PQ.Value;
      Dmod.QrUpdM.Params[05].AsFloat   := -Custo;
      Dmod.QrUpdM.Params[06].AsInteger := QrPreUsoCodigo.Value;
      Dmod.QrUpdM.Params[07].AsInteger := QrPreusoControle.Value;
      Dmod.QrUpdM.Params[08].AsInteger := VAR_FATID_0110;// mudar para VAR_FATID_1110 ?;
      Dmod.QrUpdM.ExecSQL;
}
      DataX        := DataP;
      CliOrig      := QrPreusoCli_Orig.Value;
      CliDest      := CliIntCod;
      Insumo       := QrPreusoProduto.Value;
      Peso         := -QrPreusoPeso_PQ.Value;
      Valor        := -Custo;
      OrigemCodi   := QrPreUsoCodigo.Value;
      OrigemCtrl   := QrPreusoControle.Value;
      Tipo         := VAR_FATID_0110;// mudar para VAR_FATID_1110 ?;
      //Retorno        := ;
      //StqMovIts      := ;
      {
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'p q x', False, [
      'DataX', 'CliOrig', 'CliDest',
      'Insumo', 'Peso', 'Valor'(*,
      'Retorno', 'StqMovIts'*)], [
      'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
      DataX, CliOrig, CliDest,
      Insumo, Peso, Valor(*,
      Retorno, StqMovIts*)], [
      OrigemCodi, OrigemCtrl, Tipo], False);
      }
      //
      PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OrigemCodi,
      OrigemCtrl, Tipo, Unitario, DtCorrApo, Empresa);
      //
      UnPQx.AtualizaEstoquePQ(QrPreusoCli_Orig.Value, QrPreusoProduto.Value,
        Empresa, aeMsg, CO_VAZIO);
      QrPreuso.Next;
    end;
    AtualizaCustosEmit(SumC, QrPreUsoCodigo.Value, Emit);
    if QrLotes <> nil then
    begin
      if QrLotes.State <> dsInactive then
      begin
        QrLotes.First;
        while not QrLotes.Eof do
        begin
          // Somente para caleiro e curtimento
          case TTipoSourceMP(SourcMP) of
            dmktfrmSourcMP_Ribeira:
            begin
              DmModVS.AtualizaVSMovIts_CusEmit(QrLotes.FieldByName(CO_FLD_TAB_VMI).AsInteger);
              //Geral.MB_Aviso('Parei aqui! ver: ' + sLineBreak +
              //'function TDmModVS.AtualizaVSMovIts_CusEmit(Controle: Integer): Boolean;'
              //+ sLineBreak + 'function TDmModEmit.EfetuaBaixa()');
            end;
            dmktfrmSourcMP_WetSome:
            begin
              //  Ver se precisa no futuro!
              //AtualizaMPVIts(QrLotes.FieldByName('MPVIts').AsInteger);
              // Novo! 2016-03-24
              DmModVS.AtualizaVSMovIts_CusEmit(QrLotes.FieldByName(CO_FLD_TAB_VMI).AsInteger);
            end;
            dmktfrmSourcMP_VS_BH,
            dmktfrmSourcMP_VS_WE,
            dmktfrmSourcMP_VS_FI:
            begin
              DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrLotes.FieldByName('MovimCod').AsInteger)
            end;
            //dmktfrmSourcMP_INDEFIN:
            else Geral.MB_Erro(
              '"FSourcMP" n�o definido para atualiza��o de  dados!');
          end;
          QrLotes.Next;
        end;
      end;
    end;
    PQ_PF.VerificaEquiparacaoEstoque(True);
    Result := True;
  except
    raise;
  end;
end;

procedure TDmModEmit.ExcluiLoteCus(Emit: Integer; MovimID: TEstqMovimID);
begin
{
  if (FEmit > 0) then
  begin
    ReopenLotes;
    if (QrLotes.RecordCount > 0) then
    begin
      Screen.Cursor := crHourGlass;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM emitcus ');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 ');
      Dmod.QrUpd.Params[0].AsInteger := FEmit;
      Dmod.QrUpd.ExecSQL;
      //
      Screen.Cursor := crDefault;
    end;
  end;
}
  if Emit <> 0 then
  begin
    if MovimID <> Null then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM wbmovits ',
      'WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
      'AND LnkNivXtr1=' + Geral.FF0(Emit),
      '']);
    end;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM emitcus ',
    'WHERE Codigo=' + Geral.FF0(Emit),
    '']);
  end;
end;

function TDmModEmit.MostraPrecosAlternativos(TipoReceita: TTipoReceitas;
  Receita: Integer): Boolean;
var
  Texto: String;
begin
  Result := False;
  Geral.MB_Aviso('Desativado por falta de uso!');
{ Ver o que fazer!
  Texto := 'Codigo in (';
  QrProds.Close;
  QrProds.Params[0].AsInteger := Receita;
  QrProds. O p e n ;
  Application.CreateForm(TFmFormulasImpEdit, FmFormulasImpEdit);
  while not QrProds.Eof do
  begin
    if QrProdsProduto1.Value > 0 then Texto := Texto + ''''+IntToStr(QrProdsProduto1.Value)+''',';
    if QrProdsProduto2.Value > 0 then Texto := Texto + ''''+IntToStr(QrProdsProduto2.Value)+''',';
    if QrProdsProduto3.Value > 0 then Texto := Texto + ''''+IntToStr(QrProdsProduto3.Value)+''',';
    if QrProdsProduto4.Value > 0 then Texto := Texto + ''''+IntToStr(QrProdsProduto4.Value)+''',';
    if QrProdsProduto5.Value > 0 then Texto := Texto + ''''+IntToStr(QrProdsProduto5.Value)+''',';
    QrProds.Next;
  end;
  Texto := Texto +'-10000)';
  FmFormulasImpEdit.TbPQ.Filter := Texto;
  FmFormulasImpEdit.TbPQ.Filtered := True;
  FmFormulasImpEdit.TbPQ. O p e n ;
  FmFormulasImpEdit.ShowModal;
  FmFormulasImpEdit.Destroy;
  //
  Result := True;
}
end;

procedure TDmModEmit.MostraReceitaDeAcabamento(Codigo, ModeloImp: Integer;
  TempTable: Boolean(*;
OS, SP, AreaM2, Qtde, DefPeca, CustoTotalTintas, CustoTotalM2, CustoTotUSM2,
CliInt: String*));
  procedure InsereVariaveisFixas(frx: TfrxReport);
  var
    Txt: String;
  begin
    if TempTable then
    //if QrEmitCodigo.Value = 0 then
      Txt := 'SEM BAIXA'
    else
      Txt := 'Pesagem N� ' + Geral.FF0(QrEmitCodigo.Value);
    //
    frx.Variables['VARF_PESAGEM'] := QuotedStr(Txt);
    frx.Variables['VARF_Area']    := QuotedStr(Geral.FFT(QrEmitAreaM2.Value, 2, siNegativo) + ' m�');
    frx.Variables['VARF_Qtde']    := QuotedStr(Geral.FFT(QrEmitQtde.Value, 1, siNegativo) + ' ' + QrEmitDefPeca.Value);
{
    frx.Variables['OS'] := QuotedStr(VAR_OS);
    frx.Variables['SP'] := QuotedStr(VAR_SP);
    frx.Variables['VARF_SOMATINTAS'] := QuotedStr(FCustoTotalTintas);
    frx.Variables['VARF_CUSTOTOTALM2'] := QuotedStr(FCustoTotalM2);
    frx.Variables['VARF_CUSTOTOTUSM2'] := QuotedStr(FCustoTotUSM2);
    frx.Variables['VARF_NO_EMPRESA'] := QuotedStr(CBCliInt.Text);
}
  end;
var
  ModImp: Integer;
  TabEmit, TabEmitFlu, TabEmitIts: String;
  Database: TmySQLDatabase;
begin
  if TempTable then
  begin
    TabEmit := '_emit_';
    TabEmitFlu := '_emit_flu_';
    TabEmitIts := '_emit_its_';
    Database  := DMOdG.MyPId_DB;
  end else begin
    TabEmit := 'emit';
    TabEmitFlu := 'emitflu';
    TabEmitIts := 'emitits';
    Database  := Dmod.MyDB;
  end;

  if ModeloImp > -1 then
    ModImp := ModeloImp
  else
  begin
    ModImp := MyObjects.SelRadioGroup('Fomato de Impress�o',
    'Selecione o formato de impress�o', ['Pesagem', 'Custos'], 0);
    if ModImp < 0 then
      Exit;
  end;
  ReopenEmit(Database, TabEmit, Codigo);
  //ReopenEmitIts();  // <- Fazer entes do EmitFlu para evitar bugs no flitro ?
  ReopenEmitFlu(Database, TabEmitFlu, TabEmitIts, Codigo);
  case ModImp of
    0:
    begin
      InsereVariaveisFixas(frxQUI_RECEI_105_001);
      MyObjects.frxDefineDataSets(frxQUI_RECEI_105_001, [
        frxDsEmit,
        frxDsEmitFlu,
        frxDsEmitIts,
        DmodG.frxDsMaster
        ]);
      MyObjects.frxMostra(frxQUI_RECEI_105_001, 'Pesagem de tinta de acabamento');
      //
      //
      ReopenPesaCab(Database, TabEmitFlu, Codigo);
      InsereVariaveisFixas(frxQUI_RECEI_105_004);
      MyObjects.frxDefineDataSets(frxQUI_RECEI_105_004, [
        frxDsEmit,
        frxDsPesaCab,
        frxDsPesaIts,
        DmodG.frxDsMaster
        ]);
      MyObjects.frxMostra(frxQUI_RECEI_105_004, 'Pesagem de tinta de acabamento');
      //
    end;
    1:
    begin
      InsereVariaveisFixas(frxQUI_RECEI_105_002);
      //
      MyObjects.frxDefineDataSets(frxQUI_RECEI_105_002, [
        frxDsEmit,
        frxDsEmitFlu,
        frxDsEmitIts,
        DmodG.frxDsMaster
        ]);
      //
      MyObjects.frxMostra(frxQUI_RECEI_105_002, 'Custo de tinta de acabamento');
    end;
    else Geral.MB_Erro('Modelo de impress�o n�o implementado');
  end;
end;

function TDmModEmit.PrecoEmReais(Moeda: Integer; Preco, PrecoUsuario: Double):
  Double;
begin
  if VAR_CAMBIO_DATA <> Trunc(Now()) then
    DModG.ReopenCambio();
  case Moeda of
   -1: Result := PrecoUsuario;
    0: Result := Preco;
    1: Result := Preco * VAR_CAMBIO_USD;
    2: Result := Preco * VAR_CAMBIO_EUR;
    3: Result := Preco * VAR_CAMBIO_IDX;
    else Result := 0;
  end;
end;

procedure TDmModEmit.QrEmitFluAfterScroll(DataSet: TDataSet);
begin
  ReopenEmitIts(QrEmitFlu.Database, FEmitIts);
end;

procedure TDmModEmit.QrEmitFluBeforeClose(DataSet: TDataSet);
begin
  QrEmitIts.Close;
end;

procedure TDmModEmit.QrPesaCabAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesaIts, QrPesaCab.Database, [
  'SELECT Tintas, Produto, NomePQ, Ordem, ',
  'GramasTi, GramasKg, SUM(GramasTo) GramasTo ',
  'FROM ' + FEmitIts,
  'WHERE Codigo=' + Geral.FF0(QrPesaCabCodigo.Value),
  'AND Tintas=' + Geral.FF0(QrPesaCabTintasTin.Value),
  'GROUP BY Produto ',
  'ORDER BY Ordem ',
  '']);
end;

procedure TDmModEmit.ReadPixelsPerInch(Reader: TReader);
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

procedure TDmModEmit.ReopenEmit(DataBase: TmySQLDatabase; Tabela: String; Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Database, [
  'SELECT * ',
  'FROM ' + Tabela,//F_Emit
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmModEmit.ReopenEmitFlu(DataBase: TmySQLDatabase; Tabela, TabIts: String; Codigo: Integer);
begin
  FEmitIts := TabIts; // <- Gambiarra para reabrir EmitIts
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmitFlu, Database, [
  'SELECT * ',
  'FROM ' + Tabela,//F_EmitFlu
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmModEmit.ReopenEmitIts(DataBase: TmySQLDatabase; Tabela: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmitIts, Database, [
  'SELECT * ',
  'FROM ' + Tabela,//F_EmitIts
  'WHERE Codigo=' + Geral.FF0(QrEmitFluCodigo.Value),
  'AND OrdemFlu=' + Geral.FF0(QrEmitFluOrdemFlu.Value),
  '']);
end;

procedure TDmModEmit.ReopenPesaCab(DataBase: TmySQLDatabase; Tabela: String;
  Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesaCab, Database, [
  'SELECT TintasTin, NomeTin, Codigo, OrdemFlu, ',
  'SUM(GramasTo) GramasTo ',
  'FROM ' + LowerCase(Tabela),
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'GROUP BY TintasTin ',
  'ORDER BY OrdemFlu, TintasTin ',
  '']);
end;

procedure TDmModEmit.WritePixelsPerInch(Writer: TWriter);
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

end.
