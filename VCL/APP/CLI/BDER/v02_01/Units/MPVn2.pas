unit MPVn2;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, DBGrids, Menus, frxClass, frxDBSet, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnDmkProcFunc, dmkLabel, dmkImage,
  UnDmkEnums, dmkCheckBox, UnApp_Jan;

type
  TFmMPVn2 = class(TForm)
    PainelDados: TPanel;
    DsMPV: TDataSource;
    QrMPV: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdTransp: TdmkEditCB;
    CBTransp: TdmkDBLookupComboBox;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit01: TDBEdit;
    QrTransp: TmySQLQuery;
    DsTransp: TDataSource;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrTranspCodigo: TIntegerField;
    QrTranspNOMEENTIDADE: TWideStringField;
    TPDataF: TDateTimePicker;
    Label5: TLabel;
    QrMPVNOMECLIENTE: TWideStringField;
    QrMPVNOMETRANSP: TWideStringField;
    QrMPVCodigo: TIntegerField;
    QrMPVCliente: TIntegerField;
    QrMPVTransp: TIntegerField;
    QrMPVDataF: TDateField;
    QrMPVLk: TIntegerField;
    QrMPVDataCad: TDateField;
    QrMPVDataAlt: TDateField;
    QrMPVUserCad: TIntegerField;
    QrMPVUserAlt: TIntegerField;
    DBEdit02: TDBEdit;
    Label6: TLabel;
    PnManual: TPanel;
    Painel1: TPanel;
    QrMPs: TmySQLQuery;
    DsMPs: TDataSource;
    QrMPsCodigo: TIntegerField;
    QrMPsNome: TWideStringField;
    CkContinuar1: TCheckBox;
    DBGrid4: TDBGrid;
    QrMPVIts: TmySQLQuery;
    DsMPVIts: TDataSource;
    QrMPVItsNOMEMP: TWideStringField;
    QrMPVItsCodigo: TIntegerField;
    QrMPVItsControle: TIntegerField;
    QrMPVItsMP: TIntegerField;
    QrMPVItsQtde: TFloatField;
    QrMPVItsPreco: TFloatField;
    QrMPVItsValor: TFloatField;
    QrMPVItsLk: TIntegerField;
    QrMPVItsDataCad: TDateField;
    QrMPVItsDataAlt: TDateField;
    QrMPVItsUserCad: TIntegerField;
    QrMPVItsUserAlt: TIntegerField;
    QrMPVItsTexto: TWideStringField;
    QrTotais: TmySQLQuery;
    QrTotaisQtde: TFloatField;
    QrTotaisValor: TFloatField;
    QrMPVQtde: TFloatField;
    QrMPVValor: TFloatField;
    Label17: TLabel;
    DBEdit1: TDBEdit;
    Label18: TLabel;
    DBEdit2: TDBEdit;
    QrFornecedor: TmySQLQuery;
    QrFornecedorFAX_TXT: TWideStringField;
    QrFornecedorCEL_TXT: TWideStringField;
    QrFornecedorCPF_TXT: TWideStringField;
    QrFornecedorTEL_TXT: TWideStringField;
    QrFornecedorCEP_TXT: TWideStringField;
    QrFornecedorNOME: TWideStringField;
    QrFornecedorRUA: TWideStringField;
    QrFornecedorCOMPL: TWideStringField;
    QrFornecedorBAIRRO: TWideStringField;
    QrFornecedorTELEFONE: TWideStringField;
    QrFornecedorFAX: TWideStringField;
    QrFornecedorCelular: TWideStringField;
    QrFornecedorCNPJ: TWideStringField;
    QrFornecedorIE: TWideStringField;
    QrFornecedorContato: TWideStringField;
    QrFornecedorNOMEUF: TWideStringField;
    QrFornecedorEEMail: TWideStringField;
    QrFornecedorPEmail: TWideStringField;
    QrFornecedorNUMEROTXT: TWideStringField;
    QrFornecedorENDERECO: TWideStringField;
    QrFornecedorLOGRAD: TWideStringField;
    QrTransportador: TmySQLQuery;
    QrTransportadorTEL_TXT: TWideStringField;
    QrTransportadorCEL_TXT: TWideStringField;
    QrTransportadorCPF_TXT: TWideStringField;
    QrTransportadorFAX_TXT: TWideStringField;
    QrTransportadorCEP_TXT: TWideStringField;
    QrTransportadorNOME: TWideStringField;
    QrTransportadorRUA: TWideStringField;
    QrTransportadorCOMPL: TWideStringField;
    QrTransportadorBAIRRO: TWideStringField;
    QrTransportadorTELEFONE: TWideStringField;
    QrTransportadorFAX: TWideStringField;
    QrTransportadorCelular: TWideStringField;
    QrTransportadorCNPJ: TWideStringField;
    QrTransportadorIE: TWideStringField;
    QrTransportadorContato: TWideStringField;
    QrTransportadorNOMEUF: TWideStringField;
    QrTransportadorNUMEROTXT: TWideStringField;
    QrTransportadorENDERECO: TWideStringField;
    QrTransportadorEEMail: TWideStringField;
    QrTransportadorPEmail: TWideStringField;
    QrTransportadorLOGRAD: TWideStringField;
    Label19: TLabel;
    DBEdit03: TDBEdit;
    Label20: TLabel;
    DBEdit04: TDBEdit;
    DBEdit05: TDBEdit;
    Label21: TLabel;
    QrMPVVendedor: TIntegerField;
    QrMPVComissV_Per: TFloatField;
    QrMPVComissV_Val: TFloatField;
    QrMPVNOMEVENDEDOR: TWideStringField;
    Label22: TLabel;
    EdVendedor: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    Label23: TLabel;
    EdComissV_Per: TdmkEdit;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVendedores: TDataSource;
    DBGrid1: TDBGrid;
    QrSumC: TmySQLQuery;
    DsEmissC: TDataSource;
    QrEmissC: TmySQLQuery;
    QrEmissCData: TDateField;
    QrEmissCTipo: TSmallintField;
    QrEmissCCarteira: TIntegerField;
    QrEmissCAutorizacao: TIntegerField;
    QrEmissCGenero: TIntegerField;
    QrEmissCDescricao: TWideStringField;
    QrEmissCNotaFiscal: TIntegerField;
    QrEmissCDebito: TFloatField;
    QrEmissCCredito: TFloatField;
    QrEmissCCompensado: TDateField;
    QrEmissCDocumento: TFloatField;
    QrEmissCSit: TIntegerField;
    QrEmissCVencimento: TDateField;
    QrEmissCLk: TIntegerField;
    QrEmissCNOMESIT: TWideStringField;
    QrEmissCNOMETIPO: TWideStringField;
    QrEmissCNOMECARTEIRA: TWideStringField;
    QrEmissCFornecedor: TIntegerField;
    QrEmissCCliente: TIntegerField;
    QrEmissCSub: TSmallintField;
    QrEmissCFatID: TIntegerField;
    QrEmissCFatParcela: TIntegerField;
    QrEmissCID_Sub: TSmallintField;
    QrEmissCFatura: TWideStringField;
    QrEmissCBanco: TIntegerField;
    QrEmissCLocal: TIntegerField;
    QrEmissCCartao: TIntegerField;
    QrEmissCLinha: TIntegerField;
    QrEmissCOperCount: TIntegerField;
    QrEmissCLancto: TIntegerField;
    QrEmissCPago: TFloatField;
    QrEmissCMez: TIntegerField;
    QrEmissCMoraDia: TFloatField;
    QrEmissCMulta: TFloatField;
    QrEmissCProtesto: TDateField;
    QrEmissCDataCad: TDateField;
    QrEmissCDataAlt: TDateField;
    QrEmissCUserCad: TIntegerField;
    QrEmissCUserAlt: TIntegerField;
    QrEmissCDataDoc: TDateField;
    QrEmissCNivel: TIntegerField;
    QrEmissCVendedor: TIntegerField;
    QrEmissCAccount: TIntegerField;
    QrEmissCCliInt: TIntegerField;
    QrSumCCredito: TFloatField;
    Splitter1: TSplitter;
    QrEmissCCOMPENSA_TXT: TWideStringField;
    QrEmissCVENCTO_TXT: TWideStringField;
    QrEmissCVALOR_TXT: TWideStringField;
    QrMPVItsDesco: TFloatField;
    QrMPVItsSubTo: TFloatField;
    PMEdita: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    PMImpressao: TPopupMenu;
    Impressodireta1: TMenuItem;
    ImpressoNormal1: TMenuItem;
    QrSumIts: TmySQLQuery;
    QrSumItsQtde: TFloatField;
    QrSumItsDesco: TFloatField;
    QrSumItsValor: TFloatField;
    QrSumItsPRECO: TFloatField;
    LaDescoExtra: TLabel;
    EdDescoExtra: TdmkEdit;
    QrMPVDescoExtra: TFloatField;
    Label26: TLabel;
    DBEdit06: TDBEdit;
    DBEdit07: TDBEdit;
    Label27: TLabel;
    QrMPVTOTAL: TFloatField;
    Label28: TLabel;
    DBEdit08: TDBEdit;
    QrMPVVolumes: TIntegerField;
    Label29: TLabel;
    EdVolumes: TdmkEdit;
    PMExclui: TPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    DBMemo1: TDBMemo;
    QrMPVObs: TWideStringField;
    Memo1: TMemo;
    Label30: TLabel;
    PMMercadoria: TPopupMenu;
    Incluinovamercadoria1: TMenuItem;
    Alteradadosdamercadoriaatual1: TMenuItem;
    N1: TMenuItem;
    Excluimercadoriaatual1: TMenuItem;
    PMPagtos: TPopupMenu;
    Incluinovospagamentos1: TMenuItem;
    Alterapagamentoatual1: TMenuItem;
    N2: TMenuItem;
    Excluipagamentoatual1: TMenuItem;
    Excluitodospagamentos1: TMenuItem;
    QrEmissCQtde: TFloatField;
    QrEmissCFatID_Sub: TIntegerField;
    QrEmissCForneceI: TIntegerField;
    QrEmissCICMS_P: TFloatField;
    QrEmissCICMS_V: TFloatField;
    QrEmissCDuplicata: TWideStringField;
    QrEmissCDepto: TIntegerField;
    QrEmissCDescoPor: TIntegerField;
    QrEmissCEmitente: TWideStringField;
    QrEmissCContaCorrente: TWideStringField;
    QrEmissCCNPJCPF: TWideStringField;
    QrEmissCSEQ: TIntegerField;
    QrMPVItsEntrega: TDateField;
    QrMPVItsPronto: TDateField;
    QrMPVItsPRONTO_TXT: TWideStringField;
    QrMPVItsStatus: TIntegerField;
    QrMPVItsFluxo: TIntegerField;
    QrMPVItsClasse: TWideStringField;
    QrEmissCControle: TFloatField;
    QrEmissCDescoVal: TFloatField;
    QrEmissCDescoControle: TIntegerField;
    QrEmissCNFVal: TFloatField;
    QrEmissCUnidade: TIntegerField;
    QrEmissCAntigo: TWideStringField;
    PnPedido: TPanel;
    Panel2: TPanel;
    Panel7: TPanel;
    GBProducao: TGroupBox;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label44: TLabel;
    Panel1: TPanel;
    QrPed: TmySQLQuery;
    QrPedNOMECLI: TWideStringField;
    QrPedNOMEVEN: TWideStringField;
    QrPedPEDIDO: TIntegerField;
    QrPedCliente: TIntegerField;
    QrPedVendedor: TIntegerField;
    QrPedObz: TWideStringField;
    QrPedCodigo: TIntegerField;
    QrPedControle: TIntegerField;
    QrPedMP: TIntegerField;
    QrPedQtde: TFloatField;
    QrPedPreco: TFloatField;
    QrPedValor: TFloatField;
    QrPedTexto: TWideStringField;
    QrPedDesco: TFloatField;
    QrPedEntrega: TDateField;
    QrPedPronto: TDateField;
    QrPedStatus: TIntegerField;
    QrPedFluxo: TIntegerField;
    QrPedClasse: TWideStringField;
    QrPedEspesTxt: TWideStringField;
    QrPedCorTxt: TWideStringField;
    QrPedM2Pedido: TFloatField;
    QrPedPecas: TFloatField;
    QrPedUnidade: TIntegerField;
    QrPedObserv: TWideMemoField;
    QrPedAlterWeb: TSmallintField;
    QrPedPrecoPed: TFloatField;
    QrPedValorPed: TFloatField;
    DBEdit3: TDBEdit;
    DsPed: TDataSource;
    QrPedNOMEMP: TWideStringField;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label33: TLabel;
    DBEdit6: TDBEdit;
    Label36: TLabel;
    DBEdCliente: TDBEdit;
    Label43: TLabel;
    DBEdVendedor: TDBEdit;
    Label45: TLabel;
    DBEdit9: TDBEdit;
    EncerraOS1: TMenuItem;
    Panel8: TPanel;
    Label001: TLabel;
    EdControle: TdmkEdit;
    Panel9: TPanel;
    PnDadosFat: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label32: TLabel;
    EdQtde2: TdmkEdit;
    EdPreco2: TdmkEdit;
    EdSubTo2: TdmkEdit;
    EdDesco2: TdmkEdit;
    EdValor2: TdmkEdit;
    CkContinuar2: TCheckBox;
    DBMemo2: TDBMemo;
    Label46: TLabel;
    QrPesCli: TmySQLQuery;
    QrPesCliControle: TIntegerField;
    CBOS_Cli: TDBLookupComboBox;
    CBOS_All: TDBLookupComboBox;
    DsPesCli: TDataSource;
    QrPesAll: TmySQLQuery;
    DsPesAll: TDataSource;
    QrPesAllControle: TIntegerField;
    LaOS_Cli: TLabel;
    LaOS_All: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    LaControle: TLabel;
    GroupBox2: TGroupBox;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    EdMP: TdmkEditCB;
    CBMP: TdmkDBLookupComboBox;
    EdTexto: TdmkEdit;
    EdClasse: TdmkEdit;
    TPEntrega: TDateTimePicker;
    EdEspesTxt: TdmkEdit;
    EdCorTxt: TdmkEdit;
    EdQtde1: TdmkEdit;
    Label8: TLabel;
    Label11: TLabel;
    EdPreco1: TdmkEdit;
    EdSubTo1: TdmkEdit;
    Label24: TLabel;
    Label25: TLabel;
    EdDesco1: TdmkEdit;
    EdValor1: TdmkEdit;
    Label12: TLabel;
    QrMPVItsPedido: TIntegerField;
    QrMPVItsEspesTxt: TWideStringField;
    QrMPVItsCorTxt: TWideStringField;
    QrMPVItsM2Pedido: TFloatField;
    QrMPVItsPecas: TFloatField;
    QrMPVItsUnidade: TIntegerField;
    QrMPVItsObserv: TWideMemoField;
    QrMPVItsAlterWeb: TSmallintField;
    QrMPVItsPrecoPed: TFloatField;
    QrMPVItsValorPed: TFloatField;
    QrEmissCFatNum: TFloatField;
    frxVenda: TfrxReport;
    frxDsEmissC: TfrxDBDataset;
    frxDsMPVIts: TfrxDBDataset;
    frxDsMPV: TfrxDBDataset;
    frxDsSumIts: TfrxDBDataset;
    frxDsFornecedor: TfrxDBDataset;
    frxDsTransportador: TfrxDBDataset;
    Antiga1: TMenuItem;
    Nova1: TMenuItem;
    frxMovimento: TfrxReport;
    N3: TMenuItem;
    CorrigeFatID1: TMenuItem;
    QrEmissCAgencia: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel15: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel16: TPanel;
    PnSaiDesis: TPanel;
    GroupBox1: TGroupBox;
    Panel17: TPanel;
    Panel18: TPanel;
    BitBtn1: TBitBtn;
    BtDesiste2: TBitBtn;
    BtConfirma2: TBitBtn;
    BtDesiste1: TBitBtn;
    GBTrava: TGroupBox;
    Panel4: TPanel;
    Panel6: TPanel;
    BtTrava: TBitBtn;
    BtMercadoria: TBitBtn;
    BtPagamento: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel11: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtImprime: TBitBtn;
    BtExclui: TBitBtn;
    GroupBox3: TGroupBox;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel10: TPanel;
    BtDesiste0: TBitBtn;
    QrTransportadorNUMERO: TFloatField;
    QrTransportadorCEP: TFloatField;
    QrTransportadorUF: TFloatField;
    QrMPVEntInt: TIntegerField;
    QrMPVCodCliInt: TIntegerField;
    Panel12: TPanel;
    Label7: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrFornecedorNUMERO: TFloatField;
    QrFornecedorCEP: TFloatField;
    QrFornecedorUF: TFloatField;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    QrFornecedorCIDADE: TWideStringField;
    QrFornecedorPAIS: TWideStringField;
    QrTransportadorCIDADE: TWideStringField;
    QrTransportadorPAIS: TWideStringField;
    PCObs: TPageControl;
    TabSheet1: TTabSheet;
    TSFatParc: TTabSheet;
    LaFatParcEntrega: TLabel;
    LaFatParcArea: TLabel;
    CkFatParc: TdmkCheckBox;
    TPFatParcEntrega: TDateTimePicker;
    EdFatParcArea: TdmkEdit;
    DBMemo3: TDBMemo;
    Label49: TLabel;
    Label48: TLabel;
    Label14: TLabel;
    Desfazfaturamentodemercadoriaatual1: TMenuItem;
    QrPedDescricao: TWideMemoField;
    QrPedComplementacao: TWideMemoField;
    QrPedTipoProd: TSmallintField;
    QrPedCustoPQ: TFloatField;
    QrPedGraGruX: TIntegerField;
    QrPedReceiRecu: TIntegerField;
    QrPedReceiRefu: TIntegerField;
    QrPedReceiAcab: TIntegerField;
    QrPedTxtMPs: TWideStringField;
    QrPedCustoWB: TFloatField;
    QrPedDtaCrust: TDateField;
    QrPedVSArtGGX: TIntegerField;
    QrPedVSArtCab: TIntegerField;
    QrPedFluxPcpCab: TIntegerField;
    QrPedTransp: TIntegerField;
    QrPedCondicaoPg: TIntegerField;
    QrPedPedidCli: TWideStringField;
    QrPedDataF: TDateField;
    PnCabecOS: TPanel;
    Label31: TLabel;
    EdComissV_Per2: TdmkEdit;
    Label47: TLabel;
    EdVolumes2: TdmkEdit;
    PnObs2: TPanel;
    Label53: TLabel;
    MeObs2: TMemo;
    QrPediPrzCab: TMySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    DsPediPrzCab: TDataSource;
    LaCondicaoPG: TLabel;
    EdCondicaoPG: TdmkEditCB;
    CBCondicaoPG: TdmkDBLookupComboBox;
    QrMPVCondicaoPg: TIntegerField;
    QrMPVNO_CondicaoPG: TWideStringField;
    Label57: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    QrMPVItsPedidCli: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesiste0Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure QrMPVAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMPVBeforeOpen(DataSet: TDataSet);
    procedure BtDesiste1Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure BtTravaClick(Sender: TObject);
    procedure BtMercadoriaClick(Sender: TObject);
    procedure EdQtde1Change(Sender: TObject);
    procedure EdPreco1Change(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrMPVItsCalcFields(DataSet: TDataSet);
    procedure BtPagamentoClick(Sender: TObject);
    procedure QrEmissCCalcFields(DataSet: TDataSet);
    procedure EdPreco1Exit(Sender: TObject);
    procedure EdQtde1Exit(Sender: TObject);
    procedure EdDesco1Exit(Sender: TObject);
    procedure frMovimentoUserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    procedure QrFornecedorCalcFields(DataSet: TDataSet);
    procedure QrTransportadorCalcFields(DataSet: TDataSet);
    procedure Impressodireta1Click(Sender: TObject);
    procedure EdDescoExtraExit(Sender: TObject);
    procedure QrMPVCalcFields(DataSet: TDataSet);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure Incluinovamercadoria1Click(Sender: TObject);
    procedure Alteradadosdamercadoriaatual1Click(Sender: TObject);
    procedure Excluimercadoriaatual1Click(Sender: TObject);
    procedure Incluinovospagamentos1Click(Sender: TObject);
    procedure Alterapagamentoatual1Click(Sender: TObject);
    procedure Excluipagamentoatual1Click(Sender: TObject);
    procedure Excluitodospagamentos1Click(Sender: TObject);
    procedure PMMercadoriaPopup(Sender: TObject);
    procedure PMPagtosPopup(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdControleChange(Sender: TObject);
    procedure EncerraOS1Click(Sender: TObject);
    procedure QrPedCalcFields(DataSet: TDataSet);
    procedure BtDesiste2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrPedAfterOpen(DataSet: TDataSet);
    procedure EdControleKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBOS_CliKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrPesCliAfterScroll(DataSet: TDataSet);
    procedure EdQtde2Change(Sender: TObject);
    procedure EdQtde2Exit(Sender: TObject);
    procedure EdDesco2Exit(Sender: TObject);
    procedure EdPreco2Change(Sender: TObject);
    procedure EdPreco2Exit(Sender: TObject);
    procedure CBOS_AllKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrPesAllAfterScroll(DataSet: TDataSet);
    procedure QrPedAfterClose(DataSet: TDataSet);
    procedure Antiga1Click(Sender: TObject);
    procedure Nova1Click(Sender: TObject);
    function frxVendaUserFunction(const MethodName: String;
      var Params: Variant): Variant;
    procedure frxVendaGetValue(const VarName: String; var Value: Variant);
    procedure QrMPVBeforeClose(DataSet: TDataSet);
    procedure CorrigeFatID1Click(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure CkFatParcClick(Sender: TObject);
    procedure Desfazfaturamentodemercadoriaatual1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
  private
    FFat1, FFat2: Double;
    FQtde, FDesco, FCompl, FTotal: Double;
    FEditItem, FMostraMoney: Boolean;
    FTabLctA: String;
    //
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure CalculaTotal1;
    procedure CalculaTotal2;
    procedure ImpressaoOculta;
    procedure ImpressaoNormal(ProdutosM, ProdutosMIts, Pagtos: TmySQLQuery);
    procedure ImpressaoDireta2(ProdutosM, ProdutosMIts, Pagtos: TmySQLQuery;
              Cria, Mostra: Boolean);
    procedure DefineVarDup;
    function DiferencasEmPagtos: Double;
    procedure OcultaPnPedido;
    procedure ReopenQrPed(OS_Controle: Integer);
    procedure MostraOS_Cli;
    procedure MostraOS_All;
    procedure ReopenSumC();
    procedure ReopenFornecedor(Qry: TmySQLQuery);
    procedure ReopenTransportador(Qry: TmySQLQuery);
    procedure ReopenSumIts(Qry: TmySQLQuery);
    procedure ConfiguraFatParc(Parcial: Boolean);
    procedure ConfiguraValCamposFatParc(Qtde: Double);
    function  InsereFatParc(OriControle: Integer): Boolean;
    //2023-09-25
    function  NovoCodigo(): Integer;
    function  InsereMPV(Codigo, Cliente, Transp, Vendedor: Integer; DataF:
              String; (*Qtde, Valor,*) ComissV_Per, (*ComissV_Val,*) DescoExtra:
              Double; Volumes: Integer; Obs (*Obz,*): String; EntInt, CondicaoPg:
              Integer): Boolean;
  public
    { Public declarations }
    FMPVLoc: Integer;
    //FDireto: Boolean;
    FPrimeiroItemInserido: Boolean;
    FAcaoInicial: TMPVAcao;
    procedure ReopenMPVIts;
    procedure ReopenEmissC(FatParcela: Integer);
    procedure AtualizaTotais;
    procedure ExcluiMercadoria;
    procedure ExcluiPagto;
    procedure ExcluiTodosPagtos;
    procedure MostraInclusao();
  end;

var
  FmMPVn2: TFmMPVn2;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DotPrint, UnInternalConsts3, UnPagtos, MPVLoc,
MyDBCheck, UnFinanceiro, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMPVn2.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMPVn2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMPVCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMPVn2.DefParams;
begin
  VAR_GOTOTABELA := 'MPV';
  VAR_GOTOMYSQLTABLE := QrMPV;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;
  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT');
  VAR_SQLx.Add('CASE WHEN ve.Tipo=0 THEN ve.RazaoSocial');
  VAR_SQLx.Add('ELSE ve.Nome END NOMEVENDEDOR,');
  VAR_SQLx.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  VAR_SQLx.Add('ELSE cl.Nome END NOMECLIENTE,');
  VAR_SQLx.Add('CASE WHEN tr.Tipo=0 THEN tr.RazaoSocial');
  VAR_SQLx.Add('ELSE tr.Nome END NOMETRANSP,');
  VAR_SQLx.Add('mpv.*, ei.CodCliInt, pc.Nome NO_CONDICAOPG');
  VAR_SQLx.Add('FROM mpv mpv');
  VAR_SQLx.Add('LEFT JOIN enticliint ei ON ei.CodEnti=mpv.EntInt');
  VAR_SQLx.Add('LEFT JOIN entidades cl ON cl.Codigo=mpv.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades tr ON tr.Codigo=mpv.Transp');
  VAR_SQLx.Add('LEFT JOIN entidades ve ON ve.Codigo=mpv.Vendedor');
  VAR_SQLx.Add('LEFT JOIN pediprzcab pc ON pc.Codigo=mpv.CondicaoPg');
  VAR_SQLx.Add('WHERE mpv.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND mpv.Codigo=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmMPVn2.Desfazfaturamentodemercadoriaatual1Click(Sender: TObject);
begin
  if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE mpvits SET ',
    'Codigo = 0',
    'WHERE Controle=' + Geral.FF0(QrMPVItsControle.Value),
    '']) then
  begin
    AtualizaTotais;
    LocCod(QrMPVCodigo.Value, QrMPVCodigo.Value);
    ReopenMPVIts;
  end;
end;

procedure TFmMPVn2.MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    GBCntrl.Visible:=False;
    if SQLType = stIns then
    begin
      EdCodigo.Text       := FormatFloat(FFormatFloat, Codigo);
      EdCliente.Text      := '';
      CBCliente.KeyValue  := NULL;
      EdTransp.Text       := '';
      CBTransp.KeyValue   := NULL;
      //TPDataF.Date        := Date;
      EdVendedor.Text     := '';
      CBVendedor.KeyValue := NULL;
      EdComissV_Per.Text  := '';
      LaDescoExtra.Enabled := False;
      EdDescoExtra.Enabled := False;
      EdDescoExtra.Text := '0,00';
      EdVolumes.Text := '0';
      Memo1.Lines.Clear;
    end else begin
      EdCodigo.Text      := DBEdCodigo.Text;
      EdCliente.Text     := IntToStr(QrMPVCliente.Value);
      CBCliente.KeyValue := QrMPVCliente.Value;
      EdTransp.Text      := IntToStr(QrMPVTransp.Value);
      CBTransp.KeyValue  := QrMPVTransp.Value;
      TPDataF.Date       := QrMPVDataF.Value;
      //EdNome.Text := DBEdNome.Text;
      EdVendedor.Text      := IntToStr(QrMPVVendedor.Value);
      CBVendedor.KeyValue  := QrMPVVendedor.Value;
      EdComissV_Per.Text   := Geral.FFT(QrMPVComissV_Per.Value, 4, siPositivo);
      LaDescoExtra.Enabled := True;
      EdDescoExtra.Enabled := True;
      EdDescoExtra.Text    := Geral.FFT(QrMPVDescoExtra.Value, 2, siPositivo);
      EdVolumes.Text    := Geral.FFT(QrMPVVolumes.Value, 0, siPositivo);
      EdCondicaoPg.ValueVariant := QrMPVCondicaoPg.Value;
      CBCondicaoPg.KeyValue     := QrMPVCondicaoPg.Value;
      Memo1.Text := QrMPVObs.Value;
    end;
    if EdCliente.Visible then
      EdCliente.SetFocus;
  end else begin
    GBCntrl.Visible     := True;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmMPVn2.MostraInclusao();
begin
  //if FDireto then
  case FAcaoInicial of
    TMPVAcao.mpvaNenhuma: ; // nada
    TMPVAcao.mpvaCabecalho: IncluiRegistro();
    TMPVAcao.mpvaEncerraOS:
    begin
      ImgTipo.SQLType := stIns;
      GBTrava.Visible := True;
      GBCntrl.Visible := False;
      //
      EncerraOS1Click(Self);
    end;
  end;
end;

procedure TFmMPVn2.CriaOForm;
begin
  DefParams;
  Va(vpLast);
end;

procedure TFmMPVn2.AlteraRegistro;
var
  MPV : Integer;
begin
  MPV := QrMPVCodigo.Value;
  if QrMPVCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(MPV, Dmod.MyDB, 'MPV', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(MPV, Dmod.MyDB, 'MPV', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmMPVn2.IncluiRegistro;
var
  Cursor : TCursor;
  MPV : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MPV := NovoCodigo();
    if Length(FormatFloat(FFormatFloat, MPV))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro('Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, stIns, MPV);
  finally
    Screen.Cursor := Cursor;
  end;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMPVn2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMPVn2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMPVn2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMPVn2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMPVn2.BtIncluiClick(Sender: TObject);
begin
  if FAcaoInicial = TMPVAcao.mpvaEncerraOS then
    FPrimeiroItemInserido := False;
  MostraInclusao();
  AtualizaTotais;
end;

procedure TFmMPVn2.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
  AtualizaTotais;
end;

procedure TFmMPVn2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrMPVCodigo.Value;
  Close;
end;

procedure TFmMPVn2.BtConfirmaClick(Sender: TObject);
var
  Codigo, Cliente, Vendedor, Transp, Empresa, EntInt, Volumes, CondicaoPg: Integer;
  ComissV_Per, DescoExtra: Double;
  DataF, Obs: String;
begin
  Empresa     := EdEmpresa.ValueVariant;
  EntInt      := DmodG.QrEmpresasCodigo.Value;
  Cliente     := EdCliente.ValueVariant;
  Transp      := EdTransp.ValueVariant;
  Vendedor    := EdVendedor.ValueVariant;
  ComissV_Per := EdComissV_Per.ValueVariant;
  DescoExtra  := EdDescoExtra.ValueVariant;
  DataF       := FormatDateTime(VAR_FORMATDATE, TPDataF.Date);
  Volumes     := EdVolumes.ValueVariant;
  Obs         := Memo1.Text;
  CondicaoPg  := EdCondicaoPg.ValueVariant;
  //
  if MyObjects.FIC(Cliente=0, EdCliente, 'Defina o cliente!') then
    Exit;
  if MyObjects.FIC(Empresa=0, EdEmpresa, 'Defina a empresa!') then
    Exit;
  if MyObjects.FIC((ComissV_Per > 0.009) and (Vendedor = 0), EdVendedor,
    'Defina um Vendedor para a comiss�o a pagar!')
  then
    Exit;
(*
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO mpv SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE mpv SET ');
  Dmod.QrUpdU.SQL.Add('Cliente=:P0, Transp=:P1, DataF=:P2, ');
  Dmod.QrUpdU.SQL.Add('Vendedor=:P3, ComissV_Per=:P4, DescoExtra=:P5, ');
  Dmod.QrUpdU.SQL.Add('Volumes=:P6, Obs=:P7, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsInteger := Cliente;
  Dmod.QrUpdU.Params[01].AsInteger := Transp;
  Dmod.QrUpdU.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataF.Date);
  Dmod.QrUpdU.Params[03].AsInteger := Vended;
  Dmod.QrUpdU.Params[04].AsFloat   := Comiss;
  Dmod.QrUpdU.Params[05].AsFloat   := DescoExtra;
  Dmod.QrUpdU.Params[06].AsInteger := Geral.IMV(EdVolumes.Text);
  Dmod.QrUpdU.Params[07].AsString  := Memo1.Text;
  //
  Dmod.QrUpdU.Params[08].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[09].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[10].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
*)
  //
  Codigo := EdCodigo.ValueVariant;
  //
  if InsereMPV(Codigo, Cliente, Transp, Vendedor, DataF, (*Qtde, Valor,*)
  ComissV_Per, (*ComissV_Val,*) DescoExtra, Volumes, Obs (*Obz,*), EntInt,
  CondicaoPg) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'MPV', 'Codigo');
    MostraEdicao(False, ImgTipo.SQLType, 0);
    LocCod(Codigo, Codigo);
    //
    GBTrava.Visible := True;
    GBCntrl.Visible := False;
    //
    //if FDireto then
    if FAcaoInicial = TMPVAcao.mpvaCabecalho then
    begin
      if ImgTipo.SQLType = stIns then
        //Incluinovamercadoria1Click(Self) else
        EncerraOS1Click(Self) else
      begin
        (*
        BtTravaClick(Self);
        //
        if Geral.MB_Pergunta('Deseja imprimir o pedido?')= ID_YES then
          ImpressaoOculta;
        BtSaidaClick(Self);
        *)
      end;
    end;
  end;
end;

procedure TFmMPVn2.BtDesiste0Click(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  //
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'MPV', Codigo);
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'MPV', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'MPV', 'Codigo');
  //
  //if FDireto then
  if FAcaoInicial = TMPVAcao.mpvaCabecalho then
  begin
    //Geral.MB_Aviso('A inclus�o direta foi desativada para '+
    //'permitir altera��es!');
    //FDireto := False;
  end;
end;

procedure TFmMPVn2.FormCreate(Sender: TObject);
const
  Aplicacao = '1';
begin
  ImgTipo.SQLType := stLok;
  //
  FPrimeiroItemInserido := False;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa, DModG.QrEmpresas);
  //FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrMPVCodCliInt.Value);
  //
  DBGrid4.Height := Geral.ReadAppKey('Componentes\MPV\AlturaGrade',
  Application.Title, ktInteger, 73, HKEY_LOCAL_MACHINE);
  if DBGrid4.Height < 40 then DBGrid4.Height := 40;
  while DBGrid4.Height < 40 do DBGrid4.Height := DBGrid4.Height - 20;

  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  DBGrid1.Align     := alClient;
  Panel2.Align      := alClient;
  PCObs.Align       := alClient;
  CriaOForm;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransp, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMPs, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVendedores, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzCab, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome, MaxDesco, ',
  'JurosMes, Parcelas, Percent1, Percent2 ',
  'FROM pediprzcab ',
  'WHERE Aplicacao & ' + Aplicacao + ' <> 0 ',
  'ORDER BY Nome ',
  '']);
  //
  //
  TPDataF.Date := Date;
  TPEntrega.Date := Date;
  CBOS_Cli.Left := EdControle.Left;
  CBOS_All.Left := EdControle.Left;
  CBOS_Cli.Top := EdControle.Top;
  CBOS_All.Top := EdControle.Top;
  // 2013-07-16
  CkContinuar2.Checked := True;
  // FIM 2013-07-16
end;

procedure TFmMPVn2.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMPVCodigo.Value,LaRegistro.Caption);
end;

procedure TFmMPVn2.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMPVn2.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmMPVn2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Geral.WriteAppKey('Componentes\MPV\AlturaGrade', Application.Title,
  DBGrid4.Height, ktInteger, HKEY_LOCAL_MACHINE);
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmMPVn2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'MPV', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmMPVn2.QrMPVAfterScroll(DataSet: TDataSet);
begin
  FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrMPVCodCliInt.Value);
  BtAltera.Enabled := GOTOy.BtEnabled(QrMPVCodigo.Value, False);
  ReopenMPVIts;
  ReopenEmissC(0);
end;

procedure TFmMPVn2.SbQueryClick(Sender: TObject);
var
  MPV, MPVIts: Integer;
begin
  FMPVLoc := 0;
  if DBCheck.CriaFm(TFmMPVLoc, FmMPVLoc, afmoNegarComAviso) then
  begin
    FmMPVLoc.EdCliente.Text := Geral.ReadAppKey('Pesquisa\MPVLoc\Cliente',
      Application.Title, ktString, '', HKEY_LOCAL_MACHINE);
    FmMPVLoc.CBCliente.KeyValue := Geral.IMV(FmMPVLoc.EdCliente.Text);
    //
    if FmMPVLoc.QrMPV.State = dsBrowse then
    begin
      MPV := Geral.ReadAppKey('Pesquisa\MPVLoc\MPV', Application.Title,
        ktInteger, 0, HKEY_LOCAL_MACHINE);
      MPVIts := Geral.ReadAppKey('Pesquisa\MPVLoc\MPVIts', Application.Title,
        ktInteger, 0, HKEY_LOCAL_MACHINE);
      FmMPVLoc.QrMPV.Locate('Codigo', MPV, []);
      if FmMPVLoc.QrMPVIts.State = dsBrowse then
        FmMPVLoc.QrMPVIts.Locate('Controle', MPVIts, []);
    end;
    FmMPVLoc.ShowModal;
    FMPVLoc := FmMPVLoc.FSelCod;
    FmMPVLoc.Destroy;
  end;
  if FMPVLoc > 0 then LocCod(FMPVLoc, FMPVLoc);
end;

procedure TFmMPVn2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPVn2.FormShow(Sender: TObject);
begin
  MostraInclusao();
end;

procedure TFmMPVn2.QrMPVBeforeClose(DataSet: TDataSet);
begin
  QrMPVIts.Close;
  QrEmissC.Close;
end;

procedure TFmMPVn2.QrMPVBeforeOpen(DataSet: TDataSet);
begin
  QrMPVCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmMPVn2.BtDesiste1Click(Sender: TObject);
begin
  PainelDados.Visible := True;
  PnManual.Visible := False;
  AtualizaTotais;
  LocCod(QrMPVCodigo.Value, QrMPVCodigo.Value);
  //
  //if FDireto then
  if FAcaoInicial = TMPVAcao.mpvaCabecalho then
  begin
    //Incluinovospagamentos1Click(Self);
    FmostraMoney := False;
  end;
end;

procedure TFmMPVn2.BtConfirma2Click(Sender: TObject);
var
  MP, Controle: Integer;
  Qtde, Preco, Valor, Desco: Double;
begin
  MP := Geral.IMV(EdMP.Text);
  if MP = 0 then
  begin
    Geral.MB_Aviso('Defina uma mercadoria.');
    if EdMP.Visible then
      EdMP.SetFocus;
    Exit;
  end;
  Qtde  := Geral.DMV(EdQtde1.Text);
  Preco := Geral.DMV(EdPreco1.Text);
  Valor := Geral.DMV(EdValor1.Text);
  Desco := Geral.DMV(EdDesco1.Text);
  //
  Dmod.QrUpd.SQL.Clear;
  if not FEditItem then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO mpvits SET ');
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'MPVIts', 'MPVIts', 'Codigo');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE mpvits SET ');
    Controle := QrMPVItsControle.Value;
  end;
  Dmod.QrUpd.SQL.Add('MP=:P0, Qtde=:P1, Preco=:P2, Valor=:P3, Texto=:P4,');
  Dmod.QrUpd.SQL.Add('Desco=:P5, Entrega=:P6, Classe=:P7, EspesTxt=:P8, ');
  Dmod.QrUpd.SQL.Add('CorTxt=:P9 ');
  if not FEditItem then
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb')
  else
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa AND Controle=:Pb');
  //
  Dmod.QrUpd.Params[00].AsInteger := MP;
  Dmod.QrUpd.Params[01].AsFloat   := Qtde;
  Dmod.QrUpd.Params[02].AsFloat   := Preco;
  Dmod.QrUpd.Params[03].AsFloat   := Valor;
  Dmod.QrUpd.Params[04].AsString  := EdTexto.Text;
  Dmod.QrUpd.Params[05].AsFloat   := Desco;
  Dmod.QrUpd.Params[06].AsString  := Geral.FDT(TPEntrega.Date, 1);
  Dmod.QrUpd.Params[07].AsString  := EdClasse.Text;
  Dmod.QrUpd.Params[08].AsString  := EdEspesTxt.Text;
  Dmod.QrUpd.Params[09].AsString  := EdCorTxt.Text;
  //
  Dmod.QrUpd.Params[10].AsInteger := QrMPVCodigo.Value;
  Dmod.QrUpd.Params[11].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  if CkContinuar1.Checked then
  begin
    EdMP.Text     := '';
    CBMP.KeyValue := NULL;
    EdTexto.Text  := '';
    EdQtde1.Text   := '';
    EdPreco1.Text  := '';
    if EdMP.Visible then
      EdMP.SetFocus;
  end else BtDesiste1click(Self);
end;

procedure TFmMPVn2.BtTravaClick(Sender: TObject);
begin
  GBCntrl.Visible := True;
  GBTrava.Visible := False;
  ImgTipo.SQLType := stLok;
  // Trava
  AtualizaTotais;
  LocCod(QrMPVCodigo.Value, QrMPVCodigo.Value);
  GOTOy.BotoesSb(stLok);
  //if FDireto then BtAlteraClick(Self);
end;

procedure TFmMPVn2.BtMercadoriaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMercadoria, BtMercadoria);
end;

procedure TFmMPVn2.EdQtde1Change(Sender: TObject);
begin
  if Geral.DMV(EdQtde1.Text) <= 0 then
  begin
    EdDesco1.Enabled := False;
    EdDesco1.Text := '0,00';
  end else EdDesco1.Enabled := True;
  CalculaTotal1;
end;

procedure TFmMPVn2.EdPreco1Change(Sender: TObject);
begin
  CalculaTotal1;
end;

procedure TFmMPVn2.CalculaTotal1;
var
  Preco, Qtde, Desco, SubTo, Valor: Double;
begin
  Preco := Geral.DMV(EdPreco1.Text);
  Qtde  := Geral.DMV(EdQtde1.Text);
  Desco := Geral.DMV(EdDesco1.Text);
  //
  SubTo := Preco * Qtde;
  if Desco > SubTo then
  begin
    Desco := 0;
    EdDesco1.Text := '0,00';
  end;
  Valor := SubTo - Desco;
  EdSubTo1.Text := Geral.FFT(SubTo, 2, siNegativo);
  EdValor1.Text := Geral.FFT(Valor, 2, siNegativo);
end;

procedure TFmMPVn2.CalculaTotal2;
var
  Preco, Qtde, Desco, SubTo, Valor: Double;
begin
  Preco := Geral.DMV(EdPreco2.Text);
  Qtde  := Geral.DMV(EdQtde2.Text);
  Desco := Geral.DMV(EdDesco2.Text);
  //
  SubTo := Preco * Qtde;
  if Desco > SubTo then
  begin
    Desco := 0;
    EdDesco2.Text := '0,00';
  end;
  Valor := SubTo - Desco;
  EdSubTo2.Text := Geral.FFT(SubTo, 2, siNegativo);
  EdValor2.Text := Geral.FFT(Valor, 2, siNegativo);
  //
  ConfiguraValCamposFatParc(Qtde);
end;

procedure TFmMPVn2.ConfiguraValCamposFatParc(Qtde: Double);
begin
  if (Qtde <> 0) and (Qtde < QrPedM2Pedido.Value) then
  begin
    TSFatParc.TabVisible  := True;
    PCObs.ActivePageIndex := 1;
    //
    ConfiguraFatParc(True);
    //
    CkFatParc.Checked          := True;
    TPFatParcEntrega.Date      := QrPedEntrega.Value;
    EdFatParcArea.ValueVariant := QrPedM2Pedido.Value - Qtde;
  end else
  begin
    TSFatParc.TabVisible  := False;
    PCObs.ActivePageIndex := 0;
  end;
end;

procedure TFmMPVn2.ReopenMPVIts;
begin
  QrMPVIts.Close;
  QrMPVIts.Params[0].AsInteger := QrMPVCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrMPVIts, Dmod.MyDB);
end;

procedure TFmMPVn2.ReopenEmissC(FatParcela: Integer);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissC, Dmod.MyDB, [
    'SELECT la.Controle+0.000 Controle, la.*, ca.Nome NOMECARTEIRA ',
    'FROM ' + FTabLctA + ' la, Carteiras ca ',
    'WHERE ca.Codigo=la.Carteira ',
    'AND la.FatID=' + Geral.FF0(VAR_FATID_1013),
    'AND la.FatNum=' + Geral.FF0(QrMPVCodigo.Value),
    'AND la.ID_Pgto = 0 ',
    'ORDER BY Vencimento ',
    '']);
  //
  (*
  QrEmissC.Close;
  QrEmissC.Params[00].AsInteger := VAR_FATID_1013;
  QrEmissC.Params[01].AsInteger := QrMPVCodigo.Value;
  QrEmissC.O p e n;
  *)
  //
  QrEmissC.Locate('FatParcela', FatParcela, []);
end;

procedure TFmMPVn2.ReopenFornecedor(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFornecedor, Dmod.MyDB, [
  'SELECT  ',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOME, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ERua ',
  'ELSE fo.PRua END RUA, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ENumero ',
  'ELSE fo.PNumero END + 0.000 NUMERO, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ECompl ',
  'ELSE fo.PCompl END COMPL, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EBairro ',
  'ELSE fo.PBairro END BAIRRO, ',
  'mu.Nome CIDADE, ',
  'pa.Nome PAIS, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ETe1 ',
  'ELSE fo.PTe1 END TELEFONE, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EFax ',
  'ELSE fo.PPais END FAX, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ECel ',
  'ELSE fo.PCel END Celular, ',
  'CASE WHEN fo.Tipo=0 THEN fo.CNPJ ',
  'ELSE fo.CPF END CNPJ, ',
  'CASE WHEN fo.Tipo=0 THEN fo.IE ',
  'ELSE fo.RG END IE, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ECEP ',
  'ELSE fo.PCEP END + 0.000 CEP, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EContato ',
  'ELSE fo.PContato END Contato, ',
  ' ',
  'CASE WHEN fo.Tipo=0 THEN fo.EUF ',
  'ELSE fo.PUF END + 0.000 UF, ',
  'CASE WHEN fo.Tipo=0 THEN uf1.Nome ',
  'ELSE uf2.Nome END NOMEUF, ',
  'EEMail, PEmail, ',
  ' ',
  'CASE WHEN fo.Tipo=0 THEN ll1.Nome ',
  'ELSE ll2.Nome END LOGRAD ',
  ' ',
  'FROM entidades fo ',
  'LEFT JOIN ufs uf1 ON uf1.Codigo=fo.Euf ',
  'LEFT JOIN ufs uf2 ON uf2.Codigo=fo.Puf ',
  'LEFT JOIN listalograd ll1 ON ll1.Codigo=fo.ELograd ',
  'LEFT JOIN listalograd ll2 ON ll2.Codigo=fo.PLograd ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo = IF(fo.Tipo=0, fo.ECodMunici, fo.PCodMunici) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pa ON pa.Codigo = IF(fo.Tipo=0, fo.ECodiPais, fo.PCodiPais) ',
  'WHERE fo.Codigo=' + Geral.FF0(Qry.FieldByName('Cliente').AsInteger),
  '']);
  (*
  QrFornecedor.Close;
  QrFornecedor.Params[0].AsInteger := ProdutosM.FieldByName('Cliente').AsInteger;
  QrFornecedor. O p e n ;
  *)
end;

procedure TFmMPVn2.AtualizaTotais;
var
  ComissVal: Double;
begin
  QrTotais.Close;
  QrTotais.Params[0].AsInteger := QrMPVCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrTotais, Dmod.MyDB);
  ComissVal := QrTotaisValor.Value * QrMPVComissV_Per.Value / 100;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE mpv SET Qtde=:P0, Valor=:P1, ComissV_Val=:P2');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P3');
  Dmod.QrUpd.Params[0].AsFloat   := QrTotaisQtde.Value;
  Dmod.QrUpd.Params[1].AsFloat   := QrTotaisValor.Value;
  Dmod.QrUpd.Params[2].AsFloat   := ComissVal;
  Dmod.QrUpd.Params[3].AsInteger := QrMPVCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmMPVn2.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImpressao, SbImprime);
end;

procedure TFmMPVn2.ImpressaoNormal(ProdutosM, ProdutosMIts, Pagtos: TmySQLQuery);
begin
  ReopenTransportador(ProdutosM);
  ReopenFornecedor(ProdutosM);
  ReopenSumIts(ProdutosM);
  //
  MyObjects.frxMostra(frxMovimento, 'Movimento - Impress�o Normal');
end;

procedure TFmMPVn2.ImgTipoChange(Sender: TObject);
begin
  GB_L.Enabled := ImgTipo.SQLType = stLok;
end;

procedure TFmMPVn2.ImpressaoDireta2(ProdutosM, ProdutosMIts, Pagtos: TmySQLQuery;
Cria, Mostra: Boolean);
const
  Titulo = 'ORCAMENTO N. ';
  TA1 = 11;
  TA2 = 55;
  TA3 = 11;
  TA4 = 55;
  TB1 = 11;
  TB2 = 22;
  TB3 = 11;
  TB4 = 22;
  TB5 = 11;
  TB6 = 22;
  TB7 = 11;
  TB8 = 22;
var
  //ITENS: Integer;// = 37;
  i: Integer;
  Divisor, Data: String;
  Textos: TMyTextos_136;
  Alinha: TMyAlinha_136;
  TamCol: TMyTamCol_136;
  //ColsImpCompress: Integer;
  DesTo, DesPo, Qtde, Desco, Compl, Total, Bruto: Double;
begin
  {ITENS := 0;
  case Dmod.QrControleOrcaLinhas.Value of
    0: ITENS := 42;
    1: ITENS := 11;
  end;
  if Dmod.QrControleOrcaRodape.Value = 1 then ITENS := ITENS + 4;
  if Dmod.QrControleOrcaCabecalho.Value = 2 then ITENS := ITENS - 2;}
  ////////////////////////////////////////////////////////////////////////////////
  VAR_LOCCOLUNASIMP := 132;
  ////////////////////////////////////////////////////////////////////////////////
  Data := FormatDateTime('hh:nn:ss "  "dd/mm/yyyy', Now());
  //ColsImpCompress := Trunc(VAR_LOCCOLUNASIMP * 17 / 10);
  Divisor := '';
  for i := 1 to VAR_LOCCOLUNASIMP do Divisor := Divisor + '=';
  //
  ReopenTransportador(ProdutosM);
  ReopenFornecedor(ProdutosM);
  //
  if Cria then Application.CreateForm(TFmDotPrint, FmDotPrint);
  with FmDotPrint do
  begin
    FTipoLinhas := True;
    FQtdeLinhas := Dmod.QrControleOrcaLinhas.Value;
    with RE1 do
    begin
      if Cria then
      begin
        Lines.Clear;
        AdicionaLinhaTexto('', [fdpCompress], []);
        AdicionaLinhaTexto(Geral.CompletaString(
          'PEDIDO N� '+IntToStr(QrMPVCodigo.Value)
          , ' ', 132, taRightJustify, True), [fdpCompress], []);
        AdicionaLinhaTexto(Geral.CompletaString(Data, ' ', 132, taRightJustify,
          True), [fdpCompress], []);
        if Dmod.QrControleOrcaCabecalho.Value in [1,2] then
        begin
          AdicionaLinhaTexto(DModG.QrDonoNOMEDONO.Value+ ' Fone: '+
            DModG.QrDonoFONES.Value, [fdpCompress], []);
            //
          AdicionaLinhaTexto(DModG.QrDonoE_CUC.Value, [fdpCompress], []);
        end;
        if Dmod.QrControleOrcaCabecalho.Value in [0,2] then
          AdicionaLinhaTexto(Titulo+FormatFloat('000000',
          ProdutosM.FieldByName('Codigo').AsInteger)+'    Vendedor: '+
          ProdutosM.FieldByName('NOMEVENDEDOR').AsString+
          '  VOLUMES: '+IntToStr(QrMPVVolumes.Value),
          [fdpCompress], []);
      end;
      AdicionaLinhaTexto(Divisor, [fdpCompress], []);
      AdicionaLinhaTexto('Transportadora: '+ QrTransportadorNOME.Value +
      '  Tel: '+Geral.FormataTelefone_TT(QrTransportadorTelefone.Value)+
      '  Fax: '+Geral.FormataTelefone_TT(QrTransportadorFax.Value),
      [fdpCompress], []);
      if FEntidade <> ProdutosM.FieldByName('Cliente').AsInteger then
      begin
        AdicionaLinhaTexto(Divisor, [fdpCompress], []);
        FEntidade := ProdutosM.FieldByName('Cliente').AsInteger;
        Textos[1] := 'Cliente:';
        Textos[2] := QrFornecedorNOME.Value;
        Textos[3] := 'Endereco:';
        Textos[4] := QrFornecedorENDERECO.Value;
        Alinha[1] := taLeftJustify;
        Alinha[2] := taLeftJustify;
        Alinha[3] := taLeftJustify;
        Alinha[4] := taLeftJustify;
        Alinha[5] := taLeftJustify;
        Alinha[6] := taLeftJustify;
        Alinha[7] := taLeftJustify;
        Alinha[8] := taLeftJustify;
        TamCol[1] := TA1;
        TamCol[2] := TA2;
        TamCol[3] := TA3;
        TamCol[4] := TA4;
        AdicionaLinhaColunas(4, Textos, Alinha, TamCol, [fdpCompress], '');
        //
        Textos[1] := 'Bairro:';
        Textos[2] := QrFornecedorBAIRRO.Value;
        Textos[3] := 'Cidade:';
        Textos[4] := QrFornecedorCIDADE.Value;
        AdicionaLinhaColunas(4, Textos, Alinha, TamCol, [fdpCompress], '');
        //
        TamCol[1] := TB1;
        TamCol[2] := TB2;
        TamCol[3] := TB3;
        TamCol[4] := TB4;
        TamCol[5] := TB5;
        TamCol[6] := TB6;
        TamCol[7] := TB7;
        TamCol[8] := TB8;
        Textos[1] := 'UF:';
        Textos[2] := QrFornecedorNOMEUF.Value;
        Textos[3] := 'CEP:';
        Textos[4] :=Geral.FormataCEP_NT(QrFornecedorCEP.Value);
        Textos[5] := 'CNPJ/CPF:';
        Textos[6] := Geral.FormataCNPJ_TT(QrFornecedorCNPJ.Value);
        Textos[7] := 'I.E.:/RG';
        Textos[8] := QrFornecedorIE.Value;
        AdicionaLinhaColunas(8, Textos, Alinha, TamCol, [fdpCompress], '');
        //
        Textos[1] := 'Telefone:';
        Textos[2] := Geral.FormataTelefone_TT(QrFornecedorTelefone.Value);
        Textos[3] := 'Fax:';
        Textos[4] := Geral.FormataTelefone_TT(QrFornecedorFax.Value);
        Textos[5] := 'Contato:';
        Textos[6] := QrFornecedorContato.Value;
        Textos[7] := 'Celular:';
        Textos[8] := Geral.FormataTelefone_TT(QrFornecedorCelular.Value);
        AdicionaLinhaColunas(8, Textos, Alinha, TamCol, [fdpCompress], '');
        //
        AdicionaLinhaTexto(Divisor, [fdpCompress], []);
        //
      end;
      Alinha[1] := taCenter;
      Alinha[2] := taLeftJustify;
      Alinha[3] := taLeftJustify;
      Alinha[4] := taRightJustify;
      Alinha[5] := taLeftJustify;
      Alinha[6] := taRightJustify;
      Alinha[7] := taRightJustify;
      Alinha[8] := taRightJustify;
      Alinha[9] := taRightJustify;
      TamCol[1] := 08;
      TamCol[2] := 26;
      TamCol[3] := 28;
      TamCol[4] := 08;
      TamCol[5] := 08;
      TamCol[6] := 11;
      TamCol[7] := 11;
      TamCol[8] := 11;
      TamCol[9] := 11;
      Textos[1] := 'Codigo';
      Textos[2] := 'Artigo';
      Textos[3] := 'Descri��o';
      Textos[4] := 'Qtde';
      Textos[5] := ' Un';
      Textos[6] := 'Preco';
      Textos[7] := 'Sub-Total';
      Textos[8] := 'Desconto';
      Textos[9] := 'Total';
      AdicionaLinhaColunas(9, Textos, Alinha, TamCol, [fdpCompress], '');
      ProdutosMIts.First;
      Qtde  := 0;
      Desco := 0;
      Compl := 0;
      Bruto := 0;
      Total := 0;
      while not ProdutosMIts.Eof do
      begin
        Qtde  := Qtde  + ProdutosMIts.FieldByName('Qtde').AsFloat;
        Desco := Desco + 0;//ProdutosMIts.FieldByName('DescoItens').AsFloat;
        Compl := Compl + 0;//ProdutosMIts.FieldByName('Complemento').AsFloat;
        Bruto := Bruto + (
                     ProdutosMIts.FieldByName('Qtde').AsFloat *
                     ProdutosMIts.FieldByName('Preco').AsFloat );
        Total := Total + ProdutosMIts.FieldByName('Valor').AsFloat;
        //
        Textos[1] := FormatFloat('000', ProdutosMIts.FieldByName('MP').AsInteger);
        Textos[2] := ProdutosMIts.FieldByName('NOMEMP').AsString;
        Textos[3] := ProdutosMIts.FieldByName('TEXTO').AsString;
        Textos[4] := FormatFloat('#,###,##0.00', ProdutosMIts.FieldByName('Qtde').AsFloat);
        Textos[5] := ' '+'m2';//ProdutosMIts.FieldByName('NOMEUNIDADE').AsString;
        Textos[6] := FormatFloat('#,###,##0.00', ProdutosMIts.FieldByName('Preco').AsFloat);
        Textos[7] := FormatFloat('#,###,##0.00', ProdutosMIts.FieldByName('SubTo').AsFloat);
        Textos[8] := FormatFloat('#,###,##0.00', ProdutosMIts.FieldByName('Desco').AsFloat);
        Textos[9] := FormatFloat('#,###,##0.00', ProdutosMIts.FieldByName('Valor').AsFloat );
        AdicionaLinhaColunas2(9, Textos, Alinha, TamCol, '', 0, 0);
        ProdutosMIts.Next;
      end;
      Total := Total - 0;
      //
      AdicionaLinhaTexto(Divisor, [fdpCompress], []);
      //if Qtde > 0 then Preco := Total / Qtde else Preco := 0;
      Textos[1] := '';
      Textos[2] := 'TOTAL ITENS:';
      Textos[3] := '';
      Textos[4] := FormatFloat('#,###,##0.00', Qtde);
      Textos[5] := '';
      Textos[6] := '';
      Textos[7] := '';
      Textos[8] := '';
      Textos[9] := FormatFloat('#,###,##0.00', Total);
      AdicionaLinhaColunas(9, Textos, Alinha, TamCol, [fdpCompress], '');
      AdicionaLinhaTexto(Divisor, [fdpCompress], []);
      //
      DesTo := Compl-Desco-0;
      if DesTo <> 0 then
      begin
        if Bruto > 0 then DesPo := DesTo / Bruto * 100 else DesPo := 0;
        Textos[1] := '';
        Textos[2] := '';
        if DesTo <= 0 then
        Textos[3] := 'DESCONTOS'
        else
        Textos[3] := 'ACR�SCIMOS';
        Textos[4] := FormatFloat('#,###,##0.00', Despo)+' %';
        Textos[5] := FormatFloat('#,###,##0.00', DesTo)+' $';
        Textos[6] := '';
        Textos[7] := '';
        Textos[8] := 'TOTAL';
        Textos[9] := FormatFloat('#,###,##0.00', Total);
        AdicionaLinhaColunas(7, Textos, Alinha, TamCol, [fdpCompress], '');
        AdicionaLinhaTexto(Divisor, [fdpCompress], []);
        //
      end;
      //
      if ProdutosM.FieldByName('DescoExtra').AsFloat <> 0 then
      begin
        Textos[1] := '';
        Textos[2] := 'DESCONTO EXTRA:';
        Textos[3] := '';
        Textos[4] := '';
        Textos[5] := '';
        Textos[6] := '';
        Textos[7] := '';
        Textos[8] := '';
        Textos[9] := FormatFloat('#,###,##0.00', ProdutosM.FieldByName('DescoExtra').AsFloat);
        AdicionaLinhaColunas(9, Textos, Alinha, TamCol, [fdpCompress], '');
        AdicionaLinhaTexto(Divisor, [fdpCompress], []);
        //
        Textos[1] := '';
        Textos[2] := 'TOTAL GERAL:';
        Textos[3] := '';
        Textos[4] := '';
        Textos[5] := '';
        Textos[6] := '';
        Textos[7] := '';
        Textos[8] := '';
        Textos[9] := FormatFloat('#,###,##0.00', ProdutosM.FieldByName('TOTAL').AsFloat);
        AdicionaLinhaColunas(9, Textos, Alinha, TamCol, [fdpCompress], '');
        AdicionaLinhaTexto(Divisor, [fdpCompress], []);
        //
      end;
      FQtde  := FQtde  + Qtde;
      FDesco := FDesco + Desco;
      FCompl := FCompl + Compl;
      FTotal := FTotal + Total;
    end;
    if (Cria=False) and (Mostra=True) then
    begin
        Textos[1] := '';
        Textos[2] := 'TOTAL GERAL:';
        Textos[3] := '';
        Textos[4] := '';
        Textos[5] := '';
        Textos[6] := '';
        Textos[7] := '';
        Textos[8] := '';
        Textos[9] := FormatFloat('#,###,##0.00', FTotal);
        AdicionaLinhaColunas(7, Textos, Alinha, TamCol, [fdpCompress], '');
        AdicionaLinhaTexto(Divisor, [fdpCompress], []);
    end;

    if Cria and Mostra then
    begin
      Alinha[1] := taCenter;
      Alinha[2] := taRightJustify;
      Alinha[3] := taLeftJustify;
      TamCol[1] := 11;
      TamCol[2] := 14;
      TamCol[3] := 55;
      Textos[1] := 'Vencimento';
      Textos[2] := 'Valor';
      Textos[3] := '  Carteira';
      AdicionaLinhaColunas2(3, Textos, Alinha, TamCol, '', 1, 1);
      Pagtos.First;
      Total := 0;
      while not Pagtos.Eof do
      begin
        Textos[1] := Pagtos.FieldByName('VENCTO_TXT').AsString;
        Textos[2] := Pagtos.FieldByName('VALOR_TXT').AsString;
        Textos[3] := '  '+Pagtos.FieldByName('NOMECARTEIRA').AsString;
        Total := Total + Pagtos.FieldByName('Credito').AsFloat;
        //AdicionaLinhaColunas(3, Textos, Alinha, TamCol, [fdpNormalSize], '');
        AdicionaLinhaColunas2(3, Textos, Alinha, TamCol, '', 1, 1);
        Pagtos.Next;
      end;
      AdicionaLinhaTexto(Divisor, [fdpCompress], []);
      //
      Alinha[1] := taLeftJustify;
      Alinha[2] := taRightJustify;
      Alinha[3] := taCenter;
      Alinha[4] := taLeftJustify;
      Alinha[5] := taRightJustify;
      TamCol[1] := 11;
      TamCol[2] := 14;
      TamCol[3] := 72;
      TamCol[4] := 11;
      TamCol[5] := 14;
      Textos[1] := 'Total';
      Textos[2] := FormatFloat('#,###,##0.00', Total);
      Textos[3] := '';
      Textos[4] := 'Pendente';
      Textos[5] := FormatFloat('#,###,##0.00', QrMPVTotal.Value-Total);
      AdicionaLinhaColunas2(5, Textos, Alinha, TamCol, '', 0, 0);
      AdicionaLinhaTexto(Divisor, [fdpCompress], []);
    end;
    if Mostra then
    begin
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmMPVn2.ConfiguraFatParc(Parcial: Boolean);
begin
  LaFatParcEntrega.Visible := Parcial;
  TPFatParcEntrega.Visible := Parcial;
  LaFatParcArea.Visible    := Parcial;
  EdFatParcArea.Visible    := Parcial;
end;

procedure TFmMPVn2.CorrigeFatID1Click(Sender: TObject);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctA + ' SET FatID=1013');
    Dmod.QrUpd.SQL.Add('WHERE FatID=13 AND Descricao="Venda de mercadorias"');
    Dmod.QrUpd.ExecSQL;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMPVn2.QrMPVItsCalcFields(DataSet: TDataSet);
begin
  QrMPVItsSubTo.Value := QrMPVItsValor.Value+QrMPVItsDesco.Value;
  if QrMPVItsPronto.Value < 2 then QrMPVItsPRONTO_TXT.Value := ' * N�O * '
  else QrMPVItsPRONTO_TXT.Value := Geral.FDT(QrMPVItsPronto.Value, 2);
end;

procedure TFmMPVn2.DefineVarDup;
begin
  IC3_ED_FatNum := QrMPVCodigo.Value;
  IC3_ED_NF     := 0;//Geral.IMV(EdNF.Text);
  IC3_ED_Data   := QrMPVDataF.Value;
end;

function TFmMPVn2.DiferencasEmPagtos: Double;
var
  Valor: Double;
begin
  Result := 0;
  FFat1 := 0;
  FFat2 := 0;
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  //
  LocCod(QrMPVCodigo.Value, QrMPVCodigo.Value);
  ReopenSumC();
  //
  Valor := QrMPVTOTAL.Value - QrSumCCredito.Value;
  if Valor > 0 then FFat1 := Valor else FFat1 := 0;
  //
  FFat2 := 0;//QrMPVFrete.Value - QrMPVCONFDUPLIT.Value;
end;

procedure TFmMPVn2.BtPagamentoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPagtos, BtPagamento);
end;

procedure TFmMPVn2.ExcluiPagto();
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  if UFinanceiro.ExcluiLct_FatParcela(QrEmissC, QrEmissCFatID.Value,
    QrEmissCFatNum.Value, QrEmissCFatParcela.Value, QrEmissCCarteira.Value,
    QrEmissCSit.Value, QrEmissCTipo.Value, dmkPF.MotivDel_ValidaCodigo(318),
    FTabLctA) then
  begin
    ReopenEmissC(QrEmissCFatParcela.Value);
  end;
end;

procedure TFmMPVn2.ExcluiTodosPagtos();
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  if UFinanceiro.ExcluiLct_FatNum(QrEmissC, QrEmissCFatID.Value,
  QrEmissCFatNum.Value, QrEmissCCliInt.Value, QrEmissCCarteira.Value,
  dmkPF.MotivDel_ValidaCodigo(318), True, FTabLctA) then
    ReopenEmissC(0);
end;

procedure TFmMPVn2.QrEmissCCalcFields(DataSet: TDataSet);
begin
  if QrEmissCCompensado.Value < 2 then
     QrEmissCCOMPENSA_TXT.Value := '' else
     QrEmissCCOMPENSA_TXT.Value := FormatDateTime(VAR_FORMATDATE2,
     QrEmissCCompensado.Value);
  //
  if QrEmissCVencimento.Value < 2 then
     QrEmissCVENCTO_TXT.Value := '' else
     QrEmissCVENCTO_TXT.Value := FormatDateTime(VAR_FORMATDATE2,
     QrEmissCVencimento.Value);
  //
  QrEmissCVALOR_TXT.Value := Geral.FFT(QrEmissCCredito.Value, 2, siNegativo);
  QrEmissCSEQ.Value := QrEmissC.RecordCount;
end;

procedure TFmMPVn2.EdPreco1Exit(Sender: TObject);
begin
  CalculaTotal1;
end;

procedure TFmMPVn2.EdQtde1Exit(Sender: TObject);
begin
  CalculaTotal1;
end;

procedure TFmMPVn2.EdDesco1Exit(Sender: TObject);
begin
  CalculaTotal1;
end;

procedure TFmMPVn2.frMovimentoUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  if Name = 'VARF_GRADE' then Val := 15;
  if Name = 'VARF_DESEX' then Val := dmkPF.FloatToBool(QrMPVDescoExtra.Value, 2);
  if Name = 'VARF_PENDE' then
  begin
    ReopenSumC();
    Val := dmkPF.FloatToBool(QrMPVTOTAL.Value-QrSumCCredito.Value, 2);
  end;
end;

procedure TFmMPVn2.QrFornecedorCalcFields(DataSet: TDataSet);
var
  Endereco: String;
begin
  QrFornecedorTEL_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorTelefone.Value);
  QrFornecedorFAX_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorFax.Value);
  QrFornecedorCEL_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorCelular.Value);
  QrFornecedorCPF_TXT.Value := Geral.FormataCNPJ_TT(QrFornecedorCNPJ.Value);
  QrFornecedorCEP_TXT.Value := Geral.FormataCEP_TT(FloatToStr(QrFornecedorCEP.Value));
  //
  Endereco := QrFornecedorRUA.Value;
  if Trim(QrFornecedorLOGRAD.Value) <> '' then
  begin
    if Uppercase(Copy(QrFornecedorRUA.Value, 1, Length(QrFornecedorLOGRAD.Value))) <>
      Uppercase(QrFornecedorLOGRAD.Value) then
    Endereco := QrFornecedorLOGRAD.Value + ' ' + Endereco;
  end;
  //
  if QrFornecedorNUMERO.Value = 0 then
    QrFornecedorNUMEROTXT.Value := 'S/N' else
    QrFornecedorNUMEROTXT.Value :=
    FloatToStr(QrFornecedorNUMERO.Value);
  QrFornecedorENDERECO.Value :=
    Endereco + ', '+
    QrFornecedorNUMEROTXT.Value + '  '+
    QrFornecedorCOMPL.Value;
end;

procedure TFmMPVn2.QrTransportadorCalcFields(DataSet: TDataSet);
var
  Endereco: String;
begin
  QrTransportadorTEL_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorTelefone.Value);
  QrTransportadorFAX_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorFax.Value);
  QrTransportadorCEL_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorCelular.Value);
  QrTransportadorCPF_TXT.Value := Geral.FormataCNPJ_TT(QrTransportadorCNPJ.Value);
  QrTransportadorCEP_TXT.Value := Geral.FormataCEP_TT(FloatToStr(QrTransportadorCEP.Value));
  //
  Endereco := QrTransportadorRUA.Value;
  if Trim(QrTransportadorLOGRAD.Value) <> '' then
  begin
    if Uppercase(Copy(QrTransportadorRUA.Value, 1, Length(QrTransportadorLOGRAD.Value))) <>
      Uppercase(QrTransportadorLOGRAD.Value) then
    Endereco := QrTransportadorLOGRAD.Value + ' ' + Endereco;
  end;
  //
  if QrTransportadorNUMERO.Value = 0 then
    QrTransportadorNUMEROTXT.Value := 'S/N' else
    QrTransportadorNUMEROTXT.Value :=
    FloatToStr(QrTransportadorNUMERO.Value);
  QrTransportadorENDERECO.Value :=
    Endereco + ', '+
    QrTransportadorNUMEROTXT.Value + '  '+
    QrTransportadorCOMPL.Value;
end;

procedure TFmMPVn2.Impressodireta1Click(Sender: TObject);
begin
  ImpressaoDireta2(QrMPV, QrMPVIts, QrEmissC, True, True);
end;

procedure TFmMPVn2.EdDescoExtraExit(Sender: TObject);
begin
  EdDescoExtra.Text := Geral.TFT_MinMax(EdDescoExtra.Text, 0,
  QrMPVValor.Value, 2);
end;

procedure TFmMPVn2.QrMPVCalcFields(DataSet: TDataSet);
begin
  QrMPVTOTAL.Value := QrMPVValor.Value - QrMPVDescoExtra.Value;
end;

procedure TFmMPVn2.MenuItem3Click(Sender: TObject);
begin
  ExcluiMercadoria;
end;

procedure TFmMPVn2.ExcluiMercadoria;
var
  Codigo, Controle: Integer;
begin
  Codigo := QrMPVItsCodigo.Value;
  Controle := QrMPVItsControle.Value;
  if Geral.MB_Pergunta(FIN_MSG_ASKESCLUI) = ID_YES then
  begin
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mpvits WHERE Codigo=:P0');
    Dmod.QrUpd.SQL.Add('AND Controle=:P2');
    Dmod.QrUpd.Params[0].AsInteger := Codigo;
    Dmod.QrUpd.Params[1].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    AtualizaTotais;
    LocCod(QrMPVCodigo.Value, QrMPVCodigo.Value);
    ReopenMPVIts;
  end;
end;

procedure TFmMPVn2.MenuItem4Click(Sender: TObject);
begin
  ExcluiPagto;
end;

procedure TFmMPVn2.BtImprimeClick(Sender: TObject);
begin
  ImpressaoOculta;
end;

procedure TFmMPVn2.ImpressaoOculta;
begin
  ReopenTransportador(QrMPV);
  ReopenFornecedor(QrMPV);
  ReopenSumIts(QrMPV);
  //
  {
  frMovimento.PrepareReport;
  frMovimento.PrintPreparedReport('', 2, True, frAll);
  }
  frxVenda.PrintOptions.Copies := 2;
  MyObjects.frxImprime(frxVenda, 'Border� de Venda');
end;

procedure TFmMPVn2.Incluinovamercadoria1Click(Sender: TObject);
begin
  FEditItem := False;
  PnManual.Visible   := True;
  PainelDados.Visible := False;
  if EdMP.Visible then
    EdMP.SetFocus;
end;

procedure TFmMPVn2.Alteradadosdamercadoriaatual1Click(Sender: TObject);
begin
  FEditItem := True;
  PnManual.Visible   := True;
  PainelDados.Visible := False;
  if EdMP.Visible then
    EdMP.SetFocus;
  //
  EdMP.Text := IntToStr(QrMPVItsMP.Value);
  CBMP.KeyValue := QrMPVItsMP.Value;
  EdTexto.Text := QrMPVItsTexto.Value;
  EdClasse.Text := QrMPVItsClasse.Value;
  EdEspesTxt.Text := QrMPVItsEspesTxt.Value;
  EdCorTxt.Text := QrMPVItsCorTxt.Value;
  EdQtde1.Text := Geral.FFT(QrMPVItsQtde.Value, 2, siNegativo);
  EdPreco1.Text := Geral.FFT(QrMPVItsPreco.Value, 2, siNegativo);
  EdDesco1.Text := Geral.FFT(QrMPVItsDesco.Value, 2, siNegativo);
  TPEntrega.Date := QrMPVItsEntrega.Value;
  CalculaTotal1;
end;

procedure TFmMPVn2.Excluimercadoriaatual1Click(Sender: TObject);
begin
  ExcluiMercadoria;
end;

procedure TFmMPVn2.Incluinovospagamentos1Click(Sender: TObject);
var
  Terceiro, Cod : Integer;
begin
  DiferencasEmPagtos;
  DefineVarDup;
  VAR_MYPAGTOSCONFIG   := 1;
  Cod       := QrMPVCodigo.Value;
  Terceiro  := QrMPVCliente.Value;
  UPagtos.Pagto(QrEmissC, tpCred, Cod, Terceiro, VAR_FATID_1013, -13, 0(*GenCtb*), stIns,
  'Venda de Mercadoria', FFat1, VAR_USUARIO, 0, 0, mmNenhum, 0, 0,
  True, False, 0, 0, 0, 0, 0, FTabLctA);
  ReopenEmissC(0);
  //
  (*
  if FDireto then
  begin
    BtTravaClick(Self);
    BtAlteraClick(Self);
  end;
  *)
end;

procedure TFmMPVn2.Alterapagamentoatual1Click(Sender: TObject);
var
  CliInt, Terceiro, Cod : Integer;
  Valor: Double;
begin
  IC3_ED_Controle := QrEmissCControle.Value;
  Valor     := QrEmissCCredito.Value;
  CliInt     := QrEmissCCliInt.Value;
  /////////////////
  DiferencasEmPagtos;
  DefineVarDup;
  Cod       := QrMPVCodigo.Value;
  Terceiro  := QrMPVCliente.Value;
  UPagtos.Pagto(QrEmissC, tpCred, Cod, Terceiro, VAR_FATID_1013, -13, 0(*GenCtb*), stUpd,
  'Venda de Mercadorias', Valor, VAR_USUARIO, 0, CliInt, mmNenhum, 0, 0,
  True, False, 0, 0, 0, 0, 0, FTabLctA);
  //
  AtualizaTotais;
  LocCod(QrMPVCodigo.Value, QrMPVCodigo.Value);
end;

procedure TFmMPVn2.Excluipagamentoatual1Click(Sender: TObject);
begin
  ExcluiPagto;
end;

procedure TFmMPVn2.Excluitodospagamentos1Click(Sender: TObject);
begin
  ExcluiTodosPagtos;
end;

procedure TFmMPVn2.PMMercadoriaPopup(Sender: TObject);
begin
  if (QrMPVIts.State <> dsInactive) and (QrMPVIts.RecordCount > 0) then
  begin
    Alteradadosdamercadoriaatual1.Enabled       := True;
    Excluimercadoriaatual1.Enabled              := True;
    Desfazfaturamentodemercadoriaatual1.Enabled := True;
  end else
  begin
    Alteradadosdamercadoriaatual1.Enabled       := False;
    Excluimercadoriaatual1.Enabled              := False;
    Desfazfaturamentodemercadoriaatual1.Enabled := False;
  end;
end;

procedure TFmMPVn2.PMPagtosPopup(Sender: TObject);
begin
  if QrEmissC.RecordCount > 0 then
  begin
    Alterapagamentoatual1.Enabled := True;
    Excluipagamentoatual1.Enabled := True;
    if QrEmissC.RecordCount > 1 then
      Excluitodospagamentos1.Enabled := True
    else
      Excluitodospagamentos1.Enabled := False;
  end else begin
    Alterapagamentoatual1.Enabled := False;
    Excluipagamentoatual1.Enabled := False;
    Excluitodospagamentos1.Enabled := False;
  end;
end;

procedure TFmMPVn2.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if Geral.MB_Pergunta('Confirma a EXCLUS�O DE TODO pedido?') = ID_YES then
  begin
    Codigo := QrMPVCodigo.Value;
    //
    if (QrEmissC.State <> dsInactive) and (QrEmissC.RecordCount > 0) then
    begin
      UFinanceiro.ExcluiLct_FatNum(QrEmissC, QrEmissCFatID.Value,
        QrEmissCFatNum.Value, 0, QrEmissCCarteira.Value,
        dmkPF.MotivDel_ValidaCodigo(318), False, FTabLctA);
    end;
    //
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mpvits WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mpv WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    QrMPV.Close;
    Va(vpNext);
  end;
end;

procedure TFmMPVn2.EdControleChange(Sender: TObject);
begin
  ReopenQrPed(Geral.IMV(EdControle.Text));
end;

procedure TFmMPVn2.ReopenQrPed(OS_Controle: Integer);
begin
  QrPed.Close;
  if OS_Controle > 0 then
  begin
(*  ini 2023-09-25
    QrPed.Params[0].AsInteger := OS_Controle;
    UnDmkDAC_PF.AbreQuery(QrPed, Dmod.MyDB);
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrPed, Dmod.MyDB, [
    'SELECT IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLI,',
    'IF(ve.Tipo=0, ve.RazaoSocial, ve.Nome) NOMEVEN, ag.Nome NOMEMP,',
    'pp.Codigo PEDIDO, pp.Cliente, pp.Vendedor, pp.Obz, ',
    'pp.DataF, pp.Transp, pp.CondicaoPg, pp.PedidCli, mi.* ',
    'FROM mpvits mi',
    'LEFT JOIN mpp pp ON pp.Codigo=mi.Pedido',
    'LEFT JOIN entidades cl ON cl.Codigo=pp.Cliente',
    'LEFT JOIN entidades ve ON ve.Codigo=pp.Vendedor',
    'LEFT JOIN artigosgrupos ag ON ag.Codigo=mi.MP',
    'WHERE mi.Codigo=0',
    'AND mi.Controle=' + Geral.FF0(OS_Controle),
    '']);
    // fim 2023-09-25
  end;
end;

procedure TFmMPVn2.ReopenSumC();
begin
  (*
  QrSumC.Close;
  QrSumC.Params[00].AsInteger := VAR_FATID_1013;
  QrSumC.Params[01].AsInteger := QrMPVCodigo.Value;
  QrSumC. O p e n;
  *)
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumC, Dmod.MyDB, [
  'SELECT SUM(Credito) Credito ',
  'FROM ' + FTabLctA,
  'WHERE FatID=' + Geral.FF0(VAR_FATID_1013),
  'AND FatNum=' + Geral.FF0(QrMPVCodigo.Value),
  '']);
end;

procedure TFmMPVn2.ReopenSumIts(Qry: TmySQLQuery);
begin
  QrSumIts.Close;
  QrSumIts.Params[0].AsInteger := Qry.FieldByName('Codigo').AsInteger;
  UnDmkDAC_PF.AbreQuery(QrSumIts, Dmod.MyDB);
end;

procedure TFmMPVn2.ReopenTransportador(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTransportador, Dmod.MyDB, [
  'SELECT ',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOME, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ERua ',
  'ELSE fo.PRua END RUA, ',
  '(CASE WHEN fo.Tipo=0 THEN fo.ENumero ',
  'ELSE fo.PNumero END) + 0.000 NUMERO, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ECompl ',
  'ELSE fo.PCompl END COMPL, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EBairro ',
  'ELSE fo.PBairro END BAIRRO, ',
  'mu.Nome CIDADE, ',
  'pa.Nome PAIS, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ETe1 ',
  'ELSE fo.PTe1 END TELEFONE, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EFax ',
  'ELSE fo.PPais END FAX, ',
  'CASE WHEN fo.Tipo=0 THEN fo.ECel ',
  'ELSE fo.PCel END Celular, ',
  'CASE WHEN fo.Tipo=0 THEN fo.CNPJ ',
  'ELSE fo.CPF END CNPJ, ',
  'CASE WHEN fo.Tipo=0 THEN fo.IE ',
  'ELSE fo.RG END IE, ',
  '(CASE WHEN fo.Tipo=0 THEN fo.ECEP ',
  'ELSE fo.PCEP END) + 0.000 CEP, ',
  'CASE WHEN fo.Tipo=0 THEN fo.EContato ',
  'ELSE fo.PContato END Contato, ',
  ' ',
  '(CASE WHEN fo.Tipo=0 THEN fo.EUF ',
  'ELSE fo.PUF END) + 0.000 UF, ',
  'CASE WHEN fo.Tipo=0 THEN uf1.Nome ',
  'ELSE uf2.Nome END NOMEUF, ',
  'EEMail, PEmail, ',
  ' ',
  'CASE WHEN fo.Tipo=0 THEN ll1.Nome ',
  'ELSE ll2.Nome END LOGRAD ',
  ' ',
  'FROM entidades fo ',
  'LEFT JOIN ufs uf1 ON uf1.Codigo=fo.Euf ',
  'LEFT JOIN ufs uf2 ON uf2.Codigo=fo.Puf ',
  'LEFT JOIN listalograd ll1 ON ll1.Codigo=fo.ELograd ',
  'LEFT JOIN listalograd ll2 ON ll2.Codigo=fo.PLograd ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo = IF(fo.Tipo=0, fo.ECodMunici, fo.PCodMunici) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pa ON pa.Codigo = IF(fo.Tipo=0, fo.ECodiPais, fo.PCodiPais) ',
  'WHERE fo.Codigo=' + Geral.FF0(Qry.FieldByName('Transp').AsInteger),
  '']);
end;

procedure TFmMPVn2.EncerraOS1Click(Sender: TObject);
begin
  FEditItem           := False;
  PnPedido.Visible    := True;
  PainelDados.Visible := False;
  //
  if EdControle.Visible then
    EdControle.SetFocus;
end;

procedure TFmMPVn2.QrPedCalcFields(DataSet: TDataSet);
begin
  DBEdCliente.Font.Color :=
    dmkPF.EscolhaDe2Int(QrPedCliente.Value = QrMPVCliente.Value, clNavy, clRed);
  DBEdVendedor.Font.Color :=
    dmkPF.EscolhaDe2Int(QrPedVendedor.Value = QrMPVVendedor.Value, clNavy, clRed);
end;

procedure TFmMPVn2.BtDesiste2Click(Sender: TObject);
var
  Codigo: Integer;
begin
  PainelDados.Visible := True;
  OcultaPnPedido;
  AtualizaTotais;
  LocCod(QrMPVCodigo.Value, QrMPVCodigo.Value);
  //if FDireto then
  case FAcaoInicial of
    TMPVAcao.mpvaCabecalho:
    begin
      //Incluinovospagamentos1Click(Self);
      FmostraMoney := False;
    end;
    TMPVAcao.mpvaEncerraOS:
    begin
      if FPrimeiroItemInserido = False then
      begin
        GBTrava.Visible := False;
        GBCntrl.Visible := True;
        ImgTipo.SQLType := stLok;
        Codigo := QrMPVCodigo.Value;
        LocCod(Codigo, Codigo);
      end;
    end;
  end;
end;

procedure TFmMPVn2.QrPedAfterOpen(DataSet: TDataSet);
begin
  PnDadosFat.Visible := QrPed.RecordCount > 0;
  GBProducao.Visible := QrPed.RecordCount > 0;
  PCObs.Visible      := QrPed.RecordCount > 0;
  //
  PnCabecOS.Visible  := (QrPed.RecordCount > 0) and
                        (FAcaoInicial = TMPVAcao.mpvaEncerraOS) and
                        (FPrimeiroItemInserido = False);
  PnObs2.Visible     := PnCabecOS.Visible;
  //
  EdDesco2.Text := '';
  EdQtde2.Text  := Geral.FFT(QrPedM2Pedido.Value, 2, sipositivo);
  EdPreco2.Text := Geral.FFT(QrPedPrecoPed.Value, 2, sipositivo);
  //
  DBEdCliente.Font.Color :=
    dmkPF.EscolhaDe2Int(QrPedCliente.Value = QrMPVCliente.Value, clBlue, clRed);
  DBEdVendedor.Font.Color :=
    dmkPF.EscolhaDe2Int(QrPedVendedor.Value = QrMPVVendedor.Value, clBlue, clRed);
  //
  ConfiguraValCamposFatParc(0);
end;

procedure TFmMPVn2.EdControleKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then MostraOS_Cli
  else
  if Key = VK_F5 then MostraOS_All;
end;

procedure TFmMPVn2.CBOS_CliKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in ([VK_TAB, VK_ESCAPE, VK_F6]) then
  begin
    EdControle.Visible := True;
    LaControle.Font.Color := clBlue;
    if EdControle.Visible then
      EdControle.SetFocus;
    CBOS_Cli.Visible    := False;
    LaOS_Cli.Font.Color := clGray;
    CBOS_All.Visible    := False;
    LaOS_All.Font.Color := clGray;
  end else
  if Key = VK_F5 then
  begin
    MostraOS_All;
    EdControle.Visible := False;
    CBOS_Cli.Visible   := False;
    LaOS_Cli.Font.Color := clGray;
  end;
end;

procedure TFmMPVn2.CkFatParcClick(Sender: TObject);
var
  Parcial: Boolean;
begin
  Parcial := CkFatParc.Checked;
  //
  ConfiguraFatParc(Parcial);
end;

procedure TFmMPVn2.QrPesCliAfterScroll(DataSet: TDataSet);
begin
  if CBOS_Cli.KeyValue = NULL then
    EdControle.Text := ''
  else
    EdControle.Text := IntToStr(CBOS_Cli.KeyValue);
end;

procedure TFmMPVn2.EdQtde2Change(Sender: TObject);
begin
  if Geral.DMV(EdQtde2.Text) <= 0 then
  begin
    EdDesco2.Enabled := False;
    EdDesco2.Text := '0,00';
  end else
    EdDesco2.Enabled := True;
  CalculaTotal2;
end;

procedure TFmMPVn2.EdQtde2Exit(Sender: TObject);
begin
  CalculaTotal2;
end;

procedure TFmMPVn2.EdDesco2Exit(Sender: TObject);
begin
  CalculaTotal2;
end;

procedure TFmMPVn2.EdPreco2Change(Sender: TObject);
begin
  CalculaTotal2;
end;

procedure TFmMPVn2.EdPreco2Exit(Sender: TObject);
begin
  CalculaTotal2;
end;

procedure TFmMPVn2.OcultaPnPedido;
begin
  EdControle.Visible := True;
  QrPed.Close;
  QrPesCli.Close;
  QrPesAll.Close;
  PnPedido.Visible   := False;
  CBOS_Cli.KeyValue  := NULL;
  CBOS_All.KeyValue  := NULL;
  EdControle.Text    := '';
  EdControle.Visible := True;
  CBOS_Cli.Visible   := False;
  CBOS_All.Visible   := False;
  //
  EdDesco2.Text := '';
  EdQtde2.Text  := '';
  EdPreco2.Text := '';
end;

procedure TFmMPVn2.CBOS_AllKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in ([VK_TAB, VK_ESCAPE, VK_F6]) then
  begin
    EdControle.Visible := True;
    LaControle.Font.Color := clBlue;
    if EdControle.Visible then
      EdControle.SetFocus;
    CBOS_Cli.Visible    := False;
    LaOS_Cli.Font.Color := clGray;
    CBOS_All.Visible    := False;
    LaOS_All.Font.Color := clGray;
  end else
  if Key = VK_F4 then
  begin
    MostraOS_Cli;
    EdControle.Visible := False;
    CBOS_All.Visible   := False;
     LaOS_All.Font.Color := clGray;
  end;
end;

procedure TFmMPVn2.MostraOS_Cli;
var
  Controle: Integer;
begin
  // N�o funciona
  //if TCustomComboBox(CBOS_Cli).DroppedDown then Exit;
  if SendMessage(CBOS_Cli.Handle, CB_GETDROPPEDSTATE, 0, 0) = 1 then
    SendMessage(CBOS_Cli.Handle, CB_SHOWDROPDOWN, 0, 0);
  //if TCustomComboBox(CBOS_All).DroppedDown then Exit;
  if SendMessage(CBOS_All.Handle, CB_GETDROPPEDSTATE, 0, 0) = 1 then
    SendMessage(CBOS_All.Handle, CB_SHOWDROPDOWN, 0, 0);
  //
  Controle := Geral.IMV(EdControle.Text);
  //
  QrPesCli.Close;
  QrPesCli.Params[0].AsInteger := QrMPVCliente.Value;
  UnDmkDAC_PF.AbreQuery(QrPesCli, Dmod.MyDB);
  if QrPesCli.Locate('Controle', Controle, []) then
  CBOS_Cli.KeyValue := Controle else
  begin
    CBOS_Cli.KeyValue := Controle;
    EdControle.Text := '';
  end;
  //
  CBOS_Cli.Visible   := True;
  LaOS_Cli.Font.Color := clBlue;
  if CBOS_Cli.Visible then
    CBOS_Cli.SetFocus;
  CBOS_All.Visible   := False;
  LaOS_All.Font.Color := clGray;
  EdControle.Visible := False;
  LaControle.Font.Color := clGray;
end;

procedure TFmMPVn2.MostraOS_All;
var
  Controle: Integer;
begin
  // N�o funciona
  //if TCustomComboBox(CBOS_Cli).DroppedDown then Exit;
  if SendMessage(CBOS_Cli.Handle, CB_GETDROPPEDSTATE, 0, 0) = 1 then
    SendMessage(CBOS_Cli.Handle, CB_SHOWDROPDOWN, 0, 0);
  //if TCustomComboBox(CBOS_All).DroppedDown then Exit;
  if SendMessage(CBOS_All.Handle, CB_GETDROPPEDSTATE, 0, 0) = 1 then
    SendMessage(CBOS_All.Handle, CB_SHOWDROPDOWN, 0, 0);
  //
  Controle := Geral.IMV(EdControle.Text);
  //
  QrPesAll.Close;
  UnDmkDAC_PF.AbreQuery(QrPesAll, Dmod.MyDB);
  if QrPesAll.Locate('Controle', Controle, []) then
  CBOS_All.KeyValue := Controle else
  begin
    CBOS_All.KeyValue := Controle;
    EdControle.Text := '';
  end;
  //
  CBOS_All.Visible   := True;
  LaOS_All.Font.Color := clBlue;
  if CBOS_All.Visible then
    CBOS_All.SetFocus;
  CBOS_Cli.Visible   := False;
  LaOS_Cli.Font.Color := clGray;
  EdControle.Visible := False;
  LaControle.Font.Color := clGray;
end;

function TFmMPVn2.InsereFatParc(OriControle: Integer): Boolean;
var
  Controle: Integer;
  Entrega: String;
  Qtde, M2Pedido: Double;
begin
  if (CkFatParc.Checked = True) and (TSFatParc.TabVisible = True) then
  begin
    Result   := False;
    Entrega  := Geral.FDT(TPFatParcEntrega.Date, 1);
    M2Pedido := EdFatParcArea.ValueVariant;
    Qtde     := EdQtde2.ValueVariant;
    //
    if MyObjects.FIC(TPFatParcEntrega.Date = 0, TPEntrega, 'Informe a nova data de entrega!') then Exit;
    if MyObjects.FIC((Qtde + M2Pedido) > QrPedM2Pedido.Value, EdFatParcArea,
      'A soma da quantidade com a quantidade restante n�o pode ser superior a quantidade do pedido!') then Exit;
    //
    Controle := UMyMod.BuscaEmLivreY_Def('mpvits', 'Controle', stIns, 0);
    //
    if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'mpvits', TMeuDB,
      ['Controle'], [OriControle], ['Controle', 'Entrega', 'M2Pedido'],
      [Controle, Entrega, M2Pedido], '', True, LaAviso1, LaAviso2) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mpvits', False,
        ['M2Pedido'], ['Controle'], [Qtde], [OriControle], True)
      then
        Result := True;
    end;
  end else
    Result := True;
end;

function TFmMPVn2.InsereMPV(Codigo, Cliente, Transp, Vendedor: Integer;
DataF: String; (*Qtde, Valor,*) ComissV_Per, (*ComissV_Val,*) DescoExtra: Double;
Volumes: Integer; Obs (*Obz,*): String; EntInt, CondicaoPg: Integer): Boolean;
begin
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'mpv', False, [
  'Cliente', 'Transp', 'Vendedor',
  'DataF', (*'Qtde', 'Valor',*)
  'ComissV_Per', (*'ComissV_Val',*) 'DescoExtra',
  'Volumes', 'Obs', (*'Obz',*)
  'EntInt', 'CondicaoPg'], [
  'Codigo'], [
  Cliente, Transp, Vendedor,
  DataF, (*Qtde, Valor,*)
  ComissV_Per, (*ComissV_Val,*) DescoExtra,
  Volumes, Obs, (*Obz,*)
  EntInt, CondicaoPg], [
  Codigo], True);
end;

procedure TFmMPVn2.BitBtn1Click(Sender: TObject);
var
  Controle: Integer;
  Qtde, Preco, Valor, Desco: Double;
  //
  Codigo, Cliente, Transp, Vendedor, EntInt, Volumes, CondicaoPg: Integer;
  DataF, Obs: String;
  (*Qtde, Valor,*)
  ComissV_Per, (*ComissV_Val,*) DescoExtra (*Obz,*): Double;
begin
  Controle := Geral.IMV(EdControle.Text);
  if Controle = 0 then
  begin
    Geral.MB_Aviso('Defina a O.S.');
    if EdControle.Visible then
      EdControle.SetFocus
    else if CBOS_All.Visible then
      CBOS_All.SetFocus
    else if CBOS_Cli.Visible then
      CBOS_Cli.SetFocus;
    Exit;
  end;
  if MyObjects.FIC(InsereFatParc(Controle) = False, nil,
    'Falha ao atualizar dados de faturamento parcial!') then Exit;
  //
  if FAcaoInicial = TMPVAcao.mpvaEncerraOS then
  begin
    if FPrimeiroItemInserido = False then
    begin
      Codigo := NovoCodigo();
      //
      Cliente      := QrPedCliente.Value;
      Transp       := QrPedTransp.Value;
      Vendedor     := QrPedVendedor.Value;
      DataF        := Geral.FDT(DModG.ObtemAgora(), 1);
      (*Qtde, Valor,*)
      ComissV_Per  := EdComissV_Per2.ValueVariant;
      (*ComissV_Val,*)
      DescoExtra   := 0.00;
      Volumes      := EdVolumes2.ValueVariant;
      Obs          := MeObs2.Text;
      (*Obz,*)
      EntInt       := DmodG.QrEmpresasCodigo.Value;
      CondicaoPg   := QrPedCondicaoPg.Value;
      //
      if not InsereMPV(Codigo, Cliente, Transp, Vendedor, DataF, (*Qtde, Valor,*)
      ComissV_Per, (*ComissV_Val,*) DescoExtra, Volumes, Obs (*Obz,*), EntInt,
      CondicaoPg) then
      begin
        Geral.MB_Erro('N�o foi poss�vel incluir o cabe�alho do faturamento!');
        Exit;
      end else
      begin
        LocCod(Codigo, Codigo);
        if QrMPVCodigo.Value <> Codigo then
        begin
          Geral.MB_Erro('N�o foi poss�vel localizar o novo faturamento gerado: ' +
          Geral.FF0(Codigo));
          Exit;
        end;
      end;
    end else
      Codigo := QrMPVCodigo.Value;
  end else
    Codigo := QrMPVCodigo.Value;
  //
  Qtde  := Geral.DMV(EdQtde2.Text);
  Preco := Geral.DMV(EdPreco2.Text);
  Valor := Geral.DMV(EdValor2.Text);
  Desco := Geral.DMV(EdDesco2.Text);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE mpvits SET ');
  Dmod.QrUpd.SQL.Add('Qtde=:P0, Preco=:P1, Valor=:P2, Desco=:P3, ');
  Dmod.QrUpd.SQL.Add('Codigo=:Pa WHERE Controle=:Pb');
  //
  Dmod.QrUpd.Params[00].AsFloat   := Qtde;
  Dmod.QrUpd.Params[01].AsFloat   := Preco;
  Dmod.QrUpd.Params[02].AsFloat   := Valor;
  Dmod.QrUpd.Params[03].AsFloat   := Desco;
  //
  Dmod.QrUpd.Params[04].AsInteger := Codigo;
  Dmod.QrUpd.Params[05].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  FPrimeiroItemInserido := True;
  //
  QrPed.Close;
  if CkContinuar2.Checked then
  begin
    if QrPesCli.State <> dsInactive then
    begin
      QrPesCli.Close;
      QrPesCli.Params[0].AsInteger := QrMPVCliente.Value;
      UnDmkDAC_PF.AbreQuery(QrPesCli, Dmod.MyDB);
    end;
    //
    if QrPesAll.State <> dsInactive then
    begin
      QrPesAll.Close;
      UnDmkDAC_PF.AbreQuery(QrPesAll, Dmod.MyDB);
    end;
    //
    EdControle.Text   := '';
    CBOS_All.KeyValue := NULL;
    CBOS_Cli.KeyValue := NULL;
    EdDesco2.Text     := '';
    EdQtde2.Text      := '';
    EdPreco2.Text     := '';
    //
    EdComissV_Per2.ValueVariant := 0.00;
    EdVolumes2.ValueVariant     := 0;
    MeObs2.Text                 := EmptyStr;
    //
    if EdControle.Visible then
      EdControle.SetFocus
    else if CBOS_All.Visible then
      CBOS_All.SetFocus
    else if CBOS_Cli.Visible then
      CBOS_Cli.SetFocus;
  end else
    BtDesiste2click(Self);
end;

procedure TFmMPVn2.QrPesAllAfterScroll(DataSet: TDataSet);
begin
  if CBOS_All.KeyValue = NULL then
    EdControle.Text := ''
  else
    EdControle.Text := IntToStr(CBOS_All.KeyValue);
end;

procedure TFmMPVn2.QrPedAfterClose(DataSet: TDataSet);
begin
  PnDadosFat.Visible := False;
  GBProducao.Visible := False;
  PCObs.Visible      := False;
  PnCabecOS.Visible  := False;
  PnObs2.Visible     := False;
  EdDesco2.Text := '';
  EdQtde2.Text  := '';
  EdPreco2.Text := '';
  //
  ConfiguraValCamposFatParc(0);
end;

procedure TFmMPVn2.Antiga1Click(Sender: TObject);
begin
  ImpressaoNormal(QrMPV, QrMPVIts, QrEmissC);
end;

procedure TFmMPVn2.Nova1Click(Sender: TObject);
begin
  ReopenTransportador(QrMPV);
  ReopenFornecedor(QrMPV);
  ReopenSumIts(QrMPV);
  //
  frxVenda.PrintOptions.Copies := 1;
  MyObjects.frxMostra(frxVenda, 'Border� de Venda');
end;

function TFmMPVn2.NovoCodigo(): Integer;
begin
  Result :=
    UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'MPV', 'MPV', 'Codigo');
end;

function TFmMPVn2.frxVendaUserFunction(const MethodName: String;
  var Params: Variant): Variant;
begin
  if MethodName = 'VARF_DESEX' then
    Params := dmkPF.FloatToBool(QrMPVDescoExtra.Value, 2);
end;

procedure TFmMPVn2.frxVendaGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_DESEX' then
    Value := dmkPF.FloatToBool(QrMPVDescoExtra.Value, 2);
end;

end.

