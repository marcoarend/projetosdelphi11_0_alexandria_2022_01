object FmPQB3CiCab: TFmPQB3CiCab
  Left = 339
  Top = 185
  Caption = 'QUI-BALAN-011 :: Ciclo de Balan'#231'o - Data do Estoque Parcial'
  ClientHeight = 278
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 640
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 592
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 544
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 524
        Height = 32
        Caption = 'Ciclo de Balan'#231'o - Data do Estoque Parcial'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 524
        Height = 32
        Caption = 'Ciclo de Balan'#231'o - Data do Estoque Parcial'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 524
        Height = 32
        Caption = 'Ciclo de Balan'#231'o - Data do Estoque Parcial'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 164
    Width = 640
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 586
        Height = 17
        Caption = 
          'Ser'#225' resgatado o saldo final do dia anterior ao selecionado para' +
          ' o in'#237'cio do dia selecionado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 586
        Height = 17
        Caption = 
          'Ser'#225' resgatado o saldo final do dia anterior ao selecionado para' +
          ' o in'#237'cio do dia selecionado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 208
    Width = 640
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 494
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 492
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 640
    Height = 116
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 640
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label2: TLabel
        Left = 12
        Top = 4
        Width = 41
        Height = 13
        Caption = 'Per'#237'odo:'
      end
      object Label3: TLabel
        Left = 300
        Top = 4
        Width = 70
        Height = 13
        Caption = 'In'#237'cio do ciclo:'
      end
      object EdPeriodo: TdmkEdit
        Left = 12
        Top = 20
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdPeriodo2: TdmkEdit
        Left = 68
        Top = 20
        Width = 229
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdDataAnt: TdmkEdit
        Left = 300
        Top = 20
        Width = 105
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfDate
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0d
        ValWarn = False
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 49
      Width = 640
      Height = 67
      Align = alClient
      Caption = 'Data do balan'#231'o (exclusa):'
      TabOrder = 1
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 636
        Height = 50
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 65
          Height = 13
          Caption = 'Data exclusa:'
        end
        object TPDataAtu: TdmkEditDateTimePicker
          Left = 8
          Top = 16
          Width = 112
          Height = 21
          Date = 45225.000000000000000000
          Time = 0.361018391202378600
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65527
  end
  object QrMovim: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Empresa, CliOrig, Insumo,'
      'SUM(IF(DataX<="2023-10-06", Peso, 0)) PesoInic,'
      'SUM(IF(DataX<="2023-10-06", Valor, 0)) ValorInic,'
      'SUM(Peso) PesoAtuA, SUM(Valor) ValorAtuA,'
      '0.000 PesoAtuaB, 0.00 ValorAtuaB,'
      '0.000 PesoAtuaC, 0.00 ValorAtuaC,'
      ''
      
        'SUM(IF(DataX>"2023-10-06" AND Peso>0 AND (NOT Tipo IN (0,20)), P' +
        'eso, 0)) PesoPeIn,'
      
        'SUM(IF(DataX>"2023-10-06" AND Peso>0 AND (NOT Tipo IN (0,20)), V' +
        'alor, 0)) ValorPeIn,'
      
        'SUM(IF(DataX>"2023-10-06" AND Peso<0 AND (NOT Tipo IN (0,120)), ' +
        'Peso, 0)) PesoPeBx,'
      
        'SUM(IF(DataX>"2023-10-06" AND Peso<0 AND (NOT Tipo IN (0,120)), ' +
        'Valor, 0)) ValorPeBx,'
      ''
      
        'SUM(IF(DataX>"2023-10-06" AND Peso>0 AND Tipo IN (0,20), Peso, 0' +
        ')) PesoAjIn,'
      
        'SUM(IF(DataX>"2023-10-06" AND Peso>0 AND Tipo IN (0,20), Valor, ' +
        '0)) ValorAjIn,'
      
        'SUM(IF(DataX>"2023-10-06" AND Peso<0 AND Tipo IN (0,120), Peso, ' +
        '0)) PesoAjBx,'
      
        'SUM(IF(DataX>"2023-10-06" AND Peso<0 AND Tipo IN (0,120), Valor,' +
        ' 0)) ValorAjBx,'
      ''
      '0.000 PesoInfo, 0.00 ValorInfo, 0.000 PesoDife, 0.00 ValorDife,'
      '0 Serie, 0 NF, "                    " xLote,'
      'DATE("0000-00-00") dFab, DATE("0000-00-00") dVal'
      'FROM bluederm.pqx'
      'WHERE DataX BETWEEN "2023-10-01" AND "2023-10-11"'
      'GROUP BY Empresa, CliOrig, Insumo'
      '')
    Left = 178
    Top = 128
    object QrMovimEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrMovimCliOrig: TIntegerField
      FieldName = 'CliOrig'
      Required = True
    end
    object QrMovimInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrMovimPesoInic: TFloatField
      FieldName = 'PesoInic'
    end
    object QrMovimValorInic: TFloatField
      FieldName = 'ValorInic'
    end
    object QrMovimPesoAtuA: TFloatField
      FieldName = 'PesoAtuA'
    end
    object QrMovimValorAtuA: TFloatField
      FieldName = 'ValorAtuA'
    end
    object QrMovimPesoAtuB: TFloatField
      FieldName = 'PesoAtuB'
      Required = True
    end
    object QrMovimValorAtuB: TFloatField
      FieldName = 'ValorAtuB'
      Required = True
    end
    object QrMovimPesoAtuC: TFloatField
      FieldName = 'PesoAtuC'
      Required = True
    end
    object QrMovimValorAtuC: TFloatField
      FieldName = 'ValorAtuC'
      Required = True
    end
    object QrMovimPesoPeIn: TFloatField
      FieldName = 'PesoPeIn'
    end
    object QrMovimValorPeIn: TFloatField
      FieldName = 'ValorPeIn'
    end
    object QrMovimPesoPeBx: TFloatField
      FieldName = 'PesoPeBx'
    end
    object QrMovimValorPeBx: TFloatField
      FieldName = 'ValorPeBx'
    end
    object QrMovimPesoAjIn: TFloatField
      FieldName = 'PesoAjIn'
    end
    object QrMovimValorAjIn: TFloatField
      FieldName = 'ValorAjIn'
    end
    object QrMovimPesoAjBx: TFloatField
      FieldName = 'PesoAjBx'
    end
    object QrMovimValorAjBx: TFloatField
      FieldName = 'ValorAjBx'
    end
    object QrMovimPesoInfo: TFloatField
      FieldName = 'PesoInfo'
      Required = True
    end
    object QrMovimValorInfo: TFloatField
      FieldName = 'ValorInfo'
      Required = True
    end
    object QrMovimPesoDife: TFloatField
      FieldName = 'PesoDife'
      Required = True
    end
    object QrMovimValorDife: TFloatField
      FieldName = 'ValorDife'
      Required = True
    end
    object QrMovimSerie: TLargeintField
      FieldName = 'Serie'
      Required = True
    end
    object QrMovimNF: TLargeintField
      FieldName = 'NF'
      Required = True
    end
    object QrMovimxLote: TWideStringField
      FieldName = 'xLote'
      Required = True
    end
    object QrMovimdFab: TDateField
      FieldName = 'dFab'
    end
    object QrMovimdVal: TDateField
      FieldName = 'dVal'
    end
    object QrMovimPesoAtuT: TFloatField
      FieldName = 'PesoAtuT'
    end
    object QrMovimValorAtuT: TFloatField
      FieldName = 'ValorAtuT'
    end
  end
end
