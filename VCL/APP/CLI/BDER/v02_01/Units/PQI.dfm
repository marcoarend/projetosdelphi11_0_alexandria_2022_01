object FmPQI: TFmPQI
  Left = 368
  Top = 194
  Caption = 'USO-CONSU-001 :: Baixa de EPI'
  ClientHeight = 544
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PnEdit: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 133
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 76
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Data:'
        FocusControl = DBEdit3
      end
      object Label3: TLabel
        Left = 192
        Top = 8
        Width = 80
        Height = 13
        Caption = 'Setor de destino:'
      end
      object Label4: TLabel
        Left = 368
        Top = 8
        Width = 229
        Height = 13
        Caption = 'Funcion'#225'rio / empresa de terceiro que ir'#225' utilizar:'
      end
      object SpeedButton6: TSpeedButton
        Left = 764
        Top = 23
        Width = 23
        Height = 23
        Caption = '...'
      end
      object Label5: TLabel
        Left = 16
        Top = 48
        Width = 118
        Height = 13
        Caption = 'Descri'#231#227'o / observa'#231#227'o:'
        FocusControl = DBEdit3
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDataB: TdmkEditDateTimePicker
        Left = 76
        Top = 24
        Width = 112
        Height = 21
        Date = 38799.000000000000000000
        Time = 0.930188981503306400
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdSetor: TdmkEditCB
        Left = 192
        Top = 24
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSetor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSetor: TdmkDBLookupComboBox
        Left = 224
        Top = 24
        Width = 137
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSE
        TabOrder = 3
        dmkEditCB = EdSetor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdFunci: TdmkEditCB
        Left = 368
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFunci
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFunci: TdmkDBLookupComboBox
        Left = 424
        Top = 24
        Width = 341
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NO_Funci'
        ListSource = DsFunci
        TabOrder = 5
        dmkEditCB = EdFunci
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 64
        Width = 769
        Height = 21
        MaxLength = 30
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 384
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 12
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object PnData: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 89
      Align = alTop
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label6: TLabel
        Left = 72
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Data:'
        FocusControl = DBEdit3
      end
      object Label7: TLabel
        Left = 148
        Top = 8
        Width = 154
        Height = 13
        Caption = 'Funcion'#225'rio ou empresa terceira:'
        FocusControl = DBEdit1
      end
      object Label8: TLabel
        Left = 608
        Top = 8
        Width = 25
        Height = 13
        Caption = 'Setor'
        FocusControl = DBEdit4
      end
      object Label11: TLabel
        Left = 4
        Top = 48
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
        FocusControl = DBEdit6
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 4
        Top = 24
        Width = 65
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPQI
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit3: TDBEdit
        Left = 72
        Top = 24
        Width = 73
        Height = 21
        DataField = 'DataB'
        DataSource = DsPQI
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 148
        Top = 24
        Width = 56
        Height = 21
        DataField = 'Funci'
        DataSource = DsPQI
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 204
        Top = 24
        Width = 400
        Height = 21
        DataField = 'NO_FUNCI'
        DataSource = DsPQI
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 608
        Top = 24
        Width = 32
        Height = 21
        DataField = 'Setor'
        DataSource = DsPQI
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 640
        Top = 24
        Width = 148
        Height = 21
        DataField = 'NO_SETOR'
        DataSource = DsPQI
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 4
        Top = 64
        Width = 785
        Height = 21
        DataField = 'Nome'
        DataSource = DsPQI
        TabOrder = 6
      end
    end
    object DBGIts: TDBGrid
      Left = 9
      Top = 101
      Width = 790
      Height = 84
      DataSource = DsPQIIts
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Insumo'
          Title.Alignment = taCenter
          Title.Caption = 'C'#243'digo'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPQ'
          Title.Caption = 'Mercadoria'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Title.Alignment = taRightJustify
          Title.Caption = 'Quantidade'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Title.Alignment = taRightJustify
          Title.Caption = 'Valor total'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CAprov'
          Title.Caption = 'C.A.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtaDevol_TXT'
          Title.Caption = 'Data devolu'#231#227'o'
          Visible = True
        end>
    end
    object PnInsumos: TPanel
      Left = 0
      Top = 188
      Width = 792
      Height = 132
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      Visible = False
      object Label10: TLabel
        Left = 284
        Top = 4
        Width = 40
        Height = 13
        Caption = 'Material:'
      end
      object Label12: TLabel
        Left = 4
        Top = 4
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label13: TLabel
        Left = 716
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label17: TLabel
        Left = 324
        Top = 44
        Width = 67
        Height = 13
        Caption = 'Estoque itens:'
        FocusControl = DBEdit7
      end
      object Label18: TLabel
        Left = 432
        Top = 44
        Width = 51
        Height = 13
        Caption = 'Estoque $:'
        FocusControl = DBEdit8
      end
      object Label19: TLabel
        Left = 540
        Top = 44
        Width = 63
        Height = 13
        Caption = 'Custo $/item:'
        FocusControl = DBEdit9
      end
      object Label20: TLabel
        Left = 684
        Top = 44
        Width = 60
        Height = 13
        Caption = 'Saldo futuro:'
      end
      object Label14: TLabel
        Left = 4
        Top = 84
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label15: TLabel
        Left = 644
        Top = 4
        Width = 62
        Height = 13
        Caption = 'C.A.: [F3][F4]'
      end
      object Label16: TLabel
        Left = 500
        Top = 84
        Width = 79
        Height = 13
        Caption = 'Data devolu'#231#227'o:'
        FocusControl = DBEdit9
      end
      object EdPQ: TdmkEditCB
        Left = 284
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPQChange
        DBLookupComboBox = CBPQ
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPQ: TdmkDBLookupComboBox
        Left = 340
        Top = 20
        Width = 301
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPQ
        TabOrder = 3
        dmkEditCB = EdPQ
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCI: TdmkEditCB
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCIChange
        DBLookupComboBox = CBCI
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCI: TdmkDBLookupComboBox
        Left = 60
        Top = 20
        Width = 221
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECI'
        ListSource = DsCI
        TabOrder = 1
        dmkEditCB = EdCI
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPesoAdd: TdmkEdit
        Left = 716
        Top = 20
        Width = 69
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdPesoAddExit
      end
      object DBEdit7: TDBEdit
        Left = 324
        Top = 60
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'Peso'
        DataSource = DsSaldo
        TabOrder = 6
      end
      object DBEdit8: TDBEdit
        Left = 432
        Top = 60
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'Valor'
        DataSource = DsSaldo
        TabOrder = 7
      end
      object DBEdit9: TDBEdit
        Left = 540
        Top = 60
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'CUSTO'
        DataSource = DsSaldo
        TabOrder = 8
      end
      object EdSaldoFut: TdmkEdit
        Left = 684
        Top = 60
        Width = 100
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CkDtCorrApo: TCheckBox
        Left = 4
        Top = 64
        Width = 185
        Height = 17
        Caption = #201' corre'#231#227'o de apontamento. Data:'
        TabOrder = 10
        OnClick = CkDtCorrApoClick
      end
      object EdEmpresa: TdmkEditCB
        Left = 4
        Top = 100
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 60
        Top = 100
        Width = 437
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 12
        TabStop = False
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCAprov: TdmkEdit
        Left = 644
        Top = 20
        Width = 69
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdPesoAddExit
        OnKeyDown = EdCAprovKeyDown
      end
      object TPDtaDevol: TdmkEditDateTimePicker
        Left = 500
        Top = 100
        Width = 112
        Height = 21
        Date = 44834.000000000000000000
        Time = 0.436307986114115900
        TabOrder = 13
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPDtCorrApo: TdmkEditDateTimePicker
        Left = 192
        Top = 60
        Width = 112
        Height = 21
        Date = 44834.000000000000000000
        Time = 0.436307986114115900
        Enabled = False
        TabOrder = 14
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
    end
    object GBConfirm2: TGroupBox
      Left = 0
      Top = 384
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 3
      Visible = False
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel7: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste2: TBitBtn
            Tag = 15
            Left = 12
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesiste2Click
          end
        end
        object BtConfirma2: TBitBtn
          Tag = 14
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirma2Click
        end
      end
    end
    object GBControle: TGroupBox
      Left = 0
      Top = 320
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 4
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 95
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 269
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel1: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 124
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 244
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtExcluiClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 155
        Height = 32
        Caption = 'Baixa de EPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 155
        Height = 32
        Caption = 'Baixa de EPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 155
        Height = 32
        Caption = 'Baixa de EPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsPQI: TDataSource
    DataSet = QrPQI
    Left = 24
    Top = 220
  end
  object QrPQI: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPQIBeforeOpen
    AfterOpen = QrPQIAfterOpen
    AfterClose = QrPQIAfterClose
    AfterScroll = QrPQIAfterScroll
    SQL.Strings = (
      'SELECT * FROM pqt'
      'WHERE Codigo > 0')
    Left = 24
    Top = 172
    object QrPQICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQIDataB: TDateField
      FieldName = 'DataB'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrPQILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQINome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPQISetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrPQIFunci: TIntegerField
      FieldName = 'Funci'
    end
    object QrPQINO_FUNCI: TWideStringField
      FieldName = 'NO_FUNCI'
      Size = 100
    end
    object QrPQINO_SETOR: TWideStringField
      FieldName = 'NO_SETOR'
    end
    object QrPQITipoEnt: TSmallintField
      FieldName = 'TipoEnt'
    end
    object QrPQICNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
  end
  object QrPQIIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico, '
      'pqg.Nome NOMEGRUPO'
      'FROM pqx pqx'
      'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo'
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ'
      'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico'
      'WHERE pqx.Tipo=150'
      'AND pqx.OrigemCodi=:P0')
    Left = 92
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQIItsDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQIItsTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPQIItsCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQIItsCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPQIItsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQIItsPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQIItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQIItsOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQIItsOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQIItsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQIItsGrupoQuimico: TIntegerField
      FieldName = 'GrupoQuimico'
    end
    object QrPQIItsNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 100
    end
    object QrPQIItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPQIItsCAprov: TWideStringField
      FieldKind = fkLookup
      FieldName = 'CAprov'
      LookupDataSet = QrPQII
      LookupKeyFields = 'Controle'
      LookupResultField = 'CAprov'
      KeyFields = 'OrigemCtrl'
      Lookup = True
    end
    object QrPQIItsDtaDevol_TXT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'DtaDevol_TXT'
      LookupDataSet = QrPQII
      LookupKeyFields = 'Controle'
      LookupResultField = 'DtaDevol_TXT'
      KeyFields = 'OrigemCtrl'
      Size = 10
      Lookup = True
    end
  end
  object DsPQIIts: TDataSource
    DataSet = QrPQIIts
    Left = 92
    Top = 220
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq '
      'WHERE Ativo=1'
      'AND GGXNiv2 IN (-11,11)'
      'ORDER BY Nome')
    Left = 608
    Top = 176
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 608
    Top = 224
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 552
    Top = 224
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 552
    Top = 176
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object QrSaldo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO '
      'FROM pqcli'
      'WHERE CI=:P0'
      'AND PQ=:P1')
    Left = 672
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSaldoPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSaldoValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSaldoCUSTO: TFloatField
      FieldName = 'CUSTO'
    end
    object QrSaldoCI: TIntegerField
      FieldName = 'CI'
    end
    object QrSaldoPQ: TIntegerField
      FieldName = 'PQ'
    end
  end
  object DsSaldo: TDataSource
    DataSet = QrSaldo
    Left = 672
    Top = 224
  end
  object PMInclui: TPopupMenu
    Left = 321
    Top = 488
    object IncluiNovaBaixa1: TMenuItem
      Caption = 'Inclui nova &Baixa'
      OnClick = IncluiNovaBaixa1Click
    end
    object IncluiInsumobaixaatual1: TMenuItem
      Caption = 'Inclui &Material '#224' baixa atual'
      OnClick = IncluiInsumobaixaatual1Click
    end
  end
  object QrSumPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(pqx.Valor)*-1 CUSTO'
      'FROM pqx pqx'
      'WHERE pqx.Tipo=150'
      'AND pqx.OrigemCodi=:P0')
    Left = 164
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPQCUSTO: TFloatField
      FieldName = 'CUSTO'
    end
  end
  object PMExclui: TPopupMenu
    OnPopup = PMExcluiPopup
    Left = 549
    Top = 481
    object ExcluiItemdabaixa1: TMenuItem
      Caption = 'Exclui &Item da baixa'
      Enabled = False
      OnClick = ExcluiItemdabaixa1Click
    end
    object ExcluiBaixa1: TMenuItem
      Caption = 'Exclui &Baixa'
      Enabled = False
      OnClick = ExcluiBaixa1Click
    end
  end
  object PMAltera: TPopupMenu
    Left = 437
    Top = 480
    object AlteraBaixa1: TMenuItem
      Caption = 'Altera &Baixa'
      OnClick = AlteraBaixa1Click
    end
    object Datadedevoluo1: TMenuItem
      Caption = 'Data de devolu'#231#227'o'
      OnClick = Datadedevoluo1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 12
    Top = 4
    object Estabaixa1: TMenuItem
      Caption = '&Baixa atual'
      OnClick = Estabaixa1Click
    end
    object RecibodeentrgamodeloA1: TMenuItem
      Caption = '&Recibo de entrga modelo 1'
      OnClick = RecibodeentrgamodeloA1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object SintticoCustos1: TMenuItem
      Caption = '&Sint'#233'tico (Custos)'
      OnClick = SintticoCustos1Click
    end
    object Outros1: TMenuItem
      Caption = '&Outros (Insumos)'
      OnClick = Outros1Click
    end
  end
  object frxDsPQIIts: TfrxDBDataset
    UserName = 'frxDsPQIIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataX=DataX'
      'Tipo=Tipo'
      'CliOrig=CliOrig'
      'CliDest=CliDest'
      'Insumo=Insumo'
      'Peso=Peso'
      'Valor=Valor'
      'OrigemCodi=OrigemCodi'
      'OrigemCtrl=OrigemCtrl'
      'NOMEPQ=NOMEPQ'
      'GrupoQuimico=GrupoQuimico'
      'NOMEGRUPO=NOMEGRUPO'
      'Empresa=Empresa'
      'CAprov=CAprov'
      'DtaDevol_TXT=DtaDevol_TXT')
    DataSet = QrPQIIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 92
    Top = 272
  end
  object frxDsPQI: TfrxDBDataset
    UserName = 'frxDsPQI'
    CloseDataSource = False
    DataSet = QrPQI
    BCDToCurrency = False
    DataSetOptions = []
    Left = 24
    Top = 272
  end
  object QrSE: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM listasetores'
      'ORDER BY Nome')
    Left = 264
    Top = 200
    object QrSECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSENome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object DsSE: TDataSource
    DataSet = QrSE
    Left = 264
    Top = 248
  end
  object QrFunci: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NO_Funci'
      'FROM entidades'
      'ORDER BY NO_Funci'
      '')
    Left = 352
    Top = 404
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFunciNO_Funci: TWideStringField
      FieldName = 'NO_Funci'
      Size = 120
    end
  end
  object DsFunci: TDataSource
    DataSet = QrFunci
    Left = 352
    Top = 452
  end
  object frxUSO_CONSU_001_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.850269456000000000
    ReportOptions.LastChange = 39717.850269456000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxUSO_CONSU_001_AGetValue
    Left = 412
    Top = 144
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPQIIts
        DataSetName = 'frxDsPQIIts'
      end
      item
        DataSet = frxDsPQI
        DataSetName = 'frxDsPQI'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 117.165344570000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 30.236240000000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 64.850340000000000000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 30.220470000000000000
          Top = 64.850340000000000000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPQI."DataB"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944877010000000000
          Top = 94.488176770000000000
          Width = 264.566929130000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Material')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 385.511806140000000000
          Top = 94.488176770000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102357320000000000
          Top = 94.488176770000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo R$')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 118.220470000000000000
          Top = 64.850340000000000000
          Width = 33.322820000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setor:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 151.543290000000000000
          Top = 64.850340000000000000
          Width = 148.472480000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPQI."NO_SETOR"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 300.015770000000000000
          Top = 64.850340000000000000
          Width = 72.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_FUNCI_TERC]:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 372.015770000000000000
          Top = 64.850340000000000000
          Width = 299.653680000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPQI."NO_FUNCI"]')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Baixa de EPI')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPQIIts
        DataSetName = 'frxDsPQIIts'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102357320000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DataField = 'Valor'
          DataSet = frxDsPQIIts
          DataSetName = 'frxDsPQIIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQIIts."Valor"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944877010000000000
          Width = 264.566929130000000000
          Height = 18.897637800000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsPQIIts
          DataSetName = 'frxDsPQIIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQIIts."NOMEPQ"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 385.511806140000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataField = 'Peso'
          DataSet = frxDsPQIIts
          DataSetName = 'frxDsPQIIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQIIts."Peso"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.708720000000000000
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 681.386210000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQI."NO_FUNCI"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 260.787570000000000000
        Width = 680.315400000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Top = 0.000000000000000028
          Width = 264.566929130000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 385.511889130000000000
          Top = 0.000000000000000028
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQIIts."Peso">,Band4)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102440310000000000
          Top = 0.000000000000000028
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQIIts."Valor">,Band4)]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 196.535560000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPQIIts."Tipo"'
      end
    end
  end
  object QrPQII: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pii.*, IF(pii.DtaDevol < 2, "",'
      '  DATE_FORMAT(pii.DtaDevol, "%d/%m/%y")) DtaDevol_TXT'
      'FROM pqiits pii'
      'WHERE pii.Codigo=1')
    Left = 160
    Top = 224
    object QrPQIICodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPQIIControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQIICAprov: TWideStringField
      FieldName = 'CAprov'
    end
    object QrPQIIDtaDevol: TDateField
      FieldName = 'DtaDevol'
      Required = True
    end
    object QrPQIIDtaDevol_TXT: TWideStringField
      FieldName = 'DtaDevol_TXT'
      Size = 8
    end
  end
  object frxUSO_CONSU_001_A_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.850269456000000000
    ReportOptions.LastChange = 39717.850269456000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxUSO_CONSU_001_AGetValue
    Left = 412
    Top = 200
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPQIIts
        DataSetName = 'frxDsPQIIts'
      end
      item
        DataSet = frxDsPQI
        DataSetName = 'frxDsPQI'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 75.590514570000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 41.574790940000000000
          Width = 680.315400000000000000
          Height = 30.236240000000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 45.354320940000000000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 30.220470000000000000
          Top = 45.354320940000000000
          Width = 72.881880000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPQI."DataB"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 103.102350000000000000
          Top = 45.354320940000000000
          Width = 29.543290000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setor:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 132.645640000000000000
          Top = 45.354320940000000000
          Width = 99.338590000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPQI."NO_SETOR"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 368.047310000000000000
          Top = 45.354320940000000000
          Width = 68.220470000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_FUNCI_TERC]:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 436.267780000000000000
          Top = 45.354320940000000000
          Width = 242.960730000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPQI."NO_FUNCI"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Recibo de Entrega de EPI e/ou Uniformes - N'#176' [frxDsPQI."Codigo"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Top = 45.354320940000000000
          Width = 33.322820000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DOC]:')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 263.874150000000000000
          Top = 45.354320940000000000
          Width = 103.118120000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_CNPJ_CPF] ')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 570.709030000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPQIIts
        DataSetName = 'frxDsPQIIts'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834577320000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DataField = 'Valor'
          DataSet = frxDsPQIIts
          DataSetName = 'frxDsPQIIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQIIts."Valor"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Width = 287.244109130000000000
          Height = 18.897637800000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsPQIIts
          DataSetName = 'frxDsPQIIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQIIts."NOMEPQ"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 287.244026140000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataField = 'Peso'
          DataSet = frxDsPQIIts
          DataSetName = 'frxDsPQIIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQIIts."Peso"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Width = 143.622091180000000000
          Height = 18.897637800000000000
          DataField = 'CAprov'
          DataSet = frxDsPQIIts
          DataSetName = 'frxDsPQIIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQIIts."CAprov"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataField = 'DtaDevol_TXT'
          DataSet = frxDsPQIIts
          DataSetName = 'frxDsPQIIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQIIts."DtaDevol_TXT"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 808.819420000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.708720000000000000
        Top = 657.638220000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 681.386210000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQI."NO_FUNCI"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 612.283860000000000000
        Width = 680.315400000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Width = 287.244109130000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 287.244109130000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQIIts."Peso">,Band4)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834660310000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQIIts."Valor">,Band4)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323212990000000000
          Width = 219.212691180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 529.134200000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPQIIts."Tipo"'
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Width = 287.244109130000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Material')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 287.244026140000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834577320000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo R$')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Width = 143.622091180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ceretificado de Aprova'#231#227'o (C.A.)')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data devolu'#231#227'o')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 351.495997090000000000
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 18.897650000000000000
          Width = 672.756340000000000000
          Height = 139.842317090000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haBlock
          Memo.UTF8W = (
            
              'Declaro ter recebido gratuitamente os EPI'#39's abaixo descritos (it' +
              'em 6.2 da NR 06) para uso exclusivo no desempenho dos servi'#231'os q' +
              'ue me forem atribu'#237'dos, tendo conhecimento da obrigatoriedade pr' +
              'evista em lei, de us'#225'-lo adequadamente; guard'#225'-lo e conserv'#225'-lo,' +
              ' comunicando a empresa qualquer altera'#231#227'o que o torne parcial ou' +
              ' totalmente inutilizavel; devolver o EPI '#224' empresa em caso de mi' +
              'nha demiss'#227'o ou afastamento, reembolsando no caso de extravio ou' +
              ' uso indevido desse EPI. Tamb'#233'm estou ciente que o uso de EPI '#233' ' +
              'obrigat'#243'rio, uma vez que j'#225' recebi o treinamento espec'#237'lico do u' +
              'so de cada equipamento de prote'#231#227'o individual, estando assim, ap' +
              'to ao uso, e na eventualidade de danificar ou extraviar o equipa' +
              'mento por ato doloso ou culposo, estarei sujeito, ao desconto do' +
              ' valor do mesmo em meu sal'#225'rio, conforme o Artigo 166 da CLT. Em' +
              ' caso de dano causado pelo empregado no EPI, o desconto ser'#225' l'#237'c' +
              'ito desde que esta responsabilidade tenha sido acordada ou na oc' +
              'orr'#234'ncia de dolo do empregado, n'#227'o atendendo os preceitos da NR ' +
              '06 e n'#227'o atendendo as orienta'#231#245'es que lhe foi dada, estando cien' +
              'te que poderei sofrer as seguintes a'#231#245'es por parte do empregador' +
              ' por n'#227'o usar os EPI'#39's: (1) Advert'#234'ncia verbal; (2) Advert'#234'ncia ' +
              'escrita; (3) Suspens'#227'o sem ou com vencimento; (4) Dispensa por j' +
              'usta causa.')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'TERMO DE RESPONSABILIDADE - NR 06 da Portaria 3.214/78 do MTB')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 158.740260000000000000
          Width = 672.756340000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Artigos da CLT')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 177.637910000000000000
          Width = 672.756340000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Art. 157 - Cabe '#224's empresas:')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 264.567100000000000000
          Width = 672.756340000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Art. 158 - Cabe aos empregados:')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 196.535560000000000000
          Width = 672.756340000000000000
          Height = 68.031247090000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haBlock
          Memo.UTF8W = (
            
              'I - cumprir e fazer cumprir as normas de seguran'#231'a e medicina no' +
              ' trabalho;'
            
              'II - Instruir os empregados, atrav'#233's de ordens de servi'#231'os, quan' +
              'to as precau'#231#245'es a tomar no sentido de evitar acidentes de traba' +
              'lho ou doen'#231'as ocupacionais;'
            
              'III - adotar medidas que lhe sejam determinadas pelo org'#227'o regio' +
              'nal competente;'
            
              'IV - facilitar o exerc'#237'cio da fiscaliza'#231#227'o pela autoridade compe' +
              'tente.'
            '')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 283.464750000000000000
          Width = 672.756340000000000000
          Height = 60.472187090000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haBlock
          Memo.UTF8W = (
            
              'I - observar as normas de seguran'#231'a e medicina do trabalho, incl' +
              'usive as instru'#231#245'es de que trata o item II do Artigo antenor,'
            
              'Par'#225'grafo Unico - Constitui ato faltoso do empregado a recusa in' +
              'justificada:'
            
              'a) '#224' observ'#226'ncia das instru'#231#245'es expedidas pelo empregador na for' +
              'ma do item II do artigo anterior;'
            
              'b) ao uso dos equipamentos de prote'#231#227'o individual fornecidos pel' +
              'a empresa.')
          ParentFont = False
        end
      end
    end
  end
  object QryI: TMySQLQuery
    Database = DmProd.MySQLDatabase1
    Left = 478
    Top = 87
    object QryITEXTO: TWideStringField
      FieldName = 'TEXTO'
      Size = 511
    end
  end
end
