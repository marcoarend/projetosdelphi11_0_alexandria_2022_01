object FmPQImp: TFmPQImp
  Left = 335
  Top = 168
  Caption = 'QUI-INSUM-002 :: Relat'#243'rios de Produtos Qu'#237'micos'
  ClientHeight = 704
  ClientWidth = 1165
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1165
    Height = 548
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PCRelatorio: TPageControl
      Left = 0
      Top = 0
      Width = 1165
      Height = 295
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      OnChange = PCRelatorioChange
      object TabSheet1: TTabSheet
        Caption = 'Estoque atual'
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 1157
          Height = 267
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object PC00OrigemInfo: TPageControl
            Left = 577
            Top = 0
            Width = 580
            Height = 267
            ActivePage = TabSheet12
            Align = alClient
            TabOrder = 0
            object TabSheet2: TTabSheet
              Caption = 'Insumo por cliente'
              object Panel19: TPanel
                Left = 0
                Top = 0
                Width = 369
                Height = 239
                Align = alLeft
                TabOrder = 0
                object Label6: TLabel
                  Left = 1
                  Top = 1
                  Width = 128
                  Height = 13
                  Align = alTop
                  Alignment = taCenter
                  Caption = 'Observa'#231#245'es na impress'#227'o'
                end
                object MeObservacoes: TMemo
                  Left = 1
                  Top = 14
                  Width = 367
                  Height = 224
                  Align = alClient
                  TabOrder = 0
                end
              end
            end
            object TabSheet4: TTabSheet
              Caption = 'Insumo por NF'
              ImageIndex = 1
              object DBGrid4: TDBGrid
                Left = 0
                Top = 0
                Width = 572
                Height = 239
                Align = alClient
                DataSource = DsEstqNFs
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'OrigemCodi'
                    Title.Caption = 'ID Lct'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CliOrig'
                    Title.Caption = 'Cli.Int.'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMECI'
                    Title.Caption = 'Nome do cliente interno'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataX'
                    Title.Caption = 'Data'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Insumo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEPQ'
                    Title.Caption = 'Nome do insumo'
                    Width = 200
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NF'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Tipo'
                    Title.Caption = 'Tipo mov'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_Tipo'
                    Title.Caption = 'Descri'#231#227'o do movimento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEFO'
                    Title.Caption = 'Nome do fornecedor'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Peso'
                    Title.Caption = 'Qtd Cli.Int.'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SdoPeso'
                    Title.Caption = 'Qtd NF'
                    Visible = True
                  end>
              end
            end
            object TabSheet12: TTabSheet
              Caption = 'Diverg'#234'ncias Cliente x NF'
              ImageIndex = 2
              object DBGrid1: TDBGrid
                Left = 0
                Top = 0
                Width = 572
                Height = 239
                Align = alClient
                DataSource = DsDifPqxPqw
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'CI'
                    Title.Caption = 'Cli.Int.'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_CI'
                    Title.Caption = 'Nome do cliente interno'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Insumo'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PQ'
                    Title.Caption = 'Nome do insumo'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Peso'
                    Title.Caption = 'Qtd Cli.Int.'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SdoPeso'
                    Title.Caption = 'Qtd NF'
                    Width = 68
                    Visible = True
                  end>
              end
            end
          end
          object Panel15: TPanel
            Left = 0
            Top = 0
            Width = 577
            Height = 267
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object RGRelEstq: TRadioGroup
              Left = 8
              Top = 64
              Width = 561
              Height = 61
              Caption = 'Relat'#243'rio'
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Standard'
                'Custo padr'#227'o'
                'Dura'#231#227'o estoque cadastro'
                'Dura'#231#227'o estoque produ'#231#227'o')
              TabOrder = 0
            end
            object Bt0RecalUsoPQDiario: TBitBtn
              Tag = 127
              Left = 5
              Top = 133
              Width = 244
              Height = 40
              Cursor = crHandPoint
              Caption = 'Recalcular uso di'#225'rio de produtos qu'#237'micos'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = Bt0RecalUsoPQDiarioClick
            end
            object GroupBox1: TGroupBox
              Left = 324
              Top = 124
              Width = 245
              Height = 65
              Caption = ' Per'#237'odo referencial de produ'#231#227'o: '
              TabOrder = 2
              object Label2: TLabel
                Left = 10
                Top = 20
                Width = 55
                Height = 13
                Caption = 'Data inicial:'
              end
              object Label3: TLabel
                Left = 125
                Top = 20
                Width = 48
                Height = 13
                Caption = 'Data final:'
              end
              object TPRefProdIni: TdmkEditDateTimePicker
                Left = 9
                Top = 36
                Width = 112
                Height = 21
                Date = 37670.000000000000000000
                Time = 0.521748657396528900
                TabOrder = 0
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPRefProdFim: TdmkEditDateTimePicker
                Left = 125
                Top = 36
                Width = 112
                Height = 21
                Date = 37670.000000000000000000
                Time = 0.521748657396528900
                TabOrder = 1
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
            end
            object CGPositivo1: TdmkCheckGroup
              Left = 8
              Top = 0
              Width = 273
              Height = 61
              Caption = ' Filtrar insumos pelo seu estoque em peso: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Positivos'
                'Zerados'
                'Negativos')
              TabOrder = 3
              UpdType = utYes
              Value = 1
              OldValor = 0
            end
            object CGAtivo1: TdmkCheckGroup
              Left = 284
              Top = 0
              Width = 285
              Height = 61
              Caption = 'Ativo:'
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Ativo'
                'Inativo')
              TabOrder = 4
              UpdType = utYes
              Value = 1
              OldValor = 0
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Uso terceiros'
        ImageIndex = 2
      end
      object TabSheet5: TTabSheet
        Caption = 'Consumo Setor'
        ImageIndex = 4
      end
      object TabSheet6: TTabSheet
        Caption = 'Estoque em... (movimento)'
        ImageIndex = 5
        object Panel16: TPanel
          Left = 0
          Top = 0
          Width = 1157
          Height = 267
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object RGFiltro5: TRadioGroup
            Left = 8
            Top = 8
            Width = 253
            Height = 105
            Caption = ' Filtro: '
            ItemIndex = 0
            Items.Strings = (
              'Sem filtro'
              'Somente com estoque positivo'
              'Somente com movimento '
              'Com movimento ou com estoque positivo')
            TabOrder = 0
          end
          object CkNovaForma5: TCheckBox
            Left = 20
            Top = 124
            Width = 241
            Height = 17
            Caption = 'Nova forma de gerar (2014-07-02)'
            Enabled = False
            TabOrder = 1
            OnClick = CkNovaForma5Click
          end
          object CkNovaForma6: TCheckBox
            Left = 20
            Top = 140
            Width = 241
            Height = 17
            Caption = 'Nova forma de gerar (2018-08-21)'
            Checked = True
            State = cbChecked
            TabOrder = 2
            OnClick = CkNovaForma6Click
          end
          object CkCustoPadrao5: TCheckBox
            Left = 20
            Top = 172
            Width = 161
            Height = 17
            Caption = 'Custo padr'#227'o.'
            TabOrder = 3
          end
        end
      end
      object TabSheet7: TTabSheet
        Caption = ' Lan'#231'amentos '
        ImageIndex = 6
        object Panel12: TPanel
          Left = 0
          Top = 0
          Width = 1157
          Height = 267
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitWidth = 1000
          ExplicitHeight = 252
          object DBGLancamentos: TDBGrid
            Left = 0
            Top = 0
            Width = 1000
            Height = 252
            Align = alClient
            DataSource = DsPQx
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGLancamentosDblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'DataX'
                Title.Caption = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Peso'
                Title.Caption = 'Quantidade'
                Width = 84
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Width = 84
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CLI_ORIG'
                Title.Caption = 'Quem Consumiu'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CLI_DEST'
                Title.Caption = 'Debitado'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Tipo'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OrigemCodi'
                Title.Caption = 'C'#243'digp'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OrigemCtrl'
                Title.Caption = 'Controle'
                Visible = True
              end>
          end
        end
      end
      object TabSheet8: TTabSheet
        Caption = ' Orf'#227'os '
        ImageIndex = 7
        object PnOrfaosNao: TPanel
          Left = 0
          Top = 0
          Width = 1157
          Height = 41
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = 'N'#227'o h'#225' cadastros orf'#227'os de PQ.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          Visible = False
        end
        object PnOrfaosSim: TPanel
          Left = 0
          Top = 41
          Width = 1157
          Height = 226
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object Panel21: TPanel
            Left = 0
            Top = 0
            Width = 1157
            Height = 226
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel21'
            TabOrder = 0
            object DBGrid3: TDBGrid
              Left = 0
              Top = 44
              Width = 1157
              Height = 182
              Align = alClient
              DataSource = DsOrfaos
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = DBGrid3DblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Empresa'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CliOrig'
                  Width = 79
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CI'
                  Title.Caption = 'CI (cad. estq)'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'C'#243'digo UC'
                  Visible = True
                end
                item
                  Expanded = False
                  Title.Caption = 'Nivel 1 Grade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Insumo'
                  Title.Caption = 'C'#243'd UC Estq'
                  Width = 71
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PQ'
                  Title.Caption = 'Nome Insumo'
                  Width = 232
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_GG1'
                  Title.Caption = 'Nome Grade'
                  Width = 232
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_GGXNiv2'
                  Title.Caption = 'Tipo de cadastrpo'
                  Width = 140
                  Visible = True
                end>
            end
            object Panel20: TPanel
              Left = 0
              Top = 0
              Width = 1157
              Height = 44
              Align = alTop
              BevelOuter = bvNone
              Caption = 'Panel20'
              TabOrder = 1
              object Panel22: TPanel
                Left = 0
                Top = 0
                Width = 133
                Height = 44
                Align = alLeft
                BevelOuter = bvNone
                Caption = 'Panel22'
                TabOrder = 0
                object BtOrfaos: TBitBtn
                  Tag = 10010
                  Left = 8
                  Top = 1
                  Width = 120
                  Height = 40
                  Caption = '&A'#231#227'o'
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtOrfaosClick
                end
              end
              object Panel27: TPanel
                Left = 133
                Top = 0
                Width = 1024
                Height = 44
                Align = alClient
                BevelOuter = bvNone
                Caption = 'Panel27'
                TabOrder = 1
                object Panel13: TPanel
                  Left = 0
                  Top = 21
                  Width = 1024
                  Height = 21
                  Align = alTop
                  Alignment = taLeftJustify
                  BevelOuter = bvNone
                  Caption = 
                    'Duplo clique no item, corrige cadastro de uso e consumo e abre a' +
                    ' janela do cadastro.'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clGreen
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 0
                end
                object Panel14: TPanel
                  Left = 0
                  Top = 0
                  Width = 1024
                  Height = 21
                  Align = alTop
                  Alignment = taLeftJustify
                  BevelOuter = bvNone
                  Caption = 
                    'CUIDADO! Cadastros orf'#227'os podem comprometer a integridade dos re' +
                    'lat'#243'rios!'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 1
                end
              end
            end
          end
        end
      end
      object TabSheet9: TTabSheet
        Caption = ' Erros de estoque '
        ImageIndex = 8
      end
      object TabSheet10: TTabSheet
        Caption = 'Estoque por NF CC'
        ImageIndex = 9
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1157
          Height = 267
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 1002
          ExplicitHeight = 255
          object Ck009_Individual: TCheckBox
            Left = 12
            Top = 8
            Width = 181
            Height = 17
            Caption = 'Mostrar estoque de cada NF CC.'
            Checked = True
            State = cbChecked
            TabOrder = 0
          end
        end
      end
      object TabSheet11: TTabSheet
        Caption = 'Lista pesagem'
        ImageIndex = 10
      end
    end
    object Panel23: TPanel
      Left = 0
      Top = 295
      Width = 1165
      Height = 253
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object PnSetores: TPanel
        Left = 945
        Top = 0
        Width = 220
        Height = 253
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object DBGrid2: TDBGrid
          Left = 0
          Top = 69
          Width = 220
          Height = 184
          Align = alClient
          DataSource = DsSE
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGrid2CellClick
          OnDrawColumnCell = DBGrid2DrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = ' X'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'd'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Nome setor'
              Width = 129
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 220
          Height = 69
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label8: TLabel
            Left = 0
            Top = 0
            Width = 220
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Setores:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ExplicitWidth = 39
          end
          object BtTodosSetores: TBitBtn
            Tag = 127
            Left = 5
            Top = 24
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Todos'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtTodosSetoresClick
          end
          object BtNenhumSetor: TBitBtn
            Tag = 128
            Left = 117
            Top = 24
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Nenhum'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtNenhumSetorClick
          end
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 581
        Height = 253
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Panel9: TPanel
          Left = 0
          Top = 96
          Width = 581
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object LaCI1: TLabel
            Left = 4
            Top = 4
            Width = 114
            Height = 13
            Caption = 'Cliente interno debitado:'
          end
          object Label4: TLabel
            Left = 348
            Top = 4
            Width = 55
            Height = 13
            Caption = 'Data inicial:'
          end
          object Label5: TLabel
            Left = 465
            Top = 4
            Width = 48
            Height = 13
            Caption = 'Data final:'
          end
          object EdCI1: TdmkEditCB
            Left = 4
            Top = 20
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCI1
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCI1: TdmkDBLookupComboBox
            Left = 72
            Top = 20
            Width = 269
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECI'
            ListSource = DsCI1
            TabOrder = 1
            dmkEditCB = EdCI1
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object TPIni: TdmkEditDateTimePicker
            Left = 348
            Top = 20
            Width = 112
            Height = 21
            Date = 37670.000000000000000000
            Time = 0.521748657396528900
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPFim: TdmkEditDateTimePicker
            Left = 465
            Top = 20
            Width = 112
            Height = 21
            Date = 37670.000000000000000000
            Time = 0.521748657396528900
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 581
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LaPQ: TLabel
            Left = 4
            Top = 8
            Width = 37
            Height = 13
            Caption = 'Insumo:'
          end
          object EdPQ: TdmkEditCB
            Left = 48
            Top = 4
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPQ
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBPQ: TdmkDBLookupComboBox
            Left = 116
            Top = 4
            Width = 457
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPQ
            TabOrder = 1
            dmkEditCB = EdPQ
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object Panel10: TPanel
          Left = 0
          Top = 26
          Width = 581
          Height = 27
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object LaCI: TLabel
            Left = 4
            Top = 8
            Width = 139
            Height = 13
            Caption = 'Cliente interno que consumiu:'
          end
          object EdCI: TdmkEditCB
            Left = 152
            Top = 4
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCI
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCI: TdmkDBLookupComboBox
            Left = 220
            Top = 4
            Width = 353
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECI'
            ListSource = DsCI
            TabOrder = 1
            dmkEditCB = EdCI
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object Panel6: TPanel
          Left = 0
          Top = 144
          Width = 581
          Height = 84
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 3
          object RGAgrupa: TRadioGroup
            Left = 0
            Top = 0
            Width = 92
            Height = 84
            Align = alLeft
            Caption = ' Agrupamentos: '
            ItemIndex = 1
            Items.Strings = (
              '0'
              '1'
              '2')
            TabOrder = 0
          end
          object RGOrdem1: TRadioGroup
            Left = 92
            Top = 0
            Width = 92
            Height = 84
            Align = alLeft
            Caption = ' Ordem 1: '
            ItemIndex = 0
            Items.Strings = (
              'Cliente'
              'Setor'
              'Insumo'
              'Fornecedor')
            TabOrder = 1
          end
          object RGOrdem3: TRadioGroup
            Left = 276
            Top = 0
            Width = 92
            Height = 84
            Align = alLeft
            Caption = ' Ordem 3: '
            ItemIndex = 2
            Items.Strings = (
              'Cliente'
              'Setor'
              'Insumo'
              'Fornecedor')
            TabOrder = 3
          end
          object RGOrdem2: TRadioGroup
            Left = 184
            Top = 0
            Width = 92
            Height = 84
            Align = alLeft
            Caption = ' Ordem 2: '
            ItemIndex = 1
            Items.Strings = (
              'Cliente'
              'Setor'
              'Insumo'
              'Fornecedor')
            TabOrder = 2
          end
          object PB1: TProgressBar
            Left = 376
            Top = 12
            Width = 205
            Height = 17
            TabOrder = 4
          end
          object PB2: TProgressBar
            Left = 376
            Top = 36
            Width = 205
            Height = 17
            TabOrder = 5
          end
          object PB3: TProgressBar
            Left = 376
            Top = 60
            Width = 205
            Height = 17
            TabOrder = 6
          end
        end
        object Panel24: TPanel
          Left = 0
          Top = 228
          Width = 581
          Height = 25
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 4
          object CkGrade: TCheckBox
            Left = 4
            Top = 4
            Width = 57
            Height = 17
            Caption = 'Grade.'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 0
          end
        end
        object Panel33: TPanel
          Left = 0
          Top = 53
          Width = 581
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 5
          object Label18: TLabel
            Left = 4
            Top = 8
            Width = 57
            Height = 13
            Caption = 'Fornecedor:'
          end
          object EdFornece0: TdmkEditCB
            Left = 72
            Top = 4
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBFornece0
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFornece0: TdmkDBLookupComboBox
            Left = 140
            Top = 4
            Width = 433
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NomeFO'
            ListSource = DsFornece
            TabOrder = 1
            dmkEditCB = EdFornece0
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
      object Panel3: TPanel
        Left = 581
        Top = 0
        Width = 364
        Height = 253
        Align = alLeft
        TabOrder = 2
        object CGTipoCad: TdmkCheckGroup
          Left = 1
          Top = 70
          Width = 362
          Height = 182
          Align = alClient
          Caption = ' Tipo de Cadastro: '
          Columns = 3
          Items.Strings = (
            'FmPrincipal.VAR_TipoCadPQ')
          TabOrder = 0
          UpdType = utYes
          Value = 0
          OldValor = 0
        end
        object Panel4: TPanel
          Left = 1
          Top = 1
          Width = 362
          Height = 69
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label1: TLabel
            Left = 0
            Top = 0
            Width = 362
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Tipos de Cadastro'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ExplicitWidth = 86
          end
          object BtTodosTipos: TBitBtn
            Tag = 127
            Left = 5
            Top = 24
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Todos'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtTodosTiposClick
          end
          object BtNenhumTipo: TBitBtn
            Tag = 128
            Left = 129
            Top = 24
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Nenhum'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtNenhumTipoClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1165
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1117
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1069
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 398
        Height = 32
        Caption = 'Relat'#243'rios de Produtos Qu'#237'micos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 398
        Height = 32
        Caption = 'Relat'#243'rios de Produtos Qu'#237'micos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 398
        Height = 32
        Caption = 'Relat'#243'rios de Produtos Qu'#237'micos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 596
    Width = 1165
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel17: TPanel
      Left = 2
      Top = 15
      Width = 1161
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 640
    Width = 1165
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel18: TPanel
      Left = 2
      Top = 15
      Width = 1161
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaEmpresa: TLabel
        Left = 152
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object PnSaiDesis: TPanel
        Left = 1016
        Top = 0
        Width = 145
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object EdEmpresa: TdmkEditCB
        Left = 152
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 208
        Top = 16
        Width = 437
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 3
        TabStop = False
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object QrEstq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ind.Tipo=0 THEN ind.RazaoSocial'
      'ELSE ind.Nome END NOMEFO, '
      'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECI, pq_.Nome NOMEPQ, pcl.*,'
      'lse.Nome NOMESE'
      'FROM pqcli pcl'
      'LEFT JOIN entidades    cli ON cli.Codigo=pcl.CI'
      'LEFT JOIN pq           pq_ ON pq_.Codigo=pcl.PQ'
      'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor'
      'LEFT JOIN entidades    ind ON ind.Codigo=pq_.IQ'
      'WHERE pq_.Ativo=2'
      'AND pq_.Setor in (5,2,3,4)'
      'ORDER BY NOMECI, NOMESE, NOMEPQ')
    Left = 4
    Top = 136
    object QrEstqNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrEstqNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrEstqControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEstqPQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrEstqCI: TIntegerField
      FieldName = 'CI'
    end
    object QrEstqMinEstq: TFloatField
      FieldName = 'MinEstq'
    end
    object QrEstqLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEstqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEstqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEstqUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEstqUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEstqCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
    end
    object QrEstqMoedaPadrao: TSmallintField
      FieldName = 'MoedaPadrao'
      Required = True
    end
    object QrEstqEstSegur: TIntegerField
      FieldName = 'EstSegur'
    end
    object QrEstqPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrEstqValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrEstqCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrEstqNOMESE: TWideStringField
      FieldName = 'NOMESE'
    end
    object QrEstqNOMEFO: TWideStringField
      FieldName = 'NOMEFO'
      Size = 100
    end
    object QrEstqCodProprio: TWideStringField
      FieldName = 'CodProprio'
    end
    object QrEstqCustoEstoquePadrao: TFloatField
      FieldName = 'CustoEstoquePadrao'
    end
    object QrEstqKgUsoDd: TFloatField
      FieldName = 'KgUsoDd'
    end
    object QrEstqDiasEsrq: TWideStringField
      FieldName = 'DiasEstq'
      Size = 255
    end
    object QrEstqKgEstqSegur: TFloatField
      FieldName = 'KgEstqSegur'
    end
    object QrEstqDdEstq: TFloatField
      FieldName = 'DdEstq'
    end
    object QrEstqNCM: TWideStringField
      FieldName = 'NCM'
    end
    object QrEstqDiasProd: TFloatField
      FieldName = 'DiasProd'
    end
    object QrEstqKgDdProd: TFloatField
      FieldName = 'KgDdProd'
    end
  end
  object DsEstq: TDataSource
    DataSet = QrEstq
    Left = 4
    Top = 184
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 744
    Top = 8
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 772
    Top = 8
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq '
      'WHERE Ativo NOT IN (0,3)'
      'AND Codigo > 0'
      'ORDER BY Nome')
    Left = 744
    Top = 36
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 772
    Top = 36
  end
  object DsCI1: TDataSource
    DataSet = QrCI
    Left = 716
    Top = 8
  end
  object QrSE: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT se.Codigo, se.Nome '
      'FROM listasetores se'
      'ORDER BY se.Nome')
    Left = 592
    Top = 160
    object QrSECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSENome: TWideStringField
      FieldName = 'Nome'
    end
    object QrSEAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsSE: TDataSource
    DataSet = QrSE
    Left = 592
    Top = 208
  end
  object QrSE1_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM setsetores'
      'WHERE Setado=1')
    Left = 592
    Top = 256
    object QrSE1_Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSE1_Nome: TWideStringField
      FieldName = 'Nome'
    end
    object QrSE1_Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsCI2B: TDataSource
    DataSet = QrCI
    Left = 660
    Top = 8
  end
  object QrUsoT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ori.Tipo=0 THEN ori.RazaoSocial'
      'ELSE ori.Nome END NOMEORI, pqx.*, lse.Nome NOMESE ,'
      'pq_.Nome NOMEPQ, CASE WHEN iq_.Tipo = 2 THEN '
      'iq_.RazaoSocial ELSE iq_.Nome END NOMEFO'
      'FROM pqx pqx'
      'LEFT JOIN entidades    ori ON ori.Codigo=pqx.CliOrig'
      'LEFT JOIN pq           pq_ ON pq_.Codigo=pqx.Insumo'
      'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor'
      'LEFT JOIN entidades    iq_ ON iq_.Codigo=pq_.IQ'
      'WHERE pqx.Tipo=110'
      'AND pqx.CliDest=2'
      'AND pqx.CliOrig<>2'
      'AND pq_.Setor in (2,3,4)'
      'ORDER BY NOMEORI')
    Left = 152
    Top = 8
    object QrUsoTNOMEORI: TWideStringField
      FieldName = 'NOMEORI'
      Size = 100
    end
    object QrUsoTDataX: TDateField
      FieldName = 'DataX'
    end
    object QrUsoTTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrUsoTCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrUsoTCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrUsoTInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrUsoTPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrUsoTValor: TFloatField
      FieldName = 'Valor'
    end
    object QrUsoTOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrUsoTOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrUsoTNOMESE: TWideStringField
      FieldName = 'NOMESE'
    end
    object QrUsoTNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrUsoTNOMEFO: TWideStringField
      FieldName = 'NOMEFO'
      Size = 100
    end
  end
  object QrPedComprar: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM pqurec rec'
      'WHERE Nivel=2'
      'ORDER BY NomeIQ, NOMEPQ')
    Left = 272
    Top = 526
    object QrPedComprarInsumM2PesoKg: TFloatField
      FieldName = 'InsumM2PesoKg'
    end
    object QrPedComprarCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPedComprarControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPedComprarInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPedComprarPesoEstq: TFloatField
      FieldName = 'PesoEstq'
      Required = True
    end
    object QrPedComprarPesoUso: TFloatField
      FieldName = 'PesoUso'
      Required = True
    end
    object QrPedComprarPesoFut: TFloatField
      FieldName = 'PesoFut'
      Required = True
    end
    object QrPedComprarPesoNeed: TFloatField
      FieldName = 'PesoNeed'
      Required = True
    end
    object QrPedComprarCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrPedComprarRepet: TFloatField
      FieldName = 'Repet'
      Required = True
    end
    object QrPedComprarNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Required = True
      Size = 100
    end
    object QrPedComprarNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Required = True
      Size = 100
    end
    object QrPedComprarLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrPedComprarDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPedComprarDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPedComprarUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrPedComprarUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrPedComprarAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPedComprarAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPedComprarAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPedComprarAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPedComprarNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPedComprarKgLiqEmb: TFloatField
      FieldName = 'KgLiqEmb'
    end
    object QrPedComprarPesoCompra: TFloatField
      FieldName = 'PesoCompra'
    end
    object QrPedComprarIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPedComprarNOMEIQ: TWideStringField
      FieldName = 'NOMEIQ'
      Size = 100
    end
  end
  object Qr3Bal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor'
      'FROM pqx'
      'WHERE Tipo=-1'
      'AND DataX BETWEEN :P0 AND :P1'
      'AND CliOrig=:P2'
      'GROUP BY Insumo'
      '')
    Left = 620
    Top = 496
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr3BalInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object Qr3BalPeso: TFloatField
      FieldName = 'Peso'
    end
    object Qr3BalValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object Qr3Out: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor'
      'FROM pqx'
      'WHERE Tipo>100'
      'AND DataX BETWEEN :P0 AND :P1'
      'AND CliOrig=:P2'
      'AND Empresa=:P3'
      'GROUP BY Insumo'
      '')
    Left = 676
    Top = 496
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object Qr3OutInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object Qr3OutPeso: TFloatField
      FieldName = 'Peso'
    end
    object Qr3OutValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object Qr3Inn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor'
      'FROM pqx'
      'WHERE Tipo>0 AND Tipo<=100 '
      'AND DataX BETWEEN :P0 AND :P1'
      'AND CliOrig=:P2'
      'AND Empresa=:P3'
      'GROUP BY Insumo')
    Left = 648
    Top = 496
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object Qr3InnInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object Qr3InnPeso: TFloatField
      FieldName = 'Peso'
    end
    object Qr3InnValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object Qr3PQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Insumo, pq_.Nome NOMEPQ, '
      'CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial'
      'ELSE frn.Nome END NOMEIQ, lse.Nome NOMESETOR'
      'FROM pqx pqx '
      'LEFT JOIN pq           pq_ ON pq_.Codigo=pqx.Insumo'
      'LEFT JOIN entidades    frn ON frn.Codigo=pq_.IQ'
      'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor')
    Left = 704
    Top = 496
    object Qr3PQInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object Qr3PQNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object Qr3PQINNPESO: TFloatField
      FieldKind = fkLookup
      FieldName = 'INNPESO'
      LookupDataSet = Qr3Inn
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Peso'
      KeyFields = 'Insumo'
      Lookup = True
    end
    object Qr3PQINNVALOR: TFloatField
      FieldKind = fkLookup
      FieldName = 'INNVALOR'
      LookupDataSet = Qr3Inn
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Valor'
      KeyFields = 'Insumo'
      Lookup = True
    end
    object Qr3PQOUTPESO: TFloatField
      FieldKind = fkLookup
      FieldName = 'OUTPESO'
      LookupDataSet = Qr3Out
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Peso'
      KeyFields = 'Insumo'
      Lookup = True
    end
    object Qr3PQOUTVALOR: TFloatField
      FieldKind = fkLookup
      FieldName = 'OUTVALOR'
      LookupDataSet = Qr3Out
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Valor'
      KeyFields = 'Insumo'
      Lookup = True
    end
    object Qr3PQNOMEIQ: TWideStringField
      FieldName = 'NOMEIQ'
      Size = 100
    end
    object Qr3PQNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object Qr3PQBALPESO: TFloatField
      FieldKind = fkLookup
      FieldName = 'BALPESO'
      LookupDataSet = Qr3Bal
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Peso'
      KeyFields = 'Insumo'
      Lookup = True
    end
    object Qr3PQBALVALOR: TFloatField
      FieldKind = fkLookup
      FieldName = 'BALVALOR'
      LookupDataSet = Qr3Bal
      LookupKeyFields = 'Insumo'
      LookupResultField = 'Valor'
      KeyFields = 'Insumo'
      Lookup = True
    end
  end
  object Qr4: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN cld.Tipo=0 THEN cld.RazaoSocial'
      
        'ELSE cld.Nome END NOMECLIDESTINO, CASE WHEN pqx.Tipo = 150 THEN ' +
        '4.0000'
      
        'ELSE emi.Setor END SETOR, CASE WHEN pqx.Tipo = 150 THEN '#39'ETE'#39'  E' +
        'LSE '
      'lse.Nome END NOMESETOR,'
      'CASE WHEN clo.Tipo=0 THEN clo.RazaoSocial'
      'ELSE clo.Nome END NOMECLIORIGEM, '
      'pq_.Nome NOMEPQ, -SUM(pqx.Peso) Peso, -SUM(pqx.Valor) Valor,'
      'SUM(pqx.Valor) / SUM(pqx.Peso) CUSTO_MEDIO, pqx.Insumo'
      'FROM emit emi'
      'LEFT JOIN pqx       pqx ON emi.Codigo=pqx.OrigemCodi'
      'LEFT JOIN pq        pq_ ON pq_.Codigo=pqx.Insumo'
      'LEFT JOIN entidades cld ON cld.Codigo=pqx.CliDest'
      'LEFT JOIN entidades clo ON clo.Codigo=pqx.CliOrig'
      'LEFT JOIN listasetores lse ON lse.Codigo=emi.Setor'
      'WHERE pqx.Tipo>100 /* 110 */'
      ''
      'AND pqx.DataX  BETWEEN "2007/03/01" AND "2007/03/31"'
      'GROUP BY emi.Setor, NOMEPQ, NOMECLIDESTINO, NOMECLIORIGEM'
      'ORDER BY emi.Setor, NOMEPQ, NOMECLIDESTINO, NOMECLIORIGEM')
    Left = 648
    Top = 160
    object Qr4NOMECLIDESTINO: TWideStringField
      FieldName = 'NOMECLIDESTINO'
      Size = 100
    end
    object Qr4NOMECLIORIGEM: TWideStringField
      FieldName = 'NOMECLIORIGEM'
      Size = 100
    end
    object Qr4NOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object Qr4Peso: TFloatField
      FieldName = 'Peso'
    end
    object Qr4Valor: TFloatField
      FieldName = 'Valor'
    end
    object Qr4CUSTO_MEDIO: TFloatField
      FieldName = 'CUSTO_MEDIO'
    end
    object Qr4NOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object Qr4Insumo: TIntegerField
      FieldName = 'Insumo'
    end
    object Qr4SETOR: TFloatField
      FieldName = 'SETOR'
    end
  end
  object QrMoviPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM movipq'
      '')
    Left = 620
    Top = 524
    object QrMoviPQInsumo: TIntegerField
      FieldName = 'Insumo'
      Origin = 'movipq.Insumo'
    end
    object QrMoviPQNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Origin = 'movipq.NomePQ'
      Size = 100
    end
    object QrMoviPQNomeFO: TWideStringField
      FieldName = 'NomeFO'
      Origin = 'movipq.NomeFO'
      Size = 100
    end
    object QrMoviPQNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Origin = 'movipq.NomeCI'
      Size = 100
    end
    object QrMoviPQNomeSE: TWideStringField
      FieldName = 'NomeSE'
      Origin = 'movipq.NomeSE'
      Size = 100
    end
    object QrMoviPQInnPes: TFloatField
      FieldName = 'InnPes'
      Origin = 'movipq.InnPes'
    end
    object QrMoviPQInnVal: TFloatField
      FieldName = 'InnVal'
      Origin = 'movipq.InnVal'
    end
    object QrMoviPQOutPes: TFloatField
      FieldName = 'OutPes'
      Origin = 'movipq.OutPes'
    end
    object QrMoviPQOutVal: TFloatField
      FieldName = 'OutVal'
      Origin = 'movipq.OutVal'
    end
    object QrMoviPQAntPes: TFloatField
      FieldName = 'AntPes'
      Origin = 'movipq.AntPes'
    end
    object QrMoviPQAntVal: TFloatField
      FieldName = 'AntVal'
      Origin = 'movipq.AntVal'
    end
    object QrMoviPQFimPes: TFloatField
      FieldName = 'FimPes'
      Origin = 'movipq.FimPes'
    end
    object QrMoviPQFimVal: TFloatField
      FieldName = 'FimVal'
      Origin = 'movipq.FimVal'
    end
    object QrMoviPQBalPes: TFloatField
      FieldName = 'BalPes'
      Origin = 'movipq.BalPes'
    end
    object QrMoviPQBalVal: TFloatField
      FieldName = 'BalVal'
      Origin = 'movipq.BalVal'
    end
  end
  object frxMoviPQ: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39716.814898726800000000
    ReportOptions.LastChange = 39716.814898726800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      '  case <VFR_ORD1> of '
      '    0: GH1.Condition := '#39'frxDsMoviPQ."NOMECI"'#39';'
      '    1: GH1.Condition := '#39'frxDsMoviPQ."NOMESE"'#39';'
      '    2: GH1.Condition := '#39'frxDsMoviPQ."NOMEPQ"'#39';'
      '    3: GH1.Condition := '#39'frxDsMoviPQ."NOMEFO"'#39';'
      '  end;              '
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      '  case <VFR_ORD2> of '
      '    0: GH2.Condition := '#39'frxDsMoviPQ."NOMECI"'#39';'
      '    1: GH2.Condition := '#39'frxDsMoviPQ."NOMESE"'#39';'
      '    2: GH2.Condition := '#39'frxDsMoviPQ."NOMEPQ"'#39';'
      '    3: GH2.Condition := '#39'frxDsMoviPQ."NOMEFO"'#39';'
      '  end;              '
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 648
    Top = 524
    Datasets = <
      item
        DataSet = frxDsMoviPQ
        DataSetName = 'frxDsMoviPQ'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 554.000000000000000000
          Top = 0.881879999999998800
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 4.440940000000001000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 174.000000000000000000
          Top = 16.881880000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 54.897650000000000000
        Top = 604.724800000000000000
        Width = 718.110700000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 541.779530000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 366.614410000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsMoviPQ
        DataSetName = 'frxDsMoviPQ'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984251968504000000
          Width = 75.590551181102400000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."InnVal"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559055120000000000
          Width = 352.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviPQ."NomePQ"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393700787402000000
          Width = 75.590551181102400000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."InnPes"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370078740158000000
          Width = 75.590551181102400000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."OutPes"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 634.960629921260000000
          Width = 75.590551181102400000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."OutVal"]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 25.559060000000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsMoviPQ."NomeSE"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 2.110079999999982000
          Width = 710.551591180000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 25.559060000000000000
        Top = 317.480520000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsMoviPQ."NomeFO"'
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Top = 1.196660000000008000
          Width = 695.433471180000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 457.323130000000000000
        Width = 718.110700000000000000
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 13.559060000000000000
          Top = 5.322510000000022000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 41.559060000000000000
          Top = 5.322510000000022000
          Width = 324.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984251968504000000
          Top = 5.322510000000022000
          Width = 75.590551181102400000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnVal">)]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393700787402000000
          Top = 5.322510000000022000
          Width = 75.590551181102400000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnPes">)]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370078740158000000
          Top = 5.322510000000022000
          Width = 75.590551181102400000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutPes">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 634.960629921260000000
          Top = 5.322510000000022000
          Width = 75.590551181102400000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutVal">)]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 408.189240000000000000
        Width = 718.110700000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 18.220470000000000000
          Top = 2.456399999999974000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 50.000000000000000000
          Top = 2.456399999999974000
          Width = 308.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984251968504000000
          Top = 2.456399999999974000
          Width = 75.590551181102400000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnVal">)]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393700787402000000
          Top = 2.456399999999974000
          Width = 75.590551181102400000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnPes">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370078740158000000
          Top = 2.456399999999974000
          Width = 75.590551181102400000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutPes">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 634.960629921260000000
          Top = 2.456399999999974000
          Width = 75.590551181102400000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutVal">)]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 36.000000000000000000
        Top = 544.252320000000000000
        Width = 718.110700000000000000
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 35.795300000000000000
          Top = 3.779530000000022000
          Width = 675.307050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984251968504000000
          Top = 5.070500000000038000
          Width = 76.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnVal">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393700787402000000
          Top = 5.070500000000038000
          Width = 76.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnPes">)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370078740158000000
          Top = 5.070500000000038000
          Width = 76.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutPes">)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 634.960629921260000000
          Top = 5.070500000000038000
          Width = 76.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutVal">)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 34.000000000000000000
          Top = 5.070500000000038000
          Width = 324.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 93.779530000000000000
        Top = 113.385900000000000000
        Width = 718.110700000000000000
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 26.834570000000000000
          Width = 701.543290000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 8.220470000000000000
          Top = 27.055040000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 6.661410000000000000
          Top = 3.779529999999994000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'MOVIMENTO DE INSUMOS QU'#205'MICOS ')
          ParentFont = False
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 71.779530000000010000
          Width = 705.322820000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 74.220470000000000000
          Top = 27.779530000000010000
          Width = 636.440940000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984251970000000000
          Top = 75.779530000000010000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Entrada R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559055120000000000
          Top = 75.779530000000010000
          Width = 352.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393700790000000000
          Top = 75.779530000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Entrada unit')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370078740000000000
          Top = 75.779530000000010000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sa'#237'da kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 634.960629920000000000
          Top = 75.779530000000010000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sa'#237'da R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 6.661410000000000000
          Top = 47.779530000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo:')
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 74.220470000000000000
          Top = 47.779530000000010000
          Width = 636.440940000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsMoviPQ: TfrxDBDataset
    UserName = 'frxDsMoviPQ'
    CloseDataSource = False
    DataSet = QrMoviPQ
    BCDToCurrency = False
    DataSetOptions = []
    Left = 676
    Top = 524
  end
  object frxEstoqueEm: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39716.814898726800000000
    ReportOptions.LastChange = 39716.814898726800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      '  case <VFR_ORD1> of '
      '    0: GH1.Condition := '#39'frxDsMoviPQ."NOMECI"'#39';'
      '    1: GH1.Condition := '#39'frxDsMoviPQ."NOMESE"'#39';'
      '    2: GH1.Condition := '#39'frxDsMoviPQ."NOMEPQ"'#39';'
      '    3: GH1.Condition := '#39'frxDsMoviPQ."NOMEFO"'#39';'
      '  end;              '
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      '  case <VFR_ORD2> of '
      '    0: GH2.Condition := '#39'frxDsMoviPQ."NOMECI"'#39';'
      '    1: GH2.Condition := '#39'frxDsMoviPQ."NOMESE"'#39';'
      '    2: GH2.Condition := '#39'frxDsMoviPQ."NOMEPQ"'#39';'
      '    3: GH2.Condition := '#39'frxDsMoviPQ."NOMEFO"'#39';'
      '  end;              '
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 704
    Top = 524
    Datasets = <
      item
      end
      item
        DataSet = frxDsMoviPQ
        DataSetName = 'frxDsMoviPQ'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 554.000000000000000000
          Top = 0.881879999999998800
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 4.440940000000001000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 174.000000000000000000
          Top = 16.881880000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 40.771180000000000000
        Top = 563.149970000000000000
        Width = 718.110700000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 541.779530000000000000
          Top = 7.653059999999982000
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 351.496290000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsMoviPQ
        DataSetName = 'frxDsMoviPQ'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464461970000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."InnVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Width = 94.488152360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviPQ."NomePQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992030790000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."InnPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716398740000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."OutPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188829920000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."OutVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740211180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'AntVal'
          DataSet = frxDsMoviPQ
          DataSetName = 'frxDsMoviPQ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."AntVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviPQ
          DataSetName = 'frxDsMoviPQ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."AntPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviPQ
          DataSetName = 'frxDsMoviPQ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."FimPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 657.638171180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'FimVal'
          DataSet = frxDsMoviPQ
          DataSetName = 'frxDsMoviPQ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."FimVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'BalPes'
          DataSet = frxDsMoviPQ
          DataSetName = 'frxDsMoviPQ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."BalPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913681180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'BalVal'
          DataSet = frxDsMoviPQ
          DataSetName = 'frxDsMoviPQ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."BalVal"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.110080000000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsMoviPQ."NomeSE"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 2.110079999999982000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 19.196660000000000000
        Top = 309.921460000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsMoviPQ."NomeFO"'
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Top = 1.196660000000008000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 434.645950000000000000
        Width = 718.110700000000000000
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = -1.559060000000000000
          Top = 3.779527560000020000
          Width = 24.220470000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677155590000000000
          Top = 3.779527560000020000
          Width = 71.810947950000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464461970000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992030790000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716398740000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188829920000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740211180000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."AntVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."AntPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."FimPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 657.638171180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."FimVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 3.779529999999965000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."BalPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913681180000000000
          Top = 3.779529999999965000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."BalVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 389.291590000000000000
        Width = 718.110700000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = -0.677180000000000000
          Top = 3.779527560000020000
          Width = 24.220470000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677155590000000000
          Top = 3.779527560000020000
          Width = 71.810947950000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464461970000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992030790000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716398740000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188829920000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740211180000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."AntVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."AntPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."FimPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 657.638171180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."FimVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."BalPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913681180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."BalVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 517.795610000000000000
        Width = 718.110700000000000000
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 1.779530000000000000
          Top = 3.023622049999972000
          Width = 716.881880000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464461970000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992030790000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716398740000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188829920000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779527560000020000
          Width = 94.488152360000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL  ')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740211180000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."AntVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."AntPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."FimPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 657.638171180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."FimVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."BalPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913681180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."BalVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 92.598425200000000000
        Top = 113.385900000000000000
        Width = 718.110700000000000000
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 26.834570000000000000
          Width = 701.543290000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 8.220470000000000000
          Top = 27.055040000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 6.661410000000000000
          Top = 3.779529999999994000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'MOVIMENTO DE INSUMOS QU'#205'MICOS')
          ParentFont = False
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 71.779530000000010000
          Width = 712.881880000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 74.220470000000000000
          Top = 27.779530000000010000
          Width = 636.440940000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464461970000000000
          Top = 75.590551179999990000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Entrada R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590551179999990000
          Width = 94.488108430000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992030790000000000
          Top = 75.590551180000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtd Entrada')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716398740000000000
          Top = 75.590551180000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Sa'#237'da')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188829920000000000
          Top = 75.590551179999990000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sa'#237'da R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 6.661410000000000000
          Top = 47.779530000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo:')
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 74.220470000000000000
          Top = 47.779530000000010000
          Width = 636.440940000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740211180000000000
          Top = 75.590551179999990000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Anterior R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 75.590551180000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtd Anterior')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 75.590600000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Final')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 657.638171180000000000
          Top = 75.590600000000010000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Final R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 75.590600000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Ajuste')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913681180000000000
          Top = 75.590600000000010000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ajuste R$')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxEstq1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    OldStyleProgress = True
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MePesoOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Band4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <frxDsEstq."Peso"> < 0 then'
      '  begin              '
      '    MePeso.Font.Color       := clRed;'
      '    MeCodProprio.Font.Color := clRed;  '
      '    MePQ.Font.Color         := clRed;  '
      '    MeNomePQ.Font.Color     := clRed;                        '
      '  end else'
      '  begin              '
      '    MePeso.Font.Color       := clBlack;'
      '    MeCodProprio.Font.Color := clBlack;  '
      '    MePQ.Font.Color         := clBlack;  '
      '    MeNomePQ.Font.Color     := clBlack;                        '
      '  end;            '
      'end;'
      ''
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      ''
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      '  //'
      
        '  MeObservacao.Visible := <VARF_OBSERVACAO> <> '#39#39';              ' +
        '                                         '
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 4
    Top = 232
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 593.386210000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050880
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 158.740250240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 113.385734020000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 113.385734020000000000
          Width = 613.763760000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 0.928880000000000000
          Top = 98.267604250000000000
          Width = 678.834880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_AMOSTRAS]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 143.621974020000000000
          Width = 313.700787401574800000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866151500000000000
          Top = 143.621974020000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 514.015811500000000000
          Top = 143.621974020000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165471500000000000
          Top = 143.621974020000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo Unit')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 83.149506220000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 83.149506220000000000
          Width = 614.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PQNOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE DE USO E CONSUMO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_HORA]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 143.622140000000000000
          Width = 52.913393150000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd. CI')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Top = 143.622140000000000000
          Width = 64.251983150000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000000000
          Width = 351.496062990000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866136850000000000
          Top = 3.779527560000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 514.015738270000000000
          Top = 3.779527560000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165339690000000000
          Top = 3.779527560000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."Valor">) / SUM' +
              '(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'Band4OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
        RowCount = 0
        object MeNomePQ: TfrxMemoView
          AllowVectorExport = True
          Left = 49.133855830000000000
          Width = 264.566929130000000000
          Height = 15.118110240000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstq."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePeso: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866156380000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'MePesoOnBeforePrint'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 514.015738270000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165339690000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsEstq."Peso"> <> 0,<frxDsEstq."Valor"> / <frxDsEstq."P' +
              'eso">,0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePQ: TfrxMemoView
          AllowVectorExport = True
          Width = 49.133858267716540000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstq."PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCodProprio: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 52.913393150000000000
          Height = 15.118110240000000000
          DataField = 'CodProprio'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstq."CodProprio"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Width = 64.251983150000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstq."NCM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866136850000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 514.015738270000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165339690000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."Valor">) / SUM' +
              '(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 54.456710000000000000
          Top = 3.779530000000000000
          Width = 351.496062992126000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstq."NOMECI"'
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstq."NOMEPQ"'
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000020000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 54.220470000000000000
          Top = 3.779527560000000000
          Width = 351.496062992126000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866136850000000000
          Top = 3.779527560000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 514.015738270000000000
          Top = 3.779527560000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165339690000000000
          Top = 3.779527560000000000
          Width = 83.149606300000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."Valor">) / SUM' +
              '(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 548.031850000000000000
        Width = 680.315400000000000000
        Stretched = True
        object MeObservacao: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[VARF_OBSERVACAO]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsEstq: TfrxDBDataset
    UserName = 'frxDsEstq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMECI=NOMECI'
      'NOMEPQ=NOMEPQ'
      'Controle=Controle'
      'PQ=PQ'
      'CI=CI'
      'MinEstq=MinEstq'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'CustoPadrao=CustoPadrao'
      'MoedaPadrao=MoedaPadrao'
      'EstSegur=EstSegur'
      'Peso=Peso'
      'Valor=Valor'
      'Custo=Custo'
      'NOMESE=NOMESE'
      'NOMEFO=NOMEFO'
      'CodProprio=CodProprio'
      'CustoEstoquePadrao=CustoEstoquePadrao'
      'KgUsoDd=KgUsoDd'
      'DiasEstq=DiasEstq'
      'KgEstqSegur=KgEstqSegur'
      'DdEstq=DdEstq'
      'NCM=NCM'
      'DiasProd=DiasProd'
      'KgDdProd=KgDdProd')
    DataSet = QrEstq
    BCDToCurrency = False
    DataSetOptions = []
    Top = 276
  end
  object frxUsoT: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 39717.458486319400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      
        '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsUsoT."NOMEORI"'#39 +
        ';'
      '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsUsoT."NOMESE"'#39';'
      '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsUsoT."NOMEPQ"'#39';'
      '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsUsoT."NOMEFO"'#39';'
      ''
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      
        '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsUsoT."NOMEORI"'#39 +
        ';'
      '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsUsoT."NOMESE"'#39';'
      '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsUsoT."NOMEPQ"'#39';'
      '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsUsoT."NOMEFO"'#39';'
      '    '
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 180
    Top = 8
    Datasets = <
      item
        DataSet = frxDsUsoT
        DataSetName = 'frxDsUsoT'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 554.000000000000000000
          Top = 0.881879999999998800
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 5.779530000000000000
          Top = 0.661410000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 177.779530000000000000
          Top = 21.102350000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 36.000000000000000000
        Top = 608.504330000000000000
        Width = 718.110700000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 541.779530000000000000
          Top = 7.653059999999982000
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 120.614100000000000000
        Top = 113.385900000000000000
        Width = 718.110700000000000000
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 10.614099999999990000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'USO DE INSUMOS QU'#205'MICOS DE TERCEIROS')
          ParentFont = False
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = -6.000000000000000000
          Top = 30.614099999999990000
          Width = 728.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Left = -6.000000000000000000
          Top = 98.614099999999990000
          Width = 728.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 74.614099999999990000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente:')
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 74.614099999999990000
          Width = 328.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 54.614099999999990000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 54.614099999999990000
          Width = 644.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 394.000000000000000000
          Top = 74.614099999999990000
          Width = 316.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_AMOSTRAS]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 102.614100000000000000
          Width = 432.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 438.000000000000000000
          Top = 102.614100000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 530.000000000000000000
          Top = 102.614100000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 622.000000000000000000
          Top = 102.614100000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo unit')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 34.614099999999990000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Insumo:')
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 34.614099999999990000
          Width = 652.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PQNOME]')
          ParentFont = False
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 517.795610000000000000
        Width = 718.110700000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 4.566479999999956000
          Width = 408.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUsoT."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134141420000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUsoT."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842802840000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsUsoT."Valor">) / SUM(<frxDsUsoT."Peso">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 377.953000000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsUsoT
        DataSetName = 'frxDsUsoT'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 58.000000000000000000
          Width = 380.000000000000000000
          Height = 18.000000000000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsUsoT
          DataSetName = 'frxDsUsoT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsUsoT."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425196850000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DataField = 'Peso'
          DataSet = frxDsUsoT
          DataSetName = 'frxDsUsoT'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUsoT."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133858267716000000
          Width = 90.708661417322800000
          Height = 18.000000000000000000
          DataField = 'Valor'
          DataSet = frxDsUsoT
          DataSetName = 'frxDsUsoT'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUsoT."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842519690000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DataSet = frxDsUsoT
          DataSetName = 'frxDsUsoT'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(<frxDsUsoT."Valor"> / <frxDsUsoT."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          DataField = 'Insumo'
          DataSet = frxDsUsoT
          DataSetName = 'frxDsUsoT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsUsoT."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 468.661720000000000000
        Width = 718.110700000000000000
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 3.779530000000022000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 54.456710000000000000
          Top = 3.779530000000022000
          Width = 380.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUsoT."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134141420000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUsoT."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842802840000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsUsoT."Valor">) / SUM(<frxDsUsoT."Peso">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 294.803340000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsUsoT."NOMEPQ"'
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 336.378170000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsUsoT."NOMESE"'
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 419.527830000000000000
        Width = 718.110700000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000020000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 54.220470000000000000
          Top = 3.779527560000020000
          Width = 380.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425196850000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUsoT."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133858270000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUsoT."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842519690000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsUsoT."Valor">) / SUM(<frxDsUsoT."Peso">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsUsoT: TfrxDBDataset
    UserName = 'frxDsUsoT'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEORI=NOMEORI'
      'DataX=DataX'
      'Tipo=Tipo'
      'CliOrig=CliOrig'
      'CliDest=CliDest'
      'Insumo=Insumo'
      'Peso=Peso'
      'Valor=Valor'
      'OrigemCodi=OrigemCodi'
      'OrigemCtrl=OrigemCtrl'
      'NOMESE=NOMESE'
      'NOMEPQ=NOMEPQ'
      'NOMEFO=NOMEFO')
    DataSet = QrUsoT
    BCDToCurrency = False
    DataSetOptions = []
    Left = 212
    Top = 8
  end
  object frxConSetor: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.509149479200000000
    ReportOptions.LastChange = 39717.509149479200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 652
    Top = 212
    Datasets = <
      item
        DataSet = frxDs4
        DataSetName = 'frxDs4'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 550.220470000000000000
          Top = 0.881879999999998800
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 5.779530000000000000
          Top = 4.440940000000001000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 177.779530000000000000
          Top = 21.102350000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 179.858380000000000000
          Top = 51.086580000000000000
          Width = 533.921150000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'CONSUMO DE INSUMOS POR SETOR')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 36.000000000000000000
        Top = 514.016080000000000000
        Width = 718.110700000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 534.000000000000000000
          Top = 2.897339999999985000
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 120.047244090000000000
        Top = 113.385900000000000000
        Width = 718.110700000000000000
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 10.614099999999990000
          Width = 720.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Left = -6.000000000000000000
          Top = 98.614099999999990000
          Width = 728.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 54.614099999999990000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente:')
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 54.614099999999990000
          Width = 328.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 34.614099999999990000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 34.614099999999990000
          Width = 644.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 394.000000000000000000
          Top = 54.614099999999990000
          Width = 316.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_AMOSTRAS]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 343.937007870000000000
          Top = 102.047244090000000000
          Width = 185.196850390000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133858270000000000
          Top = 102.047244090000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606299210000000000
          Top = 102.047244090000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 650.000000000000000000
          Top = 102.047244090000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo unit')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 14.614099999999990000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Insumo:')
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 14.614099999999990000
          Width = 652.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PQNOME]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 102.047244090000000000
          Width = 181.417322830000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            'Cliente interno (destino)')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417322830000000000
          Top = 102.047244090000000000
          Width = 162.519685040000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            'Cliente interno (origem)')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 74.614099999999990000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo:')
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 74.614099999999990000
          Width = 644.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 25.590290000000000000
        Top = 427.086890000000000000
        Width = 718.110700000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811023620000000000
          Top = 3.810760000000016000
          Width = 457.322834650000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133858270000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs4."Peso">)]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606299212598000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs4."Valor">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 650.078740157480100000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDs4."Valor">) / SUM(<frxDs4."Peso">))]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 336.378170000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDs4
        DataSetName = 'frxDs4'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 386.000000000000000000
          Width = 143.622047240000000000
          Height = 18.000000000000000000
          DataField = 'NOMEPQ'
          DataSet = frxDs4
          DataSetName = 'frxDs4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDs4."NOMEPQ"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133858270000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DataField = 'Peso'
          DataSet = frxDs4
          DataSetName = 'frxDs4'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs4."Peso"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606299210000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DataField = 'Valor'
          DataSet = frxDs4
          DataSetName = 'frxDs4'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs4."Valor"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 650.078740160000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DataField = 'CUSTO_MEDIO'
          DataSet = frxDs4
          DataSetName = 'frxDs4'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs4."CUSTO_MEDIO"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157477870000000000
          Width = 45.354330710000000000
          Height = 18.000000000000000000
          DataField = 'Insumo'
          DataSet = frxDs4
          DataSetName = 'frxDs4'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs4."Insumo"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Width = 181.417322830000000000
          Height = 18.000000000000000000
          DataField = 'NOMECLIDESTINO'
          DataSet = frxDs4
          DataSetName = 'frxDs4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDs4."NOMECLIDESTINO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417322830000000000
          Width = 158.740155040000000000
          Height = 18.000000000000000000
          DataField = 'NOMECLIORIGEM'
          DataSet = frxDs4
          DataSetName = 'frxDs4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDs4."NOMECLIORIGEM"]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 25.559057560000000000
        Top = 377.953000000000000000
        Width = 718.110700000000000000
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811023622047210000
          Top = 3.779527560000020000
          Width = 26.456692913385800000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 98.000000000000000000
          Top = 3.779527560000020000
          Width = 430.866141730000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDs4."NOMESETOR"]')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133858270000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs4."Peso">)]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606299212598000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs4."Valor">)]')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 650.078740157480100000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDs4."Valor">) / SUM(<frxDs4."Peso">))]')
          ParentFont = False
        end
      end
      object Band1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 294.803340000000000000
        Width = 718.110700000000000000
        Condition = 'frxDs4."NOMESETOR"'
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDs4."NOMESETOR"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDs4: TfrxDBDataset
    UserName = 'frxDs4'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMECLIDESTINO=NOMECLIDESTINO'
      'NOMECLIORIGEM=NOMECLIORIGEM'
      'NOMEPQ=NOMEPQ'
      'Peso=Peso'
      'Valor=Valor'
      'CUSTO_MEDIO=CUSTO_MEDIO'
      'NOMESETOR=NOMESETOR'
      'Insumo=Insumo'
      'SETOR=SETOR')
    DataSet = Qr4
    BCDToCurrency = False
    DataSetOptions = []
    Left = 656
    Top = 264
  end
  object QrPQx: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'IF(clo.Tipo=0, clo.RazaoSocial, clo.Nome) NO_CLI_ORIG,'
      'IF(cld.Tipo=0, cld.RazaoSocial, clo.Nome) NO_CLI_DEST,'
      'pq.Nome NO_PQ, pqx.*'
      'FROM pqx pqx'
      'LEFT JOIN pq pq ON pq.Codigo=pqx.Insumo'
      'LEFT JOIN entidades clo ON clo.Codigo=pqx.CliOrig'
      'LEFT JOIN entidades cld ON cld.Codigo=pqx.CliDest'
      'WHERE Insumo=86'
      'AND DataX BETWEEN :P0 and :P1'
      'ORDER BY NO_PQ, DataX, Tipo')
    Left = 284
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPQxNO_CLI_ORIG: TWideStringField
      FieldName = 'NO_CLI_ORIG'
      Size = 100
    end
    object QrPQxNO_CLI_DEST: TWideStringField
      FieldName = 'NO_CLI_DEST'
      Size = 100
    end
    object QrPQxNO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrPQxDataX: TDateField
      FieldName = 'DataX'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQxOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQxOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQxTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPQxCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQxCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPQxInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQxPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQxValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPQxAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQxAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQxRetorno: TSmallintField
      FieldName = 'Retorno'
    end
  end
  object DsPQx: TDataSource
    DataSet = QrPQx
    Left = 312
    Top = 12
  end
  object QrOrfaos: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrOrfaosAfterOpen
    SQL.Strings = (
      ' DROP TABLE IF EXISTS _pqcli_pqx_err_pqcli_;'
      'CREATE TABLE _pqcli_pqx_err_pqcli_'
      'SELECT DISTINCT Empresa, CI, PQ'
      'FROM pqcli'
      'WHERE PQ > 0'
      ';'
      'DROP TABLE IF EXISTS _pqcli_pqx_err_pqx_;'
      'CREATE TABLE _pqcli_pqx_err_pqx_'
      'SELECT DISTINCT Empresa, CliOrig, Insumo'
      'FROM pqx'
      'WHERE Insumo>0'
      ';'
      'SELECT pqx.Empresa, pqx.CliOrig, pcl.CI,'
      'pq.Codigo, gg1.Nivel1, pqx.Insumo, '
      'pq.Nome NO_PQ, gg1.Nome NO_GG1  '
      'FROM _pqcli_pqx_err_pqx_ pqx'
      'LEFT JOIN _pqcli_pqx_err_pqcli_ pqc'
      '  ON pqx.Empresa=pqc.Empresa'
      '  AND pqx.CliOrig=pqc.CI'
      '  AND pqx.Insumo=pqc.PQ'
      'LEFT JOIN pq pq ON pq.Codigo=pqx.Insumo'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pqx.Insumo '
      'LEFT JOIN pqcli pcl ON pcl.PQ=pqx.Insumo'
      '  AND pcl.CI=pqx.CliOrig '
      'WHERE pqc.PQ IS NULL'
      'OR pqx.Insumo IS NULL'
      'ORDER BY pqx.Empresa, pqx.CliOrig, pqx.Insumo'
      ';'
      ' ')
    Left = 36
    Top = 284
    object QrOrfaosEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrOrfaosCliOrig: TIntegerField
      DisplayLabel = 'Cli. Interno'
      FieldName = 'CliOrig'
    end
    object QrOrfaosCI: TIntegerField
      FieldName = 'CI'
    end
    object QrOrfaosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOrfaosNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrOrfaosInsumo: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'Insumo'
    end
    object QrOrfaosNO_PQ: TWideStringField
      DisplayLabel = 'Nome insumo'
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrOrfaosNO_GG1: TWideStringField
      DisplayLabel = 'Nome Reduzido'
      FieldName = 'NO_GG1'
      Size = 30
    end
    object QrOrfaosGGXNiv2: TIntegerField
      FieldName = 'GGXNiv2'
    end
    object QrOrfaosNO_GGXNiv2: TWideStringField
      FieldName = 'NO_GGXNiv2'
      Size = 60
    end
  end
  object DsOrfaos: TDataSource
    DataSet = QrOrfaos
    Left = 36
    Top = 332
  end
  object Qr3Bal_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor'
      'FROM pqx'
      'WHERE Tipo=0'
      'AND DataX BETWEEN :P0 AND :P1'
      'AND CliOrig=:P2'
      'GROUP BY Insumo'
      '')
    Left = 620
    Top = 452
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Insumo'
    end
    object FloatField1: TFloatField
      FieldName = 'Peso'
    end
    object FloatField2: TFloatField
      FieldName = 'Valor'
    end
  end
  object frxEstoqueEm_: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39716.814898726800000000
    ReportOptions.LastChange = 39716.814898726800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      '  case <VFR_ORD1> of '
      '    0: GH1.Condition := '#39'frxDsMoviPQ."NOMECI"'#39';'
      '    1: GH1.Condition := '#39'frxDsMoviPQ."NOMESE"'#39';'
      '    2: GH1.Condition := '#39'frxDsMoviPQ."NOMEPQ"'#39';'
      '    3: GH1.Condition := '#39'frxDsMoviPQ."NOMEFO"'#39';'
      '  end;              '
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      '  case <VFR_ORD2> of '
      '    0: GH2.Condition := '#39'frxDsMoviPQ."NOMECI"'#39';'
      '    1: GH2.Condition := '#39'frxDsMoviPQ."NOMESE"'#39';'
      '    2: GH2.Condition := '#39'frxDsMoviPQ."NOMEPQ"'#39';'
      '    3: GH2.Condition := '#39'frxDsMoviPQ."NOMEFO"'#39';'
      '  end;              '
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 708
    Top = 572
    Datasets = <
      item
      end
      item
        DataSet = frxDsMoviPQ
        DataSetName = 'frxDsMoviPQ'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 554.000000000000000000
          Top = 0.881879999999998800
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 4.440940000000001000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 174.000000000000000000
          Top = 16.881880000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 40.771180000000000000
        Top = 563.149970000000000000
        Width = 718.110700000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 541.779530000000000000
          Top = 7.653059999999982000
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 351.496290000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsMoviPQ
        DataSetName = 'frxDsMoviPQ'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464461970000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."InnVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Width = 94.488152360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviPQ."NomePQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992030790000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."InnPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716398740000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."OutPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188829920000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."OutVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740211180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'AntVal'
          DataSet = frxDsMoviPQ
          DataSetName = 'frxDsMoviPQ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."AntVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviPQ
          DataSetName = 'frxDsMoviPQ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."AntPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviPQ
          DataSetName = 'frxDsMoviPQ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."FimPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 657.638171180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'FimVal'
          DataSet = frxDsMoviPQ
          DataSetName = 'frxDsMoviPQ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."FimVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'BalPes'
          DataSet = frxDsMoviPQ
          DataSetName = 'frxDsMoviPQ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."BalPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913681180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'BalVal'
          DataSet = frxDsMoviPQ
          DataSetName = 'frxDsMoviPQ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ."BalVal"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.110080000000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsMoviPQ."NomeSE"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 2.110079999999982000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 19.196660000000000000
        Top = 309.921460000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsMoviPQ."NomeFO"'
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Top = 1.196660000000008000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 434.645950000000000000
        Width = 718.110700000000000000
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = -1.559060000000000000
          Top = 3.779527560000020000
          Width = 24.220470000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677155590000000000
          Top = 3.779527560000020000
          Width = 71.810947950000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464461970000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992030790000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716398740000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188829920000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740211180000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."AntVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."AntPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."FimPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 657.638171180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."FimVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 3.779529999999965000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."BalPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913681180000000000
          Top = 3.779529999999965000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."BalVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 389.291590000000000000
        Width = 718.110700000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = -0.677180000000000000
          Top = 3.779527560000020000
          Width = 24.220470000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677155590000000000
          Top = 3.779527560000020000
          Width = 71.810947950000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464461970000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992030790000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716398740000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188829920000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740211180000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."AntVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."AntPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."FimPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 657.638171180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."FimVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."BalPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913681180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."BalVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 517.795610000000000000
        Width = 718.110700000000000000
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 1.779530000000000000
          Top = 3.023622049999972000
          Width = 716.881880000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464461970000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992030790000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."InnPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716398740000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188829920000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."OutVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779527560000020000
          Width = 94.488152360000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL  ')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740211180000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."AntVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."AntPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."FimPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 657.638171180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."FimVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."BalPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913681180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ."BalVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 92.598425200000000000
        Top = 113.385900000000000000
        Width = 718.110700000000000000
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 26.834570000000000000
          Width = 701.543290000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 8.220470000000000000
          Top = 27.055040000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 6.661410000000000000
          Top = 3.779529999999994000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'MOVIMENTO DE INSUMOS QU'#205'MICOS')
          ParentFont = False
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 71.779530000000010000
          Width = 712.881880000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 74.220470000000000000
          Top = 27.779530000000010000
          Width = 636.440940000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464461970000000000
          Top = 75.590551179999990000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Entrada R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590551179999990000
          Width = 94.488108430000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992030790000000000
          Top = 75.590551180000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtd Entrada')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716398740000000000
          Top = 75.590551180000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Sa'#237'da')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188829920000000000
          Top = 75.590551179999990000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sa'#237'da R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 6.661410000000000000
          Top = 47.779530000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo:')
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 74.220470000000000000
          Top = 47.779530000000010000
          Width = 636.440940000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740211180000000000
          Top = 75.590551179999990000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Anterior R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 75.590551180000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtd Anterior')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 75.590600000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Final')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 657.638171180000000000
          Top = 75.590600000000010000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Final R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 75.590600000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Ajuste')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913681180000000000
          Top = 75.590600000000010000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ajuste R$')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxQUI_INSUM_002_009_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      
        '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsPQRSum."NOMECI"' +
        #39';'
      
        '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsPQRSum."NOMESE"' +
        #39';'
      
        '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsPQRSum."NOMEPQ"' +
        #39';'
      
        '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsPQRSum."NOMEFO"' +
        #39';'
      ''
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      
        '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsPQRSum."NOMECI"' +
        #39';'
      
        '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsPQRSum."NOMESE"' +
        #39';'
      
        '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsPQRSum."NOMEPQ"' +
        #39';'
      
        '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsPQRSum."NOMEFO"' +
        #39';'
      '    '
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 876
    Top = 332
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPQRIts
        DataSetName = 'frxDsPQRIts'
      end
      item
        DataSet = frxDsPQRSum
        DataSetName = 'frxDsPQRSum'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 158.740250240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 113.385734020000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 113.385734020000000000
          Width = 613.763760000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 0.928880000000000000
          Top = 98.267604250000000000
          Width = 678.834880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_AMOSTRAS]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 143.621974020000000000
          Width = 355.275556380000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188971500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Aberto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897691500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Aberto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606411500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ M'#233'dio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 83.149506220000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 83.149506220000000000
          Width = 614.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PQNOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE DE USO E CONSUMO - POR NFCC')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_HORA]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 143.622140000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd. CI')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527559054998000
          Width = 381.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQRSum."AbertoKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897901420000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQRSum."ValAberto">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606562840000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPQRSum."AbertoKg">)=0,0,(SUM(<frxDsPQRSum."ValAbe' +
              'rto">) / SUM(<frxDsPQRSum."AbertoKg">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPQRSum
        DataSetName = 'frxDsPQRSum'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 302.362170550000000000
          Height = 15.118110240000000000
          DataField = 'NomePQ'
          DataSet = frxDsPQRSum
          DataSetName = 'frxDsPQRSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQRSum."NomePQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188976380000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'AbertoKg'
          DataSet = frxDsPQRSum
          DataSetName = 'frxDsPQRSum'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRSum."AbertoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'ValAberto'
          DataSet = frxDsPQRSum
          DataSetName = 'frxDsPQRSum'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRSum."ValAberto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'PrecoMedio'
          DataSet = frxDsPQRSum
          DataSetName = 'frxDsPQRSum'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRSum."PrecoMedio"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsPQRSum
          DataSetName = 'frxDsPQRSum'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQRSum."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'CodProprio'
          DataSet = frxDsPQRSum
          DataSetName = 'frxDsPQRSum'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQRSum."CodProprio"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 3.779530000000022000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 54.456710000000000000
          Top = 3.779530000000022000
          Width = 353.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQRSum."AbertoKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897901420000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQRSum."ValAberto">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606562840000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPQRSum."AbertoKg">)=0,0,(SUM(<frxDsPQRSum."ValAbe' +
              'rto">) / SUM(<frxDsPQRSum."AbertoKg">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000020000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 54.220470000000000000
          Top = 3.779527560000020000
          Width = 353.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188956850000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQRSum."AbertoKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQRSum."ValAberto">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPQRSum."AbertoKg">)=0,0,(SUM(<frxDsPQRSum."ValAbe' +
              'rto">) / SUM(<frxDsPQRSum."AbertoKg">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 548.031849999999900000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrPQRSum: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPQRSumBeforeClose
    AfterScroll = QrPQRSumAfterScroll
    SQL.Strings = (
      'SELECT pqx.Insumo, pq.Nome NOMEPQ, '
      'pcl.CodProprio, pcl.CI,'
      '(SUM(pqx.Peso) + SUM(pqx.RetQtd)) AbertoKg,'
      'SUM(IF(pqx.Peso = 0, 0, pqx.Valor / pqx.Peso) '
      '* (pqx.Peso + pqx.RetQtd)) ValAberto,'
      'SUM(IF(pqx.Peso = 0, 0, pqx.Valor / pqx.Peso) '
      '* (pqx.Peso + pqx.RetQtd)) /  '
      '(SUM(pqx.Peso) + SUM(pqx.RetQtd)) PrecoMedio,'
      ''
      'CASE WHEN ind.Tipo=0 THEN ind.RazaoSocial'
      'ELSE ind.Nome END NOMEFO,'
      'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECI, '
      'lse.Nome NOMESE'
      ''
      'FROM pqcli pcl'
      'LEFT JOIN pqx pqx ON pcl.PQ=pqx.Insumo'
      'LEFT JOIN pq pq ON pq.Codigo=pcl.PQ'
      ''
      'LEFT JOIN entidades    cli ON cli.Codigo=pcl.CI'
      'LEFT JOIN listasetores lse ON lse.Codigo=pq.Setor'
      'LEFT JOIN entidades    ind ON ind.Codigo=pq.IQ'
      ''
      'WHERE pqx.CliDest=9'
      'AND pcl.CI=9'
      'AND pqx.Insumo>0'
      'AND pqx.Tipo=10'
      'AND pqx.RetQtd + pqx.Peso <> 0'
      'GROUP BY pqx.Insumo'
      ''
      '')
    Left = 192
    Top = 240
    object QrPQRSumInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQRSumNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Size = 50
    end
    object QrPQRSumAbertoKg: TFloatField
      FieldName = 'AbertoKg'
    end
    object QrPQRSumPrecoMedio: TFloatField
      FieldName = 'PrecoMedio'
    end
    object QrPQRSumCodProprio: TWideStringField
      FieldName = 'CodProprio'
    end
    object QrPQRSumValAberto: TFloatField
      FieldName = 'ValAberto'
    end
    object QrPQRSumNOMEFO: TWideStringField
      FieldName = 'NOMEFO'
      Size = 100
    end
    object QrPQRSumNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrPQRSumNOMESE: TWideStringField
      FieldName = 'NOMESE'
    end
    object QrPQRSumCI: TIntegerField
      FieldName = 'CI'
    end
  end
  object QrPQRIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Tipo,'
      'pqx.DataX, (pqx.Peso + pqx.RetQtd) AbertoKg, '
      'IF(pqx.Peso = 0, 0, pqx.Valor / pqx.Peso) PRECO,'
      'pqx.RetQtd, pqe.NF_CC '
      'FROM pqx'
      'LEFT JOIN pqe pqe ON pqe.Codigo=pqx.OrigemCodi'
      'WHERE pqx.CliDest=9'
      'AND pqx.Insumo=14'
      'AND pqx.RetQtd + pqx.Peso <> 0'
      'AND pqx.Tipo=10'
      'AND pqx.Peso > 0 '
      'ORDER BY pqx.DataX')
    Left = 188
    Top = 296
    object QrPQRItsOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQRItsOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQRItsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPQRItsDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQRItsAbertoKg: TFloatField
      FieldName = 'AbertoKg'
    end
    object QrPQRItsPRECO: TFloatField
      FieldName = 'PRECO'
    end
    object QrPQRItsRetQtd: TFloatField
      FieldName = 'RetQtd'
    end
    object QrPQRItsNF_CC: TIntegerField
      FieldName = 'NF_CC'
    end
  end
  object frxDsPQRSum: TfrxDBDataset
    UserName = 'frxDsPQRSum'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Insumo=Insumo'
      'NomePQ=NomePQ'
      'AbertoKg=AbertoKg'
      'PrecoMedio=PrecoMedio'
      'CodProprio=CodProprio'
      'ValAberto=ValAberto'
      'NOMEFO=NOMEFO'
      'NOMECI=NOMECI'
      'NOMESE=NOMESE'
      'CI=CI')
    DataSet = QrPQRSum
    BCDToCurrency = False
    DataSetOptions = []
    Left = 192
    Top = 340
  end
  object frxDsPQRIts: TfrxDBDataset
    UserName = 'frxDsPQRIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'OrigemCodi=OrigemCodi'
      'OrigemCtrl=OrigemCtrl'
      'Tipo=Tipo'
      'DataX=DataX'
      'AbertoKg=AbertoKg'
      'PRECO=PRECO'
      'RetQtd=RetQtd'
      'NF_CC=NF_CC')
    DataSet = QrPQRIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 192
    Top = 388
  end
  object frxQUI_INSUM_002_009_B: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      
        '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsPQRSum."NOMECI"' +
        #39';'
      
        '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsPQRSum."NOMESE"' +
        #39';'
      
        '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsPQRSum."NOMEPQ"' +
        #39';'
      
        '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsPQRSum."NOMEFO"' +
        #39';'
      ''
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      
        '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsPQRSum."NOMECI"' +
        #39';'
      
        '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsPQRSum."NOMESE"' +
        #39';'
      
        '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsPQRSum."NOMEPQ"' +
        #39';'
      
        '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsPQRSum."NOMEFO"' +
        #39';'
      '    '
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 876
    Top = 284
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPQRIts
        DataSetName = 'frxDsPQRIts'
      end
      item
        DataSet = frxDsPQRSum
        DataSetName = 'frxDsPQRSum'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 158.740250240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 113.385734020000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 113.385734020000000000
          Width = 613.763760000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 0.928880000000000000
          Top = 98.267604250000000000
          Width = 678.834880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_AMOSTRAS]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 143.621974020000000000
          Width = 355.275556380000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188971500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Aberto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897691500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Aberto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606411500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ M'#233'dio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 83.149506220000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 83.149506220000000000
          Width = 614.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PQNOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE DE USO E CONSUMO - POR NFCC')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_HORA]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 143.622140000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd. CI')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPQRSum
        DataSetName = 'frxDsPQRSum'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 302.362170550000000000
          Height = 15.118110240000000000
          DataField = 'NomePQ'
          DataSet = frxDsPQRSum
          DataSetName = 'frxDsPQRSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQRSum."NomePQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188976380000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'AbertoKg'
          DataSet = frxDsPQRSum
          DataSetName = 'frxDsPQRSum'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRSum."AbertoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'ValAberto'
          DataSet = frxDsPQRSum
          DataSetName = 'frxDsPQRSum'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRSum."ValAberto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'PrecoMedio'
          DataSet = frxDsPQRSum
          DataSetName = 'frxDsPQRSum'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRSum."PrecoMedio"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsPQRSum
          DataSetName = 'frxDsPQRSum'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQRSum."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'CodProprio'
          DataSet = frxDsPQRSum
          DataSetName = 'frxDsPQRSum'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQRSum."CodProprio"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 393.070856380000000000
          Top = 15.118119999999980000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Aberto')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779498270000000000
          Top = 15.118119999999980000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ unit'#225'rio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913019690000000000
          Top = 15.118119999999980000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Top = 15.118119999999980000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'NF CC')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Top = 15.118119999999980000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ori Codi')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 15.118119999999980000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ori Ctrl')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 15.118119999999980000
          Width = 30.236205830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 15.118119999999980000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd retornado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 423.307360000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 3.779530000000022000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 54.456710000000000000
          Top = 3.779530000000022000
          Width = 353.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQRSum."AbertoKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897901420000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQRSum."ValAberto">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606562840000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPQRSum."AbertoKg">)=0,0,(SUM(<frxDsPQRSum."ValAbe' +
              'rto">) / SUM(<frxDsPQRSum."AbertoKg">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 374.173470000000000000
        Width = 680.315400000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000020000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 54.220470000000000000
          Top = 3.779527560000020000
          Width = 353.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188956850000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQRSum."AbertoKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQRSum."ValAberto">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPQRSum."AbertoKg">)=0,0,(SUM(<frxDsPQRSum."ValAbe' +
              'rto">) / SUM(<frxDsPQRSum."AbertoKg">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 597.165740000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 472.441250000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPQRIts
        DataSetName = 'frxDsPQRIts'
        RowCount = 0
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 393.070856380000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'AbertoKg'
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRIts."AbertoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779498270000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'PRECO'
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRIts."PRECO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 302.361999690000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsPQRIts."RetQtd">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'NF_CC'
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRIts."NF_CC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'DataX'
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRIts."DataX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'OrigemCodi'
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRIts."OrigemCodi"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'OrigemCtrl'
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRIts."OrigemCtrl"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 30.236205830000000000
          Height = 15.118110240000000000
          DataField = 'Tipo'
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRIts."Tipo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456710000000000000
        Top = 548.031850000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000020000
          Width = 381.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 3.779529999999908000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQRSum."AbertoKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897901420000000000
          Top = 3.779529999999908000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPQRSum."ValAberto">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606562840000000000
          Top = 3.779529999999908000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPQRSum."AbertoKg">)=0,0,(SUM(<frxDsPQRSum."ValAbe' +
              'rto">) / SUM(<frxDsPQRSum."AbertoKg">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxEstqNFs: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      
        '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsEstqNFs."NOMECI' +
        '"'#39';'
      
        '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsEstqNFs."NOMESE' +
        '"'#39';'
      
        '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsEstqNFs."NOMEPQ' +
        '"'#39';'
      
        '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsEstqNFs."NOMEFO' +
        '"'#39';'
      ''
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      
        '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsEstqNFs."NOMECI' +
        '"'#39';'
      
        '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsEstqNFs."NOMESE' +
        '"'#39';'
      
        '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsEstqNFs."NOMEPQ' +
        '"'#39';'
      
        '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsEstqNFs."NOMEFO' +
        '"'#39';'
      '    '
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 504
    Top = 216
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqNFs
        DataSetName = 'frxDsEstqNFs'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 544.252320000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050900
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 158.740250240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 113.385734020000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 113.385734020000000000
          Width = 613.763760000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 0.928880000000000000
          Top = 98.267604250000000000
          Width = 678.834880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_AMOSTRAS]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 143.621974020000000000
          Width = 226.771536380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 480.000041500000000000
          Top = 143.621974020000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031581500000000000
          Top = 143.621974020000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622181500000000000
          Top = 143.621974020000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo unit')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 83.149506220000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 83.149506220000000000
          Width = 614.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PQNOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE DE USO E CONSUMO DE NFs')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_HORA]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 143.622140000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'NF')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Top = 143.622140000000000000
          Width = 151.180936380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo de movimento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 143.622140000000000000
          Width = 49.133855830000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID Movim.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692910000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Width = 427.086631260000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 480.000310000000000000
          Width = 68.031496060000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqNFs."SdoPeso">)]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031791420000000000
          Width = 75.590551180000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqNFs."SdoValr">)]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622332840000000000
          Width = 56.692913390000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqNFs."SdoPeso">)=0,0,(SUM(<frxDsEstqNFs."SdoVa' +
              'lr">) / SUM(<frxDsEstqNFs."SdoPeso">)))]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEstqNFs
        DataSetName = 'frxDsEstqNFs'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795265830000000000
          Width = 188.976270550000000000
          Height = 15.118110240000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqNFs."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 480.000046380000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'SdoPeso'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqNFs."SdoPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031508270000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'SdoValr'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqNFs."SdoValr"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622049690000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Custo'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqNFs."Custo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795275590551200000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqNFs."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'NF'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqNFs."NF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Width = 34.015506380000000000
          Height = 15.118110240000000000
          DataField = 'Tipo'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqNFs."Tipo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Width = 117.165166380000000000
          Height = 15.118110240000000000
          DataField = 'NO_Tipo'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqNFs."NO_Tipo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 49.133855830000000000
          Height = 15.118110240000000000
          DataField = 'OrigemCodi'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqNFs."OrigemCodi"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692913385830000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Width = 28.000000000000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 54.456710000000000000
          Width = 400.629921260000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 480.000310000000000000
          Width = 68.031496060000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqNFs."SdoPeso">)]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031791420000000000
          Width = 75.590551180000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqNFs."SdoValr">)]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622332840000000000
          Width = 56.692913390000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqNFs."SdoPeso">)=0,0,(SUM(<frxDsEstqNFs."SdoVa' +
              'lr">) / SUM(<frxDsEstqNFs."SdoPeso">)))]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstqNFs."NOMECI"'
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstqNFs."NOMEPQ"'
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692913385830000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Width = 28.000000000000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 54.220470000000000000
          Width = 400.629921260000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 480.000026850000000000
          Width = 68.031496060000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqNFs."SdoPeso">)]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031508270000000000
          Width = 75.590551180000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqNFs."SdoValr">)]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622049690000000000
          Width = 56.692913390000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqNFs."SdoPeso">)=0,0,(SUM(<frxDsEstqNFs."SdoVa' +
              'lr">) / SUM(<frxDsEstqNFs."SdoPeso">)))]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsEstqNFs: TfrxDBDataset
    UserName = 'frxDsEstqNFs'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEFO=NOMEFO'
      'NOMECI=NOMECI'
      'NOMEPQ=NOMEPQ'
      'NOMESE=NOMESE'
      'CodProprio=CodProprio'
      'NF=NF'
      'prod_CFOP=prod_CFOP'
      'DataX=DataX'
      'CliOrig=CliOrig'
      'Insumo=Insumo'
      'Peso=Peso'
      'Valor=Valor'
      'SdoPeso=SdoPeso'
      'SdoValr=SdoValr'
      'Custo=Custo'
      'Tipo=Tipo'
      'NO_Tipo=NO_Tipo'
      'OrigemCodi=OrigemCodi')
    DataSet = QrEstqNFs
    BCDToCurrency = False
    DataSetOptions = []
    Left = 504
    Top = 264
  end
  object QrEstqNFs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ind.Tipo=0, ind.RazaoSocial, ind.Nome) NOMEFO,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECI,'
      'pq_.Nome NOMEPQ, lse.Nome NOMESE, pcl.CodProprio,'
      'pqe.NF, pqi.prod_CFOP, pqx.DataX, pqx.CliOrig,'
      'pqx.Insumo, pqx.Peso, pqx.Valor, pqx.SdoPeso,'
      'pqx.SdoValr, (pqx.Valor / pqx.Peso) Custo,'
      'pqx.Tipo, vfi.Nome NO_Tipo'
      'FROM pqx pqx'
      'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo'
      'LEFT JOIN pqeits pqi'
      '  ON pqi.Codigo=pqx.OrigemCodi'
      '  AND pqi.Controle=pqx.OrigemCtrl'
      'LEFT JOIN pqe ON pqe.Codigo=pqi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=pqx.CliOrig'
      'LEFT JOIN entidades ind ON ind.Codigo=pq_.IQ'
      'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor'
      'LEFT JOIN pqcli pcl ON pcl.PQ=pqx.Insumo AND pcl.CI=pqx.CliOrig'
      'LEFT JOIN varfatid vfi ON vfi.Codigo=pqx.Tipo'
      'WHERE SdoPeso>0'
      '/*AND pqx.Tipo<>0*/'
      'AND pq_.GGXNiv2 IN (0,1,2,4,5)'
      'AND pqx.Empresa=-11'
      'AND pcl.Peso <> 0'
      'AND pcl.PQ=223'
      'ORDER BY NOMECI, NOMESE, NOMEPQ'
      '')
    Left = 504
    Top = 308
    object QrEstqNFsNOMEFO: TWideStringField
      FieldName = 'NOMEFO'
      Size = 100
    end
    object QrEstqNFsNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrEstqNFsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrEstqNFsNOMESE: TWideStringField
      FieldName = 'NOMESE'
    end
    object QrEstqNFsCodProprio: TWideStringField
      FieldName = 'CodProprio'
    end
    object QrEstqNFsprod_CFOP: TWideStringField
      FieldName = 'prod_CFOP'
      Size = 4
    end
    object QrEstqNFsDataX: TDateField
      FieldName = 'DataX'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEstqNFsCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrEstqNFsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrEstqNFsPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrEstqNFsValor: TFloatField
      FieldName = 'Valor'
    end
    object QrEstqNFsSdoPeso: TFloatField
      FieldName = 'SdoPeso'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrEstqNFsSdoValr: TFloatField
      FieldName = 'SdoValr'
    end
    object QrEstqNFsCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrEstqNFsTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEstqNFsNO_Tipo: TWideStringField
      FieldName = 'NO_Tipo'
      Size = 255
    end
    object QrEstqNFsOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrEstqNFsNF: TFloatField
      FieldName = 'NF'
    end
  end
  object QrListaPesagem: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrListaPesagemCalcFields
    SQL.Strings = (
      'SELECT dfp.Grandeza,  '
      'IF(dfp.Grandeza=1, emi.Peso, 0.000) ToSumPeso, '
      'IF(dfp.Grandeza=2, emi.AreaM2, 0.00) ToSumAreaM2, '
      'IF(dfp.Grandeza=3, emi.Qtde, 0.0) ToSumPecas, '
      'emi.*,  '
      'DATE_FORMAT(emi.DataEmis, "%d/%m/%Y") DataEmis_TXT  '
      'FROM emit emi '
      'LEFT JOIN defpecas dfp ON dfp.Codigo=emi.CodDefPeca '
      'WHERE DataEmis  BETWEEN "2021-01-02" AND "2021-11-02 23:59:59" '
      'AND Setor in (-9,0,1,2)  '
      'AND empresa=-11 '
      'ORDER BY DataEmis_TXT  ')
    Left = 892
    Top = 212
    object QrListaPesagemDataEmis: TDateTimeField
      FieldName = 'DataEmis'
    end
    object QrListaPesagemDataEmis_TXT: TWideStringField
      FieldName = 'DataEmis_TXT'
      Size = 10
    end
    object QrListaPesagemNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrListaPesagemNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrListaPesagemPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrListaPesagemQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrListaPesagemFulao: TWideStringField
      FieldName = 'Fulao'
      Size = 5
    end
    object QrListaPesagemSetor: TSmallintField
      FieldName = 'Setor'
    end
    object QrListaPesagemNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrListaPesagemObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
    object QrListaPesagemSemiTxtEspReb: TWideStringField
      FieldName = 'SemiTxtEspReb'
      Size = 15
    end
    object QrListaPesagemDtaBaixa: TDateField
      FieldName = 'DtaBaixa'
      Required = True
    end
    object QrListaPesagemDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
      Required = True
    end
    object QrListaPesagemEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrListaPesagemHoraIni: TTimeField
      FieldName = 'HoraIni'
      Required = True
    end
    object QrListaPesagemGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Required = True
    end
    object QrListaPesagemNoGraCorCad: TWideStringField
      FieldName = 'NoGraCorCad'
      Size = 30
    end
    object QrListaPesagemSemiCodEspReb: TIntegerField
      FieldName = 'SemiCodEspReb'
      Required = True
    end
    object QrListaPesagemGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrListaPesagemToSumPeso: TFloatField
      FieldName = 'ToSumPeso'
      Required = True
    end
    object QrListaPesagemToSumAreaM2: TFloatField
      FieldName = 'ToSumAreaM2'
      Required = True
    end
    object QrListaPesagemToSumPecas: TFloatField
      FieldName = 'ToSumPecas'
      Required = True
    end
    object QrListaPesagemCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrListaPesagemStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrListaPesagemNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrListaPesagemTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrListaPesagemClienteI: TIntegerField
      FieldName = 'ClienteI'
      Required = True
    end
    object QrListaPesagemTipificacao: TSmallintField
      FieldName = 'Tipificacao'
      Required = True
    end
    object QrListaPesagemTempoR: TIntegerField
      FieldName = 'TempoR'
      Required = True
    end
    object QrListaPesagemTempoP: TIntegerField
      FieldName = 'TempoP'
      Required = True
    end
    object QrListaPesagemTipific: TSmallintField
      FieldName = 'Tipific'
      Required = True
    end
    object QrListaPesagemEspessura: TWideStringField
      FieldName = 'Espessura'
      Size = 15
    end
    object QrListaPesagemDefPeca: TWideStringField
      FieldName = 'DefPeca'
      Size = 15
    end
    object QrListaPesagemCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrListaPesagemAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrListaPesagemSetrEmi: TSmallintField
      FieldName = 'SetrEmi'
      Required = True
    end
    object QrListaPesagemSourcMP: TSmallintField
      FieldName = 'SourcMP'
      Required = True
    end
    object QrListaPesagemCod_Espess: TIntegerField
      FieldName = 'Cod_Espess'
      Required = True
    end
    object QrListaPesagemCodDefPeca: TIntegerField
      FieldName = 'CodDefPeca'
      Required = True
    end
    object QrListaPesagemCustoTo: TFloatField
      FieldName = 'CustoTo'
      Required = True
    end
    object QrListaPesagemCustoKg: TFloatField
      FieldName = 'CustoKg'
      Required = True
    end
    object QrListaPesagemCustoM2: TFloatField
      FieldName = 'CustoM2'
      Required = True
    end
    object QrListaPesagemCusUSM2: TFloatField
      FieldName = 'CusUSM2'
      Required = True
    end
    object QrListaPesagemEmitGru: TIntegerField
      FieldName = 'EmitGru'
      Required = True
    end
    object QrListaPesagemRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
      Required = True
    end
    object QrListaPesagemSemiCodPeca: TIntegerField
      FieldName = 'SemiCodPeca'
      Required = True
    end
    object QrListaPesagemSemiTxtPeca: TWideStringField
      FieldName = 'SemiTxtPeca'
      Size = 15
    end
    object QrListaPesagemSemiPeso: TFloatField
      FieldName = 'SemiPeso'
      Required = True
    end
    object QrListaPesagemSemiQtde: TFloatField
      FieldName = 'SemiQtde'
      Required = True
    end
    object QrListaPesagemSemiAreaM2: TFloatField
      FieldName = 'SemiAreaM2'
      Required = True
    end
    object QrListaPesagemSemiRendim: TFloatField
      FieldName = 'SemiRendim'
      Required = True
    end
    object QrListaPesagemSemiCodEspe: TIntegerField
      FieldName = 'SemiCodEspe'
      Required = True
    end
    object QrListaPesagemSemiTxtEspe: TWideStringField
      FieldName = 'SemiTxtEspe'
      Size = 15
    end
    object QrListaPesagemBRL_USD: TFloatField
      FieldName = 'BRL_USD'
      Required = True
    end
    object QrListaPesagemBRL_EUR: TFloatField
      FieldName = 'BRL_EUR'
      Required = True
    end
    object QrListaPesagemDtaCambio: TDateField
      FieldName = 'DtaCambio'
      Required = True
    end
    object QrListaPesagemVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
      Required = True
    end
    object QrListaPesagemDtaBaixa_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DtaBaixa_TXT'
      Size = 10
      Calculated = True
    end
    object QrListaPesagemObsNoTrim: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ObsNoTrim'
      Size = 511
      Calculated = True
    end
  end
  object frxListaPesagem: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxListaPesagemGetValue
    Left = 892
    Top = 112
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsListaPesagem
        DataSetName = 'frxDsListaPesagem'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 483.779840000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 119.055186460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590434020000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 64.251968500000000000
          Top = 75.590434020000000000
          Width = 617.543290000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 105.826674020000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Receita - Nome')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 385.511791500000000000
          Top = 105.826674020000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LISTA PESAGEM')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_HORA]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde. P'#199)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Top = 105.826840000000000000
          Width = 22.677155590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ful'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Top = 105.826791180000000000
          Width = 37.795275590551180000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data baixa')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 105.826840000000000000
          Width = 151.181163390000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472479999999990000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 60.472479999999990000
          Width = 616.063348500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Por pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 105.826840000000000000
          Width = 60.472440944881890000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Por Peso')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Top = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Por m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 105.826840000000000000
          Width = 26.456673390000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'In'#237'cio')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007876460000000000
        Top = 404.409710000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456692910000000000
          Top = 3.779527560000000000
          Width = 162.519580080000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984481420000000000
          Top = 3.779530000000000000
          Width = 56.692891420000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."Qtde">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 3.779530000000000000
          Width = 56.692891420000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0;-#,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."ToSumPecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 3.779530000000000000
          Width = 60.472440944881890000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."ToSumPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Top = 3.779530000000000000
          Width = 56.692891420000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."ToSumAreaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Top = 3.779530000000000000
          Width = 22.677121420000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228395280000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsListaPesagem
        DataSetName = 'frxDsListaPesagem'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795265830000000000
          Top = 0.000048820000000005
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataSet = frxDsListaPesagem
          DataSetName = 'frxDsListaPesagem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsListaPesagem."Numero"] - [frxDsListaPesagem."NOME"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 385.511796380000000000
          Top = 0.000048820000000005
          Width = 60.472421420000000000
          Height = 13.228346460000000000
          DataSet = frxDsListaPesagem
          DataSetName = 'frxDsListaPesagem'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsListaPesagem."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984198270000000000
          Top = 0.000048820000000005
          Width = 56.692891420000000000
          Height = 13.228346460000000000
          DataSet = frxDsListaPesagem
          DataSetName = 'frxDsListaPesagem'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsListaPesagem."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834479690000000000
          Top = 0.000048820000000005
          Width = 22.677121420000000000
          Height = 13.228346460000000000
          DataSet = frxDsListaPesagem
          DataSetName = 'frxDsListaPesagem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsListaPesagem."Fulao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795275590551180000
          Height = 13.228346460000000000
          DataSet = frxDsListaPesagem
          DataSetName = 'frxDsListaPesagem'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsListaPesagem."DtaBaixa"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 0.000048820000000005
          Width = 151.181141420000000000
          Height = 13.228346456692910000
          DataField = 'ObsNoTrim'
          DataSet = frxDsListaPesagem
          DataSetName = 'frxDsListaPesagem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsListaPesagem."ObsNoTrim"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 0.000048820000000005
          Width = 56.692891420000000000
          Height = 13.228346460000000000
          DataSet = frxDsListaPesagem
          DataSetName = 'frxDsListaPesagem'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0;-#,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsListaPesagem."ToSumPecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 0.000048820000000005
          Width = 60.472440944881890000
          Height = 13.228346460000000000
          DataField = 'ToSumPeso'
          DataSet = frxDsListaPesagem
          DataSetName = 'frxDsListaPesagem'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsListaPesagem."ToSumPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Top = 0.000048820000000005
          Width = 56.692891420000000000
          Height = 13.228346460000000000
          DataField = 'ToSumAreaM2'
          DataSet = frxDsListaPesagem
          DataSetName = 'frxDsListaPesagem'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsListaPesagem."ToSumAreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 0.000048820000000005
          Width = 26.456612360000000000
          Height = 13.228346456692910000
          DataField = 'HoraIni'
          DataSet = frxDsListaPesagem
          DataSetName = 'frxDsListaPesagem'
          DisplayFormat.FormatStr = 'hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsListaPesagem."HoraIni"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.220470000000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456692910000000000
          Top = 3.779530000000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692913390000000000
          Top = 3.779530000000000000
          Width = 132.283359610000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsListaPesagem."DataEmis_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984481420000000000
          Top = 3.779530000000000000
          Width = 56.692891420000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."Qtde">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 3.779530000000000000
          Width = 56.692891420000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0;-#,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."ToSumPecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 3.779530000000000000
          Width = 60.472440944881890000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."ToSumPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Top = 3.779530000000000000
          Width = 56.692891420000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."ToSumAreaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Top = 3.779530000000000000
          Width = 22.677121420000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsListaPesagem."DtaBaixa_TXT"'
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = -7.559060000000000000
          Width = 680.314960630000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsListaPesagem."DtaBaixa_TXT"]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.220470000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsListaPesagem."NOMESETOR"'
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677165350000000000
          Width = 657.637805040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsListaPesagem."NOMESETOR"]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.220470000000000000
        Top = 313.700990000000000000
        Width = 680.315400000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456692910000000000
          Top = 3.779527560000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692913390000000000
          Top = 3.779527560000000000
          Width = 132.283322990000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsListaPesagem."NOMESETOR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 385.511776850000000000
          Top = 3.779527560000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984198270000000000
          Top = 3.779527560000000000
          Width = 56.692891420000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."Qtde">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 3.779530000000000000
          Width = 56.692891420000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0;-#,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."ToSumPecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 3.779530000000000000
          Width = 60.472440944881890000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."ToSumPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Top = 3.779530000000000000
          Width = 56.692891420000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsListaPesagem."ToSumAreaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Top = 3.779530000000000000
          Width = 22.677121420000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsListaPesagem: TfrxDBDataset
    UserName = 'frxDsListaPesagem'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataEmis=DataEmis'
      'DataEmis_TXT=DataEmis_TXT'
      'Numero=Numero'
      'NOME=NOME'
      'Peso=Peso'
      'Qtde=Qtde'
      'Fulao=Fulao'
      'Setor=Setor'
      'NOMESETOR=NOMESETOR'
      'Obs=Obs'
      'SemiTxtEspReb=SemiTxtEspReb'
      'DtaBaixa=DtaBaixa'
      'DtCorrApo=DtCorrApo'
      'Empresa=Empresa'
      'HoraIni=HoraIni'
      'GraCorCad=GraCorCad'
      'NoGraCorCad=NoGraCorCad'
      'SemiCodEspReb=SemiCodEspReb'
      'Grandeza=Grandeza'
      'ToSumPeso=ToSumPeso'
      'ToSumAreaM2=ToSumAreaM2'
      'ToSumPecas=ToSumPecas'
      'Codigo=Codigo'
      'Status=Status'
      'NOMECI=NOMECI'
      'Tecnico=Tecnico'
      'ClienteI=ClienteI'
      'Tipificacao=Tipificacao'
      'TempoR=TempoR'
      'TempoP=TempoP'
      'Tipific=Tipific'
      'Espessura=Espessura'
      'DefPeca=DefPeca'
      'Custo=Custo'
      'AreaM2=AreaM2'
      'SetrEmi=SetrEmi'
      'SourcMP=SourcMP'
      'Cod_Espess=Cod_Espess'
      'CodDefPeca=CodDefPeca'
      'CustoTo=CustoTo'
      'CustoKg=CustoKg'
      'CustoM2=CustoM2'
      'CusUSM2=CusUSM2'
      'EmitGru=EmitGru'
      'Retrabalho=Retrabalho'
      'SemiCodPeca=SemiCodPeca'
      'SemiTxtPeca=SemiTxtPeca'
      'SemiPeso=SemiPeso'
      'SemiQtde=SemiQtde'
      'SemiAreaM2=SemiAreaM2'
      'SemiRendim=SemiRendim'
      'SemiCodEspe=SemiCodEspe'
      'SemiTxtEspe=SemiTxtEspe'
      'BRL_USD=BRL_USD'
      'BRL_EUR=BRL_EUR'
      'DtaCambio=DtaCambio'
      'VSMovCod=VSMovCod'
      'DtaBaixa_TXT=DtaBaixa_TXT'
      'ObsNoTrim=ObsNoTrim')
    DataSet = QrListaPesagem
    BCDToCurrency = False
    DataSetOptions = []
    Left = 891
    Top = 164
  end
  object frxEstoqueEm3: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39716.814898726800000000
    ReportOptions.LastChange = 39716.814898726800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      '  case <VFR_ORD1> of '
      '    0: GH1.Condition := '#39'frxDsMoviPQ3."NOMECI"'#39';'
      '    1: GH1.Condition := '#39'frxDsMoviPQ3."NOMESE"'#39';'
      '    2: GH1.Condition := '#39'frxDsMoviPQ3."NOMEPQ"'#39';'
      '    3: GH1.Condition := '#39'frxDsMoviPQ3."NOMEFO"'#39';'
      '  end;              '
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      '  case <VFR_ORD2> of '
      '    0: GH2.Condition := '#39'frxDsMoviPQ3."NOMECI"'#39';'
      '    1: GH2.Condition := '#39'frxDsMoviPQ3."NOMESE"'#39';'
      '    2: GH2.Condition := '#39'frxDsMoviPQ3."NOMEPQ"'#39';'
      '    3: GH2.Condition := '#39'frxDsMoviPQ3."NOMEFO"'#39';'
      '  end;              '
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 792
    Top = 524
    Datasets = <
      item
      end
      item
        DataSet = frxDsMoviPQ3
        DataSetName = 'frxDsMoviPQ3'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 554.000000000000000000
          Top = 0.881879999999998800
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 4.440940000000001000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 174.000000000000000000
          Top = 16.881880000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 40.771180000000000000
        Top = 563.149970000000000000
        Width = 971.339210000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 541.779530000000000000
          Top = 7.653059999999982000
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 351.496290000000000000
        Width = 971.339210000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsMoviPQ3
        DataSetName = 'frxDsMoviPQ3'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527541970000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'InnVal'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."InnVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 196.535462360000000000
          Height = 15.118110240000000000
          DataField = 'NomePQ'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviPQ3."NomePQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055110790000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'InnPes'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."InnPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779478740000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'OutPes'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."OutPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 544.251909920000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'OutVal'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."OutVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803291180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'AntVal'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."AntVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'AntPes'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."AntPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 850.394250000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'FimPes'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."FimPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 910.866681180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'FimVal'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."FimVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 725.669760000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'AjuPes'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."AjuPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142191180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'AjuVal'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."AjuVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'DvlPes'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."DvlPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197231180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'DvlVal'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."DvlVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795202360000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviPQ3."Insumo"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.110080000000000000
        Top = 268.346630000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsMoviPQ3."NomeSE"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 2.110079999999982000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 19.196660000000000000
        Top = 309.921460000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsMoviPQ3."NomeFO"'
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Top = 1.196660000000008000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 434.645950000000000000
        Width = 971.339210000000000000
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = -1.559060000000000000
          Top = 3.779527560000020000
          Width = 24.220470000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677155590000000000
          Top = 3.779527560000020000
          Width = 207.874027950000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527541970000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."InnVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055110790000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."InnPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779478740000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."OutPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 544.251909920000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."OutVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803291180000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AntVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AntPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 850.394250000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."FimPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 910.866681180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."FimVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 3.779529999999965000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."DvlPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197231180000000000
          Top = 3.779529999999965000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."DvlVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 725.669760000000000000
          Top = 3.779529999999965000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AjuPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142191180000000000
          Top = 3.779529999999965000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AjuVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 389.291590000000000000
        Width = 971.339210000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = -0.677180000000000000
          Top = 3.779527560000020000
          Width = 24.220470000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677155590000000000
          Top = 3.779527560000020000
          Width = 207.874027950000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527541970000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."InnVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055110790000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."InnPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779478740000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."OutPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 544.251909920000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."OutVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803291180000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AntVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AntPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 850.394250000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."FimPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 910.866681180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."FimVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 725.669760000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AjuPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142191180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AjuVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."DvlPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197231180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."DvlVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 517.795610000000000000
        Width = 971.339210000000000000
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 391.071120000000000000
          Top = 3.023622049999972000
          Width = 716.881880000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527541970000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."InnVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055110790000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."InnPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779478740000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."OutPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 544.251909920000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."OutVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779527560000020000
          Width = 226.771702360000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL  ')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803291180000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AntVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AntPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 850.394250000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."FimPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 910.866681180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."FimVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."DvlPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197231180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."DvlVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 725.669760000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AjuPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142191180000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AjuVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 92.598425200000000000
        Top = 113.385900000000000000
        Width = 971.339210000000000000
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 26.834570000000000000
          Width = 701.543290000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 8.220470000000000000
          Top = 27.055040000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 6.661410000000000000
          Top = 3.779529999999994000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'MOVIMENTO DE INSUMOS QU'#205'MICOS')
          ParentFont = False
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 71.779530000000010000
          Width = 712.881880000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 74.220470000000000000
          Top = 27.779530000000010000
          Width = 636.440940000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527541970000000000
          Top = 75.590551180000030000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Entrada R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590551180000030000
          Width = 234.330718430000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055110790000000000
          Top = 75.590551180000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtd Entrada')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779478740000000000
          Top = 75.590551180000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Consumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 544.251909920000000000
          Top = 75.590551180000030000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Consumo R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 6.661410000000000000
          Top = 47.779530000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo:')
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 74.220470000000000000
          Top = 47.779530000000010000
          Width = 636.440940000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803291180000000000
          Top = 75.590551180000030000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Anterior R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 75.590551180000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtd Anterior')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 850.394250000000000000
          Top = 75.590600000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Final')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 910.866681180000000000
          Top = 75.590599999999970000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Final R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 725.669760000000000000
          Top = 75.590600000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Ajuste')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142191180000000000
          Top = 75.590599999999970000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ajuste R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 75.590600000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Devolvido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197231180000000000
          Top = 75.590599999999970000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Devolvido R$')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsMoviPQ3: TfrxDBDataset
    UserName = 'frxDsMoviPQ3'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Insumo=Insumo'
      'NomePQ=NomePQ'
      'NomeFO=NomeFO'
      'NomeCI=NomeCI'
      'NomeSE=NomeSE'
      'InnPes=InnPes'
      'InnVal=InnVal'
      'OutPes=OutPes'
      'OutVal=OutVal'
      'AntPes=AntPes'
      'AntVal=AntVal'
      'FimPes=FimPes'
      'FimVal=FimVal'
      'BalPes=BalPes'
      'BalVal=BalVal'
      'DvlPes=DvlPes'
      'DvlVal=DvlVal'
      'AjuPes=AjuPes'
      'AjuVal=AjuVal'
      'InnPad=InnPad'
      'OutPad=OutPad'
      'AntPad=AntPad'
      'FimPad=FimPad'
      'BalPad=BalPad'
      'DvlPad=DvlPad')
    DataSet = QrMoviPQ3
    BCDToCurrency = False
    DataSetOptions = []
    Left = 792
    Top = 576
  end
  object QrMoviPQ3: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMoviPQ3CalcFields
    SQL.Strings = (
      'SELECT * FROM movipq'
      '')
    Left = 792
    Top = 624
    object QrMoviPQ3Insumo: TIntegerField
      FieldName = 'Insumo'
      Origin = 'movipq.Insumo'
    end
    object QrMoviPQ3NomePQ: TWideStringField
      FieldName = 'NomePQ'
      Origin = 'movipq.NomePQ'
      Size = 100
    end
    object QrMoviPQ3NomeFO: TWideStringField
      FieldName = 'NomeFO'
      Origin = 'movipq.NomeFO'
      Size = 100
    end
    object QrMoviPQ3NomeCI: TWideStringField
      FieldName = 'NomeCI'
      Origin = 'movipq.NomeCI'
      Size = 100
    end
    object QrMoviPQ3NomeSE: TWideStringField
      FieldName = 'NomeSE'
      Origin = 'movipq.NomeSE'
      Size = 100
    end
    object QrMoviPQ3InnPes: TFloatField
      FieldName = 'InnPes'
      Origin = 'movipq.InnPes'
    end
    object QrMoviPQ3InnVal: TFloatField
      FieldName = 'InnVal'
      Origin = 'movipq.InnVal'
    end
    object QrMoviPQ3OutPes: TFloatField
      FieldName = 'OutPes'
      Origin = 'movipq.OutPes'
    end
    object QrMoviPQ3OutVal: TFloatField
      FieldName = 'OutVal'
      Origin = 'movipq.OutVal'
    end
    object QrMoviPQ3AntPes: TFloatField
      FieldName = 'AntPes'
      Origin = 'movipq.AntPes'
    end
    object QrMoviPQ3AntVal: TFloatField
      FieldName = 'AntVal'
      Origin = 'movipq.AntVal'
    end
    object QrMoviPQ3FimPes: TFloatField
      FieldName = 'FimPes'
      Origin = 'movipq.FimPes'
    end
    object QrMoviPQ3FimVal: TFloatField
      FieldName = 'FimVal'
      Origin = 'movipq.FimVal'
    end
    object QrMoviPQ3BalPes: TFloatField
      FieldName = 'BalPes'
      Origin = 'movipq.BalPes'
    end
    object QrMoviPQ3BalVal: TFloatField
      FieldName = 'BalVal'
      Origin = 'movipq.BalVal'
    end
    object QrMoviPQ3DvlPes: TFloatField
      FieldName = 'DvlPes'
    end
    object QrMoviPQ3DvlVal: TFloatField
      FieldName = 'DvlVal'
    end
    object QrMoviPQ3AjuPes: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AjuPes'
      Calculated = True
    end
    object QrMoviPQ3AjuVal: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AjuVal'
      Calculated = True
    end
    object QrMoviPQ3InnPad: TFloatField
      FieldName = 'InnPad'
      Origin = 'movipq.InnPad'
    end
    object QrMoviPQ3OutPad: TFloatField
      FieldName = 'OutPad'
      Origin = 'movipq.OutPad'
    end
    object QrMoviPQ3AntPad: TFloatField
      FieldName = 'AntPad'
      Origin = 'movipq.AntPad'
    end
    object QrMoviPQ3FimPad: TFloatField
      FieldName = 'FimPad'
      Origin = 'movipq.FimPad'
    end
    object QrMoviPQ3BalPad: TFloatField
      FieldName = 'BalPad'
      Origin = 'movipq.BalPad'
    end
    object QrMoviPQ3DvlPad: TFloatField
      FieldName = 'DvlPad'
    end
  end
  object frxEstq2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MePesoOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Band4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <frxDsEstq."Peso"> < 0 then'
      '  begin              '
      '    MePeso.Font.Color       := clRed;'
      '    MeCodProprio.Font.Color := clRed;  '
      '    MePQ.Font.Color         := clRed;  '
      '    MeNomePQ.Font.Color     := clRed;                        '
      '  end else'
      '  begin              '
      '    MePeso.Font.Color       := clBlack;'
      '    MeCodProprio.Font.Color := clBlack;  '
      '    MePQ.Font.Color         := clBlack;  '
      '    MeNomePQ.Font.Color     := clBlack;                        '
      '  end;            '
      'end;'
      ''
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      ''
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      '  //'
      
        '  MeObservacao.Visible := <VARF_OBSERVACAO> <> '#39#39';              ' +
        '                                         '
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 52
    Top = 232
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 593.386210000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 158.740250240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 113.385734020000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 113.385734020000000000
          Width = 613.763760000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 0.928880000000000000
          Top = 98.267604250000000000
          Width = 678.834880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_AMOSTRAS]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 143.621974020000000000
          Width = 355.275556380000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188971500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897691500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606411500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo unit')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 83.149506220000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 83.149506220000000000
          Width = 614.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PQNOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE DE USO E CONSUMO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_HORA]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 143.622140000000000000
          Width = 52.913385830000000000
          Height = 15.118110236220470000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd. CI')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527559054998000
          Width = 381.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188956850000000000
          Top = 3.779527559054998000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 3.779530000000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."CustoEstoquePadrao">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606621420000000000
          Top = 3.779530000000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."CustoEstoquePa' +
              'drao">) / SUM(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'Band4OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
        RowCount = 0
        object MeNomePQ: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 302.362170550000000000
          Height = 15.118110240000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstq."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePeso: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188976380000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'MePesoOnBeforePrint'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."CustoEstoquePadrao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'CustoPadrao'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."CustoPadrao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePQ: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstq."PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCodProprio: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'CodProprio'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstq."CodProprio"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188956850000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 3.779530000000022000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 54.456710000000000000
          Top = 3.779530000000022000
          Width = 353.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 3.779530000000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."CustoEstoquePadrao">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606621420000000000
          Top = 3.779530000000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."CustoEstoquePa' +
              'drao">) / SUM(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstq."NOMECI"'
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstq."NOMEPQ"'
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000020000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 54.220470000000000000
          Top = 3.779527560000020000
          Width = 353.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188956850000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Top = 3.779527560000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."CustoEstoquePadrao">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Top = 3.779527560000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."CustoEstoquePa' +
              'drao">) / SUM(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 548.031850000000000000
        Width = 680.315400000000000000
        object MeObservacao: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[VARF_OBSERVACAO]')
          ParentFont = False
        end
      end
    end
  end
  object frxEstoqueEm3CustoPadrao: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39716.814898726800000000
    ReportOptions.LastChange = 39716.814898726800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      '  case <VFR_ORD1> of '
      '    0: GH1.Condition := '#39'frxDsMoviPQ3."NOMECI"'#39';'
      '    1: GH1.Condition := '#39'frxDsMoviPQ3."NOMESE"'#39';'
      '    2: GH1.Condition := '#39'frxDsMoviPQ3."NOMEPQ"'#39';'
      '    3: GH1.Condition := '#39'frxDsMoviPQ3."NOMEFO"'#39';'
      '  end;              '
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      '  case <VFR_ORD2> of '
      '    0: GH2.Condition := '#39'frxDsMoviPQ3."NOMECI"'#39';'
      '    1: GH2.Condition := '#39'frxDsMoviPQ3."NOMESE"'#39';'
      '    2: GH2.Condition := '#39'frxDsMoviPQ3."NOMEPQ"'#39';'
      '    3: GH2.Condition := '#39'frxDsMoviPQ3."NOMEFO"'#39';'
      '  end;              '
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 904
    Top = 524
    Datasets = <
      item
      end
      item
        DataSet = frxDsMoviPQ3
        DataSetName = 'frxDsMoviPQ3'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 799.669450000000000000
          Top = 0.881880000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 4.440940000000001000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 174.000000000000000000
          Top = 16.881880000000000000
          Width = 789.889920000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 40.771180000000000000
        Top = 563.149970000000000000
        Width = 971.339210000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 791.228510000000000000
          Top = 7.653060000000000000
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 351.496290000000000000
        Width = 971.339210000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsMoviPQ3
        DataSetName = 'frxDsMoviPQ3'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527541970000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."InnPad"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 196.535462360000000000
          Height = 15.118110240000000000
          DataField = 'NomePQ'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviPQ3."NomePQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055110790000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'InnPes'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."InnPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779478740000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'OutPes'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."OutPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 544.251909920000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."OutPad"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803291180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."AntPad"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'AntPes'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."AntPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 850.394250000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'FimPes'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."FimPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 910.866681180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."FimPad"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 725.669760000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'AjuPes'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."AjuPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142191180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."AjuVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'DvlPes'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."DvlPes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197231180000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviPQ3."DvlPad"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795202360000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsMoviPQ3
          DataSetName = 'frxDsMoviPQ3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviPQ3."Insumo"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.110080000000000000
        Top = 268.346630000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsMoviPQ3."NomeSE"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 2.110079999999982000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 19.196660000000000000
        Top = 309.921460000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsMoviPQ3."NomeFO"'
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Top = 1.196660000000008000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 434.645950000000000000
        Width = 971.339210000000000000
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = -1.559060000000000000
          Top = 3.779527560000020000
          Width = 24.220470000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677155590000000000
          Top = 3.779527560000020000
          Width = 207.874027950000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055110790000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."InnPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779478740000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."OutPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AntPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 850.394250000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."FimPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 3.779529999999965000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."DvlPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 725.669760000000000000
          Top = 3.779529999999965000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AjuPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527590790000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."InnPad">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 544.251958740000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."OutPad">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AntPad">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142240000000000000
          Top = 3.779532440000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AjuVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Top = 3.779532440000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."DvlPad">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 910.866730000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."FimPad">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 389.291590000000000000
        Width = 971.339210000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = -0.677180000000000000
          Top = 3.779527560000020000
          Width = 24.220470000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677155590000000000
          Top = 3.779527560000020000
          Width = 207.874027950000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527541970000000000
          Top = 3.779527560000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."InnPad">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055110790000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."InnPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779478740000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."OutPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 544.251909920000000000
          Top = 3.779527560000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."OutPad">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803291180000000000
          Top = 3.779527560000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AntPad">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AntPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 850.394250000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."FimPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 910.866681180000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."FimPad">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 725.669760000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AjuPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142191180000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AjuVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."DvlPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197231180000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."DvlPad">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 517.795610000000000000
        Width = 971.339210000000000000
        object Line4: TfrxLineView
          AllowVectorExport = True
          Top = 3.023622050000000000
          Width = 973.889920000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055110790000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."InnPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779478740000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."OutPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779527560000020000
          Width = 226.771702360000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL  ')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 3.779527560000020000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AntPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 850.394250000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."FimPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."DvlPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 725.669760000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AjuPes">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527590790000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."InnPad">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 544.251958740000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."OutPad">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AntPad">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142240000000000000
          Top = 3.779532440000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."AjuVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Top = 3.779532440000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."DvlPad">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 910.866730000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviPQ3."FimPad">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 92.598425200000000000
        Top = 113.385900000000000000
        Width = 971.339210000000000000
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 26.834570000000000000
          Width = 962.330860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 8.220470000000000000
          Top = 27.055040000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 6.661410000000000000
          Top = 3.779530000000000000
          Width = 961.008040000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'MOVIMENTO DE INSUMOS QU'#205'MICOS')
          ParentFont = False
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 71.779530000000000000
          Width = 966.110390000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 74.220470000000000000
          Top = 27.779530000000000000
          Width = 893.448980000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527541970000000000
          Top = 75.590551180000030000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Entrada R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590551180000030000
          Width = 234.330718430000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055110790000000000
          Top = 75.590551180000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtd Entrada')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779478740000000000
          Top = 75.590551180000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Consumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 544.251909920000000000
          Top = 75.590551180000030000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Consumo R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 6.661410000000000000
          Top = 47.779530000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo:')
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 74.220470000000000000
          Top = 47.779530000000000000
          Width = 893.448980000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803291180000000000
          Top = 75.590551180000030000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Anterior R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 75.590551180000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtd Anterior')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 850.394250000000000000
          Top = 75.590600000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Final')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 910.866681180000000000
          Top = 75.590599999999970000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Final R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 725.669760000000000000
          Top = 75.590600000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Ajuste')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142191180000000000
          Top = 75.590599999999970000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ajuste R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 75.590600000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Devolvido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197231180000000000
          Top = 75.590599999999970000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Devolvido R$')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxPedOpen: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.488155740700000000
    ReportOptions.LastChange = 40119.835206539350000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 404
    Top = 476
    Datasets = <
      item
      end
      item
        DataSet = frxDsPedOpen
        DataSetName = 'frxDsPedOpen'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.425170000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 512.425170000000000000
          Top = 8.440940000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 3.921150000000000000
          Top = 1.763760000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 36.000000000000000000
        Top = 506.457020000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 541.779530000000000000
          Top = 0.093999999999937240
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 55.795258500000000000
        Top = 71.811070000000000000
        Width = 680.315400000000000000
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 3.779525120000000000
          Width = 628.409400000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'PREVIS'#195'O PARA COMPRA DE INSUMOS QU'#205'MICOS - POR PEDIDOS ABERTOS')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 30.236218030000020000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 30.236218030000000000
          Width = 394.551020000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 30.236240000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Grupo:')
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 528.661720000000000000
          Top = 30.236240000000000000
          Width = 644.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_GRUPO]')
          ParentFont = False
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 6.000000000000000000
        Top = 438.425480000000000000
        Width = 680.315400000000000000
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 355.275820000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPedOpen
        DataSetName = 'frxDsPedOpen'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 377.952838900000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soUso">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 34.015770000000000000
          Width = 222.992191890000000000
          Height = 15.118110240000000000
          DataField = 'NomePQ'
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPedOpen."NomePQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480410160000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soEstq">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 619.669450000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soCompra">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 0.000041500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soFut">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 0.000041500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Re' +
              'pet">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedOpen."Insumo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 0.000041500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soNeed">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 0.000041500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Kg' +
              'LiqEmb">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        DataSetName = 'frxDsPQUFrm2'
        RowCount = 0
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 238.110228900000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsPQU'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxPQUFrm2."PedAre' +
              'aM2">)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 200.315011890000000000
          Height = 15.118110240000000000
          DataField = 'NOMERECEITA'
          DataSetName = 'frxDsPQUFrm2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQUFrm2."NOMERECEITA"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795221890000000000
          Height = 15.118110240000000000
          DataField = 'Receita'
          DataSetName = 'frxDsPQUFrm2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQUFrm2."Receita"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsPQU'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxPQUFrm2."PedAre' +
              'aP2">)]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsPQU'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxPQUFrm2."PedPes' +
              'oKg">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Width = 79.370051890000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsPQUFrm2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxPQUFrm2."PedLinhasCul"] - [frxPQUFrm2."PedLinhasCab"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPQU."KGT"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 238.110228900000000000
          Top = 3.779530000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ' '#193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779529999999994000
          Width = 238.110311890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Receita')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Top = 3.779530000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ' '#193'rea ft'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Top = 3.779530000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 3.779530000000000000
          Width = 79.370081180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Linhas Rebaixe')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 377.952838900000000000
          Top = 3.779488500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg Usar'#225)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779488500000000000
          Width = 257.007961890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480410160000000000
          Top = 3.779488500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Estq atual')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 619.669450000000000000
          Top = 3.779488500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Comprar')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo futuro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Repeti'#231#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Necess'#225'rio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Liq. Embalagem')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPedOpen."NomeCI"'
        object frxDsPQUItsNomeCI: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 801.260360000000000000
          Height = 18.897650000000000000
          DataField = 'NomeCI'
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPedOpen."NomeCI"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 393.071120000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsPedOpen: TfrxDBDataset
    UserName = 'frxDsPedOpen'
    CloseDataSource = False
    FieldAliases.Strings = (
      'InsumM2PesoKg=InsumM2PesoKg'
      'Codigo=Codigo'
      'Controle=Controle'
      'Insumo=Insumo'
      'PesoEstq=PesoEstq'
      'PesoUso=PesoUso'
      'PesoFut=PesoFut'
      'PesoNeed=PesoNeed'
      'CliInt=CliInt'
      'Repet=Repet'
      'NomePQ=NomePQ'
      'NomeCI=NomeCI'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'Nivel=Nivel'
      'KgLiqEmb=KgLiqEmb'
      'PesoCompra=PesoCompra'
      'IQ=IQ'
      'NOMEIQ=NOMEIQ')
    DataSet = QrPedOpen
    BCDToCurrency = False
    DataSetOptions = []
    Left = 432
    Top = 476
  end
  object QrPedOpen: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM pqurec rec'
      'WHERE Nivel=2')
    Left = 408
    Top = 524
    object QrPedOpenInsumM2PesoKg: TFloatField
      FieldName = 'InsumM2PesoKg'
    end
    object QrPedOpenCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPedOpenControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPedOpenInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPedOpenPesoEstq: TFloatField
      FieldName = 'PesoEstq'
      Required = True
    end
    object QrPedOpenPesoUso: TFloatField
      FieldName = 'PesoUso'
      Required = True
    end
    object QrPedOpenPesoFut: TFloatField
      FieldName = 'PesoFut'
      Required = True
    end
    object QrPedOpenPesoNeed: TFloatField
      FieldName = 'PesoNeed'
      Required = True
    end
    object QrPedOpenCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrPedOpenRepet: TFloatField
      FieldName = 'Repet'
      Required = True
    end
    object QrPedOpenNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Required = True
      Size = 100
    end
    object QrPedOpenNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Required = True
      Size = 100
    end
    object QrPedOpenLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrPedOpenDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPedOpenDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPedOpenUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrPedOpenUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrPedOpenAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPedOpenAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPedOpenAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPedOpenAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPedOpenNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPedOpenKgLiqEmb: TFloatField
      FieldName = 'KgLiqEmb'
    end
    object QrPedOpenPesoCompra: TFloatField
      FieldName = 'PesoCompra'
    end
    object QrPedOpenIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPedOpenNOMEIQ: TWideStringField
      FieldName = 'NOMEIQ'
      Size = 100
    end
  end
  object QrPedPedEPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM pqurec rec'
      'WHERE Nivel=2')
    Left = 272
    Top = 472
    object QrPedPedEPQInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPedPedEPQPesoUso: TFloatField
      FieldName = 'PesoUso'
      Required = True
    end
    object QrPedPedEPQPedidoCod: TIntegerField
      FieldName = 'PedidoCod'
    end
    object QrPedPedEPQNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Required = True
      Size = 100
    end
    object QrPedPedEPQNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Required = True
      Size = 100
    end
    object QrPedPedEPQPedidoTxt: TWideStringField
      FieldName = 'PedidoTxt'
      Size = 255
    end
    object QrPedPedEPQCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPedPedEPQPedidoOrd: TIntegerField
      FieldName = 'PedidoOrd'
    end
    object QrPedPedEPQInsumM2PesoKg: TFloatField
      FieldName = 'InsumM2PesoKg'
    end
    object QrPedPedEPQIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPedPedEPQNOMEIQ: TWideStringField
      FieldName = 'NOMEIQ'
      Size = 100
    end
  end
  object frxPedSeq: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.488155740700000000
    ReportOptions.LastChange = 40119.835206539300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 412
    Top = 568
    Datasets = <
      item
      end
      item
        DataSet = frxDsPedComprar
        DataSetName = 'frxDsPedComprar'
      end
      item
        DataSet = frxDsPedOpen
        DataSetName = 'frxDsPedOpen'
      end
      item
        DataSet = frxDsPedSeq
        DataSetName = 'frxDsPedSeq'
      end
      item
        DataSet = frxDsPQUCab
        DataSetName = 'frxDsPQUCab'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.425170000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 512.425170000000000000
          Top = 8.440940000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 3.921150000000000000
          Top = 1.763760000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.661410000000000000
        Top = 449.764070000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 541.779530000000000000
          Top = 0.094000000000000000
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 55.795258500000000000
        Top = 71.811070000000000000
        Width = 680.315400000000000000
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 3.779525120000000000
          Width = 673.763760000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'PREVIS'#195'O PARA COMPRA DE INSUMOS QU'#205'MICOS - POR PEDIDOS ABERTOS S' +
              'EQUENCIAIS')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 30.236218030000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 30.236218030000000000
          Width = 394.551020000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 30.236240000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Grupo:')
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 528.661720000000000000
          Top = 30.236240000000000000
          Width = 644.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_GRUPO]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015760240000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPQU."KGT"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527668900000000000
          Top = 18.897650000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ' '#193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 245.669371890000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Receita')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 18.897650000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ' '#193'rea ft'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Top = 18.897650000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 18.897650000000000000
          Width = 56.692901180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Linhas Rebaixe')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 18.897650000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ' '#193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598801100000000000
          Top = 18.897650000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ' '#193'rea ft'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 3.779530000000000000
          Width = 136.063001890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Top = 3.779530000000000000
          Width = 260.787491890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Quanto do pedido falta produzir')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 0.000009760000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 215.433210000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPQUCab
        DataSetName = 'frxDsPQUCab'
        RowCount = 0
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Width = 476.220780000000000000
          Height = 30.236240000000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Pedido: [frxDsPQUCab."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data entrega')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 15.118120000000000000
          Width = 68.031518030000000000
          Height = 15.118110240000000000
          DataField = 'DtEntrega'
          DataSet = frxDsPQUCab
          DataSetName = 'frxDsPQUCab'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQUCab."DtEntrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data ini. prod.')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Top = 15.118120000000000000
          Width = 68.031518030000000000
          Height = 15.118110240000000000
          DataField = 'DtIniProd'
          DataSet = frxDsPQUCab
          DataSetName = 'frxDsPQUCab'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQUCab."DtIniProd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data pedido')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Top = 15.118120000000000000
          Width = 68.031518030000000000
          Height = 15.118110240000000000
          DataField = 'DtPedido'
          DataSet = frxDsPQUCab
          DataSetName = 'frxDsPQUCab'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQUCab."DtPedido"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
        DataSetName = 'frxDsPQUFrm2'
        RowCount = 0
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527668900000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsPQU'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsPQUFrm2."PedA' +
              'reaM2">)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 207.874071890000000000
          Height = 15.118110240000000000
          DataField = 'NOMERECEITA'
          DataSetName = 'frxDsPQUFrm2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQUFrm2."NOMERECEITA"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795221890000000000
          Height = 15.118110240000000000
          DataField = 'Receita'
          DataSetName = 'frxDsPQUFrm2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQUFrm2."Receita"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsPQU'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsPQUFrm2."PedA' +
              'reaP2">)]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsPQU'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsPQUFrm2."PedP' +
              'esoKg">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692871890000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsPQUFrm2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQUFrm2."PedLinhasCul"] - [frxDsPQUFrm2."PedLinhasCab"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsPQU'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsPQUFrm2."PedP' +
              'dArM2">)]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598801100000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsPQU'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsPQUFrm2."PedP' +
              'dArP2">)]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 385.512060000000000000
        Width = 680.315400000000000000
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader3: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 55.795258500000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 3.779525120000000000
          Width = 673.763760000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'PREVIS'#195'O PARA COMPRA DE INSUMOS QU'#205'MICOS - POR PEDIDOS ABERTOS S' +
              'EQUENCIAIS')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 30.236218030000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 30.236218030000000000
          Width = 394.551020000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPedOpen
        DataSetName = 'frxDsPedOpen'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630018900000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soUso">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 34.015770000000000000
          Width = 268.346551890000000000
          Height = 15.118110240000000000
          DataField = 'NomePQ'
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPedOpen."NomePQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157590160000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soEstq">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 619.669450000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soCompra">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 0.000041500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soFut">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000000000
          Top = 0.000041500000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Re' +
              'pet">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedOpen."Insumo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 0.000041500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsPedOpen."Pe' +
              'soNeed">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 0.000041500000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39', <frxDsPedOpen."KgLiqEmb">' +
              ')]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795290240000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPedOpen."NomeCI"'
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630018900000000000
          Top = 22.677138500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg Usar'#225)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677138500000000000
          Width = 302.362321890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157590160000000000
          Top = 22.677138500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Estq atual')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 619.669450000000000000
          Top = 22.677138500000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Comprar')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo futuro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000000000
          Top = 22.677180000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Repeti'#231#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Necess'#225'rio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 22.677180000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Liq. Embal.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object frxDsPQUItsNomeCI: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 801.260360000000000000
          Height = 18.897650000000000000
          DataField = 'NomeCI'
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPedOpen."NomeCI"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 302.362400000000000000
        Width = 680.315400000000000000
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.661410000000000000
        Top = 366.614410000000000000
        Width = 680.315400000000000000
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 541.779530000000000000
          Top = 0.094000000000000000
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object GroupHeader5: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 196.535560000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPedOpen."NOMEIQ"'
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315155910000000000
          Height = 18.897650000000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor: [frxDsPedOpen."NOMEIQ"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter5: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
      end
    end
    object Page3: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader2: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 55.795258500000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 3.779525120000000000
          Width = 673.763760000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'PREVIS'#195'O PARA COMPRA DE INSUMOS QU'#205'MICOS - POR PEDIDOS ABERTOS S' +
              'EQUENCIAIS')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 6.000000000000000000
          Top = 30.236218030000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 30.236218030000000000
          Width = 394.551020000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 30.236240000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Grupo:')
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 528.661720000000000000
          Top = 30.236240000000000000
          Width = 644.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_GRUPO]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.874057250000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPedSeq
        DataSetName = 'frxDsPedSeq'
        RowCount = 0
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897798900000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          DataField = 'PesoUso'
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedSeq."PesoUso"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Width = 275.905611890000000000
          Height = 15.874015750000000000
          DataField = 'NomePQ'
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPedSeq."NomePQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189130160000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          DataField = 'PesoEstq'
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedSeq."PesoEstq"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 0.000041500000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          DataField = 'PesoFut'
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedSeq."PesoFut"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 41.574808030000000000
          Height = 15.874015748031500000
          DataField = 'Insumo'
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedSeq."Insumo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480520000000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          DataField = 'InsumM2PesoKg'
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          DisplayFormat.FormatStr = '%2.6n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedSeq."InsumM2PesoKg"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 46.110297250000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPedSeq."PedidoCod"'
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Width = 476.220780000000000000
          Height = 30.236240000000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pedido: [frxDsPedSeq."PedidoTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data entrega')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 15.118120000000000000
          Width = 68.031518030000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          DisplayFormat.FormatStr = 'dd/mm/yyyy  hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPedSeq."DtEntrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data ini. prod.')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Top = 15.118120000000000000
          Width = 68.031518030000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          DisplayFormat.FormatStr = 'dd/mm/yyyy  hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPedSeq."DtIniProd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data pedido')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Top = 15.118120000000000000
          Width = 68.031518030000000000
          Height = 15.118110240000000000
          DataSet = frxDsPedSeq
          DataSetName = 'frxDsPedSeq'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPedSeq."DtPedido"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897798900000000000
          Top = 30.236240000000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg Usar'#225)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236240000000000000
          Width = 317.480441890000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189130160000000000
          Top = 30.236240000000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Estq atual')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 30.236281500000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo futuro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480520000000000000
          Top = 30.236240000000000000
          Width = 90.708661420000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Uso kg/m'#178)
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.661410000000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 541.779530000000000000
          Top = 0.094000000000000000
          Width = 172.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
    end
    object Page4: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 55.795258500000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 3.779525120000000000
          Width = 673.763760000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'PREVIS'#195'O PARA COMPRA DE INSUMOS QU'#205'MICOS - POR PEDIDOS ABERTOS S' +
              'EQUENCIAIS')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPedComprar
        DataSetName = 'frxDsPedComprar'
        RowCount = 0
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Width = 566.929421890000000000
          Height = 18.897637800000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsPedComprar
          DataSetName = 'frxDsPedComprar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPedComprar."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 566.756030000000000000
          Width = 113.385860940000000000
          Height = 18.897637800000000000
          DataField = 'PesoCompra'
          DataSet = frxDsPedComprar
          DataSetName = 'frxDsPedComprar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPedComprar."PesoCompra"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574776300000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPedComprar."IQ"'
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677138500000000000
          Width = 566.929421890000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          WordWrap = False
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 566.756030000000000000
          Top = 22.677138500000000000
          Width = 113.385860940000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Comprar')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Width = 676.535625910000000000
          Height = 18.897650000000000000
          DataSet = frxDsPedOpen
          DataSetName = 'frxDsPedOpen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPedComprar."NOMEIQ"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236227800000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 566.929421890000000000
          Height = 18.897637800000000000
          DataSet = frxDsPedComprar
          DataSetName = 'frxDsPedComprar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL: [frxDsPedComprar."NOMEIQ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 566.756030000000000000
          Top = 7.559060000000000000
          Width = 113.385860940000000000
          Height = 18.897637800000000000
          DataSet = frxDsPedComprar
          DataSetName = 'frxDsPedComprar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPedComprar."PesoCompra">,MasterData1)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter4: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.661410000000000000
        Top = 332.598640000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object QrPedSeq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM pqurec rec'
      'WHERE Nivel=2')
    Left = 408
    Top = 616
    object QrPedSeqCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPedSeqControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPedSeqInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPedSeqPesoEstq: TFloatField
      FieldName = 'PesoEstq'
      Required = True
    end
    object QrPedSeqPesoUso: TFloatField
      FieldName = 'PesoUso'
      Required = True
    end
    object QrPedSeqPesoFut: TFloatField
      FieldName = 'PesoFut'
      Required = True
    end
    object QrPedSeqPesoNeed: TFloatField
      FieldName = 'PesoNeed'
      Required = True
    end
    object QrPedSeqCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrPedSeqRepet: TFloatField
      FieldName = 'Repet'
      Required = True
    end
    object QrPedSeqNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Required = True
      Size = 100
    end
    object QrPedSeqNomeCI: TWideStringField
      FieldName = 'NomeCI'
      Required = True
      Size = 100
    end
    object QrPedSeqLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrPedSeqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPedSeqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPedSeqUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrPedSeqUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrPedSeqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPedSeqAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPedSeqAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPedSeqAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPedSeqNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPedSeqKgLiqEmb: TFloatField
      FieldName = 'KgLiqEmb'
    end
    object QrPedSeqPesoCompra: TFloatField
      FieldName = 'PesoCompra'
    end
    object QrPedSeqPedidoCod: TIntegerField
      FieldName = 'PedidoCod'
    end
    object QrPedSeqPedidoTxt: TWideStringField
      FieldName = 'PedidoTxt'
      Size = 100
    end
    object QrPedSeqPedidoOrd: TIntegerField
      FieldName = 'PedidoOrd'
    end
    object QrPedSeqDtPedido: TDateField
      FieldName = 'DtPedido'
    end
    object QrPedSeqInsumM2PercRetr: TFloatField
      FieldName = 'InsumM2PercRetr'
    end
    object QrPedSeqInsumM2PedKgM2: TFloatField
      FieldName = 'InsumM2PedKgM2'
    end
    object QrPedSeqInsumM2PesoKg: TFloatField
      FieldName = 'InsumM2PesoKg'
    end
    object QrPedSeqDtIniProd: TDateTimeField
      FieldName = 'DtIniProd'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrPedSeqDtEntrega: TDateTimeField
      FieldName = 'DtEntrega'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
  end
  object frxDsPedSeq: TfrxDBDataset
    UserName = 'frxDsPedSeq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Insumo=Insumo'
      'PesoEstq=PesoEstq'
      'PesoUso=PesoUso'
      'PesoFut=PesoFut'
      'PesoNeed=PesoNeed'
      'CliInt=CliInt'
      'Repet=Repet'
      'NomePQ=NomePQ'
      'NomeCI=NomeCI'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'Nivel=Nivel'
      'KgLiqEmb=KgLiqEmb'
      'PesoCompra=PesoCompra'
      'PedidoCod=PedidoCod'
      'PedidoTxt=PedidoTxt'
      'PedidoOrd=PedidoOrd'
      'DtPedido=DtPedido'
      'DtIniProd=DtIniProd'
      'DtEntrega=DtEntrega'
      'InsumM2PercRetr=InsumM2PercRetr'
      'InsumM2PedKgM2=InsumM2PedKgM2'
      'InsumM2PesoKg=InsumM2PesoKg')
    DataSet = QrPedSeq
    BCDToCurrency = False
    DataSetOptions = []
    Left = 408
    Top = 672
  end
  object frxDsPQUCab: TfrxDBDataset
    UserName = 'frxDsPQUCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'Aplicacao=Aplicacao'
      'Ordem=Ordem'
      'DtPedido=DtPedido'
      'DtIniProd=DtIniProd'
      'DtEntrega=DtEntrega')
    BCDToCurrency = False
    DataSetOptions = []
    Left = 36
    Top = 485
  end
  object QrSumCab: TMySQLQuery
    Database = Dmod.MyDB
    Left = 480
    Top = 492
    object QrSumCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSumCabPedPdArM2: TFloatField
      FieldName = 'PedPdArM2'
    end
  end
  object frxDsPedComprar: TfrxDBDataset
    UserName = 'frxDsPedComprar'
    CloseDataSource = False
    FieldAliases.Strings = (
      'InsumM2PesoKg=InsumM2PesoKg'
      'Codigo=Codigo'
      'Controle=Controle'
      'Insumo=Insumo'
      'PesoEstq=PesoEstq'
      'PesoUso=PesoUso'
      'PesoFut=PesoFut'
      'PesoNeed=PesoNeed'
      'CliInt=CliInt'
      'Repet=Repet'
      'NomePQ=NOMEPQ'
      'NomeCI=NOMECI'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'Nivel=Nivel'
      'KgLiqEmb=KgLiqEmb'
      'PesoCompra=PesoCompra'
      'IQ=IQ'
      'NOMEIQ=NOMEIQ')
    DataSet = QrPedComprar
    BCDToCurrency = False
    DataSetOptions = []
    Left = 272
    Top = 576
  end
  object QrFornece: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NomeFO'
      'FROM entidades'
      'WHERE Fornece2="V"'
      'ORDER BY NomeFO')
    Left = 360
    Top = 152
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNomeFO: TWideStringField
      FieldName = 'NomeFO'
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 360
    Top = 200
  end
  object frxEstq3: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MePesoOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Band4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <frxDsEstq."DdEstq"> < 0 then'
      '  begin              '
      '    MePQ.Font.Color         := clRed;  '
      '    MeNomePQ.Font.Color     := clRed;                        '
      '    MePeso.Font.Color       := clRed;'
      '    MeCustoTotal.Font.Color := clRed;  '
      '    MeCustoKg.Font.Color    := clRed;        '
      '    MeKgSegur.Font.Color    := clRed;  '
      '    MeUsoKgDd.Font.Color    := clRed;                '
      '    MeDias.Font.Color       := clRed;          '
      '  end else'
      '  begin              '
      '    MePQ.Font.Color         := clBlack;  '
      '    MeNomePQ.Font.Color     := clBlack;                        '
      '    MePeso.Font.Color       := clBlack;'
      '    MeCustoTotal.Font.Color := clBlack;  '
      
        '    MeCustoKg.Font.Color    := clBlack;                         ' +
        ' '
      '    MeKgSegur.Font.Color    := clBlack;  '
      '    MeUsoKgDd.Font.Color    := clBlack;          '
      '    MeDias.Font.Color       := clBlack;          '
      '  end;            '
      'end;'
      ''
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      ''
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      '  //'
      
        '  MeObservacao.Visible := <VARF_OBSERVACAO> <> '#39#39';              ' +
        '                                         '
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 100
    Top = 232
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 593.386210000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050880
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 158.740250240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 113.385734020000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 113.385734020000000000
          Width = 613.763760000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 0.928880000000000000
          Top = 98.267604250000000000
          Width = 678.834880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_AMOSTRAS]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 143.621974020000000000
          Width = 321.259786380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 321.259781500000000000
          Top = 143.621974020000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850381500000000000
          Top = 143.621974020000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 472.440981500000000000
          Top = 143.621974020000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo unit')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 83.149506220000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 83.149506220000000000
          Width = 614.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PQNOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE DE USO E CONSUMO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_HORA]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 143.622140000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Dias')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 143.622140000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtd/dia')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 143.622140000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'kg segur.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000000000
          Width = 264.566929133858300000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 321.259766850000000000
          Top = 3.779527560000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850308270000000000
          Top = 3.779527560000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 472.440849690000000000
          Top = 3.779527560000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."Valor">) / SUM' +
              '(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'Band4OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
        RowCount = 0
        object MeNomePQ: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 268.346400550000000000
          Height = 15.118110240000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstq."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePeso: TfrxMemoView
          AllowVectorExport = True
          Left = 321.259786380000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'MePesoOnBeforePrint'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCustoTotal: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850308270000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCustoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 472.440849690000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."Custo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePQ: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstq."PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeDias: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'DiasEstq'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."DiasEstq"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeUsoKgDd: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'KgUsoDd'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."KgUsoDd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeKgSegur: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'KgEstqSegur'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."KgEstqSegur"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 321.259766850000000000
          Top = 3.779530000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850308270000000000
          Top = 3.779530000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 472.440849690000000000
          Top = 3.779530000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."Valor">) / SUM' +
              '(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 54.456710000000000000
          Top = 3.779530000000000000
          Width = 264.566929133858300000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstq."NOMECI"'
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstq."NOMEPQ"'
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000020000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 54.220470000000000000
          Top = 3.779527560000000000
          Width = 264.566929133858300000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 321.259766850000000000
          Top = 3.779527560000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850308270000000000
          Top = 3.779527560000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 472.440849690000000000
          Top = 3.779527560000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."Valor">) / SUM' +
              '(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 548.031850000000000000
        Width = 680.315400000000000000
        object MeObservacao: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[VARF_OBSERVACAO]')
          ParentFont = False
        end
      end
    end
  end
  object frxEstq4: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MePesoOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Band4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <frxDsEstq."DiasProd"> < <frxDsEstq."EstSegur"> then'
      '  begin              '
      '    MePQ.Font.Color         := clRed;  '
      '    MeNomePQ.Font.Color     := clRed;                        '
      '    MePeso.Font.Color       := clRed;'
      '    MeCustoTotal.Font.Color := clRed;  '
      '    MeCustoKg.Font.Color    := clRed;        '
      '    MeKgSegur.Font.Color    := clRed;  '
      '    MeUsoKgDd.Font.Color    := clRed;                '
      '    MeDias.Font.Color       := clRed;          '
      '  end else'
      '  begin              '
      '    MePQ.Font.Color         := clBlack;  '
      '    MeNomePQ.Font.Color     := clBlack;                        '
      '    MePeso.Font.Color       := clBlack;'
      '    MeCustoTotal.Font.Color := clBlack;  '
      
        '    MeCustoKg.Font.Color    := clBlack;                         ' +
        ' '
      '    MeKgSegur.Font.Color    := clBlack;  '
      '    MeUsoKgDd.Font.Color    := clBlack;          '
      '    MeDias.Font.Color       := clBlack;          '
      '  end;            '
      'end;'
      ''
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      ''
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      '  //'
      
        '  MeObservacao.Visible := <VARF_OBSERVACAO> <> '#39#39';              ' +
        '                                         '
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 148
    Top = 232
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 623.622450000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050880
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 188.976490240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 113.385734020000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 113.385734020000000000
          Width = 613.763760000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 0.928880000000000000
          Top = 98.267604250000000000
          Width = 678.834880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_AMOSTRAS]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 158.740094020000000000
          Width = 306.141666380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141661500000000000
          Top = 158.740094020000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732261500000000000
          Top = 158.740094020000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 457.322861500000000000
          Top = 158.740094020000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo unit')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 83.149506220000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 83.149506220000000000
          Width = 614.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PQNOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE DE USO E CONSUMO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_HORA]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 158.740260000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'EDP*')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 158.740260000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd/dia')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 158.740260000000000000
          Width = 41.574798270000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'dd seg.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Top = 143.622140000000000000
          Width = 680.315136380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'EDP*: Dias de estoque baseado em [VARF_EDP] dias produtivos no p' +
              'er'#237'odo de [VARF_PERIODO_PROD]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 487.559370000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000000000
          Width = 264.566929130000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141646850000000000
          Top = 3.779527560000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732188270000000000
          Top = 3.779527560000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 457.322729690000000000
          Top = 3.779527560000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."Valor">) / SUM' +
              '(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 351.496290000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'Band4OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
        RowCount = 0
        object MeNomePQ: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 253.228280550000000000
          Height = 15.118110240000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstq."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePeso: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141666380000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'MePesoOnBeforePrint'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCustoTotal: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732188270000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCustoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 457.322729690000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."Custo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePQ: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstq."PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeDias: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'DiasProd'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."DiasProd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeUsoKgDd: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 68.031510710000000000
          Height = 15.118110240000000000
          DataField = 'KgDdProd'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."KgDdProd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeKgSegur: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Width = 41.574798270000000000
          Height = 15.118110240000000000
          DataField = 'EstSegur'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."EstSegur"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 438.425480000000000000
        Width = 680.315400000000000000
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141646850000000000
          Top = 3.779530000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732188270000000000
          Top = 3.779530000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 457.322729690000000000
          Top = 3.779530000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."Valor">) / SUM' +
              '(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 54.456710000000000000
          Top = 3.779530000000000000
          Width = 249.448809130000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstq."NOMECI"'
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstq."NOMEPQ"'
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 389.291590000000000000
        Width = 680.315400000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000020000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 54.220470000000000000
          Top = 3.779527560000000000
          Width = 253.228339130000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141646850000000000
          Top = 3.779527560000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732188270000000000
          Top = 3.779527560000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 457.322729690000000000
          Top = 3.779527560000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."Valor">) / SUM' +
              '(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 578.268090000000000000
        Width = 680.315400000000000000
        object MeObservacao: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[VARF_OBSERVACAO]')
          ParentFont = False
        end
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 340
    Top = 277
  end
  object PMOrfaos: TPopupMenu
    OnPopup = PMOrfaosPopup
    Left = 85
    Top = 133
    object VerificacadastrodeUsoeConsumo1: TMenuItem
      Caption = 'Verifica cadastro de Uso e Consumo'
      OnClick = VerificacadastrodeUsoeConsumo1Click
    end
    object CriaClienteInterno1: TMenuItem
      Caption = 'Cria Cliente Interno'
      object Doitemselecionado1: TMenuItem
        Caption = 'Do &Item selecionado'
        OnClick = Doitemselecionado1Click
      end
      object Detodospossveis1: TMenuItem
        Caption = 'De &Todos poss'#237'veis'
        OnClick = Detodospossveis1Click
      end
    end
    object Substituimovimentosporoutroinsumo1: TMenuItem
      Caption = 'Substitui movimentos por outro insumo'
      OnClick = Substituimovimentosporoutroinsumo1Click
    end
    object Excluilanamentossemmovimentosefortexto1: TMenuItem
      Caption = '&Exclui lan'#231'amentos sem movimento (se for texto)'
      OnClick = Excluilanamentossemmovimentosefortexto1Click
    end
  end
  object QrErrPqxDad: TMySQLQuery
    Database = Dmod.MyDB
    Left = 428
    Top = 248
  end
  object QrDifPqxPqw: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _pqx_pqw_compara_pqx;'
      ''
      'CREATE TABLE _pqx_pqw_compara_pqx'
      'SELECT CliOrig, Insumo, SUM(SdoPeso) SdoPeso, '
      'SUM(SdoValr) SdoValr'
      'FROM bluederm_noroeste.pqx'
      'WHERE SdoPeso<>0'
      'GROUP BY CliOrig, Insumo'
      ';'
      'SELECT pqx.Insumo, pqx.SdoPeso, pqx.SdoValr, '
      'pqc.Peso, pqc.Valor, pq.Nome NO_PQ, pqc.CI,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CI'
      'FROM _pqx_pqw_compara_pqx pqx'
      'LEFT JOIN bluederm_noroeste.pqcli pqc ON '
      '  pqc.CI=pqx.CliOrig'
      '  AND pqc.PQ=pqx.Insumo'
      'LEFT JOIN bluederm_noroeste.pq pq ON pq.Codigo=pqc.PQ'
      'LEFT JOIN bluederm_noroeste.entidades ent ON ent.Codigo=pqc.CI'
      'WHERE pqx.SdoPeso<>pqc.Peso'
      ';')
    Left = 744
    Top = 100
    object QrDifPqxPqwInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrDifPqxPqwSdoPeso: TFloatField
      FieldName = 'SdoPeso'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrDifPqxPqwSdoValr: TFloatField
      FieldName = 'SdoValr'
    end
    object QrDifPqxPqwPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrDifPqxPqwValor: TFloatField
      FieldName = 'Valor'
    end
    object QrDifPqxPqwNO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrDifPqxPqwCI: TIntegerField
      FieldName = 'CI'
    end
    object QrDifPqxPqwNO_CI: TWideStringField
      FieldName = 'NO_CI'
      Size = 100
    end
  end
  object DsDifPqxPqw: TDataSource
    DataSet = QrDifPqxPqw
    Left = 744
    Top = 148
  end
  object frxDsDifPqxPqw: TfrxDBDataset
    UserName = 'frxDsDifPqxPqw'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Insumo=Insumo'
      'SdoPeso=SdoPeso'
      'SdoValr=SdoValr'
      'Peso=Peso'
      'Valor=Valor'
      'NO_PQ=NO_PQ'
      'CI=CI'
      'NO_CI=NO_CI')
    DataSet = QrDifPqxPqw
    BCDToCurrency = False
    DataSetOptions = []
    Left = 744
    Top = 196
  end
  object frxDifPqxPqw: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    OldStyleProgress = True
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxMoviPQGetValue
    Left = 744
    Top = 244
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsDifPqxPqw
        DataSetName = 'frxDsDifPqxPqw'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 366.614410000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050880
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 158.740250240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 113.385734020000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 113.385734020000000000
          Width = 616.063348500000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 0.928880000000000000
          Top = 98.267604250000000000
          Width = 678.834880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_AMOSTRAS]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 143.621974020000000000
          Width = 260.787367400000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559101500000000000
          Top = 143.621974020000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde cliente')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811111500000000000
          Top = 143.621974020000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde NFs')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063121500000000000
          Top = 143.621974020000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataSet = frxDsDifPqxPqw
          DataSetName = 'frxDsDifPqxPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Diferen'#231'a')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 83.149506220000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 83.149506220000000000
          Width = 614.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PQNOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Diverg'#234'ncias de Estoque Cliente x NF')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_HORA]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Top = 143.622140000000000000
          Width = 226.771773150000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente Interno')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000000000
          Width = 461.102432990000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559086850000000000
          Top = 3.779527560000000000
          Width = 64.251968500000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDifPqxPqw."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811038270000000000
          Top = 3.779527560000000000
          Width = 64.251968500000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDifPqxPqw."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 616.062989690000000000
          Top = 3.779527560000000000
          Width = 64.251968500000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDifPqxPqw."Peso">)-SUM(<frxDsDifPqxPqw."SdoPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsDifPqxPqw
        DataSetName = 'frxDsDifPqxPqw'
        RowCount = 0
        object MeNomePQ: TfrxMemoView
          AllowVectorExport = True
          Left = 49.133855830000000000
          Width = 211.653509130000000000
          Height = 15.118110240000000000
          DataField = 'NO_PQ'
          DataSet = frxDsDifPqxPqw
          DataSetName = 'frxDsDifPqxPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDifPqxPqw."NO_PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePeso: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559106380000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataSet = frxDsDifPqxPqw
          DataSetName = 'frxDsDifPqxPqw'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDifPqxPqw."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811038270000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataField = 'SdoPeso'
          DataSet = frxDsDifPqxPqw
          DataSetName = 'frxDsDifPqxPqw'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDifPqxPqw."SdoPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 616.062989690000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(<frxDsDifPqxPqw."Peso">)-(<frxDsDifPqxPqw."SdoPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePQ: TfrxMemoView
          AllowVectorExport = True
          Width = 49.133858267716540000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsDifPqxPqw
          DataSetName = 'frxDsDifPqxPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDifPqxPqw."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCodProprio: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Width = 196.535533150000000000
          Height = 15.118110240000000000
          DataField = 'NO_CI'
          DataSet = frxDsDifPqxPqw
          DataSetName = 'frxDsDifPqxPqw'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDifPqxPqw."NO_CI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 30.236215590000000000
          Height = 15.118110240000000000
          DataField = 'CI'
          DataSet = frxDsDifPqxPqw
          DataSetName = 'frxDsDifPqxPqw'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDifPqxPqw."CI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object DsEstqNFs: TDataSource
    DataSet = QrEstqNFs
    Left = 504
    Top = 360
  end
end
