object FmMPReclasIns: TFmMPReclasIns
  Left = 339
  Top = 185
  Caption = 'COU-CLASS-003 :: Reclassifica'#231#227'o'
  ClientHeight = 560
  ClientWidth = 504
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 504
    Height = 404
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 88
      Width = 504
      Height = 144
      Align = alTop
      Caption = ' Produto a ser classificado: '
      TabOrder = 1
      object Label3: TLabel
        Left = 8
        Top = 16
        Width = 139
        Height = 13
        Caption = 'Centro de estoque de origem:'
      end
      object Label1: TLabel
        Left = 8
        Top = 56
        Width = 157
        Height = 13
        Caption = 'Reduzido do Produto de Origem):'
      end
      object EdStqCenCadA: TdmkEditCB
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenCadA
        IgnoraDBLookupComboBox = False
      end
      object CBStqCenCadA: TdmkDBLookupComboBox
        Left = 68
        Top = 32
        Width = 425
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsStqCenCadA
        TabOrder = 1
        dmkEditCB = EdStqCenCadA
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGraGruXA: TdmkEditCB
        Left = 8
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXAChange
        OnEnter = EdGraGruXAEnter
        OnExit = EdGraGruXAExit
        DBLookupComboBox = CBGraGruXA
        IgnoraDBLookupComboBox = False
      end
      object CBGraGruXA: TdmkDBLookupComboBox
        Left = 68
        Top = 72
        Width = 425
        Height = 21
        KeyField = 'GraGruX'
        ListField = 'Nome'
        ListSource = DsGraGruXA
        TabOrder = 3
        dmkEditCB = EdGraGruXA
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object Panel4: TPanel
        Left = 2
        Top = 96
        Width = 500
        Height = 46
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 4
        object LaPecas: TLabel
          Left = 4
          Top = 4
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object LaAreaM2: TLabel
          Left = 92
          Top = 4
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object LaAreaP2: TLabel
          Left = 180
          Top = 4
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
          Enabled = False
        end
        object LaPeso: TLabel
          Left = 260
          Top = 4
          Width = 27
          Height = 13
          Caption = 'Peso:'
        end
        object EdPecasA: TdmkEdit
          Left = 4
          Top = 20
          Width = 85
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdPecasAChange
        end
        object EdAreaM2A: TdmkEditCalc
          Left = 92
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdAreaM2AChange
          dmkEditCalcA = EdAreaP2A
          CalcType = ctM2toP2
          CalcFrac = cfQuarto
        end
        object EdAreaP2A: TdmkEditCalc
          Left = 176
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdAreaP2AChange
          dmkEditCalcA = EdAreaM2A
          CalcType = ctP2toM2
          CalcFrac = cfCento
        end
        object EdPesoA: TdmkEdit
          Left = 260
          Top = 20
          Width = 85
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdPesoAChange
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 232
      Width = 504
      Height = 144
      Align = alTop
      Caption = ' Produto resultante da classifica'#231#227'o: '
      TabOrder = 2
      object Label4: TLabel
        Left = 8
        Top = 16
        Width = 142
        Height = 13
        Caption = 'Centro de estoque de destino:'
      end
      object Label5: TLabel
        Left = 8
        Top = 56
        Width = 83
        Height = 13
        Caption = 'Produto (destino):'
      end
      object EdStqCenCadB: TdmkEditCB
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenCadB
        IgnoraDBLookupComboBox = False
      end
      object CBStqCenCadB: TdmkDBLookupComboBox
        Left = 68
        Top = 32
        Width = 425
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsStqCenCadB
        TabOrder = 1
        dmkEditCB = EdStqCenCadB
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGraGruXB: TdmkEditCB
        Left = 8
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXBChange
        OnEnter = EdGraGruXBEnter
        OnExit = EdGraGruXBExit
        DBLookupComboBox = CBGraGruXB
        IgnoraDBLookupComboBox = False
      end
      object CBGraGruXB: TdmkDBLookupComboBox
        Left = 68
        Top = 72
        Width = 425
        Height = 21
        KeyField = 'GraGruX'
        ListField = 'Nome'
        ListSource = DsGraGruXB
        TabOrder = 3
        dmkEditCB = EdGraGruXB
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object Panel5: TPanel
        Left = 2
        Top = 96
        Width = 500
        Height = 46
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 4
        object Label8: TLabel
          Left = 4
          Top = 4
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object Label9: TLabel
          Left = 92
          Top = 4
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object Label10: TLabel
          Left = 180
          Top = 4
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
          Enabled = False
        end
        object Label11: TLabel
          Left = 260
          Top = 4
          Width = 27
          Height = 13
          Caption = 'Peso:'
        end
        object EdPecasB: TdmkEdit
          Left = 4
          Top = 20
          Width = 85
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdAreaM2B: TdmkEditCalc
          Left = 92
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          dmkEditCalcA = EdAreaP2B
          CalcType = ctM2toP2
          CalcFrac = cfQuarto
        end
        object EdAreaP2B: TdmkEditCalc
          Left = 176
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          dmkEditCalcA = EdAreaM2B
          CalcType = ctP2toM2
          CalcFrac = cfCento
        end
        object EdPesoB: TdmkEdit
          Left = 260
          Top = 20
          Width = 85
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 504
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label12: TLabel
        Left = 8
        Top = 44
        Width = 188
        Height = 13
        Caption = 'Data e hora do lan'#231'amento no estoque:'
      end
      object CBFilial: TdmkDBLookupComboBox
        Left = 67
        Top = 20
        Width = 426
        Height = 21
        KeyField = 'Filial'
        ListField = 'NomeEmp'
        ListSource = DModG.DsFiliLog
        TabOrder = 1
        dmkEditCB = EdFilial
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFilial: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFilial
        IgnoraDBLookupComboBox = False
      end
      object TPData: TdmkEditDateTimePicker
        Left = 8
        Top = 60
        Width = 133
        Height = 21
        Date = 40638.507592673610000000
        Time = 40638.507592673610000000
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdHora: TdmkEdit
        Left = 144
        Top = 60
        Width = 53
        Height = 21
        TabOrder = 3
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object BitBtn1: TBitBtn
        Left = 260
        Top = 56
        Width = 233
        Height = 25
        Caption = 'Vendas do m'#234's da data selecionada'
        TabOrder = 4
        OnClick = BitBtn1Click
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 504
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 456
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 408
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 420
        Height = 32
        Caption = 'Gerenciamento de Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 420
        Height = 32
        Caption = 'Gerenciamento de Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 420
        Height = 32
        Caption = 'Gerenciamento de Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 452
    Width = 504
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 500
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 496
    Width = 504
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 500
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 356
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object CkContinuar: TCheckBox
        Left = 172
        Top = 16
        Width = 129
        Height = 17
        Caption = 'Continuar inserindo.'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
    end
  end
  object QrGraGruXA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT ggx.Controle GraGruX, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,' +
        ' '
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, '
      'HowBxaEstq, GerBxaEstq, gtc.Nome NOMEGRATAMCAD, '
      'gtc.CodUsu CODUSUGRATAMCAD, gg1.CST_A, gg1.CST_B, '
      'gg1.UnidMed, gg1.NCM, gg1.Peso, gg1.FatorClas,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, '
      'unm.Nome NOMEUNIDMED '
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      '/*WHERE pgt.TipPrd=1 */'
      'ORDER BY gg1.Nome')
    Left = 12
    Top = 8
    object QrGraGruXAGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXANivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGruXANivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGruXANivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGruXANome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGruXAPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrGraGruXAGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Required = True
    end
    object QrGraGruXANOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGruXACODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGruXACST_A: TSmallintField
      FieldName = 'CST_A'
      Required = True
    end
    object QrGraGruXACST_B: TSmallintField
      FieldName = 'CST_B'
      Required = True
    end
    object QrGraGruXAUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
    object QrGraGruXANCM: TWideStringField
      FieldName = 'NCM'
      Required = True
      Size = 10
    end
    object QrGraGruXAPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrGraGruXASIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 3
    end
    object QrGraGruXACODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGruXANOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXAHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGruXAGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXAFatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
  end
  object DsGraGruXA: TDataSource
    DataSet = QrGraGruXA
    Left = 40
    Top = 8
  end
  object QrStqCenCadA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 68
    Top = 8
    object QrStqCenCadACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadACodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenCadANome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCadA: TDataSource
    DataSet = QrStqCenCadA
    Left = 96
    Top = 8
  end
  object QrStqCenCadB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 412
    Top = 8
    object QrStqCenCadBCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadBCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadBNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCadB: TDataSource
    DataSet = QrStqCenCadB
    Left = 440
    Top = 8
  end
  object DsGraGruXB: TDataSource
    DataSet = QrGraGruXB
    Left = 384
    Top = 8
  end
  object QrGraGruXB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT ggx.Controle GraGruX, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,' +
        ' '
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, '
      'HowBxaEstq, GerBxaEstq, gtc.Nome NOMEGRATAMCAD, '
      'gtc.CodUsu CODUSUGRATAMCAD, gg1.CST_A, gg1.CST_B, '
      'gg1.UnidMed, gg1.NCM, gg1.Peso, gg1.FatorClas,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, '
      'unm.Nome NOMEUNIDMED '
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      '/*WHERE pgt.TipPrd=1 */'
      'ORDER BY gg1.Nome')
    Left = 356
    Top = 8
    object QrGraGruXBGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXBNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGruXBNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGruXBNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGruXBNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraGruXBPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGruXBGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrGraGruXBHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGruXBGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXBNOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGruXBCODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGruXBCST_A: TSmallintField
      FieldName = 'CST_A'
    end
    object QrGraGruXBCST_B: TSmallintField
      FieldName = 'CST_B'
    end
    object QrGraGruXBUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGruXBNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGruXBPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrGraGruXBFatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
    object QrGraGruXBSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXBCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGruXBNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
end
