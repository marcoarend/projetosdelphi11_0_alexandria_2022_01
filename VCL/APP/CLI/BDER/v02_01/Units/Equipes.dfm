object FmEquipes: TFmEquipes
  Left = 368
  Top = 194
  Caption = 'FUN-CADAS-001 :: Equipes'
  ClientHeight = 443
  ClientWidth = 728
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 728
    Height = 347
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 728
      Height = 253
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 120
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 120
        Top = 24
        Width = 300
        Height = 21
        MaxLength = 255
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 48
        Width = 405
        Height = 61
        Caption = ' Hor'#225'rio A: '
        TabOrder = 2
        object Label32: TLabel
          Left = 8
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 1:'
          FocusControl = EdEntraA1
        end
        object Label3: TLabel
          Left = 72
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Sa'#237'da 1:'
          FocusControl = EdSaidaA1
        end
        object Label4: TLabel
          Left = 136
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 2:'
          FocusControl = EdEntraA2
        end
        object Label5: TLabel
          Left = 200
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Sa'#237'da 2:'
          FocusControl = EdSaidaA2
        end
        object EdEntraA1: TdmkEdit
          Left = 8
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdEntraA1Exit
        end
        object EdSaidaA1: TdmkEdit
          Left = 72
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdSaidaA1Exit
        end
        object EdEntraA2: TdmkEdit
          Left = 136
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdEntraA2Exit
        end
        object EdSaidaA2: TdmkEdit
          Left = 200
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdSaidaA2Exit
        end
      end
      object GroupBox2: TGroupBox
        Left = 16
        Top = 112
        Width = 405
        Height = 61
        Caption = ' Hor'#225'rio B: '
        TabOrder = 3
        object Label6: TLabel
          Left = 8
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 1:'
          FocusControl = EdEntraB1
        end
        object Label7: TLabel
          Left = 72
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Sa'#237'da 1:'
          FocusControl = EdSaidaB1
        end
        object Label8: TLabel
          Left = 136
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 2:'
          FocusControl = EdEntraB2
        end
        object Label11: TLabel
          Left = 200
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Sa'#237'da 2:'
          FocusControl = EdSaidaB2
        end
        object EdEntraB1: TdmkEdit
          Left = 8
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdEntraB1Exit
        end
        object EdSaidaB1: TdmkEdit
          Left = 72
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdSaidaB1Exit
        end
        object EdEntraB2: TdmkEdit
          Left = 136
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdEntraB2Exit
        end
        object EdSaidaB2: TdmkEdit
          Left = 200
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdSaidaB2Exit
        end
      end
      object GroupBox3: TGroupBox
        Left = 16
        Top = 176
        Width = 405
        Height = 61
        Caption = ' Hor'#225'rio C: '
        TabOrder = 4
        object Label12: TLabel
          Left = 8
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 1:'
          FocusControl = EdEntraC1
        end
        object Label13: TLabel
          Left = 72
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Sa'#237'da 1:'
          FocusControl = EdSaidaC1
        end
        object Label17: TLabel
          Left = 136
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 2:'
          FocusControl = EdEntraC2
        end
        object Label18: TLabel
          Left = 200
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Sa'#237'da 2:'
          FocusControl = EdSaidaC2
        end
        object EdEntraC1: TdmkEdit
          Left = 8
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdEntraC1Exit
        end
        object EdSaidaC1: TdmkEdit
          Left = 72
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdSaidaC1Exit
        end
        object EdEntraC2: TdmkEdit
          Left = 136
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdEntraC2Exit
        end
        object EdSaidaC2: TdmkEdit
          Left = 200
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdSaidaC2Exit
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 283
      Width = 728
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 724
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 580
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 8
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 728
    Height = 347
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 728
      Height = 249
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 120
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsEquipes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 120
        Top = 24
        Width = 300
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsEquipes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object GroupBox4: TGroupBox
        Left = 16
        Top = 48
        Width = 405
        Height = 61
        Caption = ' Hor'#225'rio A: '
        TabOrder = 2
        object Label19: TLabel
          Left = 12
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 1:'
          FocusControl = DBEdit1
        end
        object Label20: TLabel
          Left = 80
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Sa'#237'da 1:'
          FocusControl = DBEdit2
        end
        object Label21: TLabel
          Left = 148
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 2:'
          FocusControl = DBEdit3
        end
        object Label22: TLabel
          Left = 216
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 2:'
          FocusControl = DBEdit4
        end
        object DBEdit1: TDBEdit
          Left = 12
          Top = 32
          Width = 64
          Height = 21
          DataField = 'ENTRAA1_TXT'
          DataSource = DsEquipes
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 80
          Top = 32
          Width = 64
          Height = 21
          DataField = 'SAIDAA1_TXT'
          DataSource = DsEquipes
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 148
          Top = 32
          Width = 64
          Height = 21
          DataField = 'ENTRAA2_TXT'
          DataSource = DsEquipes
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 216
          Top = 32
          Width = 64
          Height = 21
          DataField = 'SAIDAA2_TXT'
          DataSource = DsEquipes
          TabOrder = 3
        end
      end
      object GroupBox5: TGroupBox
        Left = 16
        Top = 112
        Width = 405
        Height = 61
        Caption = ' Hor'#225'rio B: '
        TabOrder = 3
        object Label23: TLabel
          Left = 216
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 2:'
          FocusControl = DBEdit5
        end
        object Label24: TLabel
          Left = 148
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 2:'
          FocusControl = DBEdit6
        end
        object Label25: TLabel
          Left = 80
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Sa'#237'da 1:'
          FocusControl = DBEdit7
        end
        object Label26: TLabel
          Left = 12
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 1:'
          FocusControl = DBEdit8
        end
        object DBEdit5: TDBEdit
          Left = 216
          Top = 32
          Width = 64
          Height = 21
          DataField = 'SAIDAB2_TXT'
          DataSource = DsEquipes
          TabOrder = 0
        end
        object DBEdit6: TDBEdit
          Left = 148
          Top = 32
          Width = 64
          Height = 21
          DataField = 'ENTRAB2_TXT'
          DataSource = DsEquipes
          TabOrder = 1
        end
        object DBEdit7: TDBEdit
          Left = 80
          Top = 32
          Width = 64
          Height = 21
          DataField = 'SAIDAB1_TXT'
          DataSource = DsEquipes
          TabOrder = 2
        end
        object DBEdit8: TDBEdit
          Left = 12
          Top = 32
          Width = 64
          Height = 21
          DataField = 'ENTRAB1_TXT'
          DataSource = DsEquipes
          TabOrder = 3
        end
      end
      object GroupBox6: TGroupBox
        Left = 16
        Top = 176
        Width = 405
        Height = 61
        Caption = ' Hor'#225'rio C: '
        TabOrder = 4
        object Label27: TLabel
          Left = 216
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 2:'
          FocusControl = DBEdit9
        end
        object Label28: TLabel
          Left = 148
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 2:'
          FocusControl = DBEdit10
        end
        object Label29: TLabel
          Left = 80
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Sa'#237'da 1:'
          FocusControl = DBEdit11
        end
        object Label30: TLabel
          Left = 12
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Entrada 1:'
          FocusControl = DBEdit12
        end
        object DBEdit9: TDBEdit
          Left = 216
          Top = 32
          Width = 64
          Height = 21
          DataField = 'SAIDAC2_TXT'
          DataSource = DsEquipes
          TabOrder = 0
        end
        object DBEdit10: TDBEdit
          Left = 148
          Top = 32
          Width = 64
          Height = 21
          DataField = 'ENTRAC2_TXT'
          DataSource = DsEquipes
          TabOrder = 1
        end
        object DBEdit11: TDBEdit
          Left = 80
          Top = 32
          Width = 64
          Height = 21
          DataField = 'SAIDAC1_TXT'
          DataSource = DsEquipes
          TabOrder = 2
        end
        object DBEdit12: TDBEdit
          Left = 12
          Top = 32
          Width = 64
          Height = 21
          DataField = 'ENTRAC1_TXT'
          DataSource = DsEquipes
          TabOrder = 3
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 283
      Width = 728
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 224
        Top = 15
        Width = 502
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 369
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 124
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 244
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 728
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 680
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 464
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 98
        Height = 32
        Caption = 'Equipes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 98
        Height = 32
        Caption = 'Equipes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 98
        Height = 32
        Caption = 'Equipes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 728
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 724
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsEquipes: TDataSource
    DataSet = QrEquipes
    Left = 488
    Top = 161
  end
  object QrEquipes: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrEquipesBeforeOpen
    AfterOpen = QrEquipesAfterOpen
    AfterScroll = QrEquipesAfterScroll
    OnCalcFields = QrEquipesCalcFields
    SQL.Strings = (
      'SELECT * FROM equipes'
      'WHERE Codigo > 0')
    Left = 460
    Top = 161
    object QrEquipesLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEquipesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEquipesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEquipesUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEquipesUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEquipesCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object QrEquipesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrEquipesEntraA1: TIntegerField
      FieldName = 'EntraA1'
    end
    object QrEquipesSaidaA1: TIntegerField
      FieldName = 'SaidaA1'
    end
    object QrEquipesEntraA2: TIntegerField
      FieldName = 'EntraA2'
    end
    object QrEquipesSaidaA2: TIntegerField
      FieldName = 'SaidaA2'
    end
    object QrEquipesEntraB1: TIntegerField
      FieldName = 'EntraB1'
    end
    object QrEquipesSaidaB1: TIntegerField
      FieldName = 'SaidaB1'
    end
    object QrEquipesEntraB2: TIntegerField
      FieldName = 'EntraB2'
    end
    object QrEquipesSaidaB2: TIntegerField
      FieldName = 'SaidaB2'
    end
    object QrEquipesEntraC1: TIntegerField
      FieldName = 'EntraC1'
    end
    object QrEquipesSaidaC1: TIntegerField
      FieldName = 'SaidaC1'
    end
    object QrEquipesEntraC2: TIntegerField
      FieldName = 'EntraC2'
    end
    object QrEquipesSaidaC2: TIntegerField
      FieldName = 'SaidaC2'
    end
    object QrEquipesENTRAA1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENTRAA1_TXT'
      Size = 5
      Calculated = True
    end
    object QrEquipesSAIDAA1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SAIDAA1_TXT'
      Size = 5
      Calculated = True
    end
    object QrEquipesENTRAA2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENTRAA2_TXT'
      Size = 5
      Calculated = True
    end
    object QrEquipesSAIDAA2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SAIDAA2_TXT'
      Size = 5
      Calculated = True
    end
    object QrEquipesENTRAB1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENTRAB1_TXT'
      Size = 5
      Calculated = True
    end
    object QrEquipesSAIDAB1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SAIDAB1_TXT'
      Size = 5
      Calculated = True
    end
    object QrEquipesENTRAB2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENTRAB2_TXT'
      Size = 5
      Calculated = True
    end
    object QrEquipesSAIDAB2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SAIDAB2_TXT'
      Size = 5
      Calculated = True
    end
    object QrEquipesENTRAC1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENTRAC1_TXT'
      Size = 5
      Calculated = True
    end
    object QrEquipesSAIDAC1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SAIDAC1_TXT'
      Size = 5
      Calculated = True
    end
    object QrEquipesENTRAC2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENTRAC2_TXT'
      Size = 5
      Calculated = True
    end
    object QrEquipesSAIDAC2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SAIDAC2_TXT'
      Size = 5
      Calculated = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = FmDefeitostip.Panel2
    CanUpd01 = FmDefeitostip.Panel3
    CanDel01 = FmDefeitostip.SpeedButton1
    Left = 516
    Top = 161
  end
end
