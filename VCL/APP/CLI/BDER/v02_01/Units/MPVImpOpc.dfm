object FmMPVImpOpc: TFmMPVImpOpc
  Left = 339
  Top = 185
  Caption = 'VEN-COURO-153 :: Usu'#225'rios de Faturamentos de Pedidos'
  ClientHeight = 453
  ClientWidth = 634
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 634
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 586
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 538
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 464
        Height = 32
        Caption = 'Usu'#225'rios de Faturamentos de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 464
        Height = 32
        Caption = 'Usu'#225'rios de Faturamentos de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 464
        Height = 32
        Caption = 'Usu'#225'rios de Faturamentos de Pedidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 634
    Height = 291
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 634
      Height = 291
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 634
        Height = 291
        Align = alClient
        TabOrder = 0
        object dmkDBGrid2: TdmkDBGrid
          Left = 2
          Top = 15
          Width = 630
          Height = 274
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Numero'
              Title.Caption = 'Usu'#225'rio'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Login'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFUNCIONARIO'
              Title.Caption = 'Nome do funcion'#225'rio'
              Width = 285
              Visible = True
            end>
          Color = clWindow
          DataSource = DsMPVImpOpc
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Numero'
              Title.Caption = 'Usu'#225'rio'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Login'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFUNCIONARIO'
              Title.Caption = 'Nome do funcion'#225'rio'
              Width = 285
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 339
    Width = 634
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 630
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 383
    Width = 634
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 488
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 486
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtUsuario: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Usu'#225'rio'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtUsuarioClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 464
    Top = 187
  end
  object PMUsuario: TPopupMenu
    Left = 216
    Top = 200
    object Adicionausurio1: TMenuItem
      Caption = '&Adiciona usu'#225'rio'
      OnClick = Adicionausurio1Click
    end
    object Retirausurio1: TMenuItem
      Caption = '&Retira usu'#225'rio'
      OnClick = Retirausurio1Click
    end
  end
  object DsMPVImpOpc: TDataSource
    DataSet = QrMPVImpOpc
    Left = 368
    Top = 184
  end
  object QrMPVImpOpc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT op.Codigo, si.Numero, si.Login, en.Nome NOMEFUNCIONARIO '
      'FROM mpvimpopc op'
      'LEFT JOIN senhas si ON si.Numero = op.Usuario'
      'LEFT JOIN entidades en ON en.Codigo=si.Funcionario'
      'ORDER BY NOMEFUNCIONARIO, Login')
    Left = 340
    Top = 184
    object QrMPVImpOpcNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrMPVImpOpcLogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
    object QrMPVImpOpcNOMEFUNCIONARIO: TWideStringField
      FieldName = 'NOMEFUNCIONARIO'
      Size = 100
    end
    object QrMPVImpOpcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
end
