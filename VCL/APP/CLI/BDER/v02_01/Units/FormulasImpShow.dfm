object FrFormulasImpShow: TFrFormulasImpShow
  Left = 387
  Top = 258
  BorderStyle = bsDialog
  Caption = 'QUI-RECEI-999 :: Imprime em FR'
  ClientHeight = 509
  ClientWidth = 717
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 717
    Height = 41
    Align = alTop
    TabOrder = 0
    object Button1: TButton
      Left = 88
      Top = 4
      Width = 75
      Height = 31
      Caption = 'Preview'
      TabOrder = 0
      OnClick = Button1Click
    end
    object BitBtn1: TBitBtn
      Left = 168
      Top = 4
      Width = 75
      Height = 31
      Caption = '&Sair'
      TabOrder = 1
      OnClick = BitBtn1Click
    end
    object Button2: TButton
      Left = 8
      Top = 4
      Width = 75
      Height = 31
      Caption = '&Abrir'
      TabOrder = 2
      OnClick = Button2Click
    end
    object Edit1: TEdit
      Left = 276
      Top = 12
      Width = 429
      Height = 21
      TabOrder = 3
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 41
    Width = 717
    Height = 468
    Align = alClient
    DataSource = DataSource1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object QrFormulas: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFormulasCalcFields
    SQL.Strings = (
      'SELECT IF(ci.Tipo=0, ci.RazaoSocial, ci.Nome) NOMECI, '
      'ls.Nome NOMESETOR, gcc.Nome NoGraCorCad, fo.* '
      'FROM Formulas fo'
      'LEFT JOIN listasetores ls ON ls.Codigo=fo.Setor'
      'LEFT JOIN entidades    ci ON ci.Codigo=fo.ClienteI'
      'LEFT JOIN  gracorcad gcc ON gcc.Codigo=fo.GraCorCad'
      'WHERE fo.Numero=:P0')
    Left = 232
    Top = 84
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFormulasNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'formulas.Numero'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'formulas.Nome'
      Size = 30
    end
    object QrFormulasClienteI: TIntegerField
      FieldName = 'ClienteI'
      Origin = 'formulas.ClienteI'
    end
    object QrFormulasTipificacao: TIntegerField
      FieldName = 'Tipificacao'
      Origin = 'formulas.Tipificacao'
    end
    object QrFormulasDataI: TDateField
      FieldName = 'DataI'
      Origin = 'formulas.DataI'
    end
    object QrFormulasDataA: TDateField
      FieldName = 'DataA'
      Origin = 'formulas.DataA'
    end
    object QrFormulasTecnico: TWideStringField
      FieldName = 'Tecnico'
      Origin = 'formulas.Tecnico'
    end
    object QrFormulasTempoR: TIntegerField
      FieldName = 'TempoR'
      Origin = 'formulas.TempoR'
    end
    object QrFormulasTempoP: TIntegerField
      FieldName = 'TempoP'
      Origin = 'formulas.TempoP'
    end
    object QrFormulasTempoT: TIntegerField
      FieldName = 'TempoT'
      Origin = 'formulas.TempoT'
    end
    object QrFormulasHorasR: TIntegerField
      FieldName = 'HorasR'
      Origin = 'formulas.HorasR'
    end
    object QrFormulasHorasP: TIntegerField
      FieldName = 'HorasP'
      Origin = 'formulas.HorasP'
    end
    object QrFormulasHorasT: TIntegerField
      FieldName = 'HorasT'
      Origin = 'formulas.HorasT'
    end
    object QrFormulasHidrica: TIntegerField
      FieldName = 'Hidrica'
      Origin = 'formulas.Hidrica'
    end
    object QrFormulasLinhaTE: TIntegerField
      FieldName = 'LinhaTE'
      Origin = 'formulas.LinhaTE'
    end
    object QrFormulasCaldeira: TIntegerField
      FieldName = 'Caldeira'
      Origin = 'formulas.Caldeira'
    end
    object QrFormulasSetor: TIntegerField
      FieldName = 'Setor'
      Origin = 'formulas.Setor'
    end
    object QrFormulasEspessura: TIntegerField
      FieldName = 'Espessura'
      Origin = 'formulas.Espessura'
    end
    object QrFormulasPeso: TFloatField
      FieldName = 'Peso'
      Origin = 'formulas.Peso'
    end
    object QrFormulasQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'formulas.Qtde'
    end
    object QrFormulasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'formulas.Lk'
    end
    object QrFormulasDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'formulas.DataCad'
    end
    object QrFormulasDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'formulas.DataAlt'
    end
    object QrFormulasUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'formulas.UserCad'
    end
    object QrFormulasUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'formulas.UserAlt'
    end
    object QrFormulasNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
      Origin = 'listasetores.Nome'
    end
    object QrFormulasNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrFormulasGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrFormulasNoGraCorCad: TWideStringField
      FieldName = 'NoGraCorCad'
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'GraCorCad'
      Size = 60
    end
    object QrFormulasNumCODIF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NumCODIF'
      Size = 11
      Calculated = True
    end
    object QrFormulasEspRebaixe: TIntegerField
      FieldName = 'EspRebaixe'
    end
  end
  object QrFormulasIts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFormulasItsCalcFields
    SQL.Strings = (
      'SELECT pq.Ativo, pq.Nome, pcl.PQ PQCI, '
      ''
      'IF(pcl.CustoPadrao > 0, pcl.CustoPadrao, '
      'pcl.Valor/pcl.Peso) CustoPadrao, '
      ''
      'IF((pcl.Valor/pcl.Peso>0) AND (pcl.Valor>0) AND (pcl.Peso>0), '
      'pcl.Valor/pcl.Peso,pcl.CustoPadrao) CustoMedio, '
      ''
      'pcl.MoedaPadrao, pcl.Controle PRODUTOCI, fi.* '
      'FROM FormulasIts fi'#9
      ''
      'LEFT JOIN PQ    pq  ON pq.Codigo=fi.Produto'
      'LEFT JOIN PQCli pcl ON pcl.PQ=fi.Produto AND pcl.CI=:P0'
      'WHERE fi.Numero=:P1'
      'AND ((fi.Minimo+fi.Maximo=0) '
      'OR(:P2 BETWEEN fi.Minimo AND fi.Maximo))'
      'ORDER BY fi.Ordem, fi.Controle')
    Left = 232
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrFormulasItsPESO_PQ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PESO_PQ'
      Calculated = True
    end
    object QrFormulasItsPRECO_PQ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO_PQ'
      Calculated = True
    end
    object QrFormulasItsCUSTO_PQ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO_PQ'
      Calculated = True
    end
    object QrFormulasItsCPRODP: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CPRODP'
      Calculated = True
    end
    object QrFormulasItsCSOLUP: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CSOLUP'
      Calculated = True
    end
    object QrFormulasItsCUSTOP: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOP'
      Calculated = True
    end
    object QrFormulasItsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrFormulasItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFormulasItsPQCI: TIntegerField
      FieldName = 'PQCI'
    end
    object QrFormulasItsCustoMedio: TFloatField
      FieldName = 'CustoMedio'
    end
    object QrFormulasItsCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
    end
    object QrFormulasItsMoedaPadrao: TSmallintField
      FieldName = 'MoedaPadrao'
    end
    object QrFormulasItsNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrFormulasItsOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrFormulasItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFormulasItsPorcent: TFloatField
      FieldName = 'Porcent'
    end
    object QrFormulasItsProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrFormulasItsTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrFormulasItsTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrFormulasItsObs: TWideStringField
      FieldName = 'Obs'
    end
    object QrFormulasItsSC: TIntegerField
      FieldName = 'SC'
    end
    object QrFormulasItsReciclo: TIntegerField
      FieldName = 'Reciclo'
    end
    object QrFormulasItsDiluicao: TFloatField
      FieldName = 'Diluicao'
    end
    object QrFormulasItsCProd: TFloatField
      FieldName = 'CProd'
    end
    object QrFormulasItsCSolu: TFloatField
      FieldName = 'CSolu'
    end
    object QrFormulasItsCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrFormulasItsMinimo: TFloatField
      FieldName = 'Minimo'
    end
    object QrFormulasItsMaximo: TFloatField
      FieldName = 'Maximo'
    end
    object QrFormulasItsGraus: TFloatField
      FieldName = 'Graus'
    end
    object QrFormulasItsBoca: TWideStringField
      FieldName = 'Boca'
      Size = 1
    end
    object QrFormulasItsProcesso: TWideStringField
      FieldName = 'Processo'
      Size = 30
    end
    object QrFormulasItsObsProces: TWideStringField
      FieldName = 'ObsProces'
      Size = 100
    end
    object QrFormulasItsReordem: TIntegerField
      FieldName = 'Reordem'
      Required = True
    end
    object QrFormulasItsPRODUTOCI: TIntegerField
      FieldName = 'PRODUTOCI'
      Required = True
    end
    object QrFormulasItspH_Min: TFloatField
      FieldName = 'pH_Min'
    end
    object QrFormulasItspH_Max: TFloatField
      FieldName = 'pH_Max'
    end
    object QrFormulasItsBe_Min: TFloatField
      FieldName = 'Be_Min'
    end
    object QrFormulasItsBe_Max: TFloatField
      FieldName = 'Be_Max'
    end
    object QrFormulasItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFormulasItsGC_Min: TFloatField
      FieldName = 'GC_Min'
    end
    object QrFormulasItsGC_Max: TFloatField
      FieldName = 'GC_Max'
    end
    object QrFormulasItspH_All: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'pH_All'
      Calculated = True
    end
    object QrFormulasItsBe_All: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Be_All'
      Calculated = True
    end
  end
  object DataSource1: TDataSource
    DataSet = QrFormulasIts
    Left = 260
    Top = 112
  end
  object QrPages: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Pagin) PAGINAS'
      'FROM Imprimir1')
    Left = 341
    Top = 169
    object QrPagesPAGINAS: TIntegerField
      FieldName = 'PAGINAS'
    end
  end
  object QrView1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT* FROM Imprimir1'
      'WHERE DTopo=:P0'
      'AND DBand=:P1'
      'AND Tipo =-1'
      'ORDER BY DMEsq')
    Left = 340
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrView1Campo: TIntegerField
      FieldName = 'Campo'
    end
    object QrView1Valor: TWideStringField
      FieldName = 'Valor'
      Size = 255
    end
    object QrView1Prefixo: TWideStringField
      FieldName = 'Prefixo'
      Size = 255
    end
    object QrView1Sufixo: TWideStringField
      FieldName = 'Sufixo'
      Size = 255
    end
    object QrView1Conta: TIntegerField
      FieldName = 'Conta'
    end
    object QrView1DBand: TIntegerField
      FieldName = 'DBand'
    end
    object QrView1DTopo: TIntegerField
      FieldName = 'DTopo'
    end
    object QrView1DMEsq: TIntegerField
      FieldName = 'DMEsq'
    end
    object QrView1DComp: TIntegerField
      FieldName = 'DComp'
    end
    object QrView1DAltu: TIntegerField
      FieldName = 'DAltu'
    end
    object QrView1Set_N: TIntegerField
      FieldName = 'Set_N'
    end
    object QrView1Set_I: TIntegerField
      FieldName = 'Set_I'
    end
    object QrView1Set_U: TIntegerField
      FieldName = 'Set_U'
    end
    object QrView1Set_T: TIntegerField
      FieldName = 'Set_T'
    end
    object QrView1Set_A: TIntegerField
      FieldName = 'Set_A'
    end
    object QrView1Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrView1Substituicao: TWideStringField
      FieldName = 'Substituicao'
      Size = 255
    end
    object QrView1Substitui: TSmallintField
      FieldName = 'Substitui'
    end
    object QrView1Nulo: TSmallintField
      FieldName = 'Nulo'
    end
    object QrView1Forma: TIntegerField
      FieldName = 'Forma'
    end
    object QrView1Linha: TIntegerField
      FieldName = 'Linha'
    end
    object QrView1Colun: TIntegerField
      FieldName = 'Colun'
    end
    object QrView1Pagin: TIntegerField
      FieldName = 'Pagin'
    end
  end
  object QrTopos1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT DTopo, Tipo '
      'FROM Imprimir1'
      'WHERE DBand=:P0'
      'AND Valor<>'#39#39
      'ORDER BY DTopo')
    Left = 340
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTopos1DTopo: TIntegerField
      FieldName = 'DTopo'
    end
    object QrTopos1Tipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrImprimeBand: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ImprimeBand'
      'WHERE Codigo=:P0'
      'ORDER BY Pos_Topo')
    Left = 340
    Top = 84
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrImprimeBandCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImprimeBandControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrImprimeBandTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrImprimeBandDataset: TIntegerField
      FieldName = 'Dataset'
    end
    object QrImprimeBandPos_Topo: TIntegerField
      FieldName = 'Pos_Topo'
    end
    object QrImprimeBandPos_Altu: TIntegerField
      FieldName = 'Pos_Altu'
    end
    object QrImprimeBandNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrImprimeBandLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrImprimeBandDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrImprimeBandDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrImprimeBandUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrImprimeBandUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrImprimeBandCol_Qtd: TIntegerField
      FieldName = 'Col_Qtd'
    end
    object QrImprimeBandCol_Lar: TIntegerField
      FieldName = 'Col_Lar'
    end
    object QrImprimeBandCol_GAP: TIntegerField
      FieldName = 'Col_GAP'
    end
    object QrImprimeBandOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrImprimeBandLinhas: TIntegerField
      FieldName = 'Linhas'
    end
    object QrImprimeBandRepetencia: TSmallintField
      FieldName = 'Repetencia'
    end
  end
  object QrImprime: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, NomeFonte, MSupDOS, Pos_MSup2 , Pos_MEsq'
      'FROM Imprime'
      'WHERE TipoImpressao=-10000')
    Left = 340
    Top = 56
    object QrImprimeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImprimeNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrImprimeNomeFonte: TWideStringField
      FieldName = 'NomeFonte'
      Size = 255
    end
    object QrImprimeMSupDOS: TIntegerField
      FieldName = 'MSupDOS'
    end
    object QrImprimePos_MSup2: TIntegerField
      FieldName = 'Pos_MSup2'
    end
    object QrImprimePos_MEsq: TIntegerField
      FieldName = 'Pos_MEsq'
    end
  end
  object DsImprime: TDataSource
    DataSet = QrImprime
    Left = 368
    Top = 56
  end
  object DsImprimeBand: TDataSource
    DataSet = QrImprimeBand
    Left = 368
    Top = 84
  end
  object QrLFormIts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLFormItsCalcFields
    SQL.Strings = (
      'SELECT * FROM ImpBMZ'
      'WHERE Peso > :P0'
      'AND Insumo > :P1'
      'ORDER BY Linha')
    Left = 356
    Top = 404
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLFormItsLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLFormItsEtapa: TIntegerField
      FieldName = 'Etapa'
    end
    object QrLFormItsNomeEtapa: TWideStringField
      FieldName = 'NomeEtapa'
      Size = 255
    end
    object QrLFormItsTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrLFormItsTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrLFormItsObservacos: TWideStringField
      FieldName = 'Observacos'
      Size = 255
    end
    object QrLFormItsPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrLFormItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrLFormItsPERC_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERC_TXT'
      Size = 50
      Calculated = True
    end
    object QrLFormItsPESO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PESO_TXT'
      Size = 50
      Calculated = True
    end
    object QrLFormItsTEXT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXT_TXT'
      Size = 255
      Calculated = True
    end
    object QrLFormItsTEMPO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEMPO_TXT'
      Size = 255
      Calculated = True
    end
    object QrLFormItsSubEtapa: TIntegerField
      FieldName = 'SubEtapa'
    end
    object QrLFormItsDiluicao: TFloatField
      FieldName = 'Diluicao'
    end
    object QrLFormItsTXT_DILUICAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DILUICAO'
      Size = 255
      Calculated = True
    end
    object QrLFormItsGraus: TFloatField
      FieldName = 'Graus'
    end
    object QrLFormItsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrLFormItsObserProce: TWideStringField
      FieldName = 'ObserProce'
      Size = 255
    end
    object QrLFormItspH_Min: TFloatField
      FieldName = 'pH_Min'
    end
    object QrLFormItspH_Max: TFloatField
      FieldName = 'pH_Max'
    end
    object QrLFormItsBe_Min: TFloatField
      FieldName = 'Be_Min'
    end
    object QrLFormItsBe_Max: TFloatField
      FieldName = 'Be_Max'
    end
    object QrLFormItsGC_Min: TFloatField
      FieldName = 'GC_Min'
    end
    object QrLFormItsGC_Max: TFloatField
      FieldName = 'GC_Max'
    end
    object QrLFormItsCustoKg: TFloatField
      FieldName = 'CustoKg'
    end
    object QrLFormItsCustoIt: TFloatField
      FieldName = 'CustoIt'
    end
    object QrLFormItsMOSTRA_LINHA: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'MOSTRA_LINHA'
      Calculated = True
    end
    object QrLFormItsPorcent: TFloatField
      FieldName = 'Porcent'
    end
    object QrLFormItsTEMPO_R_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEMPO_R_TXT'
      Calculated = True
    end
    object QrLFormItsMOSTRA_GRADE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MOSTRA_GRADE'
      Calculated = True
    end
    object QrLFormItsDESCRI_TEMP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DESCRI_TEMP'
      Size = 255
      Calculated = True
    end
  end
  object QrPreuso: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPreusoCalcFields
    SQL.Strings = (
      'SELECT pq.Ativo, ems.Codigo, ems.Controle, '
      'pqc.MoedaPadrao, ems.ProdutoCI, '
      'SUM(ems.Peso_PQ) Peso_PQ, ems.Produto, ems.PQCI, '
      'ems.NomePQ, pqc.Peso, (pqc.Peso+999999999999) EXISTE, '
      ''
      'IF(pqc.CustoPadrao > 0, pqc.CustoPadrao, '
      'pqc.Valor/pqc.Peso) CustoPadrao, '
      ''
      'IF((pqc.Valor/pqc.Peso>0) AND (pqc.Valor>0) AND (pqc.Peso>0), '
      'pqc.Valor/pqc.Peso, pqc.CustoPadrao) CustoMedio, '
      ''
      'ems.Cli_Orig, pqc.Peso-SUM(ems.Peso_PQ) PESOFUTURO, '
      'IF(cli.Tipo=0, cli.RazaoSocial,cli.Nome) NOMECLI_ORIG'
      'FROM emitits ems'
      'LEFT JOIN PQCli     pqc ON pqc.Controle=ems.ProdutoCI'
      'LEFT JOIN PQ     pq ON pq.Codigo=pqc.PQ'
      'LEFT JOIN Entidades cli ON cli.Codigo=ems.Cli_Orig'
      'WHERE ems.Ativo<>3'
      'AND ems.Codigo=:P0'
      'GROUP BY ems.Produto, Cli_Orig'
      ''
      ''
      '')
    Left = 392
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPreusoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPreusoControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPreusoPeso_PQ: TFloatField
      FieldName = 'Peso_PQ'
    end
    object QrPreusoProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrPreusoPQCI: TIntegerField
      FieldName = 'PQCI'
      Required = True
    end
    object QrPreusoProdutoCI: TIntegerField
      FieldName = 'ProdutoCI'
      Required = True
    end
    object QrPreusoNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Size = 50
    end
    object QrPreusoPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPreusoPESOFUTURO: TFloatField
      FieldName = 'PESOFUTURO'
    end
    object QrPreusoCli_Orig: TIntegerField
      FieldName = 'Cli_Orig'
      Required = True
    end
    object QrPreusoNOMECLI_ORIG: TWideStringField
      FieldName = 'NOMECLI_ORIG'
      Size = 100
    end
    object QrPreusoDEFASAGEM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DEFASAGEM'
      Calculated = True
    end
    object QrPreusoCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
    end
    object QrPreusoMoedaPadrao: TSmallintField
      FieldName = 'MoedaPadrao'
    end
    object QrPreusoEXISTE: TFloatField
      FieldName = 'EXISTE'
    end
    object QrPreusoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPreusoCustoMedio: TFloatField
      FieldName = 'CustoMedio'
    end
  end
  object QrBaixa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ems.Controle '
      'FROM EmitIts ems'
      'LEFT JOIN Entidades ori ON ori.Codigo=ems.Cli_Orig'
      'WHERE ems.Codigo=:P0'
      '')
    Left = 232
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBaixaControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrEmitIts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmitItsCalcFields
    SQL.Strings = (
      'SELECT ems.*, pqc.Peso, pqc.Valor/pqc.Peso CUSTOKG, '
      'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial '
      'ELSE cli.Nome END NOMECLI_ORIG'
      'FROM EmitIts ems'
      'LEFT JOIN PQCli     pqc ON pqc.Controle=ems.ProdutoCI'
      'LEFT JOIN Entidades cli ON cli.Codigo=ems.Cli_Orig'
      'WHERE ems.Codigo=:P0'
      'ORDER BY Ordem, Controle')
    Left = 260
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmitItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmitItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmitItsNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Size = 50
    end
    object QrEmitItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEmitItsPQCI: TIntegerField
      FieldName = 'PQCI'
      Required = True
    end
    object QrEmitItsCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
      Required = True
    end
    object QrEmitItsMoedaPadrao: TIntegerField
      FieldName = 'MoedaPadrao'
      Required = True
    end
    object QrEmitItsNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrEmitItsPorcent: TFloatField
      FieldName = 'Porcent'
      Required = True
    end
    object QrEmitItsProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrEmitItsTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrEmitItsTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrEmitItsObs: TWideStringField
      FieldName = 'Obs'
    end
    object QrEmitItsCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrEmitItsGraus: TFloatField
      FieldName = 'Graus'
    end
    object QrEmitItsBoca: TWideStringField
      FieldName = 'Boca'
      Size = 1
    end
    object QrEmitItsProcesso: TWideStringField
      FieldName = 'Processo'
      Size = 30
    end
    object QrEmitItsObsProces: TWideStringField
      FieldName = 'ObsProces'
      Size = 120
    end
    object QrEmitItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmitItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmitItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmitItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmitItsOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrEmitItsPeso_PQ: TFloatField
      FieldName = 'Peso_PQ'
      Required = True
    end
    object QrEmitItsProdutoCI: TIntegerField
      FieldName = 'ProdutoCI'
      Required = True
    end
    object QrEmitItsCli_Orig: TIntegerField
      FieldName = 'Cli_Orig'
      Required = True
    end
    object QrEmitItsCli_Dest: TIntegerField
      FieldName = 'Cli_Dest'
      Required = True
    end
    object QrEmitItsPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEmitItsCUSTOKG: TFloatField
      FieldName = 'CUSTOKG'
    end
    object QrEmitItsNOMECLI_ORIG: TWideStringField
      FieldName = 'NOMECLI_ORIG'
      Size = 100
    end
    object QrEmitItspH_Min: TFloatField
      FieldName = 'pH_Min'
    end
    object QrEmitItspH_Max: TFloatField
      FieldName = 'pH_Max'
    end
    object QrEmitItsBe_Min: TFloatField
      FieldName = 'Be_Min'
    end
    object QrEmitItsBe_Max: TFloatField
      FieldName = 'Be_Max'
    end
    object QrEmitItsGC_Min: TFloatField
      FieldName = 'GC_Min'
    end
    object QrEmitItsGC_Max: TFloatField
      FieldName = 'GC_Max'
    end
    object QrEmitItsPRECO_PQ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO_PQ'
      Calculated = True
    end
    object QrEmitItsCUSTO_PQ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO_PQ'
      Calculated = True
    end
    object QrEmitItsDiluicao: TFloatField
      FieldName = 'Diluicao'
    end
    object QrEmitItsMinimo: TFloatField
      FieldName = 'Minimo'
    end
    object QrEmitItsMaximo: TFloatField
      FieldName = 'Maximo'
    end
    object QrEmitItsCustoMedio: TFloatField
      FieldName = 'CustoMedio'
    end
  end
  object frxRec2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39541.575945451400000000
    ReportOptions.LastChange = 39541.575945451400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 24
    Top = 136
    Datasets = <
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
      end
      item
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object Band1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.141732280000000000
        Top = 343.937230000000000000
        Width = 737.008350000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
        RowCount = 0
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 654.440940000000000000
          Width = 80.000000000000000000
          Height = 18.141732280000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[kgLCusto]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 54.440940000000000000
          Width = 32.000000000000000000
          Height = 18.141732280000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."Produto' +
              '">)]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 86.440940000000000000
          Width = 148.000000000000000000
          Height = 18.141732280000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFormulasIts."Nome"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 302.440940000000000000
          Width = 68.000000000000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,##0.0000;-#,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."PRECO_PQ"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 370.440940000000000000
          Width = 20.000000000000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '##0; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."Graus"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 390.440940000000000000
          Width = 28.000000000000000000
          Height = 18.141732280000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."TempoR"' +
              '>)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 418.440940000000000000
          Width = 28.000000000000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."TempoP"' +
              '>)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 234.440940000000000000
          Width = 68.000000000000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#0.000; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."PESO_PQ"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 446.440940000000000000
          Width = 104.000000000000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFormulasIts."Obs"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 550.440939999999900000
          Width = 52.000000000000000000
          Height = 18.141732283464570000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[Inicio]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 602.440940000000000000
          Width = 52.000000000000000000
          Height = 18.141732283464570000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[Final]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 2.440940000000000000
          Width = 52.000000000000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,##0.000; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."Porcent"]')
          ParentFont = False
        end
      end
      object Band3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007874020000000000
        Top = 302.362400000000000000
        Width = 737.008350000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 86.440940000000000000
          Width = 148.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Nome do produto ou texto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 302.440940000000000000
          Width = 68.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo kg')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 370.440940000000000000
          Width = 20.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #186'C')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 390.440940000000000000
          Width = 28.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Roda')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 418.440940000000000000
          Width = 28.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'ra')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 234.440940000000000000
          Width = 68.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 446.440940000000000000
          Width = 104.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 550.440939999999900000
          Width = 52.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Ini_txt]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 602.440940000000000000
          Width = 52.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Fin_txt]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 54.440940000000000000
          Width = 32.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd.')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 2.440940000000000000
          Width = 52.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Uso %')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 654.440940000000000000
          Width = 80.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Uso kg/L]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.708700470000000000
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 1.220470000000000000
          Top = 1.204699999999999000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 642.543290000000000000
          Top = 2.747990000000002000
          Width = 86.456710000000000000
          Height = 37.338590000000010000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FU]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858267720000000000
          Top = 15.118100470000000000
          Width = 72.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_kg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976377950000000000
          Top = 30.236210710000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_m]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object meCustokg: TfrxMemoView
          AllowVectorExport = True
          Left = 264.566929130000000000
          Top = 15.118100470000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custokg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object meCustom2: TfrxMemoView
          AllowVectorExport = True
          Left = 264.566929130000000000
          Top = 30.236210710000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custom2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object meOS: TfrxMemoView
          AllowVectorExport = True
          Left = 615.866110000000000000
          Top = 42.086580000000000000
          Width = 112.913420000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[OS]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858267720000000000
          Width = 72.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_total]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 264.566929130000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[CustoTotal]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 605.188930000000000000
          Top = 8.543289999999999000
          Width = 36.881880000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Ful'#227'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834645670000000000
          Top = 15.118100470000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custokg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834645670000000000
          Top = 30.236210710000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custom2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834645670000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_CustoTotal]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102362200000000000
          Top = 15.118100470000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custokg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102362200000000000
          Top = 30.236210710000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custom2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102362200000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_CustoTotal]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 544.251968500000000000
          Width = 30.236220470000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data Moedas')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488188980000000000
          Width = 16.000000000000000000
          Height = 90.708671180000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[Data_Moedas]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 602.795300000000000000
          Top = 77.866110000000000000
          Width = 126.204700000000000000
          Height = 12.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object meSP: TfrxMemoView
          AllowVectorExport = True
          Left = 615.866110000000000000
          Top = 61.866109999999990000
          Width = 112.913420000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SP]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976377950000000000
          Top = 45.354320940000010000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_ft]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 264.566929130000000000
          Top = 45.354320940000010000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custoft2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834645670000000000
          Top = 45.354320940000010000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custoft2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102362200000000000
          Top = 45.354320940000010000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custoft2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 30.236239999999990000
          Width = 15.118110240000000000
          Height = 30.236191180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'WB')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976377950000000000
          Top = 60.472479999999990000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_m]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976377950000000000
          Top = 75.590590230000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_ft]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 60.472509290000000000
          Width = 15.118110240000000000
          Height = 30.236191180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'SA')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 264.566929130000000000
          Top = 60.472479999999990000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custom2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834645670000000000
          Top = 60.472479999999990000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custom2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102362200000000000
          Top = 60.472479999999990000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custom2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 264.566929130000000000
          Top = 75.590590230000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custoft2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834645670000000000
          Top = 75.590590230000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custoft2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102362200000000000
          Top = 75.590590230000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custoft2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 15.118110240000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'BRL')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716696530000000000
          Width = 15.118110240000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'USD [VAR_BRL_USD]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984413070000000000
          Width = 15.118110240000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'EUR [VAR_BRL_EUR]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 50.456710000000000000
        Top = 498.897960000000000000
        Width = 737.008350000000000000
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 593.779530000000000000
          Top = 2.015459999999962000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 681.779530000000000000
          Top = 2.015459999999962000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 109.779530000000000000
        Top = 132.283550000000000000
        Width = 737.008350000000000000
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Top = 0.055039999999991100
          Width = 91.558750000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677167800000010000
          Width = 60.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita de ')
          ParentFont = False
        end
        object meSetor: TfrxMemoView
          AllowVectorExport = True
          Left = 61.661410000000000000
          Top = 22.677167800000010000
          Width = 92.000000000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMESETOR"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354335590000010000
          Width = 52.000000000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita ')
          ParentFont = False
        end
        object meCodReceita: TfrxMemoView
          AllowVectorExport = True
          Left = 53.661409999999990000
          Top = 45.354335590000010000
          Width = 506.992270000000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
        end
        object meLotes: TfrxMemoView
          AllowVectorExport = True
          Left = 565.661410000000100000
          Top = 1.889763779527556000
          Width = 164.000000000000000000
          Height = 106.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Lotes]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 341.661410000000000000
          Top = 22.677167800000010000
          Width = 72.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Espessura:')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Top = 68.031500940000000000
          Width = 72.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantidade:')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Top = 86.929138739999990000
          Width = 72.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 341.661410000000000000
          Top = 68.031500940000000000
          Width = 72.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso m'#233'dio:')
          ParentFont = False
        end
        object meEspessura: TfrxMemoView
          AllowVectorExport = True
          Left = 417.661410000000000000
          Top = 22.677167800000010000
          Width = 142.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Espessura]')
          ParentFont = False
        end
        object meQtde: TfrxMemoView
          AllowVectorExport = True
          Left = 77.503710000000000000
          Top = 68.031500940000000000
          Width = 142.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
        end
        object mePeso: TfrxMemoView
          AllowVectorExport = True
          Left = 77.503710000000000000
          Top = 86.929138739999990000
          Width = 142.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
        end
        object mePesoMedio: TfrxMemoView
          AllowVectorExport = True
          Left = 417.661410000000000000
          Top = 68.031500940000000000
          Width = 142.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[PesoMedio]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 153.661410000000000000
          Top = 22.677167800000010000
          Width = 68.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 221.661410000000000000
          Top = 22.677167800000010000
          Width = 116.000000000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."Tecnico"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 341.661410000000000000
          Top = 86.929138739999990000
          Width = 72.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #193'rea m'#178':')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 417.661410000000000000
          Top = 86.929138739999990000
          Width = 142.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Area]')
          ParentFont = False
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.000000000000000000
        Top = 385.512060000000000000
        Width = 737.008350000000000000
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 1.559060000000000000
          Top = 6.047000000000082000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pesador1:')
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 53.559060000000000000
          Top = 26.047000000000030000
          Width = 284.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 385.559060000000000000
          Top = 6.047000000000082000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fuloneiro1:')
          ParentFont = False
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 441.559060000000000000
          Top = 26.047000000000030000
          Width = 284.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 1.559060000000000000
          Top = 30.046999999999970000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pesador2:')
          ParentFont = False
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 53.559060000000000000
          Top = 50.047000000000030000
          Width = 284.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 385.559060000000000000
          Top = 30.046999999999970000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fuloneiror2:')
          ParentFont = False
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 441.559060000000000000
          Top = 50.047000000000030000
          Width = 284.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
    end
  end
  object frxDsFormulasIts: TfrxDBDataset
    UserName = 'frxDsFormulasIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'PESO_PQ=PESO_PQ'
      'PRECO_PQ=PRECO_PQ'
      'CUSTO_PQ=CUSTO_PQ'
      'CPRODP=CPRODP'
      'CSOLUP=CSOLUP'
      'CUSTOP=CUSTOP'
      'SEQ=SEQ'
      'Nome=Nome'
      'PQCI=PQCI'
      'CustoMedio=CustoMedio'
      'CustoPadrao=CustoPadrao'
      'MoedaPadrao=MoedaPadrao'
      'Numero=Numero'
      'Ordem=Ordem'
      'Controle=Controle'
      'Porcent=Porcent'
      'Produto=Produto'
      'TempoR=TempoR'
      'TempoP=TempoP'
      'Obs=Obs'
      'SC=SC'
      'Reciclo=Reciclo'
      'Diluicao=Diluicao'
      'CProd=CProd'
      'CSolu=CSolu'
      'Custo=Custo'
      'Minimo=Minimo'
      'Maximo=Maximo'
      'Graus=Graus'
      'Boca=Boca'
      'Processo=Processo'
      'ObsProces=ObsProces'
      'Reordem=Reordem'
      'PRODUTOCI=PRODUTOCI'
      'pH_Min=pH_Min'
      'pH_Max=pH_Max'
      'Be_Min=Be_Min'
      'Be_Max=Be_Max'
      'Ativo=Ativo'
      'GC_Min=GC_Min'
      'GC_Max=GC_Max'
      'pH_All=pH_All'
      'Be_All=Be_All')
    DataSet = QrFormulasIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 504
    Top = 56
  end
  object frxDsFormulas: TfrxDBDataset
    UserName = 'frxDsFormulas'
    CloseDataSource = False
    DataSet = QrFormulas
    BCDToCurrency = False
    DataSetOptions = []
    Left = 532
    Top = 56
  end
  object frxRec1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38017.353737407400000000
    ReportOptions.LastChange = 41853.389075254630000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 24
    Top = 88
    Datasets = <
      item
        DataSet = frxDsBMZ
        DataSetName = 'frxDsBMZ'
      end
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
      end
      item
        DataSet = frxDsFulonadas
        DataSetName = 'frxDsFulonadas'
      end
      item
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
      end
      item
        DataSet = frxDsLotes
        DataSetName = 'frxDsLotes'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPreUso
        DataSetName = 'frxDsPreUso'
      end
      item
        DataSet = frxDsSumFul
        DataSetName = 'frxDsSumFul'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      object Band1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.141732280000000000
        Top = 351.496290000000000000
        Width = 737.008350000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 714.330711100000000000
          Width = 22.677165350000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."Seq"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 90.708661420000000000
          Height = 18.141732283464570000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[kgLCusto]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622047240000000000
          Width = 30.236220470000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."Produto' +
              '">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858267720000000000
          Width = 143.622047240000000000
          Height = 18.141732280000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFormulasIts."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598425200000000000
          Width = 18.897637800000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."Diluica' +
              'o">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496062990000000000
          Width = 18.897637800000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '##0; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."Graus"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393700790000000000
          Width = 26.456692910000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."TempoR"' +
              '>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850393700000000000
          Width = 26.456692910000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."TempoP"' +
              '>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307076850000000000
          Width = 45.354330710000000000
          Height = 18.141732283464570000
          DataSet = frxDsFormulasIts
          DataSetName = 'frxDsFormulasIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulasIts."pH_All"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661422200000000000
          Width = 45.354330710000000000
          Height = 18.141732283464570000
          DataField = 'Be_All'
          DataSet = frxDsFormulasIts
          DataSetName = 'frxDsFormulasIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulasIts."Be_All"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 514.015750469999900000
          Width = 102.047244090000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFormulasIts."Obs"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 616.062994570000000000
          Width = 49.133858270000000000
          Height = 18.141732283464570000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[Inicio]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 665.196852830000000000
          Width = 49.133858270000000000
          Height = 18.141732283464570000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[Final]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708661420000000000
          Width = 52.913385830000000000
          Height = 18.141732280000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,##0.000; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."Porcent"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480314960000000000
          Width = 15.118110240000000000
          Height = 18.141732283464570000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulasIts."Boca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007874020000000000
        Top = 309.921460000000000000
        Width = 737.008350000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858267720000000000
          Width = 143.622047240000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Nome do produto ou texto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598425200000000000
          Width = 18.897637800000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '1:x')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496062990000000000
          Width = 18.897637800000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #186'C')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393700790000000000
          Width = 26.456692910000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Roda')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850393700000000000
          Width = 26.456692910000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'ra')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307076850000000000
          Width = 45.354330710000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'pH')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661422200000000000
          Width = 45.354330710000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'B'#233)
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 514.015750469999900000
          Width = 102.047244090000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 616.062994570000000000
          Width = 49.133858270000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Ini_txt]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 665.196852830000000000
          Width = 49.133858270000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Fin_txt]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622047244094000000
          Width = 30.236220472440900000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd.')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708661417322800000
          Width = 52.913385826771700000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Uso %')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Width = 90.708661420000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Uso kg/L]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 714.330711100000000000
          Width = 22.677165350000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480314960000000000
          Width = 15.118110240000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'B')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.708700470000000000
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 9.118119999999999000
          Top = 2.866109999999999000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 600.677180000000000000
          Top = 30.866110000000000000
          Width = 48.220470000000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Ful'#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 649.118120000000000000
          Top = 2.866109999999999000
          Width = 88.000000000000000000
          Height = 66.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -64
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FU]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object meOS: TfrxMemoView
          AllowVectorExport = True
          Left = 33.716450000000000000
          Top = 44.440940000000000000
          Width = 144.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[OS]')
          ParentFont = False
          WordWrap = False
        end
        object meSP: TfrxMemoView
          AllowVectorExport = True
          Left = 33.716450000000000000
          Top = 68.440940000000000000
          Width = 144.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_PESAGEM]')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 614.692950000000000000
          Top = 70.866110000000000000
          Width = 122.425170000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Top = 15.118100470000000000
          Width = 72.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_kg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535550230000000000
          Top = 30.236210710000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_m]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 15.118100470000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custokg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 30.236210710000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custom2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Width = 72.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_total]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[CustoTotal]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 257.007991180000000000
          Width = 15.118110240000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'BRL')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 15.118100470000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custokg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 30.236210710000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custom2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_CustoTotal]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275707710000000000
          Width = 15.118110240000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'USD [VAR_BRL_USD]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 15.118100470000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custokg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 30.236210710000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custom2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_CustoTotal]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543424250000000000
          Width = 15.118110240000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'EUR [VAR_BRL_EUR]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811140779999900000
          Width = 30.236220470000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data Moedas')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047361260000000000
          Width = 16.000000000000000000
          Height = 90.708671180000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[Data_Moedas]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535550230000000000
          Top = 45.354320940000010000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_ft]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 45.354320940000010000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custoft2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 45.354320940000010000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custoft2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 45.354320940000010000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custoft2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417552280000000000
          Top = 30.236239999999990000
          Width = 15.118110240000000000
          Height = 30.236191180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'WB')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535550230000000000
          Top = 60.472479999999990000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_m]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535550230000000000
          Top = 75.590590230000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_ft]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417552280000000000
          Top = 60.472509290000000000
          Width = 15.118110240000000000
          Height = 30.236191180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'SA')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 60.472479999999990000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custom2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 60.472479999999990000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custom2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 60.472479999999990000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custom2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 75.590590230000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custoft2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 75.590590230000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custoft2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 75.590590230000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custoft2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 39.118120000000000000
        Top = 506.457020000000000000
        Width = 737.008350000000000000
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 597.559060000000000000
          Top = 2.015459999999905000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 685.559060000000000000
          Top = 2.015459999999905000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 117.338590000000000000
        Top = 132.283550000000000000
        Width = 737.008350000000000000
        object meLotes: TfrxMemoView
          AllowVectorExport = True
          Left = 572.598425199999900000
          Top = 3.779529999999994000
          Width = 164.409448820000000000
          Height = 109.779530000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Lotes]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Top = 3.779529999999994000
          Width = 572.598425199999900000
          Height = 109.606370000000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 0.157700000000000000
          Top = 22.677179999999990000
          Width = 60.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita de ')
          ParentFont = False
          WordWrap = False
        end
        object meSetor: TfrxMemoView
          AllowVectorExport = True
          Left = 63.937230000000000000
          Top = 22.677179999999990000
          Width = 95.779530000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMESETOR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 0.157700000000000000
          Top = 41.574815349999990000
          Width = 44.000000000000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object meCodReceita: TfrxMemoView
          AllowVectorExport = True
          Left = 44.157700000000000000
          Top = 41.574815349999990000
          Width = 521.574803150000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 159.716760000000000000
          Top = 22.677179999999990000
          Width = 68.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 227.716760000000000000
          Top = 22.677179999999990000
          Width = 112.220470000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."Tecnico"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Top = 3.834570000000013000
          Width = 567.779530000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Top = 68.031540000000010000
          Width = 64.251968500000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantidade:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708705360000010000
          Width = 64.251968500000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 381.323130000000000000
          Top = 68.031579060000010000
          Width = 72.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso m'#233'dio:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object meQtde: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 68.031579060000010000
          Width = 90.708661420000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object mePeso: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 90.708744419999990000
          Width = 90.708661420000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object mePesoMedio: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 68.031579060000010000
          Width = 113.385826770000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[PesoMedio]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 381.323130000000000000
          Top = 90.708744419999990000
          Width = 72.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #193'rea m'#178':')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 90.708744419999990000
          Width = 113.385826770000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Area]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 258.976810000000000000
          Top = 90.708744419999990000
          Width = 120.944960000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_RETRABALHO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 156.929500000000000000
          Top = 68.031579060000010000
          Width = 223.370130000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_EMITGRU]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 156.929500000000000000
          Top = 90.708798120000010000
          Width = 98.456710000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_TIT_RETRAB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 22.677180000000020000
          Width = 53.102350000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' Rebaixe:')
          ParentFont = False
          WordWrap = False
        end
        object meEspessura: TfrxMemoView
          AllowVectorExport = True
          Left = 393.480520000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[EspessuraRebx]')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 22.677180000000020000
          Width = 53.102350000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' Esp.Final:')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 506.866420000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[EspessuraSemi]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.000000000000000000
        Top = 393.071120000000000000
        Width = 737.008350000000000000
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 9.118120000000000000
          Top = 6.047000000000082000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pesador1:')
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 61.118120000000000000
          Top = 26.047000000000030000
          Width = 284.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 393.118120000000000000
          Top = 6.047000000000082000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fuloneiro1:')
          ParentFont = False
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 449.118120000000000000
          Top = 26.047000000000030000
          Width = 284.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 9.118120000000000000
          Top = 30.046999999999970000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pesador2:')
          ParentFont = False
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 61.118120000000000000
          Top = 50.046999999999970000
          Width = 284.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 393.118120000000000000
          Top = 30.046999999999970000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fuloneiror2:')
          ParentFont = False
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 449.118120000000000000
          Top = 50.046999999999970000
          Width = 284.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
    end
  end
  object frxBMZ: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38077.792695474500000000
    ReportOptions.Name = 'Receita Skintan'
    ReportOptions.LastChange = 39541.569016562500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MeLinhasOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with MeLinhas, Engine do'
      '  begin'
      '  if <VAR_TXT> = '#39#39' then'
      '    MeLinhas.Visible := False'
      '  else'
      '    MeLinhas.Visible := True;'
      '  end'
      'end;'
      ''
      'procedure RodapeDeGrupo1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with RodapeDeGrupo1, Engine do'
      '  begin'
      '  RodapeDeGrupo1.Visible := <VAR_TEMOBSERPROCES>;'
      '  end'
      'end;'
      ''
      'procedure RodapeDeGrupo2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with RodapeDeGrupo2, Engine do'
      '  begin'
      '    RodapeDeGrupo2.Visible := MeLinhas.Visible;'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 560
    Top = 84
    Datasets = <
      item
        DataSet = frxDsBMZ
        DataSetName = 'frxDsBMZ'
      end
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
      end
      item
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MeLinhas: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.000000000000000000
        Top = 347.716760000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'MeLinhasOnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
        RowCount = 0
        object MeAddPQ: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Width = 720.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          HideZeros = True
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 25.118120000000000000
          Width = 88.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLFormIts."PESO_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 673.118120000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLFormIts."PERC_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 117.118120000000000000
          Width = 188.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsLFormIts."DESCRI_TEMP"]')
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 309.118120000000000000
          Width = 360.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsLFormIts."TXT_DILUICAO"] [frxDsLFormIts."Observacos"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Cabecalho: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 168.000000000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 32.425170000000000000
          Width = 388.000000000000000000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMESETOR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 62.425170000000000000
          Width = 60.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 81.118120000000000000
          Top = 62.425170000000000000
          Width = 192.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMECI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 82.425170000000000000
          Width = 60.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 81.118120000000000000
          Top = 82.425170000000000000
          Width = 192.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 102.425170000000000000
          Width = 60.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Relativo a:')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 81.118120000000000000
          Top = 102.425170000000000000
          Width = 192.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 273.118120000000000000
          Top = 62.425170000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'T'#233'cnico:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 337.118120000000000000
          Top = 62.425170000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."Tecnico"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 273.118120000000000000
          Top = 82.425170000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Espessura:')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 337.118120000000000000
          Top = 82.425170000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Espessura]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 273.118120000000000000
          Top = 102.425170000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 337.118120000000000000
          Top = 102.425170000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 130.425170000000000000
          Width = 104.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita:')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 273.118120000000000000
          Top = 122.425170000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantia:')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 337.118120000000000000
          Top = 122.425170000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 457.118120000000000000
          Top = 82.425170000000000000
          Width = 52.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            #193'rea:')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 509.118120000000000000
          Top = 82.425170000000000000
          Width = 108.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Area]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 457.118120000000000000
          Top = 102.425170000000000000
          Width = 52.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Rendim.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 509.118120000000000000
          Top = 102.425170000000000000
          Width = 108.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 457.118120000000000000
          Top = 122.425170000000000000
          Width = 52.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Data:')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 509.118120000000000000
          Top = 122.425170000000000000
          Width = 108.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 20.118120000000000000
          Top = 62.425170000000000000
          Width = 721.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 62.425170000000000000
          Height = 104.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 2.000000000000000000
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 741.118120000000000000
          Top = 62.425170000000000000
          Height = 104.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 2.000000000000000000
        end
        object Logo: TfrxPictureView
          AllowVectorExport = True
          Left = 619.118120000000000000
          Top = 64.425170000000000000
          Width = 116.000000000000000000
          Height = 80.000000000000000000
          Center = True
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 621.118120000000000000
          Top = 2.425169999999991000
          Width = 112.000000000000000000
          Height = 56.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -64
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FU]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 457.118120000000000000
          Top = 63.425170000000000000
          Width = 52.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'M'#233'dia:')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 509.118120000000000000
          Top = 63.425170000000000000
          Width = 108.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[PesoMedio]')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 2.425169999999991000
          Width = 388.000000000000000000
          Height = 24.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PESAGEM]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 413.118120000000000000
          Top = 2.425169999999991000
          Width = 204.000000000000000000
          Height = 56.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[OBS]')
          ParentFont = False
          WordWrap = False
        end
        object MeObjetivo: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 146.425170000000000000
          Width = 720.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 39.338590000000000000
        Top = 563.149970000000000000
        Width = 755.906000000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 470.000000000000000000
          Top = -0.000002440000002935
          Width = 270.677180000000000000
          Height = 16.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[TIME]-[DATE] - P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object CabGrupo1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 23.000000000000000000
        Top = 249.448980000000000000
        Width = 755.906000000000000000
        Condition = 'frxDsLFormIts."Etapa"'
        Stretched = True
        object MeEtapa: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Width = 720.000000000000000000
          Height = 23.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          HideZeros = True
          Memo.UTF8W = (
            'ETAPA [frxDsLFormIts."Etapa"]: [frxDsLFormIts."NomeEtapa"]')
          ParentFont = False
          VAlign = vaBottom
        end
      end
      object RodapeDeGrupo1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.000000000000000000
        Top = 434.645950000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'RodapeDeGrupo1OnBeforePrint'
        Stretched = True
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 0.527210000000025000
          Width = 720.000000000000000000
          Color = clBlack
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 20.409448818897720000
          Width = 720.000000000000000000
          Color = clBlack
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftTop]
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Width = 720.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Style = fsDashDot
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HideZeros = True
          Memo.UTF8W = (
            '[VAR_OBSERPROCES]')
          ParentFont = False
          WordWrap = False
        end
        object Line7: TfrxLineView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 0.377952755905539600
          Height = 20.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line6: TfrxLineView
          AllowVectorExport = True
          Left = 741.118120000000000000
          Top = 0.377952755905539600
          Height = 20.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
      object CabecalhoDeGrupo1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 4.000000000000000000
        Top = 294.803340000000000000
        Visible = False
        Width = 755.906000000000000000
        Condition = 'frxDsLFormIts."SubEtapa"'
      end
      object RodapeDeGrupo2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.000000000000000000
        Top = 389.291590000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'RodapeDeGrupo2OnBeforePrint'
        object MeRodar: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Width = 720.000000000000000000
          Height = 22.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HideZeros = True
          Memo.UTF8W = (
            '[frxDsLFormIts."TEMPO_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object Band1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 4.000000000000000000
        Top = 321.260050000000000000
        Visible = False
        Width = 755.906000000000000000
        Condition = 'frxDsLFormIts."SubEtapa"'
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 517.795610000000000000
        Width = 755.906000000000000000
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 21.165354330000000000
          Top = -0.000002440000002935
          Width = 444.881880000000000000
          Height = 20.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_TEMPORECEITA]')
        end
      end
    end
  end
  object frxDsBMZ: TfrxDBDataset
    UserName = 'frxDsBMZ'
    CloseDataSource = False
    DataSet = QrLFormIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 356
    Top = 356
  end
  object frxDsLFormIts: TfrxDBDataset
    UserName = 'frxDsLFormIts'
    CloseDataSource = False
    DataSet = QrLFormIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 588
    Top = 56
  end
  object frxPesagem1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38077.792695474500000000
    ReportOptions.Name = 'Receita Skintan'
    ReportOptions.LastChange = 39541.568236400500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MeLinhasOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with MeLinhas, Engine do'
      '  begin'
      '  if <VAR_TXT> = '#39#39' then'
      '    MeLinhas.Visible := False'
      '  else'
      '    MeLinhas.Visible := True;'
      '  end'
      'end;'
      ''
      'procedure RodapeDeGrupo2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with RodapeDeGrupo2, Engine do'
      '  begin'
      '    RodapeDeGrupo2.Visible := MeLinhas.Visible;'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 588
    Top = 84
    Datasets = <
      item
        DataSet = frxDsBMZ
        DataSetName = 'frxDsBMZ'
      end
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
      end
      item
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MeLinhas: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.000000000000000000
        Top = 275.905690000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'MeLinhasOnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsBMZ
        DataSetName = 'frxDsBMZ'
        RowCount = 0
        object MeAddPQ: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Width = 720.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          HideZeros = True
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 28.897650000000000000
          Width = 88.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLFormIts."PESO_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 676.897650000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLFormIts."PERC_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 120.897650000000000000
          Width = 188.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsLFormIts."Descricao"]')
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 312.897650000000000000
          Width = 360.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsLFormIts."TXT_DILUICAO"] [frxDsLFormIts."Observacos"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Cabecalho: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 124.000000000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Top = 31.543290000000000000
          Width = 420.000000000000000000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMESETOR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Top = 61.543290000000000000
          Width = 60.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 84.897650000000000000
          Top = 61.543290000000000000
          Width = 192.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMECI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Top = 81.543290000000000000
          Width = 60.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 84.897650000000000000
          Top = 81.543290000000000000
          Width = 192.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 584.897650000000000000
          Top = 61.543290000000000000
          Width = 156.000000000000000000
          Height = 36.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '                       ')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 276.897650000000000000
          Top = 61.543290000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'T'#233'cnico:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 340.897650000000000000
          Top = 61.543290000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."Tecnico"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 276.897650000000000000
          Top = 81.543290000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Espessura:')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 340.897650000000000000
          Top = 81.543290000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Espessura]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 460.897650000000000000
          Top = 61.543290000000000000
          Width = 52.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 512.897650000000000000
          Top = 61.543290000000000000
          Width = 72.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 460.897650000000000000
          Top = 81.543290000000000000
          Width = 52.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantia:')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 512.897650000000000000
          Top = 81.543290000000000000
          Width = 72.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
          WordWrap = False
        end
        object MeObjetivo: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Top = 101.543290000000000000
          Width = 720.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            'Receita: [frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 23.897650000000000000
          Top = 61.543290000000000000
          Width = 721.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Top = 61.543290000000000000
          Height = 60.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 2.000000000000000000
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 744.897650000000000000
          Top = 61.543290000000000000
          Height = 60.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 2.000000000000000000
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 508.897650000000000000
          Top = 1.543289999999999000
          Width = 224.000000000000000000
          Height = 56.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -64
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FU]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 588.897650000000000000
          Top = 61.543290000000000000
          Width = 60.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Relativo a:')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Top = 1.543289999999999000
          Width = 420.000000000000000000
          Height = 24.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PESAGEM]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 58.236240000000000000
        Top = 423.307360000000000000
        Width = 755.906000000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 462.881880000000000000
          Top = 2.393320000000017000
          Width = 282.015770000000000000
          Height = 20.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[TIME]-[DATE] - P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 20.897650000000000000
          Top = 0.613789999999994500
          Width = 441.322820000000000000
          Height = 24.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'ATEN'#199#195'O: IMPRESS'#195'O EXCLUSIVA PARA PESAGEM')
          ParentFont = False
        end
      end
      object CabGrupo1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 23.000000000000000000
        Top = 204.094620000000000000
        Width = 755.906000000000000000
        Condition = 'frxDsLFormIts."Etapa"'
        Stretched = True
        object MeEtapa: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Width = 720.000000000000000000
          Height = 23.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          HideZeros = True
          Memo.UTF8W = (
            'ETAPA [frxDsLFormIts."Etapa"]: [frxDsLFormIts."NomeEtapa"]')
          ParentFont = False
          VAlign = vaBottom
        end
      end
      object RodapDeGrupo1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.000000000000000000
        Top = 343.937230000000000000
        Visible = False
        Width = 755.906000000000000000
        Stretched = True
      end
      object CabecalhoDeGrupo1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 4.000000000000000000
        Top = 249.448980000000000000
        Visible = False
        Width = 755.906000000000000000
        Condition = 'frxDsLFormIts."SubEtapa"'
      end
      object RodapeDeGrupo2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 4.000000000000000000
        Top = 317.480520000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'RodapeDeGrupo2OnBeforePrint'
        object MeRodar: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Width = 720.000000000000000000
          Height = 4.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HideZeros = True
          ParentFont = False
          VAlign = vaBottom
        end
      end
    end
  end
  object frxPreuso: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38077.792695474500000000
    ReportOptions.Name = 'Receita Skintan'
    ReportOptions.LastChange = 39542.827736898200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo1, Engine do'
      '  begin'
      '    if <VARF_NEGATIVO> then'
      '      Memo1.Font.Style := 1'
      '    else Memo1.Font.Style := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo4, Engine do'
      '  begin'
      '    if <VARF_NEGATIVO> then'
      '      Memo4.Font.Style := 1'
      '    else Memo4.Font.Style := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo19OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo19, Engine do'
      '  begin'
      '    if <VARF_NEGATIVO> then'
      '      Memo19.Font.Style := 1'
      '    else Memo19.Font.Style := 0;'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 616
    Top = 84
    Datasets = <
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsPreUso
        DataSetName = 'frxDsPreUso'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MeLinhas: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.000000000000000000
        Top = 226.771800000000000000
        Width = 755.906000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPreUso
        DataSetName = 'frxDsPreUso'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 28.677180000000000000
          Width = 216.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo1OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsPreuso."NomePQ"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 340.677180000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPreuso."Peso"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 244.677180000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo4OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPreuso."Peso_PQ"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 436.677180000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPreuso."DEFASAGEM"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 532.677180000000000000
          Width = 216.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo19OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsPreuso."NOMECLI_ORIG"]')
          ParentFont = False
        end
      end
      object Cabecalho: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 146.543290000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 28.677180000000000000
          Top = 31.543290000000000000
          Width = 420.000000000000000000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMESETOR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 28.677180000000000000
          Top = 61.543290000000000000
          Width = 60.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 88.677180000000000000
          Top = 61.543290000000000000
          Width = 192.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMECI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 28.677180000000000000
          Top = 81.543290000000000000
          Width = 60.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 88.677180000000000000
          Top = 81.543290000000000000
          Width = 192.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 588.677180000000000000
          Top = 61.543290000000000000
          Width = 156.000000000000000000
          Height = 36.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '                       ')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 280.677180000000000000
          Top = 61.543290000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'T'#233'cnico:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 344.677180000000000000
          Top = 61.543290000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."Tecnico"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 280.677180000000000000
          Top = 81.543290000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Espessura:')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 344.677180000000000000
          Top = 81.543290000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Espessura]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 464.677180000000000000
          Top = 61.543290000000000000
          Width = 52.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 516.677180000000000000
          Top = 61.543290000000000000
          Width = 72.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 464.677180000000000000
          Top = 81.543290000000000000
          Width = 52.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantia:')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 516.677180000000000000
          Top = 81.543290000000000000
          Width = 72.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
          WordWrap = False
        end
        object MeObjetivo: TfrxMemoView
          AllowVectorExport = True
          Left = 28.677180000000000000
          Top = 101.543290000000000000
          Width = 720.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita: [frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 27.677180000000000000
          Top = 61.543290000000000000
          Width = 721.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 28.677180000000000000
          Top = 61.543290000000000000
          Height = 60.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 748.677180000000000000
          Top = 61.543290000000000000
          Height = 60.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 512.677180000000000000
          Top = 1.543289999999999000
          Width = 224.000000000000000000
          Height = 56.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -64
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FU]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 592.677180000000000000
          Top = 61.543290000000000000
          Width = 60.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Relativo a:')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 28.677180000000000000
          Top = 129.543290000000000000
          Width = 216.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Produto qu'#237'mico')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 340.677180000000000000
          Top = 129.543290000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Estoque (kg)')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 244.677180000000000000
          Top = 129.543290000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Necess'#225'rio (kg)')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 436.677180000000000000
          Top = 129.543290000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Defasagem (kg)')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 532.677180000000000000
          Top = 129.543290000000000000
          Width = 216.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Cliente interno fornecedor')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 28.000000000000000000
        Top = 306.141930000000000000
        Width = 755.906000000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 409.968460000000000000
          Width = 323.590600000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[TIME]-[DATE] - P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsPreUso: TfrxDBDataset
    UserName = 'frxDsPreUso'
    CloseDataSource = False
    DataSet = QrPreuso
    BCDToCurrency = False
    DataSetOptions = []
    Left = 616
    Top = 56
  end
  object frxRec10: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38077.792695474500000000
    ReportOptions.Name = 'Receita Skintan'
    ReportOptions.LastChange = 39546.836925451400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  TemCusto: Double;'
      '  h1, h2, h3, hR: Extended;'
      ''
      'procedure CalculaAlturaLines;'
      'begin'
      '  if h1 > hR then hR := h1;'
      '  if h2 > hR then hR := h2;'
      '  if h3 > hR then hR := h3;'
      '  //'
      '  Line1.Height := hR;'
      '  Line2.Height := hR;'
      'end;'
      ''
      'procedure CabGrupo1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  TemCusto := 0;'
      'end;'
      ''
      'procedure MeLinhasOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  TemCusto := TemCusto + <frxDsLFormIts."CustoIt">;'
      '  MeLinhas.Visible := <VAR_MOSTRA_LINHA>;'
      '  Memo01.Visible := not (<VAR_TXT> = '#39#39');'
      '  Memo02.Visible := not (<VAR_TXT> = '#39#39');'
      '  Memo03.Visible := not (<VAR_TXT> = '#39#39');'
      '  Memo04.Visible := not (<VAR_TXT> = '#39#39');'
      '  Memo05.Visible := not (<VAR_TXT> = '#39#39');'
      '  Memo08.Visible := not (<VAR_TXT> = '#39#39');'
      '  Memo09.Visible := not (<VAR_TXT> = '#39#39');'
      '  //'
      '  Memo05.Visible := <frxDsLFormIts."TempoR"> > 0;'
      '  Memo06.Visible := <frxDsLFormIts."TempoR"> > 0;'
      '  Memo07.Visible := <frxDsLFormIts."TempoR"> > 0;'
      '  //'
      '  Memo08.Visible := <frxDsLFormIts."CustoKg"> > 0;'
      '  Memo09.Visible := <frxDsLFormIts."CustoKg"> > 0;'
      '  //'
      '  Memo01.Frame.Width := <VAR_MOSTRA_GRADE>;'
      '  Memo02.Frame.Width := <VAR_MOSTRA_GRADE>;'
      '  Memo03.Frame.Width := <VAR_MOSTRA_GRADE>;'
      '  Memo04.Frame.Width := <VAR_MOSTRA_GRADE>;'
      '  if Memo05.Visible = False then'
      '    Memo04.Frame.Width := 0;'
      'end;'
      ''
      'procedure RodapeDeGrupo1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  RodapeDeGrupo1.Visible := (<VAR_TEMOBSERPROCES> = True) or (Te' +
        'mCusto >= 0.01) ;'
      'end;'
      ''
      'procedure Memo31OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if (<VAR_OBSERPROCES> <> '#39#39') then'
      '    Memo31.Frame.Width := 0.5'
      '  else'
      '    Memo31.Frame.Width := 0;'
      'end;'
      ''
      'procedure CabecalhoOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  hR := 0;'
      '  h1 := 0;'
      '  h2 := 0;'
      '  h3 := 0;'
      'end;'
      ''
      'procedure MasterData4OnAfterCalcHeight(Sender: TfrxComponent);'
      'begin'
      '  h1 := MasterData4.Height;'
      '  CalculaAlturaLines();'
      'end;'
      ''
      'procedure GroupHeader2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  h2 := h2 + GroupHeader2.Height;'
      '  CalculaAlturaLines();'
      'end;'
      ''
      'procedure MasterData2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  h2 := h2 + MasterData2.Height;'
      '  CalculaAlturaLines();'
      'end;'
      ''
      'procedure GroupFooter2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  h2 := h2 + GroupFooter2.Height;'
      '  CalculaAlturaLines();'
      'end;'
      ''
      'procedure GroupHeader1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  h3 := h3 + GroupHeader1.Height;'
      '  CalculaAlturaLines();'
      'end;'
      ''
      'procedure MasterData3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  h3 := h3 + MasterData3.Height;'
      '  CalculaAlturaLines();'
      'end;'
      ''
      'procedure GroupFooter1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  h3 := h3 + GroupFooter1.Height;'
      '  CalculaAlturaLines();'
      'end;'
      ''
      'begin'
      '  if <LogoReceExiste> = True then'
      '  begin'
      '    Logo.LoadFromFile(<LogoReceCaminho>);'
      '  end;'
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 644
    Top = 84
    Datasets = <
      item
        DataSet = frxDsBMZ
        DataSetName = 'frxDsBMZ'
      end
      item
      end
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
      end
      item
        DataSet = frxDsFulonadas
        DataSetName = 'frxDsFulonadas'
      end
      item
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
      end
      item
        DataSet = frxDsLotes
        DataSetName = 'frxDsLotes'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPreUso
        DataSetName = 'frxDsPreUso'
      end
      item
        DataSet = frxDsSumFul
        DataSetName = 'frxDsSumFul'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object Cabecalho: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 151.181102360000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'CabecalhoOnBeforePrint'
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401655350000000000
          Top = 111.795300000000000000
          Width = 452.252010000000000000
          Height = 16.440940000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMESETOR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 740.787401570000000000
          Top = 56.692913385826800000
          Height = 94.488188976378000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Logo: TfrxPictureView
          AllowVectorExport = True
          Left = 25.731910000000000000
          Top = 60.645640000000000000
          Width = 116.000000000000000000
          Height = 80.000000000000000000
          Center = True
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 519.070810000000000000
          Top = 59.118120000000000000
          Width = 85.543290000000000000
          Height = 25.763760000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -29
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FU]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401655350000000000
          Top = 59.118120000000000000
          Width = 369.102350000000000000
          Height = 24.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PESAGEM]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 609.653680000000000000
          Top = 59.118120000000000000
          Width = 128.409400000000000000
          Height = 82.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[OBS]')
          ParentFont = False
          WordWrap = False
        end
        object MeObjetivo: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401655350000000000
          Top = 89.732220000000000000
          Width = 453.543136220000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line13: TfrxLineView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Top = 56.692913390000000000
          Height = 94.488188980000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line14: TfrxLineView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Top = 56.692949999999990000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        RowCount = 1
        object Subreport1: TfrxSubreport
          AllowVectorExport = True
          Left = 28.346456692913400000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Page = frxRec10.Page3
        end
        object Subreport2: TfrxSubreport
          AllowVectorExport = True
          Left = 277.795275590000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Page = frxRec10.Page4
        end
        object Subreport3: TfrxSubreport
          AllowVectorExport = True
          Left = 508.346456692913000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Page = frxRec10.Page5
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Height = 18.897405910000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 740.787880000000000000
          Height = 18.897405910000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
    end
    object Page3: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      PrintOnPreviousPage = True
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 120.945043000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        OnAfterCalcHeight = 'MasterData4OnAfterCalcHeight'
        RowCount = 1
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 0.000092760000001135
          Width = 52.913380940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'COUROS')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 0.000092760000001135
          Width = 79.370090940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumFul."Pecas"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118212760000000000
          Width = 52.913380940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'PESO')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 15.118212760000000000
          Width = 79.370090940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumFul."PLE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236332760000010000
          Width = 52.913380940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'FALTA')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 30.236332760000010000
          Width = 79.370090940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354452760000000000
          Width = 52.913380940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '-')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 45.354452760000000000
          Width = 79.370090940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'N'#186' COUROS')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 0.000092760000001135
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAL DA CARGA')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 15.118212760000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 30.236332760000010000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 45.354452760000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
          WordWrap = False
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590692760000000000
          Width = 132.283510940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'M'#233'dia de peso ful'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 75.590692760000000000
          Width = 113.385860940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[PesoMedio]')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708812760000000000
          Width = 132.283510940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'M'#233'dia de peso anterior')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 90.708812760000000000
          Width = 113.385860940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Top = 105.826932760000000000
          Width = 132.283510940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Quebra')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 105.826932760000000000
          Width = 113.385860940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 245.669410940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          WordWrap = False
        end
      end
    end
    object Page4: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      PrintOnPreviousPage = True
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 71.811070000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'MasterData2OnBeforePrint'
        DataSet = frxDsFulonadas
        DataSetName = 'frxDsFulonadas'
        RowCount = 0
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149620940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[Line#]'#186' fulonada')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 64.251970940000000000
          Height = 15.118110240000000000
          DataField = 'PecasF'
          DataSet = frxDsFulonadas
          DataSetName = 'frxDsFulonadas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFulonadas."PecasF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 79.370090940000000000
          Height = 15.118110240000000000
          DataField = 'PesoF'
          DataSet = frxDsFulonadas
          DataSetName = 'frxDsFulonadas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFulonadas."PesoF"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236323000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'GroupHeader2OnBeforePrint'
        Condition = 'frxDsFulonadas."Controle"'
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Width = 226.771450940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFulonadas."NOMECLII"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 0.000126930000000000
          Top = 15.118212760000000000
          Width = 83.149620940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFulonadas."Lote"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149786930000000000
          Top = 15.118212760000000000
          Width = 64.251970940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401796930000000000
          Top = 15.118212760000000000
          Width = 79.370090940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 109.606370000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'GroupFooter2OnBeforePrint'
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149620940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 64.251970940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFulonadas."PecasF">,MasterData2,1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 79.370090940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFulonadas."PesoF">,MasterData2)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118120000000010000
          Width = 83.149620940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'FALTA')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 15.118120000000010000
          Width = 64.251970940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<frxDsFulonadas."PecasL"> - SUM(<frxDsFulonadas."PecasF">,Maste' +
              'rData2,1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Top = 15.118120000000010000
          Width = 79.370090940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<frxDsFulonadas."PesoL"> - SUM(<frxDsFulonadas."PesoF">,MasterD' +
              'ata2,1)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
    object Page5: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      PrintOnPreviousPage = True
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 71.811070000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'MasterData3OnBeforePrint'
        DataSet = frxDsLotes
        DataSetName = 'frxDsLotes'
        RowCount = 0
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149620940000000000
          Height = 15.118110240000000000
          DataField = 'Lote'
          DataSet = frxDsLotes
          DataSetName = 'frxDsLotes'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotes."Lote"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 64.251970940000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsLotes
          DataSetName = 'frxDsLotes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLotes."Pecas"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 79.370090940000000000
          Height = 15.118110240000000000
          DataField = 'Peso'
          DataSet = frxDsLotes
          DataSetName = 'frxDsLotes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLotes."Peso"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236323000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'GroupHeader1OnBeforePrint'
        Condition = 'frxDsLotes."Lote"'
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Width = 226.771450940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Couros carregados no ful'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 0.000126930000000000
          Top = 15.118212760000000000
          Width = 83.149620940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149786930000000000
          Top = 15.118212760000000000
          Width = 64.251970940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401796930000000000
          Top = 15.118212760000000000
          Width = 79.370090940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 109.606370000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'GroupFooter1OnBeforePrint'
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 0.000000000000014211
          Width = 83.149620940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 64.251970940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLotes."Pecas">,MasterData3,1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 79.370090940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLotes."Peso">,MasterData3,1)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
    object Page6: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      PrintOnPreviousPage = True
      object MeLinhas: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 143.622140000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'MeLinhasOnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
        RowCount = 0
        object Memo02: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708676060000000000
          Width = 71.811030940000000000
          Height = 13.228346460000000000
          DataField = 'PESO_TXT'
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLFormIts."PESO_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo01: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Width = 68.031510710000000000
          Height = 13.228346460000000000
          DataField = 'PERC_TXT'
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.LeftLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLFormIts."PERC_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo03: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519707010000000000
          Width = 139.842451340000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.RightLine.Width = 1.000000000000000000
          Memo.UTF8W = (
            '[frxDsLFormIts."Descricao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo05: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188976380000000000
          Width = 75.590560940000000000
          Height = 13.228346460000000000
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLFormIts."TEMPO_R_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo06: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Width = 64.251970940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INI')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo07: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Width = 64.251970940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FIM')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo08: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLFormIts."CustoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 672.756340000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLFormIts."CustoIt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo04: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Width = 105.826681340000000000
          Height = 13.228346460000000000
          DataField = 'Observacos'
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.RightLine.Width = 1.000000000000000000
          Memo.UTF8W = (
            '[frxDsLFormIts."Observacos"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Height = 13.228346460000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line8: TfrxLineView
          AllowVectorExport = True
          Left = 740.787880000000000000
          Height = 13.228346460000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 39.338590000000000000
        Top = 340.157700000000000000
        Width = 755.906000000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 470.000000000000000000
          Top = -0.000002440000002935
          Width = 270.677180000000000000
          Height = 16.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[TIME]-[DATE] - P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object CabGrupo1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007874020000000000
        Top = 102.047310000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'CabGrupo1OnBeforePrint'
        Condition = 'frxDsLFormIts."Etapa"'
        Stretched = True
        object MeEtapa: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519714330000000000
          Width = 245.669269370000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HideZeros = True
          Memo.UTF8W = (
            '[frxDsLFormIts."NomeEtapa"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Line7: TfrxLineView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Height = 17.007874020000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line10: TfrxLineView
          AllowVectorExport = True
          Left = 740.787880000000000000
          Height = 17.007874020000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
      object RodapeDeGrupo1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 181.417440000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'RodapeDeGrupo1OnBeforePrint'
        Stretched = True
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677165350000000000
          Width = 461.102196220000000000
          Height = 13.228346460000000000
          OnBeforePrint = 'Memo31OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HideZeros = True
          Memo.UTF8W = (
            '[VAR_OBSERPROCES]')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLFormIts."CustoIt">,MeLinhas,1)  /  <PesoFulao>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 672.756340000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLFormIts."CustoIt">,MeLinhas,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Width = 128.503980940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Custo [frxDsBMZ."NomeEtapa"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Height = 13.228346460000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line9: TfrxLineView
          AllowVectorExport = True
          Left = 740.787880000000000000
          Height = 13.228346460000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 60.472480000000000000
        Top = 257.008040000000000000
        Width = 755.906000000000000000
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456695350000000000
          Top = 30.236237560000000000
          Width = 713.228510000000000000
          Height = 16.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_TEMPORECEITA]')
          ParentFont = False
        end
        object Line6: TfrxLineView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line11: TfrxLineView
          AllowVectorExport = True
          Left = 740.787880000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line12: TfrxLineView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Top = 49.133890000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 7.559059999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLFormIts."CustoIt">,MeLinhas,1)  /  <PesoFulao>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 672.756340000000000000
          Top = 7.559059999999990000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLFormIts."CustoIt">,MeLinhas,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 7.559059999999990000
          Width = 128.503980940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Custo total [frxDsFormulas."NOMESETOR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787406460000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708676060000000000
          Top = 7.559060000000000000
          Width = 71.811030940000000000
          Height = 13.228346460000000000
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg/L')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Top = 7.559060000000000000
          Width = 68.031510710000000000
          Height = 13.228346460000000000
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.LeftLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% uso')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519707010000000000
          Top = 7.559060000000000000
          Width = 139.842451340000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.RightLine.Width = 1.000000000000000000
          Memo.UTF8W = (
            'Produto / texto')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188976380000000000
          Top = 7.559060000000000000
          Width = 75.590560940000000000
          Height = 13.228346460000000000
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Tempo roda')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 7.559060000000000000
          Width = 128.503980940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Inicio e fim de fulonamento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 7.559060000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ kg produto')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 672.756340000000000000
          Top = 7.559060000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ item')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 7.559060000000000000
          Width = 105.826681340000000000
          Height = 13.228346460000000000
          DataSet = frxDsLFormIts
          DataSetName = 'frxDsLFormIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.RightLine.Width = 1.000000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrLotes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ec.Controle, ec.MPIn, ec.Peso, ec.Pecas, '
      'mi.Ficha, mi.Marca, mi.Lote, ec.VSMovIts'
      'FROM emitcus ec'
      'LEFT JOIN mpin mi ON mi.Controle=ec.MPIn'
      'WHERE ec.Codigo=:P0')
    Left = 644
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotesControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLotesMPIn: TIntegerField
      FieldName = 'MPIn'
      Required = True
    end
    object QrLotesPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrLotesPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrLotesFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrLotesMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrLotesLote: TWideStringField
      FieldName = 'Lote'
      Size = 11
    end
  end
  object QrFulonadas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mpi.Controle, mpi.Lote, emc.Peso PesoF, '
      'emc.Pecas PecasF, mpi.PLE PesoL, mpi.Pecas PecasL,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLII'
      'FROM Emit emi'
      'LEFT JOIN emitcus emc ON emc.Codigo=emi.Codigo'
      'LEFT JOIN MPIn mpi ON mpi.Controle=emc.MPIn'
      'LEFT JOIN Entidades ent ON ent.Codigo=mpi.ClienteI'
      ''
      'WHERE emi.Setor>=:P0'
      'AND emc.MPIn IN (:P1)'
      ''
      'ORDER BY mpi.Data, mpi.Controle')
    Left = 644
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFulonadasLote: TWideStringField
      FieldName = 'Lote'
      Size = 11
    end
    object QrFulonadasPesoF: TFloatField
      FieldName = 'PesoF'
    end
    object QrFulonadasPecasF: TFloatField
      FieldName = 'PecasF'
    end
    object QrFulonadasPesoL: TFloatField
      FieldName = 'PesoL'
      Required = True
    end
    object QrFulonadasPecasL: TFloatField
      FieldName = 'PecasL'
      Required = True
    end
    object QrFulonadasNOMECLII: TWideStringField
      FieldName = 'NOMECLII'
      Size = 100
    end
    object QrFulonadasControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object frxDsLotes: TfrxDBDataset
    UserName = 'frxDsLotes'
    CloseDataSource = False
    DataSet = QrLotes
    BCDToCurrency = False
    DataSetOptions = []
    Left = 672
    Top = 168
  end
  object frxDsFulonadas: TfrxDBDataset
    UserName = 'frxDsFulonadas'
    CloseDataSource = False
    DataSet = QrFulonadas
    BCDToCurrency = False
    DataSetOptions = []
    Left = 672
    Top = 112
  end
  object QrSumFul: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(mpi.Pecas) Pecas, SUM(mpi.PLE) PLE'
      'FROM Emit emi'
      'LEFT JOIN emitcus emc ON emc.Codigo=emi.Codigo'
      'LEFT JOIN MPIn mpi ON mpi.Controle=emc.MPIn'
      ''
      'WHERE emi.Setor=3'
      'AND emc.MPIn IN (2)'
      ''
      'ORDER BY mpi.Data, mpi.Controle'
      '')
    Left = 644
    Top = 140
  end
  object frxDsSumFul: TfrxDBDataset
    UserName = 'frxDsSumFul'
    CloseDataSource = False
    DataSet = QrSumFul
    BCDToCurrency = False
    DataSetOptions = []
    Left = 672
    Top = 144
  end
  object frxBMZMini: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38077.792695474500000000
    ReportOptions.Name = 'Receita Skintan'
    ReportOptions.LastChange = 39541.569016562500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MeLinhasOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with MeLinhas, Engine do'
      '  begin'
      '  if <VAR_TXT> = '#39#39' then'
      '    MeLinhas.Visible := False'
      '  else'
      '    MeLinhas.Visible := True;'
      '  end'
      'end;'
      ''
      'procedure RodapeDeGrupo1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with RodapeDeGrupo1, Engine do'
      '  begin'
      '  RodapeDeGrupo1.Visible := <VAR_TEMOBSERPROCES>;'
      '  end'
      'end;'
      ''
      'procedure RodapeDeGrupo2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with RodapeDeGrupo2, Engine do'
      '  begin'
      '    RodapeDeGrupo2.Visible := MeLinhas.Visible;'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 560
    Top = 112
    Datasets = <
      item
        DataSet = frxDsBMZ
        DataSetName = 'frxDsBMZ'
      end
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
      end
      item
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MeLinhas: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338582680000000000
        Top = 275.905690000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'MeLinhasOnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
        RowCount = 0
        object MeAddPQ: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Width = 720.000000000000000000
          Height = 11.338582677165350000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          HideZeros = True
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 25.118120000000000000
          Width = 88.000000000000000000
          Height = 11.338582677165350000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLFormIts."PESO_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 673.118120000000000000
          Width = 64.000000000000000000
          Height = 11.338582677165350000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLFormIts."PERC_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 117.118120000000000000
          Width = 188.000000000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsLFormIts."DESCRI_TEMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 309.118120000000000000
          Width = 360.000000000000000000
          Height = 11.338582677165350000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsLFormIts."TXT_DILUICAO"] [frxDsLFormIts."Observacos"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Cabecalho: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 114.188930000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 32.125964720000000000
          Width = 41.102350000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 62.220470000000000000
          Top = 32.125964720000000000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMECI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 47.244074960000010000
          Width = 41.102350000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 62.220470000000000000
          Top = 47.244074960000010000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 62.362185200000000000
          Width = 41.102350000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Relativo a:')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 62.220470000000000000
          Top = 62.362185200000000000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 189.968460000000000000
          Top = 32.125964720000000000
          Width = 41.322820000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'T'#233'cnico:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 231.291280000000000000
          Top = 32.125964720000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."Tecnico"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 189.968460000000000000
          Top = 47.244074960000010000
          Width = 41.322820000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Espessura:')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 231.291280000000000000
          Top = 47.244074960000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Espessura]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 189.968460000000000000
          Top = 62.362185200000000000
          Width = 41.322820000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 231.291280000000000000
          Top = 62.362185200000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 79.370059210000000000
          Width = 104.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita:')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 189.968460000000000000
          Top = 77.480295430000000000
          Width = 41.322820000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantia:')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 231.291280000000000000
          Top = 77.480295430000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 309.716450000000000000
          Top = 47.244074960000010000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            #193'rea:')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 350.377860000000000000
          Top = 47.244074960000010000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Area]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 309.716450000000000000
          Top = 62.362185200000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Rendim.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 350.377860000000000000
          Top = 62.362185200000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 309.716450000000000000
          Top = 77.480295430000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Data:')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 350.377860000000000000
          Top = 77.480295430000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 20.118120000000000000
          Top = 32.188930000000000000
          Width = 721.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 32.188930000000000000
          Height = 81.322819999999990000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 2.000000000000000000
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 741.118120000000000000
          Top = 32.188930000000000000
          Height = 81.322819999999990000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 2.000000000000000000
        end
        object Logo: TfrxPictureView
          AllowVectorExport = True
          Left = 619.118120000000000000
          Top = 34.188930000000000000
          Width = 116.000000000000000000
          Height = 80.000000000000000000
          Center = True
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 621.118120000000000000
          Top = 2.425170000000001000
          Width = 112.000000000000000000
          Height = 29.543290000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -35
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FU]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 309.716450000000000000
          Top = 32.125964720000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'M'#233'dia:')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 350.377860000000000000
          Top = 32.125964720000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[PesoMedio]')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 2.425170000000001000
          Width = 388.000000000000000000
          Height = 24.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PESAGEM] - [frxDsFormulas."NOMESETOR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 420.677180000000000000
          Top = 32.661409999999990000
          Width = 192.661410000000000000
          Height = 56.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[OBS]')
          ParentFont = False
          WordWrap = False
        end
        object MeObjetivo: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 93.511750000000000000
          Width = 595.275510000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line8: TfrxLineView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Top = 34.015770000000010000
          Height = 75.590600000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 2.000000000000000000
        end
        object Line9: TfrxLineView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Top = 113.385900000000000000
          Width = 124.724490000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 39.338590000000000000
        Top = 461.102660000000000000
        Width = 755.906000000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 470.000000000000000000
          Top = -0.000002440000002935
          Width = 270.677180000000000000
          Height = 16.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[TIME]-[DATE] - P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object CabGrupo1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 192.756030000000000000
        Width = 755.906000000000000000
        Condition = 'frxDsLFormIts."Etapa"'
        Stretched = True
        object MeEtapa: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Width = 720.000000000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          HideZeros = True
          Memo.UTF8W = (
            'ETAPA [frxDsLFormIts."Etapa"]: [frxDsLFormIts."NomeEtapa"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object RodapeDeGrupo1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 347.716760000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'RodapeDeGrupo1OnBeforePrint'
        Stretched = True
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 0.527210000000025000
          Width = 720.000000000000000000
          Color = clBlack
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftTop]
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Width = 720.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Style = fsDashDot
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HideZeros = True
          Memo.UTF8W = (
            '[VAR_OBSERPROCES]')
          ParentFont = False
          WordWrap = False
        end
        object Line7: TfrxLineView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 0.377952760000027800
          Height = 14.740157480314960000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line6: TfrxLineView
          AllowVectorExport = True
          Left = 741.118120000000000000
          Top = 0.377952760000027800
          Height = 14.740157480314960000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Top = 14.740157480314960000
          Width = 720.000000000000000000
          Color = clBlack
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftTop]
        end
      end
      object CabecalhoDeGrupo1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 0.377952755905511800
        Top = 230.551330000000000000
        Visible = False
        Width = 755.906000000000000000
        Condition = 'frxDsLFormIts."SubEtapa"'
      end
      object RodapeDeGrupo2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110236220470000
        Top = 309.921460000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'RodapeDeGrupo2OnBeforePrint'
        object MeRodar: TfrxMemoView
          AllowVectorExport = True
          Left = 21.118120000000000000
          Width = 720.000000000000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HideZeros = True
          Memo.UTF8W = (
            '[frxDsLFormIts."TEMPO_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object Band1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 0.377952755905511800
        Top = 253.228510000000000000
        Visible = False
        Width = 755.906000000000000000
        Condition = 'frxDsLFormIts."SubEtapa"'
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118107800000000000
        Top = 423.307360000000000000
        Width = 755.906000000000000000
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 21.165354330000000000
          Width = 444.881880000000000000
          Height = 15.118107800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_TEMPORECEITA]')
          ParentFont = False
        end
      end
    end
  end
  object frxRec1_MS: TfrxReport
    Version = '2022.1'
    DotMatrixReport = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39752.572895682900000000
    ReportOptions.LastChange = 39752.572895682900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 532
    Top = 112
    Datasets = <
      item
      end
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
      end
      item
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxDMPPage
      PaperWidth = 248.919839239270500000
      PaperHeight = 278.870653229369800000
      PaperSize = 256
      Frame.Typ = []
      MirrorMode = []
      FontStyle = []
      object Band1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 17.000000000000000000
        ParentFont = False
        Top = 204.000000000000000000
        Width = 940.800000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
        RowCount = 0
        object DMPMemo1: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold]
          Frame.Typ = [ftLeft, ftRight]
          HAlign = haRight
          Memo.UTF8W = (
            '[kgLCusto]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo3: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 201.600000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."Produto' +
              '">)]')
          TruncOutboundText = False
        end
        object DMPMemo4: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 268.800000000000000000
          Width = 268.800000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold]
          Frame.Typ = [ftLeft, ftRight]
          Memo.UTF8W = (
            '[frxDsFormulasIts."Nome"]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo5: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 115.200000000000000000
          Width = 76.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39',<frxDsFormulasIts.' +
              '"Porcent">)]')
          TruncOutboundText = False
        end
        object DMPMemo2: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 547.200000000000000000
          Width = 28.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."Graus">' +
              ')]')
          TruncOutboundText = False
        end
        object DMPMemo11: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 624.000000000000000000
          Width = 153.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulasIts."Obs"]')
          TruncOutboundText = False
        end
        object DMPMemo13: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 787.200000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          Memo.UTF8W = (
            '[Inicio]')
          TruncOutboundText = False
        end
        object DMPMemo14: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 854.400000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          Memo.UTF8W = (
            '[Final]')
          TruncOutboundText = False
        end
        object DMPMemo34: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 585.600000000000000000
          Width = 28.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."TempoR"' +
              '>)]')
          TruncOutboundText = False
        end
      end
      object Band3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 17.000000000000000000
        ParentFont = False
        Top = 170.000000000000000000
        Width = 940.800000000000000000
        object DMPMemo6: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold]
          Frame.Typ = [ftLeft, ftRight]
          HAlign = haRight
          Memo.UTF8W = (
            'kg/L')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo7: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 201.600000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          HAlign = haRight
          Memo.UTF8W = (
            'Cod.')
          TruncOutboundText = False
        end
        object DMPMemo8: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 268.800000000000000000
          Width = 268.800000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold]
          Frame.Typ = [ftLeft, ftRight]
          Memo.UTF8W = (
            'Nome produto / texto')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo9: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 115.200000000000000000
          Width = 76.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          HAlign = haRight
          Memo.UTF8W = (
            '%')
          TruncOutboundText = False
        end
        object DMPMemo10: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 547.200000000000000000
          Width = 28.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          HAlign = haCenter
          Memo.UTF8W = (
            'oC')
          TruncOutboundText = False
        end
        object DMPMemo12: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 624.000000000000000000
          Width = 153.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          HAlign = haCenter
          Memo.UTF8W = (
            'Observacoes')
          TruncOutboundText = False
        end
        object DMPMemo15: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 787.200000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          Memo.UTF8W = (
            'Inicio')
          TruncOutboundText = False
        end
        object DMPMemo16: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 854.400000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          Memo.UTF8W = (
            'Final')
          TruncOutboundText = False
        end
        object DMPMemo35: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 585.600000000000000000
          Width = 28.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight]
          HAlign = haCenter
          Memo.UTF8W = (
            'Roda')
          TruncOutboundText = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 34.000000000000000000
        ParentFont = False
        Top = 17.000000000000000000
        Width = 940.800000000000000000
        object DMPMemo17: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 556.800000000000000000
          Width = 355.200000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Fulao [FU]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo18: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Width = 211.200000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[OS]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo19: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 220.800000000000000000
          Width = 211.200000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PESAGEM]')
          ParentFont = False
          TruncOutboundText = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 34.000000000000000000
        ParentFont = False
        Top = 289.000000000000000000
        Width = 940.800000000000000000
        object DMPMemo33: TfrxDMPMemoView
          AllowVectorExport = True
          Left = -9.600000000000000000
          Width = 921.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]   -   pagina [Page#] de [TotalPages#]')
          TruncOutboundText = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 68.000000000000000000
        ParentFont = False
        Top = 68.000000000000000000
        Width = 940.800000000000000000
        object DMPMemo20: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Width = 86.400000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita:')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo21: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 105.600000000000000000
          Width = 806.400000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo22: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Top = 17.000000000000000000
          Width = 86.400000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Espessura:')
          TruncOutboundText = False
        end
        object DMPMemo23: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 105.600000000000000000
          Top = 17.000000000000000000
          Width = 153.600000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Espessura]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo24: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 240.000000000000000000
          Top = 34.000000000000000000
          Width = 105.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Quantidade:')
          TruncOutboundText = False
        end
        object DMPMemo25: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 355.200000000000000000
          Top = 34.000000000000000000
          Width = 144.000000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo26: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Top = 34.000000000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Peso:')
          TruncOutboundText = False
        end
        object DMPMemo27: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 67.200000000000000000
          Top = 34.000000000000000000
          Width = 163.200000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo28: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 480.000000000000000000
          Top = 17.000000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso medio:')
          TruncOutboundText = False
        end
        object DMPMemo29: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 585.600000000000000000
          Top = 17.000000000000000000
          Width = 326.400000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[PesoMedio]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo30: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 508.800000000000000000
          Top = 34.000000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Area:')
          TruncOutboundText = False
        end
        object DMPMemo31: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 614.400000000000000000
          Top = 34.000000000000000000
          Width = 297.600000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            '[Area]')
          ParentFont = False
          TruncOutboundText = False
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 17.000000000000000000
        ParentFont = False
        Top = 238.000000000000000000
        Width = 940.800000000000000000
        object DMPMemo32: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Top = 17.000000000000000000
          Width = 902.400000000000000000
          Frame.Typ = [ftTop]
          HAlign = haRight
          TruncOutboundText = False
        end
      end
    end
  end
  object frxDotMatrixExport1: TfrxDotMatrixExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    EscModel = 0
    GraphicFrames = False
    SaveToFile = False
    UseIniSettings = True
    Left = 536
    Top = 176
  end
  object frxReport1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39752.572895682900000000
    ReportOptions.LastChange = 39752.572895682900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 484
    Top = 160
    Datasets = <
      item
      end
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
      end
      item
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxDMPPage
      PaperWidth = 210.819863845504600000
      PaperHeight = 296.862308276425900000
      PaperSize = 9
      Frame.Typ = []
      MirrorMode = []
      FontStyle = []
      object Band1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 34.000000000000000000
        ParentFont = False
        Top = 238.000000000000000000
        Width = 796.800000000000100000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
        RowCount = 0
        object DMPMemo1: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[kgLCusto]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo3: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 201.600000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."Produto' +
              '">)]')
          TruncOutboundText = False
        end
        object DMPMemo4: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 268.800000000000000000
          Width = 192.000000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFormulasIts."Nome"]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo5: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 115.200000000000000000
          Width = 76.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39',<frxDsFormulasIts.' +
              '"Porcent">)]')
          TruncOutboundText = False
        end
        object DMPMemo2: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 470.400000000000000000
          Width = 28.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."TempoR"' +
              '>)]')
          TruncOutboundText = False
        end
        object DMPMemo11: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 508.800000000000000000
          Width = 134.400000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulasIts."Obs"]')
          TruncOutboundText = False
        end
        object DMPMemo13: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 652.800000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Inicio]')
          TruncOutboundText = False
        end
        object DMPMemo14: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 720.000000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Final]')
          TruncOutboundText = False
        end
      end
      object Band3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 34.000000000000000000
        ParentFont = False
        Top = 187.000000000000000000
        Width = 796.800000000000100000
        object DMPMemo6: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'kg/L')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo7: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 201.600000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd.')
          TruncOutboundText = False
        end
        object DMPMemo8: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 268.800000000000000000
          Width = 192.000000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Nome produto / texto')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo9: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 115.200000000000000000
          Width = 76.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '%')
          TruncOutboundText = False
        end
        object DMPMemo10: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 470.400000000000000000
          Width = 28.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Roda')
          TruncOutboundText = False
        end
        object DMPMemo12: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 508.800000000000000000
          Width = 134.400000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#227'oes')
          TruncOutboundText = False
        end
        object DMPMemo15: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 652.800000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'In'#237'cio')
          TruncOutboundText = False
        end
        object DMPMemo16: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 720.000000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Final')
          TruncOutboundText = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 34.000000000000000000
        ParentFont = False
        Top = 17.000000000000000000
        Width = 796.800000000000100000
        object DMPMemo17: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 432.000000000000000000
          Width = 355.200000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Fulao [FU]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo18: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Width = 211.200000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[OS]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo19: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 220.800000000000000000
          Width = 211.200000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PESAGEM]')
          ParentFont = False
          TruncOutboundText = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 34.000000000000000000
        ParentFont = False
        Top = 374.000000000000000000
        Width = 796.800000000000100000
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 85.000000000000000000
        ParentFont = False
        Top = 68.000000000000000000
        Width = 796.800000000000100000
        object DMPMemo20: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Width = 67.200000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita:')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo21: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 76.800000000000000000
          Width = 710.400000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo22: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Top = 17.000000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Espessura:')
          TruncOutboundText = False
        end
        object DMPMemo23: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 105.600000000000000000
          Top = 17.000000000000000000
          Width = 153.600000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Espessura]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo24: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 259.200000000000000000
          Top = 17.000000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantidade:')
          TruncOutboundText = False
        end
        object DMPMemo25: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 355.200000000000000000
          Top = 17.000000000000000000
          Width = 153.600000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo26: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Top = 34.000000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso:')
          TruncOutboundText = False
        end
        object DMPMemo27: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 105.600000000000000000
          Top = 34.000000000000000000
          Width = 153.600000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo28: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 259.200000000000000000
          Top = 34.000000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso m'#233'dio:')
          TruncOutboundText = False
        end
        object DMPMemo29: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 355.200000000000000000
          Top = 34.000000000000000000
          Width = 153.600000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[PesoMedio]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo30: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Top = 51.000000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            #193'rea:')
          TruncOutboundText = False
        end
        object DMPMemo31: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 105.600000000000000000
          Top = 51.000000000000000000
          Width = 153.600000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Area]')
          ParentFont = False
          TruncOutboundText = False
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 51.000000000000000000
        ParentFont = False
        Top = 289.000000000000000000
        Width = 796.800000000000100000
      end
    end
  end
  object frxRec1_ML: TfrxReport
    Version = '2022.1'
    DotMatrixReport = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39752.572895682900000000
    ReportOptions.LastChange = 39752.572895682900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 532
    Top = 140
    Datasets = <
      item
      end
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
      end
      item
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxDMPPage
      PaperWidth = 248.919839239270500000
      PaperHeight = 278.870653229369800000
      PaperSize = 256
      Frame.Typ = []
      MirrorMode = []
      FontStyle = []
      object Band1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 34.000000000000000000
        ParentFont = False
        Top = 221.000000000000000000
        Width = 940.800000002757200000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
        RowCount = 0
        object DMPMemo1: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[kgLCusto]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo3: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 201.600000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."Produto' +
              '">)]')
          TruncOutboundText = False
        end
        object DMPMemo4: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 268.800000000000000000
          Width = 268.800000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Memo.UTF8W = (
            '[frxDsFormulasIts."Nome"]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo5: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 115.200000000000000000
          Width = 76.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39',<frxDsFormulasIts.' +
              '"Porcent">)]')
          TruncOutboundText = False
        end
        object DMPMemo2: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 547.200000000000000000
          Width = 28.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."Graus">' +
              ')]')
          TruncOutboundText = False
        end
        object DMPMemo11: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 624.000000000000000000
          Width = 153.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulasIts."Obs"]')
          TruncOutboundText = False
        end
        object DMPMemo13: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 787.200000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Memo.UTF8W = (
            '[Inicio]')
          TruncOutboundText = False
        end
        object DMPMemo14: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 854.400000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Memo.UTF8W = (
            '[Final]')
          TruncOutboundText = False
        end
        object DMPMemo34: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 585.600000000000000000
          Width = 28.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."TempoR"' +
              '>)]')
          TruncOutboundText = False
        end
      end
      object Band3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 34.000000000000000000
        ParentFont = False
        Top = 170.000000000000000000
        Width = 940.800000002757200000
        object DMPMemo6: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            'kg/L')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo7: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 201.600000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            'Cod.')
          TruncOutboundText = False
        end
        object DMPMemo8: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 268.800000000000000000
          Width = 268.800000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Memo.UTF8W = (
            'Nome produto / texto')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo9: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 115.200000000000000000
          Width = 76.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '%')
          TruncOutboundText = False
        end
        object DMPMemo10: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 547.200000000000000000
          Width = 28.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            'oC')
          TruncOutboundText = False
        end
        object DMPMemo12: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 624.000000000000000000
          Width = 153.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            'Observacoes')
          TruncOutboundText = False
        end
        object DMPMemo15: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 787.200000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Memo.UTF8W = (
            'Inicio')
          TruncOutboundText = False
        end
        object DMPMemo16: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 854.400000000000000000
          Width = 57.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Memo.UTF8W = (
            'Final')
          TruncOutboundText = False
        end
        object DMPMemo35: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 585.600000000000000000
          Width = 28.800000000000000000
          Height = 17.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            'Roda')
          TruncOutboundText = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 34.000000000000000000
        ParentFont = False
        Top = 17.000000000000000000
        Width = 940.800000002757200000
        object DMPMemo17: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 556.800000000000000000
          Width = 355.200000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Fulao [FU]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo18: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Width = 211.200000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[OS]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo19: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 220.800000000000000000
          Width = 211.200000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PESAGEM]')
          ParentFont = False
          TruncOutboundText = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 34.000000000000000000
        ParentFont = False
        Top = 323.000000000000000000
        Width = 940.800000002757200000
        object DMPMemo33: TfrxDMPMemoView
          AllowVectorExport = True
          Left = -9.600000000000000000
          Width = 921.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]   -   pagina [Page#] de [TotalPages#]')
          TruncOutboundText = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 68.000000000000000000
        ParentFont = False
        Top = 68.000000000000000000
        Width = 940.800000002757200000
        object DMPMemo20: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Width = 86.400000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita:')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo21: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 105.600000000000000000
          Width = 700.800000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo22: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Top = 17.000000000000000000
          Width = 86.400000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Espessura:')
          TruncOutboundText = False
        end
        object DMPMemo23: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 105.600000000000000000
          Top = 17.000000000000000000
          Width = 153.600000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Espessura]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo24: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 240.000000000000000000
          Top = 34.000000000000000000
          Width = 105.600000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantidade:')
          TruncOutboundText = False
        end
        object DMPMemo25: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 355.200000000000000000
          Top = 34.000000000000000000
          Width = 144.000000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo26: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Top = 34.000000000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso:')
          TruncOutboundText = False
        end
        object DMPMemo27: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 67.200000000000000000
          Top = 34.000000000000000000
          Width = 163.200000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo28: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 480.000000000000000000
          Top = 17.000000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso medio:')
          TruncOutboundText = False
        end
        object DMPMemo29: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 585.600000000000000000
          Top = 17.000000000000000000
          Width = 220.800000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[PesoMedio]')
          ParentFont = False
          TruncOutboundText = False
        end
        object DMPMemo30: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 508.800000000000000000
          Top = 34.000000000000000000
          Width = 96.000000000000000000
          Height = 17.000000000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Area:')
          TruncOutboundText = False
        end
        object DMPMemo31: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 614.400000000000000000
          Top = 34.000000000000000000
          Width = 192.000000000000000000
          Height = 17.000000000000000000
          FontStyle = [fsxBold, fsxWide]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Area]')
          ParentFont = False
          TruncOutboundText = False
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Height = 17.000000000000000000
        ParentFont = False
        Top = 272.000000000000000000
        Width = 940.800000002757200000
        object DMPMemo32: TfrxDMPMemoView
          AllowVectorExport = True
          Left = 9.600000000000000000
          Width = 902.400000000000000000
          Frame.Typ = [ftTop]
          HAlign = haRight
          TruncOutboundText = False
        end
      end
    end
  end
  object frxRecIdeal: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38017.353737407400000000
    ReportOptions.LastChange = 40674.647764259260000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <LogoReceExiste> = True then'
      '    Picture1.LoadFromFile(<LogoRecePath>);  '
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 644
    Top = 56
    Datasets = <
      item
        DataSet = frxDsBMZ
        DataSetName = 'frxDsBMZ'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
      end
      item
        DataSet = frxDsFulonadas
        DataSetName = 'frxDsFulonadas'
      end
      item
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
      end
      item
        DataSet = frxDsLotes
        DataSetName = 'frxDsLotes'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPreUso
        DataSetName = 'frxDsPreUso'
      end
      item
        DataSet = frxDsSumFul
        DataSetName = 'frxDsSumFul'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      object Band1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677165350000000000
        Top = 336.378170000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
        RowCount = 0
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 90.708661420000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[kgLCusto]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622027720000000000
          Width = 173.858289690000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFormulasIts."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480292990000000000
          Width = 18.897637800000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '##0; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."Graus"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 336.377930790000000000
          Width = 26.456692910000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."TempoR"' +
              '>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834623700000000000
          Width = 26.456692910000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."TempoP"' +
              '>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291306850000000000
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          DataSet = frxDsFormulasIts
          DataSetName = 'frxDsFormulasIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulasIts."pH_All"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645652200000000000
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          DataField = 'Be_All'
          DataSet = frxDsFormulasIts
          DataSetName = 'frxDsFormulasIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulasIts."Be_All"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 479.999980470000000000
          Width = 102.047244090000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFormulasIts."Obs"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047224570000000000
          Width = 49.133858270000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Inicio]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181082830000000000
          Width = 49.133858270000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Final]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708661420000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,##0.000; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."Porcent"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007874020000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622027720000000000
          Width = 173.858289690000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Nome do produto ou texto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480292990000000000
          Width = 18.897637800000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #186'C')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 336.377930790000000000
          Width = 26.456692910000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Roda')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834623700000000000
          Width = 26.456692910000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'ra')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291306850000000000
          Width = 45.354330710000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'pH')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645652200000000000
          Width = 45.354330710000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'B'#233)
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 479.999980470000000000
          Width = 102.047244090000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047224570000000000
          Width = 49.133858270000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Ini_txt]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181082830000000000
          Width = 49.133858270000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Fin_txt]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708661420000000000
          Width = 52.913385830000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Uso %')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Width = 90.708661420000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Uso kg/L]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 215.433210000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Top = 173.858380000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Left = 168.188976377952800000
          Top = 64.252010000000000000
          Width = 249.448818897637800000
          Height = 105.826840000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Top = 64.252010000000000000
          Width = 90.708720000000000000
          Height = 105.826840000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 341.559060000000000000
          Top = 173.857884490000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Custo_kg]')
          ParentFont = False
          VAlign = vaCenter
        end
        object meCustokg: TfrxMemoView
          AllowVectorExport = True
          Left = 402.440940000000000000
          Top = 173.857884490000000000
          Width = 128.503937010000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Custokg]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 148.803030000000000000
          Top = 192.755522280000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Custo_ton]')
          ParentFont = False
          VAlign = vaCenter
        end
        object meCustoTon: TfrxMemoView
          AllowVectorExport = True
          Left = 209.684910000000000000
          Top = 192.755522280000000000
          Width = 128.503937010000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CustoTon]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 148.803030000000000000
          Top = 173.857884490000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Custo_total]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 209.684910000000000000
          Top = 173.857884490000000000
          Width = 128.503937010000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CustoTotal]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 341.559060000000000000
          Top = 192.755522280000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Custo_m]')
          ParentFont = False
          VAlign = vaCenter
        end
        object meCustom2: TfrxMemoView
          AllowVectorExport = True
          Left = 402.440940000000000000
          Top = 192.755522280000000000
          Width = 128.503937010000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Custom2]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT_TXT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Receita de [frxDsFormulas."NOMESETOR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 75.590536540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[DATE],')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
        end
        object meLotes: TfrxMemoView
          AllowVectorExport = True
          Left = 512.645640000000000000
          Top = 64.251587720000000000
          Width = 166.299212600000000000
          Height = 106.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Lotes]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527529760000000000
          Top = 60.472057720000000000
          Width = 90.708661420000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ful'#227'o:')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527529760000000000
          Top = 90.708278190000000000
          Width = 90.708661420000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -64
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FU]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 182.818800000000000000
          Top = 65.952690000000000000
          Width = 72.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Espessura:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 182.818800000000000000
          Top = 85.952690000000000000
          Width = 72.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quantidade:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 182.818800000000000000
          Top = 105.952690000000000000
          Width = 72.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 182.818800000000000000
          Top = 125.952690000000000000
          Width = 72.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Peso m'#233'dio:')
          ParentFont = False
          VAlign = vaBottom
        end
        object meEspessura: TfrxMemoView
          AllowVectorExport = True
          Left = 258.818800000000000000
          Top = 65.952690000000000000
          Width = 142.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Espessura]')
          ParentFont = False
          VAlign = vaBottom
        end
        object meQtde: TfrxMemoView
          AllowVectorExport = True
          Left = 258.818800000000000000
          Top = 85.952690000000000000
          Width = 142.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
          VAlign = vaBottom
        end
        object mePeso: TfrxMemoView
          AllowVectorExport = True
          Left = 258.818800000000000000
          Top = 105.952690000000000000
          Width = 142.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
          VAlign = vaBottom
        end
        object mePesoMedio: TfrxMemoView
          AllowVectorExport = True
          Left = 258.818800000000000000
          Top = 125.952690000000000000
          Width = 142.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[PesoMedio]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 182.818800000000000000
          Top = 145.952690000000000000
          Width = 72.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            #193'rea m'#178':')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 258.818800000000000000
          Top = 145.952690000000000000
          Width = 142.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Area]')
          ParentFont = False
          VAlign = vaBottom
        end
        object meOS: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779527560000000000
          Top = 173.858270160000000000
          Width = 143.622059450000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[OS]')
          ParentFont = False
          VAlign = vaCenter
        end
        object meSP: TfrxMemoView
          AllowVectorExport = True
          Left = 532.614410000000000000
          Top = 173.858270160000000000
          Width = 143.622059450000000000
          Height = 37.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_PESAGEM]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 18.897650000000000000
          Width = 68.031476540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[TIME]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Top = 64.251587720000000000
          Width = 166.299212600000000000
          Height = 105.826771650000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 59.559060000000000000
        Top = 381.732530000000000000
        Width = 680.315400000000000000
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559057560000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pesador1:')
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 56.692913390000000000
          Top = 26.456695350000000000
          Width = 283.464566930000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 340.204700000000000000
          Top = 7.559055120000000000
          Width = 52.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fuloneiro1:')
          ParentFont = False
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 396.204700000000000000
          Top = 26.456692910000000000
          Width = 283.464566930000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Top = 34.015752910000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pesador2:')
          ParentFont = False
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 56.692913390000000000
          Top = 52.913390710000000000
          Width = 283.464566930000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 340.204700000000000000
          Top = 34.015748030000000000
          Width = 52.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fuloneiror2:')
          ParentFont = False
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 396.204700000000000000
          Top = 52.913385830000000000
          Width = 283.464566930000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 502.677490000000000000
        Width = 680.315400000000000000
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object QrVMIOri: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_FORNECE, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 144
    Top = 385
    object QrVMIOriCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVMIOriControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVMIOriMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVMIOriMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVMIOriMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVMIOriEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVMIOriTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVMIOriCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVMIOriMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVMIOriDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVMIOriPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVMIOriGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVMIOriPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVMIOriSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVMIOriSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVMIOriSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVMIOriSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVMIOriSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVMIOriSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVMIOriFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVMIOriMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVMIOriFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVMIOriCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVMIOriDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVMIOriDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVMIOriDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVMIOriQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVMIOriQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVMIOriQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVMIOriNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVMIOriNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVMIOriNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVMIOriID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVMIOriNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVMIOriNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVMIOriReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVMIOriNO_ClientMO: TWideStringField
      FieldName = 'NO_ClientMO'
      Size = 100
    end
    object QrVMIOriMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVMIOriClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVMIOriFatorImp: TFloatField
      FieldName = 'FatorImp'
    end
  end
  object frxDsVMIOri: TfrxDBDataset
    UserName = 'frxDsVMIOri'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'NO_PALLET=NO_PALLET'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch'
      'ReqMovEstq=ReqMovEstq'
      'NO_ClientMO=NO_ClientMO'
      'Marca=Marca'
      'ClientMO=ClientMO'
      'FatorImp=FatorImp')
    DataSet = QrVMIOri
    BCDToCurrency = False
    DataSetOptions = []
    Left = 144
    Top = 432
  end
  object frxRec1_BH: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38017.353737407400000000
    ReportOptions.LastChange = 41853.389075254630000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 84
    Top = 52
    Datasets = <
      item
        DataSet = frxDsBMZ
        DataSetName = 'frxDsBMZ'
      end
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
      end
      item
        DataSet = frxDsFulonadas
        DataSetName = 'frxDsFulonadas'
      end
      item
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
      end
      item
        DataSet = frxDsLotes
        DataSetName = 'frxDsLotes'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPreUso
        DataSetName = 'frxDsPreUso'
      end
      item
        DataSet = frxDsSumFul
        DataSetName = 'frxDsSumFul'
      end
      item
        DataSet = frxDsVMIOri
        DataSetName = 'frxDsVMIOri'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      object Band1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 423.307360000000000000
        Width = 737.008350000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 714.330711100000000000
          Width = 22.677165350000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."Seq"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 90.708661420000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[kgLCusto]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622047240000000000
          Width = 30.236220470000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."Produto' +
              '">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858267720000000000
          Width = 143.622047240000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFormulasIts."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598425200000000000
          Width = 18.897637800000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."Diluica' +
              'o">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496062990000000000
          Width = 18.897637800000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '##0; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."Graus"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393700790000000000
          Width = 26.456692910000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."TempoR"' +
              '>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850393700000000000
          Width = 26.456692910000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."TempoP"' +
              '>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307076850000000000
          Width = 45.354330710000000000
          Height = 15.118110236220470000
          DataSet = frxDsFormulasIts
          DataSetName = 'frxDsFormulasIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulasIts."pH_All"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661422200000000000
          Width = 45.354330710000000000
          Height = 15.118110236220470000
          DataField = 'Be_All'
          DataSet = frxDsFormulasIts
          DataSetName = 'frxDsFormulasIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulasIts."Be_All"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 514.015750469999900000
          Width = 102.047244090000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFormulasIts."Obs"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 616.062994570000000000
          Width = 49.133858270000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[Inicio]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 665.196852830000000000
          Width = 49.133858270000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[Final]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708661420000000000
          Width = 52.913385830000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,##0.000; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."Porcent"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480314960000000000
          Width = 15.118110240000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulasIts."Boca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 385.512060000000000000
        Width = 737.008350000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858267720000000000
          Width = 143.622047244094500000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Nome do produto ou texto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598425200000000000
          Width = 18.897637800000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '1:x')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496062990000000000
          Width = 18.897637800000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #186'C')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393700790000000000
          Width = 26.456692910000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Roda')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850393700000000000
          Width = 26.456692910000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'ra')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307076850000000000
          Width = 45.354330710000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'pH')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661422200000000000
          Width = 45.354330710000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'B'#233)
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 514.015750469999900000
          Width = 102.047244090000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 616.062994570000000000
          Width = 49.133858270000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Ini_txt]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 665.196852830000000000
          Width = 49.133858270000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Fin_txt]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622047244094000000
          Width = 30.236220472440900000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd.')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708661417322800000
          Width = 52.913385826771700000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Uso %')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Width = 90.708661420000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Uso kg/L]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 714.330711100000000000
          Width = 22.677165350000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480314960000000000
          Width = 15.118110240000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'B')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.708700470000000000
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 9.118119999999999000
          Top = 2.866109999999999000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 600.677180000000000000
          Top = 30.866110000000000000
          Width = 48.220470000000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Ful'#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 649.118120000000000000
          Top = 2.866109999999999000
          Width = 88.000000000000000000
          Height = 66.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -64
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FU]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object meOS: TfrxMemoView
          AllowVectorExport = True
          Left = 33.716450000000000000
          Top = 44.440940000000000000
          Width = 144.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[OS]')
          ParentFont = False
          WordWrap = False
        end
        object meSP: TfrxMemoView
          AllowVectorExport = True
          Left = 33.716450000000000000
          Top = 68.440940000000000000
          Width = 144.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_PESAGEM]')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 614.692950000000000000
          Top = 70.866110000000000000
          Width = 122.425170000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Top = 15.118100470000000000
          Width = 72.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_kg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535550230000000000
          Top = 30.236210710000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_m]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 15.118100470000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custokg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 30.236210710000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custom2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Width = 72.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_total]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[CustoTotal]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 257.007991180000000000
          Width = 15.118110240000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'BRL')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 15.118100470000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custokg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 30.236210710000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custom2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_CustoTotal]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275707710000000000
          Width = 15.118110240000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'USD [VAR_BRL_USD]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 15.118100470000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custokg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 30.236210710000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custom2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_CustoTotal]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543424250000000000
          Width = 15.118110240000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'EUR [VAR_BRL_EUR]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811140779999900000
          Width = 30.236220470000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data Moedas')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047361260000000000
          Width = 16.000000000000000000
          Height = 90.708671180000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[Data_Moedas]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535550230000000000
          Top = 45.354320940000010000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_ft]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 45.354320940000010000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custoft2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 45.354320940000010000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custoft2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 45.354320940000010000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custoft2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417552280000000000
          Top = 30.236239999999990000
          Width = 15.118110240000000000
          Height = 30.236191180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'WB')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535550230000000000
          Top = 60.472479999999990000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_m]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535550230000000000
          Top = 75.590590230000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_ft]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417552280000000000
          Top = 60.472509290000000000
          Width = 15.118110240000000000
          Height = 30.236191180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'SA')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 60.472479999999990000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custom2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 60.472479999999990000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custom2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 60.472479999999990000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custom2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 75.590590230000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custoft2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 75.590590230000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custoft2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 75.590590230000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custoft2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677153150000000000
        Top = 498.897960000000000000
        Width = 737.008350000000000000
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 673.149660000000000000
          Top = 2.015474650000000000
          Width = 61.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [PAGE#] de [TOTALPAGES]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Top = 0.000014650000000005
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pesador1:')
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 53.559060000000000000
          Top = 18.897652450000000000
          Width = 113.385826771653500000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 332.645640000000000000
          Top = 0.000014650000000005
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fuloneiro1:')
          ParentFont = False
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 388.645640000000000000
          Top = 18.897652450000000000
          Width = 113.385826770000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pesador2:')
          ParentFont = False
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 223.637910000000000000
          Top = 18.897637800000000000
          Width = 113.385826770000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 506.504020000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fuloneiror2:')
          ParentFont = False
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 558.724490000000000000
          Top = 18.897637800000000000
          Width = 113.385826770000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.881880000000000000
        Top = 132.283550000000000000
        Width = 737.008350000000000000
        object meLotes: TfrxMemoView
          AllowVectorExport = True
          Left = 572.598425200000000000
          Width = 164.409448820000000000
          Height = 90.881880000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Lotes]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 572.598425200000000000
          Height = 90.708720000000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 0.157700000000000000
          Top = 20.787401574803150000
          Width = 60.000000000000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita de ')
          ParentFont = False
          WordWrap = False
        end
        object meSetor: TfrxMemoView
          AllowVectorExport = True
          Left = 63.937230000000000000
          Top = 20.787401574803150000
          Width = 95.779530000000000000
          Height = 17.007874015748030000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMESETOR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 0.157700000000000000
          Top = 37.795285350000000000
          Width = 44.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object meCodReceita: TfrxMemoView
          AllowVectorExport = True
          Left = 44.157700000000000000
          Top = 37.795285350000000000
          Width = 521.574803150000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 20.787401574803150000
          Width = 72.000000000000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Espessura:')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Top = 54.803149606299210000
          Width = 64.251968500000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantidade:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811013860000000000
          Width = 64.251968500000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 54.803149606299210000
          Width = 72.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso m'#233'dio:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object meEspessura: TfrxMemoView
          AllowVectorExport = True
          Left = 423.716760000000000000
          Top = 20.787401574803150000
          Width = 142.000000000000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Espessura]')
          ParentFont = False
          WordWrap = False
        end
        object meQtde: TfrxMemoView
          AllowVectorExport = True
          Left = 64.661410000000000000
          Top = 54.803149606299210000
          Width = 102.047244090000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object mePeso: TfrxMemoView
          AllowVectorExport = True
          Left = 64.661410000000000000
          Top = 71.811013860000000000
          Width = 102.047244090000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object mePesoMedio: TfrxMemoView
          AllowVectorExport = True
          Left = 423.716760000000000000
          Top = 54.803149606299210000
          Width = 142.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[PesoMedio]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 159.716760000000000000
          Top = 20.787401574803150000
          Width = 68.000000000000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 227.716760000000000000
          Top = 20.787401574803150000
          Width = 116.000000000000000000
          Height = 17.007874015748030000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."Tecnico"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 71.811013860000000000
          Width = 72.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #193'rea m'#178':')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 423.716760000000000000
          Top = 71.811013860000000000
          Width = 142.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Area]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 270.724800000000000000
          Top = 71.811013860000000000
          Width = 71.811070000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_RETRABALHO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 168.677490000000000000
          Top = 54.803149606299210000
          Width = 174.236240000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_EMITGRU]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 168.677490000000000000
          Top = 71.811013860000000000
          Width = 98.456710000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_TIT_RETRAB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Top = 3.834570000000013000
          Width = 567.779530000000000000
          Height = 17.007874015748030000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.984251970000000000
        Top = 283.464750000000000000
        Width = 737.008350000000000000
        Condition = 'frxDsVMIOri."ID_TTW"'
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354360000000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 56.692913390000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / Hora')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 45.354360000000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Width = 30.236220470000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'GGX')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Width = 151.181102360000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome, espessura e cor da mat'#233'ria prima')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 34.015748030000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 56.692913390000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Width = 45.354330710000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 83.149562360000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie e Ficha')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 41.574732360000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 147.401572360000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente de m'#227'o-de-obra')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 359.055350000000000000
        Width = 737.008350000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.984251970000000000
        Top = 321.260050000000000000
        Width = 737.008350000000000000
        DataSet = frxDsVMIOri
        DataSetName = 'frxDsVMIOri'
        RowCount = 0
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354360000000000000
          Height = 13.984251968503940000
          DataField = 'Controle'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVMIOri."Controle"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 56.692913390000000000
          Height = 13.984251968503940000
          DataField = 'DataHora'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVMIOri."DataHora"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 45.354360000000000000
          Height = 13.984251968503940000
          DataField = 'Pallet'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVMIOri."Pallet"]')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Width = 30.236220470000000000
          Height = 13.984251968503940000
          DataField = 'GraGruX'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVMIOri."GraGruX"]')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Width = 151.181102360000000000
          Height = 13.984251968503940000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVMIOri."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 34.015748030000000000
          Height = 13.984251970000000000
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsVMIOri."FatorImp">*<frxDsVMIOri."Pecas">]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 56.692913390000000000
          Height = 13.984251970000000000
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsVMIOri."FatorImp">*<frxDsVMIOri."PesoKg">]')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Width = 45.354330710000000000
          Height = 13.984251970000000000
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsVMIOri."FatorImp">*<frxDsVMIOri."AreaM2">]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 83.149562360000000000
          Height = 13.984251970000000000
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVMIOri."NO_SerieFch"] [frxDsVMIOri."Ficha"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 41.574732360000000000
          Height = 13.984251968503940000
          DataField = 'Marca'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVMIOri."Marca"]')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 147.401572360000000000
          Height = 13.984251968503940000
          DataField = 'NO_ClientMO'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVMIOri."NO_ClientMO"]')
          ParentFont = False
        end
      end
    end
  end
  object frxPesagem2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38077.792695474500000000
    ReportOptions.Name = 'Receita Skintan'
    ReportOptions.LastChange = 39541.568236400500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MeLinhasOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with MeLinhas, Engine do'
      '  begin'
      '  if <VAR_TXT> = '#39#39' then'
      '    MeLinhas.Visible := False'
      '  else'
      '    MeLinhas.Visible := True;'
      '  end'
      'end;'
      ''
      'procedure RodapeDeGrupo2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with RodapeDeGrupo2, Engine do'
      '  begin'
      '    RodapeDeGrupo2.Visible := MeLinhas.Visible;'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 356
    Top = 308
    Datasets = <
      item
        DataSet = frxDsBMZ
        DataSetName = 'frxDsBMZ'
      end
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
      end
      item
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MeLinhas: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.000000000000000000
        Top = 275.905690000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'MeLinhasOnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsBMZ
        DataSetName = 'frxDsBMZ'
        RowCount = 0
        object MeAddPQ: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Width = 720.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          HideZeros = True
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 28.897650000000000000
          Width = 88.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLFormIts."PESO_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 676.897650000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLFormIts."PERC_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 120.897650000000000000
          Width = 188.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsLFormIts."Descricao"]')
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 312.897650000000000000
          Width = 360.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsLFormIts."TXT_DILUICAO"] [frxDsLFormIts."Observacos"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Cabecalho: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 124.000000000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Top = 31.543290000000000000
          Width = 420.000000000000000000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMESETOR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Top = 61.543290000000000000
          Width = 60.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 84.897650000000000000
          Top = 61.543290000000000000
          Width = 192.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMECI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Top = 81.543290000000000000
          Width = 60.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 84.897650000000000000
          Top = 81.543290000000000000
          Width = 192.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 584.897650000000000000
          Top = 61.543290000000000000
          Width = 156.000000000000000000
          Height = 36.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '                       ')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 276.897650000000000000
          Top = 61.543290000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'T'#233'cnico:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 340.897650000000000000
          Top = 61.543290000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."Tecnico"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 276.897650000000000000
          Top = 81.543290000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Espessura:')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 340.897650000000000000
          Top = 81.543290000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Espessura]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 460.897650000000000000
          Top = 61.543290000000000000
          Width = 52.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 512.897650000000000000
          Top = 61.543290000000000000
          Width = 72.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 460.897650000000000000
          Top = 81.543290000000000000
          Width = 52.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantia:')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 512.897650000000000000
          Top = 81.543290000000000000
          Width = 72.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
          WordWrap = False
        end
        object MeObjetivo: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Top = 101.543290000000000000
          Width = 720.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            'Receita: [frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 23.897650000000000000
          Top = 61.543290000000000000
          Width = 721.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Top = 61.543290000000000000
          Height = 60.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 2.000000000000000000
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 744.897650000000000000
          Top = 61.543290000000000000
          Height = 60.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 2.000000000000000000
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 508.897650000000000000
          Top = 1.543289999999999000
          Width = 224.000000000000000000
          Height = 56.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -64
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FU]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 588.897650000000000000
          Top = 61.543290000000000000
          Width = 60.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Relativo a:')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Top = 1.543289999999999000
          Width = 420.000000000000000000
          Height = 24.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PESAGEM]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 58.236240000000000000
        Top = 423.307360000000000000
        Width = 755.906000000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 462.881880000000000000
          Top = 2.393320000000017000
          Width = 282.015770000000000000
          Height = 20.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[TIME]-[DATE] - P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 20.897650000000000000
          Top = 0.613789999999994500
          Width = 441.322820000000000000
          Height = 24.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'ATEN'#199#195'O: IMPRESS'#195'O EXCLUSIVA PARA PESAGEM')
          ParentFont = False
        end
      end
      object CabGrupo1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 23.000000000000000000
        Top = 204.094620000000000000
        Width = 755.906000000000000000
        Condition = 'frxDsLFormIts."Etapa"'
        Stretched = True
        object MeEtapa: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Width = 720.000000000000000000
          Height = 23.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          HideZeros = True
          Memo.UTF8W = (
            'ETAPA [frxDsLFormIts."Etapa"]: [frxDsLFormIts."NomeEtapa"]')
          ParentFont = False
          VAlign = vaBottom
        end
      end
      object RodapDeGrupo1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.000000000000000000
        Top = 343.937230000000000000
        Visible = False
        Width = 755.906000000000000000
        Stretched = True
      end
      object CabecalhoDeGrupo1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 4.000000000000000000
        Top = 249.448980000000000000
        Visible = False
        Width = 755.906000000000000000
        Condition = 'frxDsLFormIts."SubEtapa"'
      end
      object RodapeDeGrupo2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 4.000000000000000000
        Top = 317.480520000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'RodapeDeGrupo2OnBeforePrint'
        object MeRodar: TfrxMemoView
          AllowVectorExport = True
          Left = 24.897650000000000000
          Width = 720.000000000000000000
          Height = 4.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HideZeros = True
          ParentFont = False
          VAlign = vaBottom
        end
      end
    end
  end
  object frxRec1_WE: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38017.353737407400000000
    ReportOptions.LastChange = 41853.389075254630000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxRec2GetValue
    Left = 84
    Top = 100
    Datasets = <
      item
        DataSet = frxDsBMZ
        DataSetName = 'frxDsBMZ'
      end
      item
        DataSet = frxDsFormulas
        DataSetName = 'frxDsFormulas'
      end
      item
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
      end
      item
        DataSet = frxDsFulonadas
        DataSetName = 'frxDsFulonadas'
      end
      item
        DataSet = frxDsLFormIts
        DataSetName = 'frxDsLFormIts'
      end
      item
        DataSet = frxDsLotes
        DataSetName = 'frxDsLotes'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPreUso
        DataSetName = 'frxDsPreUso'
      end
      item
        DataSet = frxDsSumFul
        DataSetName = 'frxDsSumFul'
      end
      item
        DataSet = frxDsVMIOri
        DataSetName = 'frxDsVMIOri'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      object Band1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637795275590000
        Top = 427.086890000000000000
        Width = 737.008350000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsFormulasIts
        DataSetName = 'frxDsFormulasIts'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 714.330711100000000000
          Width = 22.677165350000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."Seq"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 75.590551181102360000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[kgLCusto]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503927240000000000
          Width = 37.795275590000000000
          Height = 18.897637795275590000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."Produto' +
              '">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299207720000000000
          Width = 204.094527240000000000
          Height = 18.897637795275590000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFormulasIts."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393725200000000000
          Width = 18.897637800000000000
          Height = 18.897637795275590000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."Diluica' +
              'o">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291362990000000000
          Width = 18.897637800000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '##0; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."Graus"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189000790000000000
          Width = 26.456692910000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."TempoR"' +
              '>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645693700000000000
          Width = 26.456692910000000000
          Height = 18.897637795275590000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0;-#,###,##0; '#39',<frxDsFormulasIts."TempoP"' +
              '>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102376850000000000
          Width = 45.354330710000000000
          Height = 18.897637795275590000
          DataSet = frxDsFormulasIts
          DataSetName = 'frxDsFormulasIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFormulasIts."pH_All"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 506.456690470000000000
          Width = 109.606304090000000000
          Height = 18.897637795275590000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFormulasIts."Obs"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 616.062994570000000000
          Width = 49.133858270000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[Inicio]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 665.196852830000000000
          Width = 49.133858270000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[Final]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590541420000000000
          Width = 52.913385830000000000
          Height = 18.897637795275590000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,##0.000; ; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFormulasIts."Porcent"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object Band3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007874015748030000
        Top = 385.512060000000000000
        Width = 737.008350000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299207720000000000
          Width = 204.094527240000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Nome do produto ou texto')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393725200000000000
          Width = 18.897637800000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '1:x')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291362990000000000
          Width = 18.897637800000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #186'C')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189000790000000000
          Width = 26.456692910000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Roda')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645693700000000000
          Width = 26.456692910000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'ra')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102376850000000000
          Width = 45.354330710000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'pH')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 506.456690470000000000
          Width = 109.606304090000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 616.062994570000000000
          Width = 49.133858270000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Ini_txt]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 665.196852830000000000
          Width = 49.133858270000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Fin_txt]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503927240000000000
          Width = 37.795275590000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590541420000000000
          Width = 52.913385830000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Uso %')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Width = 75.590551181102360000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Uso kg/L]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 714.330711100000000000
          Width = 22.677165350000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.708700470000000000
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 9.118119999999999000
          Top = 2.866109999999999000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 600.677180000000000000
          Top = 30.866110000000000000
          Width = 48.220470000000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Ful'#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 649.118120000000000000
          Top = 2.866109999999999000
          Width = 88.000000000000000000
          Height = 66.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -64
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FU]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object meOS: TfrxMemoView
          AllowVectorExport = True
          Left = 33.716450000000000000
          Top = 44.440940000000000000
          Width = 144.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[OS]')
          ParentFont = False
          WordWrap = False
        end
        object meSP: TfrxMemoView
          AllowVectorExport = True
          Left = 33.716450000000000000
          Top = 68.440940000000000000
          Width = 144.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_PESAGEM]')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 614.692950000000000000
          Top = 70.866110000000000000
          Width = 122.425170000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Top = 15.118100470000000000
          Width = 72.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_kg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535550230000000000
          Top = 30.236210710000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_m]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 15.118100470000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custokg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 30.236210710000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custom2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Width = 72.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_total]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[CustoTotal]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 257.007991180000000000
          Width = 15.118110240000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'BRL')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 15.118100470000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custokg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 30.236210710000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custom2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_CustoTotal]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275707710000000000
          Width = 15.118110240000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'USD [VAR_BRL_USD]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 15.118100470000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custokg]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 30.236210710000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custom2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_CustoTotal]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543424250000000000
          Width = 15.118110240000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'EUR [VAR_BRL_EUR]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811140779999900000
          Width = 30.236220470000000000
          Height = 90.708671180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data Moedas')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047361260000000000
          Width = 16.000000000000000000
          Height = 90.708671180000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[Data_Moedas]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535550230000000000
          Top = 45.354320940000010000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_ft]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 45.354320940000010000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custoft2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 45.354320940000010000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custoft2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 45.354320940000010000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custoft2_WB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417552280000000000
          Top = 30.236239999999990000
          Width = 15.118110240000000000
          Height = 30.236191180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'WB')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535550230000000000
          Top = 60.472479999999990000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_m]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535550230000000000
          Top = 75.590590230000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Custo_ft]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417552280000000000
          Top = 60.472509290000000000
          Width = 15.118110240000000000
          Height = 30.236191180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'SA')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 60.472479999999990000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custom2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 60.472479999999990000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custom2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 60.472479999999990000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custom2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126101410000000000
          Top = 75.590590230000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Custoft2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393817950000000000
          Top = 75.590590230000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[U_Custoft2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661534480000000000
          Top = 75.590590230000000000
          Width = 80.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[E_Custoft2_SA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677153150000000000
        Top = 506.457020000000000000
        Width = 737.008350000000000000
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 673.149660000000000000
          Top = 2.015474649999988000
          Width = 61.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [PAGE#] de [TOTALPAGES]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Top = 0.000014650000025540
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pesador1:')
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 53.559060000000000000
          Top = 18.897652449999950000
          Width = 113.385826771653500000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 332.645640000000000000
          Top = 0.000014650000025540
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fuloneiro1:')
          ParentFont = False
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 388.645640000000000000
          Top = 18.897652449999950000
          Width = 113.385826770000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pesador2:')
          ParentFont = False
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 223.637910000000000000
          Top = 18.897637800000040000
          Width = 113.385826770000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 506.504020000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fuloneiror2:')
          ParentFont = False
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 558.724490000000000000
          Top = 18.897637800000040000
          Width = 113.385826770000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.881880000000000000
        Top = 132.283550000000000000
        Width = 737.008350000000000000
        object meLotes: TfrxMemoView
          AllowVectorExport = True
          Left = 572.598425200000000000
          Width = 164.409448820000000000
          Height = 90.881880000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[Lotes]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 572.598425200000000000
          Height = 90.708720000000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 0.157700000000000000
          Top = 20.787401574803140000
          Width = 60.000000000000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita de ')
          ParentFont = False
          WordWrap = False
        end
        object meSetor: TfrxMemoView
          AllowVectorExport = True
          Left = 63.937230000000000000
          Top = 20.787401574803140000
          Width = 95.779530000000000000
          Height = 17.007874015748030000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NOMESETOR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 0.157700000000000000
          Top = 37.795285350000000000
          Width = 44.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object meCodReceita: TfrxMemoView
          AllowVectorExport = True
          Left = 44.157700000000000000
          Top = 37.795285350000000000
          Width = 521.574803150000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."NumCODIF"] - [frxDsFormulas."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 20.787401574803140000
          Width = 72.000000000000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Espessura:')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Top = 54.803149606299200000
          Width = 64.251968500000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantidade:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811013860000000000
          Width = 64.251968500000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 54.803149606299200000
          Width = 72.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Peso m'#233'dio:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object meEspessura: TfrxMemoView
          AllowVectorExport = True
          Left = 423.716760000000000000
          Top = 20.787401574803140000
          Width = 142.000000000000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Espessura]')
          ParentFont = False
          WordWrap = False
        end
        object meQtde: TfrxMemoView
          AllowVectorExport = True
          Left = 64.661410000000000000
          Top = 54.803149606299200000
          Width = 102.047244090000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Qtde]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object mePeso: TfrxMemoView
          AllowVectorExport = True
          Left = 64.661410000000000000
          Top = 71.811013860000000000
          Width = 102.047244090000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Peso]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object mePesoMedio: TfrxMemoView
          AllowVectorExport = True
          Left = 423.716760000000000000
          Top = 54.803149606299200000
          Width = 142.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[PesoMedio]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 159.716760000000000000
          Top = 20.787401574803140000
          Width = 68.000000000000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 227.716760000000000000
          Top = 20.787401574803140000
          Width = 116.000000000000000000
          Height = 17.007874015748030000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFormulas."Tecnico"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 71.811013860000000000
          Width = 72.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #193'rea m'#178':')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 423.716760000000000000
          Top = 71.811013860000000000
          Width = 142.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[Area]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 270.724800000000000000
          Top = 71.811013860000000000
          Width = 71.811070000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_RETRABALHO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 168.677490000000000000
          Top = 54.803149606299200000
          Width = 174.236240000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_EMITGRU]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 168.677490000000000000
          Top = 71.811013860000000000
          Width = 98.456710000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_TIT_RETRAB]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Top = 3.834570000000013000
          Width = 567.779530000000000000
          Height = 17.007874015748030000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.984251970000000000
        Top = 283.464750000000000000
        Width = 737.008350000000000000
        Condition = 'frxDsVMIOri."ID_TTW"'
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354360000000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 56.692913390000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / Hora')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 45.354360000000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Width = 30.236220470000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'GGX')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Width = 151.181102360000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome, espessura e cor da mat'#233'ria prima')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 34.015748030000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 56.692913390000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Width = 45.354330710000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 83.149562360000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie e Ficha')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 41.574732360000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 147.401572360000000000
          Height = 13.984251968503940000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente de m'#227'o-de-obra')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 359.055350000000000000
        Width = 737.008350000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.984251970000000000
        Top = 321.260050000000000000
        Width = 737.008350000000000000
        DataSet = frxDsVMIOri
        DataSetName = 'frxDsVMIOri'
        RowCount = 0
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354360000000000000
          Height = 13.984251968503940000
          DataField = 'Controle'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVMIOri."Controle"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 56.692913390000000000
          Height = 13.984251968503940000
          DataField = 'DataHora'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVMIOri."DataHora"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 45.354360000000000000
          Height = 13.984251968503940000
          DataField = 'Pallet'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVMIOri."Pallet"]')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Width = 30.236220470000000000
          Height = 13.984251968503940000
          DataField = 'GraGruX'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVMIOri."GraGruX"]')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Width = 151.181102360000000000
          Height = 13.984251968503940000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVMIOri."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 34.015748030000000000
          Height = 13.984251970000000000
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsVMIOri."FatorImp">*<frxDsVMIOri."Pecas">]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 56.692913390000000000
          Height = 13.984251970000000000
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsVMIOri."FatorImp">*<frxDsVMIOri."PesoKg">]')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Width = 45.354330710000000000
          Height = 13.984251970000000000
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsVMIOri."FatorImp">*<frxDsVMIOri."AreaM2">]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 83.149562360000000000
          Height = 13.984251970000000000
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVMIOri."NO_SerieFch"] [frxDsVMIOri."Ficha"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 41.574732360000000000
          Height = 13.984251968503940000
          DataField = 'Marca'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVMIOri."Marca"]')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 147.401572360000000000
          Height = 13.984251968503940000
          DataField = 'NO_ClientMO'
          DataSet = frxDsVMIOri
          DataSetName = 'frxDsVMIOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVMIOri."NO_ClientMO"]')
          ParentFont = False
        end
      end
    end
  end
end
