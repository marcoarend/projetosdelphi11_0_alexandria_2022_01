unit PQEnvia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, UnInternalConsts, Buttons, Db, (*DBTables,*)
  mySQLDbTables, frxClass,
  dmkGeral, dmkImage, UnDmkEnums;

type
  TFmPQEnvia = class(TForm)
    OpenDialog1: TOpenDialog;
    Query1: TmySQLQuery;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EdSMTP: TEdit;
    EdConta: TEdit;
    EdEMail: TEdit;
    EdDono: TEdit;
    EdAssunto: TEdit;
    MePara: TMemo;
    MeCega: TMemo;
    MeCC: TMemo;
    MeCorpo: TMemo;
    Memo5: TMemo;
    LBAnexos: TListBox;
    CheckBox1: TCheckBox;
    RadioGroup1: TRadioGroup;
    EdDialUp: TEdit;
    BtLimpa: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtEMail: TBitBtn;
    BitBtn1: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtOutlookClick(Sender: TObject);
    procedure LBAnexosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure NMSMTP1AttachmentNotFound(Filename: String);
    procedure NMSMTP1AuthenticationFailed(var Handled: Boolean);
    procedure NMSMTP1Connect(Sender: TObject);
    procedure NMSMTP1SendStart(Sender: TObject);
    procedure NMSMTP1EncodeStart(Filename: String);
    procedure NMSMTP1EncodeEnd(Filename: String);
    procedure NMSMTP1Failure(Sender: TObject);
    procedure NMSMTP1Success(Sender: TObject);
    procedure NMSMTP1HeaderIncomplete(var handled: Boolean;
      hiType: Integer);
    procedure NMSMTP1RecipientNotFound(Recipient: String);
    procedure FormActivate(Sender: TObject);
    procedure BtEMailClick(Sender: TObject);
    procedure BtLimpaClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmPQEnvia: TFmPQEnvia;

implementation

uses UnMyObjects, Principal, Module, PQPed;

{$R *.DFM}

procedure TFmPQEnvia.BtOutlookClick(Sender: TObject);
{
var
  i: Integer;
}
begin
{
  for i := 0 to LBAnexos.Items.Count - 1 do
    if Trim(LBAnexos.Items[i]) <> '' then
      _L_M_D_Mail.Attachment.Add(LBAnexos.Items[i]);
  for i := 0 to MePara.Lines.Count - 1 do
    _L_M_D_Mail.ToRecipient.Add(MePara.Lines[0]);
  for i := 0 to MeCC.Lines.Count - 1 do
    if Trim(MeCC.Lines[i]) <> '' then
      _L_M_D_Mail.CcRecipient.Add(MeCC.Lines[i]);
  _L_M_D_Mail.Subject := EdAssunto.Text;
  for i := 0 to MeCorpo.Lines.Count - 1 do
    _L_M_D_Mail.MessageBody.Add(MeCorpo.Lines[i]);
  //
  _L_M_D_Mail.SendMail;
}
end;

procedure TFmPQEnvia.LBAnexosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_INSERT then
    if OpenDialog1.Execute then
      LBAnexos.Items.Add(OpenDialog1.FileName);
  if Key = VK_DELETE then
    LBAnexos.Items.Delete(LBAnexos.ItemIndex);
end;

procedure TFmPQEnvia.NMSMTP1AttachmentNotFound(Filename: String);
begin
  Memo5.Lines.Add('O arquivo anexado: '+FileName+' n�o existe');
end;

procedure TFmPQEnvia.NMSMTP1AuthenticationFailed(var Handled: Boolean);
//var
  //S: String;
begin
  { Parei Aqui fazer com indy
  S := NMSMTP1.UserID;
  if InputQuery('Falha de autentica��o', 'Nome de conta inv�lido. Novo nome de conta: ', S) then
  begin
    NMSMTP1.UserID := S;
    Handled := TRUE;
  end;
  }
end;

procedure TFmPQEnvia.NMSMTP1Connect(Sender: TObject);
begin
  { Parei Aqui fazer com indy
  Memo5.Lines.Add('Conectado');
  }
end;

procedure TFmPQEnvia.NMSMTP1SendStart(Sender: TObject);
begin
  Memo5.Lines.Add('Enviando Mensagem');
end;

procedure TFmPQEnvia.NMSMTP1EncodeStart(Filename: String);
begin
  Memo5.Lines.Add('Codificando '+FileName);
end;

procedure TFmPQEnvia.NMSMTP1EncodeEnd(Filename: String);
begin
  Memo5.Lines.Add(FileName+' codificado');
end;

procedure TFmPQEnvia.NMSMTP1Failure(Sender: TObject);
begin
  Memo5.Lines.Add('A entrega da mensagem falhou');
end;

procedure TFmPQEnvia.NMSMTP1Success(Sender: TObject);
begin
  Memo5.Lines.Add('Mensagem enviada com sucesso');
end;

procedure TFmPQEnvia.NMSMTP1HeaderIncomplete(var handled: Boolean;
  hiType: Integer);
//var
  //S: String;
begin
  { Parei Aqui fazer com indy
  case hiType of
    hiFromAddress:
      if InputQuery('O e-mail de envio est� vazio', 'Informe o endere�o: ', S) then
      begin
        NMSMTP1.PostMessage.FromAddress := S;
        Handled := TRUE;
      end;

    hiToAddress:
      if InputQuery('Destinat�rio vazio', 'Informe o endere�o: ', S) then
      begin
        NMSMTP1.PostMessage.ToAddress.Text := S;
        Handled := TRUE;
      end;

  end;
  }
end;

procedure TFmPQEnvia.NMSMTP1RecipientNotFound(Recipient: String);
begin
  Memo5.Lines.Add('Destinat�rio '+Recipient+' inv�lido.');
end;

procedure TFmPQEnvia.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  MeCorpo.SetFocus;
end;

procedure TFmPQEnvia.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmPQEnvia.BtEMailClick(Sender: TObject);
begin
  { Parei Aqui fazer com indy
  if NMSMTP1.Connected then NMSMTP1.Disconnect;
  try
    NMSMTP1.Host := EdSMTP.Text;
    NMSMTP1.UserID := EdConta.Text;
    NMSMTP1.Connect;
  except
    //winexec(PChar('rundll32.exe rnaui.dll, RnaDial '+EdDialUp.Text), SW_SHOWNORMAL);
    NMSMTP1.Host := EdSMTP.Text;
    NMSMTP1.UserID := EdConta.Text;
    NMSMTP1.Connect;
    if not NMSMTP1.Connected then
    begin
      ShowMessage('N�o foi poss�vel encontrar o provedor. Verifique a conex�o.');
      Exit;
    end;
  end;
  NMSMTP1.ClearParams := CheckBox1.Checked;

  NMSMTP1.SubType := mtPlain;
  case RadioGroup1.ItemIndex of
    0: NMSMTP1.EncodeType := uuMime;
    1: NMSMTP1.EncodeType := uuCode;
  end;
  NMSMTP1.PostMessage.FromAddress := EdEMail.Text;
  NMSMTP1.PostMessage.FromName := EdDono.Text;
  NMSMTP1.PostMessage.ToAddress.Text := MePara.Text;
  NMSMTP1.PostMessage.ToCarbonCopy.Text := MeCC.Text;
  NMSMTP1.PostMessage.ToBlindCarbonCopy.Text := MeCega.Text;
  NMSMTP1.PostMessage.Body.Text := MeCorpo.Text;

  NMSMTP1.PostMessage.Attachments.Text := LBAnexos.Items.Text;
  NMSMTP1.PostMessage.Subject := EdAssunto.Text;
  NMSMTP1.PostMessage.LocalProgram :=
    Application.Title+' '+FmPrincipal.StatusBar.Panels[3].Text;
  NMSMTP1.PostMessage.Date := DateToStr(Date);
  NMSMTP1.PostMessage.ReplyTo := EdEMail.Text;
  try
    NMSMTP1.SendMail;
    Dmod.QrUpdW.SQL.Clear;
    Dmod.QrUpdW.SQL.Add('UPDATE pqped SET DataEmail=:P0 WHERE Codigo=:P1');
    Dmod.QrUpdW.Params[0].AsDateTime := Now;
    Dmod.QrUpdW.Params[1].AsInteger := FmPQPed.QrPQPedCodigo.Value;
    Dmod.QrUpdW.ExecSQL;
    ShowMessage('E-mail enviado com sucesso!')
  except
    ShowMessage('Falha durante o envio do e-mail!');
  end;
}
end;

procedure TFmPQEnvia.BtLimpaClick(Sender: TObject);
begin
  { Parei Aqui fazer com indy
  NMSMTP1.ClearParameters;
  EdEmail.Clear;
  EdAssunto.Clear;
  EdDono.Clear;
  MePara.Clear;
  MeCC.Clear;
  MeCega.Clear;
  MeCorpo.Clear;
  Memo5.Clear;
  LBAnexos.Clear;
  }
end;

procedure TFmPQEnvia.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQEnvia.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

