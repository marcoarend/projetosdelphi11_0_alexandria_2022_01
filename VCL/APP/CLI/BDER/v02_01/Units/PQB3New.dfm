object FmPQB3New: TFmPQB3New
  Left = 569
  Top = 308
  Caption = 'QUI-BALAN-002 :: Novo Balan'#231'o de PQ'
  ClientHeight = 282
  ClientWidth = 406
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 406
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 358
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 310
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 252
        Height = 32
        Caption = 'Novo Balan'#231'o de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 252
        Height = 32
        Caption = 'Novo Balan'#231'o de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 252
        Height = 32
        Caption = 'Novo Balan'#231'o de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 165
    Width = 406
    Height = 53
    Align = alBottom
    Caption = ' Avisos: '
    Color = clBtnFace
    ParentColor = False
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 402
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 294
        Height = 16
        Caption = 'INFORME O M'#202'S ATUAL E N'#195'O O ANTERIOR!!!!!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 294
        Height = 16
        Caption = 'INFORME O M'#202'S ATUAL E N'#195'O O ANTERIOR!!!!!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 402
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 48
    Width = 406
    Height = 117
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Label3: TLabel
      Left = 240
      Top = 56
      Width = 91
      Height = 13
      Caption = 'Ano do movimento:'
    end
    object Label2: TLabel
      Left = 52
      Top = 56
      Width = 92
      Height = 13
      Caption = 'M'#234's do movimento:'
    end
    object Label8: TLabel
      Left = 24
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object CBAno: TComboBox
      Left = 239
      Top = 73
      Width = 95
      Height = 21
      Color = clWhite
      DropDownCount = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      Text = 'CBAno'
    end
    object CBMes: TComboBox
      Left = 53
      Top = 73
      Width = 182
      Height = 21
      Color = clWhite
      DropDownCount = 12
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Text = 'CBMes'
    end
    object EdEmpresa: TdmkEditCB
      Left = 24
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Empresa'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 81
      Top = 32
      Width = 304
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 3
      dmkEditCB = EdEmpresa
      QryCampo = 'Empresa'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object GBRodape: TGroupBox
    Left = 0
    Top = 218
    Width = 406
    Height = 64
    Align = alBottom
    TabOrder = 3
    object PnBotoes: TPanel
      Left = 2
      Top = 15
      Width = 402
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 258
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrLocPeriodo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Periodo '
      'FROM balancos'
      'WHERE Periodo=:P0')
    Left = 176
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPeriodoPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
  end
end
