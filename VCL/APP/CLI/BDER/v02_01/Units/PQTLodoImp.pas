unit PQTLodoImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, dmkDBLookupCbUM, dmkEditUM, dmkEditCbUM, UnAppPF,
  dmkEditDateTimePicker, frxClass, frxDBSet;

type
  TFmPQTLodoImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    QrVeiculos: TMySQLQuery;
    QrVeiculosCodigo: TIntegerField;
    QrVeiculosDescricao: TWideStringField;
    QrVeiculosMotorisPadr: TIntegerField;
    DsVeiculos: TDataSource;
    QrMotorista: TMySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    QrStqCenLoc: TMySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    QrUnidMed: TMySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    EdVeiculo: TdmkEditCB;
    Label1: TLabel;
    CBVeiculo: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdMotorista: TdmkEditCB;
    CBMotorista: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdLocal: TdmkEditCB;
    CBLocal: TdmkDBLookupComboBox;
    Label17: TLabel;
    EdUnidMed: TdmkEditCbUM;
    EdSigla: TdmkEditUM;
    CBUnidMed: TdmkDBLookupCbUM;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Label2: TLabel;
    Label5: TLabel;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    RGAgrupa: TRadioGroup;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    frxQUI_RECEI_032_1: TfrxReport;
    QrLodo: TMySQLQuery;
    QrLodoPlaca: TWideStringField;
    QrLodoNO_Unidade: TWideStringField;
    QrLodoNO_Motorista: TWideStringField;
    QrLodoNO_LOC_CEN: TWideStringField;
    QrLodoCodigo: TIntegerField;
    QrLodoControle: TIntegerField;
    QrLodoVeiculo: TIntegerField;
    QrLodoMotorista: TIntegerField;
    QrLodoLocal: TIntegerField;
    QrLodoQtde: TFloatField;
    QrLodoUnidade: TIntegerField;
    QrLodoAtivo: TSmallintField;
    QrLodoSigla: TWideStringField;
    frxDsLodo: TfrxDBDataset;
    QrLodoDataB: TDateField;
    QrLodoNO_Veiculo: TWideStringField;
    LaEmpresa: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrLodoDtaOrd: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frxQUI_RECEI_032_1GetValue(const VarName: string;
      var Value: Variant);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPQTLodo();
  public
    { Public declarations }
    FEmpresa: Integer;
  end;

  var
  FmPQTLodoImp: TFmPQTLodoImp;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UnEmpresas, UnDmkProcFunc;

{$R *.DFM}

procedure TFmPQTLodoImp.BtOKClick(Sender: TObject);
begin
  ReopenPQTLodo();
  MyObjects.frxDefineDataSets(frxQUI_RECEI_032_1, [
    DModG.frxDsDono,
    frxDsLodo
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_032_1, 'Transporte de Lodo');
end;

procedure TFmPQTLodoImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQTLodoImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQTLodoImp.FormCreate(Sender: TObject);
var
  Data: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  //
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  UnDmkDAC_PF.AbreQuery(QrVeiculos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenLoc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  //
  CBVeiculo.LocF7SQLText.Text := AppPF.SQLPesqVeiculo();
  //
  Data := Trunc(DModG.ObtemAgora());
  TPIni.Date := Data - 30;
  TPFIm.Date := Data;
end;

procedure TFmPQTLodoImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQTLodoImp.frxQUI_RECEI_032_1GetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_NO_EMPRESA') = 0 then
    Value := CBEmpresa.Text
  else
  if AnsiCompareText(VarName, 'VFR_AGR1NOME') = 0 then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := QrLodoDtaOrd.Value;
      1: Value := QrLodoNO_Veiculo.Value;
      2: Value := QrLodoNO_Motorista.Value;
      3: Value := QrLodoNO_LOC_CEN.Value;
      4: Value := QrLodoNO_Unidade.Value;
      else Value := '';
    end;
  end else if AnsiCompareText(VarName, 'VFR_AGR2NOME') = 0 then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := QrLodoDtaOrd.Value;
      1: Value := QrLodoNO_Veiculo.Value;
      2: Value := QrLodoNO_Motorista.Value;
      3: Value := QrLodoNO_LOC_CEN.Value;
      4: Value := QrLodoNO_Unidade.Value;
      else Value := '';
    end;
  end
  else if AnsiCompareText(VarName, 'VAR_DATA_HORA') = 0 then
    Value := Geral.Maiusculas(Geral.FDT(Now(), 8), False)
  else if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then
    Value := dmkPF.PeriodoImp(TPIni.Date, TPFim.Date, 0, 0, True, True,
    False, False, '', '')
  else if AnsiCompareText(VarName, 'VARF_VEICULO') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBVeiculo.Text, EdVeiculo.ValueVariant)
  else if AnsiCompareText(VarName, 'VARF_MOTORISTA') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBMotorista.Text, EdMotorista.ValueVariant)
  else if AnsiCompareText(VarName, 'VARF_LOC_CEN') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBLocal.Text, EdLocal.ValueVariant)
  else if AnsiCompareText(VarName, 'VARF_UNIDADE') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBUnidMed.Text, EdUnidMed.ValueVariant)


  else if AnsiCompareText(VarName, 'VFR_AGR')  = 0 then Value := RGAgrupa.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD1') = 0 then Value := RGOrdem1.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD2') = 0 then Value := RGOrdem2.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD3') = 0 then Value := RGOrdem3.ItemIndex

  else if AnsiCompareText(VarName, 'VFR_EMPRESA') = 0 then Value := CBEmpresa.Text
end;

procedure TFmPQTLodoImp.ReopenPQTLodo();
const
  ArrOrdBy: array[0..4] of String = ('pqt.DataB', 'pql.Veiculo',
    'pql.Motorista', 'pql.Local', 'pql.Unidade');
var
  //Filial, Empresa,
  Veiculo, Motorista, Local, UnidMed: Integer;
  //SQL_Empresa,
  OrderBy,
  SQL_Periodo, SQL_Veiculo, SQL_Motorista, SQL_Local, SQL_UnidMed: String;
begin
  (*Empresa := 0;
  Filial := EdEmpresa.ValueVariant;
  if Filial <> 0 then
    Empresa := DModG.QrEmpresasCodigo.MaxValue;*)
  Veiculo := EdVeiculo.ValueVariant;
  Motorista := EdMotorista.ValueVariant;
  Local := EdLocal.ValueVariant;
  UnidMed := EdUnidMed.ValueVariant;
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE pqt.DataB', TPIni.Date, TPFim.Date, True, True);
  (*if Empresa <> 0 then
    SQL_Empresa :=*)
  if Veiculo <> 0 then
    SQL_Veiculo := 'AND pql.Veiculo=' + Geral.FF0(Veiculo);
  if Motorista <> 0 then
    SQL_Motorista := 'AND pql.Motorista=' + Geral.FF0(Motorista);
  if Local <> 0 then
    SQL_Local := 'AND pql.Local=' + Geral.FF0(Local);
  if UnidMed <> 0 then
    SQL_UnidMed:= 'AND pql.Unidade=' + Geral.FF0(UnidMed);
  //
  OrderBy := 'ORDER BY '+
    ArrOrdBy[RGOrdem1.ItemIndex] + ', ' +
    ArrOrdBy[RGOrdem2.ItemIndex] + ', ' +
    ArrOrdBy[RGOrdem3.ItemIndex];
  //
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLodo, Dmod.MyDB, [
  //'SELECT DATE_FORMAT(pqt.DataB, "%Y-%m-%d") DtaOrd, ',
  'SELECT DATE_FORMAT(pqt.DataB, "%d/%m/%Y") DtaOrd, ',
  'pqt.DataB, vei.Placa, med.Nome NO_Unidade, med.Sigla, ',
  'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_Motorista, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ',
  'CONCAT(vei.placa, " - ",',
  'vma.Nome, " ", vmo.Nome) NO_Veiculo,',
  'pql.*  ',
  'FROM pqtlodo pql ',
  'LEFT JOIN pqt pqt ON pqt.Codigo=pql.Codigo',
  'LEFT JOIN veiculos vei ON vei.Codigo=pql.Veiculo ',
  'LEFT JOIN unidmed med ON med.Codigo=pql.Unidade ',
  'LEFT JOIN entidades mot ON mot.Codigo=pql.Motorista ',
  'LEFT JOIN stqcenloc scl ON scl.Controle=pql.Local ',
  'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
  'LEFT JOIN veicmarcas vma ON vma.Codigo=vei.Marca',
  'LEFT JOIN veicmodels vmo ON vmo.Codigo=vei.Modelo',
  SQL_Periodo,
  //SQL_Empresa,
  SQL_Veiculo,
  SQL_Motorista,
  SQL_Local,
  SQL_UnidMed,
  OrderBy,
  '']);
  //Geral.MB_Teste(QrLodo.SQL.Text);
end;

end.
