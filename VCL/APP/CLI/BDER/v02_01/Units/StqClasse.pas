unit StqClasse;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, ComCtrls, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, UnInternalConsts, dmkImage, UnDmkEnums,
  DmkDAC_PF;

type
  TFmStqClasse = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GradeC: TStringGrid;
    TabSheet2: TTabSheet;
    GradeA: TStringGrid;
    TabSheet3: TTabSheet;
    GradeX: TStringGrid;
    Panel3: TPanel;
    DsGraGru1: TDataSource;
    QrGraGru1: TmySQLQuery;
    QrGraGru1Codusu: TIntegerField;
    QrGraGru1Nivel3: TIntegerField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru1GraTamCad: TIntegerField;
    QrGraGru1NOMEGRATAMCAD: TWideStringField;
    QrGraGru1CODUSUGRATAMCAD: TIntegerField;
    QrGraGru1CST_A: TSmallintField;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1UnidMed: TIntegerField;
    QrGraGru1NCM: TWideStringField;
    QrGraGru1Peso: TFloatField;
    QrGraGru1SIGLAUNIDMED: TWideStringField;
    QrGraGru1CODUSUUNIDMED: TIntegerField;
    QrGraGru1NOMEUNIDMED: TWideStringField;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    Label1: TLabel;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    GradeQI: TStringGrid;
    GradeQA: TStringGrid;
    GradeQK: TStringGrid;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    Label2: TLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    EdStqCenCad: TdmkEditCB;
    Label3: TLabel;
    CBStqCenCad: TdmkDBLookupComboBox;
    QrGraGru1HowBxaEstq: TSmallintField;
    QrGraGru1GerBxaEstq: TSmallintField;
    RGGrandeza: TRadioGroup;
    RGFatorClas: TRadioGroup;
    StaticText1: TStaticText;
    QrGraGru1FatorClas: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure EdGraGru1Exit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeQIDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeQADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeQKDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeQIKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GradeQAKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GradeQKKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtOKClick(Sender: TObject);
    procedure RGGrandezaClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReconfiguraGradeQ();
  public
    { Public declarations }
    FOriCodi,
    //FOriCtrl, n�o pode � um para cada lancto
    FOriCnta,
    FOriPart,
    // GraGruX Fonte:
    FStqCenCad,
    FGraGruX,
    FDebCtrl: Integer;
    //
    FTPecas,
    FTPLE: Double;
  end;

  var
  FmStqClasse: TFmStqClasse;

implementation

uses ModProd, UnMyObjects, ModuleGeral, dmkGeral, Module, UMySQLModule,
UnGrade_Tabs;

{$R *.DFM}

procedure TFmStqClasse.BtOKClick(Sender: TObject);
var
  Col, Row, GraGruX: Integer;
  Tot, FatorClas: Double;
  Txt1, Txt2: String;
  QtdRed: Integer;
  //
  NeedQA, NeedQK: Boolean;
  QtdeQI, QtdeQA, QtdeQK, AreaM2, AreaP2, QtdeGE: Double;
  DataHora: String;
  Empresa, StqCenCad, DebCtrl, IDCtrl, OriCtrl: Integer;
begin
  if FTPecas = 0 then FTPecas := 1;
  if RGFatorClas.ItemIndex < 1 then
  begin
    Application.MessageBox('Defina o fator de divis�o!', 'Aviso', MB_OK+MB_ICONWARNING);
    RGFatorClas.SetFocus;
    Exit;
  end;
  if EdFilial.ValueVariant = 0 then
  begin
    Application.MessageBox('Defina a empresa!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdFilial.SetFocus;
    Exit;
  end;
  if EdStqCenCad.ValueVariant = 0 then
  begin
    Application.MessageBox('Defina o centro de estoque!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdStqCenCad.SetFocus;
    Exit;
  end;
  if EdGraGru1.ValueVariant = 0 then
  begin
    Application.MessageBox('Defina o produto!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdGraGru1.SetFocus;
    Exit;
  end;
  if MyObjects.FIC(QrGraGru1GerBxaEstq.Value < 1, nil,
    'Produto sem defini��o da "Unidade Controladora de Estoque" no seu cadastro!') then Exit;
  if MyObjects.FIC(QrGraGru1uNIDmED.Value = 0, nil,
    'Produto sem defini��o da "Unidade de Medida" no seu cadastro!') then Exit;
  FatorClas := RGFatorClas.ItemIndex;
  UMyMod.ObtemCodigoDeCodUsu(EdFilial, Empresa, '', 'Codigo', 'Filial');
  UMyMod.ObtemCodigoDeCodUsu(EdStqCenCad, StqCenCad, '');
  if DmProd.ExisteBalancoAberto_StqCen_Mul(
    QrGraGru1PrdGrupTip.Value, Empresa, StqCenCad) then
    Exit;
  //  Parei aqui!! verificar se a classifica��o foi matada pelo administrador
  //NeedQI  := Geral.IntInConjunto(1, QrGraGru1HowBxaEstq.Value);
  NeedQA  := Geral.IntInConjunto(2, QrGraGru1HowBxaEstq.Value);
  NeedQK  := Geral.IntInConjunto(4, QrGraGru1HowBxaEstq.Value);
  //
  //Nivel1  := QrGraGru1Nivel1.Value;
  DebCtrl := FDebCtrl;
  Screen.Cursor := crHourGlass;
  try
    Tot       := 0;
    //ErrPreco  := 0;
    //AvisoPrc  := 0;
    QtdRed    := 0;
    for Col := 1 to GradeQI.ColCount -1 do
    begin
      for Row := 1 to GradeQI.RowCount - 1 do
      begin
        QtdeQI := Geral.DMV(GradeQI.Cells[Col,Row]);
        QtdeQA := Geral.DMV(GradeQA.Cells[Col,Row]);
        QtdeQK := Geral.DMV(GradeQK.Cells[Col,Row]);
        //
        Tot := Tot + QtdeQI;
        if QtdeQI > 0 then
          QtdRed := QtdRed + 1;
        if (QtdeQI <> 0) or (QtdeQA <> 0 ) or (QtdeQK <> 0 ) then
        begin
          Txt1 := GradeQI.Cells[Col, 0];
          Txt2 := GradeQI.Cells[0, Row];
          if QtdeQI = 0 then
          begin
            Screen.Cursor := crDefault;
            Geral.MB_Aviso('O item "' + Txt1 + ' ' + Txt2 +
            '" deve ter informado sua quantidade!');
            Exit;
          end;
          if (QtdeQA = 0) and NeedQA then
          begin
            Screen.Cursor := crDefault;
            Geral.MB_Aviso('O item "' + Txt1 + ' ' + Txt2 +
            '" deve ter informado sua �rea!');
            Exit;
          end;
          if (QtdeQA > 0) and (RGGrandeza.ItemIndex = 0) then
          begin
            Screen.Cursor := crDefault;
            Geral.MB_Aviso('O item "' + Txt1 + ' ' + Txt2 +
            '" tem �rea informada mas a grandeza est� indefinida!');
            Exit;
          end;
          if (QtdeQK = 0) and NeedQK then
          begin
            Screen.Cursor := crDefault;
            Geral.MB_Aviso('O item "' + Txt1 + ' ' + Txt2 +
            '" deve ter informado seu Peso!');
            Exit;
          end;
        end;
        //
        {
        PrecoO := Geral.DMV(GradeL.Cells[Col,Row]);
        PrecoR := Geral.DMV(GradeF.Cells[Col,Row]);
        if (PrecoO <>  PrecoR) and (Qtd = 0) then
          AvisoPrc := AvisoPrc + 1;
        if (Qtd > 0) and (PrecoR = 0) then
          ErrPreco := ErrPreco + 1;
        }
      end;
    end;
    if Tot = 0 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(
        'N�o h� defini��o de quantidades!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    {
    end
    else if ErrPreco > 0 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(PChar('Existem ' + IntToStr(ErrPreco) +
      ' itens sem pre�o de faturamento definido!'), 'Aviso',
      MB_OK+MB_ICONWARNING);

      Exit;
    end
    else if AvisoPrc > 0 then
    begin
      if Application.MessageBox(PChar('Existem ' + IntToStr(AvisoPrc) +
      ' itens com pre�o de faturamento alterado, mas n�o h� defini��o de ' +
      'quantidades de itens!' + sLineBreak + 'Deseja continuar assim mesmo?'),
      'Pergunta', MB_YESNOCANCEL+MB_ICONWARNING) <> ID_YES then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
    }
    end;
    DataHora := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    PB1.Position := 0;
    PB1.Max := QtdRed;
    for Col := 1 to GradeQI.ColCount -1 do
    begin
      if GradeX.Cells[Col,0] <> '' then
      begin
        for Row := 1 to GradeQI.RowCount - 1 do
        begin
          if Geral.IMV(GradeX.Cells[0,Row]) > 0 then
          begin
            QtdeQI := Geral.DMV(GradeQI.Cells[Col,Row]);
            QtdeQA := Geral.DMV(GradeQA.Cells[Col,Row]);
            QtdeQK := Geral.DMV(GradeQK.Cells[Col,Row]);
            //
            if (QtdeQI <> 0) or (QtdeQA <> 0 ) or (QtdeQK <> 0 ) then
            begin
              GraGruX := Geral.IMV(GradeC.Cells[Col, Row]);
              if GraGruX > 0 then
              begin
{
                Casas  := Dmod.QrControleCasasProd.Value;
                PrecoO := Geral.DMV(GradeL.Cells[Col, Row]);
                PrecoR := Geral.DMV(GradeF.Cells[Col, Row]);
                QuantP := Geral.DMV(GradeQ.Cells[Col, Row]);
                ValCal := PrecoR;// * QuantP;
                DescoP := Geral.DMV(GradeD.Cells[Col, Row]);
                DescoI := dmkPF.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
                PrecoF := PrecoR - DescoI;
                DescoV := DescoI * QuantP;
                ValBru := PrecoR * QuantP;
                ValLiq := ValBru - DescoV;
                //
                Controle := UMyMod.BuscaEmLivreY_Def('pedivdaits', 'Controle',
                  ImgTipo.SQLType, 0);
                if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pedivdaits', False,[
                'Codigo', 'GraGruX', 'PrecoO',
                'PrecoR', 'QuantP', 'ValBru',
                'DescoP', 'DescoV', 'ValLiq',
                'PrecoF'  ], [
                'Controle'], [
                Codigo, GraGruX, PrecoO,
                PrecoR, QuantP, ValBru,
                DescoP, DescoV, ValLiq,
                PrecoF], [
                Controle], True) then
                DmPediVda.AtzSdosPedido(Codigo);

}
                case RGGrandeza.ItemIndex of
                  1:
                  begin
                    AreaM2 := QtdeQA;
                    //AreaP2 := M L A G e r a l .ConverteArea(QtdeQA, cgMtoFT);
                    AreaP2 := Geral.ConverteArea(QtdeQA, ctM2toP2, cfQuarto);
                  end;
                  2:
                  begin
                    //AreaM2 := M L A G e r a l.ConverteArea(QtdeQA, cgFTtoM);
                    AreaM2 := Geral.ConverteArea(QtdeQA, ctP2toM2, cfCento);
                    AreaP2 := QtdeQA;
                  end;
                  else begin
                    AreaM2 := 0;
                    AreaP2 := 0;
                  end;
                end;
                case QrGraGru1GerBxaEstq.Value of
                  1: QtdeGE := QtdeQI;
                  2: QtdeGE := AreaM2; // Controlar sempre por M2 o estoque da �rea?
                  3: QtdeGE := QtdeQK;
                  else QtdeGE := 0;
                end;
                //
                IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
                OriCtrl := IDCtrl;
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
                  'DataHora', 'Tipo', 'OriCodi',
                  'OriCtrl', 'OriCnta', 'OriPart',
                  'Empresa', 'StqCenCad', 'GraGruX',
                  'Qtde', 'Pecas', 'Peso', 'AreaM2', 'AreaP2',
                  'FatorClas', 'DebCtrl'
                ], ['IDCtrl'], [
                  DataHora, VAR_FATID_0101, FOriCodi,
                  OriCtrl, FOriCnta, FOriPart,
                  Empresa, StqCenCad, GraGruX,
                  //Qtde, Pecas,  Peso,   Area
                  QtdeGE, QtdeQI, QtdeQK, AreaM2, AreaP2,
                  FatorClas, DebCtrl
                ], [IDCtrl], False) then
                begin
                    IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
                    FatorClas := 1 / FatorClas; // Inverter!!
                    StqCenCad := FStqCenCad;
                    GraGruX   := FGraGruX;
                    QtdeQI    := -QtdeQI * FatorClas;
                    QtdeGE    := QtdeQI;
                    QtdeQK    := QtdeQI / FTPecas * FTPLE; // Parei Aqui!!!
                    AreaM2    := 0; // Parei Aqui!!!
                    AreaP2    := 0; // Parei Aqui!!!
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
                    'DataHora', 'Tipo', 'OriCodi',
                    'OriCtrl', 'OriCnta', 'OriPart',
                    'Empresa', 'StqCenCad', 'GraGruX',
                    'Qtde', 'Pecas', 'Peso', 'AreaM2', 'AreaP2',
                    'FatorClas'
                  ], ['IDCtrl'], [
                    DataHora, VAR_FATID_0104, FOriCodi,
                    OriCtrl, FOriCnta, FOriPart,
                    Empresa, StqCenCad, GraGruX,
                    //Qtde, Pecas,  Peso,   Area
                    QtdeGE, QtdeQI, QtdeQK, AreaM2, AreaP2,
                    FatorClas
                  ], [IDCtrl], False) then ;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
    Dmod.AtualizaMPIn(FOriCodi);
    {FmPediVda.ReopenPediVdaGru(Nivel1);
    EdGraGru1.ValueVariant := 0;
    EdGraGru1.SetFocus;
    }
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmStqClasse.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqClasse.EdGraGru1Change(Sender: TObject);
begin
  RGFatorClas.ItemIndex := QrGraGru1FatorClas.Value;
  if EdGraGru1.Focused = False then
    ReconfiguraGradeQ();
end;

procedure TFmStqClasse.EdGraGru1Exit(Sender: TObject);
begin
  ReconfiguraGradeQ();
end;

procedure TFmStqClasse.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if (EdFilial.ValueVariant <> 0) and (EdStqCenCad.ValueVariant <> 0) then
    EdGraGru1.SetFocus;
end;

procedure TFmStqClasse.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FOriCodi := 0;
  //FOriCtrl := 0;
  FOriCnta := 0;
  FOriPart := 0;
  PageControl1.ActivePageIndex := 0;
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQI.ColWidths[0] := 128;
  GradeQA.ColWidths[0] := 128;
  GradeQK.ColWidths[0] := 128;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  if QrStqCenCad.RecordCount = 1 then
  begin
    EdStqCenCad.ValueVariant := QrStqCenCadCodUsu.Value;
    CBStqCenCad.KeyValue     := QrStqCenCadCodUsu.Value;
  end;
  //
  if DModG.QrFiliLog.State = dsInactive then
    Geral.MB_Aviso('N�o h� empresa logada!')
  else
  if DModG.QrFiliLog.RecordCount = 1 then
  begin
    EdFilial.ValueVariant := DModG.QrFiliLogFilial.Value;
    CBFilial.KeyValue     := DModG.QrFiliLogFilial.Value;
  end;
end;

procedure TFmStqClasse.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqClasse.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmStqClasse.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmStqClasse.GradeQADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQA, GradeA, nil, ACol, ARow, Rect, State,
  '#,###,##0.00;-#,###,##0.00; ', 0, 0, False);
end;

procedure TFmStqClasse.GradeQAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInputEx(GradeQA, GradeC, Key, Shift, BtOK);
end;

procedure TFmStqClasse.GradeQIDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQI, GradeA, nil, ACol, ARow, Rect, State,
  '#,###,##0;-#,###,##0; ', 0, 0, False);
end;

procedure TFmStqClasse.GradeQIKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInputEx(GradeQI, GradeC, Key, Shift, BtOK);
end;

procedure TFmStqClasse.GradeQKDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQK, GradeA, nil, ACol, ARow, Rect, State,
  '#,###,##0.000;-#,###,##0.000; ', 0, 0, False);
end;

procedure TFmStqClasse.GradeQKKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInputEx(GradeQK, GradeC, Key, Shift, BtOK);
end;

procedure TFmStqClasse.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmStqClasse.ReconfiguraGradeQ;
begin
{
  DmProd.ConfigGrades0(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
    GradeA, GradeX, GradeC, GradeQI, GradeQA, GradeQK);
}
  DmProd.ConfigGrades10(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
    GradeA, GradeX, GradeC, GradeQI, GradeQA, GradeQK);
end;

procedure TFmStqClasse.RGGrandezaClick(Sender: TObject);
begin
  TabSheet5.Caption := ' �rea (' + RGGrandeza.Items[RGGrandeza.ItemIndex] + ')';
end;

// Parei aqui !!! Colocar no form principal uma op��o de :
//  UPDATE mpin SET EmClassif
// para cancelat todad classifica��es
end.
