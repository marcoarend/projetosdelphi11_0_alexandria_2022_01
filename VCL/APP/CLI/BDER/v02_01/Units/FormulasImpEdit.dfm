object FmFormulasImpEdit: TFmFormulasImpEdit
  Left = 339
  Top = 183
  Caption = 'QUI-RECEI-006 :: Edi'#231#227'o de Pre'#231'os de Insumos'
  ClientHeight = 444
  ClientWidth = 680
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 680
    Height = 288
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitTop = 40
    ExplicitHeight = 356
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 678
      Height = 286
      Align = alClient
      DataSource = DsPQ
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Caption = 'Pre'#231'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoComparativo'
          Title.Caption = 'Pre'#231'o Comparat.'
          Width = 97
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 680
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 632
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 584
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 362
        Height = 32
        Caption = 'Edi'#231#227'o de Pre'#231'os de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 362
        Height = 32
        Caption = 'Edi'#231#227'o de Pre'#231'os de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 362
        Height = 32
        Caption = 'Edi'#231#227'o de Pre'#231'os de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 336
    Width = 680
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 288
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 676
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 380
    Width = 680
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 332
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 676
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 532
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtCancela: TBitBtn
          Tag = 13
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sair'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCancelaClick
        end
      end
    end
  end
  object TbPQ: TmySQLTable
    Database = Dmod.MyDB
    Filter = 'Codigo in ('#39'1'#39','#39'2'#39')'
    Filtered = True
    TableName = 'pq'
    Left = 128
    Top = 104
    object TbPQCodigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TbPQNome: TWideStringField
      FieldName = 'Nome'
      ReadOnly = True
      Size = 50
    end
    object TbPQPreco: TFloatField
      FieldName = 'Preco'
      ReadOnly = True
      DisplayFormat = '#,###,##0.0000'
    end
    object TbPQPrecoComparativo: TFloatField
      FieldName = 'PrecoComparativo'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object DsPQ: TDataSource
    DataSet = TbPQ
    Left = 156
    Top = 104
  end
end
