unit EmitGru;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkCheckBox, Variants, frxClass, frxDBSet,
  dmkDBGridZTO, AppListas, Vcl.ComCtrls, UnProjGroup_Consts, UnGrl_Consts,
  CreateBlueDerm, UnAppEnums;

type
  TGrupoRendiCusto =
          (grcIndef=0, grcSemGrupo=1, grcCor=2, grcRebaixe=3, grcCorERebaixe=4);
  TFmEmitGru = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrEmitGru: TmySQLQuery;
    DsEmitGru: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    QrEmitGruLk: TIntegerField;
    QrEmitGruDataCad: TDateField;
    QrEmitGruDataAlt: TDateField;
    QrEmitGruUserCad: TIntegerField;
    QrEmitGruUserAlt: TIntegerField;
    QrEmitGruAlterWeb: TSmallintField;
    QrEmitGruAtivo: TSmallintField;
    QrCabs: TmySQLQuery;
    QrCabsCodigo: TIntegerField;
    QrCabsData: TDateTimeField;
    QrCabsTipoTXT: TWideStringField;
    QrCabsTipo: TFloatField;
    QrCliOrig: TmySQLQuery;
    QrCliOrigCliOrig: TIntegerField;
    DsCliOrig: TDataSource;
    DsCabs: TDataSource;
    frxQUI_RECEI_014_00_A: TfrxReport;
    QrEmit0: TmySQLQuery;
    QrEmit0Codigo: TIntegerField;
    QrEmit0DataEmis: TDateTimeField;
    QrEmit0Status: TSmallintField;
    QrEmit0Numero: TIntegerField;
    QrEmit0NOMECI: TWideStringField;
    QrEmit0NOMESETOR: TWideStringField;
    QrEmit0Tecnico: TWideStringField;
    QrEmit0NOME: TWideStringField;
    QrEmit0ClienteI: TIntegerField;
    QrEmit0Tipificacao: TSmallintField;
    QrEmit0TempoR: TIntegerField;
    QrEmit0TempoP: TIntegerField;
    QrEmit0Setor: TSmallintField;
    QrEmit0Tipific: TSmallintField;
    QrEmit0Espessura: TWideStringField;
    QrEmit0DefPeca: TWideStringField;
    QrEmit0Peso: TFloatField;
    QrEmit0Custo: TFloatField;
    QrEmit0Qtde: TFloatField;
    QrEmit0AreaM2: TFloatField;
    QrEmit0Fulao: TWideStringField;
    QrEmit0Obs: TWideStringField;
    QrEmit0SetrEmi: TSmallintField;
    QrEmit0SourcMP: TSmallintField;
    QrEmit0Cod_Espess: TIntegerField;
    QrEmit0CodDefPeca: TIntegerField;
    QrEmit0CustoTo: TFloatField;
    QrEmit0CustoKg: TFloatField;
    QrEmit0CustoM2: TFloatField;
    QrEmit0CusUSM2: TFloatField;
    QrEmit0Lk: TIntegerField;
    QrEmit0DataCad: TDateField;
    QrEmit0DataAlt: TDateField;
    QrEmit0UserCad: TIntegerField;
    QrEmit0UserAlt: TIntegerField;
    QrEmit0AlterWeb: TSmallintField;
    QrEmit0Ativo: TSmallintField;
    QrEmit0EmitGru: TIntegerField;
    QrEmit0Retrabalho: TSmallintField;
    QrEmit0SemiCodPeca: TIntegerField;
    QrEmit0SemiTxtPeca: TWideStringField;
    QrEmit0SemiPeso: TFloatField;
    QrEmit0SemiQtde: TFloatField;
    QrEmit0SemiAreaM2: TFloatField;
    QrEmit0SemiRendim: TFloatField;
    frxDsEmit0: TfrxDBDataset;
    QrEmit0AREAP2: TFloatField;
    QrEmit0SEMIAREAP2: TFloatField;
    QrCliOrigNO_ENT: TWideStringField;
    QrEmit1: TmySQLQuery;
    frxDsEmit1: TfrxDBDataset;
    QrEmit1Codigo: TIntegerField;
    QrEmit1DataEmis: TDateTimeField;
    QrEmit1Status: TSmallintField;
    QrEmit1Numero: TIntegerField;
    QrEmit1NOMECI: TWideStringField;
    QrEmit1NOMESETOR: TWideStringField;
    QrEmit1Tecnico: TWideStringField;
    QrEmit1NOME: TWideStringField;
    QrEmit1ClienteI: TIntegerField;
    QrEmit1Tipificacao: TSmallintField;
    QrEmit1TempoR: TIntegerField;
    QrEmit1TempoP: TIntegerField;
    QrEmit1Setor: TSmallintField;
    QrEmit1Tipific: TSmallintField;
    QrEmit1Espessura: TWideStringField;
    QrEmit1DefPeca: TWideStringField;
    QrEmit1Peso: TFloatField;
    QrEmit1Custo: TFloatField;
    QrEmit1Qtde: TFloatField;
    QrEmit1AreaM2: TFloatField;
    QrEmit1Fulao: TWideStringField;
    QrEmit1Obs: TWideStringField;
    QrEmit1SetrEmi: TSmallintField;
    QrEmit1SourcMP: TSmallintField;
    QrEmit1Cod_Espess: TIntegerField;
    QrEmit1CodDefPeca: TIntegerField;
    QrEmit1CustoTo: TFloatField;
    QrEmit1CustoKg: TFloatField;
    QrEmit1CustoM2: TFloatField;
    QrEmit1CusUSM2: TFloatField;
    QrEmit1Lk: TIntegerField;
    QrEmit1DataCad: TDateField;
    QrEmit1DataAlt: TDateField;
    QrEmit1UserCad: TIntegerField;
    QrEmit1UserAlt: TIntegerField;
    QrEmit1AlterWeb: TSmallintField;
    QrEmit1Ativo: TSmallintField;
    QrEmit1EmitGru: TIntegerField;
    QrEmit1Retrabalho: TSmallintField;
    QrEmit1SemiCodPeca: TIntegerField;
    QrEmit1SemiTxtPeca: TWideStringField;
    QrEmit1SemiPeso: TFloatField;
    QrEmit1SemiQtde: TFloatField;
    QrEmit1SemiAreaM2: TFloatField;
    QrEmit1SemiRendim: TFloatField;
    QrEmit1AREAP2: TFloatField;
    QrEmit1SEMIAREAP2: TFloatField;
    QrPQO: TmySQLQuery;
    QrPQOCodigo: TIntegerField;
    QrPQODataB: TDateField;
    QrPQONome: TWideStringField;
    QrPQOPeso: TFloatField;
    QrPQOValor: TFloatField;
    frxDsPQO: TfrxDBDataset;
    QrRendim: TmySQLQuery;
    QrRendimAREAP2: TFloatField;
    QrRendimSEMIAREAP2: TFloatField;
    QrRendimAreaM2: TFloatField;
    QrRendimSemiAreaM2: TFloatField;
    frxDsRendim: TfrxDBDataset;
    frxDsEmitGru: TfrxDBDataset;
    QrEmitGruEncerrado: TDateTimeField;
    QrEmitGruUSD_BRL: TFloatField;
    QrEmitGruAreaManM2: TFloatField;
    QrEmitGruAreaManP2: TFloatField;
    QrEmitGruAreaIniM2: TFloatField;
    QrEmitGruAreaIniP2: TFloatField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    QrEmitGruENCERRADO_TXT: TWideStringField;
    N1: TMenuItem;
    Encerra1: TMenuItem;
    QrCustoPQ: TmySQLQuery;
    QrCustoPQValor: TFloatField;
    frxDsCustoPQ: TfrxDBDataset;
    QrInsumos: TmySQLQuery;
    QrInsumosInsumo: TIntegerField;
    QrInsumosNome: TWideStringField;
    QrInsumosPeso: TFloatField;
    QrInsumosValor: TFloatField;
    QrInsumosCustoUni: TFloatField;
    frxDsInsumos: TfrxDBDataset;
    PMImprime: TPopupMenu;
    Fechamento1: TMenuItem;
    Insumosusados1: TMenuItem;
    frxQUI_RECEI_014_00_B: TfrxReport;
    InsumosporNFs1: TMenuItem;
    QrInsumo2: TMySQLQuery;
    _frxQUI_RECEI_014_00_C_: TfrxReport;
    frxDsInsumo2: TfrxDBDataset;
    QrInsumo2NF: TIntegerField;
    QrInsumo2NF_RP: TIntegerField;
    QrInsumo2NF_CC: TIntegerField;
    QrInsumo2IQ: TIntegerField;
    QrInsumo2DataE: TDateField;
    QrInsumo2Peso: TFloatField;
    QrInsumo2Valor: TFloatField;
    QrInsumo2CustoBXA: TFloatField;
    QrInsumo2CustoINN: TFloatField;
    QrInsumo2Insumo: TIntegerField;
    QrInsumo2Nome: TWideStringField;
    QrVSPWECab: TmySQLQuery;
    DsVSPWECab: TDataSource;
    QrVSPWECabNO_MovimID: TWideStringField;
    QrVSPWECabNO_PRD_TAM_COR: TWideStringField;
    QrVSPWECabNO_VSCOPCab: TWideStringField;
    QrVSPWECabPecasINI: TFloatField;
    QrVSPWECabAreaINIM2: TFloatField;
    QrVSPWECabAreaINIP2: TFloatField;
    QrVSPWECabPesoKgINI: TFloatField;
    QrVSPWECabNO_Operacoes: TWideStringField;
    QrVSPWECabCodigo: TIntegerField;
    QrVSPWECabMovimCod: TIntegerField;
    QrVSPWECabCacCod: TIntegerField;
    QrVSPWECabGraGruX: TIntegerField;
    QrVSPWECabNome: TWideStringField;
    QrVSPWECabEmpresa: TIntegerField;
    QrVSPWECabDtHrAberto: TDateTimeField;
    QrVSPWECabDtHrLibOpe: TDateTimeField;
    QrVSPWECabDtHrCfgOpe: TDateTimeField;
    QrVSPWECabDtHrFimOpe: TDateTimeField;
    QrVSPWECabPesoKgMan: TFloatField;
    QrVSPWECabPecasMan: TFloatField;
    QrVSPWECabAreaManM2: TFloatField;
    QrVSPWECabAreaManP2: TFloatField;
    QrVSPWECabValorTMan: TFloatField;
    QrVSPWECabPesoKgSrc: TFloatField;
    QrVSPWECabPecasSrc: TFloatField;
    QrVSPWECabAreaSrcM2: TFloatField;
    QrVSPWECabAreaSrcP2: TFloatField;
    QrVSPWECabValorTSrc: TFloatField;
    QrVSPWECabPesoKgDst: TFloatField;
    QrVSPWECabPecasDst: TFloatField;
    QrVSPWECabAreaDstM2: TFloatField;
    QrVSPWECabAreaDstP2: TFloatField;
    QrVSPWECabValorTDst: TFloatField;
    QrVSPWECabPesoKgBxa: TFloatField;
    QrVSPWECabPecasBxa: TFloatField;
    QrVSPWECabAreaBxaM2: TFloatField;
    QrVSPWECabAreaBxaP2: TFloatField;
    QrVSPWECabValorTBxa: TFloatField;
    QrVSPWECabPesoKgSdo: TFloatField;
    QrVSPWECabPecasSdo: TFloatField;
    QrVSPWECabAreaSdoM2: TFloatField;
    QrVSPWECabAreaSdoP2: TFloatField;
    QrVSPWECabValorTSdo: TFloatField;
    QrVSPWECabTipoArea: TSmallintField;
    QrVSPWECabCustoManMOKg: TFloatField;
    QrVSPWECabCustoManMOTot: TFloatField;
    QrVSPWECabValorManMP: TFloatField;
    QrVSPWECabValorManT: TFloatField;
    QrVSPWECabLk: TIntegerField;
    QrVSPWECabDataCad: TDateField;
    QrVSPWECabDataAlt: TDateField;
    QrVSPWECabUserCad: TIntegerField;
    QrVSPWECabUserAlt: TIntegerField;
    QrVSPWECabAlterWeb: TSmallintField;
    QrVSPWECabAtivo: TSmallintField;
    QrVSPWECabCliente: TIntegerField;
    QrVSPWECabNFeRem: TIntegerField;
    QrVSPWECabLPFMO: TWideStringField;
    QrVSPWECabTemIMEIMrt: TSmallintField;
    QrVSPWECabKgCouPQ: TFloatField;
    QrVSPWECabGGXDst: TIntegerField;
    QrVSPWECabNFeStatus: TIntegerField;
    QrVSPWECabGGXSrc: TIntegerField;
    QrVSPWECabSerieRem: TIntegerField;
    QrVSPWECabOperacoes: TIntegerField;
    QrVSPWECabVSCOPCab: TIntegerField;
    QrVSPWECabEmitGru: TIntegerField;
    QrVSPWECabNO_EMPRESA: TWideStringField;
    QrCabsVSMovCod: TIntegerField;
    BtPesagem: TBitBtn;
    PMPesagem: TPopupMenu;
    InformeIMEC1: TMenuItem;
    QrCabsAreaM2: TFloatField;
    QrCabsPecas: TFloatField;
    RemovedoIMEC1: TMenuItem;
    frxQUI_RECEI_014_00_D: TfrxReport;
    QrVSPWEOriPallet: TmySQLQuery;
    QrVSPWEOriPalletPallet: TLargeintField;
    QrVSPWEOriPalletGraGruX: TLargeintField;
    QrVSPWEOriPalletPecas: TFloatField;
    QrVSPWEOriPalletAreaM2: TFloatField;
    QrVSPWEOriPalletAreaP2: TFloatField;
    QrVSPWEOriPalletPesoKg: TFloatField;
    QrVSPWEOriPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEOriPalletNO_Pallet: TWideStringField;
    QrVSPWEOriPalletSerieFch: TLargeintField;
    QrVSPWEOriPalletFicha: TLargeintField;
    QrVSPWEOriPalletNO_TTW: TWideStringField;
    QrVSPWEOriPalletID_TTW: TLargeintField;
    QrVSPWEOriPalletSeries_E_Fichas: TWideStringField;
    QrVSPWEOriPalletTerceiro: TLargeintField;
    QrVSPWEOriPalletMarca: TWideStringField;
    QrVSPWEOriPalletNO_FORNECE: TWideStringField;
    QrVSPWEOriPalletNO_SerieFch: TWideStringField;
    QrVSPWEOriPalletValorT: TFloatField;
    QrVSPWEOriPalletSdoVrtPeca: TFloatField;
    QrVSPWEOriPalletSdoVrtPeso: TFloatField;
    QrVSPWEOriPalletSdoVrtArM2: TFloatField;
    QrVSPWEOriIMEI: TmySQLQuery;
    QrVSPWEOriIMEICodigo: TLargeintField;
    QrVSPWEOriIMEIControle: TLargeintField;
    QrVSPWEOriIMEIMovimCod: TLargeintField;
    QrVSPWEOriIMEIMovimNiv: TLargeintField;
    QrVSPWEOriIMEIMovimTwn: TLargeintField;
    QrVSPWEOriIMEIEmpresa: TLargeintField;
    QrVSPWEOriIMEITerceiro: TLargeintField;
    QrVSPWEOriIMEICliVenda: TLargeintField;
    QrVSPWEOriIMEIMovimID: TLargeintField;
    QrVSPWEOriIMEIDataHora: TDateTimeField;
    QrVSPWEOriIMEIPallet: TLargeintField;
    QrVSPWEOriIMEIGraGruX: TLargeintField;
    QrVSPWEOriIMEIPecas: TFloatField;
    QrVSPWEOriIMEIPesoKg: TFloatField;
    QrVSPWEOriIMEIAreaM2: TFloatField;
    QrVSPWEOriIMEIAreaP2: TFloatField;
    QrVSPWEOriIMEIValorT: TFloatField;
    QrVSPWEOriIMEISrcMovID: TLargeintField;
    QrVSPWEOriIMEISrcNivel1: TLargeintField;
    QrVSPWEOriIMEISrcNivel2: TLargeintField;
    QrVSPWEOriIMEISrcGGX: TLargeintField;
    QrVSPWEOriIMEISdoVrtPeca: TFloatField;
    QrVSPWEOriIMEISdoVrtPeso: TFloatField;
    QrVSPWEOriIMEISdoVrtArM2: TFloatField;
    QrVSPWEOriIMEIObserv: TWideStringField;
    QrVSPWEOriIMEISerieFch: TLargeintField;
    QrVSPWEOriIMEIFicha: TLargeintField;
    QrVSPWEOriIMEIMisturou: TLargeintField;
    QrVSPWEOriIMEIFornecMO: TLargeintField;
    QrVSPWEOriIMEICustoMOKg: TFloatField;
    QrVSPWEOriIMEICustoMOM2: TFloatField;
    QrVSPWEOriIMEICustoMOTot: TFloatField;
    QrVSPWEOriIMEIValorMP: TFloatField;
    QrVSPWEOriIMEIDstMovID: TLargeintField;
    QrVSPWEOriIMEIDstNivel1: TLargeintField;
    QrVSPWEOriIMEIDstNivel2: TLargeintField;
    QrVSPWEOriIMEIDstGGX: TLargeintField;
    QrVSPWEOriIMEIQtdGerPeca: TFloatField;
    QrVSPWEOriIMEIQtdGerPeso: TFloatField;
    QrVSPWEOriIMEIQtdGerArM2: TFloatField;
    QrVSPWEOriIMEIQtdGerArP2: TFloatField;
    QrVSPWEOriIMEIQtdAntPeca: TFloatField;
    QrVSPWEOriIMEIQtdAntPeso: TFloatField;
    QrVSPWEOriIMEIQtdAntArM2: TFloatField;
    QrVSPWEOriIMEIQtdAntArP2: TFloatField;
    QrVSPWEOriIMEINotaMPAG: TFloatField;
    QrVSPWEOriIMEINO_PALLET: TWideStringField;
    QrVSPWEOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVSPWEOriIMEINO_TTW: TWideStringField;
    QrVSPWEOriIMEIID_TTW: TLargeintField;
    QrVSPWEOriIMEINO_FORNECE: TWideStringField;
    QrVSPWEOriIMEINO_SerieFch: TWideStringField;
    QrVSPWEOriIMEIReqMovEstq: TLargeintField;
    DsVSPWEOriIMEI: TDataSource;
    DsVSPWEOriPallet: TDataSource;
    QrVSPWEDst: TmySQLQuery;
    QrVSPWEDstCodigo: TLargeintField;
    QrVSPWEDstControle: TLargeintField;
    QrVSPWEDstMovimCod: TLargeintField;
    QrVSPWEDstMovimNiv: TLargeintField;
    QrVSPWEDstMovimTwn: TLargeintField;
    QrVSPWEDstEmpresa: TLargeintField;
    QrVSPWEDstTerceiro: TLargeintField;
    QrVSPWEDstCliVenda: TLargeintField;
    QrVSPWEDstMovimID: TLargeintField;
    QrVSPWEDstDataHora: TDateTimeField;
    QrVSPWEDstPallet: TLargeintField;
    QrVSPWEDstGraGruX: TLargeintField;
    QrVSPWEDstPecas: TFloatField;
    QrVSPWEDstPesoKg: TFloatField;
    QrVSPWEDstAreaM2: TFloatField;
    QrVSPWEDstAreaP2: TFloatField;
    QrVSPWEDstValorT: TFloatField;
    QrVSPWEDstSrcMovID: TLargeintField;
    QrVSPWEDstSrcNivel1: TLargeintField;
    QrVSPWEDstSrcNivel2: TLargeintField;
    QrVSPWEDstSrcGGX: TLargeintField;
    QrVSPWEDstSdoVrtPeca: TFloatField;
    QrVSPWEDstSdoVrtPeso: TFloatField;
    QrVSPWEDstSdoVrtArM2: TFloatField;
    QrVSPWEDstObserv: TWideStringField;
    QrVSPWEDstSerieFch: TLargeintField;
    QrVSPWEDstFicha: TLargeintField;
    QrVSPWEDstMisturou: TLargeintField;
    QrVSPWEDstFornecMO: TLargeintField;
    QrVSPWEDstCustoMOKg: TFloatField;
    QrVSPWEDstCustoMOM2: TFloatField;
    QrVSPWEDstCustoMOTot: TFloatField;
    QrVSPWEDstValorMP: TFloatField;
    QrVSPWEDstDstMovID: TLargeintField;
    QrVSPWEDstDstNivel1: TLargeintField;
    QrVSPWEDstDstNivel2: TLargeintField;
    QrVSPWEDstDstGGX: TLargeintField;
    QrVSPWEDstQtdGerPeca: TFloatField;
    QrVSPWEDstQtdGerPeso: TFloatField;
    QrVSPWEDstQtdGerArM2: TFloatField;
    QrVSPWEDstQtdGerArP2: TFloatField;
    QrVSPWEDstQtdAntPeca: TFloatField;
    QrVSPWEDstQtdAntPeso: TFloatField;
    QrVSPWEDstQtdAntArM2: TFloatField;
    QrVSPWEDstQtdAntArP2: TFloatField;
    QrVSPWEDstNotaMPAG: TFloatField;
    QrVSPWEDstNO_PALLET: TWideStringField;
    QrVSPWEDstNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEDstNO_TTW: TWideStringField;
    QrVSPWEDstID_TTW: TLargeintField;
    QrVSPWEDstNO_FORNECE: TWideStringField;
    QrVSPWEDstNO_SerieFch: TWideStringField;
    QrVSPWEDstReqMovEstq: TLargeintField;
    QrVSPWEDstPedItsFin: TLargeintField;
    QrVSPWEDstMarca: TWideStringField;
    QrVSPWEDstStqCenLoc: TLargeintField;
    DsVSPWEDst: TDataSource;
    QrVSPWEBxa: TmySQLQuery;
    QrVSPWEBxaCodigo: TLargeintField;
    QrVSPWEBxaControle: TLargeintField;
    QrVSPWEBxaMovimCod: TLargeintField;
    QrVSPWEBxaMovimNiv: TLargeintField;
    QrVSPWEBxaMovimTwn: TLargeintField;
    QrVSPWEBxaEmpresa: TLargeintField;
    QrVSPWEBxaTerceiro: TLargeintField;
    QrVSPWEBxaCliVenda: TLargeintField;
    QrVSPWEBxaMovimID: TLargeintField;
    QrVSPWEBxaDataHora: TDateTimeField;
    QrVSPWEBxaPallet: TLargeintField;
    QrVSPWEBxaGraGruX: TLargeintField;
    QrVSPWEBxaPecas: TFloatField;
    QrVSPWEBxaPesoKg: TFloatField;
    QrVSPWEBxaAreaM2: TFloatField;
    QrVSPWEBxaAreaP2: TFloatField;
    QrVSPWEBxaValorT: TFloatField;
    QrVSPWEBxaSrcMovID: TLargeintField;
    QrVSPWEBxaSrcNivel1: TLargeintField;
    QrVSPWEBxaSrcNivel2: TLargeintField;
    QrVSPWEBxaSrcGGX: TLargeintField;
    QrVSPWEBxaSdoVrtPeca: TFloatField;
    QrVSPWEBxaSdoVrtPeso: TFloatField;
    QrVSPWEBxaSdoVrtArM2: TFloatField;
    QrVSPWEBxaObserv: TWideStringField;
    QrVSPWEBxaSerieFch: TLargeintField;
    QrVSPWEBxaFicha: TLargeintField;
    QrVSPWEBxaMisturou: TLargeintField;
    QrVSPWEBxaFornecMO: TLargeintField;
    QrVSPWEBxaCustoMOKg: TFloatField;
    QrVSPWEBxaCustoMOM2: TFloatField;
    QrVSPWEBxaCustoMOTot: TFloatField;
    QrVSPWEBxaValorMP: TFloatField;
    QrVSPWEBxaDstMovID: TLargeintField;
    QrVSPWEBxaDstNivel1: TLargeintField;
    QrVSPWEBxaDstNivel2: TLargeintField;
    QrVSPWEBxaDstGGX: TLargeintField;
    QrVSPWEBxaQtdGerPeca: TFloatField;
    QrVSPWEBxaQtdGerPeso: TFloatField;
    QrVSPWEBxaQtdGerArM2: TFloatField;
    QrVSPWEBxaQtdGerArP2: TFloatField;
    QrVSPWEBxaQtdAntPeca: TFloatField;
    QrVSPWEBxaQtdAntPeso: TFloatField;
    QrVSPWEBxaQtdAntArM2: TFloatField;
    QrVSPWEBxaQtdAntArP2: TFloatField;
    QrVSPWEBxaNotaMPAG: TFloatField;
    QrVSPWEBxaNO_PALLET: TWideStringField;
    QrVSPWEBxaNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEBxaNO_TTW: TWideStringField;
    QrVSPWEBxaID_TTW: TLargeintField;
    QrVSPWEBxaNO_FORNECE: TWideStringField;
    QrVSPWEBxaNO_SerieFch: TWideStringField;
    QrVSPWEBxaReqMovEstq: TLargeintField;
    DsVSPWEBxa: TDataSource;
    QrVSPWEDesclDst: TmySQLQuery;
    QrVSPWEDesclDstCodigo: TLargeintField;
    QrVSPWEDesclDstControle: TLargeintField;
    QrVSPWEDesclDstMovimCod: TLargeintField;
    QrVSPWEDesclDstMovimNiv: TLargeintField;
    QrVSPWEDesclDstMovimTwn: TLargeintField;
    QrVSPWEDesclDstEmpresa: TLargeintField;
    QrVSPWEDesclDstTerceiro: TLargeintField;
    QrVSPWEDesclDstCliVenda: TLargeintField;
    QrVSPWEDesclDstMovimID: TLargeintField;
    QrVSPWEDesclDstDataHora: TDateTimeField;
    QrVSPWEDesclDstPallet: TLargeintField;
    QrVSPWEDesclDstGraGruX: TLargeintField;
    QrVSPWEDesclDstPecas: TFloatField;
    QrVSPWEDesclDstPesoKg: TFloatField;
    QrVSPWEDesclDstAreaM2: TFloatField;
    QrVSPWEDesclDstAreaP2: TFloatField;
    QrVSPWEDesclDstValorT: TFloatField;
    QrVSPWEDesclDstSrcMovID: TLargeintField;
    QrVSPWEDesclDstSrcNivel1: TLargeintField;
    QrVSPWEDesclDstSrcNivel2: TLargeintField;
    QrVSPWEDesclDstSrcGGX: TLargeintField;
    QrVSPWEDesclDstSdoVrtPeca: TFloatField;
    QrVSPWEDesclDstSdoVrtPeso: TFloatField;
    QrVSPWEDesclDstSdoVrtArM2: TFloatField;
    QrVSPWEDesclDstObserv: TWideStringField;
    QrVSPWEDesclDstSerieFch: TLargeintField;
    QrVSPWEDesclDstFicha: TLargeintField;
    QrVSPWEDesclDstMisturou: TLargeintField;
    QrVSPWEDesclDstFornecMO: TLargeintField;
    QrVSPWEDesclDstCustoMOKg: TFloatField;
    QrVSPWEDesclDstCustoMOM2: TFloatField;
    QrVSPWEDesclDstCustoMOTot: TFloatField;
    QrVSPWEDesclDstValorMP: TFloatField;
    QrVSPWEDesclDstDstMovID: TLargeintField;
    QrVSPWEDesclDstDstNivel1: TLargeintField;
    QrVSPWEDesclDstDstNivel2: TLargeintField;
    QrVSPWEDesclDstDstGGX: TLargeintField;
    QrVSPWEDesclDstQtdGerPeca: TFloatField;
    QrVSPWEDesclDstQtdGerPeso: TFloatField;
    QrVSPWEDesclDstQtdGerArM2: TFloatField;
    QrVSPWEDesclDstQtdGerArP2: TFloatField;
    QrVSPWEDesclDstQtdAntPeca: TFloatField;
    QrVSPWEDesclDstQtdAntPeso: TFloatField;
    QrVSPWEDesclDstQtdAntArM2: TFloatField;
    QrVSPWEDesclDstQtdAntArP2: TFloatField;
    QrVSPWEDesclDstNotaMPAG: TFloatField;
    QrVSPWEDesclDstNO_PALLET: TWideStringField;
    QrVSPWEDesclDstNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEDesclDstNO_TTW: TWideStringField;
    QrVSPWEDesclDstID_TTW: TLargeintField;
    QrVSPWEDesclDstNO_FORNECE: TWideStringField;
    QrVSPWEDesclDstNO_SerieFch: TWideStringField;
    QrVSPWEDesclDstReqMovEstq: TLargeintField;
    QrVSPWEDesclDstPedItsFin: TLargeintField;
    QrVSPWEDesclDstMarca: TWideStringField;
    QrVSPWEDesclDstStqCenLoc: TLargeintField;
    DsVSPWEDesclDst: TDataSource;
    QrVSPWEDesclBxa: TmySQLQuery;
    QrVSPWEDesclBxaCodigo: TLargeintField;
    QrVSPWEDesclBxaControle: TLargeintField;
    QrVSPWEDesclBxaMovimCod: TLargeintField;
    QrVSPWEDesclBxaMovimNiv: TLargeintField;
    QrVSPWEDesclBxaMovimTwn: TLargeintField;
    QrVSPWEDesclBxaEmpresa: TLargeintField;
    QrVSPWEDesclBxaTerceiro: TLargeintField;
    QrVSPWEDesclBxaCliVenda: TLargeintField;
    QrVSPWEDesclBxaMovimID: TLargeintField;
    QrVSPWEDesclBxaDataHora: TDateTimeField;
    QrVSPWEDesclBxaPallet: TLargeintField;
    QrVSPWEDesclBxaGraGruX: TLargeintField;
    QrVSPWEDesclBxaPecas: TFloatField;
    QrVSPWEDesclBxaPesoKg: TFloatField;
    QrVSPWEDesclBxaAreaM2: TFloatField;
    QrVSPWEDesclBxaAreaP2: TFloatField;
    QrVSPWEDesclBxaValorT: TFloatField;
    QrVSPWEDesclBxaSrcMovID: TLargeintField;
    QrVSPWEDesclBxaSrcNivel1: TLargeintField;
    QrVSPWEDesclBxaSrcNivel2: TLargeintField;
    QrVSPWEDesclBxaSrcGGX: TLargeintField;
    QrVSPWEDesclBxaSdoVrtPeca: TFloatField;
    QrVSPWEDesclBxaSdoVrtPeso: TFloatField;
    QrVSPWEDesclBxaSdoVrtArM2: TFloatField;
    QrVSPWEDesclBxaObserv: TWideStringField;
    QrVSPWEDesclBxaSerieFch: TLargeintField;
    QrVSPWEDesclBxaFicha: TLargeintField;
    QrVSPWEDesclBxaMisturou: TLargeintField;
    QrVSPWEDesclBxaFornecMO: TLargeintField;
    QrVSPWEDesclBxaCustoMOKg: TFloatField;
    QrVSPWEDesclBxaCustoMOM2: TFloatField;
    QrVSPWEDesclBxaCustoMOTot: TFloatField;
    QrVSPWEDesclBxaValorMP: TFloatField;
    QrVSPWEDesclBxaDstMovID: TLargeintField;
    QrVSPWEDesclBxaDstNivel1: TLargeintField;
    QrVSPWEDesclBxaDstNivel2: TLargeintField;
    QrVSPWEDesclBxaDstGGX: TLargeintField;
    QrVSPWEDesclBxaQtdGerPeca: TFloatField;
    QrVSPWEDesclBxaQtdGerPeso: TFloatField;
    QrVSPWEDesclBxaQtdGerArM2: TFloatField;
    QrVSPWEDesclBxaQtdGerArP2: TFloatField;
    QrVSPWEDesclBxaQtdAntPeca: TFloatField;
    QrVSPWEDesclBxaQtdAntPeso: TFloatField;
    QrVSPWEDesclBxaQtdAntArM2: TFloatField;
    QrVSPWEDesclBxaQtdAntArP2: TFloatField;
    QrVSPWEDesclBxaNotaMPAG: TFloatField;
    QrVSPWEDesclBxaNO_PALLET: TWideStringField;
    QrVSPWEDesclBxaNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEDesclBxaNO_TTW: TWideStringField;
    QrVSPWEDesclBxaID_TTW: TLargeintField;
    QrVSPWEDesclBxaNO_FORNECE: TWideStringField;
    QrVSPWEDesclBxaNO_SerieFch: TWideStringField;
    QrVSPWEDesclBxaReqMovEstq: TLargeintField;
    DsVSPWEDesclBxa: TDataSource;
    frxDsVSPWECab: TfrxDBDataset;
    CustoseRendimentoscomCouroB1: TMenuItem;
    frxDsVSPWEOriIMEI: TfrxDBDataset;
    frxDsVSPWEDst: TfrxDBDataset;
    QrVSPWESubPrd: TmySQLQuery;
    QrVSPWESubPrdCodigo: TLargeintField;
    QrVSPWESubPrdControle: TLargeintField;
    QrVSPWESubPrdMovimCod: TLargeintField;
    QrVSPWESubPrdMovimNiv: TLargeintField;
    QrVSPWESubPrdMovimTwn: TLargeintField;
    QrVSPWESubPrdEmpresa: TLargeintField;
    QrVSPWESubPrdTerceiro: TLargeintField;
    QrVSPWESubPrdCliVenda: TLargeintField;
    QrVSPWESubPrdMovimID: TLargeintField;
    QrVSPWESubPrdDataHora: TDateTimeField;
    QrVSPWESubPrdPallet: TLargeintField;
    QrVSPWESubPrdGraGruX: TLargeintField;
    QrVSPWESubPrdPecas: TFloatField;
    QrVSPWESubPrdPesoKg: TFloatField;
    QrVSPWESubPrdAreaM2: TFloatField;
    QrVSPWESubPrdAreaP2: TFloatField;
    QrVSPWESubPrdValorT: TFloatField;
    QrVSPWESubPrdSrcMovID: TLargeintField;
    QrVSPWESubPrdSrcNivel1: TLargeintField;
    QrVSPWESubPrdSrcNivel2: TLargeintField;
    QrVSPWESubPrdSrcGGX: TLargeintField;
    QrVSPWESubPrdSdoVrtPeca: TFloatField;
    QrVSPWESubPrdSdoVrtPeso: TFloatField;
    QrVSPWESubPrdSdoVrtArM2: TFloatField;
    QrVSPWESubPrdObserv: TWideStringField;
    QrVSPWESubPrdSerieFch: TLargeintField;
    QrVSPWESubPrdFicha: TLargeintField;
    QrVSPWESubPrdMisturou: TLargeintField;
    QrVSPWESubPrdFornecMO: TLargeintField;
    QrVSPWESubPrdCustoMOKg: TFloatField;
    QrVSPWESubPrdCustoMOM2: TFloatField;
    QrVSPWESubPrdCustoMOTot: TFloatField;
    QrVSPWESubPrdValorMP: TFloatField;
    QrVSPWESubPrdDstMovID: TLargeintField;
    QrVSPWESubPrdDstNivel1: TLargeintField;
    QrVSPWESubPrdDstNivel2: TLargeintField;
    QrVSPWESubPrdDstGGX: TLargeintField;
    QrVSPWESubPrdQtdGerPeca: TFloatField;
    QrVSPWESubPrdQtdGerPeso: TFloatField;
    QrVSPWESubPrdQtdGerArM2: TFloatField;
    QrVSPWESubPrdQtdGerArP2: TFloatField;
    QrVSPWESubPrdQtdAntPeca: TFloatField;
    QrVSPWESubPrdQtdAntPeso: TFloatField;
    QrVSPWESubPrdQtdAntArM2: TFloatField;
    QrVSPWESubPrdQtdAntArP2: TFloatField;
    QrVSPWESubPrdNotaMPAG: TFloatField;
    QrVSPWESubPrdNO_PALLET: TWideStringField;
    QrVSPWESubPrdNO_PRD_TAM_COR: TWideStringField;
    QrVSPWESubPrdNO_TTW: TWideStringField;
    QrVSPWESubPrdID_TTW: TLargeintField;
    QrVSPWESubPrdNO_FORNECE: TWideStringField;
    QrVSPWESubPrdNO_SerieFch: TWideStringField;
    QrVSPWESubPrdReqMovEstq: TLargeintField;
    QrVSPWESubPrdPedItsFin: TLargeintField;
    QrVSPWESubPrdMarca: TWideStringField;
    QrVSPWESubPrdStqCenLoc: TLargeintField;
    DsVSPWESubPrd: TDataSource;
    frxDsVSPWESubPrd: TfrxDBDataset;
    QrVSPWEOriIMEINFeNum: TLargeintField;
    CustoseRendimentoscomCouroA1: TMenuItem;
    QrOriVMI: TmySQLQuery;
    QrOriVMINFeNum: TIntegerField;
    QrOriVMINO_PRD_TAM_COR: TWideStringField;
    QrOriVMICodigo: TIntegerField;
    QrOriVMIControle: TIntegerField;
    QrOriVMIMovimCod: TIntegerField;
    QrOriVMIMovimNiv: TIntegerField;
    QrOriVMIMovimTwn: TIntegerField;
    QrOriVMIEmpresa: TIntegerField;
    QrOriVMITerceiro: TIntegerField;
    QrOriVMICliVenda: TIntegerField;
    QrOriVMIMovimID: TIntegerField;
    QrOriVMILnkNivXtr1: TIntegerField;
    QrOriVMILnkNivXtr2: TIntegerField;
    QrOriVMIDataHora: TDateTimeField;
    QrOriVMIPallet: TIntegerField;
    QrOriVMIGraGruX: TIntegerField;
    QrOriVMIPecas: TFloatField;
    QrOriVMIPesoKg: TFloatField;
    QrOriVMIAreaM2: TFloatField;
    QrOriVMIAreaP2: TFloatField;
    QrOriVMIValorT: TFloatField;
    QrOriVMISrcMovID: TIntegerField;
    QrOriVMISrcNivel1: TIntegerField;
    QrOriVMISrcNivel2: TIntegerField;
    QrOriVMISdoVrtPeca: TFloatField;
    QrOriVMISdoVrtArM2: TFloatField;
    QrOriVMIObserv: TWideStringField;
    QrOriVMIFicha: TIntegerField;
    QrOriVMIMisturou: TSmallintField;
    QrOriVMICustoMOKg: TFloatField;
    QrOriVMICustoMOTot: TFloatField;
    QrOriVMILk: TIntegerField;
    QrOriVMIDataCad: TDateField;
    QrOriVMIDataAlt: TDateField;
    QrOriVMIUserCad: TIntegerField;
    QrOriVMIUserAlt: TIntegerField;
    QrOriVMIAlterWeb: TSmallintField;
    QrOriVMIAtivo: TSmallintField;
    QrOriVMISrcGGX: TIntegerField;
    QrOriVMISdoVrtPeso: TFloatField;
    QrOriVMISerieFch: TIntegerField;
    QrOriVMIFornecMO: TIntegerField;
    QrOriVMIValorMP: TFloatField;
    QrOriVMIDstMovID: TIntegerField;
    QrOriVMIDstNivel1: TIntegerField;
    QrOriVMIDstNivel2: TIntegerField;
    QrOriVMIDstGGX: TIntegerField;
    QrOriVMIQtdGerPeca: TFloatField;
    QrOriVMIQtdGerPeso: TFloatField;
    QrOriVMIQtdGerArM2: TFloatField;
    QrOriVMIQtdGerArP2: TFloatField;
    QrOriVMIQtdAntPeca: TFloatField;
    QrOriVMIQtdAntPeso: TFloatField;
    QrOriVMIQtdAntArM2: TFloatField;
    QrOriVMIQtdAntArP2: TFloatField;
    QrOriVMIAptoUso: TSmallintField;
    QrOriVMINotaMPAG: TFloatField;
    QrOriVMIMarca: TWideStringField;
    QrOriVMITpCalcAuto: TIntegerField;
    QrOriVMIZerado: TSmallintField;
    QrOriVMIEmFluxo: TSmallintField;
    QrOriVMILnkIDXtr: TIntegerField;
    QrOriVMICustoMOM2: TFloatField;
    QrOriVMINotFluxo: TIntegerField;
    QrOriVMIFatNotaVNC: TFloatField;
    QrOriVMIFatNotaVRC: TFloatField;
    QrOriVMIPedItsLib: TIntegerField;
    QrOriVMIPedItsFin: TIntegerField;
    QrOriVMIPedItsVda: TIntegerField;
    QrOriVMIReqMovEstq: TIntegerField;
    QrOriVMIStqCenLoc: TIntegerField;
    QrOriVMIItemNFe: TIntegerField;
    QrOriVMIVSMorCab: TIntegerField;
    QrOriVMIVSMulFrnCab: TIntegerField;
    QrOriVMIClientMO: TIntegerField;
    QrOriVMICustoPQ: TFloatField;
    QrOriVMIKgCouPQ: TFloatField;
    QrOriVMINFeSer: TSmallintField;
    QrOriVMINFeNum_1: TIntegerField;
    QrOriVMIVSMulNFeCab: TIntegerField;
    QrOriVMIGGXRcl: TIntegerField;
    QrOriVMIJmpMovID: TIntegerField;
    QrOriVMIJmpNivel1: TIntegerField;
    QrOriVMIJmpNivel2: TIntegerField;
    QrOriVMIJmpGGX: TIntegerField;
    frxQUI_RECEI_014_00_E: TfrxReport;
    QrEmit0GraCorCad: TIntegerField;
    QrEmit0NoGraCorCad: TWideStringField;
    QrEmit1GraCorCad: TIntegerField;
    QrEmit1NoGraCorCad: TWideStringField;
    frxQUI_RECEI_014_00_F: TfrxReport;
    QrPQOGraCorCad: TIntegerField;
    QrPQONoGraCorCad: TWideStringField;
    CustosERendimentoAgrupadoPor: TMenuItem;
    QrEmitCor: TmySQLQuery;
    frxDsEmitCor: TfrxDBDataset;
    Cor1: TMenuItem;
    Rebaixe1: TMenuItem;
    CoreRebaixe1: TMenuItem;
    QrEmitCorGRUPO: TWideStringField;
    QrEmitCorGraCorCad: TIntegerField;
    QrEmitCorSemiCodEspReb: TIntegerField;
    InsumosUtilizadosAgrupadospor1: TMenuItem;
    Cor2: TMenuItem;
    Rebaixe2: TMenuItem;
    CoreRebaixe2: TMenuItem;
    QrCusCor_: TmySQLQuery;
    QrCusCor_GRUPO: TWideStringField;
    QrCusCor_Peso: TFloatField;
    QrCusCor_Valor: TFloatField;
    QrCusCor_CustoUni: TFloatField;
    frxDsCusCor_: TfrxDBDataset;
    QrGruCor_: TmySQLQuery;
    QrGruCor_GrupoQuimico: TIntegerField;
    QrGruCor_NoGrupoQuimico: TWideStringField;
    QrGruCor_GraCorCad: TLargeintField;
    QrGruCor_SemiCodEspReb: TLargeintField;
    QrGruCor_Peso: TFloatField;
    QrGruCor_Valor: TFloatField;
    QrGruCor_CustoUni: TFloatField;
    QrCusCor_GraCorCad: TLargeintField;
    QrCusCor_SemiCodEspReb: TLargeintField;
    frxDsGruCor_: TfrxDBDataset;
    QrInsum2: TmySQLQuery;
    QrInsum2GRUPO: TWideStringField;
    QrInsum2GrupoQuimico: TIntegerField;
    QrInsum2NoGrupoQuimico: TWideStringField;
    QrInsum2Insumo: TIntegerField;
    QrInsum2Nome: TWideStringField;
    QrInsum2GraCorCad: TLargeintField;
    QrInsum2SemiCodEspReb: TLargeintField;
    QrInsum2Peso: TFloatField;
    QrInsum2Valor: TFloatField;
    QrInsum2CustoUni: TFloatField;
    frxDsinsum2: TfrxDBDataset;
    frxQUI_RECEI_014_00_G: TfrxReport;
    QrEmit0SemiCodEspe: TIntegerField;
    QrEmit0SemiTxtEspe: TWideStringField;
    QrEmit0BRL_USD: TFloatField;
    QrEmit0BRL_EUR: TFloatField;
    QrEmit0DtaCambio: TDateField;
    QrEmit0VSMovCod: TIntegerField;
    QrEmit0HoraIni: TTimeField;
    QrEmit0SemiCodEspReb: TIntegerField;
    QrEmit0SemiTxtEspReb: TWideStringField;
    QrEmit0DtaBaixa: TDateField;
    QrEmit0DtCorrApo: TDateTimeField;
    QrEmit0Empresa: TIntegerField;
    QrEmit0AWServerID: TIntegerField;
    QrEmit0AWStatSinc: TSmallintField;
    QrEmit1SemiCodEspe: TIntegerField;
    QrEmit1SemiTxtEspe: TWideStringField;
    QrEmit1BRL_USD: TFloatField;
    QrEmit1BRL_EUR: TFloatField;
    QrEmit1DtaCambio: TDateField;
    QrEmit1VSMovCod: TIntegerField;
    QrEmit1HoraIni: TTimeField;
    QrEmit1SemiCodEspReb: TIntegerField;
    QrEmit1SemiTxtEspReb: TWideStringField;
    QrEmit1DtaBaixa: TDateField;
    QrEmit1DtCorrApo: TDateTimeField;
    QrEmit1Empresa: TIntegerField;
    QrEmit1AWServerID: TIntegerField;
    QrEmit1AWStatSinc: TSmallintField;
    PageControl1: TPageControl;
    TabSheet8: TTabSheet;
    Splitter1: TSplitter;
    DGDados: TDBGrid;
    Panel6: TPanel;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    dmkDBGridZTO1: TdmkDBGridZTO;
    PCPWEOri: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid2: TDBGrid;
    GroupBox7: TGroupBox;
    DBGrid3: TDBGrid;
    TabSheet2: TTabSheet;
    DGDadosOri: TDBGrid;
    TabSheet5: TTabSheet;
    DBGrid6: TDBGrid;
    TabSheet6: TTabSheet;
    DBGrid7: TDBGrid;
    DBGIts: TDBGrid;
    PCPWEDst: TPageControl;
    TabSheet3: TTabSheet;
    Panel12: TPanel;
    DGDadosDst: TDBGrid;
    DBGrid1: TDBGrid;
    TabSheet4: TTabSheet;
    Panel13: TPanel;
    DBGrid4: TDBGrid;
    DBGrid5: TDBGrid;
    TabSheet7: TTabSheet;
    DBGrid8: TDBGrid;
    TabSheet9: TTabSheet;
    BtEmitSbtQtd: TBitBtn;
    PMEmitSbtQtd: TPopupMenu;
    IncluinovoSubstratodeRendimento1: TMenuItem;
    AlteraSubstratodeRendimentoselecionado1: TMenuItem;
    QrEmitSbtQtd: TMySQLQuery;
    QrEmitSbtQtdNO_EmitSbtCad: TWideStringField;
    QrEmitSbtQtdCodigo: TIntegerField;
    QrEmitSbtQtdControle: TIntegerField;
    QrEmitSbtQtdEmitSbtCad: TIntegerField;
    QrEmitSbtQtdCouIntros: TFloatField;
    QrEmitSbtQtdVSUmidade: TSmallintField;
    QrEmitSbtQtdVSBastidao: TSmallintField;
    QrEmitSbtQtdAreaM2: TFloatField;
    QrEmitSbtQtdAreaP2: TFloatField;
    QrEmitSbtQtdPesoKg: TFloatField;
    QrEmitSbtQtdMediaKg: TFloatField;
    QrEmitSbtQtdMediaM2: TFloatField;
    QrEmitSbtQtdMediaP2: TFloatField;
    QrEmitSbtQtdEspMedLin: TFloatField;
    QrEmitSbtQtdLk: TIntegerField;
    QrEmitSbtQtdDataCad: TDateField;
    QrEmitSbtQtdDataAlt: TDateField;
    QrEmitSbtQtdUserCad: TIntegerField;
    QrEmitSbtQtdUserAlt: TIntegerField;
    QrEmitSbtQtdAlterWeb: TSmallintField;
    QrEmitSbtQtdAWServerID: TIntegerField;
    QrEmitSbtQtdAWStatSinc: TSmallintField;
    QrEmitSbtQtdAtivo: TSmallintField;
    QrEmitSbtQtdNO_VSUmidade: TWideStringField;
    QrEmitSbtQtdNO_VSBastidao: TWideStringField;
    DsEmitSbtQtd: TDataSource;
    DBGrid9: TDBGrid;
    ExcluiSubstratodeRendimentoselecionado1: TMenuItem;
    QrEmitSbtQtdLinRebCul: TFloatField;
    QrEmitSbtQtdLinRebCab: TFloatField;
    QrEmitGruSbtCouIntros: TFloatField;
    QrEmitGruSbtAreaM2: TFloatField;
    QrEmitGruSbtAreaP2: TFloatField;
    QrEmitGruSbtPesoKg: TFloatField;
    QrEmitGruSbtMediaKg: TFloatField;
    QrEmitGruSbtMediaM2: TFloatField;
    QrEmitGruSbtMediaP2: TFloatField;
    QrEmitGruSbtPercEsperRend: TFloatField;
    QrEmitGruSbtEspMedLinReb: TFloatField;
    QrEmitGruSbtEspMedLinWB: TFloatField;
    QrEmitGruSbtEspMedLinTrpFator: TFloatField;
    Panel7: TPanel;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label10: TLabel;
    DBEdit6: TDBEdit;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    Label12: TLabel;
    DBEdit8: TDBEdit;
    Label13: TLabel;
    DBEdit9: TDBEdit;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    Label15: TLabel;
    DBEdit11: TDBEdit;
    Label16: TLabel;
    DBEdit12: TDBEdit;
    Label17: TLabel;
    DBEdit13: TDBEdit;
    Label18: TLabel;
    DBEdit14: TDBEdit;
    Label19: TLabel;
    DBEdit15: TDBEdit;
    BtEmitUsoSbt: TBitBtn;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    DBGBaixaPQ: TdmkDBGridZTO;
    Label20: TLabel;
    DBGrid10: TDBGrid;
    Label21: TLabel;
    PMEmitUsoSbt: TPopupMenu;
    Incluiusodesubstratoderendimento1: TMenuItem;
    Alterausodesubstratoderendimentoselecionado1: TMenuItem;
    Excluiusodesubstratoderendimentoselecionado1: TMenuItem;
    QrEmitUsoSbt: TMySQLQuery;
    DsEmitUsoSbt: TDataSource;
    QrEmitUsoSbtCodigo: TIntegerField;
    QrEmitUsoSbtControle: TIntegerField;
    QrEmitUsoSbtIDEmitSbtQtd: TIntegerField;
    QrEmitUsoSbtCouIntros: TFloatField;
    QrEmitUsoSbtAreaM2: TFloatField;
    QrEmitUsoSbtAreaP2: TFloatField;
    QrEmitUsoSbtPesoKg: TFloatField;
    QrEmitUsoSbtPercEsperRend: TFloatField;
    QrEmitSbtQtdFatorUmidade: TFloatField;
    QrEmitSbtQtdPercEsperRend: TFloatField;
    QrEmitSbtQtdLinRebMed: TFloatField;
    QrEmitSbtQtdSdoCouIntros: TFloatField;
    QrEmitSbtQtdSdoAreaM2: TFloatField;
    QrEmitSbtQtdSdoAreaP2: TFloatField;
    QrCabsAreaP2: TFloatField;
    QrEmitUsoSbtNO_EmitSbtCad: TWideStringField;
    QrCabsSbtRendEsper: TFloatField;
    frxQUI_RECEI_014_00_A1: TfrxReport;
    QrEmit0SbtCouIntros: TFloatField;
    QrEmit0SbtAreaM2: TFloatField;
    QrEmit0SbtAreaP2: TFloatField;
    QrEmit0SbtRendEsper: TFloatField;
    QrEmit0PercDePercRend: TFloatField;
    frxDsEmitSbtQtd: TfrxDBDataset;
    frxQUI_RECEI_014_00_C1: TfrxReport;
    InsumosporNFseLotes1: TMenuItem;
    frxQUI_RECEI_014_00_C2: TfrxReport;
    QrInsumo3: TMySQLQuery;
    QrInsumo3NF: TIntegerField;
    QrInsumo3NF_RP: TIntegerField;
    QrInsumo3NF_CC: TIntegerField;
    QrInsumo3IQ: TIntegerField;
    QrInsumo3DataE: TDateField;
    QrInsumo3Peso: TFloatField;
    QrInsumo3Valor: TFloatField;
    QrInsumo3CustoBXA: TFloatField;
    QrInsumo3CustoINN: TFloatField;
    QrInsumo3Insumo: TIntegerField;
    xLote: TWideStringField;
    frxDsInsumo3: TfrxDBDataset;
    QrInsumo3xLote: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEmitGruAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEmitGruBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrEmitGruAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrEmitGruBeforeClose(DataSet: TDataSet);
    procedure QrCliOrigBeforeClose(DataSet: TDataSet);
    procedure QrCliOrigAfterScroll(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxQUI_RECEI_014_00_AGetValue(const VarName: string;
      var Value: Variant);
    procedure QrEmitGruCalcFields(DataSet: TDataSet);
    procedure Encerra1Click(Sender: TObject);
    procedure Fechamento1Click(Sender: TObject);
    procedure Insumosusados1Click(Sender: TObject);
    procedure InsumosporNFs1Click(Sender: TObject);
    procedure PMPesagemPopup(Sender: TObject);
    procedure InformeIMEC1Click(Sender: TObject);
    procedure BtPesagemClick(Sender: TObject);
    procedure RemovedoIMEC1Click(Sender: TObject);
    procedure QrVSPWECabAfterScroll(DataSet: TDataSet);
    procedure CustoseRendimentoscomCouroB1Click(Sender: TObject);
    procedure QrVSPWECabBeforeClose(DataSet: TDataSet);
    procedure CustoseRendimentoscomCouroA1Click(Sender: TObject);
    procedure QrEmitCorAfterScroll(DataSet: TDataSet);
    procedure DBGBaixaPQDblClick(Sender: TObject);
    procedure Cor1Click(Sender: TObject);
    procedure Rebaixe1Click(Sender: TObject);
    procedure CoreRebaixe1Click(Sender: TObject);
    procedure Cor2Click(Sender: TObject);
    procedure Rebaixe2Click(Sender: TObject);
    procedure CoreRebaixe2Click(Sender: TObject);
    procedure QrCusCor_AfterScroll(DataSet: TDataSet);
    procedure QrGruCor_AfterScroll(DataSet: TDataSet);
    procedure BtEmitSbtQtdClick(Sender: TObject);
    procedure IncluinovoSubstratodeRendimento1Click(Sender: TObject);
    procedure AlteraSubstratodeRendimentoselecionado1Click(Sender: TObject);
    procedure PMEmitSbtQtdPopup(Sender: TObject);
    procedure PMEmitUsoSbtPopup(Sender: TObject);
    procedure QrCabsAfterScroll(DataSet: TDataSet);
    procedure QrCabsBeforeClose(DataSet: TDataSet);
    procedure Incluiusodesubstratoderendimento1Click(Sender: TObject);
    procedure Alterausodesubstratoderendimentoselecionado1Click(
      Sender: TObject);
    procedure Excluiusodesubstratoderendimentoselecionado1Click(
      Sender: TObject);
    procedure ExcluiSubstratodeRendimentoselecionado1Click(Sender: TObject);
    procedure BtEmitUsoSbtClick(Sender: TObject);
    procedure QrEmit0CalcFields(DataSet: TDataSet);
    procedure InsumosporNFseLotes1Click(Sender: TObject);
  private
    FAgrupar: TGrupoRendiCusto;
    F_Emit, F_PQX, F_PQO: String;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure CriaDadosTemporarios();
    procedure Erro(Idx: Integer);
    procedure ImprimeInsumosUtilizadosAgrupados(Agrupar: TGrupoRendiCusto);
    procedure ImprimeRendimentoECustoAgrupado(Agrupar: TGrupoRendiCusto);
    procedure MostraFormEmitCab(SQLType: TSQLType);
    procedure MostraFormEmitUsoSbt(SQLType: TSQLType);
    procedure MostraFormEmitSbtQtd(SQLType: TSQLType);
    function  ObtemCampoAgrupamento(Agrupar: TGrupoRendiCusto): String;

    procedure ReopenCustoPQ(Agrupar: TGrupoRendiCusto);
    //procedure ReopenGruCor();
    procedure ReopenInsumos(Agrupar: TGrupoRendiCusto);
    procedure ReopenEmitX(Qry: TmySQLQuery; Retrabalho: Integer;
              Agrupar: TGrupoRendiCusto);
    procedure ReopenPQO(Agrupar: TGrupoRendiCusto);
    procedure ReopenVSPWECab(MovimCod: Integer);
    procedure ReopenEmitSbtQtd(Controle: Integer);
    procedure ReopenEmitUsoSbt(Controle: Integer);
    function  PreparaCustoERendimento(Agrupar: TGrupoRendiCusto): String;

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenCabs(Tipo, Codigo: Integer);
    procedure ReopenCliOrig(CliOrig: Integer);
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmEmitGru: TFmEmitGru;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, EmitCab,
  EmitGruFim, UnPQ_PF, ModVS, UnVS_PF, PQx, ModVS_CRC, EmitSbtQtd, EmitUsoSbt;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEmitGru.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEmitGru.MostraFormEmitCab(SQLType: TSQLType);
var
  Grupo, CliInt, Pesagem: Integer;
begin
  Grupo   := QrEmitGruCodigo.Value;
  CliInt  := QrCliOrigCliOrig.Value;
  Pesagem := QrCabsCodigo.Value;
  //
  UnPQx.MostraFormEmitCab(SQLType, Pesagem);
  //
  LocCod(Grupo, Grupo);
  //
  if (QrCliOrig.State <> dsInactive) and (QrCliOrig.RecordCount > 0) then
    QrCliOrig.Locate('CliOrig', CliInt, []);
  if (QrCabs.State <> dsInactive) and (QrCabs.RecordCount > 0) then
    QrCabs.Locate('Codigo', Pesagem, []);
  //
end;

procedure TFmEmitGru.MostraFormEmitSbtQtd(SQLType: TSQLType);
var
  Codigo, Controle: Integer;
begin
  Codigo := QrEmitGruCodigo.Value;
  Controle := 0;
  if DBCheck.CriaFm(TFmEmitSbtQtd, FmEmitSbtQtd, afmoNegarComAviso) then
  begin
    FmEmitSbtQtd.ImgTipo.SQLType := SQLType;
    FmEmitSbtQtd.EdCodigo.ValueVariant := Codigo;
    if SQLType = stUpd then
    begin
      FmEmitSbtQtd.EdControle.ValueVariant   := QrEmitSbtQtdControle.Value;
      FmEmitSbtQtd.EdEmitSbtCad.ValueVariant := QrEmitSbtQtdEmitSbtCad.Value;
      FmEmitSbtQtd.CBEmitSbtCad.KeyValue     := QrEmitSbtQtdEmitSbtCad.Value;
      FmEmitSbtQtd.EdCouIntros.ValueVariant  := QrEmitSbtQtdCouIntros.Value;
      FmEmitSbtQtd.RGVSUmidade.ItemIndex     := QrEmitSbtQtdVSUmidade.Value;
      FmEmitSbtQtd.RGVSBastidao.ItemIndex    := QrEmitSbtQtdVSBastidao.Value;
      FmEmitSbtQtd.EdAreaM2.ValueVariant     := QrEmitSbtQtdAreaM2.Value;
      FmEmitSbtQtd.EdAreaP2.ValueVariant     := QrEmitSbtQtdAreaP2.Value;
      FmEmitSbtQtd.EdPesoKg.ValueVariant     := QrEmitSbtQtdPesoKg.Value;
      FmEmitSbtQtd.EdMediaKg.ValueVariant    := QrEmitSbtQtdMediaKg.Value;
      FmEmitSbtQtd.EdMediaM2.ValueVariant    := QrEmitSbtQtdMediaM2.Value;
      FmEmitSbtQtd.EdMediaP2.ValueVariant    := QrEmitSbtQtdMediaP2.Value;
      FmEmitSbtQtd.EdEspMedLin.ValueVariant  := QrEmitSbtQtdEspMedLin.Value;
      FmEmitSbtQtd.EdLinRebCul.ValueVariant  := QrEmitSbtQtdLinRebCul.Value;
      FmEmitSbtQtd.EdLinRebCab.ValueVariant  := QrEmitSbtQtdLinRebCab.Value;
    end;
    //
    FmEmitSbtQtd.ShowModal;
    //
    Controle := FmEmitSbtQtd.EdControle.ValueVariant;
    LocCod(Codigo, Codigo);
    QrEmitSbtQtd.Locate('Controle', Controle, []);
    //QrCliOrig.Locate('CliOrig', CliInt, []);
    //QrCabs.Locate('Codigo', Pesagem, []);
    //
    FmEmitSbtQtd.Destroy;
  end;
end;

procedure TFmEmitGru.MostraFormEmitUsoSbt(SQLType: TSQLType);
var
  Grupo, CliInt, Pesagem, IDUsoSbt: Integer;
begin
  Grupo   := QrEmitGruCodigo.Value;
  CliInt  := QrCliOrigCliOrig.Value;
  Pesagem := QrCabsCodigo.Value;
  //
  //UnPQx.MostraFormEmitUsoSbt(SQLType, Pesagem, Grupo);
  if DBCheck.CriaFm(TFmEmitUsoSbt, FmEmitUsoSbt, afmoNegarComAviso) then
  begin
    FmEmitUsoSbt.ImgTipo.SQLType := SQLTYpe;
    //FmEmitUsoSbt.FPesagem := EmitGru;
    FmEmitUsoSbt.FEmitGru := Grupo;
    //
    if SQLType = stUpd then
    begin
      FmEmitUsoSbt.EdControle.ValueVariant     := QrEmitUsoSbtControle.Value;
      //
      FmEmitUsoSbt.EdIDEmitSbtQtd.ValueVariant := QrEmitUsoSbtIDEmitSbtQtd.Value;
      FmEmitUsoSbt.EdCouIntros.ValueVariant    := QrEmitUsoSbtCouIntros.Value;
      FmEmitUsoSbt.EdAreaM2.ValueVariant       := QrEmitUsoSbtAreaM2.Value;
      FmEmitUsoSbt.EdAreaP2.ValueVariant       := QrEmitUsoSbtAreaP2.Value;
      FmEmitUsoSbt.EdPesoKg.ValueVariant       := QrEmitUsoSbtPesoKg.Value;
    end;
    //
    FmEmitUsoSbt.ReopenEmit(Pesagem);
    FmEmitUsoSbt.ReopenEmitSbtQtd();
    FmEmitUsoSbt.ShowModal;
    //
    IDUsoSbt := FmEmitUsoSbt.EdControle.ValueVariant;
    //
    FmEmitUsoSbt.Destroy;
  end;
  //
  LocCod(Grupo, Grupo);
  QrCabs.Locate('Codigo', Pesagem, []);
  QrEmitUsoSbt.Locate('Controle', IDUsoSbt, []);
  //
end;

function TFmEmitGru.ObtemCampoAgrupamento(Agrupar: TGrupoRendiCusto): String;
begin
  case Agrupar of
    //grcIndef: ;//
    grcSemGrupo: ;
    grcCor:         Result := '"Cor: ", IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) +
                              ', emi.GraCorCad, pqo.GraCorCad), " - ", gcc.Nome';
    grcRebaixe:     Result := '"Rebaixe: ", reb.Linhas';
    grcCorERebaixe: Result := '"Cor: ", IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) +
                              ', emi.GraCorCad, pqo.GraCorCad), " - ", gcc.Nome, " @ Rebaixe: ", reb.Linhas';
    else
    begin
      Result := '';
      Erro(2);
    end;
  end;
end;

procedure TFmEmitGru.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrEmitGru);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrEmitGru, QrCliOrig);
  MyObjects.HabilitaMenuItemCabUpd(Encerra1, QrEmitGru);
end;

procedure TFmEmitGru.PMEmitSbtQtdPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluinovoSubstratodeRendimento1, QrEmitGru);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSubstratodeRendimentoselecionado1, QrEmitSbtQtd);
  MyObjects.HabilitaMenuItemItsDel(ExcluiSubstratodeRendimentoselecionado1, QrEmitSbtQtd);
end;

procedure TFmEmitGru.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrEmitGru);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrCliOrig);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrCliOrig);
end;

procedure TFmEmitGru.PMPesagemPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsUpd(InformeIMEC1, QrCabs);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrCliOrig);
  //
  InformeIMEC1.Enabled := InformeIMEC1.Enabled and (QrCabsVSMovCod.Value = 0);
end;

procedure TFmEmitGru.PMEmitUsoSbtPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluiusodesubstratoderendimento1, QrCabs);
  MyObjects.HabilitaMenuItemItsUpd(Alterausodesubstratoderendimentoselecionado1, QrEmitUsoSbt);
  MyObjects.HabilitaMenuItemItsDel(Excluiusodesubstratoderendimentoselecionado1, QrEmitUsoSbt);
end;

function TFmEmitGru.PreparaCustoERendimento(Agrupar: TGrupoRendiCusto): String;
const
  PorCor = True;
var
  sFldGrupo: String;
begin
  case Agrupar of
    //grcIndef: ;//
    grcSemGrupo:    Result := 'Custos e Rendimentos';
    grcCor:         Result := 'Custos e Rendimentos por Cor';
    grcRebaixe:     Result := 'Custos e Rendimentos pelo Rebaixe';
    grcCorERebaixe: Result := 'Custos e Rendimentos por Cor e Rebaixe';
    else
    begin
      Result := '???';
      Erro(1);
    end;
  end;
  if Agrupar <> grcSemGrupo then
  begin
    sFldGrupo := ObtemCampoAgrupamento(Agrupar);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmitCor, Dmod.MyDB, [
(*
    'SELECT emi.GraCorCad, emi.SemiCodEspReb, ',
    'CONCAT(' + sFldGrupo + ') GRUPO',
    'FROM emit emi  ',
    'LEFT JOIN gracorcad gcc ON gcc.Codigo=emi.GraCorCad ',
    'LEFT JOIN espessuras reb ON reb.Codigo=emi.SemiCodEspReb',
    'WHERE emi.EmitGru>0',
    'GROUP BY GRUPO',
    'ORDER BY GRUPO',
    '']);
*)
    'SELECT emi.GraCorCad, emi.SemiCodEspReb, ',
    'CONCAT(' + sFldGrupo + ') GRUPO',
    'FROM pqx pqx ',
    'LEFT JOIN emit       emi ON emi.Codigo=pqx.OrigemCodi AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0110),
    'LEFT JOIN pqd        pqd ON pqd.Codigo=pqx.OrigemCodi AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0170),
    //'LEFT JOIN pqi        pqd ON pqi.Codigo=pqx.OrigemCodi AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0180),
    //'LEFT JOIN pqn        pqd ON pqn.Codigo=pqx.OrigemCodi AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0185),
    'LEFT JOIN pqo        pqo ON pqo.Codigo=pqx.OrigemCodi AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', emi.GraCorCad, pqo.GraCorCad) ',
    'LEFT JOIN espessuras reb ON reb.Codigo=IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', emi.SemiCodEspReb, pqo.SemiCodEspReb) ',
    'WHERE emi.EmitGru>0 ',
    'GROUP BY GRUPO ',
    'ORDER BY GRUPO ',
    '']);
  end;
  // Precisa Fazer?
  //DMod.AualizaTotaiEmiGru(Codigo);
  UnDmkDAC_PF.AbreMySQLQuery0(QrRendim, Dmod.MyDB, [
  'SELECT SUM(ROUND(emi.AreaM2 * 10000 / 929.0304 * 4) / 4) AREAP2, ',
  'SUM(ROUND(emi.SemiAreaM2 * 10000 / 929.0304 * 4) / 4) SEMIAREAP2, ',
  'SUM(emi.AreaM2) AreaM2, SUM(emi.SemiAreaM2) SemiAreaM2',
  'FROM emit emi ',
  'WHERE emi.EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  'AND emi.ClienteI=' + Geral.FF0(QrCliOrigCliOrig.Value),
  'AND emi.Retrabalho=0',
  '']);
  //
  ReopenCustoPQ(grcSemGrupo);
  //
  if (QrCliOrig.State = dsInactive) or (QrCliOrig.RecordCount = 0) then
  begin
    Geral.MB_Aviso('N�o h� dados a serem impressos!');
    Exit;
  end;
  //
  if Agrupar = grcSemGrupo then
  begin
    ReopenEmitX(QrEmit0, 0, Agrupar);
    ReopenEmitX(QrEmit1, 1, Agrupar);
    ReopenPQO(Agrupar);
  end;
end;

procedure TFmEmitGru.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEmitGruCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEmitGru.DefParams;
begin
  VAR_GOTOTABELA := 'emitgru';
  VAR_GOTOMYSQLTABLE := QrEmitGru;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM emitgru');
  //VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('WHERE Codigo=:P0');
  //
  //VAR_SQL2.Add('WHERE CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE Nome Like :P0');
  //
end;

procedure TFmEmitGru.Encerra1Click(Sender: TObject);
var
  Grupo, CliInt, Pesagem: Integer;
begin
  Grupo   := QrEmitGruCodigo.Value;
  CliInt  := QrCliOrigCliOrig.Value;
  Pesagem := QrCabsCodigo.Value;
  //
  if DBCheck.CriaFm(TFmEmitGruFim, FmEmitGruFim, afmoNegarComAviso) then
  begin
    FmEmitGruFim.ImgTipo.SQLType := stUpd;
    FmEmitGruFim.FCodigo := QrEmitGruCodigo.Value;
    if QrEmitGruEncerrado.Value > 2 then
      FmEmitGruFim.EdEncerrado.ValueVariant := QrEmitGruEncerrado.Value;
    FmEmitGruFim.EdUSD_BRL.ValueVariant   := QrEmitGruUSD_BRL.Value;
    FmEmitGruFim.EdAreaManP2.ValueVariant := QrEmitGruAreaManP2.Value;
    // Fazer depois do p2?
    FmEmitGruFim.EdAreaManM2.ValueVariant := QrEmitGruAreaManM2.Value;
    //
    FmEmitGruFim.ShowModal;
    //
    LocCod(Grupo, Grupo);
    QrCliOrig.Locate('CliOrig', CliInt, []);
    QrCabs.Locate('Codigo', Pesagem, []);
    //
    FmEmitGruFim.Destroy;
  end;
end;

procedure TFmEmitGru.Erro(Idx: Integer);
begin
  begin
    Geral.MB_Erro('Tipo de agrupamento indefinido em "PreparaCustoERendimento('
    + Geral.FF0(Idx) + ')"');
  end;
end;

procedure TFmEmitGru.ExcluiSubstratodeRendimentoselecionado1Click(
  Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'emitsbtqtd', 'Controle', QrEmitSbtQtdControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrEmitSbtQtd,
      QrEmitSbtQtdControle, QrEmitSbtQtdControle.Value);
    ReopenEmitSbtQtd(Controle);
  end;
end;

procedure TFmEmitGru.Excluiusodesubstratoderendimentoselecionado1Click(
  Sender: TObject);
var
  Controle, Pesagem: Integer;
begin
  Pesagem  := QrEmitUsoSbtCodigo.Value;
  Controle := QrEmitUsoSbtControle.Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'emitusosbt', 'Controle', Controle, Dmod.MyDB) = ID_YES then
  begin
    PQ_PF.AtualizaSaldo_Emit_EMitUsoSbt(Pesagem);
    PQ_PF.AtualizaSaldo_EmitSbtQtd(Controle);
    //
    Controle := GOTOy.LocalizaPriorNextIntQr(QrEmitUsoSbt,
      QrEmitUsoSbtControle, QrEmitUsoSbtControle.Value);
    ReopenEmitUsoSbt(Controle);
  end;
end;

procedure TFmEmitGru.ImprimeInsumosUtilizadosAgrupados(
  Agrupar: TGrupoRendiCusto);
var
  sFldGrupo: String;
begin
  FAgrupar := Agrupar;
  //
  if MyObjects.FIC((QrCliOrig.State = dsInactive) or (QrCliOrig.RecordCount = 0),
  nil, 'N�o h� dados a serem impressos!') then
    Exit;
  //
  CriaDadosTemporarios();
  //
  ReopenCustoPQ(grcSemGrupo);
  //
  sFldGrupo := ObtemCampoAgrupamento(Agrupar);
  //
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrInsum2, DModG.MyPID_DB, [
  'SELECT CONCAT(' + sFldGrupo + ') GRUPO, ',
  'pq_.GrupoQuimico, gpq.Nome NoGrupoQuimico,',
  'pqx.Insumo, pq_.Nome,',
  'IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', emi.GraCorCad, pqo.GraCorCad) GraCorCad, ',
  'IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', emi.SemiCodEspReb, pqo.SemiCodEspReb) SemiCodEspReb,',
(*
  '-pqx.Peso Peso, -pqx.Valor Valor,',
  'IF(-pqx.Peso > 0, -pqx.Valor / -pqx.Peso, 0) CustoUni ',
*)
  '-SUM(pqx.Peso) Peso, -SUM(pqx.Valor) Valor,',
  'IF(-SUM(pqx.Peso) > 0, -SUM(pqx.Valor) / -SUM(pqx.Peso), 0) CustoUni ',

  'FROM _pqx_ pqx',
  'LEFT JOIN ' + TMeuDB + '.pq pq_ ON pq_.Codigo=pqx.Insumo',
  'LEFT JOIN ' + TMeuDB + '.gruposquimicos gpq ON gpq.Codigo=pq_.GrupoQuimico',
  'LEFT JOIN _emit_ emi ON emi.Codigo=pqx.OrigemCodi',
  '  AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + '',
  // precisa?
  //'LEFT JOIN _pqd_ pqd ON pqd.Codigo=pqx.OrigemCodi',
  //'  AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0170) + '',
  'LEFT JOIN _pqo_ pqo ON pqo.Codigo=pqx.OrigemCodi',
  '  AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0190) + '',
  'LEFT JOIN ' + TMeuDB + '.gracorcad gcc ON gcc.Codigo=',
  '  IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', emi.GraCorCad, pqo.GraCorCad)',
  'LEFT JOIN ' + TMeuDB + '.espessuras reb ON reb.Codigo=',
  '  IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', emi.SemiCodEspReb, pqo.SemiCodEspReb)',
  'WHERE pqx.CliOrig=' + Geral.FF0(QrCliOrigCliOrig.Value),
  'AND (IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', emi.EmitGru, ',
  '  IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0190) + ', pqo.EmitGru, ',
  '  1=2))) =' + Geral.FF0(QrEmitGruCodigo.Value),
//  'ORDER BY GRUPO, GrupoQuimico, pq_.Nome',
  'GROUP BY GRUPO, GrupoQuimico, Insumo',
  'ORDER BY GRUPO, GrupoQuimico, pq_.Nome',
  '']);
  //Geral.MB_SQL(Self, QrInsum2);
  //
  MyObjects.frxDefineDataSets(frxQUI_RECEI_014_00_G, [
  DModG.frxDsDono,
  frxDsCustoPQ,
  frxDsInsum2
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_014_00_G, 'Insumos utilizados por...');
end;

procedure TFmEmitGru.ImprimeRendimentoECustoAgrupado(Agrupar: TGrupoRendiCusto);
var
  Titulo: String;
begin
  FAgrupar := Agrupar;
  Titulo := PreparaCustoERendimento(Agrupar);
  //
  MyObjects.frxDefineDataSets(frxQUI_RECEI_014_00_F, [
  DModG.frxDsDono,
  frxDsEmitGru,
  frxDsEmitCor,
  frxDsEmit0,
  frxDsEmit1,
  frxDsPQO,
  frxDsRendim,
  frxDsCustoPQ
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_014_00_F, Titulo);
end;

procedure TFmEmitGru.IncluinovoSubstratodeRendimento1Click(Sender: TObject);
begin
  MostraFormEmitSbtQtd(stIns);
end;

procedure TFmEmitGru.Incluiusodesubstratoderendimento1Click(Sender: TObject);
begin
  MostraFormEmitUsoSbt(stIns);
end;

procedure TFmEmitGru.InformeIMEC1Click(Sender: TObject);
var
  VSMovCod, Codigo, Tipo, I, Erros: Integer;
  Tabela: String;
  //
  function SelecionaVSMovCod(): Boolean;
  const
    Aviso  = '...';
    Titulo = 'Sele��o de IME-C';
    Prompt = 'Informe o IME-C';
    Campo  = 'Descricao';
  var
    Resp: Variant;
  begin
    Result := False;
    Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT vmi.MovimCod Codigo,',
    'CONCAT(gg1.Nome,',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ' + Campo,
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'LEFT JOIN vspwecab cab ON cab.MovimCod=vmi.MovimCod',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed',
    'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidEmProcWE)),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminEmWEndInn)),
    'AND cab.EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
    ''], Dmod.MyDB, True);
    if Resp <> Null then
    begin
      VSMovCod := Resp;
      Result := VSMovCod <> 0;
    end;
  end;
begin
  if MyObjects.FIC(DBGBaixaPQ.SelectedRows.Count = 0, nil,
  'Nenhum item foi selecionado!') then Exit;
  //
  Erros := 0;
  with DBGBaixaPQ.DataSource.DataSet do
  for I := 0 to DBGBaixaPQ.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBGBaixaPQ.SelectedRows.Items[i]));
    GotoBookmark(DBGBaixaPQ.SelectedRows.Items[i]);
    //
    if QrCabsVSMovCod.Value <> 0 then
      Erros := Erros + 1;
  end;
  if Erros > 0 then
  begin
    Geral.MB_Aviso('A��o abortada j� existem ' + Geral.FF0(Erros) +
    ' pesagens atreladas a IME-C!');
    Exit;
  end;
  if Geral.MB_Pergunta('Confirma a adi��o das baixas a IME-C a ser selecionado?') = ID_YES then
  begin
    if not SelecionaVSMovCod() then
      Exit;
    with DBGBaixaPQ.DataSource.DataSet do
    for I := 0 to DBGBaixaPQ.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGBaixaPQ.SelectedRows.Items[i]));
      GotoBookmark(DBGBaixaPQ.SelectedRows.Items[i]);
      //
      Tipo   := Trunc(QrCabsTipo.Value);
      Tabela := PQ_PF.ObtemNomeTabelaTipoMovPQ(Tipo);
      Codigo := QrCabsCodigo.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
      'VSMovCod'], ['Codigo'], [VSMovCod], [Codigo], True) then
      begin
        DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(VSMovCod);
        VS_PF.AtualizaTotaisVSCurCab(VSMovCod);
      end;
    end;
    LocCod(QrEmitGruCodigo.Value, QrEmitGruCodigo.Value);
  end;
end;

procedure TFmEmitGru.InsumosporNFs1Click(Sender: TObject);
begin
  if (QrCliOrig.State = dsInactive) or (QrCliOrig.RecordCount = 0) then
  begin
    Geral.MB_Aviso('N�o h� dados a serem impressos!');
    Exit;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrInsumo2, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _PQS_NF_EMITGRU_PQX_BXA_1; ',
  'CREATE TABLE _PQS_NF_EMITGRU_PQX_BXA_1 ',
  'SELECT DISTINCT pqx.OrigemCodi, pqx.OrigemCtrl,  ',
  'pqx.Tipo, pqx.Insumo  ',
  'FROM ' + TMeuDB + '.pqx pqx',
  'LEFT JOIN ' + TMeuDB + '.pq pq_ ON pq_.Codigo=pqx.Insumo',
  'LEFT JOIN ' + TMeuDB + '.emit emi ON emi.Codigo=pqx.OrigemCodi AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0110),
  'LEFT JOIN ' + TMeuDB + '.pqo pqo ON pqo.Codigo=pqx.OrigemCodi AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
  'WHERE pqx.CliOrig=' + Geral.FF0(QrCliOrigCliOrig.Value),
  'AND (IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', emi.EmitGru, ',
  '  IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0190) + ', pqo.EmitGru, ',
  '  1=2))) =' + Geral.FF0(QrEmitGruCodigo.Value),
  'GROUP BY pqx.Insumo, pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Tipo ',
  'ORDER BY pq_.Nome; ',
  'DROP TABLE IF EXISTS _PQS_NF_EMITGRU_PQX_BXA_2 ',
  '; ',
  'CREATE TABLE _PQS_NF_EMITGRU_PQX_BXA_2 ',
  'SELECT bxa.OrigemCodi, bxa.OrigemCtrl, bxa.Tipo,  ',
  'bxa.Insumo, SUM(pqw.Peso) Peso,  ',
  'SUM(pqw.Valor) Valor,  ',
  'pqw.DstCtrl, pqw.DstCodi, pqw.DstTipo ',
  'FROM _PQS_NF_EMITGRU_PQX_BXA_1 bxa ',
  'LEFT JOIN ' + TMeuDB + '.pqw pqw ',
  '  ON pqw.OriCodi=bxa.OrigemCodi ',
  '  AND pqw.OriCtrl=bxa.OrigemCtrl ',
  '  AND pqw.OriTipo=bxa.Tipo ',
  'GROUP BY pqw.DstCtrl, pqw.DstCodi, pqw.DstTipo ',
  '; ',
  // ini 2022-03-01
(*
  'SELECT Bxa.Insumo, pq_.Nome,  ',
  'pqe.NF, pqe.NF_RP, pqe.NF_CC, pqe.IQ, pqe.DataE,  ',
  'bxa.Peso, bxa.Valor, bxa.Valor / bxa.Peso CustoBXA, ',
  'pqi.ValorItem / pqi.PesoVL CustoINN ',
  'FROM _PQS_NF_EMITGRU_PQX_BXA_2 bxa ',
  'LEFT JOIN ' + TMeuDB + '.pqeits pqi ON pqi.Controle=bxa.DstCtrl ',
  'LEFT JOIN ' + TMeuDB + '.pqe pqe ON pqe.Codigo=bxa.DstCodi ',
  'LEFT JOIN ' + TMeuDB + '.pq  pq_ ON pq_.Codigo=bxa.Insumo ',
  'ORDER BY Nome ',
*)
  //
  'SELECT Bxa.Insumo, pq_.Nome,  ',
  'pqe.NF, pqe.NF_RP, pqe.NF_CC, pqe.IQ, pqe.DataE,  ',
  'bxa.Peso, bxa.Valor, bxa.Valor / bxa.Peso CustoBXA, ',
  'pqi.ValorItem / pqi.PesoVL CustoINN ',
  'FROM _PQS_NF_EMITGRU_PQX_BXA_2 bxa ',
  'LEFT JOIN ' + TMeuDB + '.pqeits pqi ON pqi.Controle=bxa.DstCtrl ',
  'LEFT JOIN ' + TMeuDB + '.pqe pqe ON pqe.Codigo=bxa.DstCodi ',
  'LEFT JOIN ' + TMeuDB + '.pq  pq_ ON pq_.Codigo=bxa.Insumo ',
  'ORDER BY Nome ',
  //

  // fim 2022-03-01
  '']);
  //Geral.MB_Teste(QrInsumo2.SQL.Text);
  ReopenCustoPQ(grcSemGrupo);
  //
  MyObjects.frxDefineDataSets(frxQUI_RECEI_014_00_C1, [
  DModG.frxDsDono,
  frxDsEmitGru,
  frxDsCustoPQ,
  frxDsInsumo2
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_014_00_C1, 'Insumos utilizados');
end;

procedure TFmEmitGru.InsumosporNFseLotes1Click(Sender: TObject);
begin
  if (QrCliOrig.State = dsInactive) or (QrCliOrig.RecordCount = 0) then
  begin
    Geral.MB_Aviso('N�o h� dados a serem impressos!');
    Exit;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrInsumo3, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _PQS_NF_EMITGRU_PQX_BXA_1; ',
  'CREATE TABLE _PQS_NF_EMITGRU_PQX_BXA_1 ',
  'SELECT DISTINCT pqx.OrigemCodi, pqx.OrigemCtrl,  ',
  'pqx.Tipo, pqx.Insumo  ',
  'FROM ' + TMeuDB + '.pqx pqx',
  'LEFT JOIN ' + TMeuDB + '.pq pq_ ON pq_.Codigo=pqx.Insumo',
  'LEFT JOIN ' + TMeuDB + '.emit emi ON emi.Codigo=pqx.OrigemCodi AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0110),
  'LEFT JOIN ' + TMeuDB + '.pqo pqo ON pqo.Codigo=pqx.OrigemCodi AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
  'WHERE pqx.CliOrig=' + Geral.FF0(QrCliOrigCliOrig.Value),
  'AND (IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', emi.EmitGru, ',
  '  IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0190) + ', pqo.EmitGru, ',
  '  1=2))) =' + Geral.FF0(QrEmitGruCodigo.Value),
  'GROUP BY pqx.Insumo, pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Tipo ',
  'ORDER BY pq_.Nome; ',
  'DROP TABLE IF EXISTS _PQS_NF_EMITGRU_PQX_BXA_2 ',
  '; ',
  'CREATE TABLE _PQS_NF_EMITGRU_PQX_BXA_2 ',
  'SELECT bxa.OrigemCodi, bxa.OrigemCtrl, bxa.Tipo,  ',
  'bxa.Insumo, SUM(pqw.Peso) Peso,  ',
  'SUM(pqw.Valor) Valor,  ',
  'pqw.DstCtrl, pqw.DstCodi, pqw.DstTipo ',
  'FROM _PQS_NF_EMITGRU_PQX_BXA_1 bxa ',
  'LEFT JOIN ' + TMeuDB + '.pqw pqw ',
  '  ON pqw.OriCodi=bxa.OrigemCodi ',
  '  AND pqw.OriCtrl=bxa.OrigemCtrl ',
  '  AND pqw.OriTipo=bxa.Tipo ',
  'GROUP BY pqw.DstCtrl, pqw.DstCodi, pqw.DstTipo ',
  '; ',
  // ini 2022-03-01
(*
  'SELECT Bxa.Insumo, pq_.Nome,  ',
  'pqe.NF, pqe.NF_RP, pqe.NF_CC, pqe.IQ, pqe.DataE,  ',
  'bxa.Peso, bxa.Valor, bxa.Valor / bxa.Peso CustoBXA, ',
  'pqi.ValorItem / pqi.PesoVL CustoINN ',
  'FROM _PQS_NF_EMITGRU_PQX_BXA_2 bxa ',
  'LEFT JOIN ' + TMeuDB + '.pqeits pqi ON pqi.Controle=bxa.DstCtrl ',
  'LEFT JOIN ' + TMeuDB + '.pqe pqe ON pqe.Codigo=bxa.DstCodi ',
  'LEFT JOIN ' + TMeuDB + '.pq  pq_ ON pq_.Codigo=bxa.Insumo ',
  'ORDER BY Nome ',
*)
  //
  'SELECT bxa.Insumo, pq_.Nome,  ',
  'pqe.NF, pqe.NF_RP, pqe.NF_CC, pqe.IQ, pqe.DataE,  ',
  'bxa.Peso, bxa.Valor, bxa.Valor / bxa.Peso CustoBXA, ',
  //'GROUP_CONCAT(xLote SEPARATOR ", ") GROUP_CONCATxLote, ',
  'pqi.xLote,',
  'pqi.ValorItem / pqi.PesoVL CustoINN ',
  'FROM _PQS_NF_EMITGRU_PQX_BXA_2 bxa ',
  'LEFT JOIN ' + TMeuDB + '.pqeits pqi ON pqi.Controle=bxa.DstCtrl ',
  'LEFT JOIN ' + TMeuDB + '.pqe pqe ON pqe.Codigo=bxa.DstCodi ',
  'LEFT JOIN ' + TMeuDB + '.pq  pq_ ON pq_.Codigo=bxa.Insumo ',
  'GROUP BY bxa.Insumo, pqe.NF_CC, pqi.xLote ',
  'ORDER BY Nome ',
  //

  // fim 2022-03-01
  '']);
  //Geral.MB_SQL(Self, QrInsumo2);
  ReopenCustoPQ(grcSemGrupo);
  //
  MyObjects.frxDefineDataSets(frxQUI_RECEI_014_00_C2, [
  DModG.frxDsDono,
  frxDsEmitGru,
  frxDsCustoPQ,
  frxDsInsumo3
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_014_00_C2, 'Insumos utilizados');
end;

procedure TFmEmitGru.Insumosusados1Click(Sender: TObject);
begin
  if (QrCliOrig.State = dsInactive) or (QrCliOrig.RecordCount = 0) then
  begin
    Geral.MB_Aviso('N�o h� dados a serem impressos!');
    Exit;
  end;
  //
  ReopenInsumos(grcSemGrupo);
  ReopenCustoPQ(grcSemGrupo);
  //
  MyObjects.frxDefineDataSets(frxQUI_RECEI_014_00_B, [
  DModG.frxDsDono,
  frxDsEmitGru,
  frxDsCustoPQ,
  frxDsInsumos
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_014_00_B, 'Insumos utilizados');
end;

procedure TFmEmitGru.ItsAltera1Click(Sender: TObject);
begin
  MostraFormEmitCab(stUpd);
end;

procedure TFmEmitGru.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmEmitGru.CriaDadosTemporarios();
var
  Campos: String;
  QtdReg1, QtdReg2, QtdReg3: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando tabela tempor�ria 1');
  F_Emit :=
    UnCreateBlueDerm.RecriaTempTableNovo(ntrttEmit, DmodG.QrUpdPID1, False);
  Campos := UMyMod.ObtemCamposDeTabelaIdentica(DModG.MyPID_DB, F_Emit, '', QtdReg1);
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DELETE FROM ' + F_emit + '; ',
  'INSERT INTO ' + F_emit,
  'SELECT ',
  Campos,
  'FROM ' + TMeuDB + '.emit ',
  'WHERE EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  '']);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando tabela tempor�ria 2');
  F_PQO :=
    UnCreateBlueDerm.RecriaTempTableNovo(ntrttPQO, DmodG.QrUpdPID1, False);
  Campos := UMyMod.ObtemCamposDeTabelaIdentica(DModG.MyPID_DB, F_PQO, '', QtdReg2);
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DELETE FROM ' + F_PQO + ';',
  'INSERT INTO ' + F_PQO,
  'SELECT  ',
  Campos,
  'FROM ' + TMeuDB + '.pqo',
  'WHERE EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  '']);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando tabela tempor�ria 3');
  F_PQX :=
    UnCreateBlueDerm.RecriaTempTableNovo(ntrttPQX, DmodG.QrUpdPID1, False);
  Campos := UMyMod.ObtemCamposDeTabelaIdentica(DModG.MyPID_DB, F_PQX, 'pqx.', QtdReg3);
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DELETE FROM ' + F_PQX + '; ',
  'INSERT INTO ' + F_PQX + ' ',
  'SELECT ',
  Campos,
  'FROM ' + TMeuDB + '.pqx pqx',
  'LEFT JOIN ' + F_emit + ' emi ON emi.Codigo=pqx.OrigemCodi',
  '  AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0110),
  'WHERE EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  ';',
  '']);
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + F_PQX + ' ',
  'SELECT ',
  Campos,
  'FROM ' + TMeuDB + '.pqx pqx',
  'LEFT JOIN ' + F_PQO + ' pqo ON pqo.Codigo=pqx.OrigemCodi',
  '  AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
  'WHERE EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  '']);
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmEmitGru.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmEmitGru.CustoseRendimentoscomCouroA1Click(Sender: TObject);
const
  PorCor = True;
begin
  Geral.MB_Info('Em desenvolvimento!');
  Exit;
  PreparaCustoERendimento(grcSemGrupo);
  //  prepara...
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _EMITGRU_PWE_CAB_; ',
  'CREATE TABLE _EMITGRU_PWE_CAB_ ',
  'SELECT MovimCod, Codigo, MovimID  ',
  'FROM ' + TMeuDB + '.vspwecab ',
  'WHERE EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _EMITGRU_PWE_VMI_; ',
  'CREATE TABLE _EMITGRU_PWE_VMI_ ',
  'SELECT vmi.*  ',
  'FROM _EMITGRU_PWE_CAB_ cab ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  '  ON vmi.MovimCod=cab.MovimCod ',
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _EMITGRU_ORI_NFE_VMI_; ',
  'CREATE TABLE _EMITGRU_ORI_NFE_VMI_ ',
  'SELECT * FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  'WHERE Controle IN (',
  '  SELECT SrcNivel2',
  '  FROM _EMITGRU_PWE_VMI_',
  '  WHERE MovimNiv=20',
  ')',
  ';',
  '']);
  // wet blue de origem
  UnDmkDAC_PF.AbreMySQLQuery0(QrOriVMI, DModG.MyPID_DB, [
  'SELECT ori.NFeNum, ',
  'CONCAT(gg1.Nome,  ',
  '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  '  NO_PRD_TAM_COR, vmi.* ',
  'FROM _EMITGRU_PWE_VMI_ vmi ',
  'LEFT JOIN _EMITGRU_ORI_NFE_VMI_ ori ',
  '  ON ori.Controle=vmi.SrcNivel2',
  'LEFT JOIN ' + TMeuDB + 'gragrux    ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN ' + TMeuDB + 'gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN ' + TMeuDB + 'gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN ' + TMeuDB + 'gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN ' + TMeuDB + 'gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN ' + TMeuDB + 'unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE vmi.MovimNiv=20',
  'ORDER BY vmi.Controle',
  '']);
  //
(* crust ou acabado de destino
procedure TUnVS_PF.ReopenVSPWEExtra(Qry: TmySQLQuery; Codigo, //Controle,
  TemIMEIMrt: Integer; MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //TemIMEIMrt := QrVSPWECab.FieldByName('TemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(MovimID)),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  'AND vmi.DstMovID=' + Geral.FF0(Integer(emidFinished)),
  'AND vmi.DstNivel1=' + Geral.FF0(Codigo),
  //'AND vmi.DstNivel2=' + Geral.FF0(Controle),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  '',
  'FROM _EMITGRU_PWE_VMI_ vmi ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  '',
  'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB + '.entidades  frn ON frn.Codigo=vmi.Terceiro',
  '',
  '',
  'WHERE vmi.MovimNiv=22',
  '',
  '',
  '',
  '',
  '',
  'ORDER BY NO_Pallet, Controle ',
  /
*)
  // Raspa Wet Blue
(*
  'SELECT vmi.* ',
  'FROM _emitgru_pwe_cab_ cab',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi',
  '  ON vmi.DstMovID=20 AND vmi.DstNivel1=cab.Codigo',
  'WHERE vmi.MovimID=23',
*)

  MyObjects.frxDefineDataSets(frxQUI_RECEI_014_00_E, [
  DModG.frxDsDono,
  frxDsEmitGru,
  frxDsEmit0,
  frxDsEmit1,
  frxDsPQO,
  frxDsRendim,
  frxDsCustoPQ,
  frxDsVSPWECab,
  frxDsVSPWEOriIMEI,
  frxDsVSPWEDst,
  frxDsVSPWESubPrd
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_014_00_D, 'Custos e rendimentos com couro');
end;

procedure TFmEmitGru.CustoseRendimentoscomCouroB1Click(Sender: TObject);
const
  PorCor = True;
begin
  PreparaCustoERendimento(grcSemGrupo);
  //
  MyObjects.frxDefineDataSets(frxQUI_RECEI_014_00_D, [
  DModG.frxDsDono,
  frxDsEmitGru,
  frxDsEmit0,
  frxDsEmit1,
  frxDsPQO,
  frxDsRendim,
  frxDsCustoPQ,
  frxDsVSPWECab,
  frxDsVSPWEOriIMEI,
  frxDsVSPWEDst,
  frxDsVSPWESubPrd
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_014_00_D, 'Custos e rendimentos com couro');
end;

procedure TFmEmitGru.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEmitGru.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Cadastro_Com_Itens_ITS', 'Controle', QrCliOrigControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrCliOrig,
      QrCliOrigControle, QrCliOrigControle.Value);
    ReopenCadastro_Com_Itens_ITS(Controle);
  end;
}
end;

procedure TFmEmitGru.Rebaixe1Click(Sender: TObject);
begin
  ImprimeRendimentoECustoAgrupado(grcRebaixe);
end;

procedure TFmEmitGru.Rebaixe2Click(Sender: TObject);
begin
  ImprimeInsumosUtilizadosAgrupados(grcRebaixe);
end;

procedure TFmEmitGru.RemovedoIMEC1Click(Sender: TObject);
var
  VSMovCod, Codigo, Tipo, I: Integer;
  Tabela: String;
  //
begin
  if MyObjects.FIC(DBGBaixaPQ.SelectedRows.Count = 0, nil,
  'Nenhum item foi selecionado!') then Exit;
  //
  if Geral.MB_Pergunta('Confirma a remo��o dos IME-C?') = ID_YES then
  begin
    VSMovCod := 0;
    with DBGBaixaPQ.DataSource.DataSet do
    for I := 0 to DBGBaixaPQ.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGBaixaPQ.SelectedRows.Items[i]));
      GotoBookmark(DBGBaixaPQ.SelectedRows.Items[i]);
      //
      if QrCabsVSMovCod.Value <> 0 then
      begin
        Tipo   := Trunc(QrCabsTipo.Value);
        Tabela := PQ_PF.ObtemNomeTabelaTipoMovPQ(Tipo);
        Codigo := QrCabsCodigo.Value;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
        'VSMovCod'], ['Codigo'], [VSMovCod], [Codigo], True) then
        begin
          DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrCabsVSMovCod.Value);
          VS_PF.AtualizaTotaisVSCurCab(QrCabsVSMovCod.Value);
        end;
      end;
    end;
    LocCod(QrEmitGruCodigo.Value, QrEmitGruCodigo.Value);
  end;
end;

procedure TFmEmitGru.ReopenCabs(Tipo, Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCabs, Dmod.MyDB, [
  'SELECT ' + Geral.FF0(VAR_FATID_0110) + '.000 Tipo, ',
  'Codigo, CAST(DataEmis + 0 AS DATETIME) Data, "Pesagem" TipoTXT, ',
  'VSMovCod, AreaM2, FLOOR(((AreaM2*10.76391)+0.125)*4)/4 AreaP2, ',
  'Qtde Pecas, SbtRendEsper ',
  'FROM emit emi',
  'LEFT JOIN pqx pqx ON emi.Codigo=pqx.OrigemCodi',
  'WHERE EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  'AND pqx.CliOrig=' + Geral.FF0(QrCliOrigCliOrig.Value),
  '',
  'UNION',
  '',
  'SELECT ' + Geral.FF0(VAR_FATID_0190) + '.000 Tipo, ',
  'Codigo, CAST(DataB + 0 AS DATETIME) Data, "Baixa Manual" TipoTXT, ',
  'VSMovCod, 0.00 AreaM2, 0.00 AreaP2, 0.000 Pecas, 0.00 SbtRendEsper ',
  'FROM pqo pqo ',
  'LEFT JOIN pqx pqx ON pqo.Codigo=pqx.OrigemCodi',
  'WHERE EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  'AND pqx.CliOrig=' + Geral.FF0(QrCliOrigCliOrig.Value),
  '',
  'ORDER BY Data DESC, Tipo DESC, Codigo DESC',
  '']);
  //
  //Geral.MB_SQL(Self, QrCabs);
  QrCabs.Locate('Tipo;Codigo', VarArrayOf([Tipo, Codigo]), []);
end;

procedure TFmEmitGru.ReopenCliOrig(CliOrig: Integer);
begin

  UnDmkDAC_PF.AbreMySQLQuery0(QrCliOrig, Dmod.MyDB, [
  'SELECT DISTINCT pqx.CliOrig,  ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  'FROM pqx pqx ',
  'LEFT JOIN pqo pqo ON pqo.Codigo=pqx.OrigemCodi ',
  'LEFT JOIN entidades ent ON ent.Codigo=pqx.CliOrig ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
  'AND pqo.EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  ' ',
  'UNION ',
  ' ',
  'SELECT DISTINCT pqx.CliOrig, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  'FROM pqx pqx ',
  'LEFT JOIN emit emi ON emi.Codigo=pqx.OrigemCodi ',
  'LEFT JOIN entidades ent ON ent.Codigo=pqx.CliOrig ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0110),
  'AND emi.EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  ' ',
  '']);
end;

procedure TFmEmitGru.ReopenCustoPQ(Agrupar: TGrupoRendiCusto);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCustoPQ, Dmod.MyDB, [
  'SELECT -SUM(pqx.Valor) Valor ',
  'FROM pqx pqx',
  'LEFT JOIN emit emi ON emi.Codigo=pqx.OrigemCodi',
  '  AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0110),
  'LEFT JOIN pqo pqo ON pqo.Codigo=pqx.OrigemCodi',
  '  AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
  //'WHERE pqx.CliOrig=' + Geral.FF0(QrCliOrigCliOrig.Value),
  //'AND (IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', emi.EmitGru, ',
  'WHERE (IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', emi.EmitGru, ',
  '  IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0190) + ', pqo.EmitGru, ',
  // "1=2" => Evitar erro de SQL!!!
  '  1=2))) =' + Geral.FF0(QrEmitGruCodigo.Value),
  '']);
  //
  //Geral.MB_Teste(QrCustoPQ.SQL.Text);
end;

procedure TFmEmitGru.ReopenEmitSbtQtd(Controle: Integer);
var
  ATT_VSBastidao, ATT_VSUmidade: String;
begin
  ATT_VSBastidao := dmkPF.ArrayToTexto('sbq.VSBastidao', 'NO_VSBastidao', pvPos, True,
        sVSBastidao);
  ATT_VSUmidade := dmkPF.ArrayToTexto('sbq.VSUmidade', 'NO_VSUmidade', pvPos, True,
        sVSUmidade);
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmitSbtQtd, Dmod.MyDB, [
  'SELECT sbc.Nome NO_EmitSbtCad, ',
  ATT_VSBastidao,
  ATT_VSUmidade,
  'sbq.* ',
  'FROM emitsbtqtd sbq ',
  'LEFT JOIN emitsbtcad sbc ON sbc.Codigo=sbq.EmitSbtCad ',
  'WHERE sbq.Codigo=' + Geral.FF0(QrEmitGruCodigo.Value),
  'ORDER BY Controle ',
  ' ']);
end;

procedure TFmEmitGru.ReopenEmitUsoSbt(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmitUsoSbt, Dmod.MyDB, [
  'SELECT cad.Nome NO_EmitSbtCad, sbt.*  ',
  'FROM emitusosbt sbt ',
  'LEFT JOIN emitsbtqtd qtd ON qtd.Controle=sbt.IDEmitSbtQtd ',
  'LEFT JOIN emitsbtcad cad ON cad.Codigo=qtd.EmitSbtCad ',
  ' ',
  'WHERE sbt.Codigo=' + Geral.FF0(QrCabsCodigo.Value),
  '']);
  if Controle <> 0 then
    QrEmitUsoSbt.Locate('Controle', Controle, []);
end;

procedure TFmEmitGru.ReopenEmitX(Qry: TmySQLQuery; Retrabalho: Integer;
  Agrupar: TGrupoRendiCusto);
var
  SQL_Cor, SQL_Espessura: String;
begin
  SQL_Cor := '';
  SQL_Espessura := '';
  case Agrupar of
    //grcIndef: ;//
    grcSemGrupo: ; // nada!
    grcCor: SQL_Cor := 'AND emi.GraCorCad=' + Geral.FF0(QrEmitCorGraCorCad.Value);
    grcRebaixe: SQL_Espessura := 'AND emi.SemiCodEspReb=' + Geral.FF0(QrEmitCorSemiCodEspReb.Value);
    grcCorERebaixe:
    begin
      SQL_Cor := 'AND emi.GraCorCad=' + Geral.FF0(QrEmitCorGraCorCad.Value);
      SQL_Espessura := 'AND emi.SemiCodEspReb=' + Geral.FF0(QrEmitCorSemiCodEspReb.Value);
    end;
    else Geral.MB_Erro('Agrupamento n�o definido em "ReopenEmitX()"');
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT ROUND(emi.AreaM2 * 10000 / 929.0304 * 4) / 4 AREAP2, ',
  'ROUND(emi.SemiAreaM2 * 10000 / 929.0304 * 4) / 4 SEMIAREAP2, ',
  'emi.* ',
  'FROM emit emi ',
  'WHERE emi.EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  'AND emi.ClienteI=' + Geral.FF0(QrCliOrigCliOrig.Value),
  'AND emi.Retrabalho=' + Geral.FF0(Retrabalho),
  SQL_Cor,
  SQL_Espessura,
  '']);
  //Geral.MB_SQL(Self, Qry);
end;

procedure TFmEmitGru.ReopenInsumos(Agrupar: TGrupoRendiCusto);
var
  SQL_Cor_PQX, SQL_Cor_PQO, SQL_Espessura_PQX, SQL_Espessura_PQO,
  SQL_GrupoQuimico: String;
begin
  SQL_Cor_PQX       := '';
  SQL_Cor_PQO       := '';
  SQL_Espessura_PQX := '';
  SQL_Espessura_PQO := '';
  SQL_GrupoQuimico  := '';
  //
  case Agrupar of
    //grcIndef: ;//
    grcSemGrupo: ; // nada!
(*
    grcCor:
    begin
      SQL_Cor_PQX := 'AND emi.GraCorCad=' + Geral.FF0(QrGruCorGraCorCad.Value);
      SQL_Cor_PQO := 'AND pqo.GraCorCad=' + Geral.FF0(QrGruCorGraCorCad.Value);
    end;
    grcRebaixe:
    begin
      SQL_Espessura_PQX := 'AND emi.SemiCodEspReb=' + Geral.FF0(QrGruCorSemiCodEspReb.Value);
      SQL_Espessura_PQO := 'AND pqo.SemiCodEspReb=' + Geral.FF0(QrGruCorSemiCodEspReb.Value);
    end;
    grcCorERebaixe:
    begin
      SQL_Cor_PQX := 'AND emi.GraCorCad=' + Geral.FF0(QrGruCorGraCorCad.Value);
      SQL_Cor_PQO := 'AND pqo.GraCorCad=' + Geral.FF0(QrGruCorGraCorCad.Value);
      SQL_Espessura_PQX := 'AND emi.SemiCodEspReb=' + Geral.FF0(QrGruCorSemiCodEspReb.Value);
      SQL_Espessura_PQO := 'AND pqo.SemiCodEspReb=' + Geral.FF0(QrGruCorSemiCodEspReb.Value);
    end;
*)
    else Geral.MB_Erro('Agrupamento n�o definido em "ReopenEmitX(1)"');
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrInsumos, Dmod.MyDB, [
  'SELECT pqx.Insumo, pq_.Nome, SUM(-pqx.Peso) Peso, SUM(-pqx.Valor) Valor, ',
  'IF(SUM(-pqx.Peso) > 0, SUM(-pqx.Valor) / SUM(-pqx.Peso), 0) CustoUni  ',
  'FROM pqx pqx ',
  'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo ',
  //'LEFT JOIN gruposquimicos gqu ON gqu.Codigo=pq_.GrupoQuimico ',
  'LEFT JOIN emit emi ON emi.Codigo=pqx.OrigemCodi ',
  '  AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0110),
  ' ',
  SQL_Cor_PQX,
  SQL_Espessura_PQX,
  ' ',
  'LEFT JOIN pqo pqo ON pqo.Codigo=pqx.OrigemCodi ',
  '  AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
  ' ',
  SQL_Cor_PQO,
  SQL_Espessura_PQO,
  ' ',
  'WHERE pqx.CliOrig=' + Geral.FF0(QrCliOrigCliOrig.Value),
  'AND (IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0110) + ', emi.EmitGru, ',
  '  IF(pqx.Tipo=' + Geral.FF0(VAR_FATID_0190) + ', pqo.EmitGru, ',
  // "1=2" => Evitar erro de SQL!!!
  '  1=2))) =' + Geral.FF0(QrEmitGruCodigo.Value),
  SQL_GrupoQuimico,
  'GROUP BY pqx.Insumo ',
  'ORDER BY pq_.Nome ',
  '']);
  //Geral.MB_Teste(QrInsumos.SQL.Text);
end;

procedure TFmEmitGru.ReopenPQO(Agrupar: TGrupoRendiCusto);
var
  SQL_Cor, SQL_Espessura: String;
begin
  SQL_Cor := '';
  SQL_Espessura := '';
  case Agrupar of
    //grcIndef: ;//
    grcSemGrupo: ;
    grcCor: SQL_Cor := 'AND pqo.GraCorCad=' + Geral.FF0(QrEmitCorGraCorCad.Value);
    grcRebaixe: SQL_Espessura := 'AND pqo.SemiCodEspReb=' + Geral.FF0(QrEmitCorSemiCodEspReb.Value);
    grcCorERebaixe:
    begin
      SQL_Cor := 'AND pqo.GraCorCad=' + Geral.FF0(QrEmitCorGraCorCad.Value);
      SQL_Espessura := 'AND pqo.SemiCodEspReb=' + Geral.FF0(QrEmitCorSemiCodEspReb.Value);
    end;
    else Geral.MB_Erro('Agrupamento n�o definido em "ReopenEmitX()"');
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQO, Dmod.MyDB, [
  'SELECT pqo.Codigo, pqo.DataB, pqo.Nome, ',
  'pqo.GraCorCad, gcc.Nome NoGraCorCad, ',
  '-SUM(pqx.Peso) Peso, -SUM(pqx.Valor) Valor ',
  'FROM pqx pqx  ',
  'LEFT JOIN pqo pqo ON pqo.Codigo=pqx.OrigemCodi ',
  'LEFT JOIN gracorcad gcc ON gcc.Codigo=pqo.GraCorCad ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
  'AND pqo.EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  //'AND pqx.CliOrig=' + Geral.FF0(QrCliOrigCliOrig.Value),
  SQL_Cor,
  SQL_Espessura,
  'GROUP BY pqo.Codigo ',
  'ORDER BY pqo.DataB ',
  '']);
end;

procedure TFmEmitGru.ReopenVSPWECab(MovimCod: Integer);
var
  ATT_MovimID: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto(Geral.FF0(Integer(emidEmProcWE)), 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPWECab, Dmod.MyDB, [
  'SELECT ' + ATT_MovimID,
  'CONCAT(gg1.Nome,  ',
  '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  '  NO_PRD_TAM_COR, ',
  'vcc.Nome NO_VSCOPCab,  ',
  'IF(PecasMan<>0, PecasMan, -PecasSrc) PecasINI, ',
  'IF(AreaManM2<>0, AreaManM2, -AreaSrcM2) AreaINIM2, ',
  'IF(AreaManP2<>0, AreaManP2, -AreaSrcP2) AreaINIP2, ',
  'IF(PesoKgMan<>0, PesoKgMan, -PesoKgSrc) PesoKgINI, ',
  'ope.Nome NO_Operacoes, voc.*, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA ',
  'FROM vspwecab voc ',
  'LEFT JOIN entidades  ent ON ent.Codigo=voc.Empresa ',
  'LEFT JOIN operacoes  ope ON ope.Codigo=voc.Operacoes ',
  'LEFT JOIN vscopcab   vcc ON vcc.Codigo=voc.VSCOPCab ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=voc.GGXDst ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE voc.EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  '']);
  if MovimCod <> 0 then
    QrVSPWECab.Locate('MovimCod', MovimCod, []);
end;

procedure TFmEmitGru.DBGBaixaPQDblClick(Sender: TObject);
begin
  UnPQx.GerenciaBaixaPQ(Trunc(QrCabsTipo.Value), QrCabsCodigo.Value);
end;

procedure TFmEmitGru.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEmitGru.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEmitGru.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEmitGru.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEmitGru.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEmitGru.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmitGru.BtEmitUsoSbtClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMEmitUsoSbt, BtEmitUsoSbt);
end;

procedure TFmEmitGru.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEmitGruCodigo.Value;
  Close;
end;

procedure TFmEmitGru.ItsInclui1Click(Sender: TObject);
begin
  MostraFormEmitCab(stUpd);
end;

procedure TFmEmitGru.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEmitGru, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'emitgru');
end;

procedure TFmEmitGru.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  Nome   := EdNome.Text;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('emitgru', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'emitgru', False, [
  'Nome'], ['Codigo'], [Nome], [Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmEmitGru.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'emitgru', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'emitgru', 'Codigo');
end;

procedure TFmEmitGru.BtEmitSbtQtdClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMEmitSbtQtd, BtEmitSbtQtd);
end;

procedure TFmEmitGru.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmEmitGru.BtPesagemClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesagem, BtPesagem);
end;

procedure TFmEmitGru.AlteraSubstratodeRendimentoselecionado1Click(
  Sender: TObject);
begin
  MostraFormEmitSbtQtd(stUpd);
end;

procedure TFmEmitGru.Alterausodesubstratoderendimentoselecionado1Click(
  Sender: TObject);
begin
  MostraFormEmitUsoSbt(stUpd);
end;

procedure TFmEmitGru.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmEmitGru.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  //DGDados.Align := alClient;
  //
  PCPWEOri.ActivePageIndex := 0;
  PCPWEDst.ActivePageIndex := 0;
  PCPWEDst.Align := alClient;
  //
  PageControl1.ActivePageIndex := 0;
  //
  CriaOForm;
  FSeq := 0;
end;

procedure TFmEmitGru.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEmitGruCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEmitGru.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmEmitGru.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEmitGru.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEmitGruCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEmitGru.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEmitGru.QrCabsAfterScroll(DataSet: TDataSet);
begin
  ReopenEmitUsoSbt(0);
end;

procedure TFmEmitGru.QrCabsBeforeClose(DataSet: TDataSet);
begin
  QrEmitUsoSbt.Close;
end;

procedure TFmEmitGru.QrCliOrigAfterScroll(DataSet: TDataSet);
begin
  ReopenCabs(0,0);
end;

procedure TFmEmitGru.QrCliOrigBeforeClose(DataSet: TDataSet);
begin
  QrCabs.Close;
end;

procedure TFmEmitGru.QrCusCor_AfterScroll(DataSet: TDataSet);
begin
  //ReopenGruCor;//(FAgrupar);
end;

procedure TFmEmitGru.QrEmit0CalcFields(DataSet: TDataSet);
begin
  if (QrEmit0SbtRendEsper.Value = 0) or (QrEmit0SemiRendim.Value = 0)  then
    QrEmit0PercDePercRend.Value := 0
  else
    QrEmit0PercDePercRend.Value := (QrEmit0SemiRendim.Value -
      QrEmit0SbtRendEsper.Value) / QrEmit0SbtRendEsper.Value * 100
end;

procedure TFmEmitGru.QrEmitCorAfterScroll(DataSet: TDataSet);
const
  PorCor = True;
begin
  ReopenEmitX(QrEmit0, 0, FAgrupar);
  ReopenEmitX(QrEmit1, 1, FAgrupar);
  ReopenPQO(FAgrupar);
end;

procedure TFmEmitGru.QrEmitGruAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEmitGru.QrEmitGruAfterScroll(DataSet: TDataSet);
begin
  ReopenCliOrig(0);
  ReopenVSPWECab(0);
  ReopenEmitSbtQtd(0);
end;

procedure TFmEmitGru.Fechamento1Click(Sender: TObject);
const
  PorCor = True;
begin
  PreparaCustoERendimento(grcSemGrupo);
  //
  //MyObjects.frxDefineDataSets(frxQUI_RECEI_014_00_A, [
  MyObjects.frxDefineDataSets(frxQUI_RECEI_014_00_A1, [
  DModG.frxDsDono,
  frxDsEmitGru,
  frxDsEmit0,
  frxDsEmit1,
  frxDsPQO,
  frxDsRendim,
  frxDsCustoPQ,
  frxDsEmitSbtQtd
  ]);
  //MyObjects.frxMostra(frxQUI_RECEI_014_00_A, 'Custos e Rendimentos');
  MyObjects.frxMostra(frxQUI_RECEI_014_00_A1, 'Custos e Rendimentos');
end;

procedure TFmEmitGru.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrEmitGruCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmEmitGru.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEmitGruCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'emitgru', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEmitGru.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmitGru.frxQUI_RECEI_014_00_AGetValue(const VarName: string;
  var Value: Variant);
var
  Fator: Double;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', QrCliOrigNO_ENT.Value, QrCliOrigCliOrig.Value, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_FILTRO' then
    Value := dmkPF.ParValueCodTxt(
      'Grupo de emiss�es: ', QrEmitGruNome.Value, Null, 'TODAS')
(*
  else
  if VarName = 'VARF_PERIODO_1' then
    Value := dmkPF.PeriodoImp1(TP01DataIni.Date, TP01DataFim.Date,
    Ck01DataIni.Checked, Ck01DataFim.Checked, '', 'at�', '')
*)
  else
  if VarName ='VARF_CUSTO_TOTAL_USD' then
  begin
    if QrEmitGruUSD_BRL.Value > 0 then
    begin
      Value := QrCustoPQValor.Value / QrEmitGruUSD_BRL.Value
    end else
      Value := 0;
  end else
  if VarName ='VARF_REDIM_M2' then
  begin
    if QrRendimAreaM2.Value > 0 then
    begin
      if QrEmitGruAreaManM2.Value > 0 then
        Value := ((QrEmitGruAreaManM2.Value / QrRendimAreaM2.Value) - 1) * 100
      else
      if QrRendimSemiAreaM2.Value > 0 then
        Value := ((QrRendimSemiAreaM2.Value / QrRendimAreaM2.Value) - 1) * 100
      else
        Value := 0;
    end else
      Value := 0;
  end else
  if VarName ='VARF_REDIM_P2' then
  begin
    if QrRendimAreaP2.Value > 0 then
    begin
      if QrEmitGruAreaManP2.Value > 0 then
        Value := ((QrEmitGruAreaManP2.Value / QrRendimAreaP2.Value) - 1) * 100
      else
      if QrRendimSemiAreaP2.Value > 0 then
        Value := ((QrRendimSemiAreaP2.Value / QrRendimAreaP2.Value) - 1) * 100
      else
        Value := 0;
    end else
      Value := 0;
  end else
  if VarName ='VARF_CUSTO_BRL_WB_M2' then
  begin
    if QrRendimAreaM2.Value > 0 then
    begin
       Value := QrCustoPQValor.Value / QrRendimAreaM2.Value
    end else
      Value := 0;
  end else
  if VarName ='VARF_CUSTO_BRL_WB_P2' then
  begin
    if QrRendimAreaP2.Value > 0 then
    begin
       Value := QrCustoPQValor.Value / QrRendimAreaP2.Value
    end else
      Value := 0;
  end else
  if VarName ='VARF_CUSTO_USD_WB_M2' then
  begin
    if (QrRendimAreaM2.Value > 0) and
    (QrEmitGruUSD_BRL.Value > 0) then
    begin
       Value := QrCustoPQValor.Value / QrRendimAreaM2.Value / QrEmitGruUSD_BRL.Value
    end else
      Value := 0;
  end else
  if VarName ='VARF_CUSTO_USD_WB_P2' then
  begin
    if (QrRendimAreaP2.Value > 0) and
    (QrEmitGruUSD_BRL.Value > 0) then
    begin
       Value := QrCustoPQValor.Value / QrRendimAreaP2.Value / QrEmitGruUSD_BRL.Value
    end else
      Value := 0;
  end else
  if VarName ='VARF_CUSTO_BRL_SA_M2' then
  begin
    if QrEmitGruAreaManM2.Value > 0 then
       Value := QrCustoPQValor.Value / QrEmitGruAreaManM2.Value
    else
    if QrRendimSemiAreaM2.Value > 0 then
      Value := QrCustoPQValor.Value / QrRendimSemiAreaM2.Value
    else
      Value := 0;
  end else
  if VarName ='VARF_CUSTO_BRL_SA_P2' then
  begin
    if QrEmitGruAreaManP2.Value > 0 then
       Value := QrCustoPQValor.Value / QrEmitGruAreaManP2.Value
    else
    if QrRendimSemiAreaP2.Value > 0 then
      Value := QrCustoPQValor.Value / QrRendimSemiAreaP2.Value
    else
      Value := 0;
  end else
  if VarName ='VARF_CUSTO_USD_SA_M2' then
  begin
    if QrEmitGruUSD_BRL.Value > 0 then
    begin
      if QrEmitGruAreaManM2.Value > 0 then
         Value := QrCustoPQValor.Value / QrEmitGruAreaManM2.Value / QrEmitGruUSD_BRL.Value
      else
      if QrRendimSemiAreaM2.Value > 0 then
        Value := QrCustoPQValor.Value / QrRendimSemiAreaM2.Value / QrEmitGruUSD_BRL.Value
      else
        Value := 0;
    end else
      Value := 0;
  end else
  if VarName ='VARF_CUSTO_USD_SA_P2' then
  begin
    if QrEmitGruUSD_BRL.Value > 0 then
    begin
      if QrEmitGruAreaManP2.Value > 0 then
         Value := QrCustoPQValor.Value / QrEmitGruAreaManP2.Value / QrEmitGruUSD_BRL.Value
      else
      if QrRendimSemiAreaP2.Value > 0 then
        Value := QrCustoPQValor.Value / QrRendimSemiAreaP2.Value / QrEmitGruUSD_BRL.Value
      else
        Value := 0;
    end else
      Value := 0;
  end else
end;

procedure TFmEmitGru.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEmitGru, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'emitgru');
end;

procedure TFmEmitGru.Cor1Click(Sender: TObject);
begin
  ImprimeRendimentoECustoAgrupado(grcCor);
end;

procedure TFmEmitGru.Cor2Click(Sender: TObject);
begin
  ImprimeInsumosUtilizadosAgrupados(grcCor);
end;

procedure TFmEmitGru.CoreRebaixe1Click(Sender: TObject);
begin
  ImprimeRendimentoECustoAgrupado(grcCorERebaixe);
end;

procedure TFmEmitGru.CoreRebaixe2Click(Sender: TObject);
begin
  ImprimeInsumosUtilizadosAgrupados(grcCorERebaixe);
end;

procedure TFmEmitGru.QrEmitGruBeforeClose(
  DataSet: TDataSet);
begin
  QrCliOrig.Close;
  QrVSPWECab.Close;
  QrEmitSbtQtd.Close;
end;

procedure TFmEmitGru.QrEmitGruBeforeOpen(DataSet: TDataSet);
begin
  QrEmitGruCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEmitGru.QrEmitGruCalcFields(DataSet: TDataSet);
begin
  QrEmitGruENCERRADO_TXT.Value := Geral.FDT(QrEmitGruEncerrado.Value, 0);
end;

procedure TFmEmitGru.QrGruCor_AfterScroll(DataSet: TDataSet);
begin
  ReopenInsumos(FAgrupar);
end;

procedure TFmEmitGru.QrVSPWECabAfterScroll(DataSet: TDataSet);
begin
  (*VS_PF.ReopenVSOpePrcAtu(QrVSPWEAtu, QrVSPWECabMovimCod.Value, 0,
  QrVSPWECabTemIMEIMrt.Value, eminEmWEndInn);*)
  VS_PF.ReopenVSXxxOris(QrVSPWECab, QrVSPWEOriIMEI, QrVSPWEOriPallet, 0, 0,
  eminSorcWEnd);
  //ReopenVSPWEDesclDst(0, 0);
  VS_PF.ReopenVSOpePrcDst(QrVSPWEDst, QrVSPWECabMovimCod.Value, 0,
  QrVSPWECabTemIMEIMrt.Value, TEstqMovimNiv.eminDestWEnd);
  (*VS_PF.ReopenVSOpePrcForcados(QrForcados, 0, QrVSPWECabCodigo.Value,
    QrVSPWEAtuControle.Value, QrVSPWECabTemIMEIMrt.Value, emidEmProcWE);
  ReopenEmit();
  PQ_PF.ReopenPQOVS(QrPQO, QrVSPWECabMovimCod.Value, 0);*)
  VS_PF.ReopenVSXxxExtra(QrVSPWEDesclDst, QrVSPWECabCodigo.Value,
    (*QrVSPWEAtuControle.Value,*) QrVSPWECabTemIMEIMrt.Value,
    emidDesclasse, emidFinished, eminDestClass);
  //Geral.MB_SQL(Self, QrVSPWEDesclDst);
  VS_PF.ReopenVSXxxExtra(QrVSPWESubPrd, QrVSPWECabCodigo.Value,
    (*QrVSPWEAtuControle.Value,*) QrVSPWECabTemIMEIMrt.Value,
    emidGeraSubProd, emidFinished, eminSemNiv);
end;

procedure TFmEmitGru.QrVSPWECabBeforeClose(DataSet: TDataSet);
begin
  //QrVSPWEAtu.Close;
  QrVSPWEOriIMEI.Close;
  QrVSPWEOriPallet.Close;
  QrVSPWEDst.Close;
  QrVSPWESubPrd.Close;
  //QrForcados.Close;
  //QrEmit.Close;
  //QrPQO.Close;
end;

end.

