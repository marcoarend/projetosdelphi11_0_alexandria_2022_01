object FmPQChange: TFmPQChange
  Left = 433
  Top = 212
  Caption = 'QUI-RECEI-007 :: Aviso de Baixa Cancelada!'
  ClientHeight = 585
  ClientWidth = 780
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 90
    Width = 780
    Height = 369
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 0
      Width = 780
      Height = 369
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      DataSource = DsBaixa
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = DBGrid1DrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'NomePQ'
          Title.Caption = 'Produto qu'#237'mico'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso_PQ'
          Title.Caption = 'Peso'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI_ORIG'
          Title.Caption = 'Cliente interno'
          Width = 272
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PESOFUTURO'
          Title.Caption = 'Saldo futuro'
          Width = 77
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 780
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 721
      Top = 0
      Width = 59
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 662
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 380
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Aviso de Baixa Cancelada!'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 380
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Aviso de Baixa Cancelada!'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 380
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Aviso de Baixa Cancelada!'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 47
    Width = 780
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 776
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBEmpresta: TGroupBox
    Left = 0
    Top = 522
    Width = 780
    Height = 63
    Align = alBottom
    TabOrder = 3
    Visible = False
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 776
      Height = 46
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label36: TLabel
        Left = 7
        Top = 0
        Width = 70
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente interno:'
      end
      object PnSaiDesis: TPanel
        Left = 640
        Top = 0
        Width = 136
        Height = 46
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn4: TBitBtn
          Tag = 15
          Left = 7
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn4Click
        end
      end
      object EdCI: TdmkEditCB
        Left = 7
        Top = 17
        Width = 56
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCI
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCI: TdmkDBLookupComboBox
        Left = 65
        Top = 17
        Width = 440
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMECLI'
        ListSource = DsEntiCli2
        TabOrder = 2
        dmkEditCB = EdCI
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object BtConfirmaE: TBitBtn
        Tag = 14
        Left = 511
        Top = 5
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtConfirmaEClick
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 459
    Width = 780
    Height = 63
    Align = alBottom
    TabOrder = 4
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 776
      Height = 46
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 599
        Top = 0
        Width = 177
        Height = 46
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 15
          Left = 15
          Top = 0
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtBaixa: TBitBtn
        Tag = 14
        Left = 25
        Top = 0
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Baixa'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtBaixaClick
      end
      object BitBtn1: TBitBtn
        Tag = 5
        Left = 172
        Top = 0
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Relat'#243'rio'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BitBtn1Click
      end
      object BtEmpresta: TBitBtn
        Tag = 47
        Left = 320
        Top = 0
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Empresta'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtEmprestaClick
      end
      object BitBtn3: TBitBtn
        Left = 468
        Top = 0
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Intera'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
      end
    end
  end
  object QrBaixa: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrBaixaAfterOpen
    SQL.Strings = (
      'SELECT CASE WHEN ori.Tipo=0 THEN ori.RazaoSocial'
      'ELSE ori.Nome END NOMECLI_ORIG, ems.* '
      'FROM emitits ems'
      'LEFT JOIN entidades ori ON ori.Codigo=ems.Cli_Orig'
      'WHERE ems.Ativo=2'
      'AND ems.Codigo=:P0')
    Left = 136
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBaixaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBaixaControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBaixaNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Size = 50
    end
    object QrBaixaAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrBaixaPQCI: TIntegerField
      FieldName = 'PQCI'
      Required = True
    end
    object QrBaixaCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
      Required = True
    end
    object QrBaixaMoedaPadrao: TIntegerField
      FieldName = 'MoedaPadrao'
      Required = True
    end
    object QrBaixaNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrBaixaPorcent: TFloatField
      FieldName = 'Porcent'
      Required = True
    end
    object QrBaixaProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrBaixaTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrBaixaTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrBaixapH: TFloatField
      FieldName = 'pH'
    end
    object QrBaixaBe: TFloatField
      FieldName = 'Be'
    end
    object QrBaixaObs: TWideStringField
      FieldName = 'Obs'
    end
    object QrBaixaCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrBaixaGraus: TFloatField
      FieldName = 'Graus'
    end
    object QrBaixaBoca: TWideStringField
      FieldName = 'Boca'
      Size = 1
    end
    object QrBaixaProcesso: TWideStringField
      FieldName = 'Processo'
      Size = 30
    end
    object QrBaixaObsProces: TWideStringField
      FieldName = 'ObsProces'
      Size = 120
    end
    object QrBaixaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBaixaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBaixaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBaixaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBaixaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBaixaOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrBaixaPeso_PQ: TFloatField
      FieldName = 'Peso_PQ'
      Required = True
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrBaixaProdutoCI: TIntegerField
      FieldName = 'ProdutoCI'
      Required = True
    end
    object QrBaixaNOMECLI_ORIG: TWideStringField
      FieldName = 'NOMECLI_ORIG'
      Size = 100
    end
    object QrBaixaCli_Orig: TIntegerField
      FieldName = 'Cli_Orig'
      Required = True
    end
    object QrBaixaCli_Dest: TIntegerField
      FieldName = 'Cli_Dest'
      Required = True
    end
    object QrBaixaPESOFUTURO: TFloatField
      FieldKind = fkLookup
      FieldName = 'PESOFUTURO'
      LookupDataSet = QrPreuso
      LookupKeyFields = 'ProdutoCI'
      LookupResultField = 'PESOFUTURO'
      KeyFields = 'ProdutoCI'
      DisplayFormat = '#,###,###,##0.000'
      Lookup = True
    end
    object QrBaixaDiluicao: TFloatField
      FieldName = 'Diluicao'
    end
    object QrBaixaMinimo: TFloatField
      FieldName = 'Minimo'
    end
    object QrBaixaMaximo: TFloatField
      FieldName = 'Maximo'
    end
  end
  object DsBaixa: TDataSource
    DataSet = QrBaixa
    Left = 136
    Top = 196
  end
  object QrPreuso: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ems.Peso_PQ) Peso_PQ, ems.Produto, ems.PQCI, '
      'ems.ProdutoCI, ems.NomePQ, pqc.Peso, ems.Cli_Orig,'
      'pqc.Peso-SUM(ems.Peso_PQ) PESOFUTURO, CASE WHEN cli.Tipo=0'
      'THEN cli.RazaoSocial ELSE cli.Nome END NOMECLI_ORIG'
      'FROM emitits ems'
      'LEFT JOIN pqcli     pqc ON pqc.Controle=ems.ProdutoCI'
      'LEFT JOIN entidades cli ON cli.Codigo=ems.Cli_Orig'
      'WHERE ems.Ativo=2'
      'AND ems.Codigo=:P0'
      'GROUP BY ems.Produto, Cli_Orig')
    Left = 72
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPreusoPeso_PQ: TFloatField
      FieldName = 'Peso_PQ'
    end
    object QrPreusoProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrPreusoPQCI: TIntegerField
      FieldName = 'PQCI'
      Required = True
    end
    object QrPreusoProdutoCI: TIntegerField
      FieldName = 'ProdutoCI'
      Required = True
    end
    object QrPreusoNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Size = 50
    end
    object QrPreusoPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPreusoPESOFUTURO: TFloatField
      FieldName = 'PESOFUTURO'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrPreusoCli_Orig: TIntegerField
      FieldName = 'Cli_Orig'
      Required = True
    end
    object QrPreusoNOMECLI_ORIG: TWideStringField
      FieldName = 'NOMECLI_ORIG'
      Size = 100
    end
  end
  object QrEntiCli2: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEntiCli2AfterOpen
    SQL.Strings = (
      'SELECT pqc.Peso, pqc.Valor, ent.Codigo, '
      'pqc.Controle PRODUTOCI,'
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial '
      'ELSE ent.Nome END NOMECLI'
      'FROM entidades ent'
      'LEFT JOIN pqcli pqc ON pqc.CI=ent.Codigo'
      'WHERE pqc.PQ=:P0'
      'AND pqc.Peso>=:P1'
      'ORDER BY NOMECLI'
      '')
    Left = 16
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEntiCli2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiCli2NOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrEntiCli2Peso: TFloatField
      FieldName = 'Peso'
    end
    object QrEntiCli2Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrEntiCli2PRODUTOCI: TIntegerField
      FieldName = 'PRODUTOCI'
    end
  end
  object DsEntiCli2: TDataSource
    DataSet = QrEntiCli2
    Left = 140
    Top = 144
  end
  object PMEmpresta: TPopupMenu
    Left = 304
    Top = 308
    object Insumoatual1: TMenuItem
      Caption = '&Insumo atual'
      OnClick = Insumoatual1Click
    end
    object Todospendentes1: TMenuItem
      Caption = '&Todos pendentes'
      OnClick = Todospendentes1Click
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEntiCli2AfterOpen
    SQL.Strings = (
      'SELECT pqc.Peso, pqc.Valor, pqc.Controle PRODUTOCI, pqc.CI'
      'FROM pqcli pqc '
      'WHERE pqc.PQ=:P0'
      'AND pqc.Peso>=:P1'
      'AND pqc.CI=:P2')
    Left = 72
    Top = 188
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPesqPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrPesqValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrPesqPRODUTOCI: TIntegerField
      FieldName = 'PRODUTOCI'
      Required = True
    end
    object QrPesqCI: TIntegerField
      FieldName = 'CI'
    end
  end
end
