object FmMPVLoc: TFmMPVLoc
  Left = 246
  Top = 224
  Caption = 'VEN-COURO-103 :: Localiza Pedido por Cliente'
  ClientHeight = 507
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 351
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label10: TLabel
        Left = 20
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object EdCliente: TdmkEditCB
        Left = 20
        Top = 20
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdClienteRedefinido
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 76
        Top = 20
        Width = 341
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientes
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPedidCli: TEdit
        Left = 424
        Top = 20
        Width = 305
        Height = 21
        TabOrder = 2
        Text = '%%'
        OnChange = EdPedidCliChange
      end
      object CkPedidCli: TCheckBox
        Left = 424
        Top = 0
        Width = 117
        Height = 17
        Caption = 'Pedido do cliente:'
        TabOrder = 3
        OnClick = CkPedidCliClick
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 48
      Width = 1008
      Height = 303
      Align = alClient
      TabOrder = 1
      object Splitter1: TSplitter
        Left = 1
        Top = 121
        Width = 1006
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 1012
      end
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 1006
        Height = 120
        Align = alTop
        DataSource = DsMPV
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Faturam.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataF'
            Title.Caption = 'Data fatura'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLIENTE'
            Title.Caption = 'Cliente'
            Width = 147
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMETRANSP'
            Title.Caption = 'Transportadora'
            Width = 129
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEVENDEDOR'
            Title.Caption = 'Vendedor'
            Width = 121
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Caption = 'Quantidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Title.Caption = 'Sub-total'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DescoExtra'
            Title.Caption = 'Desc. Extra'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TOTAL'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Volumes'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ComissV_Per'
            Title.Caption = 'ComissV %'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ComissV_Val'
            Title.Caption = 'ComissV $'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Obs'
            Title.Caption = 'Observa'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cliente'
            Visible = True
          end>
      end
      object DBGrid4: TDBGrid
        Left = 1
        Top = 124
        Width = 1006
        Height = 178
        Align = alClient
        DataSource = DsMPVIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NOMEMP'
            Title.Caption = 'Artigo'
            Width = 113
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Texto'
            Title.Caption = 'Descri'#231#227'o'
            Width = 271
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CorTxt'
            Title.Caption = 'Cor'
            Width = 139
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Classe'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EspesTxt'
            Title.Caption = 'Espessura'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Caption = 'Quantidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Preco'
            Title.Caption = 'Pre'#231'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Desco'
            Title.Caption = 'Desconto'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Title.Caption = 'Total'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'OS'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pedido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PedidCli'
            Title.Caption = 'Pedido do cliente'
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 330
        Height = 32
        Caption = 'Localiza Pedido por Cliente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 330
        Height = 32
        Caption = 'Localiza Pedido por Cliente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 330
        Height = 32
        Caption = 'Localiza Pedido por Cliente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 399
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 443
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 6
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 197
    Top = 261
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 197
    Top = 313
  end
  object QrMPVIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ag.Nome NOMEMP, mvi.*, mpp.PedidCli  '
      'FROM mpvits mvi'
      'LEFT JOIN artigosgrupos ag ON ag.Codigo=mvi.MP'
      'LEFT JOIN mpp mpp ON mpp.Codigo=mvi.Pedido'
      'WHERE mvi.Codigo=:P0')
    Left = 120
    Top = 261
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPVItsNOMEMP: TWideStringField
      FieldName = 'NOMEMP'
      Required = True
      Size = 50
    end
    object QrMPVItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMPVItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMPVItsMP: TIntegerField
      FieldName = 'MP'
      Required = True
    end
    object QrMPVItsQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrMPVItsPreco: TFloatField
      FieldName = 'Preco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVItsDesco: TFloatField
      FieldName = 'Desco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVItsTexto: TWideStringField
      FieldName = 'Texto'
      Required = True
      Size = 50
    end
    object QrMPVItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMPVItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMPVItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMPVItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMPVItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMPVItsCorTxt: TWideStringField
      FieldName = 'CorTxt'
      Size = 30
    end
    object QrMPVItsEntrega: TDateField
      FieldName = 'Entrega'
      Required = True
    end
    object QrMPVItsPronto: TDateField
      FieldName = 'Pronto'
      Required = True
    end
    object QrMPVItsStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object QrMPVItsFluxo: TIntegerField
      FieldName = 'Fluxo'
      Required = True
    end
    object QrMPVItsClasse: TWideStringField
      FieldName = 'Classe'
      Size = 15
    end
    object QrMPVItsPedido: TIntegerField
      FieldName = 'Pedido'
      Required = True
    end
    object QrMPVItsEspesTxt: TWideStringField
      FieldName = 'EspesTxt'
      Size = 10
    end
    object QrMPVItsM2Pedido: TFloatField
      FieldName = 'M2Pedido'
      Required = True
    end
    object QrMPVItsPrecoPed: TFloatField
      FieldName = 'PrecoPed'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVItsValorPed: TFloatField
      FieldName = 'ValorPed'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPVItsPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrMPVItsUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrMPVItsObserv: TWideMemoField
      FieldName = 'Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPVItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrMPVItsDescricao: TWideMemoField
      FieldName = 'Descricao'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPVItsComplementacao: TWideMemoField
      FieldName = 'Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPVItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrMPVItsPedidCli: TWideStringField
      FieldName = 'PedidCli'
      Size = 60
    end
  end
  object DsMPVIts: TDataSource
    DataSet = QrMPVIts
    Left = 120
    Top = 313
  end
  object DsMPV: TDataSource
    DataSet = QrMPV
    Left = 44
    Top = 313
  end
  object QrMPV: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrMPVBeforeClose
    AfterScroll = QrMPVAfterScroll
    SQL.Strings = (
      'SELECT '
      'CASE WHEN ve.Tipo=0 THEN ve.RazaoSocial'
      'ELSE ve.Nome END NOMEVENDEDOR, '
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMECLIENTE, '
      'CASE WHEN tr.Tipo=0 THEN tr.RazaoSocial'
      'ELSE tr.Nome END NOMETRANSP,'
      'mpv.* '
      'FROM mpv mpv'
      'LEFT JOIN entidades cl ON cl.Codigo=mpv.Cliente'
      'LEFT JOIN entidades tr ON tr.Codigo=mpv.Transp'
      'LEFT JOIN entidades ve ON ve.Codigo=mpv.Vendedor'
      'WHERE mpv.Cliente=:P0'
      'ORDER BY DataF DESC')
    Left = 44
    Top = 261
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPVNOMEVENDEDOR: TWideStringField
      FieldName = 'NOMEVENDEDOR'
      Size = 100
    end
    object QrMPVNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrMPVNOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrMPVCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPVCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrMPVTransp: TIntegerField
      FieldName = 'Transp'
    end
    object QrMPVVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrMPVDataF: TDateField
      FieldName = 'DataF'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPVQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrMPVValor: TFloatField
      FieldName = 'Valor'
    end
    object QrMPVComissV_Per: TFloatField
      FieldName = 'ComissV_Per'
    end
    object QrMPVComissV_Val: TFloatField
      FieldName = 'ComissV_Val'
    end
    object QrMPVLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMPVDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMPVDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMPVUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMPVUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMPVDescoExtra: TFloatField
      FieldName = 'DescoExtra'
    end
    object QrMPVVolumes: TIntegerField
      FieldName = 'Volumes'
    end
    object QrMPVObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
  end
end
