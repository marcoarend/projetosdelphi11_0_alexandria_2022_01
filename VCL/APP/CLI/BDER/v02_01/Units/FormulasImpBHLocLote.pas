unit FormulasImpBHLocLote;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, Db, mySQLDbTables, Grids, DmkDAC_PF,
  DBGrids, DBCtrls, Menus, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkImage, UnDmkEnums, UnDmkProcFunc, AppListas;

type
  TFmFormulasImpBHLocLote = class(TForm)
    PainelDados: TPanel;
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrLotes: TmySQLQuery;
    DsLotes: TDataSource;
    DBGrid1: TDBGrid;
    Panel2: TPanel;
    QrMarcas: TmySQLQuery;
    DsMarcas: TDataSource;
    Label6: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    EdPesoF: TdmkEdit;
    Label1: TLabel;
    QrCliInt: TmySQLQuery;
    QrCliIntNOMECI: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    DsCliInt: TDataSource;
    Panel3: TPanel;
    QrLotesNOMECLIINT: TWideStringField;
    QrLotesNOMEPROCEDE: TWideStringField;
    QrLotesCodigo: TIntegerField;
    QrLotesControle: TIntegerField;
    QrLotesFicha: TIntegerField;
    QrLotesData: TDateField;
    QrLotesClienteI: TIntegerField;
    QrLotesProcedencia: TIntegerField;
    QrLotesTransportadora: TIntegerField;
    QrLotesMarca: TWideStringField;
    QrLotesPecas: TFloatField;
    QrLotesPecasNF: TFloatField;
    QrLotesM2: TFloatField;
    QrLotesPNF: TFloatField;
    QrLotesPLE: TFloatField;
    QrLotesPDA: TFloatField;
    QrLotesRecorte_PDA: TFloatField;
    QrLotesPTA: TFloatField;
    QrLotesRecorte_PTA: TFloatField;
    QrLotesRaspa_PTA: TFloatField;
    QrLotesTipificacao: TIntegerField;
    QrLotesAparasCabelo: TSmallintField;
    QrLotesSeboPreDescarne: TSmallintField;
    QrLotesValor: TFloatField;
    QrLotesCustoPQ: TFloatField;
    QrLotesFrete: TFloatField;
    QrLotesAbateTipo: TIntegerField;
    QrLotesLk: TIntegerField;
    QrLotesDataCad: TDateField;
    QrLotesDataAlt: TDateField;
    QrLotesUserCad: TIntegerField;
    QrLotesUserAlt: TIntegerField;
    QrLotesNOMETIPO: TWideStringField;
    DBGrid2: TDBGrid;
    QrMarcasNOMECLIINT: TWideStringField;
    QrMarcasNOMEPROCEDE: TWideStringField;
    QrMarcasCodigo: TIntegerField;
    QrMarcasControle: TIntegerField;
    QrMarcasFicha: TIntegerField;
    QrMarcasData: TDateField;
    QrMarcasClienteI: TIntegerField;
    QrMarcasProcedencia: TIntegerField;
    QrMarcasTransportadora: TIntegerField;
    QrMarcasMarca: TWideStringField;
    QrMarcasPecas: TFloatField;
    QrMarcasPecasNF: TFloatField;
    QrMarcasM2: TFloatField;
    QrMarcasPNF: TFloatField;
    QrMarcasPLE: TFloatField;
    QrMarcasPDA: TFloatField;
    QrMarcasRecorte_PDA: TFloatField;
    QrMarcasPTA: TFloatField;
    QrMarcasRecorte_PTA: TFloatField;
    QrMarcasRaspa_PTA: TFloatField;
    QrMarcasTipificacao: TIntegerField;
    QrMarcasAparasCabelo: TSmallintField;
    QrMarcasSeboPreDescarne: TSmallintField;
    QrMarcasValor: TFloatField;
    QrMarcasCustoPQ: TFloatField;
    QrMarcasFrete: TFloatField;
    QrMarcasAbateTipo: TIntegerField;
    QrMarcasLk: TIntegerField;
    QrMarcasDataCad: TDateField;
    QrMarcasDataAlt: TDateField;
    QrMarcasUserCad: TIntegerField;
    QrMarcasUserAlt: TIntegerField;
    QrMarcasNOMETIPO: TWideStringField;
    EdLote: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdMarca: TdmkEdit;
    PMPeso: TPopupMenu;
    PLE1: TMenuItem;
    PNF1: TMenuItem;
    PDA1: TMenuItem;
    PTA1: TMenuItem;
    EdPecas: TdmkEdit;
    Label4: TLabel;
    Panel5: TPanel;
    RGFonte: TRadioGroup;
    RGCasas: TRadioGroup;
    QrLotesLote: TWideStringField;
    QrMarcasLote: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCliIntChange(Sender: TObject);
    procedure EdPesoFExit(Sender: TObject);
    procedure QrLotesCalcFields(DataSet: TDataSet);
    procedure QrMarcasCalcFields(DataSet: TDataSet);
    procedure EdLoteExit(Sender: TObject);
    procedure EdMarcaExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure PNF1Click(Sender: TObject);
    procedure PLE1Click(Sender: TObject);
    procedure PDA1Click(Sender: TObject);
    procedure PTA1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPecasChange(Sender: TObject);
    procedure RGFonteClick(Sender: TObject);
    procedure RGCasasClick(Sender: TObject);
    procedure QrMarcasAfterScroll(DataSet: TDataSet);
    procedure QrLotesAfterScroll(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenMPs;
    procedure CalculaPesoProporcional();
  public
    { Public declarations }
    FEmit: Integer;
  end;

  var
  FmFormulasImpBHLocLote: TFmFormulasImpBHLocLote;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmFormulasImpBHLocLote.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulasImpBHLocLote.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormulasImpBHLocLote.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulasImpBHLocLote.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrLotes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMarcas, Dmod.MyDB);
  dmkPF.ObtemInfoRegEdit('PageControle', Caption, 0, ktInteger);
end;

procedure TFmFormulasImpBHLocLote.EdCliIntChange(Sender: TObject);
begin
  ReopenMPs;
end;

procedure TFmFormulasImpBHLocLote.ReopenMPs;
var
  CliInt: Integer;
begin
  CliInt := Geral.IMV(EdCliInt.Text);
  //
  QrLotes.Close;
  QrLotes.SQL.Clear;
  QrLotes.SQL.Add('SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial');
  QrLotes.SQL.Add('ELSE cli.Nome END NOMECLIINT, CASE WHEN pro.Tipo=0');
  QrLotes.SQL.Add('THEN pro.RazaoSocial ELSE pro.Nome END NOMEPROCEDE, mpi.*');
  QrLotes.SQL.Add('FROM mpin mpi');
  QrLotes.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=mpi.ClienteI');
  QrLotes.SQL.Add('LEFT JOIN entidades pro ON pro.Codigo=mpi.Procedencia');
  if CliInt <> 0 then
    QrLotes.SQL.Add('WHERE mpi.ClienteI = '+IntToStr(CliInt));
  QrLotes.SQL.Add('ORDER BY mpi.Lote DESC, mpi.Controle DESC');
  QrLotes.SQL.Add('LIMIT 1000');
  UnDmkDAC_PF.AbreQuery(QrLotes, Dmod.MyDB);
  //////////////////////////////////////////////////////////////////////////////
  QrMarcas.Close;
  QrMarcas.SQL.Clear;
  QrMarcas.SQL.Add('SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial');
  QrMarcas.SQL.Add('ELSE cli.Nome END NOMECLIINT, CASE WHEN pro.Tipo=0');
  QrMarcas.SQL.Add('THEN pro.RazaoSocial ELSE pro.Nome END NOMEPROCEDE, mpi.*');
  QrMarcas.SQL.Add('FROM mpin mpi');
  QrMarcas.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=mpi.ClienteI');
  QrMarcas.SQL.Add('LEFT JOIN entidades pro ON pro.Codigo=mpi.Procedencia');
  if CliInt <> 0 then
    QrMarcas.SQL.Add('WHERE mpi.ClienteI = '+IntToStr(CliInt));
  QrMarcas.SQL.Add('ORDER BY mpi.Marca DESC, mpi.Controle DESC');
  QrMarcas.SQL.Add('LIMIT 1000');
  UnDmkDAC_PF.AbreQuery(QrMarcas, Dmod.MyDB);
  //////////////////////////////////////////////////////////////////////////////
end;

procedure TFmFormulasImpBHLocLote.EdPesoFExit(Sender: TObject);
begin
  EdPesoF.DecimalSize := RGCasas.ItemIndex;
end;

procedure TFmFormulasImpBHLocLote.QrLotesCalcFields(DataSet: TDataSet);
begin
  QrLotesNOMETIPO.Value := UnAppListas.DefineNOMETipificacaoCouro(
    -3, QrLotesTipificacao.Value);
end;

procedure TFmFormulasImpBHLocLote.QrMarcasCalcFields(DataSet: TDataSet);
begin
  QrMarcasNOMETIPO.Value := UnAppListas.DefineNOMETipificacaoCouro(
    -3, QrMarcasTipificacao.Value);
end;

procedure TFmFormulasImpBHLocLote.EdLoteExit(Sender: TObject);
begin
  EdLote.Text := Geral.TFT(EdLote.Text, 0, siPositivo);
  QrLotes.Locate('Lote', EdLote.Text, []);
end;

procedure TFmFormulasImpBHLocLote.EdMarcaExit(Sender: TObject);
begin
  //EdMarca.Text := Geral.TFT(EdMarca.Text, 0, siPositivo);
  QrMarcas.Locate('Marca', EdMarca.Text, []);
end;

procedure TFmFormulasImpBHLocLote.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dmkPF.SalvaInfoRegEdit('PageControl', Caption,
    PageControl1.ActivePageIndex, KtInteger);
end;

procedure TFmFormulasImpBHLocLote.DBGrid2DblClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotaoObject(PMPeso, Sender, 0, 0);
  EdPesoF.SetFocus;
end;

procedure TFmFormulasImpBHLocLote.DBGrid1DblClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotaoObject(PMPeso, Sender, 0, 0);
  EdPesoF.SetFocus;
end;

procedure TFmFormulasImpBHLocLote.PNF1Click(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: EdPesoF.Text := Geral.FFT(QrLotesPNF.Value, RGCasas.ItemIndex, siPositivo);
    1: EdPesoF.Text := Geral.FFT(QrMarcasPNF.Value, RGCasas.ItemIndex, siPositivo);
  end;
end;

procedure TFmFormulasImpBHLocLote.PLE1Click(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: EdPesoF.Text := Geral.FFT(QrLotesPLE.Value, RGCasas.ItemIndex, siPositivo);
    1: EdPesoF.Text := Geral.FFT(QrMarcasPLE.Value, RGCasas.ItemIndex, siPositivo);
  end;
end;

procedure TFmFormulasImpBHLocLote.PDA1Click(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: EdPesoF.Text := Geral.FFT(QrLotesPDA.Value, RGCasas.ItemIndex, siPositivo);
    1: EdPesoF.Text := Geral.FFT(QrMarcasPDA.Value, RGCasas.ItemIndex, siPositivo);
  end;
end;

procedure TFmFormulasImpBHLocLote.PTA1Click(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: EdPesoF.Text := Geral.FFT(QrLotesPTA.Value, RGCasas.ItemIndex, siPositivo);
    1: EdPesoF.Text := Geral.FFT(QrMarcasPTA.Value, RGCasas.ItemIndex, siPositivo);
  end;
end;

procedure TFmFormulasImpBHLocLote.BtOKClick(Sender: TObject);
var
  Controle: Integer;
  Pecas, PesoF, Custo: Double;
begin
  if FEmit = 0 then
  begin
    Application.MessageBox('O n�mero de pesagem n�o foi definido!',
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  //
  PesoF   := Geral.DMV(EdPesoF.Text);
  Pecas   := Geral.DMV(EdPecas.Text);
  {if PesoF = QrEmitPeso.Value then Custo := QrEmitCusto.Value
  else Custo := QrEmitCusto.Value / QrEmitPeso.Value * PesoF;}
  Custo   := 0; // Calcula Depois ?
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'EmitCus', 'EmitCus', 'Controle');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO emitcus SET Peso=:P0, Pecas=:P1, ');
  Dmod.QrUpd.SQL.Add('Custo=:P2, Formula=:P3, DataEmis=:P4, MPIn=:P5, ');
  Dmod.QrUpd.SQL.Add('Codigo=:Pa, Controle=:Pb');
  Dmod.QrUpd.Params[00].AsFloat    := PesoF;
  Dmod.QrUpd.Params[01].AsFloat    := Pecas;
  Dmod.QrUpd.Params[02].AsFloat    := Custo;
  Dmod.QrUpd.Params[03].AsInteger  := 0;// Depois? QrEmitNumero.Value em  TFmFormulasImpBH.BtConfirmaClick
  Dmod.QrUpd.Params[04].AsDateTime := 0;// Depois? QrEmitDataEmis.Value em TFmFormulasImpBH.BtConfirmaClick
  Dmod.QrUpd.Params[05].AsInteger  := QrLotesControle.Value;
  //
  Dmod.QrUpd.Params[06].AsInteger  := FEmit;
  Dmod.QrUpd.Params[07].AsInteger  := Controle;
  Dmod.QrUpd.ExecSQL;
  //RecalculaCustos(QrMPInControle.Value);
  Screen.Cursor := crDefault;
  Application.MessageBox('Lote inclu�do com sucesso!',
    'Informa��o', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmFormulasImpBHLocLote.EdPecasChange(Sender: TObject);
begin
  CalculaPesoProporcional;
end;

procedure TFmFormulasImpBHLocLote.RGFonteClick(Sender: TObject);
begin
  CalculaPesoProporcional;
end;

procedure TFmFormulasImpBHLocLote.RGCasasClick(Sender: TObject);
begin
  CalculaPesoProporcional;
end;

procedure TFmFormulasImpBHLocLote.QrMarcasAfterScroll(DataSet: TDataSet);
begin
  CalculaPesoProporcional;
end;

procedure TFmFormulasImpBHLocLote.QrLotesAfterScroll(DataSet: TDataSet);
begin
  CalculaPesoProporcional;
end;

procedure TFmFormulasImpBHLocLote.PageControl1Change(Sender: TObject);
begin
  CalculaPesoProporcional;
end;

procedure TFmFormulasImpBHLocLote.CalculaPesoProporcional();
var
  Total, PesoF, Pecas, TotPc: Double;
  i: Integer;
begin
  case PageControl1.ActivePageIndex of
    0:
    begin
      TotPc := QrLotesPecas.Value;
      case RGFonte.ItemIndex of
        0: Total := QrLotesPNF.Value;
        1: Total := QrLotesPLE.Value;
        2: Total := QrLotesPDA.Value;
        3: Total := QrLotesPTA.Value;
        else Total := 0;
      end;
    end;
    1:
    begin
      TotPc := QrMarcasPecas.Value;
      case RGFonte.ItemIndex of
        0: Total := QrMarcasPNF.Value;
        1: Total := QrMarcasPLE.Value;
        2: Total := QrMarcasPDA.Value;
        3: Total := QrMarcasPTA.Value;
        else Total := 0;
      end;
    end;
    else
    begin
      TotPc := 0;
      Total := 0;
    end;
  end;
  if TotPc >= 0.001 then
  begin
    Pecas := Geral.DMV(EdPecas.Text);
    PesoF := Total / TotPc * Pecas;
    for i := 1 to RGCasas.ItemIndex do
      PesoF := PesoF * 10;
    PesoF := Round(PesoF);
    for i := 1 to RGCasas.ItemIndex do
      PesoF := PesoF / 10;
    EdPesoF.Text := Geral.FFT(PesoF, RGCasas.ItemIndex, siPositivo);
  end;
end;

end.

