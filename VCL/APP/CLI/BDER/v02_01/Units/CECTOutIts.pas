unit CECTOutIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral,  DB, Mask, DBCtrls, DmkDAC_PF,
  dmkLabel, Grids, DBGrids, dmkDBGrid, mySQLDbTables, dmkEdit, dmkImage,
  UnDmkEnums;

type
  TTipoMedidaAlterada = (tmaAuto, tmaPeso, tmaPeca, tmaValor);
  TFmCECTOutIts = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    PnAdd: TPanel;
    GradeCECTInn: TdmkDBGrid;
    QrCECTInn: TmySQLQuery;
    DsCECTInn: TDataSource;
    QrCECTInnCodigo: TIntegerField;
    QrCECTInnCliente: TIntegerField;
    QrCECTInnDataE: TDateField;
    QrCECTInnNFInn: TIntegerField;
    QrCECTInnNFRef: TIntegerField;
    QrCECTInnInnQtdkg: TFloatField;
    QrCECTInnInnQtdPc: TFloatField;
    QrCECTInnInnQtdVal: TFloatField;
    QrCECTInnSdoQtdkg: TFloatField;
    QrCECTInnSdoQtdPc: TFloatField;
    QrCECTInnSdoQtdVal: TFloatField;
    GroupBox1: TGroupBox;
    EdOutQtdkg: TdmkEdit;
    Label4: TLabel;
    Label6: TLabel;
    EdOutQtdPc: TdmkEdit;
    EdOutQtdVal: TdmkEdit;
    Label7: TLabel;
    GroupBox2: TGroupBox;
    EdSdoQtdkg: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdSdoQtdPc: TdmkEdit;
    EdSdoQtdVal: TdmkEdit;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBControla: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    GBConfirma: TGroupBox;
    Panel2: TPanel;
    Panel6: TPanel;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeCECTInnDblClick(Sender: TObject);
    procedure EdOutQtdkgEnter(Sender: TObject);
    procedure EdOutQtdkgChange(Sender: TObject);
    procedure EdOutQtdPcEnter(Sender: TObject);
    procedure EdOutQtdPcChange(Sender: TObject);
    procedure EdOutQtdValEnter(Sender: TObject);
    procedure EdOutQtdValChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FTipoMedidaAlterada: TTipoMedidaAlterada;
    procedure ReopenCECTInn(Codigo: Integer);
    procedure AtualizaEstoque(Fonte: TTipoMedidaAlterada);
  public
    { Public declarations }
  end;

  var
  FmCECTOutIts: TFmCECTOutIts;

implementation

uses UnMyObjects, Module, CECTOut, UmySQLModule;

{$R *.DFM}

procedure TFmCECTOutIts.AtualizaEstoque(Fonte: TTipoMedidaAlterada);
var
  Peso, Peca, Valr: Double;
begin
  if Fonte = tmaAuto then Fonte := FTipoMedidaAlterada;
  //
  Peso := EdOutQtdkg.ValueVariant;
  Peca := EdOutQtdPc.ValueVariant;
  Valr := EdOutQtdVal.ValueVariant;
  //
  case Fonte of
    tmaPeso:
    begin
      if Peso >= QrCECTInnSdoQtdkg.Value then
      begin
        Peca := QrCECTInnSdoQtdPc.Value;
        Valr := QrCECTInnSdoQtdVal.Value;
      end else begin
        if QrCECTInnSdoQtdkg.Value <= 0 then
        begin
          Peca := 0;
          Valr := 0;
        end else begin
          Peca := Trunc(Peso / QrCECTInnSdoQtdkg.Value * QrCECTInnSdoQtdPc.Value);
          Valr := Trunc(Peso / QrCECTInnSdoQtdkg.Value * QrCECTInnSdoQtdVal.Value * 100) / 100;
        end;
      end;
    end;
    tmaPeca:
    begin
      if Peca >= QrCECTInnSdoQtdPc.Value then
      begin
        Peso := QrCECTInnSdoQtdkg.Value;
        Valr := QrCECTInnSdoQtdVal.Value;
      end else begin
        if QrCECTInnSdoQtdPc.Value <= 0 then
        begin
          Peso := 0;
          Valr := 0;
        end else begin
          Peso := Trunc(Peca / QrCECTInnSdoQtdPc.Value * QrCECTInnSdoQtdkg.Value * 1000) / 1000;
          Valr := Trunc(Peca / QrCECTInnSdoQtdPc.Value * QrCECTInnSdoQtdVal.Value * 100) / 100;
        end;
      end;
    end;
    tmaValor:
    begin
      if Valr >= QrCECTInnSdoQtdVal.Value then
      begin
        Peso := QrCECTInnSdoQtdkg.Value;
        Peca := QrCECTInnSdoQtdPc.Value;
      end else begin
        if QrCECTInnSdoQtdVal.Value <= 0 then
        begin
          Peso := 0;
          Peca := 0;
        end else begin
          Peso := Trunc(Valr / QrCECTInnSdoQtdVal.Value * QrCECTInnSdoQtdkg.Value * 1000) / 1000;
          Peca := Trunc(Valr / QrCECTInnSdoQtdVal.Value * QrCECTInnSdoQtdPc.Value);
        end;
      end;
    end;
  end;
  if Fonte <> tmaPeso then
    EdOutQtdkg.ValueVariant  := Peso;
  if Fonte <> tmaPeca then
    EdOutQtdPc.ValueVariant  := Peca;
  if Fonte <> tmaValor then
    EdOutQtdVal.ValueVariant := Valr;
  //
  Peso := QrCECTInnSdoQtdkg.Value  - Peso;
  Peca := QrCECTInnSdoQtdPc.Value  - Peca;
  Valr := QrCECTInnSdoQtdVal.Value - Valr;
  //
  EdSdoQtdkg.ValueVariant  := Peso;
  EdSdoQtdPc.ValueVariant  := Peca;
  EdSdoQtdval.ValueVariant := Valr;
end;

procedure TFmCECTOutIts.BitBtn1Click(Sender: TObject);
begin
  GradeCECTInn.Enabled := True;
  GBConfirma.Visible   := False;
  PnAdd.Visible        := False;
  //
  EdOutQtdPc.ValueVariant  := 0;
  AtualizaEstoque(tmaPeca);
end;

procedure TFmCECTOutIts.BtOKClick(Sender: TObject);
var
  Peso, Peca, Valr: Double;
  Controle, Codigo, CECTInn: Integer;
begin
  Peso := EdSdoQtdkg.ValueVariant;
  Peca := EdSdoQtdPc.ValueVariant;
  Valr := EdSdoQtdVal.ValueVariant;
  //
  if ((Peso = 0) or (Peca = 0) or (Valr = 0)) and (Peso + Peca + Valr <> 0) then
  begin
    Application.MessageBox(PChar('Inclus�o cancelada!'+sLineBreak+'Todos itens de ' +
    'medida devem ter saldo, ou todos devem estar zerados!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Peso := EdOutQtdkg.ValueVariant;
  Peca := EdOutQtdPc.ValueVariant;
  Valr := EdOutQtdVal.ValueVariant;
  //
  Codigo   := FmCECTOut.QrCECTOutCodigo.Value;
  CECTInn  := QrCECTInnCodigo.Value;
  Controle := UMyMod.BuscaEmLivreY_Def('cectoutits', 'Controle', ImgTipo.SQLType,
    QrCECTInnCodigo.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cectoutits', False, [
    'Codigo', 'CECTInn', 'OutQtdkg', 'OutQtdPc', 'OutQtdVal'
  ], ['Controle'], [
    Codigo, CECTInn, Peso, Peca, Valr
  ], [Controle], True) then
  begin
    Dmod.AtualizaEstoqueCECT(CECTInn, Codigo);
    FmCECTOut.ReopenCECTOut(Codigo);
    //
    Close;
  end;
end;

procedure TFmCECTOutIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCECTOutIts.EdOutQtdkgChange(Sender: TObject);
begin
  AtualizaEstoque(tmaAuto);
end;

procedure TFmCECTOutIts.EdOutQtdkgEnter(Sender: TObject);
begin
  FTipoMedidaAlterada := tmaPeso;
end;

procedure TFmCECTOutIts.EdOutQtdPcChange(Sender: TObject);
begin
  AtualizaEstoque(tmaAuto);
end;

procedure TFmCECTOutIts.EdOutQtdPcEnter(Sender: TObject);
begin
  FTipoMedidaAlterada := tmaPeca;
end;

procedure TFmCECTOutIts.EdOutQtdValChange(Sender: TObject);
begin
  AtualizaEstoque(tmaAuto);
end;

procedure TFmCECTOutIts.EdOutQtdValEnter(Sender: TObject);
begin
  FTipoMedidaAlterada := tmaValor;
end;

procedure TFmCECTOutIts.GradeCECTInnDblClick(Sender: TObject);
begin
  GBConfirma.Visible   := True;
  GradeCECTInn.Enabled := False;
  PnAdd.Visible        := True;
  //
  EdOutQtdkg.ValueVariant  := QrCECTInnInnQtdkg.Value;
  EdOutQtdPc.ValueVariant  := QrCECTInnInnQtdPc.Value;
  EdOutQtdVal.ValueVariant := QrCECTInnInnQtdVal.Value;
  //
  EdOutQtdkg.SetFocus;
end;

procedure TFmCECTOutIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCECTOutIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenCECTInn(0);
end;

procedure TFmCECTOutIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCECTOutIts.ReopenCECTInn(Codigo: Integer);
begin
  QrCECTInn.Close;
  QrCECTInn.Params[0].AsInteger := FmCECTOut.QrClientesPesqCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrCECTInn, Dmod.MyDB);
end;

end.
