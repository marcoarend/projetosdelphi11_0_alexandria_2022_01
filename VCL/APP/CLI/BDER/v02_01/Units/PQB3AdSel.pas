unit PQB3AdSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmPQB3AdSel = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPqx: TMySQLQuery;
    DsSPqx: TDataSource;
    Panel2: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel3: TPanel;
    EdEmpresa: TdmkEdit;
    Label1: TLabel;
    EdCliInt: TdmkEdit;
    Label2: TLabel;
    EdInsumo: TdmkEdit;
    Label3: TLabel;
    EdNO_Empresa: TdmkEdit;
    EdNO_CliInt: TdmkEdit;
    EdNO_Insumo: TdmkEdit;
    QrPqxOrigemCtrl: TIntegerField;
    QrPqxDataX: TDateField;
    QrPqxSerie: TIntegerField;
    QrPqxNF: TIntegerField;
    QrPqxxLote: TWideStringField;
    QrPqxCustoUnit: TFloatField;
    QrPqxdFab: TDateField;
    QrPqxdVal: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure dmkDBGridZTO1DblClick(Sender: TObject);
  private
    { Private declarations }
    procedure SelecionaAtual();
  public
    { Public declarations }
    FSelecionou: Boolean;
    //
    procedure ReopenPQX(Empresa, CliOrig, Insumo: Integer);
  end;

  var
  FmPQB3AdSel: TFmPQB3AdSel;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmPQB3AdSel.BtOKClick(Sender: TObject);
begin
  SelecionaAtual();
end;

procedure TFmPQB3AdSel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQB3AdSel.dmkDBGridZTO1DblClick(Sender: TObject);
begin
  SelecionaAtual();
end;

procedure TFmPQB3AdSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQB3AdSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSelecionou := False;
end;

procedure TFmPQB3AdSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQB3AdSel.ReopenPQX(Empresa, CliOrig, Insumo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPqx, Dmod.MyDB, [
  'SELECT pqx.OrigemCtrl, pqx.DataX, pqe.Serie, ',
  'pqe.NF, its.xLote, its.dFab, its.dVal, ',
  'IF(its.TotalPeso=0, 0.0000,  ',
  '  its.TotalCusto / its.TotalPeso) CustoUnit ',
  'FROM pqx ',
  'LEFT JOIN pqe pqe ON pqe.Codigo=pqx.OrigemCodi ',
  'LEFT JOIN pqeits its ON its.Controle=pqx.OrigemCtrl ',
  'WHERE pqx.Empresa=' + Geral.FF0(Empresa),
  'AND pqx.CliOrig=' + Geral.FF0(CliOrig),
  'AND pqx.Insumo=' + Geral.FF0(Insumo),
  'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
  'ORDER BY DataX DESC, OrigemCtrl DESC ',
  '']);
end;

procedure TFmPQB3AdSel.SelecionaAtual();
begin
  if QrPqx.RecordCount > 0 then
  begin
    FSelecionou := True;
    Close;
  end;
end;

end.
