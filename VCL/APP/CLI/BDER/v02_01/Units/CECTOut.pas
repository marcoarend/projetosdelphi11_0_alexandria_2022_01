unit CECTOut;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkDBGrid, DB, DmkDAC_PF,
  mySQLDbTables, dmkEdit, dmkEditCB, DBCtrls, dmkDBLookupComboBox, ComCtrls,
  dmkEditDateTimePicker, dmkLabel, dmkGeral, Menus, MyDBCheck, UnDmkProcFunc,
  dmkImage, UnDmkEnums;

type
  TFmCECTOut = class(TForm)
    Panel1: TPanel;
    PnPesquisa: TPanel;
    Panel4: TPanel;
    PnEdita: TPanel;
    PnGrades: TPanel;
    GradeOut: TdmkDBGrid;
    QrCECTOut: TmySQLQuery;
    DsCECTOut: TDataSource;
    QrClientesPesq: TmySQLQuery;
    DsClientesPesq: TDataSource;
    QrClientesPesqCodigo: TIntegerField;
    QrClientesPesqNOMECLI: TWideStringField;
    TPDataS: TdmkEditDateTimePicker;
    EdNFOut: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdClienteEdit: TdmkEditCB;
    Label5: TLabel;
    CBClienteEdit: TdmkDBLookupComboBox;
    GradeIts: TDBGrid;
    Splitter1: TSplitter;
    QrCECTOutIts: TmySQLQuery;
    DsCECTOutIts: TDataSource;
    QrClientesEdit: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientesEdit: TDataSource;
    QrCECTOutNOMECLI: TWideStringField;
    QrCECTOutCodigo: TIntegerField;
    QrCECTOutCliente: TIntegerField;
    QrCECTOutDataS: TDateField;
    QrCECTOutNFOut: TIntegerField;
    QrCECTOutTotQtdkg: TFloatField;
    QrCECTOutTotQtdPc: TFloatField;
    QrCECTOutTotQtdVal: TFloatField;
    QrCECTOutItsDataE: TDateField;
    QrCECTOutItsNFInn: TIntegerField;
    QrCECTOutItsNFRef: TIntegerField;
    QrCECTOutItsCodigo: TIntegerField;
    QrCECTOutItsControle: TIntegerField;
    QrCECTOutItsCECTInn: TIntegerField;
    QrCECTOutItsOutQtdkg: TFloatField;
    QrCECTOutItsOutQtdPc: TFloatField;
    QrCECTOutItsOutQtdVal: TFloatField;
    PMNF: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    PMItens: TPopupMenu;
    Incluiitemdedevoluo1: TMenuItem;
    Retiraitemdedevoluo1: TMenuItem;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    Label1: TLabel;
    CBClientePesq: TdmkDBLookupComboBox;
    EdClientePesq: TdmkEditCB;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    CkIni: TCheckBox;
    CkFim: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBControla: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtNF: TBitBtn;
    BtItens: TBitBtn;
    BtSaida: TBitBtn;
    GBConfirma: TGroupBox;
    Panel2: TPanel;
    Panel7: TPanel;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdClientePesqChange(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrCECTOutBeforeClose(DataSet: TDataSet);
    procedure QrCECTOutAfterScroll(DataSet: TDataSet);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure BtNFClick(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure Incluiitemdedevoluo1Click(Sender: TObject);
    procedure RGAbertosClick(Sender: TObject);
    procedure CkIniClick(Sender: TObject);
    procedure TPIniClick(Sender: TObject);
    procedure CkFimClick(Sender: TObject);
    procedure TPFimClick(Sender: TObject);
    procedure Retiraitemdedevoluo1Click(Sender: TObject);
  private
    { Private declarations }
    FCriando: Boolean;
  public
    { Public declarations }
    procedure MostraEdicao(Acao: TSQLType);
    procedure MostraEdicaoIts(Acao: TSQLType);
    procedure ReopenCECTOut(Codigo: Integer);
    procedure ReopenCECTOutIts(Controle: Integer);
  end;

  var
  FmCECTOut: TFmCECTOut;

implementation

uses UnMyObjects, Module, UMySQLModule, CECTOutIts;

{$R *.DFM}

procedure TFmCECTOut.Altera1Click(Sender: TObject);
begin
  MostraEdicao(stUpd);
end;

procedure TFmCECTOut.BitBtn1Click(Sender: TObject);
begin
  MostraEdicao(stLok);
end;

procedure TFmCECTOut.BitBtn2Click(Sender: TObject);
var
  Cliente, Codigo: Integer;
  DataS: String;
  {
  SdoQtdkg, SdoQtdPc, SdoQtdVal,
  OutQtdkg, OutQtdPc, OutQtdVal: Double;
  }
begin
  Cliente   := Geral.IMV(EdClienteEdit.Text);
  if Cliente = 0 then
  begin
    Application.MessageBox('Defina o cliente!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  DataS     := Geral.FDT(TPDataS.Date, 1);
  {
  OutQtdkg  := EdOutQtdkg.ValueVariant;
  OutQtdPc  := EdOutQtdPc.ValueVariant;
  OutQtdVal := EdOutQtdVal.ValueVariant;
  if ImgTipo.SQLType = stUpd then
  begin
    SdoQtdkg  := QrCECTOutSdoQtdkg.Value;
    SdoQtdPc  := QrCECTOutSdoQtdPc.Value;
    SdoQtdVal := QrCECTOutSdoQtdVal.Value;
  end else begin
    SdoQtdkg  := OutQtdkg;
    SdoQtdPc  := OutQtdPc;
    SdoQtdVal := OutQtdVal;
  end;
  }
  Codigo := UMyMod.BuscaEmLivreY_Def('cectOut', 'Codigo', ImgTipo.SQLType,
    QrCECTOutCodigo.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cectout', False, [
    'Cliente', 'DataS', 'NFOut'
  ], ['Codigo'], [
    Cliente, DataS, EdNFOut.ValueVariant
  ], [Codigo], True) then
  begin
    MostraEdicao(stLok);
    ReopenCECTOut(Codigo);
  end;
end;

procedure TFmCECTOut.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmCECTOut.BtNFClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNF, BtNF);
end;

procedure TFmCECTOut.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCECTOut.CkFimClick(Sender: TObject);
begin
  ReopenCECTOut(QrCECTOutCodigo.Value);
end;

procedure TFmCECTOut.CkIniClick(Sender: TObject);
begin
  ReopenCECTOut(QrCECTOutCodigo.Value);
end;

procedure TFmCECTOut.EdClientePesqChange(Sender: TObject);
begin
  ReopenCECTOut(QrCECTOutCodigo.Value);
end;

procedure TFmCECTOut.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCECTOut.FormCreate(Sender: TObject);
begin
  FCriando := True;
  //
  GBConfirma.Align := alClient;
  UnDmkDAC_PF.AbreQuery(QrClientesEdit, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientesPesq, Dmod.MyDB);
  TPIni.date := Date - 30;
  TPFim.Date := Date;
  //
  FCriando := False;
end;

procedure TFmCECTOut.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCECTOut.Inclui1Click(Sender: TObject);
begin
  MostraEdicao(stIns);
end;

procedure TFmCECTOut.Incluiitemdedevoluo1Click(Sender: TObject);
begin
  MostraEdicaoIts(stIns);
end;

procedure TFmCECTOut.MostraEdicao(Acao: TSQLType);
begin
  case Acao of
    stIns, stUpd:
    begin
      PnEdita.Visible    := True;
      GBConfirma.Visible := True;
      GBControla.Visible := False;
      PnPesquisa.Enabled := False;
      PnGrades.Enabled   := False;
      if Acao = stUpd then
      begin
        TPDataS.Date               := QrCECTOutDataS.Value;
        EdClienteEdit.ValueVariant := QrCECTOutCliente.Value;
        CBClienteEdit.KeyValue     := QrCECTOutCliente.Value;
        EdNFOut.ValueVariant       := QrCECTOutNFOut.Value;
        //
        if QrCECTOutIts.RecordCount > 0 then
        begin
          EdClienteEdit.Enabled := False;
          CBClienteEdit.Enabled := False;
          Application.MessageBox(PChar('N�o ser� poss�vel alterar o cliente, ' +
          'pois j� existe devolu��o para esta NF!'), 'Aviso', MB_OK+MB_ICONWARNING);
        end else begin
          EdClienteEdit.Enabled := True;
          CBClienteEdit.Enabled := True;
        end;
      end;
      TPDataS.SetFocus;
    end;
    //stDel: ;
    //stCpy: ;
    stLok:
    begin
      GBControla.Visible := True;
      PnPesquisa.Enabled := True;
      PnGrades.Enabled   := True;
      GBConfirma.Visible := False;
      PnEdita.Visible    := False;
    end;
  end;
  ImgTipo.SQLType := Acao;
end;

procedure TFmCECTOut.MostraEdicaoIts(Acao: TSQLType);
begin
  if DBCheck.CriaFm(TFmCECTOutIts, FmCECTOutIts, afmoNegarComAviso) then
  begin
    FmCECTOutIts.ImgTipo.SQLType := Acao;
    FmCECTOutIts.ShowModal;
    FmCECTOutIts.Destroy;
  end;
end;

procedure TFmCECTOut.QrCECTOutAfterScroll(DataSet: TDataSet);
begin
  ReopenCECTOutIts(QrCECTOutItsControle.Value);
end;

procedure TFmCECTOut.QrCECTOutBeforeClose(DataSet: TDataSet);
begin
  QrCECTOutIts.Close;
end;

procedure TFmCECTOut.ReopenCECTOut(Codigo: Integer);
var
  Cliente: Integer;
begin
  if FCriando then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    Cliente := Geral.IMV(EdClientePesq.Text);
    //
    QrCECTOut.Close;
    QrCECTOut.SQL.Clear;
    QrCECTOut.SQL.Add('SELECT IF(cli.Tipo=0, RazaoSocial, cli.Nome) NOMECLI, Out.*');
    QrCECTOut.SQL.Add('FROM cectout Out');
    QrCECTOut.SQL.Add('LEFT JOIN entidades cli ON Out.Cliente=cli.Codigo');
    QrCECTOut.SQL.Add(dmkPF.SQL_Periodo('WHERE out.DataS ',
      TPIni.date, TPFim.Date, CkIni.Checked, CkFim.Checked));
    if Cliente <> 0 then
      QrCECTOut.SQL.Add(' AND out.Cliente=' + FormatFloat('0', Cliente));
    UnDmkDAC_PF.AbreQuery(QrCECTOut, Dmod.MyDB);
    //
    if Codigo > 0 then
      QrCECTOut.Locate('Codigo', Codigo, []);
  finally
    Screen.Cursor := crDefault;
  end;
  //
end;

procedure TFmCECTOut.ReopenCECTOutIts(Controle: Integer);
begin
  QrCECTOutIts.Close;
  QrCECTOutIts.Params[0].AsInteger := QrCECTOutCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrCECTOutIts, Dmod.MyDB);
  //
  if Controle < 0 then
    QrCECTOutIts.Locate('Controle', Controle, []);
end;

procedure TFmCECTOut.Retiraitemdedevoluo1Click(Sender: TObject);
var
  CECTInn, Codigo, Controle: Integer;
begin
  if Application.MessageBox('Confirma a retirada do item selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    CECTInn  := QrCECTOutItsCECTinn.Value;
    Codigo   := QrCECTOutItsCodigo.Value;
    Controle := QrCECTOutItsControle.Value;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM cectoutits WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.AtualizaEstoqueCECT(CECTInn, Codigo);
    FmCECTOut.ReopenCECTOut(Codigo);
    //
  end;
end;

procedure TFmCECTOut.RGAbertosClick(Sender: TObject);
begin
  ReopenCECTOut(QrCECTOutCodigo.Value);
end;

procedure TFmCECTOut.TPFimClick(Sender: TObject);
begin
  ReopenCECTOut(QrCECTOutCodigo.Value);
end;

procedure TFmCECTOut.TPIniClick(Sender: TObject);
begin
  ReopenCECTOut(QrCECTOutCodigo.Value);
end;

// Parei aqui. Fazer impress�o da NF
// somar itens ou n�o?
end.

