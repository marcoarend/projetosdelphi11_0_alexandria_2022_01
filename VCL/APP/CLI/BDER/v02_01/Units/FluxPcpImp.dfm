object FmFluxPcpImp: TFmFluxPcpImp
  Left = 339
  Top = 185
  Caption = 'PCP-FLUXO-005 :: Impress'#245'es de PCP'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 241
        Height = 32
        Caption = 'Impress'#245'es de PCP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 241
        Height = 32
        Caption = 'Impress'#245'es de PCP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 241
        Height = 32
        Caption = 'Impress'#245'es de PCP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 222
          Align = alTop
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 1
            Top = 1
            Width = 806
            Height = 64
            Align = alTop
            Caption = ' Per'#237'odo'
            TabOrder = 0
            object Panel6: TPanel
              Left = 2
              Top = 15
              Width = 802
              Height = 47
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label1: TLabel
                Left = 8
                Top = 4
                Width = 55
                Height = 13
                Caption = 'Data inicial:'
              end
              object Label2: TLabel
                Left = 124
                Top = 4
                Width = 48
                Height = 13
                Caption = 'Data final:'
              end
              object TPIni: TdmkEditDateTimePicker
                Left = 8
                Top = 20
                Width = 112
                Height = 21
                Date = 38832.987851921300000000
                Time = 38832.987851921300000000
                TabOrder = 0
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPFim: TdmkEditDateTimePicker
                Left = 124
                Top = 20
                Width = 112
                Height = 21
                Date = 38832.987851921300000000
                Time = 38832.987851921300000000
                TabOrder = 1
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
            end
          end
          object Panel7: TPanel
            Left = 1
            Top = 65
            Width = 806
            Height = 156
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object RGAgrupa: TRadioGroup
              Left = 0
              Top = 0
              Width = 92
              Height = 156
              Align = alLeft
              Caption = ' Agrupamentos: '
              ItemIndex = 1
              Items.Strings = (
                '0'
                '1'
                '2')
              TabOrder = 0
            end
            object RGOrdem1: TRadioGroup
              Left = 92
              Top = 0
              Width = 120
              Height = 156
              Align = alLeft
              Caption = ' Ordem 1: '
              ItemIndex = 0
              Items.Strings = (
                'Artigo (texto)'
                'Espessura (Texto)'
                'Cor (Texto)'
                'Classe (Texto)'
                'Entrega'
                'Dia a produzir')
              TabOrder = 1
            end
            object RGOrdem2: TRadioGroup
              Left = 212
              Top = 0
              Width = 120
              Height = 156
              Align = alLeft
              Caption = ' Ordem 2: '
              ItemIndex = 1
              Items.Strings = (
                'Artigo (texto)'
                'Espessura (Texto)'
                'Cor (Texto)'
                'Classe (Texto)'
                'Entrega'
                'Dia a produzir')
              TabOrder = 2
            end
            object RGOrdem3: TRadioGroup
              Left = 332
              Top = 0
              Width = 120
              Height = 156
              Align = alLeft
              Caption = ' Ordem 3: '
              ItemIndex = 2
              Items.Strings = (
                'Artigo (texto)'
                'Espessura (Texto)'
                'Cor (Texto)'
                'Classe (Texto)'
                'Entrega'
                'Dia a produzir')
              TabOrder = 3
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrAProdEstg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE_FORMAT(its.Entrega, "%d/%m/%Y") Entrega_TXT,'
      'DATE_FORMAT(mps.DataSeq, "%d/%m/%Y") DataSeq_TXT,'
      'its.Entrega, its.M2Pedido, its.Controle,'
      'its.Texto, its.CorTxt, its.EspesTxt, its.Classe,'
      'fpi.Nome NO_IniStg, fpf.Nome NO_FimStg, mps.*'
      'FROM mpvpss mps'
      'LEFT JOIN fluxpcpstg fpi ON fpi.Codigo=mps.IniStg'
      'LEFT JOIN fluxpcpstg fpf ON fpf.Codigo=mps.FimStg'
      'LEFT JOIN mpvits its ON its.Controle=mps.MPVIts'
      'WHERE mps.DataSeq BETWEEN "2020-02-02" AND "2020-02-28"'
      'ORDER BY mps.DataSeq, fpi.Ordem, fpf.Ordem')
    Left = 500
    Top = 300
    object QrAProdEstgEntrega: TDateField
      FieldName = 'Entrega'
    end
    object QrAProdEstgM2Pedido: TFloatField
      FieldName = 'M2Pedido'
    end
    object QrAProdEstgControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAProdEstgTexto: TWideStringField
      FieldName = 'Texto'
      Size = 50
    end
    object QrAProdEstgCorTxt: TWideStringField
      FieldName = 'CorTxt'
      Size = 30
    end
    object QrAProdEstgEspesTxt: TWideStringField
      FieldName = 'EspesTxt'
      Size = 10
    end
    object QrAProdEstgClasse: TWideStringField
      FieldName = 'Classe'
      Size = 15
    end
    object QrAProdEstgNO_IniStg: TWideStringField
      FieldName = 'NO_IniStg'
      Size = 30
    end
    object QrAProdEstgNO_FimStg: TWideStringField
      FieldName = 'NO_FimStg'
      Size = 30
    end
    object QrAProdEstgMPVIts: TIntegerField
      FieldName = 'MPVIts'
      Required = True
    end
    object QrAProdEstgDiaSeq: TIntegerField
      FieldName = 'DiaSeq'
      Required = True
    end
    object QrAProdEstgConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrAProdEstgDataSeq: TDateField
      FieldName = 'DataSeq'
      Required = True
    end
    object QrAProdEstgIniStg: TIntegerField
      FieldName = 'IniStg'
      Required = True
    end
    object QrAProdEstgFimStg: TIntegerField
      FieldName = 'FimStg'
      Required = True
    end
    object QrAProdEstgDtHrReal: TDateTimeField
      FieldName = 'DtHrReal'
      Required = True
    end
    object QrAProdEstgEntrega_TXT: TWideStringField
      FieldName = 'Entrega_TXT'
      Size = 10
    end
    object QrAProdEstgDataSeq_TXT: TWideStringField
      FieldName = 'DataSeq_TXT'
      Size = 10
    end
  end
  object frxAProdEstg: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MePesoOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Band4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '{  if <frxDsEstq."Peso"> < 0 then'
      '  begin              '
      '    MePeso.Font.Color       := clRed;'
      '    MeCodProprio.Font.Color := clRed;  '
      '    MePQ.Font.Color         := clRed;  '
      '    MeNomePQ.Font.Color     := clRed;                        '
      '  end else'
      '  begin              '
      '    MePeso.Font.Color       := clBlack;'
      '    MeCodProprio.Font.Color := clBlack;  '
      '    MePQ.Font.Color         := clBlack;  '
      '    MeNomePQ.Font.Color     := clBlack;                        '
      '  end;            '
      '}end;'
      ''
      'begin'
      '{'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      ''
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      '  //'
      '}'
      'end.')
    OnGetValue = frxAProdEstgGetValue
    Left = 500
    Top = 348
    Datasets = <
      item
        DataSet = frxDsAProdEstg
        DataSetName = 'frxDsAProdEstg'
      end
      item
        DataSet = frxDsEstgAProd
        DataSetName = 'frxDsEstgAPord'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 14.314470000000000000
        Top = 498.897960000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          Left = 507.763760000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 56.692950000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 41.574830000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'A Produzir no Dia por Est'#225'gio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 75.118120000000000000
          Top = 41.574830000000000000
          Width = 530.614100000000000000
          Height = 15.118100470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        Height = 37.795290240000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'Band4OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEstgAProd
        DataSetName = 'frxDsEstgAPord'
        RowCount = 0
        object Memo12: TfrxMemoView
          Top = 22.677014020000000000
          Width = 52.913156380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'OP')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 529.133931500000000000
          Top = 22.677014020000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Entrega')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 589.606411500000000000
          Top = 22.677014020000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea pedido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 52.913151500000000000
          Top = 22.677014020000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Data Seq')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 113.385900000000000000
          Top = 22.677180000000000000
          Width = 207.874015748031500000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 321.260050000000000000
          Top = 22.677180000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Espessura')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 396.850442520000000000
          Top = 22.677180000000000000
          Width = 132.283464566929100000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 170.078850000000000000
          Top = 3.779530000000000000
          Width = 507.464440000000000000
          Height = 18.000000000000000000
          DataField = 'NO_Estagio'
          DataSet = frxDsEstgAProd
          DataSetName = 'frxDsEstgAPord'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEstgAPord."NO_Estagio"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Top = 3.779530000000000000
          Width = 110.613790000000000000
          Height = 18.000000000000000000
          DataSet = frxDsEstgAProd
          DataSetName = 'frxDsEstgAPord'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Ordem do(s) Est'#225'gio(s):')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 113.385900000000000000
          Top = 3.779530000000000000
          Width = 53.920840000000000000
          Height = 18.000000000000000000
          DataField = 'Ordem'
          DataSet = frxDsEstgAProd
          DataSetName = 'frxDsEstgAPord'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstgAPord."Ordem"]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 366.614410000000000000
        Width = 680.315400000000000000
        object Memo23: TfrxMemoView
          Left = 589.606338270000000000
          Top = 3.779530000000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAProdEstg."M2Pedido">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeGF1: TfrxMemoView
          Left = 35.559060000000000000
          Top = 3.779530000000000000
          Width = 550.078850000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 196.535560000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstq."NOMECI"'
        object MeGH1: TfrxMemoView
          Left = 2.220470000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstq."NOMEPQ"'
        object MeGH2: TfrxMemoView
          Left = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        object Memo4: TfrxMemoView
          Left = 26.220470000000000000
          Top = 3.779527560000000000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeGF2: TfrxMemoView
          Left = 54.220470000000000000
          Top = 3.779527560000000000
          Width = 531.181200000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 589.606338270000000000
          Top = 3.779527560000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAProdEstg."M2Pedido">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        DataSet = frxDsAProdEstg
        DataSetName = 'frxDsAProdEstg'
        RowCount = 0
        object MeNomePQ: TfrxMemoView
          Left = 113.385865830000000000
          Width = 207.874015748031500000
          Height = 15.118110240000000000
          DataField = 'Texto'
          DataSetName = 'frxDsAProdEstg'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAProdEstg."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 589.606338270000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsAProdEstg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAProdEstg."M2Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 52.913019690000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'DataSeq'
          DataSetName = 'frxDsAProdEstg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAProdEstg."DataSeq"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePQ: TfrxMemoView
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSetName = 'frxDsAProdEstg'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAProdEstg."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCodProprio: TfrxMemoView
          Left = 321.260050000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsAProdEstg'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAProdEstg."EspesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 396.850442520000000000
          Width = 132.283464566929100000
          Height = 15.118110240000000000
          DataField = 'CorTxt'
          DataSet = frxDsAProdEstg
          DataSetName = 'frxDsAProdEstg'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAProdEstg."CorTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 529.134200000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Entrega'
          DataSetName = 'frxDsAProdEstg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAProdEstg."Entrega"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 453.543600000000000000
        Width = 680.315400000000000000
        object Memo3: TfrxMemoView
          Left = 589.606338270000000000
          Top = 3.779530000000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAProdEstg."M2Pedido">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 34.015770000000000000
          Top = 3.779530000000000000
          Width = 550.078850000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsAProdEstg: TfrxDBDataset
    UserName = 'frxDsAProdEstg'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Entrega=Entrega'
      'M2Pedido=M2Pedido'
      'Controle=Controle'
      'Texto=Texto'
      'CorTxt=CorTxt'
      'EspesTxt=EspesTxt'
      'Classe=Classe'
      'NO_IniStg=NO_IniStg'
      'NO_FimStg=NO_FimStg'
      'MPVIts=MPVIts'
      'DiaSeq=DiaSeq'
      'Conta=Conta'
      'DataSeq=DataSeq'
      'IniStg=IniStg'
      'FimStg=FimStg'
      'DtHrReal=DtHrReal'
      'Entrega_TXT=Entrega_TXT'
      'DataSeq_TXT=DataSeq_TXT')
    OpenDataSource = False
    DataSet = QrAProdEstg
    BCDToCurrency = False
    Left = 500
    Top = 396
  end
  object QrEstgAProd: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrEstgAProdAfterScroll
    SQL.Strings = (
      'SELECT fpi.Ordem, fpi.Nome NO_Estagio'
      'FROM mpvpss mps'
      'LEFT JOIN fluxpcpstg fpi ON fpi.Codigo=mps.IniStg'
      'LEFT JOIN mpvits its ON its.Controle=mps.MPVIts'
      'WHERE its.Entrega >= "2020-02-02"'
      ''
      'UNION'
      ''
      'SELECT fpf.Ordem, fpf.Nome NO_Estagio'
      'FROM mpvpss mps'
      'LEFT JOIN fluxpcpstg fpf ON fpf.Codigo=mps.FimStg'
      'LEFT JOIN mpvits its ON its.Controle=mps.MPVIts'
      'WHERE its.Entrega >= "2020-02-02"'
      ''
      'ORDER BY Ordem')
    Left = 592
    Top = 304
    object QrEstgAProdOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEstgAProdNO_Estagio: TWideStringField
      FieldName = 'NO_Estagio'
      Size = 30
    end
  end
  object frxDsEstgAProd: TfrxDBDataset
    UserName = 'frxDsEstgAPord'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Ordem=Ordem'
      'NO_Estagio=NO_Estagio')
    OpenDataSource = False
    DataSet = QrEstgAProd
    BCDToCurrency = False
    Left = 592
    Top = 352
  end
end
