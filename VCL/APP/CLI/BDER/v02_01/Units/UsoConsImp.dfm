object FmUsoConsImp: TFmUsoConsImp
  Left = 339
  Top = 185
  Caption = 'USO-CONSU-005 :: Impress'#245'es de Uso e Consumo'
  ClientHeight = 343
  ClientWidth = 880
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 880
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 832
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 784
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 382
        Height = 32
        Caption = 'Impress'#245'es de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 382
        Height = 32
        Caption = 'Impress'#245'es de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 382
        Height = 32
        Caption = 'Impress'#245'es de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 229
    Width = 880
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 876
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 273
    Width = 880
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 734
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 732
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 880
    Height = 181
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 812
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 489
      Height = 181
      Align = alLeft
      TabOrder = 0
      object Label3: TLabel
        Left = 248
        Top = 52
        Width = 80
        Height = 13
        Caption = 'Setor de destino:'
      end
      object Label4: TLabel
        Left = 16
        Top = 92
        Width = 229
        Height = 13
        Caption = 'Funcion'#225'rio / empresa de terceiro que ir'#225' utilizar:'
      end
      object Label1: TLabel
        Left = 16
        Top = 52
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label5: TLabel
        Left = 133
        Top = 52
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object Label10: TLabel
        Left = 16
        Top = 132
        Width = 40
        Height = 13
        Caption = 'Material:'
      end
      object Label12: TLabel
        Left = 16
        Top = 12
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object EdSetor: TdmkEditCB
        Left = 248
        Top = 68
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSetor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSetor: TdmkDBLookupComboBox
        Left = 280
        Top = 68
        Width = 201
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSE
        TabOrder = 5
        dmkEditCB = EdSetor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdFunci: TdmkEditCB
        Left = 16
        Top = 108
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFunci
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFunci: TdmkDBLookupComboBox
        Left = 72
        Top = 108
        Width = 409
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NO_Funci'
        ListSource = DsFunci
        TabOrder = 7
        dmkEditCB = EdFunci
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPIni: TdmkEditDateTimePicker
        Left = 16
        Top = 68
        Width = 112
        Height = 21
        Date = 44619.000000000000000000
        Time = 0.521748657396528900
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPFim: TdmkEditDateTimePicker
        Left = 133
        Top = 68
        Width = 112
        Height = 21
        Date = 44619.000000000000000000
        Time = 0.521748657396528900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdPQ: TdmkEditCB
        Left = 16
        Top = 148
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPQ
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPQ: TdmkDBLookupComboBox
        Left = 72
        Top = 148
        Width = 409
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPQ
        TabOrder = 9
        dmkEditCB = EdPQ
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCI: TdmkEditCB
        Left = 16
        Top = 28
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCI
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCI: TdmkDBLookupComboBox
        Left = 72
        Top = 28
        Width = 409
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECI'
        ListSource = DsCI
        TabOrder = 1
        dmkEditCB = EdCI
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object RGOrdem1: TRadioGroup
      Left = 577
      Top = 0
      Width = 100
      Height = 181
      Align = alLeft
      Caption = ' Ordem 1: '
      ItemIndex = 1
      Items.Strings = (
        'Material'
        'Entidade'
        'Setor'
        'Data'
        'Cliente interno')
      TabOrder = 2
    end
    object RGOrdem2: TRadioGroup
      Left = 677
      Top = 0
      Width = 100
      Height = 181
      Align = alLeft
      Caption = ' Ordem: '
      ItemIndex = 0
      Items.Strings = (
        'Material'
        'Entidade'
        'Setor'
        'Data'
        'Cliente interno')
      TabOrder = 3
    end
    object RGOrdem3: TRadioGroup
      Left = 777
      Top = 0
      Width = 100
      Height = 181
      Align = alLeft
      Caption = ' Ordem: '
      ItemIndex = 3
      Items.Strings = (
        'Material'
        'Entidade'
        'Setor'
        'Data'
        'Cliente interno')
      TabOrder = 4
    end
    object Panel5: TPanel
      Left = 489
      Top = 0
      Width = 88
      Height = 181
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 536
      ExplicitTop = 68
      ExplicitHeight = 41
      object RGAgrup: TRadioGroup
        Left = 0
        Top = 0
        Width = 88
        Height = 108
        Align = alClient
        Caption = 'Agrupamentos: '
        ItemIndex = 0
        Items.Strings = (
          '0'
          '1'
          '2')
        TabOrder = 0
        ExplicitHeight = 105
      end
      object RGDetalha: TRadioGroup
        Left = 0
        Top = 108
        Width = 88
        Height = 73
        Align = alBottom
        Caption = 'Detalhamento: '
        ItemIndex = 0
        Items.Strings = (
          'Anal'#237'tico'
          'Sint'#233'tico')
        TabOrder = 1
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65535
  end
  object QrSE: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM listasetores'
      'ORDER BY Nome')
    Left = 64
    Top = 180
    object QrSECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSENome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object DsSE: TDataSource
    DataSet = QrSE
    Left = 64
    Top = 228
  end
  object QrFunci: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NO_Funci'
      'FROM entidades'
      'ORDER BY Nome'
      '')
    Left = 316
    Top = 164
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFunciNO_Funci: TWideStringField
      FieldName = 'NO_Funci'
      Size = 120
    end
  end
  object DsFunci: TDataSource
    DataSet = QrFunci
    Left = 316
    Top = 212
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq '
      'WHERE Ativo=1'
      'AND GGXNiv2 IN (-12,12)'
      'ORDER BY Nome')
    Left = 248
    Top = 160
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 248
    Top = 208
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 192
    Top = 160
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 192
    Top = 208
  end
  object frxDsPeriodoA: TfrxDBDataset
    UserName = 'frxDsPeriodoA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Setor=Setor'
      'Funci=Funci'
      'DataX=DataX'
      'OrigemCodi=OrigemCodi'
      'OrigemCtrl=OrigemCtrl'
      'Tipo=Tipo'
      'CliOrig=CliOrig'
      'CliDest=CliDest'
      'Insumo=Insumo'
      'Peso=Peso'
      'Valor=Valor'
      'Retorno=Retorno'
      'StqMovIts=StqMovIts'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'RetQtd=RetQtd'
      'HowLoad=HowLoad'
      'StqCenLoc=StqCenLoc'
      'SdoPeso=SdoPeso'
      'SdoValr=SdoValr'
      'AcePeso=AcePeso'
      'AceValr=AceValr'
      'DtCorrApo=DtCorrApo'
      'Empresa=Empresa'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'NOMEPQ=NOMEPQ'
      'NO_FUNCI=NO_FUNCI'
      'NO_EMP=NO_EMP'
      'DataX_TXT=DataX_TXT'
      'NO_SETOR=NO_SETOR')
    DataSet = QrPeriodoA
    BCDToCurrency = False
    DataSetOptions = []
    Left = 132
    Top = 204
  end
  object frxUSO_CONSU_005_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.850269456000000000
    ReportOptions.LastChange = 39717.850269456000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxUSO_CONSU_005_AGetValue
    Left = 132
    Top = 256
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPeriodoA
        DataSetName = 'frxDsPeriodoA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 113.385887800000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 30.236240000000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 64.850340000000000000
          Width = 35.559060000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 37.779530000000000000
          Top = 64.850340000000000000
          Width = 174.929190000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362317010000000000
          Top = 94.488176770000000000
          Width = 226.771653540000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Material')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133946140000000000
          Top = 94.488176770000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826847320000000000
          Top = 94.488176770000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo R$')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 212.708720000000000000
          Top = 64.850340000000000000
          Width = 25.763760000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setor:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 238.472480000000000000
          Top = 64.850340000000000000
          Width = 124.724404570000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_SETOR]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 368.047310000000000000
          Top = 64.850340000000000000
          Width = 41.763760000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Entidade:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 409.811070000000000000
          Top = 64.850340000000000000
          Width = 264.566929130000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_FUNCI]')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 136.062992125984300000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 536.692913385826800000
          Top = 18.897650000000000000
          Width = 136.062992125984300000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Empresa: [VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 94.488250000000000000
          Width = 60.472431180000000000
          Height = 18.897637800000000000
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 94.488250000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Setor')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 94.488250000000000000
          Width = 151.181102360000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Funcion'#225'rio / terceiro')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPeriodoA
        DataSetName = 'frxDsPeriodoA'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826847320000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DataField = 'Valor'
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPeriodoA."Valor"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362317010000000000
          Width = 226.771653540000000000
          Height = 18.897637800000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPeriodoA."NOMEPQ"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133946140000000000
          Width = 56.692901180000000000
          Height = 18.897637800000000000
          DataField = 'Peso'
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPeriodoA."Peso"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 0.000073230000000007
          Width = 60.472431180000000000
          Height = 18.897637800000000000
          DataField = 'DataX'
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPeriodoA."DataX"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 0.000073229999999994
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataField = 'NO_SETOR'
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPeriodoA."NO_SETOR"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 0.000073230000000007
          Width = 151.181102360000000000
          Height = 18.897637800000000000
          DataField = 'NO_FUNCI'
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPeriodoA."NO_FUNCI"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 483.779840000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 438.425480000000000000
        Width = 680.315400000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Width = 529.134029130000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827101180000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodoA."Valor">,Band4)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodoA."Peso">,Band4)]')
          ParentFont = False
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 192.756030000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPeriodo."DataX"'
        object Me01_GH01: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315302360000000000
          Height = 18.897637800000000000
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPeriodo."?????"]')
          ParentFont = False
        end
      end
      object GF_02: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        object Me01_GF02: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 521.575042360000000000
          Height = 18.897637800000000000
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: [frxDsPeriodo."?????"]  ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827101180000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodoA."Valor">,Band4)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodoA."Peso">,Band4)]')
          ParentFont = False
        end
      end
      object GH_02: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPeriodo."Setor"'
        object Me01_GH02: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 672.756242360000000000
          Height = 18.897637800000000000
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPeriodo."?????"]')
          ParentFont = False
        end
      end
      object GF_01: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Me01_GF01: TfrxMemoView
          AllowVectorExport = True
          Width = 529.134102360000000000
          Height = 18.897637800000000000
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: [frxDsPeriodo."?????"]  ')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827101180000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodoA."Valor">,Band4)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodoA."Peso">,Band4)]')
          ParentFont = False
        end
      end
    end
  end
  object QrPeriodoA: TMySQLQuery
    SQL.Strings = (
      'SELECT DISTINCT uso.Setor, uso.Funci, pqx.*, pq_.Nome NOMEPQ'
      'FROM pqx pqx '
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pqx.Insumo'
      'LEFT JOIN pqn uso ON uso.Codigo=pqx.OrigemCodi '
      'WHERE pqx.Tipo=185 '
      'AND pqx.DataX BETWEEN "1899-12-30" AND "2022-12-31" '
      'AND pqx.Insumo=809'
      'AND CliOrig=-11'
      'AND uso.Setor>-99999999'
      'AND uso.Funci>-99999999'
      ' ')
    Left = 132
    Top = 152
    object QrPeriodoASetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrPeriodoAFunci: TIntegerField
      FieldName = 'Funci'
    end
    object QrPeriodoADataX: TDateField
      FieldName = 'DataX'
      Required = True
    end
    object QrPeriodoAOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
      Required = True
    end
    object QrPeriodoAOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
      Required = True
    end
    object QrPeriodoATipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPeriodoACliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPeriodoACliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPeriodoAInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPeriodoAPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPeriodoAValor: TFloatField
      FieldName = 'Valor'
    end
    object QrPeriodoARetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrPeriodoAStqMovIts: TIntegerField
      FieldName = 'StqMovIts'
      Required = True
    end
    object QrPeriodoAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPeriodoAAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPeriodoARetQtd: TFloatField
      FieldName = 'RetQtd'
    end
    object QrPeriodoAHowLoad: TSmallintField
      FieldName = 'HowLoad'
      Required = True
    end
    object QrPeriodoAStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrPeriodoASdoPeso: TFloatField
      FieldName = 'SdoPeso'
    end
    object QrPeriodoASdoValr: TFloatField
      FieldName = 'SdoValr'
    end
    object QrPeriodoAAcePeso: TFloatField
      FieldName = 'AcePeso'
    end
    object QrPeriodoAAceValr: TFloatField
      FieldName = 'AceValr'
    end
    object QrPeriodoADtCorrApo: TDateField
      FieldName = 'DtCorrApo'
      Required = True
    end
    object QrPeriodoAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPeriodoAAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPeriodoAAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPeriodoANOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPeriodoANO_FUNCI: TWideStringField
      FieldName = 'NO_FUNCI'
      Size = 120
    end
    object QrPeriodoANO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 120
    end
    object QrPeriodoADataX_TXT: TWideStringField
      FieldName = 'DataX_TXT'
      Size = 10
    end
    object QrPeriodoANO_SETOR: TWideStringField
      FieldName = 'NO_SETOR'
    end
  end
  object frxReport1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.850269456000000000
    ReportOptions.LastChange = 39717.850269456000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxUSO_CONSU_005_AGetValue
    Left = 596
    Top = 232
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPeriodoA
        DataSetName = 'frxDsPeriodoA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 117.165344570000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 30.236240000000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 64.850340000000000000
          Width = 35.559060000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 37.779530000000000000
          Top = 64.850340000000000000
          Width = 174.929190000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362317010000000000
          Top = 94.488176770000000000
          Width = 226.771653540000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Material')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133946140000000000
          Top = 94.488176770000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826847320000000000
          Top = 94.488176770000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo R$')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 212.708720000000000000
          Top = 64.850340000000000000
          Width = 25.763760000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setor:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 238.472480000000000000
          Top = 64.850340000000000000
          Width = 124.724404570000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_SETOR]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 368.047310000000000000
          Top = 64.850340000000000000
          Width = 41.763760000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Entidade:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 409.811070000000000000
          Top = 64.850340000000000000
          Width = 264.566929130000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_FUNCI]')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 136.062992125984300000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 536.692913385826800000
          Top = 18.897650000000000000
          Width = 136.062992125984300000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Empresa: [VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 94.488250000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 94.488250000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Setor')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 94.488250000000000000
          Width = 151.181102360000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Funcion'#225'rio / terceiro')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 196.535560000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPeriodoA
        DataSetName = 'frxDsPeriodoA'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826847320000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DataField = 'Valor'
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPeriodo."Valor"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362317010000000000
          Width = 226.771653540000000000
          Height = 18.897637800000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPeriodo."NOMEPQ"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133946140000000000
          Width = 56.692901180000000000
          Height = 18.897637800000000000
          DataField = 'Peso'
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPeriodo."Peso"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Top = 0.000073229999999994
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataField = 'DataX'
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'mm/dd/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPeriodo."DataX"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 0.000073229999999994
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataField = 'NO_SETOR'
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPeriodo."NO_SETOR"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 0.000073230000000007
          Width = 151.181102362204700000
          Height = 18.897637800000000000
          DataField = 'NO_FUNCI'
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPeriodo."NO_FUNCI"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826930310000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodo."Valor">,Band4)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Width = 529.134029130000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134029130000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodo."Peso">,Band4)]')
          ParentFont = False
        end
      end
    end
  end
  object QrPeriodoS: TMySQLQuery
    SQL.Strings = (
      'SELECT DISTINCT uso.Setor, uso.Funci, pqx.*, pq_.Nome NOMEPQ'
      'FROM pqx pqx '
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pqx.Insumo'
      'LEFT JOIN pqn uso ON uso.Codigo=pqx.OrigemCodi '
      'WHERE pqx.Tipo=185 '
      'AND pqx.DataX BETWEEN "1899-12-30" AND "2022-12-31" '
      'AND pqx.Insumo=809'
      'AND CliOrig=-11'
      'AND uso.Setor>-99999999'
      'AND uso.Funci>-99999999'
      ' ')
    Left = 464
    Top = 176
    object QrPeriodoSSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrPeriodoSFunci: TIntegerField
      FieldName = 'Funci'
    end
    object QrPeriodoSDataX: TDateField
      FieldName = 'DataX'
      Required = True
    end
    object QrPeriodoSTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPeriodoSCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPeriodoSInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPeriodoSPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPeriodoSValor: TFloatField
      FieldName = 'Valor'
    end
    object QrPeriodoSEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPeriodoSNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPeriodoSNO_FUNCI: TWideStringField
      FieldName = 'NO_FUNCI'
      Size = 120
    end
    object QrPeriodoSNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 120
    end
    object QrPeriodoSDataX_TXT: TWideStringField
      FieldName = 'DataX_TXT'
      Size = 10
    end
    object QrPeriodoSNO_SETOR: TWideStringField
      FieldName = 'NO_SETOR'
    end
  end
  object frxDsPeriodoS: TfrxDBDataset
    UserName = 'frxDsPeriodoS'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Setor=Setor'
      'Funci=Funci'
      'DataX=DataX'
      'Tipo=Tipo'
      'CliOrig=CliOrig'
      'Insumo=Insumo'
      'Peso=Peso'
      'Valor=Valor'
      'Empresa=Empresa'
      'NOMEPQ=NOMEPQ'
      'NO_FUNCI=NO_FUNCI'
      'NO_EMP=NO_EMP'
      'DataX_TXT=DataX_TXT'
      'NO_SETOR=NO_SETOR')
    DataSet = QrPeriodoS
    BCDToCurrency = False
    DataSetOptions = []
    Left = 464
    Top = 228
  end
  object frxUSO_CONSU_005_S: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.850269456000000000
    ReportOptions.LastChange = 39717.850269456000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxUSO_CONSU_005_AGetValue
    Left = 468
    Top = 276
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPeriodoS
        DataSetName = 'frxDsPeriodoS'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 113.385887800000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 30.236240000000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 64.850340000000000000
          Width = 35.559060000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 37.779530000000000000
          Top = 64.850340000000000000
          Width = 174.929190000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133946140000000000
          Top = 94.488176770000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826847320000000000
          Top = 94.488176770000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo R$')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 212.708720000000000000
          Top = 64.850340000000000000
          Width = 25.763760000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setor:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 238.472480000000000000
          Top = 64.850340000000000000
          Width = 124.724404570000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_SETOR]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 368.047310000000000000
          Top = 64.850340000000000000
          Width = 41.763760000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Entidade:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 409.811070000000000000
          Top = 64.850340000000000000
          Width = 264.566929130000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_FUNCI]')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 136.062992125984300000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 536.692913385826800000
          Top = 18.897650000000000000
          Width = 136.062992125984300000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Empresa: [VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 94.488250000000000000
          Width = 529.134151180000000000
          Height = 18.897637800000000000
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TIT_FLD]')
          ParentFont = False
        end
      end
      object MD_01: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPeriodoS
        DataSetName = 'frxDsPeriodoS'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826847320000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DataField = 'Valor'
          DataSet = frxDsPeriodoS
          DataSetName = 'frxDsPeriodoS'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPeriodoS."Valor"]')
          ParentFont = False
        end
        object MD01_ME01: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118037010000000000
          Width = 514.015933540000000000
          Height = 18.897637800000000000
          DataSet = frxDsPeriodoS
          DataSetName = 'frxDsPeriodoS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPeriodoS."?????"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133946140000000000
          Width = 56.692901180000000000
          Height = 18.897637800000000000
          DataField = 'Peso'
          DataSet = frxDsPeriodoS
          DataSetName = 'frxDsPeriodoS'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPeriodoS."Peso"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 483.779840000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 438.425480000000000000
        Width = 680.315400000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Width = 529.134029130000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827101180000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodoS."Valor">,MD_01)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodoS."Peso">,MD_01)]')
          ParentFont = False
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 192.756030000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPeriodoS."DataX"'
        object Me01_GH01: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315302360000000000
          Height = 18.897637800000000000
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPeriodo."?????"]')
          ParentFont = False
        end
      end
      object GF_02: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        object Me01_GF02: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 521.575042360000000000
          Height = 18.897637800000000000
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: [frxDsPeriodo."?????"]  ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827101180000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodoS."Valor">,MD_01)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodoS."Peso">,MD_01)]')
          ParentFont = False
        end
      end
      object GH_02: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPeriodoS."Setor"'
        object Me01_GH02: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 672.756242360000000000
          Height = 18.897637800000000000
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPeriodo."?????"]')
          ParentFont = False
        end
      end
      object GF_01: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Me01_GF01: TfrxMemoView
          AllowVectorExport = True
          Width = 529.134102360000000000
          Height = 18.897637800000000000
          DataSet = frxDsPeriodoA
          DataSetName = 'frxDsPeriodoA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: [frxDsPeriodo."?????"]  ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827101180000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodoS."Valor">,MD_01)]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPeriodoS."Peso">,MD_01)]')
          ParentFont = False
        end
      end
    end
  end
end
