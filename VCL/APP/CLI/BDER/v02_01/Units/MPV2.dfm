object FmMPV2: TFmMPV2
  Left = 368
  Top = 168
  Caption = 'VEN-COURO-201 :: Venda de Couros'
  ClientHeight = 857
  ClientWidth = 1100
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelIts: TPanel
    Left = 0
    Top = 118
    Width = 1100
    Height = 739
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    Visible = False
    object Painel1: TPanel
      Left = 1
      Top = 1
      Width = 1009
      Height = 468
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      BevelOuter = bvNone
      TabOrder = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 635
        Height = 468
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label7: TLabel
          Left = 10
          Top = 5
          Width = 72
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Mercadoria:'
        end
        object Label40: TLabel
          Left = 10
          Top = 54
          Width = 165
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Est'#225'gio de processamento:'
        end
        object SpeedButton6: TSpeedButton
          Left = 601
          Top = 25
          Width = 25
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
        end
        object SpeedButton7: TSpeedButton
          Left = 601
          Top = 74
          Width = 25
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
        end
        object EdMP: TdmkEditCB
          Left = 10
          Top = 25
          Width = 65
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBMP
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBMP: TdmkDBLookupComboBox
          Left = 78
          Top = 25
          Width = 519
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsMPs
          TabOrder = 1
          dmkEditCB = EdMP
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdEstagio: TdmkEditCB
          Left = 10
          Top = 74
          Width = 65
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEstagio
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEstagio: TdmkDBLookupComboBox
          Left = 78
          Top = 74
          Width = 519
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsMPEstagios
          TabOrder = 3
          dmkEditCB = EdEstagio
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object RGGrandeza: TRadioGroup
          Left = 10
          Top = 108
          Width = 616
          Height = 56
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Grandeza de venda deste item: '
          Columns = 6
          ItemIndex = 0
          Items.Strings = (
            'N/D'
            'Pe'#231'as'
            'Peso kg'
            'Peso ton'
            #193'rea m'#178
            #193'rea ft'#178)
          TabOrder = 4
        end
        object GroupBox2: TGroupBox
          Left = 320
          Top = 167
          Width = 306
          Height = 75
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Peso e '#225'rea: '
          TabOrder = 6
          object Label34: TLabel
            Left = 10
            Top = 20
            Width = 53
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Peso kg:'
          end
          object Label8: TLabel
            Left = 113
            Top = 20
            Width = 50
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = #193'rea m'#178':'
          end
          object Label35: TLabel
            Left = 207
            Top = 20
            Width = 45
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = #193'rea ft'#178':'
          end
          object dmkEdPeso: TdmkEdit
            Left = 10
            Top = 39
            Width = 100
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdM2: TdmkEdit
            Left = 113
            Top = 39
            Width = 89
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdP2: TdmkEdit
            Left = 207
            Top = 39
            Width = 88
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GroupBox3: TGroupBox
          Left = 10
          Top = 246
          Width = 616
          Height = 75
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Valores de venda: '
          TabOrder = 7
          object Label11: TLabel
            Left = 10
            Top = 20
            Width = 98
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pre'#231'o / unidade:'
          end
          object Label12: TLabel
            Left = 118
            Top = 20
            Width = 78
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor l'#237'quido:'
          end
          object Label31: TLabel
            Left = 226
            Top = 20
            Width = 50
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '% ICMS:'
          end
          object Label36: TLabel
            Left = 310
            Top = 20
            Width = 70
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor ICMS:'
          end
          object Label37: TLabel
            Left = 502
            Top = 20
            Width = 53
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor IPI:'
          end
          object Label39: TLabel
            Left = 418
            Top = 20
            Width = 33
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '% IPI:'
          end
          object dmkEdPreco: TdmkEdit
            Left = 10
            Top = 39
            Width = 103
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdTotal: TdmkEdit
            Left = 118
            Top = 39
            Width = 104
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdICMS_Per: TdmkEdit
            Left = 226
            Top = 39
            Width = 79
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdICMS_Val: TdmkEdit
            Left = 310
            Top = 39
            Width = 104
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdIPI_Per: TdmkEdit
            Left = 418
            Top = 39
            Width = 79
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdIPI_Val: TdmkEdit
            Left = 502
            Top = 39
            Width = 104
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object CkContinuar: TCheckBox
          Left = 10
          Top = 325
          Width = 149
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Continuar inserindo.'
          TabOrder = 8
        end
        object GroupBox1: TGroupBox
          Left = 10
          Top = 167
          Width = 306
          Height = 75
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Pe'#231'as: '
          TabOrder = 5
          object Label32: TLabel
            Left = 10
            Top = 20
            Width = 55
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Unidade:'
          end
          object Label33: TLabel
            Left = 202
            Top = 20
            Width = 73
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade:'
          end
          object SpeedButton5: TSpeedButton
            Left = 170
            Top = 39
            Width = 26
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
          end
          object CBUnidade: TdmkDBLookupComboBox
            Left = 59
            Top = 39
            Width = 110
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsDefPecas
            TabOrder = 1
            dmkEditCB = EdUnidade
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdUnidade: TdmkEditCB
            Left = 10
            Top = 39
            Width = 49
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBUnidade
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object dmkEdPecas: TdmkEdit
            Left = 202
            Top = 39
            Width = 95
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
      object Panel2: TPanel
        Left = 635
        Top = 0
        Width = 374
        Height = 468
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 1
        object StaticText1: TStaticText
          Left = 1
          Top = 1
          Width = 372
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          Caption = 'Baixa de couros'#13#10'Por marca ?'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -20
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object DBGrid2: TDBGrid
          Left = 1
          Top = 60
          Width = 372
          Height = 407
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          Align = alClient
          DataSource = DsMPV2Bxa
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
    object GroupBox6: TGroupBox
      Left = 0
      Top = 661
      Width = 1100
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object Panel13: TPanel
        Left = 2
        Top = 18
        Width = 1096
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel17: TPanel
          Left = 919
          Top = 0
          Width = 177
          Height = 58
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste2: TBitBtn
            Tag = 15
            Left = 10
            Top = 4
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesiste2Click
          end
        end
        object BtConfirma2: TBitBtn
          Tag = 14
          Left = 10
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirma2Click
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 118
    Width = 1100
    Height = 739
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 55
      Width = 1100
      Height = 370
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 10
        Top = 5
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 10
        Top = 54
        Width = 44
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente:'
      end
      object Label5: TLabel
        Left = 128
        Top = 5
        Width = 32
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data:'
      end
      object Label22: TLabel
        Left = 10
        Top = 103
        Width = 63
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Vendedor:'
      end
      object Label23: TLabel
        Left = 502
        Top = 103
        Width = 79
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Comiss'#227'o %:'
      end
      object LaDescoExtra: TLabel
        Left = 502
        Top = 54
        Width = 78
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '$ desc. extra:'
        Enabled = False
      end
      object Label25: TLabel
        Left = 487
        Top = 5
        Width = 66
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nota fiscal:'
      end
      object Label30: TLabel
        Left = 10
        Top = 187
        Width = 431
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Observa'#231#245'es que ser'#227'o impressas para o cliente (no pedido de ven' +
          'da):'
      end
      object Label38: TLabel
        Left = 10
        Top = 276
        Width = 485
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Observa'#231#245'es internas de uso da empresa (n'#227'o ser'#225' impresso de neh' +
          'uma forma):'
      end
      object Label61: TLabel
        Left = 369
        Top = 5
        Width = 57
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '$ Seguro:'
      end
      object Label62: TLabel
        Left = 251
        Top = 5
        Width = 109
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Desp. acess'#243'rias:'
      end
      object EdCodigo: TdmkEdit
        Left = 10
        Top = 25
        Width = 114
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 10
        Top = 74
        Width = 65
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 79
        Top = 74
        Width = 419
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientes
        TabOrder = 6
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPDataF: TDateTimePicker
        Left = 128
        Top = 25
        Width = 119
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 38579.624225613400000000
        Time = 38579.624225613400000000
        TabOrder = 1
      end
      object EdVendedor: TdmkEditCB
        Left = 10
        Top = 123
        Width = 65
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBVendedor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBVendedor: TdmkDBLookupComboBox
        Left = 78
        Top = 123
        Width = 420
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsVendedores
        TabOrder = 9
        dmkEditCB = EdVendedor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object GroupBox4: TGroupBox
        Left = 596
        Top = 5
        Width = 493
        Height = 173
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Transportadora:'
        TabOrder = 11
        object Label41: TLabel
          Left = 10
          Top = 49
          Width = 68
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'N'#186' conhec.:'
        end
        object Label29: TLabel
          Left = 89
          Top = 49
          Width = 56
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Volumes:'
        end
        object Label43: TLabel
          Left = 153
          Top = 49
          Width = 38
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Placa:'
        end
        object Label44: TLabel
          Left = 241
          Top = 49
          Width = 21
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'UF:'
        end
        object Label45: TLabel
          Left = 281
          Top = 49
          Width = 61
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'kg l'#237'quido:'
        end
        object Label46: TLabel
          Left = 384
          Top = 49
          Width = 52
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'kg Bruto:'
        end
        object Label47: TLabel
          Left = 10
          Top = 98
          Width = 53
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Esp'#233'cie:'
        end
        object Label48: TLabel
          Left = 158
          Top = 98
          Width = 41
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Marca:'
        end
        object Label49: TLabel
          Left = 271
          Top = 98
          Width = 51
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'N'#250'mero:'
        end
        object Label3: TLabel
          Left = 384
          Top = 98
          Width = 61
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Frete N.F.:'
        end
        object CkCobraFrete: TCheckBox
          Left = 10
          Top = 148
          Width = 164
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cobra o frete do cliente.'
          TabOrder = 12
        end
        object EdTransp: TdmkEditCB
          Left = 10
          Top = 20
          Width = 65
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBTransp
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBTransp: TdmkDBLookupComboBox
          Left = 78
          Top = 20
          Width = 406
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsTransp
          TabOrder = 1
          dmkEditCB = EdTransp
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdPlaca: TEdit
          Left = 153
          Top = 69
          Width = 85
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          MaxLength = 9
          TabOrder = 4
        end
        object EdUFPlaca: TEdit
          Left = 241
          Top = 69
          Width = 36
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          MaxLength = 2
          TabOrder = 5
        end
        object dmkEdkgBruto: TdmkEdit
          Left = 281
          Top = 69
          Width = 99
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object dmkEdkgLiqui: TdmkEdit
          Left = 384
          Top = 69
          Width = 100
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object dmkEdConhecim: TdmkEdit
          Left = 10
          Top = 69
          Width = 75
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdVolumes: TdmkEdit
          Left = 89
          Top = 69
          Width = 60
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdEspecie: TEdit
          Left = 10
          Top = 118
          Width = 144
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 8
        end
        object EdMarca: TEdit
          Left = 158
          Top = 118
          Width = 108
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 9
        end
        object EdNumero: TEdit
          Left = 271
          Top = 118
          Width = 108
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 10
        end
        object dmkEdFreteNF: TdmkEdit
          Left = 384
          Top = 118
          Width = 100
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object Memo1: TMemo
        Left = 10
        Top = 207
        Width = 1079
        Height = 63
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 254
        TabOrder = 12
      end
      object Memo2: TMemo
        Left = 10
        Top = 295
        Width = 1079
        Height = 63
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 254
        TabOrder = 13
      end
      object dmkEdComissV_Per: TdmkEdit
        Left = 502
        Top = 123
        Width = 85
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object dmkEdDescoExtra: TdmkEdit
        Left = 502
        Top = 74
        Width = 85
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object dmkEdNF: TdmkEdit
        Left = 487
        Top = 25
        Width = 100
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdSeguro: TdmkEdit
        Left = 369
        Top = 25
        Width = 113
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object dmkEdDesp_Acess: TdmkEdit
        Left = 251
        Top = 25
        Width = 113
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CkCobraSegur: TCheckBox
        Left = 10
        Top = 158
        Width = 198
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cobra o seguro do cliente.'
        TabOrder = 14
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 661
      Width = 1100
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object Panel16: TPanel
        Left = 2
        Top = 18
        Width = 1096
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 919
          Top = 0
          Width = 177
          Height = 58
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 10
            Top = 4
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 10
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
    object Panel18: TPanel
      Left = 0
      Top = 0
      Width = 1100
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Label13: TLabel
        Left = 10
        Top = 5
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 10
        Top = 25
        Width = 65
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 79
        Top = 25
        Width = 503
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 118
    Width = 1100
    Height = 739
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 290
      Width = 1100
      Height = 7
      Cursor = crVSplit
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
    end
    object Splitter2: TSplitter
      Left = 0
      Top = 366
      Width = 1100
      Height = 6
      Cursor = crVSplit
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
    end
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1100
      Height = 290
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 1100
        Height = 290
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PageControl1: TPageControl
          Left = 0
          Top = 207
          Width = 1100
          Height = 83
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet1
          Align = alClient
          TabOrder = 0
          object TabSheet1: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Marcas do item selecionado'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 1092
              Height = 52
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Caption = 'Panel7'
              TabOrder = 0
              object DBGrid3: TDBGrid
                Left = 1
                Top = 1
                Width = 1090
                Height = 50
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsMPV2Bxa
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -15
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Marca'
                    Width = 86
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Peso'
                    Width = 76
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'M2'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'P2'
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet2: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'Observa'#231#245'es que ser'#227'o impressas para o cliente (no pedido de ven' +
              'da)'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBMemo1: TDBMemo
              Left = 0
              Top = 0
              Width = 1090
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataField = 'Obs'
              DataSource = DsMPV2
              TabOrder = 0
            end
          end
          object TabSheet3: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'Observa'#231#245'es internas de uso da empresa (n'#227'o ser'#225' impresso de neh' +
              'uma forma)'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBMemo2: TDBMemo
              Left = 0
              Top = 0
              Width = 1090
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataField = 'Obz'
              DataSource = DsMPV2
              TabOrder = 0
            end
          end
        end
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 1100
          Height = 207
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 1
          object Label1: TLabel
            Left = 10
            Top = 5
            Width = 47
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'C'#243'digo:'
          end
          object Label2: TLabel
            Left = 10
            Top = 54
            Width = 44
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente:'
          end
          object Label4: TLabel
            Left = 79
            Top = 103
            Width = 63
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vendedor:'
          end
          object Label54: TLabel
            Left = 10
            Top = 153
            Width = 73
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade:'
            FocusControl = DBEdit8
          end
          object Label55: TLabel
            Left = 89
            Top = 153
            Width = 78
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Faturamento:'
            FocusControl = DBEdit9
          end
          object Label6: TLabel
            Left = 128
            Top = 5
            Width = 32
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data:'
          end
          object Label17: TLabel
            Left = 251
            Top = 5
            Width = 109
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Desp. acess'#243'rias:'
          end
          object Label18: TLabel
            Left = 369
            Top = 5
            Width = 57
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '$ Seguro:'
          end
          object Label19: TLabel
            Left = 487
            Top = 5
            Width = 66
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Nota fiscal:'
          end
          object Label63: TLabel
            Left = 502
            Top = 54
            Width = 78
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '$ desc. extra:'
            Enabled = False
          end
          object Label64: TLabel
            Left = 423
            Top = 103
            Width = 79
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Comiss'#227'o %:'
          end
          object Label60: TLabel
            Left = 482
            Top = 153
            Width = 53
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'SALDO*:'
            FocusControl = DBEdit29
          end
          object Label59: TLabel
            Left = 399
            Top = 153
            Width = 69
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Frete pago:'
            FocusControl = DBEdit20
          end
          object Label56: TLabel
            Left = 192
            Top = 153
            Width = 56
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Sub-total:'
            FocusControl = DBEdit14
          end
          object Label57: TLabel
            Left = 871
            Top = 182
            Width = 221
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '* Todas receitas menos o frete pago.'
          end
          object Label58: TLabel
            Left = 295
            Top = 153
            Width = 61
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Total N.F.:'
            FocusControl = DBEdit30
          end
          object Label14: TLabel
            Left = 10
            Top = 103
            Width = 37
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cli.Int.:'
            FocusControl = DBEdit31
          end
          object DBEdit4: TDBEdit
            Left = 10
            Top = 25
            Width = 114
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Codigo'
            DataSource = DsMPV2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8281908
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object DBEdit5: TDBEdit
            Left = 10
            Top = 74
            Width = 65
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Cliente'
            DataSource = DsMPV2
            TabOrder = 1
          end
          object DBEdit10: TDBEdit
            Left = 79
            Top = 123
            Width = 65
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Vendedor'
            DataSource = DsMPV2
            TabOrder = 2
          end
          object DBEdit8: TDBEdit
            Left = 10
            Top = 172
            Width = 75
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Qtde'
            DataSource = DsMPV2
            TabOrder = 3
          end
          object DBEdit9: TDBEdit
            Left = 89
            Top = 172
            Width = 98
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Valor'
            DataSource = DsMPV2
            TabOrder = 4
          end
          object DBEdit3: TDBEdit
            Left = 79
            Top = 123
            Width = 341
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMEVENDEDOR'
            DataSource = DsMPV2
            TabOrder = 5
          end
          object DBEdit1: TDBEdit
            Left = 79
            Top = 74
            Width = 419
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMECLIENTE'
            DataSource = DsMPV2
            TabOrder = 6
          end
          object DBEdit7: TDBEdit
            Left = 128
            Top = 25
            Width = 119
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'DataF'
            DataSource = DsMPV2
            TabOrder = 7
          end
          object DBEdit18: TDBEdit
            Left = 251
            Top = 25
            Width = 115
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Desp_Acess'
            DataSource = DsMPV2
            TabOrder = 8
          end
          object DBEdit17: TDBEdit
            Left = 369
            Top = 25
            Width = 115
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Seguro'
            DataSource = DsMPV2
            TabOrder = 9
          end
          object DBEdit16: TDBEdit
            Left = 487
            Top = 25
            Width = 100
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NF'
            DataSource = DsMPV2
            TabOrder = 10
          end
          object DBEdit13: TDBEdit
            Left = 502
            Top = 74
            Width = 85
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'DescoExtra'
            DataSource = DsMPV2
            TabOrder = 11
          end
          object DBEdit11: TDBEdit
            Left = 423
            Top = 123
            Width = 61
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ComissV_Per'
            DataSource = DsMPV2
            TabOrder = 12
          end
          object DBEdit12: TDBEdit
            Left = 487
            Top = 123
            Width = 99
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ComissV_Val'
            DataSource = DsMPV2
            TabOrder = 13
          end
          object DBEdit29: TDBEdit
            Left = 482
            Top = 172
            Width = 104
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'RECEITAREAL'
            DataSource = DsMPV2
            TabOrder = 14
          end
          object DBEdit20: TDBEdit
            Left = 399
            Top = 172
            Width = 80
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'FretePg'
            DataSource = DsMPV2
            TabOrder = 15
          end
          object DBEdit14: TDBEdit
            Left = 192
            Top = 172
            Width = 98
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'SUBTOTAL'
            DataSource = DsMPV2
            TabOrder = 16
          end
          object DBCheckBox2: TDBCheckBox
            Left = 606
            Top = 180
            Width = 183
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cobra o seguro do cliente.'
            DataField = 'CobraSegur'
            DataSource = DsMPV2
            TabOrder = 17
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object GroupBox5: TGroupBox
            Left = 596
            Top = 5
            Width = 493
            Height = 173
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Transportadora:'
            TabOrder = 18
            object Label20: TLabel
              Left = 10
              Top = 49
              Width = 68
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'N'#186' conhec.:'
            end
            object Label21: TLabel
              Left = 89
              Top = 49
              Width = 56
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Volumes:'
            end
            object Label26: TLabel
              Left = 153
              Top = 49
              Width = 38
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Placa:'
            end
            object Label27: TLabel
              Left = 241
              Top = 49
              Width = 21
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'UF:'
            end
            object Label28: TLabel
              Left = 281
              Top = 49
              Width = 61
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'kg l'#237'quido:'
            end
            object Label42: TLabel
              Left = 384
              Top = 49
              Width = 52
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'kg Bruto:'
            end
            object Label50: TLabel
              Left = 10
              Top = 98
              Width = 53
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Esp'#233'cie:'
            end
            object Label51: TLabel
              Left = 158
              Top = 98
              Width = 41
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Marca:'
            end
            object Label52: TLabel
              Left = 271
              Top = 98
              Width = 51
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'N'#250'mero:'
            end
            object Label53: TLabel
              Left = 384
              Top = 98
              Width = 61
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Frete N.F.:'
            end
            object DBEdit2: TDBEdit
              Left = 79
              Top = 20
              Width = 403
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NOMETRANSP'
              DataSource = DsMPV2
              TabOrder = 0
            end
            object DBEdit6: TDBEdit
              Left = 10
              Top = 20
              Width = 65
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Transp'
              DataSource = DsMPV2
              TabOrder = 1
            end
            object DBEdit15: TDBEdit
              Left = 89
              Top = 69
              Width = 60
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Volumes'
              DataSource = DsMPV2
              TabOrder = 2
            end
            object DBEdit19: TDBEdit
              Left = 10
              Top = 69
              Width = 75
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Conhecim'
              DataSource = DsMPV2
              TabOrder = 3
            end
            object DBEdit21: TDBEdit
              Left = 384
              Top = 118
              Width = 100
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'FreteNF'
              DataSource = DsMPV2
              TabOrder = 4
            end
            object DBEdit22: TDBEdit
              Left = 153
              Top = 69
              Width = 85
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Placa'
              DataSource = DsMPV2
              TabOrder = 5
            end
            object DBEdit23: TDBEdit
              Left = 241
              Top = 69
              Width = 36
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'UFPlaca'
              DataSource = DsMPV2
              TabOrder = 6
            end
            object DBEdit24: TDBEdit
              Left = 10
              Top = 118
              Width = 144
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Especie'
              DataSource = DsMPV2
              TabOrder = 7
            end
            object DBEdit25: TDBEdit
              Left = 158
              Top = 118
              Width = 109
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Marca'
              DataSource = DsMPV2
              TabOrder = 8
            end
            object DBEdit26: TDBEdit
              Left = 271
              Top = 118
              Width = 108
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Numero'
              DataSource = DsMPV2
              TabOrder = 9
            end
            object DBEdit27: TDBEdit
              Left = 281
              Top = 69
              Width = 99
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'kgLiqui'
              DataSource = DsMPV2
              TabOrder = 10
            end
            object DBEdit28: TDBEdit
              Left = 384
              Top = 69
              Width = 100
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'kgBruto'
              DataSource = DsMPV2
              TabOrder = 11
            end
            object DBCheckBox1: TDBCheckBox
              Left = 10
              Top = 148
              Width = 183
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Cobra o frete do cliente.'
              DataField = 'CobraFrete'
              DataSource = DsMPV2
              TabOrder = 12
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
          end
          object DBEdit30: TDBEdit
            Left = 295
            Top = 172
            Width = 100
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'TOTALNOTA'
            DataSource = DsMPV2
            TabOrder = 19
          end
          object DBEdit31: TDBEdit
            Left = 10
            Top = 123
            Width = 65
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'EntInt'
            DataSource = DsMPV2
            TabOrder = 20
          end
        end
      end
    end
    object DBGrid4: TDBGrid
      Left = 0
      Top = 297
      Width = 1100
      Height = 69
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      DataSource = DsMPV2Its
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -15
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEMP'
          Title.Caption = 'Artigo'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEESTAGIO'
          Title.Caption = 'Est'#225'gio'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEGRANDEZA'
          Title.Caption = 'Grandeza'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Title.Caption = 'Quantidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Caption = 'Pre'#231'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ICMS_Per'
          Title.Caption = '% ICMS'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ICMS_Val'
          Title.Caption = '$ ICMS'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IPI_Per'
          Title.Caption = '% IPI'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IPI_Val'
          Title.Caption = '$ IPI'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'M2'
          Title.Caption = 'm'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'P2'
          Title.Caption = 'ft'#178
          Visible = True
        end>
    end
    object Panel9: TPanel
      Left = 0
      Top = 470
      Width = 1100
      Height = 112
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'Panel9'
      TabOrder = 2
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 714
        Height = 112
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'Panel10'
        TabOrder = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 20
          Width = 714
          Height = 92
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsEmissC
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 300
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'COMPENSA_TXT'
              Title.Alignment = taCenter
              Title.Caption = 'Compensado'
              Width = 72
              Visible = True
            end>
        end
        object StaticText3: TStaticText
          Left = 0
          Top = 0
          Width = 124
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Alignment = taCenter
          Caption = 'Itens de faturamento'
          TabOrder = 1
        end
      end
      object Panel11: TPanel
        Left = 714
        Top = 0
        Width = 386
        Height = 112
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = 'Panel10'
        TabOrder = 1
        object DBGrid5: TDBGrid
          Left = 1
          Top = 21
          Width = 384
          Height = 90
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsEmissT
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 300
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'COMPENSA_TXT'
              Title.Alignment = taCenter
              Title.Caption = 'Compensado'
              Width = 72
              Visible = True
            end>
        end
        object StaticText4: TStaticText
          Left = 1
          Top = 1
          Width = 164
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Alignment = taCenter
          Caption = 'Itens de duplicatas de frete'
          TabOrder = 1
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 661
      Width = 1100
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 3
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 35
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 457
        Top = 18
        Width = 641
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel4: TPanel
          Left = 478
          Top = 0
          Width = 163
          Height = 58
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 4
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 118
          Top = 4
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtImprime: TBitBtn
          Tag = 5
          Left = 231
          Top = 4
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'I&mprime'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 345
          Top = 4
          Width = 110
          Height = 49
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
        end
      end
    end
    object GBTrava: TGroupBox
      Left = 0
      Top = 582
      Width = 1100
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 4
      Visible = False
      object Panel12: TPanel
        Left = 2
        Top = 18
        Width = 1096
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel14: TPanel
          Left = 919
          Top = 0
          Width = 177
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
        end
        object BtTrava: TBitBtn
          Tag = 14
          Left = 10
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Trava'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtTravaClick
        end
        object BtItem: TBitBtn
          Tag = 224
          Left = 236
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ite&m'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItemClick
        end
        object BtBaixa: TBitBtn
          Tag = 224
          Left = 350
          Top = 5
          Width = 110
          Height = 49
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Baixa'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtBaixaClick
        end
        object BtPagamento: TBitBtn
          Tag = 20
          Left = 463
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Pagto'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtPagamentoClick
        end
        object BtFrete: TBitBtn
          Tag = 20
          Left = 576
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Frete'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtFreteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1100
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 1041
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 775
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 247
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Venda de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 247
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Venda de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 247
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Venda de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 64
    Width = 1100
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel15: TPanel
      Left = 2
      Top = 18
      Width = 1096
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsMPV2: TDataSource
    DataSet = QrMPV2
    Left = 492
    Top = 9
  end
  object QrMPV2: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrMPV2BeforeOpen
    AfterScroll = QrMPV2AfterScroll
    SQL.Strings = (
      'SELECT '
      'CASE WHEN ve.Tipo=0 THEN ve.RazaoSocial'
      'ELSE ve.Nome END NOMEVENDEDOR, '
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMECLIENTE, '
      'CASE WHEN tr.Tipo=0 THEN tr.RazaoSocial'
      'ELSE tr.Nome END NOMETRANSP,'
      'mpv2.* '
      'FROM mpv2 mpv2'
      'LEFT JOIN entidades cl ON cl.Codigo=mpv2.Cliente'
      'LEFT JOIN entidades tr ON tr.Codigo=mpv2.Transp'
      'LEFT JOIN entidades ve ON ve.Codigo=mpv2.Vendedor'
      'WHERE mpv2.Codigo > 0')
    Left = 464
    Top = 9
    object QrMPV2NOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrMPV2NOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrMPV2NOMEVENDEDOR: TWideStringField
      FieldName = 'NOMEVENDEDOR'
      Size = 100
    end
    object QrMPV2Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mpv2.Codigo'
      DisplayFormat = '000000'
    end
    object QrMPV2Cliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'mpv2.Cliente'
    end
    object QrMPV2Transp: TIntegerField
      FieldName = 'Transp'
      Origin = 'mpv2.Transp'
    end
    object QrMPV2DataF: TDateField
      FieldName = 'DataF'
      Origin = 'mpv2.DataF'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrMPV2Lk: TIntegerField
      FieldName = 'Lk'
      Origin = 'mpv2.Lk'
    end
    object QrMPV2DataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'mpv2.DataCad'
    end
    object QrMPV2DataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'mpv2.DataAlt'
    end
    object QrMPV2UserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'mpv2.UserCad'
    end
    object QrMPV2UserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'mpv2.UserAlt'
    end
    object QrMPV2Qtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'mpv2.Qtde'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPV2Valor: TFloatField
      FieldName = 'Valor'
      Origin = 'mpv2.Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPV2Vendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'mpv2.Vendedor'
    end
    object QrMPV2ComissV_Per: TFloatField
      FieldName = 'ComissV_Per'
      Origin = 'mpv2.ComissV_Per'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrMPV2ComissV_Val: TFloatField
      FieldName = 'ComissV_Val'
      Origin = 'mpv2.ComissV_Val'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPV2DescoExtra: TFloatField
      FieldName = 'DescoExtra'
      Origin = 'mpv2.DescoExtra'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPV2Volumes: TIntegerField
      FieldName = 'Volumes'
      Origin = 'mpv2.Volumes'
    end
    object QrMPV2Obs: TWideStringField
      FieldName = 'Obs'
      Origin = 'mpv2.Obs'
      Size = 255
    end
    object QrMPV2Obz: TWideStringField
      FieldName = 'Obz'
      Origin = 'mpv2.Obz'
      Size = 255
    end
    object QrMPV2AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'mpv2.AlterWeb'
    end
    object QrMPV2Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'mpv2.Ativo'
    end
    object QrMPV2NF: TIntegerField
      FieldName = 'NF'
      Origin = 'mpv2.NF'
      DisplayFormat = '000000;-000000; '
    end
    object QrMPV2Seguro: TFloatField
      FieldName = 'Seguro'
      Origin = 'mpv2.Seguro'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPV2Desp_Acess: TFloatField
      FieldName = 'Desp_Acess'
      Origin = 'mpv2.Desp_Acess'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPV2Conhecim: TIntegerField
      FieldName = 'Conhecim'
      Origin = 'mpv2.Conhecim'
      DisplayFormat = '000000;-000000; '
    end
    object QrMPV2CobraFrete: TSmallintField
      FieldName = 'CobraFrete'
      Origin = 'mpv2.CobraFrete'
    end
    object QrMPV2FretePg: TFloatField
      FieldName = 'FretePg'
      Origin = 'mpv2.FretePg'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPV2FreteNF: TFloatField
      FieldName = 'FreteNF'
      Origin = 'mpv2.FreteNF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPV2Placa: TWideStringField
      FieldName = 'Placa'
      Origin = 'mpv2.Placa'
      Size = 9
    end
    object QrMPV2UFPlaca: TWideStringField
      FieldName = 'UFPlaca'
      Origin = 'mpv2.UFPlaca'
      Size = 2
    end
    object QrMPV2Especie: TWideStringField
      FieldName = 'Especie'
      Origin = 'mpv2.Especie'
      Size = 30
    end
    object QrMPV2Marca: TWideStringField
      FieldName = 'Marca'
      Origin = 'mpv2.Marca'
      Size = 30
    end
    object QrMPV2Numero: TWideStringField
      FieldName = 'Numero'
      Origin = 'mpv2.Numero'
      Size = 30
    end
    object QrMPV2kgBruto: TFloatField
      FieldName = 'kgBruto'
      Origin = 'mpv2.kgBruto'
      DisplayFormat = '#,###,##0.000'
    end
    object QrMPV2kgLiqui: TFloatField
      FieldName = 'kgLiqui'
      Origin = 'mpv2.kgLiqui'
      DisplayFormat = '#,###,##0.000'
    end
    object QrMPV2CobraSegur: TSmallintField
      FieldName = 'CobraSegur'
      Origin = 'mpv2.CobraSegur'
    end
    object QrMPV2Pago: TFloatField
      FieldName = 'Pago'
      Origin = 'mpv2.Pago'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPV2ICMS_Val: TFloatField
      FieldName = 'ICMS_Val'
      Origin = 'mpv2.ICMS_Val'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPV2IPI_Val: TFloatField
      FieldName = 'IPI_Val'
      Origin = 'mpv2.IPI_Val'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPV2SUBTOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUBTOTAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMPV2RECEITAREAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'RECEITAREAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMPV2TOTALNOTA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALNOTA'
      Calculated = True
    end
    object QrMPV2EntInt: TIntegerField
      FieldName = 'EntInt'
    end
    object QrMPV2CodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
  end
  object QrTransp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 193
    Top = 409
    object QrTranspCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTranspNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransp: TDataSource
    DataSet = QrTransp
    Left = 221
    Top = 409
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 753
    Top = 17
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 781
    Top = 17
  end
  object QrMPs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Preco, ICMS, IPI, PrecoICM'
      'FROM artigosgrupos'
      'ORDER BY Nome'
      '')
    Left = 137
    Top = 381
    object QrMPsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrMPsPreco: TFloatField
      FieldName = 'Preco'
    end
    object QrMPsICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrMPsIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrMPsPrecoICM: TSmallintField
      FieldName = 'PrecoICM'
    end
  end
  object DsMPs: TDataSource
    DataSet = QrMPs
    Left = 165
    Top = 381
  end
  object QrMPV2Its: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ag.CF, ag.ST, ag.CodImp, ag.Nome NOMEMP, '
      'dp.Nome NOMEUNIDADE, me.Nome NOMEESTAGIO, '
      'CASE mi.Grandeza '
      '  WHEN 1 THEN "Pe'#231'as"'
      '  WHEN 2 THEN "kg"'
      '  WHEN 3 THEN "ton"'
      '  WHEN 4 THEN "m'#178'"'
      '  WHEN 5 THEN "ft'#178'"'
      '  ELSE "N/D" END NOMEGRANDEZA,'
      ''
      'mi.* '
      'FROM mpv2its mi'
      'LEFT JOIN defpecas      dp ON dp.Codigo=mi.Unidade'
      'LEFT JOIN artigosgrupos ag ON ag.Codigo=mi.MP'
      'LEFT JOIN mpestagios    me ON me.Codigo=mi.Estagio'
      'WHERE mi.Codigo=:P0')
    Left = 520
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPV2ItsNOMEUNIDADE: TWideStringField
      FieldName = 'NOMEUNIDADE'
      Origin = 'defpecas.Nome'
      Size = 30
    end
    object QrMPV2ItsNOMEMP: TWideStringField
      FieldName = 'NOMEMP'
      Origin = 'artigosgrupos.Nome'
      Required = True
      Size = 50
    end
    object QrMPV2ItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mpv2its.Codigo'
      Required = True
    end
    object QrMPV2ItsPedido: TIntegerField
      FieldName = 'Pedido'
      Origin = 'mpv2its.Pedido'
      Required = True
    end
    object QrMPV2ItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'mpv2its.Controle'
      Required = True
    end
    object QrMPV2ItsEstagio: TIntegerField
      FieldName = 'Estagio'
      Origin = 'mpv2its.Estagio'
      Required = True
    end
    object QrMPV2ItsQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'mpv2its.Qtde'
      Required = True
    end
    object QrMPV2ItsPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'mpv2its.Preco'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPV2ItsUnidade: TSmallintField
      FieldName = 'Unidade'
      Origin = 'mpv2its.Unidade'
      Required = True
    end
    object QrMPV2ItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'mpv2its.Lk'
    end
    object QrMPV2ItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'mpv2its.DataCad'
    end
    object QrMPV2ItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'mpv2its.DataAlt'
    end
    object QrMPV2ItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'mpv2its.UserCad'
    end
    object QrMPV2ItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'mpv2its.UserAlt'
    end
    object QrMPV2ItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'mpv2its.AlterWeb'
      Required = True
    end
    object QrMPV2ItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'mpv2its.Ativo'
      Required = True
    end
    object QrMPV2ItsMP: TIntegerField
      FieldName = 'MP'
      Origin = 'mpv2its.MP'
      Required = True
    end
    object QrMPV2ItsPeso: TFloatField
      FieldName = 'Peso'
      Origin = 'mpv2its.Peso'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrMPV2ItsPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'mpv2its.Pecas'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrMPV2ItsM2: TFloatField
      FieldName = 'M2'
      Origin = 'mpv2its.M2'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPV2ItsP2: TFloatField
      FieldName = 'P2'
      Origin = 'mpv2its.P2'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPV2ItsTotal: TFloatField
      FieldName = 'Total'
      Origin = 'mpv2its.Total'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPV2ItsICMS_Per: TFloatField
      FieldName = 'ICMS_Per'
      Origin = 'mpv2its.ICMS_Per'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPV2ItsICMS_Val: TFloatField
      FieldName = 'ICMS_Val'
      Origin = 'mpv2its.ICMS_Val'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPV2ItsIPI_Per: TFloatField
      FieldName = 'IPI_Per'
      Origin = 'mpv2its.IPI_Per'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPV2ItsIPI_Val: TFloatField
      FieldName = 'IPI_Val'
      Origin = 'mpv2its.IPI_Val'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPV2ItsGrandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'mpv2its.Grandeza'
    end
    object QrMPV2ItsNOMEESTAGIO: TWideStringField
      FieldName = 'NOMEESTAGIO'
      Size = 50
    end
    object QrMPV2ItsNOMEGRANDEZA: TWideStringField
      FieldName = 'NOMEGRANDEZA'
      Required = True
      Size = 5
    end
    object QrMPV2ItsCodImp: TWideStringField
      FieldName = 'CodImp'
      Size = 10
    end
    object QrMPV2ItsCF: TWideStringField
      FieldName = 'CF'
      Size = 10
    end
    object QrMPV2ItsST: TWideStringField
      FieldName = 'ST'
      Size = 10
    end
  end
  object DsMPV2Its: TDataSource
    DataSet = QrMPV2Its
    Left = 548
    Top = 9
  end
  object QrTotais: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtde) Qtde, SUM(Total) Total,'
      'SUM(ICMS_Val) ICMS_Val, SUM(IPI_Val) IPI_Val'
      'FROM mpv2its'
      'WHERE Codigo=:P0')
    Left = 813
    Top = 17
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotaisQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrTotaisTotal: TFloatField
      FieldName = 'Total'
    end
    object QrTotaisICMS_Val: TFloatField
      FieldName = 'ICMS_Val'
    end
    object QrTotaisIPI_Val: TFloatField
      FieldName = 'IPI_Val'
    end
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOME,'
      'CASE WHEN fo.Tipo=0 THEN fo.ERua'
      'ELSE fo.PRua END RUA,'
      'CASE WHEN fo.Tipo=0 THEN fo.ENumero'
      'ELSE fo.PNumero END NUMERO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECompl'
      'ELSE fo.PCompl END COMPL,'
      'CASE WHEN fo.Tipo=0 THEN fo.EBairro'
      'ELSE fo.PBairro END BAIRRO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECidade'
      'ELSE fo.PCidade END CIDADE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EPais'
      'ELSE fo.PPais END PAIS,'
      'CASE WHEN fo.Tipo=0 THEN fo.ETe1'
      'ELSE fo.PTe1 END TELEFONE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EFax'
      'ELSE fo.PPais END FAX,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECel'
      'ELSE fo.PCel END Celular,'
      'CASE WHEN fo.Tipo=0 THEN fo.CNPJ'
      'ELSE fo.CPF END CNPJ,'
      'CASE WHEN fo.Tipo=0 THEN fo.IE'
      'ELSE fo.RG END IE,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECEP'
      'ELSE fo.PCEP END CEP,'
      'CASE WHEN fo.Tipo=0 THEN fo.EContato'
      'ELSE fo.PContato END Contato,'
      ''
      'CASE WHEN fo.Tipo=0 THEN fo.EUF'
      'ELSE fo.PUF END UF,'
      'CASE WHEN fo.Tipo=0 THEN uf1.Nome'
      'ELSE uf2.Nome END NOMEUF,'
      'EEMail, PEmail,'
      ''
      'CASE WHEN fo.Tipo=0 THEN ll1.Nome'
      'ELSE ll2.Nome END LOGRAD'
      ''
      'FROM entidades fo'
      'LEFT JOIN ufs uf1 ON uf1.Codigo=fo.Euf'
      'LEFT JOIN ufs uf2 ON uf2.Codigo=fo.Puf'
      'LEFT JOIN listalograd ll1 ON ll1.Codigo=fo.ELograd'
      'LEFT JOIN listalograd ll2 ON ll2.Codigo=fo.PLograd'
      'WHERE fo.Codigo=:P0')
    Left = 332
    Top = 380
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFornecedorFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrFornecedorCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Calculated = True
    end
    object QrFornecedorCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Calculated = True
    end
    object QrFornecedorTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrFornecedorCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrFornecedorNOME: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOME'
      Size = 100
    end
    object QrFornecedorRUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 30
    end
    object QrFornecedorCOMPL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'COMPL'
      Size = 30
    end
    object QrFornecedorBAIRRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrFornecedorCIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrFornecedorPAIS: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'PAIS'
    end
    object QrFornecedorTELEFONE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TELEFONE'
    end
    object QrFornecedorFAX: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FAX'
    end
    object QrFornecedorCelular: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Celular'
    end
    object QrFornecedorCNPJ: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrFornecedorIE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'IE'
    end
    object QrFornecedorContato: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Contato'
      Size = 60
    end
    object QrFornecedorNOMEUF: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrFornecedorEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 100
    end
    object QrFornecedorPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrFornecedorNUMEROTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMEROTXT'
      Size = 40
      Calculated = True
    end
    object QrFornecedorENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrFornecedorNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrFornecedorCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrFornecedorUF: TFloatField
      FieldName = 'UF'
    end
    object QrFornecedorLOGRAD: TWideStringField
      FieldName = 'LOGRAD'
      Size = 10
    end
  end
  object QrTransportador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOME,'
      'CASE WHEN fo.Tipo=0 THEN fo.ERua'
      'ELSE fo.PRua END RUA,'
      'CASE WHEN fo.Tipo=0 THEN fo.ENumero'
      'ELSE fo.PNumero END NUMERO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECompl'
      'ELSE fo.PCompl END COMPL,'
      'CASE WHEN fo.Tipo=0 THEN fo.EBairro'
      'ELSE fo.PBairro END BAIRRO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECidade'
      'ELSE fo.PCidade END CIDADE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EPais'
      'ELSE fo.PPais END PAIS,'
      'CASE WHEN fo.Tipo=0 THEN fo.ETe1'
      'ELSE fo.PTe1 END TELEFONE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EFax'
      'ELSE fo.PPais END FAX,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECel'
      'ELSE fo.PCel END Celular,'
      'CASE WHEN fo.Tipo=0 THEN fo.CNPJ'
      'ELSE fo.CPF END CNPJ,'
      'CASE WHEN fo.Tipo=0 THEN fo.IE'
      'ELSE fo.RG END IE,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECEP'
      'ELSE fo.PCEP END CEP,'
      'CASE WHEN fo.Tipo=0 THEN fo.EContato'
      'ELSE fo.PContato END Contato,'
      ''
      'CASE WHEN fo.Tipo=0 THEN fo.EUF'
      'ELSE fo.PUF END UF,'
      'CASE WHEN fo.Tipo=0 THEN uf1.Nome'
      'ELSE uf2.Nome END NOMEUF,'
      'EEMail, PEmail,'
      ''
      'CASE WHEN fo.Tipo=0 THEN ll1.Nome'
      'ELSE ll2.Nome END LOGRAD'
      ''
      'FROM entidades fo'
      'LEFT JOIN ufs uf1 ON uf1.Codigo=fo.Euf'
      'LEFT JOIN ufs uf2 ON uf2.Codigo=fo.Puf'
      'LEFT JOIN listalograd ll1 ON ll1.Codigo=fo.ELograd'
      'LEFT JOIN listalograd ll2 ON ll2.Codigo=fo.PLograd'
      'WHERE fo.Codigo=:P0')
    Left = 304
    Top = 380
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTransportadorTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrTransportadorCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Calculated = True
    end
    object QrTransportadorCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Calculated = True
    end
    object QrTransportadorFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrTransportadorCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrTransportadorNOME: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOME'
      Size = 100
    end
    object QrTransportadorRUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 30
    end
    object QrTransportadorCOMPL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'COMPL'
      Size = 30
    end
    object QrTransportadorBAIRRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrTransportadorCIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrTransportadorPAIS: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'PAIS'
    end
    object QrTransportadorTELEFONE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TELEFONE'
    end
    object QrTransportadorFAX: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FAX'
    end
    object QrTransportadorCelular: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Celular'
    end
    object QrTransportadorCNPJ: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTransportadorIE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'IE'
    end
    object QrTransportadorContato: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Contato'
      Size = 60
    end
    object QrTransportadorNOMEUF: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrTransportadorNUMEROTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMEROTXT'
      Size = 40
      Calculated = True
    end
    object QrTransportadorENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrTransportadorEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 100
    end
    object QrTransportadorPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrTransportadorNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrTransportadorCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrTransportadorUF: TFloatField
      FieldName = 'UF'
    end
    object QrTransportadorLOGRAD: TWideStringField
      FieldName = 'LOGRAD'
      Size = 10
    end
  end
  object QrVendedores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece6="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 361
    Top = 381
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 389
    Top = 381
  end
  object QrSumC: TMySQLQuery
    Database = Dmod.MyDB
    Left = 63
    Top = 420
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumCCredito: TFloatField
      FieldName = 'Credito'
    end
  end
  object DsEmissC: TDataSource
    DataSet = QrEmissC
    Left = 36
    Top = 420
  end
  object QrEmissC: TMySQLQuery
    Database = Dmod.MyDB
    Left = 8
    Top = 420
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmissCData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissCTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmissCCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrEmissCAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrEmissCGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrEmissCDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 128
    end
    object QrEmissCNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrEmissCDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissCCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissCCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissCDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrEmissCSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrEmissCVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissCLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmissCNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrEmissCNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 10
      Calculated = True
    end
    object QrEmissCNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 128
    end
    object QrEmissCFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrEmissCCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEmissCSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrEmissCFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrEmissCFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrEmissCID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrEmissCFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrEmissCBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrEmissCLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrEmissCCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrEmissCLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrEmissCOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrEmissCLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrEmissCPago: TFloatField
      FieldName = 'Pago'
    end
    object QrEmissCMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEmissCMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrEmissCMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrEmissCProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrEmissCDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmissCDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmissCUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmissCUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmissCDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrEmissCNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrEmissCVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrEmissCAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrEmissCCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmissCCOMPENSA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSA_TXT'
      Calculated = True
    end
    object QrEmissCVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEmissCVALOR_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VALOR_TXT'
      Size = 30
      Calculated = True
    end
    object QrEmissCQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmissCFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrEmissCForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrEmissCICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrEmissCICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrEmissCDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrEmissCDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEmissCDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrEmissCEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrEmissCContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrEmissCCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrEmissCSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrEmissCControle: TFloatField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmissCDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrEmissCDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrEmissCNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrEmissCUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrEmissCAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrEmissCFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEmissCAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object PMEdita: TPopupMenu
    Left = 504
    Top = 472
    object MenuItem1: TMenuItem
      Caption = '&Mercadoria'
    end
    object MenuItem2: TMenuItem
      Caption = '&Pagamento'
    end
  end
  object PMImpressao: TPopupMenu
    Left = 8
    Top = 8
    object ImpressoNormal1: TMenuItem
      Caption = 'Impress'#227'o &Normal'
    end
    object Impressodireta1: TMenuItem
      Caption = '&Impress'#227'o &Direta'
    end
  end
  object QrSumIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtde) Qtde, SUM(Desco) Desco,'
      'SUM(Valor) Valor, SUM(Valor) / SUM(Qtde) PRECO'
      'FROM mpv2its '
      'WHERE Codigo=:P0')
    Left = 660
    Top = 20
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumItsQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSumItsDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrSumItsValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumItsPRECO: TFloatField
      FieldName = 'PRECO'
    end
  end
  object PMExclui: TPopupMenu
    Left = 712
    Top = 460
    object MenuItem3: TMenuItem
      Caption = '&Mercadoria'
    end
    object MenuItem4: TMenuItem
      Caption = '&Pagamento'
    end
  end
  object PMItem: TPopupMenu
    Left = 188
    Top = 476
    object Incluinovamercadoria1: TMenuItem
      Caption = '&Inclui nova mercadoria'
      OnClick = Incluinovamercadoria1Click
    end
    object Alteradadosdamercadoriaatual1: TMenuItem
      Caption = '&Altera dados da mercadoria atual'
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Excluimercadoriaatual1: TMenuItem
      Caption = '&Exclui mercadoria atual'
    end
  end
  object PMPagtos: TPopupMenu
    Left = 384
    Top = 416
    object Incluinovospagamentos1: TMenuItem
      Caption = '&Inclui novo(s) pagamento(s)'
    end
    object Alterapagamentoatual1: TMenuItem
      Caption = '&Altera pagamento atual'
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excluipagamentoatual1: TMenuItem
      Caption = '&Exclui pagamento atual'
    end
    object Excluitodospagamentos1: TMenuItem
      Caption = 'E&xclui todos pagamentos'
    end
  end
  object QrDefPecas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM defpecas'
      'WHERE Grandeza=3'
      'ORDER BY Nome')
    Left = 193
    Top = 381
    object QrDefPecasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDefPecasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsDefPecas: TDataSource
    DataSet = QrDefPecas
    Left = 221
    Top = 381
  end
  object frxDsMPV2Its: TfrxDBDataset
    UserName = 'frxDsMPV2Its'
    CloseDataSource = False
    DataSet = QrMPV2Its
    BCDToCurrency = False
    Left = 689
    Top = 21
  end
  object frxOS: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39496.531362662000000000
    ReportOptions.LastChange = 39496.531362662000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 717
    Top = 21
    Datasets = <
      item
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMPV2Its
        DataSetName = 'frxDsMPV2Its'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 283.464750000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo2: TfrxMemoView
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 566.929255910000000000
          Height = 18.897650000000000000
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object frxMemoView1: TfrxMemoView
          Left = 566.929499999999900000
          Top = 207.874150000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'impresso em: [Date] [Time]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 604.724800000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 30.236240000000000000
          DataSet = frxDsMPV2Its
          DataSetName = 'frxDsMPV2Its'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'OS: [frxDsMPV2Its."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Top = 49.133889999999990000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 83.149660000000000000
          Top = 49.133889999999990000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 241.889920000000000000
          Top = 49.133889999999990000
          Width = 260.787570000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 502.677490000000000000
          Top = 49.133889999999990000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 593.386210000000000000
          Top = 49.133889999999990000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Espessura')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 665.197280000000000000
          Top = 49.133889999999990000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 173.858380000000000000
          Top = 49.133889999999990000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 37.795300000000000000
          Top = 68.031540000000010000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39',<frxDsMPV2Its."Pec' +
              'as">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 83.149660000000000000
          Top = 68.031540000000010000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPV2Its."NOMEUNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 241.889920000000000000
          Top = 68.031540000000010000
          Width = 260.787570000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPV2Its."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 502.677490000000000000
          Top = 68.031540000000010000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPV2Its."CorTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 593.386210000000000000
          Top = 68.031540000000010000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPV2Its."EspesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 665.197280000000000000
          Top = 68.031540000000010000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPV2Its."Classe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 173.858380000000000000
          Top = 68.031540000000010000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPV2Its."M2Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 37.795300000000000000
          Top = 86.929190000000000000
          Width = 718.110700000000000000
          Height = 102.047310000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPV2Its."Observ"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 37.795300000000000000
          Top = 188.976500000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data da entrega: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 151.181200000000000000
          Top = 188.976500000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPV2Its."Entrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 415.748300000000000000
          Top = 188.976500000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 468.661720000000000000
          Top = 188.976500000000000000
          Width = 287.244280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPV2Its."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object PMImprimeDireto: TPopupMenu
    Left = 569
    Top = 415
    object Notafiscalmatricial1: TMenuItem
      Caption = 'Nota fiscal (matricial)'
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Pedido1: TMenuItem
      Caption = '&Pedido (Fatura)'
    end
    object Ordemdeproduo1: TMenuItem
      Caption = '&Ordem de produ'#231#227'o'
    end
  end
  object QrMPEstagios: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM mpestagios'
      'ORDER BY Nome')
    Left = 249
    Top = 381
    object QrMPEstagiosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPEstagiosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMPEstagios: TDataSource
    DataSet = QrMPEstagios
    Left = 277
    Top = 381
  end
  object QrMPV2Bxa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mi.Marca, bx.*'
      'FROM mpv2bxa bx'
      'LEFT JOIN mpin mi ON mi.Controle=bx.Marca_ID'
      'WHERE bx.Controle=:P0')
    Left = 576
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPV2BxaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPV2BxaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMPV2BxaConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMPV2BxaEstagio: TIntegerField
      FieldName = 'Estagio'
    end
    object QrMPV2BxaMarca_ID: TIntegerField
      FieldName = 'Marca_ID'
    end
    object QrMPV2BxaPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrMPV2BxaPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrMPV2BxaM2: TFloatField
      FieldName = 'M2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPV2BxaP2: TFloatField
      FieldName = 'P2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPV2BxaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMPV2BxaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMPV2BxaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMPV2BxaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMPV2BxaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMPV2BxaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrMPV2BxaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMPV2BxaMarca: TWideStringField
      FieldName = 'Marca'
    end
  end
  object DsMPV2Bxa: TDataSource
    DataSet = QrMPV2Bxa
    Left = 604
    Top = 9
  end
  object PMBaixa: TPopupMenu
    Left = 296
    Top = 412
    object Incluibaixadecouro1: TMenuItem
      Caption = '&Inclui baixa de couro'
    end
    object Alterabaixadecouro1: TMenuItem
      Caption = '&Altera baixa de couro'
      Enabled = False
    end
    object Excluiaixadecouro1: TMenuItem
      Caption = '&Exclui baixa de couro'
    end
  end
  object PMFrete: TPopupMenu
    Left = 480
    Top = 408
    object Incluinovoitemdefrete1: TMenuItem
      Caption = '&Inclui novo item de frete'
    end
    object Alteraitemdefrete1: TMenuItem
      Caption = '&Altera item de frete'
    end
  end
  object QrEmissT: TMySQLQuery
    Database = Dmod.MyDB
    Left = 632
    Top = 416
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmissTData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissTTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmissTCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrEmissTAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrEmissTGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrEmissTDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 128
    end
    object QrEmissTNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrEmissTDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissTCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrEmissTCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissTDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrEmissTSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrEmissTVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissTLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmissTNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrEmissTNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 10
      Calculated = True
    end
    object QrEmissTNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 128
    end
    object QrEmissTFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrEmissTCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEmissTControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmissTSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrEmissTFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrEmissTFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrEmissTID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrEmissTID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrEmissTFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrEmissTBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrEmissTLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrEmissTCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrEmissTLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrEmissTOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrEmissTLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrEmissTPago: TFloatField
      FieldName = 'Pago'
    end
    object QrEmissTMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEmissTMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrEmissTMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrEmissTProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrEmissTDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmissTDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmissTUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmissTUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmissTDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrEmissTCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrEmissTNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrEmissTVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrEmissTAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrEmissTCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmissTQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmissTFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrEmissTEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrEmissTContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrEmissTCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrEmissTForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrEmissTICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrEmissTICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrEmissTDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrEmissTDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEmissTDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrEmissTDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrEmissTDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrEmissTNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrEmissTFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEmissTAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsEmissT: TDataSource
    DataSet = QrEmissT
    Left = 660
    Top = 416
  end
  object QrSumT: TMySQLQuery
    Database = Dmod.MyDB
    Left = 688
    Top = 416
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumTDebito: TFloatField
      FieldName = 'Debito'
    end
  end
end
