unit PQEXml;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, dmkEditDateTimePicker, dmkRadioGroup;

type
  TFmPQEXml = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrFisRegCad: TMySQLQuery;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadModeloNF: TIntegerField;
    QrFisRegCadFinanceiro: TSmallintField;
    QrFisRegCadGenCtbD: TIntegerField;
    QrFisRegCadGenCtbC: TIntegerField;
    DsFisRegCad: TDataSource;
    Panel3: TPanel;
    Label6: TLabel;
    Label29: TLabel;
    MeWarn: TMemo;
    MeInfo: TMemo;
    LaEmpresa: TLabel;
    Label17: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    TPEntrada: TdmkEditDateTimePicker;
    RGMadeBy: TdmkRadioGroup;
    Panel2: TPanel;
    PnMensagens: TPanel;
    QrA: TMySQLQuery;
    QrAFatID: TIntegerField;
    QrAFatNum: TIntegerField;
    QrAEmpresa: TIntegerField;
    QrAIDCtrl: TIntegerField;
    QrALoteEnv: TIntegerField;
    QrAversao: TFloatField;
    QrAId: TWideStringField;
    QrAide_cUF: TSmallintField;
    QrAide_cNF: TIntegerField;
    QrAide_natOp: TWideStringField;
    QrAide_indPag: TSmallintField;
    QrAide_mod: TSmallintField;
    QrAide_serie: TIntegerField;
    QrAide_nNF: TIntegerField;
    QrAide_dEmi: TDateField;
    QrAide_dSaiEnt: TDateField;
    QrAide_tpNF: TSmallintField;
    QrAide_cMunFG: TIntegerField;
    QrAide_tpImp: TSmallintField;
    QrAide_tpEmis: TSmallintField;
    QrAide_cDV: TSmallintField;
    QrAide_tpAmb: TSmallintField;
    QrAide_finNFe: TSmallintField;
    QrAide_procEmi: TSmallintField;
    QrAide_verProc: TWideStringField;
    QrAemit_CNPJ: TWideStringField;
    QrAemit_CPF: TWideStringField;
    QrAemit_xNome: TWideStringField;
    QrAemit_xFant: TWideStringField;
    QrAemit_xLgr: TWideStringField;
    QrAemit_nro: TWideStringField;
    QrAemit_xCpl: TWideStringField;
    QrAemit_xBairro: TWideStringField;
    QrAemit_cMun: TIntegerField;
    QrAemit_xMun: TWideStringField;
    QrAemit_UF: TWideStringField;
    QrAemit_CEP: TIntegerField;
    QrAemit_cPais: TIntegerField;
    QrAemit_xPais: TWideStringField;
    QrAemit_fone: TWideStringField;
    QrAemit_IE: TWideStringField;
    QrAemit_IEST: TWideStringField;
    QrAemit_IM: TWideStringField;
    QrAemit_CNAE: TWideStringField;
    QrAdest_CNPJ: TWideStringField;
    QrAdest_CPF: TWideStringField;
    QrAdest_xNome: TWideStringField;
    QrAdest_xLgr: TWideStringField;
    QrAdest_nro: TWideStringField;
    QrAdest_xCpl: TWideStringField;
    QrAdest_xBairro: TWideStringField;
    QrAdest_cMun: TIntegerField;
    QrAdest_xMun: TWideStringField;
    QrAdest_UF: TWideStringField;
    QrAdest_CEP: TWideStringField;
    QrAdest_cPais: TIntegerField;
    QrAdest_xPais: TWideStringField;
    QrAdest_fone: TWideStringField;
    QrAdest_IE: TWideStringField;
    QrAdest_ISUF: TWideStringField;
    QrAICMSTot_vBC: TFloatField;
    QrAICMSTot_vICMS: TFloatField;
    QrAICMSTot_vBCST: TFloatField;
    QrAICMSTot_vST: TFloatField;
    QrAICMSTot_vProd: TFloatField;
    QrAICMSTot_vFrete: TFloatField;
    QrAICMSTot_vSeg: TFloatField;
    QrAICMSTot_vDesc: TFloatField;
    QrAICMSTot_vII: TFloatField;
    QrAICMSTot_vIPI: TFloatField;
    QrAICMSTot_vPIS: TFloatField;
    QrAICMSTot_vCOFINS: TFloatField;
    QrAICMSTot_vOutro: TFloatField;
    QrAICMSTot_vNF: TFloatField;
    QrAISSQNtot_vServ: TFloatField;
    QrAISSQNtot_vBC: TFloatField;
    QrAISSQNtot_vISS: TFloatField;
    QrAISSQNtot_vPIS: TFloatField;
    QrAISSQNtot_vCOFINS: TFloatField;
    QrARetTrib_vRetPIS: TFloatField;
    QrARetTrib_vRetCOFINS: TFloatField;
    QrARetTrib_vRetCSLL: TFloatField;
    QrARetTrib_vBCIRRF: TFloatField;
    QrARetTrib_vIRRF: TFloatField;
    QrARetTrib_vBCRetPrev: TFloatField;
    QrARetTrib_vRetPrev: TFloatField;
    QrAModFrete: TSmallintField;
    QrATransporta_CNPJ: TWideStringField;
    QrATransporta_CPF: TWideStringField;
    QrATransporta_XNome: TWideStringField;
    QrATransporta_IE: TWideStringField;
    QrATransporta_XEnder: TWideStringField;
    QrATransporta_XMun: TWideStringField;
    QrATransporta_UF: TWideStringField;
    QrARetTransp_vServ: TFloatField;
    QrARetTransp_vBCRet: TFloatField;
    QrARetTransp_PICMSRet: TFloatField;
    QrARetTransp_vICMSRet: TFloatField;
    QrARetTransp_CFOP: TWideStringField;
    QrARetTransp_CMunFG: TWideStringField;
    QrAVeicTransp_Placa: TWideStringField;
    QrAVeicTransp_UF: TWideStringField;
    QrAVeicTransp_RNTC: TWideStringField;
    QrACobr_Fat_nFat: TWideStringField;
    QrACobr_Fat_vOrig: TFloatField;
    QrACobr_Fat_vDesc: TFloatField;
    QrACobr_Fat_vLiq: TFloatField;
    QrAInfAdic_InfAdFisco: TWideMemoField;
    QrAInfAdic_InfCpl: TWideMemoField;
    QrAExporta_UFEmbarq: TWideStringField;
    QrAExporta_XLocEmbarq: TWideStringField;
    QrACompra_XNEmp: TWideStringField;
    QrACompra_XPed: TWideStringField;
    QrACompra_XCont: TWideStringField;
    QrAStatus: TIntegerField;
    QrAinfProt_Id: TWideStringField;
    QrAinfProt_tpAmb: TSmallintField;
    QrAinfProt_verAplic: TWideStringField;
    QrAinfProt_dhRecbto: TDateTimeField;
    QrAinfProt_nProt: TWideStringField;
    QrAinfProt_digVal: TWideStringField;
    QrAinfProt_cStat: TIntegerField;
    QrAinfProt_xMotivo: TWideStringField;
    QrAinfCanc_Id: TWideStringField;
    QrAinfCanc_tpAmb: TSmallintField;
    QrAinfCanc_verAplic: TWideStringField;
    QrAinfCanc_dhRecbto: TDateTimeField;
    QrAinfCanc_nProt: TWideStringField;
    QrAinfCanc_digVal: TWideStringField;
    QrAinfCanc_cStat: TIntegerField;
    QrAinfCanc_xMotivo: TWideStringField;
    QrAinfCanc_cJust: TIntegerField;
    QrAinfCanc_xJust: TWideStringField;
    QrA_Ativo_: TSmallintField;
    QrAFisRegCad: TIntegerField;
    QrACartEmiss: TIntegerField;
    QrATabelaPrc: TIntegerField;
    QrACondicaoPg: TIntegerField;
    QrAFreteExtra: TFloatField;
    QrASegurExtra: TFloatField;
    QrALk: TIntegerField;
    QrADataCad: TDateField;
    QrADataAlt: TDateField;
    QrAUserCad: TIntegerField;
    QrAUserAlt: TIntegerField;
    QrAAlterWeb: TSmallintField;
    QrAAtivo: TSmallintField;
    QrAICMSRec_pRedBC: TFloatField;
    QrAICMSRec_vBC: TFloatField;
    QrAICMSRec_pAliq: TFloatField;
    QrAICMSRec_vICMS: TFloatField;
    QrAIPIRec_pRedBC: TFloatField;
    QrAIPIRec_vBC: TFloatField;
    QrAIPIRec_pAliq: TFloatField;
    QrAIPIRec_vIPI: TFloatField;
    QrAPISRec_pRedBC: TFloatField;
    QrAPISRec_vBC: TFloatField;
    QrAPISRec_pAliq: TFloatField;
    QrAPISRec_vPIS: TFloatField;
    QrACOFINSRec_pRedBC: TFloatField;
    QrACOFINSRec_vBC: TFloatField;
    QrACOFINSRec_pAliq: TFloatField;
    QrACOFINSRec_vCOFINS: TFloatField;
    QrACodInfoEmit: TIntegerField;
    QrACodInfoDest: TIntegerField;
    QrI: TMySQLQuery;
    QrInItem: TIntegerField;
    QrINivel1: TIntegerField;
    QrIprod_qCom: TFloatField;
    QrIprod_vProd: TFloatField;
    QrIprod_vFrete: TFloatField;
    QrIprod_vSeg: TFloatField;
    QrIprod_vDesc: TFloatField;
    QrIprod_cProd: TWideStringField;
    QrIprod_cEAN: TWideStringField;
    QrIprod_xProd: TWideStringField;
    QrIprod_NCM: TWideStringField;
    QrIprod_EXTIPI: TWideStringField;
    QrIprod_genero: TSmallintField;
    QrIprod_CFOP: TIntegerField;
    QrIprod_uCom: TWideStringField;
    QrIprod_vUnCom: TFloatField;
    QrIprod_cEANTrib: TWideStringField;
    QrIprod_uTrib: TWideStringField;
    QrIprod_qTrib: TFloatField;
    QrIprod_vUnTrib: TFloatField;
    QrN: TMySQLQuery;
    QrNICMS_Orig: TSmallintField;
    QrNICMS_CST: TSmallintField;
    QrNICMS_modBC: TSmallintField;
    QrNICMS_pRedBC: TFloatField;
    QrNICMS_vBC: TFloatField;
    QrNICMS_pICMS: TFloatField;
    QrNICMS_vICMS: TFloatField;
    QrNICMS_modBCST: TSmallintField;
    QrNICMS_pMVAST: TFloatField;
    QrNICMS_pRedBCST: TFloatField;
    QrNICMS_vBCST: TFloatField;
    QrNICMS_pICMSST: TFloatField;
    QrNICMS_vICMSST: TFloatField;
    QrO: TMySQLQuery;
    QrOIPI_vIPI: TFloatField;
    QrOIPI_cEnq: TWideStringField;
    QrOIPI_CST: TSmallintField;
    QrOIPI_vBC: TFloatField;
    QrOIPI_qUnid: TFloatField;
    QrOIPI_vUnid: TFloatField;
    QrOIPI_pIPI: TFloatField;
    QrQ: TMySQLQuery;
    QrQPIS_CST: TSmallintField;
    QrQPIS_vBC: TFloatField;
    QrQPIS_pPIS: TFloatField;
    QrQPIS_vPIS: TFloatField;
    QrQPIS_qBCProd: TFloatField;
    QrQPIS_vAliqProd: TFloatField;
    QrR: TMySQLQuery;
    QrRPISST_vBC: TFloatField;
    QrRPISST_pPIS: TFloatField;
    QrRPISST_qBCProd: TFloatField;
    QrRPISST_vAliqProd: TFloatField;
    QrRPISST_vPIS: TFloatField;
    QrRPISST_fatorBCST: TFloatField;
    QrS: TMySQLQuery;
    QrSCOFINS_CST: TSmallintField;
    QrSCOFINS_vBC: TFloatField;
    QrSCOFINS_pCOFINS: TFloatField;
    QrSCOFINS_qBCProd: TFloatField;
    QrSCOFINS_vAliqProd: TFloatField;
    QrSCOFINS_vCOFINS: TFloatField;
    QrT: TMySQLQuery;
    QrTCOFINSST_vBC: TFloatField;
    QrTCOFINSST_pCOFINS: TFloatField;
    QrTCOFINSST_qBCProd: TFloatField;
    QrTCOFINSST_vAliqProd: TFloatField;
    QrTCOFINSST_vCOFINS: TFloatField;
    QrTCOFINSST_fatorBCST: TFloatField;
    EdCodigo: TdmkEdit;
    LaLancamento: TLabel;
    QrV: TMySQLQuery;
    QrVnItem: TIntegerField;
    QrVInfAdProd: TWideMemoField;
    QrIGraGruX: TIntegerField;
    QrIprod_vTotItm: TFloatField;
    QrIprod_vOutro: TFloatField;
    QrIFatID: TIntegerField;
    QrIFatNum: TIntegerField;
    QrIEmpresa: TIntegerField;
    QrIprod_indTot: TSmallintField;
    QrIprod_xPed: TWideStringField;
    QrIprod_nItemPed: TIntegerField;
    QrITem_IPI: TSmallintField;
    QrI_Ativo_: TSmallintField;
    QrIInfAdCuztm: TIntegerField;
    QrIEhServico: TIntegerField;
    QrIICMSRec_pRedBC: TFloatField;
    QrIICMSRec_vBC: TFloatField;
    QrIICMSRec_pAliq: TFloatField;
    QrIICMSRec_vICMS: TFloatField;
    QrIIPIRec_pRedBC: TFloatField;
    QrIIPIRec_vBC: TFloatField;
    QrIIPIRec_pAliq: TFloatField;
    QrIIPIRec_vIPI: TFloatField;
    QrIPISRec_pRedBC: TFloatField;
    QrIPISRec_vBC: TFloatField;
    QrIPISRec_pAliq: TFloatField;
    QrIPISRec_vPIS: TFloatField;
    QrICOFINSRec_pRedBC: TFloatField;
    QrICOFINSRec_vBC: TFloatField;
    QrICOFINSRec_pAliq: TFloatField;
    QrICOFINSRec_vCOFINS: TFloatField;
    QrIMeuID: TIntegerField;
    QrIUnidMedCom: TIntegerField;
    QrIUnidMedTrib: TIntegerField;
    QrIICMSRec_vBCST: TFloatField;
    QrIICMSRec_vICMSST: TFloatField;
    QrIICMSRec_pAliqST: TFloatField;
    QrITem_II: TSmallintField;
    QrILk: TIntegerField;
    QrIDataCad: TDateField;
    QrIDataAlt: TDateField;
    QrIUserCad: TIntegerField;
    QrIUserAlt: TIntegerField;
    QrIAlterWeb: TSmallintField;
    QrIAtivo: TSmallintField;
    QrIprod_nFCI: TWideStringField;
    QrIprod_CEST: TIntegerField;
    QrIStqMovValA: TIntegerField;
    QrIprod_indEscala: TWideStringField;
    QrIprod_CNPJFab: TWideStringField;
    QrIprod_cBenef: TWideStringField;
    QrIAWServerID: TIntegerField;
    QrIAWStatSinc: TSmallintField;
    QrIprod_cBarra: TWideStringField;
    QrIprod_cBarraTrib: TWideStringField;
    QrIUsaSubsTrib: TSmallintField;
    QrIAtrelaID: TIntegerField;
    QrAide_hSaiEnt: TTimeField;
    QrAide_dhCont: TDateTimeField;
    QrAide_xJust: TWideStringField;
    QrAemit_CRT: TSmallintField;
    QrAdest_email: TWideStringField;
    QrAVagao: TWideStringField;
    QrABalsa: TWideStringField;
    QrAprotNFe_versao: TFloatField;
    QrAretCancNFe_versao: TFloatField;
    QrADataFiscal: TDateField;
    QrASINTEGRA_ExpDeclNum: TWideStringField;
    QrASINTEGRA_ExpDeclDta: TDateField;
    QrASINTEGRA_ExpNat: TWideStringField;
    QrASINTEGRA_ExpRegNum: TWideStringField;
    QrASINTEGRA_ExpRegDta: TDateField;
    QrASINTEGRA_ExpConhNum: TWideStringField;
    QrASINTEGRA_ExpConhDta: TDateField;
    QrASINTEGRA_ExpConhTip: TWideStringField;
    QrASINTEGRA_ExpPais: TWideStringField;
    QrASINTEGRA_ExpAverDta: TDateField;
    QrACriAForca: TSmallintField;
    QrACodInfoTrsp: TIntegerField;
    QrAOrdemServ: TIntegerField;
    QrASituacao: TSmallintField;
    QrAAntigo: TWideStringField;
    QrANFG_Serie: TWideStringField;
    QrANF_ICMSAlq: TFloatField;
    QrANF_CFOP: TWideStringField;
    QrAImportado: TSmallintField;
    QrANFG_SubSerie: TWideStringField;
    QrANFG_ValIsen: TFloatField;
    QrANFG_NaoTrib: TFloatField;
    QrANFG_Outros: TFloatField;
    QrACOD_MOD: TWideStringField;
    QrACOD_SIT: TSmallintField;
    QrAVL_ABAT_NT: TFloatField;
    QrAEFD_INN_AnoMes: TIntegerField;
    QrAEFD_INN_Empresa: TIntegerField;
    QrAEFD_INN_LinArq: TIntegerField;
    QrAICMSRec_vBCST: TFloatField;
    QrAICMSRec_vICMSST: TFloatField;
    QrAEFD_EXP_REG: TWideStringField;
    QrAICMSRec_pAliqST: TFloatField;
    QrAeveMDe_Id: TWideStringField;
    QrAeveMDe_tpAmb: TSmallintField;
    QrAeveMDe_verAplic: TWideStringField;
    QrAeveMDe_cOrgao: TSmallintField;
    QrAeveMDe_cStat: TIntegerField;
    QrAeveMDe_xMotivo: TWideStringField;
    QrAeveMDe_chNFe: TWideStringField;
    QrAeveMDe_tpEvento: TIntegerField;
    QrAeveMDe_xEvento: TWideStringField;
    QrAeveMDe_nSeqEvento: TSmallintField;
    QrAeveMDe_CNPJDest: TWideStringField;
    QrAeveMDe_CPFDest: TWideStringField;
    QrAeveMDe_emailDest: TWideStringField;
    QrAeveMDe_dhRegEvento: TDateTimeField;
    QrAeveMDe_TZD_UTC: TFloatField;
    QrAeveMDe_nProt: TWideStringField;
    QrAcSitNFe: TSmallintField;
    QrAcSitConf: TSmallintField;
    QrANFeNT2013_003LTT: TSmallintField;
    QrAvTotTrib: TFloatField;
    QrAide_hEmi: TTimeField;
    QrAide_dhEmiTZD: TFloatField;
    QrAide_dhSaiEntTZD: TFloatField;
    QrAide_idDest: TSmallintField;
    QrAide_indFinal: TSmallintField;
    QrAide_indPres: TSmallintField;
    QrAide_dhContTZD: TFloatField;
    QrAEstrangDef: TSmallintField;
    QrAdest_idEstrangeiro: TWideStringField;
    QrAdest_indIEDest: TSmallintField;
    QrAdest_IM: TWideStringField;
    QrAICMSTot_vICMSDeson: TFloatField;
    QrAISSQNtot_dCompet: TDateField;
    QrAISSQNtot_vDeducao: TFloatField;
    QrAISSQNtot_vOutro: TFloatField;
    QrAISSQNtot_vDescIncond: TFloatField;
    QrAISSQNtot_vDescCond: TFloatField;
    QrAISSQNtot_vISSRet: TFloatField;
    QrAISSQNtot_cRegTrib: TSmallintField;
    QrAinfProt_dhRecbtoTZD: TFloatField;
    QrAinfCanc_dhRecbtoTZD: TFloatField;
    QrAvBasTrib: TFloatField;
    QrApTotTrib: TFloatField;
    QrAinfCCe_verAplic: TWideStringField;
    QrAinfCCe_cOrgao: TSmallintField;
    QrAinfCCe_tpAmb: TSmallintField;
    QrAinfCCe_CNPJ: TWideStringField;
    QrAinfCCe_CPF: TWideStringField;
    QrAinfCCe_chNFe: TWideStringField;
    QrAinfCCe_dhEvento: TDateTimeField;
    QrAinfCCe_dhEventoTZD: TFloatField;
    QrAinfCCe_tpEvento: TIntegerField;
    QrAinfCCe_nSeqEvento: TIntegerField;
    QrAinfCCe_verEvento: TFloatField;
    QrAinfCCe_xCorrecao: TWideMemoField;
    QrAinfCCe_cStat: TIntegerField;
    QrAinfCCe_dhRegEvento: TDateTimeField;
    QrAinfCCe_dhRegEventoTZD: TFloatField;
    QrAinfCCe_nProt: TWideStringField;
    QrAinfCCe_nCondUso: TIntegerField;
    QrAInfCpl_totTrib: TWideStringField;
    QrAICMSTot_vFCPUFDest: TFloatField;
    QrAICMSTot_vICMSUFDest: TFloatField;
    QrAICMSTot_vICMSUFRemet: TFloatField;
    QrAExporta_XLocDespacho: TWideStringField;
    QrACodInfoCliI: TIntegerField;
    QrAICMSTot_vFCP: TFloatField;
    QrAICMSTot_vFCPST: TFloatField;
    QrAICMSTot_vFCPSTRet: TFloatField;
    QrAICMSTot_vIPIDevol: TFloatField;
    QrAAWServerID: TIntegerField;
    QrAAWStatSinc: TSmallintField;
    QrAURL_QRCode: TWideStringField;
    QrAURL_Consulta: TWideStringField;
    QrAAllTxtLink: TWideStringField;
    QrACNPJCPFAvulso: TWideStringField;
    QrARazaoNomeAvulso: TWideStringField;
    QrAide_indIntermed: TSmallintField;
    QrAInfIntermedEnti: TIntegerField;
    QrAInfIntermed_CNPJ: TWideStringField;
    QrAInfIntermed_idCadIntTran: TWideStringField;
    QrAEmiteAvulso: TSmallintField;
    QrAAtrelaFatID: TIntegerField;
    QrAAtrelaFatNum: TIntegerField;
    QrAAtrelaStaLnk: TSmallintField;
    QrNICMS_CSOSN: TIntegerField;
    QrNICMS_UFST: TWideStringField;
    QrNICMS_pBCOp: TFloatField;
    QrNICMS_vBCSTRet: TFloatField;
    QrNICMS_vICMSSTRet: TFloatField;
    QrNICMS_motDesICMS: TSmallintField;
    QrNICMS_pCredSN: TFloatField;
    QrNICMS_vCredICMSSN: TFloatField;
    DsPediPrzCab: TDataSource;
    QrPediPrzCab: TMySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    QrPediPrzCabCondPg: TSmallintField;
    QrIFatorQtd: TFloatField;
    Label31: TLabel;
    EdRegrFiscal: TdmkEditCB;
    CBRegrFiscal: TdmkDBLookupComboBox;
    LaCondicaoPG: TLabel;
    EdCondicaoPG: TdmkEditCB;
    CBCondicaoPG: TdmkDBLookupComboBox;
    BtCondicaoPG: TSpeedButton;
    QrI80s: TMySQLQuery;
    QrItsI80: TMySQLQuery;
    QrItsI80rastro_nLote: TWideStringField;
    QrItsI80rastro_qLote: TFloatField;
    QrItsI80rastro_dFab: TDateField;
    QrItsI80rastro_dVal: TDateField;
    QrItsI80rastro_cAgreg: TWideStringField;
    QrFisRegCadNO_MODELO_NF: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdIND_PGTOChange(Sender: TObject);
    procedure EdIND_PGTOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdIND_FRTChange(Sender: TObject);
    procedure EdIND_FRTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure BtCondicaoPGClick(Sender: TObject);
  private
    { Private declarations }
    F_IND_PGTO_EFD, F_IND_FRT_EFD: MyArrayLista;
    function  InserePQEItsDeNFe(const Codigo: Integer; var FisRegGenCtbD,
              FisRegGenCtbC, MadeBy, OriPart: Integer): Boolean;
    procedure ReopenItsI();
    procedure ReopenItsN();
    procedure ReopenPediPrzCab();
  public
    { Public declarations }
    FPQE, FFatID, FFatNum, FEmpresa, FIDCtrl, FTerceiro, FTransporta, Fcab_qVol:
    Integer;
    Fcab_PesoB, Fcab_PesoL, FSumPesoLIts: Double;
    FDataFiscal: TDateTime;
    //
    procedure ReopenA_e_ConfiguraRegrFiscal();
  end;

  var
  FmPQEXml: TFmPQEXml;

implementation

uses UnMyObjects, Module, DmkDAC_PF, NFeImporta_0400, ModuleNFe_0000, PQE,
  ModuleGeral, UMySQLModule, NFe_PF, PQx, UnSPED_PF, ModProd, ModPediVda,
  UnPraz_PF;

{$R *.DFM}

procedure TFmPQEXml.BtCondicaoPGClick(Sender: TObject);
var
  CondicaoPG: Integer;
begin
  VAR_CADASTRO := 0;
  //
  Praz_PF.MostraFormPediPrzCab1(0);
  //
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCondicaoPg, CBCondicaoPg, QrPediPrzCab, VAR_CADASTRO);
end;

procedure TFmPQEXml.BtOKClick(Sender: TObject);
const
  UsaEntradaFiscal = True; // Vai calcular autom�tico varias informa��es!
  //
  function ExcluirEntrada(FatID, FatNum, Empresa: Integer; Avisa: Boolean): Boolean;
  begin
    if Avisa then
    Geral.MB_Aviso('Ocorreu um erro na importa��o do XML.' + sLineBreak +
    'Os dados da entrada ser�o exclu�dos, pois s�o parciais!');
    DmNFe_0000.ExcluiNfe(0(*Status*), FatID, FatNum, Empresa, True, True);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM pqeits WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := FatNum;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM pqe WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := FatNum;
    Dmod.QrUpd.ExecSQL;
    //
    Result := True;
    //
    FmPQE.LocCod(FatNum, FatNum);
    //
    Screen.Cursor := crDefault;
  end;
var

  //  FatID, FatNum, Empresa, IDCtrl, Terceiro, Transporta, cab_qVol,
  cab_Pedido, cab_TipoNF, cab_Conhecimento, cab_NF, cab_modNF, cab_Serie: Integer;
  // cab_PesoB, cab_PesoL,
  cab_Juros, cab_RICMS, cab_RICMSF, ValFrete, cab_ICMS,
  cab_ValProd, cab_Frete, cab_Seguro, cab_Desconto, cab_IPI, cab_PIS,
  cab_COFINS, cab_Outros, cab_ValorNF: Double;
  cab_Data, cab_DataE, cab_DataS, cab_Cancelado, cab_refNFe: String;
  //
  (*
  Conta, Controle, Insumo, its_Volumes: Integer;
  its_PesoVB, its_PesoVL, its_ValorItem, its_RIPI, its_CFin, its_Seguro,
  its_Frete, its_Desconto, its_TotalCusto, its_TotalPeso, its_IPI, its_ICMS,
  its_RICMS: Double;*)
  //DataFiscal: TDateTime;
  _Empresa, _Filial: Integer;
  //_UF,
  //emit_UF, dest_UF: String;
  NF_RP, NF_CC, modRP, modCC, SerRP, SerCC, CTe_mod, CTe_serie, NFe_RP, NFe_CC,
        NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
        NFRP_FatID, NFRP_FatNum, NFRP_StaLnk,
        NFCC_FatID, NFCC_FatNum, NFCC_StaLnk,
  Codigo, RegrFiscal, IND_PGTO, IND_FRT, MadeBy, CondicaoPg,
  FisRegGenCtbD, FisRegGenCtbC, OriPart: Integer;
  FreteRpICMS, FreteRpPIS, FreteRpCOFINS,
  FreteRvICMS, FreteRvPIS, FreteRvCOFINS,
  ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg,
  ICMSTot_vDesc, ICMSTot_vOutro, IPI: Double;
  Cte_Id: String;






  NFe_FatID, NFe_FatNum: Integer;

  //
  function InserePQE(): Boolean;
  var
    PesoL: Double;
  begin
    if Fcab_PesoL >= FSumPesoLIts then
      PesoL := Fcab_PesoL
    else
      PesoL := FSumPesoLIts;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpdW, ImgTipo.SQLType, 'pqe', False, [
        'Data', 'IQ', 'CI',
        'Transportadora', 'NF', 'Frete',
        'PesoB', 'PesoL', 'ValorNF',
        'RICMS', 'RICMSF', 'Conhecimento',
        'Pedido', 'DataE', 'Juros',
        'ICMS', 'Cancelado', 'TipoNF',
        'refNFe', 'modNF', 'Serie',

        'NF_RP', 'NF_CC', 'Empresa',
        'FreteRpICMS', 'FreteRpPIS', 'FreteRpCOFINS',
        'FreteRvICMS', 'FreteRvPIS', 'FreteRvCOFINS',
        'modRP', 'modCC', 'SerRP', 'SerCC',
        'NFe_RP', 'NFe_CC', 'Cte_Id',
        'CTe_mod', 'CTe_serie',
        'NFVP_FatID', 'NFVP_FatNum', 'NFVP_StaLnk',
        'NFRP_FatID', 'NFRP_FatNum', 'NFRP_StaLnk',
        'NFCC_FatID', 'NFCC_FatNum', 'NFCC_StaLnk',
        'IND_PGTO', 'IND_FRT',
        'ICMSTot_vProd', 'ICMSTot_vFrete', 'ICMSTot_vSeg',
        'ICMSTot_vDesc', 'ICMSTot_vOutro', 'IPI',
        'RegrFiscal', 'CondicaoPg', 'MadeBy',
        'NFe_FatID', 'NFe_FatNum'], [
        'Codigo'], [
        cab_Data, FTerceiro, FEmpresa,
        FTransporta, cab_NF, cab_Frete,
        Fcab_PesoB, (*Fcab_PesoL*)PesoL, cab_ValorNF,
        cab_RICMS, cab_RICMSF, cab_Conhecimento,
        cab_Pedido, cab_DataE, cab_Juros,
        cab_ICMS, cab_Cancelado, cab_TipoNF,
        cab_refNFe, cab_modNF, cab_Serie,

        NF_RP, NF_CC, _Empresa,
        FreteRpICMS, FreteRpPIS, FreteRpCOFINS,
        FreteRvICMS, FreteRvPIS, FreteRvCOFINS,
        modRP, modCC, SerRP, SerCC,
        NFe_RP, NFe_CC, Cte_Id,
        CTe_mod, CTe_serie,
        NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
        NFRP_FatID, NFRP_FatNum, NFRP_StaLnk,
        NFCC_FatID, NFCC_FatNum, NFCC_StaLnk,
        IND_PGTO, IND_FRT,
        ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg,
        ICMSTot_vDesc, ICMSTot_vOutro, IPI,
        RegrFiscal, CondicaoPg, MadeBy,
        NFe_FatID, NFe_FatNum], [
        Codigo], True);
        FPQE := Codigo;
  end;
begin
  OriPart              := 0;
  NF_RP                := 0;
  NF_CC                := 0;
  FreteRpICMS          := 0.00;
  FreteRpPIS           := 0.00;
  FreteRpCOFINS        := 0.00;
  FreteRvICMS          := 0.00;
  FreteRvPIS           := 0.00;
  FreteRvCOFINS        := 0.00;
  modRP                := 0;
  modCC                := 0;
  SerRP                := 0;
  SerCC                := 0;
  CTe_mod              := 0;
  CTe_serie            := 0;
  NFe_RP               := 0;
  NFe_CC               := 0;
  Cte_Id               := '';
  NFVP_FatID           := 0;
  NFVP_FatNum          := 0;
  NFVP_StaLnk          := 0;
  NFRP_FatID           := 0;
  NFRP_FatNum          := 0;
  NFRP_StaLnk          := 0;
  NFCC_FatID           := 0;
  NFCC_FatNum          := 0;
  NFCC_StaLnk          := 0;
  //
  if not DModG.ObtemEmpresaSelecionadaESuaFilial(EdEmpresa, _Empresa, _Filial) then
    Exit;
  //
  (*
  FatID := VAR_FATID_0051;
  FatNum := UMyMod.BuscaEmLivreY_Def('pqe', 'Codigo', stIns, 0);
  //
  cab_PesoB := 0;
  cab_PesoL := 0;
  if UnNFeImporta_0400.ImportaDadosNFeDeXML(tpemiTerceiros, tpnfEntrada,
  MeWarn, MeInfo, MeWarn, '', FmPQE, FatID, FatNum,
  Empresa, IDCtrl, Terceiro, Transporta, cab_qVol, cab_PesoB, cab_PesoL,
  DataFiscal, FTabLcta) then
  begin
    //cab_Data          := FormatDateTime('yyyy-mm-dd', DataFiscal); // Entrada
    cab_Data          := FormatDateTime('yyyy-mm-dd', DModG.ObtemAgora()); // Entrada
*)
    cab_Data          := FormatDateTime('yyyy-mm-dd', TPEntrada.Date); // Entrada
    cab_TipoNF        := 1; // Nota Fiscal Eletr�nica
    //
    cab_Pedido        := 0;
    cab_Juros         := 0;
    cab_RICMS         := 0;
    cab_RICMSF        := 0;
    cab_Conhecimento  := 0;
    cab_Cancelado     := 'V';
    {
    QrA.Close;
    QrA.Params[00].AsInteger := FFatID;
    QrA.Params[01].AsInteger := FFatNum;
    QrA.Params[02].AsInteger := FEmpresa;
    UnDmkDAC_PF.AbreQuery(QrA, Dmod.MyDB);
    }
    (*
    _UF := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(QrAide_cUF.Value);
    //Geral.MB_Teste('UF emitente: ' + _UF);
    *)
    //
    cab_NF      := QrAide_nNF.Value;
    cab_dataS   := Geral.FDT(QrAide_dSaiEnt.Value, 1);
    cab_dataE   := Geral.FDT(QrAide_dEmi.Value, 1);
    cab_refNFe  := QrAId.Value;
    cab_modNF   := QrAide_mod.Value;
    cab_Serie   := QrAide_serie.Value;
    cab_ICMS    := QrAICMSTot_vICMS.Value;
    cab_ValProd := QrAICMSTot_vProd.Value;
    cab_Frete   := QrAICMSTot_vFrete.Value;
    cab_Seguro  := QrAICMSTot_vSeg.Value;
    cab_Desconto:= QrAICMSTot_vDesc.Value;
    cab_IPI     := QrAICMSTot_vIPI.Value;
    cab_PIS     := QrAICMSTot_vPIS.Value;
    cab_COFINS  := QrAICMSTot_vCOFINS.Value;
    cab_Outros  := QrAICMSTot_vOutro.Value;
    cab_ValorNF := QrAICMSTot_vNF.Value;
    //
    MadeBy          := RGMadeBy.ItemIndex;
    CondicaoPg      := EdCondicaoPg.ValueVariant;
    RegrFiscal      := EdRegrFiscal.ValueVariant;
    IND_PGTO        := QrAide_indPag.Value; //EdIND_PGTO.ValueVariant;
    IND_FRT         := QrAModFrete.Value; //EdIND_FRT.ValueVariant;
    ICMSTot_vProd   := QrAICMSTot_vProd.Value;
    ICMSTot_vFrete  := QrAICMSTot_vFrete.Value;
    ICMSTot_vSeg    := QrAICMSTot_vSeg.Value;
    ICMSTot_vDesc   := QrAICMSTot_vDesc.Value;
    ICMSTot_vOutro  := QrAICMSTot_vOutro.Value;
    IPI             := QrAICMSTot_vIPI.Value;
    //
  if MyObjects.FIC(RGMadeBy.ItemIndex < 1, RGMadeBy,
    'Informe o modo de fornecimento dos produtos!') then Exit;
  if MyObjects.FIC((RegrFiscal = 0) and UsaEntradaFiscal, EdRegrFiscal, 'Informe a regra fiscal') then Exit;
  if UnPQx.ImpedePeloBalanco(TPEntrada.Date, True) then Exit;
(*
    if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqe', False, [
    'Data', 'IQ', 'CI', 'Empresa',
    'Transportadora', 'NF', 'Frete',
    'PesoB', 'PesoL', 'ValorNF',
    'RICMS', 'RICMSF', 'Conhecimento',
    'Pedido', 'DataE', 'Juros',
    'ICMS', 'Cancelado', 'TipoNF',
    'refNFe', 'modNF', 'Serie',
    'ValProd', 'Volumes', 'IPI',
    'PIS', 'COFINS', 'Seguro',
    'Desconto', 'Outros', 'DataS'], [
    'Codigo'], [
    cab_Data, FTerceiro, FEmpresa, _Empresa,
    FTransporta, cab_NF, cab_Frete,
    Fcab_PesoB, Fcab_PesoL, cab_ValorNF,
    cab_RICMS, cab_RICMSF, cab_Conhecimento,
    cab_Pedido, cab_DataE, cab_Juros,
    cab_ICMS, cab_Cancelado, cab_TipoNF,
    cab_refNFe, cab_modNF, cab_Serie,
    cab_ValProd, Fcab_qVol, cab_IPI,
    cab_PIS, cab_COFINS, cab_Seguro,
    cab_Desconto, cab_Outros, cab_DataS], [
    FFatNum], True) then
*)
   NFe_FatID := QrAFatID.Value;
   NFe_FatNum := QrAFatNum.Value;
   Codigo := EdCodigo.ValueVariant;
    if not InserePQE() then
    begin
      ExcluirEntrada(FFatID, FFatNum, FEmpresa, False);
      Exit;
    end;

    // ITENS

(*
    QrI.Close;
    QrI.Params[00].AsInteger := FFatID;
    QrI.Params[01].AsInteger := FFatNum;
    QrI.Params[02].AsInteger := FEmpresa;
    UnDmkDAC_PF.AbreQuery(QrI, Dmod.MyDB);
*)
    ReopenItsI();
    //
    QrI.First;
    while not QrI.Eof do
    begin
      if InserePQEItsDeNFe(Codigo, FisRegGenCtbD, FisRegGenCtbC, MadeBy, OriPart) then
        QrI.Next
      else
      begin
        FmPQE.ExcluiTodaEntradaPvdNFeEmProgresso(Codigo);
        Exit;
      end;
    end;
  //end;
  FmPQE.LocCod(Codigo, Codigo);
  FmPQE.FinalizaEntrada(True);
  FmPQE.LocCod(Codigo, Codigo);
  Close;
end;

procedure TFmPQEXml.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQEXml.ReopenA_e_ConfiguraRegrFiscal();
var
  emit_UF, dest_UF: String;
  RegrFiscal: Integer;
begin
  QrA.Close;
  QrA.Params[00].AsInteger := FFatID;
  QrA.Params[01].AsInteger := FFatNum;
  QrA.Params[02].AsInteger := FEmpresa;
  UnDmkDAC_PF.AbreQuery(QrA, Dmod.MyDB);
  //
  emit_UF         := QrAemit_UF.Value;
  dest_UF         := QrAdest_UF.Value;
  if emit_UF = dest_UF then
    RegrFiscal := DModG.ObtemCodigo_TabApp('ParamsEmp', 'Codigo', 'UCFisRegMesmaUF', FEmpresa)
  else
    RegrFiscal := DModG.ObtemCodigo_TabApp('ParamsEmp', 'Codigo', 'UCFisRegOutraUF', FEmpresa);
  //
  EdRegrFiscal.ValueVariant := RegrFiscal;
  CBRegrFiscal.KeyValue     := RegrFiscal;
  //
  RGMadeBy.Itemindex := DModG.ObtemCodigo_TabApp('ParamsEmp', 'Codigo', 'UCFisRegMadBy', FEmpresa);
end;

procedure TFmPQEXml.ReopenItsI();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrI, Dmod.MyDB, [
  'SELECT nfi.*,  ',
  '  nfi.prod_vProd + ',
  '  nfi.prod_vFrete + ',
  '  nfi.prod_vSeg - ',
  '  nfi.prod_vDesc + ',
  '  nfi.prod_vOutro',
  '    prod_vTotItm',
  'FROM nfeitsi nfi',
  'WHERE nfi.FatID=' + Geral.FF0(FFatID),
  'AND nfi.FatNum=' + Geral.FF0(FFatNum),
  'AND nfi.Empresa=' + Geral.FF0(FEmpresa),
  'ORDER BY nItem ',
  '']);
end;

procedure TFmPQEXml.ReopenItsN();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrV, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitsv',
  'WHERE FatID=' + Geral.FF0(FFatID),
  'AND FatNum=' + Geral.FF0(FFatNum),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND nItem=' + Geral.FF0(QrInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrN, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitsn',
  'WHERE FatID=' + Geral.FF0(FFatID),
  'AND FatNum=' + Geral.FF0(FFatNum),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND nItem=' + Geral.FF0(QrInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrO, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitso',
  'WHERE FatID=' + Geral.FF0(FFatID),
  'AND FatNum=' + Geral.FF0(FFatNum),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND nItem=' + Geral.FF0(QrInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrQ, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitsq',
  'WHERE FatID=' + Geral.FF0(FFatID),
  'AND FatNum=' + Geral.FF0(FFatNum),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND nItem=' + Geral.FF0(QrInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrR, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitsr',
  'WHERE FatID=' + Geral.FF0(FFatID),
  'AND FatNum=' + Geral.FF0(FFatNum),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND nItem=' + Geral.FF0(QrInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrS, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitss',
  'WHERE FatID=' + Geral.FF0(FFatID),
  'AND FatNum=' + Geral.FF0(FFatNum),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND nItem=' + Geral.FF0(QrInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrT, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitst',
  'WHERE FatID=' + Geral.FF0(FFatID),
  'AND FatNum=' + Geral.FF0(FFatNum),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND nItem=' + Geral.FF0(QrInItem.Value),
  ' ']);
end;

procedure TFmPQEXml.ReopenPediPrzCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzCab, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome, MaxDesco, ',
  'JurosMes, Parcelas, Percent1, Percent2, CondPg ',
  'FROM pediprzcab',
  'WHERE Aplicacao & ' + Geral.FF0(1) + ' <> 0',
  'ORDER BY Nome',
  '']);
end;

procedure TFmPQEXml.EdIND_FRTChange(Sender: TObject);
(*
var
  IND_FRT: Integer;
*)
begin
(*
  EdIND_FRT_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_FRT_EFD,  EdIND_FRT.ValueVariant);
  //
  IND_FRT := Geral.IMV('0' + EdIND_FRT.Text);
  case IND_FRT of
    3:
    begin
      EdTransportador.ValueVariant := EdCI.ValueVariant;
      CBTransportador.KeyValue     := EdCI.ValueVariant;
    end;
    4:
    begin
      EdTransportador.ValueVariant := DModG.QrEmpresasCodigo.Value;
      CBTransportador.KeyValue     := DModG.QrEmpresasCodigo.Value;
    end;
  end;
*)
end;

procedure TFmPQEXml.EdIND_FRTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(*
var
  TitCols: array[0..1] of String;
*)
begin
(*
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_FRT.Text := Geral.SelecionaItem(F_IND_FRT_EFD, 0,
      'SEL-LISTA-000 :: Indicador do Tipo de Frete',
      TitCols, Screen.Width)
  end;
*)
end;

procedure TFmPQEXml.EdIND_PGTOChange(Sender: TObject);
begin
(*
  EdIND_PGTO_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_PAG_EFD,  EdIND_PGTO.ValueVariant);
*)
end;

procedure TFmPQEXml.EdIND_PGTOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(*
var
  TitCols: array[0..1] of String;
*)
begin
(*
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_PGTO.Text := Geral.SelecionaItem(F_IND_PGTO_EFD, 0,
      'SEL-LISTA-000 :: Indicador do tipo de pagamento',
      TitCols, Screen.Width)
  end;
*)
end;

procedure TFmPQEXml.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQEXml.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FPQE := 0;
  UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
  ReopenPediPrzCab();
(*
  F_IND_PGTO_EFD := UnNFe_PF.ListaIND_PAG_EFD();
  F_IND_FRT_EFD  := UnNFe_PF.ListaIND_FRT_EFD();
*)
end;

procedure TFmPQEXml.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQEXml.FormShow(Sender: TObject);
begin
  //EdIND_PGTO_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_PAG_EFD,  EdIND_PGTO.ValueVariant);
  //EdIND_FRT_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_FRT_EFD,  EdIND_FRT.ValueVariant);
  //
end;

{
function TFmPQEXml.InserePQEItsDeNFe(const Codigo: Integer; var FisRegGenCtbD,
  FisRegGenCtbC, MadeBy, OriPart: Integer): Boolean;
const
  sProcName = 'TFmPQENew.InserePQEItsDeNFe()';
var
  prod_cProd, prod_cEAN, prod_xProd, prod_NCM, prod_EX_TIPI, prod_CFOP,
  prod_uCom, prod_cEANTrib, prod_uTrib, IPI_cEnq, DtCorrApo, xLote: String;
  //Codigo,
  Controle, Conta, Insumo, Volumes, Moeda, prod_genero, ICMS_Orig, ICMS_CST,
  ICMS_modBC, ICMS_modBCST, IPI_CST, PIS_CST, COFINS_CST, HowLoad: Integer;
  FatID, FatNum, Empresa, nItem, NFeItsI_nItem: Integer;
  PesoVB, PesoVL, ValorItem, IPI, RIPI, CFin, ICMS, RICMS, TotalCusto, TotalPeso,
  Cotacao: Double;
  RpICMS, RpPIS, RpCOFINS: Double;
  RvICMS, RvPIS, RvCOFINS,
  prod_qCom, prod_vUnCom,
  prod_vProd, prod_vFrete, prod_vSeg, prod_vOutro, prod_vDesc: Double;

  prod_qTrib, prod_vUnTrib, ICMS_pRedBC, ICMS_vBC, ICMS_pICMS, ICMS_vICMS,
  ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST, ICMS_vICMSST, IPI_vBC,
  IPI_qUnid, IPI_vUnid, IPI_pIPI, IPI_vIPI, PIS_vBC, PIS_pPIS, PIS_vPIS,
  PIS_qBCProd, PIS_vAliqProd, PISST_vBC, PISST_pPIS, PISST_qBCProd,
  PISST_vAliqProd, PISST_vPIS, COFINS_vBC, COFINS_pCOFINS, COFINS_qBCProd,
  COFINS_vAliqProd, COFINS_vCOFINS, COFINSST_vBC, COFINSST_pCOFINS,
  COFINSST_qBCProd, COFINSST_vAliqProd, COFINSST_vCOFINS, RpIPI, RvIPI,
  RpICMSST, RvICMSST: Double;

  SQLType: TSQLType;
  CustoItem: Double;
  EhServico: Boolean;
  //OriPart,
  CodInfoEmit, RegrFiscal, CFOP_Emitido, CRT_Emitido, CST_A_Emitido,
  CST_B_Emitido, CSOSN_Emitido: Integer;
  //
  CFOP_Entrada: String; (*var ICMS_Aliq: Double;*)
  CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer;
  InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST, TempX: String;
  OriTES, EFD_II_C195, GenCtbD, GenCtbC, CopyIni, CopyTam,
  TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
  TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS: Integer;
  //
  _prod_CFOP, _ICMS_CST, _IPI_CST, _PIS_CST, _COFINS_CST: String;
  _ICMS_vBC, _IPI_vBC, _PIS_vBC, _COFINS_vBC,
  ICMSAliq, PISAliq, COFINSAliq: Double;
  //FisRegGenCtbD, FisRegGenCtbC: Integer;
  AjusteVL_BC_ICMS, AjusteALIQ_ICMS, AjusteVL_ICMS, AjusteVL_OUTROS,
  AjusteVL_OPR, AjusteVL_RED_BC, ori_pIPI, ori_vIPI, VL_OPR: Double;
begin
  SQLType        := stIns;
  //
  ReopenItsN();
  //Codigo             := Codigo;
  Controle           := 0; // Abaixo
  Conta              := QrI.RecNO;
  Insumo             := QrINivel1.Value;
  if Insumo = 0 then
    Insumo           := DmProd.ObtemGraGru1deGraGruX(QrIGraGruX.Value);
  if Insumo = 0 then
  begin
    Geral.MB_Aviso('Insumo n�o pode ser zero! ' + sProcName);
    FmPQE.ExcluiTodaEntradaPvdNFeEmProgresso(Codigo);
    Exit;
  end;
  Volumes            := 1;
  PesoVB             := 0;
  PesoVL             := QrIprod_qCom.Value * QrIFatorQtd.Value; // Quantidade do Pedido! pode ser fracionado!
  ValorItem          := QrIprod_vTotItm.Value;
  IPI                := QrOIPI_vIPI.Value;
  RIPI               := 0;
  CFin               := 0;
  ICMS               := 0.00; // deprecado. QrNICMS_vICMS.Value;
  RICMS              := 0;
  TotalPeso          := Volumes * PesoVL;
  Moeda              := 0; // BRL
  Cotacao            := 0;
  prod_cProd               := QrIprod_cProd.Value;
  prod_cEAN                := QrIprod_cEAN.Value;
  prod_xProd               := QrIprod_xProd.Value;
  prod_NCM                 := QrIprod_NCM.Value;
  prod_EX_TIPI             := QrIprod_EXTIPI.Value;
  prod_CFOP                := Geral.FF0(QrIprod_CFOP.Value);
  prod_uCom                := QrIprod_uCom.Value;
  prod_cEANTrib            := QrIprod_cEANTrib.Value;
  prod_uTrib               := QrIprod_uTrib.Value;
  IPI_cEnq                 := QrOIPI_cEnq.Value;
  //
  prod_genero              := QrIprod_genero .Value;
  prod_qCom                := QrIprod_qCom.Value;
  prod_vUnCom              := QrIprod_vUnCom.Value;
  prod_vProd               := QrIprod_vProd.Value;
  prod_qTrib               := QrIprod_qTrib.Value;
  prod_vUnTrib             := QrIprod_vUnTrib.Value;
  prod_vFrete              := QrIprod_vFrete.Value;
  prod_vSeg                := QrIprod_vSeg.Value;
  prod_vOutro              := QrIprod_vOutro.Value;
  prod_vDesc               := QrIprod_vDesc.Value;
  //
  ICMS_Orig                := QrNICMS_Orig.Value;
  ICMS_modBC               := QrNICMS_modBC.Value;
  ICMS_modBCST             := QrNICMS_modBCST.Value;
  ICMS_pRedBC              := QrNICMS_pRedBC.Value;
  ICMS_CST                 := QrNICMS_CST.Value;
  ICMS_vBC                 := QrNICMS_vBC.Value;
  ICMS_pICMS               := QrNICMS_pICMS.Value;
  ICMS_vICMS               := QrNICMS_vICMS.Value;
  ICMS_modBCST             := QrNICMS_modBCST.Value;
  ICMS_pMVAST              := QrNICMS_pMVAST.Value;
  ICMS_pRedBCST            := QrNICMS_pRedBCST.Value;
  ICMS_vBCST               := QrNICMS_vBCST.Value;
  ICMS_pICMSST             := QrNICMS_pICMSST.Value;
  ICMS_vICMSST             := QrNICMS_vICMSST.Value;
  IPI_cEnq                 := QrOIPI_cEnq.Value;
  //
  IPI_CST                  := QrOIPI_CST.Value;
  IPI_vBC                  := QrOIPI_vBC.Value;
  IPI_qUnid                := QrOIPI_qUnid.Value;
  IPI_vUnid                := QrOIPI_vUnid.Value;
  IPI_pIPI                 := QrOIPI_pIPI.Value;
  IPI_vIPI                 := QrOIPI_vIPI.Value;
  //
  PIS_CST                  := QrQPIS_CST.Value;
  PIS_vBC                  := QrQPIS_vBC.Value;
  PIS_pPIS                 := QrQPIS_pPIS.Value;
  PIS_vPIS                 := QrQPIS_vPIS.Value;
  PIS_qBCProd              := QrQPIS_qBCProd.Value;
  PIS_vAliqProd            := QrQPIS_vAliqProd.Value;
  //
  PISST_vBC                := QrRPISST_vBC.Value;
  PISST_pPIS               := QrRPISST_pPIS.Value;
  PISST_qBCProd            := QrRPISST_qBCProd.Value;
  PISST_vAliqProd          := QrRPISST_vAliqProd.Value;
  PISST_vPIS               := QrRPISST_vPIS.Value;
  //
  COFINS_CST               := QrSCOFINS_CST.Value;
  COFINS_vBC               := QrSCOFINS_vBC.Value;
  COFINS_pCOFINS           := QrSCOFINS_pCOFINS.Value;
  COFINS_qBCProd           := QrSCOFINS_qBCProd.Value;
  COFINS_vAliqProd         := QrSCOFINS_vAliqProd.Value;
  COFINS_vCOFINS           := QrSCOFINS_vCOFINS.Value;
  //
  COFINSST_vBC             := QrTCOFINSST_vBC.Value;
  COFINSST_pCOFINS         := QrTCOFINSST_pCOFINS.Value;
  COFINSST_qBCProd         := QrTCOFINSST_qBCProd.Value;
  COFINSST_vAliqProd       := QrTCOFINSST_vAliqProd.Value;
  COFINSST_vCOFINS         := QrTCOFINSST_vCOFINS.Value;
  //
  HowLoad                  := Integer(TPQEHowLoad.pqehlPdvNFe);
  FatID                    := QrIFatID.Value;
  FatNum                   := QrIFatNum.Value;
  Empresa                  := QrIEmpresa.Value;
  nItem                    := QrInItem.Value;
  DtCorrApo                := '0000-00-00';
  // ini 2023-09-20
  //xLote                    := ''; // ver no futuro Grupo I80 - QrIxLote.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsI80, Dmod.MyDB, [
  'SELECT GROUP_CONCAT(rastro_nLote) xLotes ',
  'FROM nfeitsi80 ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND nItem=' + Geral.FF0(nItem),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  xLote := QrNFeItsI80.FieldByName('xLotes').AsString;
  //
  // fim 2023-09-20
  CodInfoEmit              := QrACodInfoEmit.Value;

  //Result :=
  EhServico := QrIEhServico.Value = 1;
  //
    RegrFiscal       := EdRegrFiscal.ValueVariant;
    EhServico        := QrIEhServico.Value = 1;
    CFOP_Emitido     := QrIprod_CFOP.Value;
    CRT_Emitido      := QrAemit_CRT.Value;
    CST_A_Emitido    := QrNICMS_Orig.Value;
    CST_B_Emitido    := QrNICMS_CST.Value;
    CSOSN_Emitido    := QrNICMS_CSOSN.Value;
    //PVD_MadeBy       := QrPediVdaMadeBy.Value;

    //
  DmNFe_0000.ObtemNumeroCFOP_Entrada(
    (*Tabela_FatPedFis: String; Parametros: array of integer;*)
    Empresa, CodInfoEmit, (*Item_MadeBy,*) (*PVD_MadeBy*)MadeBy, RegrFiscal, EhServico,
    CRT_Emitido, CST_A_Emitido, CST_B_Emitido, CSOSN_Emitido,
    Geral.FormataCFOP(Geral.FF0(CFOP_Emitido)),
    CFOP_Entrada, (*var ICMS_Aliq: Double;*)
    CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib,
    InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST,
    OriTES, EFD_II_C195, GenCtbD, GenCtbC,
    TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
    TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS,
    ICMSAliq, PISAliq, COFINSAliq, FisRegGenCtbD, FisRegGenCtbC);
(* Deprecado!
  if Dmod.QrControleNaoRecupImposPQ.Value = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
      'SELECT ICMSRec_pAliq, PISRec_pAliq,  ',
      'COFINSRec_pAliq  ',
      'FROM gragru1 ',
      'WHERE Nivel1=' + Geral.FF0(Insumo),
      '']);
    //
*)
  // Parei aqui!

  _ICMS_vBC                 := 0.00;
  _IPI_vBC                  := 0.00;
  _PIS_vBC                  := 0.00;
  _COFINS_vBC               := 0.00;
  AjusteVL_RED_BC           := 0.00;

  VL_OPR := prod_vProd + prod_vFrete + prod_vSeg + prod_vOutro - prod_vDesc;

  case OriTES of
    //N�o definido
    0:
    begin
      RpICMS                    := 0.00;
      RpICMSST                  := 0.00;
      RpIPI                     := 0.00;
      RpPIS                     := 0.00;
      RpCOFINS                  := 0.00;
      //
      RvICMS                    := 0.00;
      RvICMSST                  := 0.00;
      RvIPI                     := 0.00;
      RvPIS                     := 0.00;
      RvCOFINS                  := 0.00;
      //
      AjusteVL_OPR              := 0.00;
      AjusteVL_RED_BC           := 0.00;
      AjusteVL_OUTROS           := 0.00;
    end;
    1: // Livro fiscal: Tributa
    begin
      RpICMSST                  := ICMS_pICMSST;
      RpIPI                     := IPI_pIPI;
      RpPIS                     := PIS_pPIS;
      RpCOFINS                  := COFINS_pCOFINS;
      //
      RvIPI                     := IPI_vIPI;
      RvICMSST                  := ICMS_vICMSST;
      RvPIS                     := PIS_vPIS;
      RvCOFINS                  := COFINS_vCOFINS;
      //
      case TES_ICMS of
        0: // N�o tributa
        begin
          RpICMS                    := 0.00;
          RvICMS                    := 0.00;
        end;
        1: // do XML da NFe
        begin
          RpICMS                    := ICMS_pICMS;
          RvICMS                    := ICMS_vICMS;
        end;
        2: // da Regra Fiscal
        begin
          RpICMS                    := ICMSAliq;
          RvICMS                    := Geral.RoundC(ICMSAliq * ICMS_vBC / 100, 2);
        end;
        else
          Geral.MB_Erro('TES ICMS n�o implementado em ' + sProcName);
      end;
      case TES_BC_ICMS of
        0: _ICMS_vBC := 0.00;
        1: _ICMS_vBC := ICMS_vBC;
        2: _ICMS_vBC := VL_OPR;
      end;
      //
      case TES_IPI of
        0: // N�o tributa
        begin
          _IPI_vBC                 := 0.00;
          RpIPI                    := 0.00;
          RvIPI                    := 0.00;
        end;
        1: // do XML da NFe
        begin
          _IPI_vBC                 := IPI_vBC;
          RpIPI                    := IPI_pIPI;
          RvIPI                    := IPI_vIPI;
        end;
(*
        2: // da Regra Fiscal
        begin
          _IPI_vBC                 := IPI_vBC;
          RpIPI                    := IPIAliq;
          RvIPI                    := Geral.RoundC(IPIAliq * IPI_vBC / 100, 2);
        end;
*)
        else
          Geral.MB_Erro('TES IPI n�o implementado em ' + sProcName);
      end;
      case TES_BC_IPI of
        0: _IPI_vBC := 0.00;
        1: _IPI_vBC := IPI_vBC;
        2: _IPI_vBC := VL_OPR;
      end;
      //
      case TES_PIS of
        0: // N�o tributa
        begin
          _PIS_vBC                 := 0.00;
          RpPIS                    := 0.00;
          RvPIS                    := 0.00;
        end;
        1: // do XML da NFe
        begin
          _PIS_vBC                 := PIS_vBC;
          RpPIS                    := PIS_pPIS;
          RvPIS                    := PIS_vPIS;
        end;
        2: // da Regra Fiscal
        begin
          _PIS_vBC                 := PIS_vBC;
          RpPIS                    := PISAliq;
          RvPIS                    := Geral.RoundC(PISAliq * PIS_vBC / 100, 2);
        end;
        else
          Geral.MB_Erro('TES PIS n�o implementado em ' + sProcName);
      end;
      case TES_BC_PIS of
        0: _PIS_vBC := 0.00;
        1: _PIS_vBC := PIS_vBC;
        2: _PIS_vBC := VL_OPR;
      end;
      //
      case TES_COFINS of
        0: // N�o tributa
        begin
          _COFINS_vBC                 := 0.00;
          RpCOFINS                    := 0.00;
          RvCOFINS                    := 0.00;
        end;
        1: // do XML da NFe
        begin
          _COFINS_vBC                 := COFINS_vBC;
          RpCOFINS                    := COFINS_pCOFINS;
          RvCOFINS                    := COFINS_vCOFINS;
        end;
        2: // da Regra Fiscal
        begin
          _COFINS_vBC                 := COFINS_vBC;
          RpCOFINS                    := COFINSAliq;
          RvCOFINS                    := Geral.RoundC(COFINSAliq * COFINS_vBC / 100, 2);
        end;
        else
          Geral.MB_Erro('TES COFINS n�o implementado em ' + sProcName);
      end;
      case TES_BC_COFINS of
        0: _COFINS_vBC := 0.0;
        1: _COFINS_vBC := COFINS_vBC;
        2: _COFINS_vBC := VL_OPR;
      end;
      // Geral.MB_Info('Ver quando entrar pq se estah certo!'); OK!
      AjusteVL_OPR    := SPED_PF.FormulaVL_OPR(QrIprod_vProd.Value,
                         QrIprod_vFrete.Value, QrIprod_vSeg.Value,
                         QrIprod_vOutro.Value, QrIprod_vDesc.Value,
                         QrOIPI_vIPI.Value);
      AjusteVL_RED_BC := SPED_PF.FormulaVL_RED_BC(AjusteVL_OPR,
                         QrNICMS_vBC.Value, QrNICMS_pRedBC.Value);
      AjusteVL_OUTROS := SPED_PF.FormulaVL_OUTROS(AjusteVL_OPR,
                         QrNICMS_vBC.Value, QrNICMS_vICMSST.Value,
                         AjusteVL_RED_BC, QrOIPI_vIPI.Value);
    end;
    2: // Livro fiscal: Outros
    begin
      RpICMS                    := 0.00;
      RpICMSST                  := 0.00;
      RpIPI                     := 0.00;
      RpPIS                     := 0.00;
      RpCOFINS                  := 0.00;
      //
      RvICMS                    := 0.00;
      RvICMSST                  := 0.00;
      RvIPI                     := 0.00;
      RvPIS                     := 0.00;
      RvCOFINS                  := 0.00;
      //

      AjusteVL_OPR    := SPED_PF.FormulaVL_OPR(QrIprod_vProd.Value,
                         QrIprod_vFrete.Value, QrIprod_vSeg.Value,
                         QrIprod_vOutro.Value, QrIprod_vDesc.Value,
                         QrOIPI_vIPI.Value);
      (*
      AjusteVL_RED_BC := SPED_PF.FormulaVL_RED_BC(AjusteVL_OPR,
                         QrNICMS_vBC.Value, QrNICMS_pRedBC.Value);
      AjusteVL_OUTROS := SPED_PF.FormulaVL_OUTROS(AjusteVL_OPR,
                         QrNICMS_vBC.Value, QrNICMS_vICMSST.Value,
                         AjusteVL_RED_BC, QrOIPI_vIPI.Value);
      *)
      AjusteVL_RED_BC := 0.00;
      AjusteVL_OUTROS := AjusteVL_OPR; //� o valor total pois n�o credita nada!
    end;
    else
      Geral.MB_Erro('TES indefinido em ' + sProcName);
  end;
  AjusteVL_BC_ICMS := 0.00; //QrNICMS_vBC.Value;
  AjusteALIQ_ICMS  := 0.00; //QrNICMS_pICMS.Value;
  AjusteVL_ICMS    := 0.00; //QrNICMS_vICMS.Value;
  //
  NFeItsI_nItem             := nItem;
  //
  _prod_CFOP  := Geral.SoNumero_TT(CFOP_Entrada);
  //ICMS_Orig     := QrNICMS_Orig.Value;
  //_ICMS_CST   := SPED_PF.MontaCST_ICMS_Inn_de_CST_ICMS_emi(ICMS_Orig, InnICMS_CST);
  _ICMS_CST   := InnICMS_CST;
  _IPI_CST    := InnIPI_CST;
  _PIS_CST    := InnPIS_CST;
  _COFINS_CST := InnCOFINS_CST;
  //
  CustoItem          := ValorItem; //(ValorItem + IPI_vIPI) * CFin;
  TotalCusto         := CustoItem;
  //
  //OriPart            := QrPediVdaItsControle.Value;
  //
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PQEIts','PQEIts','Controle');
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqeits', False, [
  'OriPart',
  'Codigo', 'Conta', 'Insumo',
  'Volumes', 'PesoVB', 'PesoVL',
  'ValorItem', 'IPI', 'RIPI',
  'CFin', 'ICMS', 'RICMS',
  'TotalCusto', 'TotalPeso', 'Moeda',
  'Cotacao', 'RpICMS', 'RpPIS',
  'RpCOFINS', 'RvICMS', 'RvPIS',
  'RvCOFINS', 'prod_cProd', 'prod_cEAN',
  'prod_xProd', 'prod_NCM', 'prod_EX_TIPI',
  'prod_genero', 'prod_CFOP', 'prod_uCom',
  'prod_qCom', 'prod_vUnCom', 'prod_vProd',
  'prod_cEANTrib', 'prod_uTrib', 'prod_qTrib',
  'prod_vUnTrib', 'prod_vFrete', 'prod_vSeg', 'prod_vOutro',
  'prod_vDesc', 'ICMS_Orig', 'ICMS_CST',
  'ICMS_modBC', 'ICMS_pRedBC', 'ICMS_vBC',
  'ICMS_pICMS', 'ICMS_vICMS', 'ICMS_modBCST',
  'ICMS_pMVAST', 'ICMS_pRedBCST', 'ICMS_vBCST',
  'ICMS_pICMSST', 'ICMS_vICMSST', 'IPI_cEnq',
  'IPI_CST', 'IPI_vBC', 'IPI_qUnid',
  'IPI_vUnid', 'IPI_pIPI', 'IPI_vIPI',
  'PIS_CST', 'PIS_vBC', 'PIS_pPIS',
  'PIS_vPIS', 'PIS_qBCProd', 'PIS_vAliqProd',
  'PISST_vBC', 'PISST_pPIS', 'PISST_qBCProd',
  'PISST_vAliqProd', 'PISST_vPIS', 'COFINS_CST',
  'COFINS_vBC', 'COFINS_pCOFINS', 'COFINS_qBCProd',
  'COFINS_vAliqProd', 'COFINS_vCOFINS', 'COFINSST_vBC',
  'COFINSST_pCOFINS', 'COFINSST_qBCProd', 'COFINSST_vAliqProd',
  'COFINSST_vCOFINS', 'HowLoad', 'FatID',
  'FatNum', 'Empresa', 'nItem',
  'DtCorrApo', 'xLote', 'RpIPI',
  'RvIPI', 'NFeItsI_nItem', 'RpICMSST',
  'RvICMSST', 'TES_ICMS', 'TES_IPI',
  'TES_PIS', 'TES_COFINS',
  'FisRegGenCtbD', 'FisRegGenCtbC',
  'EFD_II_C195',
  'AjusteVL_BC_ICMS', 'AjusteALIQ_ICMS', 'AjusteVL_ICMS',
  'AjusteVL_OUTROS', 'AjusteVL_OPR', 'AjusteVL_RED_BC'], [
  'Controle'], [
  OriPart,
  Codigo, Conta, Insumo,
  Volumes, PesoVB, PesoVL,
  ValorItem, IPI, RIPI,
  CFin, ICMS, RICMS,
  TotalCusto, TotalPeso, Moeda,
  Cotacao, RpICMS, RpPIS,
  RpCOFINS, RvICMS, RvPIS,
  RvCOFINS, prod_cProd, prod_cEAN,
  prod_xProd, prod_NCM, prod_EX_TIPI,
  prod_genero, _prod_CFOP, prod_uCom,
  prod_qCom, prod_vUnCom, prod_vProd,
  prod_cEANTrib, prod_uTrib, prod_qTrib,
  prod_vUnTrib, prod_vFrete, prod_vSeg, prod_vOutro,
  prod_vDesc, ICMS_Orig, _ICMS_CST,
  ICMS_modBC, ICMS_pRedBC, _ICMS_vBC,
  ICMS_pICMS, ICMS_vICMS, ICMS_modBCST,
  ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST,
  ICMS_pICMSST, ICMS_vICMSST, IPI_cEnq,
  _IPI_CST, _IPI_vBC, IPI_qUnid,
  IPI_vUnid, IPI_pIPI, IPI_vIPI,
  _PIS_CST, _PIS_vBC, PIS_pPIS,
  PIS_vPIS, PIS_qBCProd, PIS_vAliqProd,
  PISST_vBC, PISST_pPIS, PISST_qBCProd,
  PISST_vAliqProd, PISST_vPIS, _COFINS_CST,
  _COFINS_vBC, COFINS_pCOFINS, COFINS_qBCProd,
  COFINS_vAliqProd, COFINS_vCOFINS, COFINSST_vBC,
  COFINSST_pCOFINS, COFINSST_qBCProd, COFINSST_vAliqProd,
  COFINSST_vCOFINS, HowLoad, FatID,
  FatNum, Empresa, nItem,
  DtCorrApo, xLote, RpIPI,
  RvIPI, NFeItsI_nItem, RpICMSST,
  RvICMSST, TES_ICMS, TES_IPI,
  TES_PIS, TES_COFINS,
  FisRegGenCtbD, FisRegGenCtbC,
  EFD_II_C195,
  AjusteVL_BC_ICMS, AjusteALIQ_ICMS, AjusteVL_ICMS,
  AjusteVL_OUTROS, AjusteVL_OPR, AjusteVL_RED_BC], [
  Controle], True);
  //
  if OriPart <> 0 then
    DmPediVda.AtualizaUmItemPediVda(OriPart);
end;
}

function TFmPQEXml.InserePQEItsDeNFe(const Codigo: Integer; var FisRegGenCtbD,
  FisRegGenCtbC, MadeBy, OriPart: Integer): Boolean;
const
  sProcName = 'TFmPQENew.InserePQEItsDeNFe()';
  procedure InsereItemOuLoteAtual(OriPart, Codigo, Conta, Insumo, Volumes:
  Integer; PesoVB, PesoVL, ValorItem, IPI, RIPI, CFin, ICMS, RICMS, TotalCusto,
  TotalPeso: Double; Moeda: Integer; Cotacao, RpICMS, RpPIS, RpCOFINS, RvICMS,
  RvPIS, RvCOFINS: Double; prod_cProd, prod_cEAN, prod_xProd, prod_NCM,
  prod_EX_TIPI: String; prod_genero: Integer; _prod_CFOP, prod_uCom: String;
  prod_qCom, prod_vUnCom, prod_vProd: Double;  prod_cEANTrib, prod_uTrib:
  String; prod_qTrib, prod_vUnTrib, prod_vFrete, prod_vSeg, prod_vOutro,
  prod_vDesc: Double; ICMS_Orig: Integer; _ICMS_CST: String; ICMS_modBC: Integer;
  ICMS_pRedBC, _ICMS_vBC, ICMS_pICMS, ICMS_vICMS: Double; ICMS_modBCST: Integer;
  ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST, ICMS_vICMSST: Double;
  IPI_cEnq, _IPI_CST: String; _IPI_vBC, IPI_qUnid, IPI_vUnid, IPI_pIPI,
  IPI_vIPI: Double; _PIS_CST: String; _PIS_vBC, PIS_pPIS, PIS_vPIS, PIS_qBCProd,
  PIS_vAliqProd, PISST_vBC, PISST_pPIS, PISST_qBCProd, PISST_vAliqProd,
  PISST_vPIS: Double; _COFINS_CST: String; _COFINS_vBC, COFINS_pCOFINS,
  COFINS_qBCProd, COFINS_vAliqProd, COFINS_vCOFINS, COFINSST_vBC,
  COFINSST_pCOFINS, COFINSST_qBCProd, COFINSST_vAliqProd, COFINSST_vCOFINS:
  Double; HowLoad, FatID, FatNum, Empresa, nItem: Integer; DtCorrApo, xLote:
  String; RpIPI, RvIPI: Double; NFeItsI_nItem: Integer; RpICMSST, RvICMSST:
  Double; TES_ICMS, TES_IPI, TES_PIS, TES_COFINS, FisRegGenCtbD, FisRegGenCtbC,
  EFD_II_C195: Integer; AjusteVL_BC_ICMS, AjusteALIQ_ICMS, AjusteVL_ICMS,
  AjusteVL_OUTROS, AjusteVL_OPR, AjusteVL_RED_BC: Double; dFab, dVal: String);
  var
    Controle: Integer;
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'PQEIts','PQEIts','Controle');
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqeits', False, [
    'OriPart',
    'Codigo', 'Conta', 'Insumo',
    'Volumes', 'PesoVB', 'PesoVL',
    'ValorItem', 'IPI', 'RIPI',
    'CFin', 'ICMS', 'RICMS',
    'TotalCusto', 'TotalPeso', 'Moeda',
    'Cotacao', 'RpICMS', 'RpPIS',
    'RpCOFINS', 'RvICMS', 'RvPIS',
    'RvCOFINS', 'prod_cProd', 'prod_cEAN',
    'prod_xProd', 'prod_NCM', 'prod_EX_TIPI',
    'prod_genero', 'prod_CFOP', 'prod_uCom',
    'prod_qCom', 'prod_vUnCom', 'prod_vProd',
    'prod_cEANTrib', 'prod_uTrib', 'prod_qTrib',
    'prod_vUnTrib', 'prod_vFrete', 'prod_vSeg', 'prod_vOutro',
    'prod_vDesc', 'ICMS_Orig', 'ICMS_CST',
    'ICMS_modBC', 'ICMS_pRedBC', 'ICMS_vBC',
    'ICMS_pICMS', 'ICMS_vICMS', 'ICMS_modBCST',
    'ICMS_pMVAST', 'ICMS_pRedBCST', 'ICMS_vBCST',
    'ICMS_pICMSST', 'ICMS_vICMSST', 'IPI_cEnq',
    'IPI_CST', 'IPI_vBC', 'IPI_qUnid',
    'IPI_vUnid', 'IPI_pIPI', 'IPI_vIPI',
    'PIS_CST', 'PIS_vBC', 'PIS_pPIS',
    'PIS_vPIS', 'PIS_qBCProd', 'PIS_vAliqProd',
    'PISST_vBC', 'PISST_pPIS', 'PISST_qBCProd',
    'PISST_vAliqProd', 'PISST_vPIS', 'COFINS_CST',
    'COFINS_vBC', 'COFINS_pCOFINS', 'COFINS_qBCProd',
    'COFINS_vAliqProd', 'COFINS_vCOFINS', 'COFINSST_vBC',
    'COFINSST_pCOFINS', 'COFINSST_qBCProd', 'COFINSST_vAliqProd',
    'COFINSST_vCOFINS', 'HowLoad', 'FatID',
    'FatNum', 'Empresa', 'nItem',
    'DtCorrApo', 'xLote', 'RpIPI',
    'RvIPI', 'NFeItsI_nItem', 'RpICMSST',
    'RvICMSST', 'TES_ICMS', 'TES_IPI',
    'TES_PIS', 'TES_COFINS',
    'FisRegGenCtbD', 'FisRegGenCtbC',
    'EFD_II_C195',
    'AjusteVL_BC_ICMS', 'AjusteALIQ_ICMS', 'AjusteVL_ICMS',
    'AjusteVL_OUTROS', 'AjusteVL_OPR', 'AjusteVL_RED_BC',
    'dFab', 'dVal'], [
    'Controle'], [
    OriPart,
    Codigo, Conta, Insumo,
    Volumes, PesoVB, PesoVL,
    ValorItem, IPI, RIPI,
    CFin, ICMS, RICMS,
    TotalCusto, TotalPeso, Moeda,
    Cotacao, RpICMS, RpPIS,
    RpCOFINS, RvICMS, RvPIS,
    RvCOFINS, prod_cProd, prod_cEAN,
    prod_xProd, prod_NCM, prod_EX_TIPI,
    prod_genero, _prod_CFOP, prod_uCom,
    prod_qCom, prod_vUnCom, prod_vProd,
    prod_cEANTrib, prod_uTrib, prod_qTrib,
    prod_vUnTrib, prod_vFrete, prod_vSeg, prod_vOutro,
    prod_vDesc, ICMS_Orig, _ICMS_CST,
    ICMS_modBC, ICMS_pRedBC, _ICMS_vBC,
    ICMS_pICMS, ICMS_vICMS, ICMS_modBCST,
    ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST,
    ICMS_pICMSST, ICMS_vICMSST, IPI_cEnq,
    _IPI_CST, _IPI_vBC, IPI_qUnid,
    IPI_vUnid, IPI_pIPI, IPI_vIPI,
    _PIS_CST, _PIS_vBC, PIS_pPIS,
    PIS_vPIS, PIS_qBCProd, PIS_vAliqProd,
    PISST_vBC, PISST_pPIS, PISST_qBCProd,
    PISST_vAliqProd, PISST_vPIS, _COFINS_CST,
    _COFINS_vBC, COFINS_pCOFINS, COFINS_qBCProd,
    COFINS_vAliqProd, COFINS_vCOFINS, COFINSST_vBC,
    COFINSST_pCOFINS, COFINSST_qBCProd, COFINSST_vAliqProd,
    COFINSST_vCOFINS, HowLoad, FatID,
    FatNum, Empresa, nItem,
    DtCorrApo, xLote, RpIPI,
    RvIPI, NFeItsI_nItem, RpICMSST,
    RvICMSST, TES_ICMS, TES_IPI,
    TES_PIS, TES_COFINS,
    FisRegGenCtbD, FisRegGenCtbC,
    EFD_II_C195,
    AjusteVL_BC_ICMS, AjusteALIQ_ICMS, AjusteVL_ICMS,
    AjusteVL_OUTROS, AjusteVL_OPR, AjusteVL_RED_BC,
    dFab, dVal], [
    Controle], True);
    //
    if OriPart <> 0 then
      DmPediVda.AtualizaUmItemPediVda(OriPart);

  end;
var
  prod_cProd, prod_cEAN, prod_xProd, prod_NCM, prod_EX_TIPI, prod_CFOP,
  prod_uCom, prod_cEANTrib, prod_uTrib, IPI_cEnq, DtCorrApo, xLote: String;
  //Codigo, Controle,
  Conta, Insumo, Volumes, Moeda, prod_genero, ICMS_Orig, ICMS_CST,
  ICMS_modBC, ICMS_modBCST, IPI_CST, PIS_CST, COFINS_CST, HowLoad: Integer;
  FatID, FatNum, Empresa, nItem, NFeItsI_nItem: Integer;
  PesoVB, PesoVL, ValorItem, IPI, RIPI, CFin, ICMS, RICMS, TotalCusto, TotalPeso,
  Cotacao: Double;
  RpICMS, RpPIS, RpCOFINS: Double;
  RvICMS, RvPIS, RvCOFINS,
  prod_qCom, prod_vUnCom,
  prod_vProd, prod_vFrete, prod_vSeg, prod_vOutro, prod_vDesc: Double;

  prod_qTrib, prod_vUnTrib, ICMS_pRedBC, ICMS_vBC, ICMS_pICMS, ICMS_vICMS,
  ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST, ICMS_vICMSST, IPI_vBC,
  IPI_qUnid, IPI_vUnid, IPI_pIPI, IPI_vIPI, PIS_vBC, PIS_pPIS, PIS_vPIS,
  PIS_qBCProd, PIS_vAliqProd, PISST_vBC, PISST_pPIS, PISST_qBCProd,
  PISST_vAliqProd, PISST_vPIS, COFINS_vBC, COFINS_pCOFINS, COFINS_qBCProd,
  COFINS_vAliqProd, COFINS_vCOFINS, COFINSST_vBC, COFINSST_pCOFINS,
  COFINSST_qBCProd, COFINSST_vAliqProd, COFINSST_vCOFINS, RpIPI, RvIPI,
  RpICMSST, RvICMSST: Double;

  SQLType: TSQLType;
  CustoItem: Double;
  EhServico: Boolean;
  //OriPart,
  CodInfoEmit, RegrFiscal, CFOP_Emitido, CRT_Emitido, CST_A_Emitido,
  CST_B_Emitido, CSOSN_Emitido: Integer;
  //
  CFOP_Entrada: String; (*var ICMS_Aliq: Double;*)
  CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer;
  InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST, TempX: String;
  OriTES, EFD_II_C195, GenCtbD, GenCtbC, CopyIni, CopyTam,
  TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
  TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS: Integer;
  //
  _prod_CFOP, _ICMS_CST, _IPI_CST, _PIS_CST, _COFINS_CST: String;
  _ICMS_vBC, _IPI_vBC, _PIS_vBC, _COFINS_vBC,
  ICMSAliq, PISAliq, COFINSAliq: Double;
  //FisRegGenCtbD, FisRegGenCtbC: Integer;
  AjusteVL_BC_ICMS, AjusteALIQ_ICMS, AjusteVL_ICMS, AjusteVL_OUTROS,
  AjusteVL_OPR, AjusteVL_RED_BC, ori_pIPI, ori_vIPI, VL_OPR: Double;
  dFab, dVal: String;
  Fator: Double;
  //
  Lote_PesoVB, Lote_PesoVL, Lote_ValorItem, Lote_IPI, Lote_RIPI, Lote_CFin,
  Lote_ICMS, Lote_RICMS, Lote_TotalPeso, Lote_prod_qCom,
  Lote_prod_vUnCom, Lote_prod_vProd, Lote_prod_qTrib, Lote_prod_vUnTrib,
  Lote_prod_vFrete, Lote_prod_vSeg, Lote_prod_vOutro, Lote_prod_vDesc,
  Lote_ICMS_vBC, Lote_ICMS_vICMS, Lote_ICMS_vICMSST, Lote_IPI_vBC,
  Lote_IPI_vIPI, Lote_PIS_vBC, Lote_PIS_vPIS, Lote_PISST_vBC, Lote_PISST_vPIS,
  Lote_COFINS_vBC, Lote_COFINS_vCOFINS, Lote_COFINSST_vBC,
  Lote_COFINSST_vCOFINS, Lote__ICMS_vBC, Lote__IPI_vBC, Lote__PIS_vBC,
  Lote__COFINS_vBC, Lote_AjusteVL_RED_BC, Lote_VL_OPR, Lote_RpICMS,
  Lote_RpICMSST, Lote_RpIPI, Lote_RpPIS, Lote_RpCOFINS, Lote_RvICMS,
  Lote_RvICMSST, Lote_RvIPI, Lote_RvPIS, Lote_RvCOFINS, Lote_AjusteVL_OPR,
  Lote_AjusteVL_OUTROS, Lote_AjusteVL_BC_ICMS, Lote_AjusteALIQ_ICMS,
  Lote_AjusteVL_ICMS, Lote_CustoItem, Lote_TotalCusto: Double;
begin
  SQLType        := stIns;
  //
  ReopenItsN();
  //Codigo             := Codigo;
  //Controle           := 0; // Abaixo
  Conta              := QrI.RecNO;
  Insumo             := QrINivel1.Value;
  if Insumo = 0 then
    Insumo           := DmProd.ObtemGraGru1deGraGruX(QrIGraGruX.Value);
  if Insumo = 0 then
  begin
    Geral.MB_Aviso('Insumo n�o pode ser zero! ' + sProcName);
    FmPQE.ExcluiTodaEntradaPvdNFeEmProgresso(Codigo);
    Exit;
  end;
  Volumes            := 1;
  PesoVB             := 0;
  PesoVL             := QrIprod_qCom.Value * QrIFatorQtd.Value; // Quantidade do Pedido! pode ser fracionado!
  ValorItem          := QrIprod_vTotItm.Value;
  IPI                := QrOIPI_vIPI.Value;
  RIPI               := 0;
  CFin               := 0;
  ICMS               := 0.00; // deprecado. QrNICMS_vICMS.Value;
  RICMS              := 0;
  TotalPeso          := Volumes * PesoVL;
  Moeda              := 0; // BRL
  Cotacao            := 0;
  prod_cProd               := QrIprod_cProd.Value;
  prod_cEAN                := QrIprod_cEAN.Value;
  prod_xProd               := QrIprod_xProd.Value;
  prod_NCM                 := QrIprod_NCM.Value;
  prod_EX_TIPI             := QrIprod_EXTIPI.Value;
  prod_CFOP                := Geral.FF0(QrIprod_CFOP.Value);
  prod_uCom                := QrIprod_uCom.Value;
  prod_cEANTrib            := QrIprod_cEANTrib.Value;
  prod_uTrib               := QrIprod_uTrib.Value;
  IPI_cEnq                 := QrOIPI_cEnq.Value;
  //
  prod_genero              := QrIprod_genero .Value;
  prod_qCom                := QrIprod_qCom.Value;
  prod_vUnCom              := QrIprod_vUnCom.Value;
  prod_vProd               := QrIprod_vProd.Value;
  prod_qTrib               := QrIprod_qTrib.Value;
  prod_vUnTrib             := QrIprod_vUnTrib.Value;
  prod_vFrete              := QrIprod_vFrete.Value;
  prod_vSeg                := QrIprod_vSeg.Value;
  prod_vOutro              := QrIprod_vOutro.Value;
  prod_vDesc               := QrIprod_vDesc.Value;
  //
  ICMS_Orig                := QrNICMS_Orig.Value;
  ICMS_modBC               := QrNICMS_modBC.Value;
  ICMS_modBCST             := QrNICMS_modBCST.Value;
  ICMS_pRedBC              := QrNICMS_pRedBC.Value;
  ICMS_CST                 := QrNICMS_CST.Value;
  ICMS_vBC                 := QrNICMS_vBC.Value;
  ICMS_pICMS               := QrNICMS_pICMS.Value;
  ICMS_vICMS               := QrNICMS_vICMS.Value;
  ICMS_modBCST             := QrNICMS_modBCST.Value;
  ICMS_pMVAST              := QrNICMS_pMVAST.Value;
  ICMS_pRedBCST            := QrNICMS_pRedBCST.Value;
  ICMS_vBCST               := QrNICMS_vBCST.Value;
  ICMS_pICMSST             := QrNICMS_pICMSST.Value;
  ICMS_vICMSST             := QrNICMS_vICMSST.Value;
  IPI_cEnq                 := QrOIPI_cEnq.Value;
  //
  IPI_CST                  := QrOIPI_CST.Value;
  IPI_vBC                  := QrOIPI_vBC.Value;
  IPI_qUnid                := QrOIPI_qUnid.Value;
  IPI_vUnid                := QrOIPI_vUnid.Value;
  IPI_pIPI                 := QrOIPI_pIPI.Value;
  IPI_vIPI                 := QrOIPI_vIPI.Value;
  //
  PIS_CST                  := QrQPIS_CST.Value;
  PIS_vBC                  := QrQPIS_vBC.Value;
  PIS_pPIS                 := QrQPIS_pPIS.Value;
  PIS_vPIS                 := QrQPIS_vPIS.Value;
  PIS_qBCProd              := QrQPIS_qBCProd.Value;
  PIS_vAliqProd            := QrQPIS_vAliqProd.Value;
  //
  PISST_vBC                := QrRPISST_vBC.Value;
  PISST_pPIS               := QrRPISST_pPIS.Value;
  PISST_qBCProd            := QrRPISST_qBCProd.Value;
  PISST_vAliqProd          := QrRPISST_vAliqProd.Value;
  PISST_vPIS               := QrRPISST_vPIS.Value;
  //
  COFINS_CST               := QrSCOFINS_CST.Value;
  COFINS_vBC               := QrSCOFINS_vBC.Value;
  COFINS_pCOFINS           := QrSCOFINS_pCOFINS.Value;
  COFINS_qBCProd           := QrSCOFINS_qBCProd.Value;
  COFINS_vAliqProd         := QrSCOFINS_vAliqProd.Value;
  COFINS_vCOFINS           := QrSCOFINS_vCOFINS.Value;
  //
  COFINSST_vBC             := QrTCOFINSST_vBC.Value;
  COFINSST_pCOFINS         := QrTCOFINSST_pCOFINS.Value;
  COFINSST_qBCProd         := QrTCOFINSST_qBCProd.Value;
  COFINSST_vAliqProd       := QrTCOFINSST_vAliqProd.Value;
  COFINSST_vCOFINS         := QrTCOFINSST_vCOFINS.Value;
  //
  HowLoad                  := Integer(TPQEHowLoad.pqehlPdvNFe);
  FatID                    := QrIFatID.Value;
  FatNum                   := QrIFatNum.Value;
  Empresa                  := QrIEmpresa.Value;
  nItem                    := QrInItem.Value;
  DtCorrApo                := '0000-00-00';
  xLote                    := ''; // ver abaixo
  dFab                     := '0000-00-00'; // ver abaixo
  dVal                     := '0000-00-00'; // ver abaixo
  //
  // fim 2023-09-20
  CodInfoEmit              := QrACodInfoEmit.Value;

  //Result :=
  EhServico := QrIEhServico.Value = 1;
  //
    RegrFiscal       := EdRegrFiscal.ValueVariant;
    EhServico        := QrIEhServico.Value = 1;
    CFOP_Emitido     := QrIprod_CFOP.Value;
    CRT_Emitido      := QrAemit_CRT.Value;
    CST_A_Emitido    := QrNICMS_Orig.Value;
    CST_B_Emitido    := QrNICMS_CST.Value;
    CSOSN_Emitido    := QrNICMS_CSOSN.Value;
    //PVD_MadeBy       := QrPediVdaMadeBy.Value;

    //
  DmNFe_0000.ObtemNumeroCFOP_Entrada(
    (*Tabela_FatPedFis: String; Parametros: array of integer;*)
    Empresa, CodInfoEmit, (*Item_MadeBy,*) (*PVD_MadeBy*)MadeBy, RegrFiscal, EhServico,
    CRT_Emitido, CST_A_Emitido, CST_B_Emitido, CSOSN_Emitido,
    Geral.FormataCFOP(Geral.FF0(CFOP_Emitido)),
    CFOP_Entrada, (*var ICMS_Aliq: Double;*)
    CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib,
    InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST,
    OriTES, EFD_II_C195, GenCtbD, GenCtbC,
    TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
    TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS,
    ICMSAliq, PISAliq, COFINSAliq, FisRegGenCtbD, FisRegGenCtbC);
(* Deprecado!
  if Dmod.QrControleNaoRecupImposPQ.Value = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
      'SELECT ICMSRec_pAliq, PISRec_pAliq,  ',
      'COFINSRec_pAliq  ',
      'FROM gragru1 ',
      'WHERE Nivel1=' + Geral.FF0(Insumo),
      '']);
    //
*)
  // Parei aqui!

  _ICMS_vBC                 := 0.00;
  _IPI_vBC                  := 0.00;
  _PIS_vBC                  := 0.00;
  _COFINS_vBC               := 0.00;
  AjusteVL_RED_BC           := 0.00;

  VL_OPR := prod_vProd + prod_vFrete + prod_vSeg + prod_vOutro - prod_vDesc;

  case OriTES of
    //N�o definido
    0:
    begin
      RpICMS                    := 0.00;
      RpICMSST                  := 0.00;
      RpIPI                     := 0.00;
      RpPIS                     := 0.00;
      RpCOFINS                  := 0.00;
      //
      RvICMS                    := 0.00;
      RvICMSST                  := 0.00;
      RvIPI                     := 0.00;
      RvPIS                     := 0.00;
      RvCOFINS                  := 0.00;
      //
      AjusteVL_OPR              := 0.00;
      AjusteVL_RED_BC           := 0.00;
      AjusteVL_OUTROS           := 0.00;
    end;
    1: // Livro fiscal: Tributa
    begin
      RpICMSST                  := ICMS_pICMSST;
      RpIPI                     := IPI_pIPI;
      RpPIS                     := PIS_pPIS;
      RpCOFINS                  := COFINS_pCOFINS;
      //
      RvIPI                     := IPI_vIPI;
      RvICMSST                  := ICMS_vICMSST;
      RvPIS                     := PIS_vPIS;
      RvCOFINS                  := COFINS_vCOFINS;
      //
      case TES_ICMS of
        0: // N�o tributa
        begin
          RpICMS                    := 0.00;
          RvICMS                    := 0.00;
        end;
        1: // do XML da NFe
        begin
          RpICMS                    := ICMS_pICMS;
          RvICMS                    := ICMS_vICMS;
        end;
        2: // da Regra Fiscal
        begin
          RpICMS                    := ICMSAliq;
          RvICMS                    := Geral.RoundC(ICMSAliq * ICMS_vBC / 100, 2);
        end;
        else
          Geral.MB_Erro('TES ICMS n�o implementado em ' + sProcName);
      end;
      case TES_BC_ICMS of
        0: _ICMS_vBC := 0.00;
        1: _ICMS_vBC := ICMS_vBC;
        2: _ICMS_vBC := VL_OPR;
      end;
      //
      case TES_IPI of
        0: // N�o tributa
        begin
          _IPI_vBC                 := 0.00;
          RpIPI                    := 0.00;
          RvIPI                    := 0.00;
        end;
        1: // do XML da NFe
        begin
          _IPI_vBC                 := IPI_vBC;
          RpIPI                    := IPI_pIPI;
          RvIPI                    := IPI_vIPI;
        end;
(*
        2: // da Regra Fiscal
        begin
          _IPI_vBC                 := IPI_vBC;
          RpIPI                    := IPIAliq;
          RvIPI                    := Geral.RoundC(IPIAliq * IPI_vBC / 100, 2);
        end;
*)
        else
          Geral.MB_Erro('TES IPI n�o implementado em ' + sProcName);
      end;
      case TES_BC_IPI of
        0: _IPI_vBC := 0.00;
        1: _IPI_vBC := IPI_vBC;
        2: _IPI_vBC := VL_OPR;
      end;
      //
      case TES_PIS of
        0: // N�o tributa
        begin
          _PIS_vBC                 := 0.00;
          RpPIS                    := 0.00;
          RvPIS                    := 0.00;
        end;
        1: // do XML da NFe
        begin
          _PIS_vBC                 := PIS_vBC;
          RpPIS                    := PIS_pPIS;
          RvPIS                    := PIS_vPIS;
        end;
        2: // da Regra Fiscal
        begin
          _PIS_vBC                 := PIS_vBC;
          RpPIS                    := PISAliq;
          RvPIS                    := Geral.RoundC(PISAliq * PIS_vBC / 100, 2);
        end;
        else
          Geral.MB_Erro('TES PIS n�o implementado em ' + sProcName);
      end;
      case TES_BC_PIS of
        0: _PIS_vBC := 0.00;
        1: _PIS_vBC := PIS_vBC;
        2: _PIS_vBC := VL_OPR;
      end;
      //
      case TES_COFINS of
        0: // N�o tributa
        begin
          _COFINS_vBC                 := 0.00;
          RpCOFINS                    := 0.00;
          RvCOFINS                    := 0.00;
        end;
        1: // do XML da NFe
        begin
          _COFINS_vBC                 := COFINS_vBC;
          RpCOFINS                    := COFINS_pCOFINS;
          RvCOFINS                    := COFINS_vCOFINS;
        end;
        2: // da Regra Fiscal
        begin
          _COFINS_vBC                 := COFINS_vBC;
          RpCOFINS                    := COFINSAliq;
          RvCOFINS                    := Geral.RoundC(COFINSAliq * COFINS_vBC / 100, 2);
        end;
        else
          Geral.MB_Erro('TES COFINS n�o implementado em ' + sProcName);
      end;
      case TES_BC_COFINS of
        0: _COFINS_vBC := 0.0;
        1: _COFINS_vBC := COFINS_vBC;
        2: _COFINS_vBC := VL_OPR;
      end;
      // Geral.MB_Info('Ver quando entrar pq se estah certo!'); OK!
      AjusteVL_OPR    := SPED_PF.FormulaVL_OPR(QrIprod_vProd.Value,
                         QrIprod_vFrete.Value, QrIprod_vSeg.Value,
                         QrIprod_vOutro.Value, QrIprod_vDesc.Value,
                         QrOIPI_vIPI.Value);
      AjusteVL_RED_BC := SPED_PF.FormulaVL_RED_BC(AjusteVL_OPR,
                         QrNICMS_vBC.Value, QrNICMS_pRedBC.Value);
      AjusteVL_OUTROS := SPED_PF.FormulaVL_OUTROS(AjusteVL_OPR,
                         QrNICMS_vBC.Value, QrNICMS_vICMSST.Value,
                         AjusteVL_RED_BC, QrOIPI_vIPI.Value);
    end;
    2: // Livro fiscal: Outros
    begin
      RpICMS                    := 0.00;
      RpICMSST                  := 0.00;
      RpIPI                     := 0.00;
      RpPIS                     := 0.00;
      RpCOFINS                  := 0.00;
      //
      RvICMS                    := 0.00;
      RvICMSST                  := 0.00;
      RvIPI                     := 0.00;
      RvPIS                     := 0.00;
      RvCOFINS                  := 0.00;
      //

      AjusteVL_OPR    := SPED_PF.FormulaVL_OPR(QrIprod_vProd.Value,
                         QrIprod_vFrete.Value, QrIprod_vSeg.Value,
                         QrIprod_vOutro.Value, QrIprod_vDesc.Value,
                         QrOIPI_vIPI.Value);
      (*
      AjusteVL_RED_BC := SPED_PF.FormulaVL_RED_BC(AjusteVL_OPR,
                         QrNICMS_vBC.Value, QrNICMS_pRedBC.Value);
      AjusteVL_OUTROS := SPED_PF.FormulaVL_OUTROS(AjusteVL_OPR,
                         QrNICMS_vBC.Value, QrNICMS_vICMSST.Value,
                         AjusteVL_RED_BC, QrOIPI_vIPI.Value);
      *)
      AjusteVL_RED_BC := 0.00;
      AjusteVL_OUTROS := AjusteVL_OPR; //� o valor total pois n�o credita nada!
    end;
    else
      Geral.MB_Erro('TES indefinido em ' + sProcName);
  end;
  AjusteVL_BC_ICMS := 0.00; //QrNICMS_vBC.Value;
  AjusteALIQ_ICMS  := 0.00; //QrNICMS_pICMS.Value;
  AjusteVL_ICMS    := 0.00; //QrNICMS_vICMS.Value;
  //
  NFeItsI_nItem             := nItem;
  //
  _prod_CFOP  := Geral.SoNumero_TT(CFOP_Entrada);
  //ICMS_Orig     := QrNICMS_Orig.Value;
  //_ICMS_CST   := SPED_PF.MontaCST_ICMS_Inn_de_CST_ICMS_emi(ICMS_Orig, InnICMS_CST);
  _ICMS_CST   := InnICMS_CST;
  _IPI_CST    := InnIPI_CST;
  _PIS_CST    := InnPIS_CST;
  _COFINS_CST := InnCOFINS_CST;
  //
  CustoItem          := ValorItem; //(ValorItem + IPI_vIPI) * CFin;
  TotalCusto         := CustoItem;
  //
  //OriPart            := QrPediVdaItsControle.Value;
  //

  UnDmkDAC_PF.AbreMySQLQuery0(QrItsI80, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsi80 ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrI80s, Dmod.MyDB, [
  'SELECT SUM(rastro_qLote) rastro_qLote,',
  'GROUP_CONCAT(rastro_nLote) xLotes',
  'FROM nfeitsi80 ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
  if (QrItsI80.RecordCount < 2) or
  (DmProd.QrOpcoesGrad.FieldByName('LoadXMLPorLoteFab').AsInteger = 0) or
  (PesoVL = 0) or
  (QrI80s.FieldByName('rastro_qLote').AsFloat <> PesoVL) then
  begin
    if (QrItsI80.RecordCount = 1) and
    (QrI80s.FieldByName('rastro_qLote').AsFloat <> PesoVL) then
      Geral.MB_Info('Quantidade do item ' + Geral.FF0(nItem) +
      ' difere da soma dos pesos do lotes: ' + sLineBreak +
      'Quantidade item: ' + FloatToStr(PesoVL) + sLineBreak +
      'Quantidade lotes: ' +
      FloatToStr(QrI80s.FieldByName('rastro_qLote').AsFloat) + sLineBreak +
      'O item n�o ser� dividido em lotes unit�rios!');
    xLote := QrI80s.FieldByName('xLotes').AsString;
    //
    if QrItsI80.RecordCount = 1 then
    begin
      dFab  := Geral.FDT(QrItsI80rastro_dFab.Value, 1);
      dVal  := Geral.FDT(QrItsI80rastro_dVal.Value, 1);
    end else
    begin
      dFab  := '1899-12-30';
      dVal  := '1899-12-30';
    end;
    //
    InsereItemOuLoteAtual(
    OriPart,
    Codigo, Conta, Insumo,
    Volumes, PesoVB, PesoVL,
    ValorItem, IPI, RIPI,
    CFin, ICMS, RICMS,
    TotalCusto, TotalPeso, Moeda,
    Cotacao, RpICMS, RpPIS,
    RpCOFINS, RvICMS, RvPIS,
    RvCOFINS, prod_cProd, prod_cEAN,
    prod_xProd, prod_NCM, prod_EX_TIPI,
    prod_genero, _prod_CFOP, prod_uCom,
    prod_qCom, prod_vUnCom, prod_vProd,
    prod_cEANTrib, prod_uTrib, prod_qTrib,
    prod_vUnTrib, prod_vFrete, prod_vSeg, prod_vOutro,
    prod_vDesc, ICMS_Orig, _ICMS_CST,
    ICMS_modBC, ICMS_pRedBC, _ICMS_vBC,
    ICMS_pICMS, ICMS_vICMS, ICMS_modBCST,
    ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST,
    ICMS_pICMSST, ICMS_vICMSST, IPI_cEnq,
    _IPI_CST, _IPI_vBC, IPI_qUnid,
    IPI_vUnid, IPI_pIPI, IPI_vIPI,
    _PIS_CST, _PIS_vBC, PIS_pPIS,
    PIS_vPIS, PIS_qBCProd, PIS_vAliqProd,
    PISST_vBC, PISST_pPIS, PISST_qBCProd,
    PISST_vAliqProd, PISST_vPIS, _COFINS_CST,
    _COFINS_vBC, COFINS_pCOFINS, COFINS_qBCProd,
    COFINS_vAliqProd, COFINS_vCOFINS, COFINSST_vBC,
    COFINSST_pCOFINS, COFINSST_qBCProd, COFINSST_vAliqProd,
    COFINSST_vCOFINS, HowLoad, FatID,
    FatNum, Empresa, nItem,
    DtCorrApo, xLote, RpIPI,
    RvIPI, NFeItsI_nItem, RpICMSST,
    RvICMSST, TES_ICMS, TES_IPI,
    TES_PIS, TES_COFINS,
    FisRegGenCtbD, FisRegGenCtbC,
    EFD_II_C195,
    AjusteVL_BC_ICMS, AjusteALIQ_ICMS, AjusteVL_ICMS,
    AjusteVL_OUTROS, AjusteVL_OPR, AjusteVL_RED_BC,
    dFab, dVal);
  end else
  begin
    QrItsI80.First;
    while not QrItsI80.Eof do
    begin
      xLote := QrItsI80rastro_nLote.Value;
      dFab  := Geral.FDT(QrItsI80rastro_dFab.Value, 1);
      dVal  := Geral.FDT(QrItsI80rastro_dVal.Value, 1);
      //
      Fator := QrItsI80rastro_qLote.Value / PesoVL;
      //
      Lote_PesoVB             := Fator * PesoVB;
      Lote_PesoVL             := Fator * PesoVL;
      Lote_ValorItem          := Fator * ValorItem;
      Lote_IPI                := Fator * IPI;
      Lote_RIPI               := Fator * RIPI;
      Lote_CFin               := Fator * CFin;
      Lote_ICMS               := Fator * ICMS;
      Lote_RICMS              := Fator * RICMS;
      Lote_TotalPeso          := Fator * TotalPeso;
      Lote_prod_qCom          := Fator * prod_qCom;
      Lote_prod_vUnCom        := Fator * prod_vUnCom;
      Lote_prod_vProd         := Fator * prod_vProd;
      Lote_prod_qTrib         := Fator * prod_qTrib;
      Lote_prod_vUnTrib       := Fator * prod_vUnTrib;
      Lote_prod_vFrete        := Fator * prod_vFrete;
      Lote_prod_vSeg          := Fator * prod_vSeg;
      Lote_prod_vOutro        := Fator * prod_vOutro;
      Lote_prod_vDesc         := Fator * prod_vDesc;
      //
      Lote_ICMS_vBC           := Fator * ICMS_vBC;
      Lote_ICMS_vICMS         := Fator * ICMS_vICMS;
      Lote_ICMS_vICMSST       := Fator * ICMS_vICMSST;
      //
      Lote_IPI_vBC            := Fator * IPI_vBC;
      Lote_IPI_vIPI           := Fator * IPI_vIPI;
      //
      Lote_PIS_vBC            := Fator * PIS_vBC;
      Lote_PIS_vPIS           := Fator * PIS_vPIS;
      //
      Lote_PISST_vBC          := Fator * PISST_vBC;
      Lote_PISST_vPIS         := Fator * PISST_vPIS;
      //
      Lote_COFINS_vBC         := Fator * COFINS_vBC;
      Lote_COFINS_vCOFINS     := Fator * COFINS_vCOFINS;
      //
      Lote_COFINSST_vBC       := Fator * COFINSST_vBC;
      Lote_COFINSST_vCOFINS   := Fator * COFINSST_vCOFINS;
      //
      Lote__ICMS_vBC          := Fator * _ICMS_vBC;
      Lote__IPI_vBC           := Fator * _IPI_vBC;
      Lote__PIS_vBC           := Fator * _PIS_vBC;
      Lote__COFINS_vBC        := Fator * _COFINS_vBC;

      Lote_VL_OPR             := Fator * VL_OPR;

      Lote_RpICMS             := Fator * RpICMS;
      Lote_RpICMSST           := Fator * RpICMSST;
      Lote_RpIPI              := Fator * RpIPI;
      Lote_RpPIS              := Fator * RpPIS;
      Lote_RpCOFINS           := Fator * RpCOFINS;
      //
      Lote_RvICMS             := Fator * RvICMS;
      Lote_RvICMSST           := Fator * RvICMSST;
      Lote_RvIPI              := Fator * RvIPI;
      Lote_RvPIS              := Fator * RvPIS;
      Lote_RvCOFINS           := Fator * RvCOFINS;
      //
      Lote_AjusteVL_OPR       := Fator * AjusteVL_OPR;
      Lote_AjusteVL_RED_BC    := Fator * AjusteVL_RED_BC;
      Lote_AjusteVL_OUTROS    := Fator * AjusteVL_OUTROS;
      Lote_AjusteVL_BC_ICMS   := Fator * AjusteVL_BC_ICMS;
      Lote_AjusteALIQ_ICMS    := Fator * AjusteALIQ_ICMS;
      Lote_AjusteVL_ICMS      := Fator * AjusteVL_ICMS;
      //
      Lote_CustoItem          := Fator * CustoItem;
      Lote_TotalCusto         := Fator * TotalCusto;
      //
      InsereItemOuLoteAtual(
      OriPart,
      Codigo, Conta, Insumo,
      Volumes, Lote_PesoVB, Lote_PesoVL,
      Lote_ValorItem, Lote_IPI, Lote_RIPI,
      Lote_CFin, Lote_ICMS, Lote_RICMS,
      Lote_TotalCusto, Lote_TotalPeso, Moeda,
      Cotacao, Lote_RpICMS, Lote_RpPIS,
      Lote_RpCOFINS, Lote_RvICMS, Lote_RvPIS,
      Lote_RvCOFINS, prod_cProd, prod_cEAN,
      prod_xProd, prod_NCM, prod_EX_TIPI,
      prod_genero, _prod_CFOP, prod_uCom,
      Lote_prod_qCom, Lote_prod_vUnCom, Lote_prod_vProd,
      prod_cEANTrib, prod_uTrib, Lote_prod_qTrib,
      Lote_prod_vUnTrib, Lote_prod_vFrete, Lote_prod_vSeg, Lote_prod_vOutro,
      Lote_prod_vDesc, ICMS_Orig, _ICMS_CST,
      ICMS_modBC, ICMS_pRedBC, Lote__ICMS_vBC,
      ICMS_pICMS, Lote_ICMS_vICMS, ICMS_modBCST,
      ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST,
      ICMS_pICMSST, Lote_ICMS_vICMSST, IPI_cEnq,
      _IPI_CST, Lote__IPI_vBC, IPI_qUnid,
      IPI_vUnid, IPI_pIPI, Lote_IPI_vIPI,
      _PIS_CST, Lote__PIS_vBC, PIS_pPIS,
      Lote_PIS_vPIS, PIS_qBCProd, PIS_vAliqProd,
      Lote_PISST_vBC, PISST_pPIS, PISST_qBCProd,
      PISST_vAliqProd, Lote_PISST_vPIS, _COFINS_CST,
      Lote__COFINS_vBC, COFINS_pCOFINS, COFINS_qBCProd,
      COFINS_vAliqProd, Lote_COFINS_vCOFINS, Lote_COFINSST_vBC,
      COFINSST_pCOFINS, COFINSST_qBCProd, COFINSST_vAliqProd,
      Lote_COFINSST_vCOFINS, HowLoad, FatID,
      FatNum, Empresa, nItem,
      DtCorrApo, xLote, Lote_RpIPI,
      Lote_RvIPI, NFeItsI_nItem, Lote_RpICMSST,
      Lote_RvICMSST, TES_ICMS, TES_IPI,
      TES_PIS, TES_COFINS,
      FisRegGenCtbD, FisRegGenCtbC,
      EFD_II_C195,
      Lote_AjusteVL_BC_ICMS, Lote_AjusteALIQ_ICMS, Lote_AjusteVL_ICMS,
      Lote_AjusteVL_OUTROS, Lote_AjusteVL_OPR, Lote_AjusteVL_RED_BC,
      dFab, dVal);
      //
      QrItsI80.Next;
    end;
  end;
end;

(*
object Label16: TLabel
  Left = 8
  Top = 131
  Width = 171
  Height = 13
  Caption = 'Indicador do tipo de pagamento [F4]'
  Enabled = False
  FocusControl = EdIND_PGTO
end
object EdIND_PGTO: TdmkEdit
  Left = 8
  Top = 147
  Width = 21
  Height = 21
  Alignment = taRightJustify
  Enabled = False
  TabOrder = 6
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ValMin = '0'
  ValMax = '9'
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  QryCampo = 'IND_PGTO'
  UpdCampo = 'IND_PGTO'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
  ValWarn = False
  OnChange = EdIND_PGTOChange
  OnKeyDown = EdIND_PGTOKeyDown
end
object EdIND_PGTO_TXT: TdmkEdit
  Left = 30
  Top = 147
  Width = 232
  Height = 21
  Enabled = False
  ReadOnly = True
  TabOrder = 7
  FormatType = dmktfString
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = ''
  ValWarn = False
end
object EdIND_FRT: TdmkEdit
  Left = 266
  Top = 147
  Width = 21
  Height = 21
  Alignment = taRightJustify
  Enabled = False
  TabOrder = 8
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ValMin = '0'
  ValMax = '9'
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  QryCampo = 'IND_PGTO'
  UpdCampo = 'IND_PGTO'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
  ValWarn = False
  OnChange = EdIND_FRTChange
  OnKeyDown = EdIND_FRTKeyDown
end
object Label108: TLabel
  Left = 266
  Top = 131
  Width = 139
  Height = 13
  Caption = 'Indicador do tipo de frete [F4]'
  Enabled = False
  FocusControl = EdIND_FRT
end
object EdIND_FRT_TXT: TdmkEdit
  Left = 288
  Top = 147
  Width = 232
  Height = 21
  Enabled = False
  ReadOnly = True
  TabOrder = 9
  FormatType = dmktfString
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = ''
  ValWarn = False
end
*)
end.
