unit PQPed;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  Grids, DBGrids, UnInternalConsts, UnMsgInt, UnInternalConsts2, UnGOTOy,
  ResIntStrings, ZCF2, Menus, ShellAPI, UMySQLModule, mySQLDbTables,
  UnMyLinguas, frxClass, Variants, dmkGeral, UnDmkProcFunc, dmkLabel, dmkImage,
  UnDmkEnums;

type
  TFmPQPed = class(TForm)
    PainelDados: TPanel;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    PainelGrid1: TPanel;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    QrPQPed: TmySQLQuery;
    DsPQPed: TDataSource;
    QrPQPedIts: TmySQLQuery;
    DsPQPedIts: TDataSource;
    QrPQPedCodigo: TIntegerField;
    QrPQPedData: TDateField;
    QrPQPedEntrega: TDateField;
    QrPQPedFornece: TIntegerField;
    QrPQPedContato: TWideStringField;
    QrPQPedTransporte: TIntegerField;
    QrPQPedPesoL: TFloatField;
    QrPQPedPesoB: TFloatField;
    QrPQPedValor: TFloatField;
    QrPQPedEntrada: TFloatField;
    QrPQPedSit: TIntegerField;
    QrPQPedLk: TIntegerField;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    QrPQPedNOMETRANSPORTADORA: TWideStringField;
    QrPQPedNOMEFORNECEDOR: TWideStringField;
    QrIns: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrPQPedItsCodigo: TIntegerField;
    QrPQPedItsControle: TIntegerField;
    QrPQPedItsConta: TIntegerField;
    QrPQPedItsInsumo: TIntegerField;
    QrPQPedItsVolumes: TIntegerField;
    QrPQPedItsPesoVB: TFloatField;
    QrPQPedItsPesoVL: TFloatField;
    QrPQPedItsValorItem: TFloatField;
    QrPQPedItsIPI: TFloatField;
    QrPQPedItsTotalCusto: TFloatField;
    QrPQPedItsTotalPeso: TFloatField;
    QrPQPedItsNOMEINSUMO: TWideStringField;
    QrPQPedItsPreco: TFloatField;
    QrPQPedItsICMS: TFloatField;
    QrSoma: TmySQLQuery;
    QrPQPedItsEntrada: TFloatField;
    QrSomaPesoB: TFloatField;
    QrSomaPesoL: TFloatField;
    QrSomaValor: TFloatField;
    QrSomaEntrada: TFloatField;
    QrSomaUpd: TmySQLQuery;
    QrPQPedItsTOTALBRUTO: TFloatField;
    QrEncerra: TmySQLQuery;
    Label12: TLabel;
    DBEdit11: TDBEdit;
    QrPQPedSetor: TSmallintField;
    QrPQPedNOMESETOR: TWideStringField;
    QrPQPedItsPrazo: TWideStringField;
    QrPQPedItsCFin: TFloatField;
    EdEmail: TLabel;
    QrPQPedEMail: TWideStringField;
    DsFornecedor: TDataSource;
    QrFornecedor: TmySQLQuery;
    QrConsts: TmySQLQuery;
    QrConstsCompras: TWideStringField;
    QrConstsCaleiro: TWideStringField;
    QrConstsCurtimento: TWideStringField;
    QrConstsRecurtimento: TWideStringField;
    QrConstsAcabamento: TWideStringField;
    QrConstsCaldeiraETE: TWideStringField;
    QrTransportador: TmySQLQuery;
    DsConsts: TDataSource;
    DsTransportador: TDataSource;
    EdMailContato: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    QrPQPedEMailContato: TWideStringField;
    Label15: TLabel;
    EdDataEnvio: TLabel;
    QrPQPedDataEmail: TDateTimeField;
    QrPQPedItsddEstq: TFloatField;
    QrPQPedItsEstqkg: TFloatField;
    PmImprime: TPopupMenu;
    Standard1: TMenuItem;
    Completo1: TMenuItem;
    QrPQPedItsUCCodigo: TIntegerField;
    QrPQPedItsUCConta: TIntegerField;
    QrPQPedItsUCControle: TIntegerField;
    QrPQPedItsConsumodd: TIntegerField;
    QrPQPedItsConsumokg: TFloatField;
    DsPQPedIts2: TDataSource;
    QrPQPedIts2: TmySQLQuery;
    QrPQE: TmySQLQuery;
    QrPQEIts: TmySQLQuery;
    QrPQPedIts2Codigo: TIntegerField;
    QrPQPedIts2Controle: TIntegerField;
    QrPQPedIts2Conta: TIntegerField;
    QrPQPedIts2Insumo: TIntegerField;
    QrPQPedIts2Volumes: TIntegerField;
    QrPQPedIts2PesoVB: TFloatField;
    QrPQPedIts2PesoVL: TFloatField;
    QrPQPedIts2ValorItem: TFloatField;
    QrPQPedIts2IPI: TFloatField;
    QrPQPedIts2TotalCusto: TFloatField;
    QrPQPedIts2TotalPeso: TFloatField;
    QrPQPedIts2Preco: TFloatField;
    QrPQPedIts2ICMS: TFloatField;
    QrPQPedIts2Entrada: TFloatField;
    QrPQPedIts2Prazo: TWideStringField;
    QrPQPedIts2cfin: TFloatField;
    QrPQPedIts2ddEstq: TFloatField;
    QrPQPedIts2Estqkg: TFloatField;
    QrPQPedIts2UCCodigo: TIntegerField;
    QrPQPedIts2UCConta: TIntegerField;
    QrPQPedIts2UCControle: TIntegerField;
    QrPQPedIts2Consumodd: TIntegerField;
    QrPQPedIts2Consumokg: TFloatField;
    QrPQPedIts2NOMEINSUMO: TWideStringField;
    QrPQECodigo: TIntegerField;
    QrPQEData: TDateField;
    QrPQEIQ: TIntegerField;
    QrPQETransportadora: TIntegerField;
    QrPQENF: TIntegerField;
    QrPQEFrete: TFloatField;
    QrPQEPesoB: TFloatField;
    QrPQEPesoL: TFloatField;
    QrPQEValorNF: TFloatField;
    QrPQERICMS: TFloatField;
    QrPQERICMSF: TFloatField;
    QrPQELk: TIntegerField;
    QrPQEConhecimento: TIntegerField;
    QrPQEPedido: TIntegerField;
    QrPQEDataE: TDateField;
    QrPQEJuros: TFloatField;
    QrPQEICMS: TFloatField;
    QrPQEItsCodigo: TIntegerField;
    QrPQEItsControle: TIntegerField;
    QrPQEItsConta: TIntegerField;
    QrPQEItsInsumo: TIntegerField;
    QrPQEItsVolumes: TIntegerField;
    QrPQEItsPesoVB: TFloatField;
    QrPQEItsPesoVL: TFloatField;
    QrPQEItsValorItem: TFloatField;
    QrPQEItsIPI: TFloatField;
    QrPQEItsRIPI: TFloatField;
    QrPQEItsTotalCusto: TFloatField;
    QrPQEItsTotalPeso: TFloatField;
    QrPQEItsCFin: TFloatField;
    QrPQEItsICMS: TFloatField;
    QrPQEItsRICMS: TFloatField;
    QrPQPedIts2UCData: TDateField;
    QrPQPedIts2UCPreco: TFloatField;
    QrPQPedIts2UCICMS: TFloatField;
    QrPQPedIts2UCIPI: TFloatField;
    QrPQPedIts2UCCusto: TFloatField;
    QrPQPedIts2UCPeso: TFloatField;
    QrPQPedIts2UCValor: TFloatField;
    QrPQPedIts2UCCFin: TFloatField;
    QrPQPedIts2MEDIAKG: TFloatField;
    QrPQPedIts2UCPrazo: TWideStringField;
    QrEmissF: TmySQLQuery;
    QrEmissFData: TDateField;
    QrPQPedIts2KGDD: TFloatField;
    QrFornecedorFAX_TXT: TWideStringField;
    QrFornecedorCEL_TXT: TWideStringField;
    QrFornecedorCPF_TXT: TWideStringField;
    QrFornecedorTEL_TXT: TWideStringField;
    QrTransportadorTEL_TXT: TWideStringField;
    QrTransportadorCEL_TXT: TWideStringField;
    QrTransportadorCPF_TXT: TWideStringField;
    QrTransportadorFAX_TXT: TWideStringField;
    QrTransportadorCEP_TXT: TWideStringField;
    QrFornecedorCEP_TXT: TWideStringField;
    QrPQPedIts2UCDATA2: TWideStringField;
    QrPQPedIts2UCFRETE: TFloatField;
    QrPQPedIts2UCFRETEKG: TFloatField;
    QrPQPedIts2UCPESOTOTAL: TFloatField;
    QrPQPedIts2UCRICMSFRETE: TFloatField;
    QrPQPedIts2UCPRECOBASE: TFloatField;
    QrPQPedIts2UCPRECOREAL: TFloatField;
    QrFornecedorNOME: TWideStringField;
    QrFornecedorRUA: TWideStringField;
    QrFornecedorCOMPL: TWideStringField;
    QrFornecedorBAIRRO: TWideStringField;
    QrFornecedorCIDADE: TWideStringField;
    QrFornecedorPAIS: TWideStringField;
    QrFornecedorTELEFONE: TWideStringField;
    QrFornecedorFAX: TWideStringField;
    QrFornecedorCelular: TWideStringField;
    QrFornecedorCNPJ: TWideStringField;
    QrFornecedorIE: TWideStringField;
    QrFornecedorContato: TWideStringField;
    QrFornecedorNOMEUF: TWideStringField;
    QrTransportadorNOME: TWideStringField;
    QrTransportadorRUA: TWideStringField;
    QrTransportadorCOMPL: TWideStringField;
    QrTransportadorBAIRRO: TWideStringField;
    QrTransportadorCIDADE: TWideStringField;
    QrTransportadorPAIS: TWideStringField;
    QrTransportadorTELEFONE: TWideStringField;
    QrTransportadorFAX: TWideStringField;
    QrTransportadorCelular: TWideStringField;
    QrTransportadorCNPJ: TWideStringField;
    QrTransportadorIE: TWideStringField;
    QrTransportadorContato: TWideStringField;
    QrTransportadorNOMEUF: TWideStringField;
    QrFornecedorEEMail: TWideStringField;
    QrFornecedorPEmail: TWideStringField;
    QrFornecedorNUMEROTXT: TWideStringField;
    QrFornecedorENDERECO: TWideStringField;
    QrTransportadorNUMEROTXT: TWideStringField;
    QrTransportadorENDERECO: TWideStringField;
    QrPQPedItsReais: TFloatField;
    QrPQPedItsDolar: TFloatField;
    QrPQPedItsEuro: TFloatField;
    QrPQPedItsMoeda: TIntegerField;
    Label11: TLabel;
    DBEdit12: TDBEdit;
    QrPQPedCliInt: TIntegerField;
    QrPQPedNOMECLIINT: TWideStringField;
    QrPQPedItsIdx: TFloatField;
    QrPQPedItsNOMEMOEDA: TWideStringField;
    FrxPQPedExp: TfrxReport;
    frxPQPed: TfrxReport;
    frxPQPed2: TfrxReport;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBControle: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtEMail: TBitBtn;
    BtRaio: TBitBtn;
    GBConfirma: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtIncluiIts: TBitBtn;
    BtAlteraIts: TBitBtn;
    QrPQECodCliInt: TIntegerField;
    QrFornecedorNUMERO: TFloatField;
    QrFornecedorCEP: TFloatField;
    QrFornecedorUF: TFloatField;
    QrTransportadorNUMERO: TFloatField;
    QrTransportadorCEP: TFloatField;
    QrTransportadorUF: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtEMailClick(Sender: TObject);
    procedure QrPQPedAfterOpen(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SbNumeroClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtIncluiItsClick(Sender: TObject);
    procedure BtAlteraItsClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrPQPedItsCalcFields(DataSet: TDataSet);
    procedure BtRaioClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdEmailClick(Sender: TObject);
    procedure Standard1Click(Sender: TObject);
    procedure Completo1Click(Sender: TObject);
    procedure QrPQPedIts2CalcFields(DataSet: TDataSet);
    procedure QrFornecedorCalcFields(DataSet: TDataSet);
    procedure QrTransportadorCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FrxPQPedExpGetValue(const VarName: String;
      var Value: Variant);
    procedure SbNovoClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
  private
    { Private declarations }
    procedure CriaOForm;
//    procedure LocalizaNome(Nome: String);
    procedure SubQuery1Reopen(Incremento: Integer);
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
    procedure IncluiSubRegistro;
    procedure ExcluiSubRegistro;
    procedure AlteraSubRegistro;

    procedure TravaOForm;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);

    //procs do form
    procedure SomaItens;
    procedure ReopenEmissF();
    //procedure ImprimePedido;
  public
    { Public declarations }
  end;

var
  FmPQPed: TFmPQPed;

implementation

uses UnMyObjects, Principal, Module, PQPedNew, PQPedEdit, PQEnvia, MyDBCheck,
DmkDAC_PF, ModuleGeral;

{$R *.DFM}

const
  FCaminhoEMail = 'C:\Dermatek\OCs\OC';

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPQPed.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPQPed.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPQPedCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPQPed.DefParams;
begin
  VAR_GOTOTABELA := 'PQPed';
  VAR_GOTOMySQLTABLE := QrPQPed;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT pp.*,');
  VAR_SQLx.Add('CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial');
  VAR_SQLx.Add('ELSE ci.Nome END NOMECLIINT,');
  VAR_SQLx.Add('CASE WHEN tr.Tipo=0 THEN tr.RazaoSocial');
  VAR_SQLx.Add('ELSE tr.Nome END NOMETRANSPORTADORA,');
  VAR_SQLx.Add('CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial');
  VAR_SQLx.Add('ELSE fo.Nome END NOMEFORNECEDOR,');
  VAR_SQLx.Add('se.Nome NOMESETOR, fo.EEMail EMail, fo.PEMail EMailContato');
  VAR_SQLx.Add('FROM pqped pp');
  VAR_SQLx.Add('LEFT JOIN entidades ci ON ci.Codigo=pp.CliInt');
  VAR_SQLx.Add('LEFT JOIN entidades tr ON tr.Codigo=pp.Transporte');
  VAR_SQLx.Add('LEFT JOIN entidades fo ON fo.Codigo=pp.Fornece');
  VAR_SQLx.Add('LEFT JOIN listasetores se ON se.Codigo=pp.Setor');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('WHERE pp.Codigo=:P0');
  //
  VAR_SQLa.Add('');//AND pp.Nome Like :P0');
  //
end;


procedure TFmPQPed.SomaItens;
begin
  QrSoma.Close;
  QrSoma.Params[0].AsInteger := QrPQPedCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSoma, Dmod.MyDB);
  QrSomaUpd.Params[0].AsFloat := QrSomaPesoL.Value;
  QrSomaUpd.Params[1].AsFloat := QrSomaPesoB.Value;
  QrSomaUpd.Params[2].AsFloat := QrSomaValor.Value;
  QrSomaUpd.Params[3].AsFloat := QrSomaEntrada.Value;
  QrSomaUpd.Params[4].AsInteger := QrPQPedCodigo.Value;
  QrSomaUpd.ExecSQL;
  LocCod(QrPQPedCodigo.Value, QrPQPedCodigo.Value);
end;

procedure TFmPQPed.ExcluiSubRegistro;
var
  Codigo, Conta, Controle : Integer;
begin
  Codigo := QrPQPedItsCodigo.Value;
  Conta := QrPQPedItsConta.Value;
  Controle := QrPQPedItsControle.Value;
  if Application.MessageBox(PChar(FIN_MSG_ASKESCLUI), 'Pergunta', MB_YESNOCANCEL) = ID_YES then
  begin
    QrUpd.Close;
    QrUpd.SQL.Clear;
    QrUpd.SQL.Add('DELETE FROM pqpedits WHERE Codigo=:P0');
    QrUpd.SQL.Add('AND Conta=:P1 AND Controle=:P2');
    QrUpd.Params[0].AsInteger := Codigo;
    QrUpd.Params[1].AsInteger := Conta;
    QrUpd.Params[2].AsInteger := Controle;
    QrUpd.ExecSQL;

    SubQuery1Reopen(0);

    SomaItens;
  end;
end;

procedure TFmPQPed.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);

  with FmPQPed do
    Width := 800;
    if Screen.Width = 640 then // 480
      begin
        ShowMessage('Para melhor resolu��o desta janela,' + sLineBreak +
                    'modifique a configura��o de v�deo' + sLineBreak +
                    'para 600 x 800 pixels.');
        Top := 0;
        Left := 0;
        Height := 450;
      end
    else
    if Screen.Width = 800 then  // 600
      begin
        Top := 40;
        Left := 0;
        Height := 530;
      end
    else                       //  1024 * 768
      begin
        Top := 120;
        Left := 112;
        Height := 600;
    end;
end;

procedure TFmPQPed.TravaOForm;
begin
  ImgTipo.SQLType := stLok;
  GBConfirma.Visible:=False;
  GBControle.Visible:=True;
  if QrPQPedCodigo.Value > 0 then
    BtAltera.Enabled := True;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  UMyMod.UpdUnlockY(QrPQPedCodigo.Value, Dmod.MyDB, 'PQPed', 'Codigo');
end;

procedure TFmPQPed.AlteraSubRegistro;
begin
  if GOTOy.Registros(QrPQPedIts) = 0 then Exit;
  if DBCheck.CriaFm(TFmPQPedEdit,FmPQPedEdit, afmoNegarComAviso) then
  begin
    FmPQPedEdit.EdCodigo.Enabled := False;
    FmPQPedEdit.ImgTipo.SQLType := stUpd;
    FmPQPedEdit.EdCodigo.ValueVariant := QrPQPedItsConta.Value;

    FmPQPedEdit.EdProduto.Text := IntToStr(QrPQPedItsInsumo.Value);
    FmPQPedEdit.CBProduto.KeyValue := QrPQPedItsInsumo.Value;
    FmPQPedEdit.EdVolumes.Text := FloatToStr(QrPQPedItsVolumes.Value);
    FmPQPedEdit.EdKgBruto.Text :=  Geral.FFT(QrPQPedItsPesoVB.Value, 3, siPositivo);
    FmPQPedEdit.EdkgLiq.Text :=  Geral.FFT(QrPQPedItsPesoVL.Value, 3, siPositivo);
    FmPQPedEdit.EdTotalItem.Text :=  Geral.FFT(QrPQPedItsValorItem.Value, 6, siPositivo);
    FmPQPedEdit.EdIPI.Text :=  Geral.FFT(QrPQPedItsIPI.Value, 2, siPositivo);
    FmPQPedEdit.EdICMS.Text :=  Geral.FFT(QrPQPedItsICMS.Value, 2, siPositivo);
    FmPQPedEdit.EdCFin.Text :=  Geral.FFT(QrPQPedItsCFin.Value, 4, siPositivo);
    FmPQPedEdit.EdPrazo.Text := QrPQPedItsPrazo.Value;
    FmPQPedEdit.EdConsumodd.Text := IntToStr(QrPQPedItsConsumodd.Value);
    // Deixar por �ltimo
    FmPQPedEdit.EdDolar.Text := Geral.FFT(QrPQPedItsDolar.Value, 4, siPositivo);
    FmPQPedEdit.EdEuro .Text := Geral.FFT(QrPQPedItsEuro. Value, 4, siPositivo);
    FmPQPedEdit.EdIdx  .Text := Geral.FFT(QrPQPedItsIdx  .Value, 4, siPositivo);
    FmPQPedEdit.EdPreco.Text := Geral.FFT(QrPQPedItsPreco.Value, 4, siPositivo);
    FmPQPedEdit.RGMoeda.ItemIndex := QrPQPedItsMoeda.Value;
    // Calcula autom�tico
    //FmPQPedEdit.EdReais.Text := Geral.FFT(QrPQPedItsReais.Value, 4, siPositivo);
    FmPQPedEdit.ShowModal;
    FmPQPedEdit.Destroy;
    SubQuery1Reopen(0);
  end;
end;

//procedure TFmPQPed.AlteraRegistro(EnableBtDesiste, VerificaLk : Boolean);
procedure TFmPQPed.AlteraRegistro;
var
  PQPed : Integer;
begin
  PQPed := QrPQPedCodigo.Value;
  if QrPQPedCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SellockY(PQPed, Dmod.MyDB, 'PQPed', 'Codigo') then
  begin
    try
      UMyMod.UpdlockY(PQPed, Dmod.MyDB, 'PQPed', 'Codigo');
      GBControle.Visible:=False;
      GBConfirma.Visible:=True;
      ImgTipo.SQLType := stUpd;
      GOTOy.BotoesSb(ImgTipo.SQLType);
      Application.CreateForm(TFmPQPedNew,FmPQPedNew);
      FmPQPedNew.ImgTipo.SQLType := stUpd;
      FmPQPedNew.EdCodigo.Text := IntToStr(PQPed);
      FmPQPedNew.CBCliInt.KeyValue := QrPQPedCliInt.Value;
      FmPQPedNew.CBFornecedor.KeyValue := QrPQPedFornece.Value;
      FmPQPedNew.CBTransportador.KeyValue := QrPQPedTransporte.Value;
      FmPQPedNew.CBSetor.KeyValue := QrPQPedSetor.Value;
      FmPQPedNew.EdCliInt.Text := IntToStr(QrPQPedCliInt.Value);
      FmPQPedNew.EdFornecedor.Text := IntToStr(QrPQPedFornece.Value);
      FmPQPedNew.EdTransportador.Text := IntToStr(QrPQPedTransporte.Value);
      FmPQPedNew.EdSetor.Text := IntToStr(QrPQPedSetor.Value);
      FmPQPedNew.EdLiquido.Text := Geral.TFT(FloatToStr(QrPQPedPesoL.Value), 3, siPositivo);
      FmPQPedNew.EdBruto.Text := Geral.TFT(FloatToStr(QrPQPedPesoB.Value), 3, siPositivo);
      FmPQPedNew.EdEntrada.Text := Geral.TFT(FloatToStr(QrPQPedEntrada.Value), 3, siPositivo);
      FmPQPedNew.EdValor.Text := Geral.TFT(FloatToStr(QrPQPedValor.Value), 2, siPositivo);
      FmPQPedNew.EdContato.Text := QrPQPedContato.Value;
      FmPQPedNew.TPEntrega.Date := QrPQPedEntrega.Value;
      FmPQPedNew.TPSolicitado.Date := QrPQPedData.Value;
      FmPQPedNew.RGSit.ItemIndex := QrPQPedSit.Value;
      GOTOy.BotoesSb(ImgTipo.SQLType);
    finally
    Screen.Cursor := Cursor;
    end;
    FmPQPedNew.ShowModal;
    FmPQPedNew.Destroy;
    LocCod(PQPed, PQPed);
  end;
end;

procedure TFmPQPed.IncluiRegistro;
var
  PQPed : Integer;
begin
  if DBCheck.CriaFm(TFmPQPedNew,FmPQPedNew, afmoNegarComAviso) then
  begin
    PQPed := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'PQPed', 'PQPed', 'Codigo');
    FmPQPedNew.ImgTipo.SQLType := stIns;
    FmPQPedNew.EdCodigo.Text := IntToStr(PQPed);
    FmPQPedNew.TPSolicitado.Date := Now;
    FmPQPedNew.TPEntrega.Date := Now+7;
    GBControle.Visible:=False;
    GBConfirma.Visible:=True;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    FmPQPedNew.ShowModal;
    FmPQPedNew.Destroy;
    LocCod(PQPed, PQPed);
  end;
end;

procedure TFmPQPed.QueryPrincipalAfterOpen;
begin
  SubQuery1Reopen(0);
end;

procedure TFmPQPed.ReopenEmissF();
var
  TabLctA: String;
begin
  if QrPQECodCliInt.Value <> 0 then
    TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrPQECodCliInt.Value)
  else
    TabLctA := sTabLctErr;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissF, Dmod.MyDB, [
  'SELECT Data ',
  'FROM ' + TabLctA,
  'WHERE FatID=' + Geral.FF0(VAR_FATID_1001),
  'AND FatNum=' + Geral.FF0(QrPQECodigo.Value),
  '']);
end;

procedure TFmPQPed.DefineONomeDoForm;
begin
end;

procedure TFmPQPed.SubQuery1Reopen(Incremento: Integer);
var
  Controle, Conta: Integer;
begin
  Controle := 0;
  Conta := Incremento;
  if (QrPQPedIts.State in [dsBrowse]) and (GOTOy.Registros(QrPQPedIts) > 0) then
  begin
    Controle := QrPQPedItsControle.Value;
    Conta := Conta + QrPQPedItsConta.Value;
  end;
  QrPQPedIts.Close;
  QrPQPedIts.Params[0].AsInteger := QrPQPedCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPQPedIts, Dmod.MyDB);
  if (Controle <> 0) and (Conta <> 0) then
    QrPQPedIts.Locate('Codigo; Controle; Conta',
    VarArrayOf([QrPQPedCodigo.Value, Controle, Conta]), []);
end;

procedure TFmPQPed.IncluiSubRegistro;
var
  Conta : Integer;
begin
  Conta := 1;
  if DBCheck.CriaFm(TFmPQPedEdit,FmPQPedEdit, afmoNegarComAviso) then
  begin
    FmPQPedEdit.ImgTipo.SQLType := stIns;
    if FmPQPed.QrPQPedItsConta.Value > 0 then
       Conta := FmPQPed.QrPQPedItsConta.Value+1;
    FmPQPedEdit.EdCodigo.ValMax := FormatFloat('0', QrPQPedIts.RecordCount + 1);
    FmPQPedEdit.EdCodigo.ValueVariant := Conta;
    FmPQPedEdit.EdConsumodd.Enabled := True;
    FmPQPedEdit.EdDolar.ValueVariant := VAR_CAMBIO_USD;
    FmPQPedEdit.EdEuro.ValueVariant := VAR_CAMBIO_EUR;
    FmPQPedEdit.EdIdx.ValueVariant := VAR_CAMBIO_IDX;
    FmPQPedEdit.ShowModal;
    FmPQPedEdit.Destroy;
    SubQuery1Reopen(1);
  end;  
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPQPed.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPQPed.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPQPed.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPQPed.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPQPed.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQPed.BtConfirmaClick(Sender: TObject);
begin
  TravaOForm;
end;

procedure TFmPQPed.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CriaOForm;
end;

procedure TFmPQPed.BtEMailClick(Sender: TObject);
begin
  ForceDirectories(FCaminhoEmail);
  VAR_ARQUIVO := FCaminhoEmail+IntToStr(QrPQPedCodigo.Value)+'.doc';
  QrFornecedor.Close;
  QrFornecedor.Params[0].AsInteger := QrPQPedFornece.Value;
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  QrTransportador.Close;
  QrTransportador.Params[0].AsInteger := QrPQPedTransporte.Value;
  UnDmkDAC_PF.AbreQuery(QrTransportador, Dmod.MyDB);
  QrConsts.Close;
  UnDmkDAC_PF.AbreQuery(QrConsts, Dmod.MyDB);
  DeleteFile(VAR_ARQUIVO);
  MyObjects.frxMostra(FrxPQPedExp, '');
  // Parei aqui - falta fazer
  //FrPQPedExp.ExportTo(frRTFExport1,VAR_ARQUIVO);

  Application.CreateForm(TFmPQEnvia, FmPQEnvia);
  with FmPQEnvia do
  begin
    EdSMTP.Text    := Dmod.QrControle.FieldByName('ServSMTP').AsString;
    EdConta.Text   := Dmod.QrControle.FieldByName('NomeMailOC').AsString;
    EdEMail.Text   := Dmod.QrControle.FieldByName('MailOC').AsString;
    EdDono.Text    := Dmod.QrControle.FieldByName('DonoMailOC').AsString;
    EdAssunto.Text := CO_PEDIDO+' '+Dmod.QrMasterEm.Value+' #'+
                      IntToStr(QrPQPedCodigo.Value);
    LBAnexos.Items.Add(VAR_ARQUIVO);
    MePara.Lines.Clear;
    MePara.Lines.Add(QrFornecedorEEMail.Value);
    MeCC.Lines.Clear;
    MeCC.Lines.Add(QrFornecedorPEMail.Value);
    MeCega.Lines.Clear;
    MeCega.Lines.Add(Dmod.QrControle.FieldByName('MailCCCega').AsString);
    EdDialUp.Text := Dmod.QrControle.FieldByName('ConexaoDialUp').AsString;
    MeCorpo.Lines.Add(Dmod.QrControle.FieldByName('CorpoMailOC').AsString);
    MeCorpo.Lines.Add(Dmod.QrControle.FieldByName('DonoMailOC').AsString);
    MeCorpo.Lines.Add(Dmod.QrMasterEm.Value);
    MeCorpo.Lines.Add(Dmod.QrControle.FieldByName('MailOC').AsString);
    if (EdSMTP.Text = CO_VAZIO)
    or (EdConta.Text = CO_VAZIO)
    or (EdEMail.Text = CO_VAZIO)
    or (EdDono.Text = CO_VAZIO) then
      Application.MessageBox('Utilize o Blue Derm Admin para padr�es pendentes.',
        'Aviso', MB_OK+MB_ICONWARNING);
    ShowModal;
    Destroy;
  end;
  LocCod(QrPQPedCodigo.Value, QrPQPedCodigo.Value);
end;

procedure TFmPQPed.QrPQPedAfterOpen(DataSet: TDataSet);
begin
  GOTOy.BtEnabled(QrPQPedCodigo.Value, False);
  QueryPrincipalAfterOpen;
end;

procedure TFmPQPed.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQPed.SbNomeClick(Sender: TObject);
begin
//
end;

procedure TFmPQPed.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmPQPed.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPQPedCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPQPed.SbQueryClick(Sender: TObject);
begin
//
end;

procedure TFmPQPed.BtIncluiClick(Sender: TObject);
begin
 IncluiRegistro;
end;

procedure TFmPQPed.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmPQPed.BtIncluiItsClick(Sender: TObject);
begin
  IncluiSubRegistro;
  SomaItens;
end;

procedure TFmPQPed.BtAlteraItsClick(Sender: TObject);
begin
  AlteraSubRegistro;
  SomaItens;
end;

procedure TFmPQPed.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  Parei Aqui
  if ImgTipo.SQLType <> stLok then
  begin
    if (key=VK_DELETE) and (Shift = [ssCtrl]) then ExcluiSubRegistro;
  end;
end;

procedure TFmPQPed.QrPQPedItsCalcFields(DataSet: TDataSet);
begin
  QrPQPedItsTOTALBRUTO.Value :=
  QrPQPedItsVolumes.Value *
  QrPQPedItsPesoVB.Value;
  //
  QrPQPedItsNOMEMOEDA.Value := dmkPF.NomeMoeda(QrPQPedItsMoeda.Value);
end;

procedure TFmPQPed.BtRaioClick(Sender: TObject);
begin
  if Application.MessageBox('Esta a��o apenas cofigura o pedido como encerrado. Confirma seu encerramento?',
  PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
  MB_APPLMODAL) = ID_YES then
  begin
    QrEncerra.Params[0].AsInteger := QrPQPedCodigo.Value;
    QrEncerra.ExecSQL;
    LocCod(QrPQPedCodigo.Value, QrPQPedCodigo.Value);
  end;
end;

procedure TFmPQPed.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQPed.EdEmailClick(Sender: TObject);
begin
//  if TCP.LocalIP= '0.0.0.0' then ShowMessage('Voce n�o est� conectado na internet!')
end;

procedure TFmPQPed.Standard1Click(Sender: TObject);
begin
  QrFornecedor.Close;
  QrFornecedor.Params[0].AsInteger := QrPQPedFornece.Value;
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
(*
SELECT
CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial
ELSE fo.Nome END NOME,
CASE WHEN fo.Tipo=0 THEN fo.ERua
ELSE fo.PRua END RUA,
CASE WHEN fo.Tipo=0 THEN fo.ENumero
ELSE fo.PNumero END NUMERO,
CASE WHEN fo.Tipo=0 THEN fo.ECompl
ELSE fo.PCompl END COMPL,
CASE WHEN fo.Tipo=0 THEN fo.EBairro
ELSE fo.PBairro END BAIRRO,
CASE WHEN fo.Tipo=0 THEN fo.ECidade
ELSE fo.PCidade END CIDADE,
CASE WHEN fo.Tipo=0 THEN fo.EPais
ELSE fo.PPais END PAIS,
CASE WHEN fo.Tipo=0 THEN fo.ETe1
ELSE fo.PTe1 END TELEFONE,
CASE WHEN fo.Tipo=0 THEN fo.EFax
ELSE fo.PPais END FAX,
CASE WHEN fo.Tipo=0 THEN fo.ECel
ELSE fo.PCel END Celular,
CASE WHEN fo.Tipo=0 THEN fo.CNPJ
ELSE fo.CPF END CNPJ,
CASE WHEN fo.Tipo=0 THEN fo.IE
ELSE fo.RG END IE,
CASE WHEN fo.Tipo=0 THEN fo.ECEP
ELSE fo.PCEP END CEP,
CASE WHEN fo.Tipo=0 THEN fo.EContato
ELSE fo.PContato END Contato,

CASE WHEN fo.Tipo=0 THEN fo.EUF
ELSE fo.PUF END UF,
CASE WHEN fo.Tipo=0 THEN uf1.Nome
ELSE uf2.Nome END NOMEUF,
EEMail, PEmail

FROM entidades fo, UFs uf1, Ufs uf2
WHERE fo.Codigo=:P0
AND uf1.Codigo=fo.Euf
AND uf2.Codigo=fo.Puf
*)
  QrTransportador.Close;
  QrTransportador.Params[0].AsInteger := QrPQPedTransporte.Value;
  UnDmkDAC_PF.AbreQuery(QrTransportador, Dmod.MyDB);
  QrConsts.Close;
  UnDmkDAC_PF.AbreQuery(QrConsts, Dmod.MyDB);
  MyObjects.frxMostra(FrxPQPed, '');
end;

procedure TFmPQPed.Completo1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQE, Dmod.MyDB, [
  'SELECT pqe.*, ei.CodCliInt ',
  'FROM pqe pqe ',
  'LEFT JOIN enticliint ei ON ei.CodEnti=pqe.CI ',
  'ORDER BY pqe.Codigo ',
  '']);
  QrPQPedIts2.Close;
  QrPQPedIts2.Params[0].AsInteger := QrPQPedCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPQPedIts2, Dmod.MyDB);
  QrFornecedor.Close;
  QrFornecedor.Params[0].AsInteger := QrPQPedFornece.Value;
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  QrTransportador.Close;
  QrTransportador.Params[0].AsInteger := QrPQPedTransporte.Value;
  UnDmkDAC_PF.AbreQuery(QrTransportador, Dmod.MyDB);
  QrConsts.Close;
  UnDmkDAC_PF.AbreQuery(QrConsts, Dmod.MyDB);
  //
  MyObjects.frxMostra(FrxPQPed2, '');
end;

procedure TFmPQPed.QrPQPedIts2CalcFields(DataSet: TDataSet);
begin
  QrPQEIts.Close;
  QrPQEIts.Params[0].AsInteger := QrPQPedIts2UCCodigo.Value;
  QrPQEIts.Params[1].AsInteger := QrPQPedIts2UCConta.Value;
  QrPQEIts.Params[2].AsInteger := QrPQPedIts2UCControle.Value;
  UnDmkDAC_PF.AbreQuery(QrPQEIts, Dmod.MyDB);
  if GOTOy.Registros(QrPQEIts) > 0 then
  begin
    if QrPQEItsInsumo.Value = QrPQPedIts2Insumo.Value then
    begin
      if QrPQEItsTotalPeso.Value > 0 then
        QrPQPedIts2UCCusto.Value :=
        QrPQEItsTotalCusto.Value / QrPQEItsTotalPeso.Value
      else QrPQPedIts2UCCusto.Value := 0;
      if QrPQEItsTotalPeso.Value > 0 then
        QrPQPedIts2UCPreco.Value :=
        QrPQEItsValorItem.Value / QrPQEItsTotalPeso.Value
      else QrPQPedIts2UCPreco.Value := 0;
      QrPQPedIts2UCCFin.Value := QrPQEItsCFin.Value;
      QrPQPedIts2UCICMS.Value := QrPQEItsICMS.Value;
      QrPQPedIts2UCIPI.Value := QrPQEItsIPI.Value;
      QrPQPedIts2UCPeso.Value := QrPQEItsTotalPeso.Value;
      QrPQPedIts2UCValor.Value := QrPQEItsTotalCusto.Value;
      ReopenEmissF();
      case QrEmissF.RecordCount of
        0: QrPQPedIts2UCPrazo.Value := 'N�o informado';
        1:
        begin
          if QrEmissFData.Value = QrPQEData.Value then
          QrPQPedIts2UCPrazo.Value := '� vista'
          else QrPQPedIts2UCPrazo.Value :=
          FormatFloat('0', QrEmissFData.Value-QrPQEData.Value)+' dd';
        end;
        else begin
          QrPQPedIts2UCPrazo.Value := FormatFloat('0', QrEmissFData.Value-QrPQEData.Value);
          QrEmissF.Next;
          while not QrEmissF.Eof do
          begin
            QrPQPedIts2UCPrazo.Value := QrPQPedIts2UCPrazo.Value+
            FormatFloat('0', QrEmissFData.Value-QrPQEData.Value);
            QrEmissF.Next;
          end;
          QrPQPedIts2UCPrazo.Value := QrPQPedIts2UCPrazo.Value+' dd';

        end;
      end;
    end else begin
      QrPQPedIts2UCCusto.Value := 0;
      QrPQPedIts2UCCFin.Value := 0;
      QrPQPedIts2UCICMS.Value := 0;
      QrPQPedIts2UCIPI.Value := 0;
      QrPQPedIts2UCPeso.Value := 0;
      QrPQPedIts2UCPreco.Value := 0;
      QrPQPedIts2UCValor.Value := 0;
      QrPQPedIts2UCPrazo.Value := CO_VAZIO;
    end;
  end else begin
    QrPQPedIts2UCCusto.Value := 0;
    QrPQPedIts2UCCFin.Value := 0;
    QrPQPedIts2UCICMS.Value := 0;
    QrPQPedIts2UCIPI.Value := 0;
    QrPQPedIts2UCPeso.Value := 0;
    QrPQPedIts2UCPreco.Value := 0;
    QrPQPedIts2UCValor.Value := 0;
    QrPQPedIts2UCPrazo.Value := CO_VAZIO;
  end;
  QrPQEIts.Close;
  //
  if QrPQPedIts2Consumodd.Value > 0 then
  QrPQPedIts2MEDIAKG.Value :=
  QrPQPedIts2Consumokg.Value /
  QrPQPedIts2Consumodd.Value
  else QrPQPedIts2MEDIAKG.Value := 0;
  //
  if QrPQPedIts2MEDIAKG.Value > 0 then
  QrPQPedIts2KGDD.Value :=
  QrPQPedIts2Estqkg.Value /
  QrPQPedIts2MEDIAKG.Value
  else QrPQPedIts2MEDIAKG.Value := 0;
  //
  if QrPQPedIts2UCPESOTOTAL.Value > 0 then
  QrPQPedIts2UCFRETEKG.Value :=
  (QrPQPedIts2UCFRETE.Value /
  QrPQPedIts2UCPESOTOTAL.Value) *
  (1-(QrPQPedIts2UCRICMSFRETE.Value/100))
  else QrPQPedIts2UCFRETEKG.Value := 0;
  //
  QrPQPedIts2UCPRECOBASE.Value :=
  QrPQPedIts2UCPreco.Value *
  (1-(QrPQPedIts2UCICMS.Value/100));
  //
  QrPQPedIts2UCPRECOREAL.Value :=
  QrPQPedIts2UCPreco.Value *
  (1+(QrPQPedIts2UCIPI.Value/100));
  //
  if QrPQPedIts2UCData.Value = 0 then QrPQPedIts2UCDATA2.Value := CO_VAZIO
  else QrPQPedIts2UCDATA2.Value :=
    FormatDateTime(VAR_FORMATDATE3, QrPQPedIts2UCData.Value);
end;

procedure TFmPQPed.QrFornecedorCalcFields(DataSet: TDataSet);
begin
  QrFornecedorTEL_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorTelefone.Value);
  QrFornecedorFAX_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorFax.Value);
  QrFornecedorCEL_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorCelular.Value);
  QrFornecedorCPF_TXT.Value := Geral.FormataCNPJ_TT(QrFornecedorCNPJ.Value);
  QrFornecedorCEP_TXT.Value := Geral.FormataCEP_TT(FloatToStr(QrFornecedorCEP.Value));
  if QrFornecedorNUMERO.Value = 0 then
     QrFornecedorNUMEROTXT.Value := 'S/N' else
     QrFornecedorNUMEROTXT.Value :=
     FloatToStr(QrFornecedorNUMERO.Value);
  QrFornecedorENDERECO.Value :=
  QrFornecedorRUA.Value + ', '+
  QrFornecedorNUMEROTXT.Value + '  '+
  QrFornecedorCOMPL.Value;
end;

procedure TFmPQPed.QrTransportadorCalcFields(DataSet: TDataSet);
begin
  QrTransportadorTEL_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorTelefone.Value);
  QrTransportadorFAX_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorFax.Value);
  QrTransportadorCEL_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorCelular.Value);
  QrTransportadorCPF_TXT.Value := Geral.FormataCNPJ_TT(QrTransportadorCNPJ.Value);
  QrTransportadorCEP_TXT.Value := Geral.FormataCEP_TT(FloatToStr(QrTransportadorCEP.Value));
  if QrTransportadorNUMERO.Value = 0 then
     QrTransportadorNUMEROTXT.Value := 'S/N' else
     QrTransportadorNUMEROTXT.Value :=
     FloatToStr(QrTransportadorNUMERO.Value);
  QrTransportadorENDERECO.Value :=
  QrTransportadorRUA.Value + ', '+
  QrTransportadorNUMEROTXT.Value + '  '+
  QrTransportadorCOMPL.Value;
end;

procedure TFmPQPed.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmPQPed.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQPed.FrxPQPedExpGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'Vazio' then Value := CO_VAZIO;

  if VarName = 'Setor: ' then Value := CO_SETOR2P;
  if VarName = 'OrdemDeCompraN' then Value := CO_ORDEMCOMPRANUM;
  if VarName = 'Fornecedor: ' then Value := CO_FORNECEDOR2P;
  if VarName = 'Transportador: ' then Value := CO_TRANSPORTADOR2P;
  if VarName = 'Endereco: ' then Value := CO_ENDERECO2P;
  if VarName = 'Bairro: ' then Value := CO_BAIRRO2P;
  if VarName = 'Cidade: ' then Value := CO_CIDADE2P;
  if VarName = 'UF: ' then Value := CO_UF2P;
  if VarName = 'CEP: ' then Value := CO_CEP2P;
  if VarName = 'Pais: ' then Value := CO_PAIS2P;
  if VarName = 'CNPJ: ' then Value := CO_CNPJ2P;
  if VarName = 'IE: ' then Value := CO_IE2P;
  if VarName = 'Telefone: ' then Value := CO_TELEFONE2P;
  if VarName = 'Fax: ' then Value := CO_FAX2P;
  if VarName = 'Contato: ' then Value := CO_CONTATO2P;
  if VarName = 'TelefoneContato: ' then Value := CO_TELEFONE2P;
  if VarName = 'Data de entrega: ' then Value := CO_DATAENTREGA2P;

  if VarName = 'EnderecoT: ' then Value := CO_ENDERECO2P;
  if VarName = 'BairroT: ' then Value := CO_BAIRRO2P;
  if VarName = 'CidadeT: ' then Value := CO_CIDADE2P;
  if VarName = 'UFT: ' then Value := CO_UF2P;
  if VarName = 'CEPT: ' then Value := CO_CEP2P;
  if VarName = 'PaisT: ' then Value := CO_PAIS2P;
  if VarName = 'CNPJT: ' then Value := CO_CNPJ2P;
  if VarName = 'IET: ' then Value := CO_IE2P;
  if VarName = 'TelefoneT: ' then Value := CO_TELEFONE2P;
  if VarName = 'FaxT: ' then Value := CO_FAX2P;
  if VarName = 'ContatoT: ' then Value := CO_CONTATO2P;
  if VarName = 'TelefoneContatoT: ' then Value := CO_TELEFONE2P;

  if VarName = 'Nomedoproduto' then Value := CO_NOMEPRODUTO;
  if VarName = 'Volumes' then Value := CO_VOLUMES;
  if VarName = 'Embalagem' then Value := CO_EMBALAGEM;
  if VarName = 'PesoL' then Value := CO_PESOL;
  if VarName = 'PesoB' then Value := CO_PESOB;
  if VarName = 'Base' then Value := '$ Base';
  if VarName = 'Preco' then Value := CO_PRECO;
  if VarName = 'ICMS' then Value := 'ICM';//CO_ICMS+' e '+CO_IPI;
  if VarName = 'IPI' then Value := 'IPI';//CO_ICMS+' e '+CO_IPI;
  if VarName = 'Valor' then Value := CO_VALOR;
  if VarName = 'Custo' then Value := CO_CUSTO;
  if VarName = 'Prazo' then Value := CO_PRAZO;
  if VarName = 'CFin' then Value := CO_CUSTOFINANCEIRO;
  if VarName = 'DUC' then Value := 'Data';
  if VarName = 'Fornecedor' then Value := QrPQPedNOMEFORNECEDOR.Value;

  if VarName = 'RespSetor' then
  begin
    case QrPQPedSetor.Value of
      -5 : Value := QrConstsCaldeiraETE.Value;
      -4 : Value := QrConstsAcabamento.Value;
      -3 : Value := QrConstsCaleiro.Value;
      -2 : Value := QrConstsCurtimento.Value;
      -1 : Value := QrConstsRecurtimento.Value;
      else Value := CO_VAZIO;
    end;
  end;

  if VarName = 'DDESTQ' then Value := 'Dias';
  if VarName = 'ESTQKG' then Value := 'kg';
end;

end.



