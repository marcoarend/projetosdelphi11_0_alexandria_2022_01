unit PQD;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, DBGrids, Menus, frxClass, frxDBSet, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, dmkEditDateTimePicker, UnDmkProcFunc,
  dmkLabel, dmkImage, UnDmkEnums, UnProjGroup_Consts, UnEmpresas;

type
  TFmPQD = class(TForm)
    PnDados: TPanel;
    DsPQD: TDataSource;
    QrPQD: TmySQLQuery;
    PnEdita: TPanel;
    PnEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    PnData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label2: TLabel;
    TPDataB: TdmkEditDateTimePicker;
    QrPQDIts: TmySQLQuery;
    QrPQDItsDataX: TDateField;
    QrPQDItsTipo: TSmallintField;
    QrPQDItsCliOrig: TIntegerField;
    QrPQDItsCliDest: TIntegerField;
    QrPQDItsInsumo: TIntegerField;
    QrPQDItsPeso: TFloatField;
    QrPQDItsValor: TFloatField;
    QrPQDItsOrigemCodi: TIntegerField;
    QrPQDItsOrigemCtrl: TIntegerField;
    QrPQDItsNOMEPQ: TWideStringField;
    QrPQDItsGrupoQuimico: TIntegerField;
    QrPQDItsNOMEGRUPO: TWideStringField;
    DsPQDIts: TDataSource;
    DBGIts: TDBGrid;
    QrPQ: TmySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsPQ: TDataSource;
    DsCI: TDataSource;
    QrCI: TmySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    QrSaldo: TmySQLQuery;
    QrSaldoPeso: TFloatField;
    QrSaldoValor: TFloatField;
    QrSaldoCUSTO: TFloatField;
    QrSaldoCI: TIntegerField;
    QrSaldoPQ: TIntegerField;
    DsSaldo: TDataSource;
    PnInsumos: TPanel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    EdPesoAdd: TdmkEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    EdSaldoFut: TdmkEdit;
    PMInclui: TPopupMenu;
    IncluiNovaBaixa1: TMenuItem;
    IncluiInsumobaixaatual1: TMenuItem;
    QrSumPQ: TmySQLQuery;
    QrSumPQCUSTO: TFloatField;
    PMExclui: TPopupMenu;
    ExcluiItemdabaixa1: TMenuItem;
    ExcluiBaixa1: TMenuItem;
    PMAltera: TPopupMenu;
    AlteraBaixa1: TMenuItem;
    PMImprime: TPopupMenu;
    Estabaixa1: TMenuItem;
    Outros1: TMenuItem;
    QrPeriodo: TmySQLQuery;
    EdSetor: TdmkEditCB;
    Label3: TLabel;
    CBSetor: TdmkDBLookupComboBox;
    QrSE: TmySQLQuery;
    QrSECodigo: TIntegerField;
    QrSENome: TWideStringField;
    DsSE: TDataSource;
    QrPQDCodigo: TIntegerField;
    QrPQDSetor: TIntegerField;
    QrPQDCustoInsumo: TFloatField;
    QrPQDCustoTotal: TFloatField;
    QrPQDDataB: TDateField;
    QrPQDLk: TIntegerField;
    QrPQDDataCad: TDateField;
    QrPQDDataAlt: TDateField;
    QrPQDUserCad: TIntegerField;
    QrPQDUserAlt: TIntegerField;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    QrPeriodoCodigo: TIntegerField;
    QrPeriodoSetor: TIntegerField;
    QrPeriodoCustoInsumo: TFloatField;
    QrPeriodoCustoTotal: TFloatField;
    QrPeriodoDataB: TDateField;
    QrPeriodoLk: TIntegerField;
    QrPeriodoDataCad: TDateField;
    QrPeriodoDataAlt: TDateField;
    QrPeriodoUserCad: TIntegerField;
    QrPeriodoUserAlt: TIntegerField;
    QrPQDNOMESETOR: TWideStringField;
    QrPeriodoNOMESETOR: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBControle: TGroupBox;
    PnNaviDwn: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBConfirm2: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma2: TBitBtn;
    BtDesiste2: TBitBtn;
    GBConfirma: TGroupBox;
    Panel1: TPanel;
    Panel7: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Alteraitematual1: TMenuItem;
    QrEmitGru: TmySQLQuery;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    DsEmitGru: TDataSource;
    Label14: TLabel;
    EdEmitGru: TdmkEditCB;
    CBEmitGru: TdmkDBLookupComboBox;
    QrPQDNO_EMITGRU: TWideStringField;
    QrPQDAlterWeb: TSmallintField;
    QrPQDAtivo: TSmallintField;
    QrPQDEmitGru: TIntegerField;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    QrPQDNome: TWideStringField;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    EdNome: TdmkEdit;
    Label11: TLabel;
    frxQUI_RECEI_011_A: TfrxReport;
    frxDsPQDIts: TfrxDBDataset;
    frxDsPQD: TfrxDBDataset;
    SpeedButton5: TSpeedButton;
    QrGraCorCad: TmySQLQuery;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    DsGraCorCad: TDataSource;
    QrPQDGraCorCad: TIntegerField;
    QrPQDNoGraCorCad: TWideStringField;
    DBEdit10: TDBEdit;
    Label16: TLabel;
    QrRebaixe: TmySQLQuery;
    QrRebaixeCodigo: TIntegerField;
    QrRebaixeLinhas: TWideStringField;
    QrRebaixeEMCM: TFloatField;
    DsRebaixe: TDataSource;
    Label24: TLabel;
    EdSemiCodEspReb: TdmkEditCB;
    CBSemiCodEspReb: TdmkDBLookupComboBox;
    QrPQDSemiCodEspReb: TIntegerField;
    QrPQDRebLin: TWideStringField;
    Label21: TLabel;
    DBEdit6: TDBEdit;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    Label22: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrPQDItsEmpresa: TIntegerField;
    EdNFDevol: TdmkEdit;
    Label23: TLabel;
    QrPQDNFDevol: TIntegerField;
    EdGraCorCad: TdmkEditCB;
    Label15: TLabel;
    CBGraCorCad: TdmkDBLookupComboBox;
    DBEdit11: TDBEdit;
    Label25: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPQDAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrPQDAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPQDBeforeOpen(DataSet: TDataSet);
    procedure QrPQDAfterClose(DataSet: TDataSet);
    procedure EdPQChange(Sender: TObject);
    procedure EdCIChange(Sender: TObject);
    procedure EdPesoAddExit(Sender: TObject);
    procedure BtDesiste2Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure IncluiNovaBaixa1Click(Sender: TObject);
    procedure IncluiInsumobaixaatual1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure AlteraBaixa1Click(Sender: TObject);
    procedure Outros1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frBaixaAtualUserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    procedure ExcluiBaixa1Click(Sender: TObject);
    procedure ExcluiItemdabaixa1Click(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure Alteraitematual1Click(Sender: TObject);
    procedure PMAlteraPopup(Sender: TObject);
    procedure Estabaixa1Click(Sender: TObject);
    procedure frxQUI_RECEI_011_AGetValue(const VarName: string;
      var Value: Variant);
    procedure SpeedButton5Click(Sender: TObject);
    procedure CkDtCorrApoClick(Sender: TObject);
  private
    FPQDIts: Integer;
    FEmpresa: Integer;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenPQDIts;
    procedure CalculaSaldoFuturo();
    procedure AtualizaCusto;
  public
    { Public declarations }
    procedure ExcluiItemSelecionado(AtzCusto: Boolean);
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmPQD: TFmPQD;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, PQx, Principal, ModuleGeral, PQDEdit, MyDBCheck,
  DmkDAC_PF, UnPQ_PF, UnApp_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPQD.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPQD.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPQDCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPQD.DefParams;
begin
  VAR_GOTOTABELA := 'PQD';
  VAR_GOTOMYSQLTABLE := QrPQD;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := 'Empresa=' + Geral.FF0(FEmpresa);

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('lse.Nome NOMESETOR, reb.Linhas RebLin, ');
  VAR_SQLx.Add('emg.Nome NO_EMITGRU, gcc.Nome NoGraCorCad, pqd.*');
  VAR_SQLx.Add('FROM pqd pqd');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=pqd.Empresa');
  VAR_SQLx.Add('LEFT JOIN listasetores lse ON lse.Codigo=pqd.Setor');
  VAR_SQLx.Add('LEFT JOIN emitgru emg ON emg.Codigo=pqd.EmitGru');
  VAR_SQLx.Add('LEFT JOIN gracorcad gcc ON gcc.Codigo=pqd.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN espessuras reb ON reb.Codigo=pqd.SemiCodEspReb');
  VAR_SQLx.Add('WHERE pqd.Codigo > 0');
  VAR_SQLx.Add('AND pqd.Empresa=' + Geral.FF0(FEmpresa));
  //
  VAR_SQL1.Add('AND pqd.Codigo=:P0');
  //
  VAR_SQLa.Add('AND lse.Nome Like :P0');
  //
end;

procedure TFmPQD.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      GBControle.Visible := True;
      PnDados.Visible    := True;
      PnEdita.Visible    := False;
      PnInsumos.Visible  := False;
      GBConfirm2.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      GBControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant  := 0;
        EdNome.Text            := '';
        TPDataB.Date           := Date;
        EdSetor.ValueVariant   := 0;
        CBSetor.KeyValue       := Null;
        EdEmitGru.ValueVariant := 0;
        CBEmitGru.KeyValue     := Null;
        EdGraCorCad.ValueVariant := 0;
        CBGraCorCad.KeyValue     := Null;
        EdSemiCodEspReb.ValueVariant := 0;
        CBSemiCodEspReb.KeyValue     := Null;
        EdNFDevol.ValueVariant       := 0;
      end else begin
        EdCodigo.ValueVariant  := QrPQDCodigo.Value;
        EdNome.ValueVariant    := QrPQDNome.Value;
        TPDataB.Date           := QrPQDDataB.Value;
        EdSetor.ValueVariant   := QrPQDSetor.Value;
        CBSetor.KeyValue       := QrPQDSetor.Value;
        EdEmitGru.ValueVariant := QrPQDEmitGru.Value;
        CBEmitGru.KeyValue     := QrPQDEmitGru.Value;
        EdGraCorCad.ValueVariant := QrPQDGraCorCad.Value;
        CBGraCorCad.KeyValue     := QrPQDGraCorCad.Value;
        EdSemiCodEspReb.ValueVariant := QrPQDSemiCodEspReb.Value;
        CBSemiCodEspReb.KeyValue     := QrPQDSemiCodEspReb.Value;
        EdNFDevol.ValueVariant       := QrPQDNFDevol.Value;
      end;
      TPDataB.SetFocus;
    end;
    2:
    begin
      // 2023-09-09
      if UnPQx.ImpedePeloBalanco(QrPQDDataB.Value) then Exit;
      // 2023-09-09
      PnInsumos.Visible  := True;
      GBConfirm2.Visible := True;
      GBControle.Visible := False;
      //
      EdCI.ValueVariant  := Dmod.QrMasterDono.Value;
      CBCI.KeyValue      := Dmod.QrMasterDono.Value;
      //
      EdCI.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmPQD.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPQD.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPQD.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPQD.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPQD.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPQD.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPQD.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPQD.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  App_Jan.MostraFormEmitGru(EdEmitGru.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEmitGru, CBEmitGru, QrEmitGru, VAR_CADASTRO);
    //
    EdEmitGru.SetFocus;
  end;
end;

procedure TFmPQD.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmPQD.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmPQD.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPQDCodigo.Value;
  Close;
end;

procedure TFmPQD.BtConfirmaClick(Sender: TObject);
const
  Obriga = False;
var
  DataB, Nome: String;
  Codigo, Setor, EmitGru, GraCorCad, SemiCodEspReb, Empresa, NFDevol: Integer;
  //CustoInsumo, CustoTotal: Double;
begin
  if UnPQx.ImpedePeloBalanco(TPDataB.Date) then Exit;
  //
  Codigo         := QrPQDCodigo.Value;
  Setor          := EdSetor.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //CustoInsumo    := ;
  //CustoTotal     := ;
  DataB          := Geral.FDT(TPDataB.Date, 1);
  NFDevol        := EdNFDevol.ValueVariant;
  EmitGru        := EdEmitGru.ValueVariant;
  if MyObjects.FIC(Setor = 0, EdSetor, 'Informe o setor!') then
    Exit;
  if Dmod.ObrigaInformarEmitGru(Obriga, EmitGru, EdEmitGru) then
    Exit;
  GraCorCad := EdGraCorCad.ValueVariant;
  SemiCodEspReb := EdSemiCodEspReb.ValueVariant;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('pqd', 'Codigo', ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pqd', False, [
  'Setor', (*'CustoInsumo', 'CustoTotal',*)
  'Nome', 'DataB', 'EmitGru',
  'GraCorCad', 'SemiCodEspReb', 'Empresa',
  'NFDevol'], [
  'Codigo'], [
  Setor, (*CustoInsumo, CustoTotal,*)
  Nome, DataB, EmitGru,
  GraCorCad, SemiCodEspReb, Empresa,
  NFDevol], [
  Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PQD', 'Codigo');
    AtualizaCusto;
    MostraEdicao(0, stLok, Codigo);
  end;
end;

procedure TFmPQD.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PQD', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PQD', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PQD', 'Codigo');
end;

procedure TFmPQD.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //
  TPDtCorrApo.Date  := 0;
  TPDataB.Date      := Date;
  PnEdit.Align      := alClient;
  PnDados.Align     := alClient;
  PnEdita.Align     := alClient;
  DBGIts.Align      := alClient;
  LaRegistro.Align  := alClient;
  // ini 2023/11/21
  //UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQ, Dmod.MyDB, [
  'SELECT Codigo, Nome',
  'FROM pq ',
  'WHERE Ativo=1',
  'AND GGXNiv2 IN (-1, -2, -4, -7, -8, -11, 1, 2, 4, 7, 8, 11)',
  'ORDER BY Nome',
  '']);
  // fim 2023/11/21
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSE, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEmitGru, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrRebaixe, Dmod.MyDB);
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  CriaOForm;
end;

procedure TFmPQD.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPQDCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPQD.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPQD.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmPQD.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQD.QrPQDAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPQD.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'PQD', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmPQD.QrPQDAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrPQDCodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrPQDCodigo.Value, False);
  ReopenPQDIts;
end;

procedure TFmPQD.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPQDCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PQD', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPQD.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQD.QrPQDBeforeOpen(DataSet: TDataSet);
begin
  QrPQDCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPQD.QrPQDAfterClose(DataSet: TDataSet);
begin
  QrPQDIts.Close;
end;

procedure TFmPQD.ReopenPQDIts;
begin
  QrPQDIts.Close;
  QrPQDIts.Params[0].AsInteger := QrPQDCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPQDIts, Dmod.MyDB);
  if FPQDIts <> 0 then QrPQDIts.Locate('OrigemCtrl', FPQDIts, []);
end;

procedure TFmPQD.EdPQChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQD.Estabaixa1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxQUI_RECEI_011_A, 'Outras Baixas de Insumos Qu�micos');
end;

procedure TFmPQD.EdCIChange(Sender: TObject);
begin
  CalculaSaldoFuturo;
end;

procedure TFmPQD.EdPesoAddExit(Sender: TObject);
begin

  CalculaSaldoFuturo;
end;

procedure TFmPQD.CalculaSaldoFuturo();
var
  CI, PQ: Integer;
  PesoAdd, SaldoFut: Double;
begin
  CI := Geral.IMV(EdCI.Text);
  PQ := Geral.IMV(EdPQ.Text);
(*
  QrSaldo.Close;
  QrSaldo.Params[0].AsInteger := CI;
  QrSaldo.Params[1].AsInteger := PQ;
  UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrSaldo, Dmod.MyDB, [
  'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO ',
  'FROM pqcli ',
  'WHERE CI=' + Geral.FF0(CI),
  'AND PQ=' + Geral.FF0(PQ),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  '']);
  //
  PesoAdd := Geral.DMV(EdPesoAdd.Text);
  SaldoFut := QrSaldoPeso.Value - PesoAdd;
  EdSaldoFut.Text := Geral.FFT(SaldoFut, 3, siNegativo);
end;

procedure TFmPQD.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmPQD.BtDesiste2Click(Sender: TObject);
begin
  AtualizaCusto;
  MostraEdicao(0, stLok, 0);
end;

procedure TFmPQD.BtConfirma2Click(Sender: TObject);
const
  Unitario = True;
var
  Controle: Integer;
  KG, RS, SF: Double;
  //
  DataX, DtCorrApo: String;
  OriCodi, OriCtrl, OriTipo, CliOrig, CliDest, Insumo, Empresa: Integer;
  Peso, Valor: Double;
begin
  if PnInsumos.Visible then
  begin
    if QrSaldo.RecordCount = 0 then
    begin
      Geral.MB_Erro('Insumo / cliente interno n�o definido!');
      Exit;
    end;
    SF := Geral.DMV(EdSaldoFut.Text);
    //
    if (SF < 0) and (EdCI.ValueVariant < 0) then
    begin
      Geral.MB_Erro('Quantidade insuficiente no estoque!');
      EdPesoAdd.SetFocus;
      Exit;
    end;
    KG      := Geral.DMV(EdPesoAdd.Text);
    RS      := KG * QrSaldoCUSTO.Value;
    CliOrig := EdCI.ValueVariant;
    CliDest := EdCI.ValueVariant;
    //Controle := UMyMod.BuscaIntSafe(Dmod.MyDB, 'Livres', 'Controle', 'EmitIts',
      //'EmitIts', 'Controle');
    //
    if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
    if MyObjects.FIC(KG <= 0, EdPesoAdd, 'Quantidade inv�lida!') then Exit;
    //
    Controle := DModG.BuscaProximoInteiro(CO_FLD_TAB_PQX, 'OrigemCtrl', 'Tipo', VAR_FATID_0170);
    //
(*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO p q x SET Tipo=' + Geral.FF0(VAR_FATID_0170) + ', DataX=:P0, ');
    Dmod.QrUpd.SQL.Add('CliOrig=:P1, CliDest=:P2, Insumo=:P3, Peso=:P4, ');
    Dmod.QrUpd.SQL.Add('Valor=:P5, OrigemCodi=:P6, OrigemCtrl=:P7');
    Dmod.QrUpd.Params[00].AsString  := Geral.FDT(QrPQDDataB.Value, 1);
    Dmod.QrUpd.Params[01].AsInteger := CliOrig;
    Dmod.QrUpd.Params[02].AsInteger := CliDest;
    Dmod.QrUpd.Params[03].AsInteger := QrSaldoPQ.Value;
    Dmod.QrUpd.Params[04].AsFloat   := -KG;
    Dmod.QrUpd.Params[05].AsFloat   := -RS;
    Dmod.QrUpd.Params[06].AsInteger := QrPQDCodigo.Value;
    Dmod.QrUpd.Params[07].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
*)
    DataX      := Geral.FDT(QrPQDDataB.Value, 1);
    OriCodi    := QrPQDCodigo.Value;
    OriCtrl    := Controle;
    OriTipo    := VAR_FATID_0170;
    CliOrig    := EdCI.ValueVariant;
    CliDest    := EdCI.ValueVariant;
    Insumo     := QrSaldoPQ.Value;
    Peso       := -KG;
    Valor      := -RS;
    DtCorrApo  := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
    //
    PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
      OriCtrl, OriTipo, Unitario, DtCorrApo, Empresa);
    //
    FPQDIts := Controle;
    UnPQx.AtualizaEstoquePQ(QrSaldoCI.Value, QrSaldoPQ.Value, Empresa, aeMsg, '');
    QrSaldo.Close;
    UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
    ReopenPQDIts;
    AtualizaCusto;
    EdPesoAdd.Text := '';
    EdPQ.Text := '';
    CBPQ.KeyValue := NULL;
    EdPQ.SetFocus;
  end;
end;

procedure TFmPQD.IncluiNovaBaixa1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmPQD.IncluiInsumobaixaatual1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmPQD.Alteraitematual1Click(Sender: TObject);
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQDDataB.Value) then Exit;
  // 2023-09-09
  if DBCheck.CriaFm(TFmPQDEdit, FmPQDEdit, afmoNegarComAviso) then
  begin
    FmPQDEdit.EdPeso.ValueVariant  := -QrPQDItsPeso.Value;
    FmPQDEdit.EdValor.ValueVariant := -QrPQDItsValor.Value;
    //
    FmPQDEdit.FEmpresa    := QrPQDItsEmpresa.Value;
    FmPQDEdit.FInsumo     := QrPQDItsInsumo.Value;
    FmPQDEdit.FCliOrig    := QrPQDItsCliOrig.Value;
    FmPQDEdit.FCliDest    := QrPQDItsCliDest.Value;
    FmPQDEdit.FDataX      := QrPQDItsDataX.Value;
    FmPQDEdit.FCliInt     := QrPQDItsCliOrig.Value;
    FmPQDEdit.FOrigemCodi := QrPQDItsOrigemCodi.Value;
    FmPQDEdit.FOrigemCtrl := QrPQDItsOrigemCtrl.Value;
    FmPQDEdit.FTipo       := QrPQDItsTipo.Value;
    //
    FmPQDEdit.ShowModal;
    FmPQDEdit.Destroy;
    //
    QrPQDIts.Close;
    UnDmkDAC_PF.AbreQuery(QrPQDIts, Dmod.MyDB);
  end;
end;

procedure TFmPQD.AtualizaCusto;
begin
  LocCod(QrPQDCodigo.Value, QrPQDCodigo.Value);
  //
  QrSumPQ.Close;
  QrSumPQ.Params[0].AsInteger := QrPQDCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSumPQ, Dmod.MyDB);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE pqd SET CustoInsumo=:P0, ');
  Dmod.QrUpd.SQL.Add('CustoTotal=:P1 WHERE Codigo=:Pa ');
  Dmod.QrUpd.Params[00].AsFloat   := QrSumPQCUSTO.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPQCUSTO.Value;
  Dmod.QrUpd.Params[02].AsInteger := QrPQDCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  LocCod(QrPQDCodigo.Value, QrPQDCodigo.Value);
end;

procedure TFmPQD.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmPQD.PMAlteraPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPQD.State <> dsInactive) and (QrPQD.RecordCount > 0);
  Enab2 := (QrPQDIts.State <> dsInactive) and (QrPQDIts.RecordCount > 0);
  //
  Alteraitematual1.Enabled := Enab and Enab2;
  AlteraBaixa1.Enabled     := Enab;
end;

procedure TFmPQD.PMExcluiPopup(Sender: TObject);
begin
  ExcluiBaixa1.Enabled       := not Geral.IntToBool_0(QrPQDIts.RecordCount);
  ExcluiItemdabaixa1.Enabled := Geral.IntToBool_0(QrPQDIts.RecordCount);
end;

procedure TFmPQD.ExcluiBaixa1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o de TODOS ITENS '+
  'desta baixa e a pr�pria baixa?')= ID_YES then
  begin
    QrPQDIts.First;
    while not QrPQDIts.Eof do
    begin
      ExcluiItemSelecionado(False);
      QrPQDIts.Next;
    end;
    //AtualizaCusto;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM pqd WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrPQDCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    LocCod(QrPQDCodigo.Value, QrPQDCodigo.Value);
  end;
end;

procedure TFmPQD.ExcluiItemdabaixa1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do item selecionado '+
  'nesta baixa?')= ID_YES then
     ExcluiItemSelecionado(True);
end;

procedure TFmPQD.ExcluiItemSelecionado(AtzCusto: Boolean);
var
  Atual, OriCodi, OriCtrl, OriTipo, Insumo, CliInt, Empresa: Integer;
begin
  Insumo  := QrPQDItsInsumo.Value;
  CliInt  := QrPQDItsCliOrig.Value;
  Empresa := QrPQDItsEmpresa.Value;
  Atual := QrPQDItsOrigemCtrl.Value;
(*
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM p q x WHERE Tipo=' + Geral.FF0(VAR_FATID_0170) + ' AND OrigemCtrl=:P0');
  Dmod.QrUpd.SQL.Add('AND OrigemCodi=:P1');
  Dmod.QrUpd.Params[00].AsInteger := Atual;
  Dmod.QrUpd.Params[01].AsInteger := QrPQDCodigo.Value;
  Dmod.QrUpd.ExecSQL;
*)
    OriCodi := QrPQDCodigo.Value;
    OriCtrl := Atual;
    OriTipo := VAR_FATID_0170;
    PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, True, Empresa);
    //
  if AtzCusto then
    AtualizaCusto;
  UnPQx.AtualizaEstoquePQ(CliInt, Insumo, Empresa, aeNenhum, '');
  //FPQDIts := UMyMod.ProximoRegistro(QrPQDIts, 'OrigemCtrl', Atual);
  //
end;

procedure TFmPQD.AlteraBaixa1Click(Sender: TObject);
var
  PQD : Integer;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQDDataB.Value) then Exit;
  // 2023-09-09
  PQD := QrPQDCodigo.Value;
  if not UMyMod.SelLockY(PQD, Dmod.MyDB, 'PQD', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PQD, Dmod.MyDB, 'PQD', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPQD.Outros1Click(Sender: TObject);
begin
  FmPrincipal.Relatrios1Click(Self);
end;

procedure TFmPQD.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmPQD.frBaixaAtualUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  if AnsiCompareText(Name, 'VFR_GRD') = 0 then Val := 15//Geral.BoolToInt2(CkGrade.Checked, 15, 0)
end;

procedure TFmPQD.frxQUI_RECEI_011_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VFR_GRD' then Value := 15 else
  if VarName = 'VARF_DATA' then Value := Now() else
end;

end.

