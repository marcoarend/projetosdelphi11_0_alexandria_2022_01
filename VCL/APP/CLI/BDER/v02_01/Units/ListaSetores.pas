unit ListaSetores;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums;

type
  TFmListaSetores = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrListaSetores: TmySQLQuery;
    QrListaSetoresLk: TIntegerField;
    QrListaSetoresDataCad: TDateField;
    QrListaSetoresDataAlt: TDateField;
    QrListaSetoresUserCad: TIntegerField;
    QrListaSetoresUserAlt: TIntegerField;
    QrListaSetoresCodigo: TSmallintField;
    QrListaSetoresNome: TWideStringField;
    QrListaSetoresSigla: TWideStringField;
    QrListaSetoresm2Dia: TFloatField;
    DsListaSetores: TDataSource;
    DBEdNome: TdmkDBEdit;
    DBEdCodigo: TdmkDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    EdSigla: TdmkEdit;
    Label4: TLabel;
    RGTpReceita: TdmkRadioGroup;
    dmkRadioGroup1: TDBRadioGroup;
    QrListaSetoresTpReceita: TSmallintField;
    DBRadioGroup1: TDBRadioGroup;
    QrListaSetoresTpSetor: TSmallintField;
    RGTpSetor: TdmkRadioGroup;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Label5: TLabel;
    Edm2Dia: TdmkEdit;
    EdCourosDia: TdmkEdit;
    Label8: TLabel;
    EdKgDia: TdmkEdit;
    Label10: TLabel;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrListaSetoresCourosDia: TFloatField;
    QrListaSetoresKgDia: TFloatField;
    EdOperPosProcDescr: TdmkEdit;
    Label6: TLabel;
    QrListaSetoresOperPosProcDescr: TWideStringField;
    Label14: TLabel;
    DBEdit5: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrListaSetoresAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrListaSetoresBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmListaSetores: TFmListaSetores;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmListaSetores.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmListaSetores.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrListaSetoresCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmListaSetores.DefParams;
begin
  VAR_GOTOTABELA := 'listasetores';
  VAR_GOTOMYSQLTABLE := QrListaSetores;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM listasetores');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome LIKE :P0');
  //
end;

procedure TFmListaSetores.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmListaSetores.QueryPrincipalAfterOpen;
begin
end;

procedure TFmListaSetores.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmListaSetores.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmListaSetores.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmListaSetores.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmListaSetores.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmListaSetores.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmListaSetores.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrListaSetores, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'listasetores');
end;

procedure TFmListaSetores.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrListaSetoresCodigo.Value;
  Close;
end;

procedure TFmListaSetores.BtConfirmaClick(Sender: TObject);
var
  Codigo, Loc, TpReceita, TpSetor: Integer;
  Nome, Sigla, OperPosProcDescr: String;
  m2Dia, CourosDia, KgDia: Double;
  //
  Qry: TmySQLQuery;
begin
  Nome := Trim(EdNome.Text);
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o.') then
    Exit;
  //
  Sigla := Trim(EdSigla.Text);
  if MyObjects.FIC(Sigla = '', EdSigla, 'Defina uma sigla.') then
    Exit;
  //
  OperPosProcDescr := EdOperPosProcDescr.Text;
  if MyObjects.FIC(OperPosProcDescr = '', EdOperPosProcDescr, 'Descri��o da opera��o subsequente.') then
    Exit;
  //
  if ImgTipo.SQLType = stIns then
    Loc := -1000
  else
    Loc := QrListaSetoresCodigo.Value;
  //
  Qry := TmySQLQuery.Create(Dmod);
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT * ',
  'FROM listasetores ',
  'WHERE Sigla="' + Sigla + '" ',
  'AND Codigo<>' + Geral.FF0(Loc),
  '']);
  if MyObjects.FIC(Qry.RecordCount > 0, EdSigla,
  'Inclus�o cancelada, a sigla informada j� est� cadastrada para o sertor n� ' +
  Geral.FF0(Qry.FieldByName('Codigo').AsInteger) + ' - "' +
  Qry.FieldByName('Nome').AsString + '"') then
    Exit;
  m2Dia     := Edm2Dia.ValueVariant;
  CourosDia := EdCourosDia.ValueVariant;
  KgDia     := EdKgDia.ValueVariant;
  TpReceita := RGTpReceita.ItemIndex;
  TpSetor   := RGTpSetor.ItemIndex;
  Codigo := EdCodigo.ValueVariant;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('listasetores', 'Codigo', ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'listasetores', False, [
  'Nome', 'Sigla', 'm2Dia',
  'TpReceita', 'TpSetor',
  'CourosDia', 'KgDia',
  'OperPosProcDescr'], [
  'Codigo'], [
  Nome, Sigla, m2Dia,
  TpReceita, TpSetor,
  CourosDia, KgDia,
  OperPosProcDescr], [
  Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'listasetores', 'Codigo');
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmListaSetores.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'listasetores', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmListaSetores.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrListaSetores, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'listasetores');
end;

procedure TFmListaSetores.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmListaSetores.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrListaSetoresCodigo.Value, LaRegistro.Caption);
end;

procedure TFmListaSetores.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmListaSetores.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrListaSetoresCodigo.Value, LaRegistro.Caption);
end;

procedure TFmListaSetores.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmListaSetores.QrListaSetoresAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmListaSetores.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmListaSetores.SbQueryClick(Sender: TObject);
begin
  LocCod(QrListaSetoresCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'listasetores', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmListaSetores.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmListaSetores.QrListaSetoresBeforeOpen(DataSet: TDataSet);
begin
  QrListaSetoresCodigo.DisplayFormat := FFormatFloat;
end;

end.

