unit PQSubstitui;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkDBLookupComboBox, dmkEditCB, dmkEdit,
  mySQLDbTables, UnDmkEnums, frxClass, frxDBSet, Vcl.Menus;

type
  TFmPQSubstitui = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrPQ: TmySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsPQ: TDataSource;
    QrCI: TmySQLQuery;
    QrCINome: TWideStringField;
    QrCICodigo: TIntegerField;
    DsCI: TDataSource;
    PainelDados: TPanel;
    LaOld: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    EdClienteI: TdmkEditCB;
    CBClienteI: TdmkDBLookupComboBox;
    CkClienteI: TCheckBox;
    QrFormulasIts: TmySQLQuery;
    DsFormulasIts: TDataSource;
    QrTintasIts: TmySQLQuery;
    DsTintasIts: TDataSource;
    QrFormulasItsNumero: TIntegerField;
    QrFormulasItsOrdem: TIntegerField;
    QrFormulasItsControle: TIntegerField;
    QrFormulasItsPorcent: TFloatField;
    QrFormulasItsNO_FRM: TWideStringField;
    QrTintasItsNumero: TIntegerField;
    QrTintasItsOrdem: TIntegerField;
    QrTintasItsControle: TIntegerField;
    QrTintasItsGramasTi: TFloatField;
    QrTintasItsGramasKg: TFloatField;
    QrTintasItsNO_FRM: TWideStringField;
    QrTintasItsNO_TIN: TWideStringField;
    PnPsq: TPanel;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    GroupBox3: TGroupBox;
    DBGrid2: TDBGrid;
    BtExecuta: TBitBtn;
    Panel5: TPanel;
    QrPrd: TmySQLQuery;
    DsPrd: TDataSource;
    LaNew: TLabel;
    EdPrd: TdmkEditCB;
    CBPrd: TdmkDBLookupComboBox;
    EdProporcao: TdmkEdit;
    Label4: TLabel;
    QrPrdCodigo: TIntegerField;
    QrPrdNome: TWideStringField;
    SbImprime: TBitBtn;
    frxQUI_INSUM_005_01: TfrxReport;
    QrRibIts: TmySQLQuery;
    QrAcaIts: TmySQLQuery;
    QrRibItsProduto: TIntegerField;
    QrRibItsITENS: TLargeintField;
    QrRibItsNome: TWideStringField;
    QrAcaItsProduto: TIntegerField;
    QrAcaItsITENS: TLargeintField;
    QrAcaItsNome: TWideStringField;
    frxDsRibIts: TfrxDBDataset;
    frxDsAcaIts: TfrxDBDataset;
    frxQUI_INSUM_005_02: TfrxReport;
    QrRoll: TmySQLQuery;
    frxDsRoll: TfrxDBDataset;
    QrRollNumero: TIntegerField;
    QrRollNome: TWideStringField;
    QrRollClienteI: TIntegerField;
    QrRollNO_EMPRESA: TWideStringField;
    PMImprime: TPopupMenu;
    Insumosativosemreceitas1: TMenuItem;
    Listadereceitasderibeira1: TMenuItem;
    ListadereceitasdeAcabamento1: TMenuItem;
    QrTintasItsCodigo: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure EdPQChange(Sender: TObject);
    procedure CkClienteIClick(Sender: TObject);
    procedure EdClienteIChange(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxQUI_INSUM_005_01GetValue(const VarName: string; var Value: Variant);
    procedure Listadereceitasderibeira1Click(Sender: TObject);
    procedure Insumosativosemreceitas1Click(Sender: TObject);
    procedure ListadereceitasdeAcabamento1Click(Sender: TObject);
    procedure BtExecutaClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FTipoReceita: String;
    //
    procedure FechaPesquisa();
    procedure ImprimeReceita(Titulo: String);
    procedure ReabrePesquisa(FormulasIts, TintasIts: Integer);


  public
    { Public declarations }
  end;

  var
  FmPQSubstitui: TFmPQSubstitui;

implementation

uses UnMyObjects, Module, DmkDAC_PF, Principal, UnDmkProcFunc, ModuleGeral,
  UMySQLModule, UnPQ_PF;

{$R *.DFM}

resourcestring
  sRibeira = 'Ribeira';
  sAcabamento = 'Acabamento';

procedure TFmPQSubstitui.BtExecutaClick(Sender: TObject);
var
  PQ_Old, PQ_New: Integer;
  Fator, Porcent, GramasTi(*, GramasKg*): Double;
  Produto, GraGruX, Numero, Ordem, Controle, Codigo: Integer;
  FormulasIts, TintasIts: Integer;
begin
  PQ_Old := EdPQ.ValueVariant;
  PQ_New := EdPrd.ValueVariant;
  //
  if MyObjects.FIC(PQ_Old = 0, EdPQ, '"' + LaOld.Caption + '" inv�lido!') then
    Exit;
  if MyObjects.FIC(PQ_New = 0, EdPrd, '"' + LaNew.Caption + '" inv�lido!') then
    Exit;
  //
  if Geral.MB_Pergunta('Confirma a substitui��o de todos insumos c�digo ' +
  Geral.FF0(PQ_Old) + ' pelo insumo c�digo ' + Geral.FF0(PQ_New) +
  ' em todas receitas de ribeira e de acabamento?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      FormulasIts := QrFormulasItsControle.Value;
      TintasIts   := QrTintasItsControle.Value;
      //
      Fator := EdProporcao.ValueVariant / 100;
      //
      QrFormulasIts.First;
      while not QrFormulasIts.Eof do
      begin
        GraGruX  := 0; // Ver o que fazer!
        Porcent  := Fator * QrFormulasItsPorcent.Value;
        Produto  := PQ_New;
        Numero   := QrFormulasItsNumero.Value;
        Ordem    := QrFormulasItsOrdem.Value;
        Controle := QrFormulasItsControle.Value;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'formulasits', False, [
        'Porcent', 'Produto', 'GraGruX'], [
        'Numero', 'Ordem', 'Controle'], [
        Porcent, Produto, GraGruX], [
        Numero, Ordem, Controle], False);
        //
        QrFormulasIts.Next;
      end;
      //
      QrTintasIts.First;
      while not QrTintasIts.Eof do
      begin
        Fator    := EdProporcao.ValueVariant / 100;
        GraGruX  := 0; // Ver o que fazer!
        GramasTi := Fator * QrTintasItsGramasTi.Value;
        Codigo   := QrTintasItsCodigo.Value;
        (*
        GramasKg := ?
        *)
        if Codigo <> 0 then
        begin
          Produto  := PQ_New;
          Controle := QrTintasItsControle.Value;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tintasits', False, [
            'Produto', 'GramasTi',
            (*'GramasKg',*) 'GraGruX'], [
            'Controle'], [
            Produto, GramasTi,
            (*GramasKg,*) GraGruX], [
            Controle], True);
          //
          PQ_PF.AtualizaItensProcesso(Codigo, Dmod.MyDB);
        end;
        //
        QrTintasIts.Next;
      end;
      //
      Geral.MB_Info('Atualiza��o finalizada!');
      //
      ReabrePesquisa(FormulasIts, TintasIts);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPQSubstitui.BtOKClick(Sender: TObject);
begin
  ReabrePesquisa(0, 0);
end;

procedure TFmPQSubstitui.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQSubstitui.CkClienteIClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPQSubstitui.DBGrid1DblClick(Sender: TObject);
var
  Numero, Controle, FormulasIts, TintasIts: Integer;
begin
  if (QrFormulasIts.State <> dsInactive) and (QrFormulasIts.RecordCount > 0) then
  begin
    Numero   := QrFormulasItsNumero.Value;
    Controle := QrFormulasItsControle.Value;
    //
    FmPrincipal.CadastroFormulas(Numero, Controle);
    //
    FormulasIts := QrFormulasItsControle.Value;
    TintasIts   := QrTintasItsControle.Value;
    //
    ReabrePesquisa(FormulasIts, TintasIts);
  end;
end;

procedure TFmPQSubstitui.DBGrid2DblClick(Sender: TObject);
var
  Numero, Codigo, Controle, FormulasIts, TintasIts: Integer;
begin
  if (QrTintasIts.State <> dsInactive) and (QrTintasIts.RecordCount > 0) then
  begin
    Numero   := QrTintasItsNumero.Value;
    Codigo := QrTintasItsCodigo.Value;
    Controle := QrTintasItsControle.Value;
    //
    FmPrincipal.CadastroTintas(Numero, Codigo, Controle);
    //
    TintasIts := QrTintasItsControle.Value;
    TintasIts   := QrTintasItsControle.Value;
    //
    ReabrePesquisa(TintasIts, TintasIts);
  end;
end;

procedure TFmPQSubstitui.EdClienteIChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPQSubstitui.EdPQChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPQSubstitui.FechaPesquisa();
begin
  BtExecuta.Enabled := False;
  PnPsq.Visible := False;
  QrFormulasIts.Close;
  QrTintasIts.Close;
end;

procedure TFmPQSubstitui.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQSubstitui.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrd, Dmod.MyDB);
end;

procedure TFmPQSubstitui.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQSubstitui.frxQUI_INSUM_005_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'NO_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBClienteI.Text, EdClienteI.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_TIPO_RECEITA' then
    Value := FTipoReceita
  else
end;

procedure TFmPQSubstitui.ImprimeReceita(Titulo: String);
var
  Tabela: String;
begin
  FTipoReceita := Titulo;
  if Titulo = sRibeira then
    Tabela := 'formulas'
  else
    Tabela := 'tintascab';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRoll, Dmod.MyDB, [
    'SELECT frm.Numero, frm.Nome, frm.ClienteI, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA ',
    'FROM ' + LowerCase(Tabela) + ' frm ',
    'LEFT JOIN entidades ent ON ent.Codigo=frm.ClienteI ',
    'WHERE frm.Numero<>0 ',
    'ORDER BY NO_EMPRESA, Nome ',
    '']);
  //
  MyObjects.frxDefineDataSets(frxQUI_INSUM_005_02, [
    DModG.frxDsDono,
    frxDsRoll
  ]);
  MyObjects.frxMostra(frxQUI_INSUM_005_02, 'Lista de receitas de ' + Titulo);
end;

procedure TFmPQSubstitui.Insumosativosemreceitas1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRibIts, Dmod.MyDB, [
    'SELECT foi.Produto, ',
    'COUNT(foi.Produto) ITENS, pq_.Nome ',
    'FROM formulasits foi ',
    'LEFT JOIN pq pq_ ON pq_.Codigo=foi.Produto ',
    'WHERE Produto > 0 ',
    'GROUP BY foi.Produto ',
    'ORDER BY pq_.Nome ',
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAcaIts, Dmod.MyDB, [
    'SELECT tin.Produto, ',
    'COUNT(tin.Produto) ITENS, pq_.Nome ',
    'FROM tintasits tin ',
    'LEFT JOIN pq pq_ ON pq_.Codigo=tin.Produto ',
    'WHERE Produto > 0 ',
    'GROUP BY tin.Produto ',
    'ORDER BY pq_.Nome ',
    '']);
  //
  MyObjects.frxDefineDataSets(frxQUI_INSUM_005_01, [
    DModG.frxDsDono,
    frxDsAcaIts,
    frxDsRibIts
  ]);
  MyObjects.frxMostra(frxQUI_INSUM_005_01, 'Insumos ativos em receitas');
end;

procedure TFmPQSubstitui.ListadereceitasdeAcabamento1Click(Sender: TObject);
begin
  ImprimeReceita(sAcabamento);
end;

procedure TFmPQSubstitui.Listadereceitasderibeira1Click(Sender: TObject);
begin
  ImprimeReceita(sRibeira);
end;

procedure TFmPQSubstitui.ReabrePesquisa(FormulasIts, TintasIts: Integer);
var
  Produto, ClienteI: Integer;
  SQL_ClienteI: String;
begin
  Produto := EdPQ.ValueVariant;
  //if MyObjects.FIC(Produto = 0, EdProduto, ''
  ClienteI := EdClienteI.ValueVariant;
  if CkClienteI.Checked then
    SQL_ClienteI := 'AND frm.ClienteI=' + Geral.FF0(ClienteI)
  else
    SQL_ClienteI := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulasIts, Dmod.MyDb, [
    'SELECT foi.Numero, foi.Ordem, foi.Controle, foi.Porcent, ',
    'frm.Nome NO_FRM ',
    'FROM formulasits foi ',
    'LEFT JOIN formulas frm ON frm.Numero=foi.Numero ',
    'WHERE foi.Produto=' + Geral.FF0(Produto),
    SQL_ClienteI,
    'ORDER BY foi.Numero, foi.Ordem ',
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTintasIts, Dmod.MyDb, [
    'SELECT foi.Numero, foi.Ordem, ',
    'foi.Codigo, foi.Controle, foi.GramasTi, ',
    'foi.GramasKg, frm.Nome NO_FRM, tin.Nome NO_TIN ',
    'FROM tintasits foi ',
    'LEFT JOIN tintascab frm ON frm.Numero=foi.Numero ',
    'LEFT JOIN tintastin tin ON tin.Codigo=foi.Codigo ',
    'WHERE foi.Produto=' + Geral.FF0(Produto),
    SQL_ClienteI,
    'ORDER BY foi.Numero, tin.Ordem, foi.Ordem ',
    '']);
  //
  PnPsq.Visible := True;
  BtExecuta.Enabled := (QrFormulasIts.RecordCount + QrTintasIts.RecordCount) > 0;
end;

procedure TFmPQSubstitui.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

end.
