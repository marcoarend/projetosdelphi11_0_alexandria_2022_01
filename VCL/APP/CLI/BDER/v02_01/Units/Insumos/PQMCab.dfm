object FmPQMCab: TFmPQMCab
  Left = 368
  Top = 194
  Caption = 'QUI-RECEI-019 :: Dilui'#231#245'es e Misturas de Insumos'
  ClientHeight = 631
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 535
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 4
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 97
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 16
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 452
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Data:'
        FocusControl = DBEdit3
      end
      object Label10: TLabel
        Left = 672
        Top = 16
        Width = 35
        Height = 13
        Caption = '$ Baixa'
        FocusControl = DBEdit4
      end
      object Label11: TLabel
        Left = 668
        Top = 56
        Width = 47
        Height = 13
        Caption = '$ Gerado:'
        FocusControl = DBEdit5
      end
      object Label12: TLabel
        Left = 568
        Top = 16
        Width = 49
        Height = 13
        Caption = 'QtdeBaixa'
        FocusControl = DBEdit6
      end
      object Label13: TLabel
        Left = 568
        Top = 56
        Width = 52
        Height = 13
        Caption = 'QtdeGerou'
        FocusControl = DBEdit7
      end
      object Label14: TLabel
        Left = 500
        Top = 56
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        FocusControl = DBEdit8
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPQMCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 16
        Top = 72
        Width = 481
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsPQMCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsPQMCab
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 317
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsPQMCab
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 452
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DataB'
        DataSource = DsPQMCab
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 672
        Top = 32
        Width = 100
        Height = 21
        DataField = 'CustoBaixa'
        DataSource = DsPQMCab
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 672
        Top = 72
        Width = 100
        Height = 21
        DataField = 'CustoGerou'
        DataSource = DsPQMCab
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 568
        Top = 32
        Width = 100
        Height = 21
        DataField = 'QtdeBaixa'
        DataSource = DsPQMCab
        TabOrder = 7
      end
      object DBEdit7: TDBEdit
        Left = 568
        Top = 72
        Width = 100
        Height = 21
        DataField = 'QtdeGerou'
        DataSource = DsPQMCab
        TabOrder = 8
      end
      object DBEdit8: TDBEdit
        Left = 500
        Top = 72
        Width = 64
        Height = 21
        DataField = 'VSMovCod'
        DataSource = DsPQMCab
        TabOrder = 9
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 471
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 10001
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtBxa: TBitBtn
          Tag = 450
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Baixa'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtBxaClick
        end
        object BtGer: TBitBtn
          Tag = 10207
          Left = 252
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Gera'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtGerClick
        end
      end
    end
    object GBBxa: TGroupBox
      Left = 0
      Top = 97
      Width = 784
      Height = 105
      Align = alTop
      Caption = ' Insumos utilizados (Baixas):'
      TabOrder = 2
      object DBGBxa: TDBGrid
        Left = 2
        Top = 15
        Width = 780
        Height = 88
        Align = alClient
        DataSource = DsPQMBxa
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'OrigemCtrl'
            Title.Caption = 'ID item'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Insumo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEPQ'
            Title.Caption = 'Descri'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Title.Caption = 'Quantidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Sigla'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end>
      end
    end
    object GBGer: TGroupBox
      Left = 0
      Top = 366
      Width = 784
      Height = 105
      Align = alBottom
      Caption = ' Insumos produzidos (Gerados):'
      TabOrder = 3
      object DBGGer: TDBGrid
        Left = 2
        Top = 15
        Width = 780
        Height = 88
        Align = alClient
        DataSource = DsPQMGer
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'OrigemCtrl'
            Title.Caption = 'ID item'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Insumo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEPQ'
            Title.Caption = 'Descri'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Title.Caption = 'Quantidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Sigla'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end>
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 535
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 508
        Top = 56
        Width = 80
        Height = 13
        Caption = 'Setor de destino:'
      end
      object Label8: TLabel
        Left = 16
        Top = 56
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label4: TLabel
        Left = 404
        Top = 56
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSetor: TdmkEditCB
        Left = 508
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Setor'
        UpdCampo = 'Setor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSetor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSetor: TdmkDBLookupComboBox
        Left = 564
        Top = 72
        Width = 145
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSE
        TabOrder = 3
        dmkEditCB = EdSetor
        QryCampo = 'Setor'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEmpresa: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 76
        Top = 72
        Width = 325
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 5
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPDataB: TdmkEditDateTimePicker
        Left = 404
        Top = 72
        Width = 101
        Height = 21
        Date = 38799.000000000000000000
        Time = 0.930188981503306400
        TabOrder = 6
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataB'
        UpdCampo = 'DataB'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 472
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 395
        Height = 32
        Caption = 'Dilui'#231#245'es e Misturas de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 395
        Height = 32
        Caption = 'Dilui'#231#245'es e Misturas de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 395
        Height = 32
        Caption = 'Dilui'#231#245'es e Misturas de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 16
  end
  object QrPQMCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPQMCabBeforeOpen
    AfterOpen = QrPQMCabAfterOpen
    BeforeClose = QrPQMCabBeforeClose
    AfterScroll = QrPQMCabAfterScroll
    SQL.Strings = (
      'SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'lse.Nome NOMESETOR, reb.Linhas RebLin, '
      'emg.Nome NO_EMITGRU, gcc.Nome NoGraCorCad, pqm.*'
      'FROM pqm pqm'
      'LEFT JOIN entidades emp ON emp.Codigo=pqm.Empresa'
      'LEFT JOIN listasetores lse ON lse.Codigo=pqm.Setor'
      'LEFT JOIN emitgru emg ON emg.Codigo=pqm.EmitGru'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=pqm.GraCorCad'
      'LEFT JOIN espessuras reb ON reb.Codigo=pqm.SemiCodEspReb'
      'WHERE pqm.Codigo > 0'
      'AND pqm.Empresa=-11')
    Left = 148
    Top = 61
    object QrPQMCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrPQMCabNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrPQMCabRebLin: TWideStringField
      FieldName = 'RebLin'
      Size = 7
    end
    object QrPQMCabNO_EMITGRU: TWideStringField
      FieldName = 'NO_EMITGRU'
      Size = 60
    end
    object QrPQMCabNoGraCorCad: TWideStringField
      FieldName = 'NoGraCorCad'
      Size = 30
    end
    object QrPQMCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQMCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPQMCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPQMCabSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrPQMCabQtdeBaixa: TFloatField
      FieldName = 'QtdeBaixa'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQMCabQtdeGerou: TFloatField
      FieldName = 'QtdeGerou'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQMCabCustoBaixa: TFloatField
      FieldName = 'CustoBaixa'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPQMCabCustoGerou: TFloatField
      FieldName = 'CustoGerou'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPQMCabDataB: TDateField
      FieldName = 'DataB'
    end
    object QrPQMCabEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrPQMCabVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
    end
    object QrPQMCabGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrPQMCabSemiCodEspReb: TIntegerField
      FieldName = 'SemiCodEspReb'
    end
    object QrPQMCabSemiTxtEspReb: TWideStringField
      FieldName = 'SemiTxtEspReb'
      Size = 15
    end
    object QrPQMCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQMCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQMCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQMCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQMCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQMCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQMCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsPQMCab: TDataSource
    DataSet = QrPQMCab
    Left = 144
    Top = 105
  end
  object QrPQMBxa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  '
      'pqg.Nome NOMEGRUPO '
      'FROM pqx pqx '
      'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo '
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ '
      'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico '
      'WHERE pqx.Tipo=130 '
      'AND pqx.OrigemCodi>0 '
      ' ')
    Left = 188
    Top = 237
    object QrPQMBxaDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQMBxaOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQMBxaOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQMBxaTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPQMBxaCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQMBxaCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPQMBxaInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQMBxaPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQMBxaValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPQMBxaRetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrPQMBxaStqMovIts: TIntegerField
      FieldName = 'StqMovIts'
    end
    object QrPQMBxaRetQtd: TFloatField
      FieldName = 'RetQtd'
    end
    object QrPQMBxaHowLoad: TSmallintField
      FieldName = 'HowLoad'
    end
    object QrPQMBxaStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrPQMBxaSdoPeso: TFloatField
      FieldName = 'SdoPeso'
    end
    object QrPQMBxaSdoValr: TFloatField
      FieldName = 'SdoValr'
    end
    object QrPQMBxaAcePeso: TFloatField
      FieldName = 'AcePeso'
    end
    object QrPQMBxaAceValr: TFloatField
      FieldName = 'AceValr'
    end
    object QrPQMBxaDtCorrApo: TDateField
      FieldName = 'DtCorrApo'
    end
    object QrPQMBxaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPQMBxaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQMBxaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQMBxaNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQMBxaGrupoQuimico: TIntegerField
      FieldName = 'GrupoQuimico'
    end
    object QrPQMBxaNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 100
    end
    object QrPQMBxaSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
  end
  object DsPQMBxa: TDataSource
    DataSet = QrPQMBxa
    Left = 188
    Top = 281
  end
  object PMBxa: TPopupMenu
    OnPopup = PMBxaPopup
    Left = 432
    Top = 536
    object BxaInclui1: TMenuItem
      Caption = '&Baixa insumo'
      Enabled = False
      OnClick = BxaInclui1Click
    end
    object BxaAltera1: TMenuItem
      Caption = '&Edita baixa de insumo atual'
      Enabled = False
      Visible = False
      OnClick = BxaAltera1Click
    end
    object BxaExclui1: TMenuItem
      Caption = '&Remove baixa de insumo'
      Enabled = False
      OnClick = BxaExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 308
    Top = 532
    object CabInclui1: TMenuItem
      Caption = '&Nova mistura'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera mistura atual'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui mistura atual'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrSE: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM listasetores'
      'ORDER BY Nome')
    Left = 204
    Top = 64
    object QrSECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSENome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object DsSE: TDataSource
    DataSet = QrSE
    Left = 204
    Top = 112
  end
  object PMGer: TPopupMenu
    OnPopup = PMGerPopup
    Left = 560
    Top = 532
    object GerInclui1: TMenuItem
      Caption = '&Gera insumo'
      Enabled = False
      OnClick = GerInclui1Click
    end
    object GerAltera1: TMenuItem
      Caption = '&Edita item gerado'
      Enabled = False
      Visible = False
      OnClick = GerAltera1Click
    end
    object GerExclui1: TMenuItem
      Caption = '&Remove item gerado'
      Enabled = False
      OnClick = GerExclui1Click
    end
  end
  object QrPQMGer: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  '
      'pqg.Nome NOMEGRUPO '
      'FROM pqx pqx '
      'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo '
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ '
      'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico '
      'WHERE pqx.Tipo=130 '
      'AND pqx.OrigemCodi>0 '
      ' ')
    Left = 256
    Top = 241
    object QrPQMGerDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQMGerOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQMGerOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQMGerTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPQMGerCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQMGerCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPQMGerInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQMGerPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQMGerValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPQMGerRetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrPQMGerStqMovIts: TIntegerField
      FieldName = 'StqMovIts'
    end
    object QrPQMGerRetQtd: TFloatField
      FieldName = 'RetQtd'
    end
    object QrPQMGerHowLoad: TSmallintField
      FieldName = 'HowLoad'
    end
    object QrPQMGerStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrPQMGerSdoPeso: TFloatField
      FieldName = 'SdoPeso'
    end
    object QrPQMGerSdoValr: TFloatField
      FieldName = 'SdoValr'
    end
    object QrPQMGerAcePeso: TFloatField
      FieldName = 'AcePeso'
    end
    object QrPQMGerAceValr: TFloatField
      FieldName = 'AceValr'
    end
    object QrPQMGerDtCorrApo: TDateField
      FieldName = 'DtCorrApo'
    end
    object QrPQMGerEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPQMGerAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQMGerAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQMGerNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQMGerGrupoQuimico: TIntegerField
      FieldName = 'GrupoQuimico'
    end
    object QrPQMGerNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 100
    end
    object QrPQMGerSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
  end
  object DsPQMGer: TDataSource
    DataSet = QrPQMGer
    Left = 256
    Top = 285
  end
end
