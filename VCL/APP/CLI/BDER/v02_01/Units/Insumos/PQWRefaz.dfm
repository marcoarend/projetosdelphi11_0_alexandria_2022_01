object FmPQWRefaz: TFmPQWRefaz
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-000 :: Refaz Baixas de Uso e Consumo por NF'
  ClientHeight = 348
  ClientWidth = 612
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 84
    Top = 44
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object Label4: TLabel
    Left = 84
    Top = 24
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 612
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 564
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 516
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 490
        Height = 32
        Caption = 'Refaz Baixas de Uso e Consumo por NF'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 490
        Height = 32
        Caption = 'Refaz Baixas de Uso e Consumo por NF'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 490
        Height = 32
        Caption = 'Refaz Baixas de Uso e Consumo por NF'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 200
    Width = 612
    Height = 78
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 608
      Height = 61
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 27
        Width = 608
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
      object PB2: TProgressBar
        Left = 0
        Top = 44
        Width = 608
        Height = 17
        Align = alBottom
        TabOrder = 1
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 278
    Width = 612
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 466
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 464
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 612
    Height = 152
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 612
      Height = 81
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 44
        Top = 28
        Width = 30
        Height = 13
        Caption = 'In'#237'cio:'
      end
      object Label2: TLabel
        Left = 44
        Top = 48
        Width = 25
        Height = 13
        Caption = 'Final:'
      end
      object LaFim: TLabel
        Left = 84
        Top = 48
        Width = 36
        Height = 13
        Caption = '0:00:00'
      end
      object LaIni: TLabel
        Left = 84
        Top = 28
        Width = 36
        Height = 13
        Caption = '0:00:00'
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrPQs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pqx.CliOrig, pqx.Insumo, pq_.Nome NO_PQ '
      'FROM pqx '
      'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo '
      'GROUP BY pqx.CliOrig, pqx.Insumo '
      'ORDER BY pqx.CliOrig, pqx.Insumo ')
    Left = 36
    Top = 76
    object QrPQsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQsNO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrPQsCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object DsPQs: TDataSource
    DataSet = QrPQs
    Left = 36
    Top = 124
  end
  object QrPQBIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DataX, OrigemCodi, '
      'SUM(Peso) Peso, SUM(Valor) Valor'
      'FROM pqx '
      'WHERE Insumo=21 '
      'AND CliOrig=-11'
      'AND Empresa=-11'
      'AND Tipo=0 '
      'GROUP BY OrigemCodi'
      'ORDER BY OrigemCodi')
    Left = 112
    Top = 76
    object QrPQBItsDataX: TDateField
      FieldName = 'DataX'
      Required = True
    end
    object QrPQBItsOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
      Required = True
    end
    object QrPQBItsPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPQBItsValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object DsPQBIts: TDataSource
    DataSet = QrPQBIts
    Left = 112
    Top = 124
  end
  object QrBalDif: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Peso) Peso, SUM(Valor) Valor   '
      'FROM pqx  '
      'WHERE Insumo=21 '
      'AND CliOrig=-11'
      'AND Empresa=-11 '
      'AND DataX BETWEEN "0000-00-00" AND "2022-01-31 23:59:59"  ')
    Left = 188
    Top = 76
    object QrBalDifPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrBalDifValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object DsBalDif: TDataSource
    DataSet = QrBalDif
    Left = 188
    Top = 124
  end
  object QrPQB: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPQBCalcFields
    SQL.Strings = (
      'SELECT pqx.CliOrig, pqx.Insumo, pq_.Nome NO_PQ '
      'FROM pqx '
      'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo '
      'GROUP BY pqx.CliOrig, pqx.Insumo '
      'ORDER BY pqx.CliOrig, pqx.Insumo ')
    Left = 264
    Top = 76
    object QrPQBPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrPQBPeso: TFloatField
      FieldKind = fkLookup
      FieldName = 'Peso'
      LookupDataSet = QrPQBIts
      LookupKeyFields = 'OrigemCodi'
      LookupResultField = 'Peso'
      KeyFields = 'Periodo'
      Lookup = True
    end
    object QrPQBValor: TFloatField
      FieldKind = fkLookup
      FieldName = 'Valor'
      LookupDataSet = QrPQBIts
      LookupKeyFields = 'OrigemCodi'
      LookupResultField = 'Valor'
      KeyFields = 'Periodo'
      Lookup = True
    end
    object QrPQBDataX: TDateField
      FieldKind = fkCalculated
      FieldName = 'DataX'
      Calculated = True
    end
  end
  object DsPQB: TDataSource
    DataSet = QrPQB
    Left = 264
    Top = 124
  end
end
