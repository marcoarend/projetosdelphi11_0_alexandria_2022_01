unit PQMCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox,
  dmkEditDateTimePicker, Vcl.ComCtrls, UnEmpresas, UnAppEnums;

type
  TFmPQMCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrPQMCab: TmySQLQuery;
    DsPQMCab: TDataSource;
    QrPQMBxa: TmySQLQuery;
    DsPQMBxa: TDataSource;
    PMBxa: TPopupMenu;
    BxaInclui1: TMenuItem;
    BxaExclui1: TMenuItem;
    BxaAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtBxa: TBitBtn;
    QrSE: TmySQLQuery;
    QrSECodigo: TIntegerField;
    QrSENome: TWideStringField;
    DsSE: TDataSource;
    Label3: TLabel;
    EdSetor: TdmkEditCB;
    CBSetor: TdmkDBLookupComboBox;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    TPDataB: TdmkEditDateTimePicker;
    Label4: TLabel;
    QrPQMCabNO_EMPRESA: TWideStringField;
    QrPQMCabNOMESETOR: TWideStringField;
    QrPQMCabRebLin: TWideStringField;
    QrPQMCabNO_EMITGRU: TWideStringField;
    QrPQMCabNoGraCorCad: TWideStringField;
    QrPQMCabCodigo: TIntegerField;
    QrPQMCabNome: TWideStringField;
    QrPQMCabEmpresa: TIntegerField;
    QrPQMCabSetor: TIntegerField;
    QrPQMCabCustoBaixa: TFloatField;
    QrPQMCabCustoGerou: TFloatField;
    QrPQMCabDataB: TDateField;
    QrPQMCabEmitGru: TIntegerField;
    QrPQMCabVSMovCod: TIntegerField;
    QrPQMCabGraCorCad: TIntegerField;
    QrPQMCabSemiCodEspReb: TIntegerField;
    QrPQMCabSemiTxtEspReb: TWideStringField;
    QrPQMCabLk: TIntegerField;
    QrPQMCabDataCad: TDateField;
    QrPQMCabDataAlt: TDateField;
    QrPQMCabUserCad: TIntegerField;
    QrPQMCabUserAlt: TIntegerField;
    QrPQMCabAlterWeb: TSmallintField;
    QrPQMCabAtivo: TSmallintField;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    Label10: TLabel;
    DBEdit4: TDBEdit;
    Label11: TLabel;
    DBEdit5: TDBEdit;
    BtGer: TBitBtn;
    PMGer: TPopupMenu;
    GerInclui1: TMenuItem;
    GerAltera1: TMenuItem;
    GerExclui1: TMenuItem;
    QrPQMBxaDataX: TDateField;
    QrPQMBxaOrigemCodi: TIntegerField;
    QrPQMBxaOrigemCtrl: TIntegerField;
    QrPQMBxaTipo: TIntegerField;
    QrPQMBxaCliOrig: TIntegerField;
    QrPQMBxaCliDest: TIntegerField;
    QrPQMBxaInsumo: TIntegerField;
    QrPQMBxaPeso: TFloatField;
    QrPQMBxaValor: TFloatField;
    QrPQMBxaRetorno: TSmallintField;
    QrPQMBxaStqMovIts: TIntegerField;
    QrPQMBxaRetQtd: TFloatField;
    QrPQMBxaHowLoad: TSmallintField;
    QrPQMBxaStqCenLoc: TIntegerField;
    QrPQMBxaSdoPeso: TFloatField;
    QrPQMBxaSdoValr: TFloatField;
    QrPQMBxaAcePeso: TFloatField;
    QrPQMBxaAceValr: TFloatField;
    QrPQMBxaDtCorrApo: TDateField;
    QrPQMBxaEmpresa: TIntegerField;
    QrPQMBxaAlterWeb: TSmallintField;
    QrPQMBxaAtivo: TSmallintField;
    QrPQMBxaNOMEPQ: TWideStringField;
    QrPQMBxaGrupoQuimico: TIntegerField;
    QrPQMBxaNOMEGRUPO: TWideStringField;
    GBBxa: TGroupBox;
    DBGBxa: TDBGrid;
    GBGer: TGroupBox;
    DBGGer: TDBGrid;
    QrPQMGer: TmySQLQuery;
    QrPQMGerDataX: TDateField;
    QrPQMGerOrigemCodi: TIntegerField;
    QrPQMGerOrigemCtrl: TIntegerField;
    QrPQMGerTipo: TIntegerField;
    QrPQMGerCliOrig: TIntegerField;
    QrPQMGerCliDest: TIntegerField;
    QrPQMGerInsumo: TIntegerField;
    QrPQMGerPeso: TFloatField;
    QrPQMGerValor: TFloatField;
    QrPQMGerRetorno: TSmallintField;
    QrPQMGerStqMovIts: TIntegerField;
    QrPQMGerRetQtd: TFloatField;
    QrPQMGerHowLoad: TSmallintField;
    QrPQMGerStqCenLoc: TIntegerField;
    QrPQMGerSdoPeso: TFloatField;
    QrPQMGerSdoValr: TFloatField;
    QrPQMGerAcePeso: TFloatField;
    QrPQMGerAceValr: TFloatField;
    QrPQMGerDtCorrApo: TDateField;
    QrPQMGerEmpresa: TIntegerField;
    QrPQMGerAlterWeb: TSmallintField;
    QrPQMGerAtivo: TSmallintField;
    QrPQMGerNOMEPQ: TWideStringField;
    QrPQMGerGrupoQuimico: TIntegerField;
    QrPQMGerNOMEGRUPO: TWideStringField;
    DsPQMGer: TDataSource;
    QrPQMCabQtdeBaixa: TFloatField;
    QrPQMCabQtdeGerou: TFloatField;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    Label13: TLabel;
    DBEdit7: TDBEdit;
    QrPQMGerSigla: TWideStringField;
    QrPQMBxaSigla: TWideStringField;
    Label14: TLabel;
    DBEdit8: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPQMCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPQMCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrPQMCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtBxaClick(Sender: TObject);
    procedure BxaInclui1Click(Sender: TObject);
    procedure BxaExclui1Click(Sender: TObject);
    procedure BxaAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMBxaPopup(Sender: TObject);
    procedure QrPQMCabBeforeClose(DataSet: TDataSet);
    procedure GerInclui1Click(Sender: TObject);
    procedure GerAltera1Click(Sender: TObject);
    procedure GerExclui1Click(Sender: TObject);
    procedure PMGerPopup(Sender: TObject);
    procedure BtGerClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraPQMBxa(SQLType: TSQLType);
    procedure MostraPQMGer(SQLType: TSQLType);
    procedure HabilitaEmpresa(SQLType: TSQLType);


  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure AtualizaCusto();
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPQMBxa(OrigemCtrl: Integer);
    procedure ReopenPQMGer(OrigemCtrl: Integer);

  end;

var
  FmPQMCab: TFmPQMCab;
const
  FFormatFloat = '00000';

implementation

uses
  UnMyObjects, Module, MyDBCheck, DmkDAC_PF, PQMBxa, ModuleGeral, PQMGer,
  UnPQ_PF, UnVS_PF, PQx;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPQMCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPQMCab.MostraPQMBxa(SQLType: TSQLType);
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQMCabDataB.Value) then Exit;
  // 2023-09-09
  if DBCheck.CriaFm(TFmPQMBxa, FmPQMBxa, afmoNegarComAviso) then
  begin
    FmPQMBxa.ImgTipo.SQLType := SQLType;
    FmPQMBxa.FQrCab   := QrPQMCab;
    FmPQMBxa.FDsCab   := DsPQMCab;
    FmPQMBxa.FQrIts   := QrPQMBxa;
    FmPQMBxa.FCodigo  := QrPQMCabCodigo.Value;
    FmPQMBxa.FDataB   := QrPQMCabDataB.Value;
    FmPQMBxa.FEmpresa := QrPQMCabEmpresa.Value;
    FmPQMBxa.EdEmpresa.ValueVariant := QrPQMCabEmpresa.Value;
    FmPQMBxa.CBEmpresa.KeyValue     := QrPQMCabEmpresa.Value;
    FmPQMBxa.EdCI.ValueVariant      := QrPQMCabEmpresa.Value;
    FmPQMBxa.CBCI.KeyValue          := QrPQMCabEmpresa.Value;
    if SQLType = stIns then
     //
    else
    begin
      FmPQMBxa.EdOrigemCtrl.ValueVariant := QrPQMBxaOrigemCtrl.Value;
      //
    end;
    FmPQMBxa.ShowModal;
    FmPQMBxa.Destroy;
  end;
end;

procedure TFmPQMCab.MostraPQMGer(SQLType: TSQLType);
var
  Peso, Valor: Double;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQMCabDataB.Value) then Exit;
  // 2023-09-09
  if DBCheck.CriaFm(TFmPQMGer, FmPQMGer, afmoNegarComAviso) then
  begin
    FmPQMGer.ImgTipo.SQLType := SQLType;
    FmPQMGer.FQrCab   := QrPQMCab;
    FmPQMGer.FDsCab   := DsPQMCab;
    FmPQMGer.FQrIts   := QrPQMGer;
    FmPQMGer.FCodigo  := QrPQMCabCodigo.Value;
    FmPQMGer.FDataB   := QrPQMCabDataB.Value;
    FmPQMGer.FEmpresa := QrPQMCabEmpresa.Value;
    FmPQMGer.EdEmpresa.ValueVariant := QrPQMCabEmpresa.Value;
    FmPQMGer.CBEmpresa.KeyValue     := QrPQMCabEmpresa.Value;
    FmPQMGer.EdCI.ValueVariant      := QrPQMCabEmpresa.Value;
    FmPQMGer.CBCI.KeyValue          := QrPQMCabEmpresa.Value;
    if SQLType = stIns then
    begin
      if QrPQMCabQtdeGerou.Value < QrPQMCabQtdeBaixa.Value then
        Peso := QrPQMCabQtdeBaixa.Value - QrPQMCabQtdeGerou.Value
      else
        Peso := 0;
      //
      if QrPQMCabCustoGerou.Value < QrPQMCabCustoBaixa.Value then
        Valor := QrPQMCabCustoBaixa.Value - QrPQMCabCustoGerou.Value
      else
        Valor := 0;
      //
      FmPQMGer.EdPeso.ValueVariant := Peso;
      FmPQMGer.EdValor.ValueVariant := Valor;
    end else
    begin
      FmPQMGer.EdOrigemCtrl.ValueVariant := QrPQMGerOrigemCtrl.Value;
      //
    end;
    FmPQMGer.ShowModal;
    FmPQMGer.Destroy;
  end;
end;

procedure TFmPQMCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrPQMCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrPQMCab, QrPQMBxa);
end;

procedure TFmPQMCab.PMGerPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(GerInclui1, QrPQMCab);
  MyObjects.HabilitaMenuItemItsUpd(GerAltera1, QrPQMGer);
  MyObjects.HabilitaMenuItemItsDel(GerExclui1, QrPQMGer);
end;

procedure TFmPQMCab.PMBxaPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(BxaInclui1, QrPQMCab);
  MyObjects.HabilitaMenuItemItsUpd(BxaAltera1, QrPQMBxa);
  MyObjects.HabilitaMenuItemItsDel(BxaExclui1, QrPQMBxa);
end;

procedure TFmPQMCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPQMCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPQMCab.DefParams;
begin
  VAR_GOTOTABELA := 'pqm';
  VAR_GOTOMYSQLTABLE := QrPQMCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('lse.Nome NOMESETOR, reb.Linhas RebLin, ');
  VAR_SQLx.Add('emg.Nome NO_EMITGRU, gcc.Nome NoGraCorCad, pqm.*');
  VAR_SQLx.Add('FROM pqm pqm');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=pqm.Empresa');
  VAR_SQLx.Add('LEFT JOIN listasetores lse ON lse.Codigo=pqm.Setor');
  VAR_SQLx.Add('LEFT JOIN emitgru emg ON emg.Codigo=pqm.EmitGru');
  VAR_SQLx.Add('LEFT JOIN gracorcad gcc ON gcc.Codigo=pqm.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN espessuras reb ON reb.Codigo=pqm.SemiCodEspReb');
  VAR_SQLx.Add('WHERE pqm.Codigo > 0');
    //
  VAR_SQLx.Add('AND pqm.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmPQMCab.BxaAltera1Click(Sender: TObject);
begin
  MostraPQMBxa(stUpd);
end;

procedure TFmPQMCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQMCabDataB.Value) then Exit;
  // 2023-09-09
end;

procedure TFmPQMCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPQMCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPQMCab.BxaExclui1Click(Sender: TObject);
var
  OrigemCodi, OrigemCtrl, Tipo, Insumo, CliInt, Empresa: Integer;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQMCabDataB.Value) then Exit;
  // 2023-09-09
  if Dmod.ImpedePeloRetornoPQ(
  QrPQMBxaOrigemCodi.Value, QrPQMBxaOrigemCtrl.Value, QrPQMBxaTipo.Value, True) then
    Exit;
  //if Pertence then Exit;
  if Geral.MB_Pergunta('O item (insumo) "' + QrPQMBxaNOMEPQ.Value +
  '" ser� excluido da mistura, mas o(s) insumo(s) resulte(s) n�o!' + sLineBreak +
  'Confirma a exclus�o do insumo consumido "'+ QrPQMBxaNOMEPQ.Value +
  '" desta mistura?') = ID_YES then
  begin
    Insumo     := QrPQMBxaInsumo.Value;
    CliInt     := QrPQMBxaCliOrig.Value;
    Empresa    := QrPQMBxaEmpresa.Value;
    OrigemCodi := QrPQMBxaOrigemCodi.Value;
    OrigemCtrl := QrPQMBxaOrigemCtrl.Value;
    Tipo       := VAR_FATID_0130;
    PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OrigemCodi, OrigemCtrl, Tipo, True, Empresa);
    //
    AtualizaCusto();
    //
    //OrigemCtrl :=
    UMyMod.ProximoRegistro(QrPQMBxa, 'OrigemCtrl', QrPQMBxaOrigemCtrl.Value);
    //QrPQMBxa.Locate('OrigemCtrl', OrigemCtrl);
  end;
end;

procedure TFmPQMCab.ReopenPQMBxa(OrigemCtrl: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQMBxa, Dmod.MyDB, [
  'SELECT DISTINCT med.Sigla, pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  ',
  'pqg.Nome NOMEGRUPO ',
  'FROM pq pq_ ',
  'LEFT JOIN pqcli pci ON pci.PQ=pq_.Codigo',
  'LEFT JOIN pqx   pqx ON pq_.Codigo=pqx.Insumo ',
  'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pq_.Codigo',
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0130),
  'AND pqx.OrigemCodi=' + Geral.FF0(QrPQMCabCodigo.Value),
  '']);
  //
  QrPQMBxa.Locate('OrigemCtrl', OrigemCtrl, []);
end;


procedure TFmPQMCab.ReopenPQMGer(OrigemCtrl: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQMGer, Dmod.MyDB, [
  'SELECT DISTINCT med.Sigla, pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  ',
  'pqg.Nome NOMEGRUPO ',
  'FROM pq pq_ ',
  'LEFT JOIN pqcli pci ON pci.PQ=pq_.Codigo',
  'LEFT JOIN pqx   pqx ON pq_.Codigo=pqx.Insumo ',
  'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pq_.Codigo',
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0030),
  'AND pqx.OrigemCodi=' + Geral.FF0(QrPQMCabCodigo.Value),
  '']);
  //
  QrPQMBxa.Locate('OrigemCtrl', OrigemCtrl, []);
end;

procedure TFmPQMCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPQMCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPQMCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPQMCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPQMCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPQMCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQMCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPQMCabCodigo.Value;
  Close;
end;

procedure TFmPQMCab.BxaInclui1Click(Sender: TObject);
begin
  MostraPQMBxa(stIns);
end;

procedure TFmPQMCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
  Habilita: Boolean;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQMCabDataB.Value) then Exit;
  // 2023-09-09
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrPQMCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'pqm');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrPQMCabEmpresa.Value);
  EdEmpresa.ValueVariant    := Empresa;
  CBEmpresa.KeyValue        := Empresa;
end;

procedure TFmPQMCab.BtConfirmaClick(Sender: TObject);
var
  Nome, DataB(*, SemiTxtEspReb*): String;
  Codigo, Empresa, Setor, VSMovCod(*, EmitGru, GraCorCad, SemiCodEspReb*): Integer;
  //QtdeBaixa, QtdeGerou, CustoBaixa, CustoGerou: Double;
  SQLType: TSQLType;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(TPDataB.Date) then Exit;
  // 2023-09-09
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Setor          := EdSetor.ValueVariant;
  //QtdeBaixa      := ;
  //QtdeGerou      := ;
  //CustoBaixa     := ;
  //CustoGerou     := ;
  DataB          := Geral.FDT(TPDataB.Date, 1);
  //EmitGru        := ;
  VSMovCod       := QrPQMCabVSMovCod.Value;
  //GraCorCad      := ;
  //SemiCodEspReb  := ;
  //SemiTxtEspReb  := ;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
  //if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  VSMovCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, VSMovCod);
  Codigo   := UMyMod.BPGS1I32('pqm', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqm', False, [
  'Nome', 'Empresa', 'Setor',
  'DataB', 'VSMovCod'
  (*'QtdeBaixa', 'QtdeGerou', 'CustoBaixa', 'CustoGerou',
  'EmitGru', 'VSMovCod', 'GraCorCad',
  'SemiCodEspReb', 'SemiTxtEspReb'*)], [
  'Codigo'], [
  Nome, Empresa, Setor,
  DataB, VSMovCod
  (*QtdeBaixa, QtdeGerou, CustoBaixa, CustoGerou,
  EmitGru, GraCorCad,
  SemiCodEspReb, SemiTxtEspReb*)], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_PF.InsereVSMovCab(VSMovCod, emidMixInsum, Codigo);
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmPQMCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'pqm', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqm', 'Codigo');
end;

procedure TFmPQMCab.BtGerClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMGer, BtGer);
end;

procedure TFmPQMCab.BtBxaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBxa, BtBxa);
end;

procedure TFmPQMCab.AtualizaCusto();
const
  SQLType = stUpd;
var
  Qry: TmySQLQuery;
  Codigo: Integer;
  QtdeBaixa, QtdeGerou, CustoBaixa, CustoGerou: Double;
begin
  Codigo := QrPQMCabCodigo.Value;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT -SUM(pqx.Peso) QTDE, -SUM(pqx.Valor) CUSTO ',
    'FROM pqx pqx ',
    'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0130),
    'AND pqx.OrigemCodi=' + Geral.FF0(Codigo),
    '']);
    QtdeBaixa  := Qry.FieldByName('QTDE').AsFloat;
    CustoBaixa := Qry.FieldByName('CUSTO').AsFloat;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(pqx.Peso) QTDE, SUM(pqx.Valor) CUSTO ',
    'FROM pqx pqx ',
    'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0030),
    'AND pqx.OrigemCodi=' + Geral.FF0(Codigo),
    '']);
    QtdeGerou  := Qry.FieldByName('QTDE').AsFloat;
    CustoGerou := Qry.FieldByName('CUSTO').AsFloat;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqm', False, [
    'QtdeBaixa', 'QtdeGerou',
    'CustoBaixa', 'CustoGerou'], [
    'Codigo'], [
    QtdeBaixa, QtdeGerou,
    CustoBaixa, CustoGerou], [
    Codigo], True) then
      LocCod(Codigo, Codigo);
  finally
    Qry.Free;
  end;
end;

procedure TFmPQMCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmPQMCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBGer.Align     := alClient;
  CriaOForm;
  FSeq := 0;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //
  TPDataB.Date      := Date;
  //UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSE, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrEmitGru, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrRebaixe, Dmod.MyDB);
  CriaOForm;
end;

procedure TFmPQMCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPQMCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPQMCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPQMCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPQMCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPQMCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQMCab.QrPQMCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPQMCab.QrPQMCabAfterScroll(DataSet: TDataSet);
begin
  ReopenPQMBxa(0);
  ReopenPQMGer(0);
end;

procedure TFmPQMCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrPQMCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmPQMCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPQMCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'pqm', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPQMCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQMCab.GerAltera1Click(Sender: TObject);
begin
  MostraPQMGer(stUpd);
end;

procedure TFmPQMCab.GerExclui1Click(Sender: TObject);
var
  OrigemCodi, OrigemCtrl, Tipo, Insumo, CliInt, Empresa: Integer;
begin
  // 2023-09-09
  if UnPQx.ImpedePeloBalanco(QrPQMCabDataB.Value) then Exit;
  // 2023-09-09
(*
  if Dmod.ImpedePeloRetornoPQ(
  QrPQMGerOrigemCodi.Value, QrPQMGerOrigemCtrl.Value, QrPQMGerTipo.Value, True) then
    Exit;
*)
  //if Pertence then Exit;
  if Geral.MB_Pergunta('O item (insumo) "' + QrPQMGerNOMEPQ.Value +
  '" gerado ser� exclu�do da mistura, mas o(s) insumo(s) consumidos(s) n�o!' + sLineBreak +
  'Confirma a exclus�o do insumo gerado "'+ QrPQMGerNOMEPQ.Value +
  '" desta mistura?') = ID_YES then
  begin
    Insumo     := QrPQMGerInsumo.Value;
    CliInt     := QrPQMGerCliOrig.Value;
    Empresa    := QrPQMGerEmpresa.Value;
    OrigemCodi := QrPQMGerOrigemCodi.Value;
    OrigemCtrl := QrPQMGerOrigemCtrl.Value;
    Tipo       := VAR_FATID_0030;
    PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OrigemCodi, OrigemCtrl, Tipo, True, Empresa);
    //
    AtualizaCusto();
    //
    //OrigemCtrl :=
    UMyMod.ProximoRegistro(QrPQMGer, 'OrigemCtrl', QrPQMGerOrigemCtrl.Value);
    //QrPQMGer.Locate('OrigemCtrl', OrigemCtrl);
  end;
end;

procedure TFmPQMCab.GerInclui1Click(Sender: TObject);
begin
  MostraPQMGer(stIns);
end;

procedure TFmPQMCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmPQMCab.CabInclui1Click(Sender: TObject);
begin
{


procedure TFmVSOpeCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  EdControle.ValueVariant   := 0;
  EdGraGruX.ValueVariant    := 0;
  CBGraGruX.KeyValue        := 0;
  EdCustoMOTot.ValueVariant := 0;
  EdPecas.ValueVariant      := 0;
  EdPesoKg.ValueVariant     := 0;
  EdMovimTwn.ValueVariant   := 0;
  EdFornecMO.ValueVariant   := 0;
  CBFornecMO.KeyValue       := 0;
  EdObserv.ValueVariant     := '';
  //
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSOpeCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsopecab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
  VS_PF.ReopenPedItsXXX(QrVSPedIts, 0, 0);
  LaPedItsLib.Enabled       := True;
  EdPedItsLib.Enabled       := True;
  CBPedItsLib.Enabled       := True;
  if EdEmpresa.ValueVariant > 0 then
    TPData.SetFocus;
end;

}

  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrPQMCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'pqm');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  TPDataB.Date := DmodG.ObtemAgora();
end;

procedure TFmPQMCab.QrPQMCabBeforeClose(
  DataSet: TDataSet);
begin
  QrPQMBxa.Close;
  QrPQMGer.Close;
end;

procedure TFmPQMCab.QrPQMCabBeforeOpen(DataSet: TDataSet);
begin
  QrPQMCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

