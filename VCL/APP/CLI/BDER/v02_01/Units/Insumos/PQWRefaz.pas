unit PQWRefaz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, UnProjGroup_Consts;

type
  TFmPQWRefaz = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPQs: TMySQLQuery;
    DsPQs: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    PB1: TProgressBar;
    PB2: TProgressBar;
    QrPQBIts: TMySQLQuery;
    DsPQBIts: TDataSource;
    QrBalDif: TMySQLQuery;
    DsBalDif: TDataSource;
    QrPQsInsumo: TIntegerField;
    QrPQsNO_PQ: TWideStringField;
    QrPQBItsDataX: TDateField;
    QrPQBItsOrigemCodi: TIntegerField;
    QrPQBItsPeso: TFloatField;
    QrPQBItsValor: TFloatField;
    QrBalDifPeso: TFloatField;
    QrBalDifValor: TFloatField;
    QrPQsCliOrig: TIntegerField;
    QrPQsEmpresa: TIntegerField;
    QrPQB: TMySQLQuery;
    DsPQB: TDataSource;
    QrPQBPeriodo: TIntegerField;
    QrPQBPeso: TFloatField;
    QrPQBValor: TFloatField;
    QrPQBDataX: TDateField;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    LaFim: TLabel;
    LaIni: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrPQBCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenPQs();
    procedure ReopenPQBIts();
    procedure ReopenBalDif(DataIni, DataFim: TDateTime);
    procedure IncluiDiferencadeResgate(FatID: Integer);
    procedure InserePQX(DataX: String; CliOrig, CliDest, Insumo: Integer;
              Peso, Valor: Double; OrigemCodi, OrigemCtrl, Tipo: Integer; Unitario:
              Boolean; DtCorrApo: String; _Empresa: Integer; HowLoad: Integer);
  public
    { Public declarations }
  end;

  var
  FmPQWRefaz: TFmPQWRefaz;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkProcFunc, UMySQLModule, UnPQ_PF,
  ModuleGeral;

{$R *.DFM}

procedure TFmPQWRefaz.BtOKClick(Sender: TObject);
var
  NO_PQ: String;
  DataIni, DataFim: TDateTime;
begin
  LaIni.Caption := Geral.FDT(DModG.ObtemAgora(), 0);
  UnDmkDAC_PF.ExecutaDB(DMod.MyDB, 'DELETE FROM pqx WHERE HowLoad=5');
  UnDmkDAC_PF.ExecutaDB(DMod.MyDB, 'DELETE FROM pqx WHERE Tipo<0');
  //
  ReopenPQs();
  //
  QrPQs.First;
  while not QrPQs.Eof do
  begin
    MyObjects.UpdPB(PB1, nil, nil);
    NO_PQ := QrPQsNO_PQ.Value;
    ReopenPQBIts();
    //
    DataIni := 0;
    while not QrPQB.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True,
      'Corrigindo balan�o: ' + NO_PQ + ' > ' + Geral.FDT(QrPQBDataX.Value, 1));
      DataFim := QrPQBDataX.Value - 1;
      ReopenBalDif(DataIni, DataFim);
      QrBalDif.First;
      //
      if (QrPQBPeso.Value <> QrBalDifPeso.Value) or (QrPQBValor.Value <> QrBalDifValor.Value) then
      begin
        // peso iformado no balan�o � menor que o estoque escritural (falta produto)
        if (QrPQBPeso.Value > QrBalDifPeso.Value)
        or (QrPQBValor.Value > QrBalDifValor.Value) then
        begin
          // Incluir diferen�a no balan�o de resgate com FatID = 20...
          IncluiDiferencadeResgate(VAR_FATID_0020);
          // ... e dar baixa para ficar o peso informado pelo usu�rio.
          //
        end else
        begin
          // Incluir diferen�a no balan�o de resgate...
          IncluiDiferencadeResgate(VAR_FATID_0120);
          //
        end;
      end;
      //
      // no final...
      DataIni := QrPQBDataX.Value;
      QrPQB.Next;
    end;
    QrPQs.Next;
  end;
  LaFim.Caption := Geral.FDT(DModG.ObtemAgora(), 0);
end;

procedure TFmPQWRefaz.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQWRefaz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQWRefaz.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPQWRefaz.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQWRefaz.IncluiDiferencadeResgate(FatID: Integer);
const
  sProcName = 'TFmPQWRefaz.IncluiDiferencadeResgate()';
  HowLoad = Integer(TPQEHowLoad.pqehlPQWRefaz); // 5
  DtCorrApo = '0000-00-00';
  Tipo = VAR_FATID_0000;
  Unitario = False;
  ide_serie = 0;
  ide_nNF = 0;
  xLote = '';
  dFab = '0000-00-00';
  dVal = '0000-00-00';
var
  DataX: String;
  CliOrig, CliDest, Insumo: Integer;
  Peso, Valor: Double;
  OrigemCodi, Empresa: Integer;
  OrigemCtrl: Integer;
  //
  Controle, CliInt, OriCodi, Codigo: Integer;
begin
  DataX       := Geral.FDT(QrPQBDataX.Value, 1);
  CliOrig     := QrPQsCliOrig.Value;
  CliDest     := QrPQsCliOrig.Value;
  Insumo      := QrPQsInsumo.Value;
  Peso        := QrPQBPeso.Value - QrBalDifPeso.Value;
  Valor       := QrPQBValor.Value - QrBalDifValor.Value;
  OrigemCodi  := QrPQBPeriodo.Value;
  OrigemCtrl  := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta');
  Empresa     := QrPQsEmpresa.Value;
  //
  CliInt      := CliOrig;
  OriCodi     := OrigemCodi;
  Codigo      := OrigemCodi;
  //
  case FatID of
    VAR_FATID_0020:
    begin
      Controle := UMyMod.BPGS1I32('pqbadx', 'Controle', '', '', tsPos, stIns, Controle);
      (*
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqbadx', False, [
      'Codigo', 'Empresa', 'CliInt',
      'Insumo', 'Peso', 'Valor',
      'ide_serie', 'ide_nNF', 'DtCorrApo',
      'xLote', 'dFab', 'dVal'], [
      'Controle'], [
      Codigo, Empresa, CliInt,
      Insumo, Peso, Valor,
      ide_serie, ide_nNF, DtCorrApo,
      xLote, dFab, dVal], [
      Controle], True) then
      *)
      begin
        // Lan�amento a positivo para controlar estoque de NF (e lote)
        InserePQx(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
          (*OriCtrl*)Controle, (*OriTipo*)FatID, Unitario, DtCorrApo, Empresa, HowLoad);
        // Lan�amento para netralizar o lan�amento a positivo
        InserePQx(DataX, CliOrig, CliDest, Insumo, -Peso, -Valor, OrigemCodi,
        OrigemCtrl, Tipo, Unitario, DtCorrApo, Empresa, HowLoad);
      end;
    end;
    VAR_FATID_0120:
    begin
      Controle := UMyMod.BPGS1I32('pqbbxa', 'Controle', '', '', tsPos, stIns, Controle);
      (*
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqbbxa', False, [
      'Codigo', 'Empresa', 'CliInt',
      'Insumo', 'Peso', 'Valor',
      'DtCorrApo'], [
      'Controle'], [
      Codigo, Empresa, CliInt,
      Insumo, Peso, Valor,
      DtCorrApo], [
      Controle], True) then
      *)
      begin
        // Lan�amento a negativo para controlar estoque de NF (e lote)
        InserePQx(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
          (*OriCtrl*)Controle, (*OriTipo*)FatID, Unitario, DtCorrApo, Empresa, HowLoad);
        // Lan�amento para netralizar o lan�amento a negativo
        InserePQx(DataX, CliOrig, CliDest, Insumo, -Peso, -Valor, OrigemCodi,
        OrigemCtrl, Tipo, Unitario, DtCorrApo, Empresa, HowLoad);
      end;
    end;
    else
      Geral.MB_Erro('"FatID" n�o implementado em ' + sProcName);
  end;
end;

procedure TFmPQWRefaz.InserePQX(DataX: String; CliOrig, CliDest, Insumo: Integer;
  Peso, Valor: Double; OrigemCodi, OrigemCtrl, Tipo: Integer; Unitario:
  Boolean; DtCorrApo: String; _Empresa: Integer; HowLoad: Integer);
var
  Empresa: Integer;
begin
  //Result  := False;
  Empresa := _Empresa;
  if Empresa = 0 then
  begin
    Empresa := -11;
    Geral.MB_Erro('Empresa n�o definida! Ser� considerado -11!');
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_INS_TAB_PQX, False, [
  'DataX', 'CliOrig', 'CliDest',
  'Insumo', 'Peso', 'Valor',(*,
  'Retorno', 'StqMovIts', 'RetQtd',*)
  'HowLoad' (*'StqCenLoc', 'SdoPeso',
  'SdoValr', 'AcePeso', 'AceValr'*),
  'DtCorrApo', 'Empresa'], [
  'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
  DataX, CliOrig, CliDest,
  Insumo, Peso, Valor,(*,
  Retorno, StqMovIts, RetQtd,*)
  HowLoad (*StqCenLoc, SdoPeso,
  SdoValr, AcePeso, AceValr*),
  DtCorrApo, Empresa], [
  OrigemCodi, OrigemCtrl, Tipo], False) then
  begin
    //AtualizaSdoPQx(Query, OrigemCodi, OrigemCtrl, Tipo);
    //Result := True;
  end;
end;

procedure TFmPQWRefaz.QrPQBCalcFields(DataSet: TDataSet);
begin
  QrPQBDataX.Value := Geral.PTD(QrPQBPeriodo.Value, 1);
end;

procedure TFmPQWRefaz.ReopenBalDif(DataIni, DataFim: TDateTime);
var
  SQLPeriodo: String;
begin
  SQLPeriodo := dmkPF.SQL_Periodo('AND DataX ', DataIni, DataFim, True, True);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrBalDif, Dmod.MyDB, [
  'SELECT SUM(Peso) Peso, SUM(Valor) Valor   ',
  'FROM pqx  ',
  'WHERE Insumo=' + Geral.FF0(QrPQsInsumo.Value),
  'AND CliOrig=' + Geral.FF0(QrPQsCliOrig.Value),
  'AND Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
  SQLPeriodo,
  '']);
end;

procedure TFmPQWRefaz.ReopenPQBIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQBIts, Dmod.MyDB, [
  'SELECT DataX, OrigemCodi, ',
  'SUM(Peso) Peso, SUM(Valor) Valor ',
  'FROM pqx ',
  'WHERE Insumo=' + Geral.FF0(QrPQsInsumo.Value),
  'AND CliOrig=' + Geral.FF0(QrPQsCliOrig.Value),
  'AND Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
  'AND Tipo=0 ',
  'GROUP BY OrigemCodi ',
  'ORDER BY OrigemCodi ',
  '']);
  //
  // Deve ser depois!
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQB, Dmod.MyDB, [
  'SELECT Periodo',
  'FROM balancos',
  'ORDER BY Periodo',
  '']);
  PB2.Position := 0;
  PB2.Max := QrPQB.RecordCount;
end;

procedure TFmPQWRefaz.ReopenPQs();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQs, Dmod.MyDB, [
  'SELECT pqx.Empresa, pqx.CliOrig, pqx.Insumo, pq_.Nome NO_PQ ',
  'FROM pqx ',
  'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo ',
  // teste aqui!!!!
  //'WHERE pq_.Codigo=21', // Testar primeiro Dermascal U Ecoservice
  'WHERE pq_.Codigo>0',
  'GROUP BY pqx.CliOrig, pqx.Insumo ',
  'ORDER BY pqx.CliOrig, pqx.Insumo ',
  '']);
  //
  PB1.Position := 0;
  PB1.Max := QrPQs.RecordCount;
end;

end.
