unit PQMGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  UnProjGroup_Consts;

type
  TFmPQMGer = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdOrigemCtrl: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrSaldo: TmySQLQuery;
    QrSaldoPeso: TFloatField;
    QrSaldoValor: TFloatField;
    QrSaldoCUSTO: TFloatField;
    QrSaldoCI: TIntegerField;
    QrSaldoPQ: TIntegerField;
    DsSaldo: TDataSource;
    QrPQ: TmySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsPQ: TDataSource;
    QrCI: TmySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    DsCI: TDataSource;
    Label12: TLabel;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdInsumo: TdmkEditCB;
    CBInsumo: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdPeso: TdmkEdit;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    Label22: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdValor: TdmkEdit;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenIts(OrigemCtrl: Integer);
  public
    { Public declarations }
    FDataB: TDateTime;
    FEmpresa, FCodigo: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmPQMGer: TFmPQMGer;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral, UnPQ_PF, PQMCab, PQx;

{$R *.DFM}

procedure TFmPQMGer.BtOKClick(Sender: TObject);
const
  HowLoad = 0;
  //
  ide_serie = 0;
  ide_nNF = 0;
  xLote = '';
  dFab = '0000-00-00';
  dVal = '0000-00-00';
var
  DataX, DtCorrApo: String;
  //Retorno, StqMovIts, HowLoad, StqCenLoc,
  OrigemCodi, OrigemCtrl, Tipo, CliOrig, CliDest, Insumo, Empresa: Integer;
  //RetQtd, SdoPeso, SdoValr, AcePeso, AceValr
  Peso, Valor: Double;
  SQLType: TSQLType;
  //
  Qry: TmySQLQuery;
begin
  //  Result   := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    DataX      := Geral.FDT(FDataB, 1);
    //
    if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
    //
    CliOrig    := EdCI.ValueVariant;
    CliDest    := EdCI.ValueVariant;
    Insumo     := EdInsumo.ValueVariant;
    Peso       := EdPeso.ValueVariant;
    Valor      := EdValor.ValueVariant;
    OrigemCodi := FCodigo;
    OrigemCtrl := EdOrigemCtrl.ValueVariant;
    Tipo       := VAR_FATID_0030;
    DtCorrApo  := Geral.FDT(Int(TPDtCorrApo.Date), 1);
    //
    if MyObjects.FIC(Peso <= 0, EdPeso, 'Quantidade inv�lida!') then Exit;
    //
    OrigemCtrl := DModG.BuscaProximoInteiro(CO_FLD_TAB_PQX, 'OrigemCtrl', 'Tipo', VAR_FATID_0030);
    //
    if PQ_PF.InserePQx_Inn(Qry, DataX, CliOrig, CliDest, Insumo, Peso, Valor,
    HowLoad, OrigemCodi, OrigemCtrl, Tipo, DtCorrApo, Empresa,
    ide_serie, ide_nNF, xLote, dFab, dVal) then
    begin
      UnPQx.AtualizaEstoquePQ(CliDest, Insumo, Empresa, aeMsg, '');
      FmPQMCab.ReopenPQMGer(OrigemCtrl);
      FmPQMCab.AtualizaCusto();
      if CkContinuar.Checked then
      begin
        EdPeso.ValueVariant    := 0;
        EdValor.ValueVariant   := 0;
        EdInsumo.ValueVariant  := 0;
        CBInsumo.KeyValue      := NULL;
        EdInsumo.SetFocus;
      end else
        Close;
    end;
  finally
    Qry.Free;
  end;
      //
{
const
  Unitario = True;
var
  OrigemCtrl: Integer;
  KG, RS, SF: Double;
  //
  DataX, DtCorrApo: String;
  OrigemCodi, Tipo, CliOrig, CliDest, Insumo, Empresa: Integer;
  Peso, Valor: Double;
begin
  if MyObjects.FIC(QrSaldo.RecordCount = 0, nil,
  'Insumo / cliente interno n�o definido!') then
    Exit;
  SF := EdSaldoFut.ValueVariant;
  //
  if MyObjects.FIC((SF < 0) and (EdCI.ValueVariant < 0), EdPesoAdd,
  'Quantidade insuficiente no estoque!') then
    Exit;
  KG      := EdPesoAdd.ValueVariant;
  RS      := KG * QrSaldoCUSTO.Value;
  CliOrig := EdCI.ValueVariant;
  CliDest := EdCI.ValueVariant;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  OrigemCtrl := DModG.BuscaProximoInteiro(CO_FLD_TAB_PQX, 'OrigemCtrl', 'Tipo', VAR_FATID_0130);
  //
  DataX      := Geral.FDT(FDataB, 1);
  OrigemCodi := FCodigo;
  Tipo       := VAR_FATID_0130;
  CliOrig    := EdCI.ValueVariant;
  CliDest    := EdCI.ValueVariant;
  Insumo     := QrSaldoPQ.Value;
  Peso       := -KG;
  Valor      := -RS;
  DtCorrApo  := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
  //
  PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OrigemCodi,
    OrigemCtrl, Tipo, Unitario, DtCorrApo, Empresa);
  //
  UnPQx.AtualizaEstoquePQ(QrSaldoCI.Value, QrSaldoPQ.Value, Empresa, aeMsg, '');
  QrSaldo.Close;
  UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
  FmPQMCab.ReopenPQMBxa(OrigemCtrl);
  FmPQMCab.AtualizaCusto();
  EdPesoAdd.ValueVariant := 0;
  EdPQ.ValueVariant := 0;
  CBPQ.KeyValue := NULL;
  EdPQ.SetFocus;
}
end;

procedure TFmPQMGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQMGer.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmPQMGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
end;

procedure TFmPQMGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQMGer.ReopenIts(OrigemCtrl: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if OrigemCtrl <> 0 then
      FQrIts.Locate('OrigemCtrl', OrigemCtrl, []);
  end;
end;

{
object QrEntidades: TmySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT Codigo, CodUsu, Nome '
    'FROM enticargos'
    'ORDER BY Nome')
  Left = 216
  Top = 44
  object QrEntidadesCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrEntidadesCodUsu: TIntegerField
    FieldName = 'CodUsu'
  end
  object QrEntidadesNome: TWideStringField
    FieldName = 'Nome'
    Size = 30
  end
end
object DsEntidades: TDataSource
  DataSet = QrEntidades
  Left = 216
  Top = 92
end
}
end.
