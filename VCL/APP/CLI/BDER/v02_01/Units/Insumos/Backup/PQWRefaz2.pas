unit PQWRefaz2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, UnProjGroup_Consts,
  mySQLDirectQuery, UMySQLDB;

type
  TFmPQWRefaz2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPQs: TMySQLQuery;
    DsPQs: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    PB2: TProgressBar;
    PB3: TProgressBar;
    QrPQBIts: TMySQLQuery;
    DsPQBIts: TDataSource;
    QrBalDif: TMySQLQuery;
    DsBalDif: TDataSource;
    QrPQsInsumo: TIntegerField;
    QrPQsNO_PQ: TWideStringField;
    QrPQBItsDataX: TDateField;
    QrPQBItsOrigemCodi: TIntegerField;
    QrPQBItsPeso: TFloatField;
    QrPQBItsValor: TFloatField;
    QrBalDifPeso: TFloatField;
    QrBalDifValor: TFloatField;
    QrPQsCliOrig: TIntegerField;
    QrPQsEmpresa: TIntegerField;
    QrPQB: TMySQLQuery;
    DsPQB: TDataSource;
    QrPQBPeriodo: TIntegerField;
    QrPQBPeso: TFloatField;
    QrPQBValor: TFloatField;
    QrPQBDataX: TDateField;
    Label1: TLabel;
    Label2: TLabel;
    LaFim: TLabel;
    LaIni: TLabel;
    PB1: TProgressBar;
    QrPer: TMySQLQuery;
    QrPerPeriodo: TIntegerField;
    LaTit1: TLabel;
    LaTit2: TLabel;
    CkTmpPer: TCheckBox;
    QrEmpresas: TMySQLQuery;
    QrEmpresasEmpresa: TIntegerField;
    QrEmpresasCliOrig: TIntegerField;
    QrEmpresasITENS: TLargeintField;
    Label3: TLabel;
    Label4: TLabel;
    LaEmpresa: TLabel;
    LaCliOrig: TLabel;
    DqArr1: TMySQLDirectQuery;
    QrMMP: TMySQLQuery;
    QrMMPMinPer: TIntegerField;
    QrMMPMaxPer: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrPQBCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenPQs();
    procedure ReopenPQBIts();
    procedure ReopenBalDif(DataIni, DataFim: TDateTime);
    procedure IncluiDiferencadeResgate(FatID: Integer);
    procedure InserePQX(DataX: String; CliOrig, CliDest, Insumo: Integer;
              Peso, Valor: Double; OrigemCodi, OrigemCtrl, Tipo: Integer; Unitario:
              Boolean; DtCorrApo: String; _Empresa: Integer; HowLoad: Integer);
  public
    { Public declarations }
  end;

  var
  FmPQWRefaz2: TFmPQWRefaz2;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkProcFunc, UMySQLModule, UnPQ_PF,
  ModuleGeral;

{$R *.DFM}

procedure TFmPQWRefaz2.BtOKClick(Sender: TObject);
  procedure LimpaComps();
  begin
    PB1.Position := 0;
    PB2.Position := 0;
    PB3.Position := 0;
    MyObjects.Informa2(LaTit1, LaTit2, False,  '...');
    MyObjects.Informa2(LaAviso1, LaAviso2, False,  '...');
  end;
var
  NO_PQ, SQL_Periodo: String;
  DataIni, DataFim, DataX: TDateTime;
  HoraIni, HoraFim: TDateTime;
  Periodo, MaxPQ: Integer;
  //
  PerArr: array of array of array[0..1] of Double;
  I, J, MP, Itens, Col1, Col2: Integer;
  Col3, Col4: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    HoraIni := DModG.ObtemAgora();
    PB1.Position := 0;
    PB2.Position := 0;
    PB3.Position := 0;
    MyObjects.Informa2(LaTit1, LaTit2, True,  'Limpando informa��es');
    LaIni.Caption := Geral.FDT(DModG.ObtemAgora(), 0);
    UnDmkDAC_PF.ExecutaDB(DMod.MyDB, 'DELETE FROM pqx WHERE HowLoad=5');
    UnDmkDAC_PF.ExecutaDB(DMod.MyDB, 'DELETE FROM pqx WHERE Tipo<0');
    //
    if CkTmpPer.Checked then
    begin
      MyObjects.Informa2(LaTit1, LaTit2, True,  'Definindo per�odos de balan�os');
      UnDmkDAC_PF.AbreMySQLQuery0(QrPer, Dmod.MyDB, [
      'SELECT Periodo',
      'FROM balancos',
      'ORDER BY Periodo',
      '']);
      QrPer.First;
      Periodo := 0;
      DataIni := 0;
      PB1.Position := 0;
      PB1.Max := QrPer.RecordCount;
      while not QrPer.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,  'Per�odo: ' +
          Geral.FF0(Periodo));
        DataX := Geral.PTD(QrPerPeriodo.Value, 1);
        DataFim := DataX - 1;
        SQL_Periodo := dmkPF.SQL_Periodo(' WHERE DataX ', DataIni, DataFim, True, True);
        UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE pqx SET TmpPer=' + Geral.FF0(Periodo) + SQL_Periodo);
        //
        // no final...
        DataIni := DataX;
        Periodo := QrPerPeriodo.Value;
        QrPer.Next;
      end;
      //DataFim := EncodeDate(9999, 12, 31);
      SQL_Periodo := dmkPF.SQL_Periodo(' WHERE DataX ', DataIni, 0, True, False);
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE pqx SET TmpPer=' + Geral.FF0(Periodo) + SQL_Periodo);
      LimpaComps();
    end;
    //
    MyObjects.Informa2(LaTit1, LaTit2, True,  'Verificando clientes internos com movimento');
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT MAX(Codigo) Codigo FROM pq']);
    MaxPQ := Dmod.QrAux.Fields[0].AsInteger;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresas, Dmod.MyDB, [
    'SELECT Empresa, CliOrig, COUNT(Insumo) ITENS  ',
    'FROM pqx ',
    'WHERE CliOrig <> 0 ',
    'GROUP BY Empresa, CliOrig ',
    'ORDER BY Empresa, CliOrig ',
    '']);
    QrEmpresas.First;
    while not QrEmpresas.Eof do
    begin
      // Evitar erro
      SetLength(PerArr, 0);
      SetLength(PerArr, MaxPQ + 1);
      //
      LaEmpresa.Caption := Geral.FF0(QrEmpresasEmpresa.Value);
      LaCliOrig.Caption := Geral.FF0(QrEmpresasCliOrig.Value);
      //
      MyObjects.Informa2(LaTit1, LaTit2, True,  'Preparando array de balan�os');
      UnDmkDAC_PF.AbreMySQLQuery0(QrMMP, Dmod.MyDB, [
      'SELECT MIN(TmpPer) MinPer, ',
      'MAX(TmpPer) MaxPer',
      'FROM pqx ',
      'WHERE TmpPer > 0',
      'AND Empresa=' + Geral.FF0(QrEmpresasEmpresa.Value),
      'AND CliOrig=' + Geral.FF0(QrEmpresasCliOrig.Value),
      '']);
      MP := QrMMPMaxPer.Value - QrMMPMinPer.Value + 1;
      for I := 0 to MaxPQ do
      begin
        SetLength(PerArr[I], MP + 1);
        for J := 0 to MP do
        begin
          PerArr[I][J][0] := 0;
          PerArr[I][J][1] := 0;
        end;
      end;
      //
      MyObjects.Informa2(LaTit1, LaTit2, True,  'Preenchendo array de balan�os');
      UnDmkDAC_PF.AbreMySQLDirectQuery0(DqArr1, DModG.MyCompressDB, [
      'SELECT Insumo, TmpPer, ',
      'SUM(Peso) Peso, SUM(Valor) Valor',
      'FROM pqx',
      'WHERE Insumo > 0',
      'AND Empresa=' + Geral.FF0(QrEmpresasEmpresa.Value),
      'AND CliOrig=' + Geral.FF0(QrEmpresasCliOrig.Value),
      'GROUP BY CliOrig, Insumo, TmpPer',
      'ORDER BY CliOrig, Insumo, TmpPer ',
      '']);
      Itens := 0;
      try
        while not DqArr1.Eof do
        begin
          //Evitar erro de mem�ria!
          Application.ProcessMessages;
          //
          Col1 := USQLDB.IntIdx(DqArr1, 0);
          Col2 := USQLDB.IntIdx(DqArr1, 1);
          Col3 := USQLDB.FluIdx(DqArr1, 2);
          Col4 := USQLDB.FluIdx(DqArr1, 3);
          PerArr[Col1][Col2][0] := Col3;
          PerArr[Col1][Col2][1] := Col4;
          //
          Itens := Itens + 1;
          if (Itens / 1000) = (Itens div 1000) then
            MyObjects.Informa2(LaAviso1, LaAviso2, True, Geral.FF0(Itens));
          //
          DqArr1.Next;
        end;
        MyObjects.Informa2(LaAviso1, LaAviso2, True, Geral.FF0(Itens));
      except
        on E: Exception do
          Geral.MB_Erro(E.Message);
      end;
      //
      //...
      QrEmpresas.Next;
    end;
  except
    on E: Exception do
    begin
      Geral.MB_Erro(E.Message);
      Screen.Cursor := crDefault;
    end;
  end;
  HoraFim := DModG.ObtemAgora();
  LimpaComps();

  EXIT;

  ReopenPQs();
  //
  QrPQs.First;
  while not QrPQs.Eof do
  begin
    MyObjects.UpdPB(PB1, nil, nil);
    NO_PQ := QrPQsNO_PQ.Value;
    ReopenPQBIts();
    //
    DataIni := 0;
    while not QrPQB.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True,
      'Corrigindo balan�o: ' + NO_PQ + ' > ' + Geral.FDT(QrPQBDataX.Value, 1));
      DataFim := QrPQBDataX.Value - 1;
      ReopenBalDif(DataIni, DataFim);
      QrBalDif.First;
      //
      if (QrPQBPeso.Value <> QrBalDifPeso.Value) or (QrPQBValor.Value <> QrBalDifValor.Value) then
      begin
        // peso iformado no balan�o � menor que o estoque escritural (falta produto)
        if (QrPQBPeso.Value > QrBalDifPeso.Value)
        or (QrPQBValor.Value > QrBalDifValor.Value) then
        begin
          // Incluir diferen�a no balan�o de resgate com FatID = 20...
          IncluiDiferencadeResgate(VAR_FATID_0020);
          // ... e dar baixa para ficar o peso informado pelo usu�rio.
          //
        end else
        begin
          // Incluir diferen�a no balan�o de resgate...
          IncluiDiferencadeResgate(VAR_FATID_0120);
          //
        end;
      end;
      //
      // no final...
      DataIni := QrPQBDataX.Value;
      QrPQB.Next;
    end;
    QrPQs.Next;
  end;
  LaFim.Caption := Geral.FDT(DModG.ObtemAgora(), 0);
end;

procedure TFmPQWRefaz2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQWRefaz2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQWRefaz2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPQWRefaz2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQWRefaz2.IncluiDiferencadeResgate(FatID: Integer);
const
  sProcName = 'TFmPQWRefaz.IncluiDiferencadeResgate()';
  HowLoad = Integer(TPQEHowLoad.pqehlPQWRefaz); // 5
  DtCorrApo = '0000-00-00';
  Tipo = VAR_FATID_0000;
  Unitario = False;
  ide_serie = 0;
  ide_nNF = 0;
  xLote = '';
  dFab = '0000-00-00';
  dVal = '0000-00-00';
var
  DataX: String;
  CliOrig, CliDest, Insumo: Integer;
  Peso, Valor: Double;
  OrigemCodi, Empresa: Integer;
  OrigemCtrl: Integer;
  //
  Controle, CliInt, OriCodi, Codigo: Integer;
begin
  DataX       := Geral.FDT(QrPQBDataX.Value, 1);
  CliOrig     := QrPQsCliOrig.Value;
  CliDest     := QrPQsCliOrig.Value;
  Insumo      := QrPQsInsumo.Value;
  Peso        := QrPQBPeso.Value - QrBalDifPeso.Value;
  Valor       := QrPQBValor.Value - QrBalDifValor.Value;
  OrigemCodi  := QrPQBPeriodo.Value;
  OrigemCtrl  := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta');
  Empresa     := QrPQsEmpresa.Value;
  //
  CliInt      := CliOrig;
  OriCodi     := OrigemCodi;
  Codigo      := OrigemCodi;
  //
  case FatID of
    VAR_FATID_0020:
    begin
      Controle := UMyMod.BPGS1I32('pqbadx', 'Controle', '', '', tsPos, stIns, Controle);
      (*
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqbadx', False, [
      'Codigo', 'Empresa', 'CliInt',
      'Insumo', 'Peso', 'Valor',
      'ide_serie', 'ide_nNF', 'DtCorrApo',
      'xLote', 'dFab', 'dVal'], [
      'Controle'], [
      Codigo, Empresa, CliInt,
      Insumo, Peso, Valor,
      ide_serie, ide_nNF, DtCorrApo,
      xLote, dFab, dVal], [
      Controle], True) then
      *)
      begin
        // Lan�amento a positivo para controlar estoque de NF (e lote)
        InserePQx(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
          (*OriCtrl*)Controle, (*OriTipo*)FatID, Unitario, DtCorrApo, Empresa, HowLoad);
        // Lan�amento para netralizar o lan�amento a positivo
        InserePQx(DataX, CliOrig, CliDest, Insumo, -Peso, -Valor, OrigemCodi,
        OrigemCtrl, Tipo, Unitario, DtCorrApo, Empresa, HowLoad);
      end;
    end;
    VAR_FATID_0120:
    begin
      Controle := UMyMod.BPGS1I32('pqbbxa', 'Controle', '', '', tsPos, stIns, Controle);
      (*
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqbbxa', False, [
      'Codigo', 'Empresa', 'CliInt',
      'Insumo', 'Peso', 'Valor',
      'DtCorrApo'], [
      'Controle'], [
      Codigo, Empresa, CliInt,
      Insumo, Peso, Valor,
      DtCorrApo], [
      Controle], True) then
      *)
      begin
        // Lan�amento a negativo para controlar estoque de NF (e lote)
        InserePQx(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
          (*OriCtrl*)Controle, (*OriTipo*)FatID, Unitario, DtCorrApo, Empresa, HowLoad);
        // Lan�amento para netralizar o lan�amento a negativo
        InserePQx(DataX, CliOrig, CliDest, Insumo, -Peso, -Valor, OrigemCodi,
        OrigemCtrl, Tipo, Unitario, DtCorrApo, Empresa, HowLoad);
      end;
    end;
    else
      Geral.MB_Erro('"FatID" n�o implementado em ' + sProcName);
  end;
end;

procedure TFmPQWRefaz2.InserePQX(DataX: String; CliOrig, CliDest, Insumo: Integer;
  Peso, Valor: Double; OrigemCodi, OrigemCtrl, Tipo: Integer; Unitario:
  Boolean; DtCorrApo: String; _Empresa: Integer; HowLoad: Integer);
var
  Empresa: Integer;
begin
  //Result  := False;
  Empresa := _Empresa;
  if Empresa = 0 then
  begin
    Empresa := -11;
    Geral.MB_Erro('Empresa n�o definida! Ser� considerado -11!');
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_INS_TAB_PQX, False, [
  'DataX', 'CliOrig', 'CliDest',
  'Insumo', 'Peso', 'Valor',(*,
  'Retorno', 'StqMovIts', 'RetQtd',*)
  'HowLoad' (*'StqCenLoc', 'SdoPeso',
  'SdoValr', 'AcePeso', 'AceValr'*),
  'DtCorrApo', 'Empresa'], [
  'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
  DataX, CliOrig, CliDest,
  Insumo, Peso, Valor,(*,
  Retorno, StqMovIts, RetQtd,*)
  HowLoad (*StqCenLoc, SdoPeso,
  SdoValr, AcePeso, AceValr*),
  DtCorrApo, Empresa], [
  OrigemCodi, OrigemCtrl, Tipo], False) then
  begin
    //AtualizaSdoPQx(Query, OrigemCodi, OrigemCtrl, Tipo);
    //Result := True;
  end;
end;

procedure TFmPQWRefaz2.QrPQBCalcFields(DataSet: TDataSet);
begin
  QrPQBDataX.Value := Geral.PTD(QrPQBPeriodo.Value, 1);
end;

procedure TFmPQWRefaz2.ReopenBalDif(DataIni, DataFim: TDateTime);
var
  SQLPeriodo: String;
begin
  SQLPeriodo := dmkPF.SQL_Periodo('AND DataX ', DataIni, DataFim, True, True);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrBalDif, Dmod.MyDB, [
  'SELECT SUM(Peso) Peso, SUM(Valor) Valor   ',
  'FROM pqx  ',
  'WHERE Insumo=' + Geral.FF0(QrPQsInsumo.Value),
  'AND CliOrig=' + Geral.FF0(QrPQsCliOrig.Value),
  'AND Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
  SQLPeriodo,
  '']);
end;

procedure TFmPQWRefaz2.ReopenPQBIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQBIts, Dmod.MyDB, [
  'SELECT DataX, OrigemCodi, ',
  'SUM(Peso) Peso, SUM(Valor) Valor ',
  'FROM pqx ',
  'WHERE Insumo=' + Geral.FF0(QrPQsInsumo.Value),
  'AND CliOrig=' + Geral.FF0(QrPQsCliOrig.Value),
  'AND Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
  'AND Tipo=0 ',
  'GROUP BY OrigemCodi ',
  'ORDER BY OrigemCodi ',
  '']);
  //
  // Deve ser depois!
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQB, Dmod.MyDB, [
  'SELECT Periodo',
  'FROM balancos',
  'ORDER BY Periodo',
  '']);
  PB2.Position := 0;
  PB2.Max := QrPQB.RecordCount;
end;

procedure TFmPQWRefaz2.ReopenPQs();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQs, Dmod.MyDB, [
  'SELECT pqx.Empresa, pqx.CliOrig, pqx.Insumo, pq_.Nome NO_PQ ',
  'FROM pqx ',
  'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo ',
  // teste aqui!!!!
  //'WHERE pq_.Codigo=21', // Testar primeiro Dermascal U Ecoservice
  'WHERE pq_.Codigo>0',
  'GROUP BY pqx.CliOrig, pqx.Insumo ',
  'ORDER BY pqx.CliOrig, pqx.Insumo ',
  '']);
  //
  PB1.Position := 0;
  PB1.Max := QrPQs.RecordCount;
end;

end.
