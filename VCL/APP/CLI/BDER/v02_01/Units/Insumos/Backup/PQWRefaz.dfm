object FmPQWRefaz: TFmPQWRefaz
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-000 :: Refaz Baixas de Uso e Consumo por NF'
  ClientHeight = 549
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 490
        Height = 32
        Caption = 'Refaz Baixas de Uso e Consumo por NF'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 490
        Height = 32
        Caption = 'Refaz Baixas de Uso e Consumo por NF'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 490
        Height = 32
        Caption = 'Refaz Baixas de Uso e Consumo por NF'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 479
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 612
    Height = 431
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 612
      Height = 129
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 412
        Top = 4
        Width = 30
        Height = 13
        Caption = 'In'#237'cio:'
      end
      object Label2: TLabel
        Left = 412
        Top = 24
        Width = 25
        Height = 13
        Caption = 'Final:'
      end
      object LaFim: TLabel
        Left = 464
        Top = 24
        Width = 9
        Height = 13
        Caption = '...'
      end
      object LaIni: TLabel
        Left = 464
        Top = 4
        Width = 36
        Height = 13
        Caption = '0:00:00'
      end
      object Label3: TLabel
        Left = 12
        Top = 92
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label4: TLabel
        Left = 12
        Top = 112
        Width = 71
        Height = 13
        Caption = 'Cliente Interno:'
      end
      object LaEmpresa: TLabel
        Left = 100
        Top = 92
        Width = 9
        Height = 13
        Caption = '...'
      end
      object LaCliOrig: TLabel
        Left = 100
        Top = 112
        Width = 9
        Height = 13
        Caption = '...'
      end
      object Label8: TLabel
        Left = 412
        Top = 44
        Width = 36
        Height = 13
        Caption = 'Tempo:'
      end
      object LaTempo0: TLabel
        Left = 464
        Top = 44
        Width = 9
        Height = 13
        Caption = '...'
      end
      object LaTempo1: TLabel
        Left = 320
        Top = 4
        Width = 45
        Height = 13
        Caption = '...______'
      end
      object LaTempo2: TLabel
        Left = 320
        Top = 24
        Width = 45
        Height = 13
        Caption = '...______'
      end
      object LaTempo3: TLabel
        Left = 320
        Top = 44
        Width = 45
        Height = 13
        Caption = '...______'
      end
      object LaTempo4: TLabel
        Left = 320
        Top = 64
        Width = 45
        Height = 13
        Caption = '...______'
      end
      object CkTmpPer: TCheckBox
        Left = 16
        Top = 4
        Width = 257
        Height = 17
        Caption = 'Atualizar o per'#237'odo para cada item do movimento.'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object CkModificaBalancos: TCheckBox
        Left = 16
        Top = 24
        Width = 285
        Height = 17
        Caption = 'Modificar balan'#231'os para novo modelo de baixa popr NF.'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
      object CkRefazBaixaPorNF: TCheckBox
        Left = 16
        Top = 44
        Width = 285
        Height = 17
        Caption = 'Refazer baixas por NF.'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
      object CkConfirmaSdosNFs: TCheckBox
        Left = 16
        Top = 64
        Width = 285
        Height = 17
        Caption = 'Atualizar (confirmar) os saldos das NFs.'
        Checked = True
        State = cbChecked
        TabOrder = 3
      end
    end
    object GBAvisos1: TGroupBox
      Left = 0
      Top = 296
      Width = 612
      Height = 135
      Align = alBottom
      Caption = ' Avisos: '
      TabOrder = 1
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 608
        Height = 118
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 8
          Top = 42
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 9
          Top = 43
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaTit1: TLabel
          Left = 9
          Top = 22
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaTit2: TLabel
          Left = 8
          Top = 23
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaGrupo2: TLabel
          Left = 9
          Top = 2
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaGrupo1: TLabel
          Left = 8
          Top = 3
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object PB2: TProgressBar
          Left = 0
          Top = 84
          Width = 608
          Height = 17
          Align = alBottom
          TabOrder = 0
        end
        object PB3: TProgressBar
          Left = 0
          Top = 101
          Width = 608
          Height = 17
          Align = alBottom
          TabOrder = 1
        end
        object PB1: TProgressBar
          Left = 0
          Top = 67
          Width = 608
          Height = 17
          Align = alBottom
          TabOrder = 2
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 247
      Width = 612
      Height = 49
      Align = alBottom
      TabOrder = 2
      Visible = False
      object Label7: TLabel
        Left = 416
        Top = 8
        Width = 31
        Height = 13
        Caption = 'Query:'
      end
      object LaQuery: TLabel
        Left = 456
        Top = 8
        Width = 36
        Height = 13
        Caption = '0:00:00'
      end
      object LaDirect: TLabel
        Left = 456
        Top = 28
        Width = 36
        Height = 13
        Caption = '0:00:00'
      end
      object Label10: TLabel
        Left = 416
        Top = 28
        Width = 31
        Height = 13
        Caption = 'Direct:'
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 129
      Width = 612
      Height = 92
      Align = alTop
      TabOrder = 3
      object Label6: TLabel
        Left = 12
        Top = 48
        Width = 37
        Height = 13
        Caption = 'Insumo:'
      end
      object Label5: TLabel
        Left = 12
        Top = 8
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object RGPesquisa: TRadioGroup
        Left = 244
        Top = 4
        Width = 133
        Height = 81
        Caption = 'Pesquisa: '
        ItemIndex = 1
        Items.Strings = (
          'Um a um'
          'Todos de uma vez s'#243)
        TabOrder = 0
      end
      object RGInclusao: TRadioGroup
        Left = 104
        Top = 4
        Width = 133
        Height = 81
        Caption = ' Inclus'#227'o de registros: '
        ItemIndex = 1
        Items.Strings = (
          'Um a um'
          'Por lote')
        TabOrder = 1
      end
      object EdInsumo: TdmkEdit
        Left = 12
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCliOrig: TdmkEdit
        Left = 12
        Top = 24
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object MeAvisos: TMemo
    Left = 612
    Top = 48
    Width = 396
    Height = 431
    Align = alClient
    TabOrder = 3
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrPQs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pqx.CliOrig, pqx.Insumo, pq_.Nome NO_PQ '
      'FROM pqx '
      'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo '
      'GROUP BY pqx.CliOrig, pqx.Insumo '
      'ORDER BY pqx.CliOrig, pqx.Insumo ')
    Left = 568
    Top = 132
    object QrPQsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQsNO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrPQsCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrPer: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pqx.CliOrig, pqx.Insumo, pq_.Nome NO_PQ '
      'FROM pqx '
      'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo '
      'GROUP BY pqx.CliOrig, pqx.Insumo '
      'ORDER BY pqx.CliOrig, pqx.Insumo ')
    Left = 568
    Top = 80
    object QrPerPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
  end
  object QrEmpresas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Empresa, CliOrig, COUNT(Insumo) ITENS '
      'FROM pqx'
      'WHERE CliOrig <> 0'
      'GROUP BY Empresa, CliOrig'
      'ORDER BY Empresa, CliOrig'
      ' ')
    Left = 500
    Top = 84
    object QrEmpresasEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEmpresasCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrEmpresasITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object QrItens: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrItensAfterScroll
    OnCalcFields = QrItensCalcFields
    SQL.Strings = (
      'DELETE FROM bluederm_ecoservice.pqx WHERE HowLoad=5'
      ';'
      'DROP TABLE IF EXISTS _pqw_aj_per'
      ';'
      'CREATE TABLE _pqw_aj_per'
      'SELECT DataX, OrigemCodi, '
      'SUM(Peso) Peso, SUM(Valor) Valor '
      'FROM bluederm_ecoservice.pqx '
      'WHERE CliOrig=-11'
      'AND Empresa=-11'
      'AND Tipo=0 '
      'AND Insumo=21'
      'GROUP BY OrigemCodi '
      'ORDER BY OrigemCodi'
      ';'
      'DROP TABLE IF EXISTS _pqw_aj_its'
      ';'
      'CREATE TABLE _pqw_aj_its'
      'SELECT Insumo, TmpPer,'
      'SUM(Peso) Peso, SUM(Valor) Valor'
      'FROM bluederm_ecoservice.pqx'
      'WHERE Empresa=-11'
      'AND CliOrig=-11'
      'AND Insumo=21'
      'GROUP BY Insumo, TmpPer'
      ';'
      ''
      'SELECT bal.Periodo, '
      'per.Peso PerPeso, per.Valor PerValor, '
      'its.Peso ItsPeso, its.Valor ItsValor '
      'FROM bluederm_ecoservice.balancos bal'
      'LEFT JOIN _pqw_aj_per per ON per.OrigemCodi=bal.Periodo'
      'LEFT JOIN _pqw_aj_its its ON its.TmpPer=bal.Periodo'
      ' ')
    Left = 500
    Top = 140
    object QrItensPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrItensPerPeso: TFloatField
      FieldName = 'PerPeso'
    end
    object QrItensPerValor: TFloatField
      FieldName = 'PerValor'
    end
    object QrItensItsPeso: TFloatField
      FieldName = 'ItsPeso'
    end
    object QrItensItsValor: TFloatField
      FieldName = 'ItsValor'
    end
  end
  object DqPQs: TMySQLDirectQuery
    Database = Dmod.MyDB
    Left = 372
    Top = 288
  end
  object DqItens: TMySQLDirectQuery
    Database = Dmod.MyDB
    Left = 372
    Top = 340
  end
  object QrPosit: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DataX, OrigemCodi, OrigemCtrl, Tipo,'
      'CliOrig, Insumo, Peso, Valor '
      'FROM pqx'
      'WHERE Tipo>0'
      'AND (Peso>0 OR (Peso=0 AND Valor > 0))'
      'AND Insumo > 0'
      'ORDER BY CliOrig, Insumo, DataX, OrigemCtrl')
    Left = 188
    Top = 292
    object QrPositDataX: TDateField
      FieldName = 'DataX'
      Required = True
    end
    object QrPositOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
      Required = True
    end
    object QrPositOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
      Required = True
    end
    object QrPositTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPositCliOrig: TIntegerField
      FieldName = 'CliOrig'
      Required = True
    end
    object QrPositInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPositPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrPositValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
  end
  object QrNegat: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DataX, OrigemCodi, OrigemCtrl, Tipo,'
      'CliOrig, Insumo, Peso, Valor '
      'FROM pqx'
      'WHERE Tipo>0'
      'AND (Peso<0 OR (Peso=0 AND Valor < 0))'
      'AND Insumo > 0'
      'ORDER BY CliOrig, Insumo, DataX, OrigemCtrl')
    Left = 188
    Top = 344
    object QrNegatDataX: TDateField
      FieldName = 'DataX'
      Required = True
    end
    object QrNegatOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
      Required = True
    end
    object QrNegatOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
      Required = True
    end
    object QrNegatTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrNegatCliOrig: TIntegerField
      FieldName = 'CliOrig'
      Required = True
    end
    object QrNegatInsumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrNegatPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrNegatValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
  end
  object DqPosit: TMySQLDirectQuery
    Database = Dmod.MyDB
    Left = 272
    Top = 292
  end
  object DqNegat: TMySQLDirectQuery
    Database = Dmod.MyDB
    Left = 272
    Top = 344
  end
  object QrSdosNFs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DataX, Tipo, OrigemCodi, OrigemCtrl,'
      'SdoPeso, SdoValr '
      'FROM pqx'
      'WHERE SdoPeso<>0'
      'ORDER BY DataX, Tipo, OrigemCodi, OrigemCtrl')
    Left = 740
    Top = 224
    object QrSdosNFsDataX: TDateField
      FieldName = 'DataX'
      Required = True
    end
    object QrSdosNFsTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrSdosNFsOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
      Required = True
    end
    object QrSdosNFsOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
      Required = True
    end
    object QrSdosNFsSdoPeso: TFloatField
      FieldName = 'SdoPeso'
      Required = True
    end
    object QrSdosNFsSdoValr: TFloatField
      FieldName = 'SdoValr'
      Required = True
    end
  end
  object Qry1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 644
    Top = 280
  end
  object Qry2: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 644
    Top = 228
  end
end
