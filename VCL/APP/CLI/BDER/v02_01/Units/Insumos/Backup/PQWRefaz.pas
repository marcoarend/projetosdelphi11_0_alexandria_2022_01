unit PQWRefaz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, UnProjGroup_Consts,
  mySQLDirectQuery, UMySQLDB;

type
  TFmPQWRefaz = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPQs: TMySQLQuery;
    Panel2: TPanel;
    QrPQsInsumo: TIntegerField;
    QrPQsNO_PQ: TWideStringField;
    QrPQsCliOrig: TIntegerField;
    QrPQsEmpresa: TIntegerField;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    LaFim: TLabel;
    LaIni: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    LaEmpresa: TLabel;
    LaCliOrig: TLabel;
    CkTmpPer: TCheckBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    LaTit1: TLabel;
    LaTit2: TLabel;
    PB2: TProgressBar;
    PB3: TProgressBar;
    PB1: TProgressBar;
    QrPer: TMySQLQuery;
    QrPerPeriodo: TIntegerField;
    QrEmpresas: TMySQLQuery;
    QrEmpresasEmpresa: TIntegerField;
    QrEmpresasCliOrig: TIntegerField;
    QrEmpresasITENS: TLargeintField;
    QrItens: TMySQLQuery;
    QrItensPeriodo: TIntegerField;
    QrItensPerPeso: TFloatField;
    QrItensPerValor: TFloatField;
    QrItensItsPeso: TFloatField;
    QrItensItsValor: TFloatField;
    Panel5: TPanel;
    Label7: TLabel;
    LaQuery: TLabel;
    LaDirect: TLabel;
    Label10: TLabel;
    DqPQs: TMySQLDirectQuery;
    DqItens: TMySQLDirectQuery;
    Panel6: TPanel;
    RGPesquisa: TRadioGroup;
    RGInclusao: TRadioGroup;
    EdInsumo: TdmkEdit;
    Label6: TLabel;
    EdCliOrig: TdmkEdit;
    Label5: TLabel;
    CkModificaBalancos: TCheckBox;
    CkRefazBaixaPorNF: TCheckBox;
    QrPosit: TMySQLQuery;
    QrNegat: TMySQLQuery;
    QrPositDataX: TDateField;
    QrPositOrigemCodi: TIntegerField;
    QrPositOrigemCtrl: TIntegerField;
    QrPositTipo: TIntegerField;
    QrPositCliOrig: TIntegerField;
    QrPositInsumo: TIntegerField;
    QrPositPeso: TFloatField;
    QrPositValor: TFloatField;
    QrNegatDataX: TDateField;
    QrNegatOrigemCodi: TIntegerField;
    QrNegatOrigemCtrl: TIntegerField;
    QrNegatTipo: TIntegerField;
    QrNegatCliOrig: TIntegerField;
    QrNegatInsumo: TIntegerField;
    QrNegatPeso: TFloatField;
    QrNegatValor: TFloatField;
    DqPosit: TMySQLDirectQuery;
    DqNegat: TMySQLDirectQuery;
    Label8: TLabel;
    LaTempo0: TLabel;
    MeAvisos: TMemo;
    CkConfirmaSdosNFs: TCheckBox;
    QrSdosNFs: TMySQLQuery;
    QrSdosNFsDataX: TDateField;
    QrSdosNFsTipo: TIntegerField;
    QrSdosNFsOrigemCodi: TIntegerField;
    QrSdosNFsOrigemCtrl: TIntegerField;
    QrSdosNFsSdoPeso: TFloatField;
    QrSdosNFsSdoValr: TFloatField;
    LaTempo1: TLabel;
    LaTempo2: TLabel;
    LaTempo3: TLabel;
    LaTempo4: TLabel;
    Qry1: TMySQLQuery;
    LaGrupo2: TLabel;
    LaGrupo1: TLabel;
    Qry2: TMySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrItensCalcFields(DataSet: TDataSet);
    procedure QrItensAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    FSQL_Values_Pqx, FSQL_Values_Pqw: String;
    FControle_PQBAdx, FControle_PQBBxa, FOrigemCtrl, FSQL_StatemCounts_Pqw: Integer;
    FDataX: TDateTime;
    procedure ReopenPQsQr();
    procedure ReopenItsQr();
    procedure ReopenPQsDq();
    procedure ReopenItsDq(CliOrig, Insumo: Integer);
    procedure ReopenEmpresas();
    procedure IncluiDiferencadeResgate(FatID: Integer; AntPeso, AntValor: Double);
    procedure InserePQX(DataX: String; CliOrig, CliDest, Insumo: Integer;
              Peso, Valor: Double; OrigemCodi, OrigemCtrl, Tipo: Integer; Unitario:
              Boolean; DtCorrApo: String; _Empresa, HowLoad, TmpPer: Integer);
    procedure InserePorLote();
    procedure PesquisaTodosPQx();
    procedure ReopenPositENegat();
    procedure GeraSqlInsPQW(Peso, Valor: Double; OriCodi, OriCtrl, OriTipo,
              DstCodi, DstCtrl, DstTipo: Integer; OriEmpresa, OriCliInt,
              OriInsumo, DstEmpresa, DstCliInt, DstInsumo: Integer; DstValr:
              Double; DataE, DataX: TDateTime);
    procedure ExecutaSqlInsPQW();
    procedure VerificaPeriodos();


  public
    { Public declarations }
  end;

  var
  FmPQWRefaz: TFmPQWRefaz;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkProcFunc, UMySQLModule, UnPQ_PF,
  ModuleGeral;

{$R *.DFM}

procedure TFmPQWRefaz.BtOKClick(Sender: TObject);
  procedure LimpaComps();
  begin
    PB1.Position := 0;
    PB2.Position := 0;
    PB3.Position := 0;
    MyObjects.Informa2(LaTit1, LaTit2, False,  '...');
    MyObjects.Informa2(LaAviso1, LaAviso2, False,  '...');
  end;
var
  NO_PQ, SQL_Periodo, SQL_CliOrig, SQL_Insumo: String;
  DataIni, DataFim, HoraIni, HoraFim, DataX: TDateTime;
  CliOrig, Periodo, Insumo: Integer;
  JaBal, Inseriu: Boolean;
  AntPeso, AntValor: Double;
  //
  DqPositDataX: TDateTime;
  DqPositOrigemCodi, DqPositOrigemCtrl, DqPositTipo, DqPositEmpresa,
  DqPositCliOrig, DqPositInsumo: Integer;
  DqPositPeso, DqPositValor: Double;
  //
  DqNegatDataX: TDateTime;
  DqNegatOrigemCodi, DqNegatOrigemCtrl, DqNegatTipo, DqNegatEmpresa,
  DqNegatCliOrig, DqNegatInsumo: Integer;
  DqNegatPeso, DqNegatValor: Double;
  DqNegat_EOF: Boolean;
  //
  AtualEmpresa, AtualCliOrig, AtualInsumo(*, Contador*): Integer;
  SdoPosPeso, SdoPosValr, SdoNegPeso, SdoNegValr, PqwPeso, PqwValor, DstValr: Double;
  //
  AntEmpresa, AntCliOrig, AntInsumo: Integer;
  //
  HoraIni0, HoraIni1, HoraIni2, HoraIni3, HoraIni4: TDateTime;
  SaldoPeso: Double;
  //
  (*ComparaA, ComparaB: Integer *)
  Diferenca: Double;
  //
  procedure IncluiDiferenca();
  begin
    begin
      // peso iformado no balan�o � menor que o estoque escritural (falta produto)
      (*
      if (QrItensPerPeso.Value > QrItensItsPeso.Value)
      or (QrItensPerValor.Value > QrItensItsValor.Value) then
      *)
      if (QrItensPerPeso.Value > AntPeso)
      or (QrItensPerValor.Value > AntValor) then
      begin
        // Incluir diferen�a no balan�o de resgate com FatID = 20...
        IncluiDiferencadeResgate(VAR_FATID_0020, AntPeso, AntValor);
        //IncluiDiferencadeResgate(VAR_FATID_0120);
        // ... e dar baixa para ficar o peso informado pelo usu�rio.
        //
      end else
      begin
        // Incluir diferen�a no balan�o de resgate...
        IncluiDiferencadeResgate(VAR_FATID_0120, AntPeso, AntValor);
        //IncluiDiferencadeResgate(VAR_FATID_0020);
        //
      end;
    end;
  end;
begin
  if Geral.MB_Pergunta('Os segintes itens abaixo foram pr�-executados?' + sLineBreak +
  '[  ] Fazer backup dos dados' + sLineBreak +
  '[  ] Limpar tabela varfatid na verifica��o do banco de dados' + sLineBreak +
  '[  ] Corre��o de entradas orf�s - Incongru�ncias de Estoque de Uso e Consumo' + sLineBreak +
  '[  ] Corre��o de Datas inv�lidas - Incongru�ncias de Estoque de Uso e Consumo' + sLineBreak +
  '[  ] Corre��o de Cllientes internos inv�lidos - Incongru�ncias de Estoque de Uso e Consumo' + sLineBreak +
  '[  ] Atualizar campos novos da tabela PQW - Relat�rio Insumo por NF' + sLineBreak +
  '[  ] Impress�o ou gera��o de PDF - Relat�rio Insumo por NF (Clientes 183 e 186?)' + sLineBreak +
  '[  ] ' + sLineBreak +
  '[  ] ' + sLineBreak +
  '[  ] REFAZER BAIXAS DE USO E CONSUMO' + sLineBreak +
  '[  ] ' + sLineBreak +
  '[  ] ' + sLineBreak +
  '[  ] Informar NF em entradas sem NF' + sLineBreak +
  '[  ] ' + sLineBreak) <> ID_YES then Exit;
  //
  MeAvisos.Lines.Clear;
  //
  if MyObjects.FIC(RGInclusao.ItemIndex < 0,  RGInclusao,
    'Informe a forma de inclus�o dos registros!') then Exit;
  //
  if MyObjects.FIC(RGPesquisa.ItemIndex < 0,  RGPesquisa,
    'Informe a forma de Pesquisa!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    FSQL_Values_Pqx := EmptyStr;
    FControle_PQBAdx := 0;
    FControle_PQBBxa := 0;
    FOrigemCtrl := 0;
    HoraIni0 := DModG.ObtemAgora();
    LaIni.Caption := Geral.FDT(DModG.ObtemAgora(), 0);
    PB1.Position := 0;
    PB2.Position := 0;
    PB3.Position := 0;
    MyObjects.Informa2(LaTit1, LaTit2, True,  'Limpando informa��es');
    // teste
    CliOrig := EdCliOrig.ValueVariant;
    Insumo := EdInsumo.ValueVariant;
    SQL_CliOrig := '';
    SQL_Insumo := '';
    if CliOrig <> 0 then
      SQL_CliOrig := ' AND CliOrig=' + Geral.FF0(CliOrig);
    if Insumo <> 0 then
      SQL_Insumo := ' AND Insumo=' + Geral.FF0(Insumo);
    //
    UnDmkDAC_PF.ExecutaDB(DMod.MyDB, 'DELETE FROM pqx WHERE Tipo<0 ' + SQL_CliOrig  + SQL_Insumo);
    //

    VerificaPeriodos();

    if CkTmpPer.Checked then
    begin
      HoraIni1 := DModG.ObtemAgora();
      MyObjects.Informa2(LaGrupo1, LaGrupo2, True, 'Atualizando o per�odo de cada item de movimento.');
      MyObjects.Informa2(LaTit1, LaTit2, True,  'Definindo per�odos de balan�os');
      UnDmkDAC_PF.AbreMySQLQuery0(QrPer, Dmod.MyDB, [
      'SELECT Periodo',
      'FROM balancos',
      'ORDER BY Periodo',
      '']);
      QrPer.First;
      Periodo := 0;
      DataIni := 0;
      PB1.Position := 0;
      PB1.Max := QrPer.RecordCount;
      while not QrPer.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,  'Per�odo: ' +
          Geral.FF0(Periodo));
        DataX := Geral.PTD(QrPerPeriodo.Value, 1);
        DataFim := DataX - 1;
        SQL_Periodo := dmkPF.SQL_Periodo(' WHERE DataX ', DataIni, DataFim, True, True);
        UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE pqx SET TmpPer=' + Geral.FF0(Periodo) + SQL_Periodo);
        //
        // no final...
        DataIni := DataX;
        Periodo := QrPerPeriodo.Value;
        QrPer.Next;
      end;
      //DataFim := EncodeDate(9999, 12, 31);
      SQL_Periodo := dmkPF.SQL_Periodo(' WHERE DataX ', DataIni, 0, True, False);
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE pqx SET TmpPer=' + Geral.FF0(Periodo) + SQL_Periodo);
      LimpaComps();
      //
      HoraFim := DModG.ObtemAgora();
      LaTempo1.Caption := Geral.FDT(HoraFim - HoraIni1, 108);
    end;
    //
    LimpaComps();
    //
  except
    on E: Exception do
    begin
      Geral.MB_Erro(E.Message);
      Screen.Cursor := crDefault;
    end;
  end;







  if CkModificaBalancos.Checked then
  begin
    MyObjects.Informa2(LaGrupo1, LaGrupo2, True, 'Modificando os balan�os para novo modelo de baixa popr NF.');
    HoraIni2 := DModG.ObtemAgora();
    UnDmkDAC_PF.ExecutaDB(DMod.MyDB, 'DELETE FROM pqx WHERE HowLoad=5 ' + SQL_CliOrig  + SQL_Insumo);
    ////////////////////////////////////////////////////////////////
    if RGPesquisa.ItemIndex = 1 then
    begin
      MyObjects.Informa2(LaTit1, LaTit2, True,  'Gerando dados de pesquisa massiva');
      PesquisaTodosPQx();
    end;
    ////////////////////////////////////////////////////////////////
    ReopenEmpresas();
    PB1.Max := QrEmpresas.RecordCount;
    QrEmpresas.First;
    while not QrEmpresas.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaTit1, LaTit2, True,
      'Empresa: ' + Geral.FF0(QrEmpresasEmpresa.Value) +
      'Cliente interno: ' + Geral.FF0(QrEmpresasCliOrig.Value));
      FSQL_Values_Pqx := EmptyStr;
      FControle_PQBAdx := 0;
      FControle_PQBBxa := 0;
      FOrigemCtrl      := 0;
      //
      ReopenPQsQr();
      //
      QrPQs.First;
      while not QrPQs.Eof do
      begin
        NO_PQ := QrPQsNO_PQ.Value;
        MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True,
        'Corrigindo balan�o: ' + NO_PQ);
        //
        ReopenItsQr();
        JaBal    := False;
        AntPeso  := 0.000;
        AntValor := 0.00;
        //
        Inseriu  := False;
        while not QrItens.Eof do
        begin
          if JaBal = False then
          begin
            if (QrItensPerPeso.Value <> 0.000) or (QrItensPerValor.Value <> 0.00) then
            begin
              JaBal := True;
            end else
            begin
              if (QrItensItsPeso.Value <> 0.000) or (QrItensItsValor.Value <> 0.00) then
              begin
                AntPeso := AntPeso + QrItensItsPeso.Value;
                AntValor := AntValor + QrItensItsValor.Value;
              end;
            end;
          end;
          if JaBal = True then
          begin
            //DataFim := QrItensDataX.Value - 1;
            DataFim := FDataX - 1;
            if (QrItensPerPeso.Value <> AntPeso)
            or (QrItensPerValor.Value <> AntValor)
            //or (Inseriu = True)
            then
            begin
              IncluiDiferenca();
              Inseriu := True;
            end;
            //if Inseriu = True then
            begin
              AntPeso := QrItensItsPeso.Value;
              AntValor := QrItensItsValor.Value;
              //AntPeso := QrItensItsPeso.Value + (QrItensPerPeso.Value - AntPeso);
              //AntValor := QrItensItsValor.Value + (QrItensPerValor.Value - AntValor);
              //AntPeso := QrItensPerPeso.Value;
              //AntValor := QrItensPerValor.Value;
            end;
          end;
          //
          // no final...
          //DataIni := QrItensDataX.Value;
          DataIni := FDataX;
          QrItens.Next;
        end;
        // se nunca teve balan�o, refazer zerando no primeiro?
        if (JaBal = False) then
        begin
          // e se o ultimo balanco est� zerado
          if (QrItensItsPeso.Value = 0.000) and (QrItensItsValor.Value = 0.00) then
          begin
            QrItens.First; // 0
            QrItens.Next;  // Primeiro
            IncluiDiferenca();
          end else
          begin
            //QrItens.Prior;
            AntPeso := AntPeso - QrItensItsPeso.Value;
            AntValor := AntValor - QrItensItsValor.Value;
            IncluiDiferenca();
          end;
        end;
        //
        QrPQs.Next;
      end;
      if RGInclusao.ItemIndex = 1 then
        InsereporLote();
      QrEmpresas.Next;
    end;
    //
    LimpaComps();
    //
    HoraFim := DModG.ObtemAgora();
    LaTempo2.Caption := Geral.FDT(HoraFim - HoraIni2, 108);
  end;







  if CkRefazBaixaPorNF.Checked then
  begin
    MyObjects.Informa2(LaGrupo1, LaGrupo2, True, 'Refazendo baixas por NF');
    HoraIni3 := DModG.ObtemAgora();
    PB1.Position := 0;
    PB2.Position := 0;
    //
    FSQL_Values_Pqw := EmptyStr;
    FSQL_StatemCounts_Pqw := 0;
    //
    DqNegat_EOF := False;





    if CliOrig <> 0 then
      SQL_CliOrig := ' WHERE CliOrig=' + Geral.FF0(CliOrig)
    else
      SQL_CliOrig := EmptyStr;
    SQL_Insumo := ' ';
    if Insumo <> 0 then
    begin
      if SQL_CliOrig = EmptyStr then
        SQL_Insumo := ' WHERE Insumo=' + Geral.FF0(Insumo)
      else
        SQL_Insumo := ' AND Insumo=' + Geral.FF0(Insumo);
    end;
    UnDmkDAC_PF.ExecutaDB(DMod.MyDB,
    'UPDATE pqx SET SdoPeso=0, SdoValr=0 ' + SQL_CliOrig  + SQL_Insumo);



    if CliOrig <> 0 then
      SQL_CliOrig := ' WHERE OriCliInt=' + Geral.FF0(CliOrig)
    else
      SQL_CliOrig := EmptyStr;
    SQL_Insumo := ' ';
    if Insumo <> 0 then
    begin
      if SQL_CliOrig = EmptyStr then
        SQL_Insumo := ' WHERE OriInsumo=' + Geral.FF0(Insumo)
      else
        SQL_Insumo := ' AND OriInsumo=' + Geral.FF0(Insumo);
    end;

    // Ver o que fazer!
    UnDmkDAC_PF.ExecutaDB(DMod.MyDB, 'DELETE FROM pqw ' + SQL_CliOrig  + SQL_Insumo);
    //UnDmkDAC_PF.ExecutaDB(DMod.MyDB, 'DELETE FROM pqw');

    ReopenPositENegat();
    DqPosit.First;
    //Contador := 0;
    //
    DqPositDataX       := 0;
    DqPositOrigemCodi  := 0;
    DqPositOrigemCtrl  := 0;
    DqPositTipo        := -1;
    DqPositEmpresa     := 0;
    DqPositCliOrig     := 0;
    DqPositInsumo      := 0;
    DqPositPeso        := 0.000;
    DqPositValor       := 0.00;
    //
    DqNegatDataX       := USQLDB.DtHIdx(DqNegat, 0);
    DqNegatOrigemCodi  := USQLDB.IntIdx(DqNegat, 1);
    DqNegatOrigemCtrl  := USQLDB.IntIdx(DqNegat, 2);
    DqNegatTipo        := USQLDB.IntIdx(DqNegat, 3);
    DqNegatEmpresa     := USQLDB.IntIdx(DqNegat, 4);
    DqNegatCliOrig     := USQLDB.IntIdx(DqNegat, 5);
    DqNegatInsumo      := USQLDB.IntIdx(DqNegat, 6);
    DqNegatPeso        := -USQLDB.FluIdx(DqNegat, 7);
    DqNegatValor       := -USQLDB.FluIdx(DqNegat, 8);
    //
    SdoNegPeso         := DqNegatPeso;
    SdoNegValr         := DqNegatValor;
    //
    while not DqPosit.Eof do
    begin
      //Contador := Contador + 1;
      //
      AntEmpresa := DqPositEmpresa;
      AntCliOrig := DqPositCliOrig;
      AntInsumo  := DqPositInsumo;
      //
      DqPositDataX       := USQLDB.DtHIdx(DqPosit, 0);
      DqPositOrigemCodi  := USQLDB.IntIdx(DqPosit, 1);
      DqPositOrigemCtrl  := USQLDB.IntIdx(DqPosit, 2);
      DqPositTipo        := USQLDB.IntIdx(DqPosit, 3);
      DqPositEmpresa     := USQLDB.IntIdx(DqPosit, 4);
      DqPositCliOrig     := USQLDB.IntIdx(DqPosit, 5);
      DqPositInsumo      := USQLDB.IntIdx(DqPosit, 6);
      DqPositPeso        := USQLDB.FluIdx(DqPosit, 7);
      DqPositValor       := USQLDB.FluIdx(DqPosit, 8);
      //
      ///
//////////////////////////////////////////////////////////////////////////////////////////
      //if (DqPositCliOrig=140) and (DqPositInsumo=881) then Geral.MB_Info('140-881');
      //if (DqPositOrigemCtrl) = 3245 then Geral.MB_Info('3245');
      //if (DqPositInsumo) = 290 then Geral.MB_Info('Insumo 290');
//////////////////////////////////////////////////////////////////////////////////////////
      ///
      SdoPosPeso         := DqPositPeso;
      SdoPosValr         := DqPositValor;
      //
      AtualEmpresa       := DqPositEmpresa;
      AtualCliOrig       := DqPositCliOrig;
      AtualInsumo        := DqPositInsumo;
      //
      MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True, 'Empresa=' +
      Geral.FF0(DqPositEmpresa) + ', CliOrig=' + Geral.FF0(DqPositCliOrig) +
      ', Insumo=' + Geral.FF0(DqPositInsumo));
      //
      if (AtualEmpresa <> DqNegatEmpresa)
      or (AtualCliOrig <> DqNegatCliOrig)
      or (AtualInsumo <> DqNegatInsumo) then
      begin
        (* Loop infinito quando Posit > 0 sem baixa (produto que novo entrou e
        ainda n�o consumiu)
        while (
          (AtualEmpresa > DqNegatEmpresa)
          or (AtualCliOrig > DqNegatCliOrig)
          or (AtualInsumo > DqNegatInsumo)
        ) do
        *)
        while (AtualEmpresa = DqNegatEmpresa)
        and (AtualCliOrig = DqNegatCliOrig)
        and (AtualInsumo > DqNegatInsumo)
        and (DqNegat_EOF = False) do
        begin
          MeAvisos.Lines.Add('NEGAT - OriCodi: ' + Geral.FF0(DqNegatOrigemCodi) +
          ' e OriCtrl: ' + Geral.FF0(DqNegatOrigemCtrl) + ' CliOrig: ' +
          Geral.FF0(DqNegatCliOrig) + ' => ' + Geral.FF0(DqPositCliOrig) +
          ' e Insumo: ' + Geral.FF0(DqNegatInsumo) + ' => ' +
          Geral.FF0(DqPositInsumo));
          //
          DqNegat.Next;
          DqNegatDataX       := USQLDB.DtHIdx(DqNegat, 0);
          DqNegatOrigemCodi  := USQLDB.IntIdx(DqNegat, 1);
          DqNegatOrigemCtrl  := USQLDB.IntIdx(DqNegat, 2);
          DqNegatTipo        := USQLDB.IntIdx(DqNegat, 3);
          DqNegatEmpresa     := USQLDB.IntIdx(DqNegat, 4);
          DqNegatCliOrig     := USQLDB.IntIdx(DqNegat, 5);
          DqNegatInsumo      := USQLDB.IntIdx(DqNegat, 6);
          DqNegatPeso        := -USQLDB.FluIdx(DqNegat, 7);
          DqNegatValor       := -USQLDB.FluIdx(DqNegat, 8);
          //
          SdoNegPeso         := DqNegatPeso;
          SdoNegValr         := DqNegatValor;
          DqNegat_EOF := DqNegat.RecNo = DqNegat.RecordCount - 1; // -1 ??? Dq come�a com indice=0 no primeiro registro
        end;
      end;
      while (SdoPosPeso >= 0.0009) (*or (SdoPosValr > 0)*) do
      begin
        //
        // se continua o mesmo insumo dos mesmos CliInt X Empresa,
        if (AtualEmpresa = DqPositEmpresa)
        and (AtualCliOrig = DqPositCliOrig)
        and (AtualInsumo = DqPositInsumo) then
        begin
          // ent�o continuar a baixar da entrada (DqPosit) selecionada
          if (DqNegatEmpresa = DqPositEmpresa)
          and (DqNegatCliOrig = DqPositCliOrig)
          and (DqNegatInsumo = DqPositInsumo)
          and (DqNegat_EOF = False) then
          begin
            if (SdoPosPeso - SdoNegPeso) >= 0.0009 then // se SdoPosPeso for maior que SdoNegPeso
            begin
              DstValr  := SdoNegValr;
              PqwPeso  := SdoNegPeso;
              //PqwValor := SdoNegValr;
              if SdoPosPeso > 0 then
                PqwValor := (SdoNegPeso / SdoPosPeso) * SdoPosValr
              else
                PqwValor := 0.00;
              //GeraSqlInsPQW(Peso, Valor, OriCodi, OriCtrl, OriTipo, DstCodi, DstCtrl, DstTipo);
              {
              GeraSqlInsPQW(PqwPeso, PqwValor,
              (*Origem: Baixa*)DqNegatOrigemCodi, DqNegatOrigemCtrl, DqNegatTipo,
              (*Destino: Entrada*)DqPositOrigemCodi, DqPositOrigemCtrl, DqPositTipo);
              }
              GeraSqlInsPQW(PqwPeso, PqwValor,
              (*Origem: Baixa*)DqNegatOrigemCodi, DqNegatOrigemCtrl, DqNegatTipo,
              (*Destino: Entrada*)DqPositOrigemCodi, DqPositOrigemCtrl, DqPositTipo,
              (*Origem: Baixa*)DqNegatEmpresa, DqNegatCliOrig, DqNegatInsumo,
              (*Destino: Entrada*)DqPositEmpresa, DqPositCliOrig, DqPositInsumo,
              DstValr, DqPositDataX, DqNegatDataX);
              //
              SdoPosPeso := SdoPosPeso - PqwPeso;
              SdoPosValr := SdoPosValr - PqwValor;
              //
              SdoNegPeso := 0.000;
              SdoNegValr := 0.00;
            end else
            begin
              PqwPeso  := SdoPosPeso;
              PqwValor := SdoPosValr;
              if SdoNegPeso <> 0 then
                DstValr  := (SdoPosPeso / SdoNegPeso) * SdoNegValr
              else
                DstValr  := 0;
              //GeraSqlInsPQW(Peso, Valor, OriCodi, OriCtrl, OriTipo, DstCodi, DstCtrl, DstTipo);
              GeraSqlInsPQW(PqwPeso, PqwValor,
              (*Origem: Baixa*)DqNegatOrigemCodi, DqNegatOrigemCtrl, DqNegatTipo,
              (*Destino: Entrada*)DqPositOrigemCodi, DqPositOrigemCtrl, DqPositTipo,
              (*Origem: Baixa*)DqNegatEmpresa, DqNegatCliOrig, DqNegatInsumo,
              (*Destino: Entrada*)DqPositEmpresa, DqPositCliOrig, DqPositInsumo,
              DstValr, DqPositDataX, DqNegatDataX);
              //
              SdoPosPeso := 0.000;
              SdoPosValr := 0.00;
              //
              SdoNegPeso := SdoNegPeso - PqwPeso;
              SdoNegValr := SdoNegValr - PqwValor;
            end;
            //
            if SdoNegPeso < 0.0009 then
            begin
              DqNegat_EOF := DqNegat.RecNo = DqNegat.RecordCount - 1; // -1 ??? Dq come�a com indice=0 no primeiro registro
              if DqNegat_EOF = False then
              begin
                DqNegat.Next;
                DqNegatDataX       := USQLDB.DtHIdx(DqNegat, 0);
                DqNegatOrigemCodi  := USQLDB.IntIdx(DqNegat, 1);
                DqNegatOrigemCtrl  := USQLDB.IntIdx(DqNegat, 2);
                DqNegatTipo        := USQLDB.IntIdx(DqNegat, 3);
                DqNegatEmpresa     := USQLDB.IntIdx(DqNegat, 4);
                DqNegatCliOrig     := USQLDB.IntIdx(DqNegat, 5);
                DqNegatInsumo      := USQLDB.IntIdx(DqNegat, 6);
                DqNegatPeso        := -USQLDB.FluIdx(DqNegat, 7);
                DqNegatValor       := -USQLDB.FluIdx(DqNegat, 8);
                //
                SdoNegPeso         := DqNegatPeso;
                SdoNegValr         := DqNegatValor;
              end;
            end;
          end else
          begin
            // Atualizar Saldo da entrada!
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_PQX, False, [
            'SdoPeso', 'SdoValr'], [
            'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
            SdoPosPeso, SdoPosValr], [
            DqPositOrigemCodi, DqPositOrigemCtrl, DqPositTipo], False);
            //(**)
            // Zerar Pos!
            SdoPosPeso := 0.000;
            SdoPosValr := 0.00;

          end;
          //
        end; // if (AtualEmpresa = DqPositEmpresa) and (AtualCliOrig = DqPositCliOrig) and (AtualInsumo = DqPositInsumo) then
        //
      end; // while (SdoPosPeso > 0) or (SdoPosValr > 0) do
      //
      DqPosit.Next;
      //
    end; // while not DqPosit.Eof do
    //
    // Executa SQL dos ultimmos lan�amentos
    ExecutaSqlInsPQW();
    //
    LimpaComps();
    //
    HoraFim := DModG.ObtemAgora();
    LaTempo3.Caption := Geral.FDT(HoraFim - HoraIni3, 108);
  end; // if CkRefazBaixaPorNF.Checked then








  if CkConfirmaSdosNFs.Checked then
  begin
    MyObjects.Informa2(LaGrupo1, LaGrupo2, True, 'Atualizando (confirmando) os saldos das NFs');
    HoraIni4 := DModG.ObtemAgora();
    UnDmkDAC_PF.AbreMySQLQuery0(QrSdosNFs, Dmod.MyDB, [
    'SELECT DataX, Tipo, OrigemCodi, OrigemCtrl, ',
    'SdoPeso, SdoValr  ',
    'FROM pqx ',
    'WHERE SdoPeso<>0 ',
    'ORDER BY DataX, Tipo, OrigemCodi, OrigemCtrl ',
    '']);
    QrSdosNFs.First;
    while not QrSdosNFs.Eof do
    begin
      QrSdosNFs.Next;
      SaldoPeso := PQ_PF.AtualizaSdoPQx(Qry1, QrSdosNFsOrigemCodi.Value,
      QrSdosNFsOrigemCtrl.Value, QrSdosNFsTipo.Value);
      //ComparaA := Trunc(SaldoPeso * 1000);
      //ComparaB := Trunc(QrSdosNFsSdoPeso.Value * 1000);
      Diferenca := ABS(SaldoPeso - QrSdosNFsSdoPeso.Value);
      if Diferenca >= 0.001 then
        MeAvisos.Lines.Add('Erro Saldo: Tipo ' + Geral.FF0(QrSdosNFsTipo.Value) +
        ' Codi: ' + Geral.FF0(QrSdosNFsOrigemCodi.Value) +
        ' Ctrl: ' + Geral.FF0(QrSdosNFsOrigemCtrl.Value) +
        ' Sdo DB: ' + Geral.FFT(QrSdosNFsSdoPeso.Value, 3, siNegativo) +
        ' Sdo Calc: ' + Geral.FFT(SaldoPeso, 3, siNegativo) +
        '');
      //
      QrSdosNFs.Next;
    end;


    //
    LimpaComps();
    //
    HoraFim := DModG.ObtemAgora();
    LaTempo4.Caption := Geral.FDT(HoraFim - HoraIni4, 108);
  end;








  MyObjects.Informa2(LaGrupo1, LaGrupo2, False, '...');
  HoraFim := DModG.ObtemAgora();
  LaFim.Caption := Geral.FDT(HoraFim, 0);
  LaTempo0.Caption := Geral.FDT(HoraFim - HoraIni0, 108);
  Screen.Cursor := crDefault;
  Geral.MB_Info('A��es finalizadas!');
end;

procedure TFmPQWRefaz.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQWRefaz.ExecutaSqlInsPQW();
const
  SQL_FIELDS =  'INSERT INTO pqw (Peso, Valor, OriEmpresa, OriCliInt, ' +
  'OriInsumo, DstEmpresa, DstCliInt, DstInsumo, DstValr, OriCodi, OriCtrl, ' +
  'OriTipo, DstCodi, DstCtrl, DstTipo, DataE, DataX) VALUES ';
begin
  if (FSQL_StatemCounts_Pqw > 0) and (FSQL_Values_Pqw <> EmptyStr) then
  begin
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, SQL_FIELDS + FSQL_Values_Pqw);
    FSQL_StatemCounts_Pqw := 0;
    FSQL_Values_Pqw := EmptyStr;
  end;
end;

procedure TFmPQWRefaz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQWRefaz.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPQWRefaz.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQWRefaz.GeraSqlInsPQW(Peso, Valor: Double; OriCodi, OriCtrl,
  OriTipo, DstCodi, DstCtrl, DstTipo: Integer; OriEmpresa, OriCliInt, OriInsumo,
  DstEmpresa, DstCliInt, DstInsumo: Integer; DstValr: Double; DataE, DataX:
  TDateTime);
(*
const
  SQL_FIELDS =  'INSERT INTO pqw (Peso, Valor, OriEmpresa, OriCliInt, ' +
  'OriInsumo, DstEmpresa, DstCliInt, DstInsumo, DstValr, OriCodi, OriCtrl, ' +
  'OriTipo, DstCodi, DstCtrl, DstTipo, DataE, DataX) VALUES ';
*)
begin
  //criar campos de Empresa, CliInt e Insumo para poder deletar mais r�pido!
  FSQL_StatemCounts_Pqw := FSQL_StatemCounts_Pqw + 1;
  //
  if FSQL_Values_Pqw <> EmptyStr then
    FSQL_Values_Pqw := FSQL_Values_Pqw + ', ';
  //
  FSQL_Values_Pqw := FSQL_Values_Pqw + '(' +
  Geral.FFT_Dot(Peso, 3, siNegativo) + ', ' +  //Peso
  Geral.FFT_Dot(Valor, 2, siNegativo) + ', ' + // Valor
  Geral.FF0(OriEmpresa) + ', ' + // OriEmpresa
  Geral.FF0(OriCliInt) + ', ' +  // OriCliInt
  Geral.FF0(OriInsumo) + ', ' +  //OriInsumo
  Geral.FF0(DstEmpresa) + ', ' + // DstEmpresa
  Geral.FF0(DstCliInt) + ', ' + // DstCliInt
  Geral.FF0(DstInsumo) + ', ' + // DstInsumo
  Geral.FFT_Dot(DstValr, 2, siNegativo) + ', ' + // DstValr
  Geral.FF0(OriCodi) + ', ' + // OriCodi
  Geral.FF0(OriCtrl) + ', ' + // OriCtrl
  Geral.FF0(OriTipo) + ', ' + // OriTipo
  Geral.FF0(DstCodi) + ', ' + // DstCodi
  Geral.FF0(DstCtrl) + ', ' + // DstCtrl
  Geral.FF0(DstTipo) + ', "' + // DstTipo
  Geral.FDT(DataE, 1) + '", "' + // DataE
  Geral.FDT(DataX, 1) + '") '; // DataX
  //
  if FSQL_StatemCounts_Pqw >= 1000 then
    ExecutaSqlInsPQW();
end;

procedure TFmPQWRefaz.IncluiDiferencadeResgate(FatID: Integer; AntPeso, AntValor: Double);
const
  sProcName = 'TFmPQWRefaz.IncluiDiferencadeResgate()';
  HowLoad = Integer(TPQEHowLoad.pqehlPQWRefaz); // 5
  DtCorrApo = '0000-00-00';
  Tipo = VAR_FATID_0000;
  Unitario = False;
  ide_serie = 0;
  ide_nNF = 0;
  xLote = '';
  dFab = '0000-00-00';
  dVal = '0000-00-00';
var
  DataX: String;
  CliOrig, CliDest, Insumo: Integer;
  Peso, Valor: Double;
  Empresa, OrigemCodi, OrigemCtrl, TmpPer: Integer;
  //
  Controle, CliInt, OriCodi, Codigo, FatID_Controle: Integer;
begin
  //DataX       := Geral.FDT(QrItensDataX.Value, 1);
  DataX       := Geral.FDT(FDataX, 1);
  CliOrig     := QrPQsCliOrig.Value;
  CliDest     := QrPQsCliOrig.Value;
  Insumo      := QrPQsInsumo.Value;
  Peso        := QrItensPerPeso.Value - AntPeso;
  Valor       := QrItensPerValor.Value - AntValor;
  OrigemCodi  := QrItensPeriodo.Value;
  Empresa     := QrPQsEmpresa.Value;
  TmpPer      := QrItensPeriodo.Value;
  //
  CliInt      := CliOrig;
  OriCodi     := OrigemCodi;
  Codigo      := OrigemCodi;
  //
  case RGInclusao.ItemIndex of
    0: // Um a um
    begin
      OrigemCtrl := Trunc(UMyMod.BuscaEmLivreY_Double(
          Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta'));
      case FatID of
        VAR_FATID_0020:
        begin
          Controle := UMyMod.BPGS1I32('pqbadx', 'Controle', '', '', tsPos, stIns, Controle);
          (*
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqbadx', False, [
          'Codigo', 'Empresa', 'CliInt',
          'Insumo', 'Peso', 'Valor',
          'ide_serie', 'ide_nNF', 'DtCorrApo',
          'xLote', 'dFab', 'dVal'], [
          'Controle'], [
          Codigo, Empresa, CliInt,
          Insumo, Peso, Valor,
          ide_serie, ide_nNF, DtCorrApo,
          xLote, dFab, dVal], [
          Controle], True) then
          *)
          begin
            // Lan�amento a positivo para controlar estoque de NF (e lote)
            InserePQx(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
              (*OriCtrl*)Controle, (*OriTipo*)FatID, Unitario, DtCorrApo, Empresa, HowLoad, TmpPer);
            // Lan�amento para netralizar o lan�amento a positivo
            InserePQx(DataX, CliOrig, CliDest, Insumo, -Peso, -Valor, OrigemCodi,
            OrigemCtrl, Tipo, Unitario, DtCorrApo, Empresa, HowLoad, TmpPer);
          end;
        end;
        VAR_FATID_0120:
        begin
          Controle := UMyMod.BPGS1I32('pqbbxa', 'Controle', '', '', tsPos, stIns, Controle);
          (*
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqbbxa', False, [
          'Codigo', 'Empresa', 'CliInt',
          'Insumo', 'Peso', 'Valor',
          'DtCorrApo'], [
          'Controle'], [
          Codigo, Empresa, CliInt,
          Insumo, Peso, Valor,
          DtCorrApo], [
          Controle], True) then
          *)
          begin
            // Lan�amento a negativo para controlar estoque de NF (e lote)
            InserePQx(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
              (*OriCtrl*)Controle, (*OriTipo*)FatID, Unitario, DtCorrApo, Empresa, HowLoad, TmpPer);
            // Lan�amento para netralizar o lan�amento a negativo
            InserePQx(DataX, CliOrig, CliDest, Insumo, -Peso, -Valor, OrigemCodi,
            OrigemCtrl, Tipo, Unitario, DtCorrApo, Empresa, HowLoad, TmpPer);
          end;
        end;
        else
          Geral.MB_Erro('"FatID" n�o implementado em ' + sProcName);
      end;
    end;
    1: // Por lote
    begin
      if FOrigemCtrl = 0 then
      begin
        //FOrigemCtrl := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta')
        FOrigemCtrl := Trunc(UMyMod.BuscaEmLivreY_Double(
          Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta'));
        UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
        'SELECT MAX(OrigemCtrl) FROM pqx  ',
        'WHERE Tipo=0  ',
        '']);
        if Dmod.QrAux.Fields[0].AsInteger >= FOrigemCtrl then
          FOrigemCtrl := Dmod.QrAux.Fields[0].AsInteger + 1;
      end
      else
        FOrigemCtrl := FOrigemCtrl + 1;
      //
      if FatID = VAR_FATID_0020 then
      begin
        if FControle_PQBAdx = 0 then
          FControle_PQBAdx := UMyMod.BPGS1I32('pqbadx', 'Controle', '', '', tsPos, stIns, FControle_PQBAdx)
        else
          FControle_PQBAdx := FControle_PQBAdx + 1;
        //
        FatID_Controle := FControle_PQBAdx;
      end else
      if FatID = VAR_FATID_0120 then
      begin
        if FControle_PQBBxa = 0 then
          FControle_PQBBxa := UMyMod.BPGS1I32('pqbbxa', 'Controle', '', '', tsPos, stIns, FControle_PQBBxa)
        else
          FControle_PQBBxa := FControle_PQBBxa + 1;
        //
        FatID_Controle := FControle_PQBBxa;
      end;
      //
      if FSQL_Values_Pqx <> EmptyStr then
        FSQL_Values_Pqx := FSQL_Values_Pqx + ', ';
      //
{          InserePQx(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
              (*OriCtrl*)Controle, (*OriTipo*)FatID, Unitario, DtCorrApo, Empresa, HowLoad, TmpPer);
            // Lan�amento para netralizar o lan�amento a negativo
            InserePQx(DataX, CliOrig, CliDest, Insumo, -Peso, -Valor, OrigemCodi,
            OrigemCtrl, Tipo, Unitario, DtCorrApo, Empresa, HowLoad, TmpPer);
}
      FSQL_Values_Pqx := FSQL_Values_Pqx +
      //
      '("' + DataX + '",' + Geral.FF0(CliOrig) + ', ' + Geral.FF0(CliDest) + ', ' +
      Geral.FF0(Insumo) + ', ' + Geral.FFT_Dot(Peso, 3, siNegativo) + ', ' +
      Geral.FFT_Dot(Valor, 2, siNegativo) + ', ' + Geral.FF0(HowLoad) + ', ' +
      Geral.FF0(Empresa) + ', ' + Geral.FF0(TmpPer) + ', ' +
      Geral.FF0(OriCodi) + ', ' + Geral.FF0(FatID_Controle) + ', ' +
      Geral.FF0(FatID) + ')' +
      //
      ', ("' + DataX + '",' + Geral.FF0(CliOrig) + ', ' + Geral.FF0(CliDest) + ', ' +
      Geral.FF0(Insumo) + ', ' + Geral.FFT_Dot(-Peso, 3, siNegativo) + ', ' +
      Geral.FFT_Dot(-Valor, 2, siNegativo) + ', ' + Geral.FF0(HowLoad) + ', ' +
      Geral.FF0(Empresa) + ', ' + Geral.FF0(TmpPer) + ', ' +
      Geral.FF0(OrigemCodi) + ', ' + Geral.FF0(FOrigemCtrl) + ', ' +
      Geral.FF0(Tipo) + ')';
    end;
  end;
end;

procedure TFmPQWRefaz.InserePorLote();
const
  SQL_FIELDS =  'INSERT INTO pqx (DataX, CliOrig, CliDest, Insumo, Peso, ' +
  'Valor, HowLoad, Empresa, TmpPer, OrigemCodi, OrigemCtrl, Tipo) VALUES ';
begin
  if FSQL_Values_Pqx <> EmptyStr then
  begin
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, SQL_FIELDS + FSQL_Values_Pqx);
    //
    if FControle_PQBAdx > 0 then
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE gerlseq1 SET BigIntPos=' +
        Geral.FF0(FControle_PQBAdx) + ' WHERE Tabela="pqbadx" AND Campo="Controle"' +
        ' AND BigIntPos<' + Geral.FF0(FControle_PQBAdx));
    //
    if FControle_PQBBxa > 0 then
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE gerlseq1 SET BigIntPos=' +
      Geral.FF0(FControle_PQBBxa) + ' WHERE Tabela="pqbbxa" AND Campo="Controle"' +
        ' AND BigIntPos<' + Geral.FF0(FControle_PQBBxa));
    //
    if FOrigemCtrl > 0 then
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE controle SET BalancosIts=' +
      Geral.FF0(FOrigemCtrl) + ' WHERE BalancosIts<' + Geral.FF0(FOrigemCtrl));
    //
  end;
end;

procedure TFmPQWRefaz.InserePQX(DataX: String; CliOrig, CliDest, Insumo: Integer;
  Peso, Valor: Double; OrigemCodi, OrigemCtrl, Tipo: Integer; Unitario:
  Boolean; DtCorrApo: String; _Empresa, HowLoad, TmpPer: Integer);
var
  Empresa: Integer;
begin
  //Result  := False;
  Empresa := _Empresa;
  if Empresa = 0 then
  begin
    Empresa := -11;
    Geral.MB_Erro('Empresa n�o definida! Ser� considerado -11!');
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_INS_TAB_PQX, False, [
  'DataX', 'CliOrig', 'CliDest',
  'Insumo', 'Peso', 'Valor',(*,
  'Retorno', 'StqMovIts', 'RetQtd',*)
  'HowLoad' (*'StqCenLoc', 'SdoPeso',
  'SdoValr', 'AcePeso', 'AceValr'*),
  'DtCorrApo', 'Empresa', 'TmpPer'], [
  'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
  DataX, CliOrig, CliDest,
  Insumo, Peso, Valor,(*,
  Retorno, StqMovIts, RetQtd,*)
  HowLoad (*StqCenLoc, SdoPeso,
  SdoValr, AcePeso, AceValr*),
  DtCorrApo, Empresa, TmpPer], [
  OrigemCodi, OrigemCtrl, Tipo], False) then
  begin
    //AtualizaSdoPQx(Query, OrigemCodi, OrigemCtrl, Tipo);
    //Result := True;
  end;
end;

procedure TFmPQWRefaz.PesquisaTodosPQx();
begin
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DROP TABLE IF EXISTS _pqw_aj_per ',
  '; ',
  'CREATE TABLE _pqw_aj_per ',
  'SELECT Empresa, CliOrig, DataX, Insumo, OrigemCodi,  ',
  'SUM(Peso) Peso, SUM(Valor) Valor  ',
  'FROM ' + TMeuDB + '.pqx ',
  'WHERE Tipo=0  ',
  //'AND Empresa=' + Geral.FF0(QrEmpresasEmpresa.Value),
  'GROUP BY Empresa, CliOrig, OrigemCodi, Insumo',
  'ORDER BY Empresa, CliOrig, OrigemCodi, Insumo ',
  '; ',
  'DROP TABLE IF EXISTS _pqw_aj_its ',
  '; ',
  'CREATE TABLE _pqw_aj_its ',
  'SELECT Empresa, CliOrig,  Insumo, TmpPer, ',
  'SUM(Peso) Peso, SUM(Valor) Valor ',
  'FROM ' + TMeuDB + '.pqx ',
  //'WHERE Empresa=' + Geral.FF0(QrEmpresasEmpresa.Value),
  'GROUP BY Empresa, CliOrig, Insumo, TmpPer ',
  'ORDER BY Empresa, CliOrig, Insumo, TmpPer ',
  '; ',
  '']));
end;

procedure TFmPQWRefaz.QrItensAfterScroll(DataSet: TDataSet);
begin
  FDataX := Geral.PTD(QrItensPeriodo.Value, 1);
end;

procedure TFmPQWRefaz.QrItensCalcFields(DataSet: TDataSet);
begin
  //QrItensDataX.Value := Geral.PTD(QrItensPeriodo.Value, 1);
end;

procedure TFmPQWRefaz.ReopenEmpresas();
var
  CliOrig: Integer;
  SQL_CliOrig: String;
begin
  CliOrig := EdCliOrig.ValueVariant;
  if CliOrig <> 0 then
    SQL_CliOrig := 'WHERE CliOrig=' + Geral.FF0(CliOrig)
  else
    SQL_CliOrig := 'WHERE CliOrig <> 0 ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresas, Dmod.MyDB, [
  'SELECT Empresa, CliOrig, COUNT(Insumo) ITENS  ',
  'FROM pqx ',
  // teste aqui!!!!
  SQL_CliOrig,
  'GROUP BY Empresa, CliOrig ',
  'ORDER BY Empresa, CliOrig ',
  '']);
end;

procedure TFmPQWRefaz.ReopenItsDq(CliOrig, Insumo: Integer);
begin
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DROP TABLE IF EXISTS _pqw_aj_per ',
  '; ',
  'CREATE TABLE _pqw_aj_per ',
  'SELECT DataX, OrigemCodi,  ',
  'SUM(Peso) Peso, SUM(Valor) Valor  ',
  'FROM ' + TMeuDB + '.pqx  ',
  'WHERE Tipo=0  ',
  'AND Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
  'AND CliOrig=' + Geral.FF0(CliOrig),
  'AND Insumo=' + Geral.FF0(Insumo),
  'GROUP BY OrigemCodi  ',
  'ORDER BY OrigemCodi ',
  '; ',
  'DROP TABLE IF EXISTS _pqw_aj_its ',
  '; ',
  'CREATE TABLE _pqw_aj_its ',
  'SELECT Insumo, TmpPer, ',
  'SUM(Peso) Peso, SUM(Valor) Valor ',
  'FROM ' + TMeuDB + '.pqx ',
  'WHERE Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
  'AND CliOrig=' + Geral.FF0(CliOrig),
  'AND Insumo=' + Geral.FF0(Insumo),
  'GROUP BY Insumo, TmpPer ',
  '; ',
  '']));
  //
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqItens, DmodG.MyPID_DB, [
  'SELECT bal.Periodo,  ',
  'per.Peso PerPeso, per.Valor PerValor,  ',
  'its.Peso ItsPeso, its.Valor ItsValor  ',
  'FROM ' + TMeuDB + '.balancos bal ',
  'LEFT JOIN _pqw_aj_per per ON per.OrigemCodi=bal.Periodo ',
  'LEFT JOIN _pqw_aj_its its ON its.TmpPer=bal.Periodo ',
  'ORDER BY bal.Periodo ',
  '']);
  PB3.Position := 0;
  PB3.Max := DqItens.RecordCount;
end;

procedure TFmPQWRefaz.ReopenItsQr();
begin
  case RGPesquisa.ItemIndex of
    0:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrItens, Dmod.MyDB, [
      'DROP TABLE IF EXISTS _pqw_aj_per ',
      '; ',
      'CREATE TABLE _pqw_aj_per ',
      'SELECT DataX, OrigemCodi,  ',
      'SUM(Peso) Peso, SUM(Valor) Valor  ',
      'FROM ' + TMeuDB + '.pqx  ',
      'WHERE Tipo=0  ',
      'AND Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
      'AND CliOrig=' + Geral.FF0(QrPQsCliOrig.Value),
      'AND Insumo=' + Geral.FF0(QrPQsInsumo.Value),
      'GROUP BY OrigemCodi  ',
      'ORDER BY OrigemCodi ',
      '; ',
      'DROP TABLE IF EXISTS _pqw_aj_its ',
      '; ',
      'CREATE TABLE _pqw_aj_its ',
      'SELECT Insumo, TmpPer, ',
      'SUM(Peso) Peso, SUM(Valor) Valor ',
      'FROM ' + TMeuDB + '.pqx ',
      'WHERE Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
      'AND CliOrig=' + Geral.FF0(QrPQsCliOrig.Value),
      'AND Insumo=' + Geral.FF0(QrPQsInsumo.Value),
      'GROUP BY Insumo, TmpPer ',
      '; ',
      ' ',
      'SELECT bal.Periodo,  ',
      'per.Peso PerPeso, per.Valor PerValor,  ',
      'its.Peso ItsPeso, its.Valor ItsValor  ',
      'FROM ' + TMeuDB + '.balancos bal ',
      'LEFT JOIN _pqw_aj_per per ON per.OrigemCodi=bal.Periodo ',
      'LEFT JOIN _pqw_aj_its its ON its.TmpPer=bal.Periodo ',
      'ORDER BY bal.Periodo ',
      '']);
      //
      //Geral.MB_Teste(QrItens.SQL.Text);
    end;
    1:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrItens, DModG.MyPID_DB, [
(*
      'SELECT bal.Periodo,  ',
      'per.Peso PerPeso, per.Valor PerValor,  ',
      'its.Peso ItsPeso, its.Valor ItsValor  ',
      'FROM ' + TMeuDB + '.balancos bal ',
      'LEFT JOIN _pqw_aj_per per ON per.OrigemCodi=bal.Periodo ',
      'LEFT JOIN _pqw_aj_its its ON its.TmpPer=bal.Periodo ',
      'WHERE per.Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
      'AND its.Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
      'AND per.CliOrig=' + Geral.FF0(QrPQsCliOrig.Value),
      'AND its.CliOrig=' + Geral.FF0(QrPQsCliOrig.Value),
      'AND per.Insumo=' + Geral.FF0(QrPQsInsumo.Value),
      'AND its.Insumo=' + Geral.FF0(QrPQsInsumo.Value),
      'ORDER BY bal.Periodo ',
*)
(*
      'SELECT bal.Periodo,  ',
      'per.Peso PerPeso, per.Valor PerValor,  ',
      'its.Peso ItsPeso, its.Valor ItsValor  ',
      'FROM ' + TMeuDB + '.balancos bal ',
      'LEFT JOIN _pqw_aj_per per ON per.OrigemCodi=bal.Periodo',
      '  AND per.Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
      '  AND per.CliOrig=' + Geral.FF0(QrPQsCliOrig.Value),
      '  AND per.Insumo=' + Geral.FF0(QrPQsInsumo.Value),
      'LEFT JOIN _pqw_aj_its its ON its.TmpPer=bal.Periodo ',
      '  AND its.Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
      '  AND its.CliOrig=' + Geral.FF0(QrPQsCliOrig.Value),
      '  AND its.Insumo=' + Geral.FF0(QrPQsInsumo.Value),
      'ORDER BY bal.Periodo ',
      '']);
*)
      'DROP TABLE IF EXISTS _teste_1;',
      'CREATE TABLE _teste_1',
      'SELECT * FROM _pqw_aj_per',
      'WHERE Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
      'AND CliOrig=' + Geral.FF0(QrPQsCliOrig.Value),
      'AND Insumo=' + Geral.FF0(QrPQsInsumo.Value),
      ';',
      'DROP TABLE IF EXISTS _teste_2;',
      'CREATE TABLE _teste_2',
      'SELECT * FROM _pqw_aj_its',
      'WHERE Empresa=' + Geral.FF0(QrPQsEmpresa.Value),
      'AND CliOrig=' + Geral.FF0(QrPQsCliOrig.Value),
      'AND Insumo=' + Geral.FF0(QrPQsInsumo.Value),
      ';',
      'SELECT bal.Periodo,  ',
      'per.Peso PerPeso, per.Valor PerValor,  ',
      'its.Peso ItsPeso, its.Valor ItsValor  ',
      'FROM ' + TMeuDB + '.balancos bal ',
      'LEFT JOIN _teste_1 per ON per.OrigemCodi=bal.Periodo',
      'LEFT JOIN _teste_2 its ON its.TmpPer=bal.Periodo ',
      'ORDER BY bal.Periodo ',
      '']);

      //Geral.MB_Teste(QrItens.SQL.Text);
    end;
  end;
  //Geral.MB_Teste(QrItens.SQL.Text);
  PB3.Position := 0;
  PB3.Max := QrItens.RecordCount;
end;

procedure TFmPQWRefaz.ReopenPositENegat();
var
  Insumo: Integer;
  SQL_Insumo: String;
  CliOrig: Integer;
  SQL_CliOrig: String;
begin
  CliOrig := EdCliOrig.ValueVariant;
  if CliOrig <> 0 then
    SQL_CliOrig := 'AND CliOrig=' + Geral.FF0(CliOrig)
  else
    SQL_CliOrig := 'AND CliOrig<>0';
  //
  Insumo := EdInsumo.ValueVariant;
  if Insumo <> 0 then
    SQL_Insumo := 'AND Insumo=' + Geral.FF0(Insumo)
  else
    SQL_Insumo := 'AND Insumo>0';
  //
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqPosit, Dmod.MyDB, [
  'SELECT DataX, OrigemCodi, OrigemCtrl, Tipo,',
  'Empresa, CliOrig, Insumo, Peso, Valor,',
  'FLOOR(ABS(Peso)) PesoI, ',
  '((ABS(Peso) -FLOOR(ABS(Peso))) * 1000) PesoF,',
  'FLOOR(ABS(Valor)) ValorI, ',
  '((ABS(Valor) -FLOOR(ABS(Valor))) * 100) ValorF ',
  'FROM pqx',
  'WHERE Tipo>0',
  'AND (Peso>0 OR (Peso=0 AND Valor > 0))',
  SQL_CliOrig,
  SQL_Insumo,
  'ORDER BY Empresa, CliOrig, Insumo, DataX, OrigemCtrl',
  '']);
  //
  DqPosit.First;
  PB2.Max := DqPosit.RecordCount;
  //
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqNegat, Dmod.MyDB, [
  'SELECT DataX, OrigemCodi, OrigemCtrl, Tipo,',
  'Empresa, CliOrig, Insumo, Peso, Valor,',
  'FLOOR(ABS(Peso)) PesoI, ',
  '((ABS(Peso) -FLOOR(ABS(Peso))) * 1000) PesoF,',
  'FLOOR(ABS(Valor)) ValorI, ',
  '((ABS(Valor) -FLOOR(ABS(Valor))) * 100) ValorF ',
  'FROM pqx',
  'WHERE Tipo>0',
  'AND (Peso<0 OR (Peso=0 AND Valor < 0))',
  SQL_CliOrig,
  SQL_Insumo,
  'ORDER BY Empresa, CliOrig, Insumo, DataX, OrigemCtrl ',
  '']);
  //
  DqNegat.First;
end;

procedure TFmPQWRefaz.ReopenPQsDq();
var
  Insumo: Integer;
  SQL_Insumo: String;
begin
  Insumo := EdInsumo.ValueVariant;
  if Insumo <> 0 then
    SQL_Insumo := 'WHERE pq_.Codigo=' + Geral.FF0(Insumo)
  else
    SQL_Insumo := 'WHERE pq_.Codigo>0';
  //
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqPQs, Dmod.MyDB, [
  'SELECT pqx.Empresa, pqx.CliOrig, pqx.Insumo, pq_.Nome NO_PQ ',
  'FROM pqx ',
  'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo ',
  // teste aqui!!!!
  SQL_Insumo,                                               // Dermascal U (21) Ecoservice -11
  'AND pqx.Empresa=' + Geral.FF0(QrEmpresasEmpresa.Value),  // 149 Actypicle
  'AND pqx.CliOrig=' + Geral.FF0(QrEmpresasCliOrig.Value),  // 44 ATS 43
  'GROUP BY pqx.Insumo ',                                   // 146 Coripol SLG
  'ORDER BY pqx.Insumo ',                                   // �cido F�rmico (13) Apucarana (2)
  '']);
  //
  PB2.Position := 0;
  PB2.Max := DqPQs.RecordCount;
end;

procedure TFmPQWRefaz.ReopenPQsQr();
var
  Insumo: Integer;
  SQL_Insumo: String;
begin
  Insumo := EdInsumo.ValueVariant;
  if Insumo <> 0 then
    SQL_Insumo := 'WHERE pq_.Codigo=' + Geral.FF0(Insumo)
  else
    SQL_Insumo := 'WHERE pq_.Codigo>0';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQs, Dmod.MyDB, [
  'SELECT pqx.Empresa, pqx.CliOrig, pqx.Insumo, pq_.Nome NO_PQ ',
  'FROM pqx ',
  'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo ',
  // teste aqui!!!!
  SQL_Insumo,                                               // Dermascal U (21) Ecoservice -11
  'AND pqx.Empresa=' + Geral.FF0(QrEmpresasEmpresa.Value),  // 149 Actypicle
  'AND pqx.CliOrig=' + Geral.FF0(QrEmpresasCliOrig.Value),  // 44 ATS 43
  'GROUP BY pqx.Insumo ',                                   // 146 Coripol SLG
  'ORDER BY pqx.Insumo ',                                   // �cido F�rmico (13) Apucarana (2)
  '']);
  //
  PB2.Position := 0;
  PB2.Max := QrPQs.RecordCount;
end;

procedure TFmPQWRefaz.VerificaPeriodos();
var
  Periodo, GrupoBal, Empresa: Integer;
  sAbertura: String;
begin
  MyObjects.Informa2(LaTit1, LaTit2, True,  'Verificando integridade de per�odos de balan�os.');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
  'SELECT bal.Periodo, ',
  'pqx.OrigemCodi, pqx.Empresa, COUNT(pqx.Insumo) ITENS  ',
  'FROM pqx pqx ',
  'LEFT JOIN balancos bal ON bal.Periodo=pqx.OrigemCodi ',
  'WHERE pqx.Tipo=0 ',
  'AND bal.Periodo IS NULL ',
  'GROUP BY pqx.OrigemCodi, pqx.Empresa ',
  '']);
  //
  if Qry1.RecordCount > 0 then
  begin
    MyObjects.Informa2(LaTit1, LaTit2, True,  'Verificando integridade de per�odos de balan�os.');
    Qry1.First;
    while not Qry1.Eof do
    begin
      Periodo := Qry1.FieldByName('OrigemCodi').AsInteger;
      Empresa := Qry1.FieldByName('Empresa').AsInteger;
      //
      sAbertura := dmkPF.PrimeiroDiaDoPeriodo(Periodo, dtSystem);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
      'SELECT GrupoBal',
      'FROM stqbalcad ',
      'WHERE Abertura="' + sAbertura + '"',
      '']);
      //
      GrupoBal := Qry2.FieldByName('GrupoBal').AsInteger;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'balancos', False, [
      'Periodo', 'GrupoBal', 'Empresa'], [
      ], [
      Periodo, GrupoBal, Empresa], [
      ], True) then
      begin
        //
      end;
      //
      Qry1.Next;
    end;
  end;

end;

end.
