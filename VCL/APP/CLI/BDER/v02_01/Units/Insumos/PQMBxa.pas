unit PQMBxa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  UnProjGroup_Consts;

type
  TFmPQMBxa = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdOrigemCtrl: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrSaldo: TmySQLQuery;
    QrSaldoPeso: TFloatField;
    QrSaldoValor: TFloatField;
    QrSaldoCUSTO: TFloatField;
    QrSaldoCI: TIntegerField;
    QrSaldoPQ: TIntegerField;
    DsSaldo: TDataSource;
    QrPQ: TmySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsPQ: TDataSource;
    QrCI: TmySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    DsCI: TDataSource;
    Label12: TLabel;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdPesoAdd: TdmkEdit;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    DBEdit7: TDBEdit;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit8: TDBEdit;
    Label19: TLabel;
    DBEdit9: TDBEdit;
    Label20: TLabel;
    EdSaldoFut: TdmkEdit;
    Label22: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPQChange(Sender: TObject);
    procedure EdCIChange(Sender: TObject);
    procedure EdPesoAddExit(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenIts(OrigemCtrl: Integer);
    procedure CalculaSaldoFuturo();
  public
    { Public declarations }
    FDataB: TDateTime;
    FEmpresa, FCodigo: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmPQMBxa: TFmPQMBxa;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral, UnPQ_PF, PQMCab, PQx;

{$R *.DFM}

procedure TFmPQMBxa.BtOKClick(Sender: TObject);
const
  Unitario = True;
var
  OrigemCtrl: Integer;
  KG, RS, SF: Double;
  //
  DataX, DtCorrApo: String;
  OrigemCodi, Tipo, CliOrig, CliDest, Insumo, Empresa: Integer;
  Peso, Valor: Double;
begin
  if MyObjects.FIC(EdPesoAdd.ValueVariant <= 0, EdPesoAdd, 'Quantidade inv�lida!') then Exit;
  Insumo := EdPQ.ValueVariant;
  if Insumo <> -2 (*�gua*) then
  begin
    if MyObjects.FIC(QrSaldo.RecordCount = 0, nil,
    'Insumo / cliente interno n�o definido!') then
      Exit;
    SF := EdSaldoFut.ValueVariant;
    //
    if MyObjects.FIC((SF < 0) and (EdCI.ValueVariant < 0), EdPesoAdd,
    'Quantidade insuficiente no estoque!') then
      Exit;
    KG      := EdPesoAdd.ValueVariant;
    RS      := KG * QrSaldoCUSTO.Value;
    //
    Insumo     := QrSaldoPQ.Value;
    Peso       := -KG;
    Valor      := -RS;
    DtCorrApo  := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
    //
  end else
  begin
    Peso       := -EdPesoAdd.ValueVariant;
    Valor      := 0;
    DtCorrApo  := Geral.FDT(EncodeDate(1899, 12, 30), 1);
  end;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  DataX      := Geral.FDT(FDataB, 1);
  OrigemCodi := FCodigo;
  Tipo       := VAR_FATID_0130;
  CliOrig    := EdCI.ValueVariant;
  CliDest    := EdCI.ValueVariant;
  //
  OrigemCtrl := DModG.BuscaProximoInteiro(CO_FLD_TAB_PQX, 'OrigemCtrl', 'Tipo', VAR_FATID_0130);
  //
  PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OrigemCodi,
    OrigemCtrl, Tipo, Unitario, DtCorrApo, Empresa);
  //
  if Insumo <> -2 (*�gua*) then
  begin
    UnPQx.AtualizaEstoquePQ(QrSaldoCI.Value, QrSaldoPQ.Value, Empresa, aeMsg, '');
    QrSaldo.Close;
    UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
  end;
  FmPQMCab.ReopenPQMBxa(OrigemCtrl);
  FmPQMCab.AtualizaCusto();
  if CkContinuar.Checked then
  begin
    EdPesoAdd.ValueVariant := 0;
    EdPQ.ValueVariant := 0;
    CBPQ.KeyValue := NULL;
    EdPQ.SetFocus;
  end else
    Close;
end;

procedure TFmPQMBxa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQMBxa.CalculaSaldoFuturo();
var
  CI, PQ: Integer;
  PesoAdd, SaldoFut: Double;
begin
  CI := EdCI.ValueVariant;
  PQ := EdPQ.ValueVariant;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSaldo, Dmod.MyDB, [
  'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO ',
  'FROM pqcli ',
  'WHERE CI=' + Geral.FF0(CI),
  'AND PQ=' + Geral.FF0(PQ),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  '']);
  //
  PesoAdd                 := EdPesoAdd.ValueVariant;
  SaldoFut                := QrSaldoPeso.Value - PesoAdd;
  EdSaldoFut.ValueVariant := SaldoFut;
end;

procedure TFmPQMBxa.EdCIChange(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmPQMBxa.EdPesoAddExit(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmPQMBxa.EdPQChange(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmPQMBxa.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmPQMBxa.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
end;

procedure TFmPQMBxa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQMBxa.ReopenIts(OrigemCtrl: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if OrigemCtrl <> 0 then
      FQrIts.Locate('OrigemCtrl', OrigemCtrl, []);
  end;
end;

{
object QrEntidades: TmySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT Codigo, CodUsu, Nome '
    'FROM enticargos'
    'ORDER BY Nome')
  Left = 216
  Top = 44
  object QrEntidadesCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrEntidadesCodUsu: TIntegerField
    FieldName = 'CodUsu'
  end
  object QrEntidadesNome: TWideStringField
    FieldName = 'Nome'
    Size = 30
  end
end
object DsEntidades: TDataSource
  DataSet = QrEntidades
  Left = 216
  Top = 92
end
}
end.
