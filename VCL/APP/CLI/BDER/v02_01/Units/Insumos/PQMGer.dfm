object FmPQMGer: TFmPQMGer
  Left = 339
  Top = 185
  Caption = 
    'QUI-RECEI-021 :: Item de Gera'#231#227'o de Dilui'#231#227'o e Mistura de Insumo' +
    's'
  ClientHeight = 393
  ClientWidth = 805
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 253
    Width = 805
    Height = 26
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 12
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 805
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 721
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 805
    Height = 141
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label12: TLabel
      Left = 96
      Top = 16
      Width = 70
      Height = 13
      Caption = 'Cliente interno:'
    end
    object Label10: TLabel
      Left = 12
      Top = 56
      Width = 277
      Height = 13
      Caption = 'Material de uso e consumo (do Grupo: Insumo modificado):'
    end
    object Label13: TLabel
      Left = 576
      Top = 56
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label22: TLabel
      Left = 416
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      Enabled = False
    end
    object Label1: TLabel
      Left = 680
      Top = 56
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object EdOrigemCtrl: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdCI: TdmkEditCB
      Left = 96
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCI
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCI: TdmkDBLookupComboBox
      Left = 152
      Top = 32
      Width = 261
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMECI'
      ListSource = DsCI
      TabOrder = 2
      dmkEditCB = EdCI
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdInsumo: TdmkEditCB
      Left = 12
      Top = 72
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBInsumo
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBInsumo: TdmkDBLookupComboBox
      Left = 68
      Top = 72
      Width = 505
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPQ
      TabOrder = 6
      dmkEditCB = EdInsumo
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdPeso: TdmkEdit
      Left = 576
      Top = 72
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object CkDtCorrApo: TCheckBox
      Left = 12
      Top = 104
      Width = 185
      Height = 17
      Caption = #201' corre'#231#227'o de apontamento. Data:'
      TabOrder = 9
    end
    object TPDtCorrApo: TdmkEditDateTimePicker
      Left = 200
      Top = 100
      Width = 112
      Height = 21
      Time = 0.833253726850671200
      Enabled = False
      TabOrder = 10
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpSPED_EFD_MIN_MAX
    end
    object EdEmpresa: TdmkEditCB
      Left = 416
      Top = 32
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Empresa'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 472
      Top = 32
      Width = 321
      Height = 21
      Enabled = False
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 4
      TabStop = False
      dmkEditCB = EdEmpresa
      QryCampo = 'Empresa'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdValor: TdmkEdit
      Left = 680
      Top = 72
      Width = 112
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 805
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 757
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 709
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 616
        Height = 32
        Caption = 'Item de Gera'#231#227'o de Dilui'#231#227'o e Mistura de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 616
        Height = 32
        Caption = 'Item de Gera'#231#227'o de Dilui'#231#227'o e Mistura de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 616
        Height = 32
        Caption = 'Item de Gera'#231#227'o de Dilui'#231#227'o e Mistura de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 279
    Width = 805
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 801
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 323
    Width = 805
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 659
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 657
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrSaldo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO '
      'FROM pqcli'
      'WHERE CI=:P0'
      'AND PQ=:P1')
    Left = 516
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSaldoPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSaldoValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSaldoCUSTO: TFloatField
      FieldName = 'CUSTO'
    end
    object QrSaldoCI: TIntegerField
      FieldName = 'CI'
    end
    object QrSaldoPQ: TIntegerField
      FieldName = 'PQ'
    end
  end
  object DsSaldo: TDataSource
    DataSet = QrSaldo
    Left = 516
    Top = 292
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq '
      'WHERE Ativo=1'
      'AND GGXNiv2 IN (-10,10)'
      'AND Codigo >0'
      'ORDER BY Nome')
    Left = 96
    Top = 240
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 96
    Top = 284
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 576
    Top = 244
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 576
    Top = 296
  end
end
