unit UnERPSinc_Tabs;
// ERPSinc - Custos e Resultados Operacionais
{ Colocar no MyListas:

Uses UnERPSinc_Tabs;


//
function TMyListas.CriaListaTabelas:
      ERPSinc_Tb.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
    ERPSinc_Tb.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:

    ERPSinc_Tb.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      ERPSinc_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      ERPSinc_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    ERPSinc_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  ERPSinc_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms, mySQLDbTables,
  UnInternalConsts, dmkGeral, UnDmkEnums;

type
  TUnERPSinc_Tabs = class(TObject)
  private
    { Private declarations }
    function DefineTabOri(const Tabela: String; var TabOri: String): Boolean;
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  ERPSinc_Tabs: TUnERPSinc_Tabs;

const
  CO_ERPSync_TbPrefix = 'ERPSync_';
  CO_ERPSync_FldOri   = 'Ori_';
  CO_ERPSync_FldDst   = 'Dst_';

implementation

uses Module, DmkDAC_PF;

function TUnERPSinc_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    //Linguas.AdTbLst(Lista, False, Lowercase('Controle'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSCacCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSEntiMP'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSEscCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSGerArtA'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSGerRclA'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSMovCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSMovIts'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSPalletA'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSPaClaCabA'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSPaClaItsA'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSPaRclCabA'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSPaRclItsA'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSPrePalCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSInnCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSOpeCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSPWECab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSTrfLocCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSOutCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSMulFrnCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSMulFrnIts'), '');
    //
    //MyLinguas.AdTbLst(Lista, False, Lowercase('???'), '_lst_sample');
    MyLinguas.AdTbLst(Lista, False, Lowercase(CO_ERPSync_TbPrefix + 'VSMovimTwn'), '');
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnERPSinc_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
{
separador = "|"
}
  Result := True;
  //end else
{
  if Uppercase(Tabela) = Uppercase('CtrlGeral') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo');
    FListaSQL.Add('1');
  end;
}
end;

function TUnERPSinc_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
{
  if Uppercase(Tabela) = Uppercase('??') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"ND"');
    FListaSQL.Add('1|"Telefone"');
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades"|"Cadastro de pessoas f�sicas e jur�dicas (clientes| fornecedores| etc.)"');
    //FListaSQL.Add('""|""');
  end;
}
end;

function TUnERPSinc_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
var
  Qry: TmySQLQuery;
  Tabori: String;
begin
  Result := True;
  if not DefineTabOri(Tabela, TabOri) then
    Exit;
  if LowerCase(TabOri) = LowerCase('VSMovimTwn') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Dst_MovimTwn';
    FLIndices.Add(FRIndices);
  end else
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SHOW TABLES FROM ' + Dmod.MyDB.DatabaseName,
      'LIKE "' + TabOri + '" ',
      '']);
      if Lowercase(Qry.Fields[0].AsString) = Lowercase(TabOri) then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SHOW INDEX FROM ' + TabOri,
        'WHERE Key_name="PRIMARY" ',
        '']);
        while not Qry.Eof do
        begin
          New(FRIndices);
          FRIndices.Non_unique    := Qry.FieldByName('Non_unique').AsInteger;
          FRIndices.Key_name      := Qry.FieldByName('Key_name').AsString;
          FRIndices.Seq_in_index  := Qry.FieldByName('Seq_in_index').AsInteger;
          FRIndices.Column_name   := CO_ERPSync_FldDst + Qry.FieldByName('Column_name').AsString;
          FLIndices.Add(FRIndices);
          //
          Qry.Next;
        end;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

function TUnERPSinc_Tabs.CarregaListaFRQeiLnk(TabelaCria: String;
  FRQeiLnk: TQeiLnk; FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TUnERPSinc_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
  TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
var
  Qry: TmySQLQuery;
  TabOri: String;
  //
  procedure AdicionaRegistro(Prefix: String; PRI: Boolean);
  begin
    New(FRCampos);
    FRCampos.Field      := Prefix + Qry.FieldByName('Field').AsString;
    FRCampos.Tipo       := Qry.FieldByName('Type').AsString;
    FRCampos.Null       := Qry.FieldByName('Null').AsString;
    //FRCampos.Key        := Qry.FieldByName('Key').AsString;
    if PRI then
      FRCampos.Key        := 'PRI'
    else
      FRCampos.Key        := '';
    FRCampos.Default    := Qry.FieldByName('Default').AsString;
    FRCampos.Extra      := Qry.FieldByName('Extra').AsString;
    FLCampos.Add(FRCampos);
  end;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    if not DefineTabOri(Tabela, TabOri) then
      Exit;
    if LowerCase(TabOri) = LowerCase('VSMovimTwn') then
    begin
      New(FRCampos);
      FRCampos.Field      := 'Dst_MovimTwn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ori_MovimTwn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SHOW TABLES FROM ' + Dmod.MyDB.DatabaseName,
        'LIKE "' + TabOri + '" ',
        '']);
        if Lowercase(Qry.Fields[0].AsString) = Lowercase(TabOri) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SHOW COLUMNS FROM ' + TabOri,
          '']);
          while not Qry.Eof do
          begin
            if Qry.FieldByName('Key').AsString = 'PRI' then
            begin
              AdicionaRegistro(CO_ERPSync_FldDst, True);
              AdicionaRegistro(CO_ERPSync_FldOri, False);
            end;
            //
            Qry.Next;
          end;
        end;
      except
        Qry.Free;
      end;
    end;
    //
    New(FRCampos);
    FRCampos.Field      := 'AtrelIns';
    FRCampos.Tipo       := 'tinyint(3)';
    FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
    FRCampos.Key        := '';
    FRCampos.Default    := '0';
    FRCampos.Extra      := '';
    FLCampos.Add(FRCampos);
    //
    New(FRCampos);
    FRCampos.Field      := 'LoadCRCCod';
    FRCampos.Tipo       := 'int(11)';
    FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
    FRCampos.Key        := '';
    FRCampos.Default    := '0';
    FRCampos.Extra      := '';
    FLCampos.Add(FRCampos);
    //
  except
    raise;
    Result := False;
  end;
end;

function TUnERPSinc_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
{
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
}
  except
    raise;
    Result := False;
  end;
end;

function TUnERPSinc_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  // TABELA : perfjan
{
  //
  // ???-?????-000 :: ????
  New(FRJanelas);
  FRJanelas.ID        := 'XXX-XXXXX-000';
  FRJanelas.Nome      := 'Fm???';
  FRJanelas.Descricao := '???';
  FRJanelas.Modulo := '';
  FLJanelas.Add(FRJanelas);
}
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

function TUnERPSinc_Tabs.DefineTabOri(const Tabela: String; var TabOri: String): Boolean;
var
  P: Integer;
begin
  Result := False;
  TabOri := '';
  p := Pos(Lowercase(CO_ERPSync_TbPrefix), LowerCase(Tabela));
  if P > 0 then
  begin
    P      := Length(CO_ERPSync_TbPrefix) + P;
    TabOri := Copy(Tabela, P);
    Result := TabOri <> '';
  end;
end;

end.
