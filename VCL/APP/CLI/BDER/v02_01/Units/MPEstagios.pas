unit MPEstagios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, DBGrids, dmkDBGridDAC, Db,
  mySQLDbTables, dmkDBGrid, Menus, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmMPEstagios = class(TForm)
    Panel1: TPanel;
    DsMPEstagios: TDataSource;
    QrMPEstagios: TmySQLQuery;
    dmkDBGridDAC1: TdmkDBGridDAC;
    PMAcao: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    QrMPEstagiosCodigo: TIntegerField;
    QrMPEstagiosNome: TWideStringField;
    QrMPEstagiosLk: TIntegerField;
    QrMPEstagiosDataCad: TDateField;
    QrMPEstagiosDataAlt: TDateField;
    QrMPEstagiosUserCad: TIntegerField;
    QrMPEstagiosUserAlt: TIntegerField;
    QrMPEstagiosAlterWeb: TSmallintField;
    QrMPEstagiosAtivo: TSmallintField;
    QrMPEstagiosOrdem: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtAcao: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure dmkDBGridDAC1CellClick(Column: TColumn);
  private
    { Private declarations }
    procedure ReopenQrMPEstagios(Codigo: Double);
  public
    { Public declarations }
  end;

  var
  FmMPEstagios: TFmMPEstagios;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmMPEstagios.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPEstagios.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPEstagios.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMPEstagios.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenQrMPEstagios(0);
end;

procedure TFmMPEstagios.Inclui1Click(Sender: TObject);
var
  Codigo: String;
  CodVal: Integer;
begin
  Codigo := '0';
  if InputQuery('Novo item de acumulador', 'Informe o c�digo do acumulador:',
  Codigo) then
  begin
    CodVal := Geral.IMV(Codigo);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO mpestagios SET Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := CodVal;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrMPEstagios(CodVal);
  end;
end;

procedure TFmMPEstagios.ReopenQrMPEstagios(Codigo: Double);
begin
  QrMPEstagios.Close;
  UnDmkDAC_PF.AbreQuery(QrMPEstagios, Dmod.MyDB);
  QrMPEstagios.Locate('Codigo', Codigo, []);
end;

procedure TFmMPEstagios.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmMPEstagios.Exclui1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o do registro selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mpestagios WHERE Codigo=:P0');
    Dmod.QrUpd.Params[00].AsInteger := QrMPEstagiosCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrMPEstagios(Int(Date));
  end;
end;

procedure TFmMPEstagios.dmkDBGridDAC1CellClick(Column: TColumn);
var
  Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if QrMPEstagiosAtivo.Value = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE mpestagios SET Ativo=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
    //
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsFloat   := QrMPEstagiosCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrMPEstagios(QrMPEstagiosCodigo.Value);
  end;
end;

end.

