object FmPQB3Mul: TFmPQB3Mul
  Left = 339
  Top = 185
  Caption = 'QUI-BALAN-005 :: Atualiza'#231#227'o Multipla de Estoque de PQ'
  ClientHeight = 554
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 398
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 72
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object RadioGroup1: TRadioGroup
        Left = 0
        Top = 0
        Width = 1008
        Height = 72
        Align = alClient
        Caption = ' Filtro: '
        TabOrder = 0
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 72
      Width = 1008
      Height = 326
      Align = alClient
      TabOrder = 1
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 1006
        Height = 324
        Align = alClient
        DataSource = DsPQBIts
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnColEnter = DBGrid1ColEnter
        Columns = <
          item
            Expanded = False
            FieldName = 'CI'
            Title.Caption = 'Empresa'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PQ'
            Title.Caption = 'Produto'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PQ'
            Title.Caption = 'Nome produto (uso e consumo)'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Title.Caption = 'Peso Atual'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CustoPadrao'
            Title.Caption = '$ padr'#227'o atual'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Title.Caption = 'Valor atual'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AjPeso'
            Title.Caption = 'Peso correto'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AjCusto'
            Title.Caption = 'Custo correto'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AjValor'
            Title.Caption = 'Valor correto'
            Width = 80
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 472
        Height = 32
        Caption = 'Atualiza'#231#227'o Multipla de Estoque de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 472
        Height = 32
        Caption = 'Atualiza'#231#227'o Multipla de Estoque de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 472
        Height = 32
        Caption = 'Atualiza'#231#227'o Multipla de Estoque de PQ'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 446
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 490
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object DsPQBIts: TDataSource
    DataSet = TbPQBIts
    Left = 32
    Top = 4
  end
  object TbPQBIts: TMySQLTable
    Database = Dmod.MyDB
    BeforePost = TbPQBItsBeforePost
    TableName = 'pqbits'
    Left = 4
    Top = 4
    object TbPQBItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'pqbits.Controle'
      ReadOnly = True
    end
    object TbPQBItsPQ: TIntegerField
      FieldName = 'PQ'
      Origin = 'pqbits.PQ'
      ReadOnly = True
    end
    object TbPQBItsCI: TIntegerField
      FieldName = 'CI'
      Origin = 'pqbits.CI'
      ReadOnly = True
    end
    object TbPQBItsCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
      Origin = 'pqbits.CustoPadrao'
      ReadOnly = True
    end
    object TbPQBItsPeso: TFloatField
      FieldName = 'Peso'
      Origin = 'pqbits.Peso'
      ReadOnly = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object TbPQBItsValor: TFloatField
      FieldName = 'Valor'
      Origin = 'pqbits.Valor'
      ReadOnly = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object TbPQBItsCusto: TFloatField
      FieldName = 'Custo'
      Origin = 'pqbits.Custo'
      ReadOnly = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object TbPQBItsNO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Origin = 'pqbits.NO_PQ'
      ReadOnly = True
      Size = 50
    end
    object TbPQBItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'pqbits.Ativo'
      ReadOnly = True
    end
    object TbPQBItsAjPeso: TFloatField
      FieldName = 'AjPeso'
      Origin = 'pqbits.AjPeso'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object TbPQBItsAjValor: TFloatField
      FieldName = 'AjValor'
      Origin = 'pqbits.AjValor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object TbPQBItsAjCusto: TFloatField
      FieldName = 'AjCusto'
      Origin = 'pqbits.AjCusto'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
  end
end
