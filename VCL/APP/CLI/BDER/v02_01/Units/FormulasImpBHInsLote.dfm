object FmFormulasImpBHInsLote: TFmFormulasImpBHInsLote
  Left = 333
  Top = 183
  Caption = 'QUI-RECEI-005 :: Localiza'#231#227'o de Lote Para Impress'#227'o de Receita'
  ClientHeight = 494
  ClientWidth = 793
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 793
    Height = 338
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 204
      Width = 793
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ExplicitLeft = 1
      ExplicitTop = 229
      ExplicitWidth = 790
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 793
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label6: TLabel
        Left = 8
        Top = 3
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object EdCliInt: TdmkEditCB
        Left = 8
        Top = 19
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 54
        Top = 19
        Width = 719
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMECI'
        ListSource = DsCliInt
        TabOrder = 1
        dmkEditCB = EdCliInt
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 48
      Width = 793
      Height = 156
      ActivePage = TabSheet2
      Align = alTop
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Por Lote'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 783
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 45
          Width = 785
          Height = 83
          Align = alClient
          DataSource = DsLotes
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLIINT'
              Title.Caption = 'Cliente interno'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROCEDE'
              Title.Caption = 'Proced'#234'ncia'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Marca'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Lote'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PLE'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PDA'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PTA'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMETIPO'
              Title.Caption = 'Tipifica'#231#227'o'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Visible = True
            end>
        end
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 785
          Height = 45
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          ExplicitWidth = 783
          object Label2: TLabel
            Left = 4
            Top = 3
            Width = 24
            Height = 13
            Caption = 'Lote:'
          end
          object Label5: TLabel
            Left = 196
            Top = 3
            Width = 96
            Height = 13
            Caption = 'Quantidade (pe'#231'as):'
          end
          object Label7: TLabel
            Left = 96
            Top = 3
            Width = 83
            Height = 13
            Caption = 'Peso do lote (kg):'
          end
          object EdLote: TdmkEdit
            Left = 4
            Top = 19
            Width = 89
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdLoteExit
          end
          object EdPecas_L: TdmkEdit
            Left = 196
            Top = 19
            Width = 96
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 1
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdPecas_LExit
          end
          object EdPesoF_L: TdmkEdit
            Left = 96
            Top = 19
            Width = 96
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Por Marca'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid2: TDBGrid
          Left = 0
          Top = 45
          Width = 785
          Height = 83
          TabStop = False
          Align = alClient
          DataSource = DsMarcas
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLIINT'
              Title.Caption = 'Cliente interno'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROCEDE'
              Title.Caption = 'Proced'#234'ncia'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Marca'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Lote'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PLE'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PDA'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PTA'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMETIPO'
              Title.Caption = 'Tipifica'#231#227'o'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Visible = True
            end>
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 785
          Height = 45
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label3: TLabel
            Left = 4
            Top = 3
            Width = 33
            Height = 13
            Caption = 'Marca:'
          end
          object Label4: TLabel
            Left = 196
            Top = 3
            Width = 96
            Height = 13
            Caption = 'Quantidade (pe'#231'as):'
          end
          object Label1: TLabel
            Left = 96
            Top = 3
            Width = 83
            Height = 13
            Caption = 'Peso do lote (kg):'
          end
          object EdMarca: TdmkEdit
            Left = 4
            Top = 19
            Width = 89
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdMarcaExit
          end
          object EdPecas_M: TdmkEdit
            Left = 196
            Top = 19
            Width = 96
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 1
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdPesoF_M: TdmkEdit
            Left = 96
            Top = 19
            Width = 96
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
    object DBGrid3: TDBGrid
      Left = 0
      Top = 227
      Width = 793
      Height = 111
      Align = alClient
      DataSource = FmFormulasImpBH.DsLotes
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Marca'
          Width = 103
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ficha'
          Title.Caption = 'RAMP'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MPIn'
          Title.Caption = 'Entrada'
          Width = 60
          Visible = True
        end>
    end
    object StaticText1: TStaticText
      Left = 0
      Top = 207
      Width = 793
      Height = 20
      Align = alTop
      Alignment = taCenter
      Caption = 'Couros selecionados'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      ExplicitWidth = 151
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 793
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 745
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 697
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 580
        Height = 32
        Caption = 'Localiza'#231#227'o de Lote Para Impress'#227'o de Receita'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 580
        Height = 32
        Caption = 'Localiza'#231#227'o de Lote Para Impress'#227'o de Receita'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 580
        Height = 32
        Caption = 'Localiza'#231#227'o de Lote Para Impress'#227'o de Receita'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 386
    Width = 793
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 789
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 430
    Width = 793
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 789
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 645
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 6
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Adiciona'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Hint = 'Confirma a senha digitada'
        Caption = 'E&xclui'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 2
        OnClick = BtExcluiClick
      end
    end
  end
  object QrLotes: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLotesCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECLIINT, CASE WHEN pro.Tipo=0'
      'THEN pro.RazaoSocial ELSE pro.Nome END NOMEPROCEDE, mpi.* '
      'FROM mpin mpi'
      'LEFT JOIN entidades cli ON cli.Codigo=mpi.ClienteI'
      'LEFT JOIN entidades pro ON pro.Codigo=mpi.Procedencia'
      'WHERE mpi.Lote=:P0'
      'ORDER BY mpi.Lote DESC, mpi.Controle DESC'
      'LIMIT 1000')
    Left = 532
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotesNOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Size = 100
    end
    object QrLotesNOMEPROCEDE: TWideStringField
      FieldName = 'NOMEPROCEDE'
      Size = 100
    end
    object QrLotesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLotesControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLotesFicha: TIntegerField
      FieldName = 'Ficha'
      Required = True
    end
    object QrLotesData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotesClienteI: TIntegerField
      FieldName = 'ClienteI'
      Required = True
    end
    object QrLotesProcedencia: TIntegerField
      FieldName = 'Procedencia'
      Required = True
    end
    object QrLotesTransportadora: TIntegerField
      FieldName = 'Transportadora'
      Required = True
    end
    object QrLotesMarca: TWideStringField
      FieldName = 'Marca'
      Required = True
    end
    object QrLotesPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrLotesPecasNF: TFloatField
      FieldName = 'PecasNF'
      Required = True
    end
    object QrLotesM2: TFloatField
      FieldName = 'M2'
      Required = True
    end
    object QrLotesPNF: TFloatField
      FieldName = 'PNF'
      Required = True
    end
    object QrLotesPLE: TFloatField
      FieldName = 'PLE'
      Required = True
    end
    object QrLotesPDA: TFloatField
      FieldName = 'PDA'
      Required = True
    end
    object QrLotesRecorte_PDA: TFloatField
      FieldName = 'Recorte_PDA'
      Required = True
    end
    object QrLotesPTA: TFloatField
      FieldName = 'PTA'
      Required = True
    end
    object QrLotesRecorte_PTA: TFloatField
      FieldName = 'Recorte_PTA'
      Required = True
    end
    object QrLotesRaspa_PTA: TFloatField
      FieldName = 'Raspa_PTA'
      Required = True
    end
    object QrLotesTipificacao: TIntegerField
      FieldName = 'Tipificacao'
      Required = True
    end
    object QrLotesAparasCabelo: TSmallintField
      FieldName = 'AparasCabelo'
      Required = True
    end
    object QrLotesSeboPreDescarne: TSmallintField
      FieldName = 'SeboPreDescarne'
      Required = True
    end
    object QrLotesValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrLotesCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      Required = True
    end
    object QrLotesFrete: TFloatField
      FieldName = 'Frete'
      Required = True
    end
    object QrLotesAbateTipo: TIntegerField
      FieldName = 'AbateTipo'
      Required = True
    end
    object QrLotesLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLotesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLotesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLotesUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLotesUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLotesNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 50
      Calculated = True
    end
    object QrLotesLote: TWideStringField
      FieldName = 'Lote'
      Size = 11
    end
  end
  object DsLotes: TDataSource
    DataSet = QrLotes
    Left = 564
    Top = 8
  end
  object QrMarcas: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMarcasCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECLIINT, CASE WHEN pro.Tipo=0'
      'THEN pro.RazaoSocial ELSE pro.Nome END NOMEPROCEDE, mpi.* '
      'FROM mpin mpi'
      'LEFT JOIN entidades cli ON cli.Codigo=mpi.ClienteI'
      'LEFT JOIN entidades pro ON pro.Codigo=mpi.Procedencia'
      'WHERE mpi.Marca=:P0'
      'ORDER BY mpi.Marca DESC, mpi.Controle DESC'
      'LIMIT 1000')
    Left = 592
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMarcasNOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Size = 100
    end
    object QrMarcasNOMEPROCEDE: TWideStringField
      FieldName = 'NOMEPROCEDE'
      Size = 100
    end
    object QrMarcasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMarcasControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMarcasFicha: TIntegerField
      FieldName = 'Ficha'
      Required = True
    end
    object QrMarcasData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMarcasClienteI: TIntegerField
      FieldName = 'ClienteI'
      Required = True
    end
    object QrMarcasProcedencia: TIntegerField
      FieldName = 'Procedencia'
      Required = True
    end
    object QrMarcasTransportadora: TIntegerField
      FieldName = 'Transportadora'
      Required = True
    end
    object QrMarcasMarca: TWideStringField
      FieldName = 'Marca'
      Required = True
    end
    object QrMarcasPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrMarcasPecasNF: TFloatField
      FieldName = 'PecasNF'
      Required = True
    end
    object QrMarcasM2: TFloatField
      FieldName = 'M2'
      Required = True
    end
    object QrMarcasPNF: TFloatField
      FieldName = 'PNF'
      Required = True
    end
    object QrMarcasPLE: TFloatField
      FieldName = 'PLE'
      Required = True
    end
    object QrMarcasPDA: TFloatField
      FieldName = 'PDA'
      Required = True
    end
    object QrMarcasRecorte_PDA: TFloatField
      FieldName = 'Recorte_PDA'
      Required = True
    end
    object QrMarcasPTA: TFloatField
      FieldName = 'PTA'
      Required = True
    end
    object QrMarcasRecorte_PTA: TFloatField
      FieldName = 'Recorte_PTA'
      Required = True
    end
    object QrMarcasRaspa_PTA: TFloatField
      FieldName = 'Raspa_PTA'
      Required = True
    end
    object QrMarcasTipificacao: TIntegerField
      FieldName = 'Tipificacao'
      Required = True
    end
    object QrMarcasAparasCabelo: TSmallintField
      FieldName = 'AparasCabelo'
      Required = True
    end
    object QrMarcasSeboPreDescarne: TSmallintField
      FieldName = 'SeboPreDescarne'
      Required = True
    end
    object QrMarcasValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrMarcasCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      Required = True
    end
    object QrMarcasFrete: TFloatField
      FieldName = 'Frete'
      Required = True
    end
    object QrMarcasAbateTipo: TIntegerField
      FieldName = 'AbateTipo'
      Required = True
    end
    object QrMarcasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMarcasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMarcasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMarcasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMarcasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMarcasNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 50
      Calculated = True
    end
    object QrMarcasLote: TWideStringField
      FieldName = 'Lote'
      Size = 11
    end
  end
  object DsMarcas: TDataSource
    DataSet = QrMarcas
    Left = 620
    Top = 8
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECI, ci.Codigo'
      'FROM entidades ci'
      'WHERE ci.Cliente2="V"'
      'ORDER BY NOMECI')
    Left = 8
    Top = 8
    object QrCliIntNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 36
    Top = 8
  end
end
