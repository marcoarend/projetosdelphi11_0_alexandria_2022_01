unit CMPTInnAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, ComCtrls, dmkEditDateTimePicker, Mask, DB, mySQLDbTables,
  dmkLabel, dmkgeral, Grids, DBGrids, dmkRadioGroup, dmkImage, UnDmkEnums,
  UnInternalConsts;

type
  TFmCMPTInnAdd = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    TPDataE: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label9: TLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdClienteEdit: TdmkEditCB;
    CBClienteEdit: TdmkDBLookupComboBox;
    RGTipoNF: TdmkRadioGroup;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel5: TPanel;
    PainelGGX: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    PnEdita: TPanel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EdInnQtdkg: TdmkEdit;
    EdInnQtdPc: TdmkEdit;
    EdInnQtdVal: TdmkEdit;
    EdNFRef: TdmkEdit;
    CkContinuar: TCheckBox;
    Label3: TLabel;
    EdInnQtdM2: TdmkEdit;
    QrGraGruX: TmySQLQuery;
    QrGraGruXNO_GG1: TWideStringField;
    QrGraGruXNO_COR: TWideStringField;
    QrGraGruXNO_TAM: TWideStringField;
    QrGraGruXGraGruX: TIntegerField;
    DsGraGruX: TDataSource;
    QrClientesEdit: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientesEdit: TDataSource;
    DBGrid1: TDBGrid;
    Panel4: TPanel;
    Label15: TLabel;
    EdrefNFe: TdmkEdit;
    Panel6: TPanel;
    EdNFInn: TdmkEdit;
    Label10: TLabel;
    Label1: TLabel;
    EdSerie: TdmkEdit;
    EdmodNF: TdmkEdit;
    Label16: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    QrProcedencias: TmySQLQuery;
    QrProcedenciasCodigo: TIntegerField;
    QrProcedenciasNOMEENTIDADE: TWideStringField;
    QrProcedenciasPreDescarn: TSmallintField;
    QrProcedenciasMaskLetras: TWideStringField;
    QrProcedenciasMaskFormat: TWideStringField;
    QrProcedenciasCorretor: TIntegerField;
    QrProcedenciasComissao: TFloatField;
    QrProcedenciasDescAdiant: TFloatField;
    QrProcedenciasCondPagto: TWideStringField;
    QrProcedenciasCondComis: TWideStringField;
    QrProcedenciasLocalEntrg: TWideStringField;
    QrProcedenciasAbate: TIntegerField;
    QrProcedenciasTransporte: TIntegerField;
    QrProcedenciasLoteLetras: TWideStringField;
    QrProcedenciasLoteFormat: TWideStringField;
    DsProcedencias: TDataSource;
    Label17: TLabel;
    EdProcedencia: TdmkEditCB;
    CBProcedencia: TdmkDBLookupComboBox;
    BitBtn1: TBitBtn;
    EdNF_CC: TdmkEdit;
    Label20: TLabel;
    EdValMaoObra: TdmkEdit;
    Label21: TLabel;
    Label18: TLabel;
    EdPLEKg: TdmkEdit;
    RGHowCobrMO: TdmkRadioGroup;
    EdPrcUnitMO: TdmkEdit;
    Label19: TLabel;
    Panel2: TPanel;
    Label11: TLabel;
    EdGraGruX: TdmkEdit;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGTipoNFClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdrefNFeExit(Sender: TObject);
    procedure EdGraGruXExit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
    procedure RGHowCobrMOClick(Sender: TObject);
    procedure EdPLEKgRedefinido(Sender: TObject);
    procedure EdPrcUnitMORedefinido(Sender: TObject);
    procedure EdInnQtdkgRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure VerificaPainelGGX();
    procedure CalculaMaoDeObra();
  public
    { Public declarations }
  end;

  var
  FmCMPTInnAdd: TFmCMPTInnAdd;

implementation

uses UnMyObjects, UMySQLModule, Module, CMPTInn, UnGrade_Jan, MyDBCheck,
DmkDAC_PF, ModuleGeral;

{$R *.DFM}

procedure TFmCMPTInnAdd.BitBtn1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrProcedencias, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdProcedencia.Text := IntToStr(VAR_ENTIDADE);
    CBProcedencia.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmCMPTInnAdd.BtOKClick(Sender: TObject);
var
  Empresa, Cliente, Codigo, GraGruX, NFInn, NFRef, TipoNF, Serie, modNF,
  Procedencia, NF_VP, NF_RP, NF_CC: Integer;
  DataE, refNFe: String;
  SdoQtdkg, SdoQtdPc, SdoQtdM2, SdoQtdVal,
  InnQtdkg, InnQtdPc, InnQtdM2, InnQtdVal, ValMaoObra: Double;
  HowCobrMO: Integer;
  PLEKg, PrcUnitMO: Double;
begin
  if not UMyMod.ObtemCodigoDeCodUsu(EdFilial, Empresa, 'Informe a empresa!',
    'Codigo', 'Filial') then Exit;
  Cliente   := Geral.IMV(EdClienteEdit.Text);
  if Cliente = 0 then
  begin
    Application.MessageBox('Defina o cliente!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdClienteEdit.SetFocus;
    Exit;
  end;
  GraGruX   := Geral.IMV(EdGraGruX.Text);
  if (GraGruX = 0) or (PainelGGX.Visible = False) then
  begin
    Application.MessageBox('Defina o reduzido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdGraGruX.SetFocus;
    Exit;
  end;
  //
  DataE     := Geral.FDT(TPDataE.Date, 1);
  InnQtdkg  := EdInnQtdkg.ValueVariant;
  InnQtdPc  := EdInnQtdPc.ValueVariant;
  InnQtdM2  := EdInnQtdM2.ValueVariant;
  InnQtdVal := EdInnQtdVal.ValueVariant;
  if ImgTipo.SQLType = stUpd then
  begin
    SdoQtdkg  := FmCMPTInn.QrCMPTInnSdoQtdkg.Value;
    SdoQtdPc  := FmCMPTInn.QrCMPTInnSdoQtdPc.Value;
    SdoQtdM2  := FmCMPTInn.QrCMPTInnSdoQtdM2.Value;
    SdoQtdVal := FmCMPTInn.QrCMPTInnSdoQtdVal.Value;
  end else begin
    SdoQtdkg  := InnQtdkg;
    SdoQtdPc  := InnQtdPc;
    SdoQtdM2  := InnQtdM2;
    SdoQtdVal := InnQtdVal;
  end;
  NFInn   := EdNFInn.ValueVariant;
  NFRef   := EdNFRef.ValueVariant;
  TipoNF  := RGTipoNF.ItemIndex;
  Serie   := EdSerie.ValueVariant;
  modNF   := EdmodNF.ValueVariant;
  refNFe  := EdrefNFe.Text;
  if (TipoNF = 0) and (Length(refNFe) <> 44) then
  begin
    Geral.MB_Aviso('Defina uma chave com 44 n�meros para a NF-e!');
    PageControl1.ActivePageIndex := 0;
    EdrefNFe.SetFocus;
    Exit;
  end else begin
    if EdNFInn.ValueVariant = 0 then
    begin
      Geral.MB_Aviso('Defina o n�mero da NF!');
      PageControl1.ActivePageIndex := 1;
      EdNFInn.SetFocus;
      Exit;
    end;
  end;
  Procedencia    := EdProcedencia.ValueVariant;
  ValMaoObra     := EdValMaoObra.ValueVariant;
  //NF_VP          := EdNF_VP.ValueVariant;
  //NF_RP          := EdNF_RP.ValueVariant;
  NF_CC          := EdNF_CC.ValueVariant;
  //
  HowCobrMO      := RGHowCobrMO.ItemIndex;
  PLEKg          := EdPLEKg.ValueVariant;
  PrcUnitMO      := EdPrcUnitMO.ValueVariant;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('CMPTinn', 'Codigo', ImgTipo.SQLType,
    FmCMPTInn.QrCMPTInnCodigo.Value);
{
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'CMPTinn', False, [
    'Cliente', 'DataE', 'NFInn',
    'NFRef', 'InnQtdkg', 'InnQtdPc',
    'InnQtdM2', 'InnQtdVal', 'SdoQtdkg',
    'SdoQtdPc', 'SdoQtdM2', 'SdoQtdVal',
    'Empresa', 'GraGruX', 'TipoNF',
    'refNFe', 'modNF', 'Serie',
    'Procedencia', 'ValMaoObra', 'NF_CC'
    //'NF_VP', 'NF_RP',
  ], ['Codigo'], [
    Cliente, DataE, NFInn,
    NFRef, InnQtdkg, InnQtdPc,
    InnQtdM2, InnQtdVal, SdoQtdkg,
    SdoQtdPc, SdoQtdM2, SdoQtdVal,
    Empresa, GraGruX, TipoNF,
    refNFe, modNF, Serie,
    Procedencia, ValMaoObra, NF_CC
    //NF_VP, NF_RP
  ], [Codigo], True) then
}
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'CMPTinn', False, [
  'Empresa', 'Cliente', 'DataE',
  'NFInn', 'NFRef', 'InnQtdkg',
  'InnQtdPc', 'InnQtdM2', 'InnQtdVal',
  'SdoQtdkg', 'SdoQtdPc', 'SdoQtdM2',
  'SdoQtdVal', 'GraGruX', 'TipoNF',
  'refNFe', 'modNF', 'Serie',
  'Procedencia', 'ValMaoObra',
  'NF_CC',
  'HowCobrMO', 'PLEKg',
  'PrcUnitMO'], [
  'Codigo'], [
  Empresa, Cliente, DataE,
  NFInn, NFRef, InnQtdkg,
  InnQtdPc, InnQtdM2, InnQtdVal,
  SdoQtdkg, SdoQtdPc, SdoQtdM2,
  SdoQtdVal, GraGruX, TipoNF,
  refNFe, modNF, Serie,
  Procedencia, ValMaoObra,
  NF_CC, HowCobrMO, PLEKg,
  PrcUnitMO], [
  Codigo], True) then
  begin
    Dmod.AtualizaEstoqueCMPT(Codigo, 0);
    FmCMPTInn.ReopenCMPTInn(Codigo);
    if CkContinuar.Checked  and CkContinuar.Visible then
    begin
      EdNFInn.ValueVariant      := 0;//EdNFInn.ValueVariant + 1;
      EdNFRef.ValueVariant      := 0;//EdNFRef.ValueVariant + 1;
      EdInnQtdkg.ValueVariant   := 0;
      EdInnQtdPc.ValueVariant   := 0;
      EdInnQtdVal.ValueVariant  := 0;
      EdValMaoObra.ValueVariant := 0;
      //EdNF_VP.ValueVariant      := 0;
      //EdNF_RP.ValueVariant      := 0;
      EdNF_CC.ValueVariant      := 0;
      //
      TPDataE.SetFocus;
    end else Close;
  end;
end;

procedure TFmCMPTInnAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCMPTInnAdd.CalculaMaoDeObra();
var
  Peso, Preco, Total: Double;
begin
  if RGHowCobrMO.ItemIndex < 0 then
    RGHowCobrMO.ItemIndex := 1;
  Peso := 0;
  case RGHowCobrMO.ItemIndex of
    0: Peso := EdInnQtdkg.ValueVariant;
    1: Peso := EdPLEKg.ValueVariant;
    else
      Geral.MB_Erro('Origem do peso indefinido em "CalculaMaoDeObra()"');
  end;
  Preco := EdPrcUnitMO.ValueVariant;
  Total := Peso * Preco;
  EdValMaoObra.ValueVariant := Total;
end;

procedure TFmCMPTInnAdd.EdGraGruXChange(Sender: TObject);
begin
  VerificaPainelGGX();
end;

procedure TFmCMPTInnAdd.EdGraGruXExit(Sender: TObject);
begin
  VerificaPainelGGX();
end;

procedure TFmCMPTInnAdd.EdInnQtdkgRedefinido(Sender: TObject);
begin
  CalculaMaoDeObra();
end;

procedure TFmCMPTInnAdd.EdPLEKgRedefinido(Sender: TObject);
begin
  CalculaMaoDeObra();
end;

procedure TFmCMPTInnAdd.EdPrcUnitMORedefinido(Sender: TObject);
begin
  CalculaMaoDeObra();
end;

procedure TFmCMPTInnAdd.VerificaPainelGGX();
var
  GraGruX: Integer;
begin
  GraGruX := Geral.IMV(EdGraGruX.Text);
  PainelGGX.Visible := QrGraGruX.Locate('GraGruX', GraGruX, []);
end;

procedure TFmCMPTInnAdd.EdrefNFeExit(Sender: TObject);
begin
  EdrefNFe.Text := Geral.SoNumero_TT(EdrefNFe.Text);
end;

procedure TFmCMPTInnAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCMPTInnAdd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientesEdit, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcedencias, Dmod.MyDB);
  PageControl1.ActivePageindex := 0;
  RGTipoNF.ItemIndex := 0;
end;

procedure TFmCMPTInnAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCMPTInnAdd.ImgTipoChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    TPDataE.Date := Date;
end;

procedure TFmCMPTInnAdd.RGHowCobrMOClick(Sender: TObject);
begin
  CalculaMaoDeObra();
end;

procedure TFmCMPTInnAdd.RGTipoNFClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := RGTipoNF.ItemIndex;
end;

procedure TFmCMPTInnAdd.SpeedButton1Click(Sender: TObject);
var
  GraGruX: Integer;
  SQL: String;
begin
  SQL := Geral.ATS([
    'SELECT gg1.CodUsu, gg1.Nivel1, ',
    'gg1.GraTamCad, gg1.Nome',
    'FROM gragru1 gg1',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
    'WHERE pgt.TipPrd=2',
    'ORDER BY gg1.Nome']);
  //
  GraGruX := Grade_Jan.MostraFormGraGruPesq1(SQL);
  //
  EdGraGruX.ValueVariant := GraGruX;
end;

end.
