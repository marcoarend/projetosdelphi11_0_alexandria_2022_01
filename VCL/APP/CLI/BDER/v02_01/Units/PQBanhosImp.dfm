object FmPQBanhosImp: TFmPQBanhosImp
  Left = 339
  Top = 185
  Caption = 'QUI-RECEI-033 :: Relat'#243'rios de Uso de Banhos'
  ClientHeight = 541
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 355
        Height = 32
        Caption = 'Relat'#243'rios de Uso de Banhos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 355
        Height = 32
        Caption = 'Relat'#243'rios de Uso de Banhos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 355
        Height = 32
        Caption = 'Relat'#243'rios de Uso de Banhos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 427
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 471
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 379
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object dmkDBGridZTO1: TdmkDBGridZTO
      Left = 0
      Top = 237
      Width = 812
      Height = 142
      Align = alClient
      DataSource = DsPesq
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 129
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label2: TLabel
        Left = 328
        Top = 84
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label3: TLabel
        Left = 16
        Top = 84
        Width = 34
        Height = 13
        Caption = 'Banho:'
      end
      object Label4: TLabel
        Left = 328
        Top = 4
        Width = 77
        Height = 13
        Caption = 'Setor de origem:'
      end
      object Label16: TLabel
        Left = 328
        Top = 44
        Width = 40
        Height = 13
        Caption = 'Receita:'
      end
      object Label5: TLabel
        Left = 16
        Top = 44
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label6: TLabel
        Left = 133
        Top = 44
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object EdSetor: TdmkEditCB
        Left = 328
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSetor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSetor: TdmkDBLookupComboBox
        Left = 384
        Top = 20
        Width = 325
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSE
        TabOrder = 7
        dmkEditCB = EdSetor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdReceita: TdmkEditCB
        Left = 328
        Top = 60
        Width = 57
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBReceita
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBReceita: TdmkDBLookupComboBox
        Left = 387
        Top = 60
        Width = 322
        Height = 21
        KeyField = 'Numero'
        ListField = 'Nome'
        ListSource = DsFormulas
        TabOrder = 9
        dmkEditCB = EdReceita
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPDataI: TdmkEditDateTimePicker
        Left = 20
        Top = 60
        Width = 112
        Height = 21
        Date = 37670.000000000000000000
        Time = 0.521748657396528900
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPDataF: TdmkEditDateTimePicker
        Left = 133
        Top = 60
        Width = 112
        Height = 21
        Date = 37670.000000000000000000
        Time = 0.521748657396528900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 71
        Top = 20
        Width = 250
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        TabStop = False
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEmpresa: TdmkEditCB
        Left = 16
        Top = 20
        Width = 55
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdCI: TdmkEditCB
        Left = 328
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCI
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCI: TdmkDBLookupComboBox
        Left = 384
        Top = 100
        Width = 325
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECI'
        ListSource = DsCI
        TabOrder = 11
        dmkEditCB = EdCI
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPQ: TdmkEditCB
        Left = 16
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPQ
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPQ: TdmkDBLookupComboBox
        Left = 72
        Top = 100
        Width = 249
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPQ
        TabOrder = 5
        dmkEditCB = EdPQ
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 129
      Width = 812
      Height = 108
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object RGOrdem1: TRadioGroup
        Left = 0
        Top = 0
        Width = 117
        Height = 108
        Align = alLeft
        Caption = ' Ordem 1: '
        ItemIndex = 0
        Items.Strings = (
          'Banho'
          'Setor'
          'Receita'
          'Data'
          'Cliente Interno')
        TabOrder = 0
      end
      object RGOrdem2: TRadioGroup
        Left = 117
        Top = 0
        Width = 117
        Height = 108
        Align = alLeft
        Caption = ' Ordem 2: '
        ItemIndex = 1
        Items.Strings = (
          'Banho'
          'Setor'
          'Receita'
          'Data'
          'Cliente Interno')
        TabOrder = 1
      end
      object RGOrdem3: TRadioGroup
        Left = 234
        Top = 0
        Width = 117
        Height = 108
        Align = alLeft
        Caption = ' Ordem 3: '
        ItemIndex = 2
        Items.Strings = (
          'Banho'
          'Setor'
          'Receita'
          'Data'
          'Cliente Interno')
        TabOrder = 2
      end
      object RGOrdem4: TRadioGroup
        Left = 351
        Top = 0
        Width = 117
        Height = 108
        Align = alLeft
        Caption = ' Ordem 4: '
        ItemIndex = 3
        Items.Strings = (
          'Banho'
          'Setor'
          'Receita'
          'Data'
          'Cliente Interno')
        TabOrder = 3
      end
      object RGAgrupa: TRadioGroup
        Left = 468
        Top = 0
        Width = 101
        Height = 108
        Align = alLeft
        Caption = 'Agrupamentos: '
        ItemIndex = 0
        Items.Strings = (
          '0'
          '1'
          '2')
        TabOrder = 4
      end
      object RGRelatorio: TRadioGroup
        Left = 569
        Top = 0
        Width = 101
        Height = 108
        Align = alLeft
        Caption = 'Relat'#243'rio:'
        ItemIndex = 0
        Items.Strings = (
          'N/D'
          'Anal'#237'tico'
          'Sint'#233'tico')
        TabOrder = 5
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 3
  end
  object QrSE: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM listasetores'
      'ORDER BY Nome')
    Left = 328
    Top = 316
    object QrSECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSENome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object DsSE: TDataSource
    DataSet = QrSE
    Left = 328
    Top = 364
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 61
    Top = 457
  end
  object QrFormulas: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Numero, Nome '
      'FROM formulas'
      'ORDER BY Nome')
    Left = 161
    Top = 313
    object QrFormulasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 228
    Top = 316
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 272
    Top = 364
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Codigo, pq.Nome'
      'FROM pq pq'
      'WHERE pq.Codigo < 0'
      'AND pq.Codigo > -10'
      'ORDER BY pq.Nome')
    Left = 272
    Top = 316
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 228
    Top = 364
  end
  object frxQUI_RECEI_033_1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsPesq."NO_PQ"'#39';'
      '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsPesq."NO_SE"'#39';'
      
        '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsPesq."NO_Formul' +
        'a"'#39';'
      
        '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsPesq."DtaBaixa"' +
        #39';'
      '  if <VFR_ORD1> =  4 then GH1.Condition := '#39'frxDsLodo."NO_CI"'#39';'
      '  '
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsPesq."NO_PQ"'#39';'
      '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsPesq."NO_SE"'#39';'
      
        '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsPesq."NO_Formul' +
        'a"'#39';'
      
        '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsPesq."DtaBaixa"' +
        #39';'
      '  if <VFR_ORD2> =  4 then GH2.Condition := '#39'frxDsLodo."NO_CI"'#39';'
      '  //'
      'end.')
    OnGetValue = frxQUI_RECEI_033_1GetValue
    Left = 64
    Top = 312
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 498.897960000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050880
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 109.606360240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779527560000000000
          Top = 75.590434020000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setor:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031496060000000000
          Top = 75.590434020000000000
          Width = 268.346456690000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_SETOR]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409448820000000000
          Top = 60.472304250000000000
          Width = 268.346456690000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_FORMULA]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 94.488084020000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 472.440981500000000000
          Top = 94.488084020000000000
          Width = 60.472440944881890000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149391500000000000
          Top = 94.488188980000000000
          Width = 60.472440944881890000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Banho')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 143.621871500000000000
          Top = 94.488147480000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Setor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779527560000000000
          Top = 60.472326220000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Banho:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031496060000000000
          Top = 60.472326220000000000
          Width = 268.346456690000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_BANHO]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE UTILIZA'#199#195'O DE BANHO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 94.488250000000000000
          Width = 94.488188976377950000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente interno')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 94.488147480000000000
          Width = 120.944881890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Receita')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 60.472416540000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Receita:')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409448820000000000
          Top = 75.590600000000000000
          Width = 268.346456690000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 75.590690320000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente int.:')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 94.488250000000000000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pesagem')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 94.488250000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'm'#179' banho')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 94.488250000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'kg couros')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 94.488250000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'L banho/P'#231)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 638.740570000000000000
          Top = 94.488250000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'L banho/kg')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677165350000000000
          Top = 3.779527560000000000
          Width = 393.070878350000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 3.779530000000000000
          Width = 56.692913385826770000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."M3Banho">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0;-#,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."Qtde">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 3.779530000000000000
          Width = 64.251968500000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."PesoPecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 638.740570000000000000
          Top = 3.779530000000000000
          Width = 41.574800710000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."PesoPQ">) / SUM(<frxDsPesq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 3.779530000000000000
          Width = 41.574800710000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0;-#,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(IIF(<frxDsPesq."Qtde"> > 0, <frxDsPesq."PesoPQ">, 0)) '
            '/ '
            'SUM(IIF(<frxDsPesq."Qtde"> > 0, <frxDsPesq."Qtde">, 0))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 272.126160000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
        RowCount = 0
        object MePeso: TfrxMemoView
          AllowVectorExport = True
          Left = 472.440986380000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Qtde'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0;-#,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePQ: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795265830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."DtaBaixa_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'NO_SE'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_SE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Width = 120.944881890000000000
          Height = 15.118110240000000000
          DataField = 'NO_Formula'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_Formula"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 94.488188976377950000
          Height = 15.118110240000000000
          DataField = 'NO_CI'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_CI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 45.354325830000000000
          Height = 15.118110240000000000
          DataField = 'Pesagem'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."Pesagem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeNomePQ: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149625830000000000
          Top = -0.000061019999999995
          Width = 60.472440944881890000
          Height = 15.118110240000000000
          DataField = 'NO_PQ'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748304880000000000
          Top = 0.000165980000000010
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."M3Banho"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913734880000000000
          Top = 0.000165980000000010
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataField = 'PesoPecas'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."PesoPecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165744880000000000
          Top = 0.000165980000000010
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'LporPc'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0;-#,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."LporPc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 638.740574880000000000
          Top = 0.000165980000000010
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'LporKg'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."LporKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677165354330710000
          Top = 3.779530000000000000
          Width = 30.236220470000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Top = 3.779530000000000000
          Width = 362.834638350000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."M3Banho">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0;-#,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."Qtde">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 3.779530000000000000
          Width = 64.251968500000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."PesoPecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 638.740570000000000000
          Top = 3.779530000000000000
          Width = 41.574800710000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."PesoPQ">) / SUM(<frxDsPesq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 3.779530000000000000
          Width = 41.574800710000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0;-#,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(IIF(<frxDsPesq."Qtde"> > 0, <frxDsPesq."PesoPQ">, 0)) '
            '/ '
            'SUM(IIF(<frxDsPesq."Qtde"> > 0, <frxDsPesq."Qtde">, 0))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLodo."NO_Unidade"'
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 13.559060000000000000
          Width = 661.417713390000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLodo."NO_Unidade"'
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 10.661410000000000000
          Width = 665.197243390000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677165354330710000
          Top = 3.779527560000000000
          Width = 30.236220470000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Top = 3.779527560000000000
          Width = 362.834638350000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748016850000000000
          Top = 3.779527560000000000
          Width = 56.692913385826770000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."M3Banho">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0;-#,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."Qtde">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 3.779530000000000000
          Width = 64.251968500000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."PesoPecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 3.779530000000000000
          Width = 41.574800710000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0;-#,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(IIF(<frxDsPesq."Qtde"> > 0, <frxDsPesq."PesoPQ">, 0)) '
            '/ '
            'SUM(IIF(<frxDsPesq."Qtde"> > 0, <frxDsPesq."Qtde">, 0))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 638.740570000000000000
          Top = 3.779530000000000000
          Width = 41.574800710000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."PesoPQ">) / SUM(<frxDsPesq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPesqBeforeOpen
    SQL.Strings = (
      'SELECT frm.Nome NO_Formula, se.Nome NO_SE, pq.Nome NO_PQ,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CI,'
      'emi.Codigo, emi.DataEmis, emi.Numero,'
      'emi.ClienteI, emi.Setor, emi.Peso,'
      'emi.DtaBaixa, emi.Empresa, emi.Codigo,'
      'SUM(its.Porcent) Porcent, SUM(its.Peso_PQ) PesoPQ,'
      'frm.Peso PesoPecas, frm.Qtde,  SUM(its.Peso_PQ) / frm.Peso'
      'LporKg, SUM(its.Peso_PQ) / frm.Qtde LporPc,'
      ''
      'DATE_FORMAT(DtaBaixa, "%d/%m/%y") DtaBaixa_TXT'
      'FROM emitits its'
      'LEFT JOIN emit emi ON emi.Codigo=its.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=emi.ClienteI'
      'LEFT JOIN pq pq ON pq.Codigo=its.Produto'
      'LEFT JOIN listasetores se ON se.Codigo=emi.Setor'
      'LEFT JOIN formulas frm ON frm.Numero=emi.Numero'
      'WHERE (its.Produto < 0 AND its.Produto > -11)'
      'AND emi.DtaBaixa BETWEEN "2023-11-14" AND "2023-12-14 23:59:59"'
      'AND emi.Empresa=-11'
      'GROUP BY emi.Codigo'
      'ORDER BY NO_PQ, NO_SE, NO_Formula, emi.DtaBaixa'
      '')
    Left = 64
    Top = 360
    object QrPesqNO_Formula: TWideStringField
      FieldName = 'NO_Formula'
      Size = 30
    end
    object QrPesqNO_SE: TWideStringField
      FieldName = 'NO_SE'
    end
    object QrPesqNO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrPesqNO_CI: TWideStringField
      FieldName = 'NO_CI'
      Size = 100
    end
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqDataEmis: TDateTimeField
      FieldName = 'DataEmis'
    end
    object QrPesqNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrPesqClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrPesqSetor: TSmallintField
      FieldName = 'Setor'
    end
    object QrPesqPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPesqDtaBaixa: TDateField
      FieldName = 'DtaBaixa'
    end
    object QrPesqEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPesqPorcent: TFloatField
      FieldName = 'Porcent'
    end
    object QrPesqPesoPQ: TFloatField
      FieldName = 'PesoPQ'
    end
    object QrPesqDtaBaixa_TXT: TWideStringField
      FieldName = 'DtaBaixa_TXT'
      Size = 10
    end
    object QrPesqPesoPecas: TFloatField
      FieldName = 'PesoPecas'
    end
    object QrPesqQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrPesqLporKg: TFloatField
      FieldName = 'LporKg'
    end
    object QrPesqLporPc: TFloatField
      FieldName = 'LporPc'
    end
    object QrPesqPesagem: TWideStringField
      FieldName = 'Pesagem'
      Size = 10
    end
    object QrPesqM3Banho: TFloatField
      FieldName = 'M3Banho'
    end
  end
  object frxDsPesq: TfrxDBDataset
    UserName = 'frxDsPesq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_Formula=NO_Formula'
      'NO_SE=NO_SE'
      'NO_PQ=NO_PQ'
      'NO_CI=NO_CI'
      'Codigo=Codigo'
      'DataEmis=DataEmis'
      'Numero=Numero'
      'ClienteI=ClienteI'
      'Setor=Setor'
      'Peso=Peso'
      'DtaBaixa=DtaBaixa'
      'Empresa=Empresa'
      'Porcent=Porcent'
      'PesoPQ=PesoPQ'
      'DtaBaixa_TXT=DtaBaixa_TXT'
      'PesoPecas=PesoPecas'
      'Qtde=Qtde'
      'LporKg=LporKg'
      'LporPc=LporPc'
      'Pesagem=Pesagem'
      'M3Banho=M3Banho')
    DataSet = QrPesq
    BCDToCurrency = False
    DataSetOptions = []
    Left = 64
    Top = 410
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 161
    Top = 361
  end
end
