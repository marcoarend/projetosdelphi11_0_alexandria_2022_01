unit PQPedNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, Mask, UnMsgInt, ComCtrls, dmkGeral,
  UnInternalConsts, UnInternalConsts2, Db, (*DBTables,*) UMySQLModule,
  mySQLDbTables, dmkEdit, dmkDBLookupComboBox, dmkEditCB, dmkLabel, dmkImage,
  UnDmkEnums, DmkDAC_PF;

type
  TFmPQPedNew = class(TForm)
    QrFornecedor: TmySQLQuery;
    DsFornecedor: TDataSource;
    QrTransportador: TmySQLQuery;
    DsTransportador: TDataSource;
    QrIns: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrSetores: TmySQLQuery;
    DsSetores: TDataSource;
    QrSetoresCodigo: TIntegerField;
    QrSetoresNome: TWideStringField;
    PainelDados: TPanel;
    RGSit: TRadioGroup;
    Label2: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    TPSolicitado: TDateTimePicker;
    Label5: TLabel;
    TPEntrega: TDateTimePicker;
    Label4: TLabel;
    EdContato: TEdit;
    Label7: TLabel;
    EdLiquido: TdmkEdit;
    Label8: TLabel;
    EdBruto: TdmkEdit;
    Label9: TLabel;
    EdValor: TdmkEdit;
    Label10: TLabel;
    EdEntrada: TdmkEdit;
    Label3: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    Label19: TLabel;
    EdTransportador: TdmkEditCB;
    CBTransportador: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdSetor: TdmkEditCB;
    CBSetor: TdmkDBLookupComboBox;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorRazaoSocial: TWideStringField;
    QrFornecedorFantasia: TWideStringField;
    QrFornecedorRespons1: TWideStringField;
    QrFornecedorRespons2: TWideStringField;
    QrFornecedorPai: TWideStringField;
    QrFornecedorMae: TWideStringField;
    QrFornecedorCNPJ: TWideStringField;
    QrFornecedorIE: TWideStringField;
    QrFornecedorNome: TWideStringField;
    QrFornecedorApelido: TWideStringField;
    QrFornecedorCPF: TWideStringField;
    QrFornecedorRG: TWideStringField;
    QrFornecedorERua: TWideStringField;
    QrFornecedorENumero: TIntegerField;
    QrFornecedorECompl: TWideStringField;
    QrFornecedorEBairro: TWideStringField;
    QrFornecedorECIDADE: TWideStringField;
    QrFornecedorEUF: TSmallintField;
    QrFornecedorECEP: TIntegerField;
    QrFornecedorEPais: TWideStringField;
    QrFornecedorETe1: TWideStringField;
    QrFornecedorETe2: TWideStringField;
    QrFornecedorETe3: TWideStringField;
    QrFornecedorECel: TWideStringField;
    QrFornecedorEFax: TWideStringField;
    QrFornecedorEEMail: TWideStringField;
    QrFornecedorEContato: TWideStringField;
    QrFornecedorENatal: TDateField;
    QrFornecedorPRua: TWideStringField;
    QrFornecedorPNumero: TIntegerField;
    QrFornecedorPCompl: TWideStringField;
    QrFornecedorPBairro: TWideStringField;
    QrFornecedorPCIDADE: TWideStringField;
    QrFornecedorPUF: TSmallintField;
    QrFornecedorPCEP: TIntegerField;
    QrFornecedorPPais: TWideStringField;
    QrFornecedorPTe1: TWideStringField;
    QrFornecedorPTe2: TWideStringField;
    QrFornecedorPTe3: TWideStringField;
    QrFornecedorPCel: TWideStringField;
    QrFornecedorPFax: TWideStringField;
    QrFornecedorPEMail: TWideStringField;
    QrFornecedorPContato: TWideStringField;
    QrFornecedorPNatal: TDateField;
    QrFornecedorSexo: TWideStringField;
    QrFornecedorResponsavel: TWideStringField;
    QrFornecedorProfissao: TWideStringField;
    QrFornecedorCargo: TWideStringField;
    QrFornecedorRecibo: TSmallintField;
    QrFornecedorDiaRecibo: TSmallintField;
    QrFornecedorAjudaEmpV: TFloatField;
    QrFornecedorAjudaEmpP: TFloatField;
    QrFornecedorCliente1: TWideStringField;
    QrFornecedorCliente2: TWideStringField;
    QrFornecedorFornece1: TWideStringField;
    QrFornecedorFornece2: TWideStringField;
    QrFornecedorFornece3: TWideStringField;
    QrFornecedorFornece4: TWideStringField;
    QrFornecedorTerceiro: TWideStringField;
    QrFornecedorCadastro: TDateField;
    QrFornecedorInformacoes: TWideStringField;
    QrFornecedorLogo: TBlobField;
    QrFornecedorVeiculo: TIntegerField;
    QrFornecedorMensal: TWideStringField;
    QrFornecedorObservacoes: TWideMemoField;
    QrFornecedorTipo: TSmallintField;
    QrFornecedorLk: TIntegerField;
    QrFornecedorGrupo: TIntegerField;
    QrFornecedorDataCad: TDateField;
    QrFornecedorDataAlt: TDateField;
    QrFornecedorUserCad: TSmallintField;
    QrFornecedorUserAlt: TSmallintField;
    QrFornecedorCRua: TWideStringField;
    QrFornecedorCNumero: TIntegerField;
    QrFornecedorCCompl: TWideStringField;
    QrFornecedorCBairro: TWideStringField;
    QrFornecedorCCIDADE: TWideStringField;
    QrFornecedorCUF: TSmallintField;
    QrFornecedorCCEP: TIntegerField;
    QrFornecedorCPais: TWideStringField;
    QrFornecedorCTel: TWideStringField;
    QrFornecedorCFax: TWideStringField;
    QrFornecedorCCel: TWideStringField;
    QrFornecedorCContato: TWideStringField;
    QrFornecedorLRua: TWideStringField;
    QrFornecedorLNumero: TIntegerField;
    QrFornecedorLCompl: TWideStringField;
    QrFornecedorLBairro: TWideStringField;
    QrFornecedorLCIDADE: TWideStringField;
    QrFornecedorLUF: TSmallintField;
    QrFornecedorLCEP: TIntegerField;
    QrFornecedorLPais: TWideStringField;
    QrFornecedorLTel: TWideStringField;
    QrFornecedorLFax: TWideStringField;
    QrFornecedorLCel: TWideStringField;
    QrFornecedorLContato: TWideStringField;
    QrFornecedorComissao: TFloatField;
    QrFornecedorSituacao: TSmallintField;
    QrFornecedorNivel: TWideStringField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    QrTransportadorCodigo: TIntegerField;
    QrTransportadorRazaoSocial: TWideStringField;
    QrTransportadorFantasia: TWideStringField;
    QrTransportadorRespons1: TWideStringField;
    QrTransportadorRespons2: TWideStringField;
    QrTransportadorPai: TWideStringField;
    QrTransportadorMae: TWideStringField;
    QrTransportadorCNPJ: TWideStringField;
    QrTransportadorIE: TWideStringField;
    QrTransportadorNome: TWideStringField;
    QrTransportadorApelido: TWideStringField;
    QrTransportadorCPF: TWideStringField;
    QrTransportadorRG: TWideStringField;
    QrTransportadorERua: TWideStringField;
    QrTransportadorENumero: TIntegerField;
    QrTransportadorECompl: TWideStringField;
    QrTransportadorEBairro: TWideStringField;
    QrTransportadorECIDADE: TWideStringField;
    QrTransportadorEUF: TSmallintField;
    QrTransportadorECEP: TIntegerField;
    QrTransportadorEPais: TWideStringField;
    QrTransportadorETe1: TWideStringField;
    QrTransportadorETe2: TWideStringField;
    QrTransportadorETe3: TWideStringField;
    QrTransportadorECel: TWideStringField;
    QrTransportadorEFax: TWideStringField;
    QrTransportadorEEMail: TWideStringField;
    QrTransportadorEContato: TWideStringField;
    QrTransportadorENatal: TDateField;
    QrTransportadorPRua: TWideStringField;
    QrTransportadorPNumero: TIntegerField;
    QrTransportadorPCompl: TWideStringField;
    QrTransportadorPBairro: TWideStringField;
    QrTransportadorPCIDADE: TWideStringField;
    QrTransportadorPUF: TSmallintField;
    QrTransportadorPCEP: TIntegerField;
    QrTransportadorPPais: TWideStringField;
    QrTransportadorPTe1: TWideStringField;
    QrTransportadorPTe2: TWideStringField;
    QrTransportadorPTe3: TWideStringField;
    QrTransportadorPCel: TWideStringField;
    QrTransportadorPFax: TWideStringField;
    QrTransportadorPEMail: TWideStringField;
    QrTransportadorPContato: TWideStringField;
    QrTransportadorPNatal: TDateField;
    QrTransportadorSexo: TWideStringField;
    QrTransportadorResponsavel: TWideStringField;
    QrTransportadorProfissao: TWideStringField;
    QrTransportadorCargo: TWideStringField;
    QrTransportadorRecibo: TSmallintField;
    QrTransportadorDiaRecibo: TSmallintField;
    QrTransportadorAjudaEmpV: TFloatField;
    QrTransportadorAjudaEmpP: TFloatField;
    QrTransportadorCliente1: TWideStringField;
    QrTransportadorCliente2: TWideStringField;
    QrTransportadorFornece1: TWideStringField;
    QrTransportadorFornece2: TWideStringField;
    QrTransportadorFornece3: TWideStringField;
    QrTransportadorFornece4: TWideStringField;
    QrTransportadorTerceiro: TWideStringField;
    QrTransportadorCadastro: TDateField;
    QrTransportadorInformacoes: TWideStringField;
    QrTransportadorLogo: TBlobField;
    QrTransportadorVeiculo: TIntegerField;
    QrTransportadorMensal: TWideStringField;
    QrTransportadorObservacoes: TWideMemoField;
    QrTransportadorTipo: TSmallintField;
    QrTransportadorLk: TIntegerField;
    QrTransportadorGrupo: TIntegerField;
    QrTransportadorDataCad: TDateField;
    QrTransportadorDataAlt: TDateField;
    QrTransportadorUserCad: TSmallintField;
    QrTransportadorUserAlt: TSmallintField;
    QrTransportadorCRua: TWideStringField;
    QrTransportadorCNumero: TIntegerField;
    QrTransportadorCCompl: TWideStringField;
    QrTransportadorCBairro: TWideStringField;
    QrTransportadorCCIDADE: TWideStringField;
    QrTransportadorCUF: TSmallintField;
    QrTransportadorCCEP: TIntegerField;
    QrTransportadorCPais: TWideStringField;
    QrTransportadorCTel: TWideStringField;
    QrTransportadorCFax: TWideStringField;
    QrTransportadorCCel: TWideStringField;
    QrTransportadorCContato: TWideStringField;
    QrTransportadorLRua: TWideStringField;
    QrTransportadorLNumero: TIntegerField;
    QrTransportadorLCompl: TWideStringField;
    QrTransportadorLBairro: TWideStringField;
    QrTransportadorLCIDADE: TWideStringField;
    QrTransportadorLUF: TSmallintField;
    QrTransportadorLCEP: TIntegerField;
    QrTransportadorLPais: TWideStringField;
    QrTransportadorLTel: TWideStringField;
    QrTransportadorLFax: TWideStringField;
    QrTransportadorLCel: TWideStringField;
    QrTransportadorLContato: TWideStringField;
    QrTransportadorComissao: TFloatField;
    QrTransportadorSituacao: TSmallintField;
    QrTransportadorNivel: TWideStringField;
    QrTransportadorNOMEENTIDADE: TWideStringField;
    Label11: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    QrCI: TmySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    DsCI: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmPQPedNew: TFmPQPedNew;

implementation

uses UnMyObjects, PQPed, Module;

{$R *.DFM}

procedure TFmPQPedNew.BtDesisteClick(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
     UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PQPed', Geral.IMV(EdCodigo.Text));
  FmPQPed.ImgTipo.SQLType := stLok;
  FmPQPed.GBConfirma.Visible:=False;
  FmPQPed.GBControle.Visible:=True;
  Close;
end;

procedure TFmPQPedNew.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransportador, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSetores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
end;

procedure TFmPQPedNew.BtConfirmaClick(Sender: TObject);
var
  Fornecedor, Transportador, Setor, CliInt: Integer;
begin
  Fornecedor := Geral.IMV(EdFornecedor.Text);
  Transportador := Geral.IMV(EdTransportador.Text);
  Setor := Geral.IMV(EdSetor.Text);
  CliInt := Geral.IMV(EdCliInt.Text);
  if ImgTipo.SQLType = stIns then
  begin
    QrIns.Params[0].AsDate := TPSolicitado.Date;
    QrIns.Params[1].AsInteger := Fornecedor;
    QrIns.Params[2].AsDate := TPEntrega.Date;
    QrIns.Params[3].AsInteger := Transportador;
    QrIns.Params[4].AsString := EdContato.Text;
    QrIns.Params[5].AsInteger := Setor;
    QrIns.Params[6].AsInteger := RGSit.ItemIndex;
    QrIns.Params[7].AsInteger := CliInt;
    QrIns.Params[8].AsInteger := Geral.IMV(EdCodigo.Text);
    QrIns.ExecSQL;
  end
  else
  begin
    QrUpd.Params[0].AsDate := TPSolicitado.Date;
    QrUpd.Params[1].AsInteger := Fornecedor;
    QrUpd.Params[2].AsDate := TPEntrega.Date;
    QrUpd.Params[3].AsInteger := Transportador;
    QrUpd.Params[4].AsString := EdContato.Text;
    QrUpd.Params[5].AsInteger := Setor;
    QrUpd.Params[6].AsInteger := RGSit.ItemIndex;
    QrUpd.Params[7].AsInteger := CliInt;
    QrUpd.Params[8].AsInteger := Geral.IMV(EdCodigo.Text);
    QrUpd.ExecSQL;
  end;
  Close;
end;

procedure TFmPQPedNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if ImgTipo.SQLType = stUpd then RGSit.Enabled := True;
end;

procedure TFmPQPedNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
