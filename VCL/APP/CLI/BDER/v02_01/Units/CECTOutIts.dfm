object FmCECTOutIts: TFmCECTOutIts
  Left = 339
  Top = 185
  Caption = 'PST-_CECT-003 :: Edi'#231#227'o de Item de Devolu'#231#227'o de CECT'
  ClientHeight = 612
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 792
    Height = 392
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdit1
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 20
        Width = 57
        Height = 21
        DataField = 'Codigo'
        DataSource = DataSource1
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 64
        Top = 20
        Width = 717
        Height = 21
        DataField = 'NOMECLI'
        DataSource = DataSource1
        TabOrder = 1
      end
    end
    object PnAdd: TPanel
      Left = 0
      Top = 329
      Width = 792
      Height = 63
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 236
        Height = 63
        Align = alLeft
        Caption = ' Dados de baixa: '
        TabOrder = 0
        object Label4: TLabel
          Left = 8
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Peso (kg):'
        end
        object Label6: TLabel
          Left = 84
          Top = 16
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object Label7: TLabel
          Left = 146
          Top = 16
          Width = 50
          Height = 13
          Caption = 'Valor total:'
        end
        object EdOutQtdkg: TdmkEdit
          Left = 8
          Top = 32
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdOutQtdkgChange
          OnEnter = EdOutQtdkgEnter
        end
        object EdOutQtdPc: TdmkEdit
          Left = 84
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 1
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdOutQtdPcChange
          OnEnter = EdOutQtdPcEnter
        end
        object EdOutQtdVal: TdmkEdit
          Left = 146
          Top = 32
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdOutQtdValChange
          OnEnter = EdOutQtdValEnter
        end
      end
      object GroupBox2: TGroupBox
        Left = 236
        Top = 0
        Width = 556
        Height = 63
        Align = alClient
        Caption = ' Saldos resultantes:'
        TabOrder = 1
        object Label2: TLabel
          Left = 8
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Peso (kg):'
        end
        object Label3: TLabel
          Left = 84
          Top = 16
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object Label5: TLabel
          Left = 146
          Top = 16
          Width = 50
          Height = 13
          Caption = 'Valor total:'
        end
        object EdSdoQtdkg: TdmkEdit
          Left = 8
          Top = 32
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdSdoQtdPc: TdmkEdit
          Left = 84
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 1
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdSdoQtdVal: TdmkEdit
          Left = 146
          Top = 32
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object GradeCECTInn: TdmkDBGrid
      Left = 0
      Top = 48
      Width = 792
      Height = 281
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID Entrada'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataE'
          Title.Caption = 'Data E.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NFInn'
          Title.Caption = 'NF entrada'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InnQtdkg'
          Title.Caption = 'Peso kg E.'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InnQtdPc'
          Title.Caption = 'Pe'#231'as E.'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InnQtdVal'
          Title.Caption = 'Valor E.'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NFRef'
          Title.Caption = 'Ref. NF'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoQtdkg'
          Title.Caption = 'Saldo kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoQtdPc'
          Title.Caption = 'Saldo pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoQtdVal'
          Title.Caption = 'Saldo valor'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsCECTInn
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = GradeCECTInnDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID Entrada'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataE'
          Title.Caption = 'Data E.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NFInn'
          Title.Caption = 'NF entrada'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InnQtdkg'
          Title.Caption = 'Peso kg E.'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InnQtdPc'
          Title.Caption = 'Pe'#231'as E.'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InnQtdVal'
          Title.Caption = 'Valor E.'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NFRef'
          Title.Caption = 'Ref. NF'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoQtdkg'
          Title.Caption = 'Saldo kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoQtdPc'
          Title.Caption = 'Saldo pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoQtdVal'
          Title.Caption = 'Saldo valor'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 473
        Height = 32
        Caption = 'Edi'#231#227'o de Item de Devolu'#231#227'o de CECT'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 473
        Height = 32
        Caption = 'Edi'#231#227'o de Item de Devolu'#231#227'o de CECT'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 473
        Height = 32
        Caption = 'Edi'#231#227'o de Item de Devolu'#231#227'o de CECT'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBControla: TGroupBox
    Left = 0
    Top = 548
    Width = 792
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 644
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 6
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 484
    Width = 792
    Height = 64
    Align = alBottom
    TabOrder = 4
    Visible = False
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel6: TPanel
        Left = 644
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn1: TBitBtn
          Tag = 15
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Desiste'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BitBtn1Click
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object DataSource1: TDataSource
    DataSet = FmCECTOut.QrClientesPesq
    Left = 8
    Top = 8
  end
  object QrCECTInn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT inn.*'
      'FROM cectinn inn'
      'WHERE inn.SdoQtdPc>0'
      'AND inn.Cliente=:P0'
      '')
    Left = 36
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCECTInnCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCECTInnCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrCECTInnDataE: TDateField
      FieldName = 'DataE'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCECTInnNFInn: TIntegerField
      FieldName = 'NFInn'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCECTInnNFRef: TIntegerField
      FieldName = 'NFRef'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCECTInnInnQtdkg: TFloatField
      FieldName = 'InnQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCECTInnInnQtdPc: TFloatField
      FieldName = 'InnQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCECTInnInnQtdVal: TFloatField
      FieldName = 'InnQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCECTInnSdoQtdkg: TFloatField
      FieldName = 'SdoQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCECTInnSdoQtdPc: TFloatField
      FieldName = 'SdoQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCECTInnSdoQtdVal: TFloatField
      FieldName = 'SdoQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsCECTInn: TDataSource
    DataSet = QrCECTInn
    Left = 64
    Top = 8
  end
end
