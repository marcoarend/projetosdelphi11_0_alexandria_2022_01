unit CreateBlueDerm;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables, dmkGeral;

type
  TNomeTabRecriaTempTable = (
    ntrttEmit, ntrttEmitCus, ntrttEmitFlu, ntrttEmitIts,
    ntrttMoviPQ, ntrttMoviPQ2, ntrttMoviPQ3,
    ntrttSetores,
    ntrttTintasImpriFlu, ntrttTintasImpriIts,
    ntrttWBMovImp1, ntrttWBMovImp2, ntrttWBMovImp3,
    ntrttPQBIts, ntrttPQB2_Resgata,
    ntrttPQRAjuInn, ntrttPQRAjuOut, ntrttPQRMul,
    ntrttPQX, ntrttPQO,
    ntrttFormulas, ntrttFormulasIts,
    ntrttPQBCiPrevia);
  TAcaoCreate = (acDrop, acCreate, acFind);
  TCreateBlueDerm = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrttEmit(Qry: TmySQLQuery);
    procedure Cria_ntrttEmitCus(Qry: TmySQLQuery);
    procedure Cria_ntrttEmitFlu(Qry: TmySQLQuery);
    procedure Cria_ntrttEmitIts(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttMoviPQ(Qry: TmySQLQuery);
    procedure Cria_ntrttMoviPQ2(Qry: TmySQLQuery);
    procedure Cria_ntrttMoviPQ3(Qry: TmySQLQuery);
    procedure Cria_ntrttSetores(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttTintasImpriFlu(Qry: TmySQLQuery);
    procedure Cria_ntrttTintasImpriIts(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttWBMovImp1(Qry: TmySQLQuery);
    procedure Cria_ntrttWBMovImp2(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttPQBIts(Qry: TmySQLQuery);
    procedure Cria_ntrttPQB2_Resgata(Qry: TmySQLQuery);
    procedure Cria_ntrttPQRAjuInn(Qry: TmySQLQuery);
    procedure Cria_ntrttPQRAjuOut(Qry: TmySQLQuery);
    procedure Cria_ntrttPQRMul(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttPQO(Qry: TmySQLQuery);
    procedure Cria_ntrttPQX(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttFormulas(Qry: TmySQLQuery);
    procedure Cria_ntrttFormulasIts(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttPQBCiPrevia(Qry: TmySQLQuery);
    //
  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  UnCreateBlueDerm: TCreateBlueDerm;

implementation

uses UnMyObjects, Module;

procedure TCreateBlueDerm.Cria_ntrttEmit(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DataEmis                       datetime                                   ,');
  Qry.SQL.Add('  Status                         tinyint(1)                                 ,');
  Qry.SQL.Add('  Numero                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NOMECI                         varchar(100)                               ,');
  Qry.SQL.Add('  NOMESETOR                      varchar(20)                                ,');
  Qry.SQL.Add('  Tecnico                        varchar(20)                                ,');
  Qry.SQL.Add('  NOME                           varchar(100)                               ,');
  Qry.SQL.Add('  ClienteI                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Tipificacao                    tinyint(4)                                 ,');
  Qry.SQL.Add('  TempoR                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TempoP                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Setor                          tinyint(4)                                 ,');
  Qry.SQL.Add('  Espessura                      varchar(15)                                ,');
  Qry.SQL.Add('  DefPeca                        varchar(15)                                ,');
  Qry.SQL.Add('  Peso                           double(15,3)                               ,');
  Qry.SQL.Add('  Custo                          double(15,4)                               ,');
  Qry.SQL.Add('  Qtde                           double(15,3)                               ,');
  Qry.SQL.Add('  AreaM2                         double(15,2)                               ,');
  Qry.SQL.Add('  Fulao                          varchar(5)                                 ,');
  Qry.SQL.Add('  Obs                            varchar(255)                               ,');
  Qry.SQL.Add('  Tipific                        tinyint(4)                                 ,');
  Qry.SQL.Add('  SetrEmi                        tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SourcMP                        tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Cod_Espess                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CodDefPeca                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoTo                        double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  CustoKg                        double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CustoM2                        double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  CusUSM2                        double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  EmitGru                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Retrabalho                     tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SemiCodPeca                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SemiTxtPeca                    varchar(15)                                ,');
  Qry.SQL.Add('  SemiPeso                       double(15,3)                               ,');
  Qry.SQL.Add('  SemiQtde                       double(15,3)                               ,');
  Qry.SQL.Add('  SemiAreaM2                     double(15,2)                               ,');
  Qry.SQL.Add('  SemiRendim                     double(15,10) NULL  DEFAULT "-100.0000000000",');
  Qry.SQL.Add('  SemiCodEspe                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SemiTxtEspe                    varchar(15)                                ,');
  Qry.SQL.Add('  BRL_USD                        double(15,6)                               ,');
  Qry.SQL.Add('  BRL_EUR                        double(15,6)                               ,');
  Qry.SQL.Add('  DtaCambio                      date                                       ,');
  Qry.SQL.Add('  VSMovCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  HoraIni                        time         NOT NULL  DEFAULT "0:00:00"   ,');
  Qry.SQL.Add('  GraCorCad                      int(11)      NOT NULL  DEFAULT "-1"        ,');
  Qry.SQL.Add('  NoGraCorCad                    varchar(30)                                ,');
  Qry.SQL.Add('  SemiCodEspReb                  int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Lk                             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DataCad                        date                                       ,');
  Qry.SQL.Add('  DataAlt                        date                                       ,');
  Qry.SQL.Add('  UserCad                        int(11)                                    ,');
  Qry.SQL.Add('  UserAlt                        int(11)                                    ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)   NOT NULL DEFAULT "1"          ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AWServerID                     int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AWStatSinc                     tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttEmitCus(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MPIn                           int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Formula                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataEmis                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Peso                           double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Custo                          double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  Pecas                          double(15,1) NOT NULL  DEFAULT "0.0"       ,');
  Qry.SQL.Add('  MPVIts                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  PercTotCus                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  Lk                             int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  DataCad                        date                                       ,');
  Qry.SQL.Add('  DataAlt                        date                                       ,');
  Qry.SQL.Add('  UserCad                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  UserAlt                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttEmitFlu(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  OrdemTin                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  OrdemFlu                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TintasFlu                      int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DescriFlu                      varchar(255)                               ,');
  Qry.SQL.Add('  NomeFlu                        varchar(100)                               ,');
  Qry.SQL.Add('  NomeTin                        varchar(30)                                ,');
  Qry.SQL.Add('  TintasTin                      int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  InfoCargM2                     float                                      ,');
  Qry.SQL.Add('  GramasM2                       float                                      ,');
  Qry.SQL.Add('  GramasTo                       float                                      ,');
  Qry.SQL.Add('  CustoTo                        float                                      ,');
  Qry.SQL.Add('  CustoKg                        float                                      ,');
  Qry.SQL.Add('  AreaM2                         float                                      ,');
  Qry.SQL.Add('  CustoM2                        float                                      ,');
  Qry.SQL.Add('  CusUSM2                        float                                      ,');
  Qry.SQL.Add('  OpProc                         tinyint(1)   NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  Lk                             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DataCad                        date                                       ,');
  Qry.SQL.Add('  DataAlt                        date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  UserCad                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  UserAlt                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)   NOT NULL DEFAULT "1"          ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AWServerID                     int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AWStatSinc                     tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttEmitIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NomePQ                         varchar(50)                                ,');
  Qry.SQL.Add('  PQCI                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cli_Orig                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cli_Dest                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CustoPadrao                    double(15,4)                               ,');
  Qry.SQL.Add('  MoedaPadrao                    int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Numero                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ordem                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Porcent                        double(15,3)                               ,');
  Qry.SQL.Add('  Produto                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TempoR                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TempoP                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  pH                             double(15,2)                               ,');
  Qry.SQL.Add('  Be                             double(15,2)                               ,');
  Qry.SQL.Add('  Obs                            varchar(20)                                ,');
  Qry.SQL.Add('  Diluicao                       double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Custo                          double(15,2)                               ,');
  Qry.SQL.Add('  Minimo                         double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Maximo                         double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Graus                          double                                     ,');
  Qry.SQL.Add('  Boca                           char(1)                                    ,');
  Qry.SQL.Add('  Processo                       varchar(30)                                ,');
  Qry.SQL.Add('  ObsProces                      varchar(120)                               ,');
  Qry.SQL.Add('  Peso_PQ                        double(15,3)                               ,');
  Qry.SQL.Add('  ProdutoCI                      int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  pH_Min                         double(15,2)                               ,');
  Qry.SQL.Add('  pH_Max                         double(15,2)                               ,');
  Qry.SQL.Add('  Be_Min                         double(15,2)                               ,');
  Qry.SQL.Add('  Be_Max                         double(15,2)                               ,');
  Qry.SQL.Add('  GC_Min                         double                                     ,');
  Qry.SQL.Add('  GC_Max                         double                                     ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  OrdemFlu                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  OrdemIts                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Tintas                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  GramasTi                       float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  GramasKg                       float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  GramasTo                       float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SiglaMoeda                     varchar(10)                                ,');
  Qry.SQL.Add('  PrecoMo_Kg                     float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  Cotacao_Mo                     float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CustoRS_Kg                     float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CustoRS_To                     float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CustoMedio                     float        NOT NULL DEFAULT "0.000"      ,');
  //
  Qry.SQL.Add('  Lk                             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DataCad                        date                                       ,');
  Qry.SQL.Add('  DataAlt                        date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  UserCad                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  UserAlt                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)   NOT NULL DEFAULT "1"          ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AWServerID                     int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AWStatSinc                     tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttFormulas(Qry: TmySQLQuery);
begin
//
end;

procedure TCreateBlueDerm.Cria_ntrttFormulasIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                           varchar(50)                                ,');
  Qry.SQL.Add('  PQCI                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CustoPadrao                    double(15,4)                               ,');
  Qry.SQL.Add('  CustoMedio                     float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CusIntrn                       float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  MoedaPadrao                    int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ProdutoCI                      int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Numero                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ordem                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Reordem                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Processo                       varchar(30)                                ,');
  Qry.SQL.Add('  Porcent                        double(15,3)                               ,');
  Qry.SQL.Add('  Produto                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TempoR                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TempoP                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  pH                             double(15,2)                               ,');
  Qry.SQL.Add('  pH_Min                         double(15,2)                               ,');
  Qry.SQL.Add('  pH_Max                         double(15,2)                               ,');
  Qry.SQL.Add('  Be                             double(15,2)                               ,');
  Qry.SQL.Add('  Be_Min                         double(15,2)                               ,');
  Qry.SQL.Add('  Be_Max                         double(15,2)                               ,');
  Qry.SQL.Add('  GC_Min                         double                                     ,');
  Qry.SQL.Add('  GC_Max                         double                                     ,');
  Qry.SQL.Add('  Obs                            varchar(20)                                ,');
  Qry.SQL.Add('  ObsProces                      varchar(120)                               ,');
  Qry.SQL.Add('  SC                             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Reciclo                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Diluicao                       double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  CProd                          double(15,2)                               ,');
  Qry.SQL.Add('  CSolu                          double(15,2)                               ,');
  Qry.SQL.Add('  Custo                          double(15,2)                               ,');
  Qry.SQL.Add('  Minimo                         double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Maximo                         double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Graus                          double                                     ,');
  Qry.SQL.Add('  Boca                           char(1)                                    ,');
  Qry.SQL.Add('  EhGGX                          tinyint(1)   NOT NULL DEFAULT "1"          ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)   NOT NULL DEFAULT "1"          ,');
  //
(*
  Qry.SQL.Add('  Cli_Orig                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cli_Dest                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Peso_PQ                        double(15,3)                               ,');
  //
  Qry.SQL.Add('  OrdemFlu                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  OrdemIts                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Tintas                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  GramasTi                       float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  GramasKg                       float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  GramasTo                       float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SiglaMoeda                     varchar(10)                                ,');
  Qry.SQL.Add('  PrecoMo_Kg                     float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  Cotacao_Mo                     float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CustoRS_Kg                     float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CustoRS_To                     float        NOT NULL DEFAULT "0.000"      ,');
  //
  Qry.SQL.Add('  Lk                             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DataCad                        date                                       ,');
  Qry.SQL.Add('  DataAlt                        date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  UserCad                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  UserAlt                        int(11)      NOT NULL DEFAULT "0"          ,');
  //
*)
  Qry.SQL.Add('  SEQ                            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PESO_PQ                        double(15,7) NOT NULL DEFAULT "0.0000000"  ,');
  Qry.SQL.Add('  PRECO_PQ                       double(15,4) NOT NULL DEFAULT "0.0000"     ,');
  Qry.SQL.Add('  CUSTO_PQ                       double(15,4) NOT NULL DEFAULT "0.0000"     ,');
  Qry.SQL.Add('  pH_All                         varchar(50)                                ,');
  Qry.SQL.Add('  Be_All                         varchar(50)                                ,');
  //
  Qry.SQL.Add('  Inicio                         varchar(50)                                ,');
  Qry.SQL.Add('  Final                          varchar(50)                                ,');
  Qry.SQL.Add('  kgLCusto                       varchar(50)                                ,');
  //
  Qry.SQL.Add('  Densidade                      double(15,7) NOT NULL DEFAULT "0.0000000"  ,');
  Qry.SQL.Add('  UsarDensid                     tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  LITROS_PQ                      double(15,3) NOT NULL DEFAULT "0.000"  ,');
  //
  Qry.SQL.Add('  NxLotes                        varchar(255)                                ,');
  //
  Qry.SQL.Add('  SiglaIQ                        varchar(6)                                  ,');
  //
  Qry.SQL.Add('  Ativo_1                        tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttMoviPQ(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Insumo                         int(11)               DEFAULT "0"          ,');
  Qry.SQL.Add('  NomePQ                         varchar(100)                               ,');
  Qry.SQL.Add('  NomeFO                         varchar(100)                               ,');
  Qry.SQL.Add('  NomeCI                         varchar(100)                               ,');
  Qry.SQL.Add('  NomeSE                         varchar(100)                               ,');
  Qry.SQL.Add('  AntPes                         double(15,3)                               ,');
  Qry.SQL.Add('  AntVal                         double(15,2)                               ,');
  Qry.SQL.Add('  InnPes                         double(15,3)                               ,');
  Qry.SQL.Add('  InnVal                         double(15,2)                               ,');
  Qry.SQL.Add('  OutPes                         double(15,3)                               ,');
  Qry.SQL.Add('  OutVal                         double(15,2)                               ,');
  Qry.SQL.Add('  BalPes                         double(15,3)                               ,');
  Qry.SQL.Add('  BalVal                         double(15,2)                               ,');
  Qry.SQL.Add('  FimPes                         double(15,3)                               ,');
  Qry.SQL.Add('  FimVal                         double(15,2)                               ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttMoviPQ2(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Insumo                         int(11)               DEFAULT "0"          ,');
  Qry.SQL.Add('  NomePQ                         varchar(100)                               ,');
  Qry.SQL.Add('  NomeFO                         varchar(100)                               ,');
  Qry.SQL.Add('  NomeCI                         varchar(100)                               ,');
  Qry.SQL.Add('  NomeSE                         varchar(100)                               ,');
  Qry.SQL.Add('  AntPes                         double(15,3)                               ,');
  Qry.SQL.Add('  AntVal                         double(15,2)                               ,');
  Qry.SQL.Add('  InnPes                         double(15,3)                               ,');
  Qry.SQL.Add('  InnVal                         double(15,2)                               ,');
  Qry.SQL.Add('  OutPes                         double(15,3)                               ,');
  Qry.SQL.Add('  OutVal                         double(15,2)                               ,');
  Qry.SQL.Add('  BalPes                         double(15,3)                               ,');
  Qry.SQL.Add('  BalVal                         double(15,2)                               ,');
  Qry.SQL.Add('  FimPes                         double(15,3)                               ,');
  Qry.SQL.Add('  FimVal                         double(15,2)                               ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttMoviPQ3(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Insumo                         int(11)               DEFAULT "0"          ,');
  Qry.SQL.Add('  NomePQ                         varchar(100)          DEFAULT "?"          ,');
  Qry.SQL.Add('  NomeFO                         varchar(100)          DEFAULT "?"          ,');
  Qry.SQL.Add('  NomeCI                         varchar(100)          DEFAULT "?"          ,');
  Qry.SQL.Add('  NomeSE                         varchar(100)          DEFAULT "?"          ,');
  Qry.SQL.Add('  AntPes                         double(15,3)          DEFAULT "0.000"      ,');
  Qry.SQL.Add('  AntVal                         double(15,2)          DEFAULT "0.00"       ,');
  Qry.SQL.Add('  AntPad                         double(15,2)          DEFAULT "0.00"       ,');
  Qry.SQL.Add('  InnPes                         double(15,3)          DEFAULT "0.000"      ,');
  Qry.SQL.Add('  InnVal                         double(15,2)          DEFAULT "0.00"       ,');
  Qry.SQL.Add('  InnPad                         double(15,2)          DEFAULT "0.00"       ,');
  Qry.SQL.Add('  OutPes                         double(15,3)          DEFAULT "0.000"      ,');
  Qry.SQL.Add('  OutVal                         double(15,2)          DEFAULT "0.00"       ,');
  Qry.SQL.Add('  OutPad                         double(15,2)          DEFAULT "0.00"       ,');
  Qry.SQL.Add('  DvlPes                         double(15,3)          DEFAULT "0.000"      ,');
  Qry.SQL.Add('  DvlVal                         double(15,2)          DEFAULT "0.00"       ,');
  Qry.SQL.Add('  DvlPad                         double(15,2)          DEFAULT "0.00"       ,');
  Qry.SQL.Add('  BalPes                         double(15,3)          DEFAULT "0.000"      ,');
  Qry.SQL.Add('  BalVal                         double(15,2)          DEFAULT "0.00"       ,');
  Qry.SQL.Add('  BalPad                         double(15,2)          DEFAULT "0.00"       ,');
  Qry.SQL.Add('  FimPes                         double(15,3)          DEFAULT "0.000"      ,');
  Qry.SQL.Add('  FimVal                         double(15,2)          DEFAULT "0.00"       ,');
  Qry.SQL.Add('  FimPad                         double(15,2)          DEFAULT "0.00"       ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;

end;

procedure TCreateBlueDerm.Cria_ntrttPQB2_Resgata(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Insumo                         int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Peso                           double(15,3)                               ,');
  Qry.SQL.Add('  Valor                          double(15,2)                               ,');
  Qry.SQL.Add('  Preco                          double(15,6)                               ,');
  Qry.SQL.Add('  NO_PQ                          varchar(50)                                ,');
  //
  Qry.SQL.Add('  QtdBal                         double(15,3)                               ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Insumo)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttPQBCiPrevia(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliOrig                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Insumo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PesoInic                       double(15,3)                               ,');
  Qry.SQL.Add('  ValorInic                      double(15,2)                               ,');
  Qry.SQL.Add('  PesoAtuA                       double(15,3)                               ,');
  Qry.SQL.Add('  ValorAtuA                      double(15,2)                               ,');
  Qry.SQL.Add('  PesoAtuB                       double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorAtuB                      double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  PesoAtuC                       double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorAtuC                      double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  PesoAtuT                       double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorAtuT                      double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  PesoPeIn                       double(15,3)                               ,');
  Qry.SQL.Add('  ValorPeIn                      double(15,2)                               ,');
  Qry.SQL.Add('  PesoPeBx                       double(15,3)                               ,');
  Qry.SQL.Add('  ValorPeBx                      double(15,2)                               ,');
  Qry.SQL.Add('  PesoAjIn                       double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorAjIn                      double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  PesoAjBx                       double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorAjBx                      double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  PesoInfo                       double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorInfo                      double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  PesoDife                       double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorDife                      double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Serie                          int(3)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NF                             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  xLote                          varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  dFab                           date         NOT NULL DEFAULT "00000-00-00",');
  Qry.SQL.Add('  dVal                           date         NOT NULL DEFAULT "00000-00-00",');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttPQBIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PQ                             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CI                             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoPadrao                    double(15,4)           DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  Peso                           double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Valor                          double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  Custo                          double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  AjPeso                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AjValor                        double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  AjCusto                        double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  NO_PQ                          varchar(50)                                ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttPQO(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Nome                           varchar(50)                                ,');
  Qry.SQL.Add('  Setor                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoInsumo                    double(15,4)           DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CustoTotal                     double(15,4)           DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  DataB                          date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  EmitGru                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMovCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraCorCad                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SemiCodEspReb                  int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Codigo)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttPQRAjuInn(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Data                           date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  NF                             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         varchar(60)  NOT NULL  DEFAULT "?"         ,');
  Qry.SQL.Add('  Nome                           varchar(255)                               ,');
  Qry.SQL.Add('  Und                            varchar(60)  NOT NULL  DEFAULT "?"         ,');
  Qry.SQL.Add('  Qtd                            double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  VlUni                          double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  VlTot                          double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NO_Cliente                     varchar(255)                               ,');
  Qry.SQL.Add('  CFOP                           varchar(10)                                ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  //Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttPQRAjuOut(Qry: TmySQLQuery);
begin
  Cria_ntrttPQRAjuInn(Qry);
end;

procedure TCreateBlueDerm.Cria_ntrttPQRMul(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Nome                           varchar(255)                               ,');
  //
  Qry.SQL.Add('  QtdInn                         double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  VlUInn                         double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  VlTInn                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  QtdBxa                         double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  VlUBxa                         double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  VlTBxa                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  QtdRet                         double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  VlURet                         double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  VlTRet                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  QtdStq                         double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  VlUStq                         double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  VlTStq                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  CodErr                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TxtErr                         varchar(255)                               ,');
  Qry.SQL.Add('  QtdErr                         double(15,3)           DEFAULT "0.000"     ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Codigo)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttPQX(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  DataX                          date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  OrigemCodi                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  OrigemCtrl                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Tipo                           mediumint(4) NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliOrig                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  CliDest                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Insumo                         int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Peso                           double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Valor                          double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Retorno                        tinyint(1)             DEFAULT "0"         ,');
  Qry.SQL.Add('  StqMovIts                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  RetQtd                         double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  HowLoad                        tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  StqCenLoc                      int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoPeso                        double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoValr                        double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcePeso                        double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AceValr                        double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  DtCorrApo                      date         NOT NULL  DEFAULT "0000-00-00",');
  //
  Qry.SQL.Add('  PRIMARY KEY (OrigemCodi,OrigemCtrl,Tipo)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttSetores(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttTintasImpriFlu(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  UniqCod                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  OrdemTin                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  OrdemFlu                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NomeFlu                        varchar(100)                               ,');
  Qry.SQL.Add('  DescriFlu                      varchar(255)                               ,');
  Qry.SQL.Add('  NomeTin                        varchar(30)                                ,');
  Qry.SQL.Add('  TintasTin                      int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  InfoCargM2                     float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  GramasM2                       float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  GramasTo                       float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CustoTo                        float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CustoKg                        float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  AreaM2                         float        NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  CustoM2                        float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CusUSM2                        float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  OpProc                         tinyint(1)   NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "1"          ,');
  Qry.SQL.Add('  PRIMARY KEY (Codigo)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttTintasImpriIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  UniqCod                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  OrdemFlu                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  OrdemIts                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Tintas                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Produto                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                           varchar(50)                                ,');
  Qry.SQL.Add('  GramasTi                       float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  GramasKg                       float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  GramasTo                       float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SiglaMoeda                     varchar(10)                                ,');
  Qry.SQL.Add('  PrecoMo_Kg                     float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  Cotacao_Mo                     float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CustoRS_Kg                     float        NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CustoRS_To                     float        NOT NULL DEFAULT "0.000"      ,');
  //
  //Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "1"          ,');
  //Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "1"          ');
  //
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttWBMovImp1(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  PesoKg                         double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  AreaM2                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  AreaP2                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValorT                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(255)                               ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_EMPRESA                     varchar(100)                               ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  OrdGGX                         int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBlueDerm.Cria_ntrttWBMovImp2(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  OrdGrupSeq                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Observ                         varchar(255) NOT NULL                      ,');
  //
  Qry.SQL.Add('  ValorT                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(160)                               ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_MovimID                     varchar(20)                                ,');
  //
  Qry.SQL.Add('  AcumPecas                      double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcumPesoKg                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcumAreaM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcumAreaP2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcumValorT                     double(19,2) NOT NULL DEFAULT "0.00"       ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

function TCreateBlueDerm.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrttEmit:            Nome := Lowercase('_emit_');
      ntrttEmitIts:         Nome := Lowercase('_emit_its_');
      ntrttEmitCus:         Nome := Lowercase('_emit_cus_');
      ntrttEmitFlu:         Nome := Lowercase('_emit_flu_');
      //
      ntrttMoviPQ:          Nome := Lowercase('_movi_pq_');
      ntrttMoviPQ2:         Nome := Lowercase('_movi_pq_2_');
      ntrttMoviPQ3:         Nome := Lowercase('_movi_pq_3_');
      ntrttSetores:         Nome := Lowercase('_setores_');
      //
      ntrttTintasImpriFlu:  Nome := Lowercase('_Tintas_Impri_Flu_');
      ntrttTintasImpriIts:  Nome := Lowercase('_Tintas_Impri_Its_');
      //
      ntrttWBMovImp1:       Nome := Lowercase('_WBMovImp1_');
      ntrttWBMovImp2:       Nome := Lowercase('_WBMovImp2_');
      ntrttWBMovImp3:       Nome := Lowercase('_WBMovImp3_');
      //
      ntrttPQBIts:          Nome := Lowercase('_PQB_Its_');
      ntrttPQB2_Resgata:    Nome := Lowercase('_PQB2_Resgata_');
      ntrttPQRAjuInn:       Nome := Lowercase('_PQRAjuInn_');
      ntrttPQRAjuOut:       Nome := Lowercase('_PQRAjuOut_');
      ntrttPQRMul:          Nome := Lowercase('_PQRMul_');
      ntrttPQX:             Nome := Lowercase('_PQX_');
      ntrttPQO:             Nome := Lowercase('_PQO_');
      //
      ntrttFormulas:        Nome := Lowercase('_Formulas_');
      ntrttFormulasIts:     Nome := Lowercase('_FormulasIts_');
      //
      ntrttPQBCiPrevia:     Nome := Lowercase('_pqb_previa_');
      //
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MB_Erro(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)');
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    //
    ntrttEmit:              Cria_ntrttEmit(Qry);
    ntrttEmitCus:           Cria_ntrttEmitCus(Qry);
    ntrttEmitFlu:           Cria_ntrttEmitFlu(Qry);
    ntrttEmitIts:           Cria_ntrttEmitIts(Qry);
    //
    ntrttMoviPQ:            Cria_ntrttMoviPQ(Qry);
    ntrttMoviPQ2:           Cria_ntrttMoviPQ2(Qry);
    ntrttMoviPQ3:           Cria_ntrttMoviPQ3(Qry);
    ntrttSetores:           Cria_ntrttSetores(Qry);
    //
    ntrttTintasImpriFlu:    Cria_ntrttTintasImpriFlu(Qry);
    ntrttTintasImpriIts:    Cria_ntrttTintasImpriIts(Qry);
    //
    ntrttWBMovImp1:         Cria_ntrttWBMovImp1(Qry);
    ntrttWBMovImp2:         Cria_ntrttWBMovImp2(Qry);
    ntrttWBMovImp3:         Cria_ntrttWBMovImp1(Qry); // C�pia do WBMovImp1
    //
    ntrttPQBIts:            Cria_ntrttPQBIts(Qry);
    ntrttPQB2_Resgata:      Cria_ntrttPQB2_Resgata(Qry);
    ntrttPQRAjuInn:         Cria_ntrttPQRAjuInn(Qry);
    ntrttPQRAjuOut:         Cria_ntrttPQRAjuOut(Qry);
    ntrttPQRMul:            Cria_ntrttPQRMul(Qry);
    ntrttPQX:               Cria_ntrttPQX(Qry);
    ntrttPQO:               Cria_ntrttPQO(Qry);
    //
    ntrttFormulas:          Cria_ntrttFormulas(Qry);
    ntrttFormulasIts:       Cria_ntrttFormulasIts(Qry);
    //
    ntrttPQBCiPrevia:       Cria_ntrttPQBCiPrevia(Qry);
    //
    else Geral.MB_Erro('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!');
  end;
  Result := TabNo;
end;

(*
CREATE TABLE controle(
  MPSubProd                      int(11)                DEFAULT "0"         ,
  Fabricas                       int(11)                DEFAULT "0"         ,
  NFeMPInn                       int(11)      NOT NULL  DEFAULT "0"         ,
  NFeMPIts                       int(11)      NOT NULL  DEFAULT "0"         ,
  CMPTInn                        int(11)      NOT NULL  DEFAULT "0"         ,
  CMPTOut                        int(11)      NOT NULL  DEFAULT "0"         ,
  CMPTOutIts                     int(11)      NOT NULL  DEFAULT "0"         ,
  CanAltBalA                     tinyint(1)   NOT NULL  DEFAULT "0"         ,
  TintasCab                      int(11)      NOT NULL  DEFAULT "0"         ,
  TintasTin                      int(11)      NOT NULL  DEFAULT "0"         ,
  TintasIts                      int(11)      NOT NULL  DEFAULT "0"         ,
  TintasFlu                      int(11)      NOT NULL  DEFAULT "0"         ,
  SetorCal                       int(11)      NOT NULL  DEFAULT "1"         ,
  SetorCur                       int(11)      NOT NULL  DEFAULT "2"         ,
  CECTInn                        int(1)       NOT NULL  DEFAULT "0"         ,
  CECTOut                        int(1)       NOT NULL  DEFAULT "0"         ,
  CECTOutIts                     int(1)       NOT NULL  DEFAULT "0"         ,
  VendaCouro                     tinyint(1)   NOT NULL  DEFAULT "0"         ,
  PortSerBal                     int(11)      NOT NULL  DEFAULT "0"         ,
  PortSerLei                     int(11)      NOT NULL  DEFAULT "0"         ,
  PortSerIts                     int(11)      NOT NULL  DEFAULT "0"         ,
  CtaPQCompr                     int(11)                DEFAULT "0"         ,
  CtaPQFrete                     int(11)                DEFAULT "0"         ,
  CtaMPCompr                     int(11)                DEFAULT "0"         ,
  CtaMPFrete                     int(11)                DEFAULT "0"         ,
  CtaCVVenda                     int(11)                DEFAULT "0"         ,
  CtaCVFrete                     int(11)                DEFAULT "0"         ,
  ImpRecRib                      tinyint(2)   NOT NULL  DEFAULT "0"         ,
  Percula                        tinyint(2)   NOT NULL  DEFAULT "70"        ,
  PQNegBaixa                     tinyint(1)   NOT NULL  DEFAULT "0"         ,
  PQNegEntra                     tinyint(1)   NOT NULL  DEFAULT "0"         ,
  PathLogoDupl                   varchar(255)                               ,
  PathLogoRece                   varchar(255)                               ,
  ArtigosGrupos                  int(11)                DEFAULT "0"         ,
  Fluxos                         int(11)                DEFAULT "0"         ,
  FluxosIts                      int(11)                DEFAULT "0"         ,
  Operacoes                      int(11)                DEFAULT "0"         ,
  MPV                            int(11)                DEFAULT "0"         ,
  MPV2                           int(11)                DEFAULT "0"         ,
  MPP                            int(11)                DEFAULT "0"         ,
  MPVIts                         int(11)                DEFAULT "0"         ,
  MPV2Its                        int(11)                DEFAULT "0"         ,
  MPV2Bxa                        int(11)                DEFAULT "0"         ,
  MPPIts                         int(11)                DEFAULT "0"         ,
  Pallets                        int(11)                DEFAULT "0"         ,
  PalletsIts                     int(11)                DEFAULT "0"         ,
  MPClas                         int(11)                DEFAULT "0"         ,
  MPEstagios                     int(11)                DEFAULT "0"         ,
  MPGrup                         int(11)                DEFAULT "0"         ,
  MPArti                         int(11)                DEFAULT "0"         ,
  MPIn                           int(11)                DEFAULT "0"         ,
  MPInIts                        int(11)                DEFAULT "0"         ,
  MPInDefeitos                   int(11)                DEFAULT "0"         ,
  MPInExp                        int(11)                DEFAULT "0"         ,
  MPInPerdas                     int(11)                DEFAULT "0"         ,
  Defeitosloc                    int(11)      NOT NULL  DEFAULT "0"         ,
  Defeitostip                    int(11)      NOT NULL  DEFAULT "0"         ,
  DefPecas                       int(11)      NOT NULL  DEFAULT "0"         ,
  BalancosIts                    int(11)      NOT NULL  DEFAULT "0"         ,
  PQE                            int(11)                DEFAULT "0"         ,
  PQEIts                         int(11)                DEFAULT "0"         ,
  PQPed                          int(11)                DEFAULT "0"         ,
  PQPedIts                       int(11)                DEFAULT "0"         ,
  DOSGrades                      tinyint(1)   NOT NULL  DEFAULT "0"         ,
  DOSDotLinTop                   tinyint(2)   NOT NULL  DEFAULT "0"         ,
  DOSDotLinBot                   tinyint(2)   NOT NULL  DEFAULT "0"         ,
  Formulas                       int(11)      NOT NULL  DEFAULT "0"         ,
  FormulasIts                    int(11)      NOT NULL  DEFAULT "0"         ,
  Emit                           int(11)                DEFAULT "0"         ,
  EmitCus                        int(11)      NOT NULL  DEFAULT "0"         ,
  EmitIts                        int(11)                DEFAULT "0"         ,
  EmitFlu                        int(11)                DEFAULT "0"         ,
  Equipes                        int(11)                DEFAULT "0"         ,
  Espessuras                     int(11)                DEFAULT "0"         ,
  GruposQuimicos                 int(11)                DEFAULT "0"         ,
  ListaSetores                   int(11)                DEFAULT "0"         ,
  RolPerdas                      int(11)                DEFAULT "0"         ,
  PQ                             int(11)                DEFAULT "0"         ,
  PQFor                          int(11)                DEFAULT "0"         ,
  PQCot                          int(11)                DEFAULT "0"         ,
  PQCli                          int(11)                DEFAULT "0"         ,
  PQx                            int(11)                DEFAULT "0"         ,
  PQG                            int(11)                DEFAULT "0"         ,
  PQO                            int(11)                DEFAULT "0"         ,
  PQRet                          int(11)                DEFAULT "0"         ,
  PQRetIts                       int(11)                DEFAULT "0"         ,
  PQT                            int(11)                DEFAULT "0"         ,
  PQU                            int(11)                DEFAULT "0"         ,
  PQUIts                         int(11)                DEFAULT "0"         ,
  PQ_StqCenCad                   int(11)      NOT NULL  DEFAULT "1"         ,
  MP_StqCenCad                   int(11)      NOT NULL  DEFAULT "1"         ,
  WBNivGer                       tinyint(1)   NOT NULL  DEFAULT "1"         ,
  OSImpInfWB                     tinyint(1)   NOT NULL  DEFAULT "0"         ,
  UsaBRLMedM2                    tinyint(1)   NOT NULL  DEFAULT "1"         ,
  UsaM2Medio                     tinyint(1)   NOT NULL  DEFAULT "1"         ,
  Perfis                         int(11)                DEFAULT "0"         ,
  Senhas                         int(11)                DEFAULT "0"         ,
  EntiSrvPro                     int(11)                DEFAULT "0"         ,
  PlanRelCab                     int(11)                DEFAULT "0"         ,
  PlanRelIts                     int(11)                DEFAULT "0"         ,
  XcelCfgIts                     int(11)                DEFAULT "0"         ,
  XcelCfgCab                     int(11)                DEFAULT "0"         ,
  Anotacoes                      int(11)                DEFAULT "0"         ,
  CustomFR3                      int(11)                DEFAULT "0"         ,
  ContasU                        int(11)                DEFAULT "0"         ,
  EntDefAtr                      int(11)                DEFAULT "0"         ,
  EntAtrCad                      int(11)                DEFAULT "0"         ,
  EntAtrIts                      int(11)                DEFAULT "0"         ,
  BalTopoNom                     int(11)                DEFAULT "2000"      ,
  BalTopoTit                     int(11)                DEFAULT "5000"      ,
  BalTopoPer                     int(11)                DEFAULT "25000"     ,
  CasasProd                      tinyint(1)             DEFAULT "2"         ,
  NCMs                           int(11)                DEFAULT "0"         ,
  ParamsNFs                      int(11)                DEFAULT "0"         ,
  CambioCot                      int(11)                DEFAULT "0"         ,
  CambioMda                      int(11)                DEFAULT "0"         ,
  MoedaBr                        int(11)                DEFAULT "1"         ,
  SecuritStr                     varchar(32)                                ,
  LogoBig1                       varchar(255)                               ,
  EquiCom                        int(11)                DEFAULT "0"         ,
  ContasLnk                      int(11)                DEFAULT "0"         ,
  ContasLnkIts                   int(11)                DEFAULT "0"         ,
  LastBco                        int(11)                DEFAULT "0"         ,
  MyPerJuros                     double(15,2)           DEFAULT "0.00"      ,
  MyPerMulta                     double(15,2)           DEFAULT "0.00"      ,
  EquiGru                        int(11)                DEFAULT "0"         ,
  CNAB_Rem                       int(11)                DEFAULT "0"         ,
  CNAB_Rem_I                     int(11)                DEFAULT "0"         ,
  ContasMes                      int(11)                DEFAULT "0"         ,
  MultiPgto                      int(11)                DEFAULT "0"         ,
  ImpObs                         int(11)                DEFAULT "0"         ,
  ProLaINSSr                     double(15,2)           DEFAULT "11.00"     ,
  ProLaINSSp                     double(15,2)           DEFAULT "20.00"     ,
  VerSalTabs                     int(11)                DEFAULT "0"         ,
  VerBcoTabs                     int(11)                DEFAULT "0"         ,
  CNABCtaTar                     int(11)                DEFAULT "0"         ,
  CNABCtaJur                     int(11)                DEFAULT "0"         ,
  CNABCtaMul                     int(11)                DEFAULT "0"         ,
  CNAB_CaD                       int(11)                DEFAULT "0"         ,
  CNAB_CaG                       int(11)                DEFAULT "0"         ,
  AtzCritic                      int(11)                DEFAULT "0"         ,
  ContasTrf                      int(11)                DEFAULT "0"         ,
  SomaIts                        int(11)                DEFAULT "0"         ,
  ContasAgr                      int(11)                DEFAULT "0"         ,
  VendaCartPg                    int(11)                DEFAULT "0"         ,
  VendaParcPg                    int(11)                DEFAULT "0"         ,
  VendaPeriPg                    int(11)                DEFAULT "0"         ,
  VendaDiasPg                    int(11)                DEFAULT "0"         ,
  LogoNF                         varchar(255)                               ,
  ConfJanela                     int(11)                DEFAULT "0"         ,
  DataPesqAuto                   tinyint(1)             DEFAULT "0"         ,
  MeuLogoPath                    varchar(255)                               ,
  Atividad                       tinyint(1)             DEFAULT "0"         ,
  Cidades                        tinyint(1)             DEFAULT "0"         ,
  Chequez                        tinyint(1)             DEFAULT "0"         ,
  Paises                         tinyint(1)             DEFAULT "0"         ,
  MyPgParc                       tinyint(1)             DEFAULT "0"         ,
  MyPgQtdP                       tinyint(4)             DEFAULT "2"         ,
  MyPgPeri                       tinyint(1)             DEFAULT "0"         ,
  MyPgDias                       tinyint(4)             DEFAULT "7"         ,
  CorRecibo                      int(11)                DEFAULT "1"         ,
  IdleMinutos                    int(11)                DEFAULT "10"        ,
  CambiosData                    date                   DEFAULT "0000-00-00",
  CambiosUsuario                 int(11)                DEFAULT "0"         ,
  reg10                          tinyint(1)             DEFAULT "1"         ,
  reg11                          tinyint(1)             DEFAULT "1"         ,
  reg50                          tinyint(1)             DEFAULT "0"         ,
  reg51                          tinyint(1)             DEFAULT "0"         ,
  reg53                          tinyint(1)             DEFAULT "0"         ,
  reg54                          tinyint(1)             DEFAULT "0"         ,
  reg56                          tinyint(1)             DEFAULT "0"         ,
  reg60                          tinyint(1)             DEFAULT "0"         ,
  reg75                          tinyint(1)             DEFAULT "1"         ,
  reg88                          tinyint(1)             DEFAULT "0"         ,
  reg90                          tinyint(1)             DEFAULT "1"         ,
  NumSerieNF                     tinyint(2)             DEFAULT "0"         ,
  SerieNF                        int(3)                 DEFAULT "0"         ,
  ModeloNF                       tinyint(2)             DEFAULT "0"         ,
  MyPagTip                       tinyint(1)             DEFAULT "1"         ,
  MyPagCar                       tinyint(1)             DEFAULT "0"         ,
  ControlaNeg                    tinyint(1)             DEFAULT "1"         ,
  EventosCad                     tinyint(1)             DEFAULT "0"         ,
  Familias                       tinyint(1)             DEFAULT "0"         ,
  FamiliasIts                    tinyint(1)             DEFAULT "0"         ,
  AskNFOrca                      tinyint(1)             DEFAULT "0"         ,
  PreviewNF                      tinyint(1)             DEFAULT "0"         ,
  OrcaRapido                     tinyint(1)             DEFAULT "0"         ,
  DistriDescoItens               tinyint(1)             DEFAULT "0"         ,
  EntraSemValor                  tinyint(1)             DEFAULT "0"         ,
  MensalSempre                   tinyint(1)             DEFAULT "0"         ,
  BalType                        tinyint(1)             DEFAULT "0"         ,
  OrcaOrdem                      tinyint(1)             DEFAULT "0"         ,
  OrcaLinhas                     tinyint(1)             DEFAULT "0"         ,
  OrcaLFeed                      int(3)                 DEFAULT "0"         ,
  OrcaModelo                     tinyint(1)             DEFAULT "0"         ,
  OrcaRodaPos                    tinyint(1)             DEFAULT "0"         ,
  OrcaRodape                     tinyint(1)             DEFAULT "0"         ,
  OrcaCabecalho                  tinyint(1)             DEFAULT "0"         ,
  CoresRel                       tinyint(1)             DEFAULT "0"         ,
  MoraDD                         double(9,6)            DEFAULT "0.200000"  ,
  Multa                          double(11,4)           DEFAULT "10.0000"   ,
  CFiscalPadr                    varchar(20)                                ,
  SitTribPadr                    varchar(20)                                ,
  CFOPPadr                       varchar(20)                                ,
  AvisosCxaEdit                  tinyint(1)             DEFAULT "0"         ,
  TravaCidade                    tinyint(1)             DEFAULT "0"         ,
  ChConfCab                      int(11)                DEFAULT "0"         ,
  ImpDOS                         int(11)                DEFAULT "0"         ,
  UnidadePadrao                  int(11)                DEFAULT "0"         ,
  ProdutosV                      int(11)                DEFAULT "0"         ,
  CartDespesas                   int(11)                DEFAULT "0"         ,
  Reserva                        tinyint(1)             DEFAULT "0"         ,
  CNPJ                           varchar(18)                                ,
  Versao                         int(11)                DEFAULT "0"         ,
  VerWeb                         int(11)                DEFAULT "0"         ,
  UFPadrao                       int(11)                DEFAULT "0"         ,
  Cidade                         varchar(100)                               ,
  Dono                           int(11)                DEFAULT "-11"       ,
  SoMaiusculas                   char(1)                DEFAULT "F"         ,
  Moeda                          varchar(4)             DEFAULT "R$"        ,
  ErroHora                       int(11)                DEFAULT "0"         ,
  SenhasIts                      int(11)                DEFAULT "0"         ,
  Salarios                       int(11)                DEFAULT "0"         ,
  Entidades                      int(11)                DEFAULT "0"         ,
  EntiCtas                       int(11)                DEFAULT "0"         ,
  UFs                            int(11)                DEFAULT "0"         ,
  ListaECivil                    int(11)                DEFAULT "0"         ,
  Usuario                        int(11)                DEFAULT "0"         ,
  Contas                         int(11)                DEFAULT "0"         ,
  CentroCusto                    int(11)                DEFAULT "0"         ,
  Departamentos                  int(11)                DEFAULT "0"         ,
  Dividas                        int(11)                DEFAULT "0"         ,
  DividasIts                     int(11)                DEFAULT "0"         ,
  DividasPgs                     int(11)                DEFAULT "0"         ,
  Carteiras                      int(11)                DEFAULT "0"         ,
  CarteirasU                     int(11)                DEFAULT "0"         ,
  CartTalCH                      int(11)                DEFAULT "0"         ,
  CartaG                         int(11)                DEFAULT "0"         ,
  Cartas                         int(11)                DEFAULT "0"         ,
  Consignacao                    int(11)                DEFAULT "0"         ,
  Grupo                          int(11)                DEFAULT "0"         ,
  SubGrupo                       int(11)                DEFAULT "0"         ,
  Conjunto                       int(11)                DEFAULT "0"         ,
  Plano                          int(11)                DEFAULT "0"         ,
  Inflacao                       int(11)                DEFAULT "0"         ,
  km                             int(11)                DEFAULT "0"         ,
  kmMedia                        int(11)                DEFAULT "0"         ,
  kmIts                          int(11)                DEFAULT "0"         ,
  Fatura                         int(11)                DEFAULT "0"         ,
  lanctos                        bigint(20)             DEFAULT "0"         ,
  LctoEndoss                     int(11)                DEFAULT "0"         ,
  EntiGrupos                     int(11)                DEFAULT "0"         ,
  EntiContat                     int(11)                DEFAULT "0"         ,
  EntiCargos                     int(11)                DEFAULT "0"         ,
  EntiImgs                       int(11)                DEFAULT "0"         ,
  EntiMail                       int(11)                DEFAULT "0"         ,
  EntiTel                        int(11)                DEFAULT "0"         ,
  EntiTipCto                     int(11)                DEFAULT "0"         ,
  Aparencias                     int(11)                DEFAULT "0"         ,
  Pages                          int(11)                DEFAULT "0"         ,
  MultiEtq                       int(11)                DEFAULT "0"         ,
  EntiTransp                     int(11)                DEFAULT "0"         ,
  EntiRespon                     int(11)                DEFAULT "0"         ,
  EntiCfgRel                     int(11)                DEFAULT "0"         ,
  ExcelGru                       int(11)                DEFAULT "0"         ,
  ExcelGruImp                    int(11)                DEFAULT "0"         ,
  MediaCH                        int(11)                DEFAULT "0"         ,
  ContraSenha                    varchar(50)                                ,
  Imprime                        int(11)                DEFAULT "0"         ,
  ImprimeBand                    int(11)                DEFAULT "0"         ,
  ImprimeView                    int(11)                DEFAULT "0"         ,
  ImprimeEmpr                    int(11)                DEFAULT "0"         ,
  ComProdPerc                    int(11)                DEFAULT "0"         ,
  ComProdEdit                    int(11)                DEFAULT "0"         ,
  ComServPerc                    int(11)                DEFAULT "0"         ,
  ComServEdit                    int(11)                DEFAULT "0"         ,
  PaperLef                       int(11)                DEFAULT "0"         ,
  PaperTop                       int(11)                DEFAULT "0"         ,
  PaperHei                       int(11)                DEFAULT "2790"      ,
  PaperWid                       int(11)                DEFAULT "2160"      ,
  PaperFcl                       int(11)                DEFAULT "0"         ,
  PadrPlacaCar                   varchar(100)                               ,
  ServSMTP                       varchar(50)                                ,
  NomeMailOC                     varchar(50)                                ,
  DonoMailOC                     varchar(50)                                ,
  MailOC                         varchar(80)                                ,
  CorpoMailOC                    text                                       ,
  ConexaoDialUp                  varchar(50)                                ,
  MailCCCega                     varchar(80)                                ,
  ContVen                        int(11)                DEFAULT "0"         ,
  ContCom                        int(11)                DEFAULT "0"         ,
  CartVen                        int(11)                DEFAULT "0"         ,
  CartCom                        int(11)                DEFAULT "0"         ,
  CartDeS                        int(11)                DEFAULT "0"         ,
  CartReS                        int(11)                DEFAULT "0"         ,
  CartDeG                        int(11)                DEFAULT "0"         ,
  CartReG                        int(11)                DEFAULT "0"         ,
  CartCoE                        int(11)                DEFAULT "0"         ,
  CartCoC                        int(11)                DEFAULT "0"         ,
  CartEmD                        int(11)                DEFAULT "0"         ,
  CartEmA                        int(11)                DEFAULT "0"         ,
  MoedaVal                       double(15,6)           DEFAULT "1.000000"  ,
  Tela1                          int(11)                DEFAULT "0"         ,
  ChamarPgtoServ                 int(11)                DEFAULT "0"         ,
  FormUsaTam                     int(11)                DEFAULT "0"         ,
  FormHeight                     int(11)                DEFAULT "0"         ,
  FormWidth                      int(11)                DEFAULT "0"         ,
  FormPixEsq                     int(11)                DEFAULT "10"        ,
  FormPixDir                     int(11)                DEFAULT "10"        ,
  FormPixTop                     int(11)                DEFAULT "10"        ,
  FormPixBot                     int(11)                DEFAULT "10"        ,
  FormFoAlt                      int(11)                DEFAULT "4"         ,
  FormFoPro                      double(15,8)           DEFAULT "1.33333333",
  FormUsaPro                     int(11)                DEFAULT "1"         ,
  FormSlides                     int(11)                DEFAULT "1"         ,
  FormNeg                        int(11)                DEFAULT "1"         ,
  FormIta                        int(11)                DEFAULT "0"         ,
  FormSub                        int(11)                DEFAULT "0"         ,
  FormExt                        int(11)                DEFAULT "1"         ,
  FormFundoTipo                  int(11)                DEFAULT "1"         ,
  FormFundoBMP                   varchar(255)                               ,
  ServInterv                     int(11)                DEFAULT "1"         ,
  ServAntecip                    int(11)                DEFAULT "20"        ,
  AdiLancto                      int(11)                DEFAULT "0"         ,
  ContaSal                       int(11)                DEFAULT "0"         ,
  ContaVal                       int(11)                DEFAULT "0"         ,
  PronomeE                       varchar(20)            DEFAULT "Senhores Diretores",
  PronomeM                       varchar(20)            DEFAULT "Sr."       ,
  PronomeF                       varchar(20)            DEFAULT "Srta."     ,
  PronomeA                       varchar(20)                                ,
  SaudacaoE                      varchar(50)            DEFAULT "Prezados"  ,
  SaudacaoM                      varchar(50)            DEFAULT "Prezado"   ,
  SaudacaoF                      varchar(50)            DEFAULT "Prezada"   ,
  SaudacaoA                      varchar(50)            DEFAULT "Prezado(a)",
  Niver                          tinyint(2)             DEFAULT "1"         ,
  NiverddA                       tinyint(2)             DEFAULT "5"         ,
  NiverddD                       tinyint(2)             DEFAULT "5"         ,
  LastPassD                      datetime                                   ,
  MultiPass                      int(11)                DEFAULT "0"         ,
  Codigo                         int(11)                DEFAULT "1"         ,
  GraFabCad                      int(11)      NOT NULL  DEFAULT "0"         ,
  GraFabMar                      int(11)      NOT NULL  DEFAULT "0"         ,
  Embalagens                     int(11)                DEFAULT "0"         ,
  FatVenCab                      int(11)      NOT NULL  DEFAULT "0"         ,
  FatVenVol                      int(11)      NOT NULL  DEFAULT "0"         ,
  FatVenIts                      int(11)      NOT NULL  DEFAULT "0"         ,
  FatVenCuz                      int(11)      NOT NULL  DEFAULT "0"         ,
  GrupoBal                       int(11)      NOT NULL  DEFAULT "0"         ,
  GraGru1                        int(11)      NOT NULL  DEFAULT "0"         ,
  SMIMultIns                     int(11)      NOT NULL  DEFAULT "0"         ,
  IDCtrl_NFe                     int(11)      NOT NULL  DEFAULT "0"         ,
  GraGru2                        int(11)      NOT NULL  DEFAULT "0"         ,
  GraGru3                        int(11)      NOT NULL  DEFAULT "0"         ,
  GraGru4                        int(11)      NOT NULL  DEFAULT "0"         ,
  GraGru5                        int(11)      NOT NULL  DEFAULT "0"         ,
  GraGruAtr                      int(11)      NOT NULL  DEFAULT "0"         ,
  GraGruC                        int(11)      NOT NULL  DEFAULT "0"         ,
  GraGruVal                      int(11)      NOT NULL  DEFAULT "0"         ,
  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,
  GraMatIts                      int(11)      NOT NULL  DEFAULT "0"         ,
  GraSrv1                        int(11)      NOT NULL  DEFAULT "0"         ,
  GraTamCad                      int(11)      NOT NULL  DEFAULT "0"         ,
  GraTamIts                      int(11)      NOT NULL  DEFAULT "0"         ,
  EstqCen                        int(11)      NOT NULL  DEFAULT "0"         ,
  ClasFisc                       int(11)      NOT NULL  DEFAULT "0"         ,
  EtqGeraLot                     int(11)      NOT NULL  DEFAULT "0"         ,
  EtqPrinCad                     int(11)      NOT NULL  DEFAULT "0"         ,
  EtqPrinCmd                     int(11)      NOT NULL  DEFAULT "0"         ,
  FatPedCab                      int(11)      NOT NULL  DEFAULT "0"         ,
  FatPedVol                      int(11)      NOT NULL  DEFAULT "0"         ,
  FisAgrCad                      int(11)      NOT NULL  DEFAULT "0"         ,
  FisRegCad                      int(11)      NOT NULL  DEFAULT "0"         ,
  FisRegMvt                      int(11)      NOT NULL  DEFAULT "0"         ,
  Geosite                        int(11)      NOT NULL  DEFAULT "0"         ,
  GraAtrCad                      int(11)      NOT NULL  DEFAULT "0"         ,
  GraAtrIts                      int(11)      NOT NULL  DEFAULT "0"         ,
  GraCorCad                      int(11)      NOT NULL  DEFAULT "0"         ,
  GraCorFam                      int(11)      NOT NULL  DEFAULT "0"         ,
  GraCusPrc                      int(11)      NOT NULL  DEFAULT "0"         ,
  GraCusPrcU                     int(11)      NOT NULL  DEFAULT "0"         ,
  GraFibCad                      int(11)      NOT NULL  DEFAULT "0"         ,
  GraCorPan                      int(11)      NOT NULL  DEFAULT "0"         ,
  GraTecCad                      int(11)      NOT NULL  DEFAULT "0"         ,
  GraTecIts                      int(11)      NOT NULL  DEFAULT "0"         ,
  MatPartCad                     int(11)      NOT NULL  DEFAULT "0"         ,
  MedOrdem                       int(11)      NOT NULL  DEFAULT "0"         ,
  Motivos                        int(11)      NOT NULL  DEFAULT "0"         ,
  PediAccTab                     int(11)      NOT NULL  DEFAULT "0"         ,
  PediVda                        int(11)      NOT NULL  DEFAULT "0"         ,
  PediVdaIts                     int(11)      NOT NULL  DEFAULT "0"         ,
  PediVdaCuz                     int(11)      NOT NULL  DEFAULT "0"         ,
  PediPrzCab                     int(11)      NOT NULL  DEFAULT "0"         ,
  PediPrzIts                     int(11)      NOT NULL  DEFAULT "0"         ,
  PediPrzEmp                     int(11)      NOT NULL  DEFAULT "0"         ,
  PrdGrupAtr                     int(11)      NOT NULL  DEFAULT "0"         ,
  PrdGrupJan                     int(11)      NOT NULL  DEFAULT "0"         ,
  PrdGrupTip                     int(11)      NOT NULL  DEFAULT "0"         ,
  Regioes                        int(11)      NOT NULL  DEFAULT "0"         ,
  RegioesIts                     int(11)      NOT NULL  DEFAULT "0"         ,
  FatConCad                      int(11)      NOT NULL  DEFAULT "0"         ,
  FatConIts                      int(11)      NOT NULL  DEFAULT "0"         ,
  FatConRet                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqManCad                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqManIts                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqBalCad                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqBalIts                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqInnCad                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqCenCad                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqMovItsA                     int(1)       NOT NULL  DEFAULT "0"         ,
  StqMovNFsA                     int(1)       NOT NULL  DEFAULT "0"         ,
  StqMovValA                     int(11)      NOT NULL  DEFAULT "0"         ,
  StqMovUsu                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqMov001                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqMov002                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqMov003                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqMov004                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqMov099                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqMov102                      int(11)      NOT NULL  DEFAULT "0"         ,
  StqMov103                      int(11)      NOT NULL  DEFAULT "0"         ,
  TabePrcCab                     int(11)      NOT NULL  DEFAULT "0"         ,
  TabePrcGrI                     int(11)      NOT NULL  DEFAULT "0"         ,
  TabePrcPzG                     int(11)      NOT NULL  DEFAULT "0"         ,
  UnidMed                        int(11)      NOT NULL  DEFAULT "0"         ,
  GraGruCST                      int(11)      NOT NULL  DEFAULT "0"         ,
  CorrGrad01                     tinyint(1)   NOT NULL  DEFAULT "0"         ,
  CorrGrad02                     tinyint(1)   NOT NULL  DEFAULT "0"         ,
  CMPPVai                        int(11)      NOT NULL  DEFAULT "0"         ,
  CMPPVaiIts                     int(11)      NOT NULL  DEFAULT "0"         ,
  CMPPVem                        int(11)      NOT NULL  DEFAULT "0"         ,
  EmailConta                     int(11)                DEFAULT "0"         ,
  PreEmail                       int(11)                DEFAULT "0"         ,
  PreEmMsg                       int(11)                DEFAULT "0"         ,
  PreEmMsgIm                     int(11)                DEFAULT "0"         ,
  Lk                             int(11)                DEFAULT "0"         ,
  DataCad                        date                                       ,
  DataAlt                        date                                       ,
  UserCad                        int(11)                DEFAULT "0"         ,
  UserAlt                        int(11)                DEFAULT "0"         ,
  AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"         ,
  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,
  UsaEmitGru                     tinyint(1)   NOT NULL  DEFAULT "1"         ,
  CNAB_Cfg                       int(11)      NOT NULL  DEFAULT "0"         ,
  CNAB_Lei                       int(11)      NOT NULL  DEFAULT "0"         ,
  CNAB_Lot                       int(11)      NOT NULL  DEFAULT "0"         ,
  VerImprRecRib                  int(11)      NOT NULL  DEFAULT "0"
) CHARACTER SET latin1 COLLATE latin1_swedish_ci;

CREATE TABLE usersets(
  NomeForm                       varchar(255) NOT NULL  DEFAULT "??"        ,
  Definicao                      varchar(255)                               ,
  Abilitado                      tinyint(1)   NOT NULL  DEFAULT "1"         ,
  Visivel                        tinyint(1)   NOT NULL  DEFAULT "1"         ,
  AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"         ,
  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"
) CHARACTER SET latin1 COLLATE latin1_swedish_ci;

CREATE TABLE impdosits(
  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,
  Tabela                         int(11)      NOT NULL  DEFAULT "0"         ,
  Descricao                      varchar(255) NOT NULL  DEFAULT "???"       ,
  AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"         ,
  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,
  PRIMARY KEY (Codigo)
) CHARACTER SET latin1 COLLATE latin1_swedish_ci;

CREATE TABLE chconfits (
  Campo                          int(11)      NOT NULL  DEFAULT "0"         ,
  Nome                           varchar(50)                                ,
  Descricao                      varchar(255) NOT NULL  DEFAULT "???"       ,
  AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"         ,
  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,
  PRIMARY KEY (Codigo)
) CHARACTER SET latin1 COLLATE latin1_swedish_ci;

*)
end.

