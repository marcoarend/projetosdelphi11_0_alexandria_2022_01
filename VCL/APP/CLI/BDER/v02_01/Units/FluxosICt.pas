unit FluxosICt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmFluxosICt = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    EdAcao1: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label2: TLabel;
    Label4: TLabel;
    EdCodCodi: TdmkEdit;
    EdCodNome: TdmkEdit;
    EdCtrCodi: TdmkEdit;
    EdCtrNome: TdmkEdit;
    EdOrdem: TdmkEdit;
    Label8: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Sb_XYZClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenTop(Conta: Integer);
    procedure ReopenOVcYnsQstMag();
  public
    { Public declarations }
    (*
    FQrTop: TmySQLQuery;
    FDsCad, FDsCtx: TDataSource;
    FContexRef: Integer;
    *)
  end;

  var
  FmFluxosICt: TFmFluxosICt;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, DmkDAC_PF, FluxosCab;

{$R *.DFM}

procedure TFmFluxosICt.BtOKClick(Sender: TObject);
var
  Acao1: String;
  Codigo, Controle, Conta, Ordem: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodCodi.ValueVariant;
  Controle       := EdCtrCodi.ValueVariant;
  Conta          := EdConta.ValueVariant;
  Acao1          := EdAcao1.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  //
  Conta := UMyMod.BPGS1I32('fluxosict', 'Conta', '', '', tsPos, SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'fluxosict', False, [
  'Codigo', 'Controle', 'Acao1',
  'Ordem'], [
  'Conta'], [
  Codigo, Controle, Acao1,
  Ordem], [
  Conta], True) then
  begin
    FmFluxosCab.ReopenFluxosIct(Conta);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdConta.ValueVariant      := 0;
      EdOrdem.ValueVariant      := EdOrdem.ValueVariant + 1;
      EdAcao1.ValueVariant      := '';
      //
      EdAcao1.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmFluxosICt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFluxosICt.FormActivate(Sender: TObject);
begin
  //
  MyObjects.CorIniComponente();
end;

procedure TFmFluxosICt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenOVcYnsQstMag();
end;

procedure TFmFluxosICt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFluxosICt.ReopenTop(Conta: Integer);
begin
(*
  if FQrTop <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrTop, FQrTop.Database);
    //
    if Conta <> 0 then
      FQrTop.Locate('Conta', Conta, []);
  end;
*)
end;

procedure TFmFluxosICt.ReopenOVcYnsQstMag();
begin
 // UnDMkDAC_PF.AbreQuery(QrOVcYnsQstMag, DMod.MyDB);
end;

procedure TFmFluxosICt.Sb_XYZClick(Sender: TObject);
begin
(*
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsQstTop();
  UMyMod.SetaCodigoPesquisado(EdTopico, CBTopico, QrOVcYnsQstTop, VAR_CADASTRO);
*)
end;

end.
