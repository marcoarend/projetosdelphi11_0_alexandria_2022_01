object FmEmitSbtQtd: TFmEmitSbtQtd
  Left = 339
  Top = 185
  Caption = 'EMI-GRUPO-002 :: Adi'#231#227'o de Substratos de Rendimento'
  ClientHeight = 410
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 760
      Top = 0
      Width = 52
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 52
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 712
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 452
        Height = 32
        Caption = 'Adi'#231#227'o de Substratos de Rendimento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 452
        Height = 32
        Caption = 'Adi'#231#227'o de Substratos de Rendimento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 452
        Height = 32
        Caption = 'Adi'#231#227'o de Substratos de Rendimento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 296
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 340
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 52
    Width = 812
    Height = 244
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 233
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 812
        Height = 45
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label2: TLabel
          Left = 92
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Controle:'
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdControle: TdmkEdit
          Left = 92
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 45
        Width = 812
        Height = 188
        Align = alClient
        TabOrder = 1
        object Label3: TLabel
          Left = 12
          Top = 8
          Width = 118
          Height = 13
          Caption = 'Substrato de rendimento:'
        end
        object Label4: TLabel
          Left = 304
          Top = 8
          Width = 72
          Height = 13
          Caption = 'Couros inteiros:'
        end
        object SbEmitSbtCad: TSpeedButton
          Left = 276
          Top = 24
          Width = 23
          Height = 22
          Caption = '...'
          OnClick = SbEmitSbtCadClick
        end
        object LaAreaM2: TLabel
          Left = 400
          Top = 8
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object LaAreaP2: TLabel
          Left = 496
          Top = 8
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
        end
        object LaPeso: TLabel
          Left = 600
          Top = 8
          Width = 42
          Height = 13
          Caption = 'Peso kg:'
        end
        object Label5: TLabel
          Left = 404
          Top = 144
          Width = 83
          Height = 13
          Caption = 'M'#233'dia linhas WB:'
          Enabled = False
        end
        object Label6: TLabel
          Left = 108
          Top = 144
          Width = 78
          Height = 13
          Caption = 'M'#233'dia m'#178'/couro:'
          Enabled = False
        end
        object Label7: TLabel
          Left = 204
          Top = 144
          Width = 76
          Height = 13
          Caption = 'M'#233'dia ft'#178'/couro:'
          Enabled = False
        end
        object Label8: TLabel
          Left = 308
          Top = 144
          Width = 79
          Height = 13
          Caption = 'M'#233'dia kg/couro:'
          Enabled = False
        end
        object Label9: TLabel
          Left = 12
          Top = 144
          Width = 70
          Height = 13
          Caption = 'Fator umidade:'
          Enabled = False
        end
        object Label10: TLabel
          Left = 696
          Top = 8
          Width = 76
          Height = 13
          Caption = 'Linhas Rebaixe:'
        end
        object Label11: TLabel
          Left = 596
          Top = 144
          Width = 105
          Height = 13
          Caption = '% M'#237'nimo Rendim:'
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label12: TLabel
          Left = 500
          Top = 144
          Width = 88
          Height = 13
          Caption = 'M'#233'dia linhas Reb.:'
          Enabled = False
        end
        object EdEmitSbtCad: TdmkEditCB
          Left = 12
          Top = 24
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmitSbtCad
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmitSbtCad: TdmkDBLookupComboBox
          Left = 68
          Top = 24
          Width = 205
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsEmitSbtCad
          TabOrder = 1
          dmkEditCB = EdEmitSbtCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCouIntros: TdmkEdit
          Left = 304
          Top = 24
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = CalculaEspessuraMediaLinhas
        end
        object RGVSBastidao: TdmkRadioGroup
          Left = 11
          Top = 47
          Width = 794
          Height = 45
          Caption = ' Bastid'#227'o: '
          Columns = 8
          ItemIndex = 0
          Items.Strings = (
            'N/D'
            'Integral'
            'Laminado'
            'Dividido tripa'
            'Dividido curtido'
            'Rebaixado '
            'Dividido semi'
            'Rebaix. em semi')
          TabOrder = 8
          QryCampo = 'Grandeza'
          UpdCampo = 'Grandeza'
          UpdType = utYes
          OldValor = 0
        end
        object RGVSUmidade: TdmkRadioGroup
          Left = 11
          Top = 95
          Width = 794
          Height = 45
          Caption = ' Umidade: '
          Columns = 7
          ItemIndex = 0
          Items.Strings = (
            'N/D'
            #211'tima'
            'Muito boa'
            'Boa'
            'Baixa'
            'Muito baixa'
            'P'#233'ssima')
          TabOrder = 9
          OnClick = CalculaEspessuraMediaLinhas
          QryCampo = 'Grandeza'
          UpdCampo = 'Grandeza'
          UpdType = utYes
          OldValor = 0
        end
        object EdAreaM2: TdmkEditCalc
          Left = 400
          Top = 24
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = CalculaEspessuraMediaLinhas
          dmkEditCalcA = EdAreaP2
          CalcType = ctM2toP2
          CalcFrac = cfQuarto
        end
        object EdAreaP2: TdmkEditCalc
          Left = 496
          Top = 24
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = CalculaEspessuraMediaLinhas
          dmkEditCalcA = EdAreaM2
          CalcType = ctP2toM2
          CalcFrac = cfCento
        end
        object EdPesoKg: TdmkEdit
          Left = 600
          Top = 24
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = CalculaEspessuraMediaLinhas
        end
        object EdEspMedLin: TdmkEdit
          Left = 404
          Top = 160
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 14
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdMediaKg: TdmkEdit
          Left = 308
          Top = 160
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdMediaM2: TdmkEdit
          Left = 108
          Top = 160
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 11
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdMediaP2: TdmkEdit
          Left = 204
          Top = 160
          Width = 100
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 12
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdFatorUmidade: TdmkEdit
          Left = 12
          Top = 160
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdLinRebCul: TdmkEdit
          Left = 696
          Top = 24
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = CalculaEspessuraMediaLinhas
        end
        object EdLinRebCab: TdmkEdit
          Left = 748
          Top = 24
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = CalculaEspessuraMediaLinhas
        end
        object EdPercEsperRend: TdmkEdit
          Left = 596
          Top = 160
          Width = 105
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 15
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdLinRebMed: TdmkEdit
          Left = 500
          Top = 160
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 16
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrEmitSbtCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM stqalocits')
    Left = 616
    object QrEmitSbtCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitSbtCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEmitSbtCad: TDataSource
    DataSet = QrEmitSbtCad
    Left = 616
    Top = 44
  end
end
