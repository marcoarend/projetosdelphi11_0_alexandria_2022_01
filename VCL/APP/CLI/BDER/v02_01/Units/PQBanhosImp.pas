unit PQBanhosImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, dmkEditDateTimePicker, frxClass, frxDBSet;

type
  TFmPQBanhosImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    QrSE: TMySQLQuery;
    QrSECodigo: TIntegerField;
    QrSENome: TWideStringField;
    DsSE: TDataSource;
    Label4: TLabel;
    EdSetor: TdmkEditCB;
    CBSetor: TdmkDBLookupComboBox;
    DsPesq: TDataSource;
    QrFormulas: TMySQLQuery;
    QrFormulasNumero: TIntegerField;
    QrFormulasNome: TWideStringField;
    Label16: TLabel;
    EdReceita: TdmkEditCB;
    CBReceita: TdmkDBLookupComboBox;
    Label5: TLabel;
    TPDataI: TdmkEditDateTimePicker;
    TPDataF: TdmkEditDateTimePicker;
    Label6: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    QrCI: TMySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    DsCI: TDataSource;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    QrPQ: TMySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsPQ: TDataSource;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    Panel5: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    RGAgrupa: TRadioGroup;
    frxQUI_RECEI_033_1: TfrxReport;
    QrPesq: TMySQLQuery;
    frxDsPesq: TfrxDBDataset;
    QrPesqNO_Formula: TWideStringField;
    QrPesqNO_SE: TWideStringField;
    QrPesqNO_PQ: TWideStringField;
    QrPesqNO_CI: TWideStringField;
    QrPesqCodigo: TIntegerField;
    QrPesqDataEmis: TDateTimeField;
    QrPesqNumero: TIntegerField;
    QrPesqClienteI: TIntegerField;
    QrPesqSetor: TSmallintField;
    QrPesqPeso: TFloatField;
    QrPesqDtaBaixa: TDateField;
    QrPesqEmpresa: TIntegerField;
    QrPesqPorcent: TFloatField;
    QrPesqPesoPQ: TFloatField;
    QrPesqDtaBaixa_TXT: TWideStringField;
    DsFormulas: TDataSource;
    RGRelatorio: TRadioGroup;
    QrPesqPesoPecas: TFloatField;
    QrPesqQtde: TFloatField;
    QrPesqLporKg: TFloatField;
    QrPesqLporPc: TFloatField;
    QrPesqPesagem: TWideStringField;
    QrPesqM3Banho: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxQUI_RECEI_033_1GetValue(const VarName: string;
      var Value: Variant);
    procedure QrPesqBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
    function EstaAgrupando(OrdemIndex: Integer): Boolean;
  public
    { Public declarations }
  end;

  var
  FmPQBanhosImp: TFmPQBanhosImp;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UnDmkProcFunc;

{$R *.DFM}

procedure TFmPQBanhosImp.BtOKClick(Sender: TObject);
const
  ArrOrdBy: array[0..4] of String = ('NO_PQ', 'NO_SE', 'NO_Formula', 'emi.DtaBaixa', 'NO_CI');
  ArrGruBy: array[0..4] of String = ('NO_PQ', 'NO_SE', 'NO_Formula', 'DtaBaixa_TXT', 'NO_CI');
  //ArrGruBy: array[0..4] of String = ('its.Produto', 'emi.Setor', 'emi.Numero', 'emi.DtaBaixa', 'emi.ClienteI');
var
  Filial, Empresa, CI, SE, PQ, Formula: Integer;
  SQL_Empresa, SQL_SE, SQL_PQ, SQL_Formula, SQL_CI, SQL_Periodo, OrderBy,
  SQL_Somas, GroupBy, Fld_SE, Fld_PQ, Fld_Formula, Fld_CI, Fld_Data,
  Fld_Pesagem: String;
  DataI, DataF: TDateTime;
begin
  QrPesq.DisableControls;
  try
  Filial  := EdEmpresa.ValueVariant;
  PQ      := EdPQ.ValueVariant;
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Informe a empresa') then Exit;
  //if MyObjects.FIC(PQ = 0, EdPQ, 'Informe o banho') then Exit;
  Empresa := DModG.ObtemEntidadeDeFilial(Filial);
  CI      := EdCI.ValueVariant;
  SE      := EdSetor.ValueVariant;
  Formula := EdReceita.ValueVariant;
  DataI   := Trunc(TPDataI.Date);
  DataF   := Trunc(TPDataF.Date);
  //
  SQL_Periodo := dmkPF.SQL_Periodo('AND emi.DtaBaixa', DataI, DataF, True, True);
  SQL_Empresa := EmptyStr;
  SQL_CI      := EmptyStr;
  SQL_SE      := EmptyStr;
  SQL_PQ      := EmptyStr;
  SQL_Formula := EmptyStr;
  //
  if Empresa <> 0 then
    SQL_Empresa := 'AND emi.Empresa=' + Geral.FF0(Empresa);
  if CI <> 0 then
    SQL_CI := 'AND emi.ClienteI=' + Geral.FF0(CI);
  if SE <> 0 then
    SQL_SE := 'AND emi.Setor=' + Geral.FF0(SE);
  if PQ <> 0 then
    SQL_PQ := 'AND its.Produto=' + Geral.FF0(PQ);
  if Formula <> 0 then
    SQL_Formula := 'AND emi.Numero=' + Geral.FF0(Formula);
  //
  Fld_SE      := 'se.Nome';
  Fld_PQ      := 'pq.Nome';
  Fld_Formula := 'frm.Nome';
  Fld_Data    := 'DATE_FORMAT(DtaBaixa, "%d/%m/%y")';
  Fld_CI      := 'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)';
  Fld_Pesagem := 'CONCAT(emi.Codigo) Pesagem, ';

  case RGRelatorio.ItemIndex of
    0:
    begin
      Geral.MB_Aviso('Defina o relat�rio');
      Exit;
    end;
    1:
    begin
      SQL_Somas   := Geral.ATS([
      'SUM(its.Porcent) Porcent, SUM(its.Peso_PQ) PesoPQ, ',
      'SUM(its.Peso_PQ) / 1000 M3Banho, ',
      'emi.Peso PesoPecas, emi.Qtde,  SUM(its.Peso_PQ) / emi.Peso LporKg, ',
      'SUM(its.Peso_PQ) / emi.Qtde LporPc, '
      ]);
      GroupBy     := 'GROUP BY emi.Codigo';
      (* Acima
      Fld_SE      := 'se.Nome';
      Fld_PQ      := 'pq.Nome';
      Fld_Formula := 'frm.Nome';
      FldData     := 'DATE_FORMAT(DtaBaixa, "%d/%m/%Y")';
      Fld_CI      := 'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)';
      *)
    end;
    2:
    begin
      Fld_Pesagem := '"*Qualquer*" Pesagem, ';
      if PQ = 0 then
      begin
        if not EstaAgrupando(0) then
          Fld_PQ      := '"*Qualquer*"';
      end;
      if not EstaAgrupando(1) then
        Fld_SE      := '"*Todos*"';
      if not EstaAgrupando(2) then
        Fld_Formula := '"*Todas*"';
      if not EstaAgrupando(3) then
        Fld_Data    := '"*Per�odo"';
      if not EstaAgrupando(4) then
        Fld_CI      := '"*Todos*"';
      SQL_Somas   := Geral.ATS([
      'SUM(its.Porcent) Porcent, SUM(its.Peso_PQ) PesoPQ, ',
      'SUM(its.Peso_PQ) / 1000 M3Banho, ',
      'SUM(emi.Peso) PesoPecas, SUM(emi.Qtde) Qtde, ',
      'SUM(its.Peso_PQ) / emi.Peso LporKg, ',
      'SUM(its.Peso_PQ) / emi.Qtde LporPc, '
      ]);
      case RGAgrupa.ItemIndex of
        0: GroupBy := 'GROUP BY ' +
             ArrGruBy[RGOrdem1.ItemIndex];
        1: GroupBy := 'GROUP BY '+
             ArrGruBy[RGOrdem1.ItemIndex] + ', ' +
             ArrGruBy[RGOrdem2.ItemIndex];
        2: GroupBy := 'GROUP BY '+
             ArrGruBy[RGOrdem1.ItemIndex] + ', ' +
             ArrGruBy[RGOrdem2.ItemIndex] + ', ' +
             ArrGruBy[RGOrdem3.ItemIndex];
        3: GroupBy := 'GROUP BY '+
             ArrGruBy[RGOrdem1.ItemIndex] + ', ' +
             ArrGruBy[RGOrdem2.ItemIndex] + ', ' +
             ArrGruBy[RGOrdem3.ItemIndex] + ', ' +
             ArrGruBy[RGOrdem4.ItemIndex];
      end;
    end else
    begin
      Geral.MB_Aviso('Relat�rio n�o implementado para "' +
      RGRelatorio.Items[RGRelatorio.ItemIndex] + '"');
      Exit;
    end;
  end;
  //
  OrderBy := 'ORDER BY '+
    ArrOrdBy[RGOrdem1.ItemIndex] + ', ' +
    ArrOrdBy[RGOrdem2.ItemIndex] + ', ' +
    ArrOrdBy[RGOrdem3.ItemIndex] + ', ' +
    ArrOrdBy[RGOrdem4.ItemIndex];
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
  'SELECT ' + Fld_Formula + ' NO_Formula, ' + Fld_SE + ' NO_SE, ' + Fld_PQ + ' NO_PQ, ',
  Fld_CI + ' NO_CI, ',
  'emi.Codigo, emi.DataEmis, emi.Numero, ',
  'emi.ClienteI, emi.Setor, emi.Peso,  ',
  'emi.DtaBaixa, emi.Empresa, ',
  Fld_Pesagem,
  SQL_Somas,
  Fld_Data + ' DtaBaixa_TXT ',
  'FROM emitits its ',
  'LEFT JOIN emit emi ON emi.Codigo=its.Codigo ',
  'LEFT JOIN entidades ent ON ent.Codigo=emi.ClienteI ',
  'LEFT JOIN pq pq ON pq.Codigo=its.Produto ',
  'LEFT JOIN listasetores se ON se.Codigo=emi.Setor ',
  'LEFT JOIN formulas frm ON frm.Numero=emi.Numero ',
  'WHERE (its.Produto < 0 AND its.Produto > -11) ',
  SQL_Periodo,
  SQL_Empresa,
  SQL_CI,
  SQL_SE,
  SQL_PQ,
  SQL_Formula,
  GroupBy,
  OrderBy,
  '']);
  //Geral.MB_Teste(QrPesq.SQL.Text);
  //
  MyObjects.frxDefineDataSets(frxQUI_RECEI_033_1, [
    DModG.frxDsDono,
    frxDsPesq
  ]);
  MyObjects.frxMostra(frxQUI_RECEI_033_1, 'Relat�rio de Uso de Banhos');
  finally
    QrPesq.EnableControls;
  end;
end;

procedure TFmPQBanhosImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmPQBanhosImp.EstaAgrupando(OrdemIndex: Integer): Boolean;
begin
//errado!
  case RGAgrupa.ItemIndex of
    0: Result := False;
    1: Result := OrdemIndex = RGOrdem1.ItemIndex;
    2: Result := OrdemIndex in ([RGOrdem1.ItemIndex, RGOrdem2.ItemIndex]);
    3: Result := OrdemIndex in ([RGOrdem1.ItemIndex, RGOrdem2.ItemIndex, RGOrdem3.ItemIndex]);
    4: Result := OrdemIndex in ([RGOrdem1.ItemIndex, RGOrdem2.ItemIndex, RGOrdem3.ItemIndex, RGOrdem4.ItemIndex]);
    else Result := True;
  end;
end;

procedure TFmPQBanhosImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQBanhosImp.FormCreate(Sender: TObject);
var
  Data: TDateTime;
begin
  ImgTipo.SQLType := stPsq;
  //
  Data := DModG.ObtemAgora();
  TPDataI.Date := Data - 30;
  TPDataF.Date := Data;
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  UnDMkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrSE, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrFormulas, Dmod.MyDB);
  //
end;

procedure TFmPQBanhosImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQBanhosImp.frxQUI_RECEI_033_1GetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_NO_EMPRESA') = 0 then
    Value := CBEmpresa.Text
  else
  if AnsiCompareText(VarName, 'VFR_AGR1NOME') = 0 then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := QrPesqNO_PQ.Value;
      1: Value := QrPesqNO_SE.Value;
      2: Value := QrPesqNO_Formula.Value;
      3: Value := QrPesqDtaBaixa_TXT.Value;
      4: Value := QrPesqNO_CI.Value;
      else Value := '';
    end;
  end else if AnsiCompareText(VarName, 'VFR_AGR2NOME') = 0 then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := QrPesqNO_PQ.Value;
      1: Value := QrPesqNO_SE.Value;
      2: Value := QrPesqNO_Formula.Value;
      3: Value := QrPesqDtaBaixa_TXT.Value;
      4: Value := QrPesqNO_CI.Value;
      else Value := '';
    end;
  end
  else if AnsiCompareText(VarName, 'VAR_DATA_HORA') = 0 then
    Value := Geral.Maiusculas(Geral.FDT(Now(), 8), False)
  else if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then
    Value := dmkPF.PeriodoImp(TPDataI.Date, TPDataF.Date, 0, 0, True, True,
    False, False, '', '')
  else if AnsiCompareText(VarName, 'VARF_BANHO') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBPQ.Text, EdPQ.ValueVariant)
  else if AnsiCompareText(VarName, 'VARF_SETOR') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBSetor.Text, EdSetor.ValueVariant)
  else if AnsiCompareText(VarName, 'VARF_FORMULA') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBReceita.Text, EdReceita.ValueVariant)
  else if AnsiCompareText(VarName, 'VARF_CLIINT') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBCI.Text, EdCI.ValueVariant)


  else if AnsiCompareText(VarName, 'VFR_AGR')  = 0 then
  begin
    if RGRelatorio.ItemIndex = 2 then
      Value := RGAgrupa.ItemIndex -1
    else
      Value := RGAgrupa.ItemIndex;
  end
  else if AnsiCompareText(VarName, 'VFR_ORD1') = 0 then Value := RGOrdem1.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD2') = 0 then Value := RGOrdem2.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD3') = 0 then Value := RGOrdem3.ItemIndex

  else if AnsiCompareText(VarName, 'VFR_EMPRESA') = 0 then Value := CBEmpresa.Text
end;

procedure TFmPQBanhosImp.QrPesqBeforeOpen(DataSet: TDataSet);
begin
  // Corrigir tamanhos de campos se na primeira execu��o de pesquisa
  // o agrupamento for menor que o m�ximo. (campos *xxxxxx*)

  QrPesqNO_Formula.Size := 30;
  QrPesqNO_Formula.DisplayWidth := 30;

  QrPesqNO_SE.Size := 20;
  QrPesqNO_SE.DisplayWidth := 20;

  QrPesqNO_PQ.Size := 50;
  QrPesqNO_PQ.DisplayWidth := 50;

  QrPesqNO_SE.Size := 100;
  QrPesqNO_SE.DisplayWidth := 100;

  QrPesqDtaBaixa_TXT.Size := 10;
  QrPesqDtaBaixa_TXT.DisplayWidth := 10;
end;

end.
