object FmFormulasImpOSs: TFmFormulasImpOSs
  Left = 339
  Top = 185
  Caption = 'QUI-RECEI-106 :: Adi'#231#227'o de OSs em Baixa Por Receita'
  ClientHeight = 486
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 528
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 451
        Height = 32
        Caption = 'Adi'#231#227'o de OSs em Baixa Por Receita'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 451
        Height = 32
        Caption = 'Adi'#231#227'o de OSs em Baixa Por Receita'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 451
        Height = 32
        Caption = 'Adi'#231#227'o de OSs em Baixa Por Receita'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 624
    Height = 324
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 324
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 624
        Height = 324
        Align = alClient
        TabOrder = 0
        object DBGMPVIts: TDBGrid
          Left = 2
          Top = 63
          Width = 620
          Height = 209
          Align = alClient
          DataSource = DsMPVIts
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGMPVItsDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'OS'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'M2Pedido'
              Title.Caption = 'm'#178' pedido'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_OS'
              Title.Caption = 'Artigo >>> cliente'
              Width = 455
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 620
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label1: TLabel
            Left = 8
            Top = 6
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object Label7: TLabel
            Left = 8
            Top = 28
            Width = 18
            Height = 13
            Caption = 'OS:'
          end
          object Label8: TLabel
            Left = 132
            Top = 28
            Width = 218
            Height = 13
            Caption = 'Ao definir a OS o cliente ser'#225' desconsiderado.'
          end
          object RGFiltroOSs: TRadioGroup
            Left = 525
            Top = 0
            Width = 95
            Height = 48
            Align = alRight
            Caption = 'Filtro de OSs: '
            ItemIndex = 0
            Items.Strings = (
              'OSs abertas'
              'Todas OSs')
            TabOrder = 0
            OnClick = RGFiltroOSsClick
          end
          object EdCliente: TdmkEditCB
            Left = 44
            Top = 2
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdClienteChange
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 100
            Top = 2
            Width = 293
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NO_ENT'
            ListSource = DsClientes
            TabOrder = 2
            dmkEditCB = EdCliente
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object BitBtn1: TBitBtn
            Tag = 20
            Left = 400
            Top = 4
            Width = 120
            Height = 40
            Caption = '&Reabre'
            NumGlyphs = 2
            TabOrder = 3
            OnClick = BitBtn1Click
          end
          object EdUniOS: TdmkEdit
            Left = 44
            Top = 24
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 272
          Width = 620
          Height = 50
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 93
            Height = 50
            Align = alLeft
            BevelOuter = bvNone
            Caption = 'Panel7'
            Enabled = False
            TabOrder = 0
            object Label2: TLabel
              Left = 8
              Top = 4
              Width = 78
              Height = 13
              Caption = 'OS selecionada:'
            end
            object EdMPVIts: TdmkEdit
              Left = 8
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = EdMPVItsChange
            end
          end
          object PnMedidas: TPanel
            Left = 93
            Top = 0
            Width = 527
            Height = 50
            Align = alClient
            BevelOuter = bvNone
            Caption = 'PnMedidas'
            TabOrder = 1
            Visible = False
            object Label3: TLabel
              Left = 4
              Top = 4
              Width = 33
              Height = 13
              Caption = 'Pe'#231'as:'
            end
            object Label4: TLabel
              Left = 88
              Top = 4
              Width = 59
              Height = 13
              Caption = 'Peso em kg:'
            end
            object Label5: TLabel
              Left = 172
              Top = 4
              Width = 56
              Height = 13
              Caption = #193'rea em m'#178':'
            end
            object Label6: TLabel
              Left = 256
              Top = 4
              Width = 54
              Height = 13
              Caption = #193'rea em ft'#178':'
            end
            object EdPecas: TdmkEdit
              Left = 4
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 1
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object EdPeso: TdmkEdit
              Left = 88
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object EdAreaM2: TdmkEditCalc
              Left = 172
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              dmkEditCalcA = EdAreaP2
              CalcType = ctM2toP2
              CalcFrac = cfQuarto
            end
            object EdAreaP2: TdmkEditCalc
              Left = 256
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              dmkEditCalcA = EdAreaM2
              CalcType = ctP2toM2
              CalcFrac = cfCento
            end
            object BtOK: TBitBtn
              Tag = 14
              Left = 396
              Top = 8
              Width = 120
              Height = 40
              Caption = '&OK'
              NumGlyphs = 2
              TabOrder = 4
              OnClick = BtOKClick
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 372
    Width = 624
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 413
        Height = 16
        Caption = 
          'D'#234' um duplo clique na grade para selecionar a OS e informar os d' +
          'ados.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 413
        Height = 16
        Caption = 
          'D'#234' um duplo clique na grade para selecionar a OS e informar os d' +
          'ados.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 416
    Width = 624
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 478
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 476
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object QrMPVIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mpi.Controle, M2Pedido,'
      'CONCAT(mpi.Texto, " ", mpi.CorTxt, " >>> ", '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NOME_OS'
      'FROM mpvits mpi'
      'LEFT JOIN mpp mpp ON mpp.Codigo=mpi.Pedido'
      'LEFT JOIN entidades ent ON ent.Codigo=mpp.Cliente'
      'WHERE mpi.Codigo=0'
      'AND mpi.Pedido>0'
      '/*'
      'AND mpp.Cliente=:P0'
      '*/'
      'ORDER BY mpi.Controle')
    Left = 20
    Top = 156
    object QrMPVItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMPVItsM2Pedido: TFloatField
      FieldName = 'M2Pedido'
    end
    object QrMPVItsNOME_OS: TWideStringField
      FieldName = 'NOME_OS'
      Size = 186
    end
  end
  object DsMPVIts: TDataSource
    DataSet = QrMPVIts
    Left = 20
    Top = 204
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT'
      'FROM entidades ent'
      'ORDER BY NO_ENT')
    Left = 88
    Top = 156
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 88
    Top = 204
  end
end
