unit WBRclCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, frxCross,
  UnAppEnums;

type
  TFmWBRclCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrWBRclCab: TmySQLQuery;
    DsWBRclCab: TDataSource;
    QrWBRclIts: TmySQLQuery;
    DsWBRclIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtHrIni: TdmkEditDateTimePicker;
    EdDtHrIni: TdmkEdit;
    Label53: TLabel;
    TPDtHrFim: TdmkEditDateTimePicker;
    EdDtHrFim: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrWBRclCabCodigo: TIntegerField;
    QrWBRclCabMovimCod: TIntegerField;
    QrWBRclCabEmpresa: TIntegerField;
    QrWBRclCabDtHrIni: TDateTimeField;
    QrWBRclCabDtHrFim: TDateTimeField;
    QrWBRclCabPecas: TFloatField;
    QrWBRclCabPesoKg: TFloatField;
    QrWBRclCabAreaM2: TFloatField;
    QrWBRclCabAreaP2: TFloatField;
    QrWBRclCabLk: TIntegerField;
    QrWBRclCabDataCad: TDateField;
    QrWBRclCabDataAlt: TDateField;
    QrWBRclCabUserCad: TIntegerField;
    QrWBRclCabUserAlt: TIntegerField;
    QrWBRclCabAlterWeb: TSmallintField;
    QrWBRclCabAtivo: TSmallintField;
    QrWBRclCabNO_EMPRESA: TWideStringField;
    QrWBRclItsCodigo: TIntegerField;
    QrWBRclItsControle: TIntegerField;
    QrWBRclItsMovimCod: TIntegerField;
    QrWBRclItsEmpresa: TIntegerField;
    QrWBRclItsMovimID: TIntegerField;
    QrWBRclItsGraGruX: TIntegerField;
    QrWBRclItsPecas: TFloatField;
    QrWBRclItsPesoKg: TFloatField;
    QrWBRclItsAreaM2: TFloatField;
    QrWBRclItsAreaP2: TFloatField;
    QrWBRclItsLk: TIntegerField;
    QrWBRclItsDataCad: TDateField;
    QrWBRclItsDataAlt: TDateField;
    QrWBRclItsUserCad: TIntegerField;
    QrWBRclItsUserAlt: TIntegerField;
    QrWBRclItsAlterWeb: TSmallintField;
    QrWBRclItsAtivo: TSmallintField;
    QrWBRclItsNO_PRD_TAM_COR: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TDBGrid;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    Splitter1: TSplitter;
    QrReclasif: TmySQLQuery;
    DsReclasif: TDataSource;
    QrReclasifCodigo: TIntegerField;
    QrReclasifControle: TIntegerField;
    QrReclasifMovimCod: TIntegerField;
    QrReclasifEmpresa: TIntegerField;
    QrReclasifMovimID: TIntegerField;
    QrReclasifGraGruX: TIntegerField;
    QrReclasifPecas: TFloatField;
    QrReclasifPesoKg: TFloatField;
    QrReclasifAreaM2: TFloatField;
    QrReclasifAreaP2: TFloatField;
    QrReclasifLk: TIntegerField;
    QrReclasifDataCad: TDateField;
    QrReclasifDataAlt: TDateField;
    QrReclasifUserCad: TIntegerField;
    QrReclasifUserAlt: TIntegerField;
    QrReclasifAlterWeb: TSmallintField;
    QrReclasifAtivo: TSmallintField;
    QrReclasifNO_PRD_TAM_COR: TWideStringField;
    QrWBRclItsSrcMovID: TIntegerField;
    QrWBRclItsSrcNivel1: TIntegerField;
    QrWBRclItsSrcNivel2: TIntegerField;
    QrSumWII: TmySQLQuery;
    QrSumWIIPecas: TFloatField;
    QrSumWIISrcNivel2: TIntegerField;
    QrWBRclItsPC_RECLAS: TFloatField;
    QrWBRclItsPallet: TIntegerField;
    QrWBRclItsNO_PALLET: TWideStringField;
    QrReclasifPallet: TIntegerField;
    QrReclasifNO_Pallet: TWideStringField;
    QrWBRclItsSdoVrtArM2: TFloatField;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    Classificao1: TMenuItem;
    QrResulClas: TmySQLQuery;
    QrResulClasCodigo: TIntegerField;
    QrResulClasControle: TIntegerField;
    QrResulClasMovimCod: TIntegerField;
    QrResulClasEmpresa: TIntegerField;
    QrResulClasMovimID: TIntegerField;
    QrResulClasGraGruX: TIntegerField;
    QrResulClasPecas: TFloatField;
    QrResulClasPesoKg: TFloatField;
    QrResulClasAreaM2: TFloatField;
    QrResulClasAreaP2: TFloatField;
    QrResulClasLk: TIntegerField;
    QrResulClasDataCad: TDateField;
    QrResulClasDataAlt: TDateField;
    QrResulClasUserCad: TIntegerField;
    QrResulClasUserAlt: TIntegerField;
    QrResulClasAlterWeb: TSmallintField;
    QrResulClasAtivo: TSmallintField;
    QrResulClasNO_PRD_TAM_COR: TWideStringField;
    QrResulClasPallet: TIntegerField;
    QrResulClasNO_Pallet: TWideStringField;
    frxWET_RECUR_001_01: TfrxReport;
    frxDsResulClas: TfrxDBDataset;
    QrResulClasSrcNivel2: TIntegerField;
    QrResulClasGGX_REFER: TWideStringField;
    QrResulClasNO_ORI_PALLET: TWideStringField;
    QrResulClasPALLET_M2: TFloatField;
    frxDsResulTota: TfrxDBDataset;
    QrResulTota: TmySQLQuery;
    QrResulTotaNO_Pallet: TWideStringField;
    QrResulTotaPecas: TFloatField;
    QrResulTotaGGX_REFER: TWideStringField;
    QrResulTotaGraGruX: TIntegerField;
    QrPalletInn: TmySQLQuery;
    QrResulTotaPERCENT_CLASS: TFloatField;
    QrResulClasPERCENT_CLASS: TFloatField;
    QrResulTotaCalcM2: TFloatField;
    QrWBInnSum: TmySQLQuery;
    QrWBInnSumAreaM2: TFloatField;
    QrWBInnSumPecas: TFloatField;
    frxDsWBRclIts: TfrxDBDataset;
    frxDsWBRclCab: TfrxDBDataset;
    QrWBRclItsObserv: TWideStringField;
    N2: TMenuItem;
    Fichas1: TMenuItem;
    Entradascomestoquevirtual1: TMenuItem;
    Palletsdosclassificados1: TMenuItem;
    frxWET_RECUR_001_02: TfrxReport;
    QrPalletInnCodigo: TIntegerField;
    QrPalletInnControle: TIntegerField;
    QrPalletInnMovimCod: TIntegerField;
    QrPalletInnMovimNiv: TIntegerField;
    QrPalletInnEmpresa: TIntegerField;
    QrPalletInnTerceiro: TIntegerField;
    QrPalletInnMovimID: TIntegerField;
    QrPalletInnDataHora: TDateTimeField;
    QrPalletInnPallet: TIntegerField;
    QrPalletInnGraGruX: TIntegerField;
    QrPalletInnPecas: TFloatField;
    QrPalletInnPesoKg: TFloatField;
    QrPalletInnAreaM2: TFloatField;
    QrPalletInnAreaP2: TFloatField;
    QrPalletInnSrcMovID: TIntegerField;
    QrPalletInnSrcNivel1: TIntegerField;
    QrPalletInnSrcNivel2: TIntegerField;
    QrPalletInnLk: TIntegerField;
    QrPalletInnDataCad: TDateField;
    QrPalletInnDataAlt: TDateField;
    QrPalletInnUserCad: TIntegerField;
    QrPalletInnUserAlt: TIntegerField;
    QrPalletInnAlterWeb: TSmallintField;
    QrPalletInnAtivo: TSmallintField;
    QrPalletInnSdoVrtPeca: TFloatField;
    QrPalletInnObserv: TWideStringField;
    QrPalletInnNO_PRD_TAM_COR: TWideStringField;
    QrPalletInnNO_Pallet: TWideStringField;
    QrPalletInnNO_EMPRESA: TWideStringField;
    frxDsPalletInn: TfrxDBDataset;
    QrPalletInnNO_FORNECE: TWideStringField;
    QrPalletCla: TmySQLQuery;
    QrPalletClaPallet: TIntegerField;
    QrPalletClaDataHora: TDateTimeField;
    QrPalletClaPecas: TFloatField;
    QrPalletClaAreaM2: TFloatField;
    QrPalletClaAreaP2: TFloatField;
    QrPalletClaPesoKg: TFloatField;
    QrPalletClaNO_PRD_TAM_COR: TWideStringField;
    QrPalletClaNO_Pallet: TWideStringField;
    QrPalletClaNO_EMPRESA: TWideStringField;
    QrPalletClaNO_FORNECE: TWideStringField;
    frxWET_RECUR_001_03: TfrxReport;
    frxDsPalletCla: TfrxDBDataset;
    QrWBRclCabValorT: TFloatField;
    QrWBRclItsValorT: TFloatField;
    QrReclasifValorT: TFloatField;
    Label9: TLabel;
    DBEdit15: TDBEdit;
    BtReclasif: TBitBtn;
    PMReclasif: TPopupMenu;
    Incluireclassificao1: TMenuItem;
    Alterareclassificao1: TMenuItem;
    Excluireclassificao1: TMenuItem;
    QrReclasifSrcMovID: TIntegerField;
    QrReclasifSrcNivel1: TIntegerField;
    QrReclasifSrcNivel2: TIntegerField;
    QrResulTotaAreaM2: TFloatField;
    QrWBRclItsSdoVrtPeca: TFloatField;
    QrPalletClaTerceiro: TIntegerField;
    QrPalletClaGraGruX: TIntegerField;
    QrWBRclItsMovimTwn: TIntegerField;
    QrReclasifMovimTwn: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWBRclCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWBRclCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrWBRclCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrWBRclCabBeforeClose(DataSet: TDataSet);
    procedure QrWBRclItsBeforeClose(DataSet: TDataSet);
    procedure QrWBRclItsAfterScroll(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure Classificao1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrResulTotaCalcFields(DataSet: TDataSet);
    procedure frxWET_RECUR_001_01GetValue(const VarName: string;
      var Value: Variant);
    procedure Entradascomestoquevirtual1Click(Sender: TObject);
    procedure Palletsdosclassificados1Click(Sender: TObject);
    procedure Incluireclassificao1Click(Sender: TObject);
    procedure PMReclasifPopup(Sender: TObject);
    procedure Alterareclassificao1Click(Sender: TObject);
    procedure Excluireclassificao1Click(Sender: TObject);
    procedure BtReclasifClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormWBInnIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenWBInnIts(Controle: Integer);
    procedure ReopenReclasif(Controle: Integer);

  end;

var
  FmWBRclCab: TFmWBRclCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, WBInnIts, ModuleGeral,
Principal, WBMovImp;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWBRclCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWBRclCab.MostraFormWBInnIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmWBInnIts, FmWBInnIts, afmoNegarComAviso) then
  begin
    FmWBInnIts.ImgTipo.SQLType := SQLType;
    FmWBInnIts.FQrCab := QrWBRclCab;
    FmWBInnIts.FDsCab := DsWBRclCab;
    FmWBInnIts.FQrIts := QrWBRclIts;
    FmWBInnIts.FDataHora := QrWBRclCabDtHrIni.Value;
    if SQLType = stIns then
    begin
      //FmWBInnIts.EdCPF1.ReadOnly := False
    end else
    begin
      FmWBInnIts.EdControle.ValueVariant := QrWBRclItsControle.Value;
      //
      FmWBInnIts.EdGragruX.ValueVariant  := QrWBRclItsGraGruX.Value;
      FmWBInnIts.CBGragruX.KeyValue      := QrWBRclItsGraGruX.Value;
      FmWBInnIts.EdPecas.ValueVariant    := QrWBRclItsPecas.Value;
      FmWBInnIts.EdPesoKg.ValueVariant   := QrWBRclItsPesoKg.Value;
      FmWBInnIts.EdAreaM2.ValueVariant   := QrWBRclItsAreaM2.Value;
      FmWBInnIts.EdAreaP2.ValueVariant   := QrWBRclItsAreaP2.Value;
      //FmWBInnIts.EdValorT.ValueVariant   := QrWBRclItsValorT.Value;
      FmWBInnIts.EdPallet.ValueVariant   := QrWBRclItsPallet.Value;
      FmWBInnIts.CBPallet.KeyValue       := QrWBRclItsPallet.Value;
      FmWBInnIts.EdObserv.ValueVariant   := QrWBRclItsObserv.Value;
      //
    end;
    FmWBInnIts.ShowModal;
    FmWBInnIts.Destroy;
  end;
end;

procedure TFmWBRclCab.Palletsdosclassificados1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Pallets: String;
begin
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT Pallet ',
    'FROM wbmovits wmi ',
    'WHERE wmi.SrcMovID=' + Geral.FF0(QrWBRclItsMovimCod.Value),
    'AND wmi.SrcNivel1=' + Geral.FF0(QrWBRclItsCodigo.Value),
    'AND wmi.SrcNivel2=' + Geral.FF0(QrWBRclItsControle.Value),
    'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
    'ORDER BY wmi.Controle ',
    '']);
    Pallets := '';
    Qry.First;
    while not Qry.Eof do
    begin
      Pallets := Pallets + ', ' +
        Geral.FF0(Qry.FieldByName('Pallet').AsInteger);
      //
      Qry.next;
    end;
    Pallets := Copy(Pallets, 3);
    //
    if Pallets <> '' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPalletCla, Dmod.MyDB, [
      'SELECT wmi.GragruX, wmi.Pallet, MAX(DataHora) DataHora,  ',
      'SUM(wmi.Pecas) Pecas,  ',
      'IF(MIN(AreaM2) > 0.01, SUM(wmi.AreaM2), 0) AreaM2,  ',
      'IF(MIN(AreaP2) > 0.01, SUM(wmi.AreaP2), 0) AreaP2,  ',
      'IF(MIN(PesoKg) > 0.001, SUM(wmi.PesoKg), 0) PesoKg,  ',
      'CONCAT(gg1.Nome,   ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet, wmi.Terceiro,  ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,  ',
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE  ',
      'FROM wbmovits wmi   ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX   ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
      'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet   ',
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa ',
      'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro  ',
      'WHERE wmi.Pallet IN (' + Pallets + ')  ',
      'GROUP BY wmi.Pallet, wmi.GraGruX, wmi.Empresa, wmi.Terceiro  ',
      '']);
      //Geral.MB_Aviso(QrPalletCla.SQL.Text);
      //
      MyObjects.frxDefineDataSets(frxWET_RECUR_001_03, [
        //DModG.frxDsDono,
        frxDsPalletCla
      ]);
      //
      MyObjects.frxMostra(frxWET_RECUR_001_03,
        'Pallets - (Re)Classificados');
    end else
      Geral.MB_Aviso('Nenhum pallet foi encontrado!');
  finally
    Qry.Free;
  end;
end;

procedure TFmWBRclCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrWBRclCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrWBRclCab, QrWBRclIts);
end;

procedure TFmWBRclCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrWBRclCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrWBRclIts);
  MyObjects.HabilitaMenuItemCabDel(ItsExclui1, QrWBRclIts, QrReclasif);
end;

procedure TFmWBRclCab.PMReclasifPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluireclassificao1, QrWBRclCab);
  MyObjects.HabilitaMenuItemItsUpd(Alterareclassificao1, QrReclasif);
  MyObjects.HabilitaMenuItemItsDel(Excluireclassificao1, QrReclasif);
end;

procedure TFmWBRclCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWBRclCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWBRclCab.DefParams;
begin
  VAR_GOTOTABELA := 'wbrclcab';
  VAR_GOTOMYSQLTABLE := QrWBRclCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA');
  VAR_SQLx.Add('FROM wbrclcab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmWBRclCab.Entradascomestoquevirtual1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPalletInn, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa',
  'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrWBRclCabMovimCod.Value),
  'AND SdoVrtPeca>=0.001',
  'ORDER BY wmi.Controle ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxWET_RECUR_001_02, [
    //DModG.frxDsDono,
    frxDsPalletInn
  ]);
  //
  MyObjects.frxMostra(frxWET_RECUR_001_02,
    'Pallets - Entrada com estoque virtual');
end;

procedure TFmWBRclCab.Estoque1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmWBMovImp, FmWBMovImp, afmoNegarComAviso) then
  begin
    FmWBMovImp.ShowModal;
    FmWBMovImp.Destroy;
  end;
end;

procedure TFmWBRclCab.Excluireclassificao1Click(Sender: TObject);
(*
var
  MovimTwn, WBRclCab, WBInnIts, MovimCod, SrcNivel2: Integer;
begin
  MovimTwn := QrWBRclItsMovimTwn.Value;
  if MovimTwn = 0 then
  begin
    Geral.MB_Erro('"MovimTwn" n�o definido!. Altera��o abortada!');
    Exit;
  end;
  WBRclCab    := QrWBRclCabCodigo.Value;
  WBInnIts    := QrWBRclItsControle.Value;
  //
  MovimCod  := QrReclasifMovimCod.Value;
  SrcNivel2 := QrReclasifSrcNivel2.Value;
  //
  if Dmod.ExcluiMovimTwn(QrReclasif, QrReclasifMovimCod,
  QrReclasifMovimCod.Value, QrReclasifControle.Value,
  QrReclasifSrcNivel2.Value) then
  begin
    Dmod.AtualizaTotaisWBXxxCab('wbrclcab', MovimCod);
    //Dmod.AtualizaSaldoVirtualWBMovIts(SrcNivel2);
    LocCod(WBRclCab, WBRclCab);
    QrWBRclIts.Locate('Controle', WBInnIts, []);
  end;
*)
var
  WBRclCab, WBRclIts, MovimCod, SrcNivel2: Integer;
begin
  WBRclCab    := QrWBRclCabCodigo.Value;
  WBRclIts    := QrWBRclItsControle.Value;
  //
  MovimCod  := QrReclasifMovimCod.Value;
  SrcNivel2 := QrReclasifSrcNivel2.Value;
  //
  if Dmod.ExcluiMovimCodWBMovIts(QrReclasif, QrReclasifMovimCod,
  QrReclasifMovimCod.Value, QrReclasifControle.Value,
  QrReclasifSrcNivel2.Value) then
  begin
    Dmod.AtualizaTotaisWBXxxCab('wbrclcab', MovimCod);
    //Dmod.AtualizaSaldoVirtualWBMovIts(SrcNivel2);
    LocCod(WBRclCab, WBRclCab);
    QrWBRclIts.Locate('Controle', WBRclIts, []);
  end;
end;

procedure TFmWBRclCab.Incluireclassificao1Click(Sender: TObject);
const
  MovimTwn = 0;
var
  Filial, MovimCod, Codigo, Controle: Integer;
  ValorT, AreaM2: Double;

begin
  Filial   := QrWBRclCabEmpresa.Value;
  MovimCod := QrWBRclCabMovimCod.Value;
  Codigo   := QrWBRclCabCodigo.Value;
{
  Controle := QrWBRclItsControle.Value;
  ValorT   := QrWBRclItsValorT.Value;
  AreaM2   := QrWBRclItsAreaM2.Value;
}
  Controle := 0;
  ValorT   := 0;
  AreaM2   := 0;
  //
  FmPrincipal.MostraFormWBRclIns(stIns, Filial, MovimCod, MovimTwn, Codigo,
    Controle, ValorT, AreaM2, QrReclasif, 0, wbrEstoque);
  ReopenWBInnIts(QrWBRclItsControle.Value);
end;

procedure TFmWBRclCab.ItsAltera1Click(Sender: TObject);
begin
//  MostraFormWBInnIts(stUpd);
end;

procedure TFmWBRclCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmWBRclCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWBRclCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWBRclCab.ItsExclui1Click(Sender: TObject);
var
  Codigo, MovimCod: Integer;
begin
{
  Codigo   := QrWBRclItsCodigo.Value;
  MovimCod := QrWBRclItsMovimCod.Value;
  //
  if Dmod.ExcluiControleWBMovIts(QrWBRclIts, QrWBRclItsControle,
  QrWBRclItsControle.Value, QrWBRclItsSrcNivel2.Value) then
  begin
    Dmod.AtualizaTotaisWBXxxCab('wbrclcab', MovimCod);
    LocCod(Codigo, Codigo);
  end;
}
end;

procedure TFmWBRclCab.ReopenReclasif(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrReclasif, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrWBRclCabMovimCod.Value),
  'AND wmi.MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
  'AND wmi.MovimTwn=' + Geral.FF0(QrWBRclItsMovimTwn.Value),
  'ORDER BY wmi.Controle ',
  '']);
  //
  QrReclasif.Locate('Controle', Controle, []);
end;

procedure TFmWBRclCab.ReopenWBInnIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBRclIts, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrWBRclCabMovimCod.Value),
  'AND wmi.MovimNiv=' + Geral.FF0(Integer(eminSorcClass)),
  'ORDER BY NO_Pallet, wmi.Controle ',
  '']);
  //
  QrWBRclIts.Locate('Controle', Controle, []);
end;


procedure TFmWBRclCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWBRclCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWBRclCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWBRclCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWBRclCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWBRclCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWBRclCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWBRclCabCodigo.Value;
  Close;
end;

procedure TFmWBRclCab.ItsInclui1Click(Sender: TObject);
begin
//  MostraFormWBInnIts(stIns);
end;

procedure TFmWBRclCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWBRclCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'wbrclcab');
  Empresa := DModG.ObtemFilialDeEntidade(QrWBRclCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmWBRclCab.BtConfirmaClick(Sender: TObject);
var
  DtHrIni, DtHrFim, DataHora: String;
  Codigo, MovimCod, Empresa, Terceiro: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtHrIni       := Geral.FDT_TP_Ed(TPDtHrIni.Date, EdDtHrIni.Text);
  DtHrFim       := Geral.FDT_TP_Ed(TPDtHrFim.Date, EdDtHrFim.Text);
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(TPDtHrIni.DateTime < 2, TPDtHrIni, 'Defina uma data de in�cio!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('wbrclcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('wbmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'wbrclcab', False, [
  'MovimCod', 'Empresa', 'DtHrIni',
  'DtHrFim' (*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT'*)], [
  'Codigo'], [
  MovimCod, Empresa, DtHrIni,
  DtHrFim (* Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*)], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
      Dmod.InsereWBMovCab(MovimCod, emidCompra, Codigo)
    else
    begin
      //DataHora := DtHrIni;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'wbmovits', False, [
      'Empresa'(*, 'DataHora'*)], ['MovimCod'], [
      Empresa, Terceiro, DataHora], [MovimCod], True);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmWBRclCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'wbrclcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'wbrclcab', 'Codigo');
end;

procedure TFmWBRclCab.BtItsClick(Sender: TObject);
begin
//  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmWBRclCab.Alterareclassificao1Click(Sender: TObject);
var
  MovimCod, MovimTwn, Codigo, Controle, Filial: Integer;
  ValorT, AreaM2: Double;
begin
  MovimCod := QrReclasifMovimCod.Value;
  MovimTwn := QrReclasifMovimTwn.Value;
  Codigo   := QrReclasifCodigo.Value;
  Controle := QrReclasifControle.Value;
  ValorT   := QrReclasifValorT.Value;
  AreaM2   := QrReclasifAreaM2.Value;
  Filial   := QrWBrclCabEmpresa.Value;
  //
  FmPrincipal.MostraFormWBRclIns(stUpd, Filial, MovimCod, MovimTwn, Codigo,
    Controle, ValorT, AreaM2, QrReclasif, 0, wbrEstoque);
  ReopenWBInnIts(QrWBRclItsControle.Value);
end;

procedure TFmWBRclCab.BtReclasifClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMReclasif, BtReclasif);
end;

procedure TFmWBRclCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmWBRclCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmWBRclCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWBRclCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWBRclCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmWBRclCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWBRclCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrWBRclCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWBRclCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWBRclCab.QrResulTotaCalcFields(DataSet: TDataSet);
var
  Total, Item: Double;
begin
  if Dmod.QrControleWBNivGer.Value = Integer(encPecas) then
  begin
    Total := QrWBInnSumPecas.Value;
    Item  := QrResulTotaPecas.Value;
  end else
  begin
    Total := QrWBInnSumAreaM2.Value;
    Item  := QrResulTotaAreaM2.Value;
  end;
  if Total = 0 then
    QrResulTotaPERCENT_CLASS.Value := 0
  else
    QrResulTotaPERCENT_CLASS.Value := 100 * Item / Total;
  QrResulTotaCalcM2.Value := QrWBInnSumAreaM2.Value;
end;

procedure TFmWBRclCab.QrWBRclCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWBRclCab.QrWBRclCabAfterScroll(DataSet: TDataSet);
begin
  ReopenWBInnIts(0);
end;

procedure TFmWBRclCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrWBRclCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmWBRclCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWBRclCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wbrclcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmWBRclCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWBRclCab.frxWET_RECUR_001_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmWBRclCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWBRclCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'wbrclcab');
end;

procedure TFmWBRclCab.Classificao1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Niveis2: String;
  //DBCross1ColumnTotal0: TfrxMemoView;
  DBCross1: TfrxDBCrossView;
begin
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT SrcNivel2 ',
    'FROM wbmovits wmi',
    'WHERE wmi.SrcMovID=' + Geral.FF0(QrWBRclItsMovimCod.Value),
    'AND wmi.SrcNivel1=' + Geral.FF0(QrWBRclItsCodigo.Value),
    //'AND wmi.SrcNivel2=' + Geral.FF0(QrWBRclItsControle.Value),
    'AND wmi.MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
    '']);
    Niveis2 := '';
    Qry.First;
    while not Qry.Eof do
    begin
      Niveis2 := Niveis2 + ', ' +
        Geral.FF0(Qry.FieldByName('SrcNivel2').AsInteger);
      //
      Qry.next;
    end;
    Niveis2 := Copy(Niveis2, 3);
    //
    if Niveis2 <> '' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrWBInnSum, Dmod.MyDB, [
      'SELECT SUM(wmi.Pecas) Pecas, SUM(wmi.AreaM2) AreaM2 ',
      'FROM wbmovits wmi',
      'WHERE wmi.Controle IN (' + Niveis2 + ') ',
      '']);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrResulTota, Dmod.MyDB, [
      'SELECT "TOTAL" NO_Pallet, SUM(wmi.Pecas) Pecas, ',
      'SUM(wmi.AreaM2) AreaM2, ',
      'IF(gg1.Referencia <> "", gg1.Referencia, CONCAT("#", ',
      'ggx.Controle)) GGX_REFER, wmi.GraGruX',
      'FROM wbmovits wmi ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
      'WHERE wmi.SrcMovID=' + Geral.FF0(QrWBRclItsMovimCod.Value),
      'AND wmi.SrcNivel1=' + Geral.FF0(QrWBRclItsCodigo.Value),
      //'AND wmi.SrcNivel2=' + Geral.FF0(QrWBRclItsControle.Value),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
      'GROUP BY GraGruX',
      '']);
      //
      // SQL copiado do QrReclasif
      UnDmkDAC_PF.AbreMySQLQuery0(QrResulClas, Dmod.MyDB, [
      'SELECT IF(gg1.Referencia <> "", gg1.Referencia, CONCAT("#", ',
      'ggx.Controle)) GGX_REFER, ',
      'wmi.*, CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
      'FROM wbmovits wmi ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
      'WHERE wmi.SrcMovID=' + Geral.FF0(QrWBRclItsMovimCod.Value),
      'AND wmi.SrcNivel1=' + Geral.FF0(QrWBRclItsCodigo.Value),
      //'AND wmi.SrcNivel2=' + Geral.FF0(QrWBRclItsControle.Value),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
      'ORDER BY wmi.Controle ',
      '']);
      //
      //DBCross1ColumnTotal0 := frxWET_RECUR_001_01.FindObject('DBCross1ColumnTotal0') as TfrxMemoView;
      DBCross1 := frxWET_RECUR_001_01.FindObject('DBCross1') as TfrxDBCrossView;
      DBCross1.CellFields.Clear;
      if Dmod.QrControleWBNivGer.Value = Integer(encPecas) then
      begin
        //DBCross1ColumnTotal0.Memo.Text := 'Pe�as';
        DBCross1.CellFields.Add('Pecas');
      end else
      begin
        //DBCross1ColumnTotal0.Memo.Text := '�rea m�';
        DBCross1.CellFields.Add('AreaM2');
      end;
      //
      MyObjects.frxDefineDataSets(frxWET_RECUR_001_01, [
        DModG.frxDsDono,
        frxDsWBRclCab,
        frxDsWBRclIts,
        frxDsResulClas,
        frxDsResulTota
      ]);
      //
      MyObjects.frxMostra(frxWET_RECUR_001_01, 'Controle Couros');
    end else
      Geral.MB_Aviso('Nenhum item foi encontrado!');
  finally
    Qry.Free;
  end;
  // Erro itens DBGrid!
  // FastReport esta gerando erro no dataset?
  LocCod(QrWBRclCabCodigo.Value,QrWBRclCabCodigo.Value);
end;

procedure TFmWBRclCab.QrWBRclCabBeforeClose(
  DataSet: TDataSet);
begin
  QrWBRclIts.Close;
end;

procedure TFmWBRclCab.QrWBRclCabBeforeOpen(DataSet: TDataSet);
begin
  QrWBRclCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmWBRclCab.QrWBRclItsAfterScroll(DataSet: TDataSet);
begin
  ReopenReclasif(0);
end;

procedure TFmWBRclCab.QrWBRclItsBeforeClose(DataSet: TDataSet);
begin
  QrReclasif.Close;
end;

end.

