unit FluxPcpImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, frxClass, frxDBSet, dmkEditDateTimePicker;

type
  TFmFluxPcpImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrAProdEstg: TMySQLQuery;
    QrAProdEstgEntrega: TDateField;
    QrAProdEstgM2Pedido: TFloatField;
    QrAProdEstgControle: TIntegerField;
    QrAProdEstgTexto: TWideStringField;
    QrAProdEstgCorTxt: TWideStringField;
    QrAProdEstgEspesTxt: TWideStringField;
    QrAProdEstgClasse: TWideStringField;
    QrAProdEstgNO_IniStg: TWideStringField;
    QrAProdEstgNO_FimStg: TWideStringField;
    QrAProdEstgMPVIts: TIntegerField;
    QrAProdEstgDiaSeq: TIntegerField;
    QrAProdEstgConta: TIntegerField;
    QrAProdEstgDataSeq: TDateField;
    QrAProdEstgIniStg: TIntegerField;
    QrAProdEstgFimStg: TIntegerField;
    QrAProdEstgDtHrReal: TDateTimeField;
    frxAProdEstg: TfrxReport;
    frxDsAProdEstg: TfrxDBDataset;
    QrEstgAProd: TMySQLQuery;
    frxDsEstgAProd: TfrxDBDataset;
    QrEstgAProdOrdem: TIntegerField;
    QrEstgAProdNO_Estagio: TWideStringField;
    Panel5: TPanel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    TPIni: TdmkEditDateTimePicker;
    Label2: TLabel;
    TPFim: TdmkEditDateTimePicker;
    Panel7: TPanel;
    RGAgrupa: TRadioGroup;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    QrAProdEstgEntrega_TXT: TWideStringField;
    QrAProdEstgDataSeq_TXT: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrEstgAProdAfterScroll(DataSet: TDataSet);
    procedure frxAProdEstgGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    FIni_Txt, FFim_Txt: String;

  public
    { Public declarations }
  end;

  var
  FmFluxPcpImp: TFmFluxPcpImp;

implementation

uses UnMyObjects, DmkDAC_PF, Module, UnDmkProcFunc;

{$R *.DFM}

procedure TFmFluxPcpImp.BtOKClick(Sender: TObject);
var
  Empresa, Index: Integer;
  Ordem: String;
  Grupo1, Grupo2: TfrxGroupHeader;
  Futer1, Futer2: TfrxGroupFooter;
  Me_GH1, Me_FT1, Me_GH2, Me_FT2(*, MeValNome, MeTitNome, MeValCodi*): TfrxMemoView;
begin
  FIni_Txt := Geral.FDT(TPIni.Date, 1);
  FFim_Txt := Geral.FDT(TPFim.Date, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstgAProd, Dmod.MyDB, [
  'SELECT fpi.Ordem, fpi.Nome NO_Estagio',
  'FROM mpvpss mps',
  'LEFT JOIN fluxpcpstg fpi ON fpi.Codigo=mps.IniStg',
  'LEFT JOIN mpvits its ON its.Controle=mps.MPVIts',
  'WHERE mps.DataSeq BETWEEN "' + FIni_Txt + '" AND "' + FFim_Txt + '"',
  '',
  'UNION',
  '',
  'SELECT fpf.Ordem, fpf.Nome NO_Estagio',
  'FROM mpvpss mps',
  'LEFT JOIN fluxpcpstg fpf ON fpf.Codigo=mps.FimStg',
  'LEFT JOIN mpvits its ON its.Controle=mps.MPVIts',
  'WHERE mps.DataSeq BETWEEN "' + FIni_Txt + '" AND "' + FFim_Txt + '"',
  '',
  'ORDER BY Ordem',
  EmptyStr]);
  //
  Grupo1 := frxAProdEstg.FindObject('GH1') as TfrxGroupHeader;
  Grupo1.Visible := RGAgrupa.ItemIndex > 0;
  Futer1 := frxAProdEstg.FindObject('GF1') as TfrxGroupFooter;
  Futer1.Visible := Grupo1.Visible;
  Me_GH1 := frxAProdEstg.FindObject('MeGH1') as TfrxMemoView;
  Me_FT1 := frxAProdEstg.FindObject('MeGF1') as TfrxMemoView;
  case RGOrdem1.ItemIndex of
    0:
    begin
      Grupo1.Condition := 'frxDsAProdEstg."Texto"';
      Me_GH1.Memo.Text := '[frxDsAProdEstg."Texto"]';
      Me_FT1.Memo.Text := '[frxDsAProdEstg."Texto"]: ';
    end;
    1:
    begin
      Grupo1.Condition := 'frxDsAProdEstg."EspesTxt"';
      Me_GH1.Memo.Text := '[frxDsAProdEstg."EspesTxt"]';
      Me_FT1.Memo.Text := '[frxDsAProdEstg."EspesTxt"]: ';
    end;
    2:
    begin
      Grupo1.Condition := 'frxDsAProdEstg."CorTxt"';
      Me_GH1.Memo.Text := '[frxDsAProdEstg."CorTxt"]';
      Me_FT1.Memo.Text := '[frxDsAProdEstg."CorTxt"]: ';
    end;
    3:
    begin
      Grupo1.Condition := 'frxDsAProdEstg."Classe"';
      Me_GH1.Memo.Text := '[frxDsAProdEstg."Classe"]';
      Me_FT1.Memo.Text := '[frxDsAProdEstg."Classe"]: ';
    end;
    4:
    begin
      Grupo1.Condition := 'frxDsAProdEstg."Entrega"';
      Me_GH1.Memo.Text := '[frxDsAProdEstg."Entrega_TXT"]';
      Me_FT1.Memo.Text := '[frxDsAProdEstg."Entrega_TXT"]: ';
    end;
    5:
    begin
      Grupo1.Condition := 'frxDsAProdEstg."DataSeq"';
      Me_GH1.Memo.Text := '[frxDsAProdEstg."DataSeq_TXT"]';
      Me_FT1.Memo.Text := '[frxDsAProdEstg."DataSeq_TXT"]: ';
    end;
    else Grupo1.Condition := '***ERRO***';
  end;
  //
  Grupo2 := frxAProdEstg.FindObject('GH2') as TfrxGroupHeader;
  Grupo2.Visible := RGAgrupa.ItemIndex > 1;
  Futer2 := frxAProdEstg.FindObject('GF2') as TfrxGroupFooter;
  Futer2.Visible := Grupo2.Visible;
  Me_GH2 := frxAProdEstg.FindObject('MeGH2') as TfrxMemoView;
  Me_FT2 := frxAProdEstg.FindObject('MeGF2') as TfrxMemoView;
  case RGOrdem2.ItemIndex of
    0:
    begin
      Grupo2.Condition := 'frxDsAProdEstg."Texto"';
      Me_GH2.Memo.Text := '[frxDsAProdEstg."Texto"]';
      Me_FT2.Memo.Text := '[frxDsAProdEstg."Texto"]: ';
    end;
    1:
    begin
      Grupo2.Condition := 'frxDsAProdEstg."EspesTxt"';
      Me_GH2.Memo.Text := '[frxDsAProdEstg."EspesTxt"]';
      Me_FT2.Memo.Text := '[frxDsAProdEstg."EspesTxt"]: ';
    end;
    2:
    begin
      Grupo2.Condition := 'frxDsAProdEstg."CorTxt"';
      Me_GH2.Memo.Text := '[frxDsAProdEstg."CorTxt"]';
      Me_FT2.Memo.Text := '[frxDsAProdEstg."CorTxt"]: ';
    end;
    3:
    begin
      Grupo2.Condition := 'frxDsAProdEstg."Classe"';
      Me_GH2.Memo.Text := '[frxDsAProdEstg."Classe"]';
      Me_FT2.Memo.Text := '[frxDsAProdEstg."Classe"]: ';
    end;
    4:
    begin
      Grupo2.Condition := 'frxDsAProdEstg."Entrega"';
      Me_GH2.Memo.Text := '[frxDsAProdEstg."Entrega_TXT"]';
      Me_FT2.Memo.Text := '[frxDsAProdEstg."Entrega_TXT"]: ';
    end;
    5:
    begin
      Grupo2.Condition := 'frxDsAProdEstg."DataSeq"';
      Me_GH2.Memo.Text := '[frxDsAProdEstg."DataSeq_TXT"]';
      Me_FT2.Memo.Text := '[frxDsAProdEstg."DataSeq_TXT"]: ';
    end;
    else Grupo2.Condition := '***ERRO***';
  end;
  //
  MyObjects.frxDefineDataSets(frxAProdEstg, [
  frxDsEstgAProd,
  frxDsAProdEstg
  ]);
  MyObjects.frxMostra(frxAProdEstg, 'A Produzir no Dia por Est�gio');
end;

procedure TFmFluxPcpImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFluxPcpImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFluxPcpImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TPIni.Date := Date;
  TPFIm.Date := Date;
end;

procedure TFmFluxPcpImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFluxPcpImp.frxAProdEstgGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_NO_EMPRESA' then
    Value := Dmod.QrMasterEm.Value
  else
  if VarName = 'VAR_PERIODO' then
    Value := dmkPF.PeriodoImp(TPIni.Date, TPFim.Date, 0, 0, True, True,
    False, False, '', '')
  else if AnsiCompareText(VarName, 'VFR_AGR')  = 0 then Value := RGAgrupa.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD1') = 0 then Value := RGOrdem1.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD2') = 0 then Value := RGOrdem2.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD3') = 0 then Value := RGOrdem3.ItemIndex
  else
end;

procedure TFmFluxPcpImp.QrEstgAProdAfterScroll(DataSet: TDataSet);
const
  //Artigo (texto)-Espessura (Texto)-Cor (Texto)-Classe (Texto)-Entrega-Dia a produzir
  Ordens: array[0..5] of String = ('Texto', 'EspesTxt', 'CorTxt', 'Classe', 'Entrega', 'DataSeq');
  Agrups: array[0..5] of String = ('Texto', 'EspesTxt', 'CorTxt', 'Classe', 'Entrega', 'DataSeq');
var
  Ordem: String;
begin
  Ordem := 'ORDER BY ' +
    Ordens[RGOrdem1.ItemIndex] + ', ' +
    Ordens[RGOrdem2.ItemIndex] + ', ' +
    Ordens[RGOrdem3.ItemIndex];
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAProdEstg, Dmod.MyDB, [
  'SELECT fpi.Ordem OrdemI, fpf.Ordem OrdemF, ',
  'DATE_FORMAT(its.Entrega, "%d/%m/%Y") Entrega_TXT, ',
  'DATE_FORMAT(mps.DataSeq, "%d/%m/%Y") DataSeq_TXT, ',
  'its.Entrega, its.M2Pedido, its.Controle, ',
  'its.Texto, its.CorTxt, its.EspesTxt, its.Classe, ',
  'fpi.Nome NO_IniStg, fpf.Nome NO_FimStg, mps.* ',
  'FROM mpvpss mps ',
  'LEFT JOIN fluxpcpstg fpi ON fpi.Codigo=mps.IniStg ',
  'LEFT JOIN fluxpcpstg fpf ON fpf.Codigo=mps.FimStg ',
  'LEFT JOIN mpvits its ON its.Controle=mps.MPVIts ',
  'WHERE mps.DataSeq BETWEEN "' + FIni_Txt + '" AND "' + FFim_Txt + '" ',
  'AND ' + Geral.FF0(QrEstgAProdOrdem.Value) + ' BETWEEN fpi.Ordem AND fpf.Ordem ',
  'ORDER BY mps.DataSeq, fpi.Ordem, fpf.Ordem ',
  EmptyStr]);
end;

end.
