object FmCMPTOutIts: TFmCMPTOutIts
  Left = 339
  Top = 185
  Caption = 'PST-_CMPT-003 :: Edi'#231#227'o de Item de Devolu'#231#227'o de CMPT'
  ClientHeight = 489
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 333
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 504
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdit1
      end
      object Label10: TLabel
        Left = 8
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object DBEdit1: TDBEdit
        Left = 504
        Top = 20
        Width = 57
        Height = 21
        DataField = 'Cliente'
        DataSource = FmCMPTOut.DsCMPTOut
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 564
        Top = 20
        Width = 432
        Height = 21
        DataField = 'NOMECLI'
        DataSource = FmCMPTOut.DsCMPTOut
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 8
        Top = 20
        Width = 57
        Height = 21
        DataField = 'Empresa'
        DataSource = FmCMPTOut.DsCMPTOut
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 68
        Top = 20
        Width = 432
        Height = 21
        DataField = 'NOMEEMP'
        DataSource = FmCMPTOut.DsCMPTOut
        TabOrder = 3
      end
    end
    object PnAdd: TPanel
      Left = 0
      Top = 230
      Width = 1008
      Height = 103
      Align = alBottom
      TabOrder = 2
      Visible = False
      object GroupBox1: TGroupBox
        Left = 1
        Top = 1
        Width = 140
        Height = 101
        Align = alLeft
        Caption = ' Dados de baixa: '
        TabOrder = 0
        object Label4: TLabel
          Left = 4
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Peso (kg):'
        end
        object Label6: TLabel
          Left = 70
          Top = 16
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object Label7: TLabel
          Left = 70
          Top = 56
          Width = 50
          Height = 13
          Caption = 'Valor total:'
        end
        object Label8: TLabel
          Left = 8
          Top = 56
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object EdOutQtdkg: TdmkEdit
          Left = 4
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdOutQtdkgChange
          OnEnter = EdOutQtdkgEnter
        end
        object EdOutQtdPc: TdmkEdit
          Left = 70
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 1
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdOutQtdPcChange
          OnEnter = EdOutQtdPcEnter
        end
        object EdOutQtdVal: TdmkEdit
          Left = 70
          Top = 72
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdOutQtdValChange
          OnEnter = EdOutQtdValEnter
        end
        object EdOutQtdM2: TdmkEdit
          Left = 4
          Top = 72
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdOutQtdM2Change
          OnEnter = EdOutQtdM2Enter
        end
      end
      object GroupBox2: TGroupBox
        Left = 141
        Top = 1
        Width = 140
        Height = 101
        Align = alLeft
        Caption = ' Saldos resultantes:'
        TabOrder = 1
        object Label2: TLabel
          Left = 4
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Peso (kg):'
        end
        object Label3: TLabel
          Left = 70
          Top = 16
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object Label5: TLabel
          Left = 70
          Top = 56
          Width = 50
          Height = 13
          Caption = 'Valor total:'
        end
        object Label9: TLabel
          Left = 4
          Top = 56
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object EdSdoQtdkg: TdmkEdit
          Left = 4
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdSdoQtdPc: TdmkEdit
          Left = 70
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 1
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdSdoQtdVal: TdmkEdit
          Left = 70
          Top = 72
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdSdoQtdM2: TdmkEdit
          Left = 4
          Top = 72
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdOutQtdPcChange
          OnEnter = EdOutQtdPcEnter
        end
      end
      object GroupBox3: TGroupBox
        Left = 421
        Top = 1
        Width = 586
        Height = 101
        Align = alClient
        Caption = ' Faturamento do servi'#231'o prestado: '
        TabOrder = 3
        object Label11: TLabel
          Left = 255
          Top = 20
          Width = 64
          Height = 13
          Caption = 'Valor unit'#225'rio:'
        end
        object Label12: TLabel
          Left = 335
          Top = 20
          Width = 50
          Height = 13
          Caption = 'Valor total:'
          Enabled = False
        end
        object Label14: TLabel
          Left = 176
          Top = 20
          Width = 42
          Height = 13
          Caption = 'Peso kg:'
        end
        object Label13: TLabel
          Left = 176
          Top = 60
          Width = 44
          Height = 13
          Caption = 'NF CMO:'
        end
        object Label15: TLabel
          Left = 255
          Top = 60
          Width = 44
          Height = 13
          Caption = 'NF DMP:'
        end
        object Label18: TLabel
          Left = 335
          Top = 60
          Width = 39
          Height = 13
          Caption = 'NF DIQ:'
        end
        object EdMOPrcKg: TdmkEdit
          Left = 257
          Top = 36
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          QryCampo = 'MOPrcKg'
          UpdCampo = 'MOPrcKg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdMOPrcKgRedefinido
        end
        object EdMOValor: TdmkEdit
          Left = 337
          Top = 36
          Width = 76
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          ReadOnly = True
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'MOValor'
          UpdCampo = 'MOValor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGSitDevolu: TdmkRadioGroup
          Left = 12
          Top = 60
          Width = 157
          Height = 37
          Caption = ' Devolu'#231#227'o:  '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Parte'
            'Saldo'
            'Total')
          TabOrder = 1
          OnClick = RGComoCobraClick
          QryCampo = 'SitDevolu'
          UpdCampo = 'SitDevolu'
          UpdType = utYes
          OldValor = 0
        end
        object EdMOPesoKg: TdmkEdit
          Left = 176
          Top = 36
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'MOPesoKg'
          UpdCampo = 'MOPesoKg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdMOPesoKgRedefinido
        end
        object RGHowCobrMO: TdmkRadioGroup
          Left = 12
          Top = 20
          Width = 157
          Height = 37
          Caption = 'M'#227'o-de-obra:'
          Columns = 2
          ItemIndex = 1
          Items.Strings = (
            'PNF'
            'PLE')
          TabOrder = 0
          QryCampo = 'HowCobrMO'
          UpdCampo = 'HowCobrMO'
          UpdType = utYes
          OldValor = 0
        end
        object EdNFCMO: TdmkEdit
          Left = 176
          Top = 76
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'NFCMO'
          UpdCampo = 'NFCMO'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNFDMP: TdmkEdit
          Left = 257
          Top = 76
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'NFDMP'
          UpdCampo = 'NFDMP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNFDIQ: TdmkEdit
          Left = 337
          Top = 76
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'NFDIQ'
          UpdCampo = 'NFDIQ'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object GroupBox4: TGroupBox
        Left = 281
        Top = 1
        Width = 140
        Height = 101
        Align = alLeft
        Caption = ' Beneficiam. (deprecado): '
        TabOrder = 2
        object Label16: TLabel
          Left = 4
          Top = 16
          Width = 83
          Height = 13
          Caption = 'Peso (kg) [F6>%]:'
        end
        object Label17: TLabel
          Left = 6
          Top = 56
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object Label19: TLabel
          Left = 72
          Top = 56
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object EdBenefikg: TdmkEdit
          Left = 4
          Top = 32
          Width = 132
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdBenefikgKeyDown
        end
        object EdBenefiPc: TdmkEdit
          Left = 6
          Top = 72
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 1
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdBenefiM2: TdmkEdit
          Left = 72
          Top = 72
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object GradeCMPTInn: TDBGrid
      Left = 0
      Top = 48
      Width = 1008
      Height = 182
      TabStop = False
      Align = alClient
      DataSource = DsCMPTInn
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = GradeCMPTInnDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID Entrada'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataE'
          Title.Caption = 'Data E.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NFInn'
          Title.Caption = 'NF entrada'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InnQtdkg'
          Title.Caption = 'Kg PNF'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InnQtdPc'
          Title.Caption = 'Pe'#231'as E.'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InnQtdVal'
          Title.Caption = 'Valor E.'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NFRef'
          Title.Caption = 'Ref. NF'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoQtdkg'
          Title.Caption = 'Saldo kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoQtdPc'
          Title.Caption = 'Saldo pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoQtdVal'
          Title.Caption = 'Saldo valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PLEKg'
          Title.Caption = 'Kg PLE'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValMaoObra'
          Title.Caption = 'MO total $'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrcUnitMO'
          Title.Caption = 'MO unit. $'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoFMOKg'
          Title.Caption = 'Sdo Fat. MO Kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoFMOVal'
          Title.Caption = 'Sdo Fat MO $'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InnQtdM2'
          Title.Caption = #193'rea m'#178' E.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoQtdM2'
          Title.Caption = 'Saldo m'#178
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 476
        Height = 32
        Caption = 'Edi'#231#227'o de Item de Devolu'#231#227'o de CMPT'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 476
        Height = 32
        Caption = 'Edi'#231#227'o de Item de Devolu'#231#227'o de CMPT'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 476
        Height = 32
        Caption = 'Edi'#231#227'o de Item de Devolu'#231#227'o de CMPT'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 381
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 685
        Height = 16
        Caption = 
          'CMO: Cobran'#231'a de m'#227'o-de-obra          DMP: Devolu'#231#227'o de mat'#233'ria-' +
          'prima.         DIQ: Devolu'#231#227'o de insumos qu'#237'micos.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 685
        Height = 16
        Caption = 
          'CMO: Cobran'#231'a de m'#227'o-de-obra          DMP: Devolu'#231#227'o de mat'#233'ria-' +
          'prima.         DIQ: Devolu'#231#227'o de insumos qu'#237'micos.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 425
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 24
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BitBtn1: TBitBtn
        Tag = 15
        Left = 212
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Desiste'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn1Click
      end
    end
  end
  object QrCMPTInn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.GerBxaEstq, inn.*'
      'FROM CMPTinn inn'
      'LEFT JOIN gragrux ggx ON ggx.Controle=inn.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE inn.Empresa=:P0'
      'AND inn.Cliente=:P1'
      
        'AND IF(gg1.GerBxaEstq=1, inn.SdoQtdPc, IF(gg1.GerBxaEstq=2, inn.' +
        'SdoQtdM2,inn.SdoQtdkg) )> 0'
      '')
    Left = 48
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCMPTInnCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCMPTInnCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrCMPTInnDataE: TDateField
      FieldName = 'DataE'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCMPTInnNFInn: TIntegerField
      FieldName = 'NFInn'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTInnNFRef: TIntegerField
      FieldName = 'NFRef'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrCMPTInnInnQtdkg: TFloatField
      FieldName = 'InnQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTInnInnQtdPc: TFloatField
      FieldName = 'InnQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCMPTInnInnQtdVal: TFloatField
      FieldName = 'InnQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTInnSdoQtdkg: TFloatField
      FieldName = 'SdoQtdkg'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTInnSdoQtdPc: TFloatField
      FieldName = 'SdoQtdPc'
      Required = True
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCMPTInnSdoQtdVal: TFloatField
      FieldName = 'SdoQtdVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTInnEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCMPTInnGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrCMPTInnTipoNF: TSmallintField
      FieldName = 'TipoNF'
    end
    object QrCMPTInnrefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrCMPTInnmodNF: TSmallintField
      FieldName = 'modNF'
    end
    object QrCMPTInnSerie: TIntegerField
      FieldName = 'Serie'
    end
    object QrCMPTInnInnQtdM2: TFloatField
      FieldName = 'InnQtdM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTInnSdoQtdM2: TFloatField
      FieldName = 'SdoQtdM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTInnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCMPTInnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCMPTInnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCMPTInnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCMPTInnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCMPTInnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCMPTInnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCMPTInnjaAtz: TSmallintField
      FieldName = 'jaAtz'
    end
    object QrCMPTInnProcedencia: TIntegerField
      FieldName = 'Procedencia'
    end
    object QrCMPTInnValMaoObra: TFloatField
      FieldName = 'ValMaoObra'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCMPTInnNF_CC: TIntegerField
      FieldName = 'NF_CC'
    end
    object QrCMPTInnGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrCMPTInnHowCobrMO: TSmallintField
      FieldName = 'HowCobrMO'
    end
    object QrCMPTInnPLEKg: TFloatField
      FieldName = 'PLEKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTInnPrcUnitMO: TFloatField
      FieldName = 'PrcUnitMO'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrCMPTInnSdoFMOKg: TFloatField
      FieldName = 'SdoFMOKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCMPTInnSdoFMOVal: TFloatField
      FieldName = 'SdoFMOVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsCMPTInn: TDataSource
    DataSet = QrCMPTInn
    Left = 48
    Top = 168
  end
end
