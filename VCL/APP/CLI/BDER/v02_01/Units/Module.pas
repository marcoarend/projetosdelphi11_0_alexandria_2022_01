unit Module;
//Lic_Dmk.LiberaUso5

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, UnInternalConsts, UMySQLModule, ComCtrls,
  mySQLDbTables, UnGOTOy, Winsock, MySQLBatch, extctrls, frxClass, frxDBSet,
  dmkGeral, StdCtrls, Variants, UnDmkProcFunc, DmkDAC_PF, MyListas,
  UnProjGroup_Consts, UnDmkEnums, CreateBlueDerm, dmkEditCB,
  dmkDBLookupComboBox, UnProjGroup_Vars, UnAppEnums, UnGrl_Vars;

type
  TFormulaGGX = (fggxNone, fggxPQ, fggxGGX);
  TDmod = class(TDataModule)
    QrFields: TMySQLQuery;
    QrMaster: TMySQLQuery;
    QrLivreY_D: TmySQLQuery;
    QrRecCountX: TMySQLQuery;
    QrRecCountXRecord: TIntegerField;
    QrUser: TMySQLQuery;
    QrSelX: TMySQLQuery;
    QrSelXLk: TIntegerField;
    QrSB: TMySQLQuery;
    QrSBCodigo: TIntegerField;
    QrSBNome: TWideStringField;
    DsSB: TDataSource;
    QrSB2: TMySQLQuery;
    QrSB2Codigo: TFloatField;
    QrSB2Nome: TWideStringField;
    DsSB2: TDataSource;
    QrSB3: TMySQLQuery;
    QrSB3Codigo: TDateField;
    QrSB3Nome: TWideStringField;
    DsSB3: TDataSource;
    QrDuplicIntX: TMySQLQuery;
    QrDuplicIntXINTEIRO1: TIntegerField;
    QrDuplicIntXINTEIRO2: TIntegerField;
    QrDuplicIntXCODIGO: TIntegerField;
    QrDuplicStrX: TMySQLQuery;
    QrDuplicStrXNOME: TWideStringField;
    QrDuplicStrXCODIGO: TIntegerField;
    QrDuplicStrXANTERIOR: TIntegerField;
    QrInsLogX: TMySQLQuery;
    QrDelLogX: TMySQLQuery;
    QrDataBalY: TmySQLQuery;
    QrDataBalYData: TDateField;
    QrSenha: TMySQLQuery;
    QrSenhaLogin: TWideStringField;
    QrSenhaNumero: TIntegerField;
    QrSenhaSenha: TWideStringField;
    QrSenhaPerfil: TIntegerField;
    QrSenhaLk: TIntegerField;
    QrUserLogin: TWideStringField;
    QrMaster2: TMySQLQuery;
    QrProduto: TMySQLQuery;
    QrVendas: TMySQLQuery;
    QrEntrada: TMySQLQuery;
    QrEntradaPecas: TFloatField;
    QrEntradaValor: TFloatField;
    QrVendasPecas: TFloatField;
    QrVendasValor: TFloatField;
    QrUpdU: TmySQLQuery;
    QrUpdY: TmySQLQuery;
    QrLivreY: TmySQLQuery;
    QrLivreYCodigo: TIntegerField;
    QrMaster2Em: TWideStringField;
    QrMaster2CNPJ: TWideStringField;
    QrMaster2Monitorar: TSmallintField;
    QrMaster2Distorcao: TIntegerField;
    QrMaster2DataI: TDateField;
    QrMaster2DataF: TDateField;
    QrMaster2Hoje: TDateField;
    QrMaster2Hora: TTimeField;
    QrMaster2MasLogin: TWideStringField;
    QrMaster2MasSenha: TWideStringField;
    QrMaster2MasAtivar: TWideStringField;
    QrCountY: TmySQLQuery;
    QrCountYRecord: TIntegerField;
    QrLocY: TmySQLQuery;
    QrLocYRecord: TIntegerField;
    QrLocDataY: TmySQLQuery;
    QrLocDataYRecord: TDateField;
    QrLivreY_DCodigo: TLargeintField;
    QrIdx: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrSQL: TmySQLQuery;
    QrSB4: TmySQLQuery;
    DsSB4: TDataSource;
    QrSB4Codigo: TWideStringField;
    QrSB4Nome: TWideStringField;
    QrPriorNext: TmySQLQuery;
    QrSoma1: TmySQLQuery;
    QrSoma1TOTAL: TFloatField;
    QrSoma1Qtde: TFloatField;
    QrControle: TmySQLQuery;
    QrEstoque: TmySQLQuery;
    QrEstoqueTipo: TSmallintField;
    QrEstoqueQtde: TFloatField;
    QrEstoqueValorCus: TFloatField;
    QrPeriodoBal: TmySQLQuery;
    QrPeriodoBalPeriodo: TIntegerField;
    QrMin: TmySQLQuery;
    QrMinPeriodo: TIntegerField;
    QrBalancosIts: TmySQLQuery;
    QrBalancosItsEstqQ: TFloatField;
    QrBalancosItsEstqV: TFloatField;
    QrEstqperiodo: TmySQLQuery;
    QrBalancosItsEstqQ_G: TFloatField;
    QrBalancosItsEstqV_G: TFloatField;
    QrEstqperiodoTipo: TSmallintField;
    QrEstqperiodoQtde: TFloatField;
    QrEstqperiodoValorCus: TFloatField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrProdutoCodigo: TIntegerField;
    QrProdutoControla: TWideStringField;
    QrNTV: TmySQLQuery;
    QrNTI: TmySQLQuery;
    ZZDB: TmySQLDatabase;
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroNOMEpUF: TWideStringField;
    QrTerceiroNOMEeUF: TWideStringField;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TWideStringField;
    QrTerceiroFantasia: TWideStringField;
    QrTerceiroRespons1: TWideStringField;
    QrTerceiroRespons2: TWideStringField;
    QrTerceiroPai: TWideStringField;
    QrTerceiroMae: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroIEST: TWideStringField;
    QrTerceiroNome: TWideStringField;
    QrTerceiroApelido: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroRG: TWideStringField;
    QrTerceiroELograd: TSmallintField;
    QrTerceiroERua: TWideStringField;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroECompl: TWideStringField;
    QrTerceiroEBairro: TWideStringField;
    QrTerceiroECidade: TWideStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TWideStringField;
    QrTerceiroETe1: TWideStringField;
    QrTerceiroEte2: TWideStringField;
    QrTerceiroEte3: TWideStringField;
    QrTerceiroECel: TWideStringField;
    QrTerceiroEFax: TWideStringField;
    QrTerceiroEEmail: TWideStringField;
    QrTerceiroEContato: TWideStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPLograd: TSmallintField;
    QrTerceiroPRua: TWideStringField;
    QrTerceiroPNumero: TIntegerField;
    QrTerceiroPCompl: TWideStringField;
    QrTerceiroPBairro: TWideStringField;
    QrTerceiroPCidade: TWideStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TWideStringField;
    QrTerceiroPTe1: TWideStringField;
    QrTerceiroPte2: TWideStringField;
    QrTerceiroPte3: TWideStringField;
    QrTerceiroPCel: TWideStringField;
    QrTerceiroPFax: TWideStringField;
    QrTerceiroPEmail: TWideStringField;
    QrTerceiroPContato: TWideStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TWideStringField;
    QrTerceiroResponsavel: TWideStringField;
    QrTerceiroProfissao: TWideStringField;
    QrTerceiroCargo: TWideStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TWideStringField;
    QrTerceiroCliente2: TWideStringField;
    QrTerceiroFornece1: TWideStringField;
    QrTerceiroFornece2: TWideStringField;
    QrTerceiroFornece3: TWideStringField;
    QrTerceiroFornece4: TWideStringField;
    QrTerceiroTerceiro: TWideStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TWideStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TWideStringField;
    QrTerceiroObservacoes: TWideMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroCLograd: TSmallintField;
    QrTerceiroCRua: TWideStringField;
    QrTerceiroCNumero: TIntegerField;
    QrTerceiroCCompl: TWideStringField;
    QrTerceiroCBairro: TWideStringField;
    QrTerceiroCCidade: TWideStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TWideStringField;
    QrTerceiroCTel: TWideStringField;
    QrTerceiroCCel: TWideStringField;
    QrTerceiroCFax: TWideStringField;
    QrTerceiroCContato: TWideStringField;
    QrTerceiroLLograd: TSmallintField;
    QrTerceiroLRua: TWideStringField;
    QrTerceiroLNumero: TIntegerField;
    QrTerceiroLCompl: TWideStringField;
    QrTerceiroLBairro: TWideStringField;
    QrTerceiroLCidade: TWideStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TWideStringField;
    QrTerceiroLTel: TWideStringField;
    QrTerceiroLCel: TWideStringField;
    QrTerceiroLFax: TWideStringField;
    QrTerceiroLContato: TWideStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TWideStringField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroAccount: TIntegerField;
    QrTerceiroLogo2: TBlobField;
    QrTerceiroConjugeNome: TWideStringField;
    QrTerceiroConjugeNatal: TDateField;
    QrTerceiroNome1: TWideStringField;
    QrTerceiroNatal1: TDateField;
    QrTerceiroNome2: TWideStringField;
    QrTerceiroNatal2: TDateField;
    QrTerceiroNome3: TWideStringField;
    QrTerceiroNatal3: TDateField;
    QrTerceiroNome4: TWideStringField;
    QrTerceiroNatal4: TDateField;
    QrTerceiroCreditosI: TIntegerField;
    QrTerceiroCreditosL: TIntegerField;
    QrTerceiroCreditosF2: TFloatField;
    QrTerceiroCreditosD: TDateField;
    QrTerceiroCreditosU: TDateField;
    QrTerceiroCreditosV: TDateField;
    QrTerceiroMotivo: TIntegerField;
    QrTerceiroQuantI1: TIntegerField;
    QrTerceiroQuantI2: TIntegerField;
    QrTerceiroQuantI3: TIntegerField;
    QrTerceiroQuantI4: TIntegerField;
    QrTerceiroQuantN1: TFloatField;
    QrTerceiroQuantN2: TFloatField;
    QrTerceiroAgenda: TWideStringField;
    QrTerceiroSenhaQuer: TWideStringField;
    QrTerceiroSenha1: TWideStringField;
    QrTerceiroLimiCred: TFloatField;
    QrTerceiroDesco: TFloatField;
    QrTerceiroCasasApliDesco: TSmallintField;
    QrTerceiroTempD: TFloatField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TIntegerField;
    QrTerceiroUserAlt: TIntegerField;
    QrTerceiroCPF_Pai: TWideStringField;
    QrTerceiroSSP: TWideStringField;
    QrTerceiroCidadeNatal: TWideStringField;
    QrTerceiroUFNatal: TSmallintField;
    QrAgora: TmySQLQuery;
    QrTerminais: TmySQLQuery;
    QrTerminaisIP: TWideStringField;
    QrTerminaisTerminal: TIntegerField;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrUserSets: TmySQLQuery;
    QrUserSetsDefinicao: TWideStringField;
    QrUserSetsAbilitado: TSmallintField;
    QrUserSetsVisivel: TSmallintField;
    QrUserSetsNomeForm: TWideStringField;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctLk: TIntegerField;
    QrLctFatID: TIntegerField;
    QrLctFatParcela: TIntegerField;
    QrLctCONTA: TIntegerField;
    QrLctNOMECONTA: TWideStringField;
    QrLctNOMEEMPRESA: TWideStringField;
    QrLctNOMESUBGRUPO: TWideStringField;
    QrLctNOMEGRUPO: TWideStringField;
    QrLctNOMECONJUNTO: TWideStringField;
    QrLctNOMESIT: TWideStringField;
    QrLctAno: TFloatField;
    QrLctMENSAL: TWideStringField;
    QrLctMENSAL2: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctLocal: TIntegerField;
    QrLctFatura: TWideStringField;
    QrLctSub: TSmallintField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctPago: TFloatField;
    QrLctSALDO: TFloatField;
    QrLctID_Sub: TSmallintField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctcliente: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctNOMECLIENTE: TWideStringField;
    QrLctNOMEFORNECEDOR: TWideStringField;
    QrLctTIPOEM: TWideStringField;
    QrLctNOMERELACIONADO: TWideStringField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctMulta: TFloatField;
    QrLctATRASO: TFloatField;
    QrLctJUROS: TFloatField;
    QrLctDataDoc: TDateField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctMes2: TLargeintField;
    QrLctProtesto: TDateField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TSmallintField;
    QrLctUserAlt: TSmallintField;
    QrLctControle: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctCtrlIni: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctCOMPENSADO_TXT: TWideStringField;
    QrLctCliInt: TIntegerField;
    QrLctDepto: TIntegerField;
    QrLctDescoPor: TIntegerField;
    QrLctForneceI: TIntegerField;
    QrLctQtde: TFloatField;
    DsLct: TDataSource;
    DsCarteiras: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasNOMEDOBANCO: TWideStringField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasDIFERENCA: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTIPOPRAZO: TWideStringField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TSmallintField;
    QrCarteirasUserAlt: TSmallintField;
    QrCarteirasNOMEPAGREC: TWideStringField;
    QrCarteirasPagRec: TSmallintField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasNome2: TWideStringField;
    QrCarteirasNOMEFORNECEI: TWideStringField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasContato1: TWideStringField;
    QrTransf: TmySQLQuery;
    QrTransfData: TDateField;
    QrTransfTipo: TSmallintField;
    QrTransfCarteira: TIntegerField;
    QrTransfControle: TIntegerField;
    QrTransfGenero: TIntegerField;
    QrTransfDebito: TFloatField;
    QrTransfCredito: TFloatField;
    QrTransfDocumento: TFloatField;
    QrUpdW: TmySQLQuery;
    QrSumPQ_H: TmySQLQuery;
    QrSumPQ_HPeso: TFloatField;
    QrSumPQ_HValor: TFloatField;
    QrLocPerf: TmySQLQuery;
    SmallintField1: TSmallintField;
    StringField1: TWideStringField;
    QrMaster2Limite: TSmallintField;
    QrMaster2Licenca: TWideStringField;
    QrTerminaisLicenca: TWideStringField;
    QrSenhaFuncionario: TIntegerField;
    QrUpdZ: TmySQLQuery;
    QrMasterDono: TIntegerField;
    QrMasterSolicitaSenha: TSmallintField;
    QrErrQuit: TmySQLQuery;
    QrErrQuitControle1: TIntegerField;
    QrErrQuitControle2: TIntegerField;
    QrErrQuitData: TDateField;
    QrErrQuitSit: TIntegerField;
    QrErrQuitDescricao: TWideStringField;
    QrAPL: TmySQLQuery;
    QrAPLCredito: TFloatField;
    QrAPLDebito: TFloatField;
    QrAPLData: TDateField;
    QrAPLMoraVal: TFloatField;
    QrAPLMultaVal: TFloatField;
    QrVLA: TmySQLQuery;
    QrVLACredito: TFloatField;
    QrVLADebito: TFloatField;
    QrMPBxa: TmySQLQuery;
    QrMPBxaPeso: TFloatField;
    QrMPBxaPecas: TFloatField;
    QrMPBxaM2: TFloatField;
    QrMPBxaP2: TFloatField;
    QrLctFatNum: TFloatField;
    QrSumPQ_D: TmySQLQuery;
    QrSumPQ_DPeso: TFloatField;
    QrSumPQ_DValor: TFloatField;
    QrBoss: TmySQLQuery;
    QrBossMasSenha: TWideStringField;
    QrBossMasLogin: TWideStringField;
    QrBossEm: TWideStringField;
    QrBossCNPJ: TWideStringField;
    QrBossMasZero: TWideStringField;
    QrBSit: TmySQLQuery;
    QrBSitSitSenha: TSmallintField;
    QrSenhas: TmySQLQuery;
    QrSenhasSenhaNew: TWideStringField;
    QrSenhasSenhaOld: TWideStringField;
    QrSenhaslogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasFuncionario: TIntegerField;
    QrSSit: TmySQLQuery;
    QrSSitSitSenha: TSmallintField;
    QrSCMPTOut: TmySQLQuery;
    QrSCMPTOutOutQtdVal: TFloatField;
    QrSCMPTOutOutQtdkg: TFloatField;
    QrSCMPTOutOutQtdPc: TFloatField;
    QrSCMPTInn: TmySQLQuery;
    QrSCMPTInnValr: TFloatField;
    QrSCMPTInnPeso: TFloatField;
    QrSCMPTInnPeca: TFloatField;
    QrSumCus: TmySQLQuery;
    QrSumCusCusto: TFloatField;
    QrSCus2: TmySQLQuery;
    QrSCus2MPIn: TIntegerField;
    QrSCus2Custo: TFloatField;
    QrSCus2Pecas: TFloatField;
    QrSCus2Peso: TFloatField;
    QrSCus2MinDta: TDateTimeField;
    QrSCus2MaxDta: TDateTimeField;
    QrSCus2Fuloes: TLargeintField;
    QrSCus2Setor: TIntegerField;
    QrSPerdas: TmySQLQuery;
    QrSPerdasPecas: TFloatField;
    QrMPIn: TmySQLQuery;
    QrMPInForcaEncer: TSmallintField;
    QrMPInPecas: TFloatField;
    QrUsoPQ: TmySQLQuery;
    QrUsoPQValor: TFloatField;
    QrPQCompr: TmySQLQuery;
    QrPQComprCtaPQCompr: TIntegerField;
    QrPQComprSubgrupo: TIntegerField;
    QrPQComprGrupo: TIntegerField;
    QrPQComprConjunto: TIntegerField;
    QrPQComprNOMECONTA: TWideStringField;
    QrPQComprNOMECONTA2: TWideStringField;
    QrPQComprNOMESUBGRUPO: TWideStringField;
    QrPQComprNOMEGRUPO: TWideStringField;
    QrPQComprNOMECONJUNTO: TWideStringField;
    QrPQComprcjOrdemLista: TIntegerField;
    QrPQComprgrOrdemLista: TIntegerField;
    QrPQComprsgOrdemLista: TIntegerField;
    QrPQComprcoOrdemLista: TIntegerField;
    QrSumKgsRib: TmySQLQuery;
    QrSumCusRib1: TmySQLQuery;
    QrSumCusRib1Valor: TFloatField;
    QrSumKgsRibPeso: TFloatField;
    QrSenhasIP_Default: TWideStringField;
    QrMPInPecasNeg: TFloatField;
    QrMPInM2: TFloatField;
    QrMPInP2: TFloatField;
    QrMPInPLE: TFloatField;
    QrGGX: TmySQLQuery;
    QrGGXControle: TIntegerField;
    QrPQG: TmySQLQuery;
    QrPQGDataX: TDateField;
    QrPQGOrigemCodi: TIntegerField;
    QrPQGOrigemCtrl: TIntegerField;
    QrPQGTipo: TIntegerField;
    QrPQGCliOrig: TIntegerField;
    QrPQGCliDest: TIntegerField;
    QrPQGInsumo: TIntegerField;
    QrPQGPeso: TFloatField;
    QrPQGValor: TFloatField;
    QrPQGAlterWeb: TSmallintField;
    QrPQGAtivo: TSmallintField;
    QrPQGRetorno: TSmallintField;
    QrPQGStqMovIts: TIntegerField;
    QrPQGide_mod: TSmallintField;
    QrSMIA: TmySQLQuery;
    QrSMIAIDCtrl: TIntegerField;
    QrMPI: TmySQLQuery;
    QrMPICodigo: TIntegerField;
    QrMPIControle: TIntegerField;
    QrMPIFicha: TIntegerField;
    QrMPIData: TDateField;
    QrMPIClienteI: TIntegerField;
    QrMPIProcedencia: TIntegerField;
    QrMPITransportadora: TIntegerField;
    QrMPIMarca: TWideStringField;
    QrMPIPecas: TFloatField;
    QrMPIPecasNF: TFloatField;
    QrMPIM2: TFloatField;
    QrMPIP2: TFloatField;
    QrMPIFimM2: TFloatField;
    QrMPIFimP2: TFloatField;
    QrMPIPNF: TFloatField;
    QrMPIPLE: TFloatField;
    QrMPIPDA: TFloatField;
    QrMPIRecorte_PDA: TFloatField;
    QrMPIPTA: TFloatField;
    QrMPIRecorte_PTA: TFloatField;
    QrMPIRaspa_PTA: TFloatField;
    QrMPITipificacao: TIntegerField;
    QrMPIAparasCabelo: TSmallintField;
    QrMPISeboPreDescarne: TSmallintField;
    QrMPICustoInfo: TFloatField;
    QrMPIFreteInfo: TFloatField;
    QrMPIValor: TFloatField;
    QrMPICustoPQ: TFloatField;
    QrMPIFrete: TFloatField;
    QrMPIAbateTipo: TIntegerField;
    QrMPILote: TWideStringField;
    QrMPIPecasOut: TFloatField;
    QrMPIPecasSal: TFloatField;
    QrMPICaminhoes: TIntegerField;
    QrMPIQuemAssina: TIntegerField;
    QrMPICorretor: TIntegerField;
    QrMPIComisPer: TFloatField;
    QrMPIComisVal: TFloatField;
    QrMPIDescAdiant: TFloatField;
    QrMPICondPagto: TWideStringField;
    QrMPICondComis: TWideStringField;
    QrMPILocalEntrg: TWideStringField;
    QrMPIObserv: TWideStringField;
    QrMPIAnimal: TSmallintField;
    QrMPICMPValor: TFloatField;
    QrMPICMPFrete: TFloatField;
    QrMPIQuebrVReal: TFloatField;
    QrMPIQuebrVCobr: TFloatField;
    QrMPICMPDesQV: TFloatField;
    QrMPICMPPagar: TFloatField;
    QrMPIPcBxa: TFloatField;
    QrMPIkgBxa: TFloatField;
    QrMPIM2Bxa: TFloatField;
    QrMPIP2Bxa: TFloatField;
    QrMPIPecasCal: TFloatField;
    QrMPIPecasCur: TFloatField;
    QrMPIPecasExp: TFloatField;
    QrMPIM2Exp: TFloatField;
    QrMPIP2Exp: TFloatField;
    QrMPIPecasNeg: TFloatField;
    QrMPIPesoCal: TFloatField;
    QrMPIPesoCur: TFloatField;
    QrMPICusPQCal: TFloatField;
    QrMPICusPQCur: TFloatField;
    QrMPIFuloesCal: TIntegerField;
    QrMPIFuloesCur: TIntegerField;
    QrMPIMinDtaCal: TDateField;
    QrMPIMinDtaCur: TDateField;
    QrMPIMinDtaEnx: TDateField;
    QrMPIMaxDtaCal: TDateField;
    QrMPIMaxDtaCur: TDateField;
    QrMPIMaxDtaEnx: TDateField;
    QrMPIForcaEncer: TSmallintField;
    QrMPIEncerrado: TSmallintField;
    QrMPIPS_ValUni: TFloatField;
    QrMPIPS_QtdTot: TFloatField;
    QrMPIPS_ValTot: TFloatField;
    QrMPILk: TIntegerField;
    QrMPIDataCad: TDateField;
    QrMPIDataAlt: TDateField;
    QrMPIUserCad: TIntegerField;
    QrMPIUserAlt: TIntegerField;
    QrMPIAlterWeb: TSmallintField;
    QrMPIAtivo: TSmallintField;
    QrMPIEmClassif: TSmallintField;
    QrMPIPesoExp: TFloatField;
    QrMPIEstq_Pecas: TFloatField;
    QrMPIEstq_M2: TFloatField;
    QrMPIEstq_P2: TFloatField;
    QrMPIEstq_Peso: TFloatField;
    QrMPIStqMovIts: TIntegerField;
    QrCWB: TmySQLQuery;
    QrCWBCodigo: TIntegerField;
    QrCWBControle: TIntegerField;
    QrCWBIDCtrl: TIntegerField;
    QrCWBDataHora: TDateTimeField;
    QrCWBOriCodi: TIntegerField;
    QrCWBOriCnta: TIntegerField;
    QrCWBOriPart: TIntegerField;
    QrCWBEmpresa: TIntegerField;
    QrCWBQtde: TFloatField;
    QrCWBPecas: TFloatField;
    QrCWBPeso: TFloatField;
    QrCWBAreaM2: TFloatField;
    QrCWBAreaP2: TFloatField;
    QrCWBFatorClas: TFloatField;
    QrCWBDebCtrl: TIntegerField;
    QrSMI: TmySQLQuery;
    QrSMIStqCenCad: TIntegerField;
    QrSMIGraGruX: TIntegerField;
    QrSMIIDCtrl: TIntegerField;
    QrCWBPLE: TFloatField;
    QrSMIAGGX: TmySQLQuery;
    QrSMIAGGXIDCtrl: TIntegerField;
    QrSMIDataHora: TDateTimeField;
    QrPQ_GG1: TmySQLQuery;
    QrPQ_GG1Codigo: TIntegerField;
    QrPQ_GG1Ativo: TSmallintField;
    QrPQ_GG1NO_PQ: TWideStringField;
    QrPQ_GG1NO_GG1: TWideStringField;
    QrPQ_GG1PrdGrupTip: TIntegerField;
    QrPQ_GG1Nivel2: TIntegerField;
    QrPQ_GG1Nivel1: TIntegerField;
    QrPQCli: TmySQLQuery;
    QrPQCliControle: TIntegerField;
    QrCWBCMPValor: TFloatField;
    QrCWBCMPFrete: TFloatField;
    QrCWBCustoPQ: TFloatField;
    QrInPQ: TmySQLQuery;
    QrInPQTotalCusto: TFloatField;
    QrInPQTotalPeso: TFloatField;
    QrNiv1: TmySQLQuery;
    QrNiv1GraGruX: TIntegerField;
    QrNiv1GraTamI: TIntegerField;
    QrNiv1GraGruC: TIntegerField;
    QrNiv1QTDE: TFloatField;
    QrGraGruVal: TmySQLQuery;
    QrGraGruValControle: TIntegerField;
    QrPQEMed: TmySQLQuery;
    QrPQEMedInsumo: TIntegerField;
    QrPQxCus: TmySQLQuery;
    QrPQxCusIDCtrl: TIntegerField;
    QrPQxCusValor: TFloatField;
    QrMPInCus: TmySQLQuery;
    QrMPInCusCMPValor: TFloatField;
    QrMPInCusIDCtrl: TIntegerField;
    QrMaxPerBal: TmySQLQuery;
    QrMaxPerBalGrupoBal: TIntegerField;
    QrSCECTOut: TmySQLQuery;
    QrSCECTOutOutQtdVal: TFloatField;
    QrSCECTOutOutQtdkg: TFloatField;
    QrSCECTOutOutQtdPc: TFloatField;
    QrSCECTInn: TmySQLQuery;
    QrSCECTInnValr: TFloatField;
    QrSCECTInnPeso: TFloatField;
    QrSCECTInnPeca: TFloatField;
    //frxDsMaster: TfrxDBDataset;
    QrGGX_MP: TmySQLQuery;
    QrGGX_MPGerBxaEstq: TSmallintField;
    QrSMPIBxa: TmySQLQuery;
    QrSMPIBxaNO_PRD: TWideStringField;
    QrSMPIBxaSIGLA: TWideStringField;
    QrSMPIBxaDataHora: TDateTimeField;
    QrSMPIBxaIDCtrl: TIntegerField;
    QrSMPIBxaGraGruX: TIntegerField;
    QrSMPIBxaQtde: TFloatField;
    QrSMPIBxaPecas: TFloatField;
    QrSMPIBxaPeso: TFloatField;
    QrSMIAParCodi: TIntegerField;
    QrFormulas_EhGGX: TmySQLQuery;
    QrFormulas_EhGGXEhGGX: TSmallintField;
    QrFormulas_EhGGXItens: TLargeintField;
    QrInPQCI: TIntegerField;
    QrSumCusRib2: TmySQLQuery;
    QrSumCusRib2CustoAll: TFloatField;
    QrOX1: TmySQLQuery;
    QrOX1Controle: TIntegerField;
    QrMasterUsaAccMngr: TSmallintField;
    QrProds: TmySQLQuery;
    QrProdsProduto1: TIntegerField;
    QrProdsProduto2: TIntegerField;
    QrProdsProduto3: TIntegerField;
    QrProdsProduto4: TIntegerField;
    QrProdsProduto5: TIntegerField;
    QrLotes: TmySQLQuery;
    QrLotesControle: TIntegerField;
    QrLotesMPIn: TIntegerField;
    QrLotesPeso: TFloatField;
    QrLotesPecas: TFloatField;
    QrLotesFicha: TIntegerField;
    QrLotesMarca: TWideStringField;
    QrLotesLote: TWideStringField;
    frxDsMaster: TfrxDBDataset;
    Qr3Bal: TmySQLQuery;
    Qr3BalInsumo: TIntegerField;
    Qr3BalPeso: TFloatField;
    Qr3BalValor: TFloatField;
    Qr3Inn: TmySQLQuery;
    Qr3InnInsumo: TIntegerField;
    Qr3InnPeso: TFloatField;
    Qr3InnValor: TFloatField;
    Qr3Out: TmySQLQuery;
    Qr3OutInsumo: TIntegerField;
    Qr3OutPeso: TFloatField;
    Qr3OutValor: TFloatField;
    Qr3PQ: TmySQLQuery;
    Qr3PQInsumo: TIntegerField;
    Qr3PQNOMEPQ: TWideStringField;
    Qr3PQINNPESO: TFloatField;
    Qr3PQINNVALOR: TFloatField;
    Qr3PQOUTPESO: TFloatField;
    Qr3PQOUTVALOR: TFloatField;
    Qr3PQNOMEIQ: TWideStringField;
    Qr3PQNOMESETOR: TWideStringField;
    Qr3PQBALPESO: TFloatField;
    Qr3PQBALVALOR: TFloatField;
    QrPQRx: TmySQLQuery;
    QrPQRxPeso: TFloatField;
    QrSCMPTOutMOValor: TFloatField;
    QrSCMPTOutMOPesoKg: TFloatField;
    QrSCMPTOutOutQtdM2: TFloatField;
    QrSCMPTInnSdoFMOVal: TFloatField;
    QrSCMPTInnSdoFMOKg: TFloatField;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    QrVSMovItsGraGruY: TIntegerField;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrVSMovItsClientMO: TIntegerField;
    QrSumSMIA: TmySQLQuery;
    QrSumSMIAQtde: TFloatField;
    QrSumSMIACustoAll: TFloatField;
    Qr3Dvl: TmySQLQuery;
    Qr3DvlInsumo: TIntegerField;
    Qr3DvlPeso: TFloatField;
    Qr3DvlValor: TFloatField;
    Qr3PQDVLPESO: TFloatField;
    Qr3PQDVLVALOR: TFloatField;
    QrVSMovItsStqCenLoc: TIntegerField;
    QrControlePQ: TIntegerField;
    QrControlePQE: TIntegerField;
    QrControleControlaNeg: TSmallintField;
    QrControleFamilias: TSmallintField;
    QrControleFamiliasIts: TSmallintField;
    QrControleAskNFOrca: TSmallintField;
    QrControlePreviewNF: TSmallintField;
    QrControleOrcaRapido: TSmallintField;
    QrControleDistriDescoItens: TSmallintField;
    QrControleEntraSemValor: TSmallintField;
    QrControleMensalSempre: TSmallintField;
    QrControleBalType: TSmallintField;
    QrControleOrcaOrdem: TSmallintField;
    QrControleOrcaLinhas: TSmallintField;
    QrControleOrcaLFeed: TIntegerField;
    QrControleOrcaModelo: TSmallintField;
    QrControleOrcaRodaPos: TSmallintField;
    QrControleOrcaRodape: TSmallintField;
    QrControleOrcaCabecalho: TSmallintField;
    QrControleCoresRel: TSmallintField;
    QrControleMoraDD: TFloatField;
    QrControleMulta: TFloatField;
    QrControleAvisosCxaEdit: TSmallintField;
    QrControleTravaCidade: TSmallintField;
    QrControleChConfCab: TIntegerField;
    QrControleImpDOS: TIntegerField;
    QrControleUnidadePadrao: TIntegerField;
    QrControleProdutosV: TIntegerField;
    QrControleCartDespesas: TIntegerField;
    QrControleReserva: TSmallintField;
    QrControleUFPadrao: TIntegerField;
    QrControleDono: TIntegerField;
    QrControleErroHora: TIntegerField;
    QrControleSenhas: TIntegerField;
    QrControleSalarios: TIntegerField;
    QrControleEntidades: TIntegerField;
    QrControleUFs: TIntegerField;
    QrControleListaECivil: TIntegerField;
    QrControlePerfis: TIntegerField;
    QrControleUsuario: TIntegerField;
    QrControleContas: TIntegerField;
    QrControleCentroCusto: TIntegerField;
    QrControleDepartamentos: TIntegerField;
    QrControleDividas: TIntegerField;
    QrControleDividasIts: TIntegerField;
    QrControleDividasPgs: TIntegerField;
    QrControleCarteiras: TIntegerField;
    QrControleCartas: TIntegerField;
    QrControleConsignacao: TIntegerField;
    QrControleGrupo: TIntegerField;
    QrControleSubGrupo: TIntegerField;
    QrControleConjunto: TIntegerField;
    QrControleInflacao: TIntegerField;
    QrControlekm: TIntegerField;
    QrControlekmMedia: TIntegerField;
    QrControlekmIts: TIntegerField;
    QrControleFatura: TIntegerField;
    QrControleLanctos: TLargeintField;
    QrControleEntiGrupos: TIntegerField;
    QrControleAparencias: TIntegerField;
    QrControlePages: TIntegerField;
    QrControleMultiEtq: TIntegerField;
    QrControleEntiTransp: TIntegerField;
    QrControleMediaCH: TIntegerField;
    QrControleImprime: TIntegerField;
    QrControleImprimeBand: TIntegerField;
    QrControleImprimeView: TIntegerField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrControleCartVen: TIntegerField;
    QrControleCartCom: TIntegerField;
    QrControleCartDeS: TIntegerField;
    QrControleCartReS: TIntegerField;
    QrControleCartDeG: TIntegerField;
    QrControleCartReG: TIntegerField;
    QrControleCartCoE: TIntegerField;
    QrControleCartCoC: TIntegerField;
    QrControleCartEmD: TIntegerField;
    QrControleCartEmA: TIntegerField;
    QrControleMoedaVal: TFloatField;
    QrControleTela1: TIntegerField;
    QrControleChamarPgtoServ: TIntegerField;
    QrControleFormUsaTam: TIntegerField;
    QrControleFormHeight: TIntegerField;
    QrControleFormWidth: TIntegerField;
    QrControleFormPixEsq: TIntegerField;
    QrControleFormPixDir: TIntegerField;
    QrControleFormPixTop: TIntegerField;
    QrControleFormPixBot: TIntegerField;
    QrControleFormFoAlt: TIntegerField;
    QrControleFormFoPro: TFloatField;
    QrControleFormUsaPro: TIntegerField;
    QrControleFormSlides: TIntegerField;
    QrControleFormNeg: TIntegerField;
    QrControleFormIta: TIntegerField;
    QrControleFormSub: TIntegerField;
    QrControleFormExt: TIntegerField;
    QrControleFormFundoTipo: TIntegerField;
    QrControleServInterv: TIntegerField;
    QrControleServAntecip: TIntegerField;
    QrControleAdiLancto: TIntegerField;
    QrControleContaSal: TIntegerField;
    QrControleContaVal: TIntegerField;
    QrControleNiver: TSmallintField;
    QrControleNiverddA: TSmallintField;
    QrControleNiverddD: TSmallintField;
    QrControleLastPassD: TDateTimeField;
    QrControleMultiPass: TIntegerField;
    QrControleMPV: TIntegerField;
    QrControleMPVIts: TIntegerField;
    QrControleMyPagTip: TSmallintField;
    QrControleMyPagCar: TSmallintField;
    QrControlePQEIts: TIntegerField;
    QrControlereg10: TSmallintField;
    QrControlereg11: TSmallintField;
    QrControlereg50: TSmallintField;
    QrControlereg51: TSmallintField;
    QrControlereg53: TSmallintField;
    QrControlereg54: TSmallintField;
    QrControlereg56: TSmallintField;
    QrControlereg60: TSmallintField;
    QrControlereg75: TSmallintField;
    QrControlereg88: TSmallintField;
    QrControlereg90: TSmallintField;
    QrControleNumSerieNF: TSmallintField;
    QrControleSerieNF: TIntegerField;
    QrControleModeloNF: TSmallintField;
    QrControleDOSDotLinTop: TSmallintField;
    QrControleDOSDotLinBot: TSmallintField;
    QrControleCambiosData: TDateField;
    QrControleCambiosUsuario: TIntegerField;
    QrControleFormulas: TIntegerField;
    QrControleFormulasIts: TIntegerField;
    QrControleDOSGrades: TSmallintField;
    QrControleMPIn: TIntegerField;
    QrControleCorRecibo: TIntegerField;
    QrControleIdleMinutos: TIntegerField;
    QrControleCartaG: TIntegerField;
    QrControleMyPgParc: TSmallintField;
    QrControleMyPgQtdP: TSmallintField;
    QrControleMyPgPeri: TSmallintField;
    QrControleMyPgDias: TSmallintField;
    QrControleConfJanela: TIntegerField;
    QrControleDataPesqAuto: TSmallintField;
    QrControleAtividad: TSmallintField;
    QrControleCidades: TSmallintField;
    QrControlePaises: TSmallintField;
    QrControleFluxos: TIntegerField;
    QrControleFluxosIts: TIntegerField;
    QrControleOperacoes: TIntegerField;
    QrControlePallets: TIntegerField;
    QrControlePalletsIts: TIntegerField;
    QrControleMPClas: TIntegerField;
    QrControleMPGrup: TIntegerField;
    QrControleMPArti: TIntegerField;
    QrControleMPInDefeitos: TIntegerField;
    QrControleDefeitosloc: TIntegerField;
    QrControleDefeitostip: TIntegerField;
    QrControleDefPecas: TIntegerField;
    QrControleBalancosIts: TIntegerField;
    QrControleEmbalagens: TIntegerField;
    QrControleEmit: TIntegerField;
    QrControleEmitCus: TIntegerField;
    QrControleEmitIts: TIntegerField;
    QrControleEquipes: TIntegerField;
    QrControleEspessuras: TIntegerField;
    QrControleGruposQuimicos: TIntegerField;
    QrControleListaSetores: TIntegerField;
    QrControlePQFor: TIntegerField;
    QrControlePQCot: TIntegerField;
    QrControlePQCli: TIntegerField;
    QrControlePQx: TIntegerField;
    QrControlePQG: TIntegerField;
    QrControlePQO: TIntegerField;
    QrControlePQT: TIntegerField;
    QrControleLk: TIntegerField;
    QrControleDataCad: TDateField;
    QrControleDataAlt: TDateField;
    QrControleUserCad: TIntegerField;
    QrControleUserAlt: TIntegerField;
    QrControlePlano: TIntegerField;
    QrControleArtigosGrupos: TIntegerField;
    QrControleVendaCartPg: TIntegerField;
    QrControleVendaParcPg: TIntegerField;
    QrControleVendaPeriPg: TIntegerField;
    QrControleVendaDiasPg: TIntegerField;
    QrControleChequez: TSmallintField;
    QrControlePQPed: TIntegerField;
    QrControlePQPedIts: TIntegerField;
    QrControleProLaINSSr: TFloatField;
    QrControleProLaINSSp: TFloatField;
    QrControleVerSalTabs: TIntegerField;
    QrControleVerBcoTabs: TIntegerField;
    QrControleCNABCtaTar: TIntegerField;
    QrControleCNABCtaJur: TIntegerField;
    QrControleCNABCtaMul: TIntegerField;
    QrControleCNAB_CaD: TIntegerField;
    QrControleCNAB_CaG: TIntegerField;
    QrControleAtzCritic: TIntegerField;
    QrControleContasTrf: TIntegerField;
    QrControleSomaIts: TIntegerField;
    QrControleContasAgr: TIntegerField;
    QrControleExcelGru: TIntegerField;
    QrControleExcelGruImp: TIntegerField;
    QrControleComProdPerc: TIntegerField;
    QrControleComProdEdit: TIntegerField;
    QrControleComServPerc: TIntegerField;
    QrControleComServEdit: TIntegerField;
    QrControlePQNegBaixa: TSmallintField;
    QrControlePQNegEntra: TSmallintField;
    QrControleMPP: TIntegerField;
    QrControleMPPIts: TIntegerField;
    QrControleVerWeb: TIntegerField;
    QrControleAlterWeb: TSmallintField;
    QrControleVendaCouro: TSmallintField;
    QrControlePortSerBal: TIntegerField;
    QrControlePortSerLei: TIntegerField;
    QrControlePortSerIts: TIntegerField;
    QrControleCtaPQCompr: TIntegerField;
    QrControleCtaPQFrete: TIntegerField;
    QrControleCtaMPCompr: TIntegerField;
    QrControleCtaMPFrete: TIntegerField;
    QrControleCtaCVVenda: TIntegerField;
    QrControleCtaCVFrete: TIntegerField;
    QrControleImpRecRib: TSmallintField;
    QrControlePercula: TSmallintField;
    QrControleMPV2: TIntegerField;
    QrControleMPV2Its: TIntegerField;
    QrControleMPV2Bxa: TIntegerField;
    QrControleMPEstagios: TIntegerField;
    QrControleMPInIts: TIntegerField;
    QrControleCNAB_Rem: TIntegerField;
    QrControleCNAB_Rem_I: TIntegerField;
    QrControlePreEmMsgIm: TIntegerField;
    QrControlePreEmMsg: TIntegerField;
    QrControlePreEmail: TIntegerField;
    QrControleContasMes: TIntegerField;
    QrControleMultiPgto: TIntegerField;
    QrControleImpObs: TIntegerField;
    QrControleCodigo: TIntegerField;
    QrControleAtivo: TSmallintField;
    QrControleMyPerJuros: TFloatField;
    QrControleMyPerMulta: TFloatField;
    QrControleEquiGru: TIntegerField;
    QrControleEntiCtas: TIntegerField;
    QrControleContVen: TIntegerField;
    QrControleContCom: TIntegerField;
    QrControleCECTInn: TIntegerField;
    QrControleCECTOut: TIntegerField;
    QrControleCECTOutIts: TIntegerField;
    QrControleEquiCom: TIntegerField;
    QrControleContasLnk: TIntegerField;
    QrControleLastBco: TIntegerField;
    QrControleTintasCab: TIntegerField;
    QrControleTintasTin: TIntegerField;
    QrControleTintasIts: TIntegerField;
    QrControleTintasFlu: TIntegerField;
    QrControleSetorCal: TIntegerField;
    QrControleSetorCur: TIntegerField;
    QrControleMPInExp: TIntegerField;
    QrControleMPInPerdas: TIntegerField;
    QrControleRolPerdas: TIntegerField;
    QrControleCambioCot: TIntegerField;
    QrControleCambioMda: TIntegerField;
    QrControleMoedaBr: TIntegerField;
    QrControleSenhasIts: TIntegerField;
    QrControleCanAltBalA: TSmallintField;
    QrControleContasU: TIntegerField;
    QrControleEntDefAtr: TIntegerField;
    QrControleEntAtrCad: TIntegerField;
    QrControleEntAtrIts: TIntegerField;
    QrControleBalTopoNom: TIntegerField;
    QrControleBalTopoTit: TIntegerField;
    QrControleBalTopoPer: TIntegerField;
    QrControleCasasProd: TSmallintField;
    QrControleNCMs: TIntegerField;
    QrControleParamsNFs: TIntegerField;
    QrControleCarteirasU: TIntegerField;
    QrControleLctoEndoss: TIntegerField;
    QrControleEntiContat: TIntegerField;
    QrControleEntiCargos: TIntegerField;
    QrControleEntiMail: TIntegerField;
    QrControleEntiTel: TIntegerField;
    QrControleEntiTipCto: TIntegerField;
    QrControleEntiRespon: TIntegerField;
    QrControleEntiCfgRel: TIntegerField;
    QrControleNFeMPInn: TIntegerField;
    QrControleNFeMPIts: TIntegerField;
    QrControleCMPTInn: TIntegerField;
    QrControleCMPTOut: TIntegerField;
    QrControleCMPTOutIts: TIntegerField;
    QrControlePQRet: TIntegerField;
    QrControlePQRetIts: TIntegerField;
    QrControlePQ_StqCenCad: TIntegerField;
    QrControleMP_StqCenCad: TIntegerField;
    QrControleCustomFR3: TIntegerField;
    QrControleContasLnkIts: TIntegerField;
    QrControleEventosCad: TSmallintField;
    QrControleGraGru1: TIntegerField;
    QrControleGraGru2: TIntegerField;
    QrControleGraGru3: TIntegerField;
    QrControleGraGruAtr: TIntegerField;
    QrControleGraGruC: TIntegerField;
    QrControleGraGruVal: TIntegerField;
    QrControleGraGruX: TIntegerField;
    QrControleGraMatIts: TIntegerField;
    QrControleGraSrv1: TIntegerField;
    QrControleGraTamCad: TIntegerField;
    QrControleGraTamIts: TIntegerField;
    QrControleEstqCen: TIntegerField;
    QrControleEtqGeraLot: TIntegerField;
    QrControleEtqPrinCad: TIntegerField;
    QrControleEtqPrinCmd: TIntegerField;
    QrControleFatPedCab: TIntegerField;
    QrControleFatPedVol: TIntegerField;
    QrControleFisAgrCad: TIntegerField;
    QrControleFisRegCad: TIntegerField;
    QrControleFisRegMvt: TIntegerField;
    QrControleGeosite: TIntegerField;
    QrControleGraAtrCad: TIntegerField;
    QrControleGraAtrIts: TIntegerField;
    QrControleGraCorCad: TIntegerField;
    QrControleGraCorFam: TIntegerField;
    QrControleGraCusPrc: TIntegerField;
    QrControleGraCusPrcU: TIntegerField;
    QrControleMedOrdem: TIntegerField;
    QrControleMotivos: TIntegerField;
    QrControlePediAccTab: TIntegerField;
    QrControlePediVda: TIntegerField;
    QrControlePediVdaIts: TIntegerField;
    QrControlePediVdaCuz: TIntegerField;
    QrControlePediPrzCab: TIntegerField;
    QrControlePediPrzIts: TIntegerField;
    QrControlePediPrzEmp: TIntegerField;
    QrControlePrdGrupAtr: TIntegerField;
    QrControlePrdGrupJan: TIntegerField;
    QrControlePrdGrupTip: TIntegerField;
    QrControleRegioes: TIntegerField;
    QrControleRegioesIts: TIntegerField;
    QrControleFatConCad: TIntegerField;
    QrControleFatConIts: TIntegerField;
    QrControleFatConRet: TIntegerField;
    QrControleStqManCad: TIntegerField;
    QrControleStqManIts: TIntegerField;
    QrControleStqBalCad: TIntegerField;
    QrControleStqBalIts: TIntegerField;
    QrControleStqCenCad: TIntegerField;
    QrControleStqCenLoc: TIntegerField;
    QrControleStqMovItsA: TIntegerField;
    QrControleStqMovNFsA: TIntegerField;
    QrControleStqMovValA: TIntegerField;
    QrControleStqMovUsu: TIntegerField;
    QrControleStqMov001: TIntegerField;
    QrControleStqMov002: TIntegerField;
    QrControleStqMov003: TIntegerField;
    QrControleStqMov004: TIntegerField;
    QrControleStqMov099: TIntegerField;
    QrControleStqMov102: TIntegerField;
    QrControleStqMov103: TIntegerField;
    QrControleTabePrcCab: TIntegerField;
    QrControleTabePrcGrI: TIntegerField;
    QrControleTabePrcPzG: TIntegerField;
    QrControleUnidMed: TIntegerField;
    QrControleAnotacoes: TIntegerField;
    QrControleSMIMultIns: TIntegerField;
    QrControleIDCtrl_NFe: TIntegerField;
    QrControleCMPPVai: TIntegerField;
    QrControleCMPPVaiIts: TIntegerField;
    QrControleCMPPVem: TIntegerField;
    QrControleStqInnCad: TIntegerField;
    QrControleImprimeEmpr: TIntegerField;
    QrControleGrupoBal: TIntegerField;
    QrControleGraGruCST: TIntegerField;
    QrControleFatVenCab: TIntegerField;
    QrControleFatVenVol: TIntegerField;
    QrControleFatVenIts: TIntegerField;
    QrControleFatVenCuz: TIntegerField;
    QrControleCorrGrad01: TSmallintField;
    QrControleCorrGrad02: TSmallintField;
    QrControleCartTalCH: TIntegerField;
    QrControleFabricas: TIntegerField;
    QrControleClasFisc: TIntegerField;
    QrControleMPSubProd: TIntegerField;
    QrControleXcelCfgIts: TIntegerField;
    QrControleXcelCfgCab: TIntegerField;
    QrControlePlanRelCab: TIntegerField;
    QrControlePlanRelIts: TIntegerField;
    QrControleEntiSrvPro: TIntegerField;
    QrControleGraFabCad: TIntegerField;
    QrControleGraFabMar: TIntegerField;
    QrControleGraGru4: TIntegerField;
    QrControleGraGru5: TIntegerField;
    QrControleEntiImgs: TIntegerField;
    QrControleEmitFlu: TIntegerField;
    QrControleEmailConta: TIntegerField;
    QrControleWBNivGer: TSmallintField;
    QrControleOSImpInfWB: TSmallintField;
    QrControleUsaBRLMedM2: TSmallintField;
    QrControleUsaM2Medio: TSmallintField;
    QrControleUsaEmitGru: TSmallintField;
    QrControleCNAB_Cfg: TIntegerField;
    QrControleCNAB_Lei: TIntegerField;
    QrControleCNAB_Lot: TIntegerField;
    QrControleAtzEntiContat: TSmallintField;
    QrControleCambioMdaAtz: TSmallintField;
    QrControleMPVImpOpc: TIntegerField;
    QrControleVerImprRecRib: TIntegerField;
    QrControleCanAltBalVS: TSmallintField;
    QrControleCorrigeFisRegMvt: TIntegerField;
    QrControleInfoMulFrnImpVS: TSmallintField;
    QrControleQtdBoxClas: TSmallintField;
    QrControleVSPwdDdData: TDateField;
    QrControleVSPwdDdUser: TIntegerField;
    QrControleCons: TIntegerField;
    QrControleConsPrc: TIntegerField;
    QrControleLeiGer: TIntegerField;
    QrControleLeiGerIts: TIntegerField;
    QrControleFatPedFis: TIntegerField;
    QrControleEntiAssina: TIntegerField;
    QrControleEntiGruIts: TIntegerField;
    QrControleNObrigNFeVS: TIntegerField;
    QrControleVSWarSemNF: TSmallintField;
    QrControleVSWarNoFrn: TSmallintField;
    QrControlePediPrzEnt: TIntegerField;
    QrControleSPED_II_PDA: TSmallintField;
    QrControleSPED_II_EmCal: TSmallintField;
    QrControleSPED_II_Caleado: TSmallintField;
    QrControleSPED_II_DTA: TSmallintField;
    QrControleSPED_II_EmCur: TSmallintField;
    QrControleSPED_II_Curtido: TSmallintField;
    QrControleSPED_II_ArtCur: TSmallintField;
    QrControleSPED_II_Operacao: TSmallintField;
    QrControleSPED_II_Recurt: TSmallintField;
    QrControleSPED_II_SubProd: TSmallintField;
    QrControleSPED_II_Reparo: TSmallintField;
    QrControleVSInsPalManu: TSmallintField;
    QrControleImpRecAcabto: TSmallintField;
    QrControleAWServerID: TIntegerField;
    QrControleAWStatSinc: TSmallintField;
    QrControlePQD: TIntegerField;
    QrControleVSImpMediaPall: TSmallintField;
    QrControleCFiscalPadr: TWideStringField;
    QrControleSitTribPadr: TWideStringField;
    QrControleCFOPPadr: TWideStringField;
    QrControleCidade: TWideStringField;
    QrControleCNPJ: TWideStringField;
    QrControleSoMaiusculas: TWideStringField;
    QrControleMoeda: TWideStringField;
    QrControleContraSenha: TWideStringField;
    QrControlePadrPlacaCar: TWideStringField;
    QrControleServSMTP: TWideStringField;
    QrControleNomeMailOC: TWideStringField;
    QrControleDonoMailOC: TWideStringField;
    QrControleMailOC: TWideStringField;
    QrControleCorpoMailOC: TWideMemoField;
    QrControleConexaoDialUp: TWideStringField;
    QrControleMailCCCega: TWideStringField;
    QrControleFormFundoBMP: TWideStringField;
    QrControlePronomeE: TWideStringField;
    QrControlePronomeM: TWideStringField;
    QrControlePronomeF: TWideStringField;
    QrControlePronomeA: TWideStringField;
    QrControleSaudacaoE: TWideStringField;
    QrControleSaudacaoM: TWideStringField;
    QrControleSaudacaoF: TWideStringField;
    QrControleSaudacaoA: TWideStringField;
    QrControleMeuLogoPath: TWideStringField;
    QrControleLogoNF: TWideStringField;
    QrControlePathLogoDupl: TWideStringField;
    QrControlePathLogoRece: TWideStringField;
    QrControleLogoBig1: TWideStringField;
    QrControleSecuritStr: TWideStringField;
    QrControleVSPwdDdClas: TWideStringField;
    QrControleVSImpRandStr: TWideStringField;
    QrControleDBClarecoCDR: TWideStringField;
    QrMasterCNPJ: TWideStringField;
    QrMasterEm: TWideStringField;
    Qr3PQCustoPadrao: TFloatField;
    QrControleM2DiaAcabPCP: TFloatField;
    QrSumAreas: TMySQLQuery;
    QrSumAreasArM2ComRend: TFloatField;
    QrSumAreasArP2ComRend: TFloatField;
    QrSumAreasPesoComRend: TFloatField;
    QrAreas: TMySQLQuery;
    QrAreasPeso: TFloatField;
    QrAreasTipo: TFloatField;
    QrAreasCodigo: TIntegerField;
    QrAreasData: TDateTimeField;
    QrAreasTipoTXT: TWideStringField;
    QrAreasVSMovCod: TIntegerField;
    QrAreasAreaM2: TFloatField;
    QrAreasPecas: TFloatField;
    QrAreasAreaPeso: TFloatField;
    QrAreasPesoComRend: TFloatField;
    QrAreasArM2ComRend: TFloatField;
    QrAreasArP2ComRend: TFloatField;
    QrControleCtaComisCMP: TIntegerField;
    QrEmitSbtQtd: TMySQLQuery;
    QrEmitSbtQtdCouIntros: TFloatField;
    QrEmitSbtQtdAreaM2: TFloatField;
    QrEmitSbtQtdAreaP2: TFloatField;
    QrEmitSbtQtdPesoKg: TFloatField;
    QrEmitSbtQtdfUmiKg: TFloatField;
    QrEmitSbtQtdLinRebMed: TFloatField;
    QrEmitSbtQtdMedLinWB: TFloatField;
    QrEmitSbtQtdMedLinTrpFator: TFloatField;
    QrEmitSbtQtdMinPrcRend: TFloatField;
    QrEmitSbtQtdfator: TFloatField;
    QrSumUsoSbt: TMySQLQuery;
    QrSumUsoSbtCouIntros: TFloatField;
    QrSumUsoSbtAreaM2: TFloatField;
    QrSumUsoSbtAreaP2: TFloatField;
    QrSumUsoSbtPesoKg: TFloatField;
    QrMedUsoSbt: TMySQLQuery;
    QrMedUsoSbtCouIntros: TFloatField;
    QrMedUsoSbtAreaM2: TFloatField;
    QrMedUsoSbtAreaP2: TFloatField;
    QrMedUsoSbtPesoKg: TFloatField;
    QrMedUsoSbtPercEsperRend: TFloatField;
    QrUpdM: TMySQLQuery;
    QrControleNaoRecupImposPQ: TSmallintField;
    QrControleCreTrib_MP_CFOP_DE: TIntegerField;
    QrControleCreTrib_MP_CFOP_FE: TIntegerField;
    QrControleCreTrib_MP_ICMS_CST: TWideStringField;
    QrControleCreTrib_MP_IPI_CST: TWideStringField;
    QrControleCreTrib_MP_PIS_CST: TWideStringField;
    QrControleCreTrib_MP_COFINS_CST: TWideStringField;
    QrControleCreTrib_MP_ICMS_ALIQ: TFloatField;
    QrControleCreTrib_MP_ICMS_ALIQ_ST: TFloatField;
    QrControleCreTrib_MP_IPI_ALIQ: TFloatField;
    QrControleCreTrib_MP_PIS_ALIQ: TFloatField;
    QrControleCreTrib_MP_COFINS_ALIQ: TFloatField;
    QrControleRemTrib_MP_CFOP_DE: TIntegerField;
    QrControleRemTrib_MP_CFOP_FE: TIntegerField;
    QrControleRemTrib_MP_ICMS_CST: TWideStringField;
    QrControleRemTrib_MP_IPI_CST: TWideStringField;
    QrControleRemTrib_MP_PIS_CST: TWideStringField;
    QrControleRemTrib_MP_COFINS_CST: TWideStringField;
    QrControleRemTrib_MP_ICMS_ALIQ: TFloatField;
    QrControleRemTrib_MP_ICMS_ALIQ_ST: TFloatField;
    QrControleRemTrib_MP_IPI_ALIQ: TFloatField;
    QrControleRemTrib_MP_PIS_ALIQ: TFloatField;
    QrControleRemTrib_MP_COFINS_ALIQ: TFloatField;
    QrControleCreTrib_MP_TES_ICMS: TSmallintField;
    QrControleCreTrib_MP_TES_IPI: TSmallintField;
    QrControleCreTrib_MP_TES_PIS: TSmallintField;
    QrControleCreTrib_MP_TES_COFINS: TSmallintField;
    QrControleStqCenCadEstqPall: TIntegerField;
    MyDB: TMySQLDatabase;
    QrControleGruUsrEdtRecei: TSmallintField;
    QrDuplCli: TMySQLQuery;
    QrLocGGX: TMySQLQuery;
    QrLocGGXControle: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure TbMovCalcFields(DataSet: TDataSet);
    procedure TbMovBeforePost(DataSet: TDataSet);
    procedure TbMovBeforeDelete(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QrEndereco_CalcFields(DataSet: TDataSet);
    procedure QrControleAfterOpen(DataSet: TDataSet);
    procedure QrDono_CalcFields(DataSet: TDataSet);
    procedure MyDBAfterConnect(Sender: TObject);
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo
    //
  public
    { Public declarations }
    FFmtPrc, FProdDelete: Integer;
    FStrFmtPrc, FStrFmtCus: String;
    (* 23/02/2015 => Aparentemente n�o usa
    function Privilegios(Usuario : Integer) : Boolean;
    *)
    //
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
    //
    function  TabelasQueNaoQueroCriar(): String;
    procedure VerificaSenha(Index: Integer; Login, Senha: String);

    function ConcatenaRegistros(Matricula: Integer): String;
    function ObtemIP(Retorno: Integer): String;
    procedure DefineUserSets_Painel(NomeForm, Definicao: String;
              Painel: TPanel);

    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //procedure RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
    procedure AtualizaEstoqueCMPT(CMPTInn, CMPTOut: Integer);
    procedure AtualizaEstoqueCECT(CECTInn, CECTOut: Integer);

    function AtualizaEstoqueMarca(Controle: Integer): Boolean;
    //procedure RecalculaCustoPQdeMP(MPInControle: Integer);

    procedure InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
              DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
              CliInt: Integer; Campos: String; DescrSubstit: Boolean);
    function AtualizaCustosEmit_1(Emit: Integer): Boolean;
    function AtualizaCustosEmit_2(Emit: Integer): Boolean;
    function AtualizaMPIn(Controle: Integer): Boolean;
    //
    procedure DesfazBalancoGrade();
    procedure AtualizaStqMovIts_PQX(PB: TProgressBar; LaAviso1, LaAviso2: TLabel); // PQX = PQ
    procedure AtualizaStqMovIts_MPI(PB: TProgressBar); // MPI = Materia-prima Blue derm
    procedure AtualizaStqMovIts_CWB(); // CWB = Classifica��o de Wet Blue
    function CentroDeEstoqueDefinido(): Boolean;
    procedure AtualizaPrecosGraGruVal1(PQ: Integer);
    procedure AtualizaPrecosGraGruVal1_All(PB: TProgressBar);
    function ObtemGraGruXDeCouro(Tipificacao, Animal: Integer): Integer;
    function ObtemGraGruXDeGraGru1(GraGru1: Integer): Integer;
    procedure ExcluiItensEstqNFsCouroImportadas();
    //
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
    //
(*
    procedure AlteraSMIA(Qry: TmySQLQuery);
    procedure GeraEstoqueEm_2(Empresa_Txt: String; DataIni, DataFim: TDateTime;
              PGT, GG1, GG2, GG3, TipoPesq: Integer; GraCusPrc, TabePrcCab:
              Variant; GraCusPrcCodigo, TabePrcCabCodigo, PrdGrupTipCodigo,
              GraGru1Nivel1, GraGru2Nivel2, GraGru3Nivel3: Integer;
              QrPGT_SCC, QrAbertura2a, QrAbertura2b: TmySQLQuery;
              UsaTabePrcCab, SoPositivos: Boolean; LaAviso: TLabel;
              NomeTb_SMIC: String; QrSMIC_X: TmySQLQuery;
              var TxtSQL_Part1, TxtSQL_Part2: String);
*)
    function  ReopenSMI(OriCodi, OriCtrl: Integer): Boolean;
    procedure ReopenControle();
    function  ReopenParamsEspecificos(Empresa: Integer): Boolean;
    //
    procedure AtualizaTotaisWBXxxCab(Tabela: String; MovimCod: Integer);
    procedure AtualizaSaldoVirtualWBMovIts(Controle: Integer);
    procedure InsereWBMovCab(Codigo: Integer; MovimID: TEstqMovimID;
              CodigoID: Integer);
    function  InsUpdWBMovIts(SQLType: TSQLType; Codigo, MovimCod, MovimTwn, Empresa,
              Terceiro: Integer; MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv;
              Pallet, GraGruX: Integer;
              Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double; DataHora: String;
              SrcMovID, SrcNivel1, SrcNivel2: Integer;
              Observ: String; LnkNivXtr1, LnkNivXtr2, CliVenda, Controle:
              Integer): Boolean;
    function  ExcluiMovimCodWBMovIts(Tabela: TmySQLQuery;
              Campo: TIntegerField; MovimCod, Controle, SrcNivel2: Integer): Boolean;
    function  ExcluiControleWBMovIts(Tabela: TmySQLQuery;
              Campo: TIntegerField; Controle, SrcNivel2: Integer): Boolean;
    function  WBFic(GraGruX, Empresa, Fornecedor, Pallet: Integer;
              Pecas, AreaM2, PesoKg, ValorT: Double;
              EdGraGruX, EdPallet, EdPecas, EdAreaM2, EdValorT: TWinControl): Boolean;
    function  SelecionaOSAberta(): Integer;
    function  M2MedioWBFornecedor(GraGruX, Fornece: Integer): Double;
    function  BRLMedioWB(GraGruX: Integer): Double;
    //
    function  PreparaMoviPQ_EstqEm_v2014(Tabela : TNomeTabRecriaTempTable;
              QrMoviPQ: TmySQLQuery; CI, PQ: Integer;
              DtIni, DtFim: TDateTime; NomeCI, _WHERE, ORDERBY: String;
              LaAviso1, LaAviso2: TLabel; PBx: TProgressBar): Boolean;
    function  PreparaMoviPQ_EstqEm_v2018(Tabela : TNomeTabRecriaTempTable;
              QrMoviPQ: TmySQLQuery; CI, PQ: Integer;
              DtIni, DtFim: TDateTime; NomeCI, _WHERE, ORDERBY: String;
              LaAviso1, LaAviso2: TLabel; PBx: TProgressBar): Boolean;
    procedure ReopenLotes(Codigo: Integer);
    function  ObrigaInformarEmitGru(Obriga: Boolean; EmitGru: Integer;
              EdEmitGru: TdmkEditCB): Boolean;
////////////// RETORNO DE PQ DE PRESTACAO DE SERVICO////////////////////////////
    procedure PQR_AtualizaRetQtdInn(OriCodiInn, OriCtrlInn, TipoInn: Integer);
              //Peso: Double);
    procedure PQR_AtualizaRetQtdOut(OriCodiOut, OriCtrlOut, TipoOut: Integer);
              //Peso: Double);
    function  PQR_InsereItemPQRIts(Codigo, OriCodiInn, OriCtrlInn, TipoInn,
              OriCodiOut, OriCtrlOut, TipoOut, Insumo: Integer;
              Peso, Valor: Double; Ocorrencia: Integer): Integer;
    procedure PQR_AtualizaPQx(OrigemCodi, OrigemCtrl, Tipo: Integer;
              RetQtd: Double);
    function  ImpedePeloRetornoPQ(OrigemCodi, OrigemCtrl, Tipo: Integer;
              Avisa: Boolean): Boolean;
////////////////////////////////////////////////////////////////////////////////

/// RENDIMENTO DE SEMI ACABADO /////////////////////////////////////////////////
    procedure AualizaTotaiEmiGru(Codigo: Integer);
////////////////////////////////////////////////////////////////////////////////

  end;

var
  Dmod: TDmod;
  FVerifi: Boolean;
  //
  QvEntra3: TmySQLQuery;
  QvVenda3: TmySQLQuery;
  QvDevol3: TmySQLQuery;
  QvRecom3: TmySQLQuery;


implementation

uses UnMyObjects, Principal, SenhaBoss, Servidor, MyDBCheck, InstallMySQL41,
  BlueDerm_Dmk, ModuleGeral, ModProd, UnGrade_Tabs, PQx, CECTInn, ModuleFin,
  SMI_Edita, UnLic_Dmk, FormulasImpEdit, VSPalletAdd, GraGruXSelToY, UnALL_Jan,
  UMySQLDB, VerificaConexoes;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TDmod.DefParams;
begin
  VAR_GOTOTABELA := CO_CARTEIRAS;
  VAR_GOTOMYSQLTABLE := QrCarteiras;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := 'Tipo='+IntToStr(VAR_CARTEIRA);

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT DISTINCT ca.*,');
  VAR_SQLx.Add('CASE WHEN ba.Codigo=0 THEN "" ELSE ba.Nome END NOMEDOBANCO,');
  VAR_SQLx.Add('CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ELSE fo.Nome END NOMEFORNECEI');
  VAR_SQLx.Add('FROM carteiras ca');
  VAR_SQLx.Add('LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco');
  VAR_SQLx.Add('LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI');
  VAR_SQLx.Add('WHERE ca.Codigo>0');
  //
  VAR_SQL1.Add('AND ca.Codigo=:P0');
  //
  VAR_SQLa.Add('AND ca.Nome Like :P0');

end;

procedure TDmod.DesfazBalancoGrade();
var
  Data: String;
begin
  Data := dmkPF.PrimeiroDiaAposPeriodo(UnPQx.VerificaBalanco, dtSystem);
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('REPLACE INTO stqmovitsa');
  Dmod.QrUpd.SQL.Add('SELECT smib.*');
  Dmod.QrUpd.SQL.Add('FROM stqmovitsb smib');
  Dmod.QrUpd.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=smib.GraGruX');
  Dmod.QrUpd.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
  Dmod.QrUpd.SQL.Add('WHERE gg1.PrdGrupTip=-2');
  Dmod.QrUpd.SQL.Add('AND smib.DataHora >= :P0');
  Dmod.QrUpd.Params[0].AsString := Data;
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE smib ');
  Dmod.QrUpd.SQL.Add('FROM stqmovitsb smib, gragrux ggx, gragru1 gg1');
  Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smib.GraGruX');
  Dmod.QrUpd.SQL.Add('AND gg1.Nivel1=ggx.GraGru1');
  Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
  Dmod.QrUpd.SQL.Add('AND smib.DataHora >= :P0');
  Dmod.QrUpd.Params[0].AsString := Data;
  Dmod.QrUpd.ExecSQL;
  //
end;

{
////////////////////////////////////////////////////////////////////////////////

  Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!

////////////////////////////////////////////////////////////////////////////////
function TDmod.EhReceita_GGX(): TFormulaGGX;
var
  EhGGX: Boolean;
begin
  Result := fggxNone;
  QrFormulas_EhGGX.Close;
  UnDmkDAC_PF.AbreQuery(QrFormulas_EhGGX, MyDB);
  //
  case QrFormulas_EhGGX.RecordCount of
    0: EhGGX := True;
    1: EhGGX := QrFormulas_EhGGXEhGGX.Value = 1;
    else
    begin
      Geral.MB_Aviso(
      'Abertura de janela cancelada! Termine a convers�o de receitas para ter acesso!');
      Exit;
    end;
  end;
  if EhGGX then
    Result := fggxGGX
  else
    Result := fggxPQ;
  begin
end;
}

function TDmod.ExcluiMovimCodWBMovIts(Tabela: TmySQLQuery;
 Campo: TIntegerField; MovimCod, Controle, SrcNivel2: Integer): Boolean;
var
  Prox: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Self);
  try
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
    'wbmovits', 'MovimCod', MovimCod, Dmod.MyDB) = ID_YES then
    begin
      AtualizaSaldoVirtualWBMovIts(SrcNivel2);
      if Tabela <> nil then
      begin
        Prox := GOTOy.LocalizaPriorNextIntQr(Tabela, Campo, Controle);
        Tabela.Close;
        UnDmkDAC_PF.AbreQuery(Tabela, MyDB);
        Tabela.Locate('Controle', Prox, []);
      end;
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

function TDmod.ExcluiControleWBMovIts(Tabela: TmySQLQuery; Campo: TIntegerField;
  Controle, SrcNivel2: Integer): Boolean;
var
  Prox: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Self);
  try
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
    'wbmovits', 'Controle', Controle, Dmod.MyDB) = ID_YES then
    begin
      AtualizaSaldoVirtualWBMovIts(SrcNivel2);
      if Tabela <> nil then
      begin
        Prox := GOTOy.LocalizaPriorNextIntQr(Tabela, Campo, Controle);
        Tabela.Close;
        UnDmkDAC_PF.AbreQuery(Tabela, MyDB);
        Tabela.Locate('Controle', Prox, []);
      end;
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TDmod.ExcluiItensEstqNFsCouroImportadas();
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM stqmovitsa ');
    Dmod.QrUpdM.SQL.Add('WHERE GraGruX < 0 ');
    Dmod.QrUpdM.SQL.Add('AND GraGruX > -90000');
    Dmod.QrUpdM.SQL.Add('AND Tipo IN (51,151)');
    Dmod.QrUpdM.ExecSQL;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

{
procedure TDmod.GeraEstoqueEm_2(Empresa_Txt: String; DataIni, DataFim: TDateTime;
              PGT, GG1, GG2, GG3, TipoPesq: Integer; GraCusPrc, TabePrcCab:
              Variant; GraCusPrcCodigo, TabePrcCabCodigo, PrdGrupTipCodigo,
              GraGru1Nivel1, GraGru2Nivel2, GraGru3Nivel3: Integer;
              QrPGT_SCC, QrAbertura2a, QrAbertura2b: TmySQLQuery;
              UsaTabePrcCab, SoPositivos: Boolean; LaAviso: TLabel;
              NomeTb_SMIC: String; QrSMIC_X: TmySQLQuery;
              var TxtSQL_Part1, TxtSQL_Part2: String);
var
  PrdGrupTip_Txt, StqCenCad_Txt, DiaSeguinte, GrupoBal_Txt,
  GraCusPrc_Txt, TabePrcCab_Txt, TextoA, TextoB, TextoC, CliInt_Txt,
  EntiSitio_Txt: String;
begin
  Screen.Cursor := crHourGlass;
  try
    //
    //DiaSeguinte := FormatDateTime('yyyy-mm-dd', TPDataFim2.Date + 1);
    DiaSeguinte := FormatDateTime('yyyy-mm-dd', DataFim + 1);
    if TipoPesq = 1 then
    begin
      if GraCusPrc <> NULL then
        GraCusPrc_Txt := FormatFloat('0', GraCusPrcCodigo)
      else // N�o pode ser nulo!
        GraCusPrc_Txt := '0';
      //
    end;
    if TabePrcCab <> NULL then
      TabePrcCab_Txt := FormatFloat('0', TabePrcCabCodigo)
    else // N�o pode ser nulo?
      TabePrcCab_Txt := '0';
    //
    MyObjects.Informa(LaAviso, True, 'Tabela temporaria - Excluindo caso exista');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('DROP TABLE IF EXISTS stqmovitsc;');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso, True, 'Tabela temporaria - Adicionando dados do arquivo morto');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('CREATE TABLE stqmovitsc');
    DmodG.QrUpdPID1.SQL.Add('SELECT smi.*');
    DmodG.QrUpdPID1.SQL.Add('FROM '+TMeuDB+'.stqmovitsb smi');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smi.GraGruX');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
    DmodG.QrUpdPID1.SQL.Add('WHERE '(*NOT *)+'gg1.PrdGrupTip IN');
    DmodG.QrUpdPID1.SQL.Add('(');
    DmodG.QrUpdPID1.SQL.Add('  SELECT DISTINCT prdgruptip');
    DmodG.QrUpdPID1.SQL.Add('  FROM '+TMeuDB+'.stqbalcad');
    DmodG.QrUpdPID1.SQL.Add('  WHERE Abertura >= "' + DiaSeguinte + '"');
    DmodG.QrUpdPID1.SQL.Add(')');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso, True, 'Tabela temporaria - Adicionando dados do arquivo ativo');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO stqmovitsc');
    DmodG.QrUpdPID1.SQL.Add('SELECT * FROM '+TMeuDB+'.stqmovitsa');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso, True, 'Tipos de Grupos de Produtos - abrindo tabela');
    QrPGT_SCC.Close;
    QrPGT_SCC.SQL.Clear;
    case TipoPesq of
      1:
      begin
        QrPGT_SCC.SQL.Add('SELECT DISTINCT gg1.PrdGrupTip, smic.StqCenCad, ');
        QrPGT_SCC.SQL.Add('pgt.LstPrcFisc, ' + Empresa_Txt + ' Empresa, ' + '0' + ' EntiSitio');
        QrPGT_SCC.SQL.Add('FROM stqmovitsc smic');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smic.GraGruX');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
        QrPGT_SCC.SQL.Add('WHERE GraGruX <> 0');
        QrPGT_SCC.SQL.Add('AND Baixa = 1');
        QrPGT_SCC.SQL.Add('AND smic.Empresa=' + Empresa_Txt);
        if GG1 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', GraGru1Nivel1))
        else
        if GG2 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel2=' + FormatFloat('0', GraGru2Nivel2))
        else
        if GG3 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel3=' + FormatFloat('0', GraGru3Nivel3))
        else
        if PGT <> 0 then
          QrPGT_SCC.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', PrdGrupTipCodigo));
        QrPGT_SCC.SQL.Add('ORDER BY gg1.PrdGrupTip, smic.StqCenCad');//, smic.GraGruX');
      end;
      2:
      begin
        QrPGT_SCC.SQL.Add('SELECT DISTINCT gg1.PrdGrupTip, smic.StqCenCad,');
        QrPGT_SCC.SQL.Add('pgt.LstPrcFisc, FLOOR(smic.Empresa + 0.1) Empresa, FLOOR(scc.EntiSitio + 0.1) EntiSitio');
        QrPGT_SCC.SQL.Add('FROM stqmovitsc smic');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smic.GraGruX');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smic.StqCenCad');
        QrPGT_SCC.SQL.Add('WHERE (smic.Empresa=' + Empresa_Txt + ' OR scc.EntiSitio=' + Empresa_Txt + ')');
        QrPGT_SCC.SQL.Add('AND GraGruX <> 0');
        QrPGT_SCC.SQL.Add('AND Baixa = 1');
        if GG1 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', GraGru1Nivel1))
        else
        if GG2 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel2=' + FormatFloat('0', GraGru2Nivel2))
        else
        if GG3 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel3=' + FormatFloat('0', GraGru3Nivel3))
        else
        if PGT <> 0 then
          QrPGT_SCC.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', PrdGrupTipCodigo));
        QrPGT_SCC.SQL.Add('ORDER BY gg1.PrdGrupTip, smic.StqCenCad');
      end;
    end;
    UnDmkDAC_PF.AbreQuery(QrPGT_SCC, MyDB);
    //
    TextoA     := '';
    TextoB     := '';
    TextoC     := '';
    // Caso n�o ache nada!
    if QrPGT_SCC.RecordCount = 0 then
    begin
      QrSMIC_X.Close;
      QrSMIC_X.SQL.Clear;
      QrSMIC_X.SQL.Add('DELETE FROM ' + NomeTb_SMIC + ';');
      QrSMIC_X.ExecSQL;
    end;
    //
    while not QrPGT_SCC.Eof do
    begin
      CliInt_Txt    := FormatFloat('0', QrPGT_SCC.FieldByName('Empresa').AsInteger);
      EntiSitio_Txt := FormatFloat('0', QrPGT_SCC.FieldByName('EntiSitio').AsInteger);
      if TipoPesq = 2 then
      begin
        GraCusPrc_Txt := FormatFloat('0', QrPGT_SCC.FieldByName('LstPrcFisc').AsInteger)
      end;
      PrdGrupTip_Txt := FormatFloat('0', QrPGT_SCC.FieldByName('PrdGrupTip').AsInteger);
      StqCenCad_Txt  := FormatFloat('0', QrPGT_SCC.FieldByName('StqCenCad').AsInteger);
      TextoA := 'Tipo Prod.: ' + PrdGrupTip_Txt + '  -  ' +
                'Centro estq: ' + StqCenCad_Txt + '  -  ' +
                'Sitio: ' + EntiSitio_Txt + '  -  ' +
                'Empresa: ' + CliInt_Txt;
      TextoB := '';
      TextoC := '';
      MyObjects.Informa(LaAviso, False, TextoA + TextoB + TextoC);
      //
      QrAbertura2b.Close;
      QrAbertura2b.SQL.Clear;
      QrAbertura2b.SQL.Add('SELECT Abertura');
      QrAbertura2b.SQL.Add('FROM stqbalcad');
      QrAbertura2b.SQL.Add('WHERE PrdGrupTip=' + PrdGrupTip_Txt);
      QrAbertura2b.SQL.Add('AND StqCenCad=' + StqCenCad_Txt);
      QrAbertura2b.SQL.Add('AND Abertura >= "' + DiaSeguinte + '"');
      QrAbertura2b.SQL.Add('AND Empresa=' + Empresa_Txt);
      QrAbertura2b.SQL.Add('ORDER BY Abertura DESC');
      QrAbertura2b.SQL.Add('LIMIT 1');
      UnDmkDAC_PF.AbreQuery(QrAbertura2b, MyDB);
      if QrAbertura2b.RecordCount > 0 then
      begin
        QrAbertura2a.Close;
        QrAbertura2a.SQL.Clear;
        QrAbertura2a.SQL.Add('SELECT Abertura, GrupoBal');
        QrAbertura2a.SQL.Add('FROM stqbalcad');
        QrAbertura2a.SQL.Add('WHERE PrdGrupTip=' + PrdGrupTip_Txt);
        QrAbertura2a.SQL.Add('AND StqCenCad=' + StqCenCad_Txt);
        QrAbertura2a.SQL.Add('AND Abertura < "' + DiaSeguinte + '"');
        QrAbertura2a.SQL.Add('AND Empresa=' + Empresa_Txt);
        QrAbertura2a.SQL.Add('ORDER BY Abertura DESC');
        QrAbertura2a.SQL.Add('LIMIT 1');
        UnDmkDAC_PF.AbreQuery(QrAbertura2a, MyDB);
        //
        //if QrAbertura2aAbertura.Value > Int(TPDataFim2.Date) + 1 then
        if QrAbertura2a.FieldByName('Abertura').AsDateTime > Int(DataFim) + 1 then
          GrupoBal_Txt := FormatFloat('0', QrAbertura2a.FieldByName('GrupoBal').AsInteger)
        else
          GrupoBal_Txt := '0';
      end else GrupoBal_Txt := '0';
      //
      QrSMIC_X.Close;
      QrSMIC_X.SQL.Clear;
      if QrPGT_SCC.RecNo = 1 then
      begin
        QrSMIC_X.SQL.Add('DROP TABLE IF EXISTS ' + NomeTb_SMIC + ';');
        QrSMIC_X.SQL.Add('CREATE TABLE ' + NomeTb_SMIC);
      end else begin
        QrSMIC_X.SQL.Add('INSERT INTO ' + NomeTb_SMIC);
      end;
      QrSMIC_X.SQL.Add('SELECT smic.IDCtrl My_Idx, gg1.Nivel1, ');
      QrSMIC_X.SQL.Add('gg1.Nome NO_PRD, gg1.PrdGrupTip, gg1.UnidMed, ');
      QrSMIC_X.SQL.Add('gg1.NCM, pgt.Nome NO_PGT, ');
      QrSMIC_X.SQL.Add('med.Sigla SIGLA, gti.PrintTam, gti.Nome NO_TAM, ');
      QrSMIC_X.SQL.Add('gcc.PrintCor, gcc.Nome NO_COR, ggc.GraCorCad, ');
      QrSMIC_X.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, ');
      QrSMIC_X.SQL.Add('scc.SitProd, scc.EntiSitio,');
      QrSMIC_X.SQL.Add('smic.GraGruX, smic.Empresa, smic.StqCenCad,');
      QrSMIC_X.SQL.Add('SUM(smic.Qtde * smic.Baixa) QTDE, SUM(smic.Pecas) PECAS,');
      QrSMIC_X.SQL.Add('SUM(smic.Peso) PESO, SUM(smic.AreaM2) AREAM2,');
      QrSMIC_X.SQL.Add('SUM(smic.AreaP2) AREAP2,');
      if UsaTabePrcCab then
      begin
        // Por tabela de pre�o de GraGru1 (TPC_GCP_ID = 2) ou de GraGruX (TPC_GCP_ID = 3)
        QrSMIC_X.SQL.Add('TRUNCATE(IF(tpi.Preco IS NULL, 2, 3), 0) TPC_GCP_ID,');
        QrSMIC_X.SQL.Add('IF(tpi.Preco IS NULL, tpc.Codigo, tpi.TabePrcCab) TPC_GCP_TAB,');
        QrSMIC_X.SQL.Add('IF(tpi.Preco IS NULL, TRUNCATE(tpc.Preco, 6), TRUNCATE(tpi.Preco, 6)) PrcCusUni,');
        QrSMIC_X.SQL.Add('IF(tpi.Preco IS NULL, TRUNCATE(SUM(smic.Qtde*smic.Baixa) * TRUNCATE(tpc.Preco, 6), 2),');
        QrSMIC_X.SQL.Add('TRUNCATE(SUM(smic.Qtde*smic.Baixa) * TRUNCATE(tpi.Preco, 6), 2)) ValorTot,');
        QrSMIC_X.SQL.Add('');
      end else
      begin
        // Por lista de Pre�o (TPC_GCP_ID = 1)
        QrSMIC_X.SQL.Add('TRUNCATE(1,0) TPC_GCP_ID, ggv.GraCusPrc TPC_GCP_TAB, ');
        QrSMIC_X.SQL.Add('TRUNCATE(ggv.CustoPreco, 6) PrcCusUni, ');
        QrSMIC_X.SQL.Add('TRUNCATE(SUM(smic.Qtde*smic.Baixa) * ');
        QrSMIC_X.SQL.Add('TRUNCATE(ggv.CustoPreco, 6), 2) ValorTot, ');
      end;
      QrSMIC_X.SQL.Add('1 Exportado ');
      //QrSMIC_X.SQL.Add('1 Exportado, 0 Mot_SitPrd, 0 Mot_Sitio, 0 MotQtdVal, 0 MotProduto');
      QrSMIC_X.SQL.Add('FROM stqmovitsc smic');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux    ggx ON ggx.Controle=smic.GraGruX');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gragruc    ggc ON ggc.Controle=ggx.GraGruC');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gratamits  gti ON gti.Controle=ggx.GraTamI');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.unidmed    med ON med.Codigo=gg1.UnidMed');
      if UsaTabePrcCab then
      begin
        QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.tabeprcgrg  tpc ON tpc.Nivel1=ggx.GraGru1');
        QrSMIC_X.SQL.Add('          AND tpc.Codigo=' + TabePrcCab_Txt);
        QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.tabeprcgri  tpi ON tpi.GraGruX=smic.GraGruX');
        QrSMIC_X.SQL.Add('          AND tpi.TabePrcCab=' + TabePrcCab_Txt);
      end else begin
        QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gragruval  ggv ON ggv.GraGruX=smic.GraGruX');
        // Evitar n�o encontrar registros sem pre�o!
        QrSMIC_X.SQL.Add('          AND ggv.GraCusPrc=' + GraCusPrc_Txt);
      end;
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smic.StqCenCad');
      //
      QrSMIC_X.SQL.Add('WHERE smic.Baixa> 0');
      //QrSMIC_X.SQL.Add('AND smic.GrupoBal=' + GrupoBal_Txt);
      QrSMIC_X.SQL.Add('AND smic.DataHora < "' + DiaSeguinte  + '"');
      QrSMIC_X.SQL.Add('AND smic.StqCenCad=' + StqCenCad_Txt);
      QrSMIC_X.SQL.Add('AND gg1.PrdGrupTip=' + PrdGrupTip_Txt);
      //
      QrSMIC_X.SQL.Add('AND smic.EMPRESA=' + CliInt_Txt);
      if TipoPesq = 2 then
        QrSMIC_X.SQL.Add('AND scc.EntiSitio=' + EntiSitio_Txt);
      //
      //  Filtros de pesquisa
      if GG1 <> 0 then
        QrSMIC_X.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', GraGru1Nivel1))
      else
      if GG2 <> 0 then
        QrSMIC_X.SQL.Add('AND gg1.Nivel2=' + FormatFloat('0', GraGru2Nivel2))
      else
      if GG3 <> 0 then
        QrSMIC_X.SQL.Add('AND gg1.Nivel3=' + FormatFloat('0', GraGru3Nivel3))
      else
      if PGT <> 0 then
        QrSMIC_X.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', PrdGrupTipCodigo));
      //
      //
      QrSMIC_X.SQL.Add('GROUP BY smic.GraGruX');
      //dmkPF.LeMeuTexto(QrSMIC_X.SQL.Text);
      QrSMIC_X.ExecSQL;
      //
      QrPGT_SCC.Next;
    end;
    //
    MyObjects.Informa(LaAviso, False, 'Abrindo dados gerados');
    QrSMIC_X.Close;
    QrSMIC_X.SQL.Clear;
    QrSMIC_X.SQL.Add('SELECT sm2.*, scc.SitProd,');
    QrSMIC_X.SQL.Add('ens.CNPJ ENS_CNPJ, ens.IE ENS_IE, uf1.Nome ENS_NO_UF,');
    QrSMIC_X.SQL.Add('emp.CNPJ EMP_CNPJ, emp.IE EMP_IE, uf2.Nome EMP_NO_UF');
    QrSMIC_X.SQL.Add('FROM ' + NomeTb_SMIC + ' sm2');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=sm2.StqCenCad');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades ens ON ens.Codigo=scc.EntiSitio');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades emp ON emp.Codigo=sm2.Empresa');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.ufs uf1 ON uf1.Codigo=ens.EUF');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.ufs uf2 ON uf2.Codigo=emp.EUF');
    if SoPositivos then
      QrSMIC_X.SQL.Add('WHERE sm2.QTDE >= 0.001');
    QrSMIC_X.SQL.Add('ORDER BY NO_PGT, NO_PRD');
    UnDmkDAC_PF.AbreQuery(QrSMIC_X, MyDB);
    //
    TXTSQL_Part1 := 'SELECT SUM(smic.Qtde) QtdSum,' + sLineBreak +
    'SUM(IF(smic.Qtde>0, smic.Qtde * smic.Baixa, 0)) QtdPos,' + sLineBreak +
    'SUM(IF(smic.Qtde<0, smic.Qtde * smic.Baixa, 0)) QtdNeg' + sLineBreak +
    'FROM stqmovitsc smic' + sLineBreak +
    'LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smic.GraGruX' + sLineBreak +
    'LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1' + sLineBreak +
    'LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip' + sLineBreak +
    'LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smic.StqCenCad';
    //TXTSQL_Part := 'WHERE Tipo > 0' + sLineBreak +
    TXTSQL_Part2 := dmkPF.SQL_Periodo('AND smic.DataHora ', DataIni, DataFim, True, True) + sLineBreak +
    //
    'AND smic.EMPRESA=' + CliInt_Txt + sLineBreak;

(*
    if TipoPesq = 2 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND scc.EntiSitio=' + EntiSitio_Txt + sLineBreak;
*)
    //
    //  Filtros de pesquisa
    if GG1 <> 0 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND gg1.Nivel1=' + FormatFloat('0', GraGru1Nivel1) + sLineBreak
    else
    if GG2 <> 0 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND gg1.Nivel2=' + FormatFloat('0', GraGru2Nivel2) + sLineBreak
    else
    if GG3 <> 0 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND gg1.Nivel3=' + FormatFloat('0', GraGru3Nivel3) + sLineBreak
    else
    if PGT <> 0 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND pgt.Codigo=' + FormatFloat('0', PrdGrupTipCodigo) + sLineBreak;
    //
    TXTSQL_Part2 := TXTSQL_Part2 + 'AND GraGruX=:P0';
    //


    MyObjects.Informa(LaAviso, False, '...');
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TDmod.LocCod(Atual, Codigo: Integer);
begin
  Dmod.DefParams;
  GOTOy.LC(Atual, Codigo);
end;

function TDmod.M2MedioWBFornecedor(GraGruX, Fornece: Integer): Double;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT frn.M2Medio ',
    'FROM wbmprfrn frn ',
    'WHERE frn.GraGruX=' + Geral.FF0(GraGruX),
    'AND frn.Fornece=' + Geral.FF0(Fornece),
    '']);
    Result := Qry.FieldByName('M2Medio').AsFloat;
  finally
    Qry.Free;
  end;
end;

procedure TDmod.MyDBAfterConnect(Sender: TObject);
begin
{
  if MyDB.Utf8Used = True then
  begin
    //MyDB.Utf8Used := False;
    Geral.MB_Info('TMySQLDatabase.Utf8Used = True')
  end
  else
    Geral.MB_Info('TMySQLDatabase.Utf8Used = False');
}
end;

procedure TDmod.Va(Para: TVaiPara);
begin
  Dmod.DefParams;
  //FmPrincipal.StatusBar.Panels[13].Text :=
    GOTOy.Go(Para, Dmod.QrCarteirasCodigo.Value, 'N');
    //FmPrincipal.StatusBar.Panels[13].Text[2]);
end;

procedure TDmod.ReadPixelsPerInch(Reader: TReader);
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

procedure TDmod.ReopenControle();
begin
  UnDmkDAC_PF.AbreQuery(QrControle, MyDB);
end;

procedure TDmod.ReopenLotes(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotes, Dmod.MyDB, [
  'SELECT ec.Controle, ec.MPIn, ec.Peso, ec.Pecas, ',
  'mi.Ficha, mi.Marca, mi.Lote ',
  'FROM emitcus ec ',
  'LEFT JOIN mpin mi ON mi.Controle=ec.MPIn ',
  'WHERE ec.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

function TDmod.ReopenParamsEspecificos(Empresa: Integer): Boolean;
begin
  // Compatibilidade!!
  Result := True;
end;

function TDmod.ReopenSMI(OriCodi, OriCtrl: Integer): Boolean;
var
  Tipos: String;
begin
  Tipos :=
    FormatFloat('0', VAR_FATID_0013) + ', ' +
    FormatFloat('0', VAR_FATID_0113) + ', ' +
    FormatFloat('0', VAR_FATID_0213);

  UnDmkDAC_PF.AbreMySQLQuery0(QrSMI, MyDB, [
    'SELECT *',
    'FROM stqmovitsa',
    'WHERE Tipo IN (' + Tipos + ')',
    'AND OriCodi=' + FormatFloat('0', OriCodi),
    'AND OriCtrl=' + FormatFloat('0', OriCtrl)]);
  Result := QrSMI.RecordCount > 0;
  if not Result then
    Geral.MB_Aviso('Classifica��o interrompida!'+sLineBreak+
    'N�mero de itens de espelho no estoque de grade difere de 1!'+sLineBreak+
    'N�mero de itens de espelho: ' + IntToStr(Dmod.QrSMI.RecordCount) + sLineBreak +
    'SQL:' + sLineBreak + Dmod.QrSMI.SQL.Text);
end;

function TDmod.SelecionaOSAberta(): Integer;
const
  Aviso  = '...';
  Titulo = 'OS Aberta';
  Prompt = 'Informe a OS:';
  Campo  = 'Descricao';
  FldCod = 'Codigo';
var
  Codigo: Variant;
  N: Integer;
begin
  Result := 0;
  //
  Codigo := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Controle ' + FldCod + ', CONCAT(Texto, " (Entrega: ", ',
  'DATE_FORMAT(Entrega, "%d/%m/%Y"), ")") ' + Campo,
  'FROM mpvits ',
  'WHERE Codigo = 0 ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, False);
  //
  if Codigo <> Null then
    Result := Codigo;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TDmod.DataModuleCreate(Sender: TObject);
const
  MyLocDatabase = nil;
var
  I: Integer;
  Versao, VerZero, VerNew: Int64;
  Resp: Integer;
  BD: String;
  //OthSetCon, DataBase: String;
  Verifica, VerificaDBTerc: Boolean;
begin
  for I := 0 to Dmod.ComponentCount - 1 do
  begin
    if Dmod.Components[I] is TmySQLQuery then
    begin
      TmySQLQuery(Dmod.Components[I]).Database := MyDB;
      //
      //if Dmod.Components[I] = QrUpdU then
        //Geral.MB_Info(TmySQLDatabase(QrUpdU.Database).Name);
    end
    else
    if Dmod.Components[I] is TmySQLTable then
      TmySQLTable(Dmod.Components[I]).Database := MyDB
    else
    if Dmod.Components[I] is TMySQLBatchExecute then
      TMySQLBatchExecute(Dmod.Components[I]).Database := MyDB;
  end;
  FmPrincipal.InfoSeqIni('Modules verificados');
  FmPrincipal.FLinModErr := 2105;
  VerificaDBTerc := False;
  VAR_MyBDFINANCAS := MyDB;  // MyPagtos : CASHIER
  MyList.ConfiguracoesIniciais(1);
  FmPrincipal.InfoSeqIni('Configura��es iniciais verificados');

  // Movido para USQLDB.VerificaDmodCreate_DBs(...
  //MyDB.LoginPrompt := False;
  //ZZDB.LoginPrompt := False;
  //
  //{$IfNDEf SemDBLocal}
  //FmPrincipal.FLinModErr := 2111;
  //MyLocDatabase.LoginPrompt := False;
  //{$EndIf}

  FmPrincipal.FLinModErr := 2125;
  USQLDB.VerificaDmod_1_Create_DBs([MyDB, ZZDB, MyLocDatabase]);
  FmPrincipal.InfoSeqIni('DBs criados');
  (*if MyDB.Connected then
  begin
    MyDB.Disconnect;
    ZZDB.Disconnect;
    MyDB.Disconnect;
    //Geral.MB_Aviso('MyDB est� connectado antes da configura��o!');
    MyObjects.MeDmk_Aviso('MyDB est� connectado antes da configura��o!');
  end;}
  {$IfNDEf SemDBLocal}
  if MyLocDataBase.Connected then
  begin
    MyLocDataBase.Disconnect;
    //Geral.MB_Aviso('MyLocDataBase est� connectado antes da configura��o!');
    MyObjects.MeDmk_Aviso('MyLocDataBase est� connectado antes da configura��o!');
  end;
  {$EndIf}*)



  // 2019-09-27
  //VAR_BDSENHA := 'wkljweryhvbirt';
  //VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);

  //USQLDB.VerificaDmodDefineVarsDBConnect(Host, Port, DBName, User, Senha);
  USQLDB.VerificaDmod_2_DefineVarsDBConnect();
  FmPrincipal.InfoSeqIni('VarsDB verificados');

  {
  OthSetCon := Geral.ReadAppKeyCU('OthSetCon', Application.Title, ktString, '');
  if OthSetCon = EmptyStr then
    VAR_BDSENHA := CO_USERSPNOW
  else
    VAR_BDSENHA := dmkPF.Criptografia(OthSetCon, CO_LLM_SecureStr);
  //
  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  //Server := Geral.ReadAppKeyCU('Server', Application.Title, ktInteger, 0);
  VAR_SERVIDOR := Geral.ReadAppKeyCU('Server', Application.Title, ktInteger, 0);
  //
(*
  case Server of
    1:   VAR_SERVIDOR := 0;
    2:   VAR_SERVIDOR := 1;
    3:   VAR_SERVIDOR := 2;
    else VAR_SERVIDOR := -1;
  end;
*)
  VAR_IP       := Geral.ReadAppKeyCU('IPServer', Application.Title, ktString, CO_VAZIO);
  DataBase     := Geral.ReadAppKeyCU('Database', Application.Title, ktString, 'mysql');
  VAR_SQLUSER  := Geral.ReadAppKeyCU('OthUser', Application.Title, ktString, 'root');
  if VAR_BDSENHA = '' then
    VAR_BDSENHA := CO_USERSPNOW;
  if VAR_SQLUSER = '' then
    VAR_SQLUSER := 'root';
  //FmPrincipal.FLinModErr := 2110;
  //Geral.WriteAppKeyCU('Porta', 'Dermatek', VAR_PORTA, ktInteger);
  // FIM 2019-09-27
  }
  ///////////////////////////////////////////////////////////
(*
  ZZDB.DatabaseName := 'mysql';
  ZZDB.Host         := VAR_IP;
  ZZDB.Port         := VAR_PORTA;
  ZZDB.UserName     := VAR_SQLUSER;
  ZZDB.UserPassword := VAR_BDSENHA;
*)
  FmPrincipal.FLinModErr := 2184;
  if not GOTOy.OMySQLEstaInstalado(FmBlueDerm_dmk.LaAviso1,
  FmBlueDerm_dmk.LaAviso2, FmBlueDerm_dmk.ProgressBar1,
  FmBlueDerm_dmk.BtEntra, FmBlueDerm_dmk.BtConecta) then
    Exit;
  FmPrincipal.InfoSeqIni('MySQL verificado');
(*  begin
    //raise EAbort.Create('');
    //
    FmPrincipal.FLinModErr := 2190;
    MyObjects.Informa2(FmBlueDerm_Dmk.LaAviso1, FmBlueDerm_Dmk.LaAviso2, False,
      'N�o foi poss�vel a conex�o ao IP: [' + VAR_IP + ']');
    FmBlueDerm_Dmk.BtEntra.Visible := False;
    Exit;
  end;*)

  USQLDB.VerificaDmod_3_RedefineSenhaSeNecessario(ZZDB, QrUpd);
  FmPrincipal.InfoSeqIni('RedefPwd verificado');
{
  /////////////////////////////////////////
  FmPrincipal.FLinModErr := 2198;
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////////////////////
  (*
  ZZDB.DatabaseName := 'mysql';
  ZZDB.Host         := VAR_IP;
  ZZDB.Port         := VAR_PORTA;
  ZZDB.UserName     := VAR_SQLUSER;
  ZZDB.UserPassword := VAR_BDSENHA;*)
  (* J� implementado em senha desconhecida?
  try
FmPrincipal.FLinModErr := 2211;
    //ZZDB.Connected := True;
    USQLDB.ConectaDB(ZZDB, 'DMod._2216_');
FmPrincipal.FLinModErr := 2214;
  except
    ZZDB.UserPassword := '852456';
    try
FmPrincipal.FLinModErr := 2218;
      //ZZDB.Connected := True;
      USQLDB.ConectaDB(ZZDB, 'DMod.2145');
FmPrincipal.FLinModErr := 2221;
      QrUpd.Database := ZZDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE user SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.FLinModErr := 2233;
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      FmPrincipal.FLinModErr := 2238;
      if VAR_SERVIDOR = 2 then ShowMessage('Banco de dados teste n�o se conecta!');
    end;
  end;
*)
}
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  //
  FmPrincipal.FLinModErr := 2254;
  USQLDB.VerificaDmod_4_PreparaParaConectar();
  FmPrincipal.InfoSeqIni('PreparaConect verificados');
{
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Geral.MB_Erro('M�quina cliente sem rede.');
        //Application.Terminate;
      end;
    end;
 FmPrincipal.FLinModErr := 2264;
 except
    raise;
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
FmPrincipal.FLinModErr := 2272;
    Application.Terminate;
    Exit;
  end;
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
FmPrincipal.FLinModErr := 2280;
  // ini 2020-09-17
(*
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
*)
    Application.CreateForm(TFmVerificaConexoes,  FmVerificaConexoes);
    FmVerificaConexoes.ShowModal;
    FmVerificaConexoes.Destroy;
  // fim 2020-09-17
  end;
  if VAR_IP = CO_VAZIO then
  begin
FmPrincipal.FLinModErr := 2294;
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, VAR_IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;

}
{
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.Host := VAR_IP;
  MyDB.DatabaseName := 'mysql';// existe com certeza se estiver instalado
}

  {if not GOTOy.OMySQLEstaInstalado(
  FmBlueDerm_dmk.LaAviso1, FmBlueDerm_dmk.LaAviso2, FmBlueDerm_dmk.ProgressBar1) then
  begin
    //raise EAbort.Create('');
    //
FmPrincipal.FLinModErr := 2320;
    MyObjects.Informa2(FmBlueDerm_Dmk.LaAviso1, FmBlueDerm_Dmk.LaAviso2, False,
      'N�o foi poss�vel a conex�o ao IP: [' + VAR_IP + ']');
    FmBlueDerm_Dmk.BtEntra.Visible := False;
    Exit;
  end;}
FmPrincipal.FLinModErr := 2228;
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, 'mysql',  VAR_IP,
  VAR_PORTA,  VAR_SQLUSER, VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True,
  (*Conecta*)False);
  FmPrincipal.InfoSeqIni('Conecta mysql OK');
  //
  //



  FmPrincipal.FLinModErr := 2236;
  USQLDB.VerificaDmod_5_DefineBDPrincipal(MyDB, QrAux);
  FmPrincipal.InfoSeqIni('Define DB Principal verificado');

{
  QrAux.Close;
  QrAux.Database := MyDB;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES LIKE "%' + TMeuDB + '%"');
  UnDmkDAC_PF.AbreQueryApenas(QrAux);
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    //if Uppercase(QrAux.FieldByName('Database').AsWideString)=Uppercase(TMeuDB) then
    if Uppercase(QrAux.Fields[0].AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
FmPrincipal.FLinModErr := 2249;
  MyDB.Close;
  MyDB.DataBaseName := BD;
  if MyDB.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MB_Pergunta('O banco de dados '+TMeuDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?');
    if Resp = ID_YES then
    begin
      QrAux.Close;
      QrAux.SQL.Clear;
      QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
      QrAux.ExecSQL;
      //
      if MyDB.Connected then
        MyDB.Disconnect;
      MyDB.DatabaseName := TMeuDB;
    end else //if Resp = ID_CANCEL then
    begin
      Geral.MB_Aviso('O aplicativo ser� encerrado!');
      Halt(0);
      //Application.Terminate;
      Exit;
    end;
  end;
}
  //
  FmPrincipal.FLinModErr := 2275;
  GOTOy.DefinePathMySQL;
  FmPrincipal.InfoSeqIni('Path MySQL verificado');
  //
  // Alexandria Parei Aqui � o servidor
{
//////////////////////////  MyLOcDatabase  /////////////////////////////////////
object MyLocDatabase: TMySQLDatabase
  DatabaseName = 'bluederm'
  UserName = 'root'
  UserPassword = 'wkljweryhvbirt'
  Host = '192.168.100.60'
  ConnectOptions = []
  ConnectionTimeout = 5
  Params.Strings = (
    'Port=3306'
    'TIMEOUT=5'
    'UID=root'
    'Host=192.168.100.60'
    'DatabaseName=bluederm'
    'PWD=wkljweryhvbirt')
  SSLProperties.TLSVersion = tlsAuto
  DatasetOptions = []
  Left = 112
  Top = 59
end
////////////////////////////////////////////////////////////////////////////////

  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  try
    UnDmkDAC_PF.AbreQueryApenas(QrAuxL);
    BD := CO_VAZIO;
    while not QrAuxL.Eof do
    begin
      if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
        BD := TLocDB;
      QrAuxL.Next;
    end;
    MyLocDatabase.Close;
    MyLocDatabase.DatabaseName := BD;
    if MyLocDatabase.DataBaseName = CO_VAZIO then
    begin
      Resp := Geral.MB_Pergunta('O banco de dados local '+TLocDB+
        ' n�o existe e deve ser criado. Confirma a cria��o?');
      if Resp = ID_YES then
      begin
        QrAuxL.Close;
        QrAuxL.SQL.Clear;
        QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
        QrAuxL.ExecSQL;
        MyLocDatabase.DatabaseName := TLocDB;
      end else if Resp = ID_CANCEL then
      begin
        Geral.MB_Aviso('O aplicativo ser� encerrado!');
        Application.Terminate;
        Exit;
      end;
    end;
    FmPrincipal.FLinModErr := 2412;
    UnDmkDAC_PF.ConectaMyDB_DAC(MyLocDatabase, TLocDB,  '127.0.0.1', VAR_PORTA,
      VAR_SQLUSER, VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
  except
    Geral.MB_Aviso('O MySQL podse n�o estar instalado neste computador!');
  end;
}
    //
  USQLDB.VerificaDmod_6_AtualizaBDSeNecessario(MyDB, QrAux, VerificaDBTerc);
  FmPrincipal.InfoSeqIni('AtualizaBDSeNecessario verificado');
{
FmPrincipal.FLinModErr := 2419;
  Versao   := USQLDB.ObtemVersaoAppDB(MyDB);
  Verifica := False;
  if Versao < CO_VERSAO then
    Verifica := True
  else
(*
  //if Versao = CO_VERSAO then
  begin
    VerNew   := USQLDB.ObtemVersaoAppDB(MyDB, TVerApp.verappAppApp);
    if VerNew < CO_VERNEW then
      Verifica := True;
  end;
*)  //
  if Versao = 0 then
  begin
    Resp := Geral.MB_Pergunta('N�o h� informa��o de vers�o no registro, '+
              'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
              'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
              'banco de dados. Confirma a Verifica��o?');
    //
    if Resp = ID_YES then
      Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  //FmPrincipal.VerificaTerminal;
  // M L A G e r a l .ConfiguracoesIniciais(1, MyDB.DatabaseName);
FmPrincipal.FLinModErr := 2451;
  //MyList.ConfiguracoesIniciais(1);
FmPrincipal.FLinModErr := 2453;
  //UnDmkDAC_PF.AbreQuery(QrCambios, MyDB);
  //if VAR_SERVIDOR = 2 then GOTOy.LiberaUso;
  if Verifica then
    ALL_Jan.MostraFormVerifiDB(True);
FmPrincipal.FLinModErr := 2458;
  //
}
  // desmarcado em 2022-06-17
  try
    UnDmkDAC_PF.AbreQuery(QrMaster, MyDB, '', True);
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    try
FmPrincipal.FLinModErr := 2465;
      ALL_Jan.MostraFormVerifiDB(True);
      //
      VerificaDBTerc := True;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;
  FmPrincipal.InfoSeqIni('Master OK');
  //
FmPrincipal.FLinModErr := 2475;
{  try
    Application.CreateForm(TDmodG, DmodG);
FmPrincipal.FLinModErr := 2478;
  except
    on e: Exception do
    begin
      Geral.MB_Aviso('Imposs�vel criar Modulo de dados Geral' + sLineBreak +
      'FmPrincipal.FLinModGeralErr = ' + Geral.FF0(FmPrincipal.FLinModErr) + sLineBreak +
      e.Message);
    end;
      //Application.Terminate;
      //Exit;
  end;
}
  USQLDB.VerificaDmod_7_CriaDataModule(TDmodG, DmodG);
  FmPrincipal.InfoSeqIni('DModG criado');

{
  try
    Application.CreateForm(TDModFin, DModFin);
  except
    Geral.MB_Aviso('Imposs�vel criar Modulo Financeiro');
    Application.Terminate;
    Exit;
  end;
}
  USQLDB.VerificaDmod_7_CriaDataModule(TDModFin, DModFin);
  FmPrincipal.InfoSeqIni('DModFin Criado');
  //
  if VerificaDBTerc then
  begin
    All_Jan.MostraFormVerifiDBTerceiros(True);
    //
    VerificaDBTerc := False;
  end;
  //
FmPrincipal.FLinModErr := 2503;
  //Lic_Dmk.LiberaUso5;
  //Geral.MB_Aviso('"Lic_Dmk" desabilitado!');
  ReopenControle();
  FmPrincipal.InfoSeqIni('Controle ok');
end;

procedure TDmod.VerificaSenha(Index: Integer; Login, Senha: String);
begin
  if (Senha = VAR_BOSS) or (Senha = CO_MASTER) then VAR_SENHARESULT := 2
  else
  begin
    (*if Index = 9 then
    begin
      QrSenha.Close;
      QrSenha.Params[0].AsString := Login;
      QrSenha.Params[1].AsString := Senha;
      UnDmkDAC_PF.AbreQuery(QrSenha, MyDB);
      if QrSenha.RecordCount > 0 then
      begin
        QrPerfis.Close;
        QrPerfis.Params[0].AsInteger := QrSenhaPerfil.Value;
        UnDmkDAC_PF.AbreQuery(QrPerfis, MyDB);
        if QrPerfisLibera.Value = 1 then VAR_SENHARESULT := 2
        else MessageBox(0,'Acesso negado. Senha Inv�lida',
        'Permiss�o por senha', MB_OK+MB_ICONEXCLAMATION);
        QrPerfis.Close;
      end else ShowMessage('Login inv�lido.');
      QrSenha.Close;
    end else ShowMessage('Em constru��o');*)
  end;
end;

function TDmod.WBFic(GraGruX, Empresa, Fornecedor, Pallet: Integer;
Pecas, AreaM2, PesoKg, ValorT: Double;
EdGraGruX, EdPallet, EdPecas, EdAreaM2, EdValorT: TWinControl): Boolean;
begin
  Result := True;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe uma mat�ria-prima!') then
    Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa inv�lida!') then
    Exit;
  if MyObjects.FIC(Fornecedor = 0, nil, 'Fornecedor inv�lido!') then
    Exit;
  if MyObjects.FIC(Pallet = 0, EdPallet, 'O pallet � obrigat�rio!') then
    Exit;
  if MyObjects.FIC(Pecas = 0, EdPecas, 'A quantidade de pe�as � obrigat�ria!') then
    Exit;
  if Dmod.QrControleWBNivGer.Value > Integer(encPecas) then
    if MyObjects.FIC((AreaM2 = 0) and (PesoKg = 0), EdAreaM2, 'A �rea ou peso � obrigat�rio!') then
      Exit;
  if Dmod.QrControleWBNivGer.Value > Integer(encArea) then
    if MyObjects.FIC(ValorT = 0, EdValorT, 'O valor total � obrigat�rio!') then
      Exit;
  //
  Result := False;
end;

procedure TDmod.WritePixelsPerInch(Writer: TWriter);
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then Geral.MB_Aviso(
    'CNPJ n�o definido ou Empresa n�o definida.'+ sLineBreak +
    'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.' +
    sLineBreak + sLineBreak +
    Application.Title + '  ::  ' + QrMasterEm.Value + ' # ' +
    Geral.FormataCNPJ_TT(QrMasterCNPJ.Value));
  //
  ReopenControle();
  //
  if QrControle.FieldByName('SoMaiusculas').AsString = 'V' then VAR_SOMAIUSCULAS := True;
  VAR_UFPADRAO        := QrControleUFPadrao.Value;
  VAR_CIDADEPADRAO    := QrControle.FieldByName('Cidade').AsString;
  VAR_TRAVACIDADE     := QrControleTravaCidade.Value;
  VAR_MOEDA           := QrControle.FieldByName('Moeda').AsString;
  //
  VAR_CARTVEN         := QrControleCartVen.Value;
  VAR_CARTCOM         := QrControleCartCom.Value;
  VAR_CARTRES         := QrControleCartReS.Value;
  VAR_CARTDES         := QrControleCartDeS.Value;
  VAR_CARTREG         := QrControleCartReG.Value;
  VAR_CARTDEG         := QrControleCartDeG.Value;
  VAR_CARTCOE         := QrControleCartCoE.Value;
  VAR_CARTCOC         := QrControleCartCoC.Value;
  VAR_CARTEMD         := QrControleCartEmD.Value;
  VAR_CARTEMA         := QrControleCartEmA.Value;
  //
  (*VAR_COMISSPROD_PERC := QrControleComissProdPerc.Value;
  VAR_COMISSPROD_EDIT := QrControleComissProdEdit.Value;
  VAR_COMISSSERV_PERC := QrControleComissServPerc.Value;
  VAR_COMISSSERV_EDIT := QrControleComissServEdit.Value;*)
  //
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControlePaperTop.Value;
  VAR_PAPERLEF        := QrControlePaperLef.Value;
  VAR_PAPERWID        := QrControlePaperWid.Value;
  VAR_PAPERHEI        := QrControlePaperHei.Value;
  VAR_PAPERFCL        := QrControlePaperFcl.Value;
  //
end;

function TDmod.CentroDeEstoqueDefinido(): Boolean;
begin
  Result := Dmod.QrControlePQ_StqCenCad.Value <> 0;
  if not Result then Geral.MB_Aviso('O centro de estoque padr�o de uso e ' +
  'consumo n�o foi definido!');
end;

function TDmod.ConcatenaRegistros(Matricula: Integer): String;
(*var
  Registros, Conta: Integer;
  Liga: String;*)
begin
(*  Qr??.Close;
  Qr??.Params[0].AsInteger := Matricula;
  UnDmkDAC_PF.AbreQuery(Qr??, MyDB);
  //
  Registros := Qr.RecordCount;
  if Registros > 1 then Result := 'Pagamento de '
  else if Registros = 1 then Result := 'Pagamento de ';
  Result := Result + Qr??NOMECURSO.Value;
  Qr??.Next;
  Conta := 1;
  Liga := ', ';
  while not Qr??.Eof do
  begin
    Conta := Conta + 1;
    if Conta = Registros then Liga := ' e ';
    Result := Result + Liga + Qr??NOMECURSO.Value;
    Qr?
    ?.Next;
  end;*)
end;


procedure TDmod.TbMovCalcFields(DataSet: TDataSet);
begin
(*  TbMovSUBT.Value := TbMovQtde.Value * TbMovPreco.Value;
  TbMovTOTA.Value := TbMovSUBT.Value - TbMovDesco.Value;
  case TbMovTipo.Value of
    -1: TbMovNOMETIPO.Value := 'Venda';
     1: TbMovNOMETIPO.Value := 'Compra';
    else TbMovNOMETIPO.Value := 'INDEFINIDO';
  end;
  case TbMovENTITIPO.Value of
    0: TbMovNOMEENTIDADE.Value := TbMovENTIRAZAO.Value;
    1: TbMovNOMEENTIDADE.Value := TbMovENTINOME.Value;
    else TbMovNOMEENTIDADE.Value := 'INDEFINIDO';
  end;
  if TbMovESTQQ.Value <> 0 then
  TbMovCUSTOPROD.Value := TbMovESTQV.Value / TbMovESTQQ.Value else
  TbMovCUSTOPROD.Value := 0;*)
end;

procedure TDmod.TbMovBeforePost(DataSet: TDataSet);
begin
(*  case TbMovTipo.Value of
   -1:
    begin
      TbMovTotalV.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
      TbMovTotalC.Value := TbMovQtde.Value * TbMovCUSTOPROD.Value;
    end;
    1:
    begin
      TbMovTotalV.Value := 0;
      TbMovTotalC.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
    end;
    else begin
      //
    end;
  end;*)
end;

procedure TDmod.TbMovBeforeDelete(DataSet: TDataSet);
begin
  //FProdDelete := TbMovProduto.Value;
end;

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  WSACleanup;
end;

function TDmod.ObrigaInformarEmitGru(Obriga: Boolean; EmitGru: Integer;
EdEmitGru: TdmkEditCB): Boolean;
begin
  Result := False;
  if Obriga then
    if QrControleUsaEmitGru.Value = 1 then
      if MyObjects.FIC(EmitGru = 0, EdEmitGru, 'Informe o grupo de emiss�o!') then
        Result := True;
end;

function TDmod.ObtemGraGruXDeCouro(Tipificacao, Animal: Integer): Integer;
var
  T, A: Integer;
begin
  T := Tipificacao + 1; if T = 1 then T := 9;  // outros
  A := Animal + 1;
  Result := ((T * 100) + A) * -1;
end;

function TDmod.ObtemGraGruXDeGraGru1(GraGru1: Integer): Integer;
begin
  QrOX1.Close;
  QrOX1.Params[0].AsInteger := GraGru1;
  UnDmkDAC_PF.AbreQuery(QrOX1, MyDB);
  //
  Result := QrOX1Controle.Value;
end;

function TDmod.ObtemIP(Retorno: Integer): String;
var
  p: PHostEnt;
  s: array[0..128] of char;
  p2: PAnsiChar;
begin
  GetHostName(@s, 128);
  p := GetHostByName(@s);
  p2 := iNet_ntoa(PInAddr(p^.h_addr_list^)^);
  case Retorno of
    0: Result := p^.h_Name;
    1: Result := p2;
    else Result := '';
  end;
end;

procedure TDmod.PQR_AtualizaPQx(OrigemCodi, OrigemCtrl, Tipo: Integer;
  RetQtd: Double);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_PQX, False, [
  'RetQtd'], [
  'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
  RetQtd], [
  OrigemCodi, OrigemCtrl, Tipo], False);
end;

procedure TDmod.PQR_AtualizaRetQtdInn(OriCodiInn, OriCtrlInn, TipoInn: Integer);
  //Peso: Double);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRx, MyDB, [
  'SELECT SUM(Peso) Peso ',
  'FROM pqrits ',
  'WHERE OriCodiInn=' + Geral.FF0(OriCodiInn),
  'AND OriCtrlInn=' + Geral.FF0(OriCtrlInn),
  'AND TipoInn=' + Geral.FF0(TipoInn),
  '']);
  //
  PQR_AtualizaPQx(OriCodiInn, OriCtrlInn, TipoInn, QrPQRxPeso.Value * -1);
end;

procedure TDmod.PQR_AtualizaRetQtdOut(OriCodiOut, OriCtrlOut, TipoOut: Integer);
  //Peso: Double);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRx, MyDB, [
  'SELECT SUM(Peso) Peso ',
  'FROM pqrits ',
  'WHERE OriCodiOut=' + Geral.FF0(OriCodiOut),
  'AND OriCtrlOut=' + Geral.FF0(OriCtrlOut),
  'AND TipoOut=' + Geral.FF0(TipoOut),
  '']);
  //
  PQR_AtualizaPQx(OriCodiOut, OriCtrlOut, TipoOut, QrPQRxPeso.Value);
end;

function TDmod.PQR_InsereItemPQRIts(Codigo, OriCodiInn, OriCtrlInn, TipoInn,
  OriCodiOut, OriCtrlOut, TipoOut, Insumo: Integer; Peso, Valor: Double;
  Ocorrencia: Integer): Integer;
var
  Controle: Integer;
  Preco: Double;
begin
  Result := 0;
  Controle       := UMyMod.BPGS1I32('pqrits', 'Controle', '', '', tsPos, stIns, 0);
  if Peso = 0 then
    Preco := 0
  else
    Preco := Valor / Peso;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqrits', False, [
  'Codigo', 'Preco', 'Peso',
  'Valor', 'OriCodiInn', 'OriCtrlInn',
  'TipoInn', 'OriCodiOut', 'OriCtrlOut',
  'TipoOut', 'Ocorrecia', 'Insumo'], [
  'Controle'], [
  Codigo, Preco, Peso,
  Valor, OriCodiInn, OriCtrlInn,
  TipoInn, OriCodiOut, OriCtrlOut,
  TipoOut, Ocorrencia, Insumo], [
  Controle], True) then
  begin
    //
    // Nao computar saldo inicial de baixa!
    if TipoInn <> VAR_FATID__005 then
      if TipoInn <> VAR_FATID__004 then
        PQR_AtualizaRetQtdInn(OriCodiInn, OriCtrlInn, TipoInn);
    // Nao computar saldo inicial de NFe de entrada!
    if TipoOut <> VAR_FATID__005 then
      if TipoOut <> VAR_FATID__003 then
        PQR_AtualizaRetQtdOut(OriCodiOut, OriCtrlOut, TipoOut);
    //
    Result := Controle;
  end;
end;

function TDmod.PreparaMoviPQ_EstqEm_v2014(Tabela : TNomeTabRecriaTempTable;
QrMoviPQ: TmySQLQuery; CI, PQ: Integer; DtIni, DtFim: TDateTime; NomeCI, _WHERE,
ORDERBY: String; LaAviso1, LaAviso2: TLabel; PBx: TProgressBar):
Boolean;
  procedure Reopen3PQ(PQ: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr3PQ, Dmod.MyDB, [
    'SELECT DISTINCT Insumo, pq_.Nome NOMEPQ, ',
    'CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial ',
    'ELSE frn.Nome END NOMEIQ, lse.Nome NOMESETOR ',
    'FROM pqx pqx ',
    'LEFT JOIN pq           pq_ ON pq_.Codigo=pqx.Insumo ',
    'LEFT JOIN entidades    frn ON frn.Codigo=pq_.IQ ',
    'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor ',
    'WHERE pqx.Insumo > 0 ', // N�o mostrar [agua, reciclo e PQ zero
    'AND pqx.Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
    Geral.ATS_if(PQ <> 0, ['AND pqx.Insumo=' + Geral.FF0(PQ)]),
    '']);
  end;
var
  AntPes, AntVal, BalPes, BalVal, FimPes, FimVal: Double;
  SQL: String;
var
  MoviPQ, DataI, DataF,
  NomePQ, NomeFO, NomeSE: String;
  Insumo: Integer;
  InnPes, InnVal, OutPes, OutVal: Double;
  Agora: TDateTime;
begin
  MoviPQ :=
    UnCreateBlueDerm.RecriaTempTableNovo(Tabela, DmodG.QrUpdPID1, False);
  //
  //
  Screen.Cursor := crHourGlass;
  try
    DataI := Geral.FDT(DtIni, 1);
    DataF := Geral.FDT(DtFim, 1);
    //
{
    Qr3Inn.Close;
    Qr3Inn.Params[0].AsString  := DataI;
    Qr3Inn.Params[1].AsString  := DataF;
    Qr3Inn.Params[2].AsInteger := CI;
    UnDMkDAC_PF.AbreQuery(Qr3Inn, Dmod.MyDB);
    //
    Qr3Out.Close;
    Qr3Out.Params[0].AsString  := DataI;
    Qr3Out.Params[1].AsString  := DataF;
    Qr3Out.Params[2].AsInteger := CI;
    UnDMkDAC_PF.AbreQuery(Qr3Out, Dmod.MyDB);
}
    UnDmkDAC_PF.AbreMySQLQuery0(Qr3Bal, Dmod.MyDB, [
    'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor ',
    'FROM pqx ',
    //'WHERE Tipo=0 ',
    'WHERE Tipo=' + Geral.FF0(VAR_FATID__001),
    'AND DataX BETWEEN "' + DataI + '" AND "' + DataF + '" ',
    'AND CliOrig=' + Geral.FF0(CI),
    'AND pqx.Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
    'GROUP BY Insumo ',
    '']);
    //dmkPF.LeMeuTexto(Qr3Bal.SQL.Text);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr3Inn, Dmod.MyDB, [
    'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor ',
    'FROM pqx ',
    'WHERE Tipo>' + Geral.FF0(VAR_FATID_0000) + ' AND Tipo<=' + Geral.FF0(VAR_FATID_0100),
    'AND DataX BETWEEN "' + DataI + '" AND "' + DataF + '" ',
    'AND CliOrig=' + Geral.FF0(CI),
    'AND pqx.Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
    'GROUP BY Insumo ',
    '']);
    //dmkPF.LeMeuTexto(Qr3Inn.SQL.Text);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr3Out, Dmod.MyDB, [
    'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor ',
    'FROM pqx ',
    'WHERE Tipo>' + Geral.FF0(VAR_FATID_0100),
    'AND DataX BETWEEN "' + DataI + '" AND "' + DataF + '" ',
    'AND CliOrig=' + Geral.FF0(CI),
    'AND pqx.Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
    'GROUP BY Insumo ',
    '']);
    //
    Reopen3PQ(PQ);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DmodG.MyPID_DB, [
    'DELETE FROM ' + MoviPQ,
    '']);
    //
    if PBx <> nil then
    begin
      PBx.Position := 0;
      PBx.Max := Qr3PQ.RecordCount;
    end;
    Qr3PQ.First;
    Agora := DMOdG.ObtemAgora();
    while not Qr3PQ.Eof do
    begin
      if PBx <> nil then
      begin
        PBx.Position := PBx.Position + 1;
        PBx.Update;
      end;
      Application.ProcessMessages;
      //
      //if Qr3PQInsumo.Value = 15 then
        //ShowMessage(Qr3PQNOMEPQ.Value);
      UnPQx.VerificaEstoquePQ(CI, Qr3PQInsumo.Value, Int(DtIni) -1, AntPes, AntVal);
      UnPQx.VerificaEstoquePQ(CI, Qr3PQInsumo.Value, Int(DtFim), FimPes, FimVal);
{
      BalPes := FimPes - (AntPes + Qr3PQINNPESO.Value + Qr3PQOUTPESO.Value);
      BalVal := FimVal - (AntVal + Qr3PQINNVALOR.Value + Qr3PQOUTVALOR.Value);
}
      //
      Insumo         := Qr3PQInsumo.Value;
      NomePQ         := Qr3PQNOMEPQ.Value;
      NomeFO         := Qr3PQNOMEIQ.Value;
      //NomeCI         := CBCI.Text;
      NomeSE         := Qr3PQNOMESETOR.Value;
      //AntPes         := 0;
      //AntVal         := 0;
      InnPes         := Qr3PQINNPESO.Value;
      InnVal         := Qr3PQINNVALOR.Value;
      OutPes         := Qr3PQOUTPESO.Value;
      OutVal         := Qr3PQOUTVALOR.Value;
      BalPes         := Qr3PQBALPESO.Value;
      BalVal         := Qr3PQBALVALOR.Value;
      //FimPes         := 0;
      //FimVal         := 0;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, MoviPQ, False, [
      'Insumo', 'NomePQ', 'NomeFO',
      'NomeCI', 'NomeSE', 'AntPes',
      'AntVal', 'InnPes', 'InnVal',
      'OutPes', 'OutVal', 'BalPes',
      'BalVal', 'FimPes', 'FimVal'], [
      ], [
      Insumo, NomePQ, NomeFO,
      NomeCI, NomeSE, AntPes,
      AntVal, InnPes, InnVal,
      OutPes, OutVal, BalPes,
      BalVal, FimPes, FimVal], [
      ], False);
      //DmkPF.LeMeuTexto(DModG.QrUpdPID1.SQL.Text);
      //
      Qr3PQ.Next;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'Tempo: ' + FormatDateTime('nn:ss', DModG.ObtemAgora() - Agora));
    UnDMkDAC_PF.AbreMySQLQuery0(QrMoviPQ, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + MoviPQ,
    _WHERE,
    ORDERBY,
    '']);
    //
    PBx.Position := 0;
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDmod.PreparaMoviPQ_EstqEm_v2018(Tabela: TNomeTabRecriaTempTable;
  QrMoviPQ: TmySQLQuery; CI, PQ: Integer; DtIni, DtFim: TDateTime; NomeCI,
  _WHERE, ORDERBY: String; LaAviso1, LaAviso2: TLabel; PBx: TProgressBar): Boolean;
  procedure Reopen3PQ(PQ: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr3PQ, Dmod.MyDB, [
(*
    'SELECT DISTINCT Insumo, pq_.Nome NOMEPQ, ',
    'CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial ',
    'ELSE frn.Nome END NOMEIQ, lse.Nome NOMESETOR ',
    'FROM pqx pqx ',
    'LEFT JOIN pq           pq_ ON pq_.Codigo=pqx.Insumo ',
    'LEFT JOIN entidades    frn ON frn.Codigo=pq_.IQ ',
    'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor ',
    'WHERE pqx.Insumo > 0 ', // N�o mostrar [agua, reciclo e PQ zero
    'AND pqx.Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
*)
    'SELECT DISTINCT Insumo, pq_.Nome NOMEPQ, ',
    'CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial ',
    'ELSE frn.Nome END NOMEIQ, lse.Nome NOMESETOR,',
    'pqc.CustoPadrao ',
    'FROM pqx pqx ',
    'LEFT JOIN pq           pq_ ON pq_.Codigo=pqx.Insumo ',
    'LEFT JOIN pqcli        pqc ON pqc.PQ=pqx.Insumo AND pqc.CI=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
    'LEFT JOIN entidades    frn ON frn.Codigo=pq_.IQ ',
    'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor ',
    'WHERE pqx.Insumo > 0 ', // N�o mostrar [agua, reciclo e PQ zero
    'AND pqx.Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
    //
    Geral.ATS_if(PQ <> 0, ['AND pqx.Insumo=' + Geral.FF0(PQ)]),
    '']);
  end;
var
  AntPes, AntVal, AntPad, BalPes, BalVal, BalPad, FimPes, FimVal, FimPad, CusPad: Double;
  SQL: String;
var
  MoviPQ, DataI, DataF,
  NomePQ, NomeFO, NomeSE: String;
  Insumo: Integer;
  InnPes, InnVal, InnPad, OutPes, OutVal, OutPad, DvlPes, DvlVal, DvlPad: Double;
  Agora: TDateTime;
  ArrEstqPQsIni, ArrEstqPQsFim: TArrEstqPQs;
begin
  MoviPQ :=
    UnCreateBlueDerm.RecriaTempTableNovo(Tabela, DmodG.QrUpdPID1, False);
  //
  //
  Screen.Cursor := crHourGlass;
  try
    DataI := Geral.FDT(DtIni, 1);
    DataF := Geral.FDT(DtFim, 1);
    //
{
    Qr3Inn.Close;
    Qr3Inn.Params[0].AsString  := DataI;
    Qr3Inn.Params[1].AsString  := DataF;
    Qr3Inn.Params[2].AsInteger := CI;
    UnDMkDAC_PF.AbreQuery(Qr3Inn, Dmod.MyDB);
    //
    Qr3Out.Close;
    Qr3Out.Params[0].AsString  := DataI;
    Qr3Out.Params[1].AsString  := DataF;
    Qr3Out.Params[2].AsInteger := CI;
    UnDMkDAC_PF.AbreQuery(Qr3Out, Dmod.MyDB);
}
    UnDmkDAC_PF.AbreMySQLQuery0(Qr3Bal, Dmod.MyDB, [
    'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor ',
    'FROM pqx ',
    //'WHERE Tipo=0 ',
    'WHERE Tipo=' + Geral.FF0(VAR_FATID__001),
    //'WHERE Tipo=' + Geral.FF0(VAR_FATID_0000),
    'AND DataX BETWEEN "' + DataI + '" AND "' + DataF + '" ',
    'AND CliOrig=' + Geral.FF0(CI),
    'AND pqx.Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
    'GROUP BY Insumo ',
    '']);
    //dmkPF.LeMeuTexto(Qr3Bal.SQL.Text);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr3Inn, Dmod.MyDB, [
    'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor ',
    'FROM pqx ',
    'WHERE Tipo>' + Geral.FF0(VAR_FATID_0000) + ' AND Tipo<=' + Geral.FF0(VAR_FATID_0100),
    'AND DataX BETWEEN "' + DataI + '" AND "' + DataF + '" ',
    'AND CliOrig=' + Geral.FF0(CI),
    'AND pqx.Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
    'GROUP BY Insumo ',
    '']);
    //dmkPF.LeMeuTexto(Qr3Inn.SQL.Text);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr3Out, Dmod.MyDB, [
    'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor ',
    'FROM pqx ',
    'WHERE Tipo>' + Geral.FF0(VAR_FATID_0100),
    'AND  Tipo<>' + Geral.FF0(VAR_FATID_0170),
    'AND DataX BETWEEN "' + DataI + '" AND "' + DataF + '" ',
    'AND CliOrig=' + Geral.FF0(CI),
    'AND pqx.Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
    'GROUP BY Insumo ',
    '']);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr3Dvl, Dmod.MyDB, [
    'SELECT Insumo, SUM(Peso) Peso, SUM(Valor) Valor ',
    'FROM pqx ',
    'WHERE Tipo=' + Geral.FF0(VAR_FATID_0170),
    'AND DataX BETWEEN "' + DataI + '" AND "' + DataF + '" ',
    'AND CliOrig=' + Geral.FF0(CI),
    'AND pqx.Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
    'GROUP BY Insumo ',
    '']);
    //Geral.MB_SQL(nil, Qr3Dvl);
    //
    Reopen3PQ(PQ);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DmodG.MyPID_DB, [
    'DELETE FROM ' + MoviPQ,
    '']);
    //
    if PBx <> nil then
    begin
      PBx.Position := 0;
      PBx.Max := Qr3PQ.RecordCount;
    end;
    //
    UnPQx.VerificaEstoquePQs(CI, Int(DtIni) -1, ArrEstqPQsIni);
    UnPQx.VerificaEstoquePQs(CI, Int(DtFim), ArrEstqPQsFim);
    //
    Qr3PQ.First;
    Agora := DMOdG.ObtemAgora();
    while not Qr3PQ.Eof do
    begin
      if PBx <> nil then
      begin
        PBx.Position := PBx.Position + 1;
        PBx.Update;
      end;
      Application.ProcessMessages;
      //
      Insumo         := Qr3PQInsumo.Value;
      CusPad         := Qr3PQCustoPadrao.Value;
{
      UnPQx.VerificaEstoquePQ(CI, Qr3PQInsumo.Value, Int(DtIni) -1, AntPes, AntVal);
      UnPQx.VerificaEstoquePQ(CI, Qr3PQInsumo.Value, Int(DtFim), FimPes, FimVal);
}
      if Length(ArrEstqPQsIni) > Insumo then
      begin
        AntPes         := ArrEstqPQsIni[Insumo, 0];
        AntVal         := ArrEstqPQsIni[Insumo, 1];
        AntPad         := AntPes * CusPad;
      end else
      begin
        AntPes         := 0;
        AntVal         := 0;
        AntPad         := AntPes * CusPad;
      end;
      if Length(ArrEstqPQsFim) > Insumo then
      begin
        FimPes         := ArrEstqPQsFim[Insumo, 0];
        FimVal         := ArrEstqPQsFim[Insumo, 1];
        FimPad         := FimPes * CusPad;
      end else
      begin
        FimPes         := ArrEstqPQsFim[Insumo, 0];
        FimVal         := ArrEstqPQsFim[Insumo, 1];
        FimPad         := FimPes * CusPad;
      end;
      //
      NomePQ         := Qr3PQNOMEPQ.Value;
      NomeFO         := Qr3PQNOMEIQ.Value;
      //NomeCI         := CBCI.Text;
      NomeSE         := Qr3PQNOMESETOR.Value;
      //AntPes         := 0;
      //AntVal         := 0;
      InnPes         := Qr3PQINNPESO.Value;
      InnVal         := Qr3PQINNVALOR.Value;
      InnPad         := InnPes * CusPad;
      OutPes         := Qr3PQOUTPESO.Value;
      OutVal         := Qr3PQOUTVALOR.Value;
      OutPad         := OutPes * CusPad;
      DvlPes         := Qr3PQDVLPESO.Value;
      DvlVal         := Qr3PQDVLVALOR.Value;
      DvlPad         := DvlPes * CusPad;
      BalPes         := Qr3PQBALPESO.Value;
      BalVal         := Qr3PQBALVALOR.Value;
      BalPad         := BalPes * CusPad;

      //FimPes         := 0;
      //FimVal         := 0;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, MoviPQ, False, [
      'Insumo', 'NomePQ', 'NomeFO',
      'NomeCI', 'NomeSE',
      'AntPes', 'AntVal', 'AntPad',
      'InnPes', 'InnVal', 'InnPad',
      'OutPes', 'OutVal', 'OutPad',
      'DvlPes', 'DvlVal', 'DvlPad',
      'BalPes', 'BalVal', 'BalPad',
      'FimPes', 'FimVal', 'FimPad'], [
      ], [
      Insumo, NomePQ, NomeFO,
      NomeCI, NomeSE,
      AntPes, AntVal, AntPad,
      InnPes, InnVal, InnPad,
      OutPes, OutVal, OutPad,
      DvlPes, DvlVal, DvlPad,
      BalPes, BalVal, BalPad,
      FimPes, FimVal, FimPad], [
      ], False);
      //DmkPF.LeMeuTexto(DModG.QrUpdPID1.SQL.Text);
      //
      Qr3PQ.Next;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'Tempo: ' + FormatDateTime('nn:ss', DModG.ObtemAgora() - Agora));
    UnDMkDAC_PF.AbreMySQLQuery0(QrMoviPQ, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + MoviPQ,
    _WHERE,
    ORDERBY,
    '']);
    //
    //Geral.MB_SQL(nil, QrMoviPQ);
    PBx.Position := 0;
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

(* 23/02/2015 => Aparentemente n�o usa
function TDMod.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
  FM_MASTER := 'F';
  Dmod.QrPerfis.Close;
  Dmod.QrPerfis.Params[0].AsInteger := Usuario;
  UnDmkDAC_PF.AbreQuery(Dmod.QrPerfis, MyDB);
  if Dmod.QrPerfis.RecordCount > 0 then
  begin
    Result := True;
    //FM_EQUIPAMENTOS             := Dmod.QrPerfisEquipamentos.Value;
    //FM_MARCAS          := Dmod.QrPerfisMarcas.Value;
    //FM_MODELOS                := Dmod.QrPerfisModelos.Value;
    //FM_SERVICOS            := Dmod.QrPerfisServicos.Value;
    //FM_PRODUTOS            := Dmod.QrPerfisProdutos.Value;
  end;
  //Dmod.QrPerfis.Close;
end;
*)

procedure TDmod.QrEndereco_CalcFields(DataSet: TDataSet);
{
var
  Natal: TDateTime;
}
begin
{
  QrEnderecoTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEnderecoTe1.Value);
  QrEnderecoFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEnderecoFax.Value);
  QrEnderecoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEnderecoCNPJ_CPF.Value);
  //
  QrEnderecoNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEnderecoNumero.Value, False);
  QrEnderecoE_ALL.Value := QrEnderecoNOMELOGRAD.Value;
  if Trim(QrEnderecoE_ALL.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ';
  QrEnderecoE_ALL.Value := QrEnderecoE_ALL.Value + QrEnderecoRua.Value;
  if Trim(QrEnderecoRua.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ', ' + QrEnderecoNUMERO_TXT.Value;
  if Trim(QrEnderecoCompl.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ' + QrEnderecoCompl.Value;
  if Trim(QrEnderecoBairro.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoBairro.Value;
  if QrEnderecoCEP.Value > 0 then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  if Trim(QrEnderecoCidade.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoCidade.Value;
  //
  QrEnderecoECEP_TXT.Value :=Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  //
  if QrEnderecoTipo.Value = 0 then
  begin
    Natal := QrEnderecoENatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CNPJ';
  end else begin
    Natal := QrEnderecoPNatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CPF';
  end;
  if Natal < 2 then QrEnderecoNATAL_TXT.Value := ''
  else QrEnderecoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
}
end;

procedure TDmod.DefineProperties(Filer: TFiler);
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

procedure TDmod.DefineUserSets_Painel(NomeForm, Definicao: String;
 Painel: TPanel);
begin
  QrUserSets.Close;
  QrUserSets.Params[0].AsString := NomeForm;
  QrUserSets.Params[1].AsString := Definicao;
  UnDmkDAC_PF.AbreQuery(QrUserSets, MyDB);
  //
  if QrUserSets.RecordCount > 0 then
  begin
    if QrUserSetsAbilitado.Value = 0 then Painel.Enabled := False;
    if QrUserSetsVisivel.Value = 0 then Painel.Visible := False;
  end else Geral.MB_Erro('"DefineUserSets_Painel" n�o definido ' + 'para "' +
    Definicao + '" em "' + NomeForm + '".');
end;

procedure TDmod.QrControleAfterOpen(DataSet: TDataSet);
//var
  //Pns: Integer;
begin
  FFmtPrc    := Dmod.QrControleCasasProd.Value;
  FStrFmtPrc := dmkPF.StrFmt_Double(Dmod.QrControleCasasProd.Value);
  FStrFmtCus := dmkPF.StrFmt_Double(4);
  //
  VAR_CliIntUnico     := QrControleDono.Value;
  FmPrincipal.FEntInt := QrControleDono.Value;
  //
  VAR_LOGODUPLPATH := QrControle.FieldByName('PathLogoDupl').AsString;
  if FileExists(VAR_LOGODUPLPATH) then VAR_LOGODUPLEXISTE := True
  else VAR_LOGODUPLEXISTE := False;

  // Arruma painel de bot�es
  if Dmod.QrControleVendaCouro.Value > 0 then
    FmPrincipal.PnVendaCouro.Visible := True;
  {
  Pns := 0;
  if Dmod.QrControleVendaCouro.Value > 0 then
  begin
    Pns := Pns + 1;
    FmPrincipal.PnCouros.Visible := True;
  end;
  Pns := Pns + 2;
  FmPrincipal.PnBotoes.Height := 2 + (84 * Pns);
  FmPrincipal.PnBotoes.Visible := FmPrincipal.PnBotoes.Height > 2;
  //
  }
  TMeuERP_CDR_DB       := LowerCase(QrControle.FieldByName('DBClarecoCDR').AsString);
  VAR_MySyncDB_NOME    := TMeuERP_CDR_DB;
  VAR_VSInsPalManu     := QrControleVSInsPalManu.Value;
  VAR_InfoMulFrnImpVS  := QrControleInfoMulFrnImpVS.Value;
  VAR_VSImpRandStr     := QrControle.FieldByName('VSImpRandStr').AsString;
  VAR_VSWarSemNF       := Dmod.QrControleVSWarSemNF.Value;
  VARF_NObrigNFeVS     := Dmod.QrControleNObrigNFeVS.Value;
  VAR_SetorCal         := QrControleSetorCal.Value;
  VAR_SetorCur         := QrControleSetorCur.Value;
  VAR_CtaComprUsoCons  := QrControleCtaPQCompr.Value;
  //VAR_CartComprUsoCons := QrControleMyPagCar.Value; ParamsEmp.UCCartPag

end;

procedure TDmod.QrDono_CalcFields(DataSet: TDataSet);
{
var
  Natal: TDateTime;
}
begin
{
  QrDonoTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoTe1.Value);
  QrDonoTE2_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoTe2.Value);
  QrDonoFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoFax.Value);
  QrDonoCEL_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoECel.Value);
  QrDonoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrDonoCNPJ_CPF.Value);
  //
  QrDonoNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(Trunc(QrDonoNumero.Value), False);
  QrDonoE_LNR.Value := QrDonoNOMELOGRAD.Value;
  if Trim(QrDonoE_LNR.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ';
  QrDonoE_LNR.Value := QrDonoE_LNR.Value + QrDonoRua.Value;
  if Trim(QrDonoRua.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ', ' + QrDonoNUMERO_TXT.Value;
  if Trim(QrDonoCompl.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ' + QrDonoCompl.Value;
  if Trim(QrDonoBairro.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' - ' + QrDonoBairro.Value;
  //
  QrDonoE_LN2.Value := '';
  if Trim(QrDonoCidade.Value) <>  '' then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + ' - '+QrDonoCIDADE.Value;
  QrDonoE_LN2.Value := QrDonoE_LN2.Value + ' - '+QrDonoNOMEUF.Value;
  if QrDonoCEP.Value > 0 then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + '  CEP ' +Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  QrDonoE_CUC.Value := QrDonoE_LNR.Value + QrDonoE_LN2.Value;
  //
  QrDonoECEP_TXT.Value :=Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  if QrDonoTipo.Value = 0 then Natal := QrDonoENatal.Value
  else Natal := QrDonoPNatal.Value;
  if Natal < 2 then QrDonoNATAL_TXT.Value := ''
  else QrDonoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
  //
  if Trim(QrDonoTE1.Value) <> '' then
    QrDonoFONES.Value := QrDonoTE1_TXT.Value;
  if Trim(QrDonoTe2.Value) <> '' then
    QrDonoFONES.Value := QrDonoFONES.Value + ' - ' + QrDonoTE2_TXT.Value;
}
end;

function TDmod.AtualizaEstoqueMarca(Controle: Integer): Boolean;
begin
  // Parei Aqui
  // No Futuro fazer:
  // Estoque de kg eliminando carna�a, quebra PDA > PTA etc.
  QrMPBxa.Close;
  QrMPBxa.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrMPBxa, MyDB);
  //
  QrUpdZ.SQL.Clear;
  QrUpdZ.SQL.Add('UPDATE mpin SET ');
  QrUpdZ.SQL.Add('PcBxa=:P0, kgBxa=:P1, M2Bxa=:P2, P2Bxa=:P3 ');
  QrUpdZ.SQL.Add('WHERE Controle=:pa');
  //
  QrUpdZ.Params[00].AsFloat   := QrMPBxaPecas.Value;
  QrUpdZ.Params[01].AsFloat   := QrMPBxaPeso.Value;
  QrUpdZ.Params[02].AsFloat   := QrMPBxaM2.Value;
  QrUpdZ.Params[03].AsFloat   := QrMPBxaP2.Value;
  //
  QrUpdZ.Params[04].AsInteger := Controle;
  QrUpdZ.ExecSQL;
  //
  Result := True;
end;

procedure TDmod.AtualizaEstoqueCECT(CECTInn, CECTOut: Integer);
begin
  if CECTOut <> 0 then
  begin
    QrSCECTOut.Close;
    QrSCECTOut.Params[0].AsInteger := CECTOut;
    UnDmkDAC_PF.AbreQuery(QrSCECTOut, MyDB);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpdZ, stUpd, 'cectout', False, [
      'TotQtdkg', 'TotQtdPc', 'TotQtdVal'
    ], ['Codigo'], [
      QrSCECTOutOutQtdkg.Value, QrSCECTOutOutQtdPc.Value, QrSCECTOutOutQtdVal.Value
    ], [CECTOut], True);
  end;
  //
  if CECTInn <> 0 then
  begin
    QrSCECTInn.Close;
    QrSCECTInn.Params[0].AsInteger := CECTInn;
    UnDmkDAC_PF.AbreQuery(QrSCECTInn, MyDB);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cectinn', False, [
      'SdoQtdkg', 'SdoQtdPc', 'SdoQtdVal'
    ], ['Codigo'], [
      QrSCECTInnPeso.Value, QrSCECTInnPeca.Value, QrSCECTInnValr.Value
    ], [CECTInn], True);
  end;
end;

procedure TDmod.AtualizaEstoqueCMPT(CMPTInn, CMPTOut: Integer);
const
  JaAtz = 1;
var
  SdoQtdkg, SdoQtdPc, SdoQtdVal, TotQtdkg, TotQtdPc, TotQtdM2, TotQtdVal,
  SdoFMOKg, SdoFMOVal, MOFatuKg, MOFatuVal: Double;
  Codigo: Integer;
begin
  if CMPTOut <> 0 then
  begin
(*
    QrSCMPTOut.Close;
    QrSCMPTOut.Params[0].AsInteger := CMPTOut;
    QrSCMPTOut . O p e n ;
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrSCMPTOut, Dmod.MyDB, [
    'SELECT SUM(OutQtdVal) OutQtdVal, ',
    'SUM(OutQtdkg) OutQtdkg, ',
    'SUM(OutQtdPc) OutQtdPc, ',
    'SUM(OutQtdM2) OutQtdM2,',
    'SUM(MOValor) MOValor, ',
    'SUM(MOPesoKg) MOPesoKg ',
    'FROM CMPToutits ',
    'WHERE Codigo=' + Geral.FF0(CMPTOut),
    '']);
    //
    TotQtdkg  := QrSCMPTOutOutQtdkg.Value;
    TotQtdPc  := QrSCMPTOutOutQtdPc.Value;
    TotQtdM2  := QrSCMPTOutOutQtdM2.Value;
    TotQtdVal := QrSCMPTOutOutQtdVal.Value;
    MOFatuKg  := QrSCMPTOutMOPesoKg.Value;
    MOFatuVal := QrSCMPTOutMOValor.Value;
    //
    Codigo := CMPTOut;
    UMyMod.SQLInsUpd(Dmod.QrUpdZ, stUpd, 'CMPTout', False, [
      'TotQtdkg', 'TotQtdPc', 'TotQtdM2',
      'TotQtdVal', 'JaAtz',
      'MOFatuKg', 'MOFatuVal'
    ], ['Codigo'], [
      TotQtdkg, TotQtdPc, TotQtdVal,
      TotQtdM2, JaAtz,
      MOFatuKg, MOFatuVal
    ], [Codigo], True);
  end;
  //
  if CMPTInn <> 0 then
  begin
(*
    QrSCMPTInn.Close;
    QrSCMPTInn.Params[0].AsInteger := CMPTInn;
    QrSCMPTInn . O p e n  ;
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrSCMPTInn, Dmod.MyDB, [
    'SELECT ',
    'IF(SUM(MOValor) > 0, inn.ValMaoObra - SUM(MOValor), inn.ValMaoObra) SdoFMOVal,',
    'IF(SUM(MOPesoKg) > 0, IF(inn.HowCobrMO=0, inn.InnQtdkg, inn.PLEKg) - SUM(MOPesoKg), ',
    '  IF(inn.HowCobrMO=0, inn.InnQtdkg, inn.PLEKg)) SdoFMOKg,',
    'IF(SUM(its.OutQtdVal) > 0, inn.InnQtdVal - SUM(its.OutQtdVal), inn.InnQtdVal) Valr,',
    'IF(SUM(its.OutQtdkg)  > 0, inn.InnQtdkg  - SUM(its.OutQtdkg),  inn.InnQtdkg)  Peso,',
    'IF(SUM(its.OutQtdPc)  > 0, inn.InnQtdPc  - SUM(its.OutQtdPc),  inn.InnQtdPc)  Peca',
    'FROM CMPTinn inn ',
    'LEFT JOIN CMPToutits its ON inn.Codigo=its.CMPTInn',
    'WHERE inn.Codigo=' + Geral.FF0(CMPTInn),
    'GROUP BY inn.Codigo',
    '']);
    //
    SdoQtdkg  := QrSCMPTInnPeso.Value;
    SdoQtdPc  := QrSCMPTInnPeca.Value;
    SdoQtdVal := QrSCMPTInnValr.Value;
    SdoFMOKg  := QrSCMPTInnSdoFMOKg.Value;
    SdoFMOVal := QrSCMPTInnSdoFMOVal.Value;
    //
    Codigo := CMPTInn;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'CMPTinn', False, [
      'SdoQtdkg', 'SdoQtdPc', 'SdoQtdVal',
      'JaAtz', 'SdoFMOKg', 'SdoFMOVal'
    ], ['Codigo'], [
      SdoQtdkg, SdoQtdPc, SdoQtdVal,
      JaAtz, SdoFMOKg, SdoFMOVal
    ], [Codigo], True);
  end;
end;

{
procedure TDmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01: T_A_B_S_Query;
DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
CliInt: Integer; Campos: String; DescrSubstit: Boolean);
var
  Linha, Debito, Credito, NOMECONTA3: String;
  Data: TDateTime;
  Documento, NotaFiscal: Integer;
begin
  //Geral.MB_Aviso('Este balancete n�o possui dados industriais ' +
  //'adicionais neste aplicativo!', 'Aviso', MB_OK+MB_ICONWARNING);
  QrUsoPQ.Close;
  QrUsoPQ.SQL.Clear;
  QrUsoPQ.SQL.Add('SELECT SUM(Valor) Valor');
  QrUsoPQ.SQL.Add('FROM pqx');
  QrUsoPQ.SQL.Add('WHERE Valor < 0');
  if CliInt <> 0 then
    QrUsoPQ.SQL.Add('AND CliDest=' + FormatFloat('0', CliInt));
  //QrUsoPQ.SQL.Add('AND DataX BETWEEN :P1 AND :P2');
  QrUsoPQ.SQL.Add(dmkPF.SQL_Periodo('AND DataX ',
    DataIDate, DataFDate, DataITrue, DataFTrue));
  UnDmkDAC_PF.AbreQuery(QrUsoPQ, MyDB);
  //
  Debito     := dmkPF.FFP(- QrUsoPQValor.Value, 2);
  Credito    := dmkPF.FFP(0, 2);
  Data       := Date; // zero: ocorre erro.
  Documento  := 0;
  NotaFiscal := 0;
  //
  QrPQCompr.Close;
  UnDmkDAC_PF.AbreQuery(QrPQCompr, MyDB);
  if DescrSubstit then
    NOMECONTA3 := QrPQComprNOMECONTA2.Value
  else
    NOMECONTA3 := QrPQComprNOMECONTA.Value;
  if NOMECONTA3 = '' then NOMECONTA3 := 'Consumo de insumos qu�micos';
   
  //
  Linha := Campos + Debito + ', ' + Credito +
  ', "' + QrPQComprNOMECONTA.Value + '"' +
  ', "' + QrPQComprNOMECONTA2.Value + '"' +
  ', "' + QrPQComprNOMESUBGRUPO.Value + '"' +
  ', "' + QrPQComprNOMEGRUPO.Value + '"' +
  ', "' + QrPQComprNOMECONJUNTO.Value + '"' +
  ', "' + NOMECONTA3 + '"' +
  ', "' + Geral.FDT(Data, 1) + '"' +
  ',  ' + FormatFloat('0', Documento) +
  ',  ' + FormatFloat('0', NotaFiscal) +

  ',  ' + FormatFloat('0', QrPQComprSubgrupo.Value) +
  ',  ' + FormatFloat('0', QrPQComprGrupo.Value) +
  ',  ' + FormatFloat('0', QrPQComprConjunto.Value) +

  ',  ' + FormatFloat('0', QrPQComprcjOrdemLista.Value) +
  ',  ' + FormatFloat('0', QrPQComprgrOrdemLista.Value) +
  ',  ' + FormatFloat('0', QrPQComprsgOrdemLista.Value) +
  ',  ' + FormatFloat('0', QrPQComprcoOrdemLista.Value) +
  ');';
  QrInd01.SQL.Add(Linha);
  //
end;
}

function TDmod.ImpedePeloRetornoPQ(OrigemCodi, OrigemCtrl,
  Tipo: Integer; Avisa: Boolean): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(MyDB);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Peso ',
    'FROM pqrits ',
    'WHERE (',
    '    OriCodiInn=' + Geral.FF0(OrigemCodi),
    '    AND OriCtrlInn=' + Geral.FF0(OrigemCtrl),
    '    AND TipoInn=' + Geral.FF0(Tipo),
    ') OR (',
    '    OriCodiOut=' + Geral.FF0(OrigemCodi),
    '    AND OriCtrlOut=' + Geral.FF0(OrigemCtrl),
    '    AND TipoOut=' + Geral.FF0(Tipo),
    ')',
    '']);
    //
    Result := Qry.RecordCount > 0;
    if Result and Avisa then
      Geral.MB_Aviso('A��o cancelada!' + sLineBreak +
      'Existem ' + Geral.FF0(Qry.RecordCount) +
      ' itens de retorno de insumo impedidindo!');
  finally
    Qry.Free;
  end;
end;

procedure TDmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
CliInt: Integer; Campos: String; DescrSubstit: Boolean);
var
  Linha, Debito, Credito, NOMECONTA3: String;
  Data: TDateTime;
  Documento, NotaFiscal: Integer;
begin
  //Geral.MB_Aviso('Este balancete n�o possui dados industriais ' +
  //'adicionais neste aplicativo!', 'Aviso', MB_OK+MB_ICONWARNING);
  QrUsoPQ.Close;
  QrUsoPQ.SQL.Clear;
  QrUsoPQ.SQL.Add('SELECT SUM(Valor) Valor');
  QrUsoPQ.SQL.Add('FROM pqx');
  QrUsoPQ.SQL.Add('WHERE Valor < 0');
  if CliInt <> 0 then
    QrUsoPQ.SQL.Add('AND CliDest=' + FormatFloat('0', CliInt));
  //QrUsoPQ.SQL.Add('AND DataX BETWEEN :P1 AND :P2');
  QrUsoPQ.SQL.Add(dmkPF.SQL_Periodo('AND DataX ',
    DataIDate, DataFDate, DataITrue, DataFTrue));
  UnDmkDAC_PF.AbreQuery(QrUsoPQ, MyDB);
  //
  Debito     := dmkPF.FFP(- QrUsoPQValor.Value, 2);
  Credito    := dmkPF.FFP(0, 2);
  Data       := Date; // zero: ocorre erro.
  Documento  := 0;
  NotaFiscal := 0;
  //
  QrPQCompr.Close;
  UnDmkDAC_PF.AbreQuery(QrPQCompr, MyDB);
  if DescrSubstit then
    NOMECONTA3 := QrPQComprNOMECONTA2.Value
  else
    NOMECONTA3 := QrPQComprNOMECONTA.Value;
  if NOMECONTA3 = '' then NOMECONTA3 := 'Consumo de insumos qu�micos';
   
  //
  Linha := Campos + Debito + ', ' + Credito +
  ', "' + QrPQComprNOMECONTA.Value + '"' +
  ', "' + QrPQComprNOMECONTA2.Value + '"' +
  ', "' + QrPQComprNOMESUBGRUPO.Value + '"' +
  ', "' + QrPQComprNOMEGRUPO.Value + '"' +
  ', "' + QrPQComprNOMECONJUNTO.Value + '"' +
  ', "' + NOMECONTA3 + '"' +
  ', "' + Geral.FDT(Data, 1) + '"' +
  ',  ' + FormatFloat('0', Documento) +
  ',  ' + FormatFloat('0', NotaFiscal) +

  ',  ' + FormatFloat('0', QrPQComprSubgrupo.Value) +
  ',  ' + FormatFloat('0', QrPQComprGrupo.Value) +
  ',  ' + FormatFloat('0', QrPQComprConjunto.Value) +

  ',  ' + FormatFloat('0', QrPQComprcjOrdemLista.Value) +
  ',  ' + FormatFloat('0', QrPQComprgrOrdemLista.Value) +
  ',  ' + FormatFloat('0', QrPQComprsgOrdemLista.Value) +
  ',  ' + FormatFloat('0', QrPQComprcoOrdemLista.Value) +
  ');';
  QrInd01.SQL.Add(Linha);
  //
end;

procedure TDmod.InsereWBMovCab(Codigo: Integer; MovimID: TEstqMovimID;
CodigoID: Integer);
begin
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'wbmovcab', False, [
  'MovimID', 'CodigoID'], [
  'Codigo'], [
  MovimID, CodigoID], [
  Codigo], True);
end;

function TDmod.InsUpdWBMovIts(SQLType: TSQLType; Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro: Integer; MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv;
  Pallet, GraGruX: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataHora: String; SrcMovID, SrcNivel1, SrcNivel2: Integer;
  Observ: String; LnkNivXtr1, LnkNivXtr2, CliVenda, Controle: Integer): Boolean;
begin
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'wbmovits', False, [
  'Codigo', 'MovimCod', 'Empresa',
  'MovimID', 'MovimNiv',
  'Pallet', 'GraGruX',
  'Pecas', 'PesoKg', 'AreaM2',
  'AreaP2', 'ValorT', 'Terceiro',
  'SrcMovID', 'SrcNivel1', 'SrcNivel2',
  'DataHora', 'Observ', 'CliVenda',
  'LnkNivXtr1', 'LnkNivXtr2', 'MovimTwn'], [
  'Controle'], [
  Codigo, MovimCod, Empresa,
  Integer(MovimID), Integer(MovimNiv),
  Pallet, GraGruX,
  Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, Terceiro,
  SrcMovID, SrcNivel1, SrcNivel2,
  DataHora, Observ, CliVenda,
  LnkNivXtr1, LnkNivXtr2, MovimTwn], [
  Controle], True);
end;

(*  mudado para Grade_PF.AlteraSMIA(
procedure TDmod.AlteraSMIA(Qry: TmySQLQuery);
var
  Tipo, IDCtrl, GraGruX, StqCenCad: Integer;
  Qtde, Pecas, AreaM2, AreaP2, Peso: Double;
begin
  Tipo := Qry.FieldByName('Tipo').AsInteger;
  IDCtrl    := Qry.FieldByName('IDCtrl').AsInteger;
  GraGruX   := Qry.FieldByName('GraGruX').AsInteger;
  StqCenCad := Qry.FieldByName('StqCenCad').AsInteger;
  case Tipo of
    VAR_FATID_0101:
    begin
      Qtde      := Qry.FieldByName('Qtde').AsFloat;
      Pecas     := Qry.FieldByName('Pecas').AsFloat;
      AreaM2    := Qry.FieldByName('AreaM2').AsFloat;
      AreaP2    := Qry.FieldByName('AreaP2').AsFloat;
      Peso      := Qry.FieldByName('Peso').AsFloat;
    end;
    VAR_FATID_0104:
    begin
      Qtde      := - Qry.FieldByName('Qtde').AsFloat;
      Pecas     := - Qry.FieldByName('Pecas').AsFloat;
      AreaM2    := - Qry.FieldByName('AreaM2').AsFloat;
      AreaP2    := - Qry.FieldByName('AreaP2').AsFloat;
      Peso      := - Qry.FieldByName('Peso').AsFloat;
    end;
    else begin
      Geral.MB_Aviso('Altera��o cancelada!' + sLineBreak +
      'Tipo de lan�amento n�o implementado: ' + FormatFloat('0', Tipo) + sLineBreak
      + 'AVISE A DERMATEK!');
      Exit;
    end;
  end;
  if DBCheck.CriaFm(TFmSMI_Edita, FmSMI_Edita, afmoNegarComAviso) then
  begin
    FmSMI_Edita.FNegativos := Tipo = VAR_FATID_0104;
    //
    FmSMI_Edita.QrSMIA.Close;
    FmSMI_Edita.QrSMIA.Params[0].AsInteger := IDCtrl;
    UnDmkDAC_PF.AbreQuery(FmSMI_Edita.QrSMIA, MyDB);
    //
    FmSMI_Edita.EdGraGruX.ValueVariant   := GraGruX;
    FmSMI_Edita.CBGraGruX.KeyValue       := GraGruX;
    //
    FmSMI_Edita.EdStqCenCad.ValueVariant := StqCenCad;
    FmSMI_Edita.CBStqCenCad.KeyValue     := StqCenCad;

    FmSMI_Edita.EdQtde.ValueVariant      := Qtde;
    FmSMI_Edita.EdPecas.ValueVariant     := Pecas;
    FmSMI_Edita.EdAreaM2.ValueVariant    := AreaM2;
    FmSMI_Edita.EdAreaP2.ValueVariant    := AreaP2;
    FmSMI_Edita.EdPeso.ValueVariant      := Peso;
    //
    FmSMI_Edita.ShowModal;
    FmSMI_Edita.Destroy;
  end;
end;
*)

function TDMod.AtualizaCustosEmit_1(Emit: Integer): Boolean;
var
  SumC, PesoS, Fator, CustoL: Double;
begin
  QrSumCusRib1.Close;
  QrSumCusRib1.Params[0].AsInteger := Emit;
  UnDmkDAC_PF.AbreQuery(QrSumCusRib1, MyDB);
  SumC := -QrSumCusRib1Valor.Value;
  if SumC <= 0 then
  begin
    Geral.MB_Aviso('Valor do custo da pesagem ' +
    IntToStr(Emit) + ' n�o definido! [2]');
    Result := False;
    Exit;
  end;
  QrUpdM.SQL.Clear;
  QrUpdM.SQL.Add('UPDATE emit SET Custo=:P0, Status=1 WHERE Codigo=:P1');
  QrUpdM.Params[0].AsFloat   := SumC;
  QrUpdM.Params[1].AsInteger := Emit;
  QrUpdM.ExecSQL;
  //
  QrSumKgsRib.Close;
  QrSumKgsRib.Params[0].AsInteger := Emit;
  UnDmkDAC_PF.AbreQuery(QrSumKgsRib, MyDB);
  PesoS := QrSumKgsRibPeso.Value;
  //
  if (PesoS > 0) then
    Fator := SumC / PesoS
  else
    Fator := 0;
  QrUpd.SQL.Clear;
  QrUpd.SQL.Add('UPDATE emitcus SET ');
  QrUpd.SQL.Add('Custo=:P0 WHERE Controle=:P1 ');
(*
  QrLotes.Close;
  QrLotes.Params[0].AsInteger := Emit;
  QrLotes.O p e n ;
*)
  ReopenLotes(Emit);
  while not QrLotes.Eof do
  begin
    CustoL := QrLotesPeso.Value * Fator;
    //
    QrUpd.Params[00].AsFloat   := CustoL;
    QrUpd.Params[01].AsInteger := QrLotesControle.Value;
    QrUpd.ExecSQL;
    //
    Qrlotes.Next;
  end;
  Result := True;
end;

function TDMod.AtualizaCustosEmit_2(Emit: Integer): Boolean;
var
  SumC, PesoS, Fator, CustoL: Double;
begin
  QrSumCusRib2.Close;
  QrSumCusRib2.Params[00].AsInteger := VAR_FATID_1110;
  QrSumCusRib2.Params[01].AsInteger := VAR_FATID_1111;
  QrSumCusRib2.Params[02].AsInteger := Emit;
  UnDmkDAC_PF.AbreQuery(QrSumCusRib2, MyDB);
  SumC := -QrSumCusRib2CustoAll.Value;
  if SumC <= 0 then
  begin
    Geral.MB_Aviso('Valor do custo da pesagem ' +
    IntToStr(Emit) + ' n�o definido! [3]');
    Result := False;
    Exit;
  end;
  QrUpdM.SQL.Clear;
  QrUpdM.SQL.Add('UPDATE emit SET Custo=:P0, Status=1 WHERE Codigo=:P1');
  QrUpdM.Params[0].AsFloat   := SumC;
  QrUpdM.Params[1].AsInteger := Emit;
  QrUpdM.ExecSQL;
  //
  QrSumKgsRib.Close;
  QrSumKgsRib.Params[0].AsInteger := Emit;
  UnDmkDAC_PF.AbreQuery(QrSumKgsRib, MyDB);
  PesoS := QrSumKgsRibPeso.Value;
  //
  if (PesoS > 0) then
    Fator := SumC / PesoS
  else
    Fator := 0;
  QrUpd.SQL.Clear;
  QrUpd.SQL.Add('UPDATE emitcus SET ');
  QrUpd.SQL.Add('Custo=:P0 WHERE Controle=:P1 ');
(*
  QrLotes.Close;
  QrLotes.Params[0].AsInteger := Emit;
  UnDmkDAC_PF.AbreQuery(QrLotes, MyDB);
*)
  ReopenLotes(Emit);
  while not QrLotes.Eof do
  begin
    CustoL := QrLotesPeso.Value * Fator;
    //
    QrUpd.Params[00].AsFloat   := CustoL;
    QrUpd.Params[01].AsInteger := QrLotesControle.Value;
    QrUpd.ExecSQL;
    //
    Qrlotes.Next;
  end;
  Result := True;
end;

function TDmod.AtualizaMPIn(Controle: Integer): Boolean;
const
  TipoStqMovIts = VAR_FATID_0104;
var
  Qtde, Pecas, Peso, AreaM2, AreaP2: Double;
  EstqPC, EstqM2, EstqP2, EstqKg: Double;
  Encerrado: Integer;
//var
  PecasCal, PecasCur, PecasNeg,
  PesoCal, PesoCur, CusPQCal, CusPQCur, CustoPQ: Double;
  MinDtaCal, MinDtaCur, MinDtaEnx,
  MaxDtaCal, MaxDtaCur, MaxDtaEnx: String;
  FuloesCal, FuloesCur: Integer;
begin
  //Result := False;
  //
  AtualizaStqMovIts_MPI(nil);
  //
  QrMPIn.Close;
  QrMPIn.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrMPIn, MyDB);
  //
  QrSumCus.Close;
  QrSumCus.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrSumCus, MyDB);
  //
  QrSPerdas.Close;
  QrSPerdas.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrSPerdas, MyDB);
  PecasNeg := QrSPerdasPecas.Value;
  //
  QrSCus2.Close;
  QrSCus2.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrSCus2, MyDB);
  //
  PecasCal  := 0;
  PecasCur  := 0;
  PesoCal   := 0;
  PesoCur   := 0;
  CusPQCal  := 0;
  CusPQCur  := 0;
  FuloesCal := 0;
  FuloesCur := 0;
  MinDtaCal := '0000-00-00';
  MinDtaCur := '0000-00-00';
  MinDtaEnx := '0000-00-00';
  MaxDtaCal := '0000-00-00';
  MaxDtaCur := '0000-00-00';
  MaxDtaEnx := '0000-00-00';
  while not QrSCus2.Eof do
  begin
    if QrSCus2Setor.Value = Dmod.QrControleSetorCal.Value then
    begin
      PecasCal  := PecasCal  + QrSCus2Pecas.Value;
      PesoCal   := PesoCal   + QrSCus2Peso.Value;
      CusPQCal  := CusPQCal  + QrSCus2Custo.Value;
      FuloesCal := FuloesCal + QrSCus2Fuloes.Value;
      MinDtaCal := Geral.FDT(QrSCus2MinDta.Value, 1);
      MaxDtaCal := Geral.FDT(QrSCus2MaxDta.Value, 1);
    end else
    if QrSCus2Setor.Value = Dmod.QrControleSetorCur.Value then
    begin
      PecasCur  := PecasCur  + QrSCus2Pecas.Value;
      PesoCur   := PesoCur   + QrSCus2Peso.Value;
      CusPQCur  := CusPQCur  + QrSCus2Custo.Value;
      FuloesCur := FuloesCur + QrSCus2Fuloes.Value;
      MinDtaCur := Geral.FDT(QrSCus2MinDta.Value, 1);
      MaxDtaCur := Geral.FDT(QrSCus2MaxDta.Value, 1);
    end
    else Geral.MB_Aviso('Setor inv�lido para caleiro ou ' +
    'curtimento na defini��o estoque de couros na emiss�o de pesagem!');
    //
    QrSCus2.Next;
  end;
{
  Encerrado := QrMPInForcaEncer.Value;
  if Encerrado = 0  then
    if (PecasExp + PecasNeg >=  QrMPInPecas.Value)
    and (QrMPInPecas.Value > 0) then
      Encerrado := 1;
}
  //

  //if
  DmProd.ObtemQuantidadesBaixa(TipoStqMovIts, Controle, Qtde, Pecas, Peso, AreaM2, AreaP2);// then
//  begin
  Encerrado := QrMPInForcaEncer.Value;
  //
  if Encerrado = 0  then
    if (Pecas + PecasNeg >=  QrMPInPecas.Value) and (QrMPInPecas.Value > 0) then
      Encerrado := 1;
  //
  EstqPC := 0;
  EstqM2 := 0;
  EstqP2 := 0;
  EstqKg := 0;
  //
  if (Encerrado = 0) then
  begin
    EstqPC := QrMPInPecas.Value - QrMPInPecasNeg.Value - Pecas;
    if EstqPC > 0 then
    begin
      EstqM2 := QrMPInM2.Value - AreaM2;
      //
      if EstqM2 < 0 then EstqM2 := 0;
      //
      EstqP2 := QrMPInP2.Value - AreaP2;
      //
      if EstqP2 < 0 then EstqP2 := 0;
      //
      EstqKg := QrMPInPLE.Value - Peso;
      //
      if EstqKg < 0 then EstqKg := 0;
      //
    end else EstqPC := 0;
  end;
  //
  CustoPQ := CusPQCal + CusPQCur;
  //
  UMyMod.SQLInsUpd(QrUpd, stUpd, 'mpin', False, [
    'PecasCal', 'PecasCur', 'PecasNeg',
    'PesoCal', 'PesoCur', 'CusPQCal', 'CusPQCur',
    'MinDtaCal', 'MinDtaCur', 'MinDtaEnx',
    'MaxDtaCal', 'MaxDtaCur', 'MaxDtaEnx',
    'FuloesCal', 'FuloesCur',
    'PecasExp', 'M2Exp', 'P2Exp', 'PesoExp',
    'Estq_Pecas', 'Estq_M2', 'Estq_P2', 'Estq_Peso',
    'CustoPQ', 'Encerrado'
  ], ['Controle'], [
    PecasCal, PecasCur, PecasNeg,
    PesoCal, PesoCur, CusPQCal, CusPQCur,
    MinDtaCal, MinDtaCur, MinDtaEnx,
    MaxDtaCal, MaxDtaCur, MaxDtaEnx,
    FuloesCal, FuloesCur,
    Pecas, AreaM2, AreaP2, Peso,
    EstqPC, EstqM2, EstqP2, EstqKg,
    CustoPQ, Encerrado
  ], [Controle], True);
  //
  Result := True;
end;

procedure TDmod.AtualizaPrecosGraGruVal1(PQ: Integer);
var
  Peso, Valor, Parc, UltimaCompra, PrecoMedio: Double;
  GraCusPrc, GraGruX, GraGru1, Controle, Entidade: Integer;
  CustoPreco: Double;
  SQLType: TSQLType;
begin
  GraGru1 := PQ;
  //
  QrInPQ.Close;
  QrInPQ.Params[0].AsInteger := PQ;
  UnDmkDAC_PF.AbreQuery(QrInPQ, MyDB);
  //
  QrNiv1.Close;
  QrNiv1.Params[0].AsInteger := GraGru1;
  UnDmkDAC_PF.AbreQuery(QrNiv1, MyDB);
  //
  QrInPQ.First;
  while not QrInPQ.Eof do
  begin
    Entidade := QrInPQCI.Value;
    Peso  := QrInPQTotalPeso.Value;
    Valor := QrInPQTotalCusto.Value;
    if Peso = 0 then UltimaCompra := 0 else
    UltimaCompra := Valor / Peso;
    //
    if Peso < QrNiv1QTDE.Value then
    begin
      QrInPQ.Next;
      while not QrInPQ.Eof do
      begin
        if Peso + QrInPQTotalPeso.Value > QrNiv1QTDE.Value then
        begin
          Parc := (QrInPQTotalCusto.Value / QrInPQTotalPeso.Value) * (QrNiv1QTDE.Value - Peso);
          Valor := Valor + Parc;
          Peso  := QrNiv1QTDE.Value;
          Break;
        end else begin
          Peso  := Peso + QrInPQTotalPeso.Value;
          Valor := Valor + QrInPQTotalCusto.Value;
        end;
        QrInPQ.Next;
      end;
    end;
    if Peso = 0 then PrecoMedio := 0 else PrecoMedio := Valor / Peso;
    GraGruX := QrNiv1GraGruX.Value;
    //
    // Atualizar �ltima compra
    if UltimaCompra > 0 then
    begin
      CustoPreco := UltimaCompra;
      GraCusPrc  := 4; // �ltima compra
      QrGraGruVal.Close;
      QrGraGruVal.Params[00].AsInteger := GraGruX;
      QrGraGruVal.Params[01].AsInteger := GraCusPrc;
      QrGraGruVal.Params[02].AsInteger := Entidade;
      UnDmkDAC_PF.AbreQuery(QrGraGruVal, MyDB);
      //
      Controle := QrGraGruValControle.Value;
      if Controle = 0 then
      begin
        Controle := UMyMod.BuscaEmLivreY_Def('gragruval', 'Controle', stIns, 0);
        SQLType  := stIns;
      end else begin
        SQLType  := stUpd;
      end;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
        'GraGruX', 'GraGru1', 'GraCusPrc', 'CustoPreco', 'Entidade'], ['Controle'], [
        GraGruX, GraGru1, GraCusPrc, CustoPreco, Entidade], [Controle], True) then ;
    end;
    //
    // Atualizar Pre�o m�dio
    if PrecoMedio > 0 then
    begin
      CustoPreco := PrecoMedio;
      GraCusPrc  := 5; // Pre�o m�dio
      QrGraGruVal.Close;
      QrGraGruVal.Params[00].AsInteger := GraGruX;
      QrGraGruVal.Params[01].AsInteger := GraCusPrc;
      QrGraGruVal.Params[02].AsInteger := Entidade;
      UnDmkDAC_PF.AbreQuery(QrGraGruVal, MyDB);
      //
      Controle := QrGraGruValControle.Value;
      if Controle = 0 then
      begin
        Controle := UMyMod.BuscaEmLivreY_Def('gragruval', 'Controle', stIns, 0);
        SQLType  := stIns;
      end else begin
        SQLType  := stUpd;
      end;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
      'GraGruX', 'GraGru1', 'GraCusPrc', 'CustoPreco', 'Entidade'], ['Controle'], [
      GraGruX, GraGru1, GraCusPrc, CustoPreco, Entidade], [Controle], True) then ;
    end;
    //
    QrInPQ.Next;
  end;
end;

procedure TDmod.AtualizaPrecosGraGruVal1_All(PB: TProgressBar);
begin
  Screen.Cursor := crHourGlass;
  QrPQEMed.Close;
  UnDmkDAC_PF.AbreQuery(QrPQEMed, MyDB);
  PB.Position := 0;
  PB.Max := QrPQEMed.RecordCount;
  PB.Update;
  Application.ProcessMessages;
  while not QrPQEMed.Eof do
  begin
    PB.Position := PB.Position + 1;
    PB.Update;
    Application.ProcessMessages;
    //
    AtualizaPrecosGraGruVal1(QrPQEMedInsumo.Value);
    QrPQEMed.Next;
  end;
  Screen.Cursor := crDefault;
  Geral.MB_Info('Atualiza��o finalizada!');
  PB.Position := 0;
end;

{
procedure TDmod.RecalculaCustoPQdeMP(MPInControle: Integer);
var
  PecasCal, PecasCur, PecasExp, PecasNeg, M2Exp, P2Exp,
  PesoCal, PesoCur, CusPQCal, CusPQCur, CustoPQ: Double;
  MinDtaCal, MinDtaCur, MinDtaEnx,
  MaxDtaCal, MaxDtaCur, MaxDtaEnx: String;
  FuloesCal, FuloesCur, Encerrado: Integer;
begin
  QrMPIn.Close;
  QrMPIn.Params[0].AsInteger := MPInControle;
  UnDmkDAC_PF.AbreQuery(QrMPin, MyDB);
  //
  QrSumCus.Close;
  QrSumCus.Params[0].AsInteger := MPInControle;
  UnDmkDAC_PF.AbreQuery(QrSumCus, MyDB);
  //
  QrSPerdas.Close;
  QrSPerdas.Params[0].AsInteger := MPInControle;
  UnDmkDAC_PF.AbreQuery(QrSPerdas, MyDB);
  PecasNeg := QrSPerdasPecas.Value;
  //
  QrSExp.Close;
  QrSExp.Params[0].AsInteger := MPInControle;
  UnDmkDAC_PF.AbreQuery(QrSExp, MyDB);
  PecasExp := QrSExpPecas.Value;
  M2Exp    := QrSExpM2.Value;
  P2Exp    := QrSExpP2.Value;
  //
  QrSCus2.Close;
  QrSCus2.Params[0].AsInteger := MPInControle;
  UnDmkDAC_PF.AbreQuery(QrSCus2, MyDB);
  //
  PecasCal  := 0;
  PecasCur  := 0;
  PesoCal   := 0;
  PesoCur   := 0;
  CusPQCal  := 0;
  CusPQCur  := 0;
  FuloesCal := 0;
  FuloesCur := 0;
  while not QrSCus2.Eof do
  begin
    if QrSCus2Setor.Value = Dmod.QrControleSetorCal.Value then
    begin
      PecasCal  := PecasCal  + QrSCus2Pecas.Value;
      PesoCal   := PesoCal   + QrSCus2Peso.Value;
      CusPQCal  := CusPQCal  + QrSCus2Custo.Value;
      FuloesCal := FuloesCal + QrSCus2Fuloes.Value;
      MinDtaCal := Geral.FDT(QrSCus2MinDta.Value, 1);
      MaxDtaCal := Geral.FDT(QrSCus2MaxDta.Value, 1);
    end else
    if QrSCus2Setor.Value = Dmod.QrControleSetorCur.Value then
    begin
      PecasCur  := PecasCur  + QrSCus2Pecas.Value;
      PesoCur   := PesoCur   + QrSCus2Peso.Value;
      CusPQCur  := CusPQCur  + QrSCus2Custo.Value;
      FuloesCur := FuloesCur + QrSCus2Fuloes.Value;
      MinDtaCur := Geral.FDT(QrSCus2MinDta.Value, 1);
      MaxDtaCur := Geral.FDT(QrSCus2MaxDta.Value, 1);
    end
    else Geral.MB_Aviso('Setor inv�lido para caleiro ou ' +
    'curtimento na defini��o estoque de couros na emiss�o de pesagem!');
    //
    QrSCus2.Next;
  end;
  Encerrado := QrMPInForcaEncer.Value;
  if Encerrado = 0  then
    if (PecasExp + PecasNeg >=  QrMPInPecas.Value)
    and (QrMPInPecas.Value > 0) then
      Encerrado := 1;
  //
  CustoPQ := CusPQCal + CusPQCur;
  UMyMod.SQLInsUpd(QrUpd, stUpd, 'mpin', False, [
    'PecasCal', 'PecasCur', 'PecasExp', 'PecasNeg',
    'PesoCal', 'PesoCur', 'CusPQCal', 'CusPQCur',
    'MinDtaCal', 'MinDtaCur', 'MinDtaEnx',
    'MaxDtaCal', 'MaxDtaCur', 'MaxDtaEnx',
    'FuloesCal', 'FuloesCur',
    'M2Exp', 'P2Exp', 'Encerrado',
    'CustoPQ'
  ], ['Controle'], [
    PecasCal, PecasCur, PecasExp, PecasNeg,
    PesoCal, PesoCur, CusPQCal, CusPQCur,
    MinDtaCal, MinDtaCur, MinDtaEnx,
    MaxDtaCal, MaxDtaCur, MaxDtaEnx,
    FuloesCal, FuloesCur,
    M2Exp, P2Exp, Encerrado,
    CustoPQ
  ], [MPInControle], True);
  /
end;
}

procedure TDmod.AtualizaStqMovIts_PQX(PB: TProgressBar; LaAviso1, LaAviso2: TLabel);

////////////////////////////////////////////////////////////////////////////////
/// Desabilitado por causa da importa��o do SPED EFD  2011-06-14             ///
////////////////////////////////////////////////////////////////////////////////

{
const
  ParTipo = GRADE_TABS_PARTIPO_0001; // Identifica o par no PQX e no StqMovItsA(B)
var
  NFe_FatID, OriCodi, OriCtrl, OriCnta, OriPart, Empresa, StqCenCad, GraGruX,
  QuemUsou, IDCtrl, Retorno, ParCodi, StqMovIts, Tipo, TipoNew: Integer;
  Qtde, Pecas, Peso, AreaM2, AreaP2, FatorClas, CustoAll: Double;
  DataHora: String;
  //
  MyCursor: TCursor;
}
begin
{
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
(*
    // refazer itens sem CustoAll
    // N�o usar! Exclui lan ctos no REPLACE posterior
    QrUpd.SQL.Clear;
    QrUpd.SQL.Add('UPDATE pqx pqx, stqmovitsa smia');
    QrUpd.SQL.Add('SET pqx.StqMovIts=0');
    QrUpd.SQL.Add('WHERE smia.IDCtrl=pqx.StqMovIts');
    QrUpd.SQL.Add('AND smia.CustoAll=0');
    QrUpd.ExecSQL;
    // FIM refazer itens sem CustoAll
*)
    //
    QrGGX.Close;
    QrGGX.SQL.Clear;
    QrGGX.SQL.Add('SELECT ggx.Controle');
    QrGGX.SQL.Add('FROM pqx pqx');
    QrGGX.SQL.Add('LEFT JOIN gragrux ggx ON ggx.GraGru1=pqx.Insumo');// AND ggx.GraTamI=1');
    QrGGX.SQL.Add('WHERE pqx.Tipo IN (:P0, :P1)');
    QrGGX.SQL.Add('AND Insumo=:P2');
    QrGGX.SQL.Add('ORDER BY Controle');
    //
    QrPQG.Close;
    UnDmkDAC_PF.AbreQuery(QrPQG, MyDB);
    //
    if PB <> nil then
    begin
      PB.Position := 0;
      PB.Max := QrPQG.RecordCount;
    end;
    QrPQG.First;
    while not QrPQG.Eof do
    begin
      MyObjects.UpdPB(PB, LaAviso1, LaAviso2);
      (*
      if PB <> nil then
      begin
        PB.Position := PB.Position + 1;
        PB.Update;
      end;
      *)
      Application.ProcessMessages;
      //
      case QrPQGTipo.Value of
         -5: NFe_FatID := VAR_FATID__005; // \
         -4: NFe_FatID := VAR_FATID__004; //  |
         -3: NFe_FatID := VAR_FATID__003; //  | Retroativo (Minerva no Colorado!)
         -2: NFe_FatID := VAR_FATID__002; //  |
         -1: NFe_FatID := VAR_FATID__006; // /
         0: NFe_FatID := VAR_FATID__001; // -1 = Balanco de pq no PQG
        10:
        begin
          if QrPQGide_mod.Value = 55 then
            NFe_FatID := VAR_FATID_0051  // Entrada de Uso e consumo por NFe
          else
            NFe_FatID := VAR_FATID_0151; // Entrada de Uso e consumo por NF modelo normal
        end;
        110: NFe_FatID := VAR_FATID_1110;
        150: NFe_FatID := VAR_FATID_1150;
        190: NFe_FatID := VAR_FATID_1190;
        1110: NFe_FatID := VAR_FATID_1110;
        1150: NFe_FatID := VAR_FATID_1150;
        1190: NFe_FatID := VAR_FATID_1190;
        else
        begin
          NFe_FatID := 0;
          Geral.MB_Erro('O tipo de PQX ' + IntToStr(QrPQGTipo.Value) +
          ' n�o tem par no GGX! AVISE A DERMATEK!');
        end;
      end;
      //
      if QrPQGTipo.Value < 1000 then
        TipoNew := QrPQGTipo.Value + 1000
      else
        TipoNew := QrPQGTipo.Value;
      QrGGX.Close;
      QrGGX.Params[00].AsInteger := QrPQGTipo.Value; //Erro porque 110 <> 1110
      QrGGX.Params[01].AsInteger := TipoNew; //Erro porque 110 <> 1110
      QrGGX.Params[02].AsInteger := QrPQGInsumo.Value;
      UnDmkDAC_PF.AbreQuery(QrGGX, MyDB);

      OriCodi   := QrPQGOrigemCodi.Value;
      OriCtrl   := QrPQGOrigemCtrl.Value;
      OriCnta   := 0;//QrPQG.Value;
      OriPart   := 0;//QrPQG.Value;
      Empresa   := QrPQGCliOrig.Value;
      QuemUsou  := QrPQGCliDest.Value;
      StqCenCad := Dmod.QrControlePQ_StqCenCad.Value; // Parei Aqui !!!! Colocar centro de estoque na entrada?
      GraGruX   := QrGGXControle.Value;
      //
      Qtde      := QrPQGPeso.Value;
      Pecas     := 0; // Parei Aqui !!!! Colocar embalagens?;
      Peso      := QrPQGPeso.Value;
      AreaM2    := 0;
      AreaP2    := 0;
      FatorClas := 1;
      Retorno   := QrPQGRetorno.Value;
      Tipo      := QrPQGTipo.Value;
      DataHora  :=  FormatDateTime('YYYY-MM-DD', QrPQGDataX.Value);
      CustoAll  := QrPQGValor.Value;
      //
      // Provis�rio: exclui inclus�es deprecadas
(*
      DMod.QrUpdM.SQL.Clear;
      DMod.QrUpdM.SQL.Add('DELETE FROM stqmovitsa');
      DMod.QrUpdM.SQL.Add('WHERE TIME(DataHora) = 0');
      DMod.QrUpdM.SQL.Add('AND QuemUsou <> 0');
      DMod.QrUpdM.SQL.Add('AND ParTipo = 0');
      DMod.QrUpdM.SQL.Add('AND ParCodi = 0');
      Dmod.QrUpdM.ExecSQL;
*)
      //
      QrSMIA.Close;
      QrSMIA.Params[00].AsInteger := NFe_FatID;
      QrSMIA.Params[01].AsInteger := OriCodi;
      QrSMIA.Params[02].AsInteger := OriCtrl;
      UnDmkDAC_PF.AbreQuery(QrSMIA, MyDB);
      IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, QrPQGStqMovIts.Value);
      // Como om PQX n�o tem Indice �nico (Usa 3 campos > Tipo, OrigemCodi e OrigemCtrl)
      // ent�o ser� usado o pr�prio �ndice do StqMovItsA(B):
      ParCodi := IDCtrl;
      //
      if UMyMod.SQLReplace(Dmod.QrUpd, (*stIns,*) 'stqmovitsa', (*False,*) [
      'DataHora', 'Tipo', 'OriCodi',
      'OriCtrl', 'OriCnta', 'OriPart',
      'Empresa', 'StqCenCad', 'GraGruX',
      'Qtde', 'Pecas', 'Peso',
      'AreaM2', 'AreaP2', 'FatorClas',
      'QuemUsou', 'Retorno', 'ParTipo',
      'ParCodi', 'CustoAll'], [
      'IDCtrl'], [
      DataHora, NFe_FatID, OriCodi,
      OriCtrl, OriCnta, OriPart,
      Empresa, StqCenCad, GraGruX,
      Qtde, Pecas, Peso,
      AreaM2, AreaP2, FatorClas,
      QuemUsou, Retorno, ParTipo,
      ParCodi, CustoAll], [
      IDCtrl], False) then
      begin
        // segue o par conforme ParCodi
        StqMovIts := IDCtrl;
        //if
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'p q x', False, [
        'StqMovIts'], ['OrigemCodi', 'OrigemCtrl', 'Tipo'], [
        StqMovIts], [OriCodi, OriCtrl, Tipo], False);
      end;
      //
      QrPQG.Next;
    end;
    //
    // Acertar Pre�o de custo na entrada!
    QrPQxCus.Close;
    UnDmkDAC_PF.AbreQuery(QrPQxCus, MyDB);
    if PB <> nil then
    begin
      PB.Position := 0;
      PB.Max := QrPQxCus.RecordCount;
    end;
    while not QrPQxCus.Eof do
    begin
      if PB <> nil then
      begin
        PB.Position := PB.Position + 1;
        PB.Update;
        Application.ProcessMessages;
      end;
      CustoAll := QrPQxCusValor.Value;
      IDCtrl   := QrPQxCusIDCtrl.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovitsa', False, [
      'CustoAll'], ['IDCtrl'], [
      CustoAll], [IDCtrl], False);
      //
      QrPQxCus.Next;
    end;
    //
(*
    Data := dmkPF.PrimeiroDiaAposPeriodo(UnPQx.VerificaBalanco, dtSystem);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('REPLACE INTO stqmovitsb');
(*
    Dmod.QrUpd.SQL.Add('SELECT smia.*');
    Dmod.QrUpd.SQL.Add('FROM pqx pqx');
    Dmod.QrUpd.SQL.Add('LEFT JOIN stqmovitsa smia');
    Dmod.QrUpd.SQL.Add('  ON smia.ParCodi=pqx.StqMovIts');
    Dmod.QrUpd.SQL.Add('  AND smia.ParTipo=1');
    Dmod.QrUpd.SQL.Add('WHERE pqx.Insumo > 0');
    Dmod.QrUpd.SQL.Add('AND smia.IDCtrl IS NOT NULL');
    Dmod.QrUpd.SQL.Add('AND pqx.DataX < :P0');
*)
    Dmod.QrUpd.SQL.Add('SELECT smia.*');
    Dmod.QrUpd.SQL.Add('FROM stqmovitsa smia');
    Dmod.QrUpd.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX');
    Dmod.QrUpd.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
    Dmod.QrUpd.SQL.Add('WHERE gg1.PrdGrupTip=-2');
    Dmod.QrUpd.SQL.Add('AND smia.DataHora < :P0');
    Dmod.QrUpd.Params[0].AsString := DataHora;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
(*
    Dmod.QrUpd.SQL.Add('DELETE smia FROM stqmovitsa smia, pqx pqx');
    Dmod.QrUpd.SQL.Add('WHERE smia.ParCodi=pqx.StqMovIts');
    Dmod.QrUpd.SQL.Add('AND smia.ParTipo=1');
    Dmod.QrUpd.SQL.Add('AND smia.IDCtrl IS NOT NULL');
    Dmod.QrUpd.SQL.Add('AND pqx.DataX < :P0');
*)
    Dmod.QrUpd.SQL.Add('DELETE smia ');
    Dmod.QrUpd.SQL.Add('FROM stqmovitsa smia, gragrux ggx, gragru1 gg1');
    Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smia.GraGruX');
    Dmod.QrUpd.SQL.Add('AND gg1.Nivel1=ggx.GraGru1');
    Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
    Dmod.QrUpd.SQL.Add('AND smia.DataHora < :P0');
    Dmod.QrUpd.Params[0].AsString := DataHora;
    Dmod.QrUpd.ExecSQL;
    //
    QrMaxPerBal.Close;
    UnDmkDAC_PF.AbreQuery(QrMaxPerBal, MyDB);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa smib, gragrux ggx, gragru1 gg1');
    Dmod.QrUpd.SQL.Add('SET smib.GrupoBal=:P0');
    Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smib.GraGruX');
    Dmod.QrUpd.SQL.Add('AND smib.GrupoBal=0');
    Dmod.QrUpd.SQL.Add('AND gg1.Nivel1=ggx.GraGru1');
    Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
    Dmod.QrUpd.SQL.Add('AND smib.DataHora < :P1');
    Dmod.QrUpd.Params[00].AsInteger := QrMaxPerBalGrupoBal.Value;
    Dmod.QrUpd.Params[01].AsString  := DataHora;
    Dmod.QrUpd.ExecSQL;
    //


(*
   � MUITO DEMORADO PARA ABRIR A SQL Dmod.QrSMIAGGX !!!!
    QrUpd.SQL.Clear;
    QrUpd.SQL.Add('DELETE FROM stqmovitsa WHERE IDCtrl=:P0');
    Dmod.QrSMIAGGX.Close;
    UnDmkDAC_PF.AbreQuery(Dmod.QrSMIAGGX, MyDB);
    //
    if PB <> nil then
    begin
      PB.Position := 0;
      PB.Max := Dmod.QrSMIAGGX.RecordCount;
    end;
    //
    while not Dmod.QrSMIAGGX.Eof do
    begin
      if PB <> nil then
      begin
        PB.Position := PB.Position + 1;
        PB.Update;
      end;
      Application.ProcessMessages;
      //
      QrUpd.Params[0].AsInteger := Dmod.QrSMIAGGXIDCtrl.Value;
      QrUpd.ExecSQL;
      //
      Dmod.QrSMIAGGX.Next;
    end;
*)
    if QrPQG.RecordCount > 0 then
     Geral.MB_Info(IntToStr(QrPQG.RecordCount) +
     ' itens foram baixados do estoque!');
  finally
    Screen.Cursor := MyCursor;
  end;
}
end;

procedure TDmod.AtualizaTotaisWBXxxCab(Tabela: String; MovimCod: Integer);
var
  Qry: TmySQLQuery;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
begin
  Qry := TmySQLQuery.Create(Self);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM wbmovits',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    '']);
    //
    Pecas          := Qry.FieldByName('Pecas').AsFloat;
    PesoKg         := Qry.FieldByName('PesoKg').AsFloat;
    AreaM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorT         := Qry.FieldByName('ValorT').AsFloat;

    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, LowerCase(Tabela), False, [
    'Pecas', 'PesoKg',
    'AreaM2', 'AreaP2',
    'ValorT'], [
    'MovimCod'], [
    Pecas, PesoKg,
    AreaM2, AreaP2,
    ValorT], [
    MovimCod], True);
    //
  finally
    Qry.Free;
  end;
end;

procedure TDmod.AualizaTotaiEmiGru(Codigo: Integer);
begin
// Precisa fazer?
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrRendim, Dmod.MyDB, [
  'SELECT SUM(ROUND(emi.AreaM2 * 10 / CO_M2toFT2 * 4) / 4) AREAP2, ',
  'SUM(ROUND(emi.SemiAreaM2 * 10 / CO_M2toFT2 * 4) / 4) SEMIAREAP2, ',
  'SUM(emi.AreaM2) AreaM2, SUM(emi.SemiAreaM2) SemiAreaM2',
  'FROM emit emi ',
  'WHERE emi.EmitGru=' + Geral.FF0(QrEmitGruCodigo.Value),
  'AND emi.ClienteI=' + Geral.FF0(QrCliOrigCliOrig.Value),
  'AND emi.Retrabalho=0',
  '']);
  //
*)
end;

function TDmod.BRLMedioWB(GraGruX: Integer): Double;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT BRLMedM2 ',
    'FROM wbmprcab ',
    'WHERE GraGruX=' + Geral.FF0(GraGruX),
    '']);
    Result := Qry.FieldByName('BRLMedM2').AsFloat;
  finally
    Qry.Free;
  end;
end;

procedure TDmod.AtualizaStqMovIts_MPI(PB: TProgressBar);

////////////////////////////////////////////////////////////////////////////////
/// Desabilitado por causa da importa��o do SPED EFD  2011-06-14             ///
////////////////////////////////////////////////////////////////////////////////

const
  ParTipo = GRADE_TABS_PARTIPO_0002; // Identifica o par no MPIn e no StqMovItsA(B)
  // ERRADO! NFe_FatID = VAR_FATID_0103;// Indica o MPIn na NFe e no StqMovItsA(B)
  NFe_FatID = VAR_FATID_0113;// Indica o MPIn na NFe e no StqMovItsA(B)
var
  OriCodi, OriCtrl, OriCnta, OriPart, Empresa, StqCenCad, GraGruX,
  QuemUsou, IDCtrl, Retorno, ParCodi, StqMovIts(*, Tipo*),
  //Animal, Tipificacao,
  Controle: Integer;
  Qtde, Pecas, Peso, AreaM2, AreaP2, FatorClas, CustoAll: Double;
  DataHora: String;
  //
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
(*    // refazer itens sem CustoAll
    QrUpd.SQL.Clear;
    QrUpd.SQL.Add('UPDATE mpin mpi, stqmovitsa smia');
    QrUpd.SQL.Add('SET mpi.StqMovIts=0');
    QrUpd.SQL.Add('WHERE smia.IDCtrl=mpi.StqMovIts');
    QrUpd.SQL.Add('AND smia.CustoAll=0');
    QrUpd.ExecSQL;
*)    //
    QrMPI.Close;
    UnDmkDAC_PF.AbreQuery(QrMPI, MyDB);
    //
    if PB <> nil then
    begin
      PB.Position := 0;
      PB.Max := QrMPI.RecordCount;
    end;
    QrMPI.First;
    while not QrMPI.Eof do
    begin
      if PB <> nil then
      begin
        PB.Position := PB.Position + 1;
        PB.Update;
        Application.ProcessMessages;
      end;
(*      Animal      := QrMPIAnimal.Value + 1;
      Tipificacao := QrMPITipificacao.Value + 1; if Tipificacao = 1 then Tipificacao := 8;  // outros
      GraGruX     := ((Tipificacao * 100) + Animal) * -1;
*)
      GraGruX := ObtemGraGruXDeCouro(QrMPITipificacao.Value, QrMPIAnimal.Value);

      //
      //
      OriCodi     := QrMPICodigo.Value;
      OriCtrl     := QrMPIControle.Value;
      OriCnta     := 0;//QrMPI.Value;
      OriPart     := 0;//QrMPI.Value;
      Empresa     := QrMPIClienteI.Value;
      QuemUsou    := 0;
      StqCenCad   := Dmod.QrControleMP_StqCenCad.Value; // Parei Aqui !!!!
      //
      QrGGX_MP.Close;
      QrGGX_MP.Params[0].AsInteger := GraGruX;
      UnDmkDAC_PF.AbreQuery(QrGGX_MP, MyDB);
      // 2011-04-20
      //Qtde := QrMPIPecas.Value;
      case QrGGX_MPGerBxaEstq.Value of
        0: (* ? ? ?   *) Qtde := QrMPIPecas.Value;
        1: (* Pe�a    *) Qtde := QrMPIPecas.Value;
        2: (* m�      *) Qtde := QrMPIM2.Value;
        3: (* kg      *) Qtde := QrMPIPLE.Value;
        4: (* t (ton) *) Qtde := QrMPIPLE.Value / 1000;
        5: (* ft�     *) Qtde := QrMPIP2.Value;
        else Qtde := QrMPIPecas.Value;
      end;
      // Fim 2011-04-20
      Pecas       := QrMPIPecas.Value;
      Peso        := QrMPIPLE.Value;
      AreaM2      := QrMPIM2.Value;
      AreaP2      := QrMPIP2.Value;
      FatorClas   := 1;
      Retorno     := 0;
      //Tipo        := ?;
      Controle    := QrMPIControle.Value;
      DataHora    := FormatDateTime('YYYY-MM-DD', QrMPIData.Value);
      CustoAll    := QrMPICMPValor.Value;
      //
(*
      // Provis�rio: exclui inclus�es deprecadas
      DMod.QrUpdM.SQL.Clear;
      DMod.QrUpdM.SQL.Add('DELETE FROM stqmovitsa');
      DMod.QrUpdM.SQL.Add('WHERE TIME(DataHora) = 0');
      DMod.QrUpdM.SQL.Add('AND QuemUsou <> 0');
      DMod.QrUpdM.SQL.Add('AND ParTipo = 0');
      DMod.QrUpdM.SQL.Add('AND ParCodi = 0');
      Dmod.QrUpdM.ExecSQL;
*)
      //
      QrSMIA.Close;
      QrSMIA.Params[00].AsInteger := NFe_FatID;
      QrSMIA.Params[01].AsInteger := OriCodi;
      QrSMIA.Params[02].AsInteger := OriCtrl;
      UnDmkDAC_PF.AbreQuery(QrSMIA, MyDB);
      IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, QrSMIAIDCtrl.Value);
      ParCodi := Controle;
      //
      if UMyMod.SQLReplace(Dmod.QrUpd, (*stIns,*) 'stqmovitsa', (*False,*) [
      'DataHora', 'Tipo', 'OriCodi',
      'OriCtrl', 'OriCnta', 'OriPart',
      'Empresa', 'StqCenCad', 'GraGruX',
      'Qtde', 'Pecas', 'Peso',
      'AreaM2', 'AreaP2', 'FatorClas',
      'QuemUsou', 'Retorno', 'ParTipo',
      'ParCodi', 'CustoAll'], [
      'IDCtrl'], [
      DataHora, NFe_FatID, OriCodi,
      OriCtrl, OriCnta, OriPart,
      Empresa, StqCenCad, GraGruX,
      Qtde, Pecas, Peso,
      AreaM2, AreaP2, FatorClas,
      QuemUsou, Retorno, ParTipo,
      ParCodi, CustoAll], [
      IDCtrl], False) then
      begin
        // segue o par conforme ParCodi
        StqMovIts := IDCtrl;
        //if
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mpin', False, [
        'StqMovIts'], ['Controle'], [
        StqMovIts], [Controle], False);
      end;
      //
      // 2011-04-20
      // Arrumar itens baixados!
      QrSMPIBxa.Close;
      QrSMPIBxa.Params[0].AsInteger := QrSMIAParCodi.Value;
      UnDmkDAC_PF.AbreQuery(QrSMPIBxa, MyDB);
      if QrSMPIBxa.RecordCount > 0 then
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('UPDATE stqmovitsa SET GraGruX=:P0 WHERE IDCtrl=:P1');
        QrSMPIBxa.First;
        while not QrSMPIBxa.Eof do
        begin
          if QrSMPIBxaGraGruX.Value <> GraGruX then
          begin
            DMod.QrUpd.Params[00].AsInteger := GraGruX;
            DMod.QrUpd.Params[01].AsInteger := QrSMPIBxaIDCtrl.Value;
            DMod.QrUpd.ExecSQL;
          end;
          QrSMPIBxa.Next;
        end;
      end;
      // Fim 2011-04-20
      //
      QrMPI.Next;
    end;
    //
    // Acertar Pre�o de custo na entrada!
    QrMPInCus.Close;
    UnDmkDAC_PF.AbreQuery(QrMPInCus, MyDB);
    if PB <> nil then
    begin
      PB.Position := 0;
      PB.Max := QrMPInCus.RecordCount;
    end;
    while not QrMPInCus.Eof do
    begin
      if PB <> nil then
      begin
        PB.Position := PB.Position + 1;
        PB.Update;
        Application.ProcessMessages;
      end;
      CustoAll := QrMPInCusCMPValor.Value;
      IDCtrl   := QrMPInCusIDCtrl.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovitsa', False, [
      'CustoAll'], ['IDCtrl'], [
      CustoAll], [IDCtrl], False);
      //
      QrMPInCus.Next;
    end;
    //
(*
    Data := dmkPF.PrimeiroDiaAposPeriodo(UnPQx.VerificaBalanco, dtSystem);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('REPLACE INTO stqmovitsb');
    Dmod.QrUpd.SQL.Add('SELECT smia.*');
    Dmod.QrUpd.SQL.Add('FROM pqx pqx');
    Dmod.QrUpd.SQL.Add('LEFT JOIN stqmovitsa smia');
    Dmod.QrUpd.SQL.Add('  ON smia.ParCodi=pqx.StqMovIts');
    Dmod.QrUpd.SQL.Add('  AND smia.ParTipo=1');
    Dmod.QrUpd.SQL.Add('WHERE pqx.Insumo > 0');
    Dmod.QrUpd.SQL.Add('AND smia.IDCtrl IS NOT NULL');
    Dmod.QrUpd.SQL.Add('AND pqx.DataX < :P0');
    Dmod.QrUpd.Params[0].AsString := Data;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE smia FROM stqmovitsa smia, pqx pqx');
    Dmod.QrUpd.SQL.Add('WHERE smia.ParCodi=pqx.StqMovIts');
    Dmod.QrUpd.SQL.Add('AND smia.ParTipo=1');
    Dmod.QrUpd.SQL.Add('AND smia.IDCtrl IS NOT NULL');
    Dmod.QrUpd.SQL.Add('AND pqx.DataX < :P0');
    Dmod.QrUpd.Params[0].AsString := Data;
    Dmod.QrUpd.ExecSQL;
*)
  finally
    Screen.Cursor := MyCursor;
  end;
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
  Result := '';
end;

procedure TDmod.AtualizaSaldoVirtualWBMovIts(Controle: Integer);
var
  Qry: TmySQLQuery;
  Pecas, AreaM2, SdoVrtPeca, SdoVrtArM2: Double;
begin
  if Controle = 0 then
    Exit;
  //
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, MyDB, [
    'SELECT SUM(Pecas) Pecas, ',
    'SUM(AreaM2) AreaM2 ',
    'FROM wbmovits ',
    'WHERE SrcNivel2=' + Geral.FF0(Controle),
    'AND Pecas < 0 ',
    '']);
    Pecas := Qry.FieldByName('Pecas').AsFloat;
    AreaM2 := Qry.FieldByName('AreaM2').AsFloat;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
    'FROM wbmovits ',
    'WHERE Controle=' + Geral.FF0(Controle),
    '']);
    SdoVrtPeca := Qry.FieldByName('Pecas').AsFloat + Pecas;
    SdoVrtArM2 := Qry.FieldByName('AreaM2').AsFloat + AreaM2;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'wbmovits', False, [
    'SdoVrtPeca', 'SdoVrtArM2'], ['Controle'], [
    SdoVrtPeca, SdoVrtArM2], [Controle], True);
  finally
    Qry.Free;
  end;
end;

procedure TDmod.AtualizaStqMovIts_CWB(); // CWB = Classifica��o de Wet Blue

////////////////////////////////////////////////////////////////////////////////
/// Desabilitado por causa da importa��o do SPED EFD  2011-06-14             ///
////////////////////////////////////////////////////////////////////////////////

const
  ParTipo = GRADE_TABS_PARTIPO_0003; // Identifica o par do WB classificado no pr�prio StqMovItsA(B)
  NFe_FatID = VAR_FATID_0104;// Indica o classificado na NFe??? e no StqMovItsA(B)
  OriCnta = 0;
  OriPart = 0;
var
  OriCodi, Empresa, StqCenCad, GraGruX, IDCtrl, DebCtrl: Integer;
  Qtde, Pecas, Peso, AreaM2, AreaP2, FatorClas, TPecas, TPLE: Double;
  DataHora: String;
  //
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    QrCWB.Close;
    UnDmkDAC_PF.AbreQuery(QrCWB, MyDB);
    //
    QrCWB.First;
    while not QrCWB.Eof do
    begin
      ReopenSMI(QrCWBCodigo.Value, QrCWBControle.Value);
      TPLE   := QrCWBPLE.Value;
      TPecas := QrCWBPecas.Value;
      if TPecas = 0 then TPecas := 1; // Evitar erro de divisao
      if Dmod.QrSMI.RecordCount <> 1 then
      begin
        Geral.MB_Aviso('Classifica��o cancelada!'+sLineBreak+
        'N�mero de itens de espelho no estoque de grade difere de 1!'+sLineBreak+
        'N�mero de itens de espelho: ' + IntToStr(Dmod.QrSMI.RecordCount));

        Exit;
      end else begin
        FatorClas := 1 / QrCWBFatorClas.Value; // Inverter!!
        StqCenCad := Dmod.QrSMIStqCenCad.Value;
        GraGruX   := Dmod.QrSMIGraGruX.Value;

        Qtde      := -QrCWBQtde.Value * FatorClas;
        Pecas     := Qtde;
        Peso      := Qtde / TPecas * TPLE; // Parei Aqui!!!
        AreaM2    := 0; // Parei Aqui!!!
        AreaP2    := 0; // Parei Aqui!!!
        //
        OriCodi   := QrCWBOriCodi.Value;
        //IDCtrl    := QrCWBIDCtrl.Value;
        Empresa   := QrCWBEmpresa.Value;
        DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', QrCWBDataHora.Value);
        //CustoAll  := QrCWBCMPValor.Value + QrCWBCMPFrete.Value + QrCWBCustoPQ.Value;

        IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
        'DataHora', 'Tipo', 'OriCodi',
        'OriCtrl', 'OriCnta', 'OriPart',
        'Empresa', 'StqCenCad', 'GraGruX',
        'Qtde', 'Pecas', 'Peso', 'AreaM2',
        'AreaP2', 'FatorClas', 'CustoAll'
      ], ['IDCtrl'], [
        DataHora, VAR_FATID_0104, OriCodi,
        IDCtrl, OriCnta, OriPart,
        Empresa, StqCenCad, GraGruX,
        Qtde, Pecas, Peso, AreaM2, AreaP2,
        FatorClas
      ], [IDCtrl], False) then
      begin
       // segue o par conforme ParCodi
          DebCtrl := IDCtrl;
          IDCtrl  := QrCWBIDCtrl.Value;
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovitsa', False, [
          'DebCtrl'], ['IDCtrl'], [DebCtrl], [IDCtrl], False);
        end;
        //
      end;
      QrCWB.Next;
    end;
    //
(*
    Data := dmkPF.PrimeiroDiaAposPeriodo(UnPQx.VerificaBalanco, dtSystem);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('REPLACE INTO stqmovitsb');
    Dmod.QrUpd.SQL.Add('SELECT smia.*');
    Dmod.QrUpd.SQL.Add('FROM pqx pqx');
    Dmod.QrUpd.SQL.Add('LEFT JOIN stqmovitsa smia');
    Dmod.QrUpd.SQL.Add('  ON smia.ParCodi=pqx.StqMovIts');
    Dmod.QrUpd.SQL.Add('  AND smia.ParTipo=1');
    Dmod.QrUpd.SQL.Add('WHERE pqx.Insumo > 0');
    Dmod.QrUpd.SQL.Add('AND smia.IDCtrl IS NOT NULL');
    Dmod.QrUpd.SQL.Add('AND pqx.DataX < :P0');
    Dmod.QrUpd.Params[0].AsString := Data;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE smia FROM stqmovitsa smia, pqx pqx');
    Dmod.QrUpd.SQL.Add('WHERE smia.ParCodi=pqx.StqMovIts');
    Dmod.QrUpd.SQL.Add('AND smia.ParTipo=1');
    Dmod.QrUpd.SQL.Add('AND smia.IDCtrl IS NOT NULL');
    Dmod.QrUpd.SQL.Add('AND pqx.DataX < :P0');
    Dmod.QrUpd.Params[0].AsString := Data;
    Dmod.QrUpd.ExecSQL;
*)
  finally
    Screen.Cursor := MyCursor;
  end;
end;

{
object QrAuxL: TMySQLQuery

  Left = 160
  Top = 51
end
object QrUpdL: TMySQLQuery

  Left = 160
  Top = 95
end
object QlLocal: TMySQLBatchExecute
  Action = baContinue
  Database = ZZDB
  Delimiter = ';'
  Left = 368
  Top = 4
end

}
end.


