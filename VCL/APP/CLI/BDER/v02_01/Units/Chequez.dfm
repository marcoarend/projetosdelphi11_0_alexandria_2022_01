object FmChequez: TFmChequez
  Left = 339
  Top = 185
  Caption = 'Controle de Cheques de Terceiros'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 110
    Top = 48
    Width = 792
    Height = 400
    TabOrder = 0
    object DBGrid5: TDBGrid
      Left = 1
      Top = 173
      Width = 790
      Height = 111
      Align = alClient
      DataSource = DsCheques
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Banco'
          Title.Caption = 'Bco'
          Width = 25
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Agencia'
          Title.Caption = 'Agen.'
          Width = 31
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta'
          Title.Caption = 'Conta corrente'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cheque'
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF_TXT'
          Title.Caption = 'CPF / CNPJ'
          Width = 87
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cidade'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataEmi'
          Title.Caption = 'Emiss'#227'o'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataVct'
          Title.Caption = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Observ'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Width = 39
          Visible = True
        end>
    end
    object PnEdita: TPanel
      Left = 1
      Top = 284
      Width = 790
      Height = 115
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      Visible = False
      object Label9: TLabel
        Left = 4
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label3: TLabel
        Left = 80
        Top = 8
        Width = 123
        Height = 13
        Caption = 'Banco \ Ag'#234'ncia \ Conta:'
        FocusControl = EdBanco
      end
      object Label4: TLabel
        Left = 212
        Top = 8
        Width = 40
        Height = 13
        Caption = 'Cheque:'
      end
      object Label5: TLabel
        Left = 348
        Top = 8
        Width = 44
        Height = 13
        Caption = 'Emitente:'
        FocusControl = EdEmitente
      end
      object Label6: TLabel
        Left = 564
        Top = 8
        Width = 23
        Height = 13
        Caption = 'CPF:'
        FocusControl = EdCPF
      end
      object Label7: TLabel
        Left = 4
        Top = 48
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = EdCliente
      end
      object Label8: TLabel
        Left = 328
        Top = 48
        Width = 36
        Height = 13
        Caption = 'Cidade:'
        FocusControl = EdCidade
      end
      object Label10: TLabel
        Left = 280
        Top = 8
        Width = 27
        Height = 13
        Caption = 'Valor:'
        FocusControl = EdValor
      end
      object Label11: TLabel
        Left = 424
        Top = 48
        Width = 66
        Height = 13
        Caption = 'Observa'#231#245'es:'
        FocusControl = EdObserv
      end
      object Label1: TLabel
        Left = 680
        Top = 8
        Width = 42
        Height = 13
        Caption = 'Emiss'#227'o:'
      end
      object Label2: TLabel
        Left = 680
        Top = 48
        Width = 59
        Height = 13
        Caption = 'Vencimento:'
      end
      object Label28: TLabel
        Left = 4
        Top = 92
        Width = 165
        Height = 13
        Caption = 'F5 - Desabilita campo selecionado.'
      end
      object Label29: TLabel
        Left = 184
        Top = 92
        Width = 88
        Height = 13
        Caption = 'F6 - Habilita todos.'
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 24
        Width = 60
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '00'
        ValWarn = False
      end
      object EdBanco: TdmkEdit
        Left = 80
        Top = 24
        Width = 29
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 3
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = EdBancoExit
      end
      object EdAgencia: TdmkEdit
        Left = 108
        Top = 24
        Width = 37
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 4
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = EdAgenciaExit
        OnKeyDown = EdAgenciaKeyDown
      end
      object EdConta: TdmkEdit
        Left = 144
        Top = 24
        Width = 64
        Height = 21
        Alignment = taCenter
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdAgenciaKeyDown
      end
      object EdCheque: TdmkEdit
        Left = 212
        Top = 24
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = EdChequeExit
        OnKeyDown = EdAgenciaKeyDown
      end
      object EdEmitente: TdmkEdit
        Left = 348
        Top = 24
        Width = 212
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdAgenciaKeyDown
      end
      object EdCPF: TdmkEdit
        Left = 564
        Top = 24
        Width = 112
        Height = 21
        Alignment = taCenter
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtCPFJ
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdAgenciaKeyDown
      end
      object EdCliente: TdmkEditCB
        Left = 4
        Top = 64
        Width = 33
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdAgenciaKeyDown
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object EdCidade: TdmkEdit
        Left = 328
        Top = 64
        Width = 93
        Height = 21
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdAgenciaKeyDown
      end
      object EdValor: TdmkEdit
        Left = 280
        Top = 24
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnKeyDown = EdAgenciaKeyDown
      end
      object EdObserv: TdmkEdit
        Left = 424
        Top = 64
        Width = 252
        Height = 21
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdAgenciaKeyDown
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 40
        Top = 64
        Width = 285
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMECLIENTE'
        ListSource = DsClientes
        TabOrder = 10
        OnKeyDown = EdAgenciaKeyDown
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDataEmi: TdmkEdit
        Left = 680
        Top = 24
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = EdDataEmiExit
        OnKeyDown = EdAgenciaKeyDown
      end
      object EdDataVct: TdmkEdit
        Left = 680
        Top = 64
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = EdDataVctExit
        OnKeyDown = EdAgenciaKeyDown
      end
    end
    object GBPesquisa: TGroupBox
      Left = 1
      Top = 53
      Width = 790
      Height = 120
      Align = alTop
      Caption = '   '
      TabOrder = 0
      object PnPesq: TPanel
        Left = 2
        Top = 15
        Width = 786
        Height = 103
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label13: TLabel
          Left = 16
          Top = 0
          Width = 123
          Height = 13
          Caption = 'Banco \ Ag'#234'ncia \ Conta:'
          FocusControl = EdPesqBanco
        end
        object Label14: TLabel
          Left = 148
          Top = 0
          Width = 40
          Height = 13
          Caption = 'Cheque:'
        end
        object Label15: TLabel
          Left = 216
          Top = 0
          Width = 44
          Height = 13
          Caption = 'Emitente:'
          FocusControl = EdPesqEmitente
        end
        object Label16: TLabel
          Left = 360
          Top = 0
          Width = 23
          Height = 13
          Caption = 'CPF:'
          FocusControl = EdPesqCPF
        end
        object Label17: TLabel
          Left = 476
          Top = 0
          Width = 35
          Height = 13
          Caption = 'Cliente:'
          FocusControl = EdPesqCliente
        end
        object Label18: TLabel
          Left = 664
          Top = 0
          Width = 36
          Height = 13
          Caption = 'Cidade:'
          FocusControl = EdPesqCidade
        end
        object EdPesqBanco: TdmkEdit
          Left = 16
          Top = 16
          Width = 29
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdPesqBancoChange
          OnExit = EdPesqBancoExit
        end
        object EdPesqAgencia: TdmkEdit
          Left = 44
          Top = 16
          Width = 37
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdPesqBancoChange
          OnExit = EdPesqAgenciaExit
        end
        object EdPesqConta: TdmkEdit
          Left = 80
          Top = 16
          Width = 64
          Height = 21
          Alignment = taCenter
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdPesqBancoChange
        end
        object EdPesqCheque: TdmkEdit
          Left = 148
          Top = 16
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdPesqBancoChange
          OnExit = EdPesqChequeExit
        end
        object EdPesqEmitente: TdmkEdit
          Left = 216
          Top = 16
          Width = 141
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdPesqBancoChange
        end
        object EdPesqCPF: TdmkEdit
          Left = 360
          Top = 16
          Width = 112
          Height = 21
          Alignment = taCenter
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtCPFJ
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdPesqBancoChange
        end
        object EdPesqCliente: TdmkEditCB
          Left = 476
          Top = 16
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdPesqBancoChange
          DBLookupComboBox = CBPesqCliente
          IgnoraDBLookupComboBox = False
        end
        object EdPesqCidade: TdmkEdit
          Left = 664
          Top = 16
          Width = 117
          Height = 21
          TabOrder = 8
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdPesqBancoChange
        end
        object CBPesqCliente: TdmkDBLookupComboBox
          Left = 510
          Top = 16
          Width = 151
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMECLIENTE'
          ListSource = DsClientes
          TabOrder = 7
          dmkEditCB = EdPesqCliente
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object GroupBox2: TGroupBox
          Left = 16
          Top = 42
          Width = 177
          Height = 57
          Caption = '   '
          TabOrder = 10
          object Label19: TLabel
            Left = 12
            Top = 16
            Width = 17
            Height = 13
            Caption = 'De:'
            FocusControl = EdPesqValorMin
          end
          object Label12: TLabel
            Left = 92
            Top = 16
            Width = 19
            Height = 13
            Caption = 'At'#233':'
            FocusControl = EdPesqValorMax
          end
          object EdPesqValorMin: TdmkEdit
            Left = 12
            Top = 32
            Width = 76
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdPesqBancoChange
          end
          object EdPesqValorMax: TdmkEdit
            Left = 92
            Top = 32
            Width = 76
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdPesqBancoChange
          end
        end
        object CkPesqValor: TCheckBox
          Left = 28
          Top = 40
          Width = 49
          Height = 17
          Caption = 'Valor: '
          TabOrder = 9
          OnClick = CkOrdem1Click
        end
        object GroupBox3: TGroupBox
          Left = 196
          Top = 42
          Width = 213
          Height = 57
          Caption = '   '
          TabOrder = 12
          object Label20: TLabel
            Left = 12
            Top = 16
            Width = 17
            Height = 13
            Caption = 'De:'
            FocusControl = EdPesqValorMin
          end
          object Label23: TLabel
            Left = 112
            Top = 16
            Width = 19
            Height = 13
            Caption = 'At'#233':'
            FocusControl = EdPesqValorMax
          end
          object TPPesqDataEmiMin: TDateTimePicker
            Left = 12
            Top = 30
            Width = 93
            Height = 21
            Date = 39107.562770358800000000
            Time = 39107.562770358800000000
            TabOrder = 0
            OnChange = EdPesqBancoChange
          end
          object TPPesqDataEmiMax: TDateTimePicker
            Left = 112
            Top = 30
            Width = 93
            Height = 21
            Date = 39107.562770358800000000
            Time = 39107.562770358800000000
            TabOrder = 1
            OnChange = EdPesqBancoChange
          end
        end
        object CkPesqDataEmi: TCheckBox
          Left = 208
          Top = 40
          Width = 109
          Height = 17
          Caption = ' Data de emiss'#227'o: '
          TabOrder = 11
          OnClick = CkOrdem1Click
        end
        object GroupBox4: TGroupBox
          Left = 416
          Top = 42
          Width = 213
          Height = 57
          Caption = '   '
          TabOrder = 14
          object Label21: TLabel
            Left = 12
            Top = 16
            Width = 17
            Height = 13
            Caption = 'De:'
            FocusControl = EdPesqValorMin
          end
          object Label22: TLabel
            Left = 112
            Top = 16
            Width = 19
            Height = 13
            Caption = 'At'#233':'
            FocusControl = EdPesqValorMax
          end
          object TPPesqDataVctMin: TDateTimePicker
            Left = 12
            Top = 30
            Width = 93
            Height = 21
            Date = 39107.562770358800000000
            Time = 39107.562770358800000000
            TabOrder = 0
            OnChange = EdPesqBancoChange
          end
          object TPPesqDataVctMax: TDateTimePicker
            Left = 112
            Top = 30
            Width = 93
            Height = 21
            Date = 39107.562770358800000000
            Time = 39107.562770358800000000
            TabOrder = 1
            OnChange = EdPesqBancoChange
          end
        end
        object CkPesqDataVct: TCheckBox
          Left = 428
          Top = 40
          Width = 125
          Height = 17
          Caption = ' Data de vencimento: '
          TabOrder = 13
          OnClick = CkOrdem1Click
        end
      end
      object CkPesquisa: TCheckBox
        Left = 12
        Top = 0
        Width = 77
        Height = 17
        Caption = ' Pesquisa: '
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = CkPesquisaClick
      end
    end
    object Panel14: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 52
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 2
      object Label24: TLabel
        Left = 16
        Top = 4
        Width = 43
        Height = 13
        Caption = 'Ordem 1:'
      end
      object Label25: TLabel
        Left = 208
        Top = 4
        Width = 43
        Height = 13
        Caption = 'Ordem 2:'
      end
      object Label26: TLabel
        Left = 400
        Top = 4
        Width = 43
        Height = 13
        Caption = 'Ordem 3:'
      end
      object Label27: TLabel
        Left = 592
        Top = 4
        Width = 43
        Height = 13
        Caption = 'Ordem 4:'
      end
      object CBOrdem1: TComboBox
        Left = 16
        Top = 20
        Width = 93
        Height = 21
        Style = csDropDownList
        Sorted = True
        TabOrder = 0
        OnChange = CBOrdem1Change
        Items.Strings = (
          'Ag'#234'ncia'
          'Banco'
          'Cheque'
          'Cidade'
          'Cliente'
          'CNPJ'
          'Conta'
          'Emiss'#227'o'
          'Emitente'
          'Observ'
          'Valor'
          'Vencto')
      end
      object CkOrdem1: TCheckBox
        Left = 112
        Top = 22
        Width = 85
        Height = 17
        Caption = 'Decrescente'
        TabOrder = 1
        OnClick = CkOrdem1Click
      end
      object CBOrdem2: TComboBox
        Left = 208
        Top = 20
        Width = 93
        Height = 21
        Style = csDropDownList
        Sorted = True
        TabOrder = 2
        OnChange = CBOrdem1Change
        Items.Strings = (
          'Ag'#234'ncia'
          'Banco'
          'Cheque'
          'Cidade'
          'Cliente'
          'CNPJ'
          'Conta'
          'Emiss'#227'o'
          'Emitente'
          'Observ'
          'Valor'
          'Vencto')
      end
      object CkOrdem2: TCheckBox
        Left = 304
        Top = 22
        Width = 85
        Height = 17
        Caption = 'Decrescente'
        TabOrder = 3
        OnClick = CkOrdem1Click
      end
      object CBOrdem4: TComboBox
        Left = 592
        Top = 20
        Width = 93
        Height = 21
        Style = csDropDownList
        Sorted = True
        TabOrder = 4
        OnChange = CBOrdem1Change
        Items.Strings = (
          'Ag'#234'ncia'
          'Banco'
          'Cheque'
          'Cidade'
          'Cliente'
          'CNPJ'
          'Conta'
          'Emiss'#227'o'
          'Emitente'
          'Observ'
          'Valor'
          'Vencto')
      end
      object CkOrdem4: TCheckBox
        Left = 688
        Top = 22
        Width = 85
        Height = 17
        Caption = 'Decrescente'
        TabOrder = 5
        OnClick = CkOrdem1Click
      end
      object CkOrdem3: TCheckBox
        Left = 496
        Top = 22
        Width = 85
        Height = 17
        Caption = 'Decrescente'
        Checked = True
        State = cbChecked
        TabOrder = 6
        OnClick = CkOrdem1Click
      end
      object CBOrdem3: TComboBox
        Left = 400
        Top = 20
        Width = 93
        Height = 21
        Style = csDropDownList
        Sorted = True
        TabOrder = 7
        OnChange = CBOrdem1Change
        Items.Strings = (
          'Ag'#234'ncia'
          'Banco'
          'Cheque'
          'Cidade'
          'Cliente'
          'CNPJ'
          'Conta'
          'Emiss'#227'o'
          'Emitente'
          'Observ'
          'Valor'
          'Vencto')
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 448
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object PnControla: TPanel
      Left = 209
      Top = 1
      Width = 582
      Height = 46
      Align = alClient
      TabOrder = 1
      object Panel4: TPanel
        Left = 470
        Top = 1
        Width = 111
        Height = 44
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 16
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 108
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera banco atual'
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 200
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Exclui banco atual'
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtExcluiClick
      end
      object BitBtn1: TBitBtn
        Tag = 20
        Left = 300
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Caption = '&Reabre'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BitBtn1Click
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 392
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera banco atual'
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtImprimeClick
      end
    end
    object PnConfirma: TPanel
      Left = 1
      Top = 1
      Width = 208
      Height = 46
      Align = alLeft
      TabOrder = 0
      Visible = False
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel6: TPanel
        Left = 102
        Top = 1
        Width = 105
        Height = 44
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 8
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 452
        Height = 32
        Caption = '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 452
        Height = 32
        Caption = '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 452
        Height = 32
        Caption = '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrChequez: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrChequezAfterOpen
    BeforeClose = QrChequezBeforeClose
    OnCalcFields = QrChequezCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECLI, chz.*'
      'FROM chequez chz'
      'LEFT JOIN entidades cli ON cli.Codigo=chz.Cliente'
      '')
    Left = 12
    Top = 12
    object QrChequezCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrChequezBanco: TSmallintField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrChequezAgencia: TSmallintField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrChequezConta: TWideStringField
      FieldName = 'Conta'
      Size = 10
    end
    object QrChequezCheque: TIntegerField
      FieldName = 'Cheque'
      DisplayFormat = '000000'
    end
    object QrChequezCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrChequezEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrChequezCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrChequezValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrChequezObserv: TWideStringField
      FieldName = 'Observ'
      Size = 50
    end
    object QrChequezLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrChequezDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChequezDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChequezUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrChequezUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrChequezCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrChequezDataEmi: TDateField
      FieldName = 'DataEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequezDataVct: TDateField
      FieldName = 'DataVct'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequezNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrChequezCliente: TIntegerField
      FieldName = 'Cliente'
    end
  end
  object DsCheques: TDataSource
    DataSet = QrChequez
    Left = 40
    Top = 12
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECLIENTE'
      'FROM entidades ci'
      'WHERE Cliente1="V" OR Cliente2="V"'
      'ORDER BY NOMECLIENTE')
    Left = 69
    Top = 13
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 97
    Top = 13
  end
  object QrDatas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(DataEmi) MaxEmi, MAX(DataVct) MaxVct,'
      'MIN(DataEmi) MinEmi, MIN(DataVct) MinVct'
      'FROM chequez')
    Left = 412
    Top = 308
    object QrDatasMaxEmi: TDateField
      FieldName = 'MaxEmi'
    end
    object QrDatasMaxVct: TDateField
      FieldName = 'MaxVct'
    end
    object QrDatasMinEmi: TDateField
      FieldName = 'MinEmi'
    end
    object QrDatasMinVct: TDateField
      FieldName = 'MinVct'
    end
  end
  object frxChequez: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38928.750734444400000000
    ReportOptions.Name = 'Or'#231'amento'
    ReportOptions.LastChange = 38928.750734444400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);'
      'end.')
    Left = 617
    Top = 16
    Datasets = <
      item
        DataSet = frxDsChequez
        DataSetName = 'frxDsChequez'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <
      item
        Name = ' MyVars'
        Value = Null
      end
      item
        Name = 'MeuLogo'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 75.590600000000000000
        Top = 18.897650000000000000
        Width = 740.409927000000000000
        object Picture1: TfrxPictureView
          Left = 11.338590000000000000
          Top = 7.559060000000000000
          Width = 166.299212600000000000
          Height = 64.251968500000000000
          ShowHint = False
          DataField = 'Logo'
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo2: TfrxMemoView
          Left = 181.417440000000000000
          Top = 3.779530000000000000
          Width = 457.322885910000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object frxMemoView1: TfrxMemoView
          Left = 642.520100000000000000
          Width = 90.708720000000000000
          Height = 11.338590000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date] [Time]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Left = 181.417440000000000000
          Top = 26.456710000000000000
          Width = 551.810940630000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Controle de Cheques')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 49.133890000000000000
        Top = 332.598640000000000000
        Width = 740.409927000000000000
        object Memo86: TfrxMemoView
          Left = 619.842920000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 196.535560000000000000
        Width = 740.409927000000000000
        DataSet = frxDsChequez
        DataSetName = 'frxDsChequez'
        RowCount = 0
        object Memo13: TfrxMemoView
          Width = 26.456646540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Banco'
          DataSet = frxDsChequez
          DataSetName = 'frxDsChequez'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsChequez."Banco"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          Left = 26.456710000000000000
          Width = 30.236196060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Agencia'
          DataSet = frxDsChequez
          DataSetName = 'frxDsChequez'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsChequez."Agencia"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 56.692950000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Conta'
          DataSet = frxDsChequez
          DataSetName = 'frxDsChequez'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsChequez."Conta"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 124.724490000000000000
          Width = 45.354316060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Cheque'
          DataSet = frxDsChequez
          DataSetName = 'frxDsChequez'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsChequez."Cheque"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 170.078850000000000000
          Width = 64.251973390000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Valor'
          DataSet = frxDsChequez
          DataSetName = 'frxDsChequez'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsChequez."Valor"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 234.330860000000000000
          Width = 79.370086060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Emitente'
          DataSet = frxDsChequez
          DataSetName = 'frxDsChequez'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsChequez."Emitente"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 313.700990000000000000
          Width = 75.590556060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CPF_TXT'
          DataSet = frxDsChequez
          DataSetName = 'frxDsChequez'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsChequez."CPF_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 389.291590000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Cidade'
          DataSet = frxDsChequez
          DataSetName = 'frxDsChequez'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsChequez."Cidade"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 457.323130000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMECLI'
          DataSet = frxDsChequez
          DataSetName = 'frxDsChequez'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsChequez."NOMECLI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 525.354670000000000000
          Width = 49.133846060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DataEmi'
          DataSet = frxDsChequez
          DataSetName = 'frxDsChequez'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsChequez."DataEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 574.488560000000000000
          Width = 49.133846060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DataVct'
          DataSet = frxDsChequez
          DataSetName = 'frxDsChequez'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsChequez."DataVct"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 623.622450000000000000
          Width = 113.385856060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Observ'
          DataSet = frxDsChequez
          DataSetName = 'frxDsChequez'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsChequez."Observ"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 18.897650000000000000
        Top = 117.165430000000000000
        Width = 740.409927000000000000
        object Memo4: TfrxMemoView
          Left = 26.456710000000000000
          Width = 30.236196060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Agc.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 56.692950000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Conta cor.')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 124.724490000000000000
          Width = 45.354316060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cheque')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 170.078850000000000000
          Width = 64.251973390000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Width = 26.456514720000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 234.330860000000000000
          Width = 79.370086060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emitente')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 313.700990000000000000
          Width = 75.590556060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 389.291590000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cidade')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 457.323130000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 525.354670000000000000
          Width = 49.133846060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 574.488560000000000000
          Width = 49.133846060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 623.622450000000000000
          Width = 113.385856060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 34.015770000000000000
        Top = 275.905690000000000000
        Width = 740.409927000000000000
        object Memo10: TfrxMemoView
          Left = 117.165430000000000000
          Top = 3.779530000000020000
          Width = 117.165393390000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsChequez
          DataSetName = 'frxDsChequez'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsChequez."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 26.456710000000000000
          Top = 3.779530000000020000
          Width = 90.708676060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total pesquisa:')
          ParentFont = False
        end
      end
    end
  end
  object frxDsChequez: TfrxDBDataset
    UserName = 'frxDsChequez'
    CloseDataSource = False
    DataSet = QrChequez
    BCDToCurrency = False
    Left = 645
    Top = 16
  end
end
