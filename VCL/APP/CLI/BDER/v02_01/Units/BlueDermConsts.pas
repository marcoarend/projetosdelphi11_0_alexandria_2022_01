unit BlueDermConsts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, comctrls, mySQLDbTables;

//type

var
  // Opcoes
  BDC_APRESENTACAO_IMPRESSAO: Integer;
  BDC_CAMINHO_REGEDIT: String;
  BDC_ATUALIZA_PRECO_PQ: Boolean;
  BDC_CUSTO_PQ_CONSIGNA: Integer;
  BDC_PROCESSAMENTO_COURO: Integer;
  // Confirma custos de lotes de couro
  BDC_OK_ADD_LOTE: Boolean;
  BDC_MPINI: Integer;
  // Outros
  BDC_PQCAD: Integer;
  BDC_PESAPQ_KG, BDC_PESAPQ_PC, BDC_PESAPQ_M2, BDC_PESAPQ_P2: Double;
  VAR_PALLET_BDC: String;
  //
const
  // Retorno de PQ prestacao de servico
  VAR_PQR_OCORR_RED_SDO = 1;
  VAR_PQR_OCORR_EMI_RET = 2;
  //Normal, 1=PQRAjuInn, 2=PQRAjuOut
  CO_PQ_LOAD_000_NORMAL = 0;
  CO_PQ_LOAD_001_AJUINN = 1;
  CO_PQ_LOAD_002_AJUOUT = 2;
  //
  CO_COU_NIV2_000_INDEF = 0;
  CO_COU_NIV2_001_COURO = 1;
  CO_COU_NIV2_002_RASPA = 2;
  CO_COU_NIV2_003_SUBPR = 3;
  //
  CO_RGTIPOCOURO_COLUNAS = 3;
  //
  CO_Forml_Status_Prod_Irregular = 'Deprecado';
  CO_Forml_Status_Prod_Ok        = 'Ok';
  CO_FatorKgWB_M2CRUST = 4.000000;  // Kg de raspa / m2 semi acabado a grosso modo!
  //
  CO_STQCENLOC_ZERO = 0;
  CO_FORNECMO_ZERO = 0;
  CO_CLIENTMO_ZERO = 0;
  //
implementation

{  limpar tabelas VS


DELETE FROM vsmovcab;
DELETE FROM vsmovdif;
DELETE FROM vsmovitb;
DELETE FROM vsmovits;
DELETE FROM vsmovitz;
DELETE FROM vscaccab;
DELETE FROM vscacitsa;
DELETE FROM vscacitsb;
DELETE FROM vsdefeicac;
DELETE FROM vsajscab;
DELETE FROM vsbalcab;
DELETE FROM vsbalemp;
DELETE FROM vsbxacab;
DELETE FROM vscaccab;
DELETE FROM vscacitsb;
DELETE FROM vscgicab;
DELETE FROM vscgiits;
DELETE FROM vsdefeicac;
DELETE FROM vsdsncab;
DELETE FROM vsmulfrncab;
DELETE FROM vsmulfrnits;
DELETE FROM vsmulnfecab;
DELETE FROM vsmulnfeits;
DELETE FROM vsopecab;
DELETE FROM vsoutcab;
DELETE FROM vspaclacaba;
DELETE FROM vspaclaitsa;
DELETE FROM vspalleta;
DELETE FROM vspamulcaba;
DELETE FROM vspamulcabr;
DELETE FROM vsparclcaba;
DELETE FROM vsparclitsa;
DELETE FROM vspedcab;
DELETE FROM vspedits;
DELETE FROM vsplccab;
DELETE FROM vsprepalcab;
DELETE FROM vsproqui;
DELETE FROM vspwecab;
DELETE FROM vsdsnits;
DELETE FROM vsdvlcab;
DELETE FROM vseqzcab;
DELETE FROM vseqzits;
DELETE FROM vseqzsub;
DELETE FROM vsexbcab;
DELETE FROM vsfchrmpcab;
DELETE FROM vsfchrslcab;
DELETE FROM vsfchrslits;
DELETE FROM vsgerarta;
DELETE FROM vsgerrcla;
DELETE FROM vshisfch;
DELETE FROM vsinncab;
DELETE FROM vsmorcab;
DELETE FROM vsmovcab;
}

end.
 