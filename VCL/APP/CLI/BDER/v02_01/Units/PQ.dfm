object FmPQ: TFmPQ
  Left = 331
  Top = 172
  Caption = 'QUI-INSUM-001 :: Produtos de Uso e Consumo'
  ClientHeight = 632
  ClientWidth = 1197
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 109
    Width = 1197
    Height = 523
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 36
      Top = 4
      Width = 1197
      Height = 231
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 76
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label8: TLabel
        Left = 380
        Top = 4
        Width = 93
        Height = 13
        Caption = 'Fornecedor padr'#227'o:'
      end
      object Label22: TLabel
        Left = 668
        Top = 44
        Width = 32
        Height = 13
        Caption = 'Grupo:'
      end
      object Label31: TLabel
        Left = 8
        Top = 44
        Width = 140
        Height = 13
        Caption = 'Setor controlador do estoque:'
        FocusControl = DBEdit1
      end
      object Label32: TLabel
        Left = 172
        Top = 44
        Width = 73
        Height = 13
        Caption = 'Grupo qu'#237'mico:'
        FocusControl = DBEdit2
      end
      object Label15: TLabel
        Left = 788
        Top = 3
        Width = 62
        Height = 13
        Caption = '% de quebra:'
      end
      object Label51: TLabel
        Left = 788
        Top = 43
        Width = 84
        Height = 13
        Caption = 'Qtd gasto por dia:'
      end
      object Label52: TLabel
        Left = 788
        Top = 83
        Width = 87
        Height = 13
        Caption = 'Qtd estq seguran::'
      end
      object Label54: TLabel
        Left = 788
        Top = 144
        Width = 54
        Height = 13
        Caption = 'Densidade:'
        FocusControl = DBEdit10
      end
      object Label59: TLabel
        Left = 860
        Top = 144
        Width = 39
        Height = 13
        Caption = '$ CIPC: '
        FocusControl = DBEdit11
      end
      object DBEdit01: TDBEdit
        Left = 8
        Top = 20
        Width = 64
        Height = 21
        DataField = 'Codigo'
        DataSource = DsPQ
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object DBEdit03: TDBEdit
        Left = 76
        Top = 20
        Width = 300
        Height = 21
        DataField = 'Nome'
        DataSource = DsPQ
        TabOrder = 1
      end
      object DBEdit02: TDBEdit
        Left = 380
        Top = 20
        Width = 401
        Height = 21
        DataField = 'NOMEIQ'
        DataSource = DsPQ
        TabOrder = 2
      end
      object DBEdit04: TDBEdit
        Left = 668
        Top = 60
        Width = 113
        Height = 21
        DataField = 'NOMEGGXNiv2'
        DataSource = DsPQ
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 5112467
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 60
        Width = 160
        Height = 21
        DataField = 'NOMESETOR'
        DataSource = DsPQ
        TabOrder = 4
      end
      object DBEdit2: TDBEdit
        Left = 172
        Top = 60
        Width = 493
        Height = 21
        DataField = 'NOMEGRUPOQUIMICO'
        DataSource = DsPQ
        TabOrder = 5
      end
      object dmkDBCheckGroup1: TdmkDBCheckGroup
        Left = 8
        Top = 166
        Width = 773
        Height = 41
        Caption = ' Receitas que utilizar'#227'o este cadastro: '
        Columns = 5
        DataField = 'Receitas'
        DataSource = DsPQ
        Items.Strings = (
          'Caleiro'
          'Curtimento'
          'Recurtimento'
          'Acabamento'
          'Outras')
        ParentBackground = False
        TabOrder = 6
      end
      object DBEdit3: TDBEdit
        Left = 788
        Top = 20
        Width = 88
        Height = 21
        DataField = 'PercQuebra'
        DataSource = DsPQ
        TabOrder = 7
      end
      object GroupBox6: TGroupBox
        Left = 960
        Top = 68
        Width = 165
        Height = 145
        Caption = ' Recupera'#231#227'o de impostos: '
        TabOrder = 8
        Visible = False
        object GroupBox7: TGroupBox
          Left = 6
          Top = 15
          Width = 75
          Height = 60
          Caption = ' ICMS: '
          TabOrder = 0
          object Label39: TLabel
            Left = 7
            Top = 16
            Width = 54
            Height = 13
            Caption = '% Red BC.:'
          end
          object DBEdit4: TDBEdit
            Left = 8
            Top = 32
            Width = 60
            Height = 21
            DataField = 'ICMS_pRedBC'
            TabOrder = 0
          end
        end
        object GroupBox8: TGroupBox
          Left = 84
          Top = 15
          Width = 75
          Height = 60
          Caption = ' IPI: '
          TabOrder = 1
          object Label40: TLabel
            Left = 11
            Top = 16
            Width = 54
            Height = 13
            Caption = '% Red BC.:'
          end
          object DBEdit5: TDBEdit
            Left = 8
            Top = 32
            Width = 60
            Height = 21
            DataField = 'IPI_pRedBC'
            TabOrder = 0
          end
        end
        object GroupBox9: TGroupBox
          Left = 5
          Top = 79
          Width = 75
          Height = 60
          Caption = ' PIS: '
          TabOrder = 2
          object Label41: TLabel
            Left = 7
            Top = 16
            Width = 54
            Height = 13
            Caption = '% Red BC.:'
          end
          object DBEdit6: TDBEdit
            Left = 8
            Top = 32
            Width = 60
            Height = 21
            DataField = 'PIS_pRedBC'
            TabOrder = 0
          end
        end
        object GroupBox10: TGroupBox
          Left = 82
          Top = 79
          Width = 75
          Height = 60
          Caption = ' COFINS: '
          TabOrder = 3
          object Label42: TLabel
            Left = 7
            Top = 16
            Width = 54
            Height = 13
            Caption = '% Red BC.:'
          end
          object DBEdit7: TDBEdit
            Left = 8
            Top = 32
            Width = 60
            Height = 21
            DataField = 'COFINS_pRedBC'
            TabOrder = 0
          end
        end
      end
      object DBRGGGXNiv2: TDBRadioGroup
        Left = 12
        Top = 88
        Width = 773
        Height = 78
        Caption = ' Grupo: '
        DataField = 'GGXNiv2'
        DataSource = DsPQ
        Items.Strings = (
          'MyObjects.PreencheComponente(DBRGAtivo, sListaTipoCadPQ, 7)')
        TabOrder = 9
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9'
          '10'
          '11'
          '12'
          '13'
          '14'
          '15'
          '16')
        OnClick = RGGGXNiv2Click
      end
      object DBCheckBox2: TDBCheckBox
        Left = 788
        Top = 187
        Width = 50
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsPQ
        TabOrder = 10
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit8: TDBEdit
        Left = 788
        Top = 60
        Width = 88
        Height = 21
        DataField = 'KgUsoDd'
        DataSource = DsPQ
        TabOrder = 11
      end
      object DBEdit9: TDBEdit
        Left = 788
        Top = 100
        Width = 88
        Height = 21
        DataField = 'KgEstqSegur'
        DataSource = DsPQ
        TabOrder = 12
      end
      object DBCheckBox1: TDBCheckBox
        Left = 788
        Top = 124
        Width = 97
        Height = 17
        Caption = 'Usar densidade.'
        DataField = 'UsarDensid'
        DataSource = DsPQ
        TabOrder = 13
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit10: TDBEdit
        Left = 788
        Top = 160
        Width = 68
        Height = 21
        DataField = 'Densidade'
        DataSource = DsPQ
        TabOrder = 14
      end
      object DBEdit11: TDBEdit
        Left = 860
        Top = 160
        Width = 68
        Height = 21
        DataField = 'CusIntrn'
        DataSource = DsPQ
        TabOrder = 15
      end
    end
    object GBConfCliFor: TGroupBox
      Left = 0
      Top = 395
      Width = 1197
      Height = 64
      Align = alBottom
      TabOrder = 2
      Visible = False
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 1193
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 1048
          Top = 0
          Width = 145
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste2: TBitBtn
            Tag = 15
            Left = 15
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesiste2Click
          end
        end
        object BtConfirma2: TBitBtn
          Tag = 14
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirma2Click
        end
        object BtPadrao: TBitBtn
          Tag = 176
          Left = 348
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Padr'#227'o'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtPadraoClick
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 459
      Width = 1197
      Height = 64
      Align = alBottom
      TabOrder = 3
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 500
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 674
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel1: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtExcluiClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 14
      Top = -9
      Width = 1009
      Height = 423
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Clientes internos  '
        object PnEditFor: TPanel
          Left = 0
          Top = 109
          Width = 1001
          Height = 48
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object Label24: TLabel
            Left = 572
            Top = 4
            Width = 94
            Height = 13
            Caption = 'Prazo dd/dd/dd ... :'
          end
          object Label27: TLabel
            Left = 6
            Top = 4
            Width = 57
            Height = 13
            Caption = 'Fornecedor:'
          end
          object SpeedButton9: TSpeedButton
            Left = 544
            Top = 20
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SpeedButton9Click
          end
          object Label56: TLabel
            Left = 780
            Top = 4
            Width = 57
            Height = 13
            Caption = 'Relev'#226'ncia:'
          end
          object EdPrazo: TdmkEdit
            Left = 572
            Top = 20
            Width = 204
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdPrazoExit
          end
          object EdFornecedor: TdmkEditCB
            Left = 6
            Top = 20
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBFornecedor
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFornecedor: TdmkDBLookupComboBox
            Left = 72
            Top = 20
            Width = 472
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOME_E_DOC_ENTIDADE'
            ListSource = DsEntiFor2
            TabOrder = 1
            dmkEditCB = EdFornecedor
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdRelevancia: TdmkEdit
            Left = 780
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '99'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnExit = EdPrazoExit
          end
        end
        object PnEditCot: TPanel
          Left = 0
          Top = 157
          Width = 1001
          Height = 85
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          Visible = False
          object Label3: TLabel
            Left = 6
            Top = 44
            Width = 58
            Height = 13
            Caption = 'Embalagem:'
          end
          object Label5: TLabel
            Left = 8
            Top = 4
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object Label18: TLabel
            Left = 388
            Top = 4
            Width = 61
            Height = 13
            AutoSize = False
            Caption = 'Pre'#231'o L/kg:'
          end
          object Label23: TLabel
            Left = 456
            Top = 4
            Width = 40
            Height = 13
            Caption = '% ICMS:'
          end
          object Label19: TLabel
            Left = 516
            Top = 4
            Width = 60
            Height = 13
            AutoSize = False
            Caption = '% IPI: '
          end
          object Label20: TLabel
            Left = 584
            Top = 4
            Width = 31
            Height = 13
            Caption = '% PIS:'
          end
          object Label21: TLabel
            Left = 648
            Top = 4
            Width = 60
            Height = 13
            AutoSize = False
            Caption = '% COFINS:'
          end
          object Label6: TLabel
            Left = 388
            Top = 44
            Width = 64
            Height = 13
            AutoSize = False
            Caption = 'Frete L/kg:'
          end
          object Label7: TLabel
            Left = 456
            Top = 44
            Width = 48
            Height = 13
            Caption = '% RICMS:'
          end
          object Label11: TLabel
            Left = 516
            Top = 44
            Width = 60
            Height = 13
            AutoSize = False
            Caption = '% RIPI: '
          end
          object Label12: TLabel
            Left = 584
            Top = 44
            Width = 39
            Height = 13
            Caption = '% RPIS:'
          end
          object Label13: TLabel
            Left = 648
            Top = 44
            Width = 60
            Height = 13
            AutoSize = False
            Caption = '% RCOFINS:'
          end
          object Label17: TLabel
            Left = 712
            Top = 44
            Width = 61
            Height = 13
            AutoSize = False
            Caption = 'Custo $/kg:'
          end
          object Label25: TLabel
            Left = 712
            Top = 4
            Width = 61
            Height = 13
            AutoSize = False
            Caption = 'Custo kg:'
          end
          object SpeedButton10: TSpeedButton
            Left = 360
            Top = 60
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SpeedButton10Click
          end
          object EdEmb: TdmkEditCB
            Left = 6
            Top = 60
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmb
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmb: TdmkDBLookupComboBox
            Left = 72
            Top = 60
            Width = 288
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEmbalagens
            TabOrder = 8
            dmkEditCB = EdEmb
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object TPDataCot: TDateTimePicker
            Left = 6
            Top = 20
            Width = 97
            Height = 21
            Date = 38788.000000000000000000
            Time = 0.663026979200367400
            TabOrder = 0
          end
          object RGMoeda: TRadioGroup
            Left = 108
            Top = 4
            Width = 273
            Height = 38
            Caption = ' Moeda: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'R$ '
              'U$'
              #8364
              '??')
            TabOrder = 1
            OnClick = RGMoedaClick
          end
          object EdPreco: TdmkEdit
            Left = 388
            Top = 20
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdPrecoExit
          end
          object EdICMS: TdmkEdit
            Left = 456
            Top = 20
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdICMSExit
          end
          object EdIPI: TdmkEdit
            Left = 520
            Top = 20
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdIPIExit
          end
          object EdPIS: TdmkEdit
            Left = 584
            Top = 20
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdPISExit
          end
          object EdCOFINS: TdmkEdit
            Left = 648
            Top = 20
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdCOFINSExit
          end
          object EdFrete: TdmkEdit
            Left = 388
            Top = 60
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdFreteExit
          end
          object EdRICMS: TdmkEdit
            Left = 456
            Top = 60
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdRICMSExit
          end
          object EdRIPI: TdmkEdit
            Left = 520
            Top = 60
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdRIPIExit
          end
          object EdRPIS: TdmkEdit
            Left = 584
            Top = 60
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdRPISExit
          end
          object EdRCOFINS: TdmkEdit
            Left = 648
            Top = 60
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 13
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdRCOFINSExit
          end
          object EdReais: TdmkEdit
            Left = 712
            Top = 60
            Width = 64
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 14
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdPrecoExit
          end
          object EdCusto: TdmkEdit
            Left = 712
            Top = 20
            Width = 64
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 15
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdPrecoExit
          end
        end
        object GridPanel1: TGridPanel
          Left = 0
          Top = 242
          Width = 1001
          Height = 153
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Align = alClient
          BevelOuter = bvNone
          ColumnCollection = <
            item
              Value = 100.000000000000000000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = Panel2
              Row = 0
            end
            item
              Column = 0
              Control = GridPanel2
              Row = 1
            end>
          RowCollection = <
            item
              Value = 50.000000000000000000
            end
            item
              Value = 50.000000000000000000
            end>
          TabOrder = 3
          object Panel2: TPanel
            Left = 0
            Top = 0
            Width = 1001
            Height = 76
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            TabOrder = 0
            object Panel4: TPanel
              Left = 1
              Top = 1
              Width = 999
              Height = 44
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object DBGrid2: TDBGrid
                Left = 0
                Top = 0
                Width = 441
                Height = 44
                Align = alLeft
                DataSource = DsPQCli
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'ID'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMECLII'
                    Title.Caption = 'Cliente interno'
                    Width = 180
                    Visible = True
                  end
                  item
                    Alignment = taRightJustify
                    Expanded = False
                    FieldName = 'NOMEMOEDAPADRAO'
                    Title.Alignment = taCenter
                    Title.Caption = 'M'
                    Width = 24
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CustoPadrao'
                    Title.Caption = 'Custo padr'#227'o'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Peso'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Valor'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Custo'
                    Visible = True
                  end>
              end
              object DBGrid1: TDBGrid
                Left = 441
                Top = 0
                Width = 324
                Height = 44
                Align = alLeft
                DataSource = DsPQFor
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NOMEIQ'
                    Title.Caption = 'Fornecedor do cliente interno'
                    Width = 180
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Prazo'
                    Width = 106
                    Visible = True
                  end>
              end
              object DBGrid4: TDBGrid
                Left = 765
                Top = 0
                Width = 234
                Height = 44
                Align = alClient
                DataSource = DsPQForEmb
                TabOrder = 2
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'cProd'
                    Title.Caption = 'C'#243'digo fornecedor'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEEMBLAGEM'
                    Title.Caption = 'Embalagem'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Observacao'
                    Visible = True
                  end>
              end
            end
            object DBGrid3: TDBGrid
              Left = 1
              Top = 45
              Width = 999
              Height = 30
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsPQCot
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'DataCot'
                  Title.Caption = 'Cota'#231#227'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEEMBLAGEM'
                  Title.Caption = 'Embalagem'
                  Width = 80
                  Visible = True
                end
                item
                  Alignment = taRightJustify
                  Expanded = False
                  FieldName = 'NOMEMOEDA'
                  Title.Alignment = taCenter
                  Title.Caption = 'M'
                  Width = 24
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Preco'
                  Title.Caption = 'Pre'#231'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ICMS'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PIS'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'COFINS'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IPI'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RICMS'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RPIS'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RCOFINS'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RIPI'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Frete'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Custo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'REAIS'
                  Title.Caption = '$'
                  Visible = True
                end>
            end
          end
          object GridPanel2: TGridPanel
            Left = 0
            Top = 76
            Width = 1001
            Height = 77
            Margins.Left = 2
            Margins.Top = 2
            Margins.Right = 2
            Margins.Bottom = 2
            Align = alClient
            BevelOuter = bvNone
            ColumnCollection = <
              item
                Value = 50.000000000000000000
              end
              item
                Value = 50.000000000000000000
              end>
            ControlCollection = <
              item
                Column = 0
                Control = GroupBox11
                Row = 0
              end
              item
                Column = 1
                Control = GroupBox12
                Row = 0
              end>
            RowCollection = <
              item
                Value = 100.000000000000000000
              end>
            TabOrder = 1
            object GroupBox11: TGroupBox
              Left = 0
              Top = 0
              Width = 500
              Height = 77
              Margins.Left = 2
              Margins.Top = 2
              Margins.Right = 2
              Margins.Bottom = 2
              Align = alClient
              Caption = 
                ' Receitas ribeira onde o produto '#233' utilizado (Duplo clique para ' +
                'selecionar) '
              TabOrder = 0
              object DBGrid5: TdmkDBGrid
                Left = 2
                Top = 15
                Width = 496
                Height = 60
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Numero'
                    Title.Caption = 'Receita'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_FRM'
                    Title.Caption = 'Nome da receita'
                    Width = 192
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ordem'
                    Width = 36
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Porcent'
                    Title.Caption = '% uso'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ativo'
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsFormulasIts
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnDblClick = DBGrid5DblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Numero'
                    Title.Caption = 'Receita'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_FRM'
                    Title.Caption = 'Nome da receita'
                    Width = 192
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ordem'
                    Width = 36
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Porcent'
                    Title.Caption = '% uso'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ativo'
                    Visible = True
                  end>
              end
            end
            object GroupBox12: TGroupBox
              Left = 500
              Top = 0
              Width = 501
              Height = 77
              Margins.Left = 2
              Margins.Top = 2
              Margins.Right = 2
              Margins.Bottom = 2
              Align = alClient
              Caption = 
                ' Receitas acabamento onde o produto '#233' utilizado (Duplo clique pa' +
                'ra selecionar) '
              TabOrder = 1
              object DBGrid6: TdmkDBGrid
                Left = 2
                Top = 15
                Width = 497
                Height = 60
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Numero'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_FRM'
                    Title.Caption = 'Nome da receita'
                    Width = 184
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TIN'
                    Title.Caption = 'Nome da tinta'
                    Width = 184
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ordem'
                    Width = 36
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GramasTi'
                    Title.Caption = 'g/vol'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GramasKg'
                    Title.Caption = 'g/Kg'
                    Width = 76
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ativo'
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsTintasIts
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnDblClick = DBGrid6DblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Numero'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_FRM'
                    Title.Caption = 'Nome da receita'
                    Width = 184
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TIN'
                    Title.Caption = 'Nome da tinta'
                    Width = 184
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ordem'
                    Width = 36
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GramasTi'
                    Title.Caption = 'g/vol'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GramasKg'
                    Title.Caption = 'g/Kg'
                    Width = 76
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ativo'
                    Visible = True
                  end>
              end
            end
          end
        end
        object PnEditCli: TPanel
          Left = 0
          Top = 0
          Width = 1001
          Height = 109
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          Visible = False
          object Label28: TLabel
            Left = 376
            Top = 2
            Width = 68
            Height = 13
            Caption = 'Est.segur.(dd):'
          end
          object Label29: TLabel
            Left = 448
            Top = 2
            Width = 60
            Height = 13
            Caption = 'Est. m'#237'n. kg:'
          end
          object Label36: TLabel
            Left = 6
            Top = 4
            Width = 70
            Height = 13
            Caption = 'Cliente interno:'
          end
          object SpeedButton8: TSpeedButton
            Left = 351
            Top = 20
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SpeedButton8Click
          end
          object Label33: TLabel
            Left = 709
            Top = 4
            Width = 82
            Height = 13
            Caption = 'Cus.'#250'lt.cmpr?.kg:'
          end
          object Label43: TLabel
            Left = 273
            Top = 53
            Width = 87
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'C'#243'digo do insumo:'
          end
          object Label48: TLabel
            Left = 448
            Top = 53
            Width = 64
            Height = 13
            Caption = 'kg l'#237'q embal.:'
          end
          object Label55: TLabel
            Left = 709
            Top = 52
            Width = 60
            Height = 13
            Caption = 'Cus.cota.kg:'
          end
          object EdEstSegur: TdmkEdit
            Left = 376
            Top = 20
            Width = 68
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdMinEstq: TdmkEdit
            Left = 448
            Top = 20
            Width = 81
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCI: TdmkEditCB
            Left = 6
            Top = 20
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCI
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCI: TdmkDBLookupComboBox
            Left = 72
            Top = 20
            Width = 280
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEntiCli2
            TabOrder = 1
            dmkEditCB = EdCI
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object RGMoedaPadrao: TRadioGroup
            Left = 532
            Top = 4
            Width = 169
            Height = 38
            Caption = ' Moeda '#250'ltima compra?: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'R$ '
              'U$'
              #8364
              '??')
            TabOrder = 4
            OnClick = RGMoedaClick
          end
          object RGCustoPadraoAtz: TRadioGroup
            Left = 7
            Top = 46
            Width = 262
            Height = 48
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Atualiza o cuto padr'#227'o ('#250'ltima compra) do produto: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'Escolher'
              'Atualiza '
              'N'#195'O Atualiza')
            TabOrder = 6
          end
          object EdCodProprio: TdmkEdit
            Left = 273
            Top = 69
            Width = 172
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdPrazoExit
          end
          object EdCustoPadrao: TdmkEdit
            Left = 709
            Top = 20
            Width = 90
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdCustoPadraoExit
          end
          object EdKgLiqEmb: TdmkEdit
            Left = 448
            Top = 69
            Width = 81
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object RGMoedaMan: TRadioGroup
            Left = 532
            Top = 52
            Width = 169
            Height = 38
            Caption = ' Moeda cota'#231#227'o: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'R$ '
              'U$'
              #8364
              '??')
            TabOrder = 9
          end
          object EdCustoMan: TdmkEdit
            Left = 709
            Top = 68
            Width = 90
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdCustoPadraoExit
          end
        end
      end
      object TabSheet4: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Entradas'
        ImageIndex = 3
        object LaAviso: TLabel
          Left = 0
          Top = 46
          Width = 1001
          Height = 16
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Align = alTop
          Caption = 
            'Para selecionar entrada d'#234' um duplo clique no item correspondent' +
            'e na grade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitWidth = 536
        end
        object DBGrid7: TDBGrid
          Left = 0
          Top = 62
          Width = 1001
          Height = 333
          Align = alClient
          DataSource = DsPQE
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid7DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'CI'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLIINT'
              Title.Caption = 'Nome do Cliente Interno'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IQ'
              Title.Caption = 'ID IQ'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFORNECEDOR'
              Title.Caption = 'Nome do Fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Insumo'
              Title.Caption = 'ID PQ'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEINSUMO'
              Title.Caption = 'Nome do Insumo'
              Width = 172
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NF'
              Title.Caption = 'NF VP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NF_RP'
              Title.Caption = 'NF RP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NF_CC'
              Title.Caption = 'NF CC'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID Entrada'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conhecimento'
              Title.Caption = 'Conh.Frete'
              Width = 56
              Visible = True
            end>
        end
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 1001
          Height = 46
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Align = alTop
          ParentBackground = False
          TabOrder = 1
          object Label44: TLabel
            Left = 5
            Top = 2
            Width = 55
            Height = 13
            Caption = 'Data inicial:'
          end
          object Label45: TLabel
            Left = 121
            Top = 2
            Width = 48
            Height = 13
            Caption = 'Data final:'
          end
          object TPIni: TdmkEditDateTimePicker
            Left = 5
            Top = 18
            Width = 112
            Height = 21
            Date = 37670.000000000000000000
            Time = 0.521748657396528900
            TabOrder = 0
            OnExit = TPIniExit
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPFim: TdmkEditDateTimePicker
            Left = 121
            Top = 18
            Width = 112
            Height = 21
            Date = 37670.000000000000000000
            Time = 0.521748657396528900
            TabOrder = 1
            OnExit = TPFimExit
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
      end
      object TSGrade: TTabSheet
        Caption = ' Grade '
        ImageIndex = 1
      end
      object TSTabEstoque: TTabSheet
        Caption = ' Tabelas de estoque '
        ImageIndex = 2
      end
    end
  end
  object PainelEdita: TPanel
    Left = 612
    Top = 36
    Width = 1008
    Height = 536
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 472
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label34: TLabel
          Left = 156
          Top = 19
          Width = 261
          Height = 13
          Caption = '%Red BC.:  Percentual de redu'#231#227'o da base de c'#225'lculo.'
        end
        object Label58: TLabel
          Left = 156
          Top = 2
          Width = 330
          Height = 13
          Caption = 
            '$ *CIPC: Custo interno de produtos coadjuvantes. (ex: '#225'gua e rec' +
            'iclo).'
        end
        object Panel9: TPanel
          Left = 859
          Top = 0
          Width = 145
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 0
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 265
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 76
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 380
        Top = 4
        Width = 93
        Height = 13
        Caption = 'Fornecedor padr'#227'o:'
      end
      object Label26: TLabel
        Left = 8
        Top = 44
        Width = 140
        Height = 13
        Caption = 'Setor controlador do estoque:'
      end
      object Label30: TLabel
        Left = 380
        Top = 44
        Width = 32
        Height = 13
        Caption = 'Grupo:'
      end
      object SpeedButton5: TSpeedButton
        Left = 760
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object SpeedButton6: TSpeedButton
        Left = 760
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object SpeedButton7: TSpeedButton
        Left = 356
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton7Click
      end
      object Label14: TLabel
        Left = 788
        Top = 4
        Width = 62
        Height = 13
        Caption = '% de quebra:'
      end
      object SBUnidMed: TSpeedButton
        Left = 404
        Top = 227
        Width = 21
        Height = 22
        Caption = '...'
        OnClick = SBUnidMedClick
      end
      object Label46: TLabel
        Left = 116
        Top = 212
        Width = 117
        Height = 13
        Caption = 'Unidade de Medida [F3]:'
      end
      object Label47: TLabel
        Left = 12
        Top = 212
        Width = 27
        Height = 13
        Caption = 'NCM:'
      end
      object Label49: TLabel
        Left = 788
        Top = 44
        Width = 84
        Height = 13
        Caption = 'Qtd gasto por dia:'
      end
      object Label50: TLabel
        Left = 788
        Top = 84
        Width = 84
        Height = 13
        Caption = 'Qtd estq seguran:'
      end
      object Label53: TLabel
        Left = 788
        Top = 144
        Width = 80
        Height = 13
        Caption = 'Densidade kg/L:'
      end
      object LaCusIntrn: TLabel
        Left = 428
        Top = 212
        Width = 40
        Height = 13
        Caption = '$ *CIPC:'
        Enabled = False
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 64
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 20
        Width = 300
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdIQ: TdmkEditCB
        Left = 380
        Top = 20
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBIQ
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBIQ: TdmkDBLookupComboBox
        Left = 447
        Top = 20
        Width = 310
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEntiFor2
        TabOrder = 3
        dmkEditCB = EdIQ
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object RGGGXNiv2: TRadioGroup
        Left = 8
        Top = 88
        Width = 773
        Height = 78
        Caption = ' Grupo: '
        Items.Strings = (
          'MyObjects.PreencheComponente(DBRGAtivo, sListaTipoCadPQ, 7)')
        TabOrder = 8
        OnClick = RGGGXNiv2Click
      end
      object EdSetor: TdmkEditCB
        Left = 8
        Top = 60
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSetor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSetor: TdmkDBLookupComboBox
        Left = 75
        Top = 60
        Width = 278
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSetores
        TabOrder = 5
        dmkEditCB = EdSetor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdGrupo: TdmkEditCB
        Left = 380
        Top = 60
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGrupo
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGrupo: TdmkDBLookupComboBox
        Left = 447
        Top = 60
        Width = 310
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGruposQuimicos
        TabOrder = 7
        dmkEditCB = EdGrupo
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CGReceitas: TdmkCheckGroup
        Left = 8
        Top = 168
        Width = 773
        Height = 40
        Caption = ' Receitas que utilizar'#227'o este cadastro: '
        Columns = 5
        ItemIndex = 0
        Items.Strings = (
          'Caleiro'
          'Curtimento'
          'Recurtimento'
          'Acabamento'
          'Outras')
        TabOrder = 9
        UpdType = utYes
        Value = 1
        OldValor = 0
      end
      object EdPercQuebra: TdmkEdit
        Left = 788
        Top = 20
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '2,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 2.000000000000000000
        ValWarn = False
      end
      object GroupBox1: TGroupBox
        Left = 904
        Top = 4
        Width = 165
        Height = 169
        Caption = ' Recupera'#231#227'o de impostos: '
        TabOrder = 12
        Visible = False
        object GroupBox2: TGroupBox
          Left = 6
          Top = 19
          Width = 75
          Height = 68
          Caption = ' ICMS: '
          TabOrder = 0
          object Label16: TLabel
            Left = 7
            Top = 16
            Width = 54
            Height = 13
            Caption = '% Red BC.:'
          end
          object EdICMS_pRedBC_: TdmkEdit
            Left = 8
            Top = 31
            Width = 61
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '100'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GroupBox3: TGroupBox
          Left = 84
          Top = 19
          Width = 75
          Height = 68
          Caption = ' IPI: '
          TabOrder = 1
          object Label35: TLabel
            Left = 11
            Top = 16
            Width = 54
            Height = 13
            Caption = '% Red BC.:'
          end
          object EdIPI_pRedBC_: TdmkEdit
            Left = 8
            Top = 31
            Width = 61
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '100'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GroupBox4: TGroupBox
          Left = 5
          Top = 91
          Width = 75
          Height = 68
          Caption = ' PIS: '
          TabOrder = 2
          object Label37: TLabel
            Left = 7
            Top = 16
            Width = 54
            Height = 13
            Caption = '% Red BC.:'
          end
          object EdPIS_pRedBC_: TdmkEdit
            Left = 8
            Top = 31
            Width = 61
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '100'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GroupBox5: TGroupBox
          Left = 82
          Top = 91
          Width = 75
          Height = 68
          Caption = ' COFINS: '
          TabOrder = 3
          object Label38: TLabel
            Left = 7
            Top = 16
            Width = 54
            Height = 13
            Caption = '% Red BC.:'
          end
          object EdCOFINS_pRedBC_: TdmkEdit
            Left = 8
            Top = 31
            Width = 61
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '100'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
      object EdSigla: TdmkEdit
        Left = 172
        Top = 228
        Width = 40
        Height = 21
        TabOrder = 15
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdSiglaChange
        OnExit = EdSiglaExit
        OnKeyDown = EdSiglaKeyDown
      end
      object CBUnidMed: TdmkDBLookupComboBox
        Left = 212
        Top = 228
        Width = 193
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsUnidMed
        TabOrder = 16
        dmkEditCB = EdUnidMed
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdUnidMed: TdmkEditCB
        Left = 116
        Top = 228
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdUnidMedChange
        OnKeyDown = EdUnidMedKeyDown
        DBLookupComboBox = CBUnidMed
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdNCM: TdmkEdit
        Left = 12
        Top = 228
        Width = 100
        Height = 21
        TabOrder = 13
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NCM'
        UpdCampo = 'NCM'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnRedefinido = EdNCMRedefinido
      end
      object EdKgUsoDd: TdmkEdit
        Left = 788
        Top = 60
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdKgEstqSegur: TdmkEdit
        Left = 788
        Top = 100
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CkUsarDensid: TdmkCheckBox
        Left = 788
        Top = 124
        Width = 105
        Height = 17
        Caption = 'Usar densidade.'
        TabOrder = 19
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object EdDensidade: TdmkEdit
        Left = 788
        Top = 160
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 20
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCusIntrn: TdmkEdit
        Left = 428
        Top = 228
        Width = 88
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 21
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'CusIntrn'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CkAtivo: TdmkCheckBox
        Left = 792
        Top = 184
        Width = 51
        Height = 17
        Caption = 'Ativo'
        TabOrder = 10
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1197
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1149
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 257
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtGraGruN: TBitBtn
        Tag = 447
        Left = 215
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtGraGruNClick
      end
    end
    object GB_M: TGroupBox
      Left = 257
      Top = 0
      Width = 892
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 349
        Height = 32
        Caption = 'Produtos de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 349
        Height = 32
        Caption = 'Produtos de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 349
        Height = 32
        Caption = 'Produtos de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1197
    Height = 57
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 32
      Width = 1193
      Height = 23
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object PB1: TProgressBar
      Left = 2
      Top = 15
      Width = 1193
      Height = 17
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Align = alTop
      TabOrder = 1
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 132
    Top = 100
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPQBeforeOpen
    AfterOpen = QrPQAfterOpen
    BeforeClose = QrPQBeforeClose
    AfterScroll = QrPQAfterScroll
    OnCalcFields = QrPQCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOMEIQ, ls.Nome NOMESETOR, '
      'gq.Nome NOMEGRUPOQUIMICO, pq.* '
      'FROM pq pq'
      'LEFT JOIN entidades      fo ON fo.Codigo=pq.IQ'
      'LEFT JOIN listasetores   ls ON ls.Codigo=pq.Setor'
      'LEFT JOIN gruposquimicos gq ON gq.Codigo=pq.GrupoQuimico'
      'WHERE GrupoQuimico=:P0'
      'ORDER BY Nome')
    Left = 132
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQNOMEIQ: TWideStringField
      FieldName = 'NOMEIQ'
      Size = 100
    end
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPQIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQGrupoQuimico: TIntegerField
      FieldName = 'GrupoQuimico'
    end
    object QrPQSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrPQNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrPQNOMEGRUPOQUIMICO: TWideStringField
      FieldName = 'NOMEGRUPOQUIMICO'
      Size = 100
    end
    object QrPQReceitas: TIntegerField
      FieldName = 'Receitas'
    end
    object QrPQPercQuebra: TFloatField
      FieldName = 'PercQuebra'
      DisplayFormat = '0.00'
    end
    object QrPQGGXNiv2: TIntegerField
      FieldName = 'GGXNiv2'
    end
    object QrPQNOMEGGXNiv2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEGGXNiv2'
      Size = 30
      Calculated = True
    end
    object QrPQKgUsoDd: TFloatField
      FieldName = 'KgUsoDd'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQKgEstqSegur: TFloatField
      FieldName = 'KgEstqSegur'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQUsarDensid: TSmallintField
      FieldName = 'UsarDensid'
    end
    object QrPQDensidade: TFloatField
      FieldName = 'Densidade'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPQCusIntrn: TFloatField
      FieldName = 'CusIntrn'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object QrEntiFor2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome,'
      'CONCAT(IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome),'
      '  " - ", IF(ent.Tipo=0, ent.CNPJ, ent.CPF)) NOME_E_DOC_ENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece2="V"'
      'ORDER BY Nome'
      '')
    Left = 560
    Top = 8
    object QrEntiFor2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiFor2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrEntiFor2NOME_E_DOC_ENTIDADE: TWideStringField
      FieldName = 'NOME_E_DOC_ENTIDADE'
      Size = 121
    end
  end
  object DsEntiFor2: TDataSource
    DataSet = QrEntiFor2
    Left = 588
    Top = 8
  end
  object QrPQFor: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPQForAfterOpen
    BeforeClose = QrPQForBeforeClose
    AfterScroll = QrPQForAfterScroll
    SQL.Strings = (
      'SELECT CASE WHEN fiq.Tipo=0 THEN fiq.RazaoSocial'
      'ELSE fiq.Nome END NOMEIQ, piq.* '
      'FROM pqfor piq'
      'LEFT JOIN entidades fiq ON fiq.Codigo=piq.IQ'
      'WHERE piq.PQ=:P0'
      'AND piq.CI=:P1'
      'ORDER BY NOMEIQ')
    Left = 488
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPQForNOMEIQ: TWideStringField
      FieldName = 'NOMEIQ'
      Size = 100
    end
    object QrPQForControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQForIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQForPQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrPQForCI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQForPrazo: TWideStringField
      FieldName = 'Prazo'
    end
    object QrPQForLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQForDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQForDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQForUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQForUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQForRelevancia: TSmallintField
      FieldName = 'Relevancia'
    end
  end
  object DsPQFor: TDataSource
    DataSet = QrPQFor
    Left = 516
    Top = 64
  end
  object QrSetores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM listasetores'
      'ORDER BY Nome')
    Left = 560
    Top = 64
    object QrSetoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.listasetores.Codigo'
    end
    object QrSetoresNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.listasetores.Nome'
      Size = 12
    end
  end
  object DsSetores: TDataSource
    DataSet = QrSetores
    Left = 588
    Top = 64
  end
  object DsEntiCli2: TDataSource
    DataSet = QrEntiCli2
    Left = 588
    Top = 36
  end
  object QrEntiCli2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END Nome'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 560
    Top = 36
    object QrEntiCli2Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.artigosgrupos.Nome'
      Size = 32
    end
    object QrEntiCli2Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.artigosgrupos.Codigo'
    end
  end
  object PMInclui: TPopupMenu
    OnPopup = PMIncluiPopup
    Left = 344
    Top = 396
    object IncluinovoProdutoqumico1: TMenuItem
      Caption = 'Inclui novo &Material'
      OnClick = IncluinovoProdutoqumico1Click
    end
    object IncluiClienteinterno1: TMenuItem
      Caption = 'Inclui &Cliente interno'
      OnClick = IncluiClienteinterno1Click
    end
    object IncluiFornecedoraoclienteinterno1: TMenuItem
      Caption = 'Inclui &Fornecedor ao cliente interno'
      OnClick = IncluiFornecedoraoclienteinterno1Click
    end
    object IncluiEmbalagemaoFornecedor1: TMenuItem
      Caption = 'Inclui &Embalagem ao Fornecedor'
      Enabled = False
      OnClick = IncluiEmbalagemaoFornecedor1Click
    end
    object IncluicoTaodepreo1: TMenuItem
      Caption = 'Inclui co&Ta'#231#227'o de pre'#231'o'
      OnClick = IncluicoTaodepreo1Click
    end
  end
  object PMAltera: TPopupMenu
    OnPopup = PMAlteraPopup
    Left = 428
    Top = 396
    object AlteracadastrodoProdutoqumico1: TMenuItem
      Caption = 'Altera cadastro do &Material'
      OnClick = AlteracadastrodoProdutoqumico1Click
    end
    object AlteradadosdoClienteinterno1: TMenuItem
      Caption = 'Altera dados do &Cliente interno'
      OnClick = AlteradadosdoClienteinterno1Click
    end
    object AlteradadosdoFornecedordoclienteinterno1: TMenuItem
      Caption = 'Altera dados do &Fornecedor do cliente interno'
      OnClick = AlteradadosdoFornecedordoclienteinterno1Click
    end
    object Alteraembalagemdefornecedorselecionada1: TMenuItem
      Caption = '&Altera embalagem de fornecedor selecionada'
      Enabled = False
      OnClick = Alteraembalagemdefornecedorselecionada1Click
    end
    object AlteracoTaodepreo1: TMenuItem
      Caption = 'Altera co&Ta'#231#227'o de pre'#231'o'
      OnClick = AlteracoTaodepreo1Click
    end
  end
  object DsPQCli: TDataSource
    DataSet = QrPQCli
    Left = 516
    Top = 36
  end
  object QrPQCli: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPQCliAfterOpen
    AfterScroll = QrPQCliAfterScroll
    OnCalcFields = QrPQCliCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECLII, pci.* '
      'FROM pqcli pci'
      'LEFT JOIN entidades cli ON cli.Codigo=pci.CI'
      'WHERE pci.PQ=:P0'
      'ORDER BY NOMECLII'
      '')
    Left = 488
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQCliNOMECLII: TWideStringField
      FieldName = 'NOMECLII'
      Size = 100
    end
    object QrPQCliControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQCliPQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrPQCliCI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQCliMinEstq: TFloatField
      FieldName = 'MinEstq'
    end
    object QrPQCliEstSegur: TIntegerField
      FieldName = 'EstSegur'
    end
    object QrPQCliLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQCliDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQCliDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQCliUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQCliUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQCliCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQCliMoedaPadrao: TIntegerField
      FieldName = 'MoedaPadrao'
    end
    object QrPQCliNOMEMOEDAPADRAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMOEDAPADRAO'
      Size = 5
      Calculated = True
    end
    object QrPQCliPeso: TFloatField
      FieldName = 'Peso'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPQCliValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQCliCusto: TFloatField
      FieldName = 'Custo'
      Required = True
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQCliCodProprio: TWideStringField
      FieldName = 'CodProprio'
    end
    object QrPQCliCustoPadraoAtz: TSmallintField
      FieldName = 'CustoPadraoAtz'
    end
    object QrPQCliKgLiqEmb: TFloatField
      FieldName = 'KgLiqEmb'
    end
    object QrPQCliCustoMan: TFloatField
      FieldName = 'CustoMan'
    end
    object QrPQCliMoedaMan: TSmallintField
      FieldName = 'MoedaMan'
    end
  end
  object QrDuplFor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pqfor'
      'WHERE PQ=:P0 '
      'AND CI=:P1'
      'AND IQ=:P2')
    Left = 617
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrEmbalagens: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM embalagens'
      'ORDER BY Nome')
    Left = 560
    Top = 92
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.listasetores.Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.listasetores.Nome'
      Size = 12
    end
  end
  object DsEmbalagens: TDataSource
    DataSet = QrEmbalagens
    Left = 588
    Top = 92
  end
  object QrPQCot: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPQCotCalcFields
    SQL.Strings = (
      'SELECT emb.Nome NOMEEMBLAGEM, cot.*'
      'FROM pqcot cot'
      'LEFT JOIN embalagens emb ON emb.Codigo=cot.Embalagem'
      'WHERE cot.PQ=:P0'
      'AND cot.CI=:P1'
      'AND cot.IQ=:P2'
      'ORDER BY cot.DataCot DESC')
    Left = 488
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPQCotNOMEEMBLAGEM: TWideStringField
      FieldName = 'NOMEEMBLAGEM'
    end
    object QrPQCotControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQCotIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQCotPQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrPQCotCI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQCotDataCot: TDateField
      FieldName = 'DataCot'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQCotPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrPQCotMoeda: TSmallintField
      FieldName = 'Moeda'
      Required = True
    end
    object QrPQCotFrete: TFloatField
      FieldName = 'Frete'
      DisplayFormat = '#,##0.0000;-#,##0.0000; '
    end
    object QrPQCotICMS: TFloatField
      FieldName = 'ICMS'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrPQCotIPI: TFloatField
      FieldName = 'IPI'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrPQCotPIS: TFloatField
      FieldName = 'PIS'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrPQCotCOFINS: TFloatField
      FieldName = 'COFINS'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrPQCotRICMS: TFloatField
      FieldName = 'RICMS'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrPQCotRIPI: TFloatField
      FieldName = 'RIPI'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrPQCotRPIS: TFloatField
      FieldName = 'RPIS'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrPQCotRCOFINS: TFloatField
      FieldName = 'RCOFINS'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrPQCotLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQCotDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQCotDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQCotUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQCotUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQCotEmbalagem: TIntegerField
      FieldName = 'Embalagem'
    end
    object QrPQCotCusto: TFloatField
      FieldName = 'Custo'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrPQCotNOMEMOEDA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMOEDA'
      Size = 5
      Calculated = True
    end
    object QrPQCotREAIS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'REAIS'
      DisplayFormat = '#,###,###,##0.0000'
      Calculated = True
    end
  end
  object DsPQCot: TDataSource
    DataSet = QrPQCot
    Left = 516
    Top = 92
  end
  object QrGruposQuimicos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM gruposquimicos'
      'ORDER BY Nome')
    Left = 560
    Top = 120
    object QrGruposQuimicosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruposQuimicosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsGruposQuimicos: TDataSource
    DataSet = QrGruposQuimicos
    Left = 588
    Top = 120
  end
  object PMExclui: TPopupMenu
    OnPopup = PMExcluiPopup
    Left = 525
    Top = 402
    object Clienteinterno1: TMenuItem
      Caption = '&Cliente interno'
      OnClick = Clienteinterno1Click
    end
    object Fornecedordoclienteinterno1: TMenuItem
      Caption = '&Fornecedor do cliente interno'
      OnClick = Fornecedordoclienteinterno1Click
    end
    object CotaodePreo1: TMenuItem
      Caption = 'Cota'#231#227'o de &Pre'#231'o'
      OnClick = CotaodePreo1Click
    end
  end
  object QrPQForEmb: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPQCotCalcFields
    SQL.Strings = (
      'SELECT emb.Nome NOMEEMBLAGEM, pfe.*'
      'FROM pqforemb pfe'
      'LEFT JOIN embalagens emb ON emb.Codigo=pfe.Embalagem'
      'WHERE pfe.Controle=:P0')
    Left = 488
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object StringField2: TWideStringField
      FieldName = 'NOMEEMBLAGEM'
    end
    object QrPQForEmbcProd: TWideStringField
      FieldName = 'cProd'
      Size = 60
    end
    object QrPQForEmbControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPQForEmbEmbalagem: TIntegerField
      FieldName = 'Embalagem'
    end
    object QrPQForEmbObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 255
    end
  end
  object DsPQForEmb: TDataSource
    DataSet = QrPQForEmb
    Left = 516
    Top = 120
  end
  object QrFormulasIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT foi.Numero, foi.Ordem, foi.Controle, foi.Porcent,'
      'frm.Nome NO_FRM '
      'FROM formulasits foi'
      'LEFT JOIN formulas frm ON frm.Numero=foi.Numero'
      'WHERE foi.Produto=-2'
      'AND frm.ClienteI=-11'
      'ORDER BY foi.Numero, foi.Ordem')
    Left = 696
    Top = 404
    object QrFormulasItsNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrFormulasItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFormulasItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFormulasItsPorcent: TFloatField
      FieldName = 'Porcent'
      DisplayFormat = '#,###,##0.000'
    end
    object QrFormulasItsNO_FRM: TWideStringField
      FieldName = 'NO_FRM'
      Size = 30
    end
    object QrFormulasItsAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsFormulasIts: TDataSource
    DataSet = QrFormulasIts
    Left = 696
    Top = 452
  end
  object QrTintasIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT foi.Numero, foi.Codigo, foi.Ordem, foi.Controle, foi.Gram' +
        'asTi,'
      'foi.GramasKg, frm.Nome NO_FRM, tin.Nome NO_TIN '
      'FROM tintasits foi'
      'LEFT JOIN tintascab frm ON frm.Numero=foi.Numero'
      'LEFT JOIN tintastin tin ON tin.Codigo=foi.Codigo'
      'WHERE foi.Produto=-2'
      'AND frm.ClienteI=-11'
      'ORDER BY foi.Numero, foi.Ordem')
    Left = 772
    Top = 404
    object QrTintasItsNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrTintasItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrTintasItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTintasItsGramasTi: TFloatField
      FieldName = 'GramasTi'
      DisplayFormat = '#,###,##0.000'
    end
    object QrTintasItsGramasKg: TFloatField
      FieldName = 'GramasKg'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrTintasItsNO_FRM: TWideStringField
      FieldName = 'NO_FRM'
      Size = 100
    end
    object QrTintasItsNO_TIN: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIN'
      Size = 100
      Calculated = True
    end
    object QrTintasItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTintasItsAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsTintasIts: TDataSource
    DataSet = QrTintasIts
    Left = 772
    Top = 452
  end
  object QrPQE: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ei.*, fo.Nome NOMEFORNECEDOR,'
      'pq.Nome NOMEINSUMO, pe.*'
      'FROM pqe pe, PQEIts ei,'
      'entidades fo, PQ pq'
      'WHERE pe.Data BETWEEN '#39'2002-11-20'#39' AND '#39'2003-02-18'#39
      'AND ei.Codigo=pe.Codigo'
      'AND fo.Codigo=pe.IQ'
      'AND pq.Codigo=ei.Insumo'
      'AND pe.Codigo>0'
      'AND pe.NF>0'
      'AND pe.Conhecimento>0'
      'AND pe.IQ>0'
      'AND ei.Insumo>0')
    Left = 844
    Top = 409
    object QrPQECodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pqeits.Codigo'
      DisplayFormat = '000;-000; '
    end
    object QrPQEControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'DBMBWET.pqeits.Controle'
    end
    object QrPQEConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'DBMBWET.pqeits.Conta'
    end
    object QrPQEInsumo: TIntegerField
      FieldName = 'Insumo'
      Origin = 'DBMBWET.pqeits.Insumo'
    end
    object QrPQEVolumes: TIntegerField
      FieldName = 'Volumes'
      Origin = 'DBMBWET.pqeits.Volumes'
    end
    object QrPQEPesoVB: TFloatField
      FieldName = 'PesoVB'
      Origin = 'DBMBWET.pqeits.PesoVB'
    end
    object QrPQEPesoVL: TFloatField
      FieldName = 'PesoVL'
      Origin = 'DBMBWET.pqeits.PesoVL'
    end
    object QrPQEValorItem: TFloatField
      FieldName = 'ValorItem'
      Origin = 'DBMBWET.pqeits.ValorItem'
    end
    object QrPQEIPI: TFloatField
      FieldName = 'IPI'
      Origin = 'DBMBWET.pqeits.IPI'
    end
    object QrPQERIPI: TFloatField
      FieldName = 'RIPI'
      Origin = 'DBMBWET.pqeits.RIPI'
    end
    object QrPQETotalCusto: TFloatField
      FieldName = 'TotalCusto'
      Origin = 'DBMBWET.pqeits.TotalCusto'
    end
    object QrPQETotalPeso: TFloatField
      FieldName = 'TotalPeso'
      Origin = 'DBMBWET.pqeits.TotalPeso'
    end
    object QrPQECFin: TFloatField
      FieldName = 'CFin'
      Origin = 'DBMBWET.pqeits.CFin'
    end
    object QrPQEICMS: TFloatField
      FieldName = 'ICMS'
      Origin = 'DBMBWET.pqeits.ICMS'
    end
    object QrPQERICMS: TFloatField
      FieldName = 'RICMS'
      Origin = 'DBMBWET.pqeits.RICMS'
    end
    object QrPQENOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Origin = 'DBMBWET.fornecedores.Nome'
      Size = 128
    end
    object QrPQENOMEINSUMO: TWideStringField
      FieldName = 'NOMEINSUMO'
      Origin = 'DBMBWET.pq.Nome'
      Size = 128
    end
    object QrPQENF: TIntegerField
      FieldName = 'NF'
      Origin = 'DBMBWET.pqe.NF'
      DisplayFormat = '000000;-000000; '
    end
    object QrPQEData: TDateField
      FieldName = 'Data'
      Origin = 'DBMBWET.pqe.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQEConhecimento: TIntegerField
      FieldName = 'Conhecimento'
      Origin = 'DBMBWET.pqe.Conhecimento'
      DisplayFormat = '000000;-000000; '
    end
    object QrPQENF_RP: TIntegerField
      FieldName = 'NF_RP'
    end
    object QrPQENF_CC: TIntegerField
      FieldName = 'NF_CC'
    end
    object QrPQEIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQECI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQENOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Size = 100
    end
  end
  object DsPQE: TDataSource
    DataSet = QrPQE
    Left = 844
    Top = 453
  end
  object PMImprime: TPopupMenu
    Left = 16
    Top = 352
    object Diversos1: TMenuItem
      Caption = 'Diversos (outra janela)'
      OnClick = Diversos1Click
    end
  end
  object PMNovo: TPopupMenu
    Left = 68
    Top = 52
    object Atualizaestoque1: TMenuItem
      Caption = '&Atualiza estoque (Cliente interno selecionado)'
      OnClick = Atualizaestoque1Click
    end
    object Atualizacustopadro1: TMenuItem
      Caption = 'Atualiza &custo '#250'ltima compra'
      OnClick = Atualizacustopadro1Click
    end
    object Atualizaconsumodirio1: TMenuItem
      Caption = 'Atualiza consumo &di'#225'rio'
      OnClick = Atualizaconsumodirio1Click
    end
    object CorrigeGrupoemmassa1: TMenuItem
      Caption = 'Corrige &Grupo em massa'
      OnClick = CorrigeGrupoemmassa1Click
    end
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 752
    Top = 12
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 752
    Top = 60
  end
  object VUUnidMed: TdmkValUsu
    dmkEditCB = EdUnidMed
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 816
    Top = 12
  end
end
