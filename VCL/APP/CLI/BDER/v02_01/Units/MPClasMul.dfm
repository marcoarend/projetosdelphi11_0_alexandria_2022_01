object FmMPClasMul: TFmMPClasMul
  Left = 339
  Top = 185
  Caption = 'COU-CLASS-002 :: Classifica'#231#227'o M'#250'ltipla'
  ClientHeight = 558
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 402
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 162
      Width = 1008
      Height = 240
      Align = alClient
      DataSource = DsMPInClasMul
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Estq_Pecas'
          Title.Caption = 'Estq. Pe'#231'as'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Estq_Peso'
          Title.Caption = 'Estq. kg'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bxa_Pecas'
          Title.Caption = 'Bxa. Pe'#231'as'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bxa_M2'
          Title.Caption = 'Bxa. m'#178
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bxa_Peso'
          Title.Caption = 'Bxa peso'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data/Hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Clas_Pecas'
          Title.Caption = 'Clas. Pe'#231'as'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Clas_M2'
          Title.Caption = 'Clas. m'#178
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Clas_Peso'
          Title.Caption = 'Clas. Peso'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'StqMovIts'
          Title.Caption = 'ID Estoque'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Clas_P2'
          Title.Caption = 'Clas. ft'#178
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bxa_P2'
          Title.Caption = 'Bxa ft'#178
          Width = 60
          Visible = True
        end>
    end
    object Panel6: TPanel
      Left = 0
      Top = 98
      Width = 1008
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 393
        Height = 64
        Align = alLeft
        Caption = ' Estoque a ser classificado: '
        TabOrder = 0
        object Label4: TLabel
          Left = 8
          Top = 20
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
          FocusControl = DBEdit1
        end
        object Label5: TLabel
          Left = 104
          Top = 20
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
          FocusControl = DBEdit2
        end
        object Label6: TLabel
          Left = 200
          Top = 20
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
          FocusControl = DBEdit3
        end
        object Label7: TLabel
          Left = 296
          Top = 20
          Width = 27
          Height = 13
          Caption = 'Peso:'
          FocusControl = DBEdit4
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 36
          Width = 92
          Height = 21
          DataField = 'Estq_Pecas'
          DataSource = DsSoma
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 104
          Top = 36
          Width = 92
          Height = 21
          DataField = 'Estq_M2'
          DataSource = DsSoma
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 200
          Top = 36
          Width = 92
          Height = 21
          DataField = 'Estq_P2'
          DataSource = DsSoma
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 296
          Top = 36
          Width = 92
          Height = 21
          DataField = 'Estq_Peso'
          DataSource = DsSoma
          TabOrder = 3
        end
      end
      object GroupBox2: TGroupBox
        Left = 393
        Top = 0
        Width = 396
        Height = 64
        Align = alLeft
        Caption = ' Classifica'#231#227'o gerada: '
        TabOrder = 1
        object Label8: TLabel
          Left = 8
          Top = 20
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
          FocusControl = DBEdit1
        end
        object Label9: TLabel
          Left = 104
          Top = 20
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
          FocusControl = DBEdit2
        end
        object Label10: TLabel
          Left = 200
          Top = 20
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
          FocusControl = DBEdit3
        end
        object Label11: TLabel
          Left = 296
          Top = 20
          Width = 27
          Height = 13
          Caption = 'Peso:'
          FocusControl = DBEdit4
        end
        object DBEdit5: TDBEdit
          Left = 8
          Top = 37
          Width = 92
          Height = 21
          DataField = 'Clas_Pecas'
          DataSource = DsSoma
          TabOrder = 0
        end
        object DBEdit6: TDBEdit
          Left = 104
          Top = 37
          Width = 92
          Height = 21
          DataField = 'Clas_M2'
          DataSource = DsSoma
          TabOrder = 1
        end
        object DBEdit7: TDBEdit
          Left = 200
          Top = 37
          Width = 92
          Height = 21
          DataField = 'Clas_P2'
          DataSource = DsSoma
          TabOrder = 2
        end
        object DBEdit8: TDBEdit
          Left = 296
          Top = 37
          Width = 92
          Height = 21
          DataField = 'Clas_Peso'
          DataSource = DsSoma
          TabOrder = 3
        end
      end
      object RGBaixa: TRadioGroup
        Left = 789
        Top = 0
        Width = 92
        Height = 64
        Align = alLeft
        Caption = ' Baixa pe'#231'a por: '
        Enabled = False
        ItemIndex = 0
        Items.Strings = (
          'Estoque'
          'Classe')
        TabOrder = 2
        OnClick = RGBaixaClick
      end
      object RGData: TRadioGroup
        Left = 881
        Top = 0
        Width = 127
        Height = 64
        Align = alClient
        Caption = ' Data: '
        ItemIndex = 2
        Items.Strings = (
          'Agora'
          '2 dias ap'#243's entrada'
          '3 dias ap'#243's entrada')
        TabOrder = 3
        OnClick = RGDataClick
      end
    end
    object Panel7: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 98
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 896
        Height = 98
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object PnMultiGrandeza: TPanel
          Left = 0
          Top = 48
          Width = 896
          Height = 50
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 353
            Height = 50
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object LaPecas: TLabel
              Left = 4
              Top = 4
              Width = 33
              Height = 13
              Caption = 'Pe'#231'as:'
            end
            object LaAreaM2: TLabel
              Left = 92
              Top = 4
              Width = 39
              Height = 13
              Caption = #193'rea m'#178':'
            end
            object LaAreaP2: TLabel
              Left = 180
              Top = 4
              Width = 37
              Height = 13
              Caption = #193'rea ft'#178':'
              Enabled = False
            end
            object LaPeso: TLabel
              Left = 260
              Top = 4
              Width = 27
              Height = 13
              Caption = 'Peso:'
            end
            object EdPecas: TdmkEdit
              Left = 4
              Top = 20
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdPecasChange
            end
            object EdAreaM2: TdmkEditCalc
              Left = 92
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdAreaM2Change
              dmkEditCalcA = EdAreaP2
              CalcType = ctM2toP2
              CalcFrac = cfQuarto
            end
            object EdAreaP2: TdmkEditCalc
              Left = 176
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdAreaP2Change
              dmkEditCalcA = EdAreaM2
              CalcType = ctP2toM2
              CalcFrac = cfCento
            end
            object EdPeso: TdmkEdit
              Left = 260
              Top = 20
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdPesoChange
            end
          end
          object Panel5: TPanel
            Left = 353
            Top = 0
            Width = 96
            Height = 50
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object BtRateia: TBitBtn
              Left = 2
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Rateia'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtRateiaClick
            end
          end
          object RGMovimento: TRadioGroup
            Left = 449
            Top = 0
            Width = 447
            Height = 50
            Align = alClient
            Caption = ' Movimento no estoque: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'Nenhum'
              'Debitar e creditar'
              'Apenas debitar'
              'Apenas creditar')
            TabOrder = 2
            OnClick = RGMovimentoClick
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 896
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 4
            Top = 4
            Width = 90
            Height = 13
            Caption = 'Centro de estoque:'
          end
          object Label1: TLabel
            Left = 352
            Top = 4
            Width = 48
            Height = 13
            Caption = 'Reduzido:'
          end
          object EdStqCenCad: TdmkEditCB
            Left = 4
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdStqCenCadChange
            DBLookupComboBox = CBStqCenCad
            IgnoraDBLookupComboBox = False
          end
          object CBStqCenCad: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 281
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsStqCenCad
            TabOrder = 1
            dmkEditCB = EdStqCenCad
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdGraGrux: TdmkEditCB
            Left = 352
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdGraGruxChange
            OnEnter = EdGraGruxEnter
            OnExit = EdGraGruxExit
            DBLookupComboBox = CBGraGruX
            IgnoraDBLookupComboBox = False
          end
          object CBGraGruX: TdmkDBLookupComboBox
            Left = 412
            Top = 20
            Width = 477
            Height = 21
            KeyField = 'GraGruX'
            ListField = 'Nome'
            ListSource = DsGraGruX
            TabOrder = 3
            dmkEditCB = EdGraGrux
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
      object RGRateio: TRadioGroup
        Left = 896
        Top = 0
        Width = 112
        Height = 98
        Align = alClient
        Caption = ' Grandeza rateio:'
        ItemIndex = 0
        Items.Strings = (
          'Pe'#231'as'
          'Peso')
        TabOrder = 1
        OnClick = RGRateioClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 262
        Height = 32
        Caption = 'Classifica'#231#227'o M'#250'ltipla'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 262
        Height = 32
        Caption = 'Classifica'#231#227'o M'#250'ltipla'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 262
        Height = 32
        Caption = 'Classifica'#231#227'o M'#250'ltipla'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 450
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 494
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel10: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrMPIn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT em.CMPUnida, em.CMP_IDQV, em.CMP_LPQV, em.Contatos,'
      'IF(ci.Tipo=0, ci.RazaoSocial, ci.Nome) NOMECLIENTEI,'
      'IF(fo.Tipo=0, fo.ECidade, fo.PCidade) NOMECIDADEF,'
      'IF(ve.Tipo=0, ve.RazaoSocial, ve.Nome) NOMECORRETOR,'
      'IF(qa.Tipo=0, qa.RazaoSocial, qa.Nome) NOMEASSINA,'
      'IF(fo.Tipo=0, ufE.Nome, ufP.Nome) NOMEUFF,'
      'IF(fo.Tipo=0, fo.ETe1, fo.PTe1) TEL1F,'
      'IF(fo.Tipo=0, fo.ETe2, fo.PTe2) TEL2F,'
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOMEPROCEDENCIA, wi.*'
      'FROM mpin wi'
      'LEFT JOIN entidades ci ON ci.Codigo=wi.ClienteI'
      'LEFT JOIN entidades fo ON fo.Codigo=wi.Procedencia'
      'LEFT JOIN entidades ve ON ve.Codigo=wi.Corretor'
      'LEFT JOIN entidades qa ON qa.Codigo=wi.QuemAssina'
      'LEFT JOIN entimp    em ON em.Codigo=fo.Codigo'
      'LEFT JOIN ufs ufE ON ufE.Codigo=fo.EUF'
      'LEFT JOIN ufs ufP ON ufP.Codigo=fo.PUF'
      'WHERE wi.Data BETWEEN :P0 AND :P1'
      ''
      'ORDER BY Data DESC, Ficha DESC, Controle DESC')
    Left = 12
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMPInCMPUnida: TSmallintField
      FieldName = 'CMPUnida'
    end
    object QrMPInCMP_IDQV: TSmallintField
      FieldName = 'CMP_IDQV'
    end
    object QrMPInCMP_LPQV: TFloatField
      FieldName = 'CMP_LPQV'
    end
    object QrMPInContatos: TWideStringField
      FieldName = 'Contatos'
      Size = 100
    end
    object QrMPInNOMECLIENTEI: TWideStringField
      FieldName = 'NOMECLIENTEI'
      Size = 100
    end
    object QrMPInNOMECIDADEF: TWideStringField
      FieldName = 'NOMECIDADEF'
      Size = 25
    end
    object QrMPInNOMECORRETOR: TWideStringField
      FieldName = 'NOMECORRETOR'
      Size = 100
    end
    object QrMPInNOMEASSINA: TWideStringField
      FieldName = 'NOMEASSINA'
      Size = 100
    end
    object QrMPInNOMEUFF: TWideStringField
      FieldName = 'NOMEUFF'
      Size = 2
    end
    object QrMPInTEL1F: TWideStringField
      FieldName = 'TEL1F'
    end
    object QrMPInTEL2F: TWideStringField
      FieldName = 'TEL2F'
    end
    object QrMPInNOMEPROCEDENCIA: TWideStringField
      FieldName = 'NOMEPROCEDENCIA'
      Size = 100
    end
    object QrMPInCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPInControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMPInFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrMPInData: TDateField
      FieldName = 'Data'
    end
    object QrMPInClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrMPInProcedencia: TIntegerField
      FieldName = 'Procedencia'
    end
    object QrMPInTransportadora: TIntegerField
      FieldName = 'Transportadora'
    end
    object QrMPInMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrMPInPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrMPInPecasNF: TFloatField
      FieldName = 'PecasNF'
    end
    object QrMPInM2: TFloatField
      FieldName = 'M2'
    end
    object QrMPInP2: TFloatField
      FieldName = 'P2'
    end
    object QrMPInFimM2: TFloatField
      FieldName = 'FimM2'
    end
    object QrMPInFimP2: TFloatField
      FieldName = 'FimP2'
    end
    object QrMPInPNF: TFloatField
      FieldName = 'PNF'
    end
    object QrMPInPLE: TFloatField
      FieldName = 'PLE'
    end
    object QrMPInPDA: TFloatField
      FieldName = 'PDA'
    end
    object QrMPInRecorte_PDA: TFloatField
      FieldName = 'Recorte_PDA'
    end
    object QrMPInPTA: TFloatField
      FieldName = 'PTA'
    end
    object QrMPInRecorte_PTA: TFloatField
      FieldName = 'Recorte_PTA'
    end
    object QrMPInRaspa_PTA: TFloatField
      FieldName = 'Raspa_PTA'
    end
    object QrMPInTipificacao: TIntegerField
      FieldName = 'Tipificacao'
    end
    object QrMPInAparasCabelo: TSmallintField
      FieldName = 'AparasCabelo'
    end
    object QrMPInSeboPreDescarne: TSmallintField
      FieldName = 'SeboPreDescarne'
    end
    object QrMPInCustoInfo: TFloatField
      FieldName = 'CustoInfo'
    end
    object QrMPInFreteInfo: TFloatField
      FieldName = 'FreteInfo'
    end
    object QrMPInValor: TFloatField
      FieldName = 'Valor'
    end
    object QrMPInCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrMPInFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrMPInAbateTipo: TIntegerField
      FieldName = 'AbateTipo'
    end
    object QrMPInLote: TWideStringField
      FieldName = 'Lote'
      Size = 11
    end
    object QrMPInPecasOut: TFloatField
      FieldName = 'PecasOut'
    end
    object QrMPInPecasSal: TFloatField
      FieldName = 'PecasSal'
    end
    object QrMPInCaminhoes: TIntegerField
      FieldName = 'Caminhoes'
    end
    object QrMPInQuemAssina: TIntegerField
      FieldName = 'QuemAssina'
    end
    object QrMPInCorretor: TIntegerField
      FieldName = 'Corretor'
    end
    object QrMPInComisPer: TFloatField
      FieldName = 'ComisPer'
    end
    object QrMPInComisVal: TFloatField
      FieldName = 'ComisVal'
    end
    object QrMPInDescAdiant: TFloatField
      FieldName = 'DescAdiant'
    end
    object QrMPInCondPagto: TWideStringField
      FieldName = 'CondPagto'
      Size = 50
    end
    object QrMPInCondComis: TWideStringField
      FieldName = 'CondComis'
      Size = 50
    end
    object QrMPInLocalEntrg: TWideStringField
      FieldName = 'LocalEntrg'
      Size = 50
    end
    object QrMPInObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrMPInAnimal: TSmallintField
      FieldName = 'Animal'
    end
    object QrMPInCMPValor: TFloatField
      FieldName = 'CMPValor'
    end
    object QrMPInCMPFrete: TFloatField
      FieldName = 'CMPFrete'
    end
    object QrMPInQuebrVReal: TFloatField
      FieldName = 'QuebrVReal'
    end
    object QrMPInQuebrVCobr: TFloatField
      FieldName = 'QuebrVCobr'
    end
    object QrMPInCMPDesQV: TFloatField
      FieldName = 'CMPDesQV'
    end
    object QrMPInCMPPagar: TFloatField
      FieldName = 'CMPPagar'
    end
    object QrMPInPcBxa: TFloatField
      FieldName = 'PcBxa'
    end
    object QrMPInkgBxa: TFloatField
      FieldName = 'kgBxa'
    end
    object QrMPInM2Bxa: TFloatField
      FieldName = 'M2Bxa'
    end
    object QrMPInP2Bxa: TFloatField
      FieldName = 'P2Bxa'
    end
    object QrMPInPecasCal: TFloatField
      FieldName = 'PecasCal'
    end
    object QrMPInPecasCur: TFloatField
      FieldName = 'PecasCur'
    end
    object QrMPInPecasExp: TFloatField
      FieldName = 'PecasExp'
    end
    object QrMPInM2Exp: TFloatField
      FieldName = 'M2Exp'
    end
    object QrMPInP2Exp: TFloatField
      FieldName = 'P2Exp'
    end
    object QrMPInPecasNeg: TFloatField
      FieldName = 'PecasNeg'
    end
    object QrMPInPesoCal: TFloatField
      FieldName = 'PesoCal'
    end
    object QrMPInPesoCur: TFloatField
      FieldName = 'PesoCur'
    end
    object QrMPInCusPQCal: TFloatField
      FieldName = 'CusPQCal'
    end
    object QrMPInCusPQCur: TFloatField
      FieldName = 'CusPQCur'
    end
    object QrMPInFuloesCal: TIntegerField
      FieldName = 'FuloesCal'
    end
    object QrMPInFuloesCur: TIntegerField
      FieldName = 'FuloesCur'
    end
    object QrMPInMinDtaCal: TDateField
      FieldName = 'MinDtaCal'
    end
    object QrMPInMinDtaCur: TDateField
      FieldName = 'MinDtaCur'
    end
    object QrMPInMinDtaEnx: TDateField
      FieldName = 'MinDtaEnx'
    end
    object QrMPInMaxDtaCal: TDateField
      FieldName = 'MaxDtaCal'
    end
    object QrMPInMaxDtaCur: TDateField
      FieldName = 'MaxDtaCur'
    end
    object QrMPInMaxDtaEnx: TDateField
      FieldName = 'MaxDtaEnx'
    end
    object QrMPInForcaEncer: TSmallintField
      FieldName = 'ForcaEncer'
    end
    object QrMPInEncerrado: TSmallintField
      FieldName = 'Encerrado'
    end
    object QrMPInPS_ValUni: TFloatField
      FieldName = 'PS_ValUni'
    end
    object QrMPInPS_QtdTot: TFloatField
      FieldName = 'PS_QtdTot'
    end
    object QrMPInPS_ValTot: TFloatField
      FieldName = 'PS_ValTot'
    end
    object QrMPInLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMPInDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMPInDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMPInUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMPInUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMPInAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrMPInAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMPInEmClassif: TSmallintField
      FieldName = 'EmClassif'
    end
    object QrMPInPesoExp: TFloatField
      FieldName = 'PesoExp'
    end
    object QrMPInEstq_Pecas: TFloatField
      FieldName = 'Estq_Pecas'
    end
    object QrMPInEstq_M2: TFloatField
      FieldName = 'Estq_M2'
    end
    object QrMPInEstq_P2: TFloatField
      FieldName = 'Estq_P2'
    end
    object QrMPInEstq_Peso: TFloatField
      FieldName = 'Estq_Peso'
    end
    object QrMPInStqMovIts: TIntegerField
      FieldName = 'StqMovIts'
    end
  end
  object QrMPInClasMul: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mpinclasmul')
    Left = 40
    Top = 12
    object QrMPInClasMulCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mpinclasmul.Codigo'
    end
    object QrMPInClasMulControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'mpinclasmul.Controle'
    end
    object QrMPInClasMulData: TDateField
      FieldName = 'Data'
      Origin = 'mpinclasmul.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPInClasMulClienteI: TIntegerField
      FieldName = 'ClienteI'
      Origin = 'mpinclasmul.ClienteI'
    end
    object QrMPInClasMulProcedencia: TIntegerField
      FieldName = 'Procedencia'
      Origin = 'mpinclasmul.Procedencia'
    end
    object QrMPInClasMulTransportadora: TIntegerField
      FieldName = 'Transportadora'
      Origin = 'mpinclasmul.Transportadora'
    end
    object QrMPInClasMulMarca: TWideStringField
      FieldName = 'Marca'
      Origin = 'mpinclasmul.Marca'
    end
    object QrMPInClasMulLote: TWideStringField
      FieldName = 'Lote'
      Origin = 'mpinclasmul.Lote'
      Size = 11
    end
    object QrMPInClasMulPecasOut: TFloatField
      FieldName = 'PecasOut'
      Origin = 'mpinclasmul.PecasOut'
    end
    object QrMPInClasMulPecasSal: TFloatField
      FieldName = 'PecasSal'
      Origin = 'mpinclasmul.PecasSal'
    end
    object QrMPInClasMulEstq_Pecas: TFloatField
      FieldName = 'Estq_Pecas'
      Origin = 'mpinclasmul.Estq_Pecas'
    end
    object QrMPInClasMulEstq_M2: TFloatField
      FieldName = 'Estq_M2'
      Origin = 'mpinclasmul.Estq_M2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrMPInClasMulEstq_P2: TFloatField
      FieldName = 'Estq_P2'
      Origin = 'mpinclasmul.Estq_P2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrMPInClasMulEstq_Peso: TFloatField
      FieldName = 'Estq_Peso'
      Origin = 'mpinclasmul.Estq_Peso'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrMPInClasMulStqMovIts: TIntegerField
      FieldName = 'StqMovIts'
      Origin = 'mpinclasmul.StqMovIts'
    end
    object QrMPInClasMulClas_Pecas: TFloatField
      FieldName = 'Clas_Pecas'
      Origin = 'mpinclasmul.Clas_Pecas'
    end
    object QrMPInClasMulClas_M2: TFloatField
      FieldName = 'Clas_M2'
      Origin = 'mpinclasmul.Clas_M2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrMPInClasMulClas_P2: TFloatField
      FieldName = 'Clas_P2'
      Origin = 'mpinclasmul.Clas_P2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrMPInClasMulClas_Peso: TFloatField
      FieldName = 'Clas_Peso'
      Origin = 'mpinclasmul.Clas_Peso'
    end
    object QrMPInClasMulDebCtrl: TIntegerField
      FieldName = 'DebCtrl'
      Origin = 'mpinclasmul.DebCtrl'
    end
    object QrMPInClasMulDebStqCenCad: TIntegerField
      FieldName = 'DebStqCenCad'
      Origin = 'mpinclasmul.DebStqCenCad'
    end
    object QrMPInClasMulDebGraGruX: TIntegerField
      FieldName = 'DebGraGruX'
      Origin = 'mpinclasmul.DebGraGruX'
    end
    object QrMPInClasMulPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'mpinclasmul.Pecas'
    end
    object QrMPInClasMulPLE: TFloatField
      FieldName = 'PLE'
      Origin = 'mpinclasmul.PLE'
    end
    object QrMPInClasMulBxa_Pecas: TFloatField
      FieldName = 'Bxa_Pecas'
    end
    object QrMPInClasMulBxa_M2: TFloatField
      FieldName = 'Bxa_M2'
    end
    object QrMPInClasMulBxa_P2: TFloatField
      FieldName = 'Bxa_P2'
    end
    object QrMPInClasMulBxa_Peso: TFloatField
      FieldName = 'Bxa_Peso'
    end
    object QrMPInClasMulGerBxaEstq: TIntegerField
      FieldName = 'GerBxaEstq'
    end
    object QrMPInClasMulClas_Qtde: TFloatField
      FieldName = 'Clas_Qtde'
    end
    object QrMPInClasMulBxa_Qtde: TFloatField
      FieldName = 'Bxa_Qtde'
    end
    object QrMPInClasMulDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
  end
  object DsMPInClasMul: TDataSource
    DataSet = QrMPInClasMul
    Left = 68
    Top = 12
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(Estq_Pecas) Estq_Pecas, '
      'SUM(Estq_M2) Estq_M2, '
      'SUM(Estq_P2) Estq_P2, '
      'SUM(Estq_Peso) Estq_Peso,'
      'SUM(Clas_Pecas) Clas_Pecas, '
      'SUM(Clas_M2) Clas_M2, '
      'SUM(Clas_P2) Clas_P2, '
      'SUM(Clas_Peso) Clas_Peso'
      'FROM mpinclasmul')
    Left = 16
    Top = 192
    object QrSomaEstq_Pecas: TFloatField
      FieldName = 'Estq_Pecas'
    end
    object QrSomaEstq_M2: TFloatField
      FieldName = 'Estq_M2'
    end
    object QrSomaEstq_P2: TFloatField
      FieldName = 'Estq_P2'
    end
    object QrSomaEstq_Peso: TFloatField
      FieldName = 'Estq_Peso'
    end
    object QrSomaClas_Pecas: TFloatField
      FieldName = 'Clas_Pecas'
    end
    object QrSomaClas_M2: TFloatField
      FieldName = 'Clas_M2'
    end
    object QrSomaClas_P2: TFloatField
      FieldName = 'Clas_P2'
    end
    object QrSomaClas_Peso: TFloatField
      FieldName = 'Clas_Peso'
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT ggx.Controle GraGruX,  gg1.Nivel3, gg1.Nivel2, gg1.Nivel1' +
        ', '
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, '
      'HowBxaEstq, GerBxaEstq, gtc.Nome NOMEGRATAMCAD, '
      'gtc.CodUsu CODUSUGRATAMCAD, gg1.CST_A, gg1.CST_B, '
      'gg1.UnidMed, gg1.NCM, gg1.Peso, gg1.FatorClas,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, '
      'unm.Nome NOMEUNIDMED '
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON ggx.GraGru1=gg1.Nivel1'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      '/*WHERE pgt.TipPrd=1 */'
      'ORDER BY gg1.Nome')
    Left = 96
    Top = 12
    object QrGraGruXNivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGruXNivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGruXNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGruXNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGruXPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrGraGruXGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Required = True
    end
    object QrGraGruXNOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGruXCODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGruXCST_A: TSmallintField
      FieldName = 'CST_A'
      Required = True
    end
    object QrGraGruXCST_B: TSmallintField
      FieldName = 'CST_B'
      Required = True
    end
    object QrGraGruXUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
    object QrGraGruXNCM: TWideStringField
      FieldName = 'NCM'
      Required = True
      Size = 10
    end
    object QrGraGruXPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 3
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGruXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXFatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 124
    Top = 12
  end
  object QrStqCenCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 152
    Top = 12
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 180
    Top = 12
  end
  object DsSoma: TDataSource
    DataSet = QrSoma
    Left = 44
    Top = 192
  end
end
