unit WBAjsCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, UnAppEnums;

type
  TFmWBAjsCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrWBAjsCab: TmySQLQuery;
    DsWBAjsCab: TDataSource;
    QrWBAjsIts: TmySQLQuery;
    DsWBAjsIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDataHora: TdmkEditDateTimePicker;
    EdDataHora: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrWBAjsCabCodigo: TIntegerField;
    QrWBAjsCabMovimCod: TIntegerField;
    QrWBAjsCabEmpresa: TIntegerField;
    QrWBAjsCabDataHora: TDateTimeField;
    QrWBAjsCabPecas: TFloatField;
    QrWBAjsCabPesoKg: TFloatField;
    QrWBAjsCabAreaM2: TFloatField;
    QrWBAjsCabAreaP2: TFloatField;
    QrWBAjsCabLk: TIntegerField;
    QrWBAjsCabDataCad: TDateField;
    QrWBAjsCabDataAlt: TDateField;
    QrWBAjsCabUserCad: TIntegerField;
    QrWBAjsCabUserAlt: TIntegerField;
    QrWBAjsCabAlterWeb: TSmallintField;
    QrWBAjsCabAtivo: TSmallintField;
    QrWBAjsCabNO_EMPRESA: TWideStringField;
    QrWBAjsItsCodigo: TIntegerField;
    QrWBAjsItsControle: TIntegerField;
    QrWBAjsItsMovimCod: TIntegerField;
    QrWBAjsItsEmpresa: TIntegerField;
    QrWBAjsItsMovimID: TIntegerField;
    QrWBAjsItsGraGruX: TIntegerField;
    QrWBAjsItsPecas: TFloatField;
    QrWBAjsItsPesoKg: TFloatField;
    QrWBAjsItsAreaM2: TFloatField;
    QrWBAjsItsAreaP2: TFloatField;
    QrWBAjsItsLk: TIntegerField;
    QrWBAjsItsDataCad: TDateField;
    QrWBAjsItsDataAlt: TDateField;
    QrWBAjsItsUserCad: TIntegerField;
    QrWBAjsItsUserAlt: TIntegerField;
    QrWBAjsItsAlterWeb: TSmallintField;
    QrWBAjsItsAtivo: TSmallintField;
    QrWBAjsItsNO_PRD_TAM_COR: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TDBGrid;
    QrWBAjsItsSrcMovID: TIntegerField;
    QrWBAjsItsSrcNivel1: TIntegerField;
    QrWBAjsItsSrcNivel2: TIntegerField;
    QrWBAjsItsPallet: TIntegerField;
    QrWBAjsItsNO_PALLET: TWideStringField;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    Classificao1: TMenuItem;
    frxWET_RECUR_010_01: TfrxReport;
    frxDsWBAjsIts: TfrxDBDataset;
    frxDsWBAjsCab: TfrxDBDataset;
    QrWBAjsItsObserv: TWideStringField;
    N2: TMenuItem;
    Fichas1: TMenuItem;
    QrPalletCla: TmySQLQuery;
    QrPalletClaPallet: TIntegerField;
    QrPalletClaDataHora: TDateTimeField;
    QrPalletClaPecas: TFloatField;
    QrPalletClaAreaM2: TFloatField;
    QrPalletClaAreaP2: TFloatField;
    QrPalletClaPesoKg: TFloatField;
    QrPalletClaNO_PRD_TAM_COR: TWideStringField;
    QrPalletClaNO_Pallet: TWideStringField;
    QrPalletClaNO_EMPRESA: TWideStringField;
    QrPalletClaNO_FORNECE: TWideStringField;
    frxWET_RECUR_010_03: TfrxReport;
    frxDsPalletCla: TfrxDBDataset;
    QrWBAjsItsNO_TERCEIRO: TWideStringField;
    QrWBAjsItsMovimNiv: TIntegerField;
    QrWBAjsItsTerceiro: TIntegerField;
    QrWBAjsItsDataHora: TDateTimeField;
    QrWBAjsItsValorT: TFloatField;
    QrWBAjsItsSdoVrtPeca: TFloatField;
    QrWBAjsItsSdoVrtArM2: TFloatField;
    QrWBAjsItsLnkNivXtr1: TIntegerField;
    QrWBAjsItsLnkNivXtr2: TIntegerField;
    QrWBAjsItsCliVenda: TIntegerField;
    QrWBAjsItsMovimTwn: TIntegerField;
    QrWBAjsItsSrcGGX: TIntegerField;
    QrWBAjsItsSdoVrtPeso: TFloatField;
    QrWBAjsItsSerieFch: TIntegerField;
    QrWBAjsItsFicha: TIntegerField;
    QrWBAjsItsMisturou: TSmallintField;
    QrWBAjsItsFornecMO: TIntegerField;
    QrWBAjsItsCustoMOKg: TFloatField;
    QrWBAjsItsCustoMOTot: TFloatField;
    QrWBAjsItsValorMP: TFloatField;
    QrWBAjsItsDstMovID: TIntegerField;
    QrWBAjsItsDstNivel1: TIntegerField;
    QrWBAjsItsDstNivel2: TIntegerField;
    QrWBAjsItsDstGGX: TIntegerField;
    QrWBAjsItsQtdGerPeca: TFloatField;
    QrWBAjsItsQtdGerPeso: TFloatField;
    QrWBAjsItsQtdGerArM2: TFloatField;
    QrWBAjsItsQtdGerArP2: TFloatField;
    QrWBAjsItsQtdAntPeca: TFloatField;
    QrWBAjsItsQtdAntPeso: TFloatField;
    QrWBAjsItsQtdAntArM2: TFloatField;
    QrWBAjsItsQtdAntArP2: TFloatField;
    QrWBAjsItsAptoUso: TSmallintField;
    QrWBAjsItsNotaMPAG: TFloatField;
    QrWBAjsItsMarca: TWideStringField;
    QrWBAjsItsTpCalcAuto: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWBAjsCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWBAjsCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrWBAjsCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrWBAjsCabBeforeClose(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure Classificao1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxWET_RECUR_010_01GetValue(const VarName: string;
      var Value: Variant);
    procedure Entradascomestoquevirtual1Click(Sender: TObject);
    procedure Fichas1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormWBAjsIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenWBAjsIts(Controle: Integer);

  end;

var
  FmWBAjsCab: TFmWBAjsCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, WBAjsIts, ModuleGeral,
Principal, WBMovImp;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWBAjsCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWBAjsCab.MostraFormWBAjsIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmWBAjsIts, FmWBAjsIts, afmoNegarComAviso) then
  begin
    FmWBAjsIts.ImgTipo.SQLType := SQLType;
    FmWBAjsIts.FQrCab := QrWBAjsCab;
    FmWBAjsIts.FDsCab := DsWBAjsCab;
    FmWBAjsIts.FQrIts := QrWBAjsIts;
    FmWBAjsIts.FDataHora := QrWBAjsCabDataHora.Value;
    FmWBAjsIts.FEmpresa  := QrWBAjsCabEmpresa.Value;
    if SQLType = stIns then
    begin
      //FmWBAjsIts.EdCPF1.ReadOnly := False
    end else
    begin
      FmWBAjsIts.EdControle.ValueVariant := QrWBAjsItsControle.Value;
      //
      FmWBAjsIts.EdFornecedor.ValueVariant := QrWBAjsItsTerceiro.Value;
      FmWBAjsIts.CBFornecedor.KeyValue     := QrWBAjsItsTerceiro.Value;
      FmWBAjsIts.EdGragruX.ValueVariant    := QrWBAjsItsGraGruX.Value;
      FmWBAjsIts.CBGragruX.KeyValue        := QrWBAjsItsGraGruX.Value;
      FmWBAjsIts.EdPecas.ValueVariant      := QrWBAjsItsPecas.Value;
      FmWBAjsIts.EdPesoKg.ValueVariant     := QrWBAjsItsPesoKg.Value;
      FmWBAjsIts.EdAreaM2.ValueVariant     := QrWBAjsItsAreaM2.Value;
      FmWBAjsIts.EdAreaP2.ValueVariant     := QrWBAjsItsAreaP2.Value;
      FmWBAjsIts.EdValorT.ValueVariant     := QrWBAjsItsValorT.Value;
      FmWBAjsIts.EdObserv.ValueVariant     := QrWBAjsItsObserv.Value;
      // Deve ser os �ltimos
      FmWBAjsIts.EdPallet.ValueVariant     := QrWBAjsItsPallet.Value;
      FmWBAjsIts.CBPallet.KeyValue         := QrWBAjsItsPallet.Value;
      //
    end;
    FmWBAjsIts.ShowModal;
    FmWBAjsIts.Destroy;
  end;
end;

procedure TFmWBAjsCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrWBAjsCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrWBAjsCab, QrWBAjsIts);
end;

procedure TFmWBAjsCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrWBAjsCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrWBAjsIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrWBAjsIts);
end;

procedure TFmWBAjsCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWBAjsCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWBAjsCab.DefParams;
begin
  VAR_GOTOTABELA := 'wbajscab';
  VAR_GOTOMYSQLTABLE := QrWBAjsCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA ');
  VAR_SQLx.Add('FROM wbajscab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmWBAjsCab.Entradascomestoquevirtual1Click(Sender: TObject);
begin
{

object Entradascomestoquevirtual1: TMenuItem
  Caption = '&Entradas com estoque virtual'
  OnClick = Entradascomestoquevirtual1Click
end

object frxWET_RECUR_001_02: TfrxReport
  Version = '4.14'
  DotMatrixReport = False
  IniFile = '\Software\Fast Reports'
  PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
  PreviewOptions.Zoom = 1.000000000000000000
  PrintOptions.Printer = 'Default'
  PrintOptions.PrintOnSheet = 0
  ReportOptions.CreateDate = 41608.425381412000000000
  ReportOptions.LastChange = 41608.425381412000000000
  ScriptLanguage = 'PascalScript'
  ScriptText.Strings = (
    'var'
    '  Imprime: Boolean;'
    '    '
    'procedure Line3OnBeforePrint(Sender: TfrxComponent);'
    'begin'
    '  Line3.Visible := Imprime;                         '

      '  Imprime := not Imprime;                                       ' +
      '                                                                ' +
      '                  '
    'end;'
    ''
    'begin'
    '  Imprime := True;                                  '
    'end.')
  OnGetValue = frxWET_RECUR_001_01GetValue
  Left = 424
  Top = 312
  Datasets = <
    item
      DataSet = frxDsPalletAjs
      DataSetName = 'frxDsPalletAjs'
    end>
  Variables = <>
  Style = <>
  object Data: TfrxDataPage
    Height = 1000.000000000000000000
    Width = 1000.000000000000000000
  end
  object Page1: TfrxReportPage
    PaperWidth = 210.000000000000000000
    PaperHeight = 297.000000000000000000
    PaperSize = 9
    LeftMargin = 20.000000000000000000
    RightMargin = 10.000000000000000000
    PrintOnPreviousPage = True
    object MasterData1: TfrxMasterData
      Height = 559.370078740157500000
      Top = 18.897650000000000000
      Width = 680.315400000000000000
      DataSet = frxDsPalletAjs
      DataSetName = 'frxDsPalletAjs'
      RowCount = 0
      object Shape1: TfrxShapeView
        Top = 37.897650000000000000
        Width = 680.315400000000000000
        Height = 483.779840000000000000
        ShowHint = False
        Shape = skRoundRectangle
      end
      object Memo47: TfrxMemoView
        Left = 200.315090000000000000
        Top = 90.811070000000000000
        Width = 283.464750000000000000
        Height = 18.897650000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Mat'#233'ria-Prima para Semi / Acabado')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo48: TfrxMemoView
        Left = 18.897650000000000000
        Top = 90.811070000000000000
        Width = 181.417376540000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[VARF_DATA]')
        ParentFont = False
        VAlign = vaCenter
      end
      object MePagina: TfrxMemoView
        Left = 483.779840000000000000
        Top = 90.811070000000000000
        Width = 177.637846540000000000
        Height = 18.897650000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Item [Line#]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo1: TfrxMemoView
        Left = 20.015770000000000000
        Top = 128.795300000000000000
        Width = 411.968330630000000000
        Height = 38.000000000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -32
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          'PALLET  [frxDsPalletInn."Pallet"]')
        ParentFont = False
      end
      object Line1: TfrxLineView
        Left = 1.118120000000000000
        Top = 176.574830000000000000
        Width = 679.086580000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Line5: TfrxLineView
        Left = 1.118120000000000000
        Top = 173.015770000000000000
        Width = 679.086580000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Memo2: TfrxMemoView
        Top = 268.448828660000000000
        Width = 188.976377950000000000
        Height = 37.795275590000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          'Data')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo13: TfrxMemoView
        Left = 188.976377950000000000
        Top = 268.448828660000000000
        Width = 491.338582680000000000
        Height = 37.795275590000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          'Proced'#234'ncia')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo3: TfrxMemoView
        Top = 226.574830000000000000
        Width = 120.944881890000000000
        Height = 32.000000000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          'Mat'#233'ria-prima:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo29: TfrxMemoView
        Left = 340.157480310000000000
        Top = 351.598444720000000000
        Width = 170.078740160000000000
        Height = 37.795275590000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          #193'rea ft'#178)
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo4: TfrxMemoView
        Top = 351.598444720000000000
        Width = 170.078740160000000000
        Height = 37.795275590000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          'Pe'#231'as')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo6: TfrxMemoView
        Left = 510.236220470000000000
        Top = 351.598444720000000000
        Width = 170.078740160000000000
        Height = 37.795275590000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          'Peso kg')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo7: TfrxMemoView
        Left = 170.078740160000000000
        Top = 351.598444720000000000
        Width = 170.078740160000000000
        Height = 37.795275590000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          #193'rea m'#178)
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo8: TfrxMemoView
        Top = 306.244106690000000000
        Width = 188.976377950000000000
        Height = 37.795275590000000000
        ShowHint = False
        DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsPalletInn."DataHora"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo9: TfrxMemoView
        Left = 188.976377950000000000
        Top = 306.244106690000000000
        Width = 491.338582680000000000
        Height = 37.795275590000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsPalletInn."NO_FORNECE"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo22: TfrxMemoView
        Left = 120.944881890000000000
        Top = 226.574830000000000000
        Width = 559.370078740000100000
        Height = 32.000000000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '[frxDsPalletInn."NO_PRD_TAM_COR"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo24: TfrxMemoView
        Left = 340.157480310000000000
        Top = 389.393722760000000000
        Width = 170.078740160000000000
        Height = 37.795275590000000000
        ShowHint = False
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsPalletInn."AreaP2"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo26: TfrxMemoView
        Top = 389.393722760000000000
        Width = 170.078740160000000000
        Height = 37.795275590000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (

            '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsPalletInn."' +
            'Pecas">)]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo27: TfrxMemoView
        Left = 510.236220470000000000
        Top = 389.393722760000000000
        Width = 170.078740160000000000
        Height = 37.795275590000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (

            '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsPalletInn."' +
            'PesoKg">)]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo28: TfrxMemoView
        Left = 170.078740160000000000
        Top = 389.393722760000000000
        Width = 170.078740160000000000
        Height = 37.795275590000000000
        ShowHint = False
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsPalletInn."AreaM2"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo30: TfrxMemoView
        Left = 120.944881890000000000
        Top = 180.795300000000000000
        Width = 559.370078740000100000
        Height = 40.000000000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '[frxDsPalletInn."NO_EMPRESA"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo31: TfrxMemoView
        Top = 180.795300000000000000
        Width = 120.944881890000000000
        Height = 40.000000000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          'Cliente Interno: ')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo46: TfrxMemoView
        Top = 56.795300000000000000
        Width = 680.315400000000000000
        Height = 18.897650000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[VARF_CODI_FRX]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo5: TfrxMemoView
        Left = 226.771800000000000000
        Top = 128.606370000000000000
        Width = 434.645510630000000000
        Height = 38.000000000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -32
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsPalletInn."NO_Pallet"]')
        ParentFont = False
      end
      object Line3: TfrxLineView
        Top = 559.370103150000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'Line3OnBeforePrint'
        ShowHint = False
        Frame.Style = fsDot
        Frame.Typ = [ftTop]
      end
      object Memo10: TfrxMemoView
        Left = 37.795300000000000000
        Top = 427.086890000000000000
        Width = 604.724800000000000000
        Height = 94.488250000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsPalletInn."Observ"]')
        ParentFont = False
        VAlign = vaCenter
      end
    end
  end
end
object QrPalletAjs: TmySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT wmi.*, CONCAT(gg1.Nome, '
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), '
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
    'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, '
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE'
    'FROM wbmovits wmi '
    'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
    'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet '
    'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa'
    'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro'
    'WHERE wmi.MovimCod=38'
    'AND SdoVrtPeca>=0.001'
    'ORDER BY wmi.Controle')
  Left = 424
  Top = 356
  object QrPalletAjsCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrPalletAjsControle: TIntegerField
    FieldName = 'Controle'
  end
  object QrPalletAjsMovimCod: TIntegerField
    FieldName = 'MovimCod'
  end
  object QrPalletAjsMovimNiv: TIntegerField
    FieldName = 'MovimNiv'
  end
  object QrPalletAjsEmpresa: TIntegerField
    FieldName = 'Empresa'
  end
  object QrPalletAjsTerceiro: TIntegerField
    FieldName = 'Terceiro'
  end
  object QrPalletAjsMovimID: TIntegerField
    FieldName = 'MovimID'
  end
  object QrPalletAjsDataHora: TDateTimeField
    FieldName = 'DataHora'
  end
  object QrPalletAjsPallet: TIntegerField
    FieldName = 'Pallet'
    Required = True
  end
  object QrPalletAjsGraGruX: TIntegerField
    FieldName = 'GraGruX'
  end
  object QrPalletAjsPecas: TFloatField
    FieldName = 'Pecas'
  end
  object QrPalletAjsPesoKg: TFloatField
    FieldName = 'PesoKg'
  end
  object QrPalletAjsAreaM2: TFloatField
    FieldName = 'AreaM2'
  end
  object QrPalletAjsAreaP2: TFloatField
    FieldName = 'AreaP2'
  end
  object QrPalletAjsSrcMovID: TIntegerField
    FieldName = 'SrcMovID'
  end
  object QrPalletAjsSrcNivel1: TIntegerField
    FieldName = 'SrcNivel1'
  end
  object QrPalletAjsSrcNivel2: TIntegerField
    FieldName = 'SrcNivel2'
  end
  object QrPalletAjsLk: TIntegerField
    FieldName = 'Lk'
  end
  object QrPalletAjsDataCad: TDateField
    FieldName = 'DataCad'
  end
  object QrPalletAjsDataAlt: TDateField
    FieldName = 'DataAlt'
  end
  object QrPalletAjsUserCad: TIntegerField
    FieldName = 'UserCad'
  end
  object QrPalletAjsUserAlt: TIntegerField
    FieldName = 'UserAlt'
  end
  object QrPalletAjsAlterWeb: TSmallintField
    FieldName = 'AlterWeb'
  end
  object QrPalletAjsAtivo: TSmallintField
    FieldName = 'Ativo'
  end
  object QrPalletAjsSdoVrtPeca: TFloatField
    FieldName = 'SdoVrtPeca'
  end
  object QrPalletAjsObserv: TWideStringField
    FieldName = 'Observ'
    Required = True
    Size = 255
  end
  object QrPalletAjsNO_PRD_TAM_COR: TWideStringField
    FieldName = 'NO_PRD_TAM_COR'
    Size = 157
  end
  object QrPalletAjsNO_Pallet: TWideStringField
    FieldName = 'NO_Pallet'
    Size = 60
  end
  object QrPalletAjsNO_EMPRESA: TWideStringField
    FieldName = 'NO_EMPRESA'
    Size = 100
  end
  object QrPalletAjsNO_FORNECE: TWideStringField
    FieldName = 'NO_FORNECE'
    Size = 100
  end
end
object frxDsPalletAjs: TfrxDBDataset
  UserName = 'frxDsPalletAjs'
  CloseDataSource = False
  FieldAliases.Strings = (
    'Codigo=Codigo'
    'Controle=Controle'
    'MovimCod=MovimCod'
    'MovimNiv=MovimNiv'
    'Empresa=Empresa'
    'Terceiro=Terceiro'
    'MovimID=MovimID'
    'DataHora=DataHora'
    'Pallet=Pallet'
    'GraGruX=GraGruX'
    'Pecas=Pecas'
    'PesoKg=PesoKg'
    'AreaM2=AreaM2'
    'AreaP2=AreaP2'
    'SrcMovID=SrcMovID'
    'SrcNivel1=SrcNivel1'
    'SrcNivel2=SrcNivel2'
    'Lk=Lk'
    'DataCad=DataCad'
    'DataAlt=DataAlt'
    'UserCad=UserCad'
    'UserAlt=UserAlt'
    'AlterWeb=AlterWeb'
    'Ativo=Ativo'
    'SdoVrtPeca=SdoVrtPeca'
    'Observ=Observ'
    'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
    'NO_Pallet=NO_Pallet'
    'NO_EMPRESA=NO_EMPRESA'
    'NO_FORNECE=NO_FORNECE')
  DataSet = QrPalletAjs
  BCDToCurrency = False
  Left = 424
  Top = 400
end


////////////////////////////////////////////////////////////////////////////////

  UnDmkDAC_PF.AbreMySQLQuery0(QrPalletInn, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa',
  'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrWBAjsCabMovimCod.Value),
  'AND SdoVrtPeca>=0.001',
  'ORDER BY wmi.Controle ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxWET_RECUR_001_02, [
    //DModG.frxDsDono,
    frxDsPalletInn
  ]);
  //
  MyObjects.frxMostra(frxWET_RECUR_001_02,
    'Pallets - Entrada com estoque virtual');
}
end;

procedure TFmWBAjsCab.Estoque1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmWBMovImp, FmWBMovImp, afmoNegarComAviso) then
  begin
    FmWBMovImp.ShowModal;
    FmWBMovImp.Destroy;
  end;
end;

procedure TFmWBAjsCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormWBAjsIts(stUpd);
end;

procedure TFmWBAjsCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmWBAjsCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWBAjsCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWBAjsCab.ItsExclui1Click(Sender: TObject);
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrWBAjsItsCodigo.Value;
  MovimCod := QrWBAjsItsMovimCod.Value;
  //
  if Dmod.ExcluiControleWBMovIts(QrWBAjsIts, QrWBAjsItsMovimCod,
  QrWBAjsItsControle.Value, QrWBAjsItsSrcNivel2.Value) then
  begin
    Dmod.AtualizaTotaisWBXxxCab('wbajscab', MovimCod);
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmWBAjsCab.ReopenWBAjsIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBAjsIts, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_TERCEIRO ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrWBAjsCabMovimCod.Value),
  'ORDER BY wmi.Controle ',
  '']);
  //
  QrWBAjsIts.Locate('Controle', Controle, []);
end;


procedure TFmWBAjsCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWBAjsCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWBAjsCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWBAjsCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWBAjsCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWBAjsCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWBAjsCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWBAjsCabCodigo.Value;
  Close;
end;

procedure TFmWBAjsCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormWBAjsIts(stIns);
end;

procedure TFmWBAjsCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWBAjsCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'wbajscab');
  Empresa := DModG.ObtemFilialDeEntidade(QrWBAjsCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmWBAjsCab.BtConfirmaClick(Sender: TObject);
var
  DataHora: String;
  Codigo, MovimCod, Empresa, Terceiro: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataChegadaInvalida: Boolean;
begin
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DataHora       := Geral.FDT(TPDataHora.Date, 1) + ' ' +
                    Geral.FDT(EdDataHora.ValueVariant, 100);
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(TPDataHora.DateTime < 2, TPDataHora, 'Defina uma data de compra!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('wbajscab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('wbmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'wbajscab', False, [
  'MovimCod', 'Empresa', 'DataHora'
  (*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT',*)], [
  'Codigo'], [
  MovimCod, Empresa, DataHora
  (*, Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*)], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
      Dmod.InsereWBMovCab(MovimCod, emidCompra, Codigo)
    else
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'wbmovits', False, [
      'Empresa', 'DataHora'], ['MovimCod'], [
      Empresa, DataHora], [MovimCod], True);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmWBAjsCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'wbajscab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'wbajscab', 'Codigo');
end;

procedure TFmWBAjsCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmWBAjsCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmWBAjsCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmWBAjsCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWBAjsCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWBAjsCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmWBAjsCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWBAjsCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrWBAjsCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWBAjsCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWBAjsCab.QrWBAjsCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWBAjsCab.QrWBAjsCabAfterScroll(DataSet: TDataSet);
begin
  ReopenWBAjsIts(0);
end;

procedure TFmWBAjsCab.Fichas1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Pallets: String;
begin
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT Pallet ',
    'FROM wbmovits wmi ',
    'WHERE wmi.MovimCod=' + Geral.FF0(QrWBAjsItsMovimCod.Value),
    'AND wmi.Codigo=' + Geral.FF0(QrWBAjsItsCodigo.Value),
    'ORDER BY wmi.Controle ',
    '']);
    Pallets := '';
    Qry.First;
    while not Qry.Eof do
    begin
      Pallets := Pallets + ', ' +
        Geral.FF0(Qry.FieldByName('Pallet').AsInteger);
      //
      Qry.next;
    end;
    Pallets := Copy(Pallets, 3);
    //
    if Pallets <> '' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPalletCla, Dmod.MyDB, [
      'SELECT wmi.Pallet, MAX(DataHora) DataHora,  ',
      'SUM(wmi.Pecas) Pecas,  ',
      'IF(MIN(AreaM2) > 0.01, 0, SUM(wmi.AreaM2)) AreaM2,  ',
      'IF(MIN(AreaP2) > 0.01, 0, SUM(wmi.AreaP2)) AreaP2,  ',
      'IF(MIN(PesoKg) > 0.001, 0, SUM(wmi.PesoKg)) PesoKg,  ',
      'CONCAT(gg1.Nome,   ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,   ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,  ',
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE  ',
      'FROM wbmovits wmi   ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX   ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
      'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet   ',
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa ',
      'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro  ',
      'WHERE wmi.Pallet IN (' + Pallets + ')  ',
      'GROUP BY wmi.Pallet, wmi.GraGruX, wmi.Empresa, wmi.Terceiro  ',
      '']);
      //
      MyObjects.frxDefineDataSets(frxWET_RECUR_010_03, [
        //DModG.frxDsDono,
        frxDsPalletCla
      ]);
      //
      MyObjects.frxMostra(frxWET_RECUR_010_03,
        'Pallets - (Re)Classificados');
    end else
      Geral.MB_Aviso('Nenhum pallet foi encontrado!');
  finally
    Qry.Free;
  end;
end;

procedure TFmWBAjsCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrWBAjsCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmWBAjsCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWBAjsCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wbajscab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmWBAjsCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWBAjsCab.frxWET_RECUR_010_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmWBAjsCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWBAjsCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'wbajscab');
  //
  Agora := DModG.ObtemAgora();
  TPDataHora.Date := Agora;
  EdDataHora.ValueVariant := Agora;
end;

procedure TFmWBAjsCab.Classificao1Click(Sender: TObject);
{
var
  Qry: TmySQLQuery;
  Niveis2: String;
}
begin
{
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT SrcNivel2 ',
    'FROM wbmovits wmi',
    'WHERE wmi.SrcMovID=' + Geral.FF0(QrWBAjsItsMovimCod.Value),
    'AND wmi.SrcNivel1=' + Geral.FF0(QrWBAjsItsCodigo.Value),
    //'AND wmi.SrcNivel2=' + Geral.FF0(QrWBAjsItsControle.Value),
    //'AND wmi.MovimNiv=' + Geral.FF0(Integer(eminSemNiv)),
    '']);
    Niveis2 := '';
    Qry.First;
    while not Qry.Eof do
    begin
      Niveis2 := Niveis2 + ', ' +
        Geral.FF0(Qry.FieldByName('SrcNivel2').AsInteger);
      //
      Qry.next;
    end;
    Niveis2 := Copy(Niveis2, 3);
    //
    if Niveis2 <> '' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrWBAjsSum, Dmod.MyDB, [
      'SELECT SUM(wmi.Pecas) Pecas, SUM(wmi.AreaM2) AreaM2 ',
      'FROM wbmovits wmi',
      'WHERE wmi.Controle IN (' + Niveis2 + ') ',
      '']);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrResulTota, Dmod.MyDB, [
      'SELECT "TOTAL" NO_Pallet, SUM(wmi.Pecas) Pecas, ',
      'SUM(wmi.AreaM2) AreaM2, ',
      'IF(gg1.Referencia <> "", gg1.Referencia, CONCAT("#", ',
      'ggx.Controle)) GGX_REFER, wmi.GraGruX',
      'FROM wbmovits wmi ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
      'WHERE wmi.SrcMovID=' + Geral.FF0(QrWBAjsItsMovimCod.Value),
      'AND wmi.SrcNivel1=' + Geral.FF0(QrWBAjsItsCodigo.Value),
      //'AND wmi.SrcNivel2=' + Geral.FF0(QrWBAjsItsControle.Value),
      //'AND MovimNiv=' + Geral.FF0(Integer(eminSemNiv)),
      'GROUP BY GraGruX',
      '']);
      //
      // SQL copiado do QrReclasif
      UnDmkDAC_PF.AbreMySQLQuery0(QrResulClas, Dmod.MyDB, [
      'SELECT IF(gg1.Referencia <> "", gg1.Referencia, CONCAT("#", ',
      'ggx.Controle)) GGX_REFER, ',
      'wmi.*, CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
      'FROM wbmovits wmi ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
      'WHERE wmi.SrcMovID=' + Geral.FF0(QrWBAjsItsMovimCod.Value),
      'AND wmi.SrcNivel1=' + Geral.FF0(QrWBAjsItsCodigo.Value),
      //'AND wmi.SrcNivel2=' + Geral.FF0(QrWBAjsItsControle.Value),
      //'AND MovimNiv=' + Geral.FF0(Integer(eminSemNiv)),
      'ORDER BY wmi.Controle ',
      '']);
      //
      MyObjects.frxDefineDataSets(frxWET_RECUR_010_01, [
        DModG.frxDsDono,
        frxDsWBAjsCab,
        frxDsWBAjsIts
      ]);
      //
    end else
      Geral.MB_Aviso('Nenhum item foi encontrado!');
  finally
    Qry.Free;
  end;
}
  MyObjects.frxMostra(frxWET_RECUR_010_01, 'Ajuste estoque Couros');
end;

procedure TFmWBAjsCab.QrWBAjsCabBeforeClose(
  DataSet: TDataSet);
begin
  QrWBAjsIts.Close;
end;

procedure TFmWBAjsCab.QrWBAjsCabBeforeOpen(DataSet: TDataSet);
begin
  QrWBAjsCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

