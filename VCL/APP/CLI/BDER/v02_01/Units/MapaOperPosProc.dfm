object FmMapaOperPosProc: TFmMapaOperPosProc
  Left = 339
  Top = 185
  Caption = 'QUI-RECEI-025 :: Mapa Operacional P'#243's Processo'
  ClientHeight = 631
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 475
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 89
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label5: TLabel
        Left = 12
        Top = 4
        Width = 28
        Height = 13
        Caption = 'Setor:'
      end
      object EdSetor: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdSetorRedefinido
        DBLookupComboBox = CBSetor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSetor: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 217
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSetores
        TabOrder = 1
        dmkEditCB = EdSetor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CGOperPosStatus: TdmkCheckGroup
        Left = 292
        Top = 0
        Width = 701
        Height = 77
        Caption = ' Status das fulonadas: '
        TabOrder = 2
        OnClick = CGOperPosStatusClick
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object BtTodosTipCad: TBitBtn
        Tag = 127
        Left = 11
        Top = 45
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Todos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtTodosTipCadClick
      end
      object BtNenhumTipCad: TBitBtn
        Tag = 128
        Left = 103
        Top = 45
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Nenhum'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtNenhumTipCadClick
      end
      object BitBtn1: TBitBtn
        Tag = 100606
        Left = 195
        Top = 45
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Aptos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BitBtn1Click
      end
    end
    object DBGPesq: TDBGrid
      Left = 0
      Top = 89
      Width = 823
      Height = 266
      Align = alClient
      DataSource = DsPesq
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGPesqDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Pesagem'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VSMovCod'
          Title.Caption = 'Processo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtaBaixa'
          Title.Caption = 'Data inicio processo'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'HoraIni'
          Title.Caption = 'h Ini. proc'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtHrFinal'
          Title.Caption = 'Prev. t'#233'rm. processo'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Numero'
          Title.Caption = 'Receita'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME'
          Title.Caption = 'Nome receita'
          Width = 228
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESETOR'
          Title.Caption = 'Setor'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fulao'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DefPeca'
          Title.Caption = 'Pe'#231'a'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_OperPosStatus'
          Title.Caption = 'Status da fulonada'
          Width = 96
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OperPosDthIni'
          Title.Caption = 'In'#237'cio da Opera'#231#227'oi'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OperPosDthFim'
          Title.Caption = 'Fim da opera'#231#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OperPosQtdDon'
          Title.Caption = 'Aprontados'
          Width = 80
          Visible = True
        end>
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 355
      Width = 1008
      Height = 120
      Align = alBottom
      DataSource = DsEmitMopp
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object Memo1: TMemo
      Left = 823
      Top = 89
      Width = 185
      Height = 266
      Align = alRight
      TabOrder = 3
      Visible = False
      WordWrap = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 397
        Height = 32
        Caption = 'Mapa Operacional P'#243's Processo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 397
        Height = 32
        Caption = 'Mapa Operacional P'#243's Processo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 397
        Height = 32
        Caption = 'Mapa Operacional P'#243's Processo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 523
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 567
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 484
        Top = 20
        Width = 110
        Height = 13
        Caption = 'Receitas encontradas: '
      end
      object LaQtde: TLabel
        Left = 596
        Top = 20
        Width = 8
        Height = 13
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtPesquisa: TBitBtn
        Tag = 40
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtPesquisaClick
      end
      object BtLocaliza: TBitBtn
        Tag = 94
        Left = 352
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Localiza'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtLocalizaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 656
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Imprime'
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtImprimeClick
      end
      object BtEmitMopp: TBitBtn
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Opera'#231#227'o'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtEmitMoppClick
      end
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    AfterScroll = QrPesqAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM emit'
      'WHERE Codigo > 0'
      'AND DataEmis >= "2008-05-13" AND DataEmis < "2008-05-15" '
      'AND Numero = 4')
    Left = 28
    Top = 148
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqDataEmis: TDateTimeField
      FieldName = 'DataEmis'
    end
    object QrPesqStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrPesqNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrPesqNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrPesqNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrPesqTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrPesqNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrPesqClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrPesqTipificacao: TSmallintField
      FieldName = 'Tipificacao'
    end
    object QrPesqTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrPesqTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrPesqSetor: TSmallintField
      FieldName = 'Setor'
    end
    object QrPesqTipific: TSmallintField
      FieldName = 'Tipific'
    end
    object QrPesqEspessura: TWideStringField
      FieldName = 'Espessura'
      Size = 15
    end
    object QrPesqDefPeca: TWideStringField
      FieldName = 'DefPeca'
      Size = 15
    end
    object QrPesqPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0'
    end
    object QrPesqCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrPesqQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0'
    end
    object QrPesqAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrPesqFulao: TWideStringField
      FieldName = 'Fulao'
      Size = 5
    end
    object QrPesqObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
    object QrPesqLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPesqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPesqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPesqUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPesqUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPesqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPesqAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPesqSetrEmi: TSmallintField
      FieldName = 'SetrEmi'
      Required = True
    end
    object QrPesqSourcMP: TSmallintField
      FieldName = 'SourcMP'
      Required = True
    end
    object QrPesqCod_Espess: TIntegerField
      FieldName = 'Cod_Espess'
      Required = True
    end
    object QrPesqCodDefPeca: TIntegerField
      FieldName = 'CodDefPeca'
      Required = True
    end
    object QrPesqCustoTo: TFloatField
      FieldName = 'CustoTo'
      Required = True
    end
    object QrPesqCustoKg: TFloatField
      FieldName = 'CustoKg'
      Required = True
    end
    object QrPesqCustoM2: TFloatField
      FieldName = 'CustoM2'
      Required = True
    end
    object QrPesqCusUSM2: TFloatField
      FieldName = 'CusUSM2'
      Required = True
    end
    object QrPesqEmitGru: TIntegerField
      FieldName = 'EmitGru'
      Required = True
    end
    object QrPesqRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
      Required = True
    end
    object QrPesqSemiCodPeca: TIntegerField
      FieldName = 'SemiCodPeca'
      Required = True
    end
    object QrPesqSemiTxtPeca: TWideStringField
      FieldName = 'SemiTxtPeca'
      Size = 15
    end
    object QrPesqSemiPeso: TFloatField
      FieldName = 'SemiPeso'
      Required = True
    end
    object QrPesqSemiQtde: TFloatField
      FieldName = 'SemiQtde'
      Required = True
    end
    object QrPesqSemiAreaM2: TFloatField
      FieldName = 'SemiAreaM2'
      Required = True
    end
    object QrPesqSemiRendim: TFloatField
      FieldName = 'SemiRendim'
      Required = True
    end
    object QrPesqSemiCodEspe: TIntegerField
      FieldName = 'SemiCodEspe'
      Required = True
    end
    object QrPesqSemiTxtEspe: TWideStringField
      FieldName = 'SemiTxtEspe'
      Size = 15
    end
    object QrPesqBRL_USD: TFloatField
      FieldName = 'BRL_USD'
      Required = True
    end
    object QrPesqBRL_EUR: TFloatField
      FieldName = 'BRL_EUR'
      Required = True
    end
    object QrPesqDtaCambio: TDateField
      FieldName = 'DtaCambio'
      Required = True
    end
    object QrPesqVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
      Required = True
    end
    object QrPesqHoraIni: TTimeField
      FieldName = 'HoraIni'
      Required = True
      DisplayFormat = 'hh:nn'
    end
    object QrPesqGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Required = True
    end
    object QrPesqNoGraCorCad: TWideStringField
      FieldName = 'NoGraCorCad'
      Size = 30
    end
    object QrPesqSemiCodEspReb: TIntegerField
      FieldName = 'SemiCodEspReb'
      Required = True
    end
    object QrPesqSemiTxtEspReb: TWideStringField
      FieldName = 'SemiTxtEspReb'
      Size = 15
    end
    object QrPesqDtaBaixa: TDateField
      FieldName = 'DtaBaixa'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
      Required = True
    end
    object QrPesqEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPesqAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPesqAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPesqSbtCouIntros: TFloatField
      FieldName = 'SbtCouIntros'
      Required = True
    end
    object QrPesqSbtAreaM2: TFloatField
      FieldName = 'SbtAreaM2'
      Required = True
    end
    object QrPesqSbtAreaP2: TFloatField
      FieldName = 'SbtAreaP2'
      Required = True
    end
    object QrPesqSbtRendEsper: TFloatField
      FieldName = 'SbtRendEsper'
      Required = True
    end
    object QrPesqOperPosStatus: TSmallintField
      FieldName = 'OperPosStatus'
      Required = True
    end
    object QrPesqOperPosGrandz: TIntegerField
      FieldName = 'OperPosGrandz'
      Required = True
    end
    object QrPesqOperPosQtdDon: TFloatField
      FieldName = 'OperPosQtdDon'
      Required = True
    end
    object QrPesqOperPosDthIni: TDateTimeField
      FieldName = 'OperPosDthIni'
      Required = True
    end
    object QrPesqOperPosDthFim: TDateTimeField
      FieldName = 'OperPosDthFim'
      Required = True
    end
    object QrPesqNO_OperPosStatus: TWideStringField
      FieldName = 'NO_OperPosStatus'
      Size = 60
    end
    object QrPesqDtHrFinal: TDateTimeField
      FieldName = 'DtHrFinal'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 28
    Top = 196
  end
  object QrSetores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, OperPosProcDescr'
      'FROM listasetores'
      'ORDER BY Nome')
    Left = 200
    Top = 272
    object QrSetoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSetoresNome: TWideStringField
      FieldName = 'Nome'
    end
    object QrSetoresOperPosProcDescr: TWideStringField
      FieldName = 'OperPosProcDescr'
      Size = 60
    end
  end
  object DsSetores: TDataSource
    DataSet = QrSetores
    Left = 200
    Top = 320
  end
  object frxQUI_RECEI_025_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure MD1OnBeforePrint(Sender: TfrxComponent);'
      'var'
      
        '  Linhas, Altura: Extended;                                     ' +
        '     '
      'begin'
      '  Linhas := <VARF_LINHAS_OBS>;  '
      '  //ShowMessage(IntToStr(Linhas));'
      
        '  Altura := Linhas * (0.4 / 2.54) * 96;                         ' +
        '   '
      '  MD1.Height := Altura;'
      
        '  Me001.Height := Altura;                                       ' +
        '         '
      
        '  Me002.Height := Altura;                                       ' +
        '         '
      
        '  Me003.Height := Altura;                                       ' +
        '         '
      
        '  Me004.Height := Altura;                                       ' +
        '         '
      
        '  Me005.Height := Altura;                                       ' +
        '         '
      
        '  Me006.Height := Altura;                                       ' +
        '         '
      
        '  Me007.Height := Altura;                                       ' +
        '         '
      
        '  Me008.Height := Altura;                                       ' +
        '         '
      
        '  Me009.Height := Altura;                                       ' +
        '         '
      
        '  Me010.Height := Altura;                                       ' +
        '         '
      
        '  Me011.Height := Altura;                                       ' +
        '         '
      
        '  Me012.Height := Altura;                                       ' +
        '         '
      
        '  Me013.Height := Altura;                                       ' +
        '         '
      
        '  Me014.Height := Altura;                                       ' +
        '         '
      
        '  Me015.Height := Altura;                                       ' +
        '         '
      
        '  Me016.Height := Altura;                                       ' +
        '         '
      
        '  Me017.Height := Altura;                                       ' +
        '         '
      
        '  Me018.Height := Altura;                                       ' +
        '         '
      'end;'
      ''
      'begin'
      '    '
      'end.')
    OnGetValue = frxQUI_RECEI_025_AGetValue
    Left = 556
    Top = 316
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 71.811060240000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 56.692784020000000000
          Width = 49.133626380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fulonada')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488291500000000000
          Top = 56.692784020000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842651500000000000
          Top = 56.692784020000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtd Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1009.134510000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 687.874460000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'CRONOGRAMA OPERACIONAL P'#211'S PROCESSO DE [VAR_SET_NOME2]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 839.055660000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 56.692950000000000000
          Width = 204.094485750000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Receita')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 56.692950000000000000
          Width = 64.251746380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'In'#237'cio Processo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 37.795300000000000000
          Width = 279.684780630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_NO_STATUS]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 37.795300000000000000
          Width = 44.881880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Status:')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692950000000000000
          Width = 26.456692913385830000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ful'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Top = 56.692950000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Prev. t'#233'rmino')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 56.692950000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Top = 56.692950000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'kg m'#233'dio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 755.906000000000000000
          Top = 56.692950000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'In'#237'cio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 808.819420000000000000
          Top = 56.692950000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'T'#233'rmino')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 861.732840000000000000
          Top = 56.692950000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtd Curtir')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 914.646260000000000000
          Top = 56.692950000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtd Gelatina')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 967.559680000000000000
          Top = 56.692950000000000000
          Width = 41.574790940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '% Gelatina')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Top = 56.692950000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtd Operar')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 411.968770000000000000
          Top = 56.692950000000000000
          Width = 49.133626380000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-C')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118276220000000000
        Top = 151.181200000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'MD1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
        RowCount = 0
        object Me004: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Width = 49.133626380000000000
          Height = 15.118110240000000000
          DataField = 'Codigo'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me016: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 0.000165979999999996
          Width = 34.015735830000000000
          Height = 15.118110240000000000
          DataField = 'Numero'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."Numero"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me002: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 0.000165979999999996
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."DataEmis"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me006: TfrxMemoView
          AllowVectorExport = True
          Left = 241.889920000000000000
          Width = 170.078747480000000000
          Height = 15.118110240000000000
          DataField = 'NOME'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NOME"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me008: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me009: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me001: TfrxMemoView
          AllowVectorExport = True
          Top = 0.000165979999999996
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataField = 'Fulao'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."Fulao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me005: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Top = 0.000165979999999996
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'DtHrFinal'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."DtHrFinal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me007: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_TEXTO_OBS]')
          ParentFont = False
          WordWrap = False
        end
        object Me010: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197548500000000000
          Top = 0.000165979999999996
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsPesq."Qtde"> > 0, <frxDsPesq."Peso"> / <frxDsPesq."Qt' +
              'de">, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me003: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataField = 'HoraIni'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = 'hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."HoraIni"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me011: TfrxMemoView
          AllowVectorExport = True
          Left = 755.906268500000000000
          Top = 0.000165979999999996
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            ':')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me012: TfrxMemoView
          AllowVectorExport = True
          Left = 808.819688500000000000
          Top = 0.000165979999999996
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            ':')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me013: TfrxMemoView
          AllowVectorExport = True
          Left = 861.733108500000000000
          Top = 0.000165979999999996
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me014: TfrxMemoView
          AllowVectorExport = True
          Left = 914.646528500000000000
          Top = 0.000165979999999996
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me015: TfrxMemoView
          AllowVectorExport = True
          Left = 967.559948500000000000
          Top = 0.000165979999999996
          Width = 41.574790940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me017: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551908500000000000
          Top = 0.000165979999999996
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsPesq."Qtde"> - <frxDsPesq."OperPosQtdDon">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me018: TfrxMemoView
          AllowVectorExport = True
          Left = 411.968770000000000000
          Top = 0.000165979999999996
          Width = 49.133626380000000000
          Height = 15.118110240000000000
          DataField = 'VSMovCod'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."VSMovCod"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 275.905690000000000000
        Width = 1009.134510000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 702.992580000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 687.874460000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 26.456710000000000000
        ParentFont = False
        Top = 226.771800000000000000
        Width = 1009.134510000000000000
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."Peso">,MD1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."Qtde">,MD1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 702.992580000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."Qtde"> - <frxDsPesq."OperPosQtdDon">,MD1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 650.079160000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPesq."Qtde">,MD1) <= 0, 0, SUM(<frxDsPesq."Peso">' +
              ',MD1) / SUM(<frxDsPesq."Qtde">,MD1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPesq: TfrxDBDataset
    UserName = 'frxDsPesq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'DataEmis=DataEmis'
      'Status=Status'
      'Numero=Numero'
      'NOMECI=NOMECI'
      'NOMESETOR=NOMESETOR'
      'Tecnico=Tecnico'
      'NOME=NOME'
      'ClienteI=ClienteI'
      'Tipificacao=Tipificacao'
      'TempoR=TempoR'
      'TempoP=TempoP'
      'Setor=Setor'
      'Tipific=Tipific'
      'Espessura=Espessura'
      'DefPeca=DefPeca'
      'Peso=Peso'
      'Custo=Custo'
      'Qtde=Qtde'
      'AreaM2=AreaM2'
      'Fulao=Fulao'
      'Obs=Obs'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SetrEmi=SetrEmi'
      'SourcMP=SourcMP'
      'Cod_Espess=Cod_Espess'
      'CodDefPeca=CodDefPeca'
      'CustoTo=CustoTo'
      'CustoKg=CustoKg'
      'CustoM2=CustoM2'
      'CusUSM2=CusUSM2'
      'EmitGru=EmitGru'
      'Retrabalho=Retrabalho'
      'SemiCodPeca=SemiCodPeca'
      'SemiTxtPeca=SemiTxtPeca'
      'SemiPeso=SemiPeso'
      'SemiQtde=SemiQtde'
      'SemiAreaM2=SemiAreaM2'
      'SemiRendim=SemiRendim'
      'SemiCodEspe=SemiCodEspe'
      'SemiTxtEspe=SemiTxtEspe'
      'BRL_USD=BRL_USD'
      'BRL_EUR=BRL_EUR'
      'DtaCambio=DtaCambio'
      'VSMovCod=VSMovCod'
      'HoraIni=HoraIni'
      'GraCorCad=GraCorCad'
      'NoGraCorCad=NoGraCorCad'
      'SemiCodEspReb=SemiCodEspReb'
      'SemiTxtEspReb=SemiTxtEspReb'
      'DtaBaixa=DtaBaixa'
      'DtCorrApo=DtCorrApo'
      'Empresa=Empresa'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'SbtCouIntros=SbtCouIntros'
      'SbtAreaM2=SbtAreaM2'
      'SbtAreaP2=SbtAreaP2'
      'SbtRendEsper=SbtRendEsper'
      'OperPosStatus=OperPosStatus'
      'OperPosGrandz=OperPosGrandz'
      'OperPosQtdDon=OperPosQtdDon'
      'OperPosDthIni=OperPosDthIni'
      'OperPosDthFim=OperPosDthFim'
      'NO_OperPosStatus=NO_OperPosStatus'
      'DtHrFinal=DtHrFinal')
    DataSet = QrPesq
    BCDToCurrency = False
    DataSetOptions = []
    Left = 24
    Top = 248
  end
  object PMEmitMopp: TPopupMenu
    OnPopup = PMEmitMoppPopup
    Left = 288
    Top = 496
    object Incluinovaoperaopsprocesso1: TMenuItem
      Caption = '&Inclui nova opera'#231#227'o p'#243's processo'
      OnClick = Incluinovaoperaopsprocesso1Click
    end
    object Alteraoperaopsprocessoselecionada1: TMenuItem
      Caption = '&Altera opera'#231#227'o p'#243's processo selecionada'
      OnClick = Alteraoperaopsprocessoselecionada1Click
    end
    object Excluioperaopsprocessoselecionada1: TMenuItem
      Caption = '&Exclui opera'#231#227'o p'#243's processo selecionada'
      OnClick = Excluioperaopsprocessoselecionada1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Indefinirvrios1: TMenuItem
      Caption = '&Habilitar modo de redefinir v'#225'rios'
      Visible = False
      OnClick = Indefinirvrios1Click
    end
    object desabilitarmododeindefinirvrios1: TMenuItem
      Caption = '&Desabilitar modo de redefinir v'#225'rios'
      Visible = False
      OnClick = desabilitarmododeindefinirvrios1Click
    end
    object Redefinirselecionados1: TMenuItem
      Caption = '&Redefinir selecionados'
      OnClick = Redefinirselecionados1Click
    end
  end
  object QrEmitMopp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM emitmopp')
    Left = 24
    Top = 304
    object QrEmitMoppCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmitMoppControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmitMoppStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrEmitMoppGrandz: TIntegerField
      FieldName = 'Grandz'
      Required = True
    end
    object QrEmitMoppQtdDon: TFloatField
      FieldName = 'QtdDon'
      Required = True
    end
    object QrEmitMoppDthIni: TDateTimeField
      FieldName = 'DthIni'
      Required = True
    end
    object QrEmitMoppDthFim: TDateTimeField
      FieldName = 'DthFim'
      Required = True
    end
    object QrEmitMoppLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrEmitMoppDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmitMoppDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitMoppUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrEmitMoppUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrEmitMoppAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEmitMoppAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrEmitMoppAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrEmitMoppAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsEmitMopp: TDataSource
    DataSet = QrEmitMopp
    Left = 24
    Top = 352
  end
  object QrSumMopp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM emitmopp')
    Left = 200
    Top = 208
    object QrSumMoppStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrSumMoppQtdDon: TFloatField
      FieldName = 'QtdDon'
      Required = True
    end
    object QrSumMoppDthIni: TDateTimeField
      FieldName = 'DthIni'
      Required = True
    end
    object QrSumMoppDthFim: TDateTimeField
      FieldName = 'DthFim'
      Required = True
    end
  end
end
