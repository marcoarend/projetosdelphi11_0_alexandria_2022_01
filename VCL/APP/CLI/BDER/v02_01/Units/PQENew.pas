unit PQENew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, ExtCtrls, Buttons, Db, (*DBTables,*) ComCtrls,
  UnInternalConsts2, UnInternalConsts, UMySQLModule, mySQLDbTables, DmkDAC_PF,
  Variants, dmkGeral, dmkEdit, dmkDBLookupComboBox, dmkEditCB, dmkCheckBox,
  dmkEditDateTimePicker, dmkLabel, dmkImage, UnDmkEnums, UnDmkProcFunc,
  dmkRadioGroup, UnGrl_Vars;

type
  //
  TFmPQENew = class(TForm)
    QrFornecedores: TmySQLQuery;
    DsFornecedores: TDataSource;
    QrTransportadores: TmySQLQuery;
    DsTransportadores: TDataSource;
    QrItens: TmySQLQuery;
    QrItensCodigo: TIntegerField;
    QrItensControle: TIntegerField;
    QrItensConta: TIntegerField;
    QrItensInsumo: TIntegerField;
    QrItensVolumes: TIntegerField;
    QrItensPesoVB: TFloatField;
    QrItensPesoVL: TFloatField;
    QrItensValorItem: TFloatField;
    QrItensIPI: TFloatField;
    QrItensTotalCusto: TFloatField;
    QrItensTotalPeso: TFloatField;
    QrItensPreco: TFloatField;
    QrItensICMS: TFloatField;
    QrItensEntrada: TFloatField;
    QrInsIts: TmySQLQuery;
    QrItensPrazo: TWideStringField;
    QrItensCFin: TFloatField;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOME: TWideStringField;
    QrTransportadoresCodigo: TIntegerField;
    QrTransportadoresNOME: TWideStringField;
    QrCI: TmySQLQuery;
    DsCI: TDataSource;
    PainelDados: TPanel;
    Label17: TLabel;
    Label11: TLabel;
    Label6: TLabel;
    LaCI: TLabel;
    QrCICodigo: TIntegerField;
    QrCINOME: TWideStringField;
    PnNFs: TPanel;
    EdrefNFe: TdmkEdit;
    LarefNFe: TLabel;
    PnInativo: TPanel;
    TPEntrada: TdmkEditDateTimePicker;
    TPEmissao: TdmkEditDateTimePicker;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    CBCI: TdmkDBLookupComboBox;
    EdCI: TdmkEditCB;
    Label2: TLabel;
    EdRICMSF: TdmkEdit;
    Label12: TLabel;
    EdJuros: TdmkEdit;
    EdRICMS: TdmkEdit;
    EdICMS: TdmkEdit;
    Label1: TLabel;
    Label13: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel1: TPanel;
    LaEmpresa: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdPedido: TdmkEdit;
    LaSitPedido: TLabel;
    LaModNF: TLabel;
    EdModNF: TdmkEdit;
    EdSerie: TdmkEdit;
    LaSerie: TLabel;
    LaNF: TLabel;
    EdNF: TdmkEdit;
    EdNFe_CC: TdmkEdit;
    LaNFe_CC: TLabel;
    LamodCC: TLabel;
    EdmodCC: TdmkEdit;
    EdSerCC: TdmkEdit;
    LaSerCC: TLabel;
    EdNF_CC: TdmkEdit;
    LaNF_CC: TLabel;
    LaLancamento: TLabel;
    EdCodigo: TdmkEdit;
    SbNF_VP: TSpeedButton;
    Qr00_NFeCabA: TMySQLQuery;
    Qr00_NFeCabAFatID: TIntegerField;
    Qr00_NFeCabAFatNum: TIntegerField;
    Qr00_NFeCabAEmpresa: TIntegerField;
    Qr00_NFeCabAIDCtrl: TIntegerField;
    Qr00_NFeCabAAtrelaFatID: TIntegerField;
    Qr00_NFeCabAAtrelaStaLnk: TSmallintField;
    Qr00_NFeCabAide_mod: TSmallintField;
    Qr00_NFeCabAide_serie: TIntegerField;
    Qr00_NFeCabAide_nNF: TIntegerField;
    QrFornecedoresTipo: TSmallintField;
    QrFornecedoresCNPJ: TWideStringField;
    QrFornecedoresCPF: TWideStringField;
    QrTransportadoresTipo: TSmallintField;
    QrTransportadoresCNPJ: TWideStringField;
    QrTransportadoresCPF: TWideStringField;
    QrCITipo: TSmallintField;
    QrCICNPJ: TWideStringField;
    QrCICPF: TWideStringField;
    LaNFe_RP: TLabel;
    EdNFe_RP: TdmkEdit;
    EdmodRP: TdmkEdit;
    LamodRP: TLabel;
    LaSerRP: TLabel;
    EdSerRP: TdmkEdit;
    EdNF_RP: TdmkEdit;
    LaNF_RP: TLabel;
    Label4: TLabel;
    EdNFVP_StaLnk: TdmkEdit;
    EdNFVP_FatID: TdmkEdit;
    EdNFVP_FatNum: TdmkEdit;
    EdNFCC_StaLnk: TdmkEdit;
    EdNFCC_FatID: TdmkEdit;
    EdNFCC_FatNum: TdmkEdit;
    EdNFRP_StaLnk: TdmkEdit;
    EdNFRP_FatID: TdmkEdit;
    EdNFRP_FatNum: TdmkEdit;
    Label9: TLabel;
    Label15: TLabel;
    SbNF_CC: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Qr00_NFeCabAId: TWideStringField;
    QrPediVdaIts: TMySQLQuery;
    QrPediVdaItsCodigo: TIntegerField;
    QrPediVdaItsControle: TIntegerField;
    QrPediVdaItsGraGruX: TIntegerField;
    QrPediVdaItsPrecoO: TFloatField;
    QrPediVdaItsPrecoR: TFloatField;
    QrPediVdaItsQuantP: TFloatField;
    QrPediVdaItsQuantC: TFloatField;
    QrPediVdaItsQuantV: TFloatField;
    QrPediVdaItsValBru: TFloatField;
    QrPediVdaItsDescoP: TFloatField;
    QrPediVdaItsDescoV: TFloatField;
    QrPediVdaItsValLiq: TFloatField;
    QrPediVdaItsPrecoF: TFloatField;
    QrPediVdaItsMedidaC: TFloatField;
    QrPediVdaItsMedidaL: TFloatField;
    QrPediVdaItsMedidaA: TFloatField;
    QrPediVdaItsMedidaE: TFloatField;
    QrPediVdaItsPercCustom: TFloatField;
    QrPediVdaItsCustomizad: TSmallintField;
    QrPediVdaItsInfAdCuztm: TIntegerField;
    QrPediVdaItsOrdem: TIntegerField;
    QrPediVdaItsReferencia: TWideStringField;
    QrPediVdaItsMulti: TIntegerField;
    QrPediVdaItsvProd: TFloatField;
    QrPediVdaItsvFrete: TFloatField;
    QrPediVdaItsvSeg: TFloatField;
    QrPediVdaItsvOutro: TFloatField;
    QrPediVdaItsvDesc: TFloatField;
    QrPediVdaItsvBC: TFloatField;
    QrGraGru1: TMySQLQuery;
    QrGraGru1ICMSRec_pAliq: TFloatField;
    QrGraGru1PISRec_pAliq: TFloatField;
    QrGraGru1COFINSRec_pAliq: TFloatField;
    Qr00_NFeCabAAtrelaFatNum: TIntegerField;
    QrItsI: TMySQLQuery;
    QrItsIFatID: TIntegerField;
    QrItsIFatNum: TIntegerField;
    QrItsIEmpresa: TIntegerField;
    QrItsInItem: TIntegerField;
    QrItsIprod_cProd: TWideStringField;
    QrItsIprod_cEAN: TWideStringField;
    QrItsIprod_cBarra: TWideStringField;
    QrItsIprod_xProd: TWideStringField;
    QrItsIprod_NCM: TWideStringField;
    QrItsIprod_CEST: TIntegerField;
    QrItsIprod_indEscala: TWideStringField;
    QrItsIprod_CNPJFab: TWideStringField;
    QrItsIprod_cBenef: TWideStringField;
    QrItsIprod_EXTIPI: TWideStringField;
    QrItsIprod_genero: TSmallintField;
    QrItsIprod_CFOP: TIntegerField;
    QrItsIprod_uCom: TWideStringField;
    QrItsIprod_qCom: TFloatField;
    QrItsIprod_vUnCom: TFloatField;
    QrItsIprod_vProd: TFloatField;
    QrItsIprod_cEANTrib: TWideStringField;
    QrItsIprod_cBarraTrib: TWideStringField;
    QrItsIprod_uTrib: TWideStringField;
    QrItsIprod_qTrib: TFloatField;
    QrItsIprod_vUnTrib: TFloatField;
    QrItsIprod_vFrete: TFloatField;
    QrItsIprod_vSeg: TFloatField;
    QrItsIprod_vDesc: TFloatField;
    QrItsIprod_vOutro: TFloatField;
    QrItsIprod_indTot: TSmallintField;
    QrItsIprod_xPed: TWideStringField;
    QrItsIprod_nItemPed: TIntegerField;
    QrItsITem_IPI: TSmallintField;
    QrItsI_Ativo_: TSmallintField;
    QrItsIInfAdCuztm: TIntegerField;
    QrItsIEhServico: TIntegerField;
    QrItsIUsaSubsTrib: TSmallintField;
    QrItsIICMSRec_pRedBC: TFloatField;
    QrItsIICMSRec_vBC: TFloatField;
    QrItsIICMSRec_pAliq: TFloatField;
    QrItsIICMSRec_vICMS: TFloatField;
    QrItsIIPIRec_pRedBC: TFloatField;
    QrItsIIPIRec_vBC: TFloatField;
    QrItsIIPIRec_pAliq: TFloatField;
    QrItsIIPIRec_vIPI: TFloatField;
    QrItsIPISRec_pRedBC: TFloatField;
    QrItsIPISRec_vBC: TFloatField;
    QrItsIPISRec_pAliq: TFloatField;
    QrItsIPISRec_vPIS: TFloatField;
    QrItsICOFINSRec_pRedBC: TFloatField;
    QrItsICOFINSRec_vBC: TFloatField;
    QrItsICOFINSRec_pAliq: TFloatField;
    QrItsICOFINSRec_vCOFINS: TFloatField;
    QrItsIMeuID: TIntegerField;
    QrItsINivel1: TIntegerField;
    QrItsIGraGruX: TIntegerField;
    QrItsIUnidMedCom: TIntegerField;
    QrItsIUnidMedTrib: TIntegerField;
    QrItsIICMSRec_vBCST: TFloatField;
    QrItsIICMSRec_vICMSST: TFloatField;
    QrItsIICMSRec_pAliqST: TFloatField;
    QrItsITem_II: TSmallintField;
    QrItsIprod_nFCI: TWideStringField;
    QrItsIStqMovValA: TIntegerField;
    QrItsIAtivo: TSmallintField;
    QrItsIAtrelaID: TIntegerField;
    QrItsV: TMySQLQuery;
    QrItsVnItem: TIntegerField;
    QrItsVInfAdProd: TWideMemoField;
    QrItsN: TMySQLQuery;
    QrItsNICMS_Orig: TSmallintField;
    QrItsNICMS_CST: TSmallintField;
    QrItsO: TMySQLQuery;
    QrItsOIPI_CST: TSmallintField;
    QrItsOIPI_cEnq: TWideStringField;
    QrItsS: TMySQLQuery;
    QrItsSCOFINS_CST: TSmallintField;
    QrItsQ: TMySQLQuery;
    QrItsQPIS_CST: TSmallintField;
    QrItsIprod_vTotItm: TFloatField;
    QrItsOIPI_clEnq: TWideStringField;
    QrItsOIPI_CNPJProd: TWideStringField;
    QrItsOIPI_cSelo: TWideStringField;
    QrItsOIPI_qSelo: TFloatField;
    QrItsOIPI_vBC: TFloatField;
    QrItsOIPI_qUnid: TFloatField;
    QrItsOIPI_vUnid: TFloatField;
    QrItsOIPI_pIPI: TFloatField;
    QrItsOIPI_vIPI: TFloatField;
    QrItsOIND_APUR: TWideStringField;
    QrItsNICMS_vBCEfet: TFloatField;
    QrItsNICMS_pICMSEfet: TFloatField;
    QrItsNICMS_vICMSEfet: TFloatField;
    QrItsNICMS_modBC: TSmallintField;
    QrItsNICMS_pRedBC: TFloatField;
    QrItsNICMS_vBC: TFloatField;
    QrItsNICMS_pICMS: TFloatField;
    QrItsNICMS_vICMSOp: TFloatField;
    QrItsNICMS_pDif: TFloatField;
    QrItsNICMS_vICMSDif: TFloatField;
    QrItsNICMS_vICMS: TFloatField;
    QrItsNICMS_vBCFCP: TFloatField;
    QrItsNICMS_pFCP: TFloatField;
    QrItsNICMS_vFCP: TFloatField;
    QrItsNICMS_pFCPDif: TFloatField;
    QrItsNICMS_vFCPDif: TFloatField;
    QrItsNICMS_vFCPEfet: TFloatField;
    QrItsNICMS_modBCST: TSmallintField;
    QrItsNICMS_pMVAST: TFloatField;
    QrItsNICMS_pRedBCST: TFloatField;
    QrItsNICMS_vBCST: TFloatField;
    QrItsNICMS_pICMSST: TFloatField;
    QrItsNICMS_vICMSST: TFloatField;
    QrItsNICMS_vBCFCPST: TFloatField;
    QrItsNICMS_pFCPST: TFloatField;
    QrItsNICMS_vFCPST: TFloatField;
    QrItsNICMS_CSOSN: TIntegerField;
    QrItsNICMS_UFST: TWideStringField;
    QrItsNICMS_pBCOp: TFloatField;
    QrItsNICMS_vBCSTRet: TFloatField;
    QrItsNICMS_pST: TFloatField;
    QrItsNICMS_vICMSSTRet: TFloatField;
    QrItsNICMS_vICMSDeson: TFloatField;
    QrItsNICMS_vBCFCPSTRet: TFloatField;
    QrItsNICMS_pFCPSTRet: TFloatField;
    QrItsNICMS_vFCPSTRet: TFloatField;
    QrItsNICMS_motDesICMS: TSmallintField;
    QrItsNICMS_pCredSN: TFloatField;
    QrItsNICMS_vCredICMSSN: TFloatField;
    QrItsNCOD_NAT: TWideStringField;
    QrItsNICMS_vICMSSubstituto: TFloatField;
    QrItsNICMS_vICMSSTDeson: TFloatField;
    QrItsNICMS_motDesICMSST: TSmallintField;
    QrItsNICMS_pRedBCEfet: TFloatField;
    QrItsQPIS_vBC: TFloatField;
    QrItsQPIS_pPIS: TFloatField;
    QrItsQPIS_vPIS: TFloatField;
    QrItsQPIS_qBCProd: TFloatField;
    QrItsQPIS_vAliqProd: TFloatField;
    QrItsQPIS_fatorBC: TFloatField;
    QrItsSCOFINS_vBC: TFloatField;
    QrItsSCOFINS_pCOFINS: TFloatField;
    QrItsSCOFINS_qBCProd: TFloatField;
    QrItsSCOFINS_vAliqProd: TFloatField;
    QrItsSCOFINS_vCOFINS: TFloatField;
    QrItsSCOFINS_fatorBC: TFloatField;
    QrItsR: TMySQLQuery;
    QrItsT: TMySQLQuery;
    QrItsRPISST_vBC: TFloatField;
    QrItsRPISST_pPIS: TFloatField;
    QrItsRPISST_qBCProd: TFloatField;
    QrItsRPISST_vAliqProd: TFloatField;
    QrItsRPISST_vPIS: TFloatField;
    QrItsRPISST_indSomaPISST: TSmallintField;
    QrItsRPISST_fatorBCST: TFloatField;
    QrItsTCOFINSST_vBC: TFloatField;
    QrItsTCOFINSST_pCOFINS: TFloatField;
    QrItsTCOFINSST_qBCProd: TFloatField;
    QrItsTCOFINSST_vAliqProd: TFloatField;
    QrItsTCOFINSST_vCOFINS: TFloatField;
    QrItsTCOFINSST_indSomaCOFINSST: TSmallintField;
    QrItsTCOFINSST_fatorBCST: TFloatField;
    QrCabA: TMySQLQuery;
    QrCabAemit_CRT: TSmallintField;
    CBRegrFiscal: TdmkDBLookupComboBox;
    Label31: TLabel;
    EdRegrFiscal: TdmkEditCB;
    QrFisRegCad: TMySQLQuery;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadModeloNF: TIntegerField;
    QrFisRegCadNO_MODELO_NF: TWideStringField;
    QrFisRegCadFinanceiro: TSmallintField;
    DsFisRegCad: TDataSource;
    QrPediVda: TMySQLQuery;
    QrPediPrzCab: TMySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    DsPediPrzCab: TDataSource;
    LaCondicaoPG: TLabel;
    EdCondicaoPG: TdmkEditCB;
    CBCondicaoPG: TdmkDBLookupComboBox;
    BtCondicaoPG: TSpeedButton;
    QrPediPrzCabCondPg: TSmallintField;
    QrPediPrzIts: TMySQLQuery;
    QrPediPrzItsControle: TIntegerField;
    QrPediPrzItsDias: TIntegerField;
    QrPediPrzItsPercent1: TFloatField;
    QrCabAide_dEmi: TDateField;
    QrPediVdaTipoCart: TIntegerField;
    QrPediVdaGenCtbD: TIntegerField;
    QrPediVdaGenCtbC: TIntegerField;
    QrPediVdaCodUsu: TIntegerField;
    QrPediVdaMadeBy: TSmallintField;
    QrPediVdaCartEmis: TIntegerField;
    QrPediVdaRegrFiscal: TIntegerField;
    QrPediVdaRepresen: TIntegerField;
    QrPediVdaFinanceiro: TSmallintField;
    QrPediVdaCliente: TIntegerField;
    QrLct: TMySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctControle: TIntegerField;
    QrLctSub: TSmallintField;
    QrCabAide_nNF: TIntegerField;
    QrCabAide_serie: TIntegerField;
    QrCabAICMSTot_vNF: TFloatField;
    QrCabACodInfoEmit: TIntegerField;
    QrNFeCabY: TMySQLQuery;
    QrNFeCabYControle: TIntegerField;
    QrNFeCabYnDup: TWideStringField;
    QrNFeCabYdVenc: TDateField;
    QrNFeCabYvDup: TFloatField;
    QrNFeCabYLancto: TIntegerField;
    QrNFeCabYSub: TIntegerField;
    QrNFeCabYEmpresa: TIntegerField;
    QrSumY: TMySQLQuery;
    QrSumYvDup: TFloatField;
    QrFornecedoresNOME_E_DOC_ENTIDADE: TWideStringField;
    PnDados3: TPanel;
    Label16: TLabel;
    EdIND_PGTO_TXT: TdmkEdit;
    EdIND_PGTO: TdmkEdit;
    Label18: TLabel;
    EdICMSTot_vProd: TdmkEdit;
    Label20: TLabel;
    EdICMSTot_vFrete: TdmkEdit;
    Label22: TLabel;
    EdICMSTot_vSeg: TdmkEdit;
    Label28: TLabel;
    EdICMSTot_vOutro: TdmkEdit;
    Label29: TLabel;
    EdICMSTot_vDesc: TdmkEdit;
    Label30: TLabel;
    EdIPI: TdmkEdit;
    Label3: TLabel;
    EdValorNF: TdmkEdit;
    Label5: TLabel;
    EdkgLiquido: TdmkEdit;
    Label7: TLabel;
    EdkgBruto: TdmkEdit;
    Label108: TLabel;
    EdIND_FRT: TdmkEdit;
    EdIND_FRT_TXT: TdmkEdit;
    EdTransportador: TdmkEditCB;
    Label19: TLabel;
    CBTransportador: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    Panel4: TPanel;
    Panel5: TPanel;
    Label27: TLabel;
    Label21: TLabel;
    Label23: TLabel;
    Label25: TLabel;
    EdCte_Id: TdmkEdit;
    EdCTe_mod: TdmkEdit;
    EdCTe_serie: TdmkEdit;
    EdConhecim: TdmkEdit;
    Panel6: TPanel;
    Label8: TLabel;
    Label14: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    EdFrete: TdmkEdit;
    EdFreteRpICMS: TdmkEdit;
    EdFreteRvICMS: TdmkEdit;
    EdFreteRpPIS: TdmkEdit;
    EdFreteRvPIS: TdmkEdit;
    EdFreteRpCOFINS: TdmkEdit;
    EdFreteRvCOFINS: TdmkEdit;
    RGMadeBy: TdmkRadioGroup;
    CkTipoNF: TdmkCheckBox;
    QrCabAICMSTot_vBC: TFloatField;
    QrCabAICMSTot_vICMS: TFloatField;
    QrCabAICMSTot_vICMSDeson: TFloatField;
    QrCabAICMSTot_vFCPUFDest: TFloatField;
    QrCabAICMSTot_vICMSUFDest: TFloatField;
    QrCabAICMSTot_vICMSUFRemet: TFloatField;
    QrCabAICMSTot_vFCP: TFloatField;
    QrCabAICMSTot_vBCST: TFloatField;
    QrCabAICMSTot_vST: TFloatField;
    QrCabAICMSTot_vFCPST: TFloatField;
    QrCabAICMSTot_vFCPSTRet: TFloatField;
    QrCabAICMSTot_vProd: TFloatField;
    QrCabAICMSTot_vFrete: TFloatField;
    QrCabAICMSTot_vSeg: TFloatField;
    QrCabAICMSTot_vDesc: TFloatField;
    QrCabAICMSTot_vII: TFloatField;
    QrCabAICMSTot_vIPI: TFloatField;
    QrCabAICMSTot_vIPIDevol: TFloatField;
    QrCabAICMSTot_vPIS: TFloatField;
    QrCabAICMSTot_vCOFINS: TFloatField;
    QrCabAICMSTot_vOutro: TFloatField;
    QrCabAide_indPag: TSmallintField;
    QrCabAModFrete: TSmallintField;
    QrFisRegCadGenCtbD: TIntegerField;
    QrFisRegCadGenCtbC: TIntegerField;
    QrTransportadoresNOME_E_DOC_ENTIDADE: TWideStringField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdkgLiquidoExit(Sender: TObject);
    procedure EdkgBrutoExit(Sender: TObject);
    procedure EdFreteExit(Sender: TObject);
    procedure EdConhecimExit(Sender: TObject);
    procedure EdRICMSExit(Sender: TObject);
    procedure EdValorNFExit(Sender: TObject);
    procedure EdNFExit(Sender: TObject);
    procedure EdRICMSFExit(Sender: TObject);
    procedure EdJurosExit(Sender: TObject);
    procedure EdICMSExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure CkTipoNFClick(Sender: TObject);
    procedure EdrefNFeExit(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdFreteChange(Sender: TObject);
    procedure EdFreteRpICMSChange(Sender: TObject);
    procedure EdFreteRpPISChange(Sender: TObject);
    procedure EdFreteRpCOFINSChange(Sender: TObject);
    procedure EdrefNFeRedefinido(Sender: TObject);
    procedure EdNFe_RPRedefinido(Sender: TObject);
    procedure EdNFe_CCRedefinido(Sender: TObject);
    procedure EdNFe_RPExit(Sender: TObject);
    procedure EdNFe_CCExit(Sender: TObject);
    procedure EdIND_FRTChange(Sender: TObject);
    procedure EdIND_FRTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdIND_PGTOChange(Sender: TObject);
    procedure EdIND_PGTOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
      // Meu
    procedure CalculaTotalNF(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SbNF_VPClick(Sender: TObject);
    procedure SbNF_CCClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure EdCIChange(Sender: TObject);
    procedure BtCondicaoPGClick(Sender: TObject);
    procedure EdNFVP_FatNumChange(Sender: TObject);
    procedure EdNFCC_FatNumChange(Sender: TObject);
  private
    { Private declarations }
    F_IND_PGTO_EFD, F_IND_FRT_EFD: MyArrayLista;
//    procedure ControlChange(Sender : TObject);
    procedure CalculaRetornoImpostosFrete(Qual: TRetornImpost);
    procedure DefineDFe_Atrela(const TipoNFeEntrada: TTipoNFeEntrada);
              //var TipoNFeRetSrvr: TTipoNFeRetSrvr; var chNFe: String);
    function  InserePQEItsDeNFe(const Codigo: Integer; var FisRegGenCtbD,
              FisRegGenCtbC, MadeBy, OriPart: Integer): Boolean;
    function  InserePQEItsDePediVda(Codigo: Integer): Boolean;
    procedure ReopenPQEItsDePediVda_All(Codigo: Integer);
    procedure ReopenPQEItsDePediVda_Uni(Codigo: Integer; Quantidade,
              Preco: Double);
    procedure ReopenPediVda(Codigo: Integer);
    procedure VerificaChaveNFeDeEntrada(Empresa, CliInt: Integer; ChaveEmpresa,
              ChaveCliInt: String);
    procedure ReopenPediPrzCab();
    procedure ReopenCabA();
    // Financeiro
    procedure InsereLancamentosFinanceirosNFe(Empresa, Filial, Financeiro: Integer;
              ValorNF: Double; NumeroNF, CondicaoPG: Integer; GenCtbD, GenCtbC,
              NF_Emp_Serie, NF_Emp_Numer, Fornecedor, OriCodi: Integer;
              DataFat: TDateTime);
    procedure InsereLancamentosFinanceirosPvd(Empresa, Filial, Financeiro: Integer;
              ValorNF: Double; NumeroNF, CondicaoPG: Integer; GenCtbD, GenCtbC,
              NF_Emp_Serie, NF_Emp_Numer, Fornecedor, OriCodi: Integer;
              DataFat: TDateTime);
    //procedure ReabreQueryPrzT(CondicaoPG: Integer);
    procedure ReabreQueryPrzX(CondicaoPG: Integer);
    function  IncluiLanctoFaturasNFe(Valor, MoraDia: Double; Data, Vencto:
              TDateTime; Duplicata, Descricao: String; TipoCart, Carteira,
              Genero, GenCtb, CliInt, Parcela, NotaFiscal, Account, Financeiro,
              GenCtbD, GenCtbC, Filial, Fornecedor, OriCodi: Integer; SerieNF:
              String; VerificaCliInt: Boolean): Integer;
    procedure AtrelaPagamentoAntecipado(PQE_Codigo, FatParcRef: Integer; TabLctA: String);
    // Fim Financeiro
    procedure SetaFocusIni();
    procedure ObtemDadosAtrelamento();
    function  CadastroItensOK(const FatID, FatNum, Empresa, CodInfoEmit: Integer): Boolean;

    procedure ReopenItsI();
    procedure ReopenItsN();
  public
    { Public declarations }
    FDesiste, FUsouNFe: Boolean;
    FPediVda, FSqLinked, FVerPedi, FTpEntrd: Integer;
    FNFe_FatID, FNFe_FatNum, FNFe_Empresa, FCodInfoEmit, FPQE: Integer;
  end;

var
  FmPQENew: TFmPQENew;

implementation

uses UnMyObjects, Principal, PQE, Module, PQx, ModuleNFe_0000, NFe_PF,
  ModuleGeral, NFeXMLGerencia, ModProd, ModPediVda, UnSPED_PF, UnPraz_PF,
  UnFinanceiro, UnFinanceiroJan, UnGrade_Jan;

{$R *.DFM}

const
  FEFDInnNFSMainFatID = VAR_FATID_0010;
  FThisFatFornece     = VAR_FATID_1001;
  CO_MovimCod_ZERO    = 0;


procedure TFmPQENew.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PQE', Codigo)
  else
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PQE', 'Codigo');
  //
  FmPQE.ImgTipo.SQLType := stLok;
  FmPQE.GBTrava.Visible := False;
  FmPQE.GBCntrl.Visible := True;
  //
  FDesiste := True;
  //
  Close;
end;

procedure TFmPQENew.AtrelaPagamentoAntecipado(PQE_Codigo, FatParcRef: Integer; TabLctA: String);
var
  Data: String;
  FatNum,
  Tipo, Carteira, Controle, Sub: Integer;
begin
  FatNum := PQE_Codigo;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLct, Dmod.MyDB, [
  'SELECT la.Data, la.Tipo, la.Carteira,  ',
  'la.Controle, la.Sub ',
  'FROM ' + TabLctA + ' la ',
  'WHERE la.FatID=' + Geral.FF0(VAR_FATID_1001),
  'AND la.FatParcRef=' + Geral.FF0(FatParcRef),
  'AND la.ID_Pgto = 0 ',
  '']);
  Data     := Geral.FDT(QrLctData.Value, 1);
  Tipo     := QrLctTipo.Value;
  Carteira := QrLctCarteira.Value;
  Controle := QrLctControle.Value;
  Sub      := QrLctSub.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabLctA, False, [
  (*'Autorizacao', 'Genero', 'Qtde',
  'Descricao', 'SerieNF', 'NotaFiscal',
  'Debito', 'Credito', 'Compensado',
  'SerieCH', 'Documento', 'Sit',
  'Vencimento', 'FatID', 'FatID_Sub',*)
  'FatNum'(*, 'FatParcela', 'ID_Pgto',
  'ID_Quit', 'ID_Sub', 'Fatura',
  'Emitente', 'Banco', 'Agencia',
  'ContaCorrente', 'CNPJCPF', 'Local',
  'Cartao', 'Linha', 'OperCount',
  'Lancto', 'Pago', 'Mez',
  'Fornecedor', 'Cliente', 'CliInt',
  'ForneceI', 'MoraDia', 'Multa',
  'MoraVal', 'MultaVal', 'Protesto',
  'DataDoc', 'CtrlIni', 'Nivel',
  'Vendedor', 'Account', 'ICMS_P',
  'ICMS_V', 'Duplicata', 'Depto',
  'DescoPor', 'DescoVal', 'DescoControle',
  'Unidade', 'NFVal', 'Antigo',
  'ExcelGru', 'Doc2', 'CNAB_Sit',
  'TipoCH', 'Reparcel', 'Atrelado',
  'PagMul', 'PagJur', 'RecDes',
  'SubPgto1', 'MultiPgto', 'Protocolo',
  'CtrlQuitPg', 'Endossas', 'Endossan',
  'Endossad', 'Cancelado', 'EventosCad',
  'Encerrado', 'ErrCtrl', 'IndiPag',
  'CentroCusto', 'FatParcRef', 'FatSit',
  'FatSitSub', 'FatGrupo', 'TaxasVal',
  'FisicoSrc', 'FisicoCod', 'TemCROLct',
  'lanctos', 'Qtd2', 'VctoOriginal',
  'ModeloNF', 'HoraCad', 'HoraAlt',
  'GenCtb', 'QtDtPg', 'GenCtbD',
  'GenCtbC'*)], [
  'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
  (*Autorizacao, Genero, Qtde,
  Descricao, SerieNF, NotaFiscal,
  Debito, Credito, Compensado,
  SerieCH, Documento, Sit,
  Vencimento, FatID, FatID_Sub,*)
  FatNum(*, FatParcela, ID_Pgto,
  ID_Quit, ID_Sub, Fatura,
  Emitente, Banco, Agencia,
  ContaCorrente, CNPJCPF, Local,
  Cartao, Linha, OperCount,
  Lancto, Pago, Mez,
  Fornecedor, Cliente, CliInt,
  ForneceI, MoraDia, Multa,
  MoraVal, MultaVal, Protesto,
  DataDoc, CtrlIni, Nivel,
  Vendedor, Account, ICMS_P,
  ICMS_V, Duplicata, Depto,
  DescoPor, DescoVal, DescoControle,
  Unidade, NFVal, Antigo,
  ExcelGru, Doc2, CNAB_Sit,
  TipoCH, Reparcel, Atrelado,
  PagMul, PagJur, RecDes,
  SubPgto1, MultiPgto, Protocolo,
  CtrlQuitPg, Endossas, Endossan,
  Endossad, Cancelado, EventosCad,
  Encerrado, ErrCtrl, IndiPag,
  CentroCusto, FatParcRef, FatSit,
  FatSitSub, FatGrupo, TaxasVal,
  FisicoSrc, FisicoCod, TemCROLct,
  lanctos, Qtd2, VctoOriginal,
  ModeloNF, HoraCad, HoraAlt,
  GenCtb, QtDtPg, GenCtbD,
  GenCtbC*)], [
  Data, Tipo, Carteira, Controle, Sub], True) then
  begin
    //
  end;
end;

procedure TFmPQENew.BtCondicaoPGClick(Sender: TObject);
var
  CondicaoPG: Integer;
begin
  VAR_CADASTRO := 0;
  //
  Praz_PF.MostraFormPediPrzCab1(0);
  //
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCondicaoPg, CBCondicaoPg, QrPediPrzCab, VAR_CADASTRO);
end;

procedure TFmPQENew.BtConfirmaClick(Sender: TObject);
var
  PesoL, PesoB, ValorNF, ICMS, RICMS, RICMSF, ValorItem, Frete, Juros: Double;
  Codigo, IQ, CI, Pedido, Conta, Controle, Transportadora, NumNF, modNF, Serie,
  Conhecimento, TipoNF, Empresa, Filial: Integer;
  //
  Data, DataE, refNFe: String;
  NF_RP, NF_CC: Integer;
{
    , IQ, CI,
    Transportadora, NF, Frete,
    PesoB, PesoL, ValorNF,
    RICMS, RICMSF, Conhecimento,
    Pedido, DataE, Juros,
    ICMS, Cancelado, TipoNF,
    refNFe, modNF, Serie], [
    Codigo], True) then
}
  // 2022-03-??
  FreteRpICMS, FreteRpPIS, FreteRpCOFINS,
  FreteRvICMS, FreteRvPIS, FreteRvCOFINS: Double;
  // 2022-04=02
  NFe_RP, NFe_CC, Cte_Id: String;
  modRP, modCC, SerRP, SerCC, CTe_mod, CTe_serie: Integer;
  PrecisaNF: Boolean;
  NFeCabA_FatID, NFeCabA_FatNum, NFeCabA_StaLnk: Integer;
  NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
  NFRP_FatID, NFRP_FatNum, NFRP_StaLnk,
  NFCC_FatID, NFCC_FatNum, NFCC_StaLnk,
  IND_PGTO, IND_FRT: Integer;
  //
  ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vOutro: Double;
  //
  IsLinked, SqLinked, MovFatID, MovFatNum, MovimCod, Terceiro, Motorista: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  COD_MOD, COD_SIT, SER, NUM_DOC, NFeStatus, NFe_FatID, NFe_FatNum, NFe_StaLnk,
  VSVmcWrn, VSVmcSta, EfdInnNFsCab: Integer;
  Placa, CHV_NFE, DT_DOC, DT_E_S, VSVmcObs, VSVmcSeq: String;
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST, IPI: Double;
  UsaEntradaFiscal: Boolean;
  RegrFiscal, CondicaoPg, FisRegGenCtbD, FisRegGenCtbC, PQE_Codigo: Integer;
  DataFat: TDateTime;
  TabLctA: String;
  FatParcRef, FormaInsLct: Integer;
  ValY: Double;
  DataFiscal: String;
  MadeBy, OriPart: Integer;

  // ...
  function InsAltEfdInnNFsCab(): Boolean;
  var
    EfdSQLType: TSQLType;
    LnkTabName, LnkFldName: String;
  begin
        MovFatID  := FEFDInnNFSMainFatID;
        MovFatNum := Codigo;
        MovimCod  := CO_MovimCod_ZERO;
        //
        IsLinked  := 1; // True
        if ImgTipo.SQLType = stIns then
        begin
          EfdSQLType := stIns;
          SqLinked  := 0; // Buscar l� na inclus�o!
          EfdInnNFsCab   := 0; // Buscar!
        end else
        begin
          DmNFe_0000.ReopenEfdLinkCab(MovFatID, MovFatNum, MovimCod, Empresa);
          if DmNFe_0000.QrEfdLinkCab.RecordCount > 0 then
          //if FSqLinked > 0 then
          begin
            EfdSQLType := stUpd;
            SqLinked     := DmNFe_0000.QrEfdLinkCabSqLinked.Value;
            EfdInnNFsCab := DmNFe_0000.QrEfdLinkCabControle.Value;
          end else
          begin
            EfdSQLType := stIns;
            SqLinked  := 0; // Buscar l� na inclus�o!
            EfdInnNFsCab   := 0; // Buscar!
          end;
        end;
        if Empresa = CI then
        begin
          Terceiro := IQ;
          COD_MOD  := modNF;
          COD_SIT  := 00; // Documento regular
          SER      := Serie;
          NUM_DOC  := NumNF;
          CHV_NFE  := refNFe;
          NFe_FatID   := NFVP_FatID;
          NFe_FatNum  := NFVP_FatNum;
          NFe_StaLnk  := NFVP_StaLnk;
        end else
        begin
          Terceiro := CI;
          COD_MOD  := modCC;
          COD_SIT  := 00; // Documento regular
          SER      := SerCC;
          NUM_DOC  := NF_CC;
          CHV_NFE  := NFe_CC;
          NFe_FatID   := NFCC_FatID;
          NFe_FatNum  := NFCC_FatNum;
          NFe_StaLnk  := NFCC_StaLnk;
        end;
        NFeStatus := 100;
        //
        Pecas := 0;
        PesoKg := 0.000;
        AreaM2 := 0.00;
        AreaP2 := 0.00;
        ValorT := 0.00;
        Motorista := 0;
        Placa := '';
        DT_DOC := DataE;
        DT_E_S := Data;
        VL_DOC := ValorNF;
        VL_DESC := ICMSTot_vDesc;
        VL_ABAT_NT := 0.00;
        VL_MERC := ICMSTot_vProd;
        VL_FRT := ICMSTot_vFrete;
        VL_SEG := ICMSTot_vSeg;
        VL_OUT_DA := ICMSTot_vOutro;
        //
        IPI            := EdIPI.ValueVariant;
        //
        VL_BC_ICMS     := 0.00;
        VL_ICMS        := 0.00;
        VL_BC_ICMS_ST  := 0.00;
        VL_ICMS_ST     := 0.00;
        VL_IPI         := IPI;
        VL_PIS         := 0.00;
        VL_COFINS      := 0.00;
        VL_PIS_ST      := 0.00;
        VL_COFINS_ST   := 0.00;
        //
        VSVmcWrn       := 0;
        VSVmcObs       := '';
        VSVmcSeq       := '';
        VSVmcSta       := 0;
        //
        LnkTabName     := 'pqe';
        LnkFldName     := 'Codigo';
        //
        //
        if UsaEntradaFiscal then
          Result :=  SPED_PF.InsAltEfdInnNFsCab(EfdSQLType, MovFatID,
          MovFatNum, MovimCod, Empresa, (*Controle*)EfdInnNFsCab,
          IsLinked, SqLinked, Terceiro, CI, FTpEntrd, RegrFiscal, Pecas,
          PesoKg, AreaM2, AreaP2, ValorT, Motorista, Placa, COD_MOD, COD_SIT,
          SER, NUM_DOC, CHV_NFE, NFeStatus, DT_DOC,DT_E_S, VL_DOC,
          Geral.FF0(IND_PGTO),
          VL_DESC, VL_ABAT_NT, VL_MERC,
          Geral.FF0(IND_FRT),
          VL_FRT, VL_SEG, VL_OUT_DA,
          VL_BC_ICMS, VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS,
          VL_COFINS, VL_PIS_ST, VL_COFINS_ST, NFe_FatID, NFe_FatNum, NFe_StaLnk,
          VSVmcWrn, VSVmcObs, VSVmcSeq, VSVmcSta, LnkTabName, LnkFldName)
        else
          Result := true;
        //
    if Result then
    begin
      //if (Empresa = CI) and (NFVP_FatID <> 0) and (NFVP_FatNum <> 0) (*and (NFVP_FatNum <> 0)*) then
      begin
        DataFiscal := Data;
        Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
        'AtrelaFatID', 'AtrelaFatNum', 'AtrelaStaLnk',
        'DataFiscal', 'FisRegCad'], [
        'FatID', 'FatNum', 'Empresa'], [
        MovFatID, MovFatNum, NFe_StaLnk,
        DataFiscal, RegrFiscal], [
        NFe_FatID, NFe_FatNum, Empresa], False);
      end;
    end;

  end;
  //
  function InserePQE(): Boolean;
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpdW, ImgTipo.SQLType, 'pqe', False, [
        'Data', 'IQ', 'CI',
        'Transportadora', 'NF', 'Frete',
        'PesoB', 'PesoL', 'ValorNF',
        'RICMS', 'RICMSF', 'Conhecimento',
        'Pedido', 'DataE', 'Juros',
        'ICMS', 'TipoNF',
        'refNFe', 'modNF', 'Serie',
        'NF_RP', 'NF_CC', 'Empresa',
        'FreteRpICMS', 'FreteRpPIS', 'FreteRpCOFINS',
        'FreteRvICMS', 'FreteRvPIS', 'FreteRvCOFINS',
        'modRP', 'modCC', 'SerRP', 'SerCC',
        'NFe_RP', 'NFe_CC', 'Cte_Id',
        'CTe_mod', 'CTe_serie',
        'NFVP_FatID', 'NFVP_FatNum', 'NFVP_StaLnk',
        'NFRP_FatID', 'NFRP_FatNum', 'NFRP_StaLnk',
        'NFCC_FatID', 'NFCC_FatNum', 'NFCC_StaLnk',
        'IND_PGTO', 'IND_FRT',
        'ICMSTot_vProd', 'ICMSTot_vFrete', 'ICMSTot_vSeg',
        'ICMSTot_vDesc', 'ICMSTot_vOutro', 'IPI',
        'RegrFiscal', 'CondicaoPg', 'MadeBy'], [
        'Codigo'], [
        Data, IQ, CI,
        Transportadora, NumNF, Frete,
        PesoB, PesoL, ValorNF,
        RICMS, RICMSF, Conhecimento,
        Pedido, DataE, Juros,
        ICMS, TipoNF,
        refNFe, modNF, Serie,
        NF_RP, NF_CC, Empresa,
        FreteRpICMS, FreteRpPIS, FreteRpCOFINS,
        FreteRvICMS, FreteRvPIS, FreteRvCOFINS,
        modRP, modCC, SerRP, SerCC,
        NFe_RP, NFe_CC, Cte_Id,
        CTe_mod, CTe_serie,
        NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
        NFRP_FatID, NFRP_FatNum, NFRP_StaLnk,
        NFCC_FatID, NFCC_FatNum, NFCC_StaLnk,
        IND_PGTO, IND_FRT,
        ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg,
        ICMSTot_vDesc, ICMSTot_vOutro, IPI,
        RegrFiscal, CondicaoPg, MadeBy], [
        Codigo], True);
        FPQE := Codigo;
  end;
var
  _FatID, _FatNum, _CodInfoEmit: Integer;
  SQLIdx: String;
  SQLType: TSQLType;
begin
  //
  Controle := 0;
  SQLType  := ImgTipo.SQLType;
  PQE_Codigo := EdCodigo.ValueVariant;
  //
  if UnPQx.ImpedePeloBalanco(TPEntrada.Date, True) then Exit;
  //
  SQLIdx := 'AND Codigo <> ' + Geral.FF0(PQE_Codigo);
  if SQLType = stIns then
  begin
    if SPED_PF.ImpedePeloID_da_NFe('pqe', 'refNFe', EdrefNFe.Text, SQLIdx) then Exit;
    if SPED_PF.ImpedePeloID_da_NFe('pqe', 'NFe_CC', EdNFe_CC.Text, SQLIdx) then Exit;
    if SPED_PF.ImpedePeloID_da_NFe('pqe', 'NFe_RP', EdNFe_RP.Text, SQLIdx) then Exit;
  end;
  //
  //
  if not DModG.ObtemEmpresaSelecionadaESuaFilial(EdEmpresa, Empresa, Filial) then
    Exit;
  //
  if CBFornecedor.KeyValue = Null then
    CBFornecedor.KeyValue := 0;
  //
  if CBTransportador.KeyValue = Null then
    CBTransportador.KeyValue := 0;
  //
  Data           := Geral.FDT(TPEntrada.Date, 1);
  IQ             := EdFornecedor.ValueVariant;
  CI             := EdCI.ValueVariant;
  Transportadora := EdTransportador.ValueVariant;
  Frete          := EdFrete.ValueVariant;
  PesoB          := EdkgBruto.ValueVariant;
  PesoL          := EdkgLiquido.ValueVariant;
  ValorNF        := EdValorNF.ValueVariant;
  Conhecimento   := EdConhecim.ValueVariant;
  DataE          := Geral.FDT(TPEmissao.Date, 1);
  Juros          := EdJuros.ValueVariant;
  refNFe         := EdrefNFe.Text;
  //
  FreteRpICMS    := EdFreteRpICMS.ValueVariant;
  FreteRpPIS     := EdFreteRpPIS.ValueVariant;
  FreteRpCOFINS  := EdFreteRpCOFINS.ValueVariant;
  FreteRvICMS    := EdFreteRvICMS.ValueVariant;
  FreteRvPIS     := EdFreteRvPIS.ValueVariant;
  FreteRvCOFINS  := EdFreteRvCOFINS.ValueVariant;
  //
  RegrFiscal       := EdRegrFiscal.ValueVariant;
  CondicaoPg       := EdCondicaoPg.ValueVariant;
  UsaEntradaFiscal := DModG.QrCtrlGeralUsarEntraFiscal.Value = 1;
  MadeBy           := RGMadeBy.ItemIndex;
  //
  if MyObjects.FIC(RGMadeBy.ItemIndex < 1, RGMadeBy,
    'Informe o modo de fornecimento dos produtos!') then Exit;
  if MyObjects.FIC((TPEmissao.Date < 2), TPEmissao, 'Informe a data de emiss�o') then Exit;
  if MyObjects.FIC((RegrFiscal = 0) and UsaEntradaFiscal, EdRegrFiscal, 'Informe a regra fiscal') then Exit;
  if MyObjects.FIC(CI = 0, EdCI, 'Informe o cliente interno') then Exit;
  if MyObjects.FIC(CBFornecedor.KeyValue = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  if MyObjects.FIC((CI <> Empresa) and (CI <> IQ), EdFornecedor,
    'Quando o cliente interno n�o � a empresa o fornecedor deve ser o cliente interno !') then (*Exit*);
  //
  //
  NFVP_FatID     := EdNFVP_FatID.ValueVariant;
  NFVP_FatNum    := EdNFVP_FatNum.ValueVariant;
  NFVP_StaLnk    := EdNFVP_StaLnk.ValueVariant;
  NFRP_FatID     := EdNFRP_FatID.ValueVariant;
  NFRP_FatNum    := EdNFRP_FatNum.ValueVariant;
  NFRP_StaLnk    := EdNFRP_StaLnk.ValueVariant;
  NFCC_FatID     := EdNFCC_FatID.ValueVariant;
  NFCC_FatNum    := EdNFCC_FatNum.ValueVariant;
  NFCC_StaLnk    := EdNFCC_StaLnk.ValueVariant;
  _FatID         := 0;
  _FatNum        := 0;

  //
  {
  if ICMS > 99 then
  begin
    ShowMessage('O ICMS da N.F. � inv�lido.');
    EdICMS.SetFocus;
    Exit;
  end;
  if RICMS > 99 then
  begin
    ShowMessage('O retorno de ICMS da N.F. � inv�lido.');
    EdRICMS.SetFocus;
    Exit;
  end;
  if RICMSF > 99 then
  begin
    ShowMessage('O retorno de ICMS do frete � inv�lido.');
    EdRICMSF.SetFocus;
    Exit;
  end;
  }
  //
  //
  IND_FRT        := EdIND_FRT.ValueVariant;
  if IND_FRT <> 9 then// sem frete
    if MyObjects.FIC(Transportadora = 0, EdTransportador, 'Defina um transportador!') then Exit;
  //
  // NF de venda
  modNF := EdmodNF.ValueVariant;
  Serie := EdSerie.ValueVariant;
  NumNF  := EdNF.ValueVariant;
  PrecisaNF := (CI = Empresa) and (
    (refNFe = '') or ( (NumNF =  0) and (modNF = 0)));
  if MyObjects.FIC(UsaEntradaFiscal and PrecisaNF, EdrefNFe, 'Informe a chave da NF VP!') then Exit;
  //

  if MyObjects.FIC(NumNF = 0, EdNF, 'Informe a NF VP!') then Exit;

  // NF de Remessa para Produ��o (Industrializa��o)
  NFe_RP    := EdNFe_RP.ValueVariant;
  modRP     := EdmodRP.ValueVariant;
  SerRP     := EdSerRP.ValueVariant;
  NF_RP     := EdNF_RP.ValueVariant;
  PrecisaNF := (CI <> Empresa) and (
    (NFe_RP = '') or ( (NF_RP = 0) and (modRP = 0)));
  if MyObjects.FIC(UsaEntradaFiscal and PrecisaNF, EdNFe_RP, 'Informe a chave da NF RP!') then; // Exit;
  //

  // NF de Cobertura do Cliente
  NFe_CC    := EdNFe_CC.ValueVariant;
  modCC     := EdmodCC.ValueVariant;
  SerCC     := EdSerCC.ValueVariant;
  NF_CC     := EdNF_CC.ValueVariant;
  PrecisaNF := (CI <> Empresa) and (
    (NFe_CC = '') or ( (NF_CC = 0) and (modCC = 0)));
  if MyObjects.FIC(UsaEntradaFiscal and PrecisaNF, EdNFe_CC, 'Informe a chave da NF CC!') then Exit;
  //

  // CTe - Conhecimento de Frete
  Cte_Id    := EdCte_Id.ValueVariant;
  CTe_mod   := EdCTe_mod.ValueVariant;
  CTe_serie := EdCTe_serie.ValueVariant;

  IND_PGTO       := EdIND_PGTO.ValueVariant;
  IND_FRT        := EdIND_FRT.ValueVariant;

  ICMSTot_vProd  := EdICMSTot_vProd.ValueVariant;
  ICMSTot_vFrete := EdICMSTot_vFrete.ValueVariant;
  ICMSTot_vSeg   := EdICMSTot_vSeg.ValueVariant;
  ICMSTot_vDesc  := EdICMSTot_vDesc.ValueVariant;
  ICMSTot_vOutro := EdICMSTot_vOutro.ValueVariant;
  IPI            := EdIPI.ValueVariant;

  Codigo := EdCodigo.ValueVariant;
  //
  VerificaChaveNFeDeEntrada(Empresa, CI, refNFe, NFe_CC);
  //
  if (FVerPedi = 1) and (ImgTipo.SQLType = stIns) then
  begin
    if NFVP_FatNum <> 0 then
    begin
      _FatID  := NFVP_FatID;
      _FatNum := NFVP_FatNum;
      _CodInfoEmit := IQ;
      if not CadastroItensOK(_FatID, _FatNum, Empresa, _CodInfoEmit) then Exit;
    end else
    if NFCC_FatNum <> 0 then
    begin
      _FatID  := NFCC_FatID;
      _FatNum := NFCC_FatNum;
      _CodInfoEmit := IQ;
      if not CadastroItensOK(_FatID, _FatNum, Empresa, _CodInfoEmit) then Exit;
    end;
  end;

  if ImgTipo.SQLType = stIns then
  begin
    TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
    Pedido := Geral.IMV(EdPedido.Text);
    // Se for pelo PediVda:
    case FVerPedi of
      1: // sem pedido
      begin
        //ReopenPediVda(FPediVda);
        if InserePQE() then
        begin
          if (FNFe_FatNum <> 0) then
          begin
            ReopenCabA();
////////////////////////////////////////////////////////////////////////////////
            if InsAltEfdInnNFsCab() then
////////////////////////////////////////////////////////////////////////////////
            begin
              ReopenItsI();
              //
              QrItsI.First;
              try
                while not QrItsI.Eof do
                begin
                  MadeBy  := RGMadeBy.ItemIndex; // QrPediVdaMadeBy.Value;
                  OriPart := 0; //QrPediVdaItsControle.Value;
                  if InserePQEItsDeNFe(Codigo, FisRegGenCtbD, FisRegGenCtbC, MadeBy, OriPart) then
                    QrItsI.Next
                  else
                  begin
                    FmPQE.ExcluiTodaEntradaPvdNFeEmProgresso(Codigo);
                    Exit;
                  end
                end;
              except
                on E: Exception do
                begin
                  FmPQE.ExcluiTodaEntradaPvdNFeEmProgresso(Codigo);
                  Geral.MB_Erro(E.Message);
                  FmPQE.LocCod(Codigo, Codigo);
                  Exit;
                end;
              end;
              // Contas a pagar
              //if (QrPediVdaFinanceiro.Value > 0) then
              begin
                if CondicaoPG <> 0 then
                begin
                  case QrPediPrzCabCondPg.Value of
                    1, // Adiantado
                    2: // � Vista
                    begin
                      //N�o tem pedido!
                      //FatParcRef := FPediVda;
                      //AtrelaPagamentoAntecipado(PQE_Codigo, FatParcRef, TabLctA);
                    end;
                    3: // � Prazo
                    begin
                      DataFat := TPEmissao.Date;
                      //
                      UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabY, Dmod.MyDB, [
                      'SELECT * ',
                      'FROM nfecaby ',
                      'WHERE FatID=' + Geral.FF0(FNFe_FatID),
                      'AND FatNum=' + Geral.FF0(FNFe_FatNum),
                      'AND Empresa=' + Geral.FF0(FNFe_Empresa),
                      'ORDER BY nDup ',
                      '']);
                      //
                      ValY := 0;
                      if QrNFeCabY.RecordCount > 0 then
                      begin
                        if QrNFeCabY.RecordCount > 1 then
                        begin
                          UnDmkDAC_PF.AbreMySQLQuery0(QrSumY, Dmod.MyDB, [
                          'SELECT SUM(vDup) vDup ',
                          'FROM nfecaby ',
                          'WHERE FatID=' + Geral.FF0(FNFe_FatID),
                          'AND FatNum=' + Geral.FF0(FNFe_FatNum),
                          'AND Empresa=' + Geral.FF0(FNFe_Empresa),
                          '']);
                          ValY := QrSumYvDup.Value;
                        end else
                          ValY := QrNFeCabYvDup.Value;
                      end;
                      (*
                      if ValY > 0 then
                        FormaInsLct := MyObjects.SelRadioGroup(
                        'Lan�amentos financeiro', '',
                        ['Pela Regra Fiscal ', 'Pela NF-e'], -1)
                      else
                        FormaInsLct := 0;*)
                      //
                      FormaInsLct := 1;
                      //
                      case FormaInsLct of
                        (*0:
                        begin
                          InsereLancamentosFinanceirosPvd(Empresa, Filial,
                          QrPediVdaFinanceiro.Value, ValorNF, NumNF, CondicaoPG,
                          QrPediVdaGenCtbD.Value, QrPediVdaGenCtbC.Value, Serie,
                          NumNF, IQ, PQE_Codigo, DataFat);
                        end;*)
                        1:
                        begin
                          InsereLancamentosFinanceirosNFe(Empresa, Filial,
                          QrFisRegCadFinanceiro.Value,
                          QrCabAICMSTot_vNF.Value,
                          QrCabAide_nNF.Value,
                          CondicaoPG,
                          QrFisRegCadGenCtbD.Value, QrFisRegCadGenCtbC.Value,
                          //Serie, NumNF,
                          QrCabAide_serie.Value, QrCabAide_nNF.Value,
                          //IQ,
                          QrCabACodInfoEmit.Value,
                          PQE_Codigo, DataFat);
                          //
                        end;
                      end;
                    end;
                  end;
                end;
                // fim contas � pagar
              end;
            end;
          end;
        end;
      end;
      2:
      begin
        if InserePQE() then
        begin
          Dmod.QrUpdM.SQL.Clear;
          Dmod.QrUpdM.SQL.Add('INSERT INTO PQEIts SET Insumo=:P0, Volumes=:P1, ');
          Dmod.QrUpdM.SQL.Add('PesoVB=:P2, PesoVL=:P3, ValorItem=:P4, IPI=:P5, ');
          Dmod.QrUpdM.SQL.Add('RIPI=:P6, TotalCusto=:P7, TotalPeso=:P8, ');
          Dmod.QrUpdM.SQL.Add('CFin=:P9, Codigo=:P10, Conta=:11, Controle=:P12');
          if Pedido > 0 then
          begin
            QrItens.Close;
            QrItens.Params[0].AsInteger := Pedido;
            UnDmkDAC_PF.AbreQuery(QrItens, Dmod.MyDB);
            Conta := 0;
            while not QrItens.Eof do
            begin
              ValorItem := QrItensValorItem.Value * QrItensVolumes.Value *
                            QrItensPesoVL.Value / (1 + (QrItensIPI.Value / 100));
              Conta := Conta + 1;
              Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                'PQEIts','PQEIts','Controle');
              //
              Dmod.QrUpdM.Params[00].AsInteger := QrItensInsumo.Value;
              Dmod.QrUpdM.Params[01].AsInteger := QrItensVolumes.Value;
              Dmod.QrUpdM.Params[02].AsFloat   := QrItensPesoVB.Value;
              Dmod.QrUpdM.Params[03].AsFloat   := QrItensPesoVL.Value;
              Dmod.QrUpdM.Params[04].AsFloat   := ValorItem;
              Dmod.QrUpdM.Params[05].AsFloat   := QrItensIPI.Value;
              Dmod.QrUpdM.Params[06].AsFloat   := 0;
              Dmod.QrUpdM.Params[07].AsFloat   := 0;
              Dmod.QrUpdM.Params[08].AsFloat   := QrItensVolumes.Value*QrItensPesoVL.Value;
              Dmod.QrUpdM.Params[09].AsFloat   := QrItensCFin.Value;
              Dmod.QrUpdM.Params[10].AsInteger := PQE_Codigo;
              Dmod.QrUpdM.Params[11].AsInteger := Conta;
              Dmod.QrUpdM.Params[12].AsInteger := Controle;
              Dmod.QrUpdM.ExecSQL;
              QrItens.Next;
            end;
            FmPQE.FControle := Controle;
            QrItens.Close;
            FmPQE.SubQuery1Reopen;
            Dmod.QrUpdW.DataBase := Dmod.MyDB;
            Dmod.QrUpdW.Close;
            Dmod.QrUpdW.SQL.Clear;
            Dmod.QrUpdW.SQL.Add('UPDATE PQPed SET Sit=:P0');
            Dmod.QrUpdW.SQL.Add('WHERE Codigo=:P1');
            Dmod.QrUpdW.Params[0].AsInteger := Geral.IMV(LaSitPedido.Caption);
            Dmod.QrUpdW.Params[1].AsInteger := Pedido;
            Dmod.QrUpdW.ExecSQL;
            // Parei Aqui !!!  Falta dar baixa (entrada!) do produto no pedido
          end;
        end;
      end;
      3: //
      begin
        if FPediVda = Pedido then
        begin
          ReopenPediVda(FPediVda);
          //ReopenCabA();
          if InserePQE() then
          begin
    ////////////////////////////////////////////////////////////////////////////////
            if InsAltEfdInnNFsCab() then
    ////////////////////////////////////////////////////////////////////////////////
            begin
              ReopenPQEItsDePediVda_All(FPediVda);
              //Fornecedor := QrPediVdaCliente.Value;
              // Intens
              QrPediVdaIts.First;
              try
                while not QrPediVdaIts.Eof do
                begin
                  if InserePQEItsDePediVda(Codigo) then
                    QrPediVdaIts.Next
                  else
                  begin
                    FmPQE.ExcluiTodaEntradaPediVdaEmProgresso(Codigo);
                    Exit;
                  end
                end;
              except
                on E: Exception do
                begin
                  FmPQE.ExcluiTodaEntradaPediVdaEmProgresso(Codigo);
                  Geral.MB_Erro(E.Message);
                  FmPQE.LocCod(Codigo, Codigo);
                  Exit;
                end;
              end;
              //
              // Contas a pagar
              if (QrPediVdaFinanceiro.Value > 0) then
              begin
                if CondicaoPG <> 0 then
                begin
                  case QrPediPrzCabCondPg.Value of
                    1, // Adiantado
                    2: // � Vista
                    begin
                      FatParcRef := FPediVda;
                      AtrelaPagamentoAntecipado(PQE_Codigo, FatParcRef, TabLctA);
                    end;
                    3: // � Prazo
                    begin
                      DataFat := TPEmissao.Date;
                      //
                      InsereLancamentosFinanceirosPvd(Empresa, Filial,
                      QrPediVdaFinanceiro.Value, ValorNF, NumNF, CondicaoPG,
                      QrPediVdaGenCtbD.Value, QrPediVdaGenCtbC.Value, Serie,
                      NumNF, IQ, PQE_Codigo, DataFat);
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
      4: //
      begin
        if FPediVda = Pedido then
        begin
          ReopenPediVda(FPediVda);
          ReopenCabA();
          if InserePQE() then
          begin
    ////////////////////////////////////////////////////////////////////////////////
            if InsAltEfdInnNFsCab() then
    ////////////////////////////////////////////////////////////////////////////////
            begin
              UnDmkDAC_PF.AbreMySQLQuery0(QrItsI, Dmod.MyDB, [
              'SELECT nfi.*,  ',
              '  nfi.prod_vProd + ',
              '  nfi.prod_vFrete + ',
              '  nfi.prod_vSeg - ',
              '  nfi.prod_vDesc + ',
              '  nfi.prod_vOutro',
              '    prod_vTotItm',
              'FROM nfeitsi nfi',
              'WHERE nfi.FatID=' + Geral.FF0(FNFe_FatID),
              'AND nfi.FatNum=' + Geral.FF0(FNFe_FatNum),
              'AND nfi.Empresa=' + Geral.FF0(FNFe_Empresa),
              'ORDER BY nItem ',
              '']);
              QrItsI.First;
              try
                while not QrItsI.Eof do
                begin
                  //
                  // Parei aqui!
                  // ver aqui! N�o tem precis�o na procura!
                  ReopenPQEItsDePediVda_Uni(FPediVda,
                    QrItsIprod_qCom.Value,
                    QrItsIprod_vUnCom.Value);
                  OriPart := QrPediVdaItsControle.Value;
                  // fim ver aqui!
                  //
                  MadeBy  := QrPediVdaMadeBy.Value;
                  if InserePQEItsDeNFe(Codigo, FisRegGenCtbD, FisRegGenCtbC,
                  MadeBy, OriPart) then
                    QrItsI.Next
                  else
                  begin
                    FmPQE.ExcluiTodaEntradaPvdNFeEmProgresso(Codigo);
                    Exit;
                  end
                end;
              except
                on E: Exception do
                begin
                  FmPQE.ExcluiTodaEntradaPvdNFeEmProgresso(Codigo);
                  Geral.MB_Erro(E.Message);
                  FmPQE.LocCod(Codigo, Codigo);
                  Exit;
                end;
              end;
              // Contas a pagar
              if (QrPediVdaFinanceiro.Value > 0) then
              begin
                if CondicaoPG <> 0 then
                begin
                  case QrPediPrzCabCondPg.Value of
                    1, // Adiantado
                    2: // � Vista
                    begin
                      FatParcRef := FPediVda;
                      AtrelaPagamentoAntecipado(PQE_Codigo, FatParcRef, TabLctA);
                    end;
                    3: // � Prazo
                    begin
                      DataFat := TPEmissao.Date;
                      //
                      UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabY, Dmod.MyDB, [
                      'SELECT * ',
                      'FROM nfecaby ',
                      'WHERE FatID=' + Geral.FF0(FNFe_FatID),
                      'AND FatNum=' + Geral.FF0(FNFe_FatNum),
                      'AND Empresa=' + Geral.FF0(FNFe_Empresa),
                      'ORDER BY nDup ',
                      '']);
                      //
                      ValY := 0;
                      if QrNFeCabY.RecordCount > 0 then
                      begin
                        if QrNFeCabY.RecordCount > 1 then
                        begin
                          UnDmkDAC_PF.AbreMySQLQuery0(QrSumY, Dmod.MyDB, [
                          'SELECT SUM(vDup) vDup ',
                          'FROM nfecaby ',
                          'WHERE FatID=' + Geral.FF0(FNFe_FatID),
                          'AND FatNum=' + Geral.FF0(FNFe_FatNum),
                          'AND Empresa=' + Geral.FF0(FNFe_Empresa),
                          '']);
                          ValY := QrSumYvDup.Value;
                        end else
                          ValY := QrNFeCabYvDup.Value;
                      end;
                      if ValY > 0 then
                        FormaInsLct := MyObjects.SelRadioGroup(
                        'Lan�amentos financeiro', '',
                        ['Pela Regra Fiscal ', 'Pela NF-e'], -1)
                      else
                        FormaInsLct := 0;
                      //
                      case FormaInsLct of
                        0:
                        begin
                          InsereLancamentosFinanceirosPvd(Empresa, Filial,
                          QrPediVdaFinanceiro.Value, ValorNF, NumNF, CondicaoPG,
                          QrPediVdaGenCtbD.Value, QrPediVdaGenCtbC.Value, Serie,
                          NumNF, IQ, PQE_Codigo, DataFat);
                        end;
                        1:
                        begin
                          InsereLancamentosFinanceirosNFe(Empresa, Filial,
                          QrPediVdaFinanceiro.Value,
                          QrCabAICMSTot_vNF.Value,
                          QrCabAide_nNF.Value,
                          CondicaoPG,
                          QrPediVdaGenCtbD.Value, QrPediVdaGenCtbC.Value,
                          //Serie, NumNF,
                          QrCabAide_serie.Value, QrCabAide_nNF.Value,
                          //IQ,
                          QrCabACodInfoEmit.Value,
                          PQE_Codigo, DataFat);
                          //
                        end;
                      end;
                    end;
                  end;
                end;
                // fim contas � pagar
              end;
            end;
          end;
        end;
      end;
    end;
  end else
  begin
   if UMyMod.SQLInsUpd(Dmod.QrUpdW, stUpd, 'pqe', False, [
    'Data', 'IQ', 'CI',
    'Transportadora', 'NF', 'Frete',
    'PesoB', 'PesoL', 'ValorNF',
    'RICMS', 'RICMSF', 'Conhecimento',
    (*'Pedido',*) 'DataE', 'Juros',
    'ICMS', 'TipoNF',
    'refNFe', 'modNF', 'Serie',
    'NF_RP', 'NF_CC',
    'FreteRpICMS', 'FreteRpPIS', 'FreteRpCOFINS',
    'FreteRvICMS', 'FreteRvPIS', 'FreteRvCOFINS',
    'modRP', 'modCC', 'SerRP', 'SerCC',
    'NFe_RP', 'NFe_CC', 'Cte_Id',
    'CTe_mod', 'CTe_serie',
    'NFVP_FatID', 'NFVP_FatNum', 'NFVP_StaLnk',
    'NFRP_FatID', 'NFRP_FatNum', 'NFRP_StaLnk',
    'NFCC_FatID', 'NFCC_FatNum', 'NFCC_StaLnk',
    'IND_PGTO', 'IND_FRT',
    'ICMSTot_vProd', 'ICMSTot_vFrete', 'ICMSTot_vSeg',
    'ICMSTot_vDesc', 'ICMSTot_vOutro', 'IPI',
    'MadeBy'], [
    'Codigo'], [
    Data, IQ, CI,
    Transportadora, NumNF, Frete,
    PesoB, PesoL, ValorNF,
    RICMS, RICMSF, Conhecimento,
    (*Pedido,*) DataE, Juros,
    ICMS, TipoNF,
    refNFe, modNF, Serie,
    NF_RP, NF_CC,
    FreteRpICMS, FreteRpPIS, FreteRpCOFINS,
    FreteRvICMS, FreteRvPIS, FreteRvCOFINS,
    modRP, modCC, SerRP, SerCC,
    NFe_RP, NFe_CC, Cte_Id,
    CTe_mod, CTe_serie,
    NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
    NFRP_FatID, NFRP_FatNum, NFRP_StaLnk,
    NFCC_FatID, NFCC_FatNum, NFCC_StaLnk,
    IND_PGTO, IND_FRT,
    ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg,
    ICMSTot_vDesc, ICMSTot_vOutro, IPI,
    MadeBy], [
    Codigo], True) then
////////////////////////////////////////////////////////////////////////////////
    if InsAltEfdInnNFsCab() then
////////////////////////////////////////////////////////////////////////////////
    begin
      //
    end;
  end;
  FmPQE.ImgTipo.SQLType := ImgTipo.SQLType;
  Close;
end;

procedure TFmPQENew.FormCreate(Sender: TObject);
var
  MinData: TDate;
begin
  ImgTipo.SQLType := stLok;
  PnDados3.Align  := alClient;
  FDesiste        := False;
  FUsouNFe        := False;
  FPQE            := 0;
  FSqLinked       := 0;
  FNFe_FatID      := 0;
  FNFe_FatNum     := 0;
  FNFe_Empresa    := 0;
  FCodInfoEmit    := 0;
  //FRegrFiscal     := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransportadores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
  ReopenPediPrzCab();
  F_IND_PGTO_EFD := UnNFe_PF.ListaIND_PAG_EFD();
  F_IND_FRT_EFD  := UnNFe_PF.ListaIND_FRT_EFD();
  //
  if Dmod.QrControleCanAltBalA.Value = 0 then
  begin
    MinData        := StrToDate(dmkPF.PrimeiroDiaAposPeriodo(UnPQx.VerificaBalanco, dtSystem3));
    TPEntrada.Date := Date;
    //
    if TPEntrada.Date < MinData then
      TPENtrada.Date := MinData;
    //
    TPEntrada.MinDate := MinData;
  end;
end;

procedure TFmPQENew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if ImgTipo.SQLType = stUpd then
    BtConfirma.Setfocus
  else
    SetaFocusIni();
end;

{
function TFmPQENew.CadastroItensOK(const FatID, FatNum, Empresa, CodInfoEmit: Integer): Boolean;
const
  sProcName = 'TFmPQENew.CadastroItensOK()';
var
  prod_cProd, prod_xProd, prod_cEAN, prod_NCM, prod_uCom, infAdProd,
  prod_EXTIPI, IPI_cEnq: String;
  prod_CFOP, ICMS_Orig, ICMS_CST, IPI_CST, PIS_CST, COFINS_CST, Nivel1: Integer;
  //
  GraGruEIts_NomeGGX: String;
  GraGruEIts_GraGruX: Integer;
  //
  ItensTot, ItensOkA, ItensOkB: Integer;
  EhServico: Boolean;
  RegrFiscal, CRT_Emitido, CST_A_Emitido, CST_B_Emitido, CSOSN_Emitido,
  CFOP_Emitido: Integer;
  CFOP_Entrada: String; (*var ICMS_Aliq: Double;*)
  CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer;
  InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST: String;
  OriTES, EFD_II_C195, GenCtbD, GenCtbC: Integer;
  TES_ICMS, TES_IPI, TES_PIS, TES_COFINS, PVD_MadeBy: Integer;
  ICMSAliq, PISAliq, COFINSAliq: Double;
  FisRegGenCtbD, FisRegGenCtbC: Integer;
begin
  Result := False;
  //
  ReopenCabA();
  ReopenItsI();
  //
  if QrItsI.RecordCount = 0 then
  begin
    Geral.MB_Aviso('Nenhum item foi lozalizado na NFe baixada');
    Exit;
  end;
  // ver se est�o todos produtos atrelados!
  ItensTot     := QrItsI.RecordCount;
  ItensOkA     := 0;
  ItensOkB     := 0;
  while not QrItsI.Eof do
  begin
    ReopenItsN();
    //
    prod_cProd   := QrItsIprod_cProd.Value;
    prod_xProd   := QrItsIprod_xProd.Value;
    prod_cEAN    := QrItsIprod_cEAN.Value;
    prod_NCM     := QrItsIprod_NCM.Value;
    prod_uCom    := QrItsIprod_uCom.Value;
    infAdProd    := QrItsVinfAdProd.Value;
    prod_CFOP    := QrItsIprod_CFOP.Value;
    ICMS_Orig    := QrItsNICMS_Orig.Value;
    ICMS_CST     := QrItsNICMS_CST.Value;
    IPI_CST      := QrItsOIPI_CST.Value;
    PIS_CST      := QrItsQPIS_CST.Value;
    COFINS_CST   := QrItsSCOFINS_CST.Value;
    prod_EXTIPI  := QrItsIprod_EXTIPI.Value;
    IPI_cEnq     := QrItsOIPI_cEnq.Value;
    //
    DmNFe_0000.QrCod.Close;
    DmNFe_0000.QrCod.Params[00].AsInteger := CodInfoEmit;
    DmNFe_0000.QrCod.Params[01].AsString  := Trim(prod_cProd);
    UMyMod.AbreQuery(DmNFe_0000.QrCod, Dmod.MyDB, sProcName);
    //
    Nivel1 := 0;
    case DmNFe_0000.QrCod.RecordCount of
      0:
      begin
        //if CO_DMKID_APP <> 2 then //Bluederm => O Marco n�o quer que configure o produto
        //begin
          VAR_NOME_NOVO_GG1 := prod_xProd;
          Grade_Jan.MostraFormGraGruEIts(stIns, CodInfoEmit,
            prod_cProd, prod_xProd, prod_cEAN, prod_NCM, prod_uCom,
            infAdProd, prod_CFOP, ICMS_Orig, ICMS_CST, IPI_CST,
            PIS_CST, COFINS_CST, prod_EXTIPI, IPI_cEnq,
            GraGruEIts_GraGruX, GraGruEIts_NomeGGX);
          //
          VAR_NOME_NOVO_GG1 := EmptyStr;
          //
          DmNFe_0000.QrCod.Close;
          DmNFe_0000.QrCod.Params[00].AsInteger := CodInfoEmit;
          DmNFe_0000.QrCod.Params[01].AsString  := Trim(prod_cProd);
          UMyMod.AbreQuery(DmNFe_0000.QrCod, Dmod.MyDB, sProcName);
          //
          Nivel1 := DmNFe_0000.QrCodNivel1.Value;
        //end else
          //Nivel1 := 0;
      end;
      else
      begin
        Nivel1 := DmNFe_0000.QrCodNivel1.Value;
      end;
    end;
    if Nivel1 <> 0 then
    begin
      ItensOkA := ItensOkA + 1;
      //
      if GraGruEIts_GraGruX <> 0 then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE nfeitsi ',
        'SET GraGruX=' + Geral.FF0(GraGruEIts_GraGruX),
        ', Nivel1=' + Geral.FF0(Nivel1),
        'WHERE FatID=' + Geral.FF0(FatID),
        'AND FatNum=' + Geral.FF0(FatNum),
        'AND Empresa=' + Geral.FF0(Empresa),
        'AND nItem=' + Geral.FF0(QrItsInItem.Value),
        'AND FatID>1', // evitar erro nas NFes emitidas!
        '']);
      end;
    end;

    /////////////  CFOP ////////////////////////////////////////////////////////

    RegrFiscal       := EdRegrFiscal.ValueVariant;
    EhServico        := QrItsIEhServico.Value = 1;
    CFOP_Emitido     := QrItsIprod_CFOP.Value;
    CRT_Emitido      := QrCabAemit_CRT.Value;
    CST_A_Emitido    := QrItsNICMS_Orig.Value;
    CST_B_Emitido    := QrItsNICMS_CST.Value;
    CSOSN_Emitido    := QrItsNICMS_CSOSN.Value;
    PVD_MadeBy       := RGMadeBy.ItemIndex;

    //
    Result := DmNFe_0000.ObtemNumeroCFOP_Entrada(
    (*Tabela_FatPedFis: String; Parametros: array of integer;*)
    Empresa, CodInfoEmit, PVD_MadeBy, RegrFiscal, EhServico,
    CRT_Emitido, CST_A_Emitido, CST_B_Emitido, CSOSN_Emitido,
    Geral.FormataCFOP(Geral.FF0(CFOP_Emitido)),
    CFOP_Entrada, (*var ICMS_Aliq: Double;*)
    CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib,
    InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST,
    OriTES, EFD_II_C195, GenCtbD, GenCtbC,
    TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
    ICMSAliq, PISAliq, COFINSAliq, FisRegGenCtbD, FisRegGenCtbC);
    if Result then
    begin
      ItensOkB := ItensOkB + 1;
    end;
    //
    QrItsI.Next;
  end;
  //
  // ...
  Result := (ItensTot = ItensOkA) and (ItensTot = ItensOkB);
  if ItensTot <> ItensOkA then
    Geral.MB_Aviso('N�o foi poss�vel verificar todos itens da NFe X cadastro!');
  if ItensTot <> ItensOkB then
    Geral.MB_Aviso('N�o foi poss�vel verificar os dados fiscais de todos itens da NFe!');
end;
}

function TFmPQENew.CadastroItensOK(const FatID, FatNum, Empresa, CodInfoEmit: Integer): Boolean;
const
  sProcName = 'TFmPQENew.CadastroItensOK()';
var
  prod_cProd, prod_xProd, prod_cEAN, prod_NCM, prod_uCom, infAdProd,
  prod_EXTIPI, IPI_cEnq: String;
  prod_CFOP, ICMS_Orig, ICMS_CST, IPI_CST, PIS_CST, COFINS_CST, Nivel1: Integer;
  //
  GraGruEIts_NomeGGX: String;
  GraGruEIts_GraGruX: Integer;
  //
  ItensTot, ItensOkA, ItensOkB: Integer;
  EhServico: Boolean;
  RegrFiscal, CRT_Emitido, CST_A_Emitido, CST_B_Emitido, CSOSN_Emitido,
  CFOP_Emitido: Integer;
  CFOP_Entrada: String; (*var ICMS_Aliq: Double;*)
  CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer;
  InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST: String;
  OriTES, EFD_II_C195, GenCtbD, GenCtbC: Integer;
  TES_ICMS, TES_IPI, TES_PIS, TES_COFINS, PVD_MadeBy,
  TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS: Integer;
  ICMSAliq, PISAliq, COFINSAliq: Double;
  FisRegGenCtbD, FisRegGenCtbC: Integer;
begin
  Result := False;
  //
  ReopenCabA();
  ReopenItsI();
  //
  if QrItsI.RecordCount = 0 then
  begin
    Geral.MB_Aviso('Nenhum item foi lozalizado na NFe baixada');
    Exit;
  end;
  // ver se est�o todos produtos atrelados!
  ItensTot     := QrItsI.RecordCount;
  ItensOkA     := 0;
  ItensOkB     := 0;
  GraGruEIts_GraGruX := 0;
  Nivel1 := 0;
  //
  while not QrItsI.Eof do
  begin
    ReopenItsN();
    //
    prod_cProd   := QrItsIprod_cProd.Value;
    prod_xProd   := QrItsIprod_xProd.Value;
    prod_cEAN    := QrItsIprod_cEAN.Value;
    prod_NCM     := QrItsIprod_NCM.Value;
    prod_uCom    := QrItsIprod_uCom.Value;
    infAdProd    := QrItsVinfAdProd.Value;
    prod_CFOP    := QrItsIprod_CFOP.Value;
    ICMS_Orig    := QrItsNICMS_Orig.Value;
    ICMS_CST     := QrItsNICMS_CST.Value;
    IPI_CST      := QrItsOIPI_CST.Value;
    PIS_CST      := QrItsQPIS_CST.Value;
    COFINS_CST   := QrItsSCOFINS_CST.Value;
    prod_EXTIPI  := QrItsIprod_EXTIPI.Value;
    IPI_cEnq     := QrItsOIPI_cEnq.Value;
    //
    DmNFe_0000.QrCod.Close;
    DmNFe_0000.QrCod.Params[00].AsInteger := CodInfoEmit;
    DmNFe_0000.QrCod.Params[01].AsString  := Trim(prod_cProd);
    UMyMod.AbreQuery(DmNFe_0000.QrCod, Dmod.MyDB, sProcName);
    //
    Nivel1 := 0;
    case DmNFe_0000.QrCod.RecordCount of
      0:
      begin
        //if CO_DMKID_APP <> 2 then //Bluederm => O Marco n�o quer que configure o produto
        //begin
          VAR_NOME_NOVO_GG1 := prod_xProd;
          Grade_Jan.MostraFormGraGruEIts(stIns, CodInfoEmit,
            prod_cProd, prod_xProd, prod_cEAN, prod_NCM, prod_uCom,
            infAdProd, prod_CFOP, ICMS_Orig, ICMS_CST, IPI_CST,
            PIS_CST, COFINS_CST, prod_EXTIPI, IPI_cEnq,
            GraGruEIts_GraGruX, GraGruEIts_NomeGGX);
          //
          VAR_NOME_NOVO_GG1 := EmptyStr;
          //
          DmNFe_0000.QrCod.Close;
          DmNFe_0000.QrCod.Params[00].AsInteger := CodInfoEmit;
          DmNFe_0000.QrCod.Params[01].AsString  := Trim(prod_cProd);
          UMyMod.AbreQuery(DmNFe_0000.QrCod, Dmod.MyDB, sProcName);
          //
          Nivel1 := DmNFe_0000.QrCodNivel1.Value;
        //end else
          //Nivel1 := 0;
      end;
      else
      begin
        Nivel1 := DmNFe_0000.QrCodNivel1.Value;
        GraGruEIts_GraGruX  := DmNFe_0000.QrCodGraGruX.Value;
      end;
    end;
    if Nivel1 <> 0 then
    begin
      ItensOkA := ItensOkA + 1;
      //
      if GraGruEIts_GraGruX <> 0 then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE nfeitsi ',
        'SET GraGruX=' + Geral.FF0(GraGruEIts_GraGruX),
        ', Nivel1=' + Geral.FF0(Nivel1),
        'WHERE FatID=' + Geral.FF0(FatID),
        'AND FatNum=' + Geral.FF0(FatNum),
        'AND Empresa=' + Geral.FF0(Empresa),
        'AND nItem=' + Geral.FF0(QrItsInItem.Value),
        'AND FatID>1', // evitar erro nas NFes emitidas!
        '']);
      end;
    end;

    /////////////  CFOP ////////////////////////////////////////////////////////

    RegrFiscal       := EdRegrFiscal.ValueVariant;
    EhServico        := QrItsIEhServico.Value = 1;
    CFOP_Emitido     := QrItsIprod_CFOP.Value;
    CRT_Emitido      := QrCabAemit_CRT.Value;
    CST_A_Emitido    := QrItsNICMS_Orig.Value;
    CST_B_Emitido    := QrItsNICMS_CST.Value;
    CSOSN_Emitido    := QrItsNICMS_CSOSN.Value;
    PVD_MadeBy       := RGMadeBy.ItemIndex;

    //
    Result := DmNFe_0000.ObtemNumeroCFOP_Entrada(
    (*Tabela_FatPedFis: String; Parametros: array of integer;*)
    Empresa, CodInfoEmit, PVD_MadeBy, RegrFiscal, EhServico,
    CRT_Emitido, CST_A_Emitido, CST_B_Emitido, CSOSN_Emitido,
    Geral.FormataCFOP(Geral.FF0(CFOP_Emitido)),
    CFOP_Entrada, (*var ICMS_Aliq: Double;*)
    CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib,
    InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST,
    OriTES, EFD_II_C195, GenCtbD, GenCtbC,
    TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
    TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS,
    ICMSAliq, PISAliq, COFINSAliq, FisRegGenCtbD, FisRegGenCtbC);
    if Result then
    begin
      ItensOkB := ItensOkB + 1;
    end;
    //
    QrItsI.Next;
  end;
  //
  // ...
  Result := (ItensTot = ItensOkA) and (ItensTot = ItensOkB);
  if ItensTot <> ItensOkA then
    Geral.MB_Aviso('N�o foi poss�vel verificar todos itens da NFe X cadastro!');
  if ItensTot <> ItensOkB then
    Geral.MB_Aviso('N�o foi poss�vel verificar os dados fiscais de todos itens da NFe!');
end;

procedure TFmPQENew.CalculaRetornoImpostosFrete(Qual: TRetornImpost);
const
  sProcName = 'FmPQENew.CalculaRetornoImpostosFrete()';
var
  Frete, pICMS, pPIS, pCofins, vICMS, vPIS, vCofins: Double;
begin
  Frete   := EdFrete.ValueVariant;
  pICMS   := EdFreteRpICMS.ValueVariant;
  pPIS    := EdFreteRpPIS.ValueVariant;
  pCofins := EdFreteRpCofins.ValueVariant;
  vICMS   := Geral.RoundC(Frete * pICMS / 100, 2);
  vPIS    := Geral.RoundC(Frete * pPIS / 100, 2);
  vCofins := Geral.RoundC(Frete * pCOFINS / 100, 2);
  case Qual of
    //retimpostIndef=1,
    //retimpostNenhum: Exit;
    retimpostTodos:
    begin
      EdFreteRvICMS.ValueVariant   := vICMS;
      EdFreteRvPIS.ValueVariant    := vPIS;
      EdFreteRvCOFINS.ValueVariant := vCOFINS;
    end;
    retimpostICMS: EdFreteRvICMS.ValueVariant     := vICMS;
    retimpostPIS: EdFreteRvPIS.ValueVariant       := vPIS;
    retimpostCOFINS: EdFreteRvCOFINS.ValueVariant := vCOFINS;
    //retimpostIPI=7);
    else Geral.MB_Erro('TRetornImpost n�o implementado em ' + sProcName);
  end;
end;

procedure TFmPQENew.CalculaTotalNF(Sender: TObject);
var
  ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vOutro,
  IPI, ValorNF: Double;
begin
  ICMSTot_vProd  := EdICMSTot_vProd.ValueVariant;
  ICMSTot_vFrete := EdICMSTot_vFrete.ValueVariant;
  ICMSTot_vSeg   := EdICMSTot_vSeg.ValueVariant;
  ICMSTot_vDesc  := EdICMSTot_vDesc.ValueVariant;
  ICMSTot_vOutro := EdICMSTot_vOutro.ValueVariant;
  IPI            := EdIPI.ValueVariant;
  //
  ValorNF := ICMSTot_vProd + ICMSTot_vFrete + ICMSTot_vSeg - ICMSTot_vDesc + ICMSTot_vOutro + IPI;
  EdValorNF.ValueVariant := ValorNF;
end;

procedure TFmPQENew.CkTipoNFClick(Sender: TObject);
var
  EhNFe: Boolean;
begin
  EhNFe := CkTipoNF.Checked;
  //
  LarefNFe.Enabled := EhNFe;
  EdrefNFe.Enabled := EhNFe;

  LaNFe_RP.Enabled := EhNFe;
  EdNFe_RP.Enabled := EhNFe;

  LaNFe_CC.Enabled := EhNFe;
  EdNFe_CC.Enabled := EhNFe;



  LamodNF.Enabled := not EhNFe;
  EdmodNF.Enabled := not EhNFe;
  LaSerie.Enabled := not EhNFe;
  EdSerie.Enabled := not EhNFe;
  LaNF.Enabled    := not EhNFe;
  EdNF.Enabled    := not EhNFe;

  LamodRP.Enabled := not EhNFe;
  EdmodRP.Enabled := not EhNFe;
  LaSerRP.Enabled := not EhNFe;
  EdSerRP.Enabled := not EhNFe;
  LaNF_RP.Enabled := not EhNFe;
  EdNF_RP.Enabled := not EhNFe;

  LamodCC.Enabled := not EhNFe;
  EdmodCC.Enabled := not EhNFe;
  LaSerCC.Enabled := not EhNFe;
  EdSerCC.Enabled := not EhNFe;
  LaNF_CC.Enabled := not EhNFe;
  EdNF_CC.Enabled := not EhNFe;

end;

procedure TFmPQENew.DefineDFe_Atrela(const TipoNFeEntrada: TTipoNFeEntrada);
const
  sProcName = 'TFmPQENew.DefineDFe_Atrela()';
var
  Empresa, CI, IQ, ide_mod, ide_Serie, ide_nNF: Integer;
  SQL_CNPJ_CPF_Emit, SQL_CNPJ_CPF_Dest: String;
  Continua: Boolean;
  Ed_modCC: TdmkEdit;
  SQLIdx: String;
  SQLType: TSQLType;
begin
  Ed_modCC := nil;
  Continua := True;
  SQLType  := ImgTipo.SQLType;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  IQ := EdFornecedor.ValueVariant;
  CI := EdCI.ValueVariant;
  case TipoNFeEntrada of
    TTipoNFeEntrada.tneVP:
      if EMpresa <> CI then
        Continua := Geral.MB_Pergunta(
        'Para esta pesquisa o cliente interno deve ser o mesmo que a empresa!'
        + sLineBreak + 'Mas deseja pesquisar assim mesmo?') = ID_YES;
    TTipoNFeEntrada.tneCC:
      if Empresa = CI then
        Continua := Geral.MB_Pergunta(
        'Para esta pesquisa o cliente interno deve ser diferente da emprea!'
        + sLineBreak + 'Mas deseja pesquisar assim mesmo?') = ID_YES;
    TTipoNFeEntrada.tneRP:
      if Empresa <> CI then
        Continua := Geral.MB_Pergunta(
        'Para esta pesquisa o cliente interno deve ser diferente da emprea!'
        + sLineBreak + 'Mas deseja pesquisar assim mesmo?') = ID_YES;
  end;
  //
  if MyObjects.FIC(IQ = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  if MyObjects.FIC(CI = 0, EdCI, 'Informe o cliente interno') then Exit;
  if Continua then
  begin
  case TipoNFeEntrada of
    TTipoNFeEntrada.tneVP:
    begin
      case QrFornecedoresTipo.Value of
        0: SQL_CNPJ_CPF_Emit := 'AND emit_CNPJ="' + Geral.SoNumero_TT(QrFornecedoresCNPJ.Value) + '"';
        1: SQL_CNPJ_CPF_Emit := 'AND emit_CPF="' + Geral.SoNumero_TT(QrFornecedoresCPF.Value) + '"';
        else SQL_CNPJ_CPF_Emit := 'AND emit_CPF_CPF=????';
      end;
      //
      case QrCITipo.Value of
        0: SQL_CNPJ_CPF_Dest := 'AND dest_CNPJ="' + Geral.SoNumero_TT(QrCICNPJ.Value) + '"';
        1: SQL_CNPJ_CPF_Dest := 'AND dest_CPF="' + Geral.SoNumero_TT(QrCICPF.Value) + '"';
        else SQL_CNPJ_CPF_Dest := 'AND dest_CPF_CPF=????';
      end;
    end;
    TTipoNFeEntrada.tneCC:
    begin
      case QrFornecedoresTipo.Value of
        0: SQL_CNPJ_CPF_Emit := 'AND emit_CNPJ="' + Geral.SoNumero_TT(QrFornecedoresCNPJ.Value) + '"';
        1: SQL_CNPJ_CPF_Emit := 'AND emit_CPF="' + Geral.SoNumero_TT(QrFornecedoresCPF.Value) + '"';
        else SQL_CNPJ_CPF_Emit := 'AND emit_CPF_CPF=????';
      end;
      //
      case DModG.QrEmpresasTipo.Value of
        0: SQL_CNPJ_CPF_Dest := 'AND dest_CNPJ="' + Geral.SoNumero_TT(DModG.QrEmpresasCNPJ_CPF.Value) + '"';
        1: SQL_CNPJ_CPF_Dest := 'AND dest_CPF="' + Geral.SoNumero_TT(DModG.QrEmpresasCNPJ_CPF.Value) + '"';
        else SQL_CNPJ_CPF_Dest := 'AND dest_CPF_CPF=????';
      end;
    end;
    else
    begin
      Geral.MB_Aviso('Tipo de entrada n�o implementada em ' + sProcName);
    end;
    //
  end;
    //
    case TipoNFeEntrada of
      //tneND: //=0,
      tneVP:
      begin
        // NF de venda
        ide_mod   := EdmodNF.ValueVariant;
        ide_Serie := EdSerie.ValueVariant;
        ide_nNF   := EdNF.ValueVariant;
        Ed_modCC  := EdmodNF;
      end;
       //=1,
      tneRP: //=2,
      begin
        // NF de remessa produ��o
        ide_mod   := EdmodRP.ValueVariant;
        ide_Serie := EdSerRP.ValueVariant;
        ide_nNF   := EdNF_RP.ValueVariant;
        Ed_modCC  := EdModRP;
      end;
      tneCC: //=3)
      begin
        // NF de remessa cobertura cliente
        ide_mod   := EdmodCC.ValueVariant;
        ide_Serie := EdSerCC.ValueVariant;
        ide_nNF   := EdNF_CC.ValueVariant;
        Ed_modCC  := EdModCC;
      end;
    end;
    //
    if MyObjects.FIC(ide_mod = 0, Ed_modCC,
      'Informe o modelo do documento fiscal!') then Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr00_NFeCabA, Dmod.MyDB, [
    'SELECT FatID, FatNum, Empresa, IDCtrl,',
    'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk,',
    'ide_mod, ide_serie, ide_nNF, Id  ',
    'FROM nfecaba ',
    'WHERE FatID=' + Geral.FF0(VAR_FATID_0053),
    'AND Empresa=' + Geral.FF0(Empresa),
    SQL_CNPJ_CPF_Emit,
    SQL_CNPJ_CPF_Dest,
    'AND ide_mod IN (0,' + Geral.FF0(ide_mod) + ')',
    'AND ide_serie >= 0 ', // + Geral.FF0(ide_Serie),
    'AND ide_nNF=' + Geral.FF0(ide_nNF),
    'ORDER BY ide_mod DESC ',
    '']);
    //Geral.MB_Teste(Qr00_NFeCabA.SQL.Text);
    if Qr00_NFeCabA.RecordCount > 0 then
    begin
      if not Qr00_NFeCabA.Locate('ide_mod;ide_serie', VarArrayOf([ide_mod, ide_serie]), []) then
      begin
        if not Qr00_NFeCabA.Locate('ide_serie', ide_serie, []) then
        begin
          if not Qr00_NFeCabA.Locate('ide_mod', ide_mod, []) then
          begin

          end;
        end;
      end;
      //Geral.MB_Teste(Qr00_NFeCabA.SQL.Text);
      //
      if Qr00_NFeCabA.RecordCount > 0 then
      begin
        SQLIdx := 'AND Codigo <> ' + Geral.FF0(EdCodigo.ValueVariant);
        if ide_Serie <> Qr00_NFeCabAide_serie.Value then
        begin
          case TipoNFeEntrada of
            tneVP: EdSerie.ValueVariant := Qr00_NFeCabAide_serie.Value;
            tneRP: EdSerRP.ValueVariant := Qr00_NFeCabAide_serie.Value;
            tneCC: EdSerCC.ValueVariant := Qr00_NFeCabAide_serie.Value;
          end;
        end;
        //
        case TipoNFeEntrada of
          tneVP:
          begin
            if SQLType = stIns then
              if SPED_PF.ImpedePeloID_da_NFe('pqe', 'refNFe', Qr00_NFeCabAId.Value, SQLIdx) then Exit;
            //
            EdrefNFe.ValueVariant       := Qr00_NFeCabAId.Value;
            EdNFVP_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
            EdNFVP_FatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
            EdNFVP_FatNum.ValueVariant  := Qr00_NFeCabAFatNum.Value;
          end;
          tneRP:
          begin
            if SQLType = stIns then
              if SPED_PF.ImpedePeloID_da_NFe('pqe', 'NFe_RP',Qr00_NFeCabAId.Value, SQLIdx) then Exit;
            //
            EdNFe_RP.ValueVariant       := Qr00_NFeCabAId.Value;
            EdNFRP_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
            EdNFRP_FatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
            EdNFRP_FatNum.ValueVariant  := Qr00_NFeCabAFatNum.Value;
          end;
          tneCC:
          begin
            if SQLType = stIns then
              if SPED_PF.ImpedePeloID_da_NFe('pqe', 'NFe_CC', Qr00_NFeCabAId.Value, SQLIdx) then Exit;
            //
            EdNFe_CC.ValueVariant       := Qr00_NFeCabAId.Value;
            EdNFCC_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
            EdNFCC_FatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
            EdNFCC_FatNum.ValueVariant  := Qr00_NFeCabAFatNum.Value;
          end;
        end;
      end;
    end else
      Geral.MB_Info('N�o foi poss�vel encontrar a NFe!');
  end;
end;

procedure TFmPQENew.EdkgLiquidoExit(Sender: TObject);
begin
  EdkgLiquido.Text := Geral.TFT(EdkgLiquido.Text, 3, siPositivo);
end;

procedure TFmPQENew.EdkgBrutoExit(Sender: TObject);
begin
  EdkgBruto.Text := Geral.TFT(EdkgBruto.Text, 3, siPositivo);
end;

procedure TFmPQENew.EdFreteChange(Sender: TObject);
begin
  CalculaRetornoImpostosFrete(TRetornImpost.retimpostTodos);
end;

procedure TFmPQENew.EdFreteExit(Sender: TObject);
begin
  EdFrete.Text := Geral.TFT(EdFrete.Text, 2, siPositivo);
end;

procedure TFmPQENew.EdFreteRpCOFINSChange(Sender: TObject);
begin
  CalculaRetornoImpostosFrete(TRetornImpost.retimpostCOFINS);
end;

procedure TFmPQENew.EdFreteRpICMSChange(Sender: TObject);
begin
  CalculaRetornoImpostosFrete(TRetornImpost.retimpostICMS);
end;

procedure TFmPQENew.EdFreteRpPISChange(Sender: TObject);
begin
  CalculaRetornoImpostosFrete(TRetornImpost.retimpostPIS);
end;

procedure TFmPQENew.EdCIChange(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and
  (EdCI.ValueVariant <> 0) and
  (EdCI.ValueVariant <> DModG.QrEmpresasCodigo.Value) then
  begin
    EdModNF.ValueVariant := 0;
    EdmodCC.ValueVariant := 55;
    EdmodRP.ValueVariant := 55;
  end else
  begin
    EdModNF.ValueVariant := 55;
    EdmodCC.ValueVariant := 0;
    EdmodRP.ValueVariant := 0;
  end;
end;

procedure TFmPQENew.EdConhecimExit(Sender: TObject);
begin
  EdConhecim.Text := Geral.TFT(EdConhecim.Text, 0, siPositivo);
end;

procedure TFmPQENew.EdEmpresaChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    if EdEmpresa.ValueVariant <> 0 then
    begin
      //
      EdFreteRpICMS.ValueVariant   := DModG.QrParamsEmpFreteRpICMS.Value;
      EdFreteRpPIS.ValueVariant    := DModG.QrParamsEmpFreteRpPIS.Value;
      EdFreteRpCOFINS.ValueVariant := DModG.QrParamsEmpFreteRpCOFINS.Value;
    end;
  end;
end;

procedure TFmPQENew.EdRICMSExit(Sender: TObject);
begin
  EdRICMS.Text := Geral.TFT(EdRICMS.Text, 2, siPositivo);
end;

procedure TFmPQENew.EdValorNFExit(Sender: TObject);
begin
  EdValorNF.Text := Geral.TFT(EdValorNF.Text, 2, siPositivo);
end;

procedure TFmPQENew.EdNFCC_FatNumChange(Sender: TObject);
begin
  FUsouNFe := EdNFCC_FatNum.ValueVariant <> 0;
  //
  SbNF_VP.Enabled := EdNFCC_FatNum.ValueVariant = 0;
end;

procedure TFmPQENew.EdNFExit(Sender: TObject);
begin
  EdNF.Text := Geral.TFT(EdNF.Text, 0, siPositivo);
end;

procedure TFmPQENew.EdNFe_CCExit(Sender: TObject);
begin
  if not DmNFe_0000.VerificaChavedeAcesso(EdNFe_CC.Text, True) then
    EdNFe_CC.SetFocus;
end;

procedure TFmPQENew.EdNFe_CCRedefinido(Sender: TObject);
var
  NFe_CC: String;
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
begin
  NFe_CC := EdNFe_CC.ValueVariant;
  if Length(Trim(NFe_CC)) >= 44 then
  begin
    //UnNFe_PF.ObtemSerieNumeroByID(refNFe, Serie, NF);
    NFeXMLGeren.DesmontaChaveDeAcesso(NFe_CC, 2.00,
    UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
    EdModCC.ValueVariant := ModNFe;
    EdSerCC.ValueVariant := SerNFe;
    EdNF_CC.ValueVariant := NumNFe;
  end;
end;

procedure TFmPQENew.EdNFe_RPExit(Sender: TObject);
begin
  if not DmNFe_0000.VerificaChavedeAcesso(EdNFe_RP.Text, True) then
    EdNFe_RP.SetFocus;
end;

procedure TFmPQENew.EdNFe_RPRedefinido(Sender: TObject);
var
  NFe_RP: String;
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
begin
  NFe_RP := EdNFe_RP.ValueVariant;
  if Length(Trim(NFe_RP)) >= 44 then
  begin
    //UnNFe_PF.ObtemSerieNumeroByID(refNFe, Serie, NF);
    NFeXMLGeren.DesmontaChaveDeAcesso(NFe_RP, 2.00,
    UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
    EdModRP.ValueVariant := ModNFe;
    EdSerRP.ValueVariant := SerNFe;
    EdNF_RP.ValueVariant := NumNFe;
  end;
end;

procedure TFmPQENew.EdNFVP_FatNumChange(Sender: TObject);
begin
  FUsouNFe := EdNFVP_FatNum.ValueVariant <> 0;
  //
  SbNF_CC.Enabled := EdNFVP_FatNum.ValueVariant = 0;
end;

procedure TFmPQENew.EdrefNFeExit(Sender: TObject);
begin
  if not DmNFe_0000.VerificaChavedeAcesso(EdrefNFe.Text, True) then
    EdrefNFe.SetFocus;
end;

procedure TFmPQENew.EdrefNFeRedefinido(Sender: TObject);
var
  refNFe: String;
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
begin
  refNFe := EdrefNFe.ValueVariant;
  if Length(Trim(refNFe)) >= 44 then
  begin
    //UnNFe_PF.ObtemSerieNumeroByID(refNFe, Serie, NF);
    NFeXMLGeren.DesmontaChaveDeAcesso(refNFe, 2.00,
    UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
    EdModNF.ValueVariant := ModNFe;
    EdSerie.ValueVariant := SerNFe;
    EdNF.ValueVariant := NumNFe;
  end;
end;

procedure TFmPQENew.EdRICMSFExit(Sender: TObject);
begin
  EdRICMSF.Text := Geral.TFT(EdRICMSF.Text, 2, siPositivo);
end;

procedure TFmPQENew.EdJurosExit(Sender: TObject);
begin
  EdJuros.Text := Geral.TFT(EdJuros.Text, 2, siPositivo);
end;

procedure TFmPQENew.EdICMSExit(Sender: TObject);
begin
  EdICMS.Text := Geral.TFT(EdICMS.Text, 2, siPositivo);
end;

procedure TFmPQENew.EdIND_FRTChange(Sender: TObject);
var
  IND_FRT: Integer;
begin
  EdIND_FRT_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_FRT_EFD,  EdIND_FRT.ValueVariant);
  //
  IND_FRT := Geral.IMV('0' + EdIND_FRT.Text);
  case IND_FRT of
    3:
    begin
      EdTransportador.ValueVariant := EdCI.ValueVariant;
      CBTransportador.KeyValue     := EdCI.ValueVariant;
    end;
    4:
    begin
      EdTransportador.ValueVariant := DModG.QrEmpresasCodigo.Value;
      CBTransportador.KeyValue     := DModG.QrEmpresasCodigo.Value;
    end;
  end;
end;

procedure TFmPQENew.EdIND_FRTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_FRT.Text := Geral.SelecionaItem(F_IND_FRT_EFD, 0,
      'SEL-LISTA-000 :: Indicador do Tipo de Frete',
      TitCols, Screen.Width)
  end;
end;

procedure TFmPQENew.EdIND_PGTOChange(Sender: TObject);
begin
  EdIND_PGTO_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_PAG_EFD,  EdIND_PGTO.ValueVariant);
end;

procedure TFmPQENew.EdIND_PGTOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_PGTO.Text := Geral.SelecionaItem(F_IND_PGTO_EFD, 0,
      'SEL-LISTA-000 :: Indicador do tipo de pagamento',
      TitCols, Screen.Width)
  end;
end;

procedure TFmPQENew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQENew.FormShow(Sender: TObject);
begin
  EdIND_PGTO_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_PAG_EFD,  EdIND_PGTO.ValueVariant);
  EdIND_FRT_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_FRT_EFD,  EdIND_FRT.ValueVariant);
  //
  CkTipoNF.Checked := True;
  CkTipoNF.Checked := False;
  //
  SetaFocusIni();
end;

function TFmPQENew.IncluiLanctoFaturasNFe(Valor, MoraDia: Double; Data,
  Vencto: TDateTime; Duplicata, Descricao: String; TipoCart, Carteira, Genero,
  GenCtb, CliInt, Parcela, NotaFiscal, Account, Financeiro, GenCtbD, GenCtbC,
  Filial, Fornecedor, OriCodi: Integer; SerieNF: String;
  VerificaCliInt: Boolean): Integer;
var
  TabLctA: String;
begin
  Result := 0;
  //
  UFinanceiro.LancamentoDefaultVARS;
  //
  if Financeiro = 1 then
    FLAN_Credito := Valor
  else
  if Financeiro = 2 then
    FLAN_Debito := Valor;
  //
  FLAN_VERIFICA_CLIINT := VerificaCliInt;
  FLAN_Data       := Geral.FDT(Data, 1);
  FLAN_Tipo       := TipoCart;
  FLAN_Documento  := 0;
  FLAN_MoraDia    := MoraDia;
  FLAN_Multa      := 0;//
  FLAN_Carteira   := Carteira;
  FLAN_Genero     := Genero;  // OK para Cont�bil!!!
  FLAN_GenCtb     := GenCtb;
  FLAN_GenCtbD    := GenCtbD;
  FLAN_GenCtbC    := GenCtbC;
  FLAN_Fornecedor := Fornecedor;
  //FLAN_Cliente    := Cliente;
  FLAN_CliInt     := CliInt;
  //FLAN_Depto      := QrBolacarAApto.Value;
  //FLAN_ForneceI   := QrBolacarAPropriet.Value;
  FLAN_Vencimento := Geral.FDT(Vencto, 1);
  //FLAN_Mez        := IntToStr(MLAGeral.PeriodoToAnoMes(QrPrevPeriodo.Value));
  FLAN_FatID      := FThisFatFornece;
  FLAN_FatNum     := OriCodi;
  FLAN_FatParcela := Parcela;
  FLAN_Descricao  := Descricao;
  FLAN_Duplicata  := Duplicata;
  FLAN_SerieNF    := SerieNF;
  FLAN_NotaFiscal := NotaFiscal;
  FLAN_Account    := Account;
  {$IfDef DEFINE_VARLCT}
  TabLctA       := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', TabLctA, LAN_CTOS, 'Controle');
  //
  if UFinanceiro.InsereLancamento(TabLctA) then
  begin
    Result := FLAN_Controle;
  end;
  {$Else}
  FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', VAR_LCT, VAR_LCT, 'Controle');
  //
  if UFinanceiro.InsereLancamento() then
  begin
    Result := FLAN_Controle;
  end;
  {$EndIf}
end;

procedure TFmPQENew.InsereLancamentosFinanceirosNFe(Empresa, Filial,
  Financeiro: Integer; ValorNF: Double; NumeroNF, CondicaoPG, GenCtbD, GenCtbC,
  NF_Emp_Serie, NF_Emp_Numer, Fornecedor, OriCodi: Integer; DataFat: TDateTime);
const
  P1 = 100.00;
var
{$IfDef DEFINE_VARLCT}
  TabLctA: String;
{$EndIf}
  FaturNum, Duplicata: String;
  //V2, F2, P1, P2, T2: Double;
  //V1, F1, T1: Double;
  Parcela: Integer;
  //
  TxtFaturas,
  FaturaSep, TpDuplicata, TxtProdCom: String;
  FaturaSeq, IDDuplicata, FaturaNum, TipoCart, CartEmis, CtaFaturas, GenCtb,
  Represen: Integer;
begin
  FaturaSep   := DModG.QrParamsEmpFaturaSep.Value;
  FaturaSeq   := DModG.QrParamsEmpFaturaSeq.Value;
  FaturaNum   := DModG.QrParamsEmpFaturaNum.Value;
  TxtProdCom  := DModG.QrParamsEmpTxtProdCom.Value;
  TpDuplicata := DModG.QrParamsEmpDupProdCom.Value;

  IDDuplicata := QrPediVdaCodUsu.Value;
  //
  {
  T1 := ValorNF;
  F1 := T1;
  ReabreQueryPrzX(CondicaoPG);
  //
  QrPediPrzIts.First;
  while not QrPediPrzIts.Eof do
  begin
    if QrPediPrzIts.RecordCount = QrPediPrzIts.RecNo then
      V1 := F1
    else
    begin
      if P1 = 0 then
        V1 := 0
      else
        V1 := (Round(T1 * QrPediPrzItsPercent1.Value / P1 * 100)) / 100;
      F1 := F1 - V1;
    end;
    Parcela := QrPediPrzIts.RecNo;}
    //}
  QrNFeCabY.First;
  while not QrNFeCabY.Eof do
  begin
    if FaturaNum = 0 then
      FaturNum := FormatFloat('000000', IDDuplicata)
    else
      FaturNum := FormatFloat('000000', NumeroNF);
    Duplicata := (*TpDuplicata +*) FaturNum + FaturaSep +
      dmkPF.ParcelaFatura(QrNFeCabY.RecNo, FaturaSeq);
    //
    TxtFaturas := TxtProdCom;
    TipoCart   := QrPediVdaTipoCart.Value;
    CartEmis   := QrPediVdaCartEmis.Value;
    Represen   := QrPediVdaRepresen.Value;
    CtaFaturas := GenCtbD;
    GenCtb     := GenCtbC;
    //
    Parcela    := Geral.IMV(QrNFeCabYnDup.Value);
    if Parcela = 0 then
      Parcela := QrNFeCabY.RecNo;
    //
    IncluiLanctoFaturasNFe(QrNFeCabYvDup.Value, (*QuerySumT.FieldByName('JurosMes').AsFloat*)0.00,
      DataFat, QrNFeCabYdVenc.Value, (*NovaDup*)Duplicata,
      TxtFaturas, TipoCart, CartEmis, CtaFaturas, GenCtb, Empresa, Parcela,
      NF_Emp_Numer, Represen, Financeiro, GenCtbD, GenCtbC, Filial, Fornecedor,
      OriCodi, Geral.FF0(NF_Emp_Serie), True);
    //
    QrNFeCabY.Next;
  end;
end;

procedure TFmPQENew.InsereLancamentosFinanceirosPvd(Empresa, Filial, Financeiro:
  Integer; ValorNF: Double; NumeroNF, CondicaoPG: Integer; GenCtbD, GenCtbC,
  NF_Emp_Serie, NF_Emp_Numer, Fornecedor, OriCodi: Integer; DataFat: TDateTime);
const
  P1 = 100.00;
var
{$IfDef DEFINE_VARLCT}
  TabLctA: String;
{$EndIf}
  FaturNum, Duplicata(*, NovaDup*): String;
  //V2, F2, P1, P2, T2: Double;
  V1, F1, T1: Double;
  Parcela: Integer;
  //
  TxtFaturas,
  FaturaSep, TpDuplicata, TxtProdCom: String;
  FaturaSeq, IDDuplicata, FaturaNum, TipoCart, CartEmis, CtaFaturas, GenCtb,
  Represen: Integer;
begin
  FaturaSep   := DModG.QrParamsEmpFaturaSep.Value;
  FaturaSeq   := DModG.QrParamsEmpFaturaSeq.Value;
  FaturaNum   := DModG.QrParamsEmpFaturaNum.Value;
  TxtProdCom  := DModG.QrParamsEmpTxtProdCom.Value;
  TpDuplicata := DModG.QrParamsEmpDupProdCom.Value;

  IDDuplicata := QrPediVdaCodUsu.Value;
  //
  T1 := ValorNF;
  F1 := T1;
  ReabreQueryPrzX(CondicaoPG);
  //
  QrPediPrzIts.First;
  while not QrPediPrzIts.Eof do
  begin
    if QrPediPrzIts.RecordCount = QrPediPrzIts.RecNo then
      V1 := F1
    else
    begin
      if P1 = 0 then
        V1 := 0
      else
        V1 := (Round(T1 * QrPediPrzItsPercent1.Value / P1 * 100)) / 100;
      F1 := F1 - V1;
    end;
    Parcela := QrPediPrzIts.RecNo;
    //
    if FaturaNum = 0 then
      FaturNum := FormatFloat('000000', IDDuplicata)
    else
      FaturNum := FormatFloat('000000', NumeroNF);
    Duplicata := (*TpDuplicata +*) FaturNum + FaturaSep +
      dmkPF.ParcelaFatura(QrPediPrzIts.RecNo, FaturaSeq);
    (*// 2011-08-21
    // Teste para substituir no futuro (uso no form FmFatDivCms)
    NovaDup := DmProd.MontaDuplicata(IDDuplicata, NumeroNF, Parcela,
                 FaturaNum, FaturaSeq, TpDuplicata, FaturaSep);
    //
    if NovaDup <> Duplicata then
      Geral.MB_Aviso('Defini��o da duplicata:' + sLineBreak + Duplicata + ' <> ' + NovaDup);
    fim 2011-08-21*)
    TxtFaturas := TxtProdCom;
    TipoCart   := QrPediVdaTipoCart.Value;
    CartEmis   := QrPediVdaCartEmis.Value;
    Represen   := QrPediVdaRepresen.Value;
    CtaFaturas := GenCtbD;
    GenCtb     := GenCtbC;
    //
    IncluiLanctoFaturasNFe(V1, (*QuerySumT.FieldByName('JurosMes').AsFloat*)0.00,
      DataFat, DataFat + QrPediPrzIts.FieldByName('Dias').AsInteger, (*NovaDup*)Duplicata,
      TxtFaturas, TipoCart, CartEmis, CtaFaturas, GenCtb, Empresa, Parcela,
      NF_Emp_Numer, Represen, Financeiro, GenCtbD, GenCtbC, Filial, Fornecedor,
      OriCodi, Geral.FF0(NF_Emp_Serie), True);
    //
    QrPediPrzIts.Next;
  end;
end;

function TFmPQENew.InserePQEItsDePediVda(Codigo: Integer): Boolean;
var
  //prod_cProd, prod_cEAN, prod_xProd, prod_NCM, prod_EX_TIPI, prod_CFOP, prod_uCom, prod_cEANTrib, prod_uTrib, IPI_cEnq, DtCorrApo, xLote: String;
  //Codigo,
  Controle, Conta, Insumo, Volumes, Moeda: Integer;
  //, prod_genero, ICMS_Orig,
  ICMS_CST: Integer;
  //ICMS_modBC, ICMS_modBCST,
  IPI_CST, PIS_CST, COFINS_CST: Integer;
  HowLoad: Integer;
  //FatID, FatNum, Empresa, nItem, NFeItsI_nItem: Integer;
  PesoVB, PesoVL, ValorItem, IPI, RIPI, CFin, ICMS, RICMS, TotalCusto, TotalPeso,
  Cotacao: Double;
  RpICMS, RpPIS, RpCOFINS: Double;
  //RvICMS, RvPIS, RvCOFINS, prod_qCom, prod_vUnCom,
  prod_vProd, prod_vFrete, prod_vSeg, prod_vOutro, prod_vDesc: Double;

  //prod_qTrib, prod_vUnTrib, ICMS_pRedBC, ICMS_vBC, ICMS_pICMS, ICMS_vICMS, ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST, ICMS_vICMSST, IPI_vBC, IPI_qUnid, IPI_vUnid, IPI_pIPI, IPI_vIPI, PIS_vBC, PIS_pPIS, PIS_vPIS, PIS_qBCProd, PIS_vAliqProd, PISST_vBC, PISST_pPIS, PISST_qBCProd, PISST_vAliqProd, PISST_vPIS, COFINS_vBC, COFINS_pCOFINS, COFINS_qBCProd, COFINS_vAliqProd, COFINS_vCOFINS, COFINSST_vBC, COFINSST_pCOFINS, COFINSST_qBCProd, COFINSST_vAliqProd, COFINSST_vCOFINS, RpIPI, RvIPI, RpICMSST, RvICMSST: Double;
  SQLType: TSQLType;
  CustoItem: Double;
  OriPart: Integer;
begin
  SQLType        := stIns;

  //Codigo             := Codigo;
  Controle           := 0; // Abaixo
  Conta              := QrPediVdaIts.RecNo;
  Insumo             := DmProd.ObtemGraGru1deGraGruX(QrPediVdaItsGraGruX.Value);
  Volumes            := 1;
  PesoVB             := 0;
  PesoVL             := QrPediVdaItsQuantP.Value - QrPediVdaItsQuantV.Value;
  ValorItem          := QrPediVdaItsValLiq.Value;
  IPI                := 0;
  RIPI               := 0;
  CFin               := 0;
  ICMS               := 0;
  RICMS              := 0;
  TotalPeso          := Volumes * PesoVL;
  Moeda              := 0; // BRL
  Cotacao            := 0;
  if Dmod.QrControleNaoRecupImposPQ.Value = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
      'SELECT ICMSRec_pAliq, PISRec_pAliq,  ',
      'COFINSRec_pAliq  ',
      'FROM gragru1 ',
      'WHERE Nivel1=' + Geral.FF0(Insumo),
      '']);
    //
    RpICMS   := QrGraGru1ICMSRec_pAliq.Value;
    RpPIS    := QrGraGru1PISRec_pAliq.Value;
    RpCOFINS := QrGraGru1COFINSRec_pAliq.Value;
  end else
  begin
    RpICMS   := 0.00;
    RpPIS    := 0.00;
    RpCOFINS := 0.00;
  end;
(*
  RvICMS         := ;
  RvPIS          := ;
  RvCOFINS       := ;
  prod_cProd     := ;
  prod_cEAN      := ;
  prod_xProd     := ;
  prod_NCM       := ;
  prod_EX_TIPI   := ;
  prod_genero    := ;
  prod_CFOP      := ;
  prod_uCom      := ;
  prod_qCom      := ;
  prod_vUnCom    := ;
  *)
  prod_vProd     :=  QrPediVdaItsvProd.Value;
  (*prod_cEANTrib  := ;
  prod_uTrib     := ;
  prod_qTrib     := ;
  prod_vUnTrib   := ;*)
  prod_vFrete    := QrPediVdaItsvFrete.Value;
  prod_vSeg      := QrPediVdaItsvSeg.Value;
  prod_vOutro    := QrPediVdaItsvOutro.Value;
  prod_vDesc     := QrPediVdaItsvDesc.Value;
  (*ICMS_Orig      := ;
  ICMS_CST       := ;
  ICMS_modBC     := ;
  ICMS_pRedBC    := ;
  ICMS_vBC       := ;
  ICMS_pICMS     := ;
  ICMS_vICMS     := ;
  ICMS_modBCST   := ;
  ICMS_pMVAST    := ;
  ICMS_pRedBCST  := ;
  ICMS_vBCST     := ;
  ICMS_pICMSST   := ;
  ICMS_vICMSST   := ;
  IPI_cEnq       := ;
  IPI_CST        := ;
  IPI_vBC        := ;
  IPI_qUnid      := ;
  IPI_vUnid      := ;
  IPI_pIPI       := ;
  IPI_vIPI       := ;
  PIS_CST        := ;
  PIS_vBC        := ;
  PIS_pPIS       := ;
  PIS_vPIS       := ;
  PIS_qBCProd    := ;
  PIS_vAliqProd  := ;
  PISST_vBC      := ;
  PISST_pPIS     := ;
  PISST_qBCProd  := ;
  PISST_vAliqProd:= ;
  PISST_vPIS     := ;
  COFINS_CST     := ;
  COFINS_vBC     := ;
  COFINS_pCOFINS := ;
  COFINS_qBCProd := ;
  COFINS_vAliqProd:= ;
  COFINS_vCOFINS := ;
  COFINSST_vBC   := ;
  COFINSST_pCOFINS:= ;
  COFINSST_qBCProd:= ;
  COFINSST_vAliqProd:= ;
  COFINSST_vCOFINS:= ;
  HowLoad        := ;
  FatID          := ;
  FatNum         := ;
  Empresa        := ;
  nItem          := ;
  DtCorrApo      := ;
  xLote          := ;
  RpIPI          := ;
  RvIPI          := ;
  NFeItsI_nItem  := ;
  RpICMSST       := ;
  RvICMSST       := ;

  //
? := UMyMod.BuscaEmLivreY_Def('pqeits', 'Controle', SQLType, CodAtual?);
ou > ? := UMyMod.BPGS1I32('pqeits', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
*)
  //ICMS_Orig  := Geral.IMV(DModG.QrPrmsEmpNFeUC_ICMSOrig.Value);
  ICMS_CST   := Geral.IMV(DModG.QrPrmsEmpNFeUC_ICMS_CST_B.Value);
  IPI_CST    := Geral.IMV(DModG.QrPrmsEmpNFeUC_IPI_CST.Value);
  PIS_CST    := Geral.IMV(DModG.QrPrmsEmpNFeUC_PIS_CST.Value);
  COFINS_CST := Geral.IMV(DModG.QrPrmsEmpNFeUC_COFINS_CST.Value);
  //


  //
  HowLoad            := Integer(TPQEHowLoad.pqehlPediVda);
  CustoItem          := ValorItem; //(ValorItem + IPI_vIPI) * CFin;
  TotalCusto         := CustoItem;

  //
  OriPart            := QrPediVdaItsControle.Value;
  //
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PQEIts','PQEIts','Controle');
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqeits', False, [
  'OriPart',
  'Codigo', 'Conta', 'Insumo',
  'Volumes', 'PesoVB', 'PesoVL',
  'ValorItem', 'IPI', 'RIPI',
  'CFin', 'ICMS', 'RICMS',
  'TotalCusto', 'TotalPeso', 'Moeda',
  'Cotacao', 'RpICMS', 'RpPIS',
  'RpCOFINS', (*'RvICMS', 'RvPIS',
  'RvCOFINS', 'prod_cProd', 'prod_cEAN',
  'prod_xProd', 'prod_NCM', 'prod_EX_TIPI',
  'prod_genero', 'prod_CFOP', 'prod_uCom',
  'prod_qCom', 'prod_vUnCom',*) 'prod_vProd',
  (*'prod_cEANTrib', 'prod_uTrib', 'prod_qTrib',
  'prod_vUnTrib',*) 'prod_vFrete', 'prod_vSeg', 'prod_vOutro',
  'prod_vDesc', (*'ICMS_Orig',*) 'ICMS_CST',
  (*'ICMS_modBC', 'ICMS_pRedBC', 'ICMS_vBC',
  'ICMS_pICMS', 'ICMS_vICMS', 'ICMS_modBCST',
  'ICMS_pMVAST', 'ICMS_pRedBCST', 'ICMS_vBCST',
  'ICMS_pICMSST', 'ICMS_vICMSST', 'IPI_cEnq',*)
  'IPI_CST', (*'IPI_vBC', 'IPI_qUnid',
  'IPI_vUnid', 'IPI_pIPI', 'IPI_vIPI',*)
  'PIS_CST', (*'PIS_vBC', 'PIS_pPIS',
  'PIS_vPIS', 'PIS_qBCProd', 'PIS_vAliqProd',
  'PISST_vBC', 'PISST_pPIS', 'PISST_qBCProd',
  'PISST_vAliqProd', 'PISST_vPIS',*) 'COFINS_CST',
  (*'COFINS_vBC', 'COFINS_pCOFINS', 'COFINS_qBCProd',
  'COFINS_vAliqProd', 'COFINS_vCOFINS', 'COFINSST_vBC',
  'COFINSST_pCOFINS', 'COFINSST_qBCProd', 'COFINSST_vAliqProd',
  'COFINSST_vCOFINS',*) 'HowLoad'(*, 'FatID',
  'FatNum', 'Empresa', 'nItem',
  'DtCorrApo', 'xLote', 'RpIPI',
  'RvIPI', 'NFeItsI_nItem', 'RpICMSST',
  'RvICMSST'*)], [
  'Controle'], [
  OriPart,
  Codigo, Conta, Insumo,
  Volumes, PesoVB, PesoVL,
  ValorItem, IPI, RIPI,
  CFin, ICMS, RICMS,
  TotalCusto, TotalPeso, Moeda,
  Cotacao, RpICMS, RpPIS,
  RpCOFINS, (*RvICMS, RvPIS,
  RvCOFINS, prod_cProd, prod_cEAN,
  prod_xProd, prod_NCM, prod_EX_TIPI,
  prod_genero, prod_CFOP, prod_uCom,
  prod_qCom, prod_vUnCom,*) prod_vProd,
  (*prod_cEANTrib, prod_uTrib, prod_qTrib,
  prod_vUnTrib,*) prod_vFrete, prod_vSeg, prod_vOutro,
  prod_vDesc, (*ICMS_Orig,*) ICMS_CST,
  (*ICMS_modBC, ICMS_pRedBC, ICMS_vBC,
  ICMS_pICMS, ICMS_vICMS, ICMS_modBCST,
  ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST,
  ICMS_pICMSST, ICMS_vICMSST, IPI_cEnq,*)
  IPI_CST, (*IPI_vBC, IPI_qUnid,
  IPI_vUnid, IPI_pIPI, IPI_vIPI,*)
  PIS_CST, (*PIS_vBC, PIS_pPIS,
  PIS_vPIS, PIS_qBCProd, PIS_vAliqProd,
  PISST_vBC, PISST_pPIS, PISST_qBCProd,
  PISST_vAliqProd, PISST_vPIS,*) COFINS_CST,
  (*COFINS_vBC, COFINS_pCOFINS, COFINS_qBCProd,
  COFINS_vAliqProd, COFINS_vCOFINS, COFINSST_vBC,
  COFINSST_pCOFINS, COFINSST_qBCProd, COFINSST_vAliqProd,
  COFINSST_vCOFINS,*) HowLoad(*, FatID,
  FatNum, Empresa, nItem,
  DtCorrApo, xLote, RpIPI,
  RvIPI, NFeItsI_nItem, RpICMSST,
  RvICMSST*)], [
  Controle], True);
  //
  DmPediVda.AtualizaUmItemPediVda(OriPart);
end;

procedure TFmPQENew.ObtemDadosAtrelamento();
var
  Empresa, Filial: Integer;
begin
  if FVerPedi = 1 then
  begin
    if not DModG.ObtemEmpresaSelecionadaESuaFilial(EdEmpresa, Empresa, Filial) then
      Exit;
    //
    FNFe_FatID   := EdNFVP_FatID.ValueVariant;
    FNFe_FatNum  := EdNFVP_FatNum.ValueVariant;
    FNFe_Empresa := Empresa;
    //
    if FNFe_FatNum = 0 then
    begin
      FNFe_FatID   := EdNFCC_FatID.ValueVariant;
      FNFe_FatNum  := EdNFCC_FatNum.ValueVariant;
      FNFe_Empresa := Empresa;
    end;
    if (FNFe_FatNum <> 0) then
    begin
      ReopenCabA();
      //
      EdIND_PGTO.ValueVariant        := QrCabAide_indPag.Value;
      EdICMSTot_vProd.ValueVariant   := QrCabAICMSTot_vProd.Value;
      EdICMSTot_vFrete.ValueVariant  := QrCabAICMSTot_vFrete.Value;
      EdICMSTot_vSeg.ValueVariant    := QrCabAICMSTot_vSeg.Value;
      EdICMSTot_vOutro.ValueVariant  := QrCabAICMSTot_vOutro.Value;
      EdICMSTot_vDesc.ValueVariant   := QrCabAICMSTot_vDesc.Value;
      EdIPI.ValueVariant             := QrCabAICMSTot_vIPI.Value;
      EdValorNF.ValueVariant         := QrCabAICMSTot_vNF.Value;
      //
    end;
  end;
end;

function TFmPQENew.InserePQEItsDeNFe(const Codigo: Integer; var FisRegGenCtbD,
  FisRegGenCtbC, MadeBy, OriPart: Integer): Boolean;
const
  sProcName = 'TFmPQENew.InserePQEItsDeNFe()';
var
  prod_cProd, prod_cEAN, prod_xProd, prod_NCM, prod_EX_TIPI, prod_CFOP,
  prod_uCom, prod_cEANTrib, prod_uTrib, IPI_cEnq, DtCorrApo, xLote: String;
  //Codigo,
  Controle, Conta, Insumo, Volumes, Moeda, prod_genero, ICMS_Orig, ICMS_CST,
  ICMS_modBC, ICMS_modBCST, IPI_CST, PIS_CST, COFINS_CST, HowLoad: Integer;
  FatID, FatNum, Empresa, nItem, NFeItsI_nItem: Integer;
  PesoVB, PesoVL, ValorItem, IPI, RIPI, CFin, ICMS, RICMS, TotalCusto, TotalPeso,
  Cotacao: Double;
  RpICMS, RpPIS, RpCOFINS: Double;
  RvICMS, RvPIS, RvCOFINS,
  prod_qCom, prod_vUnCom,
  prod_vProd, prod_vFrete, prod_vSeg, prod_vOutro, prod_vDesc: Double;

  prod_qTrib, prod_vUnTrib, ICMS_pRedBC, ICMS_vBC, ICMS_pICMS, ICMS_vICMS,
  ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST, ICMS_vICMSST, IPI_vBC,
  IPI_qUnid, IPI_vUnid, IPI_pIPI, IPI_vIPI, PIS_vBC, PIS_pPIS, PIS_vPIS,
  PIS_qBCProd, PIS_vAliqProd, PISST_vBC, PISST_pPIS, PISST_qBCProd,
  PISST_vAliqProd, PISST_vPIS, COFINS_vBC, COFINS_pCOFINS, COFINS_qBCProd,
  COFINS_vAliqProd, COFINS_vCOFINS, COFINSST_vBC, COFINSST_pCOFINS,
  COFINSST_qBCProd, COFINSST_vAliqProd, COFINSST_vCOFINS, RpIPI, RvIPI,
  RpICMSST, RvICMSST: Double;

  SQLType: TSQLType;
  CustoItem: Double;
  EhServico: Boolean;
  //OriPart,
  CodInfoEmit, RegrFiscal, CFOP_Emitido, CRT_Emitido, CST_A_Emitido,
  CST_B_Emitido, CSOSN_Emitido: Integer;
  //
  CFOP_Entrada: String; (*var ICMS_Aliq: Double;*)
  CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer;
  InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST, TempX: String;
  OriTES, EFD_II_C195, GenCtbD, GenCtbC, CopyIni, CopyTam,
  TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
  TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS: Integer;
  //
  _prod_CFOP, _ICMS_CST, _IPI_CST, _PIS_CST, _COFINS_CST: String;
  _ICMS_vBC, _IPI_vBC, _PIS_vBC, _COFINS_vBC,
  ICMSAliq, PISAliq, COFINSAliq: Double;
  //FisRegGenCtbD, FisRegGenCtbC: Integer;
  AjusteVL_BC_ICMS, AjusteALIQ_ICMS, AjusteVL_ICMS, AjusteVL_OUTROS,
  AjusteVL_OPR, AjusteVL_RED_BC, ori_pIPI, ori_vIPI, VL_OPR: Double;

begin
  SQLType        := stIns;
  //
  ReopenItsN();
  //Codigo             := Codigo;
  Controle           := 0; // Abaixo
  Conta              := QrItsI.RecNO;
  Insumo             := QrItsINivel1.Value;
  if Insumo = 0 then
    Insumo           := DmProd.ObtemGraGru1deGraGruX(QrItsIGraGruX.Value);
  if Insumo = 0 then
  begin
    Geral.MB_Aviso('Insumo n�o pode ser zero! ' + sProcName);
    FmPQE.ExcluiTodaEntradaPvdNFeEmProgresso(Codigo);
    Exit;
  end;
  Volumes            := 1;
  PesoVB             := 0;
  PesoVL             := QrItsIprod_qCom.Value; // Quantidade do Pedido! pode ser fracionado!
  ValorItem          := QrItsIprod_vTotItm.Value;
  IPI                := QrItsOIPI_vIPI.Value;
  RIPI               := 0;
  CFin               := 0;
  ICMS               := 0.00; // deprecado. QrItsNICMS_vICMS.Value;
  RICMS              := 0;
  TotalPeso          := Volumes * PesoVL;
  Moeda              := 0; // BRL
  Cotacao            := 0;
  prod_cProd               := QrItsIprod_cProd.Value;
  prod_cEAN                := QrItsIprod_cEAN.Value;
  prod_xProd               := QrItsIprod_xProd.Value;
  prod_NCM                 := QrItsIprod_NCM.Value;
  prod_EX_TIPI             := QrItsIprod_EXTIPI.Value;
  prod_CFOP                := Geral.FF0(QrItsIprod_CFOP.Value);
  prod_uCom                := QrItsIprod_uCom.Value;
  prod_cEANTrib            := QrItsIprod_cEANTrib.Value;
  prod_uTrib               := QrItsIprod_uTrib.Value;
  IPI_cEnq                 := QrItsOIPI_cEnq.Value;
  //
  prod_genero              := QrItsIprod_genero .Value;
  prod_qCom                := QrItsIprod_qCom.Value;
  prod_vUnCom              := QrItsIprod_vUnCom.Value;
  prod_vProd               := QrItsIprod_vProd.Value;
  prod_qTrib               := QrItsIprod_qTrib.Value;
  prod_vUnTrib             := QrItsIprod_vUnTrib.Value;
  prod_vFrete              := QrItsIprod_vFrete.Value;
  prod_vSeg                := QrItsIprod_vSeg.Value;
  prod_vOutro              := QrItsIprod_vOutro.Value;
  prod_vDesc               := QrItsIprod_vDesc.Value;
  //
  ICMS_Orig                := QrItsNICMS_Orig.Value;
  ICMS_modBC               := QrItsNICMS_modBC.Value;
  ICMS_modBCST             := QrItsNICMS_modBCST.Value;
  ICMS_pRedBC              := QrItsNICMS_pRedBC.Value;
  ICMS_CST                 := QrItsNICMS_CST.Value;
  ICMS_vBC                 := QrItsNICMS_vBC.Value;
  ICMS_pICMS               := QrItsNICMS_pICMS.Value;
  ICMS_vICMS               := QrItsNICMS_vICMS.Value;
  ICMS_modBCST             := QrItsNICMS_modBCST.Value;
  ICMS_pMVAST              := QrItsNICMS_pMVAST.Value;
  ICMS_pRedBCST            := QrItsNICMS_pRedBCST.Value;
  ICMS_vBCST               := QrItsNICMS_vBCST.Value;
  ICMS_pICMSST             := QrItsNICMS_pICMSST.Value;
  ICMS_vICMSST             := QrItsNICMS_vICMSST.Value;
  IPI_cEnq                 := QrItsOIPI_cEnq.Value;
  //
  IPI_CST                  := QrItsOIPI_CST.Value;
  IPI_vBC                  := QrItsOIPI_vBC.Value;
  IPI_qUnid                := QrItsOIPI_qUnid.Value;
  IPI_vUnid                := QrItsOIPI_vUnid.Value;
  IPI_pIPI                 := QrItsOIPI_pIPI.Value;
  IPI_vIPI                 := QrItsOIPI_vIPI.Value;
  //
  PIS_CST                  := QrItsQPIS_CST.Value;
  PIS_vBC                  := QrItsQPIS_vBC.Value;
  PIS_pPIS                 := QrItsQPIS_pPIS.Value;
  PIS_vPIS                 := QrItsQPIS_vPIS.Value;
  PIS_qBCProd              := QrItsQPIS_qBCProd.Value;
  PIS_vAliqProd            := QrItsQPIS_vAliqProd.Value;
  //
  PISST_vBC                := QrItsRPISST_vBC.Value;
  PISST_pPIS               := QrItsRPISST_pPIS.Value;
  PISST_qBCProd            := QrItsRPISST_qBCProd.Value;
  PISST_vAliqProd          := QrItsRPISST_vAliqProd.Value;
  PISST_vPIS               := QrItsRPISST_vPIS.Value;
  //
  COFINS_CST               := QrItsSCOFINS_CST.Value;
  COFINS_vBC               := QrItsSCOFINS_vBC.Value;
  COFINS_pCOFINS           := QrItsSCOFINS_pCOFINS.Value;
  COFINS_qBCProd           := QrItsSCOFINS_qBCProd.Value;
  COFINS_vAliqProd         := QrItsSCOFINS_vAliqProd.Value;
  COFINS_vCOFINS           := QrItsSCOFINS_vCOFINS.Value;
  //
  COFINSST_vBC             := QrItsTCOFINSST_vBC.Value;
  COFINSST_pCOFINS         := QrItsTCOFINSST_pCOFINS.Value;
  COFINSST_qBCProd         := QrItsTCOFINSST_qBCProd.Value;
  COFINSST_vAliqProd       := QrItsTCOFINSST_vAliqProd.Value;
  COFINSST_vCOFINS         := QrItsTCOFINSST_vCOFINS.Value;
  //
  HowLoad                  := Integer(TPQEHowLoad.pqehlPdvNFe);
  FatID                    := QrItsIFatID.Value;
  FatNum                   := QrItsIFatNum.Value;
  Empresa                  := QrItsIEmpresa.Value;
  nItem                    := QrItsInItem.Value;
  DtCorrApo                := '0000-00-00';
  xLote                    := ''; // ver no futuro Grupo I80 - QrItsIxLote.Value;
  //
  CodInfoEmit              := EdFornecedor.ValueVariant;

  //Result :=
  EhServico := QrItsIEhServico.Value = 1;
  //
    RegrFiscal       := EdRegrFiscal.ValueVariant;
    EhServico        := QrItsIEhServico.Value = 1;
    CFOP_Emitido     := QrItsIprod_CFOP.Value;
    CRT_Emitido      := QrCabAemit_CRT.Value;
    CST_A_Emitido    := QrItsNICMS_Orig.Value;
    CST_B_Emitido    := QrItsNICMS_CST.Value;
    CSOSN_Emitido    := QrItsNICMS_CSOSN.Value;
    //PVD_MadeBy       := QrPediVdaMadeBy.Value;

    //
  DmNFe_0000.ObtemNumeroCFOP_Entrada(
    (*Tabela_FatPedFis: String; Parametros: array of integer;*)
    Empresa, CodInfoEmit, (*Item_MadeBy,*) (*PVD_MadeBy*)MadeBy, RegrFiscal, EhServico,
    CRT_Emitido, CST_A_Emitido, CST_B_Emitido, CSOSN_Emitido,
    Geral.FormataCFOP(Geral.FF0(CFOP_Emitido)),
    CFOP_Entrada, (*var ICMS_Aliq: Double;*)
    CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib,
    InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST,
    OriTES, EFD_II_C195, GenCtbD, GenCtbC,
    TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
    TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS,
    ICMSAliq, PISAliq, COFINSAliq, FisRegGenCtbD, FisRegGenCtbC);
(* Deprecado!
  if Dmod.QrControleNaoRecupImposPQ.Value = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
      'SELECT ICMSRec_pAliq, PISRec_pAliq,  ',
      'COFINSRec_pAliq  ',
      'FROM gragru1 ',
      'WHERE Nivel1=' + Geral.FF0(Insumo),
      '']);
    //
*)
  // Parei aqui!

  _ICMS_vBC                 := 0.00;
  _IPI_vBC                  := 0.00;
  _PIS_vBC                  := 0.00;
  _COFINS_vBC               := 0.00;
  AjusteVL_RED_BC           := 0.00;

  VL_OPR := prod_vProd + prod_vFrete + prod_vSeg + prod_vOutro - prod_vDesc;

  case OriTES of
    //N�o definido
    1: // Livro fiscal: Tributa
    begin
      RpICMSST                  := ICMS_pICMSST;
      RpIPI                     := IPI_pIPI;
      RpPIS                     := PIS_pPIS;
      RpCOFINS                  := COFINS_pCOFINS;
      //
      RvIPI                     := IPI_vIPI;
      RvICMSST                  := ICMS_vICMSST;
      RvPIS                     := PIS_vPIS;
      RvCOFINS                  := COFINS_vCOFINS;
      //
      case TES_ICMS of
        0: // N�o tributa
        begin
          RpICMS                    := 0.00;
          RvICMS                    := 0.00;
        end;
        1: // do XML da NFe
        begin
          RpICMS                    := ICMS_pICMS;
          RvICMS                    := ICMS_vICMS;
        end;
        2: // da Regra Fiscal
        begin
          RpICMS                    := ICMSAliq;
          RvICMS                    := Geral.RoundC(ICMSAliq * ICMS_vBC / 100, 2);
        end;
        else
          Geral.MB_Erro('TES ICMS n�o implementado em ' + sProcName);
      end;
      case TES_BC_ICMS of
        0: _ICMS_vBC := 0.00;
        1: _ICMS_vBC := ICMS_vBC;
        2: _ICMS_vBC := VL_OPR;
      end;
      //
      case TES_IPI of
        0: // N�o tributa
        begin
          _IPI_vBC                 := 0.00;
          RpIPI                    := 0.00;
          RvIPI                    := 0.00;
        end;
        1: // do XML da NFe
        begin
          _IPI_vBC                 := IPI_vBC;
          RpIPI                    := IPI_pIPI;
          RvIPI                    := IPI_vIPI;
        end;
(*
        2: // da Regra Fiscal
        begin
          _IPI_vBC                 := IPI_vBC;
          RpIPI                    := IPIAliq;
          RvIPI                    := Geral.RoundC(IPIAliq * IPI_vBC / 100, 2);
        end;
*)
        else
          Geral.MB_Erro('TES IPI n�o implementado em ' + sProcName);
      end;
      case TES_BC_IPI of
        0: _IPI_vBC := 0.00;
        1: _IPI_vBC := IPI_vBC;
        2: _IPI_vBC := VL_OPR;
      end;
      //
      case TES_PIS of
        0: // N�o tributa
        begin
          _PIS_vBC                 := 0.00;
          RpPIS                    := 0.00;
          RvPIS                    := 0.00;
        end;
        1: // do XML da NFe
        begin
          _PIS_vBC                 := PIS_vBC;
          RpPIS                    := PIS_pPIS;
          RvPIS                    := PIS_vPIS;
        end;
        2: // da Regra Fiscal
        begin
          _PIS_vBC                 := PIS_vBC;
          RpPIS                    := PISAliq;
          RvPIS                    := Geral.RoundC(PISAliq * PIS_vBC / 100, 2);
        end;
        else
          Geral.MB_Erro('TES PIS n�o implementado em ' + sProcName);
      end;
      case TES_BC_PIS of
        0: _PIS_vBC := 0.00;
        1: _PIS_vBC := PIS_vBC;
        2: _PIS_vBC := VL_OPR;
      end;
      //
      case TES_COFINS of
        0: // N�o tributa
        begin
          _COFINS_vBC                 := 0.00;
          RpCOFINS                    := 0.00;
          RvCOFINS                    := 0.00;
        end;
        1: // do XML da NFe
        begin
          _COFINS_vBC                 := COFINS_vBC;
          RpCOFINS                    := COFINS_pCOFINS;
          RvCOFINS                    := COFINS_vCOFINS;
        end;
        2: // da Regra Fiscal
        begin
          _COFINS_vBC                 := COFINS_vBC;
          RpCOFINS                    := COFINSAliq;
          RvCOFINS                    := Geral.RoundC(COFINSAliq * COFINS_vBC / 100, 2);
        end;
        else
          Geral.MB_Erro('TES COFINS n�o implementado em ' + sProcName);
      end;
      case TES_BC_COFINS of
        0: _COFINS_vBC := 0.0;
        1: _COFINS_vBC := COFINS_vBC;
        2: _COFINS_vBC := VL_OPR;
      end;
      // Geral.MB_Info('Ver quando entrar pq se estah certo!'); OK!
      AjusteVL_OPR    := SPED_PF.FormulaVL_OPR(QrItsIprod_vProd.Value,
                         QrItsIprod_vFrete.Value, QrItsIprod_vSeg.Value,
                         QrItsIprod_vOutro.Value, QrItsIprod_vDesc.Value,
                         QrItsOIPI_vIPI.Value);
      AjusteVL_RED_BC := SPED_PF.FormulaVL_RED_BC(AjusteVL_OPR,
                         QrItsNICMS_vBC.Value, QrItsNICMS_pRedBC.Value);
      AjusteVL_OUTROS := SPED_PF.FormulaVL_OUTROS(AjusteVL_OPR,
                         QrItsNICMS_vBC.Value, QrItsNICMS_vICMSST.Value,
                         AjusteVL_RED_BC, QrItsOIPI_vIPI.Value);
    end;
    2: // Livro fiscal: Outros
    begin
      RpICMS                    := 0.00;
      RpICMSST                  := 0.00;
      RpIPI                     := 0.00;
      RpPIS                     := 0.00;
      RpCOFINS                  := 0.00;
      //
      RvICMS                    := 0.00;
      RvICMSST                  := 0.00;
      RvIPI                     := 0.00;
      RvPIS                     := 0.00;
      RvCOFINS                  := 0.00;
      //

      AjusteVL_OPR    := SPED_PF.FormulaVL_OPR(QrItsIprod_vProd.Value,
                         QrItsIprod_vFrete.Value, QrItsIprod_vSeg.Value,
                         QrItsIprod_vOutro.Value, QrItsIprod_vDesc.Value,
                         QrItsOIPI_vIPI.Value);
      (*
      AjusteVL_RED_BC := SPED_PF.FormulaVL_RED_BC(AjusteVL_OPR,
                         QrItsNICMS_vBC.Value, QrItsNICMS_pRedBC.Value);
      AjusteVL_OUTROS := SPED_PF.FormulaVL_OUTROS(AjusteVL_OPR,
                         QrItsNICMS_vBC.Value, QrItsNICMS_vICMSST.Value,
                         AjusteVL_RED_BC, QrItsOIPI_vIPI.Value);
      *)
      AjusteVL_RED_BC := 0.00;
      AjusteVL_OUTROS := AjusteVL_OPR; //� o valor total pois n�o credita nada!
    end;
    else
      Geral.MB_Erro('TES indefinido em ' + sProcName);
  end;
  AjusteVL_BC_ICMS := 0.00; //QrItsNICMS_vBC.Value;
  AjusteALIQ_ICMS  := 0.00; //QrItsNICMS_pICMS.Value;
  AjusteVL_ICMS    := 0.00; //QrItsNICMS_vICMS.Value;
  //
  NFeItsI_nItem             := nItem;
  //
  _prod_CFOP  := Geral.SoNumero_TT(CFOP_Entrada);
  //ICMS_Orig     := QrItsNICMS_Orig.Value;
  //_ICMS_CST   := SPED_PF.MontaCST_ICMS_Inn_de_CST_ICMS_emi(ICMS_Orig, InnICMS_CST);
  _ICMS_CST   := InnICMS_CST;
  _IPI_CST    := InnIPI_CST;
  _PIS_CST    := InnPIS_CST;
  _COFINS_CST := InnCOFINS_CST;
  //
  CustoItem          := ValorItem; //(ValorItem + IPI_vIPI) * CFin;
  TotalCusto         := CustoItem;
  //
  //OriPart            := QrPediVdaItsControle.Value;
  //
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PQEIts','PQEIts','Controle');
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqeits', False, [
  'OriPart',
  'Codigo', 'Conta', 'Insumo',
  'Volumes', 'PesoVB', 'PesoVL',
  'ValorItem', 'IPI', 'RIPI',
  'CFin', 'ICMS', 'RICMS',
  'TotalCusto', 'TotalPeso', 'Moeda',
  'Cotacao', 'RpICMS', 'RpPIS',
  'RpCOFINS', 'RvICMS', 'RvPIS',
  'RvCOFINS', 'prod_cProd', 'prod_cEAN',
  'prod_xProd', 'prod_NCM', 'prod_EX_TIPI',
  'prod_genero', 'prod_CFOP', 'prod_uCom',
  'prod_qCom', 'prod_vUnCom', 'prod_vProd',
  'prod_cEANTrib', 'prod_uTrib', 'prod_qTrib',
  'prod_vUnTrib', 'prod_vFrete', 'prod_vSeg', 'prod_vOutro',
  'prod_vDesc', 'ICMS_Orig', 'ICMS_CST',
  'ICMS_modBC', 'ICMS_pRedBC', 'ICMS_vBC',
  'ICMS_pICMS', 'ICMS_vICMS', 'ICMS_modBCST',
  'ICMS_pMVAST', 'ICMS_pRedBCST', 'ICMS_vBCST',
  'ICMS_pICMSST', 'ICMS_vICMSST', 'IPI_cEnq',
  'IPI_CST', 'IPI_vBC', 'IPI_qUnid',
  'IPI_vUnid', 'IPI_pIPI', 'IPI_vIPI',
  'PIS_CST', 'PIS_vBC', 'PIS_pPIS',
  'PIS_vPIS', 'PIS_qBCProd', 'PIS_vAliqProd',
  'PISST_vBC', 'PISST_pPIS', 'PISST_qBCProd',
  'PISST_vAliqProd', 'PISST_vPIS', 'COFINS_CST',
  'COFINS_vBC', 'COFINS_pCOFINS', 'COFINS_qBCProd',
  'COFINS_vAliqProd', 'COFINS_vCOFINS', 'COFINSST_vBC',
  'COFINSST_pCOFINS', 'COFINSST_qBCProd', 'COFINSST_vAliqProd',
  'COFINSST_vCOFINS', 'HowLoad', 'FatID',
  'FatNum', 'Empresa', 'nItem',
  'DtCorrApo', 'xLote', 'RpIPI',
  'RvIPI', 'NFeItsI_nItem', 'RpICMSST',
  'RvICMSST', 'TES_ICMS', 'TES_IPI',
  'TES_PIS', 'TES_COFINS',
  'FisRegGenCtbD', 'FisRegGenCtbC',
  'EFD_II_C195',
  'AjusteVL_BC_ICMS', 'AjusteALIQ_ICMS', 'AjusteVL_ICMS',
  'AjusteVL_OUTROS', 'AjusteVL_OPR', 'AjusteVL_RED_BC'], [
  'Controle'], [
  OriPart,
  Codigo, Conta, Insumo,
  Volumes, PesoVB, PesoVL,
  ValorItem, IPI, RIPI,
  CFin, ICMS, RICMS,
  TotalCusto, TotalPeso, Moeda,
  Cotacao, RpICMS, RpPIS,
  RpCOFINS, RvICMS, RvPIS,
  RvCOFINS, prod_cProd, prod_cEAN,
  prod_xProd, prod_NCM, prod_EX_TIPI,
  prod_genero, _prod_CFOP, prod_uCom,
  prod_qCom, prod_vUnCom, prod_vProd,
  prod_cEANTrib, prod_uTrib, prod_qTrib,
  prod_vUnTrib, prod_vFrete, prod_vSeg, prod_vOutro,
  prod_vDesc, ICMS_Orig, _ICMS_CST,
  ICMS_modBC, ICMS_pRedBC, _ICMS_vBC,
  ICMS_pICMS, ICMS_vICMS, ICMS_modBCST,
  ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST,
  ICMS_pICMSST, ICMS_vICMSST, IPI_cEnq,
  _IPI_CST, _IPI_vBC, IPI_qUnid,
  IPI_vUnid, IPI_pIPI, IPI_vIPI,
  _PIS_CST, _PIS_vBC, PIS_pPIS,
  PIS_vPIS, PIS_qBCProd, PIS_vAliqProd,
  PISST_vBC, PISST_pPIS, PISST_qBCProd,
  PISST_vAliqProd, PISST_vPIS, _COFINS_CST,
  _COFINS_vBC, COFINS_pCOFINS, COFINS_qBCProd,
  COFINS_vAliqProd, COFINS_vCOFINS, COFINSST_vBC,
  COFINSST_pCOFINS, COFINSST_qBCProd, COFINSST_vAliqProd,
  COFINSST_vCOFINS, HowLoad, FatID,
  FatNum, Empresa, nItem,
  DtCorrApo, xLote, RpIPI,
  RvIPI, NFeItsI_nItem, RpICMSST,
  RvICMSST, TES_ICMS, TES_IPI,
  TES_PIS, TES_COFINS,
  FisRegGenCtbD, FisRegGenCtbC,
  EFD_II_C195,
  AjusteVL_BC_ICMS, AjusteALIQ_ICMS, AjusteVL_ICMS,
  AjusteVL_OUTROS, AjusteVL_OPR, AjusteVL_RED_BC], [
  Controle], True);
  //
  if OriPart <> 0 then
    DmPediVda.AtualizaUmItemPediVda(OriPart);
end;

(*procedure TFmPQENew.ReabreQueryPrzT(CondicaoPG: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Controle, Dias, Percent1 ',
    'FROM pediprzits ',
    'WHERE Codigo=' + Geral.FF0(CondicaoPG),
    'ORDER BY Dias ',
    '']);
end;
*)

procedure TFmPQENew.ReabreQueryPrzX(CondicaoPG: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzIts, Dmod.MyDB, [
    'SELECT Controle, Dias, Percent1 ',
    'FROM pediprzits ',
    'WHERE Codigo=' + Geral.FF0(CondicaoPG),
    'AND Percent1 > 0 ',
    'ORDER BY Dias ',
    '']);
end;

procedure TFmPQENew.ReopenCabA();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCabA, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfecaba',
  'WHERE FatID=' + Geral.FF0(FNFe_FatID),
  'AND FatNum=' + Geral.FF0(FNFe_FatNum),
  'AND Empresa=' + Geral.FF0(FNFe_Empresa),
  ' ']);
end;

procedure TFmPQENew.ReopenItsI();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsI, Dmod.MyDB, [
  'SELECT nfi.*,  ',
  '  nfi.prod_vProd + ',
  '  nfi.prod_vFrete + ',
  '  nfi.prod_vSeg - ',
  '  nfi.prod_vDesc + ',
  '  nfi.prod_vOutro',
  '    prod_vTotItm',
  'FROM nfeitsi nfi',
  'WHERE nfi.FatID=' + Geral.FF0(FNFe_FatID),
  'AND nfi.FatNum=' + Geral.FF0(FNFe_FatNum),
  'AND nfi.Empresa=' + Geral.FF0(FNFe_Empresa),
  'ORDER BY nItem ',
  '']);

end;

procedure TFmPQENew.ReopenItsN();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsV, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitsv',
  'WHERE FatID=' + Geral.FF0(FNFe_FatID),
  'AND FatNum=' + Geral.FF0(FNFe_FatNum),
  'AND Empresa=' + Geral.FF0(FNFe_Empresa),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsN, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitsn',
  'WHERE FatID=' + Geral.FF0(FNFe_FatID),
  'AND FatNum=' + Geral.FF0(FNFe_FatNum),
  'AND Empresa=' + Geral.FF0(FNFe_Empresa),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsO, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitso',
  'WHERE FatID=' + Geral.FF0(FNFe_FatID),
  'AND FatNum=' + Geral.FF0(FNFe_FatNum),
  'AND Empresa=' + Geral.FF0(FNFe_Empresa),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsQ, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitsq',
  'WHERE FatID=' + Geral.FF0(FNFe_FatID),
  'AND FatNum=' + Geral.FF0(FNFe_FatNum),
  'AND Empresa=' + Geral.FF0(FNFe_Empresa),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsR, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitsr',
  'WHERE FatID=' + Geral.FF0(FNFe_FatID),
  'AND FatNum=' + Geral.FF0(FNFe_FatNum),
  'AND Empresa=' + Geral.FF0(FNFe_Empresa),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsS, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitss',
  'WHERE FatID=' + Geral.FF0(FNFe_FatID),
  'AND FatNum=' + Geral.FF0(FNFe_FatNum),
  'AND Empresa=' + Geral.FF0(FNFe_Empresa),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  ' ']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsT, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeitst',
  'WHERE FatID=' + Geral.FF0(FNFe_FatID),
  'AND FatNum=' + Geral.FF0(FNFe_FatNum),
  'AND Empresa=' + Geral.FF0(FNFe_Empresa),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  ' ']);
end;

procedure TFmPQENew.ReopenPediPrzCab;
begin
  //QrPediPrzCab.Params[00].AsInteger := 1;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzCab, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome, MaxDesco, ',
  'JurosMes, Parcelas, Percent1, Percent2, CondPg ',
  'FROM pediprzcab',
  'WHERE Aplicacao & ' + Geral.FF0(1) + ' <> 0',
  'ORDER BY Nome',
  '']);
end;

procedure TFmPQENew.ReopenPediVda(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediVda, Dmod.MyDB, [
  'SELECT car.Tipo TipoCart, ',
  'frc.GenCtbD, frc.GenCtbC, frc.Financeiro, ',
  'pvd.CodUsu, pvd.MadeBy, pvd.CartEmis,',
  'pvd.RegrFiscal, pvd.Represen, pvd.Cliente ',
  'FROM pedivda pvd ',
  'LEFT JOIN carteiras car ON car.Codigo=pvd.CartEmis ',
  'LEFT JOIN fisregcad frc ON frc.Codigo=pvd.RegrFiscal',
  'WHERE pvd.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TFmPQENew.ReopenPQEItsDePediVda_All(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediVdaIts, Dmod.MyDB, [
  'SELECT pvi.* ',
  'FROM pedivdaits pvi',
//  'LEFT JOIN pedivda pvd ON pvd.Codigo=pvi.Codigo',
  'WHERE pvi.Codigo=' + Geral.FF0(Codigo),
  'AND pvi.QuantP > pvi.QuantV ',
  'ORDER BY pvi.Controle ',
  '']);
end;

procedure TFmPQENew.ReopenPQEItsDePediVda_Uni(Codigo: Integer; Quantidade,
Preco: Double);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediVdaIts, Dmod.MyDB, [
  'SELECT pvi.* ',
  'FROM pedivdaits pvi',
  'WHERE pvi.Codigo=' + Geral.FF0(Codigo),
  'AND pvi.QuantP > pvi.QuantV ',
  'AND pvi.QuantP=' + Geral.FFT(Quantidade, 10, siNegativo),
  'AND pvi.PrecoR=' + Geral.FFT(Preco, 4, siNegativo),
  'ORDER BY pvi.Controle ',
  '']);
end;

procedure TFmPQENew.SbNF_VPClick(Sender: TObject);
begin
  DefineDFe_Atrela(tneVP);
  ObtemDadosAtrelamento();
  try
    EdIND_PGTO.SetFocus;
  except
    //
  end;
end;

procedure TFmPQENew.SetaFocusIni();
begin
  if EdRegrFiscal.ValueVariant = 0 then
    EdRegrFiscal.SetFocus
  else
  if EdCondicaoPG.ValueVariant = 0 then
    EdCondicaoPG.SetFocus
  else
    TPEntrada.SetFocus;
  //
end;

procedure TFmPQENew.SbNF_CCClick(Sender: TObject);
begin
  DefineDFe_Atrela(tneCC);
  ObtemDadosAtrelamento();
  try
    EdIND_PGTO.SetFocus;
  except
    //
  end;
end;

procedure TFmPQENew.SpeedButton2Click(Sender: TObject);
begin
  DefineDFe_Atrela(tneRP);
end;

procedure TFmPQENew.VerificaChaveNFeDeEntrada(Empresa, CliInt: Integer;
  ChaveEmpresa, ChaveCliInt: String);
var
  ChaveNFe: String;
begin
  if (Empresa = CliInt) then
    ChaveNFe := ChaveEmpresa
  else
    ChaveNFe := ChaveCliInt;
  //
  if ChaveNFe = EmptyStr then
  begin
    Geral.MB_Aviso('Chave de acesso de entrada n�o definida corretamente!');
  end else
  begin
    if not DmNFe_0000.VerificaChavedeAcesso(ChaveNFe, False) then
      Exit;
  end;
end;

(*

      if UMyMod.SQLInsUpd(Dmod.QrUpdW, ImgTipo.SQLType, 'pqe', False, [
        'Data', 'IQ', 'CI',
        'Transportadora', 'NF', 'Frete',
        'PesoB', 'PesoL', 'ValorNF',
        'RICMS', 'RICMSF', 'Conhecimento',
        'Pedido', 'DataE', 'Juros',
        'ICMS', 'TipoNF',
        'refNFe', 'modNF', 'Serie',
        'NF_RP', 'NF_CC', 'Empresa',
        'FreteRpICMS', 'FreteRpPIS', 'FreteRpCOFINS',
        'FreteRvICMS', 'FreteRvPIS', 'FreteRvCOFINS',
        'modRP', 'modCC', 'SerRP', 'SerCC',
        'NFe_RP', 'NFe_CC', 'Cte_Id',
        'CTe_mod', 'CTe_serie',
        'NFVP_FatID', 'NFVP_FatNum', 'NFVP_StaLnk',
        'NFRP_FatID', 'NFRP_FatNum', 'NFRP_StaLnk',
        'NFCC_FatID', 'NFCC_FatNum', 'NFCC_StaLnk'], [
        'Codigo'], [
        Data, IQ, CI,
        Transportadora, NF, Frete,
        PesoB, PesoL, ValorNF,
        RICMS, RICMSF, Conhecimento,
        Pedido, DataE, Juros,
        ICMS, TipoNF,
        refNFe, modNF, Serie,
        NF_RP, NF_CC, Empresa,
        FreteRpICMS, FreteRpPIS, FreteRpCOFINS,
        FreteRvICMS, FreteRvPIS, FreteRvCOFINS,
        modRP, modCC, SerRP, SerCC,
        NFe_RP, NFe_CC, Cte_Id,
        CTe_mod, CTe_serie,
        NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
        NFRP_FatID, NFRP_FatNum, NFRP_StaLnk,
        NFCC_FatID, NFCC_FatNum, NFCC_StaLnk], [
        Codigo], True) then
*)

end.
