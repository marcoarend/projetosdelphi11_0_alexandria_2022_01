object FmPQUCaH: TFmPQUCaH
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-047 :: Item de Tabela de Presta'#231#227'o de Servi'#231'o'
  ClientHeight = 642
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 490
        Height = 32
        Caption = ' Item de Tabela de Presta'#231#227'o de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 490
        Height = 32
        Caption = ' Item de Tabela de Presta'#231#227'o de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 490
        Height = 32
        Caption = ' Item de Tabela de Presta'#231#227'o de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 480
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 480
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 480
        Align = alClient
        TabOrder = 0
        object DBGProdz: TdmkDBGridZTO
          Left = 453
          Top = 15
          Width = 553
          Height = 463
          Align = alClient
          DataSource = DsProdz
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Numero'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Peso'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Min_DataEmis'
              Title.Caption = 'Min.Data Emis'
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 451
          Height = 463
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 451
            Height = 65
            Align = alTop
            Caption = ' Pesquisa de produ'#231#227'o realizada:'
            TabOrder = 0
            TabStop = True
            object Panel6: TPanel
              Left = 2
              Top = 15
              Width = 447
              Height = 46
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              TabStop = True
              object Label8: TLabel
                Left = 8
                Top = 4
                Width = 124
                Height = 13
                Caption = 'Data inicial de produzidos:'
              end
              object Label9: TLabel
                Left = 144
                Top = 4
                Width = 117
                Height = 13
                Caption = 'Data final de produzidos:'
              end
              object TPProdzIni: TdmkEditDateTimePicker
                Left = 8
                Top = 20
                Width = 112
                Height = 21
                Date = 44054.549949409720000000
                Time = 44054.549949409720000000
                TabOrder = 0
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPProdzFim: TdmkEditDateTimePicker
                Left = 144
                Top = 20
                Width = 112
                Height = 21
                Date = 44054.549949409720000000
                Time = 44054.549949409720000000
                TabOrder = 1
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object BtPesquisar: TBitBtn
                Tag = 18
                Left = 276
                Top = 4
                Width = 120
                Height = 40
                Caption = '&Pesquisar'
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BtPesquisarClick
              end
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 189
            Width = 451
            Height = 220
            Align = alTop
            Caption = ' Dados do item: '
            TabOrder = 2
            object Label6: TLabel
              Left = 12
              Top = 16
              Width = 14
              Height = 13
              Caption = 'ID:'
            end
            object Label7: TLabel
              Left = 12
              Top = 96
              Width = 106
              Height = 13
              Caption = 'Pedido (ou descri'#231#227'o):'
            end
            object Label1: TLabel
              Left = 12
              Top = 136
              Width = 34
              Height = 13
              Caption = 'Ordem:'
            end
            object Label2: TLabel
              Left = 72
              Top = 136
              Width = 61
              Height = 13
              Caption = 'Data pedido:'
            end
            object Label3: TLabel
              Left = 12
              Top = 176
              Width = 135
              Height = 13
              Caption = 'Data / hora in'#237'cio produ'#231#227'o:'
            end
            object Label4: TLabel
              Left = 172
              Top = 176
              Width = 97
              Height = 13
              Caption = 'Data / hora entrega:'
            end
            object Label5: TLabel
              Left = 96
              Top = 16
              Width = 89
              Height = 13
              Caption = 'Grupo do Emiss'#227'o:'
            end
            object LaCI: TLabel
              Left = 12
              Top = 56
              Width = 70
              Height = 13
              Caption = 'Cliente interno:'
            end
            object SbDtEntrega: TSpeedButton
              Left = 328
              Top = 192
              Width = 23
              Height = 22
              Caption = '?'
              OnClick = SbDtEntregaClick
            end
            object EdCodigo: TdmkEdit
              Left = 12
              Top = 32
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdNome: TdmkEdit
              Left = 12
              Top = 112
              Width = 433
              Height = 21
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdOrdem: TdmkEdit
              Left = 12
              Top = 152
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object TPDtPedido: TdmkEditDateTimePicker
              Left = 72
              Top = 152
              Width = 112
              Height = 21
              Date = 44054.549949409720000000
              Time = 44054.549949409720000000
              TabOrder = 7
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object TPDtIniProd: TdmkEditDateTimePicker
              Left = 12
              Top = 192
              Width = 112
              Height = 21
              Date = 44054.549949409720000000
              Time = 44054.549949409720000000
              TabOrder = 8
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object TPDtEntrega: TdmkEditDateTimePicker
              Left = 172
              Top = 192
              Width = 112
              Height = 21
              Date = 44054.549949409720000000
              Time = 44054.549949409720000000
              TabOrder = 9
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdEmitGru: TdmkEditCB
              Left = 96
              Top = 32
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEmitGru
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBEmitGru: TdmkDBLookupComboBox
              Left = 163
              Top = 32
              Width = 282
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsEmitGru
              TabOrder = 2
              dmkEditCB = EdEmitGru
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCliInt: TdmkEditCB
              Left = 12
              Top = 72
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCliInt
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCliInt: TdmkDBLookupComboBox
              Left = 80
              Top = 72
              Width = 365
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMECI'
              ListSource = DsCliInt
              TabOrder = 4
              dmkEditCB = EdCliInt
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdDtIniProd: TdmkEdit
              Left = 124
              Top = 192
              Width = 41
              Height = 21
              TabOrder = 10
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdDtEntrega: TdmkEdit
              Left = 284
              Top = 192
              Width = 41
              Height = 21
              TabOrder = 11
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object Panel7: TPanel
            Left = 0
            Top = 65
            Width = 451
            Height = 124
            Align = alTop
            TabOrder = 1
            TabStop = True
            object Label23: TLabel
              Left = 8
              Top = 64
              Width = 75
              Height = 13
              Caption = 'Qtde a produzir:'
            end
            object LaQtPedido: TLabel
              Left = 8
              Top = 80
              Width = 33
              Height = 13
              Caption = '?????:'
            end
            object Label10: TLabel
              Left = 112
              Top = 64
              Width = 66
              Height = 13
              Caption = 'Produ'#231#227'o dia:'
            end
            object LaQtProdDia: TLabel
              Left = 112
              Top = 80
              Width = 33
              Height = 13
              Caption = '?????:'
            end
            object RGPedGrandeza: TRadioGroup
              Left = 2
              Top = 1
              Width = 439
              Height = 60
              Caption = ' Grandeza: '
              Columns = 5
              ItemIndex = 0
              Items.Strings = (
                'Indefinido'
                #193'rea m'#178
                #193'rea ft'#178
                'Peso kg'
                'Couros')
              TabOrder = 0
              TabStop = True
              OnClick = RGPedGrandezaClick
            end
            object EdPedQtPdAll: TdmkEdit
              Left = 8
              Top = 96
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdQtProdDia: TdmkEdit
              Left = 112
              Top = 96
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 528
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 572
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 361
    Top = 40
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 361
    Top = 88
  end
  object QrEmitGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM emitgru '
      'ORDER BY Nome ')
    Left = 482
    Top = 24
    object QrEmitGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEmitGru: TDataSource
    DataSet = QrEmitGru
    Left = 484
    Top = 69
  end
  object QrProdz: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(emi.Peso) Peso,  '
      'SUM(emi.Qtde) Qtde,  '
      'SUM(emi.AreaM2) AreaM2,  '
      'rec.Nome, MIN(DataEmis) Min_DataEmis  '
      'FROM bluederm_noroeste.emit emi '
      
        'LEFT JOIN bluederm_noroeste.listasetores lse ON lse.Codigo=emi.S' +
        'etor '
      
        'LEFT JOIN bluederm_noroeste.formulas rec ON rec.Numero=emi.Numer' +
        'o '
      'WHERE lse.TpReceita=2 '
      'AND DataEmis BETWEEN "2020-06-29" AND "2020-08-29" '
      'GROUP BY emi.Numero '
      'ORDER BY Nome ')
    Left = 370
    Top = 323
    object QrProdzNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrProdzPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrProdzQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrProdzAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrProdzNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrProdzMin_DataEmis: TWideStringField
      FieldName = 'Min_DataEmis'
    end
  end
  object DsProdz: TDataSource
    DataSet = QrProdz
    Left = 370
    Top = 371
  end
end
