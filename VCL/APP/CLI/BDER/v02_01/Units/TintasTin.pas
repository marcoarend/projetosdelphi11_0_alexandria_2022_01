unit TintasTin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, dmkLabel, Mask, DmkDAC_PF,
  DBCtrls, dmkEdit, dmkDBEdit, dmkImage, UnDmkEnums;

type
  TFmTintasTin = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    DBEdNumero: TdmkDBEdit;
    Label3: TLabel;
    DBEdTxtUsu: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    DBEdNomeCli: TDBEdit;
    Label27: TLabel;
    Panel4: TPanel;
    EdOrdem: TdmkEdit;
    Label4: TLabel;
    EdNome: TdmkEdit;
    EdObserv: TdmkEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmTintasTin: TFmTintasTin;

implementation

uses UnMyObjects, TintasCab, UnReordena, Module, UMySQLModule;

{$R *.DFM}

procedure TFmTintasTin.BtOKClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := UMyMod.BuscaEmLivreY_Def('tintastin', 'Codigo', ImgTipo.SQLType,
    FmTintasCab.QrTintasTinCodigo.Value);
  EdCodigo.ValueVariant := Codigo;
  if UMyMod.ExecSQLInsUpdFm(Fmtintastin, ImgTipo.SQLType, 'tintastin', Codigo,
  Dmod.QrUpd) then
  begin
    FmTintasCab.ReopenTintasTin(Codigo);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType       := stIns;
      EdObserv.Text         := '';
      EdNome.Text           := '';
      EdOrdem.ValueVariant  := UReordena.VerificaOrdem(Dmod.QrAux, Dmod.MyDB,
                                 'Numero', 'Ordem', 'tintastin',
                                 FmTintasCab.QrTintasCabNumero.Value,
                                 EdOrdem.ValueVariant, True);
      EdOrdem.SetFocus;
    end else Close;
  end;
end;

procedure TFmTintasTin.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTintasTin.FormActivate(Sender: TObject);
var
  Zerar: Boolean;
begin
  MyObjects.CorIniComponente();
  //
  Zerar := ImgTipo.SQLType = stIns;
  //
  EdOrdem.ValueVariant := UReordena.VerificaOrdem(Dmod.QrAux, Dmod.MyDB,
                            'Numero', 'Ordem', 'tintastin',
                            FmTintasCab.QrTintasCabNumero.Value,
                            EdOrdem.ValueVariant, Zerar);
end;

procedure TFmTintasTin.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DBEdNumero.DataSource := FmTintasCab.DsTintasCab;
  DBEdTxtUsu.DataSource := FmTintasCab.DsTintasCab;
  DBEdNome.DataSource := FmTintasCab.DsTintasCab;
  DBEdNomeCli.DataSource := FmTintasCab.DsTintasCab;
end;

procedure TFmTintasTin.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

