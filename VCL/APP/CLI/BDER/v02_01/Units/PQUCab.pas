unit PQUCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkCheckBox;

type
  TFmPQUCab = class(TForm)
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    EdCodigo: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdOrdem: TdmkEdit;
    TPDtPedido: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label3: TLabel;
    TPDtIniProd: TdmkEditDateTimePicker;
    TPDtEntrega: TdmkEditDateTimePicker;
    Label4: TLabel;
    QrEmitGru: TMySQLQuery;
    DsEmitGru: TDataSource;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    EdEmitGru: TdmkEditCB;
    CBEmitGru: TdmkDBLookupComboBox;
    Label5: TLabel;
    QrCliInt: TMySQLQuery;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMECI: TWideStringField;
    DsCliInt: TDataSource;
    LaCI: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    EdDtIniProd: TdmkEdit;
    EdDtEntrega: TdmkEdit;
    SbDtEntrega: TSpeedButton;
    Panel7: TPanel;
    Label23: TLabel;
    LaQtPedido: TLabel;
    Label10: TLabel;
    LaQtProdDia: TLabel;
    RGPedGrandeza: TRadioGroup;
    EdPedQtPdGdz: TdmkEdit;
    EdQtProdDia: TdmkEdit;
    CkEncerrado: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdEmitGruRedefinido(Sender: TObject);
    procedure SbDtEntregaClick(Sender: TObject);
    procedure RGPedGrandezaClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCab(Codigo: Integer);
  public
    { Public declarations }
    FQrPQUCab: TmySQLQuery;
    FAplicacao, FGrandezaOld: Integer;
    FM2Dia, FCourosDia, FKgDia: Double;
  end;

  var
  FmPQUCab: TFmPQUCab;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnPQ_PF;

{$R *.DFM}

procedure TFmPQUCab.BtOKClick(Sender: TObject);
var
  Nome, DtPedido, DtIniProd, DtEntrega: String;
  Codigo, Aplicacao, Ordem, EmitGru, CliInt, PedGrandeza, Encerrado: Integer;
  SQLType: TSQLType;
  QtProdDia, PedQtPdGdz: Double;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Aplicacao      := FAplicacao;
  Ordem          := EdOrdem.ValueVariant;
  DtPedido       := Geral.FDT(TPDtPedido.Date, 1);
  DtIniProd      := Geral.FDT_TP_Ed(TPDtIniProd.Date, EdDtIniProd.Text);
  DtEntrega      := Geral.FDT_TP_Ed(TPDtEntrega.Date, EdDtEntrega.Text);
  EmitGru        := EdEmitGru.ValueVariant;
  CliInt         := EdCliInt.ValueVariant;
  QtProdDia      := EdQtProdDia.ValueVariant;
  PedQtPdGdz     := EdPedQtPdGdz.ValueVariant;
  Encerrado      := Geral.BoolToInt(CkEncerrado.Checked);
  //
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  //
  if MyObjects.FIC(TpDtPedido.DateTime < 2, TPDtPedido, 'Informe a data do pedido!') then
    Exit;
  //
  if MyObjects.FIC(TPDtPedido.DateTime < 2, TPDtIniProd, 'Informe a data prevista do in�cio da produ��o!') then
    Exit;
  //
  if MyObjects.FIC(CliInt = 0, EdCliInt, 'Informe o cliente interno!') then
    Exit;
  //
  if Ordem = 0 then
    if Geral.MB_Pergunta('Confirma Ordem = 0?') <> ID_YES then
      Exit;
  //
  Codigo := UMyMod.BPGS1I32('pqucab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqucab', False, [
  'Nome', 'Aplicacao', 'Ordem',
  'DtPedido', 'DtIniProd', 'DtEntrega',
  'EmitGru', 'CliInt', 'PedGrandeza',
  'QtProdDia', 'PedQtPdGdz', 'Encerrado'], [
  'Codigo'], [
  Nome, Aplicacao, Ordem,
  DtPedido, DtIniProd, DtEntrega,
  EmitGru, CliInt, PedGrandeza,
  QtProdDia, PedQtPdGdz, Encerrado], [
  Codigo], True) then
  begin
    ReopenCab(Codigo);
    Close;
  end;
end;

procedure TFmPQUCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQUCab.EdEmitGruRedefinido(Sender: TObject);
begin
  if EdNome.ValueVariant = EmptyStr then
    EdNome.ValueVariant := CBEmitGru.Text;
end;

procedure TFmPQUCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQUCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmitGru, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM emitgru ',
  'ORDER BY Codigo DESC ',
  '']);
end;

procedure TFmPQUCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQUCab.ReopenCab(Codigo: Integer);
begin
  if FQrPQUCab <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrPQUCab, Dmod.MyDB);
    //
    if Codigo <> 0 then
      FQrPQUCab.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmPQUCab.RGPedGrandezaClick(Sender: TObject);
var
  QtProdDia, QtProdDiaOld, M2: Double;
begin
  QtProdDiaOld := EdQtProdDia.ValueVariant;
  QtProdDia    := -1;
  //
  if QtProdDiaOld > 0 then
  begin
    if RGPedGrandeza.ItemIndex <> FGrandezaOld then
    begin
      case RGPedGrandeza.ItemIndex of
        // M2
        1:
        begin
          case FGrandezaOld of
            1: ; // Nada. � o mesmo.
            2: QtProdDia := Geral.ConverteArea(QtProdDiaOld, ctP2toM2, cfCento);
            3:
            begin
              if FKgDia > 0 then
                QtProdDia := QtProdDiaOld / FKgDia * FM2Dia
              else
                QtProdDia := 0;
            end;
            4:
            begin
              if FCourosDia > 0 then
                QtProdDia := QtProdDiaOld / FCourosDia * FM2Dia
              else
                QtProdDia := 0;
            end;
          end;
        end;
        // P2
        2:
        begin
          case FGrandezaOld of
            1: QtProdDia := Geral.ConverteArea(QtProdDiaOld, ctM2toP2, cfQuarto);
            2: ; // Nada. � o mesmo.
            3:
            begin
              if FKgDia > 0 then
                QtProdDia := Geral.ConverteArea(QtProdDiaOld / FKgDia * FM2Dia, ctM2toP2, cfQuarto)
              else
                QtProdDia := 0;
            end;
            4:
            begin
              if FCourosDia > 0 then
                QtProdDia := Geral.ConverteArea(QtProdDiaOld / FCourosDia * FM2Dia, ctM2toP2, cfQuarto)
              else
                QtProdDia := 0;
            end;
          end;
        end;
        // Kg
        3:
        begin
          case FGrandezaOld of
            1:
            begin
              if FM2Dia > 0 then
                QtProdDia := QtProdDiaOld / FM2Dia * FKgDia
              else
                QtProdDia := 0;
            end;
            2:
            begin
              if FM2Dia > 0 then
                QtProdDia := Geral.ConverteArea(QtProdDiaOld, ctP2toM2, cfCento)/ FM2Dia * FKgDia
              else
                QtProdDia := 0;
            end;
            3: ; // Nada. � o mesmo.
            4:
            begin
              if FCourosDia > 0 then
                QtProdDia := QtProdDiaOld / FCourosDia * FKgDia
              else
                QtProdDia := 0;
            end;
          end;
        end;
        // Couros
        4:
        begin
          case FGrandezaOld of
            1:
            begin
              if FM2Dia > 0 then
                QtProdDia := QtProdDiaOld / FM2Dia * FCourosDia
              else
                QtProdDia := 0;
            end;
            2:
            begin
              if FM2Dia > 0 then
                QtProdDia := Geral.ConverteArea(QtProdDiaOld, ctP2toM2, cfCento)/ FM2Dia * FCourosDia
              else
                QtProdDia := 0;
            end;
            3:
            begin
              if FKgDia > 0 then
                QtProdDia := QtProdDiaOld / FKgDia * FCourosDia
              else
                QtProdDia := 0;
            end;
            4: ; // Nada. � o mesmo.
          end;
        end;
      end;
    end;
  end;
  LaQtPedido.Caption  := RGPedGrandeza.Items[RGPedGrandeza.ItemIndex];
  LaQtProdDia.Caption := RGPedGrandeza.Items[RGPedGrandeza.ItemIndex];
  //
  FGrandezaOld := RGPedGrandeza.ItemIndex;
  if QtProdDia >= 0 then
    EdQtProdDia.ValueVariant := QtProdDia;
end;

procedure TFmPQUCab.SbDtEntregaClick(Sender: TObject);
var
  PedQtPdGdz, QtProdDia: Double;
  DtIniProd, HrIniProd, DtFImProd, HrFimProd: TDateTime;
begin
  PedQtPdGdz := EdPedQtPdGdz.ValueVariant;
  QtProdDia  := EdQtProdDia.ValueVariant;
  DtIniProd  := TPDtIniProd.Date;
  HrIniProd  := EdDtIniProd.ValueVariant;
  //
  PQ_PF.CalculaDataFimProducao(PedQtPdGdz, QtProdDia, DtIniProd, HrIniProd,
  DtFImProd, HrFimProd);
  //
  TPDtEntrega.Date := DtFImProd;
  EdDtEntrega.ValueVariant := HrFimProd;
end;

end.
