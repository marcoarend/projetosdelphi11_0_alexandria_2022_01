unit MPReclasImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmMPReclasImp = class(TForm)
    Panel1: TPanel;
    QrEstq: TmySQLQuery;
    DsEstq: TDataSource;
    DBGrid1: TDBGrid;
    QrEstqEmpresa: TIntegerField;
    QrEstqGraGruX: TIntegerField;
    QrEstqQTDE: TFloatField;
    QrEstqPECAS: TFloatField;
    QrEstqPESO: TFloatField;
    QrEstqAREAM2: TFloatField;
    QrEstqAREAP2: TFloatField;
    QrEstqNivel1: TIntegerField;
    QrEstqNO_PRD: TWideStringField;
    QrEstqPrdGrupTip: TIntegerField;
    QrEstqUnidMed: TIntegerField;
    QrEstqNO_PGT: TWideStringField;
    QrEstqSIGLA: TWideStringField;
    QrEstqNO_TAM: TWideStringField;
    QrEstqNO_COR: TWideStringField;
    QrEstqGraCorCad: TIntegerField;
    QrEstqGraGruC: TIntegerField;
    QrEstqGraGru1: TIntegerField;
    QrEstqGraTamI: TIntegerField;
    Panel3: TPanel;
    QrPrdGrupTip: TmySQLQuery;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    DsPrdGrupTip: TDataSource;
    Label3: TLabel;
    EdPrdGrupTip: TdmkEditCB;
    CBPrdGrupTip: TdmkDBLookupComboBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPrdGrupTipChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmMPReclasImp: TFmMPReclasImp;

implementation

uses UnMyObjects, MPReclasIns, Module;

{$R *.DFM}

procedure TFmMPReclasImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMPReclasImp.EdPrdGrupTipChange(Sender: TObject);
var
  DataIni, DataFim: String;
  Ano, Mes, Dia: Word;
  Data: TDateTime;
begin
  DecodeDate(FmMPReclasIns.TPData.Date, Ano, Mes, Dia);
  Data := EncodeDate(Ano, Mes, 1);
  DataIni := Geral.FDT(Data, 1);
  Data := EncodeDate(Ano, Mes+1, 1);
  DataFIm := Geral.FDT(Data, 1);
  //
  QrEstq.Close;
  QrEstq.SQL.Clear;
  QrEstq.SQL.Add('SELECT smia.Empresa,  smia.GraGruX, SUM(smia.Qtde) QTDE, SUM(smia.Pecas) PECAS,');
  QrEstq.SQL.Add('SUM(smia.Peso) PESO, SUM(smia.AreaM2) AREAM2, SUM(smia.AreaP2) AREAP2,');
  QrEstq.SQL.Add(' gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,');
  QrEstq.SQL.Add('gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,');
  QrEstq.SQL.Add('gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,');
  QrEstq.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI');
  QrEstq.SQL.Add('FROM stqmovitsa smia');
  QrEstq.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX');
  QrEstq.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  QrEstq.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  QrEstq.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  QrEstq.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  QrEstq.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
  QrEstq.SQL.Add('LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed');
  QrEstq.SQL.Add('LEFT JOIN nfecaba nfa ON nfa.FatID=smia.Tipo AND');
  QrEstq.SQL.Add('  nfa.FatNum=smia.OriCodi AND nfa.Empresa=smia.Empresa');
  QrEstq.SQL.Add('WHERE smia.Tipo =1');
  QrEstq.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', QrPrdGrupTipCodigo.Value));
  QrEstq.SQL.Add('AND smia.DataHora >="' + DataIni + '" AND smia.DataHora <"' + DataFim + '"');
  QrEstq.SQL.Add('GROUP BY smia.Empresa, smia.GraGruX');
  QrEstq.SQL.Add('ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GraGruX, DataHora');
  {
  QrEstq.Params[00].AsString  := DataIni;
  QrEstq.Params[01].AsString  := DataFim;
  QrEstq.Params[02].AsInteger := QrPrdGrupTipCodigo.Value;
  }
  UnDmkDAC_PF.AbreQuery(QrEstq, Dmod.MyDB);
end;

procedure TFmMPReclasImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMPReclasImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrPrdGrupTip, Dmod.MyDB);
end;

procedure TFmMPReclasImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
