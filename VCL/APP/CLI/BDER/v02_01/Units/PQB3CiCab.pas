unit PQB3CiCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkEditDateTimePicker;

type
  TFmPQB3CiCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel5: TPanel;
    EdPeriodo: TdmkEdit;
    EdPeriodo2: TdmkEdit;
    Label2: TLabel;
    EdDataAnt: TdmkEdit;
    Label3: TLabel;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    Label1: TLabel;
    TPDataAtu: TdmkEditDateTimePicker;
    QrMovim: TMySQLQuery;
    QrMovimEmpresa: TIntegerField;
    QrMovimCliOrig: TIntegerField;
    QrMovimInsumo: TIntegerField;
    QrMovimPesoInic: TFloatField;
    QrMovimValorInic: TFloatField;
    QrMovimPesoAtuA: TFloatField;
    QrMovimValorAtuA: TFloatField;
    QrMovimPesoAtuB: TFloatField;
    QrMovimValorAtuB: TFloatField;
    QrMovimPesoAtuC: TFloatField;
    QrMovimValorAtuC: TFloatField;
    QrMovimPesoPeIn: TFloatField;
    QrMovimValorPeIn: TFloatField;
    QrMovimPesoPeBx: TFloatField;
    QrMovimValorPeBx: TFloatField;
    QrMovimPesoInfo: TFloatField;
    QrMovimValorInfo: TFloatField;
    QrMovimPesoDife: TFloatField;
    QrMovimValorDife: TFloatField;
    QrMovimSerie: TLargeintField;
    QrMovimNF: TLargeintField;
    QrMovimxLote: TWideStringField;
    QrMovimdFab: TDateField;
    QrMovimdVal: TDateField;
    QrMovimPesoAjIn: TFloatField;
    QrMovimValorAjIn: TFloatField;
    QrMovimPesoAjBx: TFloatField;
    QrMovimValorAjBx: TFloatField;
    QrMovimPesoAtuT: TFloatField;
    QrMovimValorAtuT: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FPqbPrevia: String;
    //
    procedure IncluiItensPesquisados(Controle: Integer);
    procedure RecriaSaldoFinalDoDiaAnterior(DtBal, DtIni, DtAtu: TDateTime);
  public
    { Public declarations }
  end;

  var
  FmPQB3CiCab: TFmPQB3CiCab;

implementation

uses UnMyObjects, Module, DmkDAC_PF, CreateBlueDerm, ModuleGeral, UnDmkProcFunc,
  UMySQLModule, PQx;

{$R *.DFM}

procedure TFmPQB3CiCab.BtOKClick(Sender: TObject);
var
  DataAnt, DataAtu, DataBal: String;
  Codigo, Controle, PeriAnt: Integer;
  SQLType: TSQLType;
  DtBal, _DtBal, DtAnt, DtAtu: TDateTime;
  Ano, MesB, MesF, Dia: Word;
  Conta: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdPeriodo.ValueVariant;
  Controle       := 0;
  //
  DtAnt          := Trunc(EdDataAnt.ValueVariant);
  DtAtu          := Trunc(TPDataAtu.Date);
  DtBal          := dmkPF.PrimeiroDiaDoPeriodo_Date(Codigo);
  DecodeDate(DtBal, Ano, MesB, Dia);
  DecodeDate(DtAtu, Ano, MesF, Dia);
  //
  if MyObjects.FIC(MesB <> MesF, TPDataAtu,
    'Data do balan�o itermedi�rio deve ser dentro do m�s do balan�o!') then Exit;
  //if MyObjects.FIC(DtAtu - DtAnt < 1, TPDataAtu,
    //'Data do balan�o deve ser maior que a data inicial!') then Exit;
  if MyObjects.FIC(DtAtu < DtAnt, TPDataAtu,
    'A data informada para o balan�o intermedi�rio deve ser maior que a anterior!') then Exit;
  if DtAnt = DtAtu then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT MAX(Periodo) Periodo  ',
    'FROM balancos ',
    'WHERE Periodo<' + Geral.FF0(Codigo),
    '']);
    PeriAnt := Dmod.QrAux.Fields[0].AsInteger;
    _DtBal := dmkPF.PrimeiroDiaDoPeriodo_Date(PeriAnt);
  end else
    _DtBal := DtBal;
  //
  DataAnt        := Geral.FDT(DtAnt, 1);
  DataAtu        := Geral.FDT(DtAtu, 1);
  DataBal        := Geral.FDT(_DtBal, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Controle  ',
  'FROM pqbcicab ',
  'WHERE DataAtu="' + DataAtu + '"',
  '']);
  if MyObjects.FIC(Dmod.QrAux.Fields[0].AsInteger > 0, TPDataAtu,
    'A data informada para o balan�o intermedi�rio j� existe!') then Exit;
  //
  Controle := UMyMod.BPGS1I32('pqbcicab', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqbcicab', False, [
  'Codigo', 'DataAnt', 'DataAtu'], [
  'Controle'], [
  Codigo, DataAnt, DataAtu], [
  Controle], True) then
  begin
    UnPQx.DefineVAR_Data_Insum_Movim();
    //Importa saldo do dia final selecionado
    RecriaSaldoFinalDoDiaAnterior(_DtBal, DtAnt, DtAtu);
    //
    IncluiItensPesquisados(Controle);
  end;
  //
  Close;
end;

procedure TFmPQB3CiCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQB3CiCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQB3CiCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPDataAtu.Date := Trunc(DModG.ObtemAgora());
end;

procedure TFmPQB3CiCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQB3CiCab.IncluiItensPesquisados(Controle: Integer);
const
  xLote = '';
  dFab = '0000-00-00';
  dVal = '0000-00-00';
  Tipo = '-1';
  DstCodi = '0';
  DstCtrl = '0';
  Serie = '0';
  NF = '0';
  Lancar = '0';
  //
  Flds = 'INSERT INTO pqbciits (Codigo, Controle, Empresa, CliInt, Insumo, ' +
  'Tipo, DstCodi, DstCtrl, PesoInic, ValorInic, PesoAtuA, ValorAtuA, ' +
  'PesoAtuB, ValorAtuB, PesoAtuC, ValorAtuC, PesoAtuT, ValorAtuT, PesoPeIn, ValorPeIn, PesoPeBx, ' +
  'ValorPeBx, PesoAjIn, ValorAjIn, PesoAjBx, ValorAjBx, PesoInfo, ValorInfo, ' +
  'PesoDife, ValorDife, Serie, NF, xLote, dFab, dVal, Lancar, Conta) VALUES ';
var
  Values: String;
  Codigo, Conta: Integer;
begin
  //SQLType        := ImgTipo.SQLType?;
  Codigo         := EdPeriodo.ValueVariant;
  //Controle       := ;
  Conta          := UMyMod.BPGS1I32_Varios('pqbcicab', 'Codigo', '', '', tsPos, stIns, 0, nil, QrMovim.RecordCount);
  //
  Values := EmptyStr;
  QrMovim.First;
  while not QrMovim.Eof do
  begin
    if Values <> EmptyStr then
      Values := Values + ', ';
    //
    Values := Values + '(' +
    //
    Geral.FF0(Codigo) + ', ' +
    Geral.FF0(Controle) + ', ' +

    Geral.FF0(QrMovimEmpresa.Value) + ', ' +
    Geral.FF0(QrMovimCliOrig.Value) + ', ' +
    Geral.FF0(QrMovimInsumo.Value) + ', ' +

    Tipo + ', ' +
    DstCodi + ', ' +
    DstCtrl + ', ' +

    Geral.FFT_Dot(QrMovimPesoInic.Value, 3, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimValorInic.Value, 2, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimPesoAtuA.Value, 3, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimValorAtuA.Value, 2, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimPesoAtuB.Value, 3, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimValorAtuB.Value, 2, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimPesoAtuC.Value, 3, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimValorAtuC.Value, 2, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimPesoAtuT.Value, 3, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimValorAtuT.Value, 2, siNegativo) + ', ' +

    Geral.FFT_Dot(QrMovimPesoPeIn.Value, 3, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimValorPeIn.Value, 2, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimPesoPeBx.Value, 3, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimValorPeBx.Value, 2, siNegativo) + ', ' +

    Geral.FFT_Dot(QrMovimPesoAjIn.Value, 3, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimValorAjIn.Value, 2, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimPesoAjBx.Value, 3, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimValorAjBx.Value, 2, siNegativo) + ', ' +

    Geral.FFT_Dot(QrMovimPesoInfo.Value, 3, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimValorInfo.Value, 2, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimPesoDife.Value, 3, siNegativo) + ', ' +
    Geral.FFT_Dot(QrMovimValorDife.Value, 2, siNegativo) + ', ' +
    Serie + ', ' +
    NF + ', ' +
    '"' + xLote + '", ' +
    '"' + dFab + '", ' +
    '"' + dVal + '", ' +
    Lancar + ', ' +
    Geral.FF0(Conta) + ') ';
    //
    Conta := Conta + 1;
    //
    QrMovim.Next;
  end;
  if (Values <> EmptyStr) then
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Flds + Values);
end;

procedure TFmPQB3CiCab.RecriaSaldoFinalDoDiaAnterior(DtBal, DtIni, DtAtu: TDateTime);
var
  DataBal, DataIni, DataFim, SQL: String;
begin
  DataBal := Geral.FDT(DtBal, 1);
  DataIni := Geral.FDT(DtIni, 1);
  DataFim := Geral.FDT(DtAtu - 1, 1);
  //
  FPqbPrevia :=
    UnCreateBlueDerm.RecriaTempTableNovo(ntrttPQBCiPrevia, DmodG.QrUpdPID1, False);
  //
  SQL := Geral.ATS([
  //'DELETE FROM ' +  FPqbPrevia + ';',
  //'INSERT INTO ' +  FPqbPrevia,
  'SELECT Empresa, CliOrig, Insumo, ',
  'SUM(IF(DataX<"' + DataIni + '" OR Tipo IN (0,20,120), Peso, 0)) PesoInic,  ',
  'SUM(IF(DataX<"' + DataIni + '" OR Tipo IN (0,20,120), Valor, 0)) ValorInic,  ',
  'SUM(Peso) PesoAtuA, SUM(Valor) ValorAtuA, ',
  '0.000 PesoAtuB, 0.00 ValorAtuB,',
  '0.000 PesoAtuC, 0.00 ValorAtuC,',
  'SUM(Peso) PesoAtuT, SUM(Valor) ValorAtuT, ',
  '',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso>0 AND (NOT Tipo IN (0,20,120)), Peso, 0)) PesoPeIn,  ',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso>0 AND (NOT Tipo IN (0,20,120)), Valor, 0)) ValorPeIn,  ',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso<0 AND (NOT Tipo IN (0,20,120)), Peso, 0)) PesoPeBx,  ',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso<0 AND (NOT Tipo IN (0,20,120)), Valor, 0)) ValorPeBx,',
  '',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso>0 AND (Tipo IN (20,120)), Peso, 0)) PesoAjIn,  ',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso>0 AND (Tipo IN (20,120)), Valor, 0)) ValorAjIn,  ',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso<0 AND (Tipo IN (20,120)), Peso, 0)) PesoAjBx,  ',
  'SUM(IF(DataX>="' + DataIni + '" AND Peso<0 AND (Tipo IN (20,120)), Valor, 0)) ValorAjBx,',
  '',
  '0.000 PesoInfo, 0.00 ValorInfo, 0.000 PesoDife, 0.00 ValorDife,',
  '0 Serie, 0 NF, "                    " xLote, ',
  'DATE("0000-00-00") dFab, DATE("0000-00-00") dVal   ',
  'FROM ' + TMeuDB + '.pqx ',
  'WHERE DataX BETWEEN "' + DataBal + '" AND "' + DataFim + '" ',
  //'AND CliOrig=140 ',
  'GROUP BY Empresa, CliOrig, Insumo ',
  '']);
  //
  //UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  UnDmkDAC_PF.AbreMySQLQuery0(QrMovim, Dmod.MyDB, SQL);
  //Geral.MB_teste(SQL);
  //
end;

end.
