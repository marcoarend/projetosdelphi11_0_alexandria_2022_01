object FmWBRclIns: TFmWBRclIns
  Left = 339
  Top = 185
  Caption = 'WET-RECUR-003 :: Reclassifica'#231#227'o de Wet Blue'
  ClientHeight = 605
  ClientWidth = 502
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 502
    Height = 449
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 88
      Width = 502
      Height = 177
      Align = alTop
      Caption = ' Produto a ser classificado: '
      TabOrder = 1
      object Label3: TLabel
        Left = 8
        Top = 16
        Width = 106
        Height = 13
        Caption = 'Fornecedor de origem:'
      end
      object Label1: TLabel
        Left = 8
        Top = 56
        Width = 157
        Height = 13
        Caption = 'Reduzido do Produto de Origem):'
      end
      object Label2: TLabel
        Left = 436
        Top = 56
        Width = 43
        Height = 13
        Caption = 'ID Baixa:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdForneceA: TdmkEditCB
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdForneceAChange
        OnRedefinido = EdForneceARedefinido
        DBLookupComboBox = CBForneceA
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBForneceA: TdmkDBLookupComboBox
        Left = 68
        Top = 32
        Width = 425
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsForneceA
        TabOrder = 1
        dmkEditCB = EdForneceA
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGraGruXA: TdmkEditCB
        Left = 8
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXAChange
        OnRedefinido = EdGraGruXARedefinido
        DBLookupComboBox = CBGraGruXA
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruXA: TdmkDBLookupComboBox
        Left = 68
        Top = 72
        Width = 365
        Height = 21
        KeyField = 'GraGruX'
        ListField = 'Nome'
        ListSource = DsGraGruXA
        TabOrder = 3
        dmkEditCB = EdGraGruXA
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object Panel4: TPanel
        Left = 2
        Top = 96
        Width = 498
        Height = 79
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 4
        object LaPecas: TLabel
          Left = 104
          Top = 40
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object LaAreaM2: TLabel
          Left = 180
          Top = 40
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object LaAreaP2: TLabel
          Left = 256
          Top = 40
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
          Enabled = False
        end
        object LaPeso: TLabel
          Left = 340
          Top = 40
          Width = 27
          Height = 13
          Caption = 'Peso:'
        end
        object Label15: TLabel
          Left = 8
          Top = 0
          Width = 29
          Height = 13
          Caption = 'Pallet:'
        end
        object Label18: TLabel
          Left = 416
          Top = 40
          Width = 50
          Height = 13
          Caption = 'Valor total:'
        end
        object EdPecasA: TdmkEdit
          Left = 104
          Top = 56
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdPecasAChange
        end
        object EdAreaM2A: TdmkEditCalc
          Left = 180
          Top = 56
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdAreaM2AChange
          dmkEditCalcA = EdAreaP2A
          CalcType = ctM2toP2
          CalcFrac = cfQuarto
        end
        object EdAreaP2A: TdmkEditCalc
          Left = 256
          Top = 56
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdAreaP2AChange
          dmkEditCalcA = EdAreaM2A
          CalcType = ctP2toM2
          CalcFrac = cfCento
        end
        object EdPesoKgA: TdmkEdit
          Left = 340
          Top = 56
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdPesoKgAChange
        end
        object EdPalletA: TdmkEditCB
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pallet'
          UpdCampo = 'Pallet'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPalletA
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdValorTA: TdmkEdit
          Left = 416
          Top = 56
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValorT'
          UpdCampo = 'ValorT'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValorTAChange
        end
        object CBPalletA: TdmkDBLookupComboBox
          Left = 64
          Top = 16
          Width = 425
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsWBPalletA
          TabOrder = 1
          dmkEditCB = EdPalletA
          QryCampo = 'Pallet'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object EdControleA: TdmkEdit
        Left = 436
        Top = 72
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 265
      Width = 502
      Height = 181
      Align = alTop
      Caption = ' Produto resultante da classifica'#231#227'o: '
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 95
        Width = 498
        Height = 84
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label8: TLabel
          Left = 108
          Top = 44
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object Label9: TLabel
          Left = 184
          Top = 44
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object Label10: TLabel
          Left = 256
          Top = 44
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
          Enabled = False
        end
        object Label11: TLabel
          Left = 344
          Top = 44
          Width = 27
          Height = 13
          Caption = 'Peso:'
        end
        object Label16: TLabel
          Left = 8
          Top = 0
          Width = 29
          Height = 13
          Caption = 'Pallet:'
        end
        object SBPallet: TSpeedButton
          Left = 472
          Top = 16
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBPalletClick
        end
        object Label19: TLabel
          Left = 420
          Top = 44
          Width = 50
          Height = 13
          Caption = 'Valor total:'
        end
        object EdPecasB: TdmkEdit
          Left = 108
          Top = 60
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdAreaM2B: TdmkEditCalc
          Left = 184
          Top = 60
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          dmkEditCalcA = EdAreaP2B
          CalcType = ctM2toP2
          CalcFrac = cfQuarto
        end
        object EdAreaP2B: TdmkEditCalc
          Left = 260
          Top = 60
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          dmkEditCalcA = EdAreaM2B
          CalcType = ctP2toM2
          CalcFrac = cfCento
        end
        object EdPesoKgB: TdmkEdit
          Left = 344
          Top = 60
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPalletB: TdmkEditCB
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pallet'
          UpdCampo = 'Pallet'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPalletB
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBPalletB: TdmkDBLookupComboBox
          Left = 64
          Top = 16
          Width = 405
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsWBPalletB
          TabOrder = 1
          dmkEditCB = EdPalletB
          QryCampo = 'Pallet'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdValorTB: TdmkEdit
          Left = 420
          Top = 60
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValorT'
          UpdCampo = 'ValorT'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 498
        Height = 80
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label4: TLabel
          Left = 8
          Top = 0
          Width = 109
          Height = 13
          Caption = 'Fornecedor de destino:'
        end
        object Label5: TLabel
          Left = 8
          Top = 40
          Width = 83
          Height = 13
          Caption = 'Produto (destino):'
        end
        object Label13: TLabel
          Left = 432
          Top = 0
          Width = 42
          Height = 13
          Caption = 'ID Entra:'
          Color = clBtnFace
          ParentColor = False
        end
        object EdForneceB: TdmkEditCB
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdForneceBRedefinido
          DBLookupComboBox = CBForneceB
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBForneceB: TdmkDBLookupComboBox
          Left = 68
          Top = 16
          Width = 361
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsForneceB
          TabOrder = 1
          dmkEditCB = EdForneceB
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdGraGruXB: TdmkEditCB
          Left = 8
          Top = 56
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdGraGruXBRedefinido
          DBLookupComboBox = CBGraGruXB
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGraGruXB: TdmkDBLookupComboBox
          Left = 68
          Top = 56
          Width = 425
          Height = 21
          KeyField = 'GraGruX'
          ListField = 'Nome'
          ListSource = DsGraGruXB
          TabOrder = 4
          dmkEditCB = EdGraGruXB
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdControleB: TdmkEdit
          Left = 432
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 502
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label12: TLabel
        Left = 8
        Top = 44
        Width = 188
        Height = 13
        Caption = 'Data e hora do lan'#231'amento no estoque:'
      end
      object Label14: TLabel
        Left = 200
        Top = 44
        Width = 53
        Height = 13
        Caption = 'ID Reclas.:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 260
        Top = 44
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label17: TLabel
        Left = 320
        Top = 44
        Width = 56
        Height = 13
        Caption = 'ID G'#234'meos:'
        Color = clBtnFace
        ParentColor = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 425
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object TPData: TdmkEditDateTimePicker
        Left = 8
        Top = 60
        Width = 112
        Height = 21
        Date = 40638.507592673610000000
        Time = 40638.507592673610000000
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdHora: TdmkEdit
        Left = 124
        Top = 60
        Width = 73
        Height = 21
        TabOrder = 3
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCodigo: TdmkEdit
        Left = 200
        Top = 60
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 260
        Top = 60
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMovimTwn: TdmkEdit
        Left = 320
        Top = 60
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimTwn'
        UpdCampo = 'MovimTwn'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 502
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 454
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 406
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 349
        Height = 32
        Caption = 'Reclassifica'#231#227'o de Wet Blue'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 349
        Height = 32
        Caption = 'Reclassifica'#231#227'o de Wet Blue'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 349
        Height = 32
        Caption = 'Reclassifica'#231#227'o de Wet Blue'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 497
    Width = 502
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 498
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 541
    Width = 502
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 498
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 354
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkContinuar: TCheckBox
        Left = 184
        Top = 4
        Width = 137
        Height = 37
        Caption = 'Continuar inserindo se'#13#10'houver saldo de pe'#231'as.'
        Checked = True
        State = cbChecked
        TabOrder = 2
        WordWrap = True
      end
    end
  end
  object QrGraGruXA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT ggx.Controle GraGruX, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,' +
        ' '
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, '
      'HowBxaEstq, GerBxaEstq, gtc.Nome NOMEGRATAMCAD, '
      'gtc.CodUsu CODUSUGRATAMCAD, gg1.CST_A, gg1.CST_B, '
      'gg1.UnidMed, gg1.NCM, gg1.Peso, gg1.FatorClas,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, '
      'unm.Nome NOMEUNIDMED '
      'FROM wbmprcab wmp '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      '/*WHERE pgt.TipPrd=1 */'
      'ORDER BY gg1.Nome')
    Left = 188
    Top = 140
    object QrGraGruXAGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXANivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGruXANivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGruXANivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGruXANome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGruXAPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrGraGruXAGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Required = True
    end
    object QrGraGruXANOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGruXACODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGruXACST_A: TSmallintField
      FieldName = 'CST_A'
      Required = True
    end
    object QrGraGruXACST_B: TSmallintField
      FieldName = 'CST_B'
      Required = True
    end
    object QrGraGruXAUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
    object QrGraGruXANCM: TWideStringField
      FieldName = 'NCM'
      Required = True
      Size = 10
    end
    object QrGraGruXAPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrGraGruXASIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 3
    end
    object QrGraGruXACODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGruXANOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXAHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGruXAGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXAFatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
  end
  object DsGraGruXA: TDataSource
    DataSet = QrGraGruXA
    Left = 188
    Top = 184
  end
  object QrForneceA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 268
    Top = 140
    object QrForneceACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceANOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsForneceA: TDataSource
    DataSet = QrForneceA
    Left = 268
    Top = 188
  end
  object QrForneceB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 424
    Top = 140
    object QrForneceBCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceBNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsForneceB: TDataSource
    DataSet = QrForneceB
    Left = 424
    Top = 188
  end
  object DsGraGruXB: TDataSource
    DataSet = QrGraGruXB
    Left = 348
    Top = 188
  end
  object QrGraGruXB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT ggx.Controle GraGruX, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,' +
        ' '
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, '
      'HowBxaEstq, GerBxaEstq, gtc.Nome NOMEGRATAMCAD, '
      'gtc.CodUsu CODUSUGRATAMCAD, gg1.CST_A, gg1.CST_B, '
      'gg1.UnidMed, gg1.NCM, gg1.Peso, gg1.FatorClas,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, '
      'unm.Nome NOMEUNIDMED '
      'FROM wbmprcab wmp '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      '/*WHERE pgt.TipPrd=1 */'
      'ORDER BY gg1.Nome')
    Left = 348
    Top = 140
    object QrGraGruXBGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXBNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGruXBNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGruXBNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGruXBNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraGruXBPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGruXBGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrGraGruXBHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGruXBGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXBNOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGruXBCODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGruXBCST_A: TSmallintField
      FieldName = 'CST_A'
    end
    object QrGraGruXBCST_B: TSmallintField
      FieldName = 'CST_B'
    end
    object QrGraGruXBUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGruXBNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGruXBPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrGraGruXBFatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
    object QrGraGruXBSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXBCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGruXBNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object QrWBPalletB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wbpallet'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 336
    Top = 320
    object QrWBPalletBCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWBPalletBNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsWBPalletB: TDataSource
    DataSet = QrWBPalletB
    Left = 336
    Top = 372
  end
  object QrWBPalletA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wbpallet'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 256
    Top = 320
    object QrWBPalletACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWBPalletANome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsWBPalletA: TDataSource
    DataSet = QrWBPalletA
    Left = 256
    Top = 372
  end
end
