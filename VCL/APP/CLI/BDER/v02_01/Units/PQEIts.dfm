object FmPQEIts: TFmPQEIts
  Left = 368
  Top = 178
  Caption = 'QUI-ENTRA-003 :: Item de Entrada de Uso e Consumo'
  ClientHeight = 552
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MaxHeight = 660
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 396
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Painel1: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 10
        Top = 16
        Width = 29
        Height = 13
        Caption = 'Linha:'
      end
      object EdConta: TdmkEdit
        Left = 41
        Top = 12
        Width = 57
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object PainelDados: TPanel
      Left = 0
      Top = 41
      Width = 1008
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 10
        Top = 8
        Width = 40
        Height = 13
        Caption = 'Produto:'
      end
      object SBProduto: TSpeedButton
        Left = 413
        Top = 24
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SBProdutoClick
      end
      object Label9: TLabel
        Left = 488
        Top = 8
        Width = 36
        Height = 13
        Caption = 'Moeda:'
      end
      object Label11: TLabel
        Left = 535
        Top = 8
        Width = 60
        Height = 13
        Caption = 'Cus.padr.kg:'
      end
      object SpeedButton1: TSpeedButton
        Left = 632
        Top = 23
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object Label66: TLabel
        Left = 658
        Top = 9
        Width = 31
        Height = 13
        Caption = 'CFOP:'
      end
      object SbCFOP: TSpeedButton
        Left = 977
        Top = 23
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SbCFOPClick
      end
      object Label25: TLabel
        Left = 440
        Top = 8
        Width = 43
        Height = 13
        Caption = 'Unidade:'
      end
      object EdInsumo: TdmkEditCB
        Left = 10
        Top = 24
        Width = 55
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdInsumoChange
        OnKeyDown = EdInsumoKeyDown
        DBLookupComboBox = CBInsumo
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBInsumo: TdmkDBLookupComboBox
        Left = 65
        Top = 24
        Width = 345
        Height = 21
        Color = clWhite
        KeyField = 'PQ'
        ListField = 'NOMEPQ'
        ListSource = DsPQ1
        TabOrder = 1
        OnKeyDown = CBInsumoKeyDown
        dmkEditCB = EdInsumo
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 535
        Top = 24
        Width = 92
        Height = 21
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        TabStop = False
        DataField = 'CustoPadrao'
        DataSource = DsPQCli
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 488
        Top = 24
        Width = 41
        Height = 21
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        TabStop = False
        DataField = 'NOMEMOEDAPADRAO'
        DataSource = DsPQCli
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object EdCFOP: TdmkEditCB
        Left = 658
        Top = 24
        Width = 55
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCFOP
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCFOP: TdmkDBLookupComboBox
        Left = 713
        Top = 24
        Width = 264
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCFOP
        TabOrder = 5
        TabStop = False
        OnKeyDown = CBInsumoKeyDown
        dmkEditCB = EdCFOP
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 440
        Top = 24
        Width = 45
        Height = 21
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        TabStop = False
        DataField = 'SIGLA'
        DataSource = DsPQ1
        TabOrder = 6
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 93
      Width = 1008
      Height = 303
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object Panel6: TPanel
        Left = 0
        Top = 261
        Width = 1008
        Height = 42
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        object LaCotacao: TLabel
          Left = 297
          Top = 0
          Width = 43
          Height = 13
          Caption = 'Cota'#231#227'o:'
        end
        object RGMoeda: TRadioGroup
          Left = 10
          Top = 0
          Width = 283
          Height = 38
          Caption = ' Moeda: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'R$ '
            'U$'
            #8364
            '??')
          TabOrder = 0
          TabStop = True
          OnClick = RGMoedaClick
        end
        object EdCotacao: TdmkEdit
          Left = 297
          Top = 17
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object CkDtCorrApo: TCheckBox
          Left = 638
          Top = 7
          Width = 187
          Height = 17
          Caption = #201' corre'#231#227'o de apontamento. Data:'
          TabOrder = 2
          OnClick = CkDtCorrApoClick
        end
        object TPDtCorrApo: TdmkEditDateTimePicker
          Left = 825
          Top = 5
          Width = 108
          Height = 21
          Date = 44927.000000000000000000
          Time = 0.833253726850671200
          Enabled = False
          MaxDate = 45199.999988425920000000
          TabOrder = 3
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpSPED_EFD_MIN_MAX
        end
        object CkAmostra: TCheckBox
          Left = 937
          Top = 7
          Width = 70
          Height = 17
          Caption = #201' amostra.'
          TabOrder = 4
          Visible = False
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 81
        Width = 1008
        Height = 180
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object GBRecuImpost: TGroupBox
          Left = 0
          Top = 0
          Width = 1008
          Height = 180
          Align = alClient
          Caption = ' Recupera'#231#227'o de impostos: '
          TabOrder = 0
          object Panel2: TPanel
            Left = 2
            Top = 15
            Width = 1004
            Height = 163
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label13: TLabel
              Left = 549
              Top = 0
              Width = 175
              Height = 13
              Caption = 'ICMS (retorno: BC , % e valor):'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label24: TLabel
              Left = 549
              Top = 80
              Width = 165
              Height = 13
              Caption = 'PIS (retorno: BC , % e valor):'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label26: TLabel
              Left = 549
              Top = 120
              Width = 190
              Height = 13
              Caption = 'COFINS (retorno: BC , % e valor):'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label16: TLabel
              Left = 549
              Top = 40
              Width = 130
              Height = 13
              Caption = 'IPI (retorno: BC , % e valor):'
            end
            object Label476: TLabel
              Left = 10
              Top = 0
              Width = 299
              Height = 13
              Caption = 'N12 - C'#243'digo situa'#231#227'o tribut'#225'ria do ICMS (CST): [F3]'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label477: TLabel
              Left = 8
              Top = 40
              Width = 230
              Height = 13
              Caption = 'O09: C'#243'digo situa'#231#227'o tribut'#225'ria do IPI (CST): [F3]'
            end
            object Label478: TLabel
              Left = 8
              Top = 80
              Width = 285
              Height = 13
              Caption = 'Q06: C'#243'digo situa'#231#227'o tribut'#225'ria do PIS (CST): [F3]'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label480: TLabel
              Left = 8
              Top = 120
              Width = 310
              Height = 13
              Caption = 'Q06: C'#243'digo situa'#231#227'o tribut'#225'ria da COFINS (CST): [F3]'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label20: TLabel
              Left = 765
              Top = 0
              Width = 160
              Height = 13
              Caption = 'ICMS ST (retorno: BC , % e valor):'
            end
            object EdRpICMS: TdmkEdit
              Left = 632
              Top = 16
              Width = 40
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdRpICMSChange
            end
            object EdRvICMS: TdmkEdit
              Left = 674
              Top = 16
              Width = 72
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdRpPIS: TdmkEdit
              Left = 632
              Top = 96
              Width = 40
              Height = 21
              Alignment = taRightJustify
              TabOrder = 14
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdRpPISChange
            end
            object EdRvPIS: TdmkEdit
              Left = 674
              Top = 96
              Width = 72
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 15
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdRpCOFINS: TdmkEdit
              Left = 632
              Top = 136
              Width = 40
              Height = 21
              Alignment = taRightJustify
              TabOrder = 19
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdRpCOFINSChange
            end
            object EdRvCOFINS: TdmkEdit
              Left = 674
              Top = 136
              Width = 72
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 20
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdRpIPI: TdmkEdit
              Left = 632
              Top = 56
              Width = 40
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdRpIPIChange
            end
            object EdRvIPI: TdmkEdit
              Left = 674
              Top = 56
              Width = 72
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 10
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdICMS_CST: TdmkEdit
              Left = 28
              Top = 16
              Width = 25
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '90'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00'
              QryName = 'QrNFeItsN'
              QryCampo = 'UC_ICMS_CST_B'
              UpdCampo = 'UC_ICMS_CST_B'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdICMS_CSTChange
              OnKeyDown = EdICMS_CSTKeyDown
            end
            object EdUCTextoB: TdmkEdit
              Left = 54
              Top = 16
              Width = 492
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdIPI_CST: TdmkEdit
              Left = 8
              Top = 56
              Width = 45
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryName = 'QrNFeItsO'
              QryCampo = 'UC_IPI_CST'
              UpdCampo = 'UC_IPI_CST'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdIPI_CSTChange
              OnKeyDown = EdIPI_CSTKeyDown
            end
            object EdUCTextoIPI_CST: TdmkEdit
              Left = 54
              Top = 56
              Width = 492
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnRedefinido = EdUCTextoIPI_CSTRedefinido
            end
            object EdPIS_CST: TdmkEdit
              Left = 8
              Top = 96
              Width = 45
              Height = 21
              TabOrder = 11
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '1'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryName = 'QrNFeItsQ'
              QryCampo = 'UC_PIS_CST'
              UpdCampo = 'UC_PIS_CST'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdPIS_CSTChange
              OnKeyDown = EdPIS_CSTKeyDown
            end
            object EdUCTextoPIS_CST: TdmkEdit
              Left = 54
              Top = 96
              Width = 492
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 12
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCOFINS_CST: TdmkEdit
              Left = 8
              Top = 136
              Width = 45
              Height = 21
              TabOrder = 16
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '1'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryName = 'QrNFeItsS'
              QryCampo = 'UC_COFINS_CST'
              UpdCampo = 'UC_COFINS_CST'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCOFINS_CSTChange
              OnKeyDown = EdCOFINS_CSTKeyDown
            end
            object EdUCTextoCOFINS_CST: TdmkEdit
              Left = 54
              Top = 136
              Width = 492
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 17
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdICMS_vBC: TdmkEdit
              Left = 550
              Top = 16
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdICMS_vBCChange
            end
            object EdPIS_vBC: TdmkEdit
              Left = 550
              Top = 96
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 13
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdPIS_vBCChange
            end
            object EdIPI_vBC: TdmkEdit
              Left = 550
              Top = 56
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 8
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdIPI_vBCChange
            end
            object EdCOFINS_vBC: TdmkEdit
              Left = 550
              Top = 136
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 18
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdCOFINS_vBCChange
            end
            object EdICMS_vBCST: TdmkEdit
              Left = 766
              Top = 16
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 21
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdRpICMSST: TdmkEdit
              Left = 848
              Top = 16
              Width = 40
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 22
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdRpICMSChange
            end
            object EdRvICMSST: TdmkEdit
              Left = 890
              Top = 16
              Width = 72
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 23
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdICMS_Orig: TdmkEdit
              Left = 8
              Top = 16
              Width = 20
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 1
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '9'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryName = 'QrNFeItsN'
              QryCampo = 'ICMS_Orig'
              UpdCampo = 'UC_ICMS_CST_B'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdICMS_CSTChange
              OnKeyDown = EdICMS_CSTKeyDown
            end
            object GroupBox2: TGroupBox
              Left = 764
              Top = 40
              Width = 237
              Height = 109
              Caption = ' Ajustes C197:'
              TabOrder = 24
              object Panel9: TPanel
                Left = 2
                Top = 15
                Width = 233
                Height = 92
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label27: TLabel
                  Left = 9
                  Top = 48
                  Width = 40
                  Height = 13
                  Caption = '% ICMS:'
                end
                object Label28: TLabel
                  Left = 53
                  Top = 48
                  Width = 38
                  Height = 13
                  Caption = '$ ICMS:'
                end
                object Label31: TLabel
                  Left = 5
                  Top = 4
                  Width = 59
                  Height = 13
                  Caption = '$ Opera'#231#227'o:'
                end
                object Label32: TLabel
                  Left = 157
                  Top = 4
                  Width = 49
                  Height = 13
                  Caption = '$ Red.BC:'
                end
                object Label29: TLabel
                  Left = 129
                  Top = 48
                  Width = 43
                  Height = 13
                  Caption = '$ Outros:'
                end
                object Label30: TLabel
                  Left = 85
                  Top = 4
                  Width = 55
                  Height = 13
                  Caption = '$ BC ICMS:'
                end
                object EdAjusteVL_BC_ICMS: TdmkEdit
                  Left = 82
                  Top = 20
                  Width = 72
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdAjusteALIQ_ICMS: TdmkEdit
                  Left = 8
                  Top = 64
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdAjusteVL_ICMS: TdmkEdit
                  Left = 54
                  Top = 64
                  Width = 72
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdAjusteVL_OUTROS: TdmkEdit
                  Left = 130
                  Top = 64
                  Width = 72
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdAjusteVL_OPR: TdmkEdit
                  Left = 6
                  Top = 20
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdAjusteVL_RED_BC: TdmkEdit
                  Left = 157
                  Top = 20
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 81
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label14: TLabel
          Left = 10
          Top = 0
          Width = 43
          Height = 13
          Caption = 'Volumes:'
        end
        object Label15: TLabel
          Left = 114
          Top = 0
          Width = 66
          Height = 13
          Caption = 'Qtde l'#237'q./vol.:'
        end
        object Label19: TLabel
          Left = 219
          Top = 0
          Width = 86
          Height = 13
          Caption = 'Total qtde l'#237'quido:'
        end
        object Label3: TLabel
          Left = 323
          Top = 0
          Width = 76
          Height = 13
          Caption = 'Qtde bruto/Vol.:'
        end
        object Label6: TLabel
          Left = 427
          Top = 0
          Width = 82
          Height = 13
          Caption = 'Total qtde  Bruto:'
        end
        object Label10: TLabel
          Left = 10
          Top = 40
          Width = 50
          Height = 13
          Caption = 'Valor Item:'
          Enabled = False
        end
        object Label5: TLabel
          Left = 134
          Top = 40
          Width = 64
          Height = 13
          Caption = 'Valor unit'#225'rio:'
          Enabled = False
        end
        object Label4: TLabel
          Left = 382
          Top = 40
          Width = 27
          Height = 13
          Caption = '% IPI:'
        end
        object Label7: TLabel
          Left = 425
          Top = 40
          Width = 25
          Height = 13
          Caption = '$ IPI:'
        end
        object Label8: TLabel
          Left = 259
          Top = 40
          Width = 61
          Height = 13
          Caption = '$ Custo item:'
          Enabled = False
        end
        object Label12: TLabel
          Left = 499
          Top = 40
          Width = 24
          Height = 13
          Caption = 'Lote:'
        end
        object Label17: TLabel
          Left = 821
          Top = 0
          Width = 75
          Height = 13
          Caption = '$ Desp. Acess.:'
        end
        object Label18: TLabel
          Left = 529
          Top = 0
          Width = 75
          Height = 13
          Caption = '$ Valor produto:'
        end
        object Label21: TLabel
          Left = 913
          Top = 0
          Width = 58
          Height = 13
          Caption = '$ Desconto:'
        end
        object Label22: TLabel
          Left = 729
          Top = 0
          Width = 46
          Height = 13
          Caption = '$ Seguro:'
        end
        object Label23: TLabel
          Left = 633
          Top = 0
          Width = 36
          Height = 13
          Caption = '$ Frete:'
        end
        object EdVolumes: TdmkEdit
          Left = 10
          Top = 16
          Width = 100
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '1'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1.000000000000000000
          ValWarn = False
          OnChange = EdVolumesChange
          OnExit = EdVolumesExit
        end
        object EdPesoVL: TdmkEdit
          Left = 114
          Top = 16
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdPesoVLChange
          OnExit = EdPesoVLExit
        end
        object EdTotalkgLiq: TdmkEdit
          Left = 218
          Top = 16
          Width = 100
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clWhite
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdTotalkgLiqChange
          OnExit = EdTotalkgLiqExit
        end
        object EdPesoVB: TdmkEdit
          Left = 323
          Top = 16
          Width = 100
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdPesoVBChange
        end
        object EdTotalkgBruto: TdmkEdit
          Left = 427
          Top = 16
          Width = 100
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdTotalkgBrutoChange
        end
        object EdVlrItem: TdmkEdit
          Left = 10
          Top = 56
          Width = 120
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdVlrItemChange
        end
        object EdValorkg: TdmkEdit
          Left = 134
          Top = 56
          Width = 120
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 11
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdIPI_pIPI: TdmkEdit
          Left = 382
          Top = 56
          Width = 39
          Height = 21
          Alignment = taRightJustify
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '99'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdIPI_pIPIChange
        end
        object EdIPI_vIPI: TdmkEdit
          Left = 425
          Top = 56
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 14
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdIPI_vIPIChange
        end
        object EdCustoItem: TdmkEdit
          Left = 259
          Top = 56
          Width = 120
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clWhite
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdxLote: TdmkEdit
          Left = 500
          Top = 56
          Width = 501
          Height = 21
          TabOrder = 15
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object Edprod_vOutro: TdmkEdit
          Left = 821
          Top = 16
          Width = 88
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdIPI_vIPIChange
        end
        object Edprod_vProd: TdmkEdit
          Left = 529
          Top = 16
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdIPI_vIPIChange
        end
        object Edprod_vDesc: TdmkEdit
          Left = 913
          Top = 16
          Width = 88
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdIPI_vIPIChange
        end
        object Edprod_vSeg: TdmkEdit
          Left = 729
          Top = 16
          Width = 88
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdIPI_vIPIChange
        end
        object Edprod_vFrete: TdmkEdit
          Left = 633
          Top = 16
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdIPI_vIPIChange
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 435
        Height = 32
        Caption = 'Item de Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 435
        Height = 32
        Caption = 'Item de Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 435
        Height = 32
        Caption = 'Item de Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 444
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 488
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 859
        Top = 0
        Width = 145
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 20
          Top = 4
          Width = 119
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrPQ1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT med.SIGLA, pq.Nome NOMEPQ, pfo.* '
      'FROM pqfor pfo'
      'LEFT JOIN pq pq ON pfo.PQ=pq.Codigo'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pq.Codigo'
      'LEFT JOIN unidmed med ON med.Codigo=gg1.Unidmed'
      'WHERE pfo.CI>0'
      'AND pfo.IQ>0'
      'ORDER BY pq.Nome')
    Left = 792
    Top = 18
    object QrPQ1NOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQ1PQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrPQ1SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
  end
  object DsPQ1: TDataSource
    DataSet = QrPQ1
    Left = 792
    Top = 66
  end
  object QrLocalizaPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM pq'
      'WHERE Codigo=:P0'
      'AND IQ=:P1')
    Left = 228
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object QrPQCli: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPQCliCalcFields
    Left = 848
    Top = 18
    object QrPQCliCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQCliMoedaPadrao: TIntegerField
      FieldName = 'MoedaPadrao'
    end
    object QrPQCliNOMEMOEDAPADRAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMOEDAPADRAO'
      Size = 5
      Calculated = True
    end
  end
  object DsPQCli: TDataSource
    DataSet = QrPQCli
    Left = 848
    Top = 66
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ICMSRec_pAliq, PISRec_pAliq,  '
      'COFINSRec_pAliq  '
      'FROM gragru1 '
      'WHERE Nivel1=1 ')
    Left = 144
    Top = 52
    object QrGraGru1ICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrGraGru1PISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrGraGru1COFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
  end
  object PMProduto: TPopupMenu
    Left = 568
    Top = 64
    object Cadastrodoproduto1: TMenuItem
      Caption = '&Cadastro do produto'
      OnClick = Cadastrodoproduto1Click
    end
    object Dadosfiscais1: TMenuItem
      Caption = 'Dados fiscais'
      OnClick = Dadosfiscais1Click
    end
  end
  object QrCFOP: TMySQLQuery
    Database = DmProd.MySQLDatabase1
    Left = 912
    Top = 17
    object QrCFOPCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCFOP: TDataSource
    DataSet = QrCFOP
    Left = 912
    Top = 66
  end
end
