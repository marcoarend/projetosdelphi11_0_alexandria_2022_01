object FmFormulasImpBH: TFmFormulasImpBH
  Left = 363
  Top = 167
  Caption = 'QUI-RECEI-003 :: Impress'#227'o de Receitas de Ribeira'
  ClientHeight = 657
  ClientWidth = 947
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  Scaled = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 947
    Height = 489
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 947
      Height = 73
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object RGImpRecRib: TRadioGroup
        Left = 4
        Top = 4
        Width = 700
        Height = 65
        Caption = ' Apresenta'#231#227'o: '
        Columns = 2
        ItemIndex = 3
        Items.Strings = (
          'Procure em:'
          ''
          'FmPrincipal.VAR_RecImpApresentaCol'
          'FmPrincipal.VAR_RecImpApresentaRol')
        TabOrder = 0
      end
      object GBkgTon: TGroupBox
        Left = 710
        Top = 4
        Width = 95
        Height = 65
        Caption = ' Grandeza: '
        TabOrder = 1
        object RBTon: TRadioButton
          Left = 8
          Top = 16
          Width = 50
          Height = 17
          Caption = 'Ton'
          Checked = True
          TabOrder = 0
          TabStop = True
        end
        object RBkg: TRadioButton
          Left = 8
          Top = 36
          Width = 50
          Height = 17
          Caption = 'kg'
          TabOrder = 1
        end
      end
      object CkMatricial: TCheckBox
        Left = 808
        Top = 8
        Width = 97
        Height = 17
        Caption = 'Novo (Matricial)'
        TabOrder = 2
      end
      object CkGrade: TCheckBox
        Left = 808
        Top = 28
        Width = 125
        Height = 17
        Caption = 'Ver grade (Matricial)'
        TabOrder = 3
      end
      object CkContinua: TCheckBox
        Left = 808
        Top = 48
        Width = 125
        Height = 17
        Caption = 'Continuar imprimindo.'
        TabOrder = 4
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 73
      Width = 947
      Height = 416
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Dados b'#225'sicos'
        object PainelDados: TPanel
          Left = 0
          Top = 0
          Width = 939
          Height = 388
          Align = alClient
          BevelOuter = bvSpace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object PainelReceita: TPanel
            Left = 1
            Top = 1
            Width = 937
            Height = 93
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 4
              Width = 159
              Height = 16
              Caption = 'Receita (F4 para amostra):'
            end
            object Label11: TLabel
              Left = 8
              Top = 48
              Width = 247
              Height = 16
              Caption = 'Cliente interno (dono do produto qu'#237'mico):'
            end
            object Label2: TLabel
              Left = 668
              Top = 4
              Width = 51
              Height = 16
              Caption = 'N'#250'mero:'
              FocusControl = DBEdit1
            end
            object EdReceita: TdmkEditCB
              Left = 8
              Top = 20
              Width = 72
              Height = 24
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdReceitaChange
              OnKeyDown = EdReceitaKeyDown
              DBLookupComboBox = CBReceita
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBReceita: TdmkDBLookupComboBox
              Left = 82
              Top = 20
              Width = 583
              Height = 24
              Color = clWhite
              KeyField = 'Numero'
              ListField = 'Nome'
              ListSource = DsFormulas
              TabOrder = 1
              OnKeyDown = CBReceitaKeyDown
              dmkEditCB = EdReceita
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdCliInt: TdmkEditCB
              Left = 8
              Top = 64
              Width = 72
              Height = 24
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCliInt
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCliInt: TdmkDBLookupComboBox
              Left = 82
              Top = 64
              Width = 523
              Height = 24
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMECI'
              ListSource = DsCliInt
              TabOrder = 3
              dmkEditCB = EdCliInt
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object DBRadioGroup1: TDBRadioGroup
              Left = 764
              Top = 8
              Width = 165
              Height = 41
              Caption = ' Baixa estq VS In Natura: '
              Columns = 3
              DataField = 'BxaEstqVS'
              DataSource = DsFormulas
              Items.Strings = (
                '???'
                'N'#227'o'
                'Sim')
              TabOrder = 4
              Values.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8')
            end
            object CkDtCorrApo: TCheckBox
              Left = 612
              Top = 68
              Width = 185
              Height = 17
              Caption = #201' corre'#231#227'o de apontamento. Data:'
              TabOrder = 5
              OnClick = CkDtCorrApoClick
            end
            object TPDtCorrApo: TdmkEditDateTimePicker
              Left = 800
              Top = 64
              Width = 129
              Height = 21
              Date = 44767.000000000000000000
              Time = 0.833253726850671200
              Enabled = False
              TabOrder = 6
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpSPED_EFD_MIN_MAX
            end
            object DBEdit1: TDBEdit
              Left = 668
              Top = 20
              Width = 89
              Height = 24
              DataField = 'NumCODIF'
              DataSource = DsFormulas
              TabOrder = 7
            end
          end
          object PainelEscolhas: TPanel
            Left = 1
            Top = 333
            Width = 937
            Height = 54
            Align = alBottom
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 2
            object Label10: TLabel
              Left = 8
              Top = 2
              Width = 35
              Height = 16
              Caption = 'Peso:'
            end
            object Label3: TLabel
              Left = 148
              Top = 2
              Width = 73
              Height = 16
              Caption = 'Quantidade:'
            end
            object Label7: TLabel
              Left = 252
              Top = 2
              Width = 35
              Height = 16
              Caption = 'Pe'#231'a:'
            end
            object SpeedButton1: TSpeedButton
              Left = 508
              Top = 20
              Width = 24
              Height = 24
              Caption = '...'
              OnClick = SpeedButton1Click
            end
            object Label12: TLabel
              Left = 544
              Top = 2
              Width = 54
              Height = 16
              Caption = 'kg/Pe'#231'a:'
            end
            object Label9: TLabel
              Left = 608
              Top = 2
              Width = 37
              Height = 16
              Caption = 'Ful'#227'o:'
            end
            object LaData: TLabel
              Left = 680
              Top = 2
              Width = 32
              Height = 16
              Caption = 'Data:'
              Visible = False
            end
            object LaHora: TLabel
              Left = 796
              Top = 2
              Width = 67
              Height = 16
              Caption = 'Hora in'#237'cio:'
            end
            object EdPeso: TdmkEdit
              Left = 8
              Top = 20
              Width = 137
              Height = 24
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdPesoChange
            end
            object EdQtde: TdmkEdit
              Left = 148
              Top = 20
              Width = 101
              Height = 24
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdQtdeChange
            end
            object CBPeca: TdmkDBLookupComboBox
              Left = 304
              Top = 20
              Width = 205
              Height = 24
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsDefPecas
              TabOrder = 3
              dmkEditCB = EdPeca
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdMedia: TdmkEdit
              Left = 544
              Top = 20
              Width = 61
              Height = 24
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdFulao: TdmkEdit
              Left = 608
              Top = 20
              Width = 69
              Height = 24
              MaxLength = 5
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdPesoChange
            end
            object TPDataP: TdmkEditDateTimePicker
              Left = 680
              Top = 20
              Width = 110
              Height = 24
              Date = 38795.000000000000000000
              Time = 0.975709085701964800
              TabOrder = 6
              Visible = False
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpInsumMovimMin
            end
            object EdPeca: TdmkEditCB
              Left = 252
              Top = 20
              Width = 53
              Height = 24
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdQtdeChange
              DBLookupComboBox = CBPeca
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdHoraIni: TdmkEdit
              Left = 796
              Top = 20
              Width = 80
              Height = 24
              TabOrder = 7
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfLong
              Texto = '00:00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object PainelEscolhe: TPanel
            Left = 1
            Top = 94
            Width = 937
            Height = 239
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 101
              Height = 239
              Align = alLeft
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label6: TLabel
                Left = 4
                Top = 4
                Width = 79
                Height = 13
                Caption = 'Lotes de couros:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object BtAdiciona: TBitBtn
                Tag = 10
                Left = 4
                Top = 22
                Width = 90
                Height = 40
                Hint = 'Confirma a senha digitada'
                Caption = '&Adiciona'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                TabOrder = 0
                OnClick = BtAdicionaClick
              end
              object BtExclui: TBitBtn
                Tag = 12
                Left = 4
                Top = 64
                Width = 90
                Height = 40
                Hint = 'Confirma a senha digitada'
                Caption = 'E&xclui'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                TabOrder = 1
                OnClick = BtExcluiClick
              end
            end
            object DBGrid1: TDBGrid
              Left = 101
              Top = 0
              Width = 611
              Height = 239
              Align = alClient
              DataSource = DsLotes
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_SerieFch'
                  Title.Caption = 'S'#233'rie'
                  Width = 111
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ficha'
                  Title.Caption = 'Ficha RMP'
                  Width = 60
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Peso'
                  Width = 76
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Marca'
                  Width = 103
                  Visible = True
                end>
            end
            object PageControl2: TPageControl
              Left = 712
              Top = 0
              Width = 225
              Height = 239
              ActivePage = TabSheet4
              Align = alRight
              MultiLine = True
              TabOrder = 2
              TabPosition = tpLeft
              object TabSheet4: TTabSheet
                Caption = 'Observa'#231#245'es'
                object EdMemo: TMemo
                  Left = 0
                  Top = 0
                  Width = 197
                  Height = 231
                  TabStop = False
                  Align = alClient
                  Color = clWhite
                  TabOrder = 0
                end
              end
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Configura'#231#245'es'
        ImageIndex = 2
        object PainelConfig: TPanel
          Left = 0
          Top = 0
          Width = 939
          Height = 388
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object RGTipoPreco: TRadioGroup
            Left = 12
            Top = 6
            Width = 297
            Height = 47
            Caption = ' Origem pre'#231'os: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'Estoque'
              'Cadastro'
              'A definir')
            TabOrder = 0
          end
          object RGImprime: TRadioGroup
            Left = 12
            Top = 53
            Width = 393
            Height = 47
            Caption = ' Op'#231#245'es: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'Visualizar'
              'Imprimir'
              'Matricial'
              'Arquivo')
            TabOrder = 1
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 947
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 899
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 193
      Height = 48
      Align = alLeft
      TabOrder = 1
      object LaSP_A: TLabel
        Left = 7
        Top = 9
        Width = 135
        Height = 32
        Caption = '000000000'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaSP_B: TLabel
        Left = 7
        Top = 11
        Width = 135
        Height = 32
        Caption = '000000000'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaSP_C: TLabel
        Left = 7
        Top = 10
        Width = 135
        Height = 32
        Caption = '000000000'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GB_M: TGroupBox
      Left = 193
      Top = 0
      Width = 706
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 412
        Height = 32
        Caption = 'Impress'#227'o de Receitas de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 412
        Height = 32
        Caption = 'Impress'#227'o de Receitas de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 412
        Height = 32
        Caption = 'Impress'#227'o de Receitas de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 537
    Width = 947
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 943
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 22
        Width = 943
        Height = 17
        Align = alBottom
        TabOrder = 0
        Visible = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 593
    Width = 947
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 943
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label13: TLabel
        Left = 148
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object PnSaiDesis: TPanel
        Left = 799
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtCancela: TBitBtn
          Tag = 15
          Left = 4
          Top = 1
          Width = 120
          Height = 40
          Hint = 'Cancela exibi'#231#227'o do cadastro de senhas'
          Caption = '&Desiste'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtCancelaClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 11
        Top = 1
        Width = 120
        Height = 40
        Hint = 'Confirma a senha digitada'
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object EdEmpresa: TdmkEditCB
        Left = 148
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 204
        Top = 16
        Width = 437
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        TabStop = False
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object QrDefPecas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * From DefPecas')
    Left = 612
    Top = 260
    object QrDefPecasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.defpecas.Codigo'
    end
    object QrDefPecasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.defpecas.Nome'
      Size = 6
    end
    object QrDefPecasGrandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'DBMBWET.defpecas.Grandeza'
    end
    object QrDefPecasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMBWET.defpecas.Lk'
    end
  end
  object DsDefPecas: TDataSource
    DataSet = QrDefPecas
    Left = 612
    Top = 308
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 408
    Top = 308
  end
  object QrFormulas: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFormulasCalcFields
    SQL.Strings = (
      'SELECT * FROM formulas'
      'ORDER BY Nome')
    Left = 412
    Top = 260
    object QrFormulasHHMM_P: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_P'
      Size = 10
      Calculated = True
    end
    object QrFormulasHHMM_R: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_R'
      Size = 10
      Calculated = True
    end
    object QrFormulasHHMM_T: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_T'
      Size = 10
      Calculated = True
    end
    object QrFormulasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrFormulasClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrFormulasTipificacao: TIntegerField
      FieldName = 'Tipificacao'
    end
    object QrFormulasDataI: TDateField
      FieldName = 'DataI'
    end
    object QrFormulasDataA: TDateField
      FieldName = 'DataA'
    end
    object QrFormulasTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrFormulasTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrFormulasTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrFormulasTempoT: TIntegerField
      FieldName = 'TempoT'
    end
    object QrFormulasHorasR: TIntegerField
      FieldName = 'HorasR'
    end
    object QrFormulasHorasP: TIntegerField
      FieldName = 'HorasP'
    end
    object QrFormulasHorasT: TIntegerField
      FieldName = 'HorasT'
    end
    object QrFormulasHidrica: TIntegerField
      FieldName = 'Hidrica'
    end
    object QrFormulasLinhaTE: TIntegerField
      FieldName = 'LinhaTE'
    end
    object QrFormulasCaldeira: TIntegerField
      FieldName = 'Caldeira'
    end
    object QrFormulasSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrFormulasEspessura: TIntegerField
      FieldName = 'Espessura'
    end
    object QrFormulasPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrFormulasQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrFormulasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFormulasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFormulasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFormulasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFormulasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFormulasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFormulasRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
    end
    object QrFormulasBxaEstqVS: TSmallintField
      FieldName = 'BxaEstqVS'
    end
    object QrFormulasNumCODIF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NumCODIF'
      Size = 11
      Calculated = True
    end
    object QrFormulasGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECI, ci.Codigo'
      'FROM entidades ci'
      'WHERE ci.Cliente2="V"')
    Left = 292
    Top = 260
    object QrCliIntNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 292
    Top = 308
  end
  object QrLotes: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLotesAfterOpen
    SQL.Strings = (
      'SELECT emc.Controle, emc.VSMovIts, emc.Peso, emc.Pecas,   '
      'vmi.SerieFch, vmi.Ficha, vmi.Marca,fch.Nome NO_SerieFch  '
      'FROM emitcus emc '
      'LEFT JOIN vsmovits vmi ON vmi.Controle=emc.VSMovIts '
      'LEFT JOIN vsserfch fch ON fch.Codigo=vmi.SerieFch '
      'WHERE emc.Codigo>0 ')
    Left = 352
    Top = 261
    object QrLotesControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLotesVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrLotesPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrLotesPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrLotesSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrLotesFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrLotesMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrLotesNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
  end
  object DsLotes: TDataSource
    DataSet = QrLotes
    Left = 352
    Top = 309
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Peso) Peso, '
      'SUM(Pecas) Pecas'
      'FROM emitcus'
      'WHERE Codigo=:P0')
    Left = 720
    Top = 277
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSomaPecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object PMAdiciona: TPopupMenu
    Left = 120
    Top = 270
    object IMEI1: TMenuItem
      Caption = 'IME-I'
      OnClick = IMEI1Click
    end
    object Correto1: TMenuItem
      Caption = '&Antigo'
      Enabled = False
      OnClick = Correto1Click
    end
    object Mdia1: TMenuItem
      Caption = '&M'#233'dia'
      Enabled = False
      OnClick = Mdia1Click
    end
  end
end
