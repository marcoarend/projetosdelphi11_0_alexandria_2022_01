unit WBOutKno;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, Vcl.Grids, Vcl.DBGrids,
  UnAppEnums;

type
  TFmWBOutKno = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DBEdMovimCod: TdmkDBEdit;
    Label2: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    Label3: TLabel;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    DsDefPecas: TDataSource;
    Label7: TLabel;
    DBEdDtEntrada: TdmkDBEdit;
    Label8: TLabel;
    DBEdCliVenda: TdmkDBEdit;
    DBGrid1: TDBGrid;
    QrWBPallet: TmySQLQuery;
    QrWBPalletCodigo: TIntegerField;
    QrWBPalletNome: TWideStringField;
    DsWBPallet: TDataSource;
    Panel3: TPanel;
    QrWBMovIts: TmySQLQuery;
    DsWBMovIts: TDataSource;
    QrWBMovItsNO_Pallet: TWideStringField;
    QrWBMovItsCodigo: TIntegerField;
    QrWBMovItsControle: TIntegerField;
    QrWBMovItsMovimCod: TIntegerField;
    QrWBMovItsMovimNiv: TIntegerField;
    QrWBMovItsEmpresa: TIntegerField;
    QrWBMovItsTerceiro: TIntegerField;
    QrWBMovItsMovimID: TIntegerField;
    QrWBMovItsDataHora: TDateTimeField;
    QrWBMovItsPallet: TIntegerField;
    QrWBMovItsGraGruX: TIntegerField;
    QrWBMovItsPecas: TFloatField;
    QrWBMovItsPesoKg: TFloatField;
    QrWBMovItsAreaM2: TFloatField;
    QrWBMovItsAreaP2: TFloatField;
    QrWBMovItsSrcMovID: TIntegerField;
    QrWBMovItsSrcNivel1: TIntegerField;
    QrWBMovItsSrcNivel2: TIntegerField;
    QrWBMovItsLk: TIntegerField;
    QrWBMovItsDataCad: TDateField;
    QrWBMovItsDataAlt: TDateField;
    QrWBMovItsUserCad: TIntegerField;
    QrWBMovItsUserAlt: TIntegerField;
    QrWBMovItsAlterWeb: TSmallintField;
    QrWBMovItsAtivo: TSmallintField;
    QrWBMovItsSdoVrtPeca: TFloatField;
    QrWBMovItsNO_TERCEIRO: TWideStringField;
    Panel5: TPanel;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label9: TLabel;
    EdSrcNivel1: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    Label17: TLabel;
    Panel6: TPanel;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    EdAreaP2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    LaPecas: TLabel;
    Label4: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SBPallet: TSpeedButton;
    EdValorT: TdmkEdit;
    Label18: TLabel;
    QrWBMovItsSdoVrtArM2: TFloatField;
    QrWBMovItsValorT: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdSrcMovIDChange(Sender: TObject);
    procedure EdSrcNivel1Change(Sender: TObject);
    procedure EdSrcNivel2Change(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure SBPalletClick(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    //
    procedure ReopenWBInnIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure ReopenWBMovIts((*GraGruX: Integer*));
    procedure HabilitaInclusao();

  public
    { Public declarations }
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmWBOutKno: TFmWBOutKno;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
WBOutCab, Principal;

{$R *.DFM}

procedure TFmWBOutKno.BtOKClick(Sender: TObject);
const
  Observ = '';
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  MovimTwn   = 0;
var
  DataHora: String;
  SrcMovID, SrcNivel1, SrcNivel2,
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, CliVenda: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
begin
  SrcMovID       := EdSrcMovID.ValueVariant;
  SrcNivel1      := EdSrcNivel1.ValueVariant;
  SrcNivel2      := EdSrcNivel2.ValueVariant;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  Terceiro       := QrWBMovItsTerceiro.Value;
  CliVenda       := Geral.IMV(DBEdCliVenda.Text);
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidVenda;
  MovimNiv       := eminSemNiv;
  Pallet         := QrWBMovItsPallet.Value; //EdPallet.ValueVariant;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  Pecas          := -EdPecas.ValueVariant;
  PesoKg         := -EdPesoKg.ValueVariant;
  AreaM2         := -EdAreaM2.ValueVariant;
  AreaP2         := -EdAreaP2.ValueVariant;
  ValorT         := -EdValorT.ValueVariant;
  //
  if Dmod.WBFic(GraGruX, Empresa, Terceiro, Pallet, Pecas, AreaM2, PesoKg, ValorT,
  EdGraGruX, EdPallet, EdPecas, EdAreaM2, EdValorT) then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('wbmovits', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if Dmod.InsUpdWBMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Terceiro,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle) then
  begin
    Dmod.AtualizaSaldoVirtualWBMovIts(SrcNivel2);
    Dmod.AtualizaTotaisWBXxxCab('wboutcab', MovimCod);
    FmWBOutCab.LocCod(Codigo, Codigo);
    ReopenWBInnIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdPecas.ValueVariant       := 0;
      EdPesoKg.ValueVariant      := 0;
      EdAreaM2.ValueVariant      := 0;
      EdAreaP2.ValueVariant      := 0;
      EdValorT.ValueVariant      := 0;
      EdPallet.ValueVariant      := 0;//DefineProximoPallet();
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmWBOutKno.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWBOutKno.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmWBOutKno.DBGrid1DblClick(Sender: TObject);
var
  Valor: Double;
begin
  EdSrcMovID.ValueVariant := QrWBMovItsMovimID.Value;
  EdSrcNivel1.ValueVariant := QrWBMovItsCodigo.Value;
  EdSrcNivel2.ValueVariant := QrWBMovItsControle.Value;
  //
  EdPesoKg.ValueVariant := 0;
  EdAreaP2.ValueVariant := 0;
  // Deve ser depois dos zerados!
  EdPecas.ValueVariant  := QrWBMovItsSdoVrtPeca.Value;
  EdAreaM2.ValueVariant := QrWBMovItsSdoVrtArM2.Value;
  Valor := 0;
  case TEstqNivCtrl(Dmod.QrControleWBNivGer.Value) of
    encPecas: ;//Valor := 0;
    encArea: ;//Valor := 0;
    encValor:
    begin
      if (QrWBMovItsSdoVrtArM2.Value > 0) and (QrWBMovItsAreaM2.Value > 0) then
        Valor := QrWBMovItsSdoVrtArM2.Value *
        (QrWBMovItsValorT.Value / QrWBMovItsAreaM2.Value)
      else
      if QrWBMovItsPecas.Value > 0 then
        Valor := QrWBMovItsSdoVrtPeca.Value *
        (QrWBMovItsValorT.Value / QrWBMovItsPecas.Value);
    end;
    else
    begin
      //Valor := 0;
      Geral.MB_Aviso('N�vel de controle de estoque n�o implementado! [1]');
    end;
  end;
  EdValorT.ValueVariant := Valor;
end;

{
function TFmWBOutKno.DefineProximoPallet(): String;
var
  Txt: String;
begin
  Result := '';
  Txt := EdPallet.Text;
  if Geral.SoNumero_TT(Txt) = Txt then
  begin
    try
      Result := Geral.FI64(Geral.I64(EdPallet.Text) + 1);
    except
      Result := '';
    end;
  end else
    Result := '';
end;
}

procedure TFmWBOutKno.EdSrcNivel2Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmWBOutKno.EdSrcMovIDChange(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmWBOutKno.EdGraGruXChange(Sender: TObject);
begin
  ReopenWBMovIts();
end;

procedure TFmWBOutKno.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmWBOutKno.EdSrcNivel1Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmWBOutKno.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  DBEdCliVenda.DataSource   := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmWBOutKno.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DBGrid1.Align := alClient;
  FUltGGX := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED ',
  'FROM wbmprcab wmp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBPallet, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM wbpallet ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmWBOutKno.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWBOutKno.HabilitaInclusao;
var
  Habilita: Boolean;
begin
  Habilita :=
    (EdSrcMovID.ValueVariant <> 0) and
    (EdSrcNivel1.ValueVariant <> 0) and
    (EdSrcNivel2.ValueVariant <> 0);
  //
  BtOK.Enabled := Habilita;
end;

procedure TFmWBOutKno.ReopenWBInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmWBOutKno.ReopenWBMovIts((*GraGruX: Integer*));
var
  GraGruX, Empresa: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  Empresa := Geral.IMV(DBEdEmpresa.Text);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBMovIts, Dmod.MyDB, [
  'SELECT pal.Nome NO_Pallet, wmi.*,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO  ',
  'FROM wbmovits wmi ',
  'LEFT JOIN wbpallet pal ON pal.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades ent ON ent.Codigo=wmi.Terceiro',
  'WHERE wmi.Empresa=' + Geral.FF0(Empresa),
  'AND wmi.GraGruX=' + Geral.FF0(GraGruX),
  'AND ( ',
  '  wmi.MovimID=' + Geral.FF0(Integer(emidCompra)),
  '  OR  ',
  '  wmi.SrcMovID<>0 ',
  ') ',
  'AND (wmi.SdoVrtPeca > 0 ',
  // Precisa?
  //'OR wmi.SdoVrtArM2 > 0 ',
  ') ',
  'ORDER BY DataHora, Pallet ',
  '']);
  //
end;

procedure TFmWBOutKno.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormWBPallet();
  UMyMod.SetaCodigoPesquisado(EdPallet, CBPallet, QrWBPallet, VAR_CADASTRO);
end;

procedure TFmWBOutKno.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

end.
