unit FrmlRibCusCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkDBLookupComboBox, dmkEditCB, Vcl.Grids, Vcl.DBGrids, dmkMemo, dmkEditCalc,
  frxClass, frxDBSet;

type
  TFmFrmlRibCusCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrFrmlRibCusCab: TMySQLQuery;
    DsFrmlRibCusCab: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrFormulasCal: TMySQLQuery;
    QrFormulasCalHHMM_P: TWideStringField;
    QrFormulasCalHHMM_R: TWideStringField;
    QrFormulasCalHHMM_T: TWideStringField;
    QrFormulasCalNumero: TIntegerField;
    QrFormulasCalNome: TWideStringField;
    QrFormulasCalClienteI: TIntegerField;
    QrFormulasCalTipificacao: TIntegerField;
    QrFormulasCalDataI: TDateField;
    QrFormulasCalDataA: TDateField;
    QrFormulasCalTecnico: TWideStringField;
    QrFormulasCalTempoR: TIntegerField;
    QrFormulasCalTempoP: TIntegerField;
    QrFormulasCalTempoT: TIntegerField;
    QrFormulasCalHorasR: TIntegerField;
    QrFormulasCalHorasP: TIntegerField;
    QrFormulasCalHorasT: TIntegerField;
    QrFormulasCalHidrica: TIntegerField;
    QrFormulasCalLinhaTE: TIntegerField;
    QrFormulasCalCaldeira: TIntegerField;
    QrFormulasCalSetor: TIntegerField;
    QrFormulasCalEspessura: TIntegerField;
    QrFormulasCalPeso: TFloatField;
    QrFormulasCalQtde: TFloatField;
    QrFormulasCalLk: TIntegerField;
    QrFormulasCalDataCad: TDateField;
    QrFormulasCalDataAlt: TDateField;
    QrFormulasCalUserCad: TIntegerField;
    QrFormulasCalUserAlt: TIntegerField;
    QrFormulasCalAtivo: TSmallintField;
    QrFormulasCalRetrabalho: TSmallintField;
    DsFormulasCal: TDataSource;
    EdReceiCal: TdmkEditCB;
    Label3: TLabel;
    CBReceiCal: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdReceiCur: TdmkEditCB;
    CBReceiCur: TdmkDBLookupComboBox;
    QrFormulasCur: TMySQLQuery;
    QrFormulasCurHHMM_P: TWideStringField;
    QrFormulasCurHHMM_R: TWideStringField;
    QrFormulasCurHHMM_T: TWideStringField;
    QrFormulasCurNumero: TIntegerField;
    QrFormulasCurNome: TWideStringField;
    QrFormulasCurClienteI: TIntegerField;
    QrFormulasCurTipificacao: TIntegerField;
    QrFormulasCurDataI: TDateField;
    QrFormulasCurDataA: TDateField;
    QrFormulasCurTecnico: TWideStringField;
    QrFormulasCurTempoR: TIntegerField;
    QrFormulasCurTempoP: TIntegerField;
    QrFormulasCurTempoT: TIntegerField;
    QrFormulasCurHorasR: TIntegerField;
    QrFormulasCurHorasP: TIntegerField;
    QrFormulasCurHorasT: TIntegerField;
    QrFormulasCurHidrica: TIntegerField;
    QrFormulasCurLinhaTE: TIntegerField;
    QrFormulasCurCaldeira: TIntegerField;
    QrFormulasCurSetor: TIntegerField;
    QrFormulasCurEspessura: TIntegerField;
    QrFormulasCurPeso: TFloatField;
    QrFormulasCurQtde: TFloatField;
    QrFormulasCurLk: TIntegerField;
    QrFormulasCurDataCad: TDateField;
    QrFormulasCurDataAlt: TDateField;
    QrFormulasCurUserCad: TIntegerField;
    QrFormulasCurUserAlt: TIntegerField;
    QrFormulasCurAtivo: TSmallintField;
    QrFormulasCurRetrabalho: TSmallintField;
    DsFormulasCur: TDataSource;
    RGOrigemPrc: TdmkRadioGroup;
    Label10: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    MeObserv: TdmkMemo;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    QrFrmlRibCusCabNO_ReceiCal: TWideStringField;
    QrFrmlRibCusCabNO_ReceiCur: TWideStringField;
    QrFrmlRibCusCabCodigo: TIntegerField;
    QrFrmlRibCusCabNome: TWideStringField;
    QrFrmlRibCusCabReceiCal: TIntegerField;
    QrFrmlRibCusCabReceiCur: TIntegerField;
    QrFrmlRibCusCabOrigemPrc: TIntegerField;
    QrFrmlRibCusCabDataHora: TDateTimeField;
    QrFrmlRibCusCabPecas: TFloatField;
    QrFrmlRibCusCabPesoNat: TFloatField;
    QrFrmlRibCusCabPesoPDA: TFloatField;
    QrFrmlRibCusCabPesoPTA: TFloatField;
    QrFrmlRibCusCabQbrNatPDA: TFloatField;
    QrFrmlRibCusCabQbrNatPTA: TFloatField;
    QrFrmlRibCusCabMediaM2: TFloatField;
    QrFrmlRibCusCabMediaP2: TFloatField;
    QrFrmlRibCusCabBRL_USD: TFloatField;
    QrFrmlRibCusCabUSD_EUR: TFloatField;
    QrFrmlRibCusCabSiglaOTH: TWideStringField;
    QrFrmlRibCusCabBRL_OTH: TFloatField;
    QrFrmlRibCusCabObserv: TWideStringField;
    QrFrmlRibCusCabLk: TIntegerField;
    QrFrmlRibCusCabDataCad: TDateField;
    QrFrmlRibCusCabDataAlt: TDateField;
    QrFrmlRibCusCabUserCad: TIntegerField;
    QrFrmlRibCusCabUserAlt: TIntegerField;
    QrFrmlRibCusCabAlterWeb: TSmallintField;
    QrFrmlRibCusCabAWServerID: TIntegerField;
    QrFrmlRibCusCabAWStatSinc: TSmallintField;
    QrFrmlRibCusCabAtivo: TSmallintField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBRGOrigemPrc: TDBRadioGroup;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBMemo2: TDBMemo;
    Label33: TLabel;
    DBEdit17: TDBEdit;
    PnGrades: TPanel;
    DBGCal: TDBGrid;
    EdPecas: TdmkEdit;
    EdPesoNat: TdmkEdit;
    EdQbrNatPDA: TdmkEdit;
    EdPesoPDA: TdmkEdit;
    EdQbrNatPTA: TdmkEdit;
    EdPesoPTA: TdmkEdit;
    EdBRL_USD: TdmkEdit;
    EdUSD_EUR: TdmkEdit;
    EdBRL_OTH: TdmkEdit;
    EdSiglaOTH: TdmkEdit;
    EdMediaM2: TdmkEditCalc;
    EdMediaP2: TdmkEditCalc;
    Label34: TLabel;
    EdClienteI: TdmkEditCB;
    CBClienteI: TdmkDBLookupComboBox;
    QrFrmlRibCusCabClienteI: TIntegerField;
    Label35: TLabel;
    DBEdit18: TDBEdit;
    BtRecalcula: TBitBtn;
    QrRec: TMySQLQuery;
    QrRecPQ: TIntegerField;
    QrRecCustoPadrao: TFloatField;
    QrRecCusto: TFloatField;
    QrRecCustoMan: TFloatField;
    QrRecSetor: TIntegerField;
    QrRecOrdem: TIntegerField;
    QrRecPorcent: TFloatField;
    QrCaleiro: TMySQLQuery;
    DsCaleiro: TDataSource;
    QrCurtimento: TMySQLQuery;
    DsCurtimento: TDataSource;
    QrCaleiroNO_PQ: TWideStringField;
    QrCaleiroCodigo: TIntegerField;
    QrCaleiroControle: TIntegerField;
    QrCaleiroNumero: TIntegerField;
    QrCaleiroSetor: TIntegerField;
    QrCaleiroOrdem: TIntegerField;
    QrCaleiroInsumo: TIntegerField;
    QrCaleiroPercent: TFloatField;
    QrCaleiroPeso: TFloatField;
    QrCaleiroCusto: TFloatField;
    QrCaleiroTotal: TFloatField;
    QrCaleiroPercPart: TFloatField;
    QrCaleiroCustPart: TFloatField;
    QrCaleiroLk: TIntegerField;
    QrCaleiroDataCad: TDateField;
    QrCaleiroDataAlt: TDateField;
    QrCaleiroUserCad: TIntegerField;
    QrCaleiroUserAlt: TIntegerField;
    QrCaleiroAlterWeb: TSmallintField;
    QrCaleiroAWServerID: TIntegerField;
    QrCaleiroAWStatSinc: TSmallintField;
    QrCaleiroAtivo: TSmallintField;
    QrCurtimentoNO_PQ: TWideStringField;
    QrCurtimentoCodigo: TIntegerField;
    QrCurtimentoControle: TIntegerField;
    QrCurtimentoNumero: TIntegerField;
    QrCurtimentoSetor: TIntegerField;
    QrCurtimentoOrdem: TIntegerField;
    QrCurtimentoInsumo: TIntegerField;
    QrCurtimentoPercent: TFloatField;
    QrCurtimentoPeso: TFloatField;
    QrCurtimentoCusto: TFloatField;
    QrCurtimentoTotal: TFloatField;
    QrCurtimentoPercPart: TFloatField;
    QrCurtimentoCustPart: TFloatField;
    QrCurtimentoLk: TIntegerField;
    QrCurtimentoDataCad: TDateField;
    QrCurtimentoDataAlt: TDateField;
    QrCurtimentoUserCad: TIntegerField;
    QrCurtimentoUserAlt: TIntegerField;
    QrCurtimentoAlterWeb: TSmallintField;
    QrCurtimentoAWServerID: TIntegerField;
    QrCurtimentoAWStatSinc: TSmallintField;
    QrCurtimentoAtivo: TSmallintField;
    frxQUI_RECEI_028_001: TfrxReport;
    frxDsFrmlRibCusCab: TfrxDBDataset;
    frxDsCaleiro: TfrxDBDataset;
    frxDsCurtimento: TfrxDBDataset;
    QrT: TMySQLQuery;
    QrI: TMySQLQuery;
    QrTTotal: TFloatField;
    QrITotal: TFloatField;
    QrIControle: TIntegerField;
    QrRecMoedaPadrao: TSmallintField;
    DBGrid1: TDBGrid;
    frxDsT: TfrxDBDataset;
    QrFrmlRibCusCabTipificacao: TIntegerField;
    QrRecMoedaMan: TSmallintField;
    QrCliInt: TMySQLQuery;
    QrCliIntNOMECI: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    DsCliInt: TDataSource;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFrmlRibCusCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFrmlRibCusCabBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure EdPesoPDARedefinido(Sender: TObject);
    procedure EdQbrNatPTARedefinido(Sender: TObject);
    procedure EdQbrNatPDAChange(Sender: TObject);
    procedure EdPesoPDAChange(Sender: TObject);
    procedure EdQbrNatPTAChange(Sender: TObject);
    procedure EdPesoPTAChange(Sender: TObject);
    procedure EdPesoNatChange(Sender: TObject);
    procedure BtRecalculaClick(Sender: TObject);
    procedure QrFrmlRibCusCabAfterScroll(DataSet: TDataSet);
    procedure QrFrmlRibCusCabBeforeClose(DataSet: TDataSet);
    procedure frxQUI_RECEI_028_001GetValue(const VarName: string;
      var Value: Variant);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    function  CalculaQuebra(Percentual: Double): Double;
    procedure RecalculaTudo();
    procedure ReopenCaleiroECurtimento();

  public
    { Public declarations }
  end;

var
  FmFrmlRibCusCab: TFmFrmlRibCusCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, AppListas;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFrmlRibCusCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFrmlRibCusCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFrmlRibCusCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFrmlRibCusCab.DefParams;
begin
  VAR_GOTOTABELA := 'frmlribcuscab';
  VAR_GOTOMYSQLTABLE := QrFrmlRibCusCab;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cal.Nome NO_ReceiCal, cur.Nome NO_ReceiCur, ');
  VAR_SQLx.Add('cur.Tipificacao, rcc.* ');
  VAR_SQLx.Add('FROM frmlribcuscab rcc ');
  VAR_SQLx.Add('LEFT JOIN formulas cal ON cal.Numero=rcc.ReceiCal ');
  VAR_SQLx.Add('LEFT JOIN formulas cur ON cur.Numero=rcc.ReceiCur ');
  VAR_SQLx.Add('WHERE rcc.Codigo>0 ');
  //
  VAR_SQL1.Add('AND rcc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND rcc.Nome Like :P0');
  //
end;

procedure TFmFrmlRibCusCab.EdPesoNatChange(Sender: TObject);
begin
  RecalculaTudo();
end;

procedure TFmFrmlRibCusCab.EdPesoPDAChange(Sender: TObject);
begin
  RecalculaTudo();
end;

procedure TFmFrmlRibCusCab.EdPesoPDARedefinido(Sender: TObject);
begin
  RecalculaTudo();
end;

procedure TFmFrmlRibCusCab.EdPesoPTAChange(Sender: TObject);
begin
  RecalculaTudo();
end;

procedure TFmFrmlRibCusCab.EdQbrNatPDAChange(Sender: TObject);
begin
  RecalculaTudo();
end;

procedure TFmFrmlRibCusCab.EdQbrNatPTAChange(Sender: TObject);
begin
  RecalculaTudo();
end;

procedure TFmFrmlRibCusCab.EdQbrNatPTARedefinido(Sender: TObject);
begin
  RecalculaTudo();
end;

function TFmFrmlRibCusCab.CalculaQuebra(Percentual: Double): Double;
var
  Peso, Fator: Double;
begin
  Fator := 1 - (Percentual / 100);
  Peso := EdPesoNat.ValueVariant * Fator;
  //
  Result := Peso;
end;

procedure TFmFrmlRibCusCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFrmlRibCusCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFrmlRibCusCab.RecalculaTudo();
var
  PesoNat, PesoPDA, PesoPTA, QbrNatPDA, QbrNatPTA: Double;
begin
  PesoNat        := EdPesoNat.ValueVariant;
  PesoPDA        := EdPesoPDA.ValueVariant;
  PesoPTA        := EdPesoPTA.ValueVariant;
  QbrNatPDA      := EdQbrNatPDA.ValueVariant;
  QbrNatPTA      := EdQbrNatPTA.ValueVariant;
  //
  if EdPesoPDA.Focused then
  begin
    if PesoNat > 0 then
      QbrNatPDA := ((PesoNat - PesoPDA) / PesoNat) * 100
    else
      QbrNatPDA := 0;
  end else
  //if EdQbrNatPDA.Focused then
    PesoPDA := CalculaQuebra(QbrNatPDA);

  //
  if EdPesoPTA.Focused then
  begin
    if PesoNat > 0 then
      QbrNatPTA := ((PesoNat - PesoPTA) / PesoNat) * 100
    else
      QbrNatPTA := 0;
  end else
  //if EdQbrNatPTA.Focused then
    PesoPTA := CalculaQuebra(QbrNatPTA);
  //
  //EdPesoNat.ValueVariant    := PesoNat;
  if not EdPesoPDA.Focused then
    EdPesoPDA.ValueVariant    := PesoPDA;
  if not EdPesoPTA.Focused then
    EdPesoPTA.ValueVariant    := PesoPTA;
  if not EdQbrNatPDA.Focused then
    EdQbrNatPDA.ValueVariant  := QbrNatPDA;
  if not EdQbrNatPTA.Focused then
    EdQbrNatPTA.ValueVariant  := QbrNatPTA;
  //
end;

procedure TFmFrmlRibCusCab.ReopenCaleiroECurtimento();
var
  Numero: Integer;
begin
  Numero := QrFrmlRibCusCabReceiCal.Value;
  if Numero = 0 then
    Numero := -999999;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCaleiro, Dmod.MyDB, [
  'SELECT pq_.Nome NO_PQ, its.* ',
  'FROM frmlribcusits its',
  'LEFT JOIN pq pq_ ON pq_.Codigo=its.Insumo',
  'WHERE its.Codigo=' + Geral.FF0(QrFrmlRibCusCabCodigo.Value),
  'AND its.Numero=' + Geral.FF0(Numero),
  '']);
  //
  Numero := QrFrmlRibCusCabReceiCur.Value;
  if Numero = 0 then
    Numero := -999999;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCurtimento, Dmod.MyDB, [
  'SELECT pq_.Nome NO_PQ, its.* ',
  'FROM frmlribcusits its',
  'LEFT JOIN pq pq_ ON pq_.Codigo=its.Insumo',
  'WHERE its.Codigo=' + Geral.FF0(QrFrmlRibCusCabCodigo.Value),
  'AND its.Numero=' + Geral.FF0(Numero),
  '']);
  //
end;

procedure TFmFrmlRibCusCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFrmlRibCusCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFrmlRibCusCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFrmlRibCusCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFrmlRibCusCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFrmlRibCusCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrmlRibCusCab.BtAlteraClick(Sender: TObject);
//var
  //Filial: Integer;
begin
  if (QrFrmlRibCusCab.State <> dsInactive) and (QrFrmlRibCusCab.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFrmlRibCusCab, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'frmlribcuscab');
    //Filial := DModG.ObtemFilialDeEntidade(QrFrmlRibCusCabClienteI.Value);
    //EdEmpresa.ValueVariant := Filial;
    //CBEmpresa.KeyValue := Filial;
  end;
end;

procedure TFmFrmlRibCusCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFrmlRibCusCabCodigo.Value;
  Close;
end;

procedure TFmFrmlRibCusCab.BtConfirmaClick(Sender: TObject);
var
  Nome, //DataHora,
  SiglaOTH, Observ: String;
  Codigo, ReceiCal, ReceiCur, OrigemPrc, ClienteI: Integer;
  Pecas, PesoNat, PesoPDA, PesoPTA, QbrNatPDA, QbrNatPTA, MediaM2, MediaP2, BRL_USD, USD_EUR, BRL_OTH: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  ReceiCal       := EdReceiCal.ValueVariant;
  ReceiCur       := EdReceiCur.ValueVariant;
  OrigemPrc      := RGOrigemPrc.ItemIndex;
  //DataHora       := ;
  Pecas          := EdPecas.ValueVariant;
  PesoNat        := EdPesoNat.ValueVariant;
  PesoPDA        := EdPesoPDA.ValueVariant;
  PesoPTA        := EdPesoPTA.ValueVariant;
  QbrNatPDA      := EdQbrNatPDA.ValueVariant;
  QbrNatPTA      := EdQbrNatPTA.ValueVariant;
  MediaM2        := EdMediaM2.ValueVariant;
  MediaP2        := EdMediaP2.ValueVariant;
  BRL_USD        := EdBRL_USD.ValueVariant;
  USD_EUR        := EdUSD_EUR.ValueVariant;
  SiglaOTH       := EdSiglaOTH.ValueVariant;
  BRL_OTH        := EdBRL_OTH.ValueVariant;
  Observ         := MeObserv.Text;
  //(*Empresa*)       if not DModG.ObtemEmpresaSelecionada(EdEmpresa, ClienteI) then
    //Exit;
  ClienteI       := EdClienteI.ValueVariant;
  if MyObjects.FIC(ClienteI = 0, EdClienteI, 'Informe o cliente Interno!') then
    Exit;
  if MyObjects.FIC(Nome = EmptyStr, EdNome, 'Informe a descri��o!') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('frmlribcuscab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frmlribcuscab', False, [
  'Nome', 'ReceiCal', 'ReceiCur',
  'ClienteI', 'OrigemPrc', //'DataHora',
  'Pecas',
  'PesoNat', 'PesoPDA', 'PesoPTA',
  'QbrNatPDA', 'QbrNatPTA', 'MediaM2',
  'MediaP2', 'BRL_USD', 'USD_EUR',
  'SiglaOTH', 'BRL_OTH', 'Observ'], [
  'Codigo'], [
  Nome, ReceiCal, ReceiCur,
  ClienteI, OrigemPrc, //DataHora,
  Pecas,
  PesoNat, PesoPDA, PesoPTA,
  QbrNatPDA, QbrNatPTA, MediaM2,
  MediaP2, BRL_USD, USD_EUR,
  SiglaOTH, BRL_OTH, Observ], [
  Codigo],True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if SQLType = stUpd then
      BtRecalculaClick(Self);
    //
  end;
end;

procedure TFmFrmlRibCusCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'frmlribcuscab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFrmlRibCusCab.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFrmlRibCusCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'frmlribcuscab');
  //DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmFrmlRibCusCab.BtRecalculaClick(Sender: TObject);
var
  Codigo, Controle: Integer;
  //
  function  DefineCusto(Peso: Double): Double;
  var
    Preco, Cambio: Double;
  begin
    case QrFrmlRibCusCabOrigemPrc.Value of
      //Estoque >> BRL
      0: Cambio := 1.000000;
      // Cadastro, Manual
      1:
      begin
        case QrRecMoedaPadrao.Value of
          // BRL
          0: Cambio := 1.000000;
          // USD
          1: Cambio := QrFrmlRibCusCabBRL_USD.Value;
          // EUR
          2: Cambio := QrFrmlRibCusCabBRL_USD.Value * QrFrmlRibCusCabUSD_EUR.Value;
          // ???
          3: Cambio := QrFrmlRibCusCabBRL_OTH.Value;
          else Cambio := 0.000000;
        end;
      end;
      2:
      begin
        case QrRecMoedaMan.Value of
          // BRL
          0: Cambio := 1.000000;
          // USD
          1: Cambio := QrFrmlRibCusCabBRL_USD.Value;
          // EUR
          2: Cambio := QrFrmlRibCusCabBRL_USD.Value * QrFrmlRibCusCabUSD_EUR.Value;
          // ???
          3: Cambio := QrFrmlRibCusCabBRL_OTH.Value;
          else Cambio := 0.000000;
        end;
      end;
      else Cambio := 0.000000;
    end;
    //
    case QrFrmlRibCusCabOrigemPrc.Value of
      //Estoque >> BRL
      0: Preco := QrRecCusto.Value;
      // Cadastro
      1: Preco := QrRecCustoPadrao.Value;
      //Manual
      2: Preco := QrRecCustoMan.Value;
      else Preco := 0;
    end;
    //
    Result := Preco * Cambio;
  end;
  //
  procedure CalculaReceita(Receita: Integer; PesoBase: Double);
  var
    Numero, Setor, Ordem, Insumo: Integer;
    Percent, Peso, Custo, Total, PercPart, CustPart, Media: Double;
    SQLType: TSQLType;
    sMedia: String;
  begin
    if QrFrmlRibCusCabPecas.Value > 0 then
      Media := PesoBase / QrFrmlRibCusCabPecas.Value
    else
      Media := 0.0000;
    //
    sMedia := Geral.FFT_Dot(Media, 4, siNegativo);
    UnDmkDAC_PF.AbreMySQLQuery0(QrRec, Dmod.MyDB, [
    'SELECT frm.Setor, its.Ordem, SUM(its.Porcent) Porcent, pqc.PQ,',
    'pqc.MoedaPadrao, pqc.Custo, pqc.CustoPadrao, pqc.CustoMan, pqc.MoedaMan ',
    'FROM formulasits its ',
    'LEFT JOIN formulas frm ON frm.Numero=its.Numero ',
    'LEFT JOIN pq pq_ ON pq_.Codigo=its.Produto ',
    'LEFT JOIN pqcli pqc ON pqc.PQ=its.Produto AND pqc.CI=' + Geral.FF0(QrFrmlRibCusCabClienteI.Value),
    'WHERE its.Numero=' + Geral.FF0(Receita),
    'AND (NOT pqc.Controle IS NULL ',
    '  OR ',
    '  (its.Porcent > 0 AND its.Produto > 0 AND pq_.GGXNiv2 IN (2)) ',
    ') ',
    'AND ((its.Minimo + its.Maximo=0) ',
    '    OR(' + sMedia + ' >= its.Minimo AND ' + sMedia + ' < its.Maximo)) ',
    'GROUP BY pqc.PQ',
    '']);
    //Geral.MB_Teste(QrRec.SQL.Text);
    QrRec.First;
    SQLType        := stIns;
    while not QrRec.Eof do
    begin
      Controle       := Controle + 1;
      Numero         := Receita;
      Setor          := QrRecSetor.Value;
      Ordem          := QrRecOrdem.Value;
      Insumo         := QrRecPQ.Value;
      Percent        := QrRecPorcent.Value;
      Peso           := Percent * PesoBase / 100;
      Custo          := DefineCusto(Peso);
      Total          := Custo * Peso;
      PercPart       := 0.000000;
      CustPart       := 0.000000;
      // := UMyMod.BPGS1I32('frmlribcusits', 'Codigo', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frmlribcusits', False, [
      'Numero', 'Setor', 'Ordem',
      'Insumo', 'Percent', 'Peso',
      'Custo', 'Total', 'PercPart',
      'CustPart'], [
      'Codigo', 'Controle'], [
      Numero, Setor, Ordem,
      Insumo, Percent, Peso,
      Custo, Total, PercPart,
      CustPart], [
      Codigo, Controle], True);
      //
      QrRec.Next;
    end;
  end;
var
  TTotal, ITotal, PercPart, CustPart, PesoNat: Double;
  IControle: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Iniciando atualiza��o. Deletando dados anteriores');
  Screen.Cursor := crHourGlass;
  try
    Controle := 0;
    Codigo   := QrFrmlRibCusCabCodigo.Value;
    PesoNat  := QrFrmlRibCusCabPesoNat.Value;
    Dmod.MyDB.Execute('DELETE FROM frmlribcusits WHERE Codigo=' +
      Geral.FF0(Codigo));
    //
    if QrFrmlRibCusCabReceiCal.Value <> 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando caleiro');
      CalculaReceita(QrFrmlRibCusCabReceiCal.Value, QrFrmlRibCusCabPesoPDA.Value);
    end;
    //
    if QrFrmlRibCusCabReceiCur.Value <> 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando curtimento');
      CalculaReceita(QrFrmlRibCusCabReceiCur.Value, QrFrmlRibCusCabPesoPTA.Value);
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando participa��o dos itens no total');
    UnDmkDAC_PF.AbreMySQLQuery0(QrT, Dmod.MyDB, [
    'SELECT SUM(Total) Total  ',
    'FROM frmlribcusits ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    TTotal := QrTTotal.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrI, Dmod.MyDB, [
    'SELECT Controle, Total  ',
    'FROM frmlribcusits ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    //
    QrI.First;
    while not QrI.Eof do
    begin
      ITotal    := QrITotal.Value;
      IControle := QrIControle.Value;
      if TTotal > 0 then
        PercPart  := ITotal / TTotal * 100
      else
        PercPart  := 0.0000;
      if PesoNat > 0 then
        CustPart := ITotal / PesoNat
      else
        CustPart := 0.0000;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'frmlribcusits', False, [
      'PercPart', 'CustPart'], [
      'Codigo', 'Controle'], [
      PercPart, CustPart], [
      Codigo, IControle], True);
      //
      QrI.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Data/hora da atualiza��o');
    Dmod.MyDB.Execute('UPDATE frmlribcuscab SET DataHora="' +
      Geral.FDT(Now(), 109) + '" WHERE Codigo=' + Geral.FF0(Codigo));
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
  LocCod(Codigo, Codigo);
  //ReopenCaleiroECurtimento();
end;

procedure TFmFrmlRibCusCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PnGrades.Align := alClient;
  //
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulasCal, Dmod.MyDB, [
  'SELECT frm.* ',
  'FROM formulas frm',
  'LEFT JOIN listasetores lse ON lse.Codigo=frm.Setor',
  'WHERE frm.Ativo=1',
  'AND lse.TpSetor=1',
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulasCur, Dmod.MyDB, [
  'SELECT frm.* ',
  'FROM formulas frm',
  'LEFT JOIN listasetores lse ON lse.Codigo=frm.Setor',
  'WHERE frm.Ativo=1',
  'AND lse.TpSetor=2',
  '']);
  //
  CriaOForm;
end;

procedure TFmFrmlRibCusCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFrmlRibCusCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFrmlRibCusCab.SbImprimeClick(Sender: TObject);
begin
  if (QrFrmlRibCusCab.State = dsInactive) or (QrFrmlRibCusCab.RecordCount > 0) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrT, Dmod.MyDB, [
    'SELECT SUM(Total) Total  ',
    'FROM frmlribcusits ',
    'WHERE Codigo=' + Geral.FF0(QrFrmlRibCusCabCodigo.Value),
    '']);
    //
    MyObjects.frxDefineDataSets(frxQUI_RECEI_028_001, [
    DModG.frxDsDono,
    frxDsFrmlRibCusCab,
    frxDsT,
    frxDsCaleiro,
    frxDsCurtimento
    ]);
    MyObjects.frxMostra(frxQUI_RECEI_028_001, 'Custo de Artigo');
  end else
    Geral.MB_Info('N�o h� dados a serem impressos!');
end;

procedure TFmFrmlRibCusCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFrmlRibCusCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFrmlRibCusCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFrmlRibCusCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFrmlRibCusCab.QrFrmlRibCusCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFrmlRibCusCab.QrFrmlRibCusCabAfterScroll(DataSet: TDataSet);
begin
  ReopenCaleiroECurtimento();
end;

procedure TFmFrmlRibCusCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFrmlRibCusCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFrmlRibCusCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'frmlribcuscab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFrmlRibCusCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrmlRibCusCab.frxQUI_RECEI_028_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_NO_EMPRESA') = 0 then
    Value := DModG.QrMasterEm.Value
  else
  if AnsiCompareText(VarName, 'VARF_NO_CLIINT') = 0 then
    Value := CBClienteI.Text
  else
  if AnsiCompareText(VarName, 'VAR_DATA_HORA') = 0 then
    Value := Geral.Maiusculas(Geral.FDT(Now(), 8), False)
  else
  if AnsiCompareText(VarName, 'VARF_TIPIFICACAO_CUR') = 0 then
    Value := UnAppListas.DefineNOMETipificacaoCouro(
    -2, QrFrmlRibCusCabTipificacao.Value)
  else
  if AnsiCompareText(VarName, 'VARF_ORIGEM_PRECOS') = 0 then
    Value := DBRGOrigemPrc.Items[DBRGOrigemPrc.ItemIndex]
  else
end;

procedure TFmFrmlRibCusCab.QrFrmlRibCusCabBeforeClose(DataSet: TDataSet);
begin
  QrCaleiro.Close;
  QrCurtimento.Close;
end;

procedure TFmFrmlRibCusCab.QrFrmlRibCusCabBeforeOpen(DataSet: TDataSet);
begin
  QrFrmlRibCusCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

