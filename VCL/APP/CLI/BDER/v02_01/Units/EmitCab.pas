unit EmitCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, Vcl.Mask, dmkEdit,
  mySQLDbTables, dmkEditCB, dmkDBLookupComboBox, dmkEditCalc, dmkCheckBox,
  UnDmkProcFunc, Variants;

type
  TFmEmitCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrEmit: TmySQLQuery;
    QrEmitCodigo: TIntegerField;
    QrEmitDataEmis: TDateTimeField;
    QrEmitStatus: TSmallintField;
    QrEmitNumero: TIntegerField;
    QrEmitNOMECI: TWideStringField;
    QrEmitNOMESETOR: TWideStringField;
    QrEmitTecnico: TWideStringField;
    QrEmitNOME: TWideStringField;
    QrEmitClienteI: TIntegerField;
    QrEmitTipificacao: TSmallintField;
    QrEmitTempoR: TSmallintField;
    QrEmitTempoP: TSmallintField;
    QrEmitSetor: TSmallintField;
    QrEmitEspessura: TWideStringField;
    QrEmitPeso: TFloatField;
    QrEmitQtde: TFloatField;
    QrEmitLk: TIntegerField;
    QrEmitDataCad: TDateField;
    QrEmitDataAlt: TDateField;
    QrEmitUserCad: TIntegerField;
    QrEmitUserAlt: TIntegerField;
    QrEmitAreaM2: TFloatField;
    QrEmitFulao: TWideStringField;
    QrEmitDefPeca: TWideStringField;
    QrEmitCusto: TFloatField;
    QrEmitObs: TWideStringField;
    QrEmitAlterWeb: TSmallintField;
    QrEmitTipific: TSmallintField;
    QrEmitAtivo: TSmallintField;
    QrEmitSetrEmi: TSmallintField;
    QrEmitCod_Espess: TIntegerField;
    QrEmitCodDefPeca: TIntegerField;
    QrEmitSourcMP: TSmallintField;
    QrEmitCustoTo: TFloatField;
    QrEmitCustoKg: TFloatField;
    QrEmitCustoM2: TFloatField;
    QrEmitCusUSM2: TFloatField;
    QrEmitEmitGru: TIntegerField;
    QrEmitRetrabalho: TSmallintField;
    QrEmitNO_EmitGru: TWideStringField;
    DsEmit: TDataSource;
    Panel5: TPanel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label15: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit7: TDBEdit;
    Label1: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    Label2: TLabel;
    DBEdit11: TDBEdit;
    QrEmitGru: TmySQLQuery;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    DsEmitGru: TDataSource;
    DsDefPecas1: TDataSource;
    QrDefPecas1: TmySQLQuery;
    QrDefPecas1Codigo: TIntegerField;
    QrDefPecas1Nome: TWideStringField;
    QrDefPecas1Grandeza: TSmallintField;
    QrDefPecas1Lk: TIntegerField;
    DsEspessuras1: TDataSource;
    QrEspessuras1: TmySQLQuery;
    QrEspessuras1Codigo: TIntegerField;
    QrEspessuras1Linhas: TWideStringField;
    QrEspessuras1EMCM: TFloatField;
    QrEspessuras1Lk: TIntegerField;
    GroupBox2: TGroupBox;
    PainelEscolhas: TPanel;
    Label4: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label10: TLabel;
    EdQtde: TdmkEdit;
    CBPeca: TdmkDBLookupComboBox;
    EdFulao: TdmkEdit;
    EdPeca: TdmkEditCB;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    EdEmitGru: TdmkEditCB;
    CBEmitGru: TdmkDBLookupComboBox;
    EdEspessura: TdmkEditCB;
    CBEspessura: TdmkDBLookupComboBox;
    QrDefPecas2: TmySQLQuery;
    DsDefPecas2: TDataSource;
    QrDefPecas2Codigo: TIntegerField;
    QrDefPecas2Nome: TWideStringField;
    QrDefPecas2Grandeza: TSmallintField;
    QrDefPecas2Lk: TIntegerField;
    QrEspessuras2: TmySQLQuery;
    QrEspessuras2Codigo: TIntegerField;
    QrEspessuras2Linhas: TWideStringField;
    QrEspessuras2EMCM: TFloatField;
    QrEspessuras2Lk: TIntegerField;
    DsEspessuras2: TDataSource;
    QrEmitSemiCodPeca: TIntegerField;
    QrEmitSemiTxtPeca: TWideStringField;
    QrEmitSemiPeso: TFloatField;
    QrEmitSemiQtde: TFloatField;
    QrEmitSemiAreaM2: TFloatField;
    QrEmitSemiRendim: TFloatField;
    QrEmitSemiCodEspe: TIntegerField;
    QrEmitSemiTxtEspe: TWideStringField;
    EdSemiCodEspReb: TdmkEditCB;
    Label24: TLabel;
    CBSemiCodEspReb: TdmkDBLookupComboBox;
    Label25: TLabel;
    QrRebaixe: TmySQLQuery;
    QrRebaixeCodigo: TIntegerField;
    QrRebaixeLinhas: TWideStringField;
    QrRebaixeEMCM: TFloatField;
    DsRebaixe: TDataSource;
    QrGraCorCad: TmySQLQuery;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    DsGraCorCad: TDataSource;
    EdGraCorCad: TdmkEdit;
    EdNoGraCorCad: TdmkEdit;
    QrEmitBRL_USD: TFloatField;
    QrEmitBRL_EUR: TFloatField;
    QrEmitDtaCambio: TDateField;
    QrEmitVSMovCod: TIntegerField;
    QrEmitHoraIni: TTimeField;
    QrEmitGraCorCad: TIntegerField;
    QrEmitNoGraCorCad: TWideStringField;
    QrEmitSemiCodEspReb: TIntegerField;
    Panel6: TPanel;
    Panel8: TPanel;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label12: TLabel;
    Label18: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label19: TLabel;
    EdSemiQtde: TdmkEdit;
    CBSemiDefPeca: TdmkDBLookupComboBox;
    EdSemiDefPeca: TdmkEditCB;
    EdSemiAreaM2: TdmkEditCalc;
    EdSemiAreaP2: TdmkEditCalc;
    EdSemiEspessura: TdmkEditCB;
    CBSemiEspessura: TdmkDBLookupComboBox;
    EdSemiPeso: TdmkEdit;
    BtCopiar: TBitBtn;
    EdSemiRendim: TdmkEdit;
    Panel9: TPanel;
    CkRetrabalho: TdmkCheckBox;
    Panel10: TPanel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtCopiarClick(Sender: TObject);
    procedure EdAreaM2Change(Sender: TObject);
    procedure EdSemiAreaM2Change(Sender: TObject);
    procedure EdGraCorCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FPesagem: Integer;
    REICalArea: Boolean;
    //
    procedure VerificaEspessura();
    procedure CalculaRendimento();
  public
    { Public declarations }
    procedure ReopenEmit(Codigo: Integer);
  end;

  var
  FmEmitCab: TFmEmitCab;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule, MyDBCheck;

{$R *.DFM}

procedure TFmEmitCab.BtCopiarClick(Sender: TObject);
begin
  EdSemiQtde.ValueVariant       := QrEmitQtde.Value;
  EdSemiDefPeca.ValueVariant    := EdPeca.ValueVariant;
  CBSemiDefPeca.KeyValue        := EdPeca.ValueVariant;
  EdSemiEspessura.ValueVariant  := EdEspessura.ValueVariant;
  CBSemiEspessura.KeyValue      := EdEspessura.ValueVariant;
  //
  EdSemiPeso.SetFocus;
end;

procedure TFmEmitCab.BtOKClick(Sender: TObject);
var
  //DataEmis, NOMECI, NOMESETOR, Tecnico, NOME, Obs,
  Espessura, DefPeca, Fulao, SemiTxtPeca, SemiTxtEspe, NoGraCorCad: String;
  // Status, Numero, ClienteI, Tipificacao, TempoR, TempoP, Setor, Tipific, SetrEmi, SourcMP,
  Codigo, Cod_Espess, CodDefPeca, EmitGru, Retrabalho, SemiCodPeca, GraCorCad,
  SemiCodEspe, SemiCodEspReb: Integer;
  // Peso, Custo, CustoTo, CustoKg, CustoM2, CusUSM2,
  Qtde, AreaM2, SemiPeso, SemiQtde, SemiAreaM2, SemiRendim: Double;
begin
  Codigo         := FPesagem;
  (*DataEmis       := ;
  Status         := ;
  Numero         := ;
  NOMECI         := ;
  NOMESETOR      := ;
  Tecnico        := ;
  NOME           := ;
  ClienteI       := ;
  Tipificacao    := ;
  TempoR         := ;
  TempoP         := ;
  Setor          := ;
  Tipific        := ;*)
  Espessura      := CBEspessura.Text;
  DefPeca        := CBPeca.Text;
  (*Peso           := ;
  Custo          := ;*)
  Qtde           := EdQtde.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  Fulao          := EdFulao.ValueVariant;
  (*Obs            := ;
  SetrEmi        := ;
  SourcMP        := ;*)
  Cod_Espess     := EdEspessura.ValueVariant;
  CodDefPeca     := EdPeca.ValueVariant;
  (*CustoTo        := ;
  CustoKg        := ;
  CustoM2        := ;
  CusUSM2        := ;*)
  EmitGru        := EdEmitGru.ValueVariant;
  Retrabalho     := Geral.BoolToInt(CkRetrabalho.Checked);
  SemiCodPeca    := EdSemiDefPeca.ValueVariant;
  SemiTxtPeca    := CBSemiDefPeca.Text;
  SemiPeso       := EdSemiPeso.ValueVariant;
  SemiQtde       := EdSemiQtde.ValueVariant;
  SemiAreaM2     := EdSemiAreaM2.ValueVariant;
  SemiCodEspe    := EdSemiEspessura.ValueVariant;
  SemiTxtEspe    := CBSemiEspessura.Text;
  SemiRendim     := EdSemiRendim.ValueVariant;
  SemiCodEspReb  := EdSemiCodEspReb.ValueVariant;
  GraCorCad      := EdGraCorCad.ValueVariant;
  NoGraCorCad    := EdNoGraCorCad.Text;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'emit', False, [
  (*'DataEmis', 'Status', 'Numero',
  'NOMECI', 'NOMESETOR', 'Tecnico',
  'NOME', 'ClienteI', 'Tipificacao',
  'TempoR', 'TempoP', 'Setor',
  'Tipific',*) 'Espessura', 'DefPeca',
  (*'Peso', 'Custo',*) 'Qtde',
  'AreaM2', 'Fulao', (*'Obs',
  'SetrEmi', 'SourcMP',*) 'Cod_Espess',
  'CodDefPeca', (*'CustoTo', 'CustoKg',
  'CustoM2', 'CusUSM2',*) 'EmitGru',
  'Retrabalho', 'SemiCodPeca', 'SemiTxtPeca',
  'SemiPeso', 'SemiQtde', 'SemiAreaM2',
  'SemiRendim', 'SemiCodEspe', 'SemiTxtEspe',
  'GraCorCad', 'NoGraCorCad', 'SemiCodEspReb'], [
  'Codigo'], [
  (*DataEmis, Status, Numero,
  NOMECI, NOMESETOR, Tecnico,
  NOME, ClienteI, Tipificacao,
  TempoR, TempoP, Setor,
  Tipific,*) Espessura, DefPeca,
  (*Peso, Custo,*) Qtde,
  AreaM2, Fulao, (*Obs,
  SetrEmi, SourcMP,*) Cod_Espess,
  CodDefPeca, (*CustoTo, CustoKg,
  CustoM2, CusUSM2,*) EmitGru,
  Retrabalho, SemiCodPeca, SemiTxtPeca,
  SemiPeso, SemiQtde, SemiAreaM2,
  SemiRendim, SemiCodEspe, SemiTxtEspe,
  GraCorCad, NoGraCorCad, SemiCodEspReb], [
  Codigo], True) then
  begin
    Close;
  end;
end;

procedure TFmEmitCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmitCab.CalculaRendimento();
var
  M2Ini, M2Fim: Double;
begin
  M2Ini := EdAreaM2.ValueVariant;
  M2Fim := EdSemiAreaM2.ValueVariant;
  //
  EdSemiRendim.ValueVariant := dmkPF.CalculaRendimento(M2Ini, M2Fim);
end;

procedure TFmEmitCab.EdAreaM2Change(Sender: TObject);
begin
  CalculaRendimento();
end;

procedure TFmEmitCab.EdGraCorCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Cor';
  Prompt = 'Informe a Cor';
  Campo  = 'Descricao';
var
  Resp: Variant;
begin
  if Key = VK_F4 then
  begin
    Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo, Nome ' + Campo,
    'FROM gracorcad ',
    'ORDER BY Nome ',
    ''], Dmod.MyDB, True);
    if Resp <> Null then
    begin
      EdGraCorCad.ValueVariant := VAR_SELCOD;
      EdNoGraCorCad.Text       := VAR_SELNOM;
    end;
  end;
end;

procedure TFmEmitCab.EdSemiAreaM2Change(Sender: TObject);
begin
  CalculaRendimento();
end;

procedure TFmEmitCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmitCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stUpd;
  //
  REICalArea := False;
  UnDmkDAC_PF.AbreQuery(QrEmitGru, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDefPecas1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDefPecas2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEspessuras1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEspessuras2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrRebaixe, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
end;

procedure TFmEmitCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmitCab.ReopenEmit(Codigo: Integer);
begin
  FPesagem := Codigo;
  QrEmit.Close;
  if FPesagem > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
    'SELECT emi.*, gru.Nome NO_EmitGru  ',
    'FROM emit emi ',
    'LEFT JOIN emitgru gru ON gru.Codigo=emi.EmitGru ',
    'WHERE emi.Codigo=' + Geral.FF0(FPesagem),
    '']);
    //
    EdQtde.ValueVariant       := QrEmitQtde.Value;
    EdPeca.ValueVariant       := QrEmitCodDefPeca.Value;
    CBPeca.KeyValue           := QrEmitCodDefPeca.Value;
    EdAreaM2.ValueVariant     := QrEmitAreaM2.Value;
    //EdAreaP2.ValueVariant   := ??;
    EdFulao.ValueVariant      := QrEmitFulao.Value;
    EdEspessura.ValueVariant  := QrEmitCod_Espess.Value;
    CBEspessura.KeyValue      := QrEmitCod_Espess.Value;
    EdEmitGru.ValueVariant    := QrEmitEmitGru.Value;
    CBEmitGru.KeyValue        := QrEmitEmitGru.Value;
    CkRetrabalho.Checked      := Geral.IntToBool(QrEmitRetrabalho.Value);
    //
    EdSemiQtde.ValueVariant      := QrEmitSemiQtde.Value;
    EdSemiDefPeca.ValueVariant   := QrEmitSemiCodPeca.Value;
    CBSemiDefPeca.KeyValue       := QrEmitSemiCodPeca.Value;
    EdSemiAreaM2.ValueVariant    := QrEmitSemiAreaM2.Value;
    //EdSemiAreaP2.ValueVariant    := ??
    EdSemiEspessura.ValueVariant := QrEmitSemiCodEspe.Value;
    CBSemiEspessura.KeyValue     := QrEmitSemiCodEspe.Value;
    EdSemiPeso.ValueVariant      := QrEmitSemiPeso.Value;
    //
    //2017-09-09 ::
    EdGraCorCad.ValueVariant     := QrEmitGraCorCad.Value;
    EdNoGraCorCad.Text           := QrEmitNoGraCorCad.Value;
    EdSemiCodEspReb.ValueVariant := QrEmitSemiCodEspReb.Value;
    CBSemiCodEspReb.KeyValue     := QrEmitSemiCodEspReb.Value;

    //2014-8-03 :: Compatibilidade!! Remover no futuro
    VerificaEspessura();
  end else
    Geral.MB_Erro('Pesagem n�o localizada!');
end;

procedure TFmEmitCab.VerificaEspessura();
begin
  if EdEspessura.ValueVariant = 0 then
  begin
    if (QrEmitCod_Espess.Value = 0) and (QrEmitEspessura.Value <> '') then
    begin
      if QrEspessuras1.Locate('Linhas', QrEmitEspessura.Value, []) then
      begin
        EdEspessura.ValueVariant  := QrEspessuras1Codigo.Value;
        CBEspessura.KeyValue      := QrEspessuras1Codigo.Value;
      end;
    end;
  end;
end;

end.
