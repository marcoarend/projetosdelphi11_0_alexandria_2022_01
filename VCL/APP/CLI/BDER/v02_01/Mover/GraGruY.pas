unit GraGruY;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, dmkDBGridZTO,
  Variants, MyListas;

type
  TFmGraGruY = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrGraGruY: TmySQLQuery;
    DsGraGruY: TDataSource;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TdmkDBGridZTO;
    QrGraGruYCodigo: TIntegerField;
    QrGraGruYTabela: TWideStringField;
    QrGraGruYNome: TWideStringField;
    QrGraGruYOrdem: TIntegerField;
    QrGraGruYLk: TIntegerField;
    QrGraGruYDataCad: TDateField;
    QrGraGruYDataAlt: TDateField;
    QrGraGruYUserCad: TIntegerField;
    QrGraGruYUserAlt: TIntegerField;
    QrGraGruYAlterWeb: TSmallintField;
    QrGraGruYAtivo: TSmallintField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXGraGruC: TIntegerField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXGraTamI: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXEAN13: TWideStringField;
    QrGraGruXLk: TIntegerField;
    QrGraGruXDataCad: TDateField;
    QrGraGruXDataAlt: TDateField;
    QrGraGruXUserCad: TIntegerField;
    QrGraGruXUserAlt: TIntegerField;
    QrGraGruXAlterWeb: TSmallintField;
    QrGraGruXAtivo: TSmallintField;
    QrGraGruXGraGruY: TIntegerField;
    QrPrdGrupTip: TmySQLQuery;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    QrPrdGrupTipMadeBy: TSmallintField;
    QrPrdGrupTipFracio: TSmallintField;
    QrPrdGrupTipTipPrd: TSmallintField;
    QrPrdGrupTipNivCad: TSmallintField;
    QrPrdGrupTipNOME_MADEBY: TWideStringField;
    QrPrdGrupTipNOME_FRACIO: TWideStringField;
    QrPrdGrupTipNOME_TIPPRD: TWideStringField;
    QrPrdGrupTipFaixaIni: TIntegerField;
    QrPrdGrupTipFaixaFim: TIntegerField;
    QrPrdGrupTipGradeado: TSmallintField;
    QrPrdGrupTipTitNiv1: TWideStringField;
    QrPrdGrupTipTitNiv2: TWideStringField;
    QrPrdGrupTipTitNiv3: TWideStringField;
    QrPrdGrupTipTitNiv4: TWideStringField;
    QrPrdGrupTipTitNiv5: TWideStringField;
    DsPrdGrupTip: TDataSource;
    EdPrdGrupTip: TdmkEditCB;
    Label3: TLabel;
    CBPrdGrupTip: TdmkDBLookupComboBox;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Artigonovo1: TMenuItem;
    Itemaartigoexistente1: TMenuItem;
    CkFiltrar: TCheckBox;
    EdFiltro: TdmkEdit;
    PMNovo: TPopupMenu;
    Corrigereduzidossemgrupodeestoque1: TMenuItem;
    SBNewPallet: TSpeedButton;
    SBPallet: TSpeedButton;
    GerenciaTiposdeGruposdeprodutoXGruposdeetoque1: TMenuItem;
    QrNaoCfg: TmySQLQuery;
    QrGraGruXNO_Niv1: TWideStringField;
    QrGraGruXFatorInt: TFloatField;
    QrGraGruXNO_Niv2: TWideStringField;
    ForcarGrandezaPadrao: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    Alterapartedonome1: TMenuItem;
    QrGraGruXNO_GGX: TWideStringField;
    Adicionarcoreoutamanhoaartigoexistente1: TMenuItem;
    Configurareduzidosorfos2: TMenuItem;
    Panel6: TPanel;
    Label5: TLabel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Splitter1: TSplitter;
    QrGraGruYPGT: TmySQLQuery;
    DsGraGruYPGT: TDataSource;
    QrGraGruYPGTPrdGrupTip: TIntegerField;
    QrGraGruYPGTNome: TWideStringField;
    QrGraGruYPGTITENS: TLargeintField;
    QrGraGruXPrdGrupTip: TIntegerField;
    QrGraGruYPGTPertence: TWideStringField;
    BtPrdGrupTip: TBitBtn;
    PMPrdGrupTip: TPopupMenu;
    RemoveiTipodeGrupodeProduto1: TMenuItem;
    AdicionaTipodeGrupodeProduto1: TMenuItem;
    QrGraGruYPrdGrupTip: TIntegerField;
    QrGraGruYNO_PGT: TWideStringField;
    Configurareduzidosrfos1: TMenuItem;
    Moveparaoutrogrupodeestoque1: TMenuItem;
    QrGraGruXNO_PrdGrupTip: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraGruYAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGruYBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrGraGruYAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrGraGruYBeforeClose(DataSet: TDataSet);
    procedure Artigonovo1Click(Sender: TObject);
    procedure Itemaartigoexistente1Click(Sender: TObject);
    procedure CkFiltrarClick(Sender: TObject);
    procedure EdFiltroRedefinido(Sender: TObject);
    procedure EdFiltroChange(Sender: TObject);
    procedure Corrigereduzidossemgrupodeestoque1Click(Sender: TObject);
    procedure SBPalletClick(Sender: TObject);
    procedure SBNewPalletClick(Sender: TObject);
    procedure ForcarGrandezaPadraoClick(Sender: TObject);
    procedure Alterapartedonome1Click(Sender: TObject);
    procedure Adicionarcoreoutamanhoaartigoexistente1Click(Sender: TObject);
    procedure BtPrdGrupTipClick(Sender: TObject);
    procedure AdicionaTipodeGrupodeProduto1Click(Sender: TObject);
    procedure RemoveiTipodeGrupodeProduto1Click(Sender: TObject);
    procedure PMPrdGrupTipPopup(Sender: TObject);
    procedure Configurareduzidosrfos1Click(Sender: TObject);
    procedure Moveparaoutrogrupodeestoque1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure CadastraArtigoNovo(SQLType: TSQLType; NewNome: String);
    function  SemPrdGrupTip(Avisa: Boolean = True): Boolean;
    procedure ConfiguraReduzidosOrfaos(PrdGrupTip: Integer);
  public
    { Public declarations }
    FSeq, FCabIni, FGraGruX: Integer;
    FNewNome: String;
    FLocIni: Boolean;
    //
    procedure ReopenGraGruX(Controle: Integer);
    procedure ReopenPGTs();
    procedure LocCod(Atual, Codigo: Integer);
    function  SelecionaPrdGrupTip(var PrdgrupTip: Integer): Boolean;

  end;

var
  FmGraGruY: TFmGraGruY;
  FmGraGruY1: TFmGraGruY;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnAppPF, PrdGrupTip,
  UnPQ_PF, UnGrade_PF, UnGrade_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraGruY.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraGruY.Moveparaoutrogrupodeestoque1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Grupo de Estoque';
  Prompt = 'Informe o Grupo de Estoque';
  Campo  = 'Descricao';
var
  //Terceiro, Controle, ThisCtrl: Integer;
  Resp: Variant;
  I, NoY, GraGruY, Controle: Integer;
  //
begin
  //
  //if not SelecionaPrdGrupTip(PrdGrupTip) then
    //Exit;
  if MyObjects.FIC(DGDados.SelectedRows.Count = 0, nil,
  'Selecione pelo menos um reduzido!') then
    Exit;
  //
  if not DBCheck.LiberaPelaSenhaBoss then
    Exit;
  NoY := QrGraGruYCodigo.Value;
  //
  Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo,
  GraGruY, [
    'SELECT Codigo Codigo, Nome Descricao ',
    'FROM gragruy ',
    'WHERE Codigo<>' + Geral.FF0(NoY),
    'ORDER BY Nome ',
    ''], Dmod.MyDB, True);
  //
  if Resp <> Null then
  begin
    GraGruY := Resp;
    //
    with DGDados.DataSource.DataSet do
    for I := 0 to DGDados.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(DGDados.SelectedRows.Items[I]));
      //
      Controle := QrGraGruXControle.Value;
      //
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
      'GraGruY'], ['Controle'], [GraGruY], [Controle], True);
    end;
    //
    ReopenGraGruX(Controle);
  end;
end;

procedure TFmGraGruY.CadastraArtigoNovo(SQLType: TSQLType; NewNome: String);
var
  GraGruY, PrdgrupTip: Integer;
  ForcaCor, ForcaTam: Boolean;
begin
  GraGruY := QrGraGruYCodigo.Value;
  //PrdgrupTip := QrGraGruXPrdGrupTip.Value;
  if not SelecionaPrdGrupTip(PrdGrupTip) then
    Exit;
  ForcaCor := AppPF.GGXCadGetForcaCor(QrGraGruYCodigo.Value);
  ForcaTam := AppPF.GGXCadGetForcaTam(QrGraGruYCodigo.Value);
  Grade_PF.CadastraProdutoGrade(GraGruY, PrdgrupTip, 0, 0, False, NewNome, '',
  ForcaCor, ForcaTam);
  Grade_PF.CorrigeGraGruYdeGraGruX();
///////////////////////// Inicio 2019-01-19 ////////////////////////////////////
  AppPF.CorrigeReduzidosDuplicadosDeInsumo(False);
////////////////////////// Fim 2019-01-19 //////////////////////////////////////
  AppPF.VerificaCadastroVSArtigoIncompleta();
  LocCod(GraGruY, GraGruY);
end;

procedure TFmGraGruY.CkFiltrarClick(Sender: TObject);
begin
  ReopenGraGruX(QrGraGruXControle.Value);
end;

procedure TFmGraGruY.ConfiguraReduzidosOrfaos(PrdGrupTip: Integer);
begin
  Screen.Cursor := crHourGlass;
  Grade_PF.CorrigeGraGruYdeGraGruX();
///////////////////////// Inicio 2019-01-19 ////////////////////////////////////
  AppPF.CorrigeReduzidosDuplicadosDeInsumo(False);
////////////////////////// Fim 2019-01-19 //////////////////////////////////////
  Screen.Cursor := crDefault;
  Grade_Jan.MostraFormGraGruYIncorpora(QrGraGruYCodigo.Value, PrdGrupTip);
  ReopenPGTs();
  ReopenGraGruX(0);
end;

procedure TFmGraGruY.Configurareduzidosrfos1Click(Sender: TObject);
begin
  ConfiguraReduzidosOrfaos(QrGraGruYPGTPrdGrupTip.Value);
end;

procedure TFmGraGruY.Corrigereduzidossemgrupodeestoque1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  if Geral.MB_Pergunta('Os reduzidos sem grupo de estoque ser�o cooorigidos!' +
  'Deseja continuar?') <> ID_YES then
    Exit;
  Grade_PF.CorrigeGraGruYdeGraGruX();
///////////////////////// Inicio 2019-01-19 ////////////////////////////////////
  AppPF.CorrigeReduzidosDuplicadosDeInsumo(False);
////////////////////////// Fim 2019-01-19 //////////////////////////////////////
end;

procedure TFmGraGruY.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrGraGruY);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrGraGruY, QrGraGruX);
  ForcarGrandezaPadrao.Enabled := (CO_DMKID_APP = 2)  //CO_SIGLA_APP = 'BDER';
   and (QrGraGruYCodigo.Value = 6144(*CO_GraGruY_6144_VSFinCla*));
end;

procedure TFmGraGruY.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrGraGruY);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrGraGruX);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrGraGruX);
  MyObjects.HabilitaMenuItemItsIns(ItemAArtigoExistente1, QrGraGruX);
end;

procedure TFmGraGruY.PMPrdGrupTipPopup(Sender: TObject);
begin
  RemoveiTipodeGrupodeProduto1.Enabled := SemPrdGrupTip(False) = False;
end;

procedure TFmGraGruY.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraGruYCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraGruY.DefParams;
begin
  VAR_GOTOTABELA := 'gragruy';
  VAR_GOTOMYSQLTABLE := QrGraGruY;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT pgt.Nome NO_PGT, ggy.* ');
  VAR_SQLx.Add('FROM gragruy ggy');
  VAR_SQLx.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=ggy.PrdGrupTip');
  VAR_SQLx.Add('WHERE ggy.Codigo > 0');
{
  VAR_SQLx.Add('SELECT ggy.* ');
  VAR_SQLx.Add('FROM gragruy ggy');
  VAR_SQLx.Add('WHERE ggy.Codigo > 0');
}
  //
  VAR_SQL1.Add('AND ggy.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ggy.Nome Like :P0');
  //
end;

procedure TFmGraGruY.EdFiltroChange(Sender: TObject);
begin
  ReopenGraGruX(QrGraGruXControle.Value);
end;

procedure TFmGraGruY.EdFiltroRedefinido(Sender: TObject);
begin
  //ReopenGraGruX(QrGraGruXControle.Value);
end;

procedure TFmGraGruY.Itemaartigoexistente1Click(Sender: TObject);
var
  GraGruY, PrdgrupTip, GraGru1, GraGruC: Integer;
  ForcaCor, ForcaTam: Boolean;
begin
  if SemPrdGrupTip() then
    Exit;
  //if not SelecionaPrdGrupTip(PrdGrupTip) then
    //Exit;
  PrdgrupTip := QrGraGruXPrdGrupTip.Value;
  //
  if QrGraGruX.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta(
    'A a��o selecionada ir� cadastrar novo(s) tamanho(s) / cor(es) ao artigo do item selecionado!' +
    sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
    begin
      GraGru1    := QrGraGruXGraGru1.Value;
      GraGruC    := QrGraGruXGraGruC.Value;
      GraGruY    := QrGraGruYCodigo.Value;
      ForcaCor   := AppPF.GGXCadGetForcaCor(QrGraGruYCodigo.Value);
      ForcaTam   := AppPF.GGXCadGetForcaTam(QrGraGruYCodigo.Value);
      //
      Grade_PF.CadastraProdutoGrade(GraGruY, PrdgrupTip, GraGru1, GraGruC, True,
      FNewNome, '', ForcaCor, ForcaTam);
      Grade_PF.CorrigeGraGruYdeGraGruX();
///////////////////////// Inicio 2019-01-19 ////////////////////////////////////
  AppPF.CorrigeReduzidosDuplicadosDeInsumo(False);
////////////////////////// Fim 2019-01-19 //////////////////////////////////////
      AppPF.VerificaCadastroVSArtigoIncompleta();
      LocCod(GraGruY, GraGruY);
    end;
  end;
end;

procedure TFmGraGruY.ItsAltera1Click(Sender: TObject);
const
  Edita = True;
  Qry = nil;
var
  GraGruY, GraGruX: Integer;
begin
  GraGruY := QrGraGruXGraGruY.Value;
  GraGruX := QrGraGruXControle.Value;
  //CadastraArtigoNovo(stUpd);
  AppPF.MostraFormVSXxxXxxGraGruY(GraGruY, GraGruX, Edita, Qry);
end;

procedure TFmGraGruY.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('Cadastro gerenciado somente pelo pr�prio aplicativo!' + sLineBreak +
  'Consulte a DERMATEK!');
end;

procedure TFmGraGruY.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGraGruY.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraGruY.ItsExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('Cadastro gerenciado pelo aplicativo!' + sLineBreak +
  'Consulte a DERMATEK!');
end;

procedure TFmGraGruY.RemoveiTipodeGrupodeProduto1Click(Sender: TObject);
const
  Pergunta = 'Deseja realmente remover o Tipo de Grupo de Produto selecionado?';
  Tabela =  'gragruypgt';
  Campo1 = 'GraGruY';
  Campo2 = 'PrdGrupTip';
var
  Inteiro1, Inteiro2: Integer;
  ZeraCodigo: Boolean;
begin
  Inteiro1 := QrGraGruYCodigo.Value;
  inteiro2 := QrGraGruYPGTPrdGrupTip.Value;
  //
  if UMyMod.ExcluiRegistroInt2(Pergunta, Tabela, Campo1, Campo2,
  Inteiro1, Inteiro2) = ID_YES then
  begin
    ReopenPGTs();
  end;
end;

procedure TFmGraGruY.ReopenGraGruX(Controle: Integer);
var
  SQL_Filtro: String;
begin
  SQL_Filtro := '';
  if CkFiltrar.Checked then
    SQL_Filtro := Geral.ATS([
    'AND CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
    'LIKE "' + EdFiltro.Text + '"']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT gg1.Nome NO_GGX, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ggx.*, nv1.Nome NO_Niv1, ',
  'nv1.FatorInt, nv2.Nome NO_Niv2, gg1.PrdGrupTip, ',
  'pgt.Nome NO_PrdGrupTip ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  //'LEFT JOIN gratamcad  gtc ON gtc.Codigo=gti.Codigo ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle ',
  'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 ',
  'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'WHERE ggx.GraGruY=' + Geral.FF0(QrGraGruYCodigo.Value),
  SQL_Filtro,
  '']);
  //
  QrGraGruX.Locate('Controle', Controle, []);
end;

procedure TFmGraGruY.ReopenPGTs();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruYPGT, Dmod.MyDB, [
(*
  'SELECT gg1.PrdGrupTip, pgt.Nome,',
  'COUNT(ggx.Controle) ITENS',
  'FROM GraGruX ggx',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN prdGrupTip pgt ON pgt.Codigo=gg1.PrdGrupTip',
  'WHERE ggx.GraGruY=' + Geral.FF0(QrGraGruYCodigo.Value),
  'GROUP BY gg1.PrdGrupTip',
*)
  'SELECT IF(ypg.GraGruY IS NULL, "N�o", "SIM") Pertence,',
  'gg1.PrdGrupTip, pgt.Nome,',
  'COUNT(ggx.Controle) ITENS',
  'FROM GraGruX ggx',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN prdGrupTip pgt ON pgt.Codigo=gg1.PrdGrupTip',
  'LEFT JOIN gragruypgt ypg ON ypg.GraGruY=' + Geral.FF0(QrGraGruYCodigo.Value),
  '     AND ypg.PrdGrupTip=gg1.PrdGrupTip    ',
  'WHERE ggx.GraGruY=' + Geral.FF0(QrGraGruYCodigo.Value),
  'GROUP BY gg1.PrdGrupTip',
  '']);
end;

procedure TFmGraGruY.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraGruY.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraGruY.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraGruY.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraGruY.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraGruY.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruY.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraGruYCodigo.Value;
  Close;
end;

procedure TFmGraGruY.CabAltera1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraGruY, [PnDados],
  [PnEdita], EdPrdGrupTip, ImgTipo, 'gragruy');
end;

procedure TFmGraGruY.BtConfirmaClick(Sender: TObject);
var
  //Tabela, Nome: String;
  Codigo, (*Ordem,*) PrdGrupTip: Integer;
begin
  Codigo         := Geral.IMV(EdCodigo.Text);
(*  Tabela         := ;
  Nome           := ;
  Ordem          := ;*)
  PrdGrupTip     := EdPrdGrupTip.ValueVariant;
  //
  Codigo:= UMyMod.BPGS1I32('gragruy', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'gragruy', False, [
  //'Tabela', 'Nome', 'Ordem',
  'PrdGrupTip'], [
  'Codigo'], [
  //Tabela, Nome, Ordem,
  PrdGrupTip], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmGraGruY.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'gragruy', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'gragruy', 'Codigo');
end;

procedure TFmGraGruY.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmGraGruY.BtPrdGrupTipClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPrdGrupTip, BtPrdGrupTip);
end;

procedure TFmGraGruY.Adicionarcoreoutamanhoaartigoexistente1Click(
  Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Artigo (N�vel 1)';
  Prompt = 'Informe o artigo';
  Campo  = 'Descricao';
var
  //Terceiro, Controle, ThisCtrl: Integer;
  Resp: Variant;
  GraGruY, PrdgrupTip, GraGru1, GraGruC, Nivel1: Integer;
  ForcaCor, ForcaTam: Boolean;
begin
  if SemPrdGrupTip() then
    Exit;
  //if not SelecionaPrdGrupTip(PrdGrupTip) then
    //Exit;
  PrdgrupTip := QrGraGruXPrdGrupTip.Value;
  //
  Nivel1 := QrGraGruXGraGru1.Value;
  //
  Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, Nivel1, [
    'SELECT Nivel1 Codigo, Nome Descricao ',
    'FROM gragru1 ',
    'WHERE PrdGrupTip=' + Geral.FF0(QrGraGruXPrdGrupTip.Value),
    'ORDER BY Nome ',
    ''], Dmod.MyDB, True);
  //
  if Resp <> Null then
  begin
    GraGru1 := Resp;
    GraGruC := 0;
    GraGruY := QrGraGruYCodigo.Value;
    ForcaCor := AppPF.GGXCadGetForcaCor(QrGraGruYCodigo.Value);
    ForcaTam := AppPF.GGXCadGetForcaTam(QrGraGruYCodigo.Value);
    //
    Grade_PF.CadastraProdutoGrade(GraGruY, PrdgrupTip, GraGru1, GraGruC, True,
    FNewNome, '', ForcaCor, ForcaTam);
    Grade_PF.CorrigeGraGruYdeGraGruX();
///////////////////////// Inicio 2019-01-19 ////////////////////////////////////
  AppPF.CorrigeReduzidosDuplicadosDeInsumo(False);
////////////////////////// Fim 2019-01-19 //////////////////////////////////////
    AppPF.VerificaCadastroVSArtigoIncompleta();
    LocCod(GraGruY, GraGruY);
  end;
end;

procedure TFmGraGruY.AdicionaTipodeGrupodeProduto1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Tipo de Grupo de Estoque';
  Prompt = 'Informe o Tipo de Grupo de Estoque:';
  Campo  = 'Descricao';
var
  PrdGrupTip, GraGruY: Integer;
  PGT: Variant;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    PGT := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo Codigo, Nome ' + Campo,
    'FROM prdgruptip ',
    //'WHERE Codigo NOT IN ( ' + CordaDe.. + ')',
    'ORDER BY ' + Campo,
    ''], Dmod.MyDB, True);
    if PGT <> Null then
    begin
      Screen.Cursor := crHourGlass;
      PrdGrupTip := PGT;
      GraGruY    := QrGraGruYCodigo.Value;
      if  UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, stIns, 'gragruypgt', False, [
      ], [
      'GraGruY', 'PrdGrupTip'], [
      ], [
      GraGruY, PrdGrupTip], True) then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT ggx.Controle',
        'FROM gragrux ggx',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
        'WHERE gg1.PrdGrupTip=' + Geral.FF0(PrdGrupTip),
        '']);
        Qry.First;
        while not Qry.Eof do
        begin
          Dmod.MyDB.Execute(
          ' UPDATE gragrux ' +
          ' SET GraGruY=' + Geral.FF0(GraGruY) +
          ' WHERE Controle=' + Geral.FF0(Qry.FieldByName('Controle').AsInteger) +
          '');
          //
          Qry.Next;
        end;
      end;
      //
      ReopenPGTs();
      ReopenGraGruX(0);
      ConfiguraReduzidosOrfaos(PrdgrupTip);
    end;
  finally
    Qry.Free;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGraGruY.Alterapartedonome1Click(Sender: TObject);
var
  PartOld, PartNew, NomeNew, NomeOld: String;
  I, GraGru1: Integer;
begin
  PartOld := '';
  PartNew := '';
  //
  if InputQuery('Troca de texto', 'Texto a ser trocado:', PartOld) then
  begin
    if InputQuery('Troca de texto', 'Texto novo:', PartNew) then
    begin
      for I := 0 to DGDados.SelectedRows.Count-1 do
      begin
        with DGDados.DataSource.DataSet do
          GotoBookmark(pointer(DGdados.SelectedRows.Items[I]));
        //
        NomeOld := QrGraGruXNO_GGX.Value;
        if pos(PartOld, NomeOld) > 0 then
        begin
          NomeNew := Geral.Substitui(NomeOld, PartOld, PartNew);
          GraGru1 := QrGraGruXGraGru1.Value;
          //
          Grade_PF.GraGru1_AlteraNome(GraGru1, NomeNew);
        end;
      end;
      //

    end;
  end;
end;

procedure TFmGraGruY.Artigonovo1Click(Sender: TObject);
begin
  //if SemPrdGrupTip() then
    //Exit;
  CadastraArtigoNovo(stIns, FNewNome);
end;

procedure TFmGraGruY.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmGraGruY.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  UnDmkDAC_PF.AbreQuery(QrPrdGrupTip, Dmod.MyDB);
  //
  AppPF.VerificaGraGruYsNaoConfig();
end;

procedure TFmGraGruY.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraGruYCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraGruY.SBPalletClick(Sender: TObject);
begin
//
end;

procedure TFmGraGruY.SBNewPalletClick(Sender: TObject);
var
  Nome, TitNiv1, TitNiv2, TitNiv3, TitNiv4, TitNiv5: String;
  Codigo, CodUsu, MadeBy, Fracio, Gradeado, TipPrd, NivCad, FaixaIni, FaixaFim, Customizav, ImpedeCad, LstPrcFisc, Tipo_Item, Genero, GraTabApp: Integer;
  PerComissF, PerComissR: Double;
begin
(*
INSERT INTO `prdgruptip` VALUES
(7,7,'Sub Produto'
(1,1,'Peles In Natura',2,3,0,1,1,0,0,'N�vel 1','','','','',0,1,5,0.000000,0.000000,0,0,0,'2015-05-29','2015-05-29',-2,-2,1,1,0),
(2,2,'Artigos Intermedi�riuos',1,2,0,1,1,0,0,'N�vel 1','','','','',0,1,5,0.000000,0.000000,0,0,0,'2015-05-29',NULL,-2,0,1,1,0),
(3,3,'Couros Classificados',1,2,0,1,1,0,0,'N�vel 1','','','','',0,1,5,0.000000,0.000000,0,0,0,'2015-05-29',NULL,-2,0,1,1,0),
(4,4,'Artigos em Opera��o',1,2,0,1,1,0,0,'N�vel 1','','','','',0,1,5,0.000000,0.000000,0,0,0,'2015-05-29',NULL,-2,0,1,1,0),
(5,5,'Artigo Semi Acabado',1,2,1,1,1,0,0,'N�vel 1','','','','',0,1,5,0.000000,0.000000,0,0,0,'2015-05-29',NULL,-2,0,1,1,0),
(6,6,'Artigo Acabado',1,2,1,1,1,0,0,'N�vel 1','','','','',0,1,5,0.000000,0.000000,0,0,0,'2015-05-29',NULL,-2,0,1,1,0);
  Codigo         := ;
  CodUsu         := ;
  Nome           := ;
  MadeBy         := ;
  Fracio         := ;
  Gradeado       := ;
  TipPrd         := ;
  NivCad         := ;
  FaixaIni       := ;
  FaixaFim       := ;
  TitNiv1        := ;
  TitNiv2        := ;
  TitNiv3        := ;
  TitNiv4        := ;
  TitNiv5        := ;
  Customizav     := ;
  ImpedeCad      := ;
  LstPrcFisc     := ;
  PerComissF     := ;
  PerComissR     := ;
  Tipo_Item      := ;
  Genero         := ;
  GraTabApp      := ;

  //
? := UMyMod.BuscaEmLivreY_Def('prdgruptip', 'Codigo', ImgTipo.SQLType?, CodAtual?);
ou > ? := UMyMod.BPGS1I32('prdgruptip', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, ImgTipo.SQLType?, 'prdgruptip', auto_increment?[
'CodUsu', 'Nome', 'MadeBy',
'Fracio', 'Gradeado', 'TipPrd',
'NivCad', 'FaixaIni', 'FaixaFim',
'TitNiv1', 'TitNiv2', 'TitNiv3',
'TitNiv4', 'TitNiv5', 'Customizav',
'ImpedeCad', 'LstPrcFisc', 'PerComissF',
'PerComissR', 'Tipo_Item', 'Genero',
'GraTabApp'], [
'Codigo'], [
CodUsu, Nome, MadeBy,
Fracio, Gradeado, TipPrd,
NivCad, FaixaIni, FaixaFim,
TitNiv1, TitNiv2, TitNiv3,
TitNiv4, TitNiv5, Customizav,
ImpedeCad, LstPrcFisc, PerComissF,
PerComissR, Tipo_Item, Genero,
GraTabApp], [
Codigo], UserDataAlterweb?, IGNORE?
*)
end;

procedure TFmGraGruY.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraGruY.SbNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNovo, SbNovo);
end;

procedure TFmGraGruY.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraGruY.QrGraGruYAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraGruY.QrGraGruYAfterScroll(DataSet: TDataSet);
begin
  ReopenGraGruX(0);
  ReopenPGTs();
end;

procedure TFmGraGruY.ForcarGrandezaPadraoClick(Sender: TObject);
var
  Qry: TmySQLQuery;
  GraGruY, GraGruX, Grandeza: Integer;
begin
  if Geral.MB_Pergunta(
  'Deseja realmente for�ar a grandeza de todos reduzidos deste grupo para m�?')
  = ID_YES then
  begin
    GraGruY  := QrGraGruYCodigo.Value;
    GraGruX  := QrGraGruXControle.Value;
    Grandeza := 1; // m�
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
      'UPDATE gragruxcou ',
      'SET Grandeza=' + Geral.FF0(Grandeza),
      'WHERE GraGruX IN ( ',
      '  SELECT Controle ',
      '  FROM gragrux ',
      '  WHERE GraGruY=' + Geral.FF0(GraGruY),
      ') ',
      '']);
      //
      LocCod(GraGruY, GraGruY);
      QrGraGruX.Locate('Controle', GraGruX, []);
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmGraGruY.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrGraGruYCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmGraGruY.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraGruYCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'gragruy', Dmod.MyDB, CO_VAZIO));
end;

function TFmGraGruY.SelecionaPrdGrupTip(var PrdgrupTip: Integer): Boolean;
var
  Item: Integer;
begin
  Result := False;
  PrdGrupTip := 0;
  Item := -1;
  if (QrGraGruYPGT.RecordCount > 0)
  and (Uppercase(QrGraGruYPGTPertence.Value) = 'SIM') then
  begin
    if QrGraGruYPrdGrupTip.Value = 0 then
      PrdGrupTip := QrGraGruYPGTPrdGrupTip.Value
    else
    if QrGraGruYPGTPrdGrupTip.Value = QrGraGruYPrdGrupTip.Value then
      PrdGrupTip := QrGraGruYPrdGrupTip.Value
    else
    begin
      Item := MyObjects.SelRadioGroup('Tipo de Grupo de Produto',
      'Selecione o Tipo de Grupo de Produto', [
      Geral.FF0(QrGraGruYPrdGrupTip.Value) + ' - ' + QrGraGruYNO_PGT.Value,
      Geral.FF0(QrGraGruYPGTPrdGrupTip.Value) + ' - ' + QrGraGruYPGTNome.Value
      ], -1);
      case Item of
        0: PrdGrupTip := QrGraGruYPrdGrupTip.Value;
        1: PrdGrupTip := QrGraGruYPGTPrdGrupTip.Value;
      end;
    end;
  end else
    PrdGrupTip := QrGraGruYPrdGrupTip.Value;
  Result := PrdGrupTip <> 0;
end;

function TFmGraGruY.SemPrdGrupTip(Avisa: Boolean): Boolean;
begin
  //Result := QrGraGruYPrdGrupTip.Value = 0;
  Result := QrGraGruYPGT.RecordCount = 0;
  if Result then
  begin
    if Avisa then
      Geral.MB_Aviso('A��o cancelada!' + sLineBreak +
      'Adicione um "Tipo de Grupo de Produto" ao Grupo de Estoque atual!');
    Exit;
  end;
  Result := QrGraGruYPGTPertence.Value <> 'SIM';
  if Result then
    if Avisa then
      Geral.MB_Aviso('A��o cancelada!' + sLineBreak +
      'Selecione o um "Tipo de Grupo de Produto" v�lido na grade!');
end;

procedure TFmGraGruY.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruY.CabInclui1Click(Sender: TObject);
begin
  Geral.MB_Info('Cadastro gerenciado somente pelo pr�prio aplicativo!' + sLineBreak +
  'Consulte a DERMATEK!');
{
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrGraGruY, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'gragruy');
}
end;

procedure TFmGraGruY.QrGraGruYBeforeClose(
  DataSet: TDataSet);
begin
  QrGraGruX.Close;
  QrGraGruYPGT.Close;
end;

procedure TFmGraGruY.QrGraGruYBeforeOpen(DataSet: TDataSet);
begin
  QrGraGruYCodigo.DisplayFormat := FFormatFloat;
end;

(*
    end else
    if Uppercase(Tabela) = Uppercase('GraGruY') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrdGrupTip';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
*)
end.

