

 PRD-GRUPO-035 - CADASTRO DE ARTIGO

1. Clique no bot�o "Grupos de Estoque" (fica Na guia Ribeira, subguia Cadastro Ribeira)
2. Abrir� a janela WET-CURTI-035 :: Grupos de Estoque
2a. Talvez apare�a a seguinte mensagem:
    "Existem xxx Grupos de Estoque sem Grupo de Produto definido!
     Deseja que o(s) Grupo(s) de Produto sejam criado(s) e atrelados aos respectivos Grupos de Estoque?"
     Caso esta mensagem apare�a, confirme a pergunta clicando no bot�o "Sim" ou "Yes".
======================================================================
3. Para cadastrar um novo artigo ou mat�ria prima:
3a. Localize o Grupo de estoque ao qual o artigo a ser cadastrado pertencer�
    Ex. Couro verde deve ser cadasrado no Grupo "Mat�ria-prima In Natura" C�digo 1024
3b. Ap�s localizar o Grupo de Estoque desejado, clique no bot�o "Item"
3c. Clique no item de menu "Adiciona"
3d. Clique no sub item de menu "Artigo Novo"
3e. Abrir� a janela PRD-GRUPO-020 :: Cadastro de Produto
3f. Informe o "Nome do Artigo de Ribeira"
3g. Somente para Semi e acabados: Informe a Grade de Tamanhos
3h. Somente para Semi e acabados: Informe as cores
3i. Clique no bot�o "OK" 
3h. Somente para Semi e acabados: Abrir� a Janela PRD-GRUPO-008
    3h1. Informe as combina��es de tamanho x cores checando os quadros desejados!
    3h2. Clique no bot�o "Sa�da" 
4. Abrir� a janela WET-CURTI-002 :: Configura��o de Artigos de Ribeira
4a. Informe os campos habilitados (somente o Artigo e Classe de Impress�o para clientes n�o s�o obrigat�rios!)
4b. Clique no bot�o "OK" 
======================================================================
5. Para liberar o cadastro a ser usado no sistema ele deve ser configurado:
5a. V� a janela correspondente ao Grupo de Estoque desejado:
    - Couro In Natura > Bot�o "Configura��o In Natura"
    - Couro Curtido > Bot�o "Configura��o Artigo Ribeira"
    - Couro Classificado > Bot�o "Configura��o Classificados"  
    - Couro em Opera��o > Bot�o "Configura��o Em Opera��o"
    - Couro em processo de semi > Bot�o "Configura��o Semi Acabado"
    - Couro semi ou acabado classificado > Bot�o "Configura��o Acabado Classificado"  
    - Sub produto > Bot�o "Configura��o Sub Produto"
5b. Na janela selecionada informe os Grupos de origem e destino do Grupo selecionado para liberar seu uso no sistema
    Exemplo: Para poder gerar um Couro Curtido � necess�rio dizer nos In Natura os Curtidos permitidos.
