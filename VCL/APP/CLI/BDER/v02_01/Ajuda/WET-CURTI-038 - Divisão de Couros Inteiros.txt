

  DIVIS�O DE COUROS

1. Clique no bot�o Gera��o Classificado (fica Na guia Ribeira, subguia Movimento Ribeira)
2. Abrir� a janela WET-CURTI-038 :: Artigo de Ribeira em Opera��o
======================================================================
3. Para uma nova opera��o:
3a. Clique no bot�o "Em opera��o"
3b. Clique ni item de menu "Inclui"
3c. Informe o "Artigo em opera��o" Exemplo: "Couros Wet Blue para dividir"
3d. Caso deseje, informe uma observa��o e um fornecedor
3f. Clique no bot�o "OK" para confirmar. A janela ser� fechda...
3g. ... e abrir� a janela WET-CURTI-040
3h. Selecione os pallets que ser�o levados para divis�o
3i. Clique em "OK" para confirmar
======================================================================
4. Caso tenha esquecido de informar algum pallet a ser dividido:
4a. Clique no bot�o "Origem"
4b. Clique no item de menu "Adiciona Artigo de origem"
4c. Repita os procedimentos 3h e 3i
======================================================================
5. Imprimir o "Romaneio de Ordem de Opera��o"
5a. Clique no bot�o de impress�o no canto superior esquerdo da janela
5b. Clique no item de menu "IME-I Ordem de Opera��o"
5c. Envie os couros (junto com o romaneio) para divis�o
======================================================================
6. Quando os pallets voltarem da divis�o:
6a. Execute os procedimentos 1 e 2
6b. Localize o Lote (dados est�o no romaneio) pelos bot�es de pesquisa
6c. Ap�s localizar, clique no bot�o "Destino"
6d. Clique no item de menu "Adiciona artigo de destino"
6e. Abrir� a janela WET-CURTI-041
----------------------------------------------------------------------
6f. Se gerou raspa:
6f1. Informe a raspa no "Artigo que gerou"
6f2. Informe a quantidade de pe�as
6f3. Informe a quantidade de peso
6f4. Clique no bot�ozinho "..." ao lado direito do "Pallet: Nome do Artigo de Ribeira Classificado"
6f5. Gere um novo pallet (veja ajuda espec�fica)
6f6. Ao retornar para a janela WET-CURTI-041 clique em OK
     Observa��o: N�o informe os dados de Baixa para raspa na janela WET-CURTI-041
---------------------------------------------------------------------
6g. Gera��o do novo artigo:
6g1. Caso ainda esteje na janela WET-CURTI-041 pule para o procedimento 6g3
6g2. Execute os procedimentos 6c, 6d
6g3. Informe o artigo no "Artigo que gerou"
6g4. Informe a quantidade de pe�as 
     Obs: Se foi tirado meios de inteiros a quantidade de meios deve ser o dobro dos ineiros!
6g5. Informe a quantidade de Area
6g6. Marque o checkbox "Baixar do estoque de em opera��o"
6g7. Informe a quantidade de pe�as (de couros inteiros que foram dividos)
6g8. Informe a quantidade de Area (de couros inteiros que foram dividos)
     Obs: Se os meios n'ao foram medidos a area de meios e inteiros ser� a mesma!
6g9. Clique no bot�ozinho "..." ao lado direito do "Pallet: Nome do Artigo de Ribeira Classificado"
6g10. Gere um novo pallet (veja ajuda espec�fica)
6g11. Ao retornar para a janela WET-CURTI-041 clique em OK
6g12. Caso haja mais pallets repita os procedimentos 6g3 at� 6g12
6g13. Clique no bot�o "Desiste" da janela WET-CURTI-041"
6g14. Clique no bot�o "Sa�da" da janela WET-CURTI-038"
6g15. FIM
========================================================================
 