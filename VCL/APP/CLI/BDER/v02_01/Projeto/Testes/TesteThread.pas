unit TesteThread;
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, DB,
  ExtCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage, DBCtrls,
  ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  System.Variants, mySQLDbTables, UnProjGroup_Consts;

type
   MinhaThread1 = class(TThread)
   procedure Execute; override;
   procedure Verifica;
   procedure Fechar;
   private
   constructor Create();
end;

type
   MinhaThread2 = class(TThread)
   procedure Execute; override;
   procedure Verifica;
   procedure Fechar;
   private
   constructor Create();
end;

{
type
   MinhaThread3 = class(TThread)
   procedure Execute; override;
   procedure Verifica;
   procedure Fechar;
   private
   constructor Create();
end;

type
   MinhaThread4 = class(TThread)
   procedure Execute; override;
   procedure Verifica;
   procedure Fechar;
   private
   constructor Create();
end;

type
   MinhaThread5 = class(TThread)
   procedure Execute; override;
   procedure Verifica;
   procedure Fechar;
   private
   constructor Create();
end;
}

type
  TFmTesteThread = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtNormal: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    BtTreds: TBitBtn;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    LaAviso3: TLabel;
    LaAviso4: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    mySQLQuery1: TmySQLQuery;
    Memo1: TMemo;
    BtJanela: TBitBtn;
    BtJanelas: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtNormalClick(Sender: TObject);
    procedure BtTredsClick(Sender: TObject);
    procedure BtJanelaClick(Sender: TObject);
    procedure BtJanelasClick(Sender: TObject);
  private
    { Private declarations }
    //FConta: Integer;
    Thread1: MinhaThread1;
    Thread2: MinhaThread2;
    //
    procedure ReabreQry1();
    procedure ReabreQry2();
    procedure ReabreQry3();
    procedure ReabreQry4();
    procedure ReabreQry5();
  public
    { Public declarations }
    FTempoIni, FTempoFim: TDateTime;
    //
    procedure ConsultaProcesso1(Sender: TObject);
    procedure ConsultaProcesso2(Sender: TObject);
    procedure ConsultaProcesso3(Sender: TObject);
    procedure ConsultaProcesso4(Sender: TObject);
    procedure ConsultaProcesso5(Sender: TObject);
    //
    procedure PostJSON(JSON: String);
  end;

  var
  FmTesteThread: TFmTesteThread;
  FContaT, FConta1, FConta2, FConta3, FConta4, FConta5: Integer;

implementation

uses UnMyObjects, Module, DmkDAC_PF, TesteThread_FormA, TesteThread_FormB;

{$R *.dfm}

{ MinhaThread1 }

constructor MinhaThread1.Create;
begin
inherited
  Create(True);
  FreeOnTerminate := True;
  Priority := tpLower;
  Resume;
end;

procedure MinhaThread1.Execute;
var
  Sender : TObject;
begin
  Synchronize(Verifica);
  FmTesteThread.ConsultaProcesso1(Sender); // Executar Rotina ( Procedures )
  //
  while not Terminated do
  begin
    Sleep (10);
    Terminate; // Finaliza a Thread
    Synchronize(Fechar);
  end;
end;

procedure MinhaThread1.Fechar;
begin
  //application.terminate;
end;

procedure MinhaThread1.Verifica();
begin
  //TesteThread.Caption := 'EXECUTANDO...'+IntToStr(FConta);
  MyObjects.Informa2(FmTesteThread.LaAviso3, FmTesteThread.LaAviso4, True, Geral.FF0(FConta1 + FConta2 + FConta3 + FConta4 + FConta5));
  //
end;

procedure TFmTesteThread.BtJanelaClick(Sender: TObject);
begin
  FTempoIni := Now();
  FTempoFim := Now();
  Application.CreateForm(TFmTesteThread_FormA, FmTesteThread_FormA);
  FmTesteThread_FormA.FLaAviso1 := LaAviso1;
  FmTesteThread_FormA.FLaAviso2 := LaAviso2;
  FmTesteThread_FormA.FVezes := 50;
  FmTesteThread_FormA.Timer1.Enabled := True;
end;

procedure TFmTesteThread.BtJanelasClick(Sender: TObject);
begin
  FTempoIni := Now();
  FTempoFim := Now();
  Application.CreateForm(TFmTesteThread_FormA, FmTesteThread_FormA);
  FmTesteThread_FormA.FLaAviso1 := LaAviso1;
  FmTesteThread_FormA.FLaAviso2 := LaAviso2;
  FmTesteThread_FormA.FVezes := 25;
  FmTesteThread_FormA.Timer1.Enabled := True;
  //
  Application.CreateForm(TFmTesteThread_FormB, FmTesteThread_FormB);
  FmTesteThread_FormB.FLaAviso1 := LaAviso3;
  FmTesteThread_FormB.FLaAviso2 := LaAviso4;
  FmTesteThread_FormB.FVezes := 25;
  FmTesteThread_FormB.Timer1.Enabled := True;
  //
end;

procedure TFmTesteThread.BtNormalClick(Sender: TObject);
var
  I: Integer;
  FTempo: TDateTime;
begin
  FContaT := 0;
  FConta1 := 0;
  FConta2 := 0;
  FConta3 := 0;
  FConta4 := 0;
  FConta5 := 0;
  FTempo := Now();
  for I := 1 to 100 do
    ReabreQry1();
  FTempo := Now() - FTempo;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Geral.FDT(FTempo, 108));
end;

procedure TFmTesteThread.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTesteThread.BtTredsClick(Sender: TObject);
begin
  FContaT := 0;
  FConta1 := 0;
  FConta2 := 0;
  FConta3 := 0;
  FConta4 := 0;
  FConta5 := 0;
  Thread1 := MinhaThread1.Create();
  Thread2 := MinhaThread2.Create();
  (*
  Thread3 := MinhaThread3.Create();
  Thread4 := MinhaThread4.Create();
  Thread5 := MinhaThread5.Create();
  *)
end;
{
var
  MinhasThreads: array of MinhaThread1;
  I: Integer;
begin

  SetLength(MinhasThreads, 0);

  // Criando as threads din�micamente
  for I := 0 to 9 do
    begin
      SetLength(MinhasThreads, Length(MinhasThreads) + 1);
      MinhasThreads[High(MinhasThreads)] := MinhaThread1.Create();//True);
      MinhasThreads[High(MinhasThreads)].FreeOnTerminate := True;
      MinhasThreads[High(MinhasThreads)].Start;
    end;

  // Verificar se existe alguma Thread em execu��o
  //O sistema ir� ficar rodando esse la�o de repeti��o at� que todas as threads sejam finalizadas.
  I := 0;
  while (I <= High(MinhasThreads)) do
    begin
      if (MinhasThreads[i] <> nil) then
        i := 0
      else
        Inc(i);
    end;
end;
{
  //https://www.andrecelestino.com/delphi-utilizando-o-mecanismo-de-processamento-paralelo/
var
  //Tasks: array [0..4] of ITask;
  Tasks: array [0..4] of TTask;

  Inicio: TDateTime;
  Fim: TDateTime;
begin
  Inicio := Now;

  Tasks[0] := TTask.Create(ReabreQry1());
  Tasks[0].Start;

  Tasks[1] := TTask.Create(ReabreQry2());
  Tasks[1].Start;

  Tasks[2] := TTask.Create(ReabreQry3());
  Tasks[2].Start;

  Tasks[3] := TTask.Create(ReabreQry4());
  Tasks[3].Start;

  Tasks[4] := TTask.Create(ReabreQry5());
  Tasks[4].Start;

  TTask.WaitForAll(Tasks);

  Fim := Now;

  MyObjects.Informa2(LaAviso3, LaAviso4, False, Geral.FDT(FTempo, 108));
(*
  ShowMessage(Format('Consultas realizadas em %s segundos.',
    [FormatDateTime('ss', Fim - Inicio)]));
*)
end;
}

procedure TFmTesteThread.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTesteThread.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmTesteThread.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTesteThread.PostJSON(JSON: String);
begin
  //faz um post pelo idHTTP;
  Memo1.Lines.Add(DateTimeToStr(now)+ ' - Executando JSON '+IntToStr(FConta1 + FConta2 + FConta3 + FConta4 + FConta5));
  //para simular um tempo de espera do retorno
  Sleep(1000);
  //retorno...
  Memo1.Lines.Add(DateTimeToStr(now)+ ' - Retorno JSON '+IntToStr(FConta1 + FConta2 + FConta3 + FConta4 + FConta5));
end;

procedure TFmTesteThread.ReabreQry1();
var
  Qry1: TmySQLQuery;
begin
  FConta1 := FConta1 + 1;
  MyObjects.Informa2(FmTesteThread.LaAviso3, FmTesteThread.LaAviso4, True, Geral.FF0(FConta1 + FConta2 + FConta3 + FConta4 + FConta5));
  //
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI + '',
    '']);
  finally
    Qry1.Free;
  end;
end;

procedure TFmTesteThread.ReabreQry2;
var
  Qry2: TmySQLQuery;
begin
  FConta2 := FConta2 + 1;
  MyObjects.Informa2(FmTesteThread.LaAviso3, FmTesteThread.LaAviso4, True, Geral.FF0(FConta1 + FConta2 + FConta3 + FConta4 + FConta5));
  //
  Qry2 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI + '',
    '']);
  finally
    Qry2.Free;
  end;
end;

procedure TFmTesteThread.ReabreQry3;
var
  Qry: TmySQLQuery;
begin
  //FConta := FConta + 1;
  //MyObjects.Informa2(LaAviso3, LaAviso4, True, Geral.FF0(FConta));
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI + '',
    '']);
  finally
    Qry.Free;
  end;
end;

procedure TFmTesteThread.ReabreQry4;
var
  Qry: TmySQLQuery;
begin
  //FConta := FConta + 1;
  //MyObjects.Informa2(LaAviso3, LaAviso4, True, Geral.FF0(FConta));
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI + '',
    '']);
  finally
    Qry.Free;
  end;
end;

procedure TFmTesteThread.ReabreQry5;
var
  Qry: TmySQLQuery;
begin
  //FConta := FConta + 1;
  //MyObjects.Informa2(LaAviso3, LaAviso4, True, Geral.FF0(FConta));
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI + '',
    '']);
  finally
    Qry.Free;
  end;
end;

{ TTesteThread }

procedure TFmTesteThread.ConsultaProcesso1(Sender: TObject);
begin
  //exemplo com o for
  // porem aqui eu percorro a query, passo
  // para a variavel JSON o json que est� na query
  // e chamo o metodo post
  for FConta1 := 0 to 10 do
    begin
      //postJSON(JSON);
      ReabreQry1();
    end;
end;

procedure TFmTesteThread.ConsultaProcesso2(Sender: TObject);
begin
  //exemplo com o for
  // porem aqui eu percorro a query, passo
  // para a variavel JSON o json que est� na query
  // e chamo o metodo post
  for FConta2 := 0 to 10 do
    begin
      //postJSON(JSON);
      ReabreQry2();
    end;
end;

procedure TFmTesteThread.ConsultaProcesso3(Sender: TObject);
begin
  //exemplo com o for
  // porem aqui eu percorro a query, passo
  // para a variavel JSON o json que est� na query
  // e chamo o metodo post
  for FConta3 := 0 to 10 do
    begin
      //postJSON(JSON);
      ReabreQry3();
    end;
end;

procedure TFmTesteThread.ConsultaProcesso4(Sender: TObject);
begin
  //exemplo com o for
  // porem aqui eu percorro a query, passo
  // para a variavel JSON o json que est� na query
  // e chamo o metodo post
  for FConta4 := 0 to 10 do
    begin
      //postJSON(JSON);
      ReabreQry4();
    end;
end;

procedure TFmTesteThread.ConsultaProcesso5(Sender: TObject);
begin
  //exemplo com o for
  // porem aqui eu percorro a query, passo
  // para a variavel JSON o json que est� na query
  // e chamo o metodo post
  for FConta5 := 0 to 10 do
    begin
      //postJSON(JSON);
      ReabreQry5();
    end;
end;

{ MinhaThread2 }

constructor MinhaThread2.Create;
begin
inherited
  Create(True);
  FreeOnTerminate := True;
  Priority := tpLower;
  Resume;
end;

procedure MinhaThread2.Execute;
var
  Sender : TObject;
begin
  Synchronize(Verifica);
  FmTesteThread.ConsultaProcesso2(Sender); // Executar Rotina ( Procedures )
  //
  while not Terminated do
  begin
    Sleep (10);
    Terminate; // Finaliza a Thread
    Synchronize(Fechar);
  end;
end;

procedure MinhaThread2.Fechar;
begin
  //application.terminate;
end;

procedure MinhaThread2.Verifica;
begin
  MyObjects.Informa2(FmTesteThread.LaAviso3, FmTesteThread.LaAviso4, True, Geral.FF0(FConta1 + FConta2 + FConta3 + FConta4 + FConta5));
  //TesteThread.Caption := 'EXECUTANDO...'+IntToStr(FConta);
end;

end.
