object Form8: TForm8
  Left = 0
  Top = 0
  Caption = 'Form8'
  ClientHeight = 299
  ClientWidth = 928
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object AdvToolBarPager1: TdmkPageControl
    Left = 0
    Top = 0
    Width = 928
    Height = 299
    ActivePage = AdvPage11
    Align = alClient
    TabOrder = 0
    EhAncora = False
    object AdvToolBarPager11: TTabSheet
      Caption = 'Acesso r'#225'pido'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ATBRapido: TPanel
        Left = 3
        Top = 3
        Width = 458
        Height = 170
        Caption = 'Acesso r'#225'pido'
        TabOrder = 0
        object AGBEntidades: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Entidades'
          TabOrder = 0
        end
        object AdvGlowButton91: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'C'#226'mbio Moedas'
          TabOrder = 1
        end
        object AdvGlowButton93: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Emite Nota Promiss'#243'ria'
          TabOrder = 2
        end
        object AdvGlowButton95: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Emite '#13#10'Duplicata'
          TabOrder = 3
        end
        object AdvGlowButton100: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Cadastro Insumos'
          TabOrder = 4
        end
        object AdvGlowButton101: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Relat'#243'rios Insumos'
          TabOrder = 5
        end
        object ATBFormulasImpFI: TBitBtn
          Left = 230
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Receitas Ribeira'
          TabOrder = 6
        end
        object AdvGlowButton108: TBitBtn
          Left = 344
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Pesagem Acabto'
          TabOrder = 7
        end
        object AdvGlowButton107: TBitBtn
          Left = 344
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Receitas Acabto'
          TabOrder = 8
        end
        object ATBFormulasImpWE: TBitBtn
          Left = 230
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Pesagem Recurt'
          TabOrder = 9
        end
        object ATBFormulasImpBH: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Pesagem     Cal / Cur'
          TabOrder = 10
        end
        object AGBPQSubstitui: TBitBtn
          Left = 344
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Troca de produto'
          TabOrder = 11
        end
      end
      object AdvToolBar23: TPanel
        Left = 464
        Top = 3
        Width = 344
        Height = 170
        Caption = 'Acesso r'#225'pido'
        TabOrder = 1
        object AdvGlowButton104: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Controle '#13#10'Pallets'
          TabOrder = 0
        end
        object AdvGlowButton98: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Controle Couros'
          TabOrder = 1
        end
        object AdvGlowButton99: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Relat'#243'rios Pedidos'
          TabOrder = 2
        end
        object AdvGlowButton103: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'OSs em Aberto'
          TabOrder = 3
        end
        object AdvGlowButton92: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Controle Vendas'
          TabOrder = 4
        end
        object AdvGlowButton94: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Relat'#243'rios Vendas'
          TabOrder = 5
        end
        object AdvGlowButton97: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Controle Pedidos'
          TabOrder = 6
        end
        object AdvGlowButton112: TBitBtn
          Left = 230
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Couros Terceiros'
          TabOrder = 7
        end
        object AdvGlowMenuButton2: TBitBtn
          Left = 230
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Imprime'
          TabOrder = 8
        end
      end
      object AdvToolBar24: TPanel
        Left = 811
        Top = 3
        Width = 117
        Height = 170
        Caption = 'Acesso r'#225'pido'
        TabOrder = 2
        object AdvGlowButton111: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Backup '#13#10'Dados'
          TabOrder = 0
        end
        object AdvGlowButton109: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Verifica Nova Vers'#227'o'
          TabOrder = 1
        end
        object AdvGlowButton134: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = #218'ltimo'#13#10'Balan'#231'o'
          TabOrder = 2
        end
      end
      object AdvToolBar56: TPanel
        Left = 931
        Top = 3
        Width = 120
        Height = 170
        Caption = 'Untitled'
        TabOrder = 3
      end
    end
    object AdvToolBarPager12: TTabSheet
      Caption = 'Cadastros'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar1: TPanel
        Left = 3
        Top = 3
        Width = 458
        Height = 170
        Caption = 'Diversos'
        TabOrder = 0
        object AdvGlowButton1: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Entidades'
          TabOrder = 0
        end
        object AdvGlowButton3: TBitBtn
          Left = 230
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Balan'#231'as comunic.'
          TabOrder = 1
        end
        object AdvGlowButton5: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Equipes'
          TabOrder = 2
        end
        object AdvGlowButton7: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Fornece.     '#13#10'M. P.'
          TabOrder = 3
        end
        object AdvGlowButton32: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Geral'
          TabOrder = 4
        end
        object AdvGlowButton4: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Fluxos'
          TabOrder = 5
        end
        object AdvGlowButton89: TBitBtn
          Left = 230
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Unidades '#13#10'medida'
          TabOrder = 6
        end
        object AGMBSkin: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Imprime'
          TabOrder = 7
        end
        object AdvGlowButton22: TBitBtn
          Left = 344
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Espessuras'
          TabOrder = 8
        end
        object AdvGlowButton143: TBitBtn
          Left = 344
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Pr'#233'-emeios'
          TabOrder = 9
        end
        object GBFabricas: TBitBtn
          Left = 344
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Marcas de'#13#10'Empresas'
          TabOrder = 10
        end
      end
      object AdvToolBar28: TPanel
        Left = 464
        Top = 3
        Width = 230
        Height = 170
        Caption = 'Estoque'
        TabOrder = 1
        object AdvGlowButton119: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Centros'
          TabOrder = 0
        end
        object AdvGlowButton120: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Balan'#231'o'
          TabOrder = 1
        end
        object AdvGlowButton142: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Entrada '#13#10'por '#13#10'compra'
          TabOrder = 2
        end
        object AGBConfEstq: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Confere'#13#10'Estoque'#13#10'SPED'
          TabOrder = 3
        end
      end
      object AdvToolBar51: TPanel
        Left = 697
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Consumo por leitura'
        TabOrder = 2
        object AGBConsLeit: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Consumo'#13#10'por'#13#10'Leitura'
          TabOrder = 0
        end
        object AdvGlowButton161: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Ger. Cons. '#13#10'Leitura'
          TabOrder = 1
        end
      end
    end
    object AdvPage10: TTabSheet
      Caption = 'Grade'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar27: TPanel
        Left = 3
        Top = 3
        Width = 230
        Height = 170
        Caption = 'Grade'
        TabOrder = 0
        object AdvGlowButton117: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Grade'
          TabOrder = 0
        end
        object AdvGlowButton118: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Valores'
          TabOrder = 1
        end
        object AdvGlowButton105: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Tipos'
          TabOrder = 2
        end
        object AdvGlowButton113: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Produtos'
          TabOrder = 3
        end
        object GBReduzido: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Reduzido'#13#10'de Produto'
          TabOrder = 4
        end
        object AdvGlowButton34: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Tabelas'
          TabOrder = 5
        end
      end
      object AdvToolBar26: TPanel
        Left = 236
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Cores'
        TabOrder = 1
        object AdvGlowButton114: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Pantones'
          TabOrder = 0
        end
        object AdvGlowButton115: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Cadastro'
          TabOrder = 1
        end
        object AdvGlowButton116: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Fam'#237'lias'
          TabOrder = 2
        end
      end
      object AdvToolBar53: TPanel
        Left = 355
        Top = 3
        Width = 122
        Height = 170
        Caption = 'Outros'
        TabOrder = 2
        object AdvGlowMenuButton5: TBitBtn
          Left = 5
          Top = 53
          Width = 99
          Height = 59
          TabOrder = 0
          Visible = False
        end
        object AGBGraGruEGer: TBitBtn
          Left = 2
          Top = 117
          Width = 91
          Height = 37
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Caption = 'Codigos Fornecedores'
          TabOrder = 1
        end
        object AGBGraGruGruXCou: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Configura'#231#227'o do Couro do Reduzido'
          TabOrder = 2
        end
      end
    end
    object AdvToolBarPager13: TTabSheet
      Caption = 'Uso e consumo'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar5: TPanel
        Left = 3
        Top = 3
        Width = 344
        Height = 170
        Caption = 'Geral - Uso e Consumo'
        TabOrder = 0
        object AdvGlowButton17: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Cadastro'
          TabOrder = 0
        end
        object AdvGlowButton18: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Grupos Consumo'
          TabOrder = 1
        end
        object AGBEntraUC: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Entrada'
          TabOrder = 2
        end
        object AdvGlowButton21: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Invent'#225'rio'
          TabOrder = 3
        end
        object GBBaixaETE: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Emite baixa ETE'
          TabOrder = 4
        end
        object GBBaixaOutros: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Emite outras '#13#10'baixas'
          TabOrder = 5
        end
        object AGBRelatUC: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Relat'#243'rios'
          TabOrder = 6
        end
        object AdvGlowButton37: TBitBtn
          Left = 230
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Pedidos'
          TabOrder = 7
        end
        object AdvGlowButton45: TBitBtn
          Left = 232
          Top = 104
          Width = 110
          Height = 46
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Caption = 'Dilui'#231#245'es  Misturas'
          TabOrder = 8
        end
      end
      object AdvToolBar25: TPanel
        Left = 350
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Produtos Qu'#237'micos'
        TabOrder = 1
        object AdvGlowButton10: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'F'#243'rmulas'
          TabOrder = 0
        end
        object ATBFormulasImpXX: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Emite pesagem'
          TabOrder = 1
        end
        object GBGerenciaPesagem: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Gerencia pesagens'
          TabOrder = 2
        end
      end
      object AdvToolBar33: TPanel
        Left = 469
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Ferramentas'
        TabOrder = 2
        object AdvGlowButton154: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Anula itens de NFe '#13#10'cancel. ou homol.'
          TabOrder = 0
        end
        object AdvGlowButton138: TBitBtn
          Left = 4
          Top = 56
          Width = 109
          Height = 49
          Caption = 'Corrigir PQs com reduzido duplicado'
          TabOrder = 1
        end
      end
      object AdvToolBar36: TPanel
        Left = 588
        Top = 3
        Width = 114
        Height = 170
        Caption = 'Uso e Consumo - NFe'
        TabOrder = 3
        object GBUsoConsCab: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Entrada'
          TabOrder = 0
        end
        object AdvGlowButton2: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Grupo de emiss'#227'o'
          TabOrder = 1
        end
        object BtAjustePQw: TBitBtn
          Left = 2
          Top = 104
          Width = 71
          Height = 46
          Caption = 'Ajuste Novo'
          TabOrder = 2
        end
        object AGBParar: TBitBtn
          Left = 74
          Top = 104
          Width = 35
          Height = 46
          Caption = 'Parar'
          TabOrder = 3
        end
      end
      object AdvToolBar49: TPanel
        Left = 705
        Top = 3
        Width = 228
        Height = 170
        Caption = 'Couro VS'
        TabOrder = 4
        object AGBVSCalCab: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Pesagem VS Caleiro'
          TabOrder = 0
        end
        object AGBVSCurCab: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Pesagem VS Curtimento'
          TabOrder = 1
        end
        object AGBImprimeVSEmProcBH: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Rel. em processo'
          TabOrder = 2
        end
        object AdvGlowButton162: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Erros OP Curtim.'
          TabOrder = 3
        end
      end
      object AdvToolBar50: TPanel
        Left = 936
        Top = 3
        Width = 121
        Height = 170
        Caption = 'Obsoleto'
        TabOrder = 5
        object AGBPQRAjuInn: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Ajuste (E) Retroativo'
          Enabled = False
          TabOrder = 0
        end
        object AGBPQRAjuOut: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Ajuste (S) Retroativo'
          Enabled = False
          TabOrder = 1
        end
        object AdvGlowButton127: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Retorno'
          Enabled = False
          TabOrder = 2
        end
      end
    end
    object APRibeira: TTabSheet
      Caption = 'Ribeira 1'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar44: TPanel
        Left = 3
        Top = 3
        Width = 802
        Height = 170
        Caption = 'Cadastro Ribeira'
        TabOrder = 0
        object AGBVSNatCad: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Configura'#231#227'o In Natura'
          TabOrder = 0
        end
        object AGBVSRibCad: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Configura'#231#227'o Artigo Ribeira'
          TabOrder = 1
        end
        object AGBVSRibCla: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Configura'#231#227'o Classificados'
          TabOrder = 2
        end
        object AGBVSPallet: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Cadastro '#13#10'Pallets'
          TabOrder = 3
        end
        object AGBVSPalSta: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Status de Pallet'
          TabOrder = 4
        end
        object AGBVSSerFch: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'S'#233'ries Fichas RMP'
          TabOrder = 5
        end
        object AGBVSMrtCab: TBitBtn
          Left = 344
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Cadastro '#13#10'Martelos'
          TabOrder = 6
        end
        object AGBVSRibOpe: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Configura'#231#227'o Em Opera'#231#227'o'
          TabOrder = 7
        end
        object AdvGlowButton67: TBitBtn
          Left = 128
          Top = 92
          Width = 77
          Height = 22
          TabOrder = 8
          Visible = False
        end
        object AGBVSCfgEqz: TBitBtn
          Left = 344
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Configura'#231#227'o Equ'#225'lize'
          TabOrder = 9
        end
        object AGBVSMotivBxa: TBitBtn
          Left = 458
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Motivos de Baixa Extra'
          TabOrder = 10
        end
        object AGBGraGruY: TBitBtn
          Left = 344
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Grupos de estoque'
          TabOrder = 11
        end
        object AGBVSEntiMP: TBitBtn
          Left = 458
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Config. Forneced.'
          TabOrder = 12
        end
        object AGBVSWetEnd: TBitBtn
          Left = 230
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Configura'#231#227'o Semi Acabado'
          TabOrder = 13
        end
        object AGBVSFinCla: TBitBtn
          Left = 230
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Configura'#231#227'o Acabado Classificado'
          TabOrder = 14
        end
        object AGBVSSubPrd: TBitBtn
          Left = 458
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Configura'#231#227'o Sub Produto'
          TabOrder = 15
        end
        object AGBVSArtCab: TBitBtn
          Left = 572
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Configura'#231#227'o'#13#10'Artigo'
          TabOrder = 16
        end
        object AGBVSCPMRSBCb: TBitBtn
          Left = 572
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Metas Sub Produtos'
          TabOrder = 17
        end
        object AGBVSReqMov: TBitBtn
          Left = 572
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Requisi'#231#227'o Movimenta'#231#227'o Estoque'
          TabOrder = 18
        end
        object AGBVSMovimID: TBitBtn
          Left = 686
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Movim ID'
          TabOrder = 19
        end
        object AdvGlowButton39: TBitBtn
          Left = 686
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Defeitos'
          TabOrder = 20
        end
        object AdvGlowButton170: TBitBtn
          Left = 760
          Top = 64
          Width = 23
          Height = 22
          TabOrder = 21
        end
        object AdvGlowButton171: TBitBtn
          Left = 692
          Top = 64
          Width = 23
          Height = 22
          TabOrder = 22
        end
      end
      object ATBVSGerencia: TPanel
        Left = 808
        Top = 3
        Width = 462
        Height = 170
        Caption = 'Gerenciamento'
        TabOrder = 1
        object AGBVSMovIts: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Gerencia        IME-Is'
          TabOrder = 0
        end
        object AGBVSPallet2: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Gerencia '#13#10'Pallets'
          TabOrder = 1
        end
        object AGBVSBoxes: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Gerencia Boxes'
          TabOrder = 2
        end
        object AGBVSDsnCab: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Gerencia Desnate'
          TabOrder = 3
        end
        object AGBVSFichas: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Gerencia Fichas'
          TabOrder = 4
        end
        object AGBOCs: TBitBtn
          Left = 230
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Gerencia OC'
          TabOrder = 5
        end
        object AGBVSEqzCab: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Gerencia Equ'#225'lize'
          TabOrder = 6
        end
        object AGBVSCGICab: TBitBtn
          Left = 230
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Classes Geradas IME-I'
          TabOrder = 7
        end
        object AGBVSMovCab: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Localiza        IME-Cs'
          TabOrder = 8
        end
        object AGBVSMOEnvXXX: TBitBtn
          Left = 344
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Gerencia NFs MO'
          TabOrder = 9
        end
      end
      object AdvToolBar52: TPanel
        Left = 1273
        Top = 3
        Width = 114
        Height = 170
        Caption = 'Untitled'
        TabOrder = 2
        object AGBCorrigeNFesIMIs: TBitBtn
          Left = 2
          Top = 2
          Width = 112
          Height = 46
          Caption = 'Corrige NFes IME-Is'
          TabOrder = 0
        end
        object AdvGlowButton163: TBitBtn
          Left = 2
          Top = 50
          Width = 112
          Height = 46
          Caption = 'Pre'#231'os MO'
          TabOrder = 1
        end
        object AGBVSCOPCab: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Cfg Movim'
          TabOrder = 2
        end
      end
    end
    object AdvPage13: TTabSheet
      Caption = 'Ribeira 2'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar45: TPanel
        Left = 3
        Top = 3
        Width = 688
        Height = 170
        Caption = 'Movimento Ribeira'
        TabOrder = 0
        object AGBVSInnCab: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Entrada '#13#10'In Natura'
          TabOrder = 0
        end
        object AGBVSOutCab: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Sa'#237'da '#13#10'Wet Blue'
          TabOrder = 1
        end
        object AGBVSOutIts: TBitBtn
          Left = 230
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Baixa For'#231'ada'#13#10'Wet Blue'
          TabOrder = 2
        end
        object AGBVSAjsCab: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Ajuste'#13#10'Estoque'
          TabOrder = 3
        end
        object AGBVSGeraArtCab: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Gera'#231#227'o de Artigo'
          TabOrder = 4
        end
        object AGBVSClaArt: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Classifica'#231#227'o de Artigo (OC)'
          TabOrder = 5
        end
        object AGBVSRclArt: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Reclassifica'#231#227'o'
          TabOrder = 6
        end
        object AGBVSOpeCab: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Gera'#231#227'o Classificado (Opera'#231#245'es)'
          TabOrder = 7
        end
        object AGBVSPlCCab: TBitBtn
          Left = 344
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Compra Wet Blue'
          TabOrder = 8
        end
        object AGBVSExBCab: TBitBtn
          Left = 344
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Baixa Extra(vio)'
          TabOrder = 9
        end
        object AGBVSPWECab: TBitBtn
          Left = 230
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Semi Acabado'
          TabOrder = 10
        end
        object AGBVSDvlRtb: TBitBtn
          Left = 344
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Devolu'#231#227'o de Cliente'
          TabOrder = 11
        end
        object AGBVSPedCab: TBitBtn
          Left = 458
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Pedidos'
          TabOrder = 12
        end
        object AGBVSMovItb: TBitBtn
          Left = 458
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Arquivo Morto'
          TabOrder = 13
        end
        object AGBVSTrfLocCab: TBitBtn
          Left = 572
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Transfere de Local de Estoque'
          TabOrder = 14
        end
        object AGBVSPSPCab: TBitBtn
          Left = 572
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Processo Sub-Produto'
          TabOrder = 15
        end
        object AGBVSRRMCab: TBitBtn
          Left = 458
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Reprocesso'
          TabOrder = 16
        end
        object AdvGlowMenuButton6: TBitBtn
          Left = 572
          Top = 105
          Width = 112
          Height = 46
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Caption = 'Info em Listas'
          TabOrder = 17
        end
      end
      object AdvToolBar46: TPanel
        Left = 694
        Top = 3
        Width = 346
        Height = 170
        Caption = 'Miscelanea'
        TabOrder = 1
        object AGBVSMovImp: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Relat'#243'rios'
          TabOrder = 0
        end
        object AGBVSFchRslCab: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Resultado V'#225'rias Fichas'
          TabOrder = 1
        end
        object AGBSPEDEFDEnce: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Encerra m'#234's'
          TabOrder = 2
        end
        object AGBVSCfgEFD: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Confer'#234'ncias         VS-EFD'
          TabOrder = 3
        end
        object AGBCadDiversos: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Cadastros Diversos'
          TabOrder = 4
        end
        object AdvGlowMenuButton4: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Corrige  IME-Is'
          TabOrder = 5
        end
        object AGBVSPwdDd: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Senha Dia'
          TabOrder = 6
        end
        object AGBInfoIncCab: TBitBtn
          Left = 231
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Inconsist'#234'ncias'
          TabOrder = 7
        end
        object AGBClaReCo: TBitBtn
          Left = 231
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Cla.Re.Co'
          TabOrder = 8
        end
      end
    end
    object AdvPage1: TTabSheet
      Caption = 'Couros 1'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar6: TPanel
        Left = 3
        Top = 3
        Width = 230
        Height = 170
        Caption = 'Mat'#233'ria-prima'
        TabOrder = 0
        object AdvGlowButton38: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Gerencia Mat.Prima'
          TabOrder = 0
        end
        object AdvGlowButton28: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Vendas'
          TabOrder = 1
        end
        object AdvGlowButton29: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Pedidos'
          TabOrder = 2
        end
        object AdvGlowButton50: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Couros Terceiros'
          TabOrder = 3
        end
        object AdvGlowButton128: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'NF-e'#13#10'Entrada'
          TabOrder = 4
        end
        object AdvGlowButton137: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Servi'#231'o'#13#10'Externo'
          TabOrder = 5
        end
      end
      object AdvToolBar29: TPanel
        Left = 236
        Top = 3
        Width = 344
        Height = 170
        Caption = 'Cadastros'
        TabOrder = 1
        object AdvGlowButton27: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Est'#225'gios de processo'
          TabOrder = 3
        end
        object AdvGlowButton23: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Classes'
          TabOrder = 0
        end
        object AdvGlowButton26: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Tamanhos'
          TabOrder = 2
        end
        object AdvGlowButton40: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Pallets'
          TabOrder = 4
        end
        object GBMPSubProd: TBitBtn
          Left = 230
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Sub-produtos'
          TabOrder = 5
        end
        object AdvGlowButton24: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Artigos'
          TabOrder = 1
        end
      end
      object AdvToolBar32: TPanel
        Left = 583
        Top = 3
        Width = 230
        Height = 170
        Caption = 'Estoque'
        TabOrder = 2
        object AdvGlowMenuButton1: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Imprime'
          TabOrder = 0
        end
        object GBMPReclas: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Reclassifica'
          TabOrder = 1
        end
        object AdvGlowButton139: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Multiclasse'
          TabOrder = 2
        end
        object AdvGlowButton141: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Entrada Estoque'
          Enabled = False
          TabOrder = 3
        end
        object AGBEntraMP: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Entrada'
          TabOrder = 4
        end
      end
    end
    object AdvPage12: TTabSheet
      Caption = 'Couros 2'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar22: TPanel
        Left = 122
        Top = 3
        Width = 230
        Height = 170
        Caption = 'Movimento Semi / Acabado'
        TabOrder = 0
        object AGBWBInnCab: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Entrada '#13#10'Wet Blue'
          TabOrder = 0
        end
        object AGBWBOutCab: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Sa'#237'da '#13#10'Wet Blue'
          TabOrder = 1
        end
        object AdvGlowButton20: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Baixa For'#231'ada'#13#10'Wet Blue'
          TabOrder = 2
        end
        object AGBWBAjsCab: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Ajuste'#13#10'Estoque'
          TabOrder = 3
        end
        object AGBWBRclCab: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Reclasse'
          TabOrder = 4
        end
        object AGBWBIndsWB: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Baixa em OS'
          TabOrder = 5
        end
      end
      object AdvToolBar42: TPanel
        Left = 3
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Cadastro Semi'
        TabOrder = 1
        object AGBWBPallets: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Cadastro '#13#10'Pallets'
          TabOrder = 0
        end
        object AGBWBArtCab: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Configura'#231#227'o'#13#10'Artigo'
          TabOrder = 1
        end
        object AGBWBMPrCab: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Configura'#231#227'o'#13#10'Mat. Prima'
          TabOrder = 2
        end
      end
      object AdvToolBar43: TPanel
        Left = 355
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Miscelanea'
        TabOrder = 2
        object AGBWBMovImp: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Relat'#243'rios de'#13#10' Wet Blue'
          TabOrder = 0
        end
      end
    end
    object AdvPage8: TTabSheet
      Caption = 'Pedidos'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar18: TPanel
        Left = 3
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Vendas'
        TabOrder = 0
        object AdvGlowButton72: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Relat'#243'rios'
          TabOrder = 0
        end
        object AdvGlowButton74: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Pedidos'
          TabOrder = 1
        end
      end
      object AdvToolBar13: TPanel
        Left = 122
        Top = 3
        Width = 230
        Height = 170
        Caption = 'Cadastros (Venda)'
        TabOrder = 1
        object AdvGlowButton56: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Agentes'
          TabOrder = 0
        end
        object AdvGlowButton75: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Geografia'
          TabOrder = 1
        end
        object AdvGlowButton76: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Regi'#245'es'
          TabOrder = 2
        end
        object AdvGlowButton77: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Tabelas'
          TabOrder = 3
        end
        object AdvGlowButton149: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Prazos'
          TabOrder = 4
        end
      end
    end
    object AdvPage7: TTabSheet
      Caption = 'Faturamento'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar16: TPanel
        Left = 3
        Top = 3
        Width = 230
        Height = 170
        Caption = 'Movimenta'#231#227'o'
        TabOrder = 0
        object AdvGlowButton64: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Devol.'
          Enabled = False
          TabOrder = 0
        end
        object AdvGlowButton65: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Condi.'
          Enabled = False
          TabOrder = 1
        end
        object AdvGlowButton66: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Outros'
          TabOrder = 2
        end
        object AdvGlowButton68: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'NF-e(s)'#13#10'Diversas'
          TabOrder = 3
        end
        object AdvGlowButton69: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Pedido de venda'
          TabOrder = 4
        end
      end
      object AdvToolBar17: TPanel
        Left = 236
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Sugest'#227'o'
        TabOrder = 1
        object AdvGlowButton70: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Sugest'#227'o de venda'
          Enabled = False
          TabOrder = 0
        end
        object AdvGlowButton71: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Consulta'
          Enabled = False
          TabOrder = 1
        end
      end
    end
    object AdvPage11: TTabSheet
      Caption = 'NF-e'
      ParentShowHint = False
      ShowHint = False
      object AdvToolBar39: TPanel
        Left = 584
        Top = 3
        Width = 343
        Height = 170
        Caption = 'Ferramentas'
        TabOrder = 0
        object AdvGlowButton131: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'XML/BD'
          TabOrder = 0
        end
        object AdvGlowButton148: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Exporta XML'
          TabOrder = 1
        end
        object AdvGlowButton83: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Step Lote'
          TabOrder = 2
        end
        object AdvGlowButton152: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Load XML'
          TabOrder = 3
        end
        object GBValidaXML: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Valida'#13#10'XML'
          TabOrder = 4
        end
        object AGBNFeExportaXML_B: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Monta NFe'#13#10'Autorizada'
          TabOrder = 5
        end
        object AGBNFeConsCad: TBitBtn
          Left = 230
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Consulta'#13#10'Entidade'
          TabOrder = 6
        end
        object AGBNfeNewVer: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Migrar Vers'#227'o'
          TabOrder = 7
        end
        object AdvGlowButton52: TBitBtn
          Left = 230
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Layout NF-e'
          TabOrder = 8
        end
      end
      object AdvToolBar38: TPanel
        Left = 3
        Top = 3
        Width = 458
        Height = 170
        Caption = 'Gerencia'
        TabOrder = 1
        object AGBFatPedNFs: TBitBtn
          Left = 2
          Top = 2
          Width = 112
          Height = 47
          Caption = 'Gera xml '#13#10'NF-e'
          TabOrder = 0
        end
        object AdvGlowButton85: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Lotes'#13#10'envio'
          TabOrder = 1
        end
        object AdvGlowButton80: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Gerencia NF-e(s)'
          TabOrder = 2
        end
        object AdvGlowButton81: TBitBtn
          Left = 230
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Inutilizar'
          TabOrder = 3
        end
        object AdvGlowButton126: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Status Servi'#231'o'
          TabOrder = 4
        end
        object AdvGlowButton124: TBitBtn
          Left = 117
          Top = 3
          Width = 112
          Height = 46
          Caption = 'NF-e Manual'
          TabOrder = 5
        end
        object AGBContingencia: TBitBtn
          Left = 230
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Conting'#234'ncia'
          TabOrder = 6
        end
        object AGBNFeEventos: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Lotes'#13#10'Eventos'
          TabOrder = 7
        end
        object AGBConsultaNFe: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Consulta NF-e'
          TabOrder = 8
        end
        object AGBNFeDest: TBitBtn
          Left = 344
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Distribui'#231#227'o DF-e'
          TabOrder = 9
        end
        object AGBNFeLoad_Inn: TBitBtn
          Left = 344
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Importa NFes '#13#10'Recebidas'
          TabOrder = 10
        end
        object AGBNFeDesDowC: TBitBtn
          Left = 344
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Download NFes '#13#10'Confirmadas'
          TabOrder = 11
        end
      end
      object AdvToolBar40: TPanel
        Left = 464
        Top = 3
        Width = 117
        Height = 170
        Caption = 'Cadastros'
        TabOrder = 2
        object AdvGlowButton35: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Justificat.'
          TabOrder = 0
        end
        object AdvGlowButton36: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Inform.'#13#10'Adicionais'
          TabOrder = 1
        end
        object AdvGlowButton61: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Formato'#13#10'Impress'#227'o'
          TabOrder = 2
        end
      end
      object AdvToolBar14: TPanel
        Left = 930
        Top = 3
        Width = 120
        Height = 170
        Caption = 'Untitled'
        TabOrder = 3
        object AdvGlowButton90: TBitBtn
          Left = 2
          Top = 2
          Width = 107
          Height = 51
          Caption = 'hashCSRT'
          TabOrder = 0
        end
      end
    end
    object AdvPage9: TTabSheet
      Caption = 'Fiscal'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar20: TPanel
        Left = 464
        Top = 3
        Width = 230
        Height = 170
        Caption = 'Gerencia'
        TabOrder = 0
        object AGBSintegra: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'SINTEGRA'
          TabOrder = 2
        end
        object AdvGlowButton150: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Exporta Prosoft'
          TabOrder = 4
        end
        object AGB_SPED_EFD_Imp: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'SPED-EFD'#13#10'Importa'
          TabOrder = 3
        end
        object AGB_SPED_EFD_Exp: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'SPED-EFD'#13#10'Exporta'
          TabOrder = 1
        end
        object AGB_SPED_Compara: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'SPED-EFD'#13#10'Compara'
          TabOrder = 5
        end
        object AGBApurICMSIPI: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Apura'#231#227'o'#13#10'do ICMS'#13#10'e do IPI'
          TabOrder = 0
        end
      end
      object AdvToolBar19: TPanel
        Left = 3
        Top = 3
        Width = 458
        Height = 170
        Caption = 'Cadastros'
        TabOrder = 1
        object AdvGlowButton78: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Regras'
          TabOrder = 0
        end
        object AdvGlowButton79: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'CFOP'
          TabOrder = 1
        end
        object AdvGlowButton86: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'NCM'
          TabOrder = 2
        end
        object AdvGlowButton87: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Modelos'
          TabOrder = 3
        end
        object AdvGlowButton121: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'UFs'
          TabOrder = 4
        end
        object AdvGlowButton125: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'CNAE'
          TabOrder = 5
        end
        object GBClasFisc: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Reduzidos'#13#10'de NCMs'
          TabOrder = 6
        end
        object AdvGlowButton84: TBitBtn
          Left = 230
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Al'#237'q. CSOSN'
          TabOrder = 7
        end
        object AdvGlowButton155: TBitBtn
          Left = 230
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Modelos'#13#10'Documentos'#13#10'Fiscais'
          TabOrder = 8
        end
        object GBNatOper: TBitBtn
          Left = 344
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Config.'#13#10'CFOPs'
          TabOrder = 9
        end
        object AGBTribDefCad: TBitBtn
          Left = 344
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Cadastro de Tributos'
          TabOrder = 10
        end
        object AGBTribDefCab: TBitBtn
          Left = 344
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Configura'#231#227'o de Tributos'
          TabOrder = 11
        end
      end
      object AdvToolBar48: TPanel
        Left = 697
        Top = 3
        Width = 120
        Height = 170
        Caption = 'Untitled'
        TabOrder = 2
        object AdvGlowButton153: TBitBtn
          Left = 2
          Top = 2
          Width = 107
          Height = 47
          TabOrder = 0
        end
        object AGBCFOPCFOP: TBitBtn
          Left = 2
          Top = 54
          Width = 91
          Height = 37
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Caption = 'CFOP x CFOP'
          TabOrder = 1
        end
      end
    end
    object AdvPage6: TTabSheet
      Caption = 'Financeiro'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar2: TPanel
        Left = 122
        Top = 3
        Width = 230
        Height = 170
        Caption = 'Plano de contas'
        TabOrder = 0
        object AdvGlowButton9: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Conjuntos'
          TabOrder = 0
        end
        object AdvGlowButton11: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Grupos'
          TabOrder = 1
        end
        object AdvGlowButton12: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Subgrupos'
          TabOrder = 2
        end
        object AdvGlowButton13: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Contas'
          TabOrder = 3
        end
        object AdvGlowButton6: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Planos'
          TabOrder = 4
        end
        object AdvGlowButton8: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Todos n'#237'veis'
          TabOrder = 5
        end
      end
      object AdvToolBar4: TPanel
        Left = 474
        Top = 3
        Width = 230
        Height = 170
        Caption = 'Cadastros financeiros'
        TabOrder = 1
        object AdvGlowButton16: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'C'#226'mbio'
          TabOrder = 0
        end
        object AdvGlowButton60: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Moedas'
          TabOrder = 1
        end
        object AdvGlowButton25: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Bancos'
          TabOrder = 2
        end
        object AdvGlowButton73: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Condi'#231#245'es'
          TabOrder = 3
        end
        object AdvGlowButton157: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Verif. Quit. Excl.'
          TabOrder = 4
        end
      end
      object AdvToolBar15: TPanel
        Left = 3
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Gerencia'
        TabOrder = 2
        object AdvGlowButton58: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Modelo A'
          TabOrder = 0
        end
        object AdvGlowButton59: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Modelo B'
          TabOrder = 1
        end
        object AdvGlowButton19: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Resultados'
          TabOrder = 2
        end
      end
      object AdvToolBar3: TPanel
        Left = 355
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Gerencia plano de contas'
        TabOrder = 3
        object AdvGlowButton14: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Impress'#227'o'
          TabOrder = 0
        end
        object AdvGlowButton62: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Atualiza'
          Enabled = False
          TabOrder = 1
        end
        object AdvGlowButton63: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Saldo'
          TabOrder = 2
        end
      end
      object AdvToolBar37: TPanel
        Left = 707
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Carteiras'
        TabOrder = 4
        object AGBCartNiv2: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Grupos'
          TabOrder = 0
        end
        object AGBCarteiras: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Carteiras'
          TabOrder = 1
        end
      end
    end
    object AdvPage2: TTabSheet
      Caption = 'Impress'#245'es'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar11: TPanel
        Left = 3
        Top = 3
        Width = 344
        Height = 170
        Caption = 'Impress'#245'es'
        TabOrder = 0
        object AdvGlowButton30: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Balancete'
          TabOrder = 0
        end
        object AdvGlowButton48: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Resultados'
          TabOrder = 1
        end
        object AdvGlowButton132: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Produtos'
          TabOrder = 2
        end
        object GBConflitos: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Conflitos'
          TabOrder = 3
        end
        object AdvGlowButton33: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Mala Direta'
          TabOrder = 4
        end
        object AGBReceDesp: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Receitas e'#13#10'Despesas'
          TabOrder = 5
        end
        object AGBMPRecebImp: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Recebimento'#13#10'de MP'
          TabOrder = 6
        end
      end
    end
    object AdvPage3: TTabSheet
      Caption = 'Deprecado'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar7: TPanel
        Left = 122
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Grupos'
        TabOrder = 0
        object AdvGlowButton110: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Nota fiscal'
          TabOrder = 0
        end
        object AdvGlowButton145: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Couros Terceiros'
          Enabled = False
          TabOrder = 1
        end
      end
      object AdvToolBar31: TPanel
        Left = 241
        Top = 3
        Width = 344
        Height = 170
        Caption = 'Ajustes'
        TabOrder = 1
        object AdvGlowButton133: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'PQX-GGX'
          TabOrder = 0
        end
        object AdvGlowButton135: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'MPI-GGX'
          TabOrder = 1
        end
        object AdvGlowButton136: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'CWB-GGX'
          TabOrder = 2
        end
        object AdvGlowButton140: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'PQ-GG1'
          TabOrder = 3
        end
        object AdvGlowButton144: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'GGX - R$ m'#233'dio'
          TabOrder = 4
        end
        object AdvGlowButton146: TBitBtn
          Left = 230
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Balan'#231'o PQ'
          TabOrder = 5
        end
        object AdvGlowButton147: TBitBtn
          Left = 230
          Top = 104
          Width = 112
          Height = 46
          Caption = 'CECT > CMPT'
          TabOrder = 6
        end
        object AdvGlowButton156: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Corrige PQ'#13#10'Lan'#231'amentos'
          TabOrder = 7
        end
        object AGBCorrigeFatID: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Corrige'#13#10'FatID Venda'
          TabOrder = 8
        end
      end
      object Panel1: TPanel
        Left = 836
        Top = 4
        Width = 344
        Height = 176
        TabOrder = 2
        object DBGPQ_GGX: TDBGrid
          Left = 68
          Top = 1
          Width = 210
          Height = 174
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'PQ'
              Title.Caption = 'Insumo'
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPQ'
              Title.Caption = 'Nome do Insumo'
              Width = 190
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CI'
              Title.Caption = 'Empresa'
              Width = 47
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Title.Caption = 'Nome da Empresa'
              Width = 116
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 47
              Visible = True
            end>
        end
      end
      object AdvToolBar30: TPanel
        Left = 3
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Servi'#231'os'
        TabOrder = 3
        object AdvGlowButton122: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'LC 116/03'
          Enabled = False
          TabOrder = 0
        end
        object AdvGlowButton123: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Cadastro'
          Enabled = False
          TabOrder = 1
        end
      end
      object AdvToolBar41: TPanel
        Left = 588
        Top = 3
        Width = 115
        Height = 170
        Caption = 'Outros'
        TabOrder = 4
        object AdvGlowButton96: TBitBtn
          Left = 2
          Top = 2
          Width = 112
          Height = 46
          Caption = 'Ponto Funci'
          Enabled = False
          TabOrder = 0
        end
        object AdvGlowButton82: TBitBtn
          Left = 3
          Top = 52
          Width = 110
          Height = 37
          Caption = 'CouNiv1'
          TabOrder = 1
        end
        object AdvGlowButton168: TBitBtn
          Left = 4
          Top = 96
          Width = 109
          Height = 41
          Caption = 'Thread'
          TabOrder = 2
        end
      end
      object AdvToolBar47: TPanel
        Left = 706
        Top = 3
        Width = 122
        Height = 170
        Caption = 'VS'
        TabOrder = 5
        object AdvGlowButton102: TBitBtn
          Left = 2
          Top = 8
          Width = 116
          Height = 22
          Caption = 'Corrige Sub Class'
          TabOrder = 0
        end
        object AdvGlowButton106: TBitBtn
          Left = 2
          Top = 32
          Width = 116
          Height = 22
          Caption = 'Converte IndsVS'
          TabOrder = 1
        end
        object AdvGlowButton129: TBitBtn
          Left = 2
          Top = 56
          Width = 116
          Height = 22
          Caption = 'Corrige antigos'
          TabOrder = 2
        end
        object AdvGlowButton130: TBitBtn
          Left = 2
          Top = 80
          Width = 116
          Height = 22
          Caption = 'Corrige changepall'
          TabOrder = 3
        end
      end
    end
    object AdvPage5: TTabSheet
      Caption = 'Op'#231#245'es'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar8: TPanel
        Left = 3
        Top = 3
        Width = 344
        Height = 170
        Caption = 'Op'#231#245'es'
        TabOrder = 0
        object AdvGlowButton31: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Matriz'
          TabOrder = 0
        end
        object AdvGlowButton41: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Filiais'
          TabOrder = 1
        end
        object AdvGlowButton42: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Grade X'#13#10'Entidade'
          TabOrder = 2
        end
        object AdvGlowButton43: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Espec'#237'ficos'
          TabOrder = 3
        end
        object AdvGlowButton44: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Diversos'
          TabOrder = 4
        end
        object AdvGlowButton49: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = '&Servi'#231'o do MySQL'
          TabOrder = 5
        end
        object AGBOpcoesGrad: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Op'#231#245'es'#13#10'Grade'
          TabOrder = 6
        end
        object AdvGlowButton169: TBitBtn
          Left = 232
          Top = 52
          Width = 109
          Height = 49
          Caption = 'Teste Array'
          TabOrder = 7
        end
      end
      object AdvToolBar12: TPanel
        Left = 350
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Apar'#234'ncia'
        TabOrder = 1
        object AdvGlowButton15: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Tema'
          TabOrder = 0
        end
        object AdvGlowButton53: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Menu'
          TabOrder = 1
        end
        object AdvGlowButton54: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Imagem'
          TabOrder = 2
        end
      end
    end
    object AdvPage4: TTabSheet
      Caption = 'Ferramentas'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar9: TPanel
        Left = 3
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Aplicativo'
        TabOrder = 0
        object AGBLaySPEDEFD: TBitBtn
          Left = 2
          Top = 5
          Width = 112
          Height = 46
          Caption = 'Layout'#13#10'SPED'#13#10'EFD'
          TabOrder = 0
        end
        object AGBTabsSPEDEFD: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Tabelas'#13#10'SPED'#13#10'EFD'
          TabOrder = 1
        end
      end
      object AdvToolBar10: TPanel
        Left = 122
        Top = 3
        Width = 116
        Height = 170
        Caption = 'Ferramentas'
        TabOrder = 1
        object AdvGlowButton46: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Backup'
          TabOrder = 0
        end
        object AdvGlowButton47: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Verifa BD'
          TabOrder = 1
        end
        object AdvGlowButton151: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'L'#234' arquivo'#13#10'de dados'
          TabOrder = 2
        end
      end
      object AdvToolBar21: TPanel
        Left = 241
        Top = 3
        Width = 114
        Height = 170
        Caption = 'Dados extras'
        TabOrder = 2
        object AdvGlowButton51: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'DTB'
          TabOrder = 0
        end
        object BtEntiProSoft: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Entidades'#13#10'ProSoft'
          TabOrder = 1
        end
      end
      object AdvToolBar34: TPanel
        Left = 358
        Top = 3
        Width = 345
        Height = 170
        Caption = 'Corre'#231#245'es'
        TabOrder = 3
        object AdvGlowButton159: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Lcts sem'#13#10'Cli. Int.'
          TabOrder = 0
        end
        object AdvGlowButton158: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'FatID Fin'
          TabOrder = 1
        end
        object GBTipoMov: TBitBtn
          Left = 2
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Atz Ideal'
          TabOrder = 2
          Visible = False
        end
        object AdvGlowButton55: TBitBtn
          Left = 116
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Atualiza XML'#39's'
          TabOrder = 3
        end
        object AGBNewFinMigra: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Migra'#231#227'o'
          TabOrder = 4
        end
        object AGBFatParcelaNfeCabY: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'FatParcela NfeCabY'
          TabOrder = 5
        end
        object AGBAtzArtigosPallets: TBitBtn
          Left = 230
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Atz Artigos de Pallets'
          TabOrder = 6
          Visible = False
        end
        object AdvGlowButton57: TBitBtn
          Left = 232
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Atz Produtos Ind. Escala'
          TabOrder = 7
        end
      end
      object AdvToolBar35: TPanel
        Left = 706
        Top = 3
        Width = 230
        Height = 170
        Caption = 'Importa'#231#227'o'
        TabOrder = 4
        object AdvGlowButton88: TBitBtn
          Left = 116
          Top = 3
          Width = 112
          Height = 46
          Caption = 'FoxPro'
          Enabled = False
          TabOrder = 0
        end
        object GBImportaXML: TBitBtn
          Left = 116
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Importa NF-e'#13#10'de arquivo'
          TabOrder = 1
        end
        object GBImportaNFes: TBitBtn
          Left = 2
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Importa NF-e(s)'#13#10'Emitidas'
          TabOrder = 2
        end
        object GBAbreXML: TBitBtn
          Left = 2
          Top = 3
          Width = 112
          Height = 46
          Caption = 'Abre XML'
          TabOrder = 3
        end
        object AdvGlowButton160: TBitBtn
          Left = 2
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Importa NF-e(s)'#13#10'Recebidas'
          TabOrder = 4
        end
        object AGBImportaNFeWeb: TBitBtn
          Left = 116
          Top = 104
          Width = 112
          Height = 46
          Caption = 'Importa NF-e'#13#10'da WEB'
          TabOrder = 5
        end
      end
    end
    object AdvPage14: TTabSheet
      Caption = 'Ajuda'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object AdvToolBar54: TPanel
        Left = 3
        Top = 3
        Width = 120
        Height = 170
        Caption = 'Aplicativo'
        TabOrder = 0
        object AdvGlowButton164: TBitBtn
          Left = 4
          Top = 4
          Width = 112
          Height = 46
          Caption = 'Atualiza'
          TabOrder = 0
        end
        object AdvGlowButton165: TBitBtn
          Left = 4
          Top = 54
          Width = 112
          Height = 46
          Caption = 'Sobre'
          TabOrder = 1
        end
      end
      object AdvToolBar55: TPanel
        Left = 126
        Top = 3
        Width = 121
        Height = 170
        Caption = 'Ajuda on-line'
        TabOrder = 1
        object AdvGlowButton166: TBitBtn
          Left = 4
          Top = 4
          Width = 112
          Height = 46
          Hint = 'Cadastro de carteiras'
          Caption = 'Central de '#13#10'suporte'
          TabOrder = 0
        end
        object AdvGlowButton167: TBitBtn
          Left = 4
          Top = 54
          Width = 112
          Height = 46
          Hint = 'Cadastro de carteiras'
          Caption = 'Suporte remoto'
          TabOrder = 1
        end
      end
    end
  end
end
