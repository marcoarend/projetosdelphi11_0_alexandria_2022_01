unit UnPQ_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  dmkDBGridZTO, UnDmkProcFunc, UnProjGroup_Vars, BlueDermConsts, TypInfo,
  UnProjGroup_Consts, UnEntities, dmkEditDateTimePicker, DBCtrls, Grids, Mask,
  dmkCheckGroup, UnAppEnums;

type
  TTabPqNaoCad = (tpncND=0, tpncPQEIts=1, tpncPesagem=2);
  TUnPQ_PF = class(TObject)
  private
    { Private declarations }
    procedure ReimprimePesagemSomeWet(Emit: Integer; PB1: TProgressBar);
  public
    { Public declarations }
    procedure AtualizaSdoPQx(Qry: TmySQLQuery; OrigemCodi, OrigemCtrl,
              Tipo: Integer);
    procedure AtualizaTodosPQxPositivos();
    procedure AjustePQx_Todos();
    procedure AjustePQx_Errados();
    procedure AtualizaItensProcesso(Codigo: Integer; DataBase: TmySQLDatabase);
    function  BaixaPQx(OriCodi, OriCtrl, OriTipo, CliOrig, Insumo: Integer;
              PesoBxa: Double): Boolean;
    procedure CorrigePQGGXNiv2();
    function  ExcluiPesagem(Emit, Tipo: Integer; PB: TprogressBar): Boolean;
    function  DefineDtCorrApoTxt(CkDtCorrApo: TCheckBox; TPDtCorrApo:
              TdmkEditDateTimePicker): String;
    function  ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo:
              Integer; Avisa: Boolean; Empresa: Integer): Boolean;
    function  ExcluiPQx_Mul(QrPQx: TmySQLQuery; OriCodi, OriTipo: Integer; PB:
              TProgressBar; VerificaDiferencas: Boolean): Boolean;
    procedure ExcluiReceita(Numero: Integer);
    function  InserePQx_Bxa(DataX: String; CliOrig, CliDest, Insumo: Integer; Peso,
              Valor: Double; OrigemCodi, OrigemCtrl, Tipo: Integer; Unitario:
              Boolean; DtCorrApo: String; _Empresa: Integer): Boolean;
    function  InserePQx_Inn(Query: TmySQLQuery; DataX: String; CliOrig, CliDest,
              Insumo: Integer; Peso, Valor: Double; HowLoad, OrigemCodi,
              OrigemCtrl, Tipo: Integer; DtCorrApo: String; _Empresa: Integer): Boolean;
    function  ImpedePeloEstoqueNF_Toda(OrigemCodi, OrigemTipo: Integer): Boolean;
    function  ImpedePeloEstoqueNF_Item(OrigemCodi, OrigemCtrl, OrigemTipo:
              Integer): Boolean;
    //
    procedure MostraFormFormulasImpBH();
    procedure MostraFormFormulasImpWE();
    procedure MostraFormFormulasImpFI();
    procedure MostraFormVSFormulasImp_BH(MovimCod, IMEI, TemIMEIMrt: Integer;
              MovimInn, MovimSrc: TEstqMovimNiv; ReceitaRibSetor:
              TReceitaRibSetor);
    procedure MostraFormVSFormulasImp_WE(MovimCod, IMEI, TemIMEIMrt, EmitGru:
              Integer; MovimInn, MovimSrc: TEstqMovimNiv);
    procedure MostraFormVSFormulasImp_FI(MovimCod, IMEI, TemIMEIMrt, EmitGru:
              Integer; MovimInn, MovimSrc: TEstqMovimNiv);
    function  MostraFormVSPQOCab(const SQLType: TSQLType; const VSMOvCod,
              EmitGru: Integer; const PrecisaEmitGru: Boolean;
              var Codigo: Integer): Boolean;
    function  MostraFormVSPQOIts(const SQLType: TSQLType; const Codigo: Integer;
              Data: TDateTime; var Controle: Integer): Boolean;
    //
    function  ObtemNomeTabelaTipoMovPQ(Tipo: Integer): String;
    function  ObtemSetoresSelecionados(const DBGSetores: TdmkDBGridZTO; const
              QrListaSetores: TmySQLQuery; var FSetoresCod, FSetoresTxt:
              String): Boolean;
    procedure PQO_AtualizaCusto(Codigo: Integer);
    procedure PQO_ExcluiItem(Controle: Integer; Pergunta, AtzCusto: Boolean);
    procedure PQO_ExcluiCab(Codigo: Integer);
    procedure ReimprimePesagemNovo(Emit, SetrEmi: Integer; PB: TProgressBar);
(*
    procedure ReopenPQOVS(QrPQO: TmySQLQuery; MovimCod, Codigo: Integer);
    procedure ReopenPQOIts(QrPQOIts: TmySQLQuery; Codigo, Controle: Integer);
*)
    function  VerificaEquiparacaoEstoque(Mostra: Boolean): Boolean;
    function  ValidaProdutosFormulasRibeira(Numero: Integer): Boolean;
    function  ValidaProdutosFormulasAcabto(Numero: Integer): Boolean;
    function  InsumosSemCadastroParaClienteInterno(CliInt, Empresa,
              InsumoEspecifico, FormulaEspecifica: Integer;
              Tabela: TTabPqNaoCad; MostraJanela: Boolean = True): Boolean;
    function  ObtemDadosGradeDePQ(const Insumo: Integer; var Tipo_Item,
              GraGruX, UnidMed: Integer; var Sigla, NCM: String; const Avisa:
              Boolean): Boolean;
    function  DefineVariaveisPesqusaEstoquePQ(const EdPQ, EdCIDst, EdCIOri:
              TdmkEditCB; const RGOrdem1, RGOrdem2, RGOrdem3: TRadioGroup;
              CGTipoCad: TdmkCheckGroup;
              var FCIDst, FCIOri, FPQ: Integer; var FSE, FTC: String): Boolean;
    function  TransfereReceitaDeArquivo(Orientacao: TOrientTabArqBD; Numero,
              Motivo: Integer): Boolean;
    function  AtualizaTodosPQsPosBalanco(TipoAviso: TTipoAviso; Progress1:
              TProgressBar; PnInfo, PnRegistros, PnTempo: TPanel; TimerIni:
              TDateTime; Data: TDateTime): Boolean;
    //
  end;

var
  PQ_PF: TUnPQ_PF;

implementation

uses MyDBCheck, UMySQLModule, Module, DmkDAC_PF, PQx, UnMyObjects, ModuleGeral,
  FormulasImpBH, FormulasImpWE2, FormulasImpFI,
  FormulasImpShow, FormulasImpShowNew, ModVS, ModVS_CRC, ModEmit,
  (* provisorio *) Principal, (* provisorio *)
  VSFormulasImp_BH, VSFormulasImp_WE, VSFormulasImp_FI, VSPQOCab, VSPQOIts,
  PQwPQEIts, PQCorrigeGGXNiv2, GerlShowGrid;

{ TUnPQ_PF }

procedure TUnPQ_PF.AjustePQx_Errados();
var
  Qry0, Qry1, Qry2, Qry3, Qry4: TmySQLQuery;
  OrigemCodi, OrigemCtrl, Tipo, CliOrig, Insumo, DstCodi, DstCtrl, DstTipo,
  OriCodi, OriCtrl, OriTipo: Integer;
  Peso, Valor,
  Peso1, Valor1, Peso2, SdoPeso, SdoValr, Valor2: Double;
  InsumoTxt, CliOrigTxt: String;
  //
  Linha: Integer;
  //Corda: String;
  procedure ReopenQry3();
  begin
    UnDmkDAC_PF.AbreMySQLQUery0(Qry3, Dmod.MyDB, [
      'SELECT *',
      'FROM pqx',
      'WHERE AcePeso > 0',
      'AND Tipo=' + Geral.FF0(VAR_FATID_0010),
      'AND CliOrig=' + CliOrigTxt,
      'AND Insumo=' + InsumoTxt,
      'ORDER BY DataX DESC, Tipo, OrigemCtrl DESC',
      '']);
    //Geral.MB_SQL(nil, Qry3);
  end;
  procedure AtualizaSdoAce(Qry: TmySQLQuery; OrigemCodi, OrigemCtrl,
    Tipo: Integer; PesoA, ValorA, PesoB, ValorB: Double);
  var
    SdoPeso, SdoValr: Double;
  begin
    SdoPeso := PesoA  - PesoB;
    SdoValr := ValorA - ValorB;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_PQX, False, [
      'AcePeso', 'AceValr'], [
      'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
    SdoPeso, SdoValr], [
    OrigemCodi, OrigemCtrl, Tipo], False) then
    begin
      if SdoPeso <= 0 then
        ReopenQry3();
    end;
  end;
//const
  //Itens: array[0..13] of Integer = (57,58,64,65,66,74,78,80,104,105,106,320,345,359);
var
  I, Item, CliO: Integer;
begin
  FmPrincipal.FParar := False;
  FmPrincipal.SG1.Visible := True;
  FmPrincipal.SG1.SetFocus;
  Linha := 0;
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  if Geral.MB_Pergunta(
  'Este procedimento ir� alterar lan�amentos anteriores!' + sLineBreak +
  'Deseja continuar assim mesmo?') <> ID_YES then Exit;
  //
  Qry0 := TmySQLQuery.Create(Dmod);
  try
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry2 := TmySQLQuery.Create(Dmod);
    try
    Qry3 := TmySQLQuery.Create(Dmod);
    try
    Qry4 := TmySQLQuery.Create(Dmod);
    try
    UnDmkDAC_PF.AbreMySQLQUery0(Qry0, Dmod.MyDB, [
      'DROP TABLE IF EXISTS _pq_compara_estq_nfs_; ',
      'CREATE TABLE _pq_compara_estq_nfs_ ',
      'SELECT pqx.CliOrig, pqx.Insumo,  ',
      'SUM(pqx.SdoPeso) SdoPeso ',
      'FROM ' + TMeuDB + '.pqx pqx ',
      'WHERE pqx.SdoPeso>0 ',
      'GROUP BY pqx.CliOrig, pqx.Insumo; ',
      'SELECT pcl.Peso, pqx.* ',
      'FROM _pq_compara_estq_nfs_ pqx, ' + TMeuDB + '.pqcli pcl  ',
      'WHERE  pcl.CI=pqx.CliOrig ',
      'AND pcl.PQ=pqx.Insumo  ',
      'AND pcl.Peso <> pqx.SdoPeso ',
      'ORDER BY Insumo, CliOrig',
      '']);
    FmPrincipal.PB1.Position := 0;
    FmPrincipal.PB1.Max := Qry0.RecordCount;
    Qry0.First;
    while not Qry0.Eof do
    begin
      MyObjects.UpdPB(FmPrincipal.PB1, FmPrincipal.LaAviso1, FmPrincipal.LaAviso2);
      Item := Qry0.FieldByName('Insumo').AsInteger;
      CliO := Qry0.FieldByName('CliOrig').AsInteger;
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE pqx SET SdoPeso=0, SdoValr=0 ',
        'WHERE Insumo=' + Geral.FF0(Item),
        'AND CliOrig=' + Geral.FF0(CliO),
        '']);
      UnDmkDAC_PF.AbreMySQLQUery0(Qry1, Dmod.MyDB, [
        'SELECT OrigemCtrl ',
        'FROM pqx ',
        'WHERE Insumo=' + Geral.FF0(Item),
        'AND CliOrig=' + Geral.FF0(CliO),
        '']);
      Qry1.First;
      while not Qry1.Eof do
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'DELETE FROM pqw ' ,
          'WHERE DstCtrl =' + Geral.FF0(Qry1.FieldByName('Origemctrl').AsInteger),
          '']);
        //
        Qry1.Next;
      end;
      //
      Qry0.Next;
    end;
    //
    FmPrincipal.PB1.Position := 0;
    Qry0.First;
    while not Qry0.Eof do
    begin
      MyObjects.UpdPB(FmPrincipal.PB1, FmPrincipal.LaAviso1, FmPrincipal.LaAviso2);
      Item := Qry0.FieldByName('Insumo').AsInteger;
      CliO := Qry0.FieldByName('CliOrig').AsInteger;
      //
      UnDmkDAC_PF.AbreMySQLQUery0(Qry1, Dmod.MyDB, [
        'SELECT pcl.* ',
        'FROM pqcli pcl ',
        'WHERE pcl.Peso > 0 ',
        'AND pcl.PQ=' + Geral.FF0(Item),
        'AND pcl.CI=' + Geral.FF0(CliO),
        '']);
      Qry1.First;
      while not Qry1.Eof do
      begin
        //
        CliOrig    := Qry1.FieldByName('CI').AsInteger;
        Insumo     := Qry1.FieldByName('PQ').AsInteger;
        Peso1      := Qry1.FieldByName('Peso').AsFloat;
        //
        UnDmkDAC_PF.AbreMySQLQUery0(Qry2, Dmod.MyDB, [
          'SELECT * ',
          'FROM pqx ',
          'WHERE CliOrig=' + Geral.FF0(CliOrig),
          'AND Insumo=' + Geral.FF0(Insumo),
          'AND Peso>0 ',
          'AND Tipo=' + Geral.FF0(VAR_FATID_0010), // Soh pode ser de entrada por compra!
          'ORDER BY DataX DESC, Tipo DESC, OrigemCtrl DESC ',
          '']);
        //
        Qry2.First;
        while not Qry2.Eof do
        begin
          OrigemCodi := Qry2.FieldByName('OrigemCodi').AsInteger;
          OrigemCtrl := Qry2.FieldByName('OrigemCtrl').AsInteger;
          Tipo       := Qry2.FieldByName('Tipo').AsInteger;
          Peso2      := Qry2.FieldByName('Peso').AsFloat;
          Valor2     := Qry2.FieldByName('Valor').AsFloat;
          if Peso1 <= 0 then
          begin
            Qry2.Last;
            // ter certeza!!!
            Qry2.Next;
          end else
          begin
            if Peso1 > Peso2 then
            begin
              SdoPeso := Peso2;
              Peso1 := Peso1 - Peso2;
            end else
            begin
              SdoPeso := Peso1;
              Peso1 := 0;
            end;
            if Peso2 > 0 then
              SdoValr := Valor2 / Peso2 * SdoPeso
            else
              SdoValr := 0;
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_PQX, False, [
            'SdoPeso', 'SdoValr'], [
            'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
            SdoPeso, SdoValr], [
            OrigemCodi, OrigemCtrl, Tipo], False);
          end;
          //
          Qry2.Next;
        end;
        //
        Qry1.Next;
      end;
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE pqx SET AcePeso=Peso-SdoPeso, ' +
      ' AceValr=Valor-SdoValr WHERE Peso>0 AND Tipo='+Geral.FF0(VAR_FATID_0010));
      //if not PQ_PF.VerificaEquiparacaoEstoque() then Exit;
      //
      UnDmkDAC_PF.AbreMySQLQUery0(Qry1, Dmod.MyDB, [
      'SELECT COUNT(*) Itens',
      'FROM pqx ',
      'WHERE Peso < 0 ',
      'AND Tipo>0 ',
      'AND Insumo > 0',
      'AND Insumo =' + Geral.FF0(Item),
      'AND CliOrig =' + Geral.FF0(Clio),
      'ORDER BY Insumo, CliOrig',
      '']);
      UnDmkDAC_PF.AbreMySQLQUery0(Qry1, Dmod.MyDB, [
      'SELECT DISTINCT CliOrig, Insumo',
      'FROM pqx ',
      'WHERE Peso < 0 ',
      'AND Tipo>0 ',
      'AND Insumo > 0',
      'AND Insumo=' + Geral.FF0(Item),
      'AND CliOrig =' + Geral.FF0(Clio),
      'ORDER BY Insumo, CliOrig',
      '']);
      Qry1.First;
      while not Qry1.Eof do
      begin
        CliOrigTxt  := Geral.FF0(Qry1.FieldByName('CliOrig').AsInteger);
        InsumoTxt   := Geral.FF0(Qry1.FieldByName('Insumo').AsInteger);
        //Geral.MB_Info(InsumoTXT);
        //
        UnDmkDAC_PF.AbreMySQLQUery0(Qry2, Dmod.MyDB, [
        'SELECT *',
        'FROM pqx',
        'WHERE Peso < 0',
        'AND Tipo>=0',
        'AND CliOrig=' + CliOrigTxt,
        'AND Insumo=' + InsumoTxt,
        'ORDER BY DataX DESC, Tipo, OrigemCtrl DESC',
        '']);
        //Geral.MB_SQL(nil, Qry2);
        if Qry2.RecordCount > 0 then
        begin
          Qry2.First;
          while not Qry2.Eof do
          begin
            Peso1 := -Qry2.FieldByName('Peso').AsFloat;
            ReopenQry3();
            if Qry3.RecordCount > 0 then
            begin
              Qry3.First;
              while not Qry3.Eof do
              begin
                MyObjects.Informa2(FmPrincipal.LaAviso1, FmPrincipal.LaAviso2, True,
                Geral.FF0(FmPrincipal.PB1.Position) + ' de ' +
                Geral.FF0(FmPrincipal.PB1.Max) +
                '. Insumo ' + InsumoTxt + ' do CI ' + CliOrigTxt +
                '. Qr3: ' + Geral.FF0(Qry3.RecNo) + ' de ' +
                Geral.FF0(Qry3.RecordCount));
                OrigemCodi := Qry3.FieldByName('OrigemCodi').AsInteger;
                OrigemCtrl := Qry3.FieldByName('OrigemCtrl').AsInteger;
                Tipo       := Qry3.FieldByName('Tipo').AsInteger;
                Peso2      := Qry3.FieldByName('AcePeso').AsFloat;
                Valor2     := Qry3.FieldByName('AceValr').AsFloat;
                //
                if (Peso1 <= 0) then
                begin
                  if (Peso2 > 0) and (Qry2.RecordCount = Qry2.RecNo) then
                  begin
                    OriCodi := 0;
                    OriCtrl := 0;
                    OriTipo := 0;
                    DstCodi := OrigemCodi;
                    DstCtrl := OrigemCtrl;
                    DstTipo := Tipo;
                    Peso  := Peso2;
                    Valor := Valor2;
                    //
                    //if
                    UnDmkDAC_PF.AbreMySQLQUery0(Qry4, Dmod.MyDB, [
                    'SELECT Peso-SdoPeso Peso, Valor-SdoValr Valor ',
                    'FROM pqx',
                    'WHERE OrigemCodi=' + Geral.FF0(DstCodi),
                    'AND OrigemCtrl=' + Geral.FF0(DstCtrl),
                    'AND Tipo=' + Geral.FF0(DstTipo),
                    '']);
                    Peso     := Qry4.FieldByName('Peso').AsFloat;
                    Valor    := Qry4.FieldByName('Valor').AsFloat;
                    //
                    UnDmkDAC_PF.AbreMySQLQUery0(Qry4, Dmod.MyDB, [
                    'SELECT SUM(Peso) Peso, SUM(Valor) Valor  ',
                    'FROM pqw ',
                    'WHERE DstCodi=' + Geral.FF0(DstCodi),
                    'AND DstCtrl=' + Geral.FF0(DstCtrl),
                    'AND DstTipo=' + Geral.FF0(DstTipo),
                    '']);
                    Peso  := Peso  - Qry4.FieldByName('Peso').AsFloat;
                    Valor := Valor - Qry4.FieldByName('Valor').AsFloat;
                    //
                    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqw', False, [
                    'Peso', 'Valor'], [
                    'OriCodi', 'OriCtrl', 'OriTipo', 'DstCodi', 'DstCtrl', 'DstTipo'], [
                    Peso, Valor], [
                    OriCodi, OriCtrl, OriTipo, DstCodi, DstCtrl, DstTipo], False);
                    //
                    //Peso  := Qry3.FieldByName('Peso').AsFloat;
                    //Valor := Qry3.FieldByName('Valor').AsFloat;
                    //AtualizaSdoAce(Qry4, DstCodi, DstCtrl, DstTipo, Peso2, Valor2, Peso, Valor);
                    //
                    Linha := Linha + 1;
                    FmPrincipal.SG1.RowCount := Linha + 1;
                    FmPrincipal.SG1.Cells[0, Linha] := InsumoTxt;
                    FmPrincipal.SG1.Cells[1, Linha] := CliOrigTxt;
                    FmPrincipal.SG1.Cells[2, Linha] := Geral.FF0(Qry2.FieldByName('OrigemCtrl').AsInteger);
                    FmPrincipal.SG1.Cells[3, Linha] := Geral.FFT(Peso2, 3, siNegativo);
                    FmPrincipal.SG1.Cells[4, Linha] := Geral.FFT(Valor2, 2, siNegativo);
                    FmPrincipal.SG1.Cells[5, Linha] := Geral.FFT(Peso, 3, siNegativo);
                    FmPrincipal.SG1.Cells[6, Linha] := Geral.FFT(Valor, 2, siNegativo);
                    //keybd_event(VK_DOWN, 0, 0, 0);
                    FmPrincipal.SG1.Row := Linha;
                    // ter certeza!!!
                    Qry3.Next;
                  end else
                  begin
                    Qry3.Next;
                    Qry3.Last;
                  end;
                end else
                begin
                  if Peso1 > Peso2 then
                  begin
                    Peso    := Peso2;
                    Peso1   := Peso1 - Peso2;
                    //Peso2   := 0;
                    ReopenQry3();
                  end else
                  begin
                    Peso    := Peso1;
                    //Peso2   := Peso2 - Peso1;
                    Peso1   := 0;
                  end;
                  if Peso2 > 0 then
                  begin
                    Valor := Valor2 / Peso2 * Peso
                  end else
                  begin
                    Valor := 0;
                  end;
                  OriCodi := Qry2.FieldByName('OrigemCodi').AsInteger;
                  OriCtrl := Qry2.FieldByName('OrigemCtrl').AsInteger;
                  OriTipo := Qry2.FieldByName('Tipo').AsInteger;
                  DstCodi := OrigemCodi;
                  DstCtrl := OrigemCtrl;
                  DstTipo := Tipo;
                  //
                  //if
                  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqw', False, [
                  'Peso', 'Valor'], [
                  'OriCodi', 'OriCtrl', 'OriTipo', 'DstCodi', 'DstCtrl', 'DstTipo'], [
                  Peso, Valor], [
                  OriCodi, OriCtrl, OriTipo, DstCodi, DstCtrl, DstTipo], False);
                  //
                  //Peso  := Qry3.FieldByName('Peso').AsFloat;
                  //Valor := Qry3.FieldByName('Valor').AsFloat;
                  AtualizaSdoAce(Qry4, DstCodi, DstCtrl, DstTipo, Peso2, Valor2, Peso, Valor);
                  //
                  Linha := Linha + 1;
                  FmPrincipal.SG1.RowCount := Linha + 1;
                  FmPrincipal.SG1.Cells[0, Linha] := InsumoTxt;
                  FmPrincipal.SG1.Cells[1, Linha] := CliOrigTxt;
                  FmPrincipal.SG1.Cells[2, Linha] := Geral.FF0(Qry2.FieldByName('OrigemCtrl').AsInteger);
                  FmPrincipal.SG1.Cells[3, Linha] := Geral.FFT(Peso2, 3, siNegativo);
                  FmPrincipal.SG1.Cells[4, Linha] := Geral.FFT(Valor2, 2, siNegativo);
                  FmPrincipal.SG1.Cells[5, Linha] := Geral.FFT(Peso, 3, siNegativo);
                  FmPrincipal.SG1.Cells[6, Linha] := Geral.FFT(Valor, 2, siNegativo);
                  //keybd_event(VK_DOWN, 0, 0, 0);
                  FmPrincipal.SG1.Row := Linha;
                  //
                  if FmPrincipal.FParar then
                  begin
                    FmPrincipal.FParar := False;
                    Exit;
                  end;
                end;
                //
                //Qry3.Next;
              end;
            end;
            Qry2.Next;
          end;
        end;
        Qry1.Next;
      end;
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT * ',
      'FROM pqx',
      'WHERE SdoPeso > 0',
      'AND Insumo=' + Geral.FF0(Item),
      'AND CliOrig =' + Geral.FF0(Clio),
      '']);
      Qry1.First;
      while not Qry1.Eof do
      begin
        DstCodi   := Qry1.FieldByName('OrigemCodi').AsInteger;
        DstCtrl   := Qry1.FieldByName('OrigemCtrl').AsInteger;
        DstTipo   := Qry1.FieldByName('Tipo').AsInteger;
        Peso1     := Qry1.FieldByName('Peso').AsFloat;
        Valor1    := Qry1.FieldByName('Valor').AsFloat;
        //
        PQ_PF.AtualizaSdoPQx(Qry4, DstCodi, DstCtrl, DstTipo);
        //
        Qry1.Next;
      end;
      Qry0.Next;
    end;
    //
    finally
      Qry4.Free;
    end;
    finally
      Qry3.Free;
    end;
    finally
      Qry2.Free;
    end;
  finally
    Qry1.Free;
  end;
  finally
    Qry0.Free;
  end;
  if not PQ_PF.VerificaEquiparacaoEstoque(True) then Exit;
  FmPrincipal.PB1.Position := 0;
  FmPrincipal.PB1.Max := 0;
  MyObjects.Informa2(FmPrincipal.LaAviso1, FmPrincipal.LaAviso2, False, '...');
end;

procedure TUnPQ_PF.AjustePQx_Todos();
var
  Qry1, Qry2, Qry3, Qry4: TmySQLQuery;
  OrigemCodi, OrigemCtrl, Tipo, CliOrig, Insumo, DstCodi, DstCtrl, DstTipo,
  OriCodi, OriCtrl, OriTipo: Integer;
  Peso, Valor,
  Peso1, Valor1, Peso2, SdoPeso, SdoValr, Valor2: Double;
  InsumoTxt, CliOrigTxt: String;
  //
  Linha: Integer;
  procedure ReopenQry3();
  begin
    UnDmkDAC_PF.AbreMySQLQUery0(Qry3, Dmod.MyDB, [
    'SELECT *',
    'FROM pqx',
    'WHERE AcePeso > 0',
    'AND Tipo=' + Geral.FF0(VAR_FATID_0010),
    'AND CliOrig=' + CliOrigTxt,
    'AND Insumo=' + InsumoTxt,
    'ORDER BY DataX DESC, Tipo, OrigemCtrl DESC',
    '']);
  end;
  procedure AtualizaSdoAce(Qry: TmySQLQuery; OrigemCodi, OrigemCtrl,
    Tipo: Integer; PesoA, ValorA, PesoB, ValorB: Double);
  var
    SdoPeso, SdoValr: Double;
  begin
    SdoPeso := PesoA  - PesoB;
    SdoValr := ValorA - ValorB;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_PQX, False, [
    'AcePeso', 'AceValr'], [
    'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
    SdoPeso, SdoValr], [
    OrigemCodi, OrigemCtrl, Tipo], False) then
    begin
      if SdoPeso <= 0 then
        ReopenQry3();
    end;
  end;
{
  procedure AtualizaSdoAce(Qry: TmySQLQuery; OrigemCodi, OrigemCtrl,
    Tipo: Integer; Peso1, Valor1, Peso2, Valor2: Double);
  var
    SdoPeso, SdoValr: Double;
  begin
    (*
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Peso) Peso,  SUM(Valor) Valor',
    'FROM pqw',
    'WHERE DstCodi=' + Geral.FF0(OrigemCodi),
    'AND DstCtrl=' + Geral.FF0(OrigemCtrl),
    'AND DstTipo=' + Geral.FF0(Tipo),
    '']);
    *)
    //
    SdoPeso := Peso1  - Qry.FieldByName('Peso').AsFloat;
    SdoValr := Valor1 - Qry.FieldByName('Valor').AsFloat;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'p q x', False, [
    'AcePeso', 'AceValr'], [
    'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
    SdoPeso, SdoValr], [
    OrigemCodi, OrigemCtrl, Tipo], False);
  end;
}
begin
  FmPrincipal.FParar := False;
  FmPrincipal.SG1.Visible := True;
  FmPrincipal.SG1.SetFocus;
  Linha := 0;
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  if Geral.MB_Pergunta(
  'Este procedimento ir� alterar lan�amentos anteriores!' + sLineBreak +
  'Deseja continuar assim mesmo?') <> ID_YES then Exit;
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE pqx SET SdoPeso=0, SdoValr=0');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM pqw');
  //
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry2 := TmySQLQuery.Create(Dmod);
    try
    Qry3 := TmySQLQuery.Create(Dmod);
    try
    Qry4 := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQUery0(Qry1, Dmod.MyDB, [
      'SELECT pcl.* ',
      'FROM pqcli pcl ',
      'WHERE pcl.Peso > 0 ',
      '']);
      FmPrincipal.PB1.Position := 0;
      FmPrincipal.PB1.Max := Qry1.RecordCount;
      Qry1.First;
      while not Qry1.Eof do
      begin
        MyObjects.UpdPB(FmPrincipal.PB1, FmPrincipal.LaAviso1, FmPrincipal.LaAviso2);
        //
        CliOrig    := Qry1.FieldByName('CI').AsInteger;
        Insumo     := Qry1.FieldByName('PQ').AsInteger;
        Peso1      := Qry1.FieldByName('Peso').AsFloat;
        //
        UnDmkDAC_PF.AbreMySQLQUery0(Qry2, Dmod.MyDB, [
        'SELECT * ',
        'FROM pqx ',
        'WHERE CliOrig=' + Geral.FF0(CliOrig),
        'AND Insumo=' + Geral.FF0(Insumo),
        'AND Peso>0 ',
        'AND Tipo=' + Geral.FF0(VAR_FATID_0010), // Soh pode ser de entrada por compra!
        'ORDER BY DataX DESC, Tipo DESC, OrigemCtrl DESC ',
        '']);
        //
        Qry2.First;
        while not Qry2.Eof do
        begin
          OrigemCodi := Qry2.FieldByName('OrigemCodi').AsInteger;
          OrigemCtrl := Qry2.FieldByName('OrigemCtrl').AsInteger;
          Tipo       := Qry2.FieldByName('Tipo').AsInteger;
          Peso2      := Qry2.FieldByName('Peso').AsFloat;
          Valor2     := Qry2.FieldByName('Valor').AsFloat;
          if Peso1 <= 0 then
          begin
            Qry2.Last;
            // ter certeza!!!
            Qry2.Next;
          end else
          begin
            if Peso1 > Peso2 then
            begin
              SdoPeso := Peso2;
              Peso1 := Peso1 - Peso2;
            end else
            begin
              SdoPeso := Peso1;
              Peso1 := 0;
            end;
            if Peso2 > 0 then
              SdoValr := Valor2 / Peso2 * SdoPeso
            else
              SdoValr := 0;
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_PQX, False, [
            'SdoPeso', 'SdoValr'], [
            'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
            SdoPeso, SdoValr], [
            OrigemCodi, OrigemCtrl, Tipo], False);
          end;
          //
          Qry2.Next;
        end;
        //
        Qry1.Next;
      end;
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE pqx SET AcePeso=Peso-SdoPeso, ' +
      ' AceValr=Valor-SdoValr WHERE Peso>0 AND Tipo='+Geral.FF0(VAR_FATID_0010));
      //if not PQ_PF.VerificaEquiparacaoEstoque(True) then Exit;
      //
      UnDmkDAC_PF.AbreMySQLQUery0(Qry1, Dmod.MyDB, [
      'SELECT COUNT(*) Itens',
      'FROM pqx ',
      'WHERE Peso < 0 ',
      'AND Tipo>0 ',
      'AND Insumo > 0',
      //'AND Insumo=3',
      'ORDER BY Insumo, CliOrig',
      '']);
      FmPrincipal.PB1.Position := 0;
      FmPrincipal.PB1.Max := Qry1.FieldByName('Itens').AsInteger;
      //
      UnDmkDAC_PF.AbreMySQLQUery0(Qry1, Dmod.MyDB, [
      'SELECT DISTINCT CliOrig, Insumo ',
      'FROM pqx ',
      'WHERE Peso < 0 ',
      'AND Tipo>0 ',
      'AND Insumo > 0',
      //'AND Insumo=3',
      'ORDER BY Insumo, CliOrig',
      '']);
      Qry1.First;
      while not Qry1.Eof do
      begin
        CliOrigTxt  := Geral.FF0(Qry1.FieldByName('CliOrig').AsInteger);
        InsumoTxt   := Geral.FF0(Qry1.FieldByName('Insumo').AsInteger);
        //Geral.MB_Info(InsumoTXT);
        //
        UnDmkDAC_PF.AbreMySQLQUery0(Qry2, Dmod.MyDB, [
        'SELECT *',
        'FROM pqx',
        'WHERE Peso < 0',
        'AND Tipo>0',
        'AND CliOrig=' + CliOrigTxt,
        'AND Insumo=' + InsumoTxt,
        'ORDER BY DataX DESC, Tipo, OrigemCtrl DESC',
        '']);
        if Qry2.RecordCount > 0 then
        begin
          Qry2.First;
          while not Qry2.Eof do
          begin
            FmPrincipal.PB1.Position := FmPrincipal.PB1.Position + 1;
            //
            Peso1 := -Qry2.FieldByName('Peso').AsFloat;
            ReopenQry3();
            if Qry3.RecordCount > 0 then
            begin
              Qry3.First;
              while not Qry3.Eof do
              begin
                MyObjects.Informa2(FmPrincipal.LaAviso1, FmPrincipal.LaAviso2, True,
                Geral.FF0(FmPrincipal.PB1.Position) + ' de ' +
                Geral.FF0(FmPrincipal.PB1.Max) +
                '. Insumo ' + InsumoTxt + ' do CI ' + CliOrigTxt +
                '. Qr3: ' + Geral.FF0(Qry3.RecNo) + ' de ' +
                Geral.FF0(Qry3.RecordCount));
                OrigemCodi := Qry3.FieldByName('OrigemCodi').AsInteger;
                OrigemCtrl := Qry3.FieldByName('OrigemCtrl').AsInteger;
                Tipo       := Qry3.FieldByName('Tipo').AsInteger;
                Peso2      := Qry3.FieldByName('AcePeso').AsFloat;
                Valor2     := Qry3.FieldByName('AceValr').AsFloat;
                //
                if (Peso1 <= 0) then
                begin
                  if (Peso2 > 0) and (Qry2.RecordCount = Qry2.RecNo) then
                  begin
                    OriCodi := 0;
                    OriCtrl := 0;
                    OriTipo := 0;
                    DstCodi := OrigemCodi;
                    DstCtrl := OrigemCtrl;
                    DstTipo := Tipo;
                    Peso  := Peso2;
                    Valor := Valor2;
                    //
                    //if
                    UnDmkDAC_PF.AbreMySQLQUery0(Qry4, Dmod.MyDB, [
                    'SELECT Peso-SdoPeso Peso, Valor-SdoValr Valor ',
                    'FROM pqx',
                    'WHERE OrigemCodi=' + Geral.FF0(DstCodi),
                    'AND OrigemCtrl=' + Geral.FF0(DstCtrl),
                    'AND Tipo=' + Geral.FF0(DstTipo),
                    '']);
                    Peso     := Qry4.FieldByName('Peso').AsFloat;
                    Valor    := Qry4.FieldByName('Valor').AsFloat;
                    //
                    UnDmkDAC_PF.AbreMySQLQUery0(Qry4, Dmod.MyDB, [
                    'SELECT SUM(Peso) Peso, SUM(Valor) Valor  ',
                    'FROM pqw ',
                    'WHERE DstCodi=' + Geral.FF0(DstCodi),
                    'AND DstCtrl=' + Geral.FF0(DstCtrl),
                    'AND DstTipo=' + Geral.FF0(DstTipo),
                    '']);
                    Peso  := Peso  - Qry4.FieldByName('Peso').AsFloat;
                    Valor := Valor - Qry4.FieldByName('Valor').AsFloat;
                    //
                    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqw', False, [
                    'Peso', 'Valor'], [
                    'OriCodi', 'OriCtrl', 'OriTipo', 'DstCodi', 'DstCtrl', 'DstTipo'], [
                    Peso, Valor], [
                    OriCodi, OriCtrl, OriTipo, DstCodi, DstCtrl, DstTipo], False);
                    //
                    //Peso  := Qry3.FieldByName('Peso').AsFloat;
                    //Valor := Qry3.FieldByName('Valor').AsFloat;
                    //AtualizaSdoAce(Qry4, DstCodi, DstCtrl, DstTipo, Peso2, Valor2, Peso, Valor);
                    //
                    Linha := Linha + 1;
                    FmPrincipal.SG1.RowCount := Linha + 1;
                    FmPrincipal.SG1.Cells[0, Linha] := InsumoTxt;
                    FmPrincipal.SG1.Cells[1, Linha] := CliOrigTxt;
                    FmPrincipal.SG1.Cells[2, Linha] := Geral.FF0(Qry2.FieldByName('OrigemCtrl').AsInteger);
                    FmPrincipal.SG1.Cells[3, Linha] := Geral.FFT(Peso2, 3, siNegativo);
                    FmPrincipal.SG1.Cells[4, Linha] := Geral.FFT(Valor2, 2, siNegativo);
                    FmPrincipal.SG1.Cells[5, Linha] := Geral.FFT(Peso, 3, siNegativo);
                    FmPrincipal.SG1.Cells[6, Linha] := Geral.FFT(Valor, 2, siNegativo);
                    //keybd_event(VK_DOWN, 0, 0, 0);
                    FmPrincipal.SG1.Row := Linha;
                    // ter certeza!!!
                    Qry3.Next;
                  end else
                  begin
                    Qry3.Next;
                    Qry3.Last;
                  end;
                end else
                begin
                  if Peso1 > Peso2 then
                  begin
                    Peso    := Peso2;
                    Peso1   := Peso1 - Peso2;
                    //Peso2   := 0;
                    ReopenQry3();
                  end else
                  begin
                    Peso    := Peso1;
                    //Peso2   := Peso2 - Peso1;
                    Peso1   := 0;
                  end;
                  if Peso2 > 0 then
                  begin
                    Valor := Valor2 / Peso2 * Peso
                  end else
                  begin
                    Valor := 0;
                  end;
                  OriCodi := Qry2.FieldByName('OrigemCodi').AsInteger;
                  OriCtrl := Qry2.FieldByName('OrigemCtrl').AsInteger;
                  OriTipo := Qry2.FieldByName('Tipo').AsInteger;
                  DstCodi := OrigemCodi;
                  DstCtrl := OrigemCtrl;
                  DstTipo := Tipo;
                  //
                  //if
                  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqw', False, [
                  'Peso', 'Valor'], [
                  'OriCodi', 'OriCtrl', 'OriTipo', 'DstCodi', 'DstCtrl', 'DstTipo'], [
                  Peso, Valor], [
                  OriCodi, OriCtrl, OriTipo, DstCodi, DstCtrl, DstTipo], False);
                  //
                  //Peso  := Qry3.FieldByName('Peso').AsFloat;
                  //Valor := Qry3.FieldByName('Valor').AsFloat;
                  AtualizaSdoAce(Qry4, DstCodi, DstCtrl, DstTipo, Peso2, Valor2, Peso, Valor);
                  //
                  Linha := Linha + 1;
                  FmPrincipal.SG1.RowCount := Linha + 1;
                  FmPrincipal.SG1.Cells[0, Linha] := InsumoTxt;
                  FmPrincipal.SG1.Cells[1, Linha] := CliOrigTxt;
                  FmPrincipal.SG1.Cells[2, Linha] := Geral.FF0(Qry2.FieldByName('OrigemCtrl').AsInteger);
                  FmPrincipal.SG1.Cells[3, Linha] := Geral.FFT(Peso2, 3, siNegativo);
                  FmPrincipal.SG1.Cells[4, Linha] := Geral.FFT(Valor2, 2, siNegativo);
                  FmPrincipal.SG1.Cells[5, Linha] := Geral.FFT(Peso, 3, siNegativo);
                  FmPrincipal.SG1.Cells[6, Linha] := Geral.FFT(Valor, 2, siNegativo);
                  //keybd_event(VK_DOWN, 0, 0, 0);
                  FmPrincipal.SG1.Row := Linha;
                  //
                  if FmPrincipal.FParar then
                  begin
                    FmPrincipal.FParar := False;
                    Exit;
                  end;
                end;
                //
                //Qry3.Next;
                (*
                if (Peso1 <= 0) then
                begin
                  if (Peso2 > 0) and (Qry2.RecordCount = Qry2.RecNo) then
                  begin
                    OriCodi := 0;
                    OriCtrl := 0;
                    OriTipo := 0;
                    DstCodi := OrigemCodi;
                    DstCtrl := OrigemCtrl;
                    DstTipo := Tipo;
                    Peso  := Peso2;
                    Valor := Valor2;
                    //
                    //if
                    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqw', False, [
                    'Peso', 'Valor'], [
                    'OriCodi', 'OriCtrl', 'OriTipo', 'DstCodi', 'DstCtrl', 'DstTipo'], [
                    Peso, Valor], [
                    OriCodi, OriCtrl, OriTipo, DstCodi, DstCtrl, DstTipo], False);
                    //
                    //Peso  := Qry3.FieldByName('Peso').AsFloat;
                    //Valor := Qry3.FieldByName('Valor').AsFloat;
                    AtualizaSdoAce(Qry4, DstCodi, DstCtrl, DstTipo, Peso2, Valor2, Peso, Valor);
                    //
                    Linha := Linha + 1;
                    FmPrincipal.SG1.RowCount := Linha + 1;
                    FmPrincipal.SG1.Cells[0, Linha] := InsumoTxt;
                    FmPrincipal.SG1.Cells[1, Linha] := CliOrigTxt;
                    FmPrincipal.SG1.Cells[2, Linha] := Geral.FF0(Qry2.FieldByName('OrigemCtrl').AsInteger);
                    FmPrincipal.SG1.Cells[3, Linha] := Geral.FFT(Peso2, 3, siNegativo);
                    FmPrincipal.SG1.Cells[4, Linha] := Geral.FFT(Valor2, 2, siNegativo);
                    FmPrincipal.SG1.Cells[5, Linha] := Geral.FFT(Peso, 3, siNegativo);
                    FmPrincipal.SG1.Cells[6, Linha] := Geral.FFT(Valor, 2, siNegativo);
                    //keybd_event(VK_DOWN, 0, 0, 0);
                    FmPrincipal.SG1.Row := Linha;
                    // ter certeza!!!
                  Qry3.Next;
                  end else
                  begin
                    Qry3.Next;
                    Qry3.Last;
                  end;
                end else
                begin
                  if Peso1 > Peso2 then
                  begin
                    Peso    := Peso2;
                    Peso1   := Peso1 - Peso2;
                    //Peso2   := 0;
                    ReopenQry3();
                  end else
                  begin
                    Peso    := Peso1;
                    //Peso2   := Peso2 - Peso1;
                    Peso1   := 0;
                  end;
                  if Peso2 > 0 then
                  begin
                    Valor := Valor2 / Peso2 * Peso
                  end else
                  begin
                    Valor := 0;
                  end;
                  OriCodi := Qry2.FieldByName('OrigemCodi').AsInteger;
                  OriCtrl := Qry2.FieldByName('OrigemCtrl').AsInteger;
                  OriTipo := Qry2.FieldByName('Tipo').AsInteger;
                  DstCodi := OrigemCodi;
                  DstCtrl := OrigemCtrl;
                  DstTipo := Tipo;
                  //
                  //if
                  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqw', False, [
                  'Peso', 'Valor'], [
                  'OriCodi', 'OriCtrl', 'OriTipo', 'DstCodi', 'DstCtrl', 'DstTipo'], [
                  Peso, Valor], [
                  OriCodi, OriCtrl, OriTipo, DstCodi, DstCtrl, DstTipo], False);
                  //
                  //Peso  := Qry3.FieldByName('Peso').AsFloat;
                  //Valor := Qry3.FieldByName('Valor').AsFloat;
                  AtualizaSdoAce(Qry4, DstCodi, DstCtrl, DstTipo, Peso2, Valor2, Peso, Valor);
                  //
                  Linha := Linha + 1;
                  FmPrincipal.SG1.RowCount := Linha + 1;
                  FmPrincipal.SG1.Cells[0, Linha] := InsumoTxt;
                  FmPrincipal.SG1.Cells[1, Linha] := CliOrigTxt;
                  FmPrincipal.SG1.Cells[2, Linha] := Geral.FF0(Qry2.FieldByName('OrigemCtrl').AsInteger);
                  FmPrincipal.SG1.Cells[3, Linha] := Geral.FFT(Peso2, 3, siNegativo);
                  FmPrincipal.SG1.Cells[4, Linha] := Geral.FFT(Valor2, 2, siNegativo);
                  FmPrincipal.SG1.Cells[5, Linha] := Geral.FFT(Peso, 3, siNegativo);
                  FmPrincipal.SG1.Cells[6, Linha] := Geral.FFT(Valor, 2, siNegativo);
                  //keybd_event(VK_DOWN, 0, 0, 0);
                  FmPrincipal.SG1.Row := Linha;
                  //
                  if FmPrincipal.FParar then
                  begin
                    FmPrincipal.FParar := False;
                    Exit;
                  end;
                end;
                //
                //Qry3.Next;
                *)
              end;
            end;
            Qry2.Next;
          end;
        end;
        Qry1.Next;
      end;
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT * ',
      'FROM pqx',
      'WHERE SdoPeso > 0',
      '']);
      //PB1.Position := 0;
      //PB1.Max := Qry1.RecordCount;
      Qry1.First;
      while not Qry1.Eof do
      begin
        MyObjects.UpdPB(FmPrincipal.PB1, FmPrincipal.LaAviso1, FmPrincipal.LaAviso2);
        DstCodi   := Qry1.FieldByName('OrigemCodi').AsInteger;
        DstCtrl   := Qry1.FieldByName('OrigemCtrl').AsInteger;
        DstTipo   := Qry1.FieldByName('Tipo').AsInteger;
        Peso1     := Qry1.FieldByName('Peso').AsFloat;
        Valor1    := Qry1.FieldByName('Valor').AsFloat;
        //
        PQ_PF.AtualizaSdoPQx(Qry4, DstCodi, DstCtrl, DstTipo);
        //
        Qry1.Next;
      end;
    finally
      Qry4.Free;
    end;
    finally
      Qry3.Free;
    end;
    finally
      Qry2.Free;
    end;
  finally
    Qry1.Free;
  end;
  if not PQ_PF.VerificaEquiparacaoEstoque(True) then Exit;
  FmPrincipal.PB1.Position := 0;
  FmPrincipal.PB1.Max := 0;
  MyObjects.Informa2(FmPrincipal.LaAviso1, FmPrincipal.LaAviso2, False, '...');
end;

procedure TUnPQ_PF.AtualizaSdoPQx(Qry: TmySQLQuery; OrigemCodi, OrigemCtrl,
  Tipo: Integer);
var
  Peso, Valor, SdoPeso, SdoValr: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Peso, Valor  ',
  'FROM pqx ',
  'WHERE OrigemCodi=' + Geral.FF0(OrigemCodi),
  'AND OrigemCtrl=' + Geral.FF0(OrigemCtrl),
  'AND Tipo=' + Geral.FF0(Tipo),
  '']);
  Peso := Qry.FieldByName('Peso').AsFloat;
  Valor := Qry.FieldByName('Valor').AsFloat;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT SUM(Peso) Peso,  SUM(Valor) Valor',
  'FROM pqw',
  'WHERE DstCodi=' + Geral.FF0(OrigemCodi),
  'AND DstCtrl=' + Geral.FF0(OrigemCtrl),
  'AND DstTipo=' + Geral.FF0(Tipo),
  '']);
  //
  SdoPeso := Peso  - Qry.FieldByName('Peso').AsFloat;
  SdoValr := Valor - Qry.FieldByName('Valor').AsFloat;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_PQX, False, [
  'SdoPeso', 'SdoValr'], [
  'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
  SdoPeso, SdoValr], [
  OrigemCodi, OrigemCtrl, Tipo], False);
end;

function TUnPQ_PF.AtualizaTodosPQsPosBalanco(TipoAviso: TTipoAviso;
  Progress1: TProgressBar; PnInfo, PnRegistros, PnTempo: TPanel; TimerIni:
  TDateTime; Data: TDateTime): Boolean;
var
  Qry: TmySQLQuery;
  Abrangencia: Integer;
  DtaTxt: String;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qry.Close;
    //UnDmkDAC_PF.AbreQuery(Qry, Dmod.MyDB);
    Abrangencia := -1;
    while not (Abrangencia in ([0,1])) do
      Abrangencia := MyObjects.SelRadioGroup(
      'Abrang�cia da atualiza��o de estque de insumos',
      'Selecione a abrang�ncia:', ['Apenas movimentados', 'Todos insumos'], -1);

    case Abrangencia of
      0:
      begin
        DtaTxt := Geral.FDT(Trunc(Data), 1);
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT DISTINCT Insumo PQ, CliDest CI, Empresa ',
        'FROM pqx ',
        'WHERE DataX >= "' + DtaTxt + '" ',
        'ORDER BY PQ, CI ',
        '']);
      end;
      else
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT PQ PQ, CI CI, Empresa ',
        'FROM pqcli ',
        'ORDER BY PQ, CI ',
        '']);
      end;
    end;
    Progress1.Position := 0;
    Progress1.Max := Qry.RecordCount;
    if PnInfo <> nil then PnInfo.Visible := True;
    if PnInfo <> nil then PnInfo.Refresh;
    //GBCntrl.Refresh;
    //Panel1.Refresh;
    //Panel2.Refresh;
    TimerIni := Now();

    //  fazer apenas uma vez para n�o demorar o UnPQx.AtualizaEstoquePQ !!!
    DMod.AtualizaStqMovIts_PQX(nil, nil, nil);
    //
    VAR_PQS_ESTQ_NEG := '';
    while not Qry.Eof do
    begin
      Progress1.Position := Progress1.Position + 1;
      if PnRegistros <> nil then
      begin
        PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
        PnRegistros.Refresh;
      end;
      if PnTempo <> nil then
      begin
        PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
        PnTempo.Refresh;
      end;
      Application.ProcessMessages;
      UnPQx.AtualizaEstoquePQ(Qry.FieldByName('CI').AsInteger,
        Qry.FieldByName('PQ').AsInteger, Qry.FieldByName('Empresa').AsInteger,
        TipoAviso, CO_VAZIO, False);
      Qry.Next;
    end;
    Result := True;
    //if PnInfo <> nil then PnInfo.Visible := False;
    Qry.Close;
  finally
    Qry.Free;
  end;
end;

procedure TUnPQ_PF.AtualizaTodosPQxPositivos();
begin
  if DBCheck.CriaFm(TFmPQwPQEIts, FmPQwPQEIts, afmoNegarComAviso) then
  begin
    FmPQwPQEIts.FAtualizaTodos := True;
    FmPQwPQEIts.ShowModal;
    FmPQwPQEIts.Destroy;
  end;
end;

function TUnPQ_PF.BaixaPQx(OriCodi, OriCtrl, OriTipo, CliOrig, Insumo: Integer;
  PesoBxa: Double): Boolean;
var
  Qry1, Qry2: TmySQLQuery;
  DstCodi, DstCtrl, DstTipo, OrigemCodi, OrigemCtrl, Tipo: Integer;
  Peso1, (*Valor1,*) Peso2, Peso, Valor, Valor2: Double;
begin
  Peso1 := PesoBxa;
  Qry1   := TmySQLQuery.Create(Dmod);
  try
    Qry2   := TmySQLQuery.Create(Dmod);
    try
      //
      UnDmkDAC_PF.AbreMySQLQUery0(Qry1, Dmod.MyDB, [
      'SELECT * ',
      'FROM pqx ',
      'WHERE CliOrig=' + Geral.FF0(CliOrig),
      'AND Insumo=' + Geral.FF0(Insumo),
      'AND Peso>0 ',
      'AND SdoPeso>0 ',
      'ORDER BY DataX DESC, Tipo DESC, OrigemCtrl DESC ',
      '']);
      //
      Qry1.First;
      while not Qry1.Eof do
      begin
        OrigemCodi := Qry1.FieldByName('OrigemCodi').AsInteger;
        OrigemCtrl := Qry1.FieldByName('OrigemCtrl').AsInteger;
        Tipo       := Qry1.FieldByName('Tipo').AsInteger;
        Peso2      := Qry1.FieldByName('SdoPeso').AsFloat;
        Valor2     := Qry1.FieldByName('SdoValr').AsFloat;
        if Peso1 <= 0 then
        begin
          Qry1.Last;
          // ter certeza!!!
          Qry1.Next;
        end else
        begin
          if Peso1 > Peso2 then
          begin
            Peso    := Peso2;
            Peso1   := Peso1 - Peso2;
            //Peso2   := 0;
          end else
          begin
            Peso    := Peso1;
            //Peso2   := Peso2 - Peso1;
            Peso1   := 0;
          end;
          if Peso2 > 0 then
          begin
            Valor := Valor2 / Peso2 * Peso
          end else
          begin
            Valor := 0;
          end;
          DstCodi := OrigemCodi;
          DstCtrl := OrigemCtrl;
          DstTipo := Tipo;
          //
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqw', False, [
          'Peso', 'Valor'], [
          'OriCodi', 'OriCtrl', 'OriTipo', 'DstCodi', 'DstCtrl', 'DstTipo'], [
          Peso, Valor], [
          OriCodi, OriCtrl, OriTipo, DstCodi, DstCtrl, DstTipo], False);
          //
          Peso  := Qry1.FieldByName('Peso').AsFloat;
          Valor := Qry1.FieldByName('Valor').AsFloat;
          AtualizaSdoPQx(Qry2, DstCodi, DstCtrl, DstTipo);
        end;
        //
        Qry1.Next;
      end;
    finally
      Qry2.Free;
    end;
  finally
    Qry1.Free;
  end;
end;

function TUnPQ_PF.ValidaProdutosFormulasAcabto(Numero: Integer): Boolean;
begin
  Result := True;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT tit.* ',
    'FROM tintasits tit ',
    'LEFT JOIN tintascab tca ON tca.Numero = tit.Numero ',
    'LEFT JOIN pq pqi ON pqi.Codigo = tit.Produto ',
    'WHERE (pqi.Ativo = 0 OR pqi.GGXNiv2 = 0) ',
    'AND tca.Numero=' + Geral.FF0(Numero),
    '']);
  if Dmod.QrAux.RecordCount > 0 then
    Result := False;
  //
  Dmod.QrAux.Close;
end;

function TUnPQ_PF.ValidaProdutosFormulasRibeira(Numero: Integer): Boolean;
begin
  Result := True;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT foi.* ',
    'FROM formulasits foi ',
    'LEFT JOIN formulas frm ON frm.Numero=foi.Numero ',
    'LEFT JOIN pq pqi ON pqi.Codigo=foi.Produto ',
    'WHERE (pqi.Ativo = 0 OR pqi.GGXNiv2 = 0) ',
    'AND frm.Numero=' + Geral.FF0(Numero),
    '']);
  if Dmod.QrAux.RecordCount > 0 then
    Result := False;
  //
  Dmod.QrAux.Close;
end;

procedure TUnPQ_PF.CorrigePQGGXNiv2;
var
  Nivel1, Nivel2: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT * ',
      'FROM pq ',
      'WHERE GGXNiv2 = -1 ',
      '']);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE pq SET GGXNiv2 = Ativo; ',
        'UPDATE pq SET Ativo = 1 WHERE Ativo <> 0; ',
        '']);
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT its.Codigo, its.GGXNiv2, gg1.Nivel2 ',
      'FROM pq its ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1 = its.Codigo ',
      'WHERE (its.GGXNiv2 * -1) <> ',
      '( ',
      'SELECT Nivel2 ',
      'FROM gragru1 ',
      'WHERE Nivel1 = its.Codigo ',
      ') ',
      '']);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      while not Dmod.QrAux.Eof do
      begin
        Nivel1 := Dmod.QrAux.FieldByName('Codigo').AsInteger;
        Nivel2 := Dmod.QrAux.FieldByName('GGXNiv2').AsInteger * -1;
        //
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False,
          ['Nivel2'], ['Nivel1'], [Nivel2], [Nivel1], True) then
        begin
          Geral.MB_Erro('Falha ao atualizar produtos!');
          Exit;
        end;
        //
        Dmod.QrAux.Next;
      end;
    end;
    //
    if DBCheck.CriaFm(TFmPQCorrigeGGXNiv2, FmPQCorrigeGGXNiv2, afmoNegarComAviso) then
    begin
      if FmPQCorrigeGGXNiv2.ReopenPQ then
        FmPQCorrigeGGXNiv2.ShowModal;
      FmPQCorrigeGGXNiv2.Destroy;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT * ',
      'FROM pq ',
      'WHERE GGXNiv2 = 0 ',
      '']);
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUnPQ_PF.DefineDtCorrApoTxt(CkDtCorrApo: TCheckBox;
  TPDtCorrApo: TdmkEditDateTimePicker): String;
var
  DataCA: TDateTime;
begin
  if CkDtCorrApo.Checked then
    DataCA := Int(TPDtCorrApo.Date)
  else
    DataCA := EncodeDate(1899, 12, 30);
  //
  Result := Geral.FDT(DataCA, 1);
end;

function TUnPQ_PF.DefineVariaveisPesqusaEstoquePQ(const EdPQ, EdCIDst, EdCIOri:
  TdmkEditCB; const RGOrdem1, RGOrdem2, RGOrdem3: TRadioGroup; CGTipoCad:
  TdmkCheckGroup; var FCIDst,
  FCIOri, FPQ: Integer; var FSE, FTC: String): Boolean;
const
  Setores: array[0..3] of String = ('NOMECI', 'NOMESE', 'NOMEPQ', 'NOMEFO');
var
  I, K: Integer;
begin
  FCIDst := EdCIOri.ValueVariant;
  FCIOri := EdCIDst.ValueVariant;
  FPQ    := EdPQ.ValueVariant;
  FSE := 'ORDER BY ' +
         Setores[RGOrdem1.ItemIndex] + ', ' +
         Setores[RGOrdem2.ItemIndex] + ', ' +
         Setores[RGOrdem3.ItemIndex];
  //FDI := TPIni.Date;
  //FDF := TPFim.Date;
  FTC := '';
  //F009_Individual := Ck009_Individual.Checked;
  //
  for I := 0 to CGTipoCad.Items.Count -1 do
  begin
    K := CGTipoCad.GetIndexOfCheckedStatus(I);
    if K > -1 then
      FTC := FTC + ',' + FormatFloat('0', K);
  end;
  if FTC <> '' then
    FTC := Copy(FTC, 2);
end;

function TUnPQ_PF.ExcluiPesagem(Emit, Tipo: Integer; PB: TprogressBar): Boolean;
var
  N, VSMovCod: Integer;
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    N := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT VSMovCod ',
    'FROM emit ',
    'WHERE Codigo=' + Geral.FF0(Emit),
    '']);
    VSMovCod := Qry.FieldByName('VSMovCod').AsInteger;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM pqx ',
    'WHERE OrigemCodi=' + Geral.FF0(Emit),
    'AND pqx.Tipo=' + Geral.FF0(Tipo),
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if Dmod.ImpedePeloRetornoPQ(
      Qry.FieldByName('OrigemCodi').AsInteger,
      Qry.FieldByName('OrigemCtrl').AsInteger,
      Qry.FieldByName('Tipo').AsInteger, False) then
        N := N + 1;
      //
      Qry.Next;
    end;
    if N > 0 then
    begin
      Geral.MB_Aviso(Geral.FF0(N) +
      ' itens da baixa impedem a exclus�o de toda receita porque j� possuem retorno!');
      Exit;
    end;
    if Emit <> 0 then
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o de TODA pesagem?' + sLineBreak +
      'Obs.: A receita espelho ser� mantida!') = ID_YES then
      begin
        PB.Visible := True;
        try
          //if Pertence then Exit;
          PB.Position := 0;
          PB.Max := Qry.RecordCount + 5;
          //
          PB.Position := PB.Position + 1;
          PB.Update;
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'DELETE FROM wbmovits ',
          'WHERE MovimID =' + Geral.FF0(Integer(emidIndsWE)),
          'AND LnkNivXtr1=' + Geral.FF0(Emit),
          '']);
          //
          PB.Position := PB.Position + 1;
          PB.Update;
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM emitcus WHERE Codigo=:P0');
          Dmod.QrUpd.Params[0].AsInteger := Emit;
          Dmod.QrUpd.ExecSQL;
          //
          PB.Position := PB.Position + 1;
          PB.Update;
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM emitits WHERE Codigo=:P0');
          Dmod.QrUpd.Params[0].AsInteger := Emit;
          Dmod.QrUpd.ExecSQL;
          //
          PB.Position := PB.Position + 1;
          PB.Update;
          //
(*
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM p q x WHERE OrigemCodi=:P0 AND Tipo=110');
          Dmod.QrUpd.Params[0].AsInteger := Emit;
          Dmod.QrUpd.ExecSQL;
          //
          Qry.First;
          while not Qry.Eof do
          begin
            PB.Position := PB.Position + 1;
            PB.Update;
            Application.ProcessMessages;
            UnPQx.AtualizaEstoquePQ(
              Qry.FieldByName('CliOrig').AsInteger,
              Qry.FieldByName('Insumo').AsInteger, aeMsg, '');
            Qry.Next;
          end;
*)
          ExcluiPQx_Mul(Qry, Emit, VAR_FATID_0110, PB, True);
          //
          PB.Position := PB.Position + 1;
          PB.Update;
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM emit WHERE Codigo=:P0');
          Dmod.QrUpd.Params[0].AsInteger := Emit;
          Dmod.QrUpd.ExecSQL;
          //
          Result := True;
          //
          if VSMovCod <> 0 then
            DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(VSMovCod);
        except
          raise;
        end;
        Screen.Cursor := crDefault;
        PB.Visible := False;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnPQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo:
  Integer; Avisa: Boolean; Empresa: Integer): Boolean;
var
  Qry1, Qry2: TmySQLQuery;
  DstCodi, DstCtrl, DstTipo: Integer;
  Peso, Valor: Double;
begin
  Result := False;
  //
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry2 := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT DstCodi, DstCtrl, DstTipo ',
      'FROM pqw ',
      'WHERE OriCodi=' + Geral.FF0(OriCodi),
      'AND OriCtrl=' + Geral.FF0(OriCtrl),
      'AND OriTipo=' + Geral.FF0(OriTipo),
      '']);
      //Geral.MB_SQL(nil, Qry1);
      //
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      'DELETE FROM pqx ' +
      ' WHERE Tipo=' + Geral.FF0(OriTipo) +
      ' AND OrigemCodi=' + Geral.FF0(OriCodi) +
      ' AND OrigemCtrl=' + Geral.FF0(OriCtrl));
      //
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      'DELETE FROM pqw ' +
      ' WHERE OriCodi=' + Geral.FF0(OriCodi) +
      ' AND OriCtrl=' + Geral.FF0(OriCtrl) +
      ' AND OriTipo=' + Geral.FF0(OriTipo));
      //
      UnPQx.AtualizaEstoquePQ(CliInt, Insumo, Empresa, aeVAR, '');
      //
      Qry1.First;
      while not Qry1.Eof do
      begin
        DstCodi := Qry1.FieldByName('DstCodi').AsInteger;
        DstCtrl := Qry1.FieldByName('DstCtrl').AsInteger;
        DstTipo := Qry1.FieldByName('DstTipo').AsInteger;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
        'SELECT Peso, Valor ',
        'FROM pqx ',
        'WHERE OrigemCodi=' + Geral.FF0(DstCodi),
        'AND OrigemCtrl=' + Geral.FF0(DstCtrl),
        'AND Tipo=' + Geral.FF0(DstTipo),
        '']);
        //Geral.MB_SQL(nil, Qry2);
        Peso  := Qry2.FieldByName('Peso').AsFloat;
        Valor := Qry2.FieldByName('Valor').AsFloat;
        AtualizaSdoPQx(Qry2, DstCodi, DstCtrl, DstTipo);
        //
        Qry1.Next;
      end;
      if Avisa then
        VerificaEquiparacaoEstoque(True);
      Result := True;
    finally
      Qry2.Free;
    end;
  finally
    Qry1.Free;
  end;
end;

function TUnPQ_PF.ExcluiPQx_Mul(QrPQX: TmySQLQuery; OriCodi, OriTipo:
  Integer; PB: TProgressBar; VerificaDiferencas: Boolean): Boolean;
var
  Qry1, Qry2: TmySQLQuery;
  DstCodi, DstCtrl, DstTipo: Integer;
  Peso, Valor: Double;
begin
  Result := False;
  //
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry2 := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT DstCodi, DstCtrl, DstTipo ',
      'FROM pqw ',
      'WHERE OriCodi=' + Geral.FF0(OriCodi),
      //'AND OriCtrl=' + Geral.FF0(OriCtrl),
      'AND OriTipo=' + Geral.FF0(OriTipo),
      '']);
      //Geral.MB_SQL(nil, Qry1);
      //
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      'DELETE FROM pqx ' +
      ' WHERE Tipo=' + Geral.FF0(OriTipo) +
      ' AND OrigemCodi=' + Geral.FF0(OriCodi));
      //' AND OrigemCtrl=' + Geral.FF0(OriCtrl));
      //
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      'DELETE FROM pqw ' +
      ' WHERE OriCodi=' + Geral.FF0(OriCodi) +
      //' AND OriCtrl=' + Geral.FF0(OriCtrl) +
      ' AND OriTipo=' + Geral.FF0(OriTipo));
      //
      //UnPQx.AtualizaEstoquePQ(CliInt, Insumo, aeMsg, '');
      if (OriTipo <> VAR_FATID_0010) or (QrPQx <> nil) then
      begin
        QrPQx.First;
        while not QrPQx.Eof do
        begin
          PB.Position := PB.Position + 1;
          PB.Update;
          Application.ProcessMessages;
          UnPQx.AtualizaEstoquePQ(
            QrPQx.FieldByName('CliOrig').AsInteger,
            QrPQx.FieldByName('Insumo').AsInteger,
            QrPQx.FieldByName('Empresa').AsInteger, aeMsg, '');
          QrPQx.Next;
        end;
      end;
      //
      Qry1.First;
      while not Qry1.Eof do
      begin
        DstCodi := Qry1.FieldByName('DstCodi').AsInteger;
        DstCtrl := Qry1.FieldByName('DstCtrl').AsInteger;
        DstTipo := Qry1.FieldByName('DstTipo').AsInteger;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
        'SELECT Peso, Valor ',
        'FROM pqx ',
        'WHERE OrigemCodi=' + Geral.FF0(DstCodi),
        'AND OrigemCtrl=' + Geral.FF0(DstCtrl),
        'AND Tipo=' + Geral.FF0(DstTipo),
        '']);
        //Geral.MB_SQL(nil, Qry2);
        Peso  := Qry2.FieldByName('Peso').AsFloat;
        Valor := Qry2.FieldByName('Valor').AsFloat;
        AtualizaSdoPQx(Qry2, DstCodi, DstCtrl, DstTipo);
        //
        Qry1.Next;
      end;
      if VerificaDiferencas then
        VerificaEquiparacaoEstoque(True);
      Result := True;
    finally
      Qry2.Free;
    end;
  finally
    Qry1.Free;
  end;
end;

procedure TUnPQ_PF.ExcluiReceita(Numero: Integer);
begin
  if Numero = 0 then
    Exit;
  //
  if UMyMod.ExcluiRegistro_EnviaArquivoMorto('tintazflu', 'tintasflu',
    dmkPF.MotivDel_ValidaCodigo(997), ['Numero'], [Numero],
    Dmod.MyDB) then
  begin
    UMyMod.ExcluiRegistroInt1('', 'tintasflu', 'Numero', Numero, Dmod.MyDB);
  end;
  //
  if UMyMod.ExcluiRegistro_EnviaArquivoMorto('tintazits', 'tintasits',
    dmkPF.MotivDel_ValidaCodigo(997), ['Numero'], [Numero],
    Dmod.MyDB) then
  begin
    UMyMod.ExcluiRegistroInt1('', 'tintasits', 'Numero', Numero, Dmod.MyDB);
  end;
  //
  if UMyMod.ExcluiRegistro_EnviaArquivoMorto('tintaztin', 'tintastin',
    dmkPF.MotivDel_ValidaCodigo(997), ['Numero'], [Numero],
    Dmod.MyDB) then
  begin
    UMyMod.ExcluiRegistroInt1('', 'tintastin', 'Numero', Numero, Dmod.MyDB);
  end;
  //
  if UMyMod.ExcluiRegistro_EnviaArquivoMorto('tintazcab', 'tintascab',
    dmkPF.MotivDel_ValidaCodigo(997), ['Numero'], [Numero],
    Dmod.MyDB) then
  begin
    UMyMod.ExcluiRegistroInt1('', 'tintascab', 'Numero', Numero, Dmod.MyDB);
  end;
end;

function TUnPQ_PF.ImpedePeloEstoqueNF_Item(OrigemCodi, OrigemCtrl, OrigemTipo:
  Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Peso, Valor  ',
    'FROM pqx ',
    'WHERE OrigemCodi=' + Geral.FF0(OrigemCodi),
    'AND OrigemCtrl=' + Geral.FF0(OrigemCtrl),
    'AND Tipo=' + Geral.FF0(OrigemTipo),
    'AND (Peso <> SdoPeso)',
    ' ']);
    Result := Qry.RecordCount > 0;
    if Result then
    begin
      Geral.MB_Aviso(
      'Este item entrada n�o pode mais ser editada ou exclu�do pois tem j� tem baixa!');
      Exit;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnPQ_PF.ImpedePeloEstoqueNF_Toda(OrigemCodi, OrigemTipo:
  Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Peso, Valor  ',
    'FROM pqx ',
    'WHERE OrigemCodi=' + Geral.FF0(OrigemCodi),
    'AND Tipo=' + Geral.FF0(OrigemTipo),
    'AND (Peso <> SdoPeso)',
    ' ']);
    Result := Qry.RecordCount > 0;
    if Result then
    begin
      Geral.MB_Aviso(
      'Esta entrada n�o pode mais ser editada pois tem item com estoque por NF j� baixado!');
      Exit;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnPQ_PF.InserePQx_Bxa(DataX: String; CliOrig, CliDest, Insumo: Integer;
  Peso, Valor: Double; OrigemCodi, OrigemCtrl, Tipo: Integer; Unitario:
  Boolean; DtCorrApo: String; _Empresa: Integer): Boolean;
var
  Empresa: Integer;
begin
  Empresa := _Empresa;
  if Empresa = 0 then
  begin
    Empresa := -11;
    Geral.MB_Erro('Empresa n�o definida! Ser� considerado -11!');
  end;
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_INS_TAB_PQX, False, [
  'DataX', 'CliOrig', 'CliDest',
  'Insumo', 'Peso', 'Valor'(*,
  'Retorno', 'StqMovIts'*), 'DtCorrApo',
  'Empresa'], [
  'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
  DataX, CliOrig, CliDest,
  Insumo, Peso, Valor(*,
  Retorno, StqMovIts*), DtCorrApo,
  Empresa], [
  OrigemCodi, OrigemCtrl, Tipo], False);
  //
  UnPQx.AtualizaEstoquePQ(CliOrig, Insumo, Empresa, aeMsg, CO_VAZIO);
  BaixaPQx(OrigemCodi, OrigemCtrl, Tipo, CliOrig, Insumo, -Peso);
  if Unitario then
    PQ_PF.VerificaEquiparacaoEstoque(True);
end;

function TUnPQ_PF.InserePQx_Inn(Query: TmySQLQuery; DataX: String; CliOrig,
  CliDest, Insumo: Integer; Peso, Valor: Double; HowLoad, OrigemCodi,
  OrigemCtrl, Tipo: Integer; DtCorrApo: String; _Empresa: Integer): Boolean;
var
  Empresa: Integer;
begin
  Result  := False;
  Empresa := _Empresa;
  if Empresa = 0 then
  begin
    Empresa := -11;
    Geral.MB_Erro('Empresa n�o definida! Ser� considerado -11!');
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_INS_TAB_PQX, False, [
  'DataX', 'CliOrig', 'CliDest',
  'Insumo', 'Peso', 'Valor',(*,
  'Retorno', 'StqMovIts', 'RetQtd',*)
  'HowLoad' (*'StqCenLoc', 'SdoPeso',
  'SdoValr', 'AcePeso', 'AceValr'*),
  'DtCorrApo', 'Empresa'], [
  'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
  DataX, CliOrig, CliDest,
  Insumo, Peso, Valor,(*,
  Retorno, StqMovIts, RetQtd,*)
  HowLoad (*StqCenLoc, SdoPeso,
  SdoValr, AcePeso, AceValr*),
  DtCorrApo, Empresa], [
  OrigemCodi, OrigemCtrl, Tipo], False) then
  begin
    AtualizaSdoPQx(Query, OrigemCodi, OrigemCtrl, Tipo);
    Result := True;
  end;
end;

function TUnPQ_PF.InsumosSemCadastroParaClienteInterno(CliInt, Empresa,
  InsumoEspecifico, FormulaEspecifica: Integer; Tabela: TTabPqNaoCad;
  MostraJanela: Boolean = True): Boolean;
var
  Qry: TmySQLQuery;
  Ds: TDataSource;
  //I, Numero: Integer;
  SQL_FormulaEspecifica: String;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    case Tabela of
      //TTabPqNaoCad.tpncND:
      TTabPqNaoCad.tpncPQEIts:
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT pq.Codigo, pq.Nome, pqc.* ',
        'FROM pq pq ',
        'LEFT JOIN pqcli pqc ON pqc.PQ=pq.Codigo',
        '  AND pqc.CI=' + Geral.FF0(CliInt),
        '  AND pqc.Empresa=' + Geral.FF0(Empresa),
        'WHERE pq.Codigo=' + Geral.FF0(InsumoEspecifico),
        'AND pqc.Controle IS NULL',
        '']);
      TTabPqNaoCad.tpncPesagem:
      begin
        if FormulaEspecifica <> 0 then
        SQL_FormulaEspecifica := 'AND frm.Numero=' + Geral.FF0(FormulaEspecifica);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT pq.Codigo, pq.Nome, fis.Numero Receita,  ',
        'fis.Ordem Linha, fis.Controle Item  ',
        'FROM formulasits fis  ',
        'LEFT JOIN formulas frm ON frm.Numero=fis.Numero ',
        'LEFT JOIN pq pq ON pq.Codigo=fis.Produto  ',
        'LEFT JOIN pqcli pqc ON pqc.PQ=fis.Produto  ',
        '  AND pqc.CI=' + Geral.FF0(CliInt),
        '  AND pqc.Empresa=' + Geral.FF0(Empresa),
        'WHERE fis.Produto>0  ',
        'AND fis.Porcent>0  ',
        'AND pqc.Controle IS NULL  ',
        'AND pq.GGXNiv2 IN (1,2,4,5) ',
        SQL_FormulaEspecifica,
        '']);
      end;
      else
        Geral.MB_Erro(
        'Tabela n�o implementada em "PQ_PF.InsumosSemCadastroParaClienteInterno()"');
    end;
    //
    Result := Qry.RecordCount > 0;
    //
    if (Result) and (MostraJanela) then
    begin
      Geral.MB_Aviso(
        'Existem produtos n�o cadastrados para o Cliente Interno na Empresa selecionada!' +
        sLineBreak + Qry.SQL.Text);
      //
      Ds         := TDataSource.Create(Dmod);
      Ds.DataSet := Qry;
      //
      Application.CreateForm(TFmGerlShowGrid, FmGerlShowGrid);
      FmGerlShowGrid.MeAvisos.Lines.Add('Os produtos abaixo n�o est�o cadastrados: ');
      FmGerlShowGrid.MeAvisos.Lines.Add('Cliente interno: ' + Geral.FF0(CliInt));
      FmGerlShowGrid.MeAvisos.Lines.Add('Empresa: ' + Geral.FF0(Empresa));
      FmGerlShowGrid.DBGSel.DataSource := Ds;
      FmGerlShowGrid.ShowModal;
      FmGerlShowGrid.Destroy;
    end;
  finally
    Ds.Free;
  end;
end;

procedure TUnPQ_PF.MostraFormFormulasImpBH();
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmFormulas2Imp, FmFormulas2Imp, afmoNegarComAviso) then
      begin
        FmFormulas2Imp.FEmit := UMyMod.BuscaEmLivreY(
          Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
        FmFormulas2Imp.ImgTipo.SQLType        := stIns;
        FmFormulas2Imp.PainelEscolhe.Visible := True;
        FmFormulas2Imp.FNomeForm             := 'Impress�o de Receita com Baixa';
        FmFormulas2Imp.EdReceita.Text        := '';
        FmFormulas2Imp.CBReceita.KeyValue    := NULL;
        //FmFormulas2Imp.PainelDefine.Visible  := True;
        FmFormulas2Imp.LaData.Visible        := True;
        FmFormulas2Imp.TPDataP.Visible       := True;
        FmFormulas2Imp.ShowModal;
        FmFormulas2Imp.Destroy;
      end;
    fggxPQ:
}
      if DBCheck.CriaFm(TFmFormulasImpBH, FmFormulasImpBH, afmoNegarComAviso) then
      begin
        (*FmFormulasImpBH.FEmit := UMyMod.BuscaEmLivreY(
          Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
        *)
        FmFormulasImpBH.FEmit := UMyMod.BuscaEmLivreY_Def('Emit', 'Codigo', stIns, 0);
        FmFormulasImpBH.PainelEscolhe.Visible := True;
        FmFormulasImpBH.FNomeForm             := 'Impress�o de Receita com Baixa';
        FmFormulasImpBH.EdReceita.Text        := '';
        FmFormulasImpBH.CBReceita.KeyValue    := NULL;
        //FmFormulasImpBH.PainelDefine.Visible  := True;
        FmFormulasImpBH.LaData.Visible        := True;
        FmFormulasImpBH.TPDataP.Visible       := True;
        FmFormulasImpBH.ShowModal;
        FmFormulasImpBH.Destroy;
      end;
{
  end;
}
end;

procedure TUnPQ_PF.MostraFormFormulasImpFI();
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmTintasImp2, FmTintasImp2, afmoNegarComAviso) then
      begin
        FmTintasImp2.FEmit := UMyMod.BuscaEmLivreY(
          Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
        FmTintasImp2.ImgTipo.SQLType        := stIns;
        (*
        FmTintasImp2.PainelEscolhe.Visible := True;
        FmTintasImp2.FNomeForm             := 'Impress�o de Receita com Baixa';
        FmTintasImp2.EdReceita.Text        := '';
        FmTintasImp2.CBReceita.KeyValue    := NULL;
        FmTintasImp2.LaData.Visible        := True;
        FmTintasImp2.TPDataP.Visible       := True;
        *)
        FmTintasImp2.ShowModal;
        FmTintasImp2.Destroy;
      end;
    fggxPQ:
}
      if DBCheck.CriaFm(TFmFormulasImpFI, FmFormulasImpFI, afmoNegarComAviso) then
      begin
        FmFormulasImpFI.DefineBaixa(True);
        FmFormulasImpFI.ShowModal;
        FmFormulasImpFI.Destroy;
      end;
{
  end;
}
end;

procedure TUnPQ_PF.MostraFormFormulasImpWE();
begin
  if DBCheck.CriaFm(TFmFormulasImpWE2, FmFormulasImpWE2, afmoNegarComAviso) then
  begin
    FmFormulasImpWE2.FEmit := UMyMod.BuscaEmLivreY(
      Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
    FmFormulasImpWE2.PainelEscolhe.Visible := True;
    FmFormulasImpWE2.FNomeForm             := 'Impress�o de Receita com Baixa';
    FmFormulasImpWE2.EdReceita.Text        := '';
    FmFormulasImpWE2.CBReceita.KeyValue    := NULL;
    //FmFormulasImpWE2.PainelDefine.Visible := True;
    FmFormulasImpWE2.LaData.Visible        := True;
    FmFormulasImpWE2.TPDataP.Visible       := True;
    FmFormulasImpWE2.ShowModal;
    FmFormulasImpWE2.Destroy;
  end;
end;

procedure TUnPQ_PF.MostraFormVSFormulasImp_BH(MovimCod, IMEI, TemIMEIMrt:
  Integer; MovimInn, MovimSrc: TEstqMovimNiv; ReceitaRibSetor: TReceitaRibSetor);
begin
  if DBCheck.CriaFm(TFmVSFormulasImp_BH, FmVSFormulasImp_BH, afmoNegarComAviso) then
  begin
    FmVSFormulasImp_BH.FEmit := UMyMod.BuscaEmLivreY_Def('Emit', 'Codigo', stIns, 0);
    FmVSFormulasImp_BH.FMovimInn             := MovimInn;
    FmVSFormulasImp_BH.FMovimSrc             := MovimSrc;
    FmVSFormulasImp_BH.FMovimCod             := MovimCod;
    FmVSFormulasImp_BH.FIMEI                 := IMEI;
    FmVSFormulasImp_BH.FTemIMEIMrt           := TemIMEIMrt;
    FmVSFormulasImp_BH.PainelEscolhe.Visible := True;
    FmVSFormulasImp_BH.FNomeForm             := 'Receita de Caleiro';
    FmVSFormulasImp_BH.EdReceita.Text        := '';
    FmVSFormulasImp_BH.CBReceita.KeyValue    := NULL;
    //FmVSFormulasImp_BH.PainelDefine.Visible  := True;
    FmVSFormulasImp_BH.LaData.Visible        := True;
    FmVSFormulasImp_BH.TPDataP.Visible       := True;
    FmVSFormulasImp_BH.FReceitaRibSetor      := ReceitaRibSetor;
    //
    FmVSFormulasImp_BH.ReopenVMIAtu();
    FmVSFormulasImp_BH.ReopenReceitas();
    //
    FmVSFormulasImp_BH.ShowModal;
    FmVSFormulasImp_BH.Destroy;
  end;
end;

procedure TUnPQ_PF.MostraFormVSFormulasImp_FI(MovimCod, IMEI, TemIMEIMrt,
  EmitGru: Integer; MovimInn, MovimSrc: TEstqMovimNiv);
begin
  if DBCheck.CriaFm(TFmVSFormulasImp_FI, FmVSFormulasImp_FI, afmoNegarComAviso) then
  begin
    FmVSFormulasImp_FI.FMovimInn              := MovimInn;
    FmVSFormulasImp_FI.FMovimSrc              := MovimSrc;
    FmVSFormulasImp_FI.FMovimCod              := MovimCod;
    FmVSFormulasImp_FI.FIMEI                  := IMEI;
    FmVSFormulasImp_FI.FTemIMEIMrt            := TemIMEIMrt;
    FmVSFormulasImp_FI.ReopenVMIAtu();
    FmVSFormulasImp_FI.EdEmitGru.ValueVariant := EmitGru;
    FmVSFormulasImp_FI.CBEmitGru.KeyValue     := EmitGru;
    FmVSFormulasImp_FI.DefineBaixa(True);
    FmVSFormulasImp_FI.ShowModal;
    FmVSFormulasImp_FI.Destroy;
  end;
end;

procedure TUnPQ_PF.MostraFormVSFormulasImp_WE(MovimCod, IMEI, TemIMEIMrt,
  EmitGru: Integer; MovimInn, MovimSrc: TEstqMovimNiv);
begin
  if DBCheck.CriaFm(TFmVSFormulasImp_WE, FmVSFormulasImp_WE, afmoNegarComAviso) then
  begin
    FmVSFormulasImp_WE.FEmit := UMyMod.BuscaEmLivreY(
      Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
    FmVSFormulasImp_WE.FMovimInn              := MovimInn;
    FmVSFormulasImp_WE.FMovimSrc              := MovimSrc;
    FmVSFormulasImp_WE.FMovimCod              := MovimCod;
    FmVSFormulasImp_WE.FIMEI                  := IMEI;
    FmVSFormulasImp_WE.FTemIMEIMrt            := TemIMEIMrt;
    FmVSFormulasImp_WE.PainelEscolhe.Visible  := True;
    FmVSFormulasImp_WE.FNomeForm              := 'Impress�o de Receita com Baixa';
    FmVSFormulasImp_WE.EdReceita.Text         := '';
    FmVSFormulasImp_WE.CBReceita.KeyValue     := NULL;
    //FmVSFormulasImp_WE.PainelDefine.Visible := True;
    FmVSFormulasImp_WE.LaData.Visible         := True;
    FmVSFormulasImp_WE.TPDataP.Visible        := True;
    FmVSFormulasImp_WE.ReopenVMIAtu();
    FmVSFormulasImp_WE.EdEmitGru.ValueVariant := EmitGru;
    FmVSFormulasImp_WE.CBEmitGru.KeyValue     := EmitGru;
    //
    FmVSFormulasImp_WE.ShowModal;
    FmVSFormulasImp_WE.Destroy;
  end;
end;

function TUnPQ_PF.MostraFormVSPQOCab(const SQLType: TSQLType; const VSMovCod,
  EmitGru: Integer; const PrecisaEmitGru: Boolean; var Codigo: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Codigo := 0;
  if DBCheck.CriaFm(TFmVSPQOCab, FmVSPQOCab, afmoNegarComAviso) then
  begin
    FmVSPQOCab.ImgTipo.SQLType := SQLType;
    FmVSPQOCab.EdVSMOvCod.ValueVariant := VSMOvCod;
    FmVSPQOCab.FPrecisaEmitGru         := PrecisaEmitGru;
    FmVSPQOCab.EdEmitGru.ValueVariant  := EmitGru;
    FmVSPQOCab.CBEmitGru.KeyValue      := EmitGru;
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * FROM listasetores ',
      'WHERE TpReceita=2 ',
      'AND Ativo=1 ',
      'ORDER BY Codigo ',
      '']);
      if Qry.RecordCount > 0 then
      begin
        FmVSPQOCab.EdSetor.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
        FmVSPQOCab.CBSetor.KeyValue     := Qry.FieldByName('Codigo').AsInteger;
      end;
    finally
      Qry.Free;
    end;
    //
    FmVSPQOCab.ShowModal;
    //
    if FmVSPQOCab.FCodigo <> 0 then
    begin
      Codigo := FmVSPQOCab.FCodigo;
      Result := True;
    end;
    FmVSPQOCab.Destroy;
  end;
end;

function TUnPQ_PF.MostraFormVSPQOIts(const SQLType: TSQLType; const Codigo:
  Integer; Data: TDateTime; var Controle: Integer): Boolean;
begin
  Result := False;
  Controle := 0;
  if DBCheck.CriaFm(TFmVSPQOIts, FmVSPQOIts, afmoNegarComAviso) then
  begin
    FmVSPQOIts.ImgTipo.SQLType        := SQLType;
    FmVSPQOIts.EdOriCodi.ValueVariant := Codigo;
    FmVSPQOIts.FDataX                 := Data;
    //
    FmVSPQOIts.ShowModal;
    if FmVSPQOIts.FControle <> 0 then
    begin
      Controle := FmVSPQOIts.FControle;
      Result := True;
    end;
    FmVSPQOIts.Destroy;
  end;
end;

function TUnPQ_PF.ObtemDadosGradeDePQ(const Insumo: Integer; var Tipo_Item,
  GraGruX, UnidMed: Integer; var Sigla, NCM: String; const Avisa: Boolean): Boolean;
const
  sProcName = 'PQ_PF.ObtemDadosGradeDePQ()';
var
  Qry: TmySQLQuery;
  Nivel1: Integer;
begin
  Nivel1 := Insumo;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
    'pgt.Tipo_Item) Tipo_Item, ggx.Controle, gg1.UnidMed, ',
    'gg1.NCM, med.Sigla ',
    'FROM gragrux ggx',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed',
    'WHERE ggx.GraGru1=' + Geral.FF0(Nivel1),
    'ORDER BY ggx.Controle ',
    '']);
    //
    if Qry.RecordCount > 0 then
    begin
      Tipo_Item := Qry.FieldByName('Tipo_Item').AsInteger;
      GraGruX   := Qry.FieldByName('Controle').AsInteger;
      UnidMed   := Qry.FieldByName('UnidMed').AsInteger;
      Sigla     := Qry.FieldByName('Sigla').AsString;
      NCM       := Qry.FieldByName('NCM').AsString;
      Result    := True;
    end else
    begin
      Tipo_Item := 0;
      GraGruX   := 0;
      UnidMed   := 0;
      Sigla     := '';
      NCM       := '';
      Result    := False;
      //
      if Avisa then
        Geral.MB_Aviso('Reduzido n�o encontrado para o insumo ' +
        Geral.FF0(Insumo) + ' em "' + sProcName + '"');
    end;
  except
    Qry.Free;
  end;
end;

function TUnPQ_PF.ObtemNomeTabelaTipoMovPQ(Tipo: Integer): String;
begin
  case Tipo of
    VAR_FATID_0010: Result := 'pqe';
    VAR_FATID_0030: Result := 'pqm';
    VAR_FATID_0110: Result := 'emit';
    VAR_FATID_0130: Result := 'pqm';
    VAR_FATID_0150: Result := 'pqt';
    VAR_FATID_0170: Result := 'pqd';
    VAR_FATID_0190: Result := 'pqo';
    else Result := '*??_ObtemNomeTabelaTipoMovPQ_??*';
  end;
end;
//
function TUnPQ_PF.ObtemSetoresSelecionados(const DBGSetores: TdmkDBGridZTO;
  const QrListaSetores: TmySQLQuery; var FSetoresCod, FSetoresTxt:
  String): Boolean;
var
  N, I: Integer;
  Liga1, Liga2: String;
begin
  Result := False;
  Liga1 := '';
  Liga2 := '';
  FSetoresCod := '';
  FSetoresTxt := '';
  if MyObjects.FIC(DBGSetores.SelectedRows.Count < 1, nil,
  'Selecione pelo menos um setor!') then Exit;
  //
  QrListaSetores.DisableControls;
  try
    N := 0;
    with DBGSetores.DataSource.DataSet do
    for I := 0 to DBGSetores.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(DBGSetores.SelectedRows.Items[I]));
      ///////////////
      FSetoresCod := FSetoresCod + Liga1 + Geral.FF0(
        QrListaSetores.FieldByName('Codigo').AsInteger);
      FSetoresTxt := FSetoresTxt + Liga2 +
        QrListaSetores.FieldByName('Nome').AsString;
      Liga1 := ',';
      Liga2 := ', ';
      ///////////////
    end;
    Result := True;
  finally
    QrListaSetores.EnableControls;
  end;
end;

procedure TUnPQ_PF.PQO_AtualizaCusto(Codigo: Integer);
var
  Qry: TmySQLQuery;
  CustoInsumo, CustoTotal: Double;
begin
  Qry := TmySQLQuery.Create(Dmod.MyDB);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(pqx.Valor)*-1 CUSTO ',
    'FROM pqx pqx ',
    'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
    'AND pqx.OrigemCodi=' + Geral.FF0(Codigo),
    '']);
    CustoInsumo := Qry.FieldByName('CUSTO').AsFloat;
    CustoTotal  := Qry.FieldByName('CUSTO').AsFloat;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pqo', False, [
    'CustoInsumo', 'CustoTotal'], [
    'Codigo'], [
    CustoInsumo, CustoTotal], [
    Codigo], True);
  finally
    Qry.Free;
  end;
end;

procedure TUnPQ_PF.PQO_ExcluiCab(Codigo: Integer);
var
  Qry: TmySQLQuery;
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o de TODOS ITENS desta baixa e a pr�pria baixa?') =
  ID_YES then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DISTINCT pqx.* ',
      'FROM pqx pqx ',
      'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
      'AND pqx.OrigemCodi=' + Geral.FF0(Codigo),
      '']);
      Qry.First;
      while not Qry.Eof do
      begin
        PQO_ExcluiItem(Qry.FieldByName('OrigemCtrl').AsInteger, False, False);
        Qry.Next;
      end;
      //AtualizaCusto;
      UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
      'DELETE FROM pqo ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
      //
      //LocCod(QrPQOCodigo.Value, QrPQOCodigo.Value);
    finally
      Qry.Free;
    end;
  end;
end;

procedure TUnPQ_PF.PQO_ExcluiItem(Controle: Integer; Pergunta, AtzCusto: Boolean);
var
  Atual, OriCodi, OriCtrl, OriTipo, Insumo, CliInt, Empresa: Integer;
  Qry: TmySQLQuery;
  Continua: Integer;
begin
  if Pergunta then Continua := Geral.MB_Pergunta(
    'Confirma a exclus�o do item selecionado nesta baixa?')
  else
    Continua := ID_YES;
  if Continua = ID_YES then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * FROM pqx ',
      'WHERE Tipo=' + Geral.FF0(VAR_FATID_0190),
      'AND OrigemCtrl=' + Geral.FF0(Controle),
      '']);
      Insumo  := Qry.FieldByName('Insumo').AsInteger;
      CliInt  := Qry.FieldByName('CliOrig').AsInteger;
      Empresa := Qry.FieldByName('Empresa').AsInteger;
      Atual   := Qry.FieldByName('OrigemCtrl').AsInteger;
      //
      OriCodi := Qry.FieldByName('OrigemCodi').AsInteger;
      OriCtrl := Atual;
      OriTipo := VAR_FATID_0190;
      PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, True, Empresa);
      //
      if AtzCusto then
        PQO_AtualizaCusto(OriCodi);
      UnPQx.AtualizaEstoquePQ(CliInt, Insumo, Empresa, aeNenhum, '');
    finally
      Qry.Free;
    end;
  end;
end;

procedure TUnPQ_PF.ReimprimePesagemSomeWet(Emit: Integer; PB1: TProgressBar);
const
  TemIMEIMrt = 1;
var
  QrEmit, QrVMI: TmySQLQuery;
  VSMovCod: Integer;
  MovimNiv: TEstqMovimNiv;
begin
  BDC_APRESENTACAO_IMPRESSAO := MyObjects.SelRadioGroup('Impress�o de Pesagem',
    'Apresenta��o', sListaRecImpApresenta, 5, 0);
  if BDC_APRESENTACAO_IMPRESSAO = -1 then
    Exit;
  //
  QrEmit := TmySQLQuery.Create(Dmod);
  try
  QrVMI := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
    'SELECT emi.*, gru.Nome NO_EmitGru  ',
    'FROM emit emi ',
    'LEFT JOIN emitgru gru ON gru.Codigo=emi.EmitGru ',
    'WHERE emi.Codigo=' + Geral.FF0(Emit),
    '']);
    //
    VSMovCod := QrEmit.FieldByName('VSMovCod').AsInteger;
    if VSMovCod <> 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
      'SELECT MovimID  ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(VSMovCod),
      '']);
      case TEstqMovimID(QrVMI.FieldByName('MovimID').AsInteger) of
        emidEmProcCal: MovimNiv := eminSorcCal;
        emidEmProcCur: MovimNiv := eminSorcCur;
        else
        begin
          Geral.MB_Erro('"MovimNiv" indefinido em "ReimprimePesagem()"');
          MovimNiv := eminSemNiv;
        end;
      end;
    end else
      MovimNiv := eminSemNiv;
    //
    //
    //BDC_APRESENTACAO_IMPRESSAO := RGImpRecRib.ItemIndex;
    VAR_SETOR    := '';
    VAR_SETORNUM := QrEmit.FieldByName('Setor').AsInteger;
    //
    case Dmod.QrControleVerImprRecRib.Value of
      0:
      begin
        FrFormulasImpShow.FReImprime    := True;
        FrFormulasImpShow.FEmit         := QrEmit.FieldByName('Codigo').AsInteger;
        FrFormulasImpShow.FEmitGru      := QrEmit.FieldByName('EmitGru').AsInteger;
        FrFormulasImpShow.FVSMovCod     := VSMovCod;
        FrFormulasImpShow.FVSMovimSrc   := MovimNiv;
        FrFormulasImpShow.FTemIMEIMrt   := TemIMEIMrt;
        FrFormulasImpShow.FEmitGru_TXT  := QrEmit.FieldByName('NO_EmitGru').AsString;
        FrFormulasImpShow.FRetrabalho   := QrEmit.FieldByName('Retrabalho').AsInteger;
        FrFormulasImpShow.FSourcMP      := QrEmit.FieldByName('SourcMP').AsInteger;
        FrFormulasImpShow.FDataP        := QrEmit.FieldByName('DataEmis').AsDateTime;
        //FrFormulasImpShow.FDtCorrApo    := Geral.FDT(QrEmit.FieldByName('DtCorrApo').AsDateTime, 1);
        FrFormulasImpShow.FDtCorrApo    := '????/??/??';
        FrFormulasImpShow.FEmpresa      := QrEmit.FieldByName('Empresa').AsInteger;
        FrFormulasImpShow.FHoraIni      := QrEmit.FieldByName('HoraIni').AsDateTime;
        FrFormulasImpShow.FProgressBar1 := PB1;
        FrFormulasImpShow.FTipoPreco    := -1;// n�o deve usar
        FrFormulasImpShow.FCliIntCod    := QrEmit.FieldByName('ClienteI').AsInteger;
        FrFormulasImpShow.FCliIntTxt    := QrEmit.FieldByName('NOMECI').AsString;
        FrFormulasImpShow.FFormula      := QrEmit.FieldByName('Numero').AsInteger;
        FrFormulasImpShow.FTipoImprime  := 0; //Visualiza! RGImprime.ItemIndex;
        FrFormulasImpShow.FPeso         := QrEmit.FieldByName('Peso').AsFloat;
        FrFormulasImpShow.FQtde         := QrEmit.FieldByName('Qtde').AsFloat;
        FrFormulasImpShow.FArea         := QrEmit.FieldByName('AreaM2').AsInteger;
        FrFormulasImpShow.FLinhasRebx   := QrEmit.FieldByName('Espessura').AsString;
        FrFormulasImpShow.FLinhasSemi   := QrEmit.FieldByName('SemiTxtEspReb').AsString;
        FrFormulasImpShow.FFulao        := QrEmit.FieldByName('Fulao').AsString;
        FrFormulasImpShow.FDefPeca      := QrEmit.FieldByName('DefPeca').AsString;
        FrFormulasImpShow.FHHMM_P       := dmkPF.HorasMH(QrEmit.FieldByName('TempoP').AsInteger, False);
        FrFormulasImpShow.FHHMM_R       := dmkPF.HorasMH(QrEmit.FieldByName('TempoR').AsInteger, False);
        FrFormulasImpShow.FHHMM_T       := dmkPF.HorasMH(QrEmit.FieldByName('TempoP').AsInteger + QrEmit.FieldByName('TempoR').AsInteger, False);
        FrFormulasImpShow.FObs          := QrEmit.FieldByName('Obs').AsString;
        FrFormulasImpShow.FSetor        := QrEmit.FieldByName('Setor').AsInteger;
        FrFormulasImpShow.FTipific      := QrEmit.FieldByName('Tipificacao').AsInteger;
        FrFormulasImpShow.FCod_Espess   := QrEmit.FieldByName('Cod_Espess').AsInteger;
        FrFormulasImpShow.FCodDefPeca   := QrEmit.FieldByName('CodDefPeca').AsInteger;
        FrFormulasImpShow.FSemiAreaM2   := QrEmit.FieldByName('SemiAreaM2').AsFloat;
        FrFormulasImpShow.FSemiRendim   := QrEmit.FieldByName('SemiRendim').AsInteger;
        FrFormulasImpShow.FBRL_USD      := QrEmit.FieldByName('BRL_USD').AsFloat;
        FrFormulasImpShow.FBRL_EUR      := QrEmit.FieldByName('BRL_EUR').AsFloat;
        FrFormulasImpShow.FDtaCambio    := QrEmit.FieldByName('DtaCambio').AsDateTime;
        //
        //if CkMatricial.Checked then FrFormulasImpShow.FMatricialNovo := True
        //else
        FrFormulasImpShow.FMatricialNovo := False;
        //
        if BDC_APRESENTACAO_IMPRESSAO = 1 then FrFormulasImpShow.FCalcCustos := True
          else FrFormulasImpShow.FCalcCustos := False;
        if BDC_APRESENTACAO_IMPRESSAO = 2 then FrFormulasImpShow.FCalcPrecos := True
          else FrFormulasImpShow.FCalcPrecos := False;
        //if RBkg.Checked then FrFormulasImpShow.FPesoCalc := 0.01
        //else
        FrFormulasImpShow.FPesoCalc := 10;
        //
        if FrFormulasImpShow.FLinhasRebx = '' then
          FrFormulasImpShow.FMedia := FrFormulasImpShow.FEMCM
        else begin
          if FrFormulasImpShow.FQtde > 0 then FrFormulasImpShow.FMedia := FrFormulasImpShow.FPeso / FrFormulasImpShow.FQtde
          else FrFormulasImpShow.FMedia := 0;
        end;
        //
        FrFormulasImpShow.CalculaReceita(nil);
      end;
      1:
      begin
        FrFormulasImpShowNew.FReImprime    := True;
        FrFormulasImpShowNew.FEmit         := QrEmit.FieldByName('Codigo').AsInteger;
        FrFormulasImpShowNew.FEmitGru      := QrEmit.FieldByName('EmitGru').AsInteger;
        FrFormulasImpShowNew.FVSMovCod     := QrEmit.FieldByName('VSMovCod').AsInteger;
        FrFormulasImpShowNew.FVSMovimSrc   := MovimNiv;
        FrFormulasImpShowNew.FTemIMEIMrt   := TemIMEIMrt;
        FrFormulasImpShowNew.FEmitGru_TXT  := QrEmit.FieldByName('NO_EmitGru').AsString;
        FrFormulasImpShowNew.FRetrabalho   := QrEmit.FieldByName('Retrabalho').AsInteger;
        FrFormulasImpShowNew.FSourcMP      := QrEmit.FieldByName('SourcMP').AsInteger;
        FrFormulasImpShowNew.FDataP        := QrEmit.FieldByName('DataEmis').AsDateTime;
        //FrFormulasImpShowNew.FDtCorrApo    := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
        FrFormulasImpShowNew.FDtCorrApo    := '????/??/??';
        FrFormulasImpShowNew.FEmpresa      := QrEmit.FieldByName('Empresa').AsInteger;
        FrFormulasImpShowNew.FProgressBar1 := PB1;
        FrFormulasImpShowNew.FTipoPreco    := -1;// n�o deve usar
        FrFormulasImpShowNew.FCliIntCod    := QrEmit.FieldByName('ClienteI').AsInteger;
        FrFormulasImpShowNew.FCliIntTxt    := QrEmit.FieldByName('NOMECI').AsString;
        FrFormulasImpShowNew.FFormula      := QrEmit.FieldByName('Numero').AsInteger;
        FrFormulasImpShowNew.FTipoImprime  := 0; //Visualiza! RGImprime.ItemIndex;
        FrFormulasImpShowNew.FPeso         := QrEmit.FieldByName('Peso').AsFloat;
        FrFormulasImpShowNew.FQtde         := QrEmit.FieldByName('Qtde').AsFloat;
        FrFormulasImpShowNew.FArea         := QrEmit.FieldByName('AreaM2').AsInteger;
        FrFormulasImpShowNew.FLinhasRebx   := QrEmit.FieldByName('Espessura').AsString;
        FrFormulasImpShowNew.FLinhasSemi   := QrEmit.FieldByName('SemiTxtEspReb').AsString;
        FrFormulasImpShowNew.FFulao        := QrEmit.FieldByName('Fulao').AsString;
        FrFormulasImpShowNew.FDefPeca      := QrEmit.FieldByName('DefPeca').AsString;
        FrFormulasImpShowNew.FHHMM_P       := dmkPF.HorasMH(QrEmit.FieldByName('TempoP').AsInteger, False);
        FrFormulasImpShowNew.FHHMM_R       := dmkPF.HorasMH(QrEmit.FieldByName('TempoR').AsInteger, False);
        FrFormulasImpShowNew.FHHMM_T       := dmkPF.HorasMH(QrEmit.FieldByName('TempoP').AsInteger + QrEmit.FieldByName('TempoR').AsInteger, False);
        FrFormulasImpShowNew.FObs          := QrEmit.FieldByName('Obs').AsString;
        FrFormulasImpShowNew.FSetor        := QrEmit.FieldByName('Setor').AsInteger;
        FrFormulasImpShowNew.FTipific      := QrEmit.FieldByName('Tipificacao').AsInteger;
        FrFormulasImpShowNew.FCod_Espess   := QrEmit.FieldByName('Cod_Espess').AsInteger;
        FrFormulasImpShowNew.FCodDefPeca   := QrEmit.FieldByName('CodDefPeca').AsInteger;
        FrFormulasImpShowNew.FSemiAreaM2   := QrEmit.FieldByName('SemiAreaM2').AsFloat;
        FrFormulasImpShowNew.FSemiRendim   := QrEmit.FieldByName('SemiRendim').AsInteger;
        FrFormulasImpShowNew.FBRL_USD      := QrEmit.FieldByName('BRL_USD').AsFloat;
        FrFormulasImpShowNew.FBRL_EUR      := QrEmit.FieldByName('BRL_EUR').AsFloat;
        FrFormulasImpShowNew.FDtaCambio    := QrEmit.FieldByName('DtaCambio').AsDateTime;
        //
        //if CkMatricial.Checked then FrFormulasImpShowNew.FMatricialNovo := True
        //else
        FrFormulasImpShowNew.FMatricialNovo := False;
        //
        if BDC_APRESENTACAO_IMPRESSAO = 1 then FrFormulasImpShowNew.FCalcCustos := True
          else
        FrFormulasImpShowNew.FCalcCustos := False;
        if BDC_APRESENTACAO_IMPRESSAO = 2 then FrFormulasImpShowNew.FCalcPrecos := True
          else FrFormulasImpShowNew.FCalcPrecos := False;
        //if RBkg.Checked then FrFormulasImpShowNew.FPesoCalc := 0.01
        //else
        FrFormulasImpShowNew.FPesoCalc := 10;
        //
        if FrFormulasImpShowNew.FLinhasRebx = '' then
          FrFormulasImpShowNew.FMedia := FrFormulasImpShowNew.FEMCM
        else begin
          if FrFormulasImpShowNew.FQtde > 0 then FrFormulasImpShowNew.FMedia := FrFormulasImpShowNew.FPeso / FrFormulasImpShowNew.FQtde
          else FrFormulasImpShowNew.FMedia := 0;
        end;
        //
        FrFormulasImpShowNew.CalculaReceita(nil);
      end;
      else
      begin
        Geral.MB_Erro('Vers�o de impress�o de receita de ribeira n�o implementada [4]!');
        Exit;
      end;
    end;
    Application.ProcessMessages;
  finally
    QrVMI.Free;
  end;
  finally
    QrEmit.Free;
  end;
end;

function TUnPQ_PF.TransfereReceitaDeArquivo(Orientacao: TOrientTabArqBD; Numero,
  Motivo: Integer): Boolean;
var
  Dta, Destino, Origem: String;
  //
  function ExcluiReceita_EnviaArquivoMorto(Numero, Motivo: Integer;
    Query: TmySQLQuery; DataBase: TmySQLDatabase): Boolean;
  var
    CamposZ: String;
  begin
    CamposZ := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, Destino, '');
    if Orientacao = TOrientTabArqBD.stabdAcessivelToMorto then
    begin
      CamposZ := Geral.Substitui(CamposZ,
        ', DataDel', ', "' + Dta + '" DataDel');
      CamposZ := Geral.Substitui(CamposZ,
        ', UserDel', ', ' + FormatFloat('0', VAR_USUARIO) + ' UserDel');
      CamposZ := Geral.Substitui(CamposZ,
        ', MotvDel', ', ' + FormatFloat('0', Motivo) + ' MotvDel');
    end;
    //
    CamposZ := 'INSERT INTO ' + Destino + ' SELECT ' + sLineBreak +
    CamposZ + sLineBreak +
    'FROM ' + Origem + sLineBreak +
    'WHERE Numero=' + Geral.FF0(Numero) + ';';
    //
    CamposZ := CamposZ + sLineBreak +
    DELETE_FROM + Origem + sLineBreak +
      'WHERE Numero=' + Geral.FF0(Numero) + ';';
    //
    Query.SQL.Text := 'SET sql_mode = ""; ' + CamposZ;
    //
    if Query.Database <> DataBase then
      Query.Database := DataBase;
    //
    UMyMod.ExecutaQuery(Query);
     //
    Result := True;
  end;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  Dta := Geral.FDT(DModG.ObtemAgora(), 105);
  //Numero := TbFormulasNumero.Value;
  //
  // fazer os itens primeiro...
  //Origem := 'FormulasIts';
  //Destino := 'FormulazIts';
  case Orientacao of
    TOrientTabArqBD.stabdAcessivelToMorto:
    begin
      Origem  := 'FormulasIts';
      Destino := 'FormulazIts';
    end;
    TOrientTabArqBD.stabdMortoToAcessivel:
    begin
      Origem  := 'FormulazIts';
      Destino := 'FormulasIts';
    end;
    else
    begin
      Origem  := 'Formula?Its';
      Destino := 'Formula?Its';
    end;
  end;

  ExcluiReceita_EnviaArquivoMorto(Numero, Motivo, Dmod.QrUpd, Dmod.MyDB);
  // ... somente depois o cabecalho
  //TabAtivo := 'Formulas';
  //TabMorto := 'Formulaz';
  case Orientacao of
    TOrientTabArqBD.stabdAcessivelToMorto:
    begin
      Origem  := 'Formulas';
      Destino := 'Formulaz';
    end;
    TOrientTabArqBD.stabdMortoToAcessivel:
    begin
      Origem  := 'Formulaz';
      Destino := 'Formulas';
    end;
    else
    begin
      Origem  := 'Formula?';
      Destino := 'Formula?';
    end;
  end;
  ExcluiReceita_EnviaArquivoMorto(Numero, Motivo, Dmod.QrUpd, Dmod.MyDB);
  //
{
var
  Dta: String;
  Numero: Integer;
  //
  function ExcluiReceita_EnviaArquivoMorto(Numero, Motivo: Integer;
    Query: TmySQLQuery; DataBase: TmySQLDatabase): Boolean;
  var
    CamposZ: String;
  begin
    CamposZ := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabMorto, '');
    CamposZ := Geral.Substitui(CamposZ,
      ', DataDel', ', "' + Dta + '" DataDel');
    CamposZ := Geral.Substitui(CamposZ,
      ', UserDel', ', ' + FormatFloat('0', VAR_USUARIO) + ' UserDel');
    CamposZ := Geral.Substitui(CamposZ,
      ', MotvDel', ', ' + FormatFloat('0', Motivo) + ' MotvDel');
    //
    CamposZ := 'INSERT INTO ' + TabMorto + ' SELECT ' + sLineBreak +
    CamposZ + sLineBreak +
    'FROM ' + TabAtivo + sLineBreak +
    'WHERE Numero=' + Geral.FF0(Numero) + ';';
    //
    CamposZ := CamposZ + sLineBreak +
    DELETE_FROM + TabAtivo + sLineBreak +
      'WHERE Numero=' + Geral.FF0(Numero) + ';';
    //
    Query.SQL.Text := 'SET sql_mode = ""; ' + CamposZ;
    //
    if Query.Database <> DataBase then
      Query.Database := DataBase;
    //
    UMyMod.ExecutaQuery(Query);
     //
    Result := True;
  end;
const
  Motivo = 0;
begin
  case Destino of
    TStatusTabArqBD.stabdAcessivel:
    begin
      Tab
    end;
    TStatusTabArqBD.stabdMorto:
    begin
    end;
    TStatusTabArqBD.stabdAcessivel:
    begin
    end;
  end;
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  Dta := Geral.FDT(DModG.ObtemAgora(), 105);
  Numero := TbFormulasNumero.Value;
  //
  // fazer os itens primeiro...
  TabAtivo := 'FormulasIts';
  TabMorto := 'FormulazIts';
  ExcluiReceita_EnviaArquivoMorto(Numero, Motivo, Dmod.QrUpd, Dmod.MyDB);
  // ... somente depois o cabecalho
  TabAtivo := 'Formulas';
  TabMorto := 'Formulaz';
  ExcluiReceita_EnviaArquivoMorto(Numero, Motivo, Dmod.QrUpd, Dmod.MyDB);
  //
  TbFormulas.Refresh;
  TbFormulas.Last;
}
end;

procedure TUnPQ_PF.ReimprimePesagemNovo(Emit, SetrEmi: Integer; PB: TProgressBar);
const
  TempTable = False;
  ModeloImp = -1;
begin
  //case TTipoReceitas(QrEmitSetrEmi.Value) of
  case TTipoReceitas(SetrEmi) of
    dmktfrmSetrEmi_INDEFIN: //CO_SetrEmi_INDEFIN = 0;
    begin
      Geral.MB_Aviso('Tipo de setor de baixa por receita indefinido!');
    end;
    dmktfrmSetrEmi_MOLHADO: //CO_SetrEmi_MOLHADO = 1;
    begin
      //MostraEdicao(1, stPsq, 0);
      //PQ_PF.ReimprimePesagem(QrEmitCodigo.Value, ProgressBar1);
      ReimprimePesagemSomeWet(Emit, PB);
    end;
    dmktfrmSetrEmi_ACABATO: //CO_SetrEmi_ACABATO = 2;
    begin
      DmModEmit.MostraReceitaDeAcabamento(Emit, ModeloImp, TempTable);
    end;
  end;
end;

{
procedure TUnPQ_PF.ReopenPQOVS(QrPQO: TmySQLQuery; MovimCod, Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQO, Dmod.MyDB, [
  'SELECT lse.Nome NOMESETOR, ',
  'emg.Nome NO_EMITGRU, pqo.* ',
  'FROM pqo pqo ',
  'LEFT JOIN listasetores lse ON lse.Codigo=pqo.Setor ',
  'LEFT JOIN emitgru emg ON emg.Codigo=pqo.EmitGru ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  if Codigo <> 0 then
    QrPQO.Locate('Codigo', Codigo, []);
end;

procedure TUnPQ_PF.ReopenPQOIts(QrPQOIts: TmySQLQuery; Codigo, Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQOIts, Dmod.MyDB, [
  'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  ',
  'pqg.Nome NOMEGRUPO ',
  'FROM pqx pqx ',
  'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo ',
  'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ ',
  'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
  'AND pqx.OrigemCodi=' + Geral.FF0(Codigo),
  '']);
  if Controle <> 0 then
    QrPQOIts.Locate('OrigemCtrl', Controle, []);
end;
}

function TUnPQ_PF.VerificaEquiparacaoEstoque(Mostra: Boolean): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
      'DROP TABLE IF EXISTS _pq_compara_estq_nfs_; ',
      'CREATE TABLE _pq_compara_estq_nfs_ ',
      'SELECT pqx.CliOrig, pqx.Insumo,  ',
      'SUM(pqx.SdoPeso) SdoPeso ',
      'FROM ' + TMeuDB + '.pqx pqx ',
      'WHERE pqx.SdoPeso>0 ',
      'GROUP BY pqx.CliOrig, pqx.Insumo; ',
      'SELECT pcl.Peso, pqx.* ',
      'FROM _pq_compara_estq_nfs_ pqx, ' + TMeuDB + '.pqcli pcl  ',
      'WHERE  pcl.CI=pqx.CliOrig ',
      'AND pcl.PQ=pqx.Insumo  ',
      'AND pcl.Peso <> pqx.SdoPeso ',
      '']);
    //Geral.MB_SQL(nil, Qry);
    Result := Qry.RecordCount = 0;
    if (Result = False) and Mostra then
    begin
      if DBCheck.CriaFm(TFmPQwPQEIts, FmPQwPQEIts, afmoLiberado) then
      begin
        //
        FmPQwPQEIts.ShowModal;
        FmPQwPQEIts.Destroy;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnPQ_PF.AtualizaItensProcesso(Codigo: Integer; DataBase: TmySQLDatabase);
var
  QrySum, QryUpd: TmySQLQuery;
  Fator: Double;
begin
  QrySum := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrySum, DataBase, [
      'SELECT SUM(GramasTi) GramasTi ',
      'FROM tintasits ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    //
    if QrySum.FieldByName('GramasTi').AsFloat = 0 then
      Fator := 0
    else
      Fator := 1000 / QrySum.FieldByName('GramasTi').AsFloat;
    //
    QryUpd.SQL.Clear;
    QryUpd.Database := DataBase;
    QryUpd.SQL.Add('UPDATE tintastin SET GramasTi=:P0 WHERE Codigo=:P1');
    QryUpd.Params[00].AsFloat   := QrySum.FieldByName('GramasTi').AsFloat;
    QryUpd.Params[01].AsInteger := Codigo;
    QryUpd.ExecSQL;
    //
    QryUpd.SQL.Clear;
    QryUpd.Database := DataBase;
    QryUpd.SQL.Add('UPDATE tintasits SET GramasKg=GramasTi*:P0 ');
    QryUpd.SQL.Add('WHERE Codigo=:P1');
    QryUpd.Params[00].AsFloat   := Fator;
    QryUpd.Params[01].AsInteger := Codigo;
    QryUpd.ExecSQL;
  finally
    QrySum.Free;
    QryUpd.Free;
  end;
end;

end.
