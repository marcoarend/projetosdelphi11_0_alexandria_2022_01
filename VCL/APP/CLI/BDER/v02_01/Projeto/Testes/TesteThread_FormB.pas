unit TesteThread_FormB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, UnProjGroup_Consts;

type
  TFmTesteThread_FormB = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    BitBtn1: TBitBtn;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    LaAviso3: TLabel;
    LaAviso4: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Timer1: TTimer;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    FConta: Integer;
    //
    procedure ConsultaProcesso1(Sender: TObject);
    procedure ReabreQry1();
    procedure ReabreQry2();
    procedure ReabreQry3();
    procedure ReabreQry4();
    procedure ReabreQry5();
  public
    { Public declarations }
    FVezes: Integer;
    FLaAviso1, FLaAviso2: TLabel;
    //

  end;

  var
  FmTesteThread_FormB: TFmTesteThread_FormB;

implementation

uses UnMyObjects, Module, DmkDAC_PF, TesteThread;

{$R *.DFM}

procedure TFmTesteThread_FormB.BitBtn1Click(Sender: TObject);
{
  //https://www.andrecelestino.com/delphi-utilizando-o-mecanismo-de-processamento-paralelo/
var
  Tasks: array [0..4] of ITask;
  Tasks: array [0..4] of TTask;

  Inicio: TDateTime;
  Fim: TDateTime;
}
begin
{
  Inicio := Now;

  Tasks[0] := TTask.Create(ReabreQry1());
  Tasks[0].Start;

  Tasks[1] := TTask.Create(ReabreQry2());
  Tasks[1].Start;

  Tasks[2] := TTask.Create(ReabreQry3());
  Tasks[2].Start;

  Tasks[3] := TTask.Create(ReabreQry4());
  Tasks[3].Start;

  Tasks[4] := TTask.Create(ReabreQry5());
  Tasks[4].Start;

  TTask.WaitForAll(Tasks);

  Fim := Now;

  MyObjects.Informa2(LaAviso3, LaAviso4, False, Geral.FDT(FTempo, 108));
(*
  ShowMessage(Format('Consultas realizadas em %s segundos.',
    [FormatDateTime('ss', Fim - Inicio)]));
*)
}
end;

procedure TFmTesteThread_FormB.BtOKClick(Sender: TObject);
var
  I: Integer;
  FTempo: TDateTime;
begin
  FConta := 0;
  FTempo := Now();
  for I := 1 to 100 do
    ReabreQry1();
  FTempo := Now() - FTempo;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Geral.FDT(FTempo, 108));
end;

procedure TFmTesteThread_FormB.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTesteThread_FormB.ConsultaProcesso1(Sender: TObject);
var
  I: Integer;
begin
  for I := 1 to FVezes do
    begin
      ReabreQry1();
    end;
end;

procedure TFmTesteThread_FormB.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTesteThread_FormB.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmTesteThread_FormB.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTesteThread_FormB.ReabreQry1();
var
  Qry: TmySQLQuery;
begin
  FConta := FConta + 1;
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, Self.Name + ': ' + Geral.FF0(FConta));
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI + '',
    '']);
  finally
    Qry.Free;
  end;
  Application.ProcessMessages;
  Sleep(10);
end;

procedure TFmTesteThread_FormB.ReabreQry2;
begin

end;

procedure TFmTesteThread_FormB.ReabreQry3;
begin

end;

procedure TFmTesteThread_FormB.ReabreQry4;
begin

end;

procedure TFmTesteThread_FormB.ReabreQry5;
begin

end;

procedure TFmTesteThread_FormB.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  //
  ConsultaProcesso1(Self);
  FmTesteThread.FTempoFim := Now() - FmTesteThread.FTempoIni;
  MyObjects.Informa2(FLaAviso1, FLaAviso2, False, 'Tempo: ' + Geral.FDT(FmTesteThread.FTempoFim, 108));
  //
  Self.Destroy;
end;

end.
