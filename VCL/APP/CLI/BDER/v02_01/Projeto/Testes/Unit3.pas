unit Unit3;

interface

implementation

  QrAtualiza.Close;
  UnDmkDAC_PF.AbreQuery(QrAtualiza, Dmod.MyDB);
  Progress1.Position := 0;
  Progress1.Max := QrAtualiza.RecordCount;
  PainelDados2.Visible := True;
  PainelDados2.Refresh;
  GBCntrl.Refresh;
  Panel1.Refresh;
  Panel2.Refresh;
  TimerIni := Now();

  //  fazer apenas uma vez para n�o demorar o UnPQx.AtualizaEstoquePQ !!!
  DMod.AtualizaStqMovIts_PQX(nil, nil, nil);
  //
  VAR_PQS_ESTQ_NEG := '';
  while not QrAtualiza.Eof do
  begin
    Progress1.Position := Progress1.Position + 1;
    PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
    PnRegistros.Refresh;
    PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
    PnTempo.Refresh;
    Application.ProcessMessages;
    UnPQx.AtualizaEstoquePQ(QrAtualizaCI.Value, QrAtualizaPQ.Value, QrAtualizaEmpresa.Value, aeVAR, CO_VAZIO, False);
    QrAtualiza.Next;
  end;
  //PainelDados2.Visible := False;
  QrAtualiza.Close;
end.
