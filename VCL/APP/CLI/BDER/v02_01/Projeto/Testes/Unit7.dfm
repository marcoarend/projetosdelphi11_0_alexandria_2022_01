object Form7: TForm7
  Left = 0
  Top = 0
  Caption = 'Form7'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Ribbon1: TRibbon
    Left = 0
    Top = 0
    Width = 635
    Height = 143
    Caption = 'Ribbon1'
    Tabs = <
      item
        Caption = 'RibbonPage1'
        Page = RibbonPage1
      end
      item
        Caption = 'RibbonPage2'
        Page = RibbonPage2
      end
      item
        Caption = 'RibbonPage3'
        Page = RibbonPage3
      end>
    TabIndex = 2
    ExplicitLeft = 136
    ExplicitTop = 72
    ExplicitWidth = 0
    DesignSize = (
      635
      143)
    StyleName = 'Ribbon - Luna'
    object RibbonPage1: TRibbonPage
      Left = 0
      Top = 50
      Width = 634
      Height = 93
      Caption = 'RibbonPage1'
      Index = 0
      object RibbonGroup1: TRibbonGroup
        Left = 4
        Top = 3
        Width = 100
        Height = 86
        Caption = 'RibbonGroup1'
        GroupIndex = 0
      end
    end
    object RibbonPage2: TRibbonPage
      Left = 0
      Top = 50
      Width = 634
      Height = 93
      Caption = 'RibbonPage2'
      Index = 1
    end
    object RibbonPage3: TRibbonPage
      Left = 0
      Top = 50
      Width = 634
      Height = 93
      Caption = 'RibbonPage3'
      Index = 2
    end
  end
end
