unit PQENew_Backup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, ExtCtrls, Buttons, Db, (*DBTables,*) ComCtrls,
  UnInternalConsts2, UnInternalConsts, UMySQLModule, mySQLDbTables, DmkDAC_PF,
  Variants, dmkGeral, dmkEdit, dmkDBLookupComboBox, dmkEditCB, dmkCheckBox,
  dmkEditDateTimePicker, dmkLabel, dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  TTipoNFeEntrada = (tneND=0, tneVP=1, tneRP=2, tneCC=3);
  TTipoNFeRetSrvr = (tnrsND=0, tnrsResumo=1, tnrsCompleta=2);
  //
  TFmPQENew = class(TForm)
    QrFornecedores: TmySQLQuery;
    DsFornecedores: TDataSource;
    QrTransportadores: TmySQLQuery;
    DsTransportadores: TDataSource;
    QrItens: TmySQLQuery;
    QrItensCodigo: TIntegerField;
    QrItensControle: TIntegerField;
    QrItensConta: TIntegerField;
    QrItensInsumo: TIntegerField;
    QrItensVolumes: TIntegerField;
    QrItensPesoVB: TFloatField;
    QrItensPesoVL: TFloatField;
    QrItensValorItem: TFloatField;
    QrItensIPI: TFloatField;
    QrItensTotalCusto: TFloatField;
    QrItensTotalPeso: TFloatField;
    QrItensPreco: TFloatField;
    QrItensICMS: TFloatField;
    QrItensEntrada: TFloatField;
    QrInsIts: TmySQLQuery;
    QrItensPrazo: TWideStringField;
    QrItensCFin: TFloatField;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOME: TWideStringField;
    QrTransportadoresCodigo: TIntegerField;
    QrTransportadoresNOME: TWideStringField;
    QrCI: TmySQLQuery;
    DsCI: TDataSource;
    PainelDados: TPanel;
    Label17: TLabel;
    Label11: TLabel;
    Label6: TLabel;
    Label19: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    LaCI: TLabel;
    QrCICodigo: TIntegerField;
    QrCINOME: TWideStringField;
    CkTipoNF: TdmkCheckBox;
    PnNFs: TPanel;
    EdrefNFe: TdmkEdit;
    LarefNFe: TLabel;
    PnInativo: TPanel;
    TPEntrada: TdmkEditDateTimePicker;
    TPEmissao: TdmkEditDateTimePicker;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    CBCI: TdmkDBLookupComboBox;
    EdCI: TdmkEditCB;
    CBTransportador: TdmkDBLookupComboBox;
    EdTransportador: TdmkEditCB;
    EdkgLiquido: TdmkEdit;
    EdValorNF: TdmkEdit;
    Label2: TLabel;
    EdRICMSF: TdmkEdit;
    Label12: TLabel;
    EdJuros: TdmkEdit;
    EdRICMS: TdmkEdit;
    EdICMS: TdmkEdit;
    Label1: TLabel;
    Label13: TLabel;
    EdkgBruto: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel1: TPanel;
    LaEmpresa: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    EdPedido: TdmkEdit;
    LaSitPedido: TLabel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Label8: TLabel;
    Label14: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    EdFrete: TdmkEdit;
    EdFreteRpICMS: TdmkEdit;
    EdFreteRvICMS: TdmkEdit;
    EdFreteRpPIS: TdmkEdit;
    EdFreteRvPIS: TdmkEdit;
    EdFreteRpCOFINS: TdmkEdit;
    EdFreteRvCOFINS: TdmkEdit;
    EdCte_Id: TdmkEdit;
    Label27: TLabel;
    EdCTe_mod: TdmkEdit;
    Label21: TLabel;
    Label23: TLabel;
    EdCTe_serie: TdmkEdit;
    Label25: TLabel;
    EdConhecim: TdmkEdit;
    LaModNF: TLabel;
    EdModNF: TdmkEdit;
    EdSerie: TdmkEdit;
    LaSerie: TLabel;
    LaNF: TLabel;
    EdNF: TdmkEdit;
    EdNFe_CC: TdmkEdit;
    LaNFe_CC: TLabel;
    LamodCC: TLabel;
    EdmodCC: TdmkEdit;
    EdSerCC: TdmkEdit;
    LaSerCC: TLabel;
    EdNF_CC: TdmkEdit;
    LaNF_CC: TLabel;
    LaLancamento: TLabel;
    EdCodigo: TdmkEdit;
    SbNF_VP: TSpeedButton;
    Qr00_NFeCabA: TMySQLQuery;
    Qr00_NFeCabAFatID: TIntegerField;
    Qr00_NFeCabAFatNum: TIntegerField;
    Qr00_NFeCabAEmpresa: TIntegerField;
    Qr00_NFeCabAIDCtrl: TIntegerField;
    Qr00_NFeCabAAtrelaFatID: TIntegerField;
    Qr00_NFeCabAAtrelaStaLnk: TSmallintField;
    Qr00_NFeCabAide_mod: TSmallintField;
    Qr00_NFeCabAide_serie: TIntegerField;
    Qr00_NFeCabAide_nNF: TIntegerField;
    QrFornecedoresTipo: TSmallintField;
    QrFornecedoresCNPJ: TWideStringField;
    QrFornecedoresCPF: TWideStringField;
    QrTransportadoresTipo: TSmallintField;
    QrTransportadoresCNPJ: TWideStringField;
    QrTransportadoresCPF: TWideStringField;
    QrCITipo: TSmallintField;
    QrCICNPJ: TWideStringField;
    QrCICPF: TWideStringField;
    LaNFe_RP: TLabel;
    EdNFe_RP: TdmkEdit;
    EdmodRP: TdmkEdit;
    LamodRP: TLabel;
    LaSerRP: TLabel;
    EdSerRP: TdmkEdit;
    EdNF_RP: TdmkEdit;
    LaNF_RP: TLabel;
    Label4: TLabel;
    EdNFVP_StaLnk: TdmkEdit;
    EdNFVP_FatID: TdmkEdit;
    EdNFVP_FatNum: TdmkEdit;
    EdNFCC_StaLnk: TdmkEdit;
    EdNFCC_FatID: TdmkEdit;
    EdNFCC_FatNum: TdmkEdit;
    EdNFRP_StaLnk: TdmkEdit;
    EdNFRP_FatID: TdmkEdit;
    EdNFRP_FatNum: TdmkEdit;
    Label9: TLabel;
    Label15: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Qr00_NFeCabAId: TWideStringField;
    QrPediVdaIts: TMySQLQuery;
    QrPediVdaItsCodigo: TIntegerField;
    QrPediVdaItsControle: TIntegerField;
    QrPediVdaItsGraGruX: TIntegerField;
    QrPediVdaItsPrecoO: TFloatField;
    QrPediVdaItsPrecoR: TFloatField;
    QrPediVdaItsQuantP: TFloatField;
    QrPediVdaItsQuantC: TFloatField;
    QrPediVdaItsQuantV: TFloatField;
    QrPediVdaItsValBru: TFloatField;
    QrPediVdaItsDescoP: TFloatField;
    QrPediVdaItsDescoV: TFloatField;
    QrPediVdaItsValLiq: TFloatField;
    QrPediVdaItsPrecoF: TFloatField;
    QrPediVdaItsMedidaC: TFloatField;
    QrPediVdaItsMedidaL: TFloatField;
    QrPediVdaItsMedidaA: TFloatField;
    QrPediVdaItsMedidaE: TFloatField;
    QrPediVdaItsPercCustom: TFloatField;
    QrPediVdaItsCustomizad: TSmallintField;
    QrPediVdaItsInfAdCuztm: TIntegerField;
    QrPediVdaItsOrdem: TIntegerField;
    QrPediVdaItsReferencia: TWideStringField;
    QrPediVdaItsMulti: TIntegerField;
    QrPediVdaItsvProd: TFloatField;
    QrPediVdaItsvFrete: TFloatField;
    QrPediVdaItsvSeg: TFloatField;
    QrPediVdaItsvOutro: TFloatField;
    QrPediVdaItsvDesc: TFloatField;
    QrPediVdaItsvBC: TFloatField;
    QrGraGru1: TMySQLQuery;
    QrGraGru1ICMSRec_pAliq: TFloatField;
    QrGraGru1PISRec_pAliq: TFloatField;
    QrGraGru1COFINSRec_pAliq: TFloatField;
    EdIND_FRT: TdmkEdit;
    Label108: TLabel;
    EdIND_FRT_TXT: TdmkEdit;
    Label16: TLabel;
    EdIND_PGTO: TdmkEdit;
    EdIND_PGTO_TXT: TdmkEdit;
    EdICMSTot_vProd: TdmkEdit;
    Label18: TLabel;
    EdICMSTot_vFrete: TdmkEdit;
    Label20: TLabel;
    EdICMSTot_vSeg: TdmkEdit;
    Label22: TLabel;
    EdICMSTot_vOutro: TdmkEdit;
    Label28: TLabel;
    EdICMSTot_vDesc: TdmkEdit;
    Label29: TLabel;
    Label30: TLabel;
    EdIPI: TdmkEdit;
    Qr00_NFeCabAAtrelaFatNum: TIntegerField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdkgLiquidoExit(Sender: TObject);
    procedure EdkgBrutoExit(Sender: TObject);
    procedure EdFreteExit(Sender: TObject);
    procedure EdConhecimExit(Sender: TObject);
    procedure EdRICMSExit(Sender: TObject);
    procedure EdValorNFExit(Sender: TObject);
    procedure EdNFExit(Sender: TObject);
    procedure EdRICMSFExit(Sender: TObject);
    procedure EdJurosExit(Sender: TObject);
    procedure EdICMSExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure CkTipoNFClick(Sender: TObject);
    procedure EdrefNFeExit(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdFreteChange(Sender: TObject);
    procedure EdFreteRpICMSChange(Sender: TObject);
    procedure EdFreteRpPISChange(Sender: TObject);
    procedure EdFreteRpCOFINSChange(Sender: TObject);
    procedure EdrefNFeRedefinido(Sender: TObject);
    procedure EdNFe_RPRedefinido(Sender: TObject);
    procedure EdNFe_CCRedefinido(Sender: TObject);
    procedure EdNFe_RPExit(Sender: TObject);
    procedure EdNFe_CCExit(Sender: TObject);
    procedure EdIND_FRTChange(Sender: TObject);
    procedure EdIND_FRTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdIND_PGTOChange(Sender: TObject);
    procedure EdIND_PGTOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
      // Meu
    procedure CalculaTotalNF(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SbNF_VPClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure EdCIChange(Sender: TObject);
  private
    { Private declarations }
    F_IND_PGTO_EFD, F_IND_FRT_EFD: MyArrayLista;
//    procedure ControlChange(Sender : TObject);
    procedure CalculaRetornoImpostosFrete(Qual: TRetornImpost);
    procedure DefineDFe_Atrela(const TipoNFeEntrada: TTipoNFeEntrada);
              //var TipoNFeRetSrvr: TTipoNFeRetSrvr; var chNFe: String);
    function  InserePQEItsDePediVda(Codigo: Integer): Boolean;
    procedure ReopenPQEItsDePediVda(Codigo: Integer);
    procedure VerificaChaveNFeDeEntrada(Empresa, CliInt: Integer; ChaveEmpresa,
              ChaveCliInt: String);

  public
    { Public declarations }
    FDesiste: Boolean;
    FPediVda, FSqLinked: Integer;
  end;

var
  FmPQENew: TFmPQENew;

implementation

uses UnMyObjects, Principal, PQE, Module, PQx, ModuleNFe_0000, NFe_PF,
  ModuleGeral, NFeXMLGerencia, ModProd, ModPediVda, UnSPED_PF;

{$R *.DFM}

const
  FEFDInnNFSMainFatID = VAR_FATID_0010;
  CO_MovimCod_ZERO    = 0;


procedure TFmPQENew.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  //
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PQE', Codigo)
  else
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PQE', 'Codigo');
  //
  FmPQE.ImgTipo.SQLType := stLok;
  FmPQE.GBTrava.Visible := False;
  FmPQE.GBCntrl.Visible := True;
  //
  FDesiste := True;
  //
  Close;
end;

procedure TFmPQENew.BtConfirmaClick(Sender: TObject);
var
  PesoL, PesoB, ValorNF, ICMS, RICMS, RICMSF, ValorItem, Frete, Juros: Double;
  Codigo, IQ, CI, Pedido, Conta, Controle, Transportadora, NF, modNF, Serie,
  Conhecimento, TipoNF, Empresa: Integer;
  //
  Data, DataE, refNFe: String;
  NF_RP, NF_CC: Integer;
{
    , IQ, CI,
    Transportadora, NF, Frete,
    PesoB, PesoL, ValorNF,
    RICMS, RICMSF, Conhecimento,
    Pedido, DataE, Juros,
    ICMS, Cancelado, TipoNF,
    refNFe, modNF, Serie], [
    Codigo], True) then
}
  // 2022-03-??
  FreteRpICMS, FreteRpPIS, FreteRpCOFINS,
  FreteRvICMS, FreteRvPIS, FreteRvCOFINS: Double;
  // 2022-04=02
  NFe_RP, NFe_CC, Cte_Id: String;
  modRP, modCC, SerRP, SerCC, CTe_mod, CTe_serie: Integer;
  PrecisaNF: Boolean;
  NFeCabA_FatID, NFeCabA_FatNum, NFeCabA_StaLnk: Integer;
  NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
  NFRP_FatID, NFRP_FatNum, NFRP_StaLnk,
  NFCC_FatID, NFCC_FatNum, NFCC_StaLnk,
  IND_PGTO, IND_FRT: Integer;
  //
  ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vOutro: Double;
  //
  IsLinked, SqLinked, MovFatID, MovFatNum, MovimCod, Terceiro, Motorista: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  COD_MOD, COD_SIT, SER, NUM_DOC, NFeStatus, NFe_FatID, NFe_FatNum, NFe_StaLnk,
  VSVmcWrn, VSVmcSta, EfdInnNFsCab: Integer;
  Placa, CHV_NFE, DT_DOC, DT_E_S, VSVmcObs, VSVmcSeq: String;
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST, IPI: Double;
  UsaEntradaFiscal: Boolean;
  // ...
  function InsAltEfdInnNFsCab(): Boolean;
  var
    EfdSQLType: TSQLType;
    LnkTabName, LnkFldName: String;
  begin
        MovFatID  := FEFDInnNFSMainFatID;
        MovFatNum := Codigo;
        MovimCod  := CO_MovimCod_ZERO;
        //
        IsLinked  := 1; // True
        if ImgTipo.SQLType = stIns then
        begin
          EfdSQLType := stIns;
          SqLinked  := 0; // Buscar l� na inclus�o!
          EfdInnNFsCab   := 0; // Buscar!
        end else
        begin
          DmNFe_0000.ReopenEfdLinkCab(MovFatID, MovFatNum, MovimCod, Empresa);
          if DmNFe_0000.QrEfdLinkCab.RecordCount > 0 then
          //if FSqLinked > 0 then
          begin
            EfdSQLType := stUpd;
            SqLinked     := DmNFe_0000.QrEfdLinkCabSqLinked.Value;
            EfdInnNFsCab := DmNFe_0000.QrEfdLinkCabControle.Value;
          end else
          begin
            EfdSQLType := stIns;
            SqLinked  := 0; // Buscar l� na inclus�o!
            EfdInnNFsCab   := 0; // Buscar!
          end;
        end;
        if EMpresa = CI then
        begin
          Terceiro := IQ;
          COD_MOD  := modNF;
          COD_SIT  := 00; // Documento regular
          SER      := Serie;
          NUM_DOC  := NF;
          CHV_NFE  := refNFe;
          NFe_FatID   := NFVP_FatID;
          NFe_FatNum  := NFVP_FatNum;
          NFe_StaLnk  := NFVP_StaLnk;
        end else
        begin
          Terceiro := CI;
          COD_MOD  := modCC;
          COD_SIT  := 00; // Documento regular
          SER      := SerCC;
          NUM_DOC  := NF_CC;
          CHV_NFE  := NFe_CC;
          NFe_FatID   := NFCC_FatID;
          NFe_FatNum  := NFCC_FatNum;
          NFe_StaLnk  := NFCC_StaLnk;
        end;
        NFeStatus := 100;
        //
        Pecas := 0;
        PesoKg := 0.000;
        AreaM2 := 0.00;
        AreaP2 := 0.00;
        ValorT := 0.00;
        Motorista := 0;
        Placa := '';
        DT_DOC := Data;
        DT_E_S := DataE;
        VL_DOC := ValorNF;
        VL_DESC := ICMSTot_vDesc;
        VL_ABAT_NT := 0.00;
        VL_MERC := ICMSTot_vProd;
        VL_FRT := ICMSTot_vFrete;
        VL_SEG := ICMSTot_vSeg;
        VL_OUT_DA := ICMSTot_vOutro;
        //
        IPI            := EdIPI.ValueVariant;
        //
        VL_BC_ICMS     := 0.00;
        VL_ICMS        := 0.00;
        VL_BC_ICMS_ST  := 0.00;
        VL_ICMS_ST     := 0.00;
        VL_IPI         := IPI;
        VL_PIS         := 0.00;
        VL_COFINS      := 0.00;
        VL_PIS_ST      := 0.00;
        VL_COFINS_ST   := 0.00;
        //
        VSVmcWrn       := 0;
        VSVmcObs       := '';
        VSVmcSeq       := '';
        VSVmcSta       := 0;
        //
        LnkTabName     := 'pqe';
        LnkFldName     := 'Codigo';
        //
        //
        Result :=  SPED_PF.InsAltEfdInnNFsCab(EfdSQLType, MovFatID,
        MovFatNum, MovimCod, Empresa, (*Controle*)EfdInnNFsCab,
        IsLinked, SqLinked, Terceiro, CI, Pecas,
        PesoKg, AreaM2, AreaP2, ValorT, Motorista, Placa, COD_MOD, COD_SIT,
        SER, NUM_DOC, CHV_NFE, NFeStatus, DT_DOC,DT_E_S, VL_DOC,
        Geral.FF0(IND_PGTO),
        VL_DESC, VL_ABAT_NT, VL_MERC,
        Geral.FF0(IND_FRT),
        VL_FRT, VL_SEG, VL_OUT_DA,
        VL_BC_ICMS, VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS,
        VL_COFINS, VL_PIS_ST, VL_COFINS_ST, NFe_FatID, NFe_FatNum, NFe_StaLnk,
        VSVmcWrn, VSVmcObs, VSVmcSeq, VSVmcSta, LnkTabName, LnkFldName);
        //
    if Result then
    begin
      //if (Empresa = CI) and (NFVP_FatID <> 0) and (NFVP_FatNum <> 0) (*and (NFVP_FatNum <> 0)*) then
      begin
        Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
        'AtrelaFatID', 'AtrelaFatNum', 'AtrelaStaLnk'], [
        'FatID', 'FatNum', 'Empresa'], [
        MovFatID, MovFatNum, NFe_StaLnk], [
        NFe_FatID, NFe_FatNum, Empresa], False);
      end;
    end;

  end;
  //
  function InserePQE(): Boolean;
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpdW, ImgTipo.SQLType, 'pqe', False, [
        'Data', 'IQ', 'CI',
        'Transportadora', 'NF', 'Frete',
        'PesoB', 'PesoL', 'ValorNF',
        'RICMS', 'RICMSF', 'Conhecimento',
        'Pedido', 'DataE', 'Juros',
        'ICMS', 'TipoNF',
        'refNFe', 'modNF', 'Serie',
        'NF_RP', 'NF_CC', 'Empresa',
        'FreteRpICMS', 'FreteRpPIS', 'FreteRpCOFINS',
        'FreteRvICMS', 'FreteRvPIS', 'FreteRvCOFINS',
        'modRP', 'modCC', 'SerRP', 'SerCC',
        'NFe_RP', 'NFe_CC', 'Cte_Id',
        'CTe_mod', 'CTe_serie',
        'NFVP_FatID', 'NFVP_FatNum', 'NFVP_StaLnk',
        'NFRP_FatID', 'NFRP_FatNum', 'NFRP_StaLnk',
        'NFCC_FatID', 'NFCC_FatNum', 'NFCC_StaLnk',
        'IND_PGTO', 'IND_FRT',
        'ICMSTot_vProd', 'ICMSTot_vFrete', 'ICMSTot_vSeg',
        'ICMSTot_vDesc', 'ICMSTot_vOutro', 'IPI'], [
        'Codigo'], [
        Data, IQ, CI,
        Transportadora, NF, Frete,
        PesoB, PesoL, ValorNF,
        RICMS, RICMSF, Conhecimento,
        Pedido, DataE, Juros,
        ICMS, TipoNF,
        refNFe, modNF, Serie,
        NF_RP, NF_CC, Empresa,
        FreteRpICMS, FreteRpPIS, FreteRpCOFINS,
        FreteRvICMS, FreteRvPIS, FreteRvCOFINS,
        modRP, modCC, SerRP, SerCC,
        NFe_RP, NFe_CC, Cte_Id,
        CTe_mod, CTe_serie,
        NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
        NFRP_FatID, NFRP_FatNum, NFRP_StaLnk,
        NFCC_FatID, NFCC_FatNum, NFCC_StaLnk,
        IND_PGTO, IND_FRT,
        ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg,
        ICMSTot_vDesc, ICMSTot_vOutro, IPI], [
        Codigo], True);

  end;
begin
  Controle := 0;
  //
  if UnPQx.ImpedePeloBalanco(TPEntrada.Date) then Exit;
  //
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  ICMS   := Geral.DMV(EdICMS.Text);
  RICMS  := Geral.DMV(EdRICMS.Text);
  RICMSF := Geral.DMV(EdRICMSF.Text);
  TipoNF := Geral.BoolToInt(CkTipoNF.Checked);
  //
  {
  if ICMS > 99 then
  begin
    ShowMessage('O ICMS da N.F. � inv�lido.');
    EdICMS.SetFocus;
    Exit;
  end;
  if RICMS > 99 then
  begin
    ShowMessage('O retorno de ICMS da N.F. � inv�lido.');
    EdRICMS.SetFocus;
    Exit;
  end;
  if RICMSF > 99 then
  begin
    ShowMessage('O retorno de ICMS do frete � inv�lido.');
    EdRICMSF.SetFocus;
    Exit;
  end;
  }
  //
  if CBFornecedor.KeyValue = Null then
    CBFornecedor.KeyValue := 0;
  //
  if CBTransportador.KeyValue = Null then
    CBTransportador.KeyValue := 0;
  //
  Data           := Geral.FDT(TPEntrada.Date, 1);
  IQ             := EdFornecedor.ValueVariant;
  CI             := EdCI.ValueVariant;
  Transportadora := EdTransportador.ValueVariant;
  Frete          := EdFrete.ValueVariant;
  PesoB          := EdkgBruto.ValueVariant;
  PesoL          := EdkgLiquido.ValueVariant;
  ValorNF        := EdValorNF.ValueVariant;
  Conhecimento   := EdConhecim.ValueVariant;
  DataE          := Geral.FDT(TPEmissao.Date, 1);
  Juros          := EdJuros.ValueVariant;
  refNFe         := EdrefNFe.Text;
  //
  FreteRpICMS    := EdFreteRpICMS.ValueVariant;
  FreteRpPIS     := EdFreteRpPIS.ValueVariant;
  FreteRpCOFINS  := EdFreteRpCOFINS.ValueVariant;
  FreteRvICMS    := EdFreteRvICMS.ValueVariant;
  FreteRvPIS     := EdFreteRvPIS.ValueVariant;
  FreteRvCOFINS  := EdFreteRvCOFINS.ValueVariant;
  //
  if MyObjects.FIC(CI = 0, EdCI, 'Informe o cliente interno') then Exit;
  if MyObjects.FIC(CBFornecedor.KeyValue = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  IND_FRT        := EdIND_FRT.ValueVariant;
  if IND_FRT <> 9 then// sem frete
    if MyObjects.FIC(Transportadora = 0, EdTransportador, 'Defina um transportador!') then Exit;
  //
  UsaEntradaFiscal := DModG.QrCtrlGeralUsarEntraFiscal.Value = 1;
  //
  // NF de venda
  modNF := EdmodNF.ValueVariant;
  Serie := EdSerie.ValueVariant;
  NF    := EdNF.ValueVariant;
  PrecisaNF := (CI = Empresa) and (
    (refNFe = '') or ( (NF =  0) and (modNF = 0)));
  if MyObjects.FIC(UsaEntradaFiscal and PrecisaNF, EdrefNFe, 'Informe a chave da NF VP!') then Exit;
  //

  // NF de Remessa para Produ��o (Industrializa��o)
  NFe_RP    := EdNFe_RP.ValueVariant;
  modRP     := EdmodRP.ValueVariant;
  SerRP     := EdSerRP.ValueVariant;
  NF_RP     := EdNF_RP.ValueVariant;
  PrecisaNF := (CI <> Empresa) and (
    (NFe_RP = '') or ( (NF_RP = 0) and (modRP = 0)));
  if MyObjects.FIC(UsaEntradaFiscal and PrecisaNF, EdNFe_RP, 'Informe a chave da NF RP!') then; // Exit;
  //

  // NF de Cobertura do Cliente
  NFe_CC    := EdNFe_CC.ValueVariant;
  modCC     := EdmodCC.ValueVariant;
  SerCC     := EdSerCC.ValueVariant;
  NF_CC     := EdNF_CC.ValueVariant;
  PrecisaNF := (CI <> Empresa) and (
    (NFe_CC = '') or ( (NF_CC = 0) and (modCC = 0)));
  if MyObjects.FIC(UsaEntradaFiscal and PrecisaNF, EdNFe_CC, 'Informe a chave da NF CC!') then Exit;
  //

  // CTe - Conhecimento de Frete
  Cte_Id    := EdCte_Id.ValueVariant;
  CTe_mod   := EdCTe_mod.ValueVariant;
  CTe_serie := EdCTe_serie.ValueVariant;

  //
  NFVP_FatID     := EdNFVP_FatID.ValueVariant;
  NFVP_FatNum    := EdNFVP_FatNum.ValueVariant;
  NFVP_StaLnk    := EdNFVP_StaLnk.ValueVariant;
  NFRP_FatID     := EdNFRP_FatID.ValueVariant;
  NFRP_FatNum    := EdNFRP_FatNum.ValueVariant;
  NFRP_StaLnk    := EdNFRP_StaLnk.ValueVariant;
  NFCC_FatID     := EdNFCC_FatID.ValueVariant;
  NFCC_FatNum    := EdNFCC_FatNum.ValueVariant;
  NFCC_StaLnk    := EdNFCC_StaLnk.ValueVariant;

  IND_PGTO       := EdIND_PGTO.ValueVariant;
  IND_FRT        := EdIND_FRT.ValueVariant;

  ICMSTot_vProd  := EdICMSTot_vProd.ValueVariant;
  ICMSTot_vFrete := EdICMSTot_vFrete.ValueVariant;
  ICMSTot_vSeg   := EdICMSTot_vSeg.ValueVariant;
  ICMSTot_vDesc  := EdICMSTot_vDesc.ValueVariant;
  ICMSTot_vOutro := EdICMSTot_vOutro.ValueVariant;
  IPI            := EdIPI.ValueVariant;

  Codigo := EdCodigo.ValueVariant;
  //
  VerificaChaveNFeDeEntrada(Empresa, CI, refNFe, NFe_CC);
  //
  if ImgTipo.SQLType = stIns then
  begin
    Pedido := Geral.IMV(EdPedido.Text);
    // Se for pelo PediVda:
    if FPediVda = Pedido then
    begin
      if InserePQE() then
      begin
////////////////////////////////////////////////////////////////////////////////
        if InsAltEfdInnNFsCab() then
////////////////////////////////////////////////////////////////////////////////
        begin
          ReopenPQEItsDePediVda(FPediVda);
          // Parei Aqui
          QrPediVdaIts.First;
          try
            while not QrPediVdaIts.Eof do
            begin
              if InserePQEItsDePediVda(Codigo) then
                QrPediVdaIts.Next
              else
              begin
                FmPQE.ExcluiTodaEntradaPediVdaEmProgresso(Codigo);
                Exit;
              end
            end;
          except
            on E: Exception do
            begin
              FmPQE.ExcluiTodaEntradaPediVdaEmProgresso(Codigo);
              Geral.MB_Erro(E.Message);
              FmPQE.LocCod(Codigo, Codigo);
              Exit;
            end;
          end;
        end;
      end;
    end else
    begin
      if InserePQE() then
      begin
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('INSERT INTO PQEIts SET Insumo=:P0, Volumes=:P1, ');
        Dmod.QrUpdM.SQL.Add('PesoVB=:P2, PesoVL=:P3, ValorItem=:P4, IPI=:P5, ');
        Dmod.QrUpdM.SQL.Add('RIPI=:P6, TotalCusto=:P7, TotalPeso=:P8, ');
        Dmod.QrUpdM.SQL.Add('CFin=:P9, Codigo=:P10, Conta=:11, Controle=:P12');
        if Pedido > 0 then
        begin
          QrItens.Close;
          QrItens.Params[0].AsInteger := Pedido;
          UnDmkDAC_PF.AbreQuery(QrItens, Dmod.MyDB);
          Conta := 0;
          while not QrItens.Eof do
          begin
            ValorItem := QrItensValorItem.Value * QrItensVolumes.Value *
                          QrItensPesoVL.Value / (1 + (QrItensIPI.Value / 100));
            Conta := Conta + 1;
            Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
              'PQEIts','PQEIts','Controle');
            //
            Dmod.QrUpdM.Params[00].AsInteger := QrItensInsumo.Value;
            Dmod.QrUpdM.Params[01].AsInteger := QrItensVolumes.Value;
            Dmod.QrUpdM.Params[02].AsFloat   := QrItensPesoVB.Value;
            Dmod.QrUpdM.Params[03].AsFloat   := QrItensPesoVL.Value;
            Dmod.QrUpdM.Params[04].AsFloat   := ValorItem;
            Dmod.QrUpdM.Params[05].AsFloat   := QrItensIPI.Value;
            Dmod.QrUpdM.Params[06].AsFloat   := 0;
            Dmod.QrUpdM.Params[07].AsFloat   := 0;
            Dmod.QrUpdM.Params[08].AsFloat   := QrItensVolumes.Value*QrItensPesoVL.Value;
            Dmod.QrUpdM.Params[09].AsFloat   := QrItensCFin.Value;
            Dmod.QrUpdM.Params[10].AsInteger := Geral.IMV(EdCodigo.Text);
            Dmod.QrUpdM.Params[11].AsInteger := Conta;
            Dmod.QrUpdM.Params[12].AsInteger := Controle;
            Dmod.QrUpdM.ExecSQL;
            QrItens.Next;
          end;
          FmPQE.FControle := Controle;
          QrItens.Close;
          FmPQE.SubQuery1Reopen;
          Dmod.QrUpdW.DataBase := Dmod.MyDB;
          Dmod.QrUpdW.Close;
          Dmod.QrUpdW.SQL.Clear;
          Dmod.QrUpdW.SQL.Add('UPDATE PQPed SET Sit=:P0');
          Dmod.QrUpdW.SQL.Add('WHERE Codigo=:P1');
          Dmod.QrUpdW.Params[0].AsInteger := Geral.IMV(LaSitPedido.Caption);
          Dmod.QrUpdW.Params[1].AsInteger := Pedido;
          Dmod.QrUpdW.ExecSQL;
          // Parei Aqui !!!  Falta dar baixa (entrada!) do produto no pedido
        end;
      end;
    end;
  end
  else
  begin
      if UMyMod.SQLInsUpd(Dmod.QrUpdW, stUpd, 'pqe', False, [
        'Data', 'IQ', 'CI',
        'Transportadora', 'NF', 'Frete',
        'PesoB', 'PesoL', 'ValorNF',
        'RICMS', 'RICMSF', 'Conhecimento',
        (*'Pedido',*) 'DataE', 'Juros',
        'ICMS', 'TipoNF',
        'refNFe', 'modNF', 'Serie',
        'NF_RP', 'NF_CC',
        'FreteRpICMS', 'FreteRpPIS', 'FreteRpCOFINS',
        'FreteRvICMS', 'FreteRvPIS', 'FreteRvCOFINS',
        'modRP', 'modCC', 'SerRP', 'SerCC',
        'NFe_RP', 'NFe_CC', 'Cte_Id',
        'CTe_mod', 'CTe_serie',
        'NFVP_FatID', 'NFVP_FatNum', 'NFVP_StaLnk',
        'NFRP_FatID', 'NFRP_FatNum', 'NFRP_StaLnk',
        'NFCC_FatID', 'NFCC_FatNum', 'NFCC_StaLnk',
        'IND_PGTO', 'IND_FRT',
        'ICMSTot_vProd', 'ICMSTot_vFrete', 'ICMSTot_vSeg',
        'ICMSTot_vDesc', 'ICMSTot_vOutro', 'IPI'], [
        'Codigo'], [
        Data, IQ, CI,
        Transportadora, NF, Frete,
        PesoB, PesoL, ValorNF,
        RICMS, RICMSF, Conhecimento,
        (*Pedido,*) DataE, Juros,
        ICMS, TipoNF,
        refNFe, modNF, Serie,
        NF_RP, NF_CC,
        FreteRpICMS, FreteRpPIS, FreteRpCOFINS,
        FreteRvICMS, FreteRvPIS, FreteRvCOFINS,
        modRP, modCC, SerRP, SerCC,
        NFe_RP, NFe_CC, Cte_Id,
        CTe_mod, CTe_serie,
        NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
        NFRP_FatID, NFRP_FatNum, NFRP_StaLnk,
        NFCC_FatID, NFCC_FatNum, NFCC_StaLnk,
        IND_PGTO, IND_FRT,
        ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg,
        ICMSTot_vDesc, ICMSTot_vOutro, IPI], [
        Codigo], True) then
////////////////////////////////////////////////////////////////////////////////
        if InsAltEfdInnNFsCab() then
////////////////////////////////////////////////////////////////////////////////
        begin

        end;
  end;
  FmPQE.ImgTipo.SQLType := ImgTipo.SQLType;
  Close;
end;

procedure TFmPQENew.FormCreate(Sender: TObject);
var
  MinData: TDate;
begin
  ImgTipo.SQLType := stLok;
  PnNFs.Align     := alClient;
  FDesiste        := False;
  FSqLinked       := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransportadores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  F_IND_PGTO_EFD := UnNFe_PF.ListaIND_PAG_EFD();
  F_IND_FRT_EFD  := UnNFe_PF.ListaIND_FRT_EFD();
  //
  if Dmod.QrControleCanAltBalA.Value = 0 then
  begin
    MinData        := StrToDate(dmkPF.PrimeiroDiaAposPeriodo(UnPQx.VerificaBalanco, dtSystem3));
    TPEntrada.Date := Date;
    //
    if TPEntrada.Date < MinData then
      TPENtrada.Date := MinData;
    //
    TPEntrada.MinDate := MinData;
  end;
end;

procedure TFmPQENew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if ImgTipo.SQLType = stUpd then
    BtConfirma.Setfocus
  else
    TPEntrada.SetFocus;
end;

procedure TFmPQENew.CalculaRetornoImpostosFrete(Qual: TRetornImpost);
const
  sProcName = 'FmPQENew.CalculaRetornoImpostosFrete()';
var
  Frete, pICMS, pPIS, pCofins, vICMS, vPIS, vCofins: Double;
begin
  Frete   := EdFrete.ValueVariant;
  pICMS   := EdFreteRpICMS.ValueVariant;
  pPIS    := EdFreteRpPIS.ValueVariant;
  pCofins := EdFreteRpCofins.ValueVariant;
  vICMS   := Geral.RoundC(Frete * pICMS / 100, 2);
  vPIS    := Geral.RoundC(Frete * pPIS / 100, 2);
  vCofins := Geral.RoundC(Frete * pCOFINS / 100, 2);
  case Qual of
    //retimpostIndef=1,
    //retimpostNenhum: Exit;
    retimpostTodos:
    begin
      EdFreteRvICMS.ValueVariant   := vICMS;
      EdFreteRvPIS.ValueVariant    := vPIS;
      EdFreteRvCOFINS.ValueVariant := vCOFINS;
    end;
    retimpostICMS: EdFreteRvICMS.ValueVariant     := vICMS;
    retimpostPIS: EdFreteRvPIS.ValueVariant       := vPIS;
    retimpostCOFINS: EdFreteRvCOFINS.ValueVariant := vCOFINS;
    //retimpostIPI=7);
    else Geral.MB_Erro('TRetornImpost n�o implementado em ' + sProcName);
  end;
end;

procedure TFmPQENew.CalculaTotalNF(Sender: TObject);
var
  ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vOutro,
  IPI, ValorNF: Double;
begin
  ICMSTot_vProd  := EdICMSTot_vProd.ValueVariant;
  ICMSTot_vFrete := EdICMSTot_vFrete.ValueVariant;
  ICMSTot_vSeg   := EdICMSTot_vSeg.ValueVariant;
  ICMSTot_vDesc  := EdICMSTot_vDesc.ValueVariant;
  ICMSTot_vOutro := EdICMSTot_vOutro.ValueVariant;
  IPI            := EdIPI.ValueVariant;
  //
  ValorNF := ICMSTot_vProd + ICMSTot_vFrete + ICMSTot_vSeg - ICMSTot_vDesc + ICMSTot_vOutro + IPI;
  EdValorNF.ValueVariant := ValorNF;
end;

procedure TFmPQENew.CkTipoNFClick(Sender: TObject);
var
  EhNFe: Boolean;
begin
  EhNFe := CkTipoNF.Checked;
  //
  LarefNFe.Enabled := EhNFe;
  EdrefNFe.Enabled := EhNFe;

  LaNFe_RP.Enabled := EhNFe;
  EdNFe_RP.Enabled := EhNFe;

  LaNFe_CC.Enabled := EhNFe;
  EdNFe_CC.Enabled := EhNFe;



  LamodNF.Enabled := not EhNFe;
  EdmodNF.Enabled := not EhNFe;
  LaSerie.Enabled := not EhNFe;
  EdSerie.Enabled := not EhNFe;
  LaNF.Enabled    := not EhNFe;
  EdNF.Enabled    := not EhNFe;

  LamodRP.Enabled := not EhNFe;
  EdmodRP.Enabled := not EhNFe;
  LaSerRP.Enabled := not EhNFe;
  EdSerRP.Enabled := not EhNFe;
  LaNF_RP.Enabled := not EhNFe;
  EdNF_RP.Enabled := not EhNFe;

  LamodCC.Enabled := not EhNFe;
  EdmodCC.Enabled := not EhNFe;
  LaSerCC.Enabled := not EhNFe;
  EdSerCC.Enabled := not EhNFe;
  LaNF_CC.Enabled := not EhNFe;
  EdNF_CC.Enabled := not EhNFe;

end;

procedure TFmPQENew.DefineDFe_Atrela(const TipoNFeEntrada: TTipoNFeEntrada);
const
  sProcName = 'TFmPQENew.DefineDFe_Atrela()';
var
  Empresa, CI, IQ, ide_mod, ide_Serie, ide_nNF: Integer;
  SQL_CNPJ_CPF_Emit, SQL_CNPJ_CPF_Dest: String;
  Continua: Boolean;
  Ed_modCC: TdmkEdit;
begin
  Ed_modCC := nil;
  Continua := True;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  IQ := EdFornecedor.ValueVariant;
  CI := EdCI.ValueVariant;
  case TipoNFeEntrada of
    TTipoNFeEntrada.tneVP:
      if EMpresa <> CI then
        Continua := Geral.MB_Pergunta(
        'Para esta pesquisa o cliente interno deve ser o mesmo que a empresa!'
        + sLineBreak + 'Mas deseja pesquisar assim mesmo?') = ID_YES;
    TTipoNFeEntrada.tneCC:
      if Empresa = CI then
        Continua := Geral.MB_Pergunta(
        'Para esta pesquisa o cliente interno deve ser diferente da emprea!'
        + sLineBreak + 'Mas deseja pesquisar assim mesmo?') = ID_YES;
    TTipoNFeEntrada.tneRP:
      if Empresa <> CI then
        Continua := Geral.MB_Pergunta(
        'Para esta pesquisa o cliente interno deve ser diferente da emprea!'
        + sLineBreak + 'Mas deseja pesquisar assim mesmo?') = ID_YES;
  end;
  //
  if MyObjects.FIC(IQ = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  if MyObjects.FIC(CI = 0, EdCI, 'Informe o cliente interno') then Exit;
  if Continua then
  begin
  case TipoNFeEntrada of
    TTipoNFeEntrada.tneVP:
    begin
      case QrFornecedoresTipo.Value of
        0: SQL_CNPJ_CPF_Emit := 'AND emit_CNPJ="' + Geral.SoNumero_TT(QrFornecedoresCNPJ.Value) + '"';
        1: SQL_CNPJ_CPF_Emit := 'AND emit_CPF="' + Geral.SoNumero_TT(QrFornecedoresCPF.Value) + '"';
        else SQL_CNPJ_CPF_Emit := 'AND emit_CPF_CPF=????';
      end;
      //
      case QrCITipo.Value of
        0: SQL_CNPJ_CPF_Dest := 'AND dest_CNPJ="' + Geral.SoNumero_TT(QrCICNPJ.Value) + '"';
        1: SQL_CNPJ_CPF_Dest := 'AND dest_CPF="' + Geral.SoNumero_TT(QrCICPF.Value) + '"';
        else SQL_CNPJ_CPF_Dest := 'AND dest_CPF_CPF=????';
      end;
    end;
    TTipoNFeEntrada.tneCC:
    begin
      case QrFornecedoresTipo.Value of
        0: SQL_CNPJ_CPF_Emit := 'AND emit_CNPJ="' + Geral.SoNumero_TT(QrFornecedoresCNPJ.Value) + '"';
        1: SQL_CNPJ_CPF_Emit := 'AND emit_CPF="' + Geral.SoNumero_TT(QrFornecedoresCPF.Value) + '"';
        else SQL_CNPJ_CPF_Emit := 'AND emit_CPF_CPF=????';
      end;
      //
      case DModG.QrEmpresasTipo.Value of
        0: SQL_CNPJ_CPF_Dest := 'AND dest_CNPJ="' + Geral.SoNumero_TT(DModG.QrEmpresasCNPJ_CPF.Value) + '"';
        1: SQL_CNPJ_CPF_Dest := 'AND dest_CPF="' + Geral.SoNumero_TT(DModG.QrEmpresasCNPJ_CPF.Value) + '"';
        else SQL_CNPJ_CPF_Dest := 'AND dest_CPF_CPF=????';
      end;
    end;
    else
    begin
      Geral.MB_Aviso('Tipo de entrada n�o implementada em ' + sProcName);
    end;
    //
  end;
    //
    case TipoNFeEntrada of
      //tneND: //=0,
      tneVP:
      begin
        // NF de venda
        ide_mod   := EdmodNF.ValueVariant;
        ide_Serie := EdSerie.ValueVariant;
        ide_nNF   := EdNF.ValueVariant;
        Ed_modCC  := EdmodNF;
      end;
       //=1,
      tneRP: //=2,
      begin
        // NF de remessa produ��o
        ide_mod   := EdmodRP.ValueVariant;
        ide_Serie := EdSerRP.ValueVariant;
        ide_nNF   := EdNF_RP.ValueVariant;
        Ed_modCC  := EdModRP;
      end;
      tneCC: //=3)
      begin
        // NF de remessa cobertura cliente
        ide_mod   := EdmodCC.ValueVariant;
        ide_Serie := EdSerCC.ValueVariant;
        ide_nNF   := EdNF_CC.ValueVariant;
        Ed_modCC  := EdModCC;
      end;
    end;
    //
    if MyObjects.FIC(ide_mod = 0, Ed_modCC,
      'Informe o modelo do documento fiscal!') then Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr00_NFeCabA, Dmod.MyDB, [
    'SELECT FatID, FatNum, Empresa, IDCtrl,',
    'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk,',
    'ide_mod, ide_serie, ide_nNF, Id  ',
    'FROM nfecaba ',
    'WHERE FatID=' + Geral.FF0(VAR_FATID_0053),
    'AND Empresa=' + Geral.FF0(Empresa),
    SQL_CNPJ_CPF_Emit,
    SQL_CNPJ_CPF_Dest,
    'AND ide_mod IN (0,' + Geral.FF0(ide_mod) + ')',
    'AND ide_serie >= 0 ', // + Geral.FF0(ide_Serie),
    'AND ide_nNF=' + Geral.FF0(ide_nNF),
    'ORDER BY ide_mod DESC ',
    '']);
    //Geral.MB_Teste(Qr00_NFeCabA.SQL.Text);
    if Qr00_NFeCabA.RecordCount > 0 then
    begin
      if not Qr00_NFeCabA.Locate('ide_mod;ide_serie', VarArrayOf([ide_mod, ide_serie]), []) then
      begin
        if not Qr00_NFeCabA.Locate('ide_serie', ide_serie, []) then
        begin
          if not Qr00_NFeCabA.Locate('ide_mod', ide_mod, []) then
          begin

          end;
        end;
      end;
      //Geral.MB_Teste(Qr00_NFeCabA.SQL.Text);
      //
      if Qr00_NFeCabA.RecordCount > 0 then
      begin
        if ide_Serie <> Qr00_NFeCabAide_serie.Value then
        begin
          case TipoNFeEntrada of
            tneVP: EdSerie.ValueVariant := Qr00_NFeCabAide_serie.Value;
            tneRP: EdSerRP.ValueVariant := Qr00_NFeCabAide_serie.Value;
            tneCC: EdSerCC.ValueVariant := Qr00_NFeCabAide_serie.Value;
          end;
        end;
        //
        case TipoNFeEntrada of
          tneVP:
          begin
            EdrefNFe.ValueVariant       := Qr00_NFeCabAId.Value;
            EdNFVP_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
            EdNFVP_FatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
            EdNFVP_FatNum.ValueVariant  := Qr00_NFeCabAFatNum.Value;
          end;
          tneRP:
          begin
            EdNFe_RP.ValueVariant       := Qr00_NFeCabAId.Value;
            EdNFRP_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
            EdNFRP_FatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
            EdNFRP_FatNum.ValueVariant  := Qr00_NFeCabAFatNum.Value;
          end;
          tneCC:
          begin
            EdNFe_CC.ValueVariant       := Qr00_NFeCabAId.Value;
            EdNFCC_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
            EdNFCC_FatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
            EdNFCC_FatNum.ValueVariant  := Qr00_NFeCabAFatNum.Value;
          end;
        end;
      end;
    end else
      Geral.MB_Info('N�o foi poss�vel encontrar a NFe!');
  end;
end;

procedure TFmPQENew.EdkgLiquidoExit(Sender: TObject);
begin
  EdkgLiquido.Text := Geral.TFT(EdkgLiquido.Text, 3, siPositivo);
end;

procedure TFmPQENew.EdkgBrutoExit(Sender: TObject);
begin
  EdkgBruto.Text := Geral.TFT(EdkgBruto.Text, 3, siPositivo);
end;

procedure TFmPQENew.EdFreteChange(Sender: TObject);
begin
  CalculaRetornoImpostosFrete(TRetornImpost.retimpostTodos);
end;

procedure TFmPQENew.EdFreteExit(Sender: TObject);
begin
  EdFrete.Text := Geral.TFT(EdFrete.Text, 2, siPositivo);
end;

procedure TFmPQENew.EdFreteRpCOFINSChange(Sender: TObject);
begin
  CalculaRetornoImpostosFrete(TRetornImpost.retimpostCOFINS);
end;

procedure TFmPQENew.EdFreteRpICMSChange(Sender: TObject);
begin
  CalculaRetornoImpostosFrete(TRetornImpost.retimpostICMS);
end;

procedure TFmPQENew.EdFreteRpPISChange(Sender: TObject);
begin
  CalculaRetornoImpostosFrete(TRetornImpost.retimpostPIS);
end;

procedure TFmPQENew.EdCIChange(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and
  (EdCI.ValueVariant <> 0) and
  (EdCI.ValueVariant <> DModG.QrEmpresasCodigo.Value) then
  begin
    EdModNF.ValueVariant := 0;
    EdmodCC.ValueVariant := 55;
    EdmodRP.ValueVariant := 55;
  end else
  begin
    EdModNF.ValueVariant := 55;
    EdmodCC.ValueVariant := 0;
    EdmodRP.ValueVariant := 0;
  end;
end;

procedure TFmPQENew.EdConhecimExit(Sender: TObject);
begin
  EdConhecim.Text := Geral.TFT(EdConhecim.Text, 0, siPositivo);
end;

procedure TFmPQENew.EdEmpresaChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    if EdEmpresa.ValueVariant <> 0 then
    begin
      //
      EdFreteRpICMS.ValueVariant   := DModG.QrParamsEmpFreteRpICMS.Value;
      EdFreteRpPIS.ValueVariant    := DModG.QrParamsEmpFreteRpPIS.Value;
      EdFreteRpCOFINS.ValueVariant := DModG.QrParamsEmpFreteRpCOFINS.Value;
    end;
  end;
end;

procedure TFmPQENew.EdRICMSExit(Sender: TObject);
begin
  EdRICMS.Text := Geral.TFT(EdRICMS.Text, 2, siPositivo);
end;

procedure TFmPQENew.EdValorNFExit(Sender: TObject);
begin
  EdValorNF.Text := Geral.TFT(EdValorNF.Text, 2, siPositivo);
end;

procedure TFmPQENew.EdNFExit(Sender: TObject);
begin
  EdNF.Text := Geral.TFT(EdNF.Text, 0, siPositivo);
end;

procedure TFmPQENew.EdNFe_CCExit(Sender: TObject);
begin
  if not DmNFe_0000.VerificaChavedeAcesso(EdNFe_CC.Text, True) then
    EdNFe_CC.SetFocus;
end;

procedure TFmPQENew.EdNFe_CCRedefinido(Sender: TObject);
var
  NFe_CC: String;
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
begin
  NFe_CC := EdNFe_CC.ValueVariant;
  if Length(Trim(NFe_CC)) >= 44 then
  begin
    //UnNFe_PF.ObtemSerieNumeroByID(refNFe, Serie, NF);
    NFeXMLGeren.DesmontaChaveDeAcesso(NFe_CC, 2.00,
    UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
    EdModCC.ValueVariant := ModNFe;
    EdSerCC.ValueVariant := SerNFe;
    EdNF_CC.ValueVariant := NumNFe;
  end;
end;

procedure TFmPQENew.EdNFe_RPExit(Sender: TObject);
begin
  if not DmNFe_0000.VerificaChavedeAcesso(EdNFe_RP.Text, True) then
    EdNFe_RP.SetFocus;
end;

procedure TFmPQENew.EdNFe_RPRedefinido(Sender: TObject);
var
  NFe_RP: String;
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
begin
  NFe_RP := EdNFe_RP.ValueVariant;
  if Length(Trim(NFe_RP)) >= 44 then
  begin
    //UnNFe_PF.ObtemSerieNumeroByID(refNFe, Serie, NF);
    NFeXMLGeren.DesmontaChaveDeAcesso(NFe_RP, 2.00,
    UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
    EdModRP.ValueVariant := ModNFe;
    EdSerRP.ValueVariant := SerNFe;
    EdNF_RP.ValueVariant := NumNFe;
  end;
end;

procedure TFmPQENew.EdrefNFeExit(Sender: TObject);
begin
  if not DmNFe_0000.VerificaChavedeAcesso(EdrefNFe.Text, True) then
    EdrefNFe.SetFocus;
end;

procedure TFmPQENew.EdrefNFeRedefinido(Sender: TObject);
var
  refNFe: String;
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
begin
  refNFe := EdrefNFe.ValueVariant;
  if Length(Trim(refNFe)) >= 44 then
  begin
    //UnNFe_PF.ObtemSerieNumeroByID(refNFe, Serie, NF);
    NFeXMLGeren.DesmontaChaveDeAcesso(refNFe, 2.00,
    UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
    EdModNF.ValueVariant := ModNFe;
    EdSerie.ValueVariant := SerNFe;
    EdNF.ValueVariant := NumNFe;
  end;
end;

procedure TFmPQENew.EdRICMSFExit(Sender: TObject);
begin
  EdRICMSF.Text := Geral.TFT(EdRICMSF.Text, 2, siPositivo);
end;

procedure TFmPQENew.EdJurosExit(Sender: TObject);
begin
  EdJuros.Text := Geral.TFT(EdJuros.Text, 2, siPositivo);
end;

procedure TFmPQENew.EdICMSExit(Sender: TObject);
begin
  EdICMS.Text := Geral.TFT(EdICMS.Text, 2, siPositivo);
end;

procedure TFmPQENew.EdIND_FRTChange(Sender: TObject);
var
  IND_FRT: Integer;
begin
  EdIND_FRT_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_FRT_EFD,  EdIND_FRT.ValueVariant);
  //
  IND_FRT := Geral.IMV('0' + EdIND_FRT.Text);
  case IND_FRT of
    3:
    begin
      EdTransportador.ValueVariant := EdCI.ValueVariant;
      CBTransportador.KeyValue     := EdCI.ValueVariant;
    end;
    4:
    begin
      EdTransportador.ValueVariant := DModG.QrEmpresasCodigo.Value;
      CBTransportador.KeyValue     := DModG.QrEmpresasCodigo.Value;
    end;
  end;
end;

procedure TFmPQENew.EdIND_FRTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_FRT.Text := Geral.SelecionaItem(F_IND_FRT_EFD, 0,
      'SEL-LISTA-000 :: Indicador do Tipo de Frete',
      TitCols, Screen.Width)
  end;
end;

procedure TFmPQENew.EdIND_PGTOChange(Sender: TObject);
begin
  EdIND_PGTO_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_PAG_EFD,  EdIND_PGTO.ValueVariant);
end;

procedure TFmPQENew.EdIND_PGTOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_PGTO.Text := Geral.SelecionaItem(F_IND_PGTO_EFD, 0,
      'SEL-LISTA-000 :: Indicador do tipo de pagamento',
      TitCols, Screen.Width)
  end;
end;

procedure TFmPQENew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQENew.FormShow(Sender: TObject);
begin
  EdIND_PGTO_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_PAG_EFD,  EdIND_PGTO.ValueVariant);
  EdIND_FRT_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_FRT_EFD,  EdIND_FRT.ValueVariant);
end;

function TFmPQENew.InserePQEItsDePediVda(Codigo: Integer): Boolean;
var
  //prod_cProd, prod_cEAN, prod_xProd, prod_NCM, prod_EX_TIPI, prod_CFOP, prod_uCom, prod_cEANTrib, prod_uTrib, IPI_cEnq, DtCorrApo, xLote: String;
  //Codigo,
  Controle, Conta, Insumo, Volumes, Moeda: Integer;
  //, prod_genero, ICMS_Orig,
  ICMS_CST: Integer;
  //ICMS_modBC, ICMS_modBCST,
  IPI_CST, PIS_CST, COFINS_CST: Integer;
  HowLoad: Integer;
  //FatID, FatNum, Empresa, nItem, NFeItsI_nItem: Integer;
  PesoVB, PesoVL, ValorItem, IPI, RIPI, CFin, ICMS, RICMS, TotalCusto, TotalPeso,
  Cotacao: Double;
  RpICMS, RpPIS, RpCOFINS: Double;
  //RvICMS, RvPIS, RvCOFINS, prod_qCom, prod_vUnCom,
  prod_vProd, prod_vFrete, prod_vSeg, prod_vOutro, prod_vDesc: Double;

  //prod_qTrib, prod_vUnTrib, ICMS_pRedBC, ICMS_vBC, ICMS_pICMS, ICMS_vICMS, ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST, ICMS_vICMSST, IPI_vBC, IPI_qUnid, IPI_vUnid, IPI_pIPI, IPI_vIPI, PIS_vBC, PIS_pPIS, PIS_vPIS, PIS_qBCProd, PIS_vAliqProd, PISST_vBC, PISST_pPIS, PISST_qBCProd, PISST_vAliqProd, PISST_vPIS, COFINS_vBC, COFINS_pCOFINS, COFINS_qBCProd, COFINS_vAliqProd, COFINS_vCOFINS, COFINSST_vBC, COFINSST_pCOFINS, COFINSST_qBCProd, COFINSST_vAliqProd, COFINSST_vCOFINS, RpIPI, RvIPI, RpICMSST, RvICMSST: Double;
  SQLType: TSQLType;
  CustoItem: Double;
  OriPart: Integer;
begin
  SQLType        := stIns;

  //Codigo             := Codigo;
  Controle           := 0; // Abaixo
  Conta              := QrPediVdaIts.RecNo;
  Insumo             := DmProd.ObtemGraGru1deGraGruX(QrPediVdaItsGraGruX.Value);
  Volumes            := 1;
  PesoVB             := 0;
  PesoVL             := QrPediVdaItsQuantP.Value - QrPediVdaItsQuantV.Value;
  ValorItem          := QrPediVdaItsValLiq.Value;
  IPI                := 0;
  RIPI               := 0;
  CFin               := 0;
  ICMS               := 0;
  RICMS              := 0;
  TotalPeso          := Volumes * PesoVL;
  Moeda              := 0; // BRL
  Cotacao            := 0;
  if Dmod.QrControleNaoRecupImposPQ.Value = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
      'SELECT ICMSRec_pAliq, PISRec_pAliq,  ',
      'COFINSRec_pAliq  ',
      'FROM gragru1 ',
      'WHERE Nivel1=' + Geral.FF0(Insumo),
      '']);
    //
    RpICMS   := QrGraGru1ICMSRec_pAliq.Value;
    RpPIS    := QrGraGru1PISRec_pAliq.Value;
    RpCOFINS := QrGraGru1COFINSRec_pAliq.Value;
  end else
  begin
    RpICMS   := 0.00;
    RpPIS    := 0.00;
    RpCOFINS := 0.00;
  end;
(*
  RvICMS         := ;
  RvPIS          := ;
  RvCOFINS       := ;
  prod_cProd     := ;
  prod_cEAN      := ;
  prod_xProd     := ;
  prod_NCM       := ;
  prod_EX_TIPI   := ;
  prod_genero    := ;
  prod_CFOP      := ;
  prod_uCom      := ;
  prod_qCom      := ;
  prod_vUnCom    := ;
  *)
  prod_vProd     :=  QrPediVdaItsvProd.Value;
  (*prod_cEANTrib  := ;
  prod_uTrib     := ;
  prod_qTrib     := ;
  prod_vUnTrib   := ;*)
  prod_vFrete    := QrPediVdaItsvFrete.Value;
  prod_vSeg      := QrPediVdaItsvSeg.Value;
  prod_vOutro    := QrPediVdaItsvOutro.Value;
  prod_vDesc     := QrPediVdaItsvDesc.Value;
  (*ICMS_Orig      := ;
  ICMS_CST       := ;
  ICMS_modBC     := ;
  ICMS_pRedBC    := ;
  ICMS_vBC       := ;
  ICMS_pICMS     := ;
  ICMS_vICMS     := ;
  ICMS_modBCST   := ;
  ICMS_pMVAST    := ;
  ICMS_pRedBCST  := ;
  ICMS_vBCST     := ;
  ICMS_pICMSST   := ;
  ICMS_vICMSST   := ;
  IPI_cEnq       := ;
  IPI_CST        := ;
  IPI_vBC        := ;
  IPI_qUnid      := ;
  IPI_vUnid      := ;
  IPI_pIPI       := ;
  IPI_vIPI       := ;
  PIS_CST        := ;
  PIS_vBC        := ;
  PIS_pPIS       := ;
  PIS_vPIS       := ;
  PIS_qBCProd    := ;
  PIS_vAliqProd  := ;
  PISST_vBC      := ;
  PISST_pPIS     := ;
  PISST_qBCProd  := ;
  PISST_vAliqProd:= ;
  PISST_vPIS     := ;
  COFINS_CST     := ;
  COFINS_vBC     := ;
  COFINS_pCOFINS := ;
  COFINS_qBCProd := ;
  COFINS_vAliqProd:= ;
  COFINS_vCOFINS := ;
  COFINSST_vBC   := ;
  COFINSST_pCOFINS:= ;
  COFINSST_qBCProd:= ;
  COFINSST_vAliqProd:= ;
  COFINSST_vCOFINS:= ;
  HowLoad        := ;
  FatID          := ;
  FatNum         := ;
  Empresa        := ;
  nItem          := ;
  DtCorrApo      := ;
  xLote          := ;
  RpIPI          := ;
  RvIPI          := ;
  NFeItsI_nItem  := ;
  RpICMSST       := ;
  RvICMSST       := ;

  //
? := UMyMod.BuscaEmLivreY_Def('pqeits', 'Controle', SQLType, CodAtual?);
ou > ? := UMyMod.BPGS1I32('pqeits', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
*)
  ICMS_CST   := Geral.IMV(DModG.QrPrmsEmpNFeUC_ICMS_CST_B.Value);
  IPI_CST    := Geral.IMV(DModG.QrPrmsEmpNFeUC_IPI_CST.Value);
  PIS_CST    := Geral.IMV(DModG.QrPrmsEmpNFeUC_PIS_CST.Value);
  COFINS_CST := Geral.IMV(DModG.QrPrmsEmpNFeUC_COFINS_CST.Value);
  //


  //
  HowLoad            := Integer(TPQEHowLoad.pqehlPediVda);
  CustoItem          := ValorItem; //(ValorItem + IPI_vIPI) * CFin;
  TotalCusto         := CustoItem;

  //
  OriPart            := QrPediVdaItsControle.Value;
  //
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PQEIts','PQEIts','Controle');
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqeits', False, [
  'OriPart',
  'Codigo', 'Conta', 'Insumo',
  'Volumes', 'PesoVB', 'PesoVL',
  'ValorItem', 'IPI', 'RIPI',
  'CFin', 'ICMS', 'RICMS',
  'TotalCusto', 'TotalPeso', 'Moeda',
  'Cotacao', 'RpICMS', 'RpPIS',
  'RpCOFINS', (*'RvICMS', 'RvPIS',
  'RvCOFINS', 'prod_cProd', 'prod_cEAN',
  'prod_xProd', 'prod_NCM', 'prod_EX_TIPI',
  'prod_genero', 'prod_CFOP', 'prod_uCom',
  'prod_qCom', 'prod_vUnCom',*) 'prod_vProd',
  (*'prod_cEANTrib', 'prod_uTrib', 'prod_qTrib',
  'prod_vUnTrib',*) 'prod_vFrete', 'prod_vSeg', 'prod_vOutro',
  'prod_vDesc', (*'ICMS_Orig',*) 'ICMS_CST',
  (*'ICMS_modBC', 'ICMS_pRedBC', 'ICMS_vBC',
  'ICMS_pICMS', 'ICMS_vICMS', 'ICMS_modBCST',
  'ICMS_pMVAST', 'ICMS_pRedBCST', 'ICMS_vBCST',
  'ICMS_pICMSST', 'ICMS_vICMSST', 'IPI_cEnq',*)
  'IPI_CST', (*'IPI_vBC', 'IPI_qUnid',
  'IPI_vUnid', 'IPI_pIPI', 'IPI_vIPI',*)
  'PIS_CST', (*'PIS_vBC', 'PIS_pPIS',
  'PIS_vPIS', 'PIS_qBCProd', 'PIS_vAliqProd',
  'PISST_vBC', 'PISST_pPIS', 'PISST_qBCProd',
  'PISST_vAliqProd', 'PISST_vPIS',*) 'COFINS_CST',
  (*'COFINS_vBC', 'COFINS_pCOFINS', 'COFINS_qBCProd',
  'COFINS_vAliqProd', 'COFINS_vCOFINS', 'COFINSST_vBC',
  'COFINSST_pCOFINS', 'COFINSST_qBCProd', 'COFINSST_vAliqProd',
  'COFINSST_vCOFINS',*) 'HowLoad'(*, 'FatID',
  'FatNum', 'Empresa', 'nItem',
  'DtCorrApo', 'xLote', 'RpIPI',
  'RvIPI', 'NFeItsI_nItem', 'RpICMSST',
  'RvICMSST'*)], [
  'Controle'], [
  OriPart,
  Codigo, Conta, Insumo,
  Volumes, PesoVB, PesoVL,
  ValorItem, IPI, RIPI,
  CFin, ICMS, RICMS,
  TotalCusto, TotalPeso, Moeda,
  Cotacao, RpICMS, RpPIS,
  RpCOFINS, (*RvICMS, RvPIS,
  RvCOFINS, prod_cProd, prod_cEAN,
  prod_xProd, prod_NCM, prod_EX_TIPI,
  prod_genero, prod_CFOP, prod_uCom,
  prod_qCom, prod_vUnCom,*) prod_vProd,
  (*prod_cEANTrib, prod_uTrib, prod_qTrib,
  prod_vUnTrib,*) prod_vFrete, prod_vSeg, prod_vOutro,
  prod_vDesc, (*ICMS_Orig,*) ICMS_CST,
  (*ICMS_modBC, ICMS_pRedBC, ICMS_vBC,
  ICMS_pICMS, ICMS_vICMS, ICMS_modBCST,
  ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST,
  ICMS_pICMSST, ICMS_vICMSST, IPI_cEnq,*)
  IPI_CST, (*IPI_vBC, IPI_qUnid,
  IPI_vUnid, IPI_pIPI, IPI_vIPI,*)
  PIS_CST, (*PIS_vBC, PIS_pPIS,
  PIS_vPIS, PIS_qBCProd, PIS_vAliqProd,
  PISST_vBC, PISST_pPIS, PISST_qBCProd,
  PISST_vAliqProd, PISST_vPIS,*) COFINS_CST,
  (*COFINS_vBC, COFINS_pCOFINS, COFINS_qBCProd,
  COFINS_vAliqProd, COFINS_vCOFINS, COFINSST_vBC,
  COFINSST_pCOFINS, COFINSST_qBCProd, COFINSST_vAliqProd,
  COFINSST_vCOFINS,*) HowLoad(*, FatID,
  FatNum, Empresa, nItem,
  DtCorrApo, xLote, RpIPI,
  RvIPI, NFeItsI_nItem, RpICMSST,
  RvICMSST*)], [
  Controle], True);
  //
  DmPediVda.AtualizaUmItemPediVda(OriPart);
end;

procedure TFmPQENew.ReopenPQEItsDePediVda(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediVdaIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM pedivdaits',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND QuantP > QuantV ',
  'ORDER BY Controle ',
  '']);
end;

procedure TFmPQENew.SbNF_VPClick(Sender: TObject);
begin
  DefineDFe_Atrela(tneVP);
end;

procedure TFmPQENew.SpeedButton1Click(Sender: TObject);
begin
  DefineDFe_Atrela(tneCC);
end;

procedure TFmPQENew.SpeedButton2Click(Sender: TObject);
begin
  DefineDFe_Atrela(tneRP);
end;

procedure TFmPQENew.VerificaChaveNFeDeEntrada(Empresa, CliInt: Integer;
  ChaveEmpresa, ChaveCliInt: String);
var
  ChaveNFe: String;
begin
  if (Empresa = CliInt) then
    ChaveNFe := ChaveEmpresa
  else
    ChaveNFe := ChaveCliInt;
  //
  if ChaveNFe = EmptyStr then
  begin
    Geral.MB_Aviso('Chave de acesso de entrada n�o definida corretamente!');
  end else
  begin
    if not DmNFe_0000.VerificaChavedeAcesso(ChaveNFe, False) then
      Exit;
  end;
end;

(*

      if UMyMod.SQLInsUpd(Dmod.QrUpdW, ImgTipo.SQLType, 'pqe', False, [
        'Data', 'IQ', 'CI',
        'Transportadora', 'NF', 'Frete',
        'PesoB', 'PesoL', 'ValorNF',
        'RICMS', 'RICMSF', 'Conhecimento',
        'Pedido', 'DataE', 'Juros',
        'ICMS', 'TipoNF',
        'refNFe', 'modNF', 'Serie',
        'NF_RP', 'NF_CC', 'Empresa',
        'FreteRpICMS', 'FreteRpPIS', 'FreteRpCOFINS',
        'FreteRvICMS', 'FreteRvPIS', 'FreteRvCOFINS',
        'modRP', 'modCC', 'SerRP', 'SerCC',
        'NFe_RP', 'NFe_CC', 'Cte_Id',
        'CTe_mod', 'CTe_serie',
        'NFVP_FatID', 'NFVP_FatNum', 'NFVP_StaLnk',
        'NFRP_FatID', 'NFRP_FatNum', 'NFRP_StaLnk',
        'NFCC_FatID', 'NFCC_FatNum', 'NFCC_StaLnk'], [
        'Codigo'], [
        Data, IQ, CI,
        Transportadora, NF, Frete,
        PesoB, PesoL, ValorNF,
        RICMS, RICMSF, Conhecimento,
        Pedido, DataE, Juros,
        ICMS, TipoNF,
        refNFe, modNF, Serie,
        NF_RP, NF_CC, Empresa,
        FreteRpICMS, FreteRpPIS, FreteRpCOFINS,
        FreteRvICMS, FreteRvPIS, FreteRvCOFINS,
        modRP, modCC, SerRP, SerCC,
        NFe_RP, NFe_CC, Cte_Id,
        CTe_mod, CTe_serie,
        NFVP_FatID, NFVP_FatNum, NFVP_StaLnk,
        NFRP_FatID, NFRP_FatNum, NFRP_StaLnk,
        NFCC_FatID, NFCC_FatNum, NFCC_StaLnk], [
        Codigo], True) then
*)

end.
