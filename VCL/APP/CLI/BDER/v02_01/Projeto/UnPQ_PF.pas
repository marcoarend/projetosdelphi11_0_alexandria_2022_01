unit UnPQ_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  dmkDBGridZTO, UnDmkProcFunc, UnProjGroup_Vars, BlueDermConsts, TypInfo,
  UnProjGroup_Consts, UnEntities, dmkEditDateTimePicker, DBCtrls, Grids, Mask,
  dmkCheckGroup, UnAppEnums;

type
  TTabPqNaoCad = (tpncND=0, tpncPQEIts=1, tpncPesagem=2);
  TUnPQ_PF = class(TObject)
  private
    { Private declarations }
    procedure ReimprimePesagemSomeWet(Emit: Integer; TpReceita: TReceitaTipoSetor; PB1: TProgressBar);
  public
    { Public declarations }
    function AtualizaSdoPQx(Qry: TmySQLQuery; OrigemCodi, OrigemCtrl,
              Tipo: Integer): Double;
    procedure MostraFormPQwPQEItsPos(AtualizaTodos: Boolean);
    procedure MostraFormPQwPQEItsNeg(AtualizaTodos: Boolean);
    procedure AjustePQx_Todos_2023();
    procedure AtualizaItensProcesso(Codigo: Integer; DataBase: TmySQLDatabase);
    function  BaixaPQx(OriCodi, OriCtrl, OriTipo, CliOrig, Empresa, Insumo:
              Integer; PesoBxa, ValorBxa: Double; DataX: String): Boolean;
    procedure CorrigePQGGXNiv2();
    function  ExcluiPesagem(Emit, Tipo: Integer; PB: TprogressBar): Boolean;
    function  DefineDtCorrApoTxt(CkDtCorrApo: TCheckBox; TPDtCorrApo:
              TdmkEditDateTimePicker): String;
    function  ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo:
              Integer; Avisa: Boolean; Empresa: Integer): Boolean;
    function  ExcluiPQx_Mul(QrPQx: TmySQLQuery; OriCodi, OriTipo: Integer; PB:
              TProgressBar; VerificaDiferencas: Boolean): Boolean;
    procedure ExcluiReceita(Numero: Integer);
    function  InserePQx_Bxa(DataX: String; CliOrig, CliDest, Insumo: Integer; Peso,
              Valor: Double; OrigemCodi, OrigemCtrl, Tipo: Integer; Unitario:
              Boolean; DtCorrApo: String; _Empresa: Integer;
              HowLoad: Integer = 1): Boolean;
    function  InserePQx_Inn(Query: TmySQLQuery; DataX: String; CliOrig, CliDest,
              Insumo: Integer; Peso, Valor: Double; HowLoad, OrigemCodi,
              OrigemCtrl, Tipo: Integer; DtCorrApo: String; _Empresa: Integer;
              serie, NF: Integer; xLote, dFab, dVal: String): Boolean;
    function  ImpedePeloEstoqueNF_Toda(OrigemCodi, OrigemTipo: Integer): Boolean;
    function  ImpedePeloEstoqueNF_Item(OrigemCodi, OrigemCtrl, OrigemTipo:
              Integer): Boolean;
    //
    procedure MostraFormFormulasImpBH();
    procedure MostraFormFormulasImpWE();
    procedure MostraFormFormulasImpFI();
    procedure MostraFormVSFormulasImp_BH(MovimCod, IMEI, TemIMEIMrt: Integer;
              MovimInn, MovimSrc: TEstqMovimNiv; ReceitaRibSetor:
              TReceitaRibSetor);
    procedure MostraFormVSFormulasImp_WE(MovimCod, IMEI, TemIMEIMrt, EmitGru:
              Integer; MovimInn, MovimSrc: TEstqMovimNiv);
    procedure MostraFormVSFormulasImp_FI(MovimCod, IMEI, TemIMEIMrt, EmitGru:
              Integer; MovimInn, MovimSrc: TEstqMovimNiv);
    function  MostraFormVSPQOCab(const SQLType: TSQLType; const VSMOvCod,
              EmitGru: Integer; const PrecisaEmitGru: Boolean;
              var Codigo: Integer): Boolean;
    function  MostraFormVSPQOIts(const SQLType: TSQLType; const Codigo: Integer;
              Data: TDateTime; var Controle: Integer): Boolean;
    //
    function  ObtemNomeTabelaTipoMovPQ(Tipo: Integer): String;
    function  ObtemSetoresSelecionados(const DBGSetores: TdmkDBGridZTO; const
              QrListaSetores: TmySQLQuery; var FSetoresCod, FSetoresTxt:
              String): Boolean;
    procedure PQO_AtualizaCusto(Codigo: Integer);
    procedure PQO_ExcluiItem(Controle: Integer; Pergunta, AtzCusto: Boolean);
    procedure PQO_ExcluiCab(Codigo: Integer);
    procedure ReimprimePesagemNovo(Emit, SetrEmi: Integer; TpReceita: TReceitaTipoSetor; PB: TProgressBar);
(*
    procedure ReopenPQOVS(QrPQO: TmySQLQuery; MovimCod, Codigo: Integer);
    procedure ReopenPQOIts(QrPQOIts: TmySQLQuery; Codigo, Controle: Integer);
*)
    function  VerificaEquiparacaoEstoque(Mostra: Boolean): Boolean;
    function  ValidaProdutosFormulasRibeira(Numero: Integer): Boolean;
    function  ValidaProdutosFormulasAcabto(Numero: Integer): Boolean;
    function  InsumosSemCadastroParaClienteInterno(CliInt, Empresa,
              InsumoEspecifico, FormulaEspecifica: Integer;
              Tabela: TTabPqNaoCad; MostraJanela: Boolean = True): Boolean;
    function  ObtemDadosGradeDePQ(const Insumo: Integer; var Tipo_Item,
              GraGruX, UnidMed: Integer; var Sigla, NCM: String; const Avisa:
              Boolean): Boolean;
    function  DefineVariaveisPesqusaEstoquePQ(const EdPQ, EdCIDst, EdCIOri:
              TdmkEditCB; const RGOrdem1, RGOrdem2, RGOrdem3: TRadioGroup;
              CGTipoCad: TdmkCheckGroup;
              var FCIDst, FCIOri, FPQ: Integer; var FSE, FTC: String): Boolean;
    function  TransfereReceitaDeArquivo(Orientacao: TOrientTabArqBD; Numero,
              Motivo: Integer): Boolean;
    function  AtualizaTodosPQsPosBalanco(TipoAviso: TTipoAviso; Progress1:
              TProgressBar; PnInfo, PnRegistros, PnTempo: TPanel; TimerIni:
              TDateTime; Data: TDateTime): Boolean;
    function  CalculaProducaoDeItemDePedidoDePrevisaoDeCompraDeInsumos(const
              Codigo, Controle, EmitGru, CliOrig, Formula, PedGrandeza: Integer;
              const LinhasCul, LinhasCab, RendEsperado: Double; const
              InMultiExec: Boolean; var PedQtPrGdz, ArM2ComRend, ArP2ComRend,
              PesoComRend: Double; var ErrTxt: String): Boolean;
    function  CalculaDadosPrevConsPed(const PedGrandeza: Integer; const
              PedQtdeGdz, PedLinhasCul, PedLinhasCab: Double; var PedAreaM2,
              PedAreaP2, PedPesoKg, PedQtPdGdz, PedQtPrGdz, PedPdArM2,
              PedPdArP2, PedPdPeKg, PedPrArM2, PedPrArP2, PedPrPeKg: Double):
              Boolean;
    procedure CalculaDataFimProducao(const PedQtPdGdz, QtProdDia: Double;
              DtIniProd, HrIniProd: TDateTime; var DtFImProd, HrFimProd:
              TDateTime);
    procedure TotalizaEmitSbt(EmitGru: Integer);
    procedure AtualizaSaldo_EmitSbtQtd(Controle: Integer);
    procedure AtualizaSaldo_Emit_EMitUsoSbt(Pesagem: Integer);
    procedure MostraFormPQImp();
    procedure MostraFormUsoConsImp(FatID: Integer);
    procedure MostraFormPQE(Codigo: Integer);
    procedure DefineContasContabeisEntradaInsumosPQE_UmItem(TypCtbCadMoF:
              TTypCtbCadMoF; const PQE: Integer; var GenCtbD, GenCtbC: Integer;
              var Descricao: String);
    procedure DefineContasContabeisEntradaInsumosPQE_VariosItens(TypCtbCadMoF:
              TTypCtbCadMoF; const PQE: Integer; var GenCtbD, GenCtbC: Integer;
              var Descricao: String);

    //
  end;

var
  PQ_PF: TUnPQ_PF;

implementation

uses MyDBCheck, UMySQLModule, Module, DmkDAC_PF, PQx, UnMyObjects, ModuleGeral,
  FormulasImpBH, FormulasImpWE2, FormulasImpFI,
  FormulasImpShow, FormulasImpShowNew, ModVS, ModVS_CRC, ModEmit,
  (* provisorio *) Principal, (* provisorio *)
  VSFormulasImp_BH, VSFormulasImp_WE, VSFormulasImp_FI, VSPQOCab, VSPQOIts,
  PQwPQEItsPos, PQwPQEItsNeg, PQCorrigeGGXNiv2, GerlShowGrid, PQImp, UsoConsImp,
  PQE, ModuleFin, UnContabil_PF, PQWRefaz;

{ TUnPQ_PF }

procedure TUnPQ_PF.AjustePQx_Todos_2023();
begin
  if VAR_USUARIO <> -1 then
    if not DBCheck.LiberaPelaSenhaMaster() then Exit;
  //
  Application.CreateForm(TFmPQWRefaz,FmPQWRefaz);
  FmPQWRefaz.ShowModal;
  FmPQWRefaz.Destroy;
end;

procedure TUnPQ_PF.AtualizaSaldo_EmitSbtQtd(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrSumUsoSbt, Dmod.MyDB, [
  'SELECT SUM(CouIntros) CouIntros, SUM(AreaM2) AreaM2,',
  'SUM(AreaP2) AreaP2, SUM(PesoKg) PesoKg',
  'FROM emitusosbt',
  'WHERE IDEmitSbtQtd=' + Geral.FF0(Controle),
  '']);
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE emitsbtqtd SET ',
  'SdoCouIntros=CouIntros-' + Geral.FFT_Dot(Dmod.QrSumUsoSbtCouIntros.Value, 3, siNegativo) + ',',
  'SdoAreaM2=AreaM2-' + Geral.FFT_Dot(Dmod.QrSumUsoSbtAreaM2.Value, 2, siNegativo) + ',',
  'SdoAreaP2=AreaP2-' + Geral.FFT_Dot(Dmod.QrSumUsoSbtAreaP2.Value, 2, siNegativo),
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  //
end;

procedure TUnPQ_PF.AtualizaSaldo_Emit_EMitUsoSbt(Pesagem: Integer);
const
  SQLType = TSQLType.stUpd;
var
  SbtCouIntros, SbtAreaM2, SbtAreaP2, SbtRendEsper: Double;
  Codigo: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrMedUsoSbt, Dmod.MyDB, [
  'SELECT SUM(CouIntros) CouIntros, ',
  'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
  'SUM(PesoKg) PesoKg,',
  'SUM(AreaM2 * PercEsperRend) / SUM(AreaM2) PercEsperRend   ',
  'FROM emitusosbt ',
  'WHERE Codigo=' + Geral.FF0(Pesagem),
  ' ']);
  SbtCouIntros  := Dmod.QrMedUsoSbtCouIntros.Value;
  SbtAreaM2     := Dmod.QrMedUsoSbtAreaM2.Value;
  SbtAreaP2     := Dmod.QrMedUsoSbtAreaP2.Value;
  SbtRendEsper  := Dmod.QrMedUsoSbtPercEsperRend.Value;
  Codigo        := Pesagem;
  //
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'emit', False, [
  'SbtCouIntros', 'SbtAreaM2', 'SbtAreaP2',
  'SbtRendEsper'], [
  'Codigo'], [
  SbtCouIntros, SbtAreaM2, SbtAreaP2,
  SbtRendEsper], [
  Codigo], True) ;
end;

function TUnPQ_PF.AtualizaSdoPQx(Qry: TmySQLQuery; OrigemCodi, OrigemCtrl,
  Tipo: Integer): Double;
var
  Peso, Valor, SdoPeso, SdoValr: Double;
begin
  Result := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Peso, Valor  ',
  'FROM pqx ',
  'WHERE OrigemCodi=' + Geral.FF0(OrigemCodi),
  'AND OrigemCtrl=' + Geral.FF0(OrigemCtrl),
  'AND Tipo=' + Geral.FF0(Tipo),
  '']);
  Peso := Qry.FieldByName('Peso').AsFloat;
  Valor := Qry.FieldByName('Valor').AsFloat;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT SUM(Peso) Peso,  SUM(Valor) Valor',
  'FROM pqw',
  'WHERE DstCodi=' + Geral.FF0(OrigemCodi),
  'AND DstCtrl=' + Geral.FF0(OrigemCtrl),
  'AND DstTipo=' + Geral.FF0(Tipo),
  '']);
  //
  SdoPeso := Peso  - Qry.FieldByName('Peso').AsFloat;
  SdoValr := Valor - Qry.FieldByName('Valor').AsFloat;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_PQX, False, [
  'SdoPeso', 'SdoValr'], [
  'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
  SdoPeso, SdoValr], [
  OrigemCodi, OrigemCtrl, Tipo], False);
  //
  Result := SdoPeso;
end;

function TUnPQ_PF.AtualizaTodosPQsPosBalanco(TipoAviso: TTipoAviso;
  Progress1: TProgressBar; PnInfo, PnRegistros, PnTempo: TPanel; TimerIni:
  TDateTime; Data: TDateTime): Boolean;
var
  Qry: TmySQLQuery;
  Abrangencia, PeriodoBal: Integer;
  sData, DtaTxt: String;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qry.Close;
    //UnDmkDAC_PF.AbreQuery(Qry, Dmod.MyDB);
    Abrangencia := -1;
    while not (Abrangencia in ([0,1,2])) do
      Abrangencia := MyObjects.SelRadioGroup(
      'Abrang�cia da atualiza��o de estque de insumos',
      'Selecione a abrang�ncia:', ['Apenas movimentados', 'Todos insumos', 'Todos (Novo)'], 2);

    case Abrangencia of
      0:
      begin
        DtaTxt := Geral.FDT(Trunc(Data), 1);
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT DISTINCT Insumo PQ, CliDest CI, Empresa ',
        'FROM pqx ',
        'WHERE DataX >= "' + DtaTxt + '" ',
        'ORDER BY PQ, CI ',
        '']);
      end;
      1:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT PQ PQ, CI CI, Empresa ',
        'FROM pqcli ',
        'ORDER BY PQ, CI ',
        '']);
      end;
      2:
      begin
        Screen.Cursor := crHourGlass;
        try
          PeriodoBal := UnPQx.VerificaBalanco();
          if PeriodoBal = 0 then
            sData  := FormatDateTime(VAR_FORMATDATE, 2)
          else
            sData := dmkPF.PrimeiroDiaAposPeriodo(PeriodoBal, dtSystem); //2?
          //
          UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
          'DROP TABLE IF EXISTS _pq_estq_cli_all;',
          'CREATE TABLE _pq_estq_cli_all',
          'SELECT Empresa, CliOrig, Insumo, ',
          'SUM(Peso) Peso, SUM(Valor) Valor ',
          'FROM ' + TMeuDB + '.pqx ',
          'WHERE DataX>="' + sData + '"',
          'AND Tipo>=0 ',
          'AND HowLoad<>2 ',
          'GROUP BY Empresa, CliOrig, Insumo',
          ';',
          'UPDATE ' + TMeuDB + '.pqcli SET Peso=0, Valor=0',
          ';',
          'UPDATE ' + TMeuDB + '.pqcli T1',
          'LEFT JOIN _pq_estq_cli_all T2 ON ',
          '  T1.Empresa = T2.Empresa',
          '  AND T1.CI=T2.CliOrig',
          '  AND T1.PQ=T2.Insumo',
          'SET T1.Peso = T2.Peso, ',
          '    T1.Valor = T2.Valor;',
          '']));
          Result := True;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
    if Abrangencia in ([0,1]) then
    begin
      Progress1.Position := 0;
      Progress1.Max := Qry.RecordCount;
      if PnInfo <> nil then PnInfo.Visible := True;
      if PnInfo <> nil then PnInfo.Refresh;
      //GBCntrl.Refresh;
      //Panel1.Refresh;
      //Panel2.Refresh;
      TimerIni := Now();

      //  fazer apenas uma vez para n�o demorar o UnPQx.AtualizaEstoquePQ !!!
      DMod.AtualizaStqMovIts_PQX(nil, nil, nil);
      //
      VAR_PQS_ESTQ_NEG := '';
      while not Qry.Eof do
      begin
        Progress1.Position := Progress1.Position + 1;
        if PnRegistros <> nil then
        begin
          PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
          PnRegistros.Refresh;
        end;
        if PnTempo <> nil then
        begin
          PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
          PnTempo.Refresh;
        end;
        Application.ProcessMessages;
        UnPQx.AtualizaEstoquePQ(Qry.FieldByName('CI').AsInteger,
          Qry.FieldByName('PQ').AsInteger, Qry.FieldByName('Empresa').AsInteger,
          TipoAviso, CO_VAZIO, False);
        Qry.Next;
      end;
      Result := True;
      //if PnInfo <> nil then PnInfo.Visible := False;
      Qry.Close;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnPQ_PF.MostraFormPQwPQEItsNeg(AtualizaTodos: Boolean);
begin
  if DBCheck.CriaFm(TFmPQwPQEItsNeg, FmPQwPQEItsNeg, afmoNegarComAviso) then
  begin
    FmPQwPQEItsNeg.FAtualizaTodos := AtualizaTodos;
    FmPQwPQEItsNeg.ShowModal;
    FmPQwPQEItsNeg.Destroy;
  end;
end;

procedure TUnPQ_PF.MostraFormPQwPQEItsPos(AtualizaTodos: Boolean);
begin
  if DBCheck.CriaFm(TFmPQwPQEItsPos, FmPQwPQEItsPos, afmoNegarComAviso) then
  begin
    FmPQwPQEItsPos.FAtualizaTodos := AtualizaTodos;
    FmPQwPQEItsPos.ShowModal;
    FmPQwPQEItsPos.Destroy;
  end;
end;

function TUnPQ_PF.BaixaPQx(OriCodi, OriCtrl, OriTipo, CliOrig, Empresa, Insumo:
  Integer; PesoBxa, ValorBxa: Double; DataX: String): Boolean;
var
  Qry1, Qry2: TmySQLQuery;
  DstCodi, DstCtrl, DstTipo, OrigemCodi, OrigemCtrl, Tipo, OriEmpresa,
  OriCliInt, OriInsumo, DstEmpresa, DstCliInt, DstInsumo: Integer;
  Peso1, (*Valor1,*) Peso2, Peso, Valor, Valor2, DstValr: Double;
  NxLotes, DataE: String;
  DataX2: TDateTime;
begin
  Peso1 := PesoBxa;
  Qry1   := TmySQLQuery.Create(Dmod);
  try
    Qry2   := TmySQLQuery.Create(Dmod);
    try
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT * ',
      'FROM pqx ',
      'WHERE CliOrig=' + Geral.FF0(CliOrig),
      'AND Insumo=' + Geral.FF0(Insumo),
      'AND Peso>0 ',
      'AND SdoPeso>0 ',
      //
      // ini 2023-10-18 - Migra��o para novo balan�o Com ajuste por FatID 20 e 120
      'AND Tipo<>0', // Tipo zero � usado somente para calculo de estoque dentro do per�odo de balan�o atual ou selecionado (estoque em...)
      'ORDER BY DataX, Tipo, OrigemCtrl DESC ',  // Tipo > primeiro 10 depois 20!
{
      // ini 2023-10-06 - erro de PEPS (Primeiro que entra � o primeiro que sai)
      //'ORDER BY DataX DESC, Tipo DESC, OrigemCtrl DESC ',
      'ORDER BY DataX, Tipo DESC, OrigemCtrl DESC ',
      // fim 2023-10-06
}
      // fim 2023-10-18
      //
      '']);
      //
      //Geral.MB_Teste(Qry1.SQL.Text);
      //
      Qry1.First;
      while not Qry1.Eof do
      begin
        OrigemCodi := Qry1.FieldByName('OrigemCodi').AsInteger;
        OrigemCtrl := Qry1.FieldByName('OrigemCtrl').AsInteger;
        Tipo       := Qry1.FieldByName('Tipo').AsInteger;
        Peso2      := Qry1.FieldByName('SdoPeso').AsFloat;
        Valor2     := Qry1.FieldByName('SdoValr').AsFloat;
        //
        OriEmpresa := Qry1.FieldByName('Empresa').AsInteger;
        OriCliInt  := Qry1.FieldByName('CliOrig').AsInteger;
        OriInsumo  := Qry1.FieldByName('Insumo').AsInteger;
        DataX2     := Qry1.FieldByName('DataX').AsDateTime;
        DataE      := Geral.FDT(DataX2, 1);
        //
        if Peso1 <= 0 then
        begin
          Qry1.Last;
          // ter certeza!!!
          Qry1.Next;
        end else
        begin
          if Peso1 > Peso2 then
          begin
            Peso    := Peso2;
            Peso1   := Peso1 - Peso2;
            //Peso2   := 0;
          end else
          begin
            Peso    := Peso1;
            //Peso2   := Peso2 - Peso1;
            Peso1   := 0;
          end;
          if Peso2 > 0 then
          begin
            Valor := Valor2 / Peso2 * Peso
          end else
          begin
            Valor := 0;
          end;
          DstCodi := OrigemCodi;
          DstCtrl := OrigemCtrl;
          DstTipo := Tipo;
          DstEmpresa := Empresa;
          DstCliInt  := CliOrig;
          DstInsumo  := Insumo;
          DstValr    := ValorBxa;
          //
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqw', False, [
          'Peso', 'Valor', 'OriEmpresa',
          'OriCliInt', 'OriInsumo', 'DstEmpresa',
          'DstCliInt', 'DstInsumo', 'DstValr',
          'DataE', 'DataX'], [
          'OriCodi', 'OriCtrl', 'OriTipo', 'DstCodi', 'DstCtrl', 'DstTipo'], [
          Peso, Valor, OriEmpresa,
          OriCliInt, OriInsumo, DstEmpresa,
          DstCliInt, DstInsumo, DstValr,
          DataE, DataX], [
          OriCodi, OriCtrl, OriTipo, DstCodi, DstCtrl, DstTipo], False);
          //
          Peso  := Qry1.FieldByName('Peso').AsFloat;
          Valor := Qry1.FieldByName('Valor').AsFloat;
          //
          // Atualizar saldo do estoque no pqx
          AtualizaSdoPQx(Qry2, DstCodi, DstCtrl, DstTipo);
        end;
        //
        Qry1.Next;
      end;
    finally
      Qry2.Free;
    end;
    if OriTipo = 110 then // EmitIts
    begin
      UnDmkDAC_PF.AbreMySQLQUery0(Qry1, Dmod.MyDB, [
      'SELECT pqi.Controle, GROUP_CONCAT(pqi.xLote SEPARATOR ", ") NxLotes ',
      'FROM pqw pqw ',
      'LEFT JOIN pqeits pqi  ',
      '  ON pqi.Codigo=pqw.DstCodi ',
      '  AND pqi.Controle=pqw.DstCtrl ',
      'WHERE OriCodi=' + Geral.FF0(OriCodi),
      'AND OriCtrl=' + Geral.FF0(OriCtrl),
      'AND OriTipo=' + Geral.FF0(OriTipo),
      ' ']);
      //
      NxLotes := Qry1.FieldByName('NxLotes').AsString;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'emitits', False, [
      'NxLotes'], [
      'Codigo', 'Controle'], [
      NxLotes], [
      OriCodi, OriCtrl], False);
      //
    end;
  finally
    Qry1.Free;
  end;
end;

function TUnPQ_PF.ValidaProdutosFormulasAcabto(Numero: Integer): Boolean;
begin
  Result := True;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT tit.* ',
    'FROM tintasits tit ',
    'LEFT JOIN tintascab tca ON tca.Numero = tit.Numero ',
    'LEFT JOIN pq pqi ON pqi.Codigo = tit.Produto ',
    'WHERE (pqi.Ativo = 0 OR pqi.GGXNiv2 = 0) ',
    'AND tca.Numero=' + Geral.FF0(Numero),
    '']);
  if Dmod.QrAux.RecordCount > 0 then
    Result := False;
  //
  Dmod.QrAux.Close;
end;

function TUnPQ_PF.ValidaProdutosFormulasRibeira(Numero: Integer): Boolean;
begin
  Result := True;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT foi.* ',
    'FROM formulasits foi ',
    'LEFT JOIN formulas frm ON frm.Numero=foi.Numero ',
    'LEFT JOIN pq pqi ON pqi.Codigo=foi.Produto ',
    'WHERE (pqi.Ativo = 0 OR pqi.GGXNiv2 = 0) ',
    'AND frm.Numero=' + Geral.FF0(Numero),
    '']);
  if Dmod.QrAux.RecordCount > 0 then
    Result := False;
  //
  Dmod.QrAux.Close;
end;

function TUnPQ_PF.CalculaDadosPrevConsPed(const PedGrandeza: Integer; const
              PedQtdeGdz, PedLinhasCul, PedLinhasCab: Double; var PedAreaM2,
              PedAreaP2, PedPesoKg, PedQtPdGdz, PedQtPrGdz, PedPdArM2,
              PedPdArP2, PedPdPeKg, PedPrArM2, PedPrArP2, PedPrPeKg: Double):
              Boolean;
var
  //PedGrandeza: Integer;
  //PedQtdeGdz, PedLinhasCul, PedLinhasCab, PedAreaM2, PedAreaP2, PedPesoKg,
  //PedQtPdGdz, PedQtPrGdz,
  //PedPdArM2, PedPdArP2, PedPdPeKg, PedPrArM2,
  //PedPrArP2, PedPrPeKg
  Fator: Double;
begin
  Result := False;
(*
  LaQtPedido.Caption := RGPedGrandeza.Items[RGPedGrandeza.ItemIndex];
  LaQtProduzido.Caption := RGPedGrandeza.Items[RGPedGrandeza.ItemIndex];
  LaQtAProduzir.Caption := RGPedGrandeza.Items[RGPedGrandeza.ItemIndex];
*)
(*
  PedGrandeza    := RGPedGrandeza.ItemIndex;
  PedQtdeGdz     := EdPedQtdeGdz.ValueVariant;
  PedLinhasCul   := EdPedLinhasCul.ValueVariant;
  PedLinhasCab   := EdPedLinhasCab.ValueVariant;
  PedAreaM2      := EdPedAreaM2.ValueVariant;
  PedAreaP2      := EdPedAreaP2.ValueVariant;
  PedPesoKg      := EdPedPesoKg.ValueVariant;
  //
  PedQtPdGdz     := EdPedQtPdGdz.ValueVariant;
  PedQtPrGdz     := EdPedQtPrGdz.ValueVariant;
*)
  //
  Fator := ((PedLinhasCul * 0.7) + (PedLinhasCab * 0.3)) * 1.15 / 10;
  case PedGrandeza of
    //0: //
    1: // Area m2
    begin
      PedAreaM2 := PedQtdeGdz;
      PedAreaP2 := Geral.ConverteArea(PedAreaM2, ctM2toP2, cfQuarto);
      PedPesoKg := PedAreaM2 * Fator;
      //
      PedPdArM2 := PedQtPdGdz;
      PedPdArP2 := Geral.ConverteArea(PedPdArM2, ctM2toP2, cfQuarto);
      PedPdPeKg := PedPdArM2 * Fator;
      //
      PedPrArM2 := PedQtPrGdz;
      PedPrArP2 := Geral.ConverteArea(PedPrArM2, ctM2toP2, cfQuarto);
      PedPrPeKg := PedPrArM2 * Fator;
    end;
    2: // Area m2
    begin
      PedAreaP2 := PedQtdeGdz;
      PedAreaM2 := Geral.ConverteArea(PedAreaP2, ctP2toM2, cfCento);
      PedPesoKg := PedAreaM2 * Fator;
      //
      PedPdArP2 := PedQtPdGdz;
      PedPdArM2 := Geral.ConverteArea(PedPdArP2, ctP2toM2, cfCento);
      PedPdPeKg := PedPdArM2 * Fator;
      //
      PedPrArP2 := PedQtPrGdz;
      PedPrArM2 := Geral.ConverteArea(PedPrArP2, ctP2toM2, cfCento);
      PedPrPeKg := PedPrArM2 * Fator;
    end;
    3: // Area m2
    begin
      PedPesoKg := PedQtdeGdz;
      if Fator = 0 then
        PedAreaM2 := 0
      else
        PedAreaM2 := PedPesoKg / Fator;
      PedAreaP2 := Geral.ConverteArea(PedAreaM2, ctM2toP2, cfQuarto);
      PedPesoKg := PedAreaM2 * Fator;
      //
      PedPdPeKg := PedQtPdGdz;
      if Fator = 0 then
        PedPdArM2 := 0
      else
        PedPdArM2 := PedPdPeKg / Fator;
      PedPdArP2 := Geral.ConverteArea(PedPdArM2, ctM2toP2, cfQuarto);
      PedPdPeKg := PedPdArM2 * Fator;
      //
      PedPrPeKg := PedQtPrGdz;
      if Fator = 0 then
        PedPrArM2 := 0
      else
        PedPrArM2 := PedPrPeKg / Fator;
      PedPrArP2 := Geral.ConverteArea(PedPrArM2, ctM2toP2, cfQuarto);
      PedPrPeKg := PedPrArM2 * Fator;
      //
    end;
    else
    begin
      PedAreaM2 := 0;
      PedAreaP2 := 0;
      PedPesoKg := 0;
      //
      PedPdArM2 := 0;
      PedPdArP2 := 0;
      PedPdPeKg := 0;

      PedPrArM2 := 0;
      PedPrArP2 := 0;
      PedPrPeKg := 0;
    end;
  end;
(*
  EdPedAreaM2.ValueVariant := PedAreaM2;
  EdPedAreaP2.ValueVariant := PedAreaP2;
  EdPedPesoKg.ValueVariant := PedPesoKg;
  //
  EdPedPdArM2.ValueVariant := PedPdArM2;
  EdPedPdArP2.ValueVariant := PedPdArP2;
  EdPedPdPeKg.ValueVariant := PedPdPeKg;
  //
  EdPedPrArM2.ValueVariant := PedPrArM2;
  EdPedPrArP2.ValueVariant := PedPrArP2;
  EdPedPrPeKg.ValueVariant := PedPrPeKg;
*)
  Result := True;
end;

procedure TUnPQ_PF.CalculaDataFimProducao(const PedQtPdGdz, QtProdDia: Double;
  DtIniProd, HrIniProd: TDateTime; var DtFImProd, HrFimProd: TDateTime);
const
  // Horario >> 7:30 �s 11:30 e das 13:00 �s 17:00
  //OitoHoras = 8 / 24; // 8 horas
  IniTurno = 7.5 / 24;
  IniIntervalo = 11.5 / 24;
  IntervaloAlmoco = 1.5 / 24;
  MeioTurno = 4 / 24;
  Turno = 3;  // um ter�o do dia
  DiasSemana = 7;
  Domingo = 1; // Primeiro dia da semana
  Sabado = 7; // S�timo dia da semana
  DiasFimDeSemana = 2; // S�bado e domingo
var
  DataIni, DataFim, DtEntrega, HrEntrega, Hora: TDateTime;
  Dias, Semanas: Double;
begin
  DtEntrega := 0;
  HrEntrega := 0;
  //Hora := EdDtIniProd.ValueVariant;
  Hora := HrIniProd;
  //
  if Hora >= IniIntervalo then
    Hora := (MeioTurno + (Hora - IniIntervalo)) * Turno
  else
  if (Hora < IniIntervalo) and (Hora > IniTurno) then
    Hora := (Hora - IniTurno) * Turno
  else
    Hora := 0;
  //
  //DataIni := Trunc(DtIniProd.Date) + Hora;
  DataIni := Trunc(DtIniProd) + Hora;
  //PedQtPdGdz := EdPedQtPdGdz.ValueVariant;
  //QtProdDia  := EdQtProdDia.ValueVariant;
  if QtProdDia > 0 then
    Dias := PedQtPdGdz / QtProdDia
  else
    Dias := 0;
  //
  Semanas   := Trunc(Dias / DiasSemana);
  Dias      := Dias + (Semanas * DiasFimDeSemana); // adi��o de s�bado e domingo!
  DataFim   := DataIni + Dias;
  DtEntrega := Trunc(DataFim);
  HrEntrega := DataFim - DtEntrega;
  HrEntrega := (HrEntrega / Turno) + IniTurno;
  if HrEntrega > IniIntervalo then
    HrEntrega := HrEntrega + IntervaloAlmoco;
  //
  case DayOfWeek(DtEntrega) of
    Domingo: DtEntrega := DtEntrega + Domingo;
    Sabado : DtEntrega := DtEntrega + DiasFimDeSemana;
  end;
  DtFimProd := DtEntrega;
  HrFimProd := HrEntrega;
end;

function TUnPQ_PF.CalculaProducaoDeItemDePedidoDePrevisaoDeCompraDeInsumos(
  const Codigo, Controle, EmitGru, CliOrig, Formula, PedGrandeza: Integer;
  const LinhasCul, LinhasCab, RendEsperado: Double; const InMultiExec: Boolean;
  var PedQtPrGdz, ArM2ComRend, ArP2ComRend, PesoComRend: Double; var ErrTxt:
  String): Boolean;
  //
  function CalculaAreaPeso(Peso: Double (*str: String*)): Double;
  begin
    Result := 0;
    //
    if (LinhasCab > 0) and (LinhasCul > 0) then
    begin
      Result := ((LinhasCab * 0.3) + (LinhasCul * 0.7)) * 1.15 / 10;
      //Geral.MB_Info(Geral.FFT(Result, 10, siNegativo));
      if Peso > 0 then
      begin
        Result := Peso / Result;
        //Geral.MB_Info(Geral.FFT(Result, 10, siNegativo));
      end else
        Result := 0;
    end else
      Result := 0;
  end;
  //
  function ErrSrc(Errado: Boolean; Txt: String): Boolean;
  begin
    if Errado then
    begin
      Result := True;
      ErrTxt := 'Grupo: ' + Geral.FF0(Codigo) + '  Item: ' + Geral.FF0(Controle) +
        ' > ' + Txt;
      //
      if not InMultiExec then
        MyObjects.FIC(True, nil, ErrTxt);
    end else
      Result := False;
  end;
var
  //EmitGru, CliOrig, Formula, PedGrandeza: Integer;
  //RendEsperado,
  AreaPeso, AreaM2, Peso: Double;
begin
  Result := False;
  ErrTxt := '';
{
  EmitGru := FmPQImp.QrPQUCabEmitGru.Value;
  CliOrig := FmPQImp.QrPQUCabCliInt.Value;
  Formula  := EdReceita2.ValueVariant;
  RendEsperado := EdRendEsperado.ValueVariant;
  PedGrandeza := RGPedGrandeza.ItemIndex;
  LinhasCul := EdPedLinhasCul.ValueVariant;
  LinhasCab := EdPedLinhasCab.ValueVariant;
}
  //
  if ErrSrc(CliOrig = 0, 'Cliente interno n�o definido no grupo!') then Exit;
  if ErrSrc(EmitGru = 0, 'Grupo de Emis�o n�o definido no grupo!') then Exit;
  if ErrSrc(Formula = 0, 'Defina a receita!') then Exit;
  if ErrSrc(PedGrandeza = 0, 'Defina a grandeza!') then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAreas, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _emitgru_produzido_;',
  'CREATE TABLE _emitgru_produzido_',
  'SELECT emi.Peso, 110.000 Tipo,  ',
  'Codigo, CAST(DataEmis + 0 AS DATETIME) Data, "Pesagem" TipoTXT,  ',
  'VSMovCod, AreaM2, Qtde Pecas, 999999999.99 AreaPeso, ',
  '999999999.99 ArM2ComRend, 999999999.99 ArP2ComRend, 999999999.99 PesoComRend  ',
  'FROM ' + TMeuDB + '.emit emi ',
  '/*LEFT JOIN ' + TMeuDB + '.pqx pqx ON emi.Codigo=pqx.OrigemCodi */',
  'WHERE EmitGru=' + Geral.FF0(EmitGru),
  //'AND pqx.CliOrig=' + Geral.FF0(CliOrig),
  'AND emi.Numero=' + Geral.FF0(Formula),
  ';',
  'SELECT * FROM _emitgru_produzido_',
  '']);
  //if (EmitGru = 1039) and (Formula = 821) then
    //Geral.MB_Info(Dmod.QrAreas.Text);
  Dmod.QrAreas.First;
  while not Dmod.QrAreas.Eof do
  begin
    AreaPeso := CalculaAreaPeso(Dmod.QrAreasPeso.Value);
    AreaM2   := Dmod.QrAreasAreaM2.Value;
    //
    if AreaPeso > AreaM2 then
      ArM2ComRend := AreaPeso
    else
      ArM2ComRend := AreaM2;
    //
    ArM2ComRend := ArM2ComRend * (1 + (RendEsperado / 100));
    ArP2ComRend := Geral.ConverteArea(ArM2ComRend, ctM2toP2, cfQuarto);
    PesoComRend := Dmod.QrAreasPeso.Value * (1 + (RendEsperado / 100));
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_emitgru_produzido_', False, [
    'AreaPeso', 'ArM2ComRend', 'ArP2ComRend',
    'PesoComRend'], [
    'Codigo'], [
    AreaPeso, ArM2ComRend, ArP2ComRend,
    PesoComRend], [
    Dmod.QrAreasCodigo.Value], False);
    //
    Dmod.QrAreas.Next;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAreas, DModG.MyPID_DB, [
  'SELECT * FROM _emitgru_produzido_',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrSumAreas, DModG.MyPID_DB, [
  'SELECT SUM(PesoComRend) PesoComRend, ',
  'SUM(ArM2ComRend) ArM2ComRend,',
  'SUM(ArP2ComRend) ArP2ComRend',
  'FROM _emitgru_produzido_',
  '']);
  //
  ArM2ComRend := Dmod.QrSumAreasArM2ComRend.Value;
  ArP2ComRend := Dmod.QrSumAreasArP2ComRend.Value;
  PesoComRend := Dmod.QrSumAreasPesoComRend.Value;
  //
  Result := True;
(*
  case PedGrandeza of
    1: EdPedQtPrGdz.ValueVariant := ArM2ComRend;
    2: EdPedQtPrGdz.ValueVariant := ArP2ComRend;
    3: EdPedQtPrGdz.ValueVariant := PesoComRend;
    else Result := False;
  end;
*)
  case PedGrandeza of
    1: PedQtPrGdz := ArM2ComRend;
    2: PedQtPrGdz := ArP2ComRend;
    3: PedQtPrGdz := PesoComRend;
    else Result := False;
  end;
end;

procedure TUnPQ_PF.CorrigePQGGXNiv2;
var
  Nivel1, Nivel2: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT * ',
      'FROM pq ',
      'WHERE GGXNiv2 = 0 ',
      '']);
(*
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Causou erro grave no Noroeste em 2023-10-24
      Mudou GGXNiv2 de todos cadastros para 1
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE pq SET GGXNiv2 = Ativo; ',
        'UPDATE pq SET Ativo = 1 WHERE Ativo <> 0; ',
        '']);
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT its.Codigo, its.GGXNiv2, gg1.Nivel2 ',
      'FROM pq its ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1 = its.Codigo ',
      'WHERE (its.GGXNiv2 * -1) <> ',
      '( ',
      'SELECT Nivel2 ',
      'FROM gragru1 ',
      'WHERE Nivel1 = its.Codigo ',
      ') ',
      '']);

    if Dmod.QrAux.RecordCount > 0 then
    begin
      while not Dmod.QrAux.Eof do
      begin
        Nivel1 := Dmod.QrAux.FieldByName('Codigo').AsInteger;
        Nivel2 := Dmod.QrAux.FieldByName('GGXNiv2').AsInteger * -1;
        //
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False,
          ['Nivel2'], ['Nivel1'], [Nivel2], [Nivel1], True) then
        begin
          Geral.MB_Erro('Falha ao atualizar produtos!');
          Exit;
        end;
        //
        Dmod.QrAux.Next;
      end;
    end;
    //
*)
    if Dmod.QrAux.RecordCount > 0 then
    begin
      if DBCheck.CriaFm(TFmPQCorrigeGGXNiv2, FmPQCorrigeGGXNiv2, afmoNegarComAviso) then
      begin
        if FmPQCorrigeGGXNiv2.ReopenPQ then
          FmPQCorrigeGGXNiv2.ShowModal;
        FmPQCorrigeGGXNiv2.Destroy;
      end;
(*
      UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
        'SELECT * ',
        'FROM pq ',
        'WHERE GGXNiv2 = 0 ',
        '']);
*)
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnPQ_PF.DefineContasContabeisEntradaInsumosPQE_UmItem(TypCtbCadMoF:
  TTypCtbCadMoF; const PQE: Integer; var GenCtbD, GenCtbC: Integer;
  var Descricao: String);
  //
var
  CtbPlaCta, CtbCadGru: Integer;
begin
  GenCtbD := 0;
  GenCtbC := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrGraCtbs1, Dmod.MyDB, [
  'SELECT  ',
  'gg1.CtbPlaCta gg1_CtbPlaCta,  ',
  'gg1.CtbCadGru gg1_CtbCadGru, ',
  ' ',
  'gg2.CtbPlaCta gg2_CtbPlaCta,  ',
  'gg2.CtbCadGru gg2_CtbCadGru, ',
  ' ',
  'pgt.CtbPlaCta pgt_CtbPlaCta,  ',
  'pgt.CtbCadGru pgt_CtbCadGru,  ',
  ' ',
  'IF(gg2.Nome <> "", gg2.Nome, pgt.Nome) NO_GG2, pgt.Nome NO_PGT ',
  ' ',
  'FROM pqeits pqi ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=pqi.Insumo ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2 ',
  'LEFT JOIN prdGrupTip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'WHERE pqi.Codigo=' + Geral.FF0(PQE),
  '']);
  //
  CtbCadGru := DModFin.QrGraCtbs1gg1_CtbCadGru.Value;
  if CtbCadGru <> 0 then
  begin
    if Contabil_PF.DefineGenCtbsPeloGrupo(TypCtbCadMoF, CtbCadGru, GenCtbD,
    GenCtbC) then
    begin
      Descricao := DModFin.QrGraCtbs1NO_GG2.Value;
      Exit;
    end;
  end;
  CtbPlaCta := DModFin.QrGraCtbs1gg1_CtbPlaCta.Value;
  if CtbPlaCta <> 0 then
  begin
    Descricao := DModFin.QrGraCtbs1NO_GG2.Value;
    GenCtbD := CtbPlaCta;
    Exit;
  end;
  //
  CtbCadGru := DModFin.QrGraCtbs1gg2_CtbCadGru.Value;
  if CtbCadGru <> 0 then
  begin
    if Contabil_PF.DefineGenCtbsPeloGrupo(TypCtbCadMoF, CtbCadGru, GenCtbD,
    GenCtbC) then
    begin
      Descricao := DModFin.QrGraCtbs1NO_GG2.Value;
      Exit;
    end;
  end;
  CtbPlaCta := DModFin.QrGraCtbs1gg2_CtbPlaCta.Value;
  if CtbPlaCta <> 0 then
  begin
    GenCtbD := CtbPlaCta;
    Descricao := DModFin.QrGraCtbs1NO_GG2.Value;
    Exit;
  end;
  //
  CtbCadGru := DModFin.QrGraCtbs1pgt_CtbCadGru.Value;
  if CtbCadGru <> 0 then
  begin
    if Contabil_PF.DefineGenCtbsPeloGrupo(TypCtbCadMoF, CtbCadGru, GenCtbD,
    GenCtbC) then
    begin
      Descricao := DModFin.QrGraCtbs1NO_PGT.Value;
      Exit;
    end;
  end;
  CtbPlaCta := DModFin.QrGraCtbs1pgt_CtbPlaCta.Value;
  if CtbPlaCta <> 0 then
  begin
    GenCtbD := CtbPlaCta;
    Descricao := DModFin.QrGraCtbs1NO_PGT.Value;
    Exit;
  end;
end;

procedure TUnPQ_PF.DefineContasContabeisEntradaInsumosPQE_VariosItens(
  TypCtbCadMoF: TTypCtbCadMoF; const PQE: Integer; var GenCtbD,
  GenCtbC: Integer; var Descricao: String);
  //
var
  CtbPlaCta, CtbCadGru: Integer;
begin
  GenCtbD := 0;
  GenCtbC := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrGraCtbsN, Dmod.MyDB, [
  'SELECT SUM(pqi.ValorItem) SumValor,  ',
  '  ',
  'gg2.CtbPlaCta gg2_CtbPlaCta,   ',
  'gg2.CtbCadGru gg2_CtbCadGru,  ',
  '  ',
  'pgt.CtbPlaCta pgt_CtbPlaCta,   ',
  'pgt.CtbCadGru pgt_CtbCadGru,  ',
  ' ',
  'IF(gg2.Nome <> "", gg2.Nome, pgt.Nome) NO_GG2, pgt.Nome NO_PGT ',
  ' ',
  'FROM pqeits pqi ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=pqi.Insumo ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2 ',
  'LEFT JOIN prdGrupTip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'WHERE pqi.Codigo=' + Geral.FF0(PQE),
  'GROUP BY gg1.Nivel2 ',
  'ORDER BY SumValor DESC ',
  '']);
  //
  CtbCadGru := DModFin.QrGraCtbsNgg2_CtbCadGru.Value;
  if CtbCadGru <> 0 then
  begin
    if Contabil_PF.DefineGenCtbsPeloGrupo(TypCtbCadMoF, CtbCadGru, GenCtbD,
    GenCtbC) then
    begin
      Descricao := DModFin.QrGraCtbsNNO_GG2.Value;
      Exit;
    end;
  end;
  CtbPlaCta := DModFin.QrGraCtbsNgg2_CtbPlaCta.Value;
  if CtbPlaCta <> 0 then
  begin
    Descricao := DModFin.QrGraCtbsNNO_GG2.Value;
    GenCtbD := CtbPlaCta;
    Exit;
  end;
  //
  CtbCadGru := DModFin.QrGraCtbsNpgt_CtbCadGru.Value;
  if CtbCadGru <> 0 then
  begin
    if Contabil_PF.DefineGenCtbsPeloGrupo(TypCtbCadMoF, CtbCadGru, GenCtbD,
    GenCtbC) then
    begin
      Descricao := DModFin.QrGraCtbsNNO_PGT.Value;
      Exit;
    end;
  end;
  CtbPlaCta := DModFin.QrGraCtbsNpgt_CtbPlaCta.Value;
  if CtbPlaCta <> 0 then
  begin
    GenCtbD := CtbPlaCta;
    Descricao := DModFin.QrGraCtbsNNO_PGT.Value;
    Exit;
  end;
end;

function TUnPQ_PF.DefineDtCorrApoTxt(CkDtCorrApo: TCheckBox;
  TPDtCorrApo: TdmkEditDateTimePicker): String;
var
  DataCA: TDateTime;
begin
  if CkDtCorrApo.Checked then
    DataCA := Int(TPDtCorrApo.Date)
  else
    DataCA := EncodeDate(1899, 12, 30);
  //
  Result := Geral.FDT(DataCA, 1);
end;

function TUnPQ_PF.DefineVariaveisPesqusaEstoquePQ(const EdPQ, EdCIDst, EdCIOri:
  TdmkEditCB; const RGOrdem1, RGOrdem2, RGOrdem3: TRadioGroup; CGTipoCad:
  TdmkCheckGroup; var FCIDst,
  FCIOri, FPQ: Integer; var FSE, FTC: String): Boolean;
const
  Setores: array[0..3] of String = ('NOMECI', 'NOMESE', 'NOMEPQ', 'NOMEFO');
var
  I, K: Integer;
begin
  FCIDst := EdCIOri.ValueVariant;
  FCIOri := EdCIDst.ValueVariant;
  FPQ    := EdPQ.ValueVariant;
  FSE := 'ORDER BY ' +
         Setores[RGOrdem1.ItemIndex] + ', ' +
         Setores[RGOrdem2.ItemIndex] + ', ' +
         Setores[RGOrdem3.ItemIndex];
  //FDI := TPIni.Date;
  //FDF := TPFim.Date;
  FTC := '';
  //F009_Individual := Ck009_Individual.Checked;
  //
  for I := 0 to CGTipoCad.Items.Count -1 do
  begin
    K := CGTipoCad.GetIndexOfCheckedStatus(I);
    if K > -1 then
      FTC := FTC + ',' + FormatFloat('0', K);
  end;
  if FTC <> '' then
    FTC := Copy(FTC, 2);
end;

function TUnPQ_PF.ExcluiPesagem(Emit, Tipo: Integer; PB: TprogressBar): Boolean;
var
  N, VSMovCod: Integer;
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    N := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT VSMovCod ',
    'FROM emit ',
    'WHERE Codigo=' + Geral.FF0(Emit),
    '']);
    VSMovCod := Qry.FieldByName('VSMovCod').AsInteger;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM pqx ',
    'WHERE OrigemCodi=' + Geral.FF0(Emit),
    'AND pqx.Tipo=' + Geral.FF0(Tipo),
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if Dmod.ImpedePeloRetornoPQ(
      Qry.FieldByName('OrigemCodi').AsInteger,
      Qry.FieldByName('OrigemCtrl').AsInteger,
      Qry.FieldByName('Tipo').AsInteger, False) then
        N := N + 1;
      //
      Qry.Next;
    end;
    if N > 0 then
    begin
      Geral.MB_Aviso(Geral.FF0(N) +
      ' itens da baixa impedem a exclus�o de toda receita porque j� possuem retorno!');
      Exit;
    end;
    if Emit <> 0 then
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o de TODA pesagem?' + sLineBreak +
      'Obs.: A receita espelho ser� mantida!') = ID_YES then
      begin
        PB.Visible := True;
        try
          //if Pertence then Exit;
          PB.Position := 0;
          PB.Max := Qry.RecordCount + 5;
          //
          PB.Position := PB.Position + 1;
          PB.Update;
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'DELETE FROM wbmovits ',
          'WHERE MovimID =' + Geral.FF0(Integer(emidIndsWE)),
          'AND LnkNivXtr1=' + Geral.FF0(Emit),
          '']);
          //
          PB.Position := PB.Position + 1;
          PB.Update;
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM emitcus WHERE Codigo=:P0');
          Dmod.QrUpd.Params[0].AsInteger := Emit;
          Dmod.QrUpd.ExecSQL;
          //
          PB.Position := PB.Position + 1;
          PB.Update;
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM emitits WHERE Codigo=:P0');
          Dmod.QrUpd.Params[0].AsInteger := Emit;
          Dmod.QrUpd.ExecSQL;
          //
          PB.Position := PB.Position + 1;
          PB.Update;
          //
(*
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM p q x WHERE OrigemCodi=:P0 AND Tipo=110');
          Dmod.QrUpd.Params[0].AsInteger := Emit;
          Dmod.QrUpd.ExecSQL;
          //
          Qry.First;
          while not Qry.Eof do
          begin
            PB.Position := PB.Position + 1;
            PB.Update;
            Application.ProcessMessages;
            UnPQx.AtualizaEstoquePQ(
              Qry.FieldByName('CliOrig').AsInteger,
              Qry.FieldByName('Insumo').AsInteger, aeMsg, '');
            Qry.Next;
          end;
*)
          ExcluiPQx_Mul(Qry, Emit, VAR_FATID_0110, PB, True);
          //
          PB.Position := PB.Position + 1;
          PB.Update;
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM emit WHERE Codigo=:P0');
          Dmod.QrUpd.Params[0].AsInteger := Emit;
          Dmod.QrUpd.ExecSQL;
          //
          Result := True;
          //
          if VSMovCod <> 0 then
            DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(VSMovCod);
        except
          raise;
        end;
        Screen.Cursor := crDefault;
        PB.Visible := False;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnPQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo:
  Integer; Avisa: Boolean; Empresa: Integer): Boolean;
var
  Qry1, Qry2: TmySQLQuery;
  DstCodi, DstCtrl, DstTipo: Integer;
  Peso, Valor: Double;
begin
  Result := False;
  //
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry2 := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT DstCodi, DstCtrl, DstTipo ',
      'FROM pqw ',
      'WHERE OriCodi=' + Geral.FF0(OriCodi),
      'AND OriCtrl=' + Geral.FF0(OriCtrl),
      'AND OriTipo=' + Geral.FF0(OriTipo),
      '']);
      //Geral.MB_SQL(nil, Qry1);
      //
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      'DELETE FROM pqx ' +
      ' WHERE Tipo=' + Geral.FF0(OriTipo) +
      ' AND OrigemCodi=' + Geral.FF0(OriCodi) +
      ' AND OrigemCtrl=' + Geral.FF0(OriCtrl));
      //
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      'DELETE FROM pqw ' +
      ' WHERE OriCodi=' + Geral.FF0(OriCodi) +
      ' AND OriCtrl=' + Geral.FF0(OriCtrl) +
      ' AND OriTipo=' + Geral.FF0(OriTipo));
      //
      UnPQx.AtualizaEstoquePQ(CliInt, Insumo, Empresa, aeVAR, '');
      //
      Qry1.First;
      while not Qry1.Eof do
      begin
        DstCodi := Qry1.FieldByName('DstCodi').AsInteger;
        DstCtrl := Qry1.FieldByName('DstCtrl').AsInteger;
        DstTipo := Qry1.FieldByName('DstTipo').AsInteger;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
        'SELECT Peso, Valor ',
        'FROM pqx ',
        'WHERE OrigemCodi=' + Geral.FF0(DstCodi),
        'AND OrigemCtrl=' + Geral.FF0(DstCtrl),
        'AND Tipo=' + Geral.FF0(DstTipo),
        '']);
        //Geral.MB_SQL(nil, Qry2);
        Peso  := Qry2.FieldByName('Peso').AsFloat;
        Valor := Qry2.FieldByName('Valor').AsFloat;
        AtualizaSdoPQx(Qry2, DstCodi, DstCtrl, DstTipo);
        //
        Qry1.Next;
      end;
      if Avisa then
        VerificaEquiparacaoEstoque(True);
      Result := True;
    finally
      Qry2.Free;
    end;
  finally
    Qry1.Free;
  end;
end;

function TUnPQ_PF.ExcluiPQx_Mul(QrPQX: TmySQLQuery; OriCodi, OriTipo:
  Integer; PB: TProgressBar; VerificaDiferencas: Boolean): Boolean;
var
  Qry1, Qry2: TmySQLQuery;
  DstCodi, DstCtrl, DstTipo: Integer;
  Peso, Valor: Double;
begin
  Result := False;
  //
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry2 := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT DstCodi, DstCtrl, DstTipo ',
      'FROM pqw ',
      'WHERE OriCodi=' + Geral.FF0(OriCodi),
      //'AND OriCtrl=' + Geral.FF0(OriCtrl),
      'AND OriTipo=' + Geral.FF0(OriTipo),
      '']);
      //Geral.MB_SQL(nil, Qry1);
      //
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      'DELETE FROM pqx ' +
      ' WHERE Tipo=' + Geral.FF0(OriTipo) +
      ' AND OrigemCodi=' + Geral.FF0(OriCodi));
      //' AND OrigemCtrl=' + Geral.FF0(OriCtrl));
      //
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      'DELETE FROM pqw ' +
      ' WHERE OriCodi=' + Geral.FF0(OriCodi) +
      //' AND OriCtrl=' + Geral.FF0(OriCtrl) +
      ' AND OriTipo=' + Geral.FF0(OriTipo));
      //
      //UnPQx.AtualizaEstoquePQ(CliInt, Insumo, aeMsg, '');
      if (OriTipo <> VAR_FATID_0010) or (QrPQx <> nil) then
      begin
        QrPQx.First;
        while not QrPQx.Eof do
        begin
          PB.Position := PB.Position + 1;
          PB.Update;
          Application.ProcessMessages;
          UnPQx.AtualizaEstoquePQ(
            QrPQx.FieldByName('CliOrig').AsInteger,
            QrPQx.FieldByName('Insumo').AsInteger,
            QrPQx.FieldByName('Empresa').AsInteger, aeMsg, '');
          QrPQx.Next;
        end;
      end;
      //
      Qry1.First;
      while not Qry1.Eof do
      begin
        DstCodi := Qry1.FieldByName('DstCodi').AsInteger;
        DstCtrl := Qry1.FieldByName('DstCtrl').AsInteger;
        DstTipo := Qry1.FieldByName('DstTipo').AsInteger;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
        'SELECT Peso, Valor ',
        'FROM pqx ',
        'WHERE OrigemCodi=' + Geral.FF0(DstCodi),
        'AND OrigemCtrl=' + Geral.FF0(DstCtrl),
        'AND Tipo=' + Geral.FF0(DstTipo),
        '']);
        //Geral.MB_SQL(nil, Qry2);
        Peso  := Qry2.FieldByName('Peso').AsFloat;
        Valor := Qry2.FieldByName('Valor').AsFloat;
        AtualizaSdoPQx(Qry2, DstCodi, DstCtrl, DstTipo);
        //
        Qry1.Next;
      end;
      if VerificaDiferencas then
        VerificaEquiparacaoEstoque(True);
      Result := True;
    finally
      Qry2.Free;
    end;
  finally
    Qry1.Free;
  end;
end;

procedure TUnPQ_PF.ExcluiReceita(Numero: Integer);
begin
  if Numero = 0 then
    Exit;
  //
  if UMyMod.ExcluiRegistro_EnviaArquivoMorto('tintazflu', 'tintasflu',
    dmkPF.MotivDel_ValidaCodigo(997), ['Numero'], [Numero],
    Dmod.MyDB) then
  begin
    UMyMod.ExcluiRegistroInt1('', 'tintasflu', 'Numero', Numero, Dmod.MyDB);
  end;
  //
  if UMyMod.ExcluiRegistro_EnviaArquivoMorto('tintazits', 'tintasits',
    dmkPF.MotivDel_ValidaCodigo(997), ['Numero'], [Numero],
    Dmod.MyDB) then
  begin
    UMyMod.ExcluiRegistroInt1('', 'tintasits', 'Numero', Numero, Dmod.MyDB);
  end;
  //
  if UMyMod.ExcluiRegistro_EnviaArquivoMorto('tintaztin', 'tintastin',
    dmkPF.MotivDel_ValidaCodigo(997), ['Numero'], [Numero],
    Dmod.MyDB) then
  begin
    UMyMod.ExcluiRegistroInt1('', 'tintastin', 'Numero', Numero, Dmod.MyDB);
  end;
  //
  if UMyMod.ExcluiRegistro_EnviaArquivoMorto('tintazcab', 'tintascab',
    dmkPF.MotivDel_ValidaCodigo(997), ['Numero'], [Numero],
    Dmod.MyDB) then
  begin
    UMyMod.ExcluiRegistroInt1('', 'tintascab', 'Numero', Numero, Dmod.MyDB);
  end;
end;

function TUnPQ_PF.ImpedePeloEstoqueNF_Item(OrigemCodi, OrigemCtrl, OrigemTipo:
  Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Peso, Valor  ',
    'FROM pqx ',
    'WHERE OrigemCodi=' + Geral.FF0(OrigemCodi),
    'AND OrigemCtrl=' + Geral.FF0(OrigemCtrl),
    'AND Tipo=' + Geral.FF0(OrigemTipo),
    'AND (Peso <> SdoPeso)',
    ' ']);
    Result := Qry.RecordCount > 0;
    if Result then
    begin
      Geral.MB_Aviso(
      'Este item entrada n�o pode mais ser editada ou exclu�do pois tem j� tem baixa!');
      Exit;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnPQ_PF.ImpedePeloEstoqueNF_Toda(OrigemCodi, OrigemTipo:
  Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Peso, Valor  ',
    'FROM pqx ',
    'WHERE OrigemCodi=' + Geral.FF0(OrigemCodi),
    'AND Tipo=' + Geral.FF0(OrigemTipo),
    'AND (Peso <> SdoPeso)',
    ' ']);
    Result := Qry.RecordCount > 0;
    if Result then
    begin
      Geral.MB_Aviso(
      'Esta entrada n�o pode mais ser editada pois tem item com estoque por NF j� baixado!');
      Exit;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnPQ_PF.InserePQx_Bxa(DataX: String; CliOrig, CliDest, Insumo: Integer;
  Peso, Valor: Double; OrigemCodi, OrigemCtrl, Tipo: Integer; Unitario:
  Boolean; DtCorrApo: String; _Empresa: Integer; HowLoad: Integer): Boolean;
var
  Empresa: Integer;
begin
  Empresa := _Empresa;
  if Empresa = 0 then
  begin
    Empresa := -11;
    Geral.MB_Erro('Empresa n�o definida! Ser� considerado -11!');
  end;
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_INS_TAB_PQX, False, [
  'DataX', 'CliOrig', 'CliDest',
  'Insumo', 'Peso', 'Valor'(*,
  'Retorno', 'StqMovIts'*), 'DtCorrApo',
  'Empresa', 'HowLoad'], [
  'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
  DataX, CliOrig, CliDest,
  Insumo, Peso, Valor(*,
  Retorno, StqMovIts*), DtCorrApo,
  Empresa, HowLoad], [
  OrigemCodi, OrigemCtrl, Tipo], False);
  //
  UnPQx.AtualizaEstoquePQ(CliOrig, Insumo, Empresa, aeMsg, CO_VAZIO);
  // ini 2023-10-18
  //BaixaPQx(OrigemCodi, OrigemCtrl, Tipo, CliOrig, Insumo, -Peso);
  BaixaPQx(OrigemCodi, OrigemCtrl, Tipo, CliOrig, Empresa, Insumo,
  -Peso, -Valor, DataX);
  // fim 2023-10-18
  if Unitario then
    PQ_PF.VerificaEquiparacaoEstoque(True);
end;

function TUnPQ_PF.InserePQx_Inn(Query: TmySQLQuery; DataX: String; CliOrig,
  CliDest, Insumo: Integer; Peso, Valor: Double; HowLoad, OrigemCodi,
  OrigemCtrl, Tipo: Integer; DtCorrApo: String; _Empresa: Integer; serie, NF:
  Integer; xLote, dFab, dVal: String): Boolean;
var
  Empresa: Integer;
begin
  Result  := False;
  Empresa := _Empresa;
  if Empresa = 0 then
  begin
    Empresa := -11;
    Geral.MB_Erro('Empresa n�o definida! Ser� considerado -11!');
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_INS_TAB_PQX, False, [
  'DataX', 'CliOrig', 'CliDest',
  'Insumo', 'Peso', 'Valor',(*,
  'Retorno', 'StqMovIts', 'RetQtd',*)
  'HowLoad' (*'StqCenLoc', 'SdoPeso',
  'SdoValr', 'AcePeso', 'AceValr'*),
  'DtCorrApo', 'Empresa',
  'serie', 'NF',
  'xLote'(*, 'dFab', 'dVal'*)], [
  'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
  DataX, CliOrig, CliDest,
  Insumo, Peso, Valor,(*,
  Retorno, StqMovIts, RetQtd,*)
  HowLoad (*StqCenLoc, SdoPeso,
  SdoValr, AcePeso, AceValr*),
  DtCorrApo, Empresa,
  serie, NF,
  xLote(*, dFab, dVal*)], [
  OrigemCodi, OrigemCtrl, Tipo], False) then
  begin
    AtualizaSdoPQx(Query, OrigemCodi, OrigemCtrl, Tipo);
    Result := True;
  end;
end;

function TUnPQ_PF.InsumosSemCadastroParaClienteInterno(CliInt, Empresa,
  InsumoEspecifico, FormulaEspecifica: Integer; Tabela: TTabPqNaoCad;
  MostraJanela: Boolean = True): Boolean;
var
  Qry: TmySQLQuery;
  Ds: TDataSource;
  //I, Numero: Integer;
  SQL_FormulaEspecifica: String;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    case Tabela of
      //TTabPqNaoCad.tpncND:
      TTabPqNaoCad.tpncPQEIts:
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT pq.Codigo, pq.Nome, pqc.* ',
        'FROM pq pq ',
        'LEFT JOIN pqcli pqc ON pqc.PQ=pq.Codigo',
        '  AND pqc.CI=' + Geral.FF0(CliInt),
        '  AND pqc.Empresa=' + Geral.FF0(Empresa),
        'WHERE pq.Codigo=' + Geral.FF0(InsumoEspecifico),
        'AND pqc.Controle IS NULL',
        '']);
      TTabPqNaoCad.tpncPesagem:
      begin
        if FormulaEspecifica <> 0 then
        SQL_FormulaEspecifica := 'AND frm.Numero=' + Geral.FF0(FormulaEspecifica);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT pq.Codigo, pq.Nome, fis.Numero Receita,  ',
        'fis.Ordem Linha, fis.Controle Item  ',
        'FROM formulasits fis  ',
        'LEFT JOIN formulas frm ON frm.Numero=fis.Numero ',
        'LEFT JOIN pq pq ON pq.Codigo=fis.Produto  ',
        'LEFT JOIN pqcli pqc ON pqc.PQ=fis.Produto  ',
        '  AND pqc.CI=' + Geral.FF0(CliInt),
        '  AND pqc.Empresa=' + Geral.FF0(Empresa),
        'WHERE fis.Produto>0  ',
        'AND fis.Porcent>0  ',
        'AND pqc.Controle IS NULL  ',
        // ini 2023/10/24
        //'AND pq.GGXNiv2 IN (1,2,4,5) ',
        'AND pq.GGXNiv2 IN (1,2,4,5,7,10) ',
        // fim 2023/10/24
        SQL_FormulaEspecifica,
        '']);
      end;
      else
        Geral.MB_Erro(
        'Tabela n�o implementada em "PQ_PF.InsumosSemCadastroParaClienteInterno()"');
    end;
    //
    Result := Qry.RecordCount > 0;
    //
    if (Result) and (MostraJanela) then
    begin
      Geral.MB_Aviso(
        'Existem produtos n�o cadastrados para o Cliente Interno na Empresa selecionada!' +
        sLineBreak + Qry.SQL.Text);
      //
      Ds         := TDataSource.Create(Dmod);
      Ds.DataSet := Qry;
      //
      Application.CreateForm(TFmGerlShowGrid, FmGerlShowGrid);
      FmGerlShowGrid.MeAvisos.Lines.Add('Os produtos abaixo n�o est�o cadastrados: ');
      FmGerlShowGrid.MeAvisos.Lines.Add('Cliente interno: ' + Geral.FF0(CliInt));
      FmGerlShowGrid.MeAvisos.Lines.Add('Empresa: ' + Geral.FF0(Empresa));
      FmGerlShowGrid.DBGSel.DataSource := Ds;
      FmGerlShowGrid.ShowModal;
      FmGerlShowGrid.Destroy;
    end;
  finally
    Ds.Free;
  end;
end;

procedure TUnPQ_PF.MostraFormFormulasImpBH();
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmFormulas2Imp, FmFormulas2Imp, afmoNegarComAviso) then
      begin
        FmFormulas2Imp.FEmit := UMyMod.BuscaEmLivreY(
          Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
        FmFormulas2Imp.ImgTipo.SQLType        := stIns;
        FmFormulas2Imp.PainelEscolhe.Visible := True;
        FmFormulas2Imp.FNomeForm             := 'Impress�o de Receita com Baixa';
        FmFormulas2Imp.EdReceita.Text        := '';
        FmFormulas2Imp.CBReceita.KeyValue    := NULL;
        //FmFormulas2Imp.PainelDefine.Visible  := True;
        FmFormulas2Imp.LaData.Visible        := True;
        FmFormulas2Imp.TPDataP.Visible       := True;
        FmFormulas2Imp.ShowModal;
        FmFormulas2Imp.Destroy;
      end;
    fggxPQ:
}
      if DBCheck.CriaFm(TFmFormulasImpBH, FmFormulasImpBH, afmoNegarComAviso) then
      begin
        (*FmFormulasImpBH.FEmit := UMyMod.BuscaEmLivreY(
          Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
        *)
        FmFormulasImpBH.FEmit := UMyMod.BuscaEmLivreY_Def('Emit', 'Codigo', stIns, 0);
        FmFormulasImpBH.PainelEscolhe.Visible := True;
        FmFormulasImpBH.FNomeForm             := 'Impress�o de Receita com Baixa';
        FmFormulasImpBH.EdReceita.Text        := '';
        FmFormulasImpBH.CBReceita.KeyValue    := NULL;
        //FmFormulasImpBH.PainelDefine.Visible  := True;
        FmFormulasImpBH.LaData.Visible        := True;
        FmFormulasImpBH.TPDataP.Visible       := True;
        FmFormulasImpBH.ShowModal;
        FmFormulasImpBH.Destroy;
      end;
{
  end;
}
end;

procedure TUnPQ_PF.MostraFormFormulasImpFI();
begin
{
////////////////////////////////////////////////////////////////////////////////
//Desativado em 2013-09-18. Ativar e atualizar quando usar receitas com gragrux!
//    function EhReceita_GGX(): TFormulaGGX;
////////////////////////////////////////////////////////////////////////////////
  case Dmod.EhReceita_GGX() of
    fggxGGX:
      if DBCheck.CriaFm(TFmTintasImp2, FmTintasImp2, afmoNegarComAviso) then
      begin
        FmTintasImp2.FEmit := UMyMod.BuscaEmLivreY(
          Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
        FmTintasImp2.ImgTipo.SQLType        := stIns;
        (*
        FmTintasImp2.PainelEscolhe.Visible := True;
        FmTintasImp2.FNomeForm             := 'Impress�o de Receita com Baixa';
        FmTintasImp2.EdReceita.Text        := '';
        FmTintasImp2.CBReceita.KeyValue    := NULL;
        FmTintasImp2.LaData.Visible        := True;
        FmTintasImp2.TPDataP.Visible       := True;
        *)
        FmTintasImp2.ShowModal;
        FmTintasImp2.Destroy;
      end;
    fggxPQ:
}
      if DBCheck.CriaFm(TFmFormulasImpFI, FmFormulasImpFI, afmoNegarComAviso) then
      begin
        FmFormulasImpFI.DefineBaixa(True);
        FmFormulasImpFI.ShowModal;
        FmFormulasImpFI.Destroy;
      end;
{
  end;
}
end;

procedure TUnPQ_PF.MostraFormFormulasImpWE();
begin
  if DBCheck.CriaFm(TFmFormulasImpWE2, FmFormulasImpWE2, afmoNegarComAviso) then
  begin
    FmFormulasImpWE2.FEmit := UMyMod.BuscaEmLivreY(
      Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
    FmFormulasImpWE2.PainelEscolhe.Visible := True;
    FmFormulasImpWE2.FNomeForm             := 'Impress�o de Receita com Baixa';
    FmFormulasImpWE2.EdReceita.Text        := '';
    FmFormulasImpWE2.CBReceita.KeyValue    := NULL;
    //FmFormulasImpWE2.PainelDefine.Visible := True;
    FmFormulasImpWE2.LaData.Visible        := True;
    FmFormulasImpWE2.TPDataP.Visible       := True;
    FmFormulasImpWE2.ShowModal;
    FmFormulasImpWE2.Destroy;
  end;
end;

procedure TUnPQ_PF.MostraFormPQE(Codigo: Integer);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if DBCheck.CriaFm(TFmPQE, FmPQE, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmPQE.LocCod(Codigo, Codigo);
      try
        if FmPQE.QrPQECodigo.Value <> Codigo then
        begin
          Geral.MB_Erro('N�o foi poss�vel localizar o C�digo ' +
            Geral.FF0(Codigo));
        end;
      except
        on E: Exception do
          Geral.MB_Erro('N�o foi poss�vel localizar o C�digo ' +
            Geral.FF0(Codigo) + sLineBreak + E.Message);
      end;
    end;
    FmPQE.ShowModal;
    FmPQE.Destroy;
  end;
end;

procedure TUnPQ_PF.MostraFormPQImp();
begin
  if DBCheck.CriaFm(TFmPQImp, FmPQImp, afmoNegarComAviso) then
  begin
    FmPQImp.ShowModal;
    FmPQImp.Destroy;
  end;
end;

procedure TUnPQ_PF.MostraFormUsoConsImp(FatID: Integer);
begin
  if DBCheck.CriaFm(TFmUsoConsImp, FmUsoConsImp, afmoNegarComAviso) then
  begin
    FmUsoConsImp.FSet_FATID := FatID;
    FmUsoConsImp.ShowModal;
    FmUsoConsImp.Destroy;
  end;
end;

procedure TUnPQ_PF.MostraFormVSFormulasImp_BH(MovimCod, IMEI, TemIMEIMrt:
  Integer; MovimInn, MovimSrc: TEstqMovimNiv; ReceitaRibSetor: TReceitaRibSetor);
begin
  if DBCheck.CriaFm(TFmVSFormulasImp_BH, FmVSFormulasImp_BH, afmoNegarComAviso) then
  begin
    FmVSFormulasImp_BH.FEmit := UMyMod.BuscaEmLivreY_Def('Emit', 'Codigo', stIns, 0);
    FmVSFormulasImp_BH.FMovimInn             := MovimInn;
    FmVSFormulasImp_BH.FMovimSrc             := MovimSrc;
    FmVSFormulasImp_BH.FMovimCod             := MovimCod;
    FmVSFormulasImp_BH.FIMEI                 := IMEI;
    FmVSFormulasImp_BH.FTemIMEIMrt           := TemIMEIMrt;
    FmVSFormulasImp_BH.PainelEscolhe.Visible := True;
    FmVSFormulasImp_BH.FNomeForm             := 'Receita de Caleiro';
    FmVSFormulasImp_BH.EdReceita.Text        := '';
    FmVSFormulasImp_BH.CBReceita.KeyValue    := NULL;
    //FmVSFormulasImp_BH.PainelDefine.Visible  := True;
    FmVSFormulasImp_BH.LaData.Visible        := True;
    FmVSFormulasImp_BH.TPDataP.Visible       := True;
    FmVSFormulasImp_BH.FReceitaRibSetor      := ReceitaRibSetor;
    //
    FmVSFormulasImp_BH.ReopenVMIAtu();
    FmVSFormulasImp_BH.ReopenReceitas();
    //
    FmVSFormulasImp_BH.ShowModal;
    FmVSFormulasImp_BH.Destroy;
  end;
end;

procedure TUnPQ_PF.MostraFormVSFormulasImp_FI(MovimCod, IMEI, TemIMEIMrt,
  EmitGru: Integer; MovimInn, MovimSrc: TEstqMovimNiv);
begin
  if DBCheck.CriaFm(TFmVSFormulasImp_FI, FmVSFormulasImp_FI, afmoNegarComAviso) then
  begin
    FmVSFormulasImp_FI.FMovimInn              := MovimInn;
    FmVSFormulasImp_FI.FMovimSrc              := MovimSrc;
    FmVSFormulasImp_FI.FMovimCod              := MovimCod;
    FmVSFormulasImp_FI.FIMEI                  := IMEI;
    FmVSFormulasImp_FI.FTemIMEIMrt            := TemIMEIMrt;
    FmVSFormulasImp_FI.ReopenVMIAtu();
    FmVSFormulasImp_FI.EdEmitGru.ValueVariant := EmitGru;
    FmVSFormulasImp_FI.CBEmitGru.KeyValue     := EmitGru;
    FmVSFormulasImp_FI.DefineBaixa(True);
    FmVSFormulasImp_FI.ShowModal;
    FmVSFormulasImp_FI.Destroy;
  end;
end;

procedure TUnPQ_PF.MostraFormVSFormulasImp_WE(MovimCod, IMEI, TemIMEIMrt,
  EmitGru: Integer; MovimInn, MovimSrc: TEstqMovimNiv);
begin
  if DBCheck.CriaFm(TFmVSFormulasImp_WE, FmVSFormulasImp_WE, afmoNegarComAviso) then
  begin
    FmVSFormulasImp_WE.FEmit := UMyMod.BuscaEmLivreY(
      Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
    FmVSFormulasImp_WE.FMovimInn              := MovimInn;
    FmVSFormulasImp_WE.FMovimSrc              := MovimSrc;
    FmVSFormulasImp_WE.FMovimCod              := MovimCod;
    FmVSFormulasImp_WE.FIMEI                  := IMEI;
    FmVSFormulasImp_WE.FTemIMEIMrt            := TemIMEIMrt;
    FmVSFormulasImp_WE.PainelEscolhe.Visible  := True;
    FmVSFormulasImp_WE.FNomeForm              := 'Impress�o de Receita com Baixa';
    FmVSFormulasImp_WE.EdReceita.Text         := '';
    FmVSFormulasImp_WE.CBReceita.KeyValue     := NULL;
    //FmVSFormulasImp_WE.PainelDefine.Visible := True;
    FmVSFormulasImp_WE.LaData.Visible         := True;
    FmVSFormulasImp_WE.TPDataP.Visible        := True;
    FmVSFormulasImp_WE.ReopenVMIAtu();
    FmVSFormulasImp_WE.EdEmitGru.ValueVariant := EmitGru;
    FmVSFormulasImp_WE.CBEmitGru.KeyValue     := EmitGru;
    //
    FmVSFormulasImp_WE.ShowModal;
    FmVSFormulasImp_WE.Destroy;
  end;
end;

function TUnPQ_PF.MostraFormVSPQOCab(const SQLType: TSQLType; const VSMovCod,
  EmitGru: Integer; const PrecisaEmitGru: Boolean; var Codigo: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Codigo := 0;
  if DBCheck.CriaFm(TFmVSPQOCab, FmVSPQOCab, afmoNegarComAviso) then
  begin
    FmVSPQOCab.ImgTipo.SQLType := SQLType;
    FmVSPQOCab.EdVSMOvCod.ValueVariant := VSMOvCod;
    FmVSPQOCab.FPrecisaEmitGru         := PrecisaEmitGru;
    FmVSPQOCab.EdEmitGru.ValueVariant  := EmitGru;
    FmVSPQOCab.CBEmitGru.KeyValue      := EmitGru;
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * FROM listasetores ',
      'WHERE TpReceita=2 ',
      'AND Ativo=1 ',
      'ORDER BY Codigo ',
      '']);
      if Qry.RecordCount > 0 then
      begin
        FmVSPQOCab.EdSetor.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
        FmVSPQOCab.CBSetor.KeyValue     := Qry.FieldByName('Codigo').AsInteger;
      end;
    finally
      Qry.Free;
    end;
    //
    FmVSPQOCab.ShowModal;
    //
    if FmVSPQOCab.FCodigo <> 0 then
    begin
      Codigo := FmVSPQOCab.FCodigo;
      Result := True;
    end;
    FmVSPQOCab.Destroy;
  end;
end;

function TUnPQ_PF.MostraFormVSPQOIts(const SQLType: TSQLType; const Codigo:
  Integer; Data: TDateTime; var Controle: Integer): Boolean;
begin
  Result := False;
  Controle := 0;
  if DBCheck.CriaFm(TFmVSPQOIts, FmVSPQOIts, afmoNegarComAviso) then
  begin
    FmVSPQOIts.ImgTipo.SQLType        := SQLType;
    FmVSPQOIts.EdOriCodi.ValueVariant := Codigo;
    FmVSPQOIts.FDataX                 := Data;
    //
    FmVSPQOIts.ShowModal;
    if FmVSPQOIts.FControle <> 0 then
    begin
      Controle := FmVSPQOIts.FControle;
      Result := True;
    end;
    FmVSPQOIts.Destroy;
  end;
end;

function TUnPQ_PF.ObtemDadosGradeDePQ(const Insumo: Integer; var Tipo_Item,
  GraGruX, UnidMed: Integer; var Sigla, NCM: String; const Avisa: Boolean): Boolean;
const
  sProcName = 'PQ_PF.ObtemDadosGradeDePQ()';
var
  Qry: TmySQLQuery;
  Nivel1: Integer;
begin
  Nivel1 := Insumo;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
    'pgt.Tipo_Item) Tipo_Item, ggx.Controle, gg1.UnidMed, ',
    'gg1.NCM, med.Sigla ',
    'FROM gragrux ggx',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed',
    'WHERE ggx.GraGru1=' + Geral.FF0(Nivel1),
    'ORDER BY ggx.Controle ',
    '']);
    //
    if Qry.RecordCount > 0 then
    begin
      Tipo_Item := Qry.FieldByName('Tipo_Item').AsInteger;
      GraGruX   := Qry.FieldByName('Controle').AsInteger;
      UnidMed   := Qry.FieldByName('UnidMed').AsInteger;
      Sigla     := Qry.FieldByName('Sigla').AsString;
      NCM       := Qry.FieldByName('NCM').AsString;
      Result    := True;
    end else
    begin
      Tipo_Item := 0;
      GraGruX   := 0;
      UnidMed   := 0;
      Sigla     := '';
      NCM       := '';
      Result    := False;
      //
      if Avisa then
        Geral.MB_Aviso('Reduzido n�o encontrado para o insumo ' +
        Geral.FF0(Insumo) + ' em "' + sProcName + '"');
    end;
  except
    Qry.Free;
  end;
end;

function TUnPQ_PF.ObtemNomeTabelaTipoMovPQ(Tipo: Integer): String;
begin
  case Tipo of
    VAR_FATID_0010: Result := 'pqe';
    VAR_FATID_0030: Result := 'pqm'; // manuten��o
    VAR_FATID_0110: Result := 'emit'; // Pesagens de PQ
    VAR_FATID_0130: Result := 'pqm'; // manuten��o
    VAR_FATID_0150: Result := 'pqt'; // ETE
    VAR_FATID_0170: Result := 'pqd'; // Devolucao de PQ
    VAR_FATID_0180: Result := 'pqi'; // IPI
    VAR_FATID_0185: Result := 'pqn'; // Manuten��o
    VAR_FATID_0190: Result := 'pqo'; // Outros
    else Result := '*??_ObtemNomeTabelaTipoMovPQ_??*';
  end;
end;
//
function TUnPQ_PF.ObtemSetoresSelecionados(const DBGSetores: TdmkDBGridZTO;
  const QrListaSetores: TmySQLQuery; var FSetoresCod, FSetoresTxt:
  String): Boolean;
var
  N, I: Integer;
  Liga1, Liga2: String;
begin
  Result := False;
  Liga1 := '';
  Liga2 := '';
  FSetoresCod := '';
  FSetoresTxt := '';
  if MyObjects.FIC(DBGSetores.SelectedRows.Count < 1, nil,
  'Selecione pelo menos um setor!') then Exit;
  //
  QrListaSetores.DisableControls;
  try
    N := 0;
    with DBGSetores.DataSource.DataSet do
    for I := 0 to DBGSetores.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGSetores.SelectedRows.Items[I]));
      GotoBookmark(DBGSetores.SelectedRows.Items[I]);
      ///////////////
      FSetoresCod := FSetoresCod + Liga1 + Geral.FF0(
        QrListaSetores.FieldByName('Codigo').AsInteger);
      FSetoresTxt := FSetoresTxt + Liga2 +
        QrListaSetores.FieldByName('Nome').AsString;
      Liga1 := ',';
      Liga2 := ', ';
      ///////////////
    end;
    Result := True;
  finally
    QrListaSetores.EnableControls;
  end;
end;

procedure TUnPQ_PF.PQO_AtualizaCusto(Codigo: Integer);
var
  Qry: TmySQLQuery;
  CustoInsumo, CustoTotal: Double;
begin
  Qry := TmySQLQuery.Create(Dmod.MyDB);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(pqx.Valor)*-1 CUSTO ',
    'FROM pqx pqx ',
    'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
    'AND pqx.OrigemCodi=' + Geral.FF0(Codigo),
    '']);
    CustoInsumo := Qry.FieldByName('CUSTO').AsFloat;
    CustoTotal  := Qry.FieldByName('CUSTO').AsFloat;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pqo', False, [
    'CustoInsumo', 'CustoTotal'], [
    'Codigo'], [
    CustoInsumo, CustoTotal], [
    Codigo], True);
  finally
    Qry.Free;
  end;
end;

procedure TUnPQ_PF.PQO_ExcluiCab(Codigo: Integer);
var
  Qry: TmySQLQuery;
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o de TODOS ITENS desta baixa e a pr�pria baixa?') =
  ID_YES then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DISTINCT pqx.* ',
      'FROM pqx pqx ',
      'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
      'AND pqx.OrigemCodi=' + Geral.FF0(Codigo),
      '']);
      Qry.First;
      while not Qry.Eof do
      begin
        PQO_ExcluiItem(Qry.FieldByName('OrigemCtrl').AsInteger, False, False);
        Qry.Next;
      end;
      //AtualizaCusto;
      UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
      'DELETE FROM pqo ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
      //
      //LocCod(QrPQOCodigo.Value, QrPQOCodigo.Value);
    finally
      Qry.Free;
    end;
  end;
end;

procedure TUnPQ_PF.PQO_ExcluiItem(Controle: Integer; Pergunta, AtzCusto: Boolean);
var
  Atual, OriCodi, OriCtrl, OriTipo, Insumo, CliInt, Empresa: Integer;
  Qry: TmySQLQuery;
  Continua: Integer;
begin
  if Pergunta then Continua := Geral.MB_Pergunta(
    'Confirma a exclus�o do item selecionado nesta baixa?')
  else
    Continua := ID_YES;
  if Continua = ID_YES then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * FROM pqx ',
      'WHERE Tipo=' + Geral.FF0(VAR_FATID_0190),
      'AND OrigemCtrl=' + Geral.FF0(Controle),
      '']);
      Insumo  := Qry.FieldByName('Insumo').AsInteger;
      CliInt  := Qry.FieldByName('CliOrig').AsInteger;
      Empresa := Qry.FieldByName('Empresa').AsInteger;
      Atual   := Qry.FieldByName('OrigemCtrl').AsInteger;
      //
      OriCodi := Qry.FieldByName('OrigemCodi').AsInteger;
      OriCtrl := Atual;
      OriTipo := VAR_FATID_0190;
      PQ_PF.ExcluiPQx_Itm(CliInt, Insumo, OriCodi, OriCtrl, OriTipo, True, Empresa);
      //
      if AtzCusto then
        PQO_AtualizaCusto(OriCodi);
      UnPQx.AtualizaEstoquePQ(CliInt, Insumo, Empresa, aeNenhum, '');
    finally
      Qry.Free;
    end;
  end;
end;

procedure TUnPQ_PF.ReimprimePesagemSomeWet(Emit: Integer; TpReceita: TReceitaTipoSetor; PB1: TProgressBar);
const
  TemIMEIMrt = 1;
var
  QrEmit, QrVMI: TmySQLQuery;
  VSMovCod: Integer;
  MovimNiv: TEstqMovimNiv;
begin
  BDC_APRESENTACAO_IMPRESSAO := MyObjects.SelRadioGroup('Impress�o de Pesagem',
    'Apresenta��o', sListaRecImpApresenta, 6, 0);
  if BDC_APRESENTACAO_IMPRESSAO = -1 then
    Exit;
  //
  QrEmit := TmySQLQuery.Create(Dmod);
  try
  QrVMI := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
    'SELECT emi.*, gru.Nome NO_EmitGru  ',
    'FROM emit emi ',
    'LEFT JOIN emitgru gru ON gru.Codigo=emi.EmitGru ',
    'WHERE emi.Codigo=' + Geral.FF0(Emit),
    '']);
    //
    VSMovCod := QrEmit.FieldByName('VSMovCod').AsInteger;
    if VSMovCod <> 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
      'SELECT MovimID  ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(VSMovCod),
      '']);
      case TEstqMovimID(QrVMI.FieldByName('MovimID').AsInteger) of
        emidEmProcCal: MovimNiv := eminSorcCal;
        emidEmProcCur: MovimNiv := eminSorcCur;
        else
        begin
          Geral.MB_Erro('"MovimNiv" indefinido em "ReimprimePesagem()"');
          MovimNiv := eminSemNiv;
        end;
      end;
    end else
      MovimNiv := eminSemNiv;
    //
    //
    //BDC_APRESENTACAO_IMPRESSAO := RGImpRecRib.ItemIndex;
    VAR_SETOR    := '';
    VAR_SETORNUM := QrEmit.FieldByName('Setor').AsInteger;
    //
    case Dmod.QrControleVerImprRecRib.Value of
      0:
      begin
        FrFormulasImpShow.FReImprime    := True;
        FrFormulasImpShow.FEmit         := QrEmit.FieldByName('Codigo').AsInteger;
        FrFormulasImpShow.FEmitGru      := QrEmit.FieldByName('EmitGru').AsInteger;
        FrFormulasImpShow.FVSMovCod     := VSMovCod;
        FrFormulasImpShow.FVSMovimSrc   := MovimNiv;
        FrFormulasImpShow.FTemIMEIMrt   := TemIMEIMrt;
        FrFormulasImpShow.FEmitGru_TXT  := QrEmit.FieldByName('NO_EmitGru').AsString;
        FrFormulasImpShow.FRetrabalho   := QrEmit.FieldByName('Retrabalho').AsInteger;
        FrFormulasImpShow.FSourcMP      := QrEmit.FieldByName('SourcMP').AsInteger;
        FrFormulasImpShow.FDataP        := QrEmit.FieldByName('DataEmis').AsDateTime;
        //FrFormulasImpShow.FDtCorrApo    := Geral.FDT(QrEmit.FieldByName('DtCorrApo').AsDateTime, 1);
        FrFormulasImpShow.FDtCorrApo    := '????/??/??';
        FrFormulasImpShow.FEmpresa      := QrEmit.FieldByName('Empresa').AsInteger;
        FrFormulasImpShow.FHoraIni      := QrEmit.FieldByName('HoraIni').AsDateTime;
        FrFormulasImpShow.FProgressBar1 := PB1;
        FrFormulasImpShow.FTipoPreco    := -1;// n�o deve usar
        FrFormulasImpShow.FCliIntCod    := QrEmit.FieldByName('ClienteI').AsInteger;
        FrFormulasImpShow.FCliIntTxt    := QrEmit.FieldByName('NOMECI').AsString;
        FrFormulasImpShow.FFormula      := QrEmit.FieldByName('Numero').AsInteger;
        FrFormulasImpShow.FTipoImprime  := 0; //Visualiza! RGImprime.ItemIndex;
        FrFormulasImpShow.FPeso         := QrEmit.FieldByName('Peso').AsFloat;
        FrFormulasImpShow.FQtde         := QrEmit.FieldByName('Qtde').AsFloat;
        FrFormulasImpShow.FArea         := QrEmit.FieldByName('AreaM2').AsInteger;
        FrFormulasImpShow.FLinhasRebx   := QrEmit.FieldByName('Espessura').AsString;
        FrFormulasImpShow.FLinhasSemi   := QrEmit.FieldByName('SemiTxtEspReb').AsString;
        FrFormulasImpShow.FFulao        := QrEmit.FieldByName('Fulao').AsString;
        FrFormulasImpShow.FDefPeca      := QrEmit.FieldByName('DefPeca').AsString;
        FrFormulasImpShow.FHHMM_P       := dmkPF.HorasMH(QrEmit.FieldByName('TempoP').AsInteger, False);
        FrFormulasImpShow.FHHMM_R       := dmkPF.HorasMH(QrEmit.FieldByName('TempoR').AsInteger, False);
        FrFormulasImpShow.FHHMM_T       := dmkPF.HorasMH(QrEmit.FieldByName('TempoP').AsInteger + QrEmit.FieldByName('TempoR').AsInteger, False);
        FrFormulasImpShow.FObs          := QrEmit.FieldByName('Obs').AsString;
        FrFormulasImpShow.FSetor        := QrEmit.FieldByName('Setor').AsInteger;
        FrFormulasImpShow.FTipific      := QrEmit.FieldByName('Tipificacao').AsInteger;
        FrFormulasImpShow.FCod_Espess   := QrEmit.FieldByName('Cod_Espess').AsInteger;
        FrFormulasImpShow.FCodDefPeca   := QrEmit.FieldByName('CodDefPeca').AsInteger;
        FrFormulasImpShow.FSemiAreaM2   := QrEmit.FieldByName('SemiAreaM2').AsFloat;
        FrFormulasImpShow.FSemiRendim   := QrEmit.FieldByName('SemiRendim').AsInteger;
        FrFormulasImpShow.FBRL_USD      := QrEmit.FieldByName('BRL_USD').AsFloat;
        FrFormulasImpShow.FBRL_EUR      := QrEmit.FieldByName('BRL_EUR').AsFloat;
        FrFormulasImpShow.FDtaCambio    := QrEmit.FieldByName('DtaCambio').AsDateTime;
        //
        //if CkMatricial.Checked then FrFormulasImpShow.FMatricialNovo := True
        //else
        FrFormulasImpShow.FMatricialNovo := False;
        //
        if BDC_APRESENTACAO_IMPRESSAO = 1 then FrFormulasImpShow.FCalcCustos := True
          else FrFormulasImpShow.FCalcCustos := False;
        if BDC_APRESENTACAO_IMPRESSAO = 2 then FrFormulasImpShow.FCalcPrecos := True
          else FrFormulasImpShow.FCalcPrecos := False;
        //if RBkg.Checked then FrFormulasImpShow.FPesoCalc := 0.01
        //else
        FrFormulasImpShow.FPesoCalc := 10;
        //
        if FrFormulasImpShow.FLinhasRebx = '' then
          FrFormulasImpShow.FMedia := FrFormulasImpShow.FEMCM
        else begin
          if FrFormulasImpShow.FQtde > 0 then FrFormulasImpShow.FMedia := FrFormulasImpShow.FPeso / FrFormulasImpShow.FQtde
          else FrFormulasImpShow.FMedia := 0;
        end;
        //
        FrFormulasImpShow.CalculaReceita(nil);
      end;
      1:
      begin
        FrFormulasImpShowNew.FTpReceita    := TpReceita;
        FrFormulasImpShowNew.FReImprime    := True;
        FrFormulasImpShowNew.FEmit         := QrEmit.FieldByName('Codigo').AsInteger;
        FrFormulasImpShowNew.FEmitGru      := QrEmit.FieldByName('EmitGru').AsInteger;
        FrFormulasImpShowNew.FVSMovCod     := QrEmit.FieldByName('VSMovCod').AsInteger;
        FrFormulasImpShowNew.FVSMovimSrc   := MovimNiv;
        FrFormulasImpShowNew.FTemIMEIMrt   := TemIMEIMrt;
        FrFormulasImpShowNew.FEmitGru_TXT  := QrEmit.FieldByName('NO_EmitGru').AsString;
        FrFormulasImpShowNew.FRetrabalho   := QrEmit.FieldByName('Retrabalho').AsInteger;
        FrFormulasImpShowNew.FSourcMP      := QrEmit.FieldByName('SourcMP').AsInteger;
        FrFormulasImpShowNew.FDataP        := QrEmit.FieldByName('DataEmis').AsDateTime;
        //FrFormulasImpShowNew.FDtCorrApo    := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
        FrFormulasImpShowNew.FDtCorrApo    := '????/??/??';
        FrFormulasImpShowNew.FEmpresa      := QrEmit.FieldByName('Empresa').AsInteger;
        FrFormulasImpShowNew.FProgressBar1 := PB1;
        FrFormulasImpShowNew.FTipoPreco    := -1;// n�o deve usar
        FrFormulasImpShowNew.FCliIntCod    := QrEmit.FieldByName('ClienteI').AsInteger;
        FrFormulasImpShowNew.FCliIntTxt    := QrEmit.FieldByName('NOMECI').AsString;
        FrFormulasImpShowNew.FFormula      := QrEmit.FieldByName('Numero').AsInteger;
        FrFormulasImpShowNew.FTipoImprime  := 0; //Visualiza! RGImprime.ItemIndex;
        FrFormulasImpShowNew.FPeso         := QrEmit.FieldByName('Peso').AsFloat;
        FrFormulasImpShowNew.FQtde         := QrEmit.FieldByName('Qtde').AsFloat;
        FrFormulasImpShowNew.FArea         := QrEmit.FieldByName('AreaM2').AsInteger;
        FrFormulasImpShowNew.FLinhasRebx   := QrEmit.FieldByName('Espessura').AsString;
        FrFormulasImpShowNew.FLinhasSemi   := QrEmit.FieldByName('SemiTxtEspReb').AsString;
        FrFormulasImpShowNew.FFulao        := QrEmit.FieldByName('Fulao').AsString;
        FrFormulasImpShowNew.FDefPeca      := QrEmit.FieldByName('DefPeca').AsString;
        FrFormulasImpShowNew.FHHMM_P       := dmkPF.HorasMH(QrEmit.FieldByName('TempoP').AsInteger, False);
        FrFormulasImpShowNew.FHHMM_R       := dmkPF.HorasMH(QrEmit.FieldByName('TempoR').AsInteger, False);
        FrFormulasImpShowNew.FHHMM_T       := dmkPF.HorasMH(QrEmit.FieldByName('TempoP').AsInteger + QrEmit.FieldByName('TempoR').AsInteger, False);
        FrFormulasImpShowNew.FObs          := QrEmit.FieldByName('Obs').AsString;
        FrFormulasImpShowNew.FSetor        := QrEmit.FieldByName('Setor').AsInteger;
        FrFormulasImpShowNew.FTipific      := QrEmit.FieldByName('Tipificacao').AsInteger;
        FrFormulasImpShowNew.FCod_Espess   := QrEmit.FieldByName('Cod_Espess').AsInteger;
        FrFormulasImpShowNew.FCodDefPeca   := QrEmit.FieldByName('CodDefPeca').AsInteger;
        FrFormulasImpShowNew.FSemiAreaM2   := QrEmit.FieldByName('SemiAreaM2').AsFloat;
        FrFormulasImpShowNew.FSemiRendim   := QrEmit.FieldByName('SemiRendim').AsInteger;
        FrFormulasImpShowNew.FBRL_USD      := QrEmit.FieldByName('BRL_USD').AsFloat;
        FrFormulasImpShowNew.FBRL_EUR      := QrEmit.FieldByName('BRL_EUR').AsFloat;
        FrFormulasImpShowNew.FDtaCambio    := QrEmit.FieldByName('DtaCambio').AsDateTime;
        //
        //if CkMatricial.Checked then FrFormulasImpShowNew.FMatricialNovo := True
        //else
        FrFormulasImpShowNew.FMatricialNovo := False;
        //
        if BDC_APRESENTACAO_IMPRESSAO = 1 then FrFormulasImpShowNew.FCalcCustos := True
          else
        FrFormulasImpShowNew.FCalcCustos := False;
        if BDC_APRESENTACAO_IMPRESSAO = 2 then FrFormulasImpShowNew.FCalcPrecos := True
          else FrFormulasImpShowNew.FCalcPrecos := False;
        //if RBkg.Checked then FrFormulasImpShowNew.FPesoCalc := 0.01
        //else
        FrFormulasImpShowNew.FPesoCalc := 10;
        //
        if FrFormulasImpShowNew.FLinhasRebx = '' then
          FrFormulasImpShowNew.FMedia := FrFormulasImpShowNew.FEMCM
        else begin
          if FrFormulasImpShowNew.FQtde > 0 then FrFormulasImpShowNew.FMedia := FrFormulasImpShowNew.FPeso / FrFormulasImpShowNew.FQtde
          else FrFormulasImpShowNew.FMedia := 0;
        end;
        //
        FrFormulasImpShowNew.CalculaReceita(nil);
      end;
      else
      begin
        Geral.MB_Erro('Vers�o de impress�o de receita de ribeira n�o implementada [4]!');
        Exit;
      end;
    end;
    Application.ProcessMessages;
  finally
    QrVMI.Free;
  end;
  finally
    QrEmit.Free;
  end;
end;

procedure TUnPQ_PF.TotalizaEmitSbt(EmitGru: Integer);
var
  SbtCouIntros, SbtAreaM2, SbtAreaP2, SbtPesoKg, SbtMediaKg,
  SbtMediaM2, SbtMediaP2, SbtEspMedLinReb, SbtPercEsperRend: Double;
  SQLType: TSQLType;
  //
  SbtEspMedLinWB, SbtEspMedLinTrpFator: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrEmitSbtQtd, Dmod.MyDB, [
  'SELECT SUM(CouIntros) CouIntros, SUM(AreaM2) AreaM2,   ',
  'SUM(AreaP2) AreaP2, SUM(PesoKg) PesoKg,   ',
  'SUM(FatorUmidade * PesoKg) fUmiKg,  ',
  'SUM(LinRebMed * AreaM2) / SUM(AreaM2) LinRebMed,  ',
  'SUM(PesoKg / FatorUmidade) / SUM(AreaM2) * 10 MedLinWB,  ',
  '  ',
  '(SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)   ',
  '*   ',
  '(1 + ((   ',
  '   (SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)   ',
  '    - 32) / 32 / 10   ',
  '   )   ',
  ') MedLinTrpFator,   ',
  '   ',
  '/**/   ',
  '   ',
  '(7.00 +   ',
  '  ((   ',
  '    ((SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)   ',
  '    *   ',
  '    (1 + ((   ',
  '    (SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)   ',
  '    - 32) / 32 / 10   ',
  '    ))   ',
  '  -32)*0.181818) / LinRebMed * 16.3   ',
  '   ',
  ')   ',
  ') MinPrcRend,   ',
  '/**/   ',
  '  ',
  '  ',
  '  ',
  '1 +   ',
  ' (   ',
  '   (   ',
  '     (SUM(PesoKg / FatorUmidade) / 0.75  / SUM(AreaM2) * 10)   ',
  '      -   ',
  '      32   ',
  '    )   ',
  '  / 32 / 10   ',
  ') fator   ',
  '  ',
  '  ',
  'FROM emitsbtqtd  ',
  'WHERE Codigo=' + Geral.FF0(EmitGru),
  '']);

    //
  SQLType           := stUpd;
  SbtCouIntros      := Dmod.QrEmitSbtQtdCouIntros.Value;
  SbtAreaM2         := Dmod.QrEmitSbtQtdAreaM2.Value;
  SbtAreaP2         := Dmod.QrEmitSbtQtdAreaP2.Value;
  SbtPesoKg         := Dmod.QrEmitSbtQtdPesoKg.Value;
  if SbtCouIntros > 0 then
  begin
    SbtMediaKg        := SbtPesoKg / SbtCouIntros;
    SbtMediaM2        := SbtAreaM2 / SbtCouIntros;
    SbtMediaP2        := SbtAreaP2 / SbtCouIntros;
  end else
  begin
    SbtMediaKg        := 0.000;
    SbtMediaM2        := 0.00;
    SbtMediaP2        := 0.00;
  end;
  SbtEspMedLinReb      := Dmod.QrEmitSbtQtdLinRebMed.Value;
  SbtEspMedLinTrpFator := Dmod.QrEmitSbtQtdMedLinTrpFator.Value;
  SbtPercEsperRend     := Dmod.QrEmitSbtQtdMinPrcRend.Value;
  SbtEspMedLinWB       := Dmod.QrEmitSbtQtdLinRebMed.Value;

  //

  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'emitgru', False, [
  'SbtCouIntros',
  'SbtAreaM2', 'SbtAreaP2', 'SbtPesoKg',
  'SbtMediaKg', 'SbtMediaM2', 'SbtMediaP2',
  'SbtEspMedLinReb', 'SbtEspMedLinWB', 'SbtEspMedLinTrpFator',
  'SbtPercEsperRend'], [
  'Codigo'], [
  SbtCouIntros,
  SbtAreaM2, SbtAreaP2, SbtPesoKg,
  SbtMediaKg, SbtMediaM2, SbtMediaP2,
  SbtEspMedLinReb, SbtEspMedLinWB, SbtEspMedLinTrpFator,
  SbtPercEsperRend], [
  EmitGru], True) then
  begin
    //
  end;
end;

function TUnPQ_PF.TransfereReceitaDeArquivo(Orientacao: TOrientTabArqBD; Numero,
  Motivo: Integer): Boolean;
var
  Dta, Destino, Origem: String;
  //
  function ExcluiReceita_EnviaArquivoMorto(Numero, Motivo: Integer;
    Query: TmySQLQuery; DataBase: TmySQLDatabase): Boolean;
  var
    CamposZ, SQL_DELETE: String;
    QtdReg1: Integer;
  begin
    if Orientacao = TOrientTabArqBD.stabdAcessivelToVersoes then
      SQL_DELETE := ''
    else
      SQL_DELETE := DELETE_FROM + Origem + sLineBreak + 'WHERE Numero=' + Geral.FF0(Numero) + ';';

    CamposZ := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, Destino, '', QtdReg1);
    case Orientacao of
      TOrientTabArqBD.stabdAcessivelToMorto,
      TOrientTabArqBD.stabdAcessivelToVersoes:
      begin
        CamposZ := Geral.Substitui(CamposZ,
          ', DataDel', ', "' + Dta + '" DataDel');
        CamposZ := Geral.Substitui(CamposZ,
          ', UserDel', ', ' + FormatFloat('0', VAR_USUARIO) + ' UserDel');
        CamposZ := Geral.Substitui(CamposZ,
          ', MotvDel', ', ' + FormatFloat('0', Motivo) + ' MotvDel');
      end;
    end;
    //
    CamposZ := 'INSERT INTO ' + Destino + ' SELECT ' + sLineBreak +
    CamposZ + sLineBreak +
    'FROM ' + Origem + sLineBreak +
    'WHERE Numero=' + Geral.FF0(Numero) + ';';
    //
    CamposZ := CamposZ + sLineBreak +
    //DELETE_FROM + Origem + sLineBreak + 'WHERE Numero=' + Geral.FF0(Numero) + ';';
    SQL_DELETE;
    //
    Query.SQL.Text := 'SET sql_mode = ""; ' + CamposZ;
    //
    if Query.Database <> DataBase then
      Query.Database := DataBase;
    //
    UMyMod.ExecutaQuery(Query);
     //
    Result := True;
  end;
begin
  Result := False;
  if Orientacao <> TOrientTabArqBD.stabdAcessivelToVersoes then
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
  Dta := Geral.FDT(DModG.ObtemAgora(), 105);
  //Numero := TbFormulasNumero.Value;
  //
  // fazer os itens primeiro...
  //Origem := 'FormulasIts';
  //Destino := 'FormulazIts';
  case Orientacao of
    TOrientTabArqBD.stabdAcessivelToMorto:
    begin
      Origem  := 'FormulasIts';
      Destino := 'FormulazIts';
    end;
    TOrientTabArqBD.stabdMortoToAcessivel:
    begin
      Origem  := 'FormulazIts';
      Destino := 'FormulasIts';
    end;
    TOrientTabArqBD.stabdAcessivelToVersoes:
    begin
      Origem  := 'FormulasIts';
      Destino := 'FormulaVIts';
    end;
    else
    begin
      Origem  := 'Formula?Its';
      Destino := 'Formula?Its';
    end;
  end;

  ExcluiReceita_EnviaArquivoMorto(Numero, Motivo, Dmod.QrUpd, Dmod.MyDB);
  // ... somente depois o cabecalho
  //TabAtivo := 'Formulas';
  //TabMorto := 'Formulaz';
  case Orientacao of
    TOrientTabArqBD.stabdAcessivelToMorto:
    begin
      Origem  := 'Formulas';
      Destino := 'Formulaz';
    end;
    TOrientTabArqBD.stabdMortoToAcessivel:
    begin
      Origem  := 'Formulaz';
      Destino := 'Formulas';
    end;
    TOrientTabArqBD.stabdAcessivelToVersoes:
    begin
      Origem  := 'Formulas';
      Destino := 'FormulaV';
    end;
    else
    begin
      Origem  := 'Formula?';
      Destino := 'Formula?';
    end;
  end;
  ExcluiReceita_EnviaArquivoMorto(Numero, Motivo, Dmod.QrUpd, Dmod.MyDB);
  //
  Result := True;
{
var
  Dta: String;
  Numero: Integer;
  //
  function ExcluiReceita_EnviaArquivoMorto(Numero, Motivo: Integer;
    Query: TmySQLQuery; DataBase: TmySQLDatabase): Boolean;
  var
    CamposZ: String;
  begin
    CamposZ := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabMorto, '');
    CamposZ := Geral.Substitui(CamposZ,
      ', DataDel', ', "' + Dta + '" DataDel');
    CamposZ := Geral.Substitui(CamposZ,
      ', UserDel', ', ' + FormatFloat('0', VAR_USUARIO) + ' UserDel');
    CamposZ := Geral.Substitui(CamposZ,
      ', MotvDel', ', ' + FormatFloat('0', Motivo) + ' MotvDel');
    //
    CamposZ := 'INSERT INTO ' + TabMorto + ' SELECT ' + sLineBreak +
    CamposZ + sLineBreak +
    'FROM ' + TabAtivo + sLineBreak +
    'WHERE Numero=' + Geral.FF0(Numero) + ';';
    //
    CamposZ := CamposZ + sLineBreak +
    DELETE_FROM + TabAtivo + sLineBreak +
      'WHERE Numero=' + Geral.FF0(Numero) + ';';
    //
    Query.SQL.Text := 'SET sql_mode = ""; ' + CamposZ;
    //
    if Query.Database <> DataBase then
      Query.Database := DataBase;
    //
    UMyMod.ExecutaQuery(Query);
     //
    Result := True;
  end;
const
  Motivo = 0;
begin
  case Destino of
    TStatusTabArqBD.stabdAcessivel:
    begin
      Tab
    end;
    TStatusTabArqBD.stabdMorto:
    begin
    end;
    TStatusTabArqBD.stabdAcessivel:
    begin
    end;
  end;
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  Dta := Geral.FDT(DModG.ObtemAgora(), 105);
  Numero := TbFormulasNumero.Value;
  //
  // fazer os itens primeiro...
  TabAtivo := 'FormulasIts';
  TabMorto := 'FormulazIts';
  ExcluiReceita_EnviaArquivoMorto(Numero, Motivo, Dmod.QrUpd, Dmod.MyDB);
  // ... somente depois o cabecalho
  TabAtivo := 'Formulas';
  TabMorto := 'Formulaz';
  ExcluiReceita_EnviaArquivoMorto(Numero, Motivo, Dmod.QrUpd, Dmod.MyDB);
  //
  TbFormulas.Refresh;
  TbFormulas.Last;
}
end;

procedure TUnPQ_PF.ReimprimePesagemNovo(Emit, SetrEmi: Integer; TpReceita: TReceitaTipoSetor; PB: TProgressBar);
const
  TempTable = False;
  ModeloImp = -1;
begin
  //case TTipoReceitas(QrEmitSetrEmi.Value) of
  case TTipoReceitas(SetrEmi) of
    dmktfrmSetrEmi_INDEFIN: //CO_SetrEmi_INDEFIN = 0;
    begin
      Geral.MB_Aviso('Tipo de setor de baixa por receita indefinido!');
    end;
    dmktfrmSetrEmi_MOLHADO: //CO_SetrEmi_MOLHADO = 1;
    begin
      //MostraEdicao(1, stPsq, 0);
      //PQ_PF.ReimprimePesagem(QrEmitCodigo.Value, ProgressBar1);
      ReimprimePesagemSomeWet(Emit, TpReceita, PB);
    end;
    dmktfrmSetrEmi_ACABATO: //CO_SetrEmi_ACABATO = 2;
    begin
      DmModEmit.MostraReceitaDeAcabamento(Emit, ModeloImp, TempTable);
    end;
  end;
end;

{
procedure TUnPQ_PF.ReopenPQOVS(QrPQO: TmySQLQuery; MovimCod, Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQO, Dmod.MyDB, [
  'SELECT lse.Nome NOMESETOR, ',
  'emg.Nome NO_EMITGRU, pqo.* ',
  'FROM pqo pqo ',
  'LEFT JOIN listasetores lse ON lse.Codigo=pqo.Setor ',
  'LEFT JOIN emitgru emg ON emg.Codigo=pqo.EmitGru ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  if Codigo <> 0 then
    QrPQO.Locate('Codigo', Codigo, []);
end;

procedure TUnPQ_PF.ReopenPQOIts(QrPQOIts: TmySQLQuery; Codigo, Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQOIts, Dmod.MyDB, [
  'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  ',
  'pqg.Nome NOMEGRUPO ',
  'FROM pqx pqx ',
  'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo ',
  'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ ',
  'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
  'AND pqx.OrigemCodi=' + Geral.FF0(Codigo),
  '']);
  if Controle <> 0 then
    QrPQOIts.Locate('OrigemCtrl', Controle, []);
end;
}

function TUnPQ_PF.VerificaEquiparacaoEstoque(Mostra: Boolean): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
      'DROP TABLE IF EXISTS _pq_compara_estq_nfs_; ',
      'CREATE TABLE _pq_compara_estq_nfs_ ',
      'SELECT pqx.CliOrig, pqx.Insumo,  ',
      'SUM(pqx.SdoPeso) SdoPeso ',
      'FROM ' + TMeuDB + '.pqx pqx ',
      'WHERE pqx.SdoPeso>0 ',
      'GROUP BY pqx.CliOrig, pqx.Insumo; ',
      'SELECT pcl.Peso, pqx.* ',
      'FROM _pq_compara_estq_nfs_ pqx, ' + TMeuDB + '.pqcli pcl  ',
      'WHERE  pcl.CI=pqx.CliOrig ',
      'AND pcl.PQ=pqx.Insumo  ',
      'AND pcl.Peso <> pqx.SdoPeso ',
      '']);
    //Geral.MB_Teste(Qry.SQL.Text);
    Result := Qry.RecordCount = 0;
    if (Result = False) and Mostra then
    begin
      if DBCheck.CriaFm(TFmPQwPQEItsPos, FmPQwPQEItsPos, afmoLiberado) then
      begin
        //
        FmPQwPQEItsPos.ShowModal;
        FmPQwPQEItsPos.Destroy;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnPQ_PF.AtualizaItensProcesso(Codigo: Integer; DataBase: TmySQLDatabase);
var
  QrySum, QryUpd: TmySQLQuery;
  Fator: Double;
begin
  QrySum := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrySum, DataBase, [
      'SELECT SUM(GramasTi) GramasTi ',
      'FROM tintasits ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    //
    if QrySum.FieldByName('GramasTi').AsFloat = 0 then
      Fator := 0
    else
      Fator := 1000 / QrySum.FieldByName('GramasTi').AsFloat;
    //
    QryUpd.SQL.Clear;
    QryUpd.Database := DataBase;
    QryUpd.SQL.Add('UPDATE tintastin SET GramasTi=:P0 WHERE Codigo=:P1');
    QryUpd.Params[00].AsFloat   := QrySum.FieldByName('GramasTi').AsFloat;
    QryUpd.Params[01].AsInteger := Codigo;
    QryUpd.ExecSQL;
    //
    QryUpd.SQL.Clear;
    QryUpd.Database := DataBase;
    QryUpd.SQL.Add('UPDATE tintasits SET GramasKg=GramasTi*:P0 ');
    QryUpd.SQL.Add('WHERE Codigo=:P1');
    QryUpd.Params[00].AsFloat   := Fator;
    QryUpd.Params[01].AsInteger := Codigo;
    QryUpd.ExecSQL;
  finally
    QrySum.Free;
    QryUpd.Free;
  end;
end;

end.
